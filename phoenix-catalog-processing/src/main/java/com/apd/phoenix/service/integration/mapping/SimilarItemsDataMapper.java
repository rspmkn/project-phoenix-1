package com.apd.phoenix.service.integration.mapping;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.cxf.jaxrs.impl.MetadataMap;
import com.apd.phoenix.service.model.dto.ItemRelationshipDto;
import com.apd.phoenix.service.rest.cxf.ItemSyncEndpoint;
import com.ussco.oagis._0.SyncProductRelationshipType;
import com.ussco.oagis._0.ProductRelationshipType;
import com.ussco.oagis._0.RelationshipType;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;

public class SimilarItemsDataMapper implements Processor {

    public static final String USSCO_VENDOR_NAME = "USSCO";
    public static final String USSCO_IMAGE_URL_BASE = "http://content.oppictures.com/Master_Images/Master_Variants/";
    public static final String USSCO_THUMBNAIL_IMAGE_URL = USSCO_IMAGE_URL_BASE + "/Variant_100";
    public static final String USSCO_STANDARD_IMAGE_URL = USSCO_IMAGE_URL_BASE + "/Variant_240";
    public static final String USSCO_LARGE_IMAGE_URL = USSCO_IMAGE_URL_BASE + "/Variant_400";
    public static final String USSCO_ZOOM_IMAGE_URL = USSCO_IMAGE_URL_BASE + "/Variant_1500";

    private void extractRelationshipData(Map<String, MetadataMap<String, ItemRelationshipDto>> map,
            SyncProductRelationshipType itemRelationshipMaster) {
        for (ProductRelationshipType relationships : itemRelationshipMaster.getDataArea().getProductRelationship()) {
            String vendorSku = relationships.getPrefixNumber() + relationships.getStockNumberButted();
            MetadataMap<String, ItemRelationshipDto> value = new MetadataMap<String, ItemRelationshipDto>();
            for (RelationshipType relationship : relationships.getRelationship()) {
                String name = relationship.getName();
                for (RelationshipType.RelationshipMember member : relationship.getRelationshipMember()) {
                    if (!map.containsKey(vendorSku)) {
                        map.put(vendorSku, value);
                    }
                    if (!value.containsKey(name)) {
                        value.put(name, new ArrayList<ItemRelationshipDto>());
                    }
                    ItemRelationshipDto relationshipPojo = new ItemRelationshipDto();
                    try {
                        relationshipPojo.setPosition(Integer.parseInt(member.getPosition()));
                    }
                    catch (NumberFormatException e) {
                        //bad number format
                        relationshipPojo.setPosition(0);
                    }
                    relationshipPojo.setSku(member.getPrefixNumber() + member.getStockNumberButted());
                    relationshipPojo.setVendorName(USSCO_VENDOR_NAME);
                    value.get(name).add(relationshipPojo);
                }
            }
        }
    }

    @Override
    public void process(Exchange exchng) throws Exception {
        Map<String, MetadataMap<String, ItemRelationshipDto>> relationshipMap = new HashMap<>();
        SyncProductRelationshipType relationshipData = (SyncProductRelationshipType) exchng.getIn().getBody();
        extractRelationshipData(relationshipMap, relationshipData);        
        List<MetadataMap<String, ItemRelationshipDto>> postList = new ArrayList<>();
        for (String itemSku : relationshipMap.keySet()) {

            MetadataMap<String, ItemRelationshipDto> toPost = relationshipMap.get(itemSku);
            List<ItemRelationshipDto> relationshipToAdd = new ArrayList<ItemRelationshipDto>();
            ItemRelationshipDto originalRelationship = new ItemRelationshipDto();
            originalRelationship.setPosition(0);
            originalRelationship.setSku(itemSku);
            originalRelationship.setVendorName(SimilarItemsDataMapper.USSCO_VENDOR_NAME);
            relationshipToAdd.add(originalRelationship);
            toPost.put(ItemSyncEndpoint.ORIGINAL_ITEM_RELATIONSHIP, relationshipToAdd);
            postList.add(toPost);
        }
        
        exchng.getOut().setHeaders(exchng.getIn().getHeaders());
        exchng.getOut().setBody(postList);
    }
}