package com.apd.phoenix.service.integration.routes;

import org.apache.camel.LoggingLevel;
import com.apd.phoenix.service.integration.camel.processor.catalog.UsscoRelationshipProcessor;
import com.apd.phoenix.service.integration.multitenancy.AbstractTenantAwareRouteBuilder;

public class UsscoRelationshipDataImportRouteBuilder extends AbstractTenantAwareRouteBuilder {

    private String usscoRelationshipCSVSource;

    @Override
    public void configureRouteImplementation() throws Exception {
        onException(Exception.class).maximumRedeliveries(0).handled(true).log(LoggingLevel.ERROR,
                "Error in catalog processing: ${exception.stacktrace}");

        from(usscoRelationshipCSVSource).bean(UsscoRelationshipProcessor.class, "addRelationship");

    }

    public String getUsscoRelationshipCSVSource() {
        return usscoRelationshipCSVSource;
    }

    public void setUsscoRelationshipCSVSource(String brandCSVSource) {
        this.usscoRelationshipCSVSource = brandCSVSource;
    }

}
