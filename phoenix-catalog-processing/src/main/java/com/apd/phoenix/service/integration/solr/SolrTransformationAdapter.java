package com.apd.phoenix.service.integration.solr;

import com.apd.phoenix.solr.transformers.ClassificationIcons;
import com.apd.phoenix.solr.transformers.CombineNameValuePriorityPair;
import com.apd.phoenix.solr.transformers.ParseDateValues;
import com.apd.phoenix.solr.transformers.ParsePropertyIcons;
import com.apd.phoenix.solr.transformers.ParseSkus;
import java.util.Map;

public class SolrTransformationAdapter {

    private SolrTransformationAdapter() {
    }

    private static final ClassificationIcons CLASSIFICATION_ICON_TRANSFORM = new ClassificationIcons();
    private static final CombineNameValuePriorityPair COMBINE_NAMEVALPRIORITY_TRANSFORM = new CombineNameValuePriorityPair();
    private static final ParseDateValues PARSE_DATE_TRANSFORM = new ParseDateValues();
    private static final ParsePropertyIcons PROPERTY_ICON_TRANSFORM = new ParsePropertyIcons();
    private static final ParseSkus PARSE_SKU_TRANSFORM = new ParseSkus();

    public static void classificationIcon(Map<String, Object> row) {
        CLASSIFICATION_ICON_TRANSFORM.transformRow(row);
    }

    public static void nameValuePriority(Map<String, Object> row) {
        COMBINE_NAMEVALPRIORITY_TRANSFORM.transformRow(row);
    }

    public static void date(Map<String, Object> row) {
        PARSE_DATE_TRANSFORM.transformRow(row);
    }

    public static void propertyIcon(Map<String, Object> row) {
        PROPERTY_ICON_TRANSFORM.transformRow(row);
    }

    public static void sku(Map<String, Object> row, Map<Long, String> skuTypemap) {
        PARSE_SKU_TRANSFORM.transformRow(row, skuTypemap);
    }
}
