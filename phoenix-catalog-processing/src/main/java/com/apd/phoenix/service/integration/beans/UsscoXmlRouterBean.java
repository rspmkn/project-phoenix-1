package com.apd.phoenix.service.integration.beans;

import java.util.Properties;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.camel.component.ActiveMQComponent;
import org.apache.activemq.pool.PooledConnectionFactory;
import org.apache.camel.CamelContext;
import org.apache.camel.component.cdi.CdiCamelContext;
import org.apache.camel.component.jms.JmsConfiguration;
import org.apache.camel.impl.DefaultCamelContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.integration.routes.BrandTableDataImportRouteBuilder;
import com.apd.phoenix.service.integration.routes.CatalogDataImportRouteBuilder;
import com.apd.phoenix.service.integration.routes.UsscoRelationshipDataImportRouteBuilder;
import com.apd.phoenix.service.integration.utility.PropertiesLoader;

@Startup
@Singleton
public class UsscoXmlRouterBean {

    private static final Logger LOG = LoggerFactory.getLogger(UsscoXmlRouterBean.class);

    @Inject
    protected CdiCamelContext camelContext;

    @PostConstruct
    public void init() throws Exception {
        setup();
    }

    @PreDestroy
    public void destroy() throws Exception {
        camelContext.stop();
    }

    private void setup() throws Exception {

        Properties properties = PropertiesLoader.getAsProperties("camel.item.integration");

        setupAmq(camelContext, properties.getProperty("jmsBrokerUrl"));

        CatalogDataImportRouteBuilder catalogRouteBuilder = new CatalogDataImportRouteBuilder();
        catalogRouteBuilder.setRelationshipSource(properties.getProperty("relationshipProcessSource", ""));
        catalogRouteBuilder.setRelationshipTarget(properties.getProperty("relationshipProcessTarget", ""));
        catalogRouteBuilder.setItemSource(properties.getProperty("itemProcessSource", ""));
        catalogRouteBuilder.setItemTarget(properties.getProperty("itemProcessTarget", ""));

        camelContext.addRoutes(catalogRouteBuilder);

        BrandTableDataImportRouteBuilder brandTableDataImportRouteBuilder = new BrandTableDataImportRouteBuilder();
        brandTableDataImportRouteBuilder.setBrandCSVSource(properties.getProperty("brandProcessSource", ""));
        camelContext.addRoutes(brandTableDataImportRouteBuilder);

        UsscoRelationshipDataImportRouteBuilder usscoRelationshipDataImportRouteBuilder = new UsscoRelationshipDataImportRouteBuilder();
        usscoRelationshipDataImportRouteBuilder.setUsscoRelationshipCSVSource(properties.getProperty(
                "usscoRelationshipProcessSource", ""));
        camelContext.addRoutes(usscoRelationshipDataImportRouteBuilder);

        camelContext.start();
    }

    public void setupAmq(CamelContext context, String jmsBrokerUrl) {
        Properties amqProperties = PropertiesLoader.getAsProperties("amq");
        ActiveMQConnectionFactory activeMQConnectionFactory = new ActiveMQConnectionFactory();
        activeMQConnectionFactory.setBrokerURL(jmsBrokerUrl);
        PooledConnectionFactory pooledConnectionFactory = new PooledConnectionFactory();
        pooledConnectionFactory.setConnectionFactory(activeMQConnectionFactory);
        pooledConnectionFactory.setMaxConnections(Integer.parseInt(amqProperties.getProperty(getClass().getSimpleName()
                + ".maxConnections", "2")));
        JmsConfiguration jmsConfiguration = new JmsConfiguration();
        jmsConfiguration.setConnectionFactory(pooledConnectionFactory);
        jmsConfiguration.setConcurrentConsumers(Integer.parseInt(amqProperties.getProperty(getClass().getSimpleName()
                + ".concurrentConsumers", "2")));
        ActiveMQComponent activeMQComponent = new ActiveMQComponent();
        activeMQComponent.setConfiguration(jmsConfiguration);
        context.addComponent("activemq", activeMQComponent);
    }
}
