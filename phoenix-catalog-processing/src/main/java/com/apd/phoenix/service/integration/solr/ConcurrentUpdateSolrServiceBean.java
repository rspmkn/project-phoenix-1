/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.service.integration.solr;

import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import org.apache.solr.client.solrj.SolrServer;
import org.apache.solr.client.solrj.impl.ConcurrentUpdateSolrServer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.core.utility.EcommercePropertiesLoader;
import com.apd.phoenix.core.utility.PropertiesLoader;
import com.apd.phoenix.core.utility.TenantConfigRepository;
import com.apd.phoenix.service.persistence.multitenancy.impl.CurrentTenantIdentifierResolverImpl;

/**
 *
 * @author dnorris
 */
@Startup
@Singleton
@Lock(LockType.READ)
public class ConcurrentUpdateSolrServiceBean {

    private static final Logger LOG = LoggerFactory.getLogger(ConcurrentUpdateSolrServiceBean.class);

    //should be setup in EJB configuration.
    //TODO look into EJB ocnfiguration
    private static final String[] SOLR_UPDATE_SERVER_URLS = PropertiesLoader.getAsProperties("ecommerce").getProperty(
            "SOLR_UPDATE_SERVER_URL").split(",");
    private static final int SOLR_UPDATE_SERVER_QUEUE_SIZE = EcommercePropertiesLoader.getInstance()
            .getEcommerceProperties().getInt("SOLR_UPDATE_SERVER_QUEUE_SIZE", 10000);
    private static final int SOLR_UPDATE_SERVER_THREAD_COUNT = EcommercePropertiesLoader.getInstance()
            .getEcommerceProperties().getInt("SOLR_UPDATE_SERVER_THREAD_COUNT", 4);
    private Map<String, List<SolrServer>> solrServers;

    public SolrServer getSolrServer() {
        for (SolrServer server : this.solrServers.get(CurrentTenantIdentifierResolverImpl.getCurrentTenantSchema())) {
            try {
                server.ping();
                return server;
            }
            catch (Exception e) {
                LOG.warn("Lost connection with Solr server, turn on debug logging to view stack trace");
                if (LOG.isDebugEnabled()) {
                    LOG.debug("Stack trace", e);
                }
            }
        }
        LOG.error("Unable to connect to any Solr server");
        return null;
    }

    @PostConstruct
    @Lock(LockType.WRITE)
    private void setup() throws MalformedURLException {
        solrServers = new HashMap<>();
        for (String tenant : TenantConfigRepository.getInstance().getTenantNames()) {
            String collection;
            String identifier;
            List<SolrServer> solrServerList = new ArrayList<>();
        	identifier = TenantConfigRepository.getInstance().getTenantPropertiesByName(tenant).getSchemaName();
            collection = TenantConfigRepository.getInstance().getTenantPropertiesByName(tenant).getSolrCollection();
	        //per: http://wiki.apache.org/solr/Solrj#Directly_adding_POJOs_to_Solr
	        //String solrServerUrl, int queueSize, int threadCount
	        for (String url : SOLR_UPDATE_SERVER_URLS) {
	            try {
	            	solrServerList.add(new ConcurrentUpdateSolrServer(url + collection, SOLR_UPDATE_SERVER_QUEUE_SIZE,
	                        SOLR_UPDATE_SERVER_THREAD_COUNT));
	            }
	            catch (Exception e) {
	                LOG.warn("Error connecting to Solr server, turn on debug logging to view stack trace");
	                if (LOG.isDebugEnabled()) {
	                    LOG.debug("Stack trace", e);
	                }
	            }
	        }
	        solrServers.put(identifier, solrServerList);
        }
    }

    @PreDestroy
    @Lock(LockType.WRITE)
    private void destroy() {
        for (List<SolrServer> serverList : this.solrServers.values()) {
            for (SolrServer server : serverList) {
                try {
                    server.shutdown();
                }
                catch (Exception e) {
                    LOG
                            .warn("Error shutting down connection to solr server, turn on debug logging to view stack trace");
                    if (LOG.isDebugEnabled()) {
                        LOG.debug("Stack trace", e);
                    }
                }
            }
        }
    }
}
