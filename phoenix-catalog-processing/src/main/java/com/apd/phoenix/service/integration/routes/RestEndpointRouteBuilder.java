package com.apd.phoenix.service.integration.routes;

import org.apache.camel.Exchange;
import org.apache.camel.LoggingLevel;
import org.apache.camel.model.dataformat.JsonLibrary;
import com.apd.phoenix.service.integration.multitenancy.AbstractTenantAwareRouteBuilder;

/**
 * A Camel Java DSL Router
 */
public class RestEndpointRouteBuilder extends AbstractTenantAwareRouteBuilder {

    private String processSource = "";
    private String processTarget = "";

    /**
     * Let's configure the Camel routing rules using Java code...
     */
    @Override
    public void configureRouteImplementation() {

        onException(Exception.class).maximumRedeliveries(0).handled(true).log(LoggingLevel.INFO,
                "Error in catalog processing: ${exception.stacktrace}");

        from(processSource).setHeader(Exchange.CONTENT_TYPE, constant("application/json")).marshal().json(
                JsonLibrary.Jackson).to(processTarget);
    }

    public String getProcessSource() {
        return processSource;
    }

    public void setProcessSource(String processSource) {
        this.processSource = processSource;
    }

    public String getProcessTarget() {
        return processTarget;
    }

    public void setProcessTarget(String processTarget) {
        this.processTarget = processTarget;
    }

}