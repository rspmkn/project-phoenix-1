package com.apd.phoenix.service.integration.solr;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.solr.IntegrationAttributeConstants;

public class CatalogXItemDocumentMapper {

    private static final Logger LOG = LoggerFactory.getLogger(CatalogXItemDocumentMapper.class);

    private CatalogXItemDocumentMapper() {
    }

    /**
     * Maps between the item query results and item object.
     *
     * @param results
     * @param solrDocument
     */
    public static void mapBaseQueryResultsToSolrDocument(Map<String, Object> results, SolrDocumentPojo solrDocument) {
        if (results == null) {
            return;
        }
        solrDocument.setCatalogxitemId(getLongValue(results.get(IntegrationAttributeConstants.CATALOGXITEM_ID)));
        solrDocument.setName((String) results.get(IntegrationAttributeConstants.NAME));
        solrDocument.setCustomerSku((String) results.get(IntegrationAttributeConstants.CUSTOMER_SKU));
        solrDocument.setDescription((String) results.get(IntegrationAttributeConstants.DESCRIPTION));
        solrDocument.setCatalogId(getLongValue(results.get(IntegrationAttributeConstants.CATALOG_ID)));
        solrDocument.setPrice(getFloatValue(results.get(IntegrationAttributeConstants.PRICE)));
        solrDocument.setSearchTerms((String) results.get(IntegrationAttributeConstants.SEARCHTERMS));
        solrDocument.setProtectedSearchTerms((String) results.get(IntegrationAttributeConstants.PROTECTEDSEARCHTERMS));
        solrDocument.setCoreItemExpirationDate((Date) results.get(IntegrationAttributeConstants.COREEXPIREDATE));
        solrDocument.setCoreItemStartDate((Date) results.get(IntegrationAttributeConstants.CORESTARTDATE));
        solrDocument.setUnitOfMeasureName((String) results.get(IntegrationAttributeConstants.UOM_NAME));
        solrDocument.setManufacturerName((String) results.get(IntegrationAttributeConstants.MNAME));
        solrDocument.setCategory((String) results.get(IntegrationAttributeConstants.CATEGORY));
        solrDocument.setHierarchyPath((String) results.get(IntegrationAttributeConstants.HIERARCHY_PATH));
        solrDocument.setHierarchy_path_string((String) results.get(IntegrationAttributeConstants.HIERARCHY_PATH));
        solrDocument.setVendorName((String) results.get(IntegrationAttributeConstants.VENDOR_NAME));
        solrDocument.setItem_status((String) results.get(IntegrationAttributeConstants.STATUS));
        solrDocument.setMultiple(getStringFromValue(results.get(IntegrationAttributeConstants.MULTIPLE)));
        solrDocument.setMinimum(getStringFromValue(results.get(IntegrationAttributeConstants.MINIMUM)));
        solrDocument.setHierarchyParentId(getLongValue(results.get(IntegrationAttributeConstants.HIERARCHY_PARENT_ID)));
        solrDocument.setCustomerReplacementSku((String) results.get(IntegrationAttributeConstants.REP_SKU));
        solrDocument.setCustomerReplacementVendor((String) results.get(IntegrationAttributeConstants.REP_VEND_NAME));
        solrDocument.setCustomOrder((String) results.get(IntegrationAttributeConstants.CUSTOM_ORDER));
        solrDocument.setSpecialOrder((String) results.get(IntegrationAttributeConstants.SPECIAL_ORDER));
        String overridesString = (String) results.get(IntegrationAttributeConstants.OVERRIDE_CATALOGS);
        solrDocument.setFeaturedItem(getBooleanValue(results.get(IntegrationAttributeConstants.ISFEATUREDITEM)));
        solrDocument.setMostPopular(getBooleanValue(results.get(IntegrationAttributeConstants.MOST_POPULAR)));
        solrDocument.setBrandName((String) results.get(IntegrationAttributeConstants.BRAND_NAME));
        solrDocument.setBrandLogo((String) results.get(IntegrationAttributeConstants.BRAND_LOGO));
        List<String> overridesList = new ArrayList<String>();
        if (StringUtils.isNotBlank(overridesString)) {
            for (String override : overridesString.split(",")) {
                overridesList.add(override);
            }
        }
        solrDocument.setOverrides_id(overridesList);
    }

    public static Float getFloatValue(Object o) {
        if (o instanceof BigDecimal) {
            return ((BigDecimal) o).floatValue();
        }
        else {
            return null;
        }
    }

    public static Long getLongValue(Object o) {
        if (o instanceof BigInteger) {
            return ((BigInteger) o).longValue();
        }
        else if (o instanceof Long) {
            return (Long) o;
        }
        else {
            return null;
        }
    }

    public static Integer getIntegerValue(Object o) {
        if (o instanceof Integer) {
            return ((Integer) o);
        }
        else if (o instanceof Long) {
            return (Integer) o;
        }
        else {
            return null;
        }
    }

    public static Boolean getBooleanValue(Object o) {
        if (o instanceof BigInteger) {
            return (((BigInteger) o).intValue() == 1 ? true : false);
        }
        else {
            return false;
        }
    }

    public static void mapImageToItem(List<Map<String, Object>> results, SolrDocumentPojo solrDocument) {
        if (results == null) {
            return;
        }
        for (Map<String, Object> result : results) {
            if (StringUtils.isNotBlank((String) result.get("IMAGEURL"))) {
                String imageUrl = (String) result.get("IMAGEURL");
                solrDocument.getImageUrl().add(imageUrl);
            }
        }
    }

    public static void mapSellingPointsToItem(List<Map<String, Object>> results, SolrDocumentPojo solrDocument) {
        if (results == null) {
            return;
        }
        for (Map<String, Object> result : results) {
            if (StringUtils.isNotBlank((String) result.get("VALUE"))) {
                solrDocument.getSellingPoints().add((String) result.get("VALUE"));
            }
        }
    }

    public static void mapSkuToItem(List<Map<String, Object>> results, SolrDocumentPojo solrDocument, Map<Long, String> skuTypeMap) {
        if (results == null) {
            return;
        }
        List<String> crossReferenceSkus = new ArrayList<>();
        for (Map<String, Object> result : results) {
        	if (result != null && StringUtils.isNotBlank((String)result.get("VALUE"))) {
        		crossReferenceSkus.add(((String)result.get("VALUE")).toUpperCase());
        	}
            SolrTransformationAdapter.sku(result, skuTypeMap);
            if (StringUtils.isNotBlank((String) result.get("MANUFACTURER_SKU"))) {
                solrDocument.setManufacturerSku((String) result.get("MANUFACTURER_SKU"));
            }
            if (StringUtils.isNotBlank((String) result.get("APD_SKU"))) {
                solrDocument.setApdSku((String) result.get("APD_SKU"));
            }
            if (StringUtils.isNotBlank((String) result.get("VENDOR_SKU"))) {
                solrDocument.setVendorSku((String) result.get("VENDOR_SKU"));
            }
        }
        solrDocument.setCrossReferenceSkus(crossReferenceSkus);
    }

    public static void mapClassificationIconToItem(List<Map<String, Object>> results, SolrDocumentPojo solrDocument) {
        if (results == null) {
            return;
        }
        for (Map<String, Object> result : results) {
            if ("Green_Indicator".equals(result.get("NAME"))) {
                solrDocument.setGreen(StringUtils.defaultIfBlank((String) result.get("VALUE"), "unknown"));
            }
            SolrTransformationAdapter.classificationIcon(result);
            if (StringUtils.isNotBlank((String) result.get("classificationIconMap"))) {
                String classificationIconMapValue = (String) result.get("classificationIconMap");
                solrDocument.getClassificationIconMap().add(classificationIconMapValue);
            }
        }
    }

    public static void mapPropertyIconToItem(List<Map<String, Object>> results, SolrDocumentPojo solrDocument) {
        if (results == null) {
            return;
        }
        for (Map<String, Object> result : results) {
            SolrTransformationAdapter.propertyIcon(result);
            if (StringUtils.isNotBlank((String) result.get("propertyIconMap"))) {
                solrDocument.setPropertyIconMap((String) result.get("propertyIconMap"));
            }
        }
    }

    public static void mapCategoryXPropertyIconToItem(List<Map<String, Object>> results, SolrDocumentPojo solrDocument) {
        if (results == null) {
            return;
        }
        for (Map<String, Object> result : results) {
            SolrTransformationAdapter.propertyIcon(result);
            if (StringUtils.isNotBlank((String) result.get("catXPropertyIconMap"))) {
                String catXPropertyIconMapValues = (String) result.get("catXPropertyIconMap");
                solrDocument.getCatXPropertyIconMap().add(catXPropertyIconMapValues);
            }
            if (StringUtils.isNotBlank((String) result.get("ItemPropertyNameValuePair"))) {
                String itemPropertyNameValuePairValues = (String) result.get("ItemPropertyNameValuePair");
                solrDocument.getItemPropertyNameValuePair().add(itemPropertyNameValuePairValues);
            }
        }
    }

    public static void mapPropertyToItem(List<Map<String, Object>> results, SolrDocumentPojo solrDocument) {
        if (results == null) {
            return;
        }
        for (Map<String, Object> result : results) {
            SolrTransformationAdapter.nameValuePriority(result);
            if (StringUtils.isNotBlank((String) result.get("filters"))) {
                String filtersValue = (String) result.get("filters");
                String[] splitString = filtersValue.split(" \\| ");
                if (splitString.length == 2 && !"null".equals(splitString[1])) {
                    if (splitString[0] != null && splitString[0].equals("Global Product Type")) {
                        solrDocument.setGlobalProductType(splitString[1]);
                    }
                    if (splitString[0] != null && splitString[0].equals("Market Indicator (Cartridge Number)")) {
                        solrDocument.setCartridgeNumber(splitString[1]);
                    }
                    solrDocument.getFilters().add(filtersValue);
                }
            }
            if (StringUtils.isNotBlank((String) result.get("filtersPriority"))) {
                String filtersPriorityValue = (String) result.get("filtersPriority");
                String[] splitString = filtersPriorityValue.split(" \\| ");
                if (splitString.length == 2 && !"null".equals(splitString[1])) {
                    solrDocument.getFiltersPriority().add(filtersPriorityValue);
                }
            }
        }
    }

    public static void mapFavoritesListResultsToItem(List<Map<String, Object>> results, SolrDocumentPojo solrDocument) {
        if (results == null) {
            return;
        }
        for (Map<String, Object> result : results) {
            String companyFavoritesListIdValues = result.get("FAVORITES_LIST_ID") == null ? "" : ((BigInteger) result
                    .get("FAVORITES_LIST_ID")).toString();
            if (StringUtils.isNotBlank(companyFavoritesListIdValues)) {
                solrDocument.getCompanyFavoritesListIds().add(companyFavoritesListIdValues);
            }
        }
    }

    public static void mapMatchbookResultsToItem(List<Map<String, Object>> results, SolrDocumentPojo solrDocument) {
        if (results == null) {
            return;
        }
        for (Map<String, Object> result : results) {
            String matchBookIds = result.get("MB_ID") == null ? "" : ((BigInteger) result.get("MB_ID")).toString();
            if (StringUtils.isNotBlank(matchBookIds)) {
                solrDocument.getMb_id().add(matchBookIds);
            }
            if (StringUtils.isNotBlank((String) result.get("MODEL"))) {
                String modelNumber = (String) result.get("MODEL");
                solrDocument.getModelNumber().add(modelNumber);
            }
        }
    }

    public static String getStringFromValue(Object o) {
        if (o == null) {
            return null;
        }
        return o.toString();
    }

}
