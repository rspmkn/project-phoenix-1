package com.apd.phoenix.service.integration.routes;

import org.apache.camel.LoggingLevel;
import com.apd.phoenix.service.integration.camel.processor.catalog.BrandListProcessor;
import com.apd.phoenix.service.integration.multitenancy.AbstractTenantAwareRouteBuilder;

public class BrandTableDataImportRouteBuilder extends AbstractTenantAwareRouteBuilder {

    private String brandCSVSource;

    @Override
    public void configureRouteImplementation() throws Exception {
        onException(Exception.class).maximumRedeliveries(0).handled(true).log(LoggingLevel.ERROR,
                "Error in catalog processing: ${exception.stacktrace}");

        from(brandCSVSource).convertBodyTo(String.class, "windows-1252").log(LoggingLevel.TRACE, "${body}").bean(
                BrandListProcessor.class, "csvToBrands").split(body()).bean(BrandListProcessor.class, "updateBrand");

    }

    public String getBrandCSVSource() {
        return brandCSVSource;
    }

    public void setBrandCSVSource(String brandCSVSource) {
        this.brandCSVSource = brandCSVSource;
    }

}
