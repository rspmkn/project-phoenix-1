package com.apd.phoenix.service.integration.camel.processor.catalog;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.annotation.Resource;
import javax.ejb.EJBException;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.inject.Inject;
import javax.transaction.UserTransaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.business.CatalogBp;
import com.apd.phoenix.service.business.CatalogBp.SyncAction;
import com.apd.phoenix.service.business.CatalogBp.SyncError;
import com.apd.phoenix.service.business.CatalogUploadBp;
import com.apd.phoenix.service.business.CatalogXItemBp;
import com.apd.phoenix.service.business.ItemBp;
import com.apd.phoenix.service.business.ItemFailureException;
import com.apd.phoenix.service.business.ItemRollbackException;
import com.apd.phoenix.service.catalog.CatalogChange;
import com.apd.phoenix.service.catalog.CatalogChangeResult;
import com.apd.phoenix.service.catalog.CatalogResultAggregator;
import com.apd.phoenix.service.model.Catalog;
import com.apd.phoenix.service.model.SyncItemResult;
import com.apd.phoenix.service.model.SyncItemResultSummary;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class CatalogUpdateMessageProcessor {

    private static final Logger LOGGER = LoggerFactory.getLogger(CatalogUpdateMessageProcessor.class);

    private static final int MAX_ITEM_PROCESS_ATTEMPTS = 5;

    private static final int TIMEOUT_IN_HOURS = 2;

    @Inject
    private CatalogBp catalogBp;

    @Inject
    private CatalogResultAggregator catalogResultAggregator;

    @Inject
    private ItemBp itemBp;

    @Inject
    private CatalogXItemBp catalogXItemBp;

    @Inject
    private CatalogUploadBp catalogUploadBp;

    //bean-managed transactions are used, since container-managed transactions don't really
    //work with EJBs called by camel, and using an EJB via camel is more performant than
    //making a REST call
    @Resource
    private UserTransaction tx;

    public void process(ArrayList<CatalogChange> catalogChanges) {
        Long catalogId = null;
        Long oldCatalogId = null;
        if (catalogChanges != null && !catalogChanges.isEmpty()) {
        	catalogId = catalogChanges.get(0).getCatalogId();
        	oldCatalogId = catalogChanges.get(0).getOldCatalogId();
        }
        Catalog catalog = null;
        Catalog oldCatalog = null;
        try {
            tx.begin();
            if (catalogId != null) {
                catalog = catalogBp.findById(catalogId, Catalog.class);
            }
            if (oldCatalogId != null) {
                oldCatalog = catalogBp.findById(oldCatalogId, Catalog.class);
            }
            tx.commit();
        }
        catch (Exception e) {
            try {
                LOGGER.warn("Rolling back lookup transaction", e);
                tx.rollback();
            }
            catch (Exception e1) {
                LOGGER.error("Unable to complete transaction", e1);
            }
        }
        
        ArrayList<CatalogChangeResult> catalogChangeResults = new ArrayList<>();
        SyncItemResultSummary summary = new SyncItemResultSummary();
        for (CatalogChange catalogChange : catalogChanges) {
        	CatalogChangeResult catalogChangeResult = null;
        	//for each item, attempts to process it using syncItem
        	//if an unexpected failure occurs, retries the process a number of times
        	//equal to MAX_ITEM_PROCESS_ATTEMPTS
        	int attempts = 0;
        	boolean lastProcessSucceeded = false;
        	//note: within this loop, no DB actions should be performed unless
        	//the CSV upload should be persisted
        	while (attempts < MAX_ITEM_PROCESS_ATTEMPTS && !lastProcessSucceeded) {
        		attempts++;
        		catalogChangeResult = this.processChange(catalogChange, catalog, oldCatalog, summary);
        		//if there is an unexpected failure, the process is retried
        		//(up to the max)
        		lastProcessSucceeded = !catalogChangeResult.getUnexpectedException();
        	}
        	catalogChangeResults.add(catalogChangeResult);
        }

        try {
            tx.begin();
            catalogResultAggregator.process(summary, catalogChangeResults);
            tx.commit();
        }
        catch (Exception e) {
            try {
                LOGGER.warn("Rolling back aggregator transaction", e);
                tx.rollback();
            }
            catch (Exception e1) {
                LOGGER.error("Unable to complete transaction", e1);
            }
        }
    }

    private CatalogChangeResult processChange(CatalogChange catalogChange, Catalog catalog, Catalog oldCatalog,
            SyncItemResultSummary summary) {
        Map<String, String> itemData = catalogChange.getItemData();
        SyncAction action = catalogChange.getAction();
        Boolean persistChanges = catalogChange.getPersistChanges();
        Boolean unexpectedException = false;
        SyncItemResult result = null;
        String correlationId = catalogChange.getCorrelationId();
        Long totalItems = catalogChange.getTotalItems();
        boolean forCustomerCatalog = catalogChange.isForCustomerCatalog();

        try {
            if (persistChanges) {
                tx.begin();
                if (!forCustomerCatalog) {
                    result = this.itemBp.syncItem(itemData, catalog, action, summary, catalogChange.getPropertyTypes());
                }
                else {
                    result = this.catalogXItemBp.syncItem(itemData, catalog, oldCatalog, action, summary, catalogChange
                            .getPropertyTypes());
                }
                tx.commit();
            }
            else {
                if (!forCustomerCatalog) {
                    result = this.itemBp
                            .checkItem(itemData, catalog, action, summary, catalogChange.getPropertyTypes());
                }
                else {
                    result = this.catalogXItemBp.checkItem(itemData, catalog, action, summary, catalogChange
                            .getPropertyTypes(), catalogChange.getPricingTypes());
                }
            }
        }
        catch (Exception e) {
            try {
                tx.rollback();
            }
            catch (Exception e1) {
                LOGGER.error("Unable to complete transaction", e1);
            }
            if (e instanceof EJBException && ((EJBException) e).getCause() instanceof ItemRollbackException) {
                result = ((ItemRollbackException) ((EJBException) e).getCause()).getResult();
            }
            else if (e instanceof ItemRollbackException) {
                result = ((ItemRollbackException) e).getResult();
            }
            else {
                if (e instanceof ItemFailureException) {
                    e = ((ItemFailureException) e).getException();
                }
                //catches any unexpected Java error
                LOGGER.warn("Exception! Turn on debug to view stack trace. " + e.toString());
                if (LOGGER.isDebugEnabled()) {
                    LOGGER.debug("Stack trace", e);
                }
                unexpectedException = true;
                result = new SyncItemResult();
                if (itemData.containsKey(ItemBp.APD_SKU_HEADER)) {
                    result.setResultApdSku(itemData.get(ItemBp.APD_SKU_HEADER));
                }
                else {
                    result.setResultApdSku("ERROR");
                }
                if (itemData.containsKey(ItemBp.VENDOR_HEADER)) {
                    result.setResultVendor(itemData.get(ItemBp.VENDOR_HEADER));
                }
                else {
                    result.setResultVendor("ERROR");
                }

                result.getErrorSet().add(SyncError.UNEXPECTED_ERROR.getLabel() + e.toString());
                summary.incrementErrors();
            }
            summary.incrementFailures();
        }
        Long catalogId = (catalog != null ? catalog.getId() : null);
        Long oldCatalogId = (oldCatalog != null ? oldCatalog.getId() : null);
        return new CatalogChangeResult(summary, result, correlationId, catalogId, oldCatalogId, totalItems, action,
                persistChanges, unexpectedException);
    }

    public void checkFinished(List<String> correlationIds) {
    	Set<String> correlationIdSet = new HashSet<>();
    	for (String correlationId : correlationIds) {
    		correlationIdSet.add(correlationId);
    	}
    	for (String correlationId : correlationIdSet) {
	        try {
	            tx.setTransactionTimeout(60 * 60 * TIMEOUT_IN_HOURS);
	            tx.begin();
	            if (catalogUploadBp.needsFinishing(correlationId)) {
	                catalogResultAggregator.finished(correlationId);
	            }
	            tx.commit();
	        }
	        catch (Exception e) {
	            try {
	                LOGGER.warn("Rolling back check transaction", e);
	                tx.rollback();
	            }
	            catch (Exception e1) {
	                LOGGER.error("Unable to complete transaction", e1);
	            }
	        }
    	}
    }
}
