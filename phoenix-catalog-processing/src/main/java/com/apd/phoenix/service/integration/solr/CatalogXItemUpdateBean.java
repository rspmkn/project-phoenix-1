package com.apd.phoenix.service.integration.solr;

import static com.apd.phoenix.service.integration.solr.CatalogXItemDocumentMapper.getLongValue;
import static com.apd.phoenix.service.integration.solr.CatalogXItemDocumentMapper.mapBaseQueryResultsToSolrDocument;
import static com.apd.phoenix.service.integration.solr.CatalogXItemDocumentMapper.mapCategoryXPropertyIconToItem;
import static com.apd.phoenix.service.integration.solr.CatalogXItemDocumentMapper.mapClassificationIconToItem;
import static com.apd.phoenix.service.integration.solr.CatalogXItemDocumentMapper.mapFavoritesListResultsToItem;
import static com.apd.phoenix.service.integration.solr.CatalogXItemDocumentMapper.mapImageToItem;
import static com.apd.phoenix.service.integration.solr.CatalogXItemDocumentMapper.mapMatchbookResultsToItem;
import static com.apd.phoenix.service.integration.solr.CatalogXItemDocumentMapper.mapPropertyIconToItem;
import static com.apd.phoenix.service.integration.solr.CatalogXItemDocumentMapper.mapPropertyToItem;
import static com.apd.phoenix.service.integration.solr.CatalogXItemDocumentMapper.mapSellingPointsToItem;
import static com.apd.phoenix.service.integration.solr.CatalogXItemDocumentMapper.mapSkuToItem;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.apache.solr.client.solrj.SolrServer;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.business.CatalogXItemBp;
import com.apd.phoenix.service.business.ItemBp;
import com.apd.phoenix.service.business.PendingSmartSearchDeletionBp;
import com.apd.phoenix.service.business.SkuTypeBp;
import com.apd.phoenix.service.model.Catalog.SearchType;
import com.apd.phoenix.service.model.CatalogXItem;
import com.apd.phoenix.service.model.Item;
import com.apd.phoenix.service.model.SkuType;
import com.apd.phoenix.service.model.listener.CatalogXItemListener;
import com.apd.phoenix.service.persistence.jpa.CatalogXItemDao.CatalogInfo;
import com.apd.phoenix.service.product.ItemCacheManager;
import com.apd.phoenix.service.product.ProductCacheManager;
import com.apd.phoenix.service.search.integration.client.SmartSearchConstants;
import com.apd.phoenix.service.search.integration.client.backend.SmartSearchCatalogMaintenanceMessage.SmartSearchCatalogMaintenanceMessageContent;
import com.apd.phoenix.service.search.integration.client.backend.SmartSearchCatalogMaintenanceMessage.SmartSearchMaintenanceAction;
import com.apd.phoenix.service.search.integration.client.backend.SmartSearchMessageEntryPoint;
import com.apd.phoenix.service.utility.SmartSearchUtils;

@LocalBean
@Stateless
public class CatalogXItemUpdateBean {

    private static final Logger LOG = LoggerFactory.getLogger(CatalogXItemUpdateBean.class);

    @Inject
    private ConcurrentUpdateSolrServiceBean solrService;

    @Inject
    private CatalogXItemBp catalogXItemBp;

    @Inject
    private ItemBp itemBp;

    @Inject
    private PendingSmartSearchDeletionBp smartSearchDeletionBp;

    @Inject
    private SmartSearchMessageEntryPoint smartSearchEntryPoint;

    @Inject
    private ProductCacheManager cacheManager;

    @Inject
    private ItemCacheManager itemCacheManager;

    @Inject
    private SkuTypeBp skuTypeBp;

    @PersistenceContext(unitName = "Phoenix")
    private EntityManager entityManager;

    private static final String BASE_QUERY_NO_EXPIRATION = "select i.id as ITEM_ID, "
            + "c.id as CATALOGXITEM_ID, IFNULL(c.solrOverrideDescription, i.name) as ITEM_NAME, i.description as DESCRIPTION, "
            + "i.HIERARCHYNODE_ID as HIERARCHYNODE_ID, i.VENDORCATALOG_ID as VENDORCATALOG_ID, "
            + "c.CATALOG_ID as CATALOG_ID, c.customerSkuString as CUSTOMER_SKU, "
            + "c.price as PRICE, i.searchterms as SEARCHTERMS, i.protectedsearchterms as PROTECTEDSEARCHTERMS, "
            + "c.coreitemexpirationdate as COREEXPIREDATE, "
            + "c.coreitemstartdate as CORESTARTDATE, "
            + "IFNULL(custUom.name, uom.name) as UOM_NAME, m.name as MNAME, "
            + "h.description as HIERARCHY_DESCRIPTION, "
            + "h.parent_id as HIERARCHY_PARENT_ID, h.indexedpaths as INDEXEDPATHS, v.name as VENDORNAME, "
            + "i.status as ITEM_STATUS, i.multiple as MULTIPLE, i.minimum as MINIMUM, "
            + "rep_sku.value as REP_SKU, rep_vend.name as REP_VEND_NAME, cust_order.value as CUSTOM_ORDER, "
            + "spec_order.value as SPECIAL_ORDER, c.overrideCatalogs AS OVERRIDE_CATALOGS, "
            + "i.smartSearchKeywords as SMART_SEARCH_KEYWORDS, IFNULL(c.ISFEATUREDITEM, i.ISFEATUREDITEM) as ISFEATUREDITEM, "
            + "c.mostPopular as MOSTPOPULAR, "
            + "b.name as BRANDNAME, b.logo as BRANDLOGO, i.smartSearchDealerDescription as DEALER_DESCRIPTION "
            + "from catalogxitem c "
            + "inner join Item i on i.id = c.item_id "
            + "inner join catalog cat on cat.id = i.vendorcatalog_id "
            + "inner join vendor v on v.id = cat.vendor_id "
            + "inner join catalog cust_cat on cust_cat.id = c.catalog_id "
            + "LEFT JOIN unitofmeasure uom on uom.id = i.unitofmeasure_id "
            + "LEFT JOIN unitofmeasure custUom on custUom.id = c.customerUnitOfMeasure_id "
            + "LEFT JOIN manufacturer m on m.id = i.manufacturer_id "
            + "LEFT JOIN hierarchynode h on h.id = i.hierarchynode_id "
            + "left join item rep on i.replacement_id = rep.id "
            + "left join SkuType dealer_sku_type on dealer_sku_type.name = 'dealer' "
            + "left join sku rep_sku on rep_sku.item_id = rep.id and rep_sku.type_id = dealer_sku_type.id "
            + "left join catalog rep_cat on rep_cat.id = rep.vendorcatalog_id "
            + "left join vendor rep_vend on rep_cat.vendor_id = rep_vend.id "
            + "left join brand b on i.brand_id = b.id "
            + "LEFT JOIN ITEMPROPERTYTYPE customOrderType ON customOrderType.name = 'customOrder' "
            + "LEFT JOIN ITEMPROPERTYTYPE specialOrderType ON specialOrderType.name = 'specialOrder' "
            + "left join itemxitempropertytype cust_order on cust_order.item_id = i.id and cust_order.type_id = customOrderType.id  "
            + "left join itemxitempropertytype spec_order on spec_order.item_id = i.id and spec_order.type_id = specialOrderType.id  "
            + "where c.id=:catxItemId " + "and (cat.expirationDate is null OR cat.expirationDate > SYSDATE())";

    private static final String BASE_QUERY = BASE_QUERY_NO_EXPIRATION
            + " and (cust_cat.expirationDate is null OR cust_cat.expirationDate > SYSDATE())";
    private static final String IMAGE_QUERY = "select IMAGEURL as IMAGEURL from ITEMIMAGE where item_id = :itemId";
    private static final String SELLING_POINTS_QUERY = "select VALUE as VALUE from ITEMSELLINGPOINT where item_id = :itemId";
    private static final String SKU_QUERY = "select VALUE as VALUE, TYPE_ID as TYPE_ID from SKU where ITEM_ID = :itemId";
    //This query is also used to pull the green value for the product
    private static final String CLASSIFICATION_ICON_QUERY = "select t.NAME as NAME, t.ICONURL as ICONURL, t.TOOLTIPLABEL as TOOLTIPLABEL, C.VALUE as VALUE from FLATITEMCLASSIFICATION c, ITEMCLASSIFICATIONTYPE t where c.ITEM_ID = :itemId and c.ITEMCLASSTYPE_ID=t.id";
    private static final String PROPERTY_ICON_QUERY = "select t.NAME as NAME, t.ICONURL as ICONURL, t.TOOLTIPLABEL as TOOLTIPLABEL, itemx.VALUE as VALUE from itempropertytype t, itemxitempropertytype itemx where itemx.item_id=:itemId and itemx.type_id=t.id and t.ICONURL IS NOT NULL";
    private static final String CATEGORY_X_PROPERTY_ICON_QUERY = "select t.NAME as NAME, t.ICONURL as ICONURL, t.TOOLTIPLABEL as TOOLTIPLABEL, catx.VALUE as VALUE from itempropertytype t, catalogxitemxitempropertytype catx where catx.CATALOGXITEM_ID=:catalogxitemID and catx.type_id=t.id";
    private static final String PROPERTY_QUERY = "select s.NAME as NAME,s.VALUE as VALUE, s.PRIORITY as PRIORITY from FLATITEMSPECIFICATION s where s.ITEM_ID = :itemId";
    private static final String FAVORITES_LIST_QUERY = "select favoriteslist.ID as FAVORITES_LIST_ID from favoriteslist join comp_favlist_catxitem on comp_favlist_catxitem.FAVORITESLIST_ID = favoriteslist.ID where (favoriteslist.CATALOG_ID is not null) and comp_favlist_catxitem.ITEMS_ID = :catalogXItemId";
    private static final String MATCHBOOK_QUERY = "select itemMatchbook.matchbook_id as MB_ID, matchbook.model as MODEL from item_matchbook itemMatchbook left join matchbook on itemMatchbook.matchbook_id = matchbook.id where itemMatchbook.item_id = :itemId";

    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public void insertItems(Long catalogXItemId) {
        //the postPersist method will change the item, sending an update message
        //which will index the item
        catalogXItemBp.postPersist(catalogXItemId);
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public void repriceItems(Long catalogXItemId) {
        try {
            CatalogXItem item = catalogXItemBp.findById(catalogXItemId, CatalogXItem.class);
            //setting the send message flag to false, since we're going to manually send the message
            //whether or not there's an update
            item.setSendUpdateMessage(false);
            catalogXItemBp.update(itemBp.calculatePrice(item));
            CatalogXItemListener.addUpdateSolrDocument(catalogXItemId);
        }
        catch (Exception priceEx) {
            if (LOG.isDebugEnabled()) {
                LOG.debug("error calculating price", priceEx);
            }
        }
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public void cacheRefresh(Long catalogXItemId) {
        CatalogXItem catalogXItem = catalogXItemBp.findById(catalogXItemId, CatalogXItem.class);
        catalogXItem.setSendUpdateMessage(false);
        cacheManager.updateCatalogXItem(catalogXItem);
        itemCacheManager.putCustomerItem(catalogXItem);
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public void updateItems(List<Long> catalogXItemIds) {

        Map<String, List<Long>> catalogXItemsByCatalog = new HashMap<String, List<Long>>();
        List<Long> solrCatalogXItems = new ArrayList<Long>();
        for (Long catalogXItemId : catalogXItemIds) {
            CatalogInfo catalogInfo = catalogXItemBp.getCatalogInfoFromItemId(catalogXItemId);
            if (catalogInfo != null && SearchType.SMART_SEARCH.equals(catalogInfo.getSearchType())) {
                if (catalogXItemsByCatalog.get(catalogInfo.getId().toString()) == null) {
                    List<Long> catalogXItems = new ArrayList<Long>(catalogXItemIds.size());
                    catalogXItems.add(catalogXItemId);
                    catalogXItemsByCatalog.put(catalogInfo.getId().toString(), catalogXItems);
                }
                else {
                    catalogXItemsByCatalog.get(catalogInfo.getId().toString()).add(catalogXItemId);
                }
            }
            solrCatalogXItems.add(catalogXItemId);
        }
        if (solrCatalogXItems.size() > 0) {
            updateItemInSolr(solrCatalogXItems);
        }
        if (catalogXItemsByCatalog.size() > 0) {
            updateItemInSmartSearch(catalogXItemsByCatalog);
        }
    }

    public void updateItemInSmartSearch(Map<String, List<Long>> catalogXItemsByCatalog) {
    	Map<Long, String> skuTypeMap = new HashMap<>();
    	for (SkuType skuType : skuTypeBp.findAll(SkuType.class, 0, 0)) {
    		skuTypeMap.put(skuType.getId(), skuType.getName());
    	}
        for (Map.Entry<String, List<Long>> entry : catalogXItemsByCatalog.entrySet()) {
            String catalogName = entry.getKey();
            List<Long> catalogXItemIds = entry.getValue();
            Map<Long, SmartSearchCatalogMaintenanceMessageContent> itemToUpdate = new HashMap<Long, SmartSearchCatalogMaintenanceMessageContent>();
            for (Long catalogXItemId : catalogXItemIds) {
                CatalogXItem catalogXItem = catalogXItemBp.findById(catalogXItemId, CatalogXItem.class);
                Map<String, Object> baseQueryResults = performSingleMapQuery(BASE_QUERY, "catxItemId", catalogXItemId);
                Long itemID;
                try {
                    itemID = getLongValue(baseQueryResults.get("ITEM_ID"));
                    if (itemID != null) {

                        List<Map<String, Object>> imgQueryResults = performMultiMapQuery(IMAGE_QUERY, "itemId", itemID);
                        if (imgQueryResults != null && imgQueryResults.size() > 0) {
                            baseQueryResults.putAll(imgQueryResults.get(0));
                        }

                        // perform the query for skus
                        List<Map<String, Object>> skus = performMultiMapQuery(SKU_QUERY, "itemId", itemID);
                        if (skus != null && skus.size() > 0) {
                            for (Map<String, Object> result : skus) {

                                baseQueryResults.put(skuTypeMap.get(((BigInteger) result.get("TYPE_ID")).longValue()), (Object) result.get("VALUE"));
                            }
                        }

                        if (SmartSearchUtils.isUsscoItem(catalogXItem)) {
                            baseQueryResults.put(SmartSearchConstants.IS_USSCO_CATALOG, true);
                        }
                        else {
                            baseQueryResults.put(SmartSearchConstants.IS_USSCO_CATALOG, false);
                        }

                        baseQueryResults.put("CATALOG_NAME", catalogXItem.getCatalog().getName());
                        if (baseQueryResults.size() > 0) {
                            itemToUpdate.put(itemID, new SmartSearchCatalogMaintenanceMessageContent(baseQueryResults));
                        }
                    }
                    Map<String, Map<Long, SmartSearchCatalogMaintenanceMessageContent>> catalogXItems = new HashMap<String, Map<Long, SmartSearchCatalogMaintenanceMessageContent>>();
                    catalogXItems.put(catalogName, itemToUpdate);
                    smartSearchEntryPoint.scheduleRequest(SmartSearchMaintenanceAction.UPDATE_ITEM, catalogXItems);
                }
                catch (Exception exp) {
                    if (LOG.isDebugEnabled()) {
                        LOG.debug("Exception updateItemInSmartSearch: ", exp);
                    }
                }
            }
        }

    }

    /**
     * Takes a list of CatalogXItem IDs and updates them in Solr. NOTE: DO NOT fetch the CatalogXItem entities
     * in this method, it will cause a JPA flush action to take place whenever a query to the database is
     * executed, with O(N^2) processing time, where N is the number of items in the list (defaulting to 500
     * as of this writing).
     * 
     * @param catalogXItemIds
     */
    public void updateItemInSolr(List<Long> catalogXItemIds) {
    	List<SolrDocumentPojo> solrPojoList = new ArrayList<>();
    	Map<Long, String> skuTypeMap = new HashMap<>();
    	for (SkuType skuType : skuTypeBp.findAll(SkuType.class, 0, 0)) {
    		skuTypeMap.put(skuType.getId(), skuType.getName());
    	}
        for (Long catalogXItemId : catalogXItemIds) {
            try {
                SolrDocumentPojo solrDocument = createUpdateDocument(catalogXItemId, skuTypeMap);
                if (solrDocument != null) {
                    solrPojoList.add(solrDocument);
                }
            }
            catch (Exception e) {
                LOG.error("Exception search indexing item: " + catalogXItemId, e);
            }
        }

        // commit changes
        LOG.info("Committing Batch Solr Updates");
        try {
        	if (!solrPojoList.isEmpty()) {
	            SolrServer solrServer = solrService.getSolrServer();
	            solrServer.addBeans(solrPojoList);
	            solrServer.commit();
        	}
        }
        catch (Exception e) {
            LOG.error(e.getMessage(), e);
        }
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public void deleteItems(List<CatalogXItem.DeleteItemMessage> messages) {

        Map<String, List<Long>> catalogXItemsByCatalog = new HashMap<String, List<Long>>();
        List<CatalogXItem.DeleteItemMessage> solrDeleteList = new ArrayList<CatalogXItem.DeleteItemMessage>();
        for (CatalogXItem.DeleteItemMessage message : messages) {
            catalogXItemBp.postDelete(message.getCatalogId(), message.getItemId());
            CatalogXItem catalogXItem = catalogXItemBp.findById(message.getId(), CatalogXItem.class);
            if (catalogXItem != null && SearchType.SMART_SEARCH.equals(catalogXItem.getCatalog().getSearchType())) {
                if (catalogXItemsByCatalog.get(catalogXItem.getCatalog().getId().toString()) == null) {
                    List<Long> catalogXItems = new ArrayList<Long>(messages.size());
                    catalogXItems.add(message.getId());
                    catalogXItemsByCatalog.put(catalogXItem.getCatalog().getId().toString(), catalogXItems);
                }
                else {
                    catalogXItemsByCatalog.get(catalogXItem.getCatalog().getId().toString()).add(message.getId());
                }
            }
            else {
                solrDeleteList.add(message);
            }
        }
        if (solrDeleteList.size() > 0) {
            solrDeleteById(solrDeleteList);
        }

        if (catalogXItemsByCatalog.size() > 0) {
            flagForSmartSearchDeletion(catalogXItemsByCatalog);
        }
    }

    public void solrDeleteById(List<CatalogXItem.DeleteItemMessage> messages) {
        SolrServer solrServer = solrService.getSolrServer();
        for (CatalogXItem.DeleteItemMessage message : messages) {
            try {
                solrServer.deleteById(message.toString());
            }
            catch (Exception e) {
                LOG.error("Exception deleting item: " + message, e);
            }
        }

        // commit changes
        LOG.info("Committing Batch Solr Deletes");
        try {
            solrServer.commit();
        }
        catch (Exception e) {
            LOG.error(e.getMessage(), e);
        }
    }

    public void flagForSmartSearchDeletion(Map<String, List<Long>> catalogXItemsByCatalog) {
        for (Map.Entry<String, List<Long>> entry : catalogXItemsByCatalog.entrySet()) {
            List<Long> catalogXItemIds = entry.getValue();
            for (Long catalogXItemId : catalogXItemIds) {
                CatalogXItem catalogXItem = catalogXItemBp.findById(catalogXItemId, CatalogXItem.class);
                if (catalogXItem != null) {
                    smartSearchDeletionBp.create(catalogXItem.getCatalog(), catalogXItem.getItem());
                }
            }
        }
    }

    public void deleteItemInSmartSearch(List<CatalogXItem.DeleteSmartSearchMessage> messages) {
        for (CatalogXItem.DeleteSmartSearchMessage message : messages) {
            String catalogName = message.getListName() + "";
            Item item = itemBp.findById(message.getItemId(), Item.class);
            Map<Long, SmartSearchCatalogMaintenanceMessageContent> itemToDelete = new HashMap<Long, SmartSearchCatalogMaintenanceMessageContent>();
            itemToDelete.put(item.getId(), new SmartSearchCatalogMaintenanceMessageContent(item));
            Map<String, Map<Long, SmartSearchCatalogMaintenanceMessageContent>> catalogXItems = new HashMap<String, Map<Long, SmartSearchCatalogMaintenanceMessageContent>>();
            catalogXItems.put(catalogName, itemToDelete);
            smartSearchEntryPoint.scheduleRequest(SmartSearchMaintenanceAction.REMOVE_LIST, catalogXItems);
        }
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public void insertItemInSmartSearch(List<CatalogXItem.UpdateSmartSearchMessage> messages) {
        for (CatalogXItem.UpdateSmartSearchMessage message : messages) {
            CatalogXItem item = this.catalogXItemBp.findById(message.getId(), CatalogXItem.class);
            SmartSearchCatalogMaintenanceMessageContent content = new SmartSearchCatalogMaintenanceMessageContent(item,
                    message.getCoreListName());
            String catalogName = message.getListName() + "";
            Map<String, Map<Long, SmartSearchCatalogMaintenanceMessageContent>> catalogXItems = new HashMap<String, Map<Long, SmartSearchCatalogMaintenanceMessageContent>>();
            Map<Long, SmartSearchCatalogMaintenanceMessageContent> itemToUpdate = new HashMap<Long, SmartSearchCatalogMaintenanceMessageContent>();
            itemToUpdate.put(item.getId(), content);
            catalogXItems.put(catalogName, itemToUpdate);
            smartSearchEntryPoint.scheduleRequest(SmartSearchMaintenanceAction.INSERT_LIST, catalogXItems);
        }
    }

    private Map<String, Object> performSingleMapQuery(String query, String parameterName, Long id) {
        List<Map<String, Object>> results = performMultiMapQuery(query, parameterName, id);
        if (results.size() == 1) {
            return results.get(0);
        }
        if (results.size() > 1) {
            throw new IllegalStateException("Expected 1 result, actually: " + results.size() + " for query: " + query
                    + " and id: " + id);
        }
        else {
            return null;
        }
    }

    private List<Map<String, Object>> performMultiMapQuery(String query, String parameterName, Long id) {
        // get the hibernate session from the injected entity manager.
        Session session = (Session) entityManager.getDelegate();

        final Query itemQuery = session.createSQLQuery(query);
        itemQuery.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
        itemQuery.setParameter(parameterName, id);

        return (List<Map<String, Object>>) itemQuery.list();
    }

    private SolrDocumentPojo createUpdateDocument(Long catalogXItemId, Map<Long, String> skuTypeMap) {
        // create the item document to send to solr.
        SolrDocumentPojo solrDocument = new SolrDocumentPojo();
        // perform the query for the base info.
        Map<String, Object> baseQueryResults = performSingleMapQuery(BASE_QUERY, "catxItemId", catalogXItemId);
        Long itemID;
        try {
            itemID = getLongValue(baseQueryResults.get("ITEM_ID"));
            if (itemID == null) {
                return null;
            }
        }
        catch (NullPointerException ex) {
            return null;
        }
        // init solrdocument with values from base query
        mapBaseQueryResultsToSolrDocument(baseQueryResults, solrDocument);

        // init solrdocument with values from image query
        List<Map<String, Object>> imgQueryResults = performMultiMapQuery(IMAGE_QUERY, "itemId", itemID);
        mapImageToItem(imgQueryResults, solrDocument);

        // init solrdocument with values from selling points query
        List<Map<String, Object>> sellingPointsQueryResults = performMultiMapQuery(SELLING_POINTS_QUERY, "itemId",
                itemID);
        mapSellingPointsToItem(sellingPointsQueryResults, solrDocument);

        // perform the query for skus
        List<Map<String, Object>> skus = performMultiMapQuery(SKU_QUERY, "itemId", itemID);
        mapSkuToItem(skus, solrDocument, skuTypeMap);

        List<Map<String, Object>> classificationIconResults = performMultiMapQuery(CLASSIFICATION_ICON_QUERY, "itemId",
                itemID);
        mapClassificationIconToItem(classificationIconResults, solrDocument);

        List<Map<String, Object>> propertyIcon = performMultiMapQuery(PROPERTY_ICON_QUERY, "itemId", itemID);
        mapPropertyIconToItem(propertyIcon, solrDocument);

        List<Map<String, Object>> categoryXPropertyIconQueryResults = performMultiMapQuery(
                CATEGORY_X_PROPERTY_ICON_QUERY, "catalogxitemID", catalogXItemId);
        mapCategoryXPropertyIconToItem(categoryXPropertyIconQueryResults, solrDocument);

        // get properties
        List<Map<String, Object>> properties = performMultiMapQuery(PROPERTY_QUERY, "itemId", itemID);
        mapPropertyToItem(properties, solrDocument);

        List<Map<String, Object>> favoritesListQueryResults = performMultiMapQuery(FAVORITES_LIST_QUERY,
                "catalogXItemId", catalogXItemId);
        mapFavoritesListResultsToItem(favoritesListQueryResults, solrDocument);

        List<Map<String, Object>> matchbookQueryResults = performMultiMapQuery(MATCHBOOK_QUERY, "itemId", itemID);
        mapMatchbookResultsToItem(matchbookQueryResults, solrDocument);

        // Set the id on itemDoc
        solrDocument.setId(solrDocument.getCatalogxitemId());

        return solrDocument;
    }
}
