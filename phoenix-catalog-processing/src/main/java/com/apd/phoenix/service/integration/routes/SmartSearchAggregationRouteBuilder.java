package com.apd.phoenix.service.integration.routes;

import org.apache.camel.LoggingLevel;
import com.apd.phoenix.service.camel.DelayBean;
import com.apd.phoenix.service.integration.camel.aggregation.strategy.SmartSearchCatalogMaintenanceMessageAggregationStrategy;
import com.apd.phoenix.service.integration.multitenancy.AbstractTenantAwareRouteBuilder;

public class SmartSearchAggregationRouteBuilder extends AbstractTenantAwareRouteBuilder {

    private String updateSource;
    private String updateSourceAggregated;
    private int aggregationSize;
    private Long aggregationTimeout;

    @Override
    public void configureRouteImplementation() throws Exception {

        onException(Exception.class).maximumRedeliveries(0).handled(true).log(LoggingLevel.INFO,
                "Error in smart search update: ${exception.stacktrace}");

        //Aggregate initial messages
        from(updateSource).autoStartup(false).routePolicy(DelayBean.getDelayPolicy())
        //Logging
                .log(LoggingLevel.DEBUG, "Received message for aggregation")
                //Aggregate all messages
                .aggregate(header("tenantId"), new SmartSearchCatalogMaintenanceMessageAggregationStrategy())
                //Aggregation size and timeout
                .completionSize(aggregationSize).completionTimeout(aggregationTimeout)
                //Logging
                .log(LoggingLevel.INFO, "Sending to update processor")
                //send to aggregated queue
                .to(updateSourceAggregated);

    }

    public String getUpdateSource() {
        return updateSource;
    }

    public void setUpdateSource(String updateSource) {
        this.updateSource = updateSource;
    }

    public String getUpdateSourceAggregated() {
        return updateSourceAggregated;
    }

    public void setUpdateSourceAggregated(String updateSourceAggregated) {
        this.updateSourceAggregated = updateSourceAggregated;
    }

    public int getAggregationSize() {
        return aggregationSize;
    }

    public void setAggregationSize(int aggregationSize) {
        this.aggregationSize = aggregationSize;
    }

    public Long getAggregationTimeout() {
        return aggregationTimeout;
    }

    public void setAggregationTimeout(Long aggregationTimeout) {
        this.aggregationTimeout = aggregationTimeout;
    }
}
