package com.apd.phoenix.service.integration.camel.processor.catalog;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.core.MultivaluedMap;
import org.apache.camel.Body;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.cxf.jaxrs.impl.MetadataMap;
import com.apd.phoenix.service.business.ItemBp;
import com.apd.phoenix.service.model.dto.ItemRelationshipDto;
import au.com.bytecode.opencsv.CSVReader;

@Stateless
public class UsscoRelationshipProcessor {

    private static final String USSCO_VENDOR = "USSCO";

    protected static Log logger = LogFactory.getLog(UsscoRelationshipProcessor.class);

    @Inject
    private ItemBp itemBp;

    public void addRelationship(@Body List<String> relationshipDtos) {
        if (logger.isDebugEnabled()) {
            logger.debug(relationshipDtos);
        }

        for (String relationshipDto : relationshipDtos) {
            this.parseRelationship(relationshipDto);
        }
    }

    private void parseRelationship(String relationshipDto) {
        
        try (
        	InputStream is = new ByteArrayInputStream(relationshipDto.getBytes());
        	BufferedReader br = new BufferedReader(new InputStreamReader(is));
        	CSVReader csvReader = new CSVReader(br);
        		) {
        	//CSV values are: Parent_Prod, Parent_Item, Child_Prod, Child_Item, Relationship, Priority
        	String[] values = csvReader.readNext();
            String parentSku = values[1];
            String childSku = values[3];
            String relationshipType = values[4];
            ItemRelationshipDto childDto = new ItemRelationshipDto();
            childDto.setPosition(Integer.parseInt(values[5]));
            childDto.setSku(childSku);
            childDto.setVendorName(USSCO_VENDOR);
            MultivaluedMap<String, ItemRelationshipDto> map = new MetadataMap<String, ItemRelationshipDto>();
            List<ItemRelationshipDto> childDtoList = new ArrayList<ItemRelationshipDto>();
            childDtoList.add(childDto);
            map.put(relationshipType, childDtoList);
            if (StringUtils.isNotBlank(parentSku) && StringUtils.isNotBlank(childSku)
                    && StringUtils.isNotBlank(relationshipType)) {
                itemBp.setRelationships(parentSku, USSCO_VENDOR, map);
            }
        } catch (Exception e) {
        	logger.warn("Exception during relationship CSV processing, turn on debug for stack trace");
        	if (logger.isDebugEnabled()) {
        		logger.debug("Stack trace", e);
        	}
		}
    }
}
