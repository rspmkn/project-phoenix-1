package com.apd.phoenix.service.integration.routes;

import javax.ejb.Stateful;
import javax.inject.Named;
import org.apache.camel.LoggingLevel;
import com.apd.phoenix.service.camel.DelayBean;
import com.apd.phoenix.service.integration.multitenancy.AbstractTenantAwareRouteBuilder;
import com.apd.phoenix.service.utility.exception.RetryMessageException;

@Named
@Stateful
public class MessageBeanRouteBuilder extends AbstractTenantAwareRouteBuilder {

    private String source;

    private Class<?> clazz;

    private String method;

    @Override
    public void configureRouteImplementation() {

        //this occurs whenever a RetryMessageException is thrown. This places the message back on the queue.
        onException(RetryMessageException.class).to(source);

        onException(Exception.class).maximumRedeliveries(0).handled(true).log(LoggingLevel.INFO,
                "Error in processing: ${exception.stacktrace}");

        from(source).autoStartup(false).routePolicy(DelayBean.getDelayPolicy()).bean(clazz, method);
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public Class<?> getClazz() {
        return clazz;
    }

    public void setClazz(Class<?> clazz) {
        this.clazz = clazz;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

}
