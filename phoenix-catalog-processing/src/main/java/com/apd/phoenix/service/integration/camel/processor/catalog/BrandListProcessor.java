package com.apd.phoenix.service.integration.camel.processor.catalog;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.business.BrandBp;
import com.apd.phoenix.service.model.Brand;

@Stateless
public class BrandListProcessor {

    private static final Logger LOG = LoggerFactory.getLogger(BrandListProcessor.class);

    @Inject
    private BrandBp brandBp;

    public List<Brand> csvToBrands(String csv) {
        ArrayList<Brand> returnList = new ArrayList<Brand>();
        csv = csv.trim();
        String[] lines = csv.split("\\r?\\n");
        for (String line : lines) {
            LOG.info(line);
            line = line.trim();
            String[] parts = line.split(",");
            Brand brand = new Brand();
            brand.setUsscoId(parts[0].trim());
            if (parts.length > 2) {
                brand.setName(parts[2].trim());
            }
            if (parts.length > 3) {
                brand.setLogo(parts[3].trim());
            }
            returnList.add(brand);
        }
        return returnList;
    }

    public void updateBrand(Brand brand) {
        if (brand == null) {
            return;
        }
        Brand existingBrand = brandBp.findByUsscoId(brand.getUsscoId());
        if (existingBrand == null) {
            brandBp.create(brand);
        }
        else {
            if (StringUtils.isNotBlank(brand.getLogo()) && !brand.getLogo().startsWith("http")) {
                LOG.error("Not updating brand with logo path that isn't fully qualified");
            }
            else {
                brand.setId(existingBrand.getId());
                brandBp.update(brand);
            }
        }
    }

}
