/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.service.integration.solr;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.jms.JMSException;
import javax.naming.NamingException;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.model.listener.CatalogXItemListener;
import com.apd.phoenix.service.product.ItemCacheManager;

/**
 *
 * @author dnorris
 */
@LocalBean
@Stateless
public class ItemUpdateBean {

    private static final Logger LOG = LoggerFactory.getLogger(ItemUpdateBean.class);

    @PersistenceContext(unitName = "Phoenix")
    private EntityManager entityManager;

    @Inject
    private ItemCacheManager cacheManager;

    private static final String CATALOGX_ITEM_LIST_QUERY = "select id as ID from catalogxitem where item_id = :itemId";

    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public void updateItems(Long itemId) {
        List<Map<String, Object>> catXItemListResults = performMultiMapQuery(CATALOGX_ITEM_LIST_QUERY, "itemId", itemId);

        ArrayList<Long> catalogXItemIds = new ArrayList<Long>();
        for (Map<String, Object> result : catXItemListResults) {
            catalogXItemIds.add(getLongValue(result.get("ID")));
        }
        try {
			CatalogXItemListener.addRepriceSolrDocument(catalogXItemIds);
		} catch (NamingException | JMSException e) {
			if (LOG.isDebugEnabled()) {
				LOG.debug("Error sending update message", e);
			}
		}
        
        if (itemId != null) {
        	this.cacheManager.putVendorItem(itemId);
        }
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public void deleteItems(Long itemId) {

        LOG.error("Attempted to delete item " + itemId);
    }

    private static Long getLongValue(Object o) {
        if (o instanceof BigInteger) {
            return ((BigInteger) o).longValue();
        }
        else if (o instanceof Long) {
            return (Long) o;
        }
        else {
            return null;
        }
    }

    private List<Map<String, Object>> performMultiMapQuery(String query, String parameterName, Long id) {
        //get the hibernate session from the injected entity manager.
        Session session = (Session) entityManager.getDelegate();

        final Query itemQuery = session.createSQLQuery(query);
        itemQuery.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
        itemQuery.setParameter(parameterName, id);

        return (List<Map<String, Object>>) itemQuery.list();
    }
}
