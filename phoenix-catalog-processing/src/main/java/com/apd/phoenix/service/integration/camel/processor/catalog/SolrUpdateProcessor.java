package com.apd.phoenix.service.integration.camel.processor.catalog;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import org.apache.camel.Body;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.integration.solr.CatalogXItemUpdateBean;
import com.apd.phoenix.service.integration.solr.ItemUpdateBean;
import com.apd.phoenix.service.model.CatalogXItem;
import com.apd.phoenix.service.model.listener.SolrEntityListener;

@Stateless
public class SolrUpdateProcessor {

    private static final Logger LOG = LoggerFactory.getLogger(SolrUpdateProcessor.class);

    @Inject
    private ItemUpdateBean itemUpdateBean;

    @Inject
    private CatalogXItemUpdateBean catXItemUpdateBean;

    @Inject
    private CatalogXItemProcessor catXItemProcessor;

    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public void processUpdates(@Body List<SolrEntityListener.SolrDocumentMessage> messages) {
    	LOG.info("Processing message batch of size {}", messages.size());
    	List<Long> toAtomicUpdate = new ArrayList<>();
    	List<CatalogXItem.DeleteItemMessage> toAtomicDelete = new ArrayList<>();
    	List<CatalogXItem.UpdateSmartSearchMessage> toSmartSearchInsert = new ArrayList<>();
    	List<CatalogXItem.DeleteSmartSearchMessage> toSmartSearchRemove = new ArrayList<>();
    	for (SolrEntityListener.SolrDocumentMessage message : messages) {
    		switch (message.getAction()) {
    		case CATXI_DELETE:
    			toAtomicDelete.add((CatalogXItem.DeleteItemMessage) message.getContent());
    			break;
    		case CATXI_UPDATE:
    			toAtomicUpdate.add((Long) message.getContent());
    			break;
    		case CATXI_INSERT:
    			this.catXItemUpdateBean.insertItems((Long) message.getContent());
    			break;
    		case CATXI_OVERRIDES_RECALC:
    			catXItemProcessor.process((Long) message.getContent());
    			break;
    		case CATXI_REPRICE:
    			this.catXItemUpdateBean.repriceItems((Long) message.getContent());
    			break;
    		case CATXI_CACHE:
    			this.catXItemUpdateBean.cacheRefresh((Long) message.getContent());
    			break;
    		case ITEM_UPDATE:
    			this.itemUpdateBean.updateItems((Long) message.getContent());
    			break;
    		case ITEM_DELETE:
    			this.itemUpdateBean.deleteItems((Long) message.getContent());
    			break;
			case SMARTSEARCH_LIST_INSERT:
				toSmartSearchInsert.add((CatalogXItem.UpdateSmartSearchMessage) message.getContent());
				break;
			case SMARTSEARCH_LIST_REMOVE:
				toSmartSearchRemove.add((CatalogXItem.DeleteSmartSearchMessage) message.getContent());
				break;
			default:
				break;
    		}
    	}
    	if (!toAtomicUpdate.isEmpty()) {
    		catXItemUpdateBean.updateItems(toAtomicUpdate);
    	}
    	if (!toAtomicDelete.isEmpty()) {
    		catXItemUpdateBean.deleteItems(toAtomicDelete);
    	}
    	if (!toSmartSearchInsert.isEmpty()) {
    		catXItemUpdateBean.insertItemInSmartSearch(toSmartSearchInsert);
    	}
    	if (!toSmartSearchRemove.isEmpty()) {
    		catXItemUpdateBean.deleteItemInSmartSearch(toSmartSearchRemove);
    	}
    }
}
