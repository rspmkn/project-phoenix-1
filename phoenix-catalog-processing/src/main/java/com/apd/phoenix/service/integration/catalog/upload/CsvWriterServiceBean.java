package com.apd.phoenix.service.integration.catalog.upload;

import java.util.Properties;
import java.util.concurrent.atomic.AtomicBoolean;
import javax.ejb.Singleton;
import javax.inject.Inject;
import javax.inject.Named;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.camel.component.ActiveMQComponent;
import org.apache.activemq.pool.PooledConnectionFactory;
import org.apache.camel.CamelContext;
import org.apache.camel.component.cdi.CdiCamelContext;
import org.apache.camel.component.jms.JmsConfiguration;
import org.jboss.as.server.ServerEnvironment;
import org.jboss.msc.inject.Injector;
import org.jboss.msc.service.StartContext;
import org.jboss.msc.service.StartException;
import org.jboss.msc.service.StopContext;
import org.jboss.msc.value.InjectedValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.core.utility.TenantConfigRepository;
import com.apd.phoenix.service.integration.camel.processor.catalog.CatalogCsvResultProcessor;
import com.apd.phoenix.service.integration.camel.processor.catalog.CatalogUpdateMessageProcessor;
import com.apd.phoenix.service.integration.routes.CatalogCompleteCheckRouteBuilder;
import com.apd.phoenix.service.integration.routes.MessageBeanRouteBuilder;

@Named
@Singleton
public class CsvWriterServiceBean implements CsvWriterService {

    private static final Logger LOGGER = LoggerFactory.getLogger(CsvWriterServiceBean.class.getCanonicalName());
    /**
     * A flag whether the service is started.
     */
    private final AtomicBoolean started = new AtomicBoolean(false);

    @Inject
    private CdiCamelContext camelContext;

    private String nodeName;

    private final InjectedValue<ServerEnvironment> env = new InjectedValue<ServerEnvironment>();

    public Injector<ServerEnvironment> getEnvInjector() {
        return this.env;
    }

    /**
     * @return the name of the server node
     */
    public String getValue() throws IllegalStateException, IllegalArgumentException {
        if (!started.get()) {
            throw new IllegalStateException("The service '" + this.getClass().getName() + "' is not ready!");
        }
        return this.nodeName;
    }

    public void start(StartContext arg0) throws StartException {
        if (!started.compareAndSet(false, true)) {
            throw new StartException("The service is still started!");
        }
        LOGGER.info("Start service '" + this.getClass().getName() + "'");
        this.nodeName = this.env.getValue().getNodeName();
        Properties commonProperties = TenantConfigRepository.getInstance().getProperties("catalog.upload.integration");
        setupAmq(camelContext, commonProperties.getProperty("jmsBrokerUrl"));
        MessageBeanRouteBuilder catalogRoute = new MessageBeanRouteBuilder();
        catalogRoute.setClazz(CatalogCsvResultProcessor.class);
        catalogRoute.setSource(commonProperties.getProperty("csvWriterSource"));
        catalogRoute.setMethod("process");
        CatalogCompleteCheckRouteBuilder catalogCompleteCheckRoute = new CatalogCompleteCheckRouteBuilder();
        catalogCompleteCheckRoute.setClazz(CatalogUpdateMessageProcessor.class);
        catalogCompleteCheckRoute.setSource(commonProperties.getProperty("csvCheckCompleteSource"));
        catalogCompleteCheckRoute.setMethod("checkFinished");
        catalogCompleteCheckRoute.setAggregationSize(1000);
        catalogCompleteCheckRoute.setAggregationTimeout(1000l);
        try {
            camelContext.addRoutes(catalogRoute);
            camelContext.addRoutes(catalogCompleteCheckRoute);
            camelContext.start();
            LOGGER.info("CamelContext started");
        }
        catch (Exception e) {
            LOGGER.error("An error occured:", e);
        }
    }

    public void stop(StopContext arg0) {
        if (!started.compareAndSet(true, false)) {
            LOGGER.warn("The service '" + this.getClass().getName() + "' is not active!");
        }
        else {
            LOGGER.info("Stop service '" + this.getClass().getName() + "'");
        }
        try {
            camelContext.stop();
        }
        catch (Exception e) {
            LOGGER.error("An error occured:", e);
        }
    }

    public void setupAmq(CamelContext context, String jmsBrokerUrl) {
        Properties amqProperties = TenantConfigRepository.getInstance().getProperties("amq");
        ActiveMQConnectionFactory activeMQConnectionFactory = new ActiveMQConnectionFactory();
        activeMQConnectionFactory.setBrokerURL(jmsBrokerUrl);
        PooledConnectionFactory pooledConnectionFactory = new PooledConnectionFactory();
        pooledConnectionFactory.setConnectionFactory(activeMQConnectionFactory);
        pooledConnectionFactory.setMaxConnections(Integer.parseInt(amqProperties.getProperty(getClass().getSimpleName()
                + ".maxConnections", "2")));
        JmsConfiguration jmsConfiguration = new JmsConfiguration();
        jmsConfiguration.setConnectionFactory(pooledConnectionFactory);
        jmsConfiguration.setConcurrentConsumers(Integer.parseInt(amqProperties.getProperty(getClass().getSimpleName()
                + ".concurrentConsumers", "2")));
        ActiveMQComponent activeMQComponent = new ActiveMQComponent();
        activeMQComponent.setConfiguration(jmsConfiguration);
        context.addComponent("activemq", activeMQComponent);
    }

}
