package com.apd.phoenix.service.integration.camel.processor.catalog;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.business.CatalogXItemBp;
import com.apd.phoenix.service.model.CatalogXItem;

@Stateless
public class CatalogXItemProcessor {

    private static final Logger LOGGER = LoggerFactory.getLogger(CatalogXItemProcessor.class);

    @Inject
    private CatalogXItemBp catalogXItemBp;

    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public void process(Long id) {
        CatalogXItem item = this.catalogXItemBp.findById(id, CatalogXItem.class);
        if (item != null) {
            this.catalogXItemBp.calculateOverrideCatalogs(item);
            this.catalogXItemBp.update(item);
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("Setting item " + id + " to have override catalogs " + item.getOverrideCatalogs());
            }
        }
    }
}
