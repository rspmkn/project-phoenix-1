package com.apd.phoenix.service.integration.mapping;

import com.apd.phoenix.core.StringEscape;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.core.MultivaluedMap;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.commons.lang.StringUtils;
import org.apache.cxf.jaxrs.impl.MetadataMap;
import org.openapplications.oagis._9.AttachmentBaseType;
import org.openapplications.oagis._9.DescriptionType;
import org.openapplications.oagis._9.ItemIDType;
import org.openapplications.oagis._9.NoteType;
import org.openapplications.oagis._9.PropertyType;
import org.openapplications.oagis._9.SemanticAttachmentType;
import org.openapplications.oagis._9.SequencedCodeType;
import org.openapplications.oagis._9.SequencedCodesType;
import org.openapplications.oagis._9.SpecificationType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.business.ItemBp;
import com.ussco.oagis._0.ClassificationType;
import com.ussco.oagis._0.GlobalItemType;
import com.ussco.oagis._0.ItemListType;
import com.ussco.oagis._0.ItemLocationType;
import com.ussco.oagis._0.ItemMasterHeaderType;
import com.ussco.oagis._0.ItemMasterType;
import com.ussco.oagis._0.MatchbookType;
import com.ussco.oagis._0.SyncItemMasterType;
import org.apache.xerces.dom.ElementNSImpl;

public class ECDB2DataMapper implements Processor {

    private static final String CARTRIDGE_NUMBER_SPECIFICATION_PROPERTY_NAME = "Market Indicator (Cartridge Number)";
    private static final Logger LOGGER = LoggerFactory.getLogger(ECDB2DataMapper.class);
    private static final String INVALID_IMAGE_URL = "NOA.JPG";
    public static final String USSCO_IMAGE_URL_BASE = "https://content.oppictures.com/Master_Images/Master_Variants/";
    public static final String USSCO_THUMBNAIL_IMAGE_URL = USSCO_IMAGE_URL_BASE + "/Variant_100";
    public static final String USSCO_STANDARD_IMAGE_URL = USSCO_IMAGE_URL_BASE + "/Variant_240";
    public static final String USSCO_LARGE_IMAGE_URL = USSCO_IMAGE_URL_BASE + "/Variant_400";
    public static final String USSCO_ZOOM_IMAGE_URL = USSCO_IMAGE_URL_BASE + "/Variant_1500";
    private static final String NO = "N";

    @Override
    public void process(Exchange exchng) throws Exception {
        MetadataMap<String, String> itemData = new MetadataMap<>();
        SyncItemMasterType syncItemMaster = (SyncItemMasterType) exchng.getIn().getBody();
        List<ItemMasterType> itemsToSync = syncItemMaster.getDataArea().getItemMaster();
        extractItemData(itemData, itemsToSync.get(0));
        
        // Catalog Name
        List<String> catalogName = new ArrayList<>();
        catalogName.add("USSCO Vendor Catalog");
        itemData.put("catalogName", catalogName);
        
        catalogName = new ArrayList<>();
        catalogName.add("USSCO Vendor Catalog");
        itemData.put(ItemBp.CATALOG, catalogName);
        
        // Vendor Name
        List<String> vendorName = new ArrayList<>();
        vendorName.add("USSCO");
        itemData.put(ItemBp.VENDOR_HEADER, vendorName);
        
        exchng.getOut().setHeaders(exchng.getIn().getHeaders());
        exchng.getOut().setBody(itemData);
    }

    public void extractItemData(MetadataMap<String, String> itemData, ItemMasterType itemMaster) {

        if (itemData == null) {
            itemData = new MetadataMap<String, String>();
        }
        ItemMasterHeaderType header = itemMaster.getItemMasterHeader();
        setMultimapValue(itemData, ItemBp.VENDOR_HEADER, "USSCO");

        String skuPrefix = "";
        String stockNumberButted = "";
        for (ItemIDType itemId : header.getItemID()) {
            if (itemId.getAgencyRole().equals("Prefix_Number")) {
                skuPrefix = itemId.getID().getValue();
            }
            else if (itemId.getAgencyRole().equals("Stock_Number_Butted")) {
                stockNumberButted = itemId.getID().getValue();
            }
        }
        String vendorSku = skuPrefix + stockNumberButted;

        if (header.getManufacturerItemID() != null && header.getManufacturerItemID().getID() != null
                && StringUtils.isNotBlank(header.getManufacturerItemID().getID().getValue())) {
            setMultimapValue(itemData, ItemBp.MANUFACTURER + ItemBp.SKU_SUFFIX, header.getManufacturerItemID().getID()
                    .getValue());
        }

        if (!StringUtils.isEmpty(header.getBrandId())) {
            setMultimapValue(itemData, ItemBp.BRAND_ID, header.getBrandId());
        }

        setMultimapValue(itemData, ItemBp.VENDOR_SKUTYPE + ItemBp.SKU_SUFFIX, vendorSku);
        setMultimapValue(itemData, ItemBp.MANUFACTURER, header.getManufacturerItemID().getID().getSchemeAgencyName());

        // Populate item data from itemMaster classifications
        String classifications = "";
        for (ClassificationType classification : header.getClassification()) {
            if (classification.getType().equals("SKU_Group")) {
                //do nothing
            }
            else if (classification.getCodes() != null && classification.getCodes().size() > 0
                    && classification.getCodes().get(0).getCode() != null
                    && classification.getCodes().get(0).getCode().size() > 0) {
                String code = classification.getCodes().get(0).getCode().get(0).getValue();
                if (classification.getType().equals("Product_Class_Category")) {
                    setMultimapValue(itemData, ItemBp.CATEGORY, code);
                }
                else if (classification.getType().equals("Assembly_Code") && StringUtils.isNotEmpty(code)
                        && !NO.equalsIgnoreCase(code)) {
                    classifications += "|" + classification.getType() + "=" + code;
                }
                else if (classification.getType().equals("Recycle_Indicator") && StringUtils.isNotEmpty(code)
                        && !NO.equalsIgnoreCase(code)) {
                    classifications += "|" + classification.getType() + "=" + code;
                }
                else if (classification.getType().equals("EPACPGCompliant_Code") && StringUtils.isNotEmpty(code)
                        && !NO.equalsIgnoreCase(code)) {
                    classifications += "|" + classification.getType() + "=" + code;
                }
                else if (classification.getType().equals("UNSPSC") && StringUtils.isNotEmpty(code)
                        && !NO.equalsIgnoreCase(code)) {
                    classifications += "|" + classification.getType() + "=" + code;
                }
                else if (classification.getType().equals("MSDS_Indicator") && StringUtils.isNotEmpty(code)
                        && !NO.equalsIgnoreCase(code)) {
                    classifications += "|" + classification.getType() + "=" + code;
                }
                else if (classification.getType().equals("Assembly_Code") && StringUtils.isNotEmpty(code)
                        && !NO.equalsIgnoreCase(code)) {
                    classifications += "|" + classification.getType() + "=" + code;
                }
                else if (classification.getType().equals("Green_Indicator") && StringUtils.isNotEmpty(code)
                        && !NO.equalsIgnoreCase(code)) {
                    classifications += "|" + classification.getType() + "=" + code;
                }
                else if (classification.getType().equals("ECDB3") && classification.getCodes().size() == 1) {
                    SequencedCodesType sequencedCodes = classification.getCodes().get(0);
                    SequencedCodeType leafNode = sequencedCodes.getCode().get(0);
                    for (SequencedCodeType sequencedCode : sequencedCodes.getCode()) {
                        if (sequencedCode.getSequence().compareTo(leafNode.getSequence()) > 0) {
                            leafNode = sequencedCode;
                        }
                    }
                    setMultimapValue(itemData, ItemBp.HIERARCHY_HEADER, leafNode.getListID());
                }
            }
        }
        if (StringUtils.isNotBlank(classifications)) {
            // Strip off the initial pipe
            classifications = classifications.substring(1);
            setMultimapValue(itemData, ItemBp.CLASSIFICATIONS, classifications);
        }
        String specifications = "";
        for (SpecificationType specification : header.getSpecification()) {
            for (PropertyType specProperty : specification.getProperty()) {
                if (specProperty.getNameValue() != null) {
                    if (CARTRIDGE_NUMBER_SPECIFICATION_PROPERTY_NAME.equals(specProperty.getNameValue().getName())) {
                        setMultimapValue(itemData, ItemBp.CARTRIDGE_NUMBER, specProperty.getNameValue().getValue());
                    }
                    String navigationPriority = "";
                    if (specProperty.getUserArea() != null && specProperty.getUserArea().getAny() != null) {
                        for (Object o : specProperty.getUserArea().getAny()) {
                            if (o instanceof String) {
                                StringUtils.isEmpty((String) o);
                            }
                            if (o instanceof ElementNSImpl) {
                                LOGGER.debug("found element");
                                ElementNSImpl element = (ElementNSImpl) o;
                                if ("FilterableNavigationPriority".equals(element.getLocalName())
                                        && element.getFirstChild() != null) {
                                    navigationPriority = element.getFirstChild().getNodeValue();
                                    if (!StringUtils.isEmpty(navigationPriority)) {
                                        if (navigationPriority.contains(ItemBp.SUB_DELIMITER)) {
                                            LOGGER.error("Navigation priority '" + navigationPriority + "' contains '"
                                                    + ItemBp.SUB_DELIMITER
                                                    + "'  this is unexpected and will corrupt the data");
                                        }
                                    }
                                }
                            }
                        }
                    }
                    specifications += "|" + specProperty.getNameValue().getName() + "="
                            + specProperty.getNameValue().getValue() + ItemBp.SUB_DELIMITER + navigationPriority;
                }
                if (specProperty.getDescription() != null) {
                    for (DescriptionType description : specProperty.getDescription()) {
                        if (description.getType().equals("Long_Item_Description")) {
                            setMultimapValue(itemData, ItemBp.NAME, description.getValue());
                        }
                        if (description.getType().equals("Item_Consolidated_Copy")) {
                            setMultimapValue(itemData, ItemBp.DESCRIPTION, description.getValue());
                        }
                    }
                }
            }
        }
        if (StringUtils.isNotBlank(specifications)) {
            // Strip off the initial pipe
            specifications = specifications.substring(1);
            setMultimapValue(itemData, ItemBp.SPECIFICATIONS, specifications);
        }
        if (header.getKeywords() != null && !header.getKeywords().isEmpty()) {
            setMultimapValue(itemData, ItemBp.SEARCH_TERMS, header.getKeywords().get(0));
        }

        StringBuilder itemImages = new StringBuilder();

        if (header.getDrawingAttachment() != null && !header.getDrawingAttachment().isEmpty()) {
            for (SemanticAttachmentType attachment : header.getDrawingAttachment()) {
                if (attachment.getFileName() != null && StringUtils.isNotBlank(attachment.getFileName().getValue())
                        && !INVALID_IMAGE_URL.equalsIgnoreCase(attachment.getFileName().getValue())) {
                    for (String url : attachment.getFileName().getValue().split("; ")) {
                        String mainImageFileURL = USSCO_STANDARD_IMAGE_URL + "/" + StringEscape.escapeForUrl(url);
                        itemImages.append(mainImageFileURL);
                        break;
                    }
                }
            }
        }

        for (AttachmentBaseType attachment : itemMaster.getItemMasterHeader().getAttachment()) {
            for (String url : attachment.getFileName().getValue().split("; ")) {
                if (!INVALID_IMAGE_URL.equalsIgnoreCase(url)) {
                    if (itemImages.length() != 0) {
                        itemImages.append(ItemBp.IN_ROW_DELIMETER);
                    }
                    itemImages.append(USSCO_STANDARD_IMAGE_URL).append("/").append(StringEscape.escapeForUrl(url));
                }
            }
        }
        setMultimapValue(itemData, "image URLs", itemImages.toString());

        if (itemMaster.getItemLocation() != null && itemMaster.getItemLocation().size() > 0) {
            ItemLocationType itemLocation = itemMaster.getItemLocation().get(0);
            if (itemLocation.getClassification() != null && itemLocation.getClassification().size() > 0) {
                for (ClassificationType classification : itemLocation.getClassification()) {
                    if (classification.getType().equals("Ship_Class_Code")) {
                        String shipClassCode = classification.getCodes().get(0).getCode().get(0).getValue();
                        setMultimapValue(itemData, "ship class code", shipClassCode);
                    }
                }
            }
        }
        if (itemMaster.getItemList() != null && itemMaster.getItemList().size() > 0) {
            ItemListType itemList = itemMaster.getItemList().get(0);
            BigDecimal listAmt = itemList.getListAmount().getValue();
            String listUnitCode = itemList.getListUnitCode();
            setMultimapValue(itemData, "list price", listAmt.toPlainString());
            setMultimapValue(itemData, ItemBp.UNIT_OF_MEASURE, listUnitCode);
        }
        if (itemMaster.getGlobalItem() != null) {
            for (GlobalItemType itemType : itemMaster.getGlobalItem()) {
                if (itemType != null && itemType.getItemWeight() != null && itemType.getItemWeight().getValue() != null) {
                    setMultimapValue(itemData, ItemBp.ITEM_WEIGHT, itemType.getItemWeight().getValue().toString());
                    break;
                }
            }
        }

        StringBuilder matchbookItems = new StringBuilder();
        for (MatchbookType matchbook : itemMaster.getMatchbook()) {
            if (matchbookItems.length() != 0) {
                matchbookItems.append(ItemBp.IN_ROW_DELIMETER);
            }
            if (!StringUtils.isEmpty(matchbook.getModel())) {
                matchbookItems.append(matchbook.getModel());
            }
            matchbookItems.append(ItemBp.SUB_DELIMITER);
            if (matchbook.getMatchbookManufacturer() != null
                    && !StringUtils.isEmpty(matchbook.getMatchbookManufacturer().getName())) {
                matchbookItems.append(matchbook.getMatchbookManufacturer().getName());
            }
            matchbookItems.append(ItemBp.SUB_DELIMITER);
            if (!StringUtils.isEmpty(matchbook.getDevice())) {
                matchbookItems.append(matchbook.getDevice());
            }
            matchbookItems.append(ItemBp.SUB_DELIMITER);
            if (!StringUtils.isEmpty(matchbook.getFamily())) {
                matchbookItems.append(matchbook.getFamily());
            }
        }
        if (matchbookItems.length() != 0) {
            setMultimapValue(itemData, ItemBp.MATCHBOOK, matchbookItems.toString());
        }
        StringBuilder sellingPoints = new StringBuilder();
        if (itemMaster.getItemMasterHeader() != null && itemMaster.getItemMasterHeader().getClassification() != null) {
            for (ClassificationType classification : itemMaster.getItemMasterHeader().getClassification()) {
                if (classification.getType().equals("SKU_Group") && classification.getNote() != null) {
                    for (NoteType note : classification.getNote()) {
                        if ((isSellingPoint(note.getType()) || isSellingPoint(note.getStatus()))
                                && !StringUtils.isBlank(note.getValue())) {
                            if (sellingPoints.length() != 0) {
                                sellingPoints.append(ItemBp.IN_ROW_DELIMETER);
                            }
                            sellingPoints.append(note.getValue());
                        }
                    }
                }
            }
        }
        if (StringUtils.isNotBlank(sellingPoints.toString())) {
            setMultimapValue(itemData, ItemBp.SELLING_POINT, sellingPoints.toString());
        }
    }

    @SuppressWarnings("static-method")
    private void setMultimapValue(MultivaluedMap<String, String> map, String key, String value) {
        if (!map.containsKey(key)) {
            List<String> values = new ArrayList<String>();
            map.put(key, values);
        }
        map.get(key).add(value);
    }

    private boolean isSellingPoint(String value) {
        return !StringUtils.isBlank(value) && value.startsWith("Selling_Point_");
    }
}