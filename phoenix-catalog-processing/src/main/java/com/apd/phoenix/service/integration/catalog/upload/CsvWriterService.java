package com.apd.phoenix.service.integration.catalog.upload;

import org.jboss.as.server.ServerEnvironment;
import org.jboss.msc.inject.Injector;
import org.jboss.msc.service.Service;
import org.jboss.msc.service.ServiceName;

public interface CsvWriterService extends Service<String> {

    public static final ServiceName SINGLETON_SERVICE_NAME = ServiceName.JBOSS.append("csv-writer", "ha", "singleton");

    public Injector<ServerEnvironment> getEnvInjector();
}
