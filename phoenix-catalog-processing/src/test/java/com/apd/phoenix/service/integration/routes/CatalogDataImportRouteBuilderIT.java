/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.service.integration.routes;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import org.apache.camel.Endpoint;
import org.apache.camel.EndpointInject;
import org.apache.camel.ExchangePattern;
import org.apache.camel.Produce;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.test.junit4.CamelTestSupport;
import org.apache.commons.io.IOUtils;
import org.junit.Ignore;
import org.junit.Test;

/**
 *
 * @author nreidelb
 */
public class CatalogDataImportRouteBuilderIT extends CamelTestSupport {

    @Produce
    private ProducerTemplate producer;

    @EndpointInject(uri = "direct:itemSource")
    private Endpoint itemSource;

    @EndpointInject(uri = "http://localhost:8080/administration/rest/items/xmlUpdate")
    private Endpoint restEndpoint;

    @EndpointInject(uri = "direct:relationshipSource")
    private Endpoint relationshipSource;

    // @EndpointInject(uri = "file:uspsCatalogCsvOutput")
    // private Endpoint relationshipTarget;

    @EndpointInject(uri = "http://localhost:8080/administration/rest/items/link")
    private Endpoint relationshipTarget;

    @Override
    protected RouteBuilder createRouteBuilder() {
        CatalogDataImportRouteBuilder catalogDataImportRouteBuilder = new CatalogDataImportRouteBuilder();

        catalogDataImportRouteBuilder.setItemSource(itemSource.getEndpointUri());
        catalogDataImportRouteBuilder.setItemTarget(this.restEndpoint.getEndpointUri());
        catalogDataImportRouteBuilder.setRelationshipSource(this.relationshipSource.getEndpointUri());
        catalogDataImportRouteBuilder.setRelationshipTarget(this.relationshipTarget.getEndpointUri());

        return catalogDataImportRouteBuilder;
    }

    @Test
    public void testImportRelationships() throws IOException {
        InputStream ips = this.getClass().getClassLoader().getResourceAsStream(
                "com/apd/phoenix/service/integration/mapping/relationships/itemRelationship-test.xml");

        StringWriter writer = new StringWriter();
        IOUtils.copy(ips, writer);
        String ediMessage = writer.toString();

        producer.sendBody(relationshipSource, ExchangePattern.InOut, ediMessage);
    }

    @Ignore
    @Test
    public void testImport() throws IOException {
        InputStream ips = this.getClass().getClassLoader().getResourceAsStream(
                "com/apd/phoenix/service/integration/mapping/ussco/syncItemMaster-test.xml");

        StringWriter writer = new StringWriter();
        IOUtils.copy(ips, writer);
        String ediMessage = writer.toString();

        producer.sendBody(itemSource, ExchangePattern.InOut, ediMessage);
    }
}
