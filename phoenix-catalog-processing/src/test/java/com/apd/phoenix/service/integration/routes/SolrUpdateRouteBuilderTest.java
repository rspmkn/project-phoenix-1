package com.apd.phoenix.service.integration.routes;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.camel.component.ActiveMQComponent;
import org.apache.activemq.pool.PooledConnectionFactory;
import org.apache.camel.CamelContext;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.component.jms.JmsConfiguration;
import org.apache.camel.impl.DefaultCamelContext;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SolrUpdateRouteBuilderTest {

    private static final Logger LOG = LoggerFactory.getLogger(SolrUpdateRouteBuilderTest.class);

    private static final String brokerUrl = "failover://tcp://localhost:61616";

    private CamelContext camelContext;

    private ProducerTemplate producer;

    @Before
    public void before() throws Exception {
        camelContext = new DefaultCamelContext();
        setupAmq(camelContext, brokerUrl);
        producer = camelContext.createProducerTemplate();
        camelContext.start();
    }

    @After
    public void after() throws Exception {
        camelContext.stop();
    }

    @Ignore
    @Test
    public void run() throws Exception {
        for (int i = 0; i < 30; i++) {
            producer.sendBody("activemq:queue.solr-atomic-catx-updateq", new Long(3681));
        }
    }

    public void setupAmq(CamelContext context, String jmsBrokerUrl) {
        ActiveMQConnectionFactory activeMQConnectionFactory = new ActiveMQConnectionFactory();
        activeMQConnectionFactory.setBrokerURL(jmsBrokerUrl);
        PooledConnectionFactory pooledConnectionFactory = new PooledConnectionFactory();
        pooledConnectionFactory.setConnectionFactory(activeMQConnectionFactory);
        pooledConnectionFactory.setMaxConnections(2);
        JmsConfiguration jmsConfiguration = new JmsConfiguration();
        jmsConfiguration.setConnectionFactory(pooledConnectionFactory);
        jmsConfiguration.setConcurrentConsumers(2);
        ActiveMQComponent activeMQComponent = new ActiveMQComponent();
        activeMQComponent.setConfiguration(jmsConfiguration);
        context.addComponent("activemq", activeMQComponent);
    }
}
