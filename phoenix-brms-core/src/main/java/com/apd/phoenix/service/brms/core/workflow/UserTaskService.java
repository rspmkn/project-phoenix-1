/**
 * 
 */
package com.apd.phoenix.service.brms.core.workflow;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.commons.lang.StringUtils;
import org.jbpm.services.task.utils.ContentMarshallerHelper;
import org.kie.api.task.TaskService;
import org.kie.api.task.model.Content;
import org.kie.api.task.model.OrganizationalEntity;
import org.kie.api.task.model.Status;
import org.kie.api.task.model.Task;
import org.kie.api.task.model.TaskData;
import org.kie.api.task.model.TaskSummary;
import org.kie.api.task.model.User;
import org.kie.internal.task.api.ContentMarshallerContext;
import org.kie.internal.task.api.TaskContentService;
import org.kie.internal.task.api.TaskContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.apd.phoenix.core.PageNumberDisplay;
import com.apd.phoenix.service.brms.core.impl.TaskServiceClientFactoryImpl;
import com.apd.phoenix.service.business.CustomerOrderBp;
import com.apd.phoenix.service.business.SecurityContext;
import com.apd.phoenix.service.business.SystemUserBp;
import com.apd.phoenix.service.business.WorkflowLogBp;
import com.apd.phoenix.service.executor.ExecutorServiceEntryPoint;
import com.apd.phoenix.service.executor.command.api.CompleteTaskCommand;
import com.apd.phoenix.service.model.CustomerOrder;
import com.apd.phoenix.service.model.WorkflowLog.TaskEvent;
import com.apd.phoenix.service.workflow.WorkflowService;

/**
 * @author Red Hat Consulting - Red Hat, Inc.
 *
 */
@Stateless
@LocalBean
public class UserTaskService {

    private static final String CUSTOMER_SERVICE_MANAGER = "customer service manager";

    private static final Logger logger = LoggerFactory.getLogger(UserTaskService.class);

    private static final String TASK_ADMIN = "Administrator";

    public static final String APPROVAL_TASK = "approveOrder";

    private static final String APD_CUSTOMER_SERVICE = "APD Customer Service";

    private static final String CUSTOMER_SERVICE = " Customer Service";

    @Inject
    TaskServiceClientFactoryImpl taskClientFactory;

    @Inject
    SystemUserBp systemUserBp;

    @Inject
    CustomerOrderBp customerOrderBp;

    @Inject
    private SecurityContext securityContext;

    @Inject
    private WorkflowLogBp workflowLogBp;

    @Inject
    protected ExecutorServiceEntryPoint executor;
    
    @Inject
    private WorkflowService workflowService;

    //TODO: MULTI-143 instead of hitting the datasource, use the JBPM 6 API
    @PersistenceContext(unitName = "jbpm")
    private EntityManager em;

    public List<PhoenixTaskSummary> getOrderTasks(CustomerOrder customerOrder) {
        try {
            if (customerOrder == null || customerOrder.getApdPo() == null
                    || StringUtils.isBlank(customerOrder.getApdPo().getValue())) {
                return new ArrayList<PhoenixTaskSummary>();
            }
            PhoenixTaskSearchCriteria criteria = new PhoenixTaskSearchCriteria();
            criteria.setApdPo(customerOrder.getApdPo().getValue());
            return this.criteriaQuery(criteria, 0, 0);
        }
        catch (Exception e) {
            logger.error("Exception when trying to fetch order tasks", e);
            return new ArrayList<PhoenixTaskSummary>();
        }
    }

    public PhoenixTaskSummary getApprovalTask(CustomerOrder customerOrder, boolean automatedAction) {
        String apdPo = customerOrder.getApdPo().getValue();
        String approver = customerOrderBp.getApprover(customerOrder);
        List<PhoenixTaskSummary> results = this.getAssignedTasks(approver, automatedAction);
        for (PhoenixTaskSummary taskSummary : results) {
            //There can only be one approval task for an APD PO#
            if (taskSummary.getName().equals(APPROVAL_TASK) && taskSummary.getDescription().equals(apdPo)) {
                return taskSummary;
            }
        }
        return null;
    }

    public long numberOfAvailableTasks(String user) {
        return this.numberOfAvailableTasks(user, null, false);
    }

    public long numberOfAvailableTasks(String user, String taskType) {
        return this.numberOfAvailableTasks(user, taskType, false);
    }

    public long numberOfAvailableTasks(String user, String taskType, boolean showCompleted) {
        PhoenixTaskSearchCriteria criteria = this.availableTasksCriteria(user, showCompleted);
        if (StringUtils.isNotBlank(taskType)) {
            criteria.setName(taskType);
        }
        return this.criteriaQueryQuantity(criteria);
    }

    public List<PhoenixTaskSummary> getAvailableTasksPaginated(String user, String taskType,
            PageNumberDisplay pageNumberDisplay) {
        return this.getAvailableTasksPaginated(user, taskType, pageNumberDisplay, false);
    }

    public List<PhoenixTaskSummary> getAvailableTasksPaginated(String user, String taskType,
            PageNumberDisplay pageNumberDisplay, boolean showCompleted) {
        PhoenixTaskSearchCriteria criteria = this.availableTasksCriteria(user, showCompleted);
        if (StringUtils.isNotBlank(taskType)) {
            criteria.setName(taskType);
        }
        List<PhoenixTaskSummary> result = this.criteriaQuery(criteria, pageNumberDisplay.getSize(), (pageNumberDisplay
                .getPage() - 1)
                * pageNumberDisplay.getSize());
        return result;
    }

    private PhoenixTaskSearchCriteria availableTasksCriteria(String user, boolean showCompleted) {
        PhoenixTaskSearchCriteria criteria = new PhoenixTaskSearchCriteria();
        criteria.setGroups(systemUserBp.getRolesForUser(user));
        if (!criteria.getGroups().contains(APD_CUSTOMER_SERVICE)) {
            for (String accountName : systemUserBp.getAccountsForUser(user)) {
                criteria.getGroups().add(accountName + CUSTOMER_SERVICE);
            }
        }
        criteria.getTaskStatus().add(Status.Ready);
        criteria.getNotTaskStatus().add(Status.Exited);
        criteria.setShowCompleted(showCompleted);
        return criteria;
    }

    public long numberOfAssignedTasks(String user, boolean automatedAction) {
        return this.numberOfAssignedTasks(user, null, false, automatedAction);
    }

    public long numberOfAssignedTasks(String user, String taskType, boolean automatedAction) {
        return this.numberOfAssignedTasks(user, taskType, false, automatedAction);
    }

    public long numberOfAssignedTasks(String user, String taskType, boolean showCompleted, boolean automatedAction) {
        return this.criteriaQueryQuantity(this.assignedTasksCriteria(user, taskType, showCompleted, automatedAction));
    }

    public List<PhoenixTaskSummary> getAssignedTasks(String user, boolean automatedAction) {
        return this.getAssignedTasks(user, null, 0, 0, false, automatedAction);
    }

    //Automated Action is true if there is a security context, false otherwise
    public List<PhoenixTaskSummary> getAssignedTasksPaginated(String user, String taskType,
            PageNumberDisplay pageNumberDisplay, boolean showCompleted, boolean automatedAction) {
        return this.getAssignedTasks(user, taskType, pageNumberDisplay.getSize(), (pageNumberDisplay.getPage() - 1)
                * pageNumberDisplay.getSize(), showCompleted, automatedAction);
    }

    private List<PhoenixTaskSummary> getAssignedTasks(String user, String taskType, int size, int offset,
            boolean showCompleted, boolean automatedAction) {
        return this.criteriaQuery(this.assignedTasksCriteria(user, taskType, showCompleted, automatedAction), size,
                offset);
    }

    private PhoenixTaskSearchCriteria assignedTasksCriteria(String user, String taskType, boolean showCompleted,
            boolean automatedAction) {
        PhoenixTaskSearchCriteria criteria = new PhoenixTaskSearchCriteria();

        criteria.setOwner(user);
        if (StringUtils.isNotBlank(taskType)) {
            criteria.setName(taskType);
        }

        if (automatedAction || securityContext.hasPermission(CUSTOMER_SERVICE_MANAGER)) {
            criteria.setUserRequired(false);
            criteria.setGroups(systemUserBp.getRolesForUser(user));
            if (!criteria.getGroups().contains(APD_CUSTOMER_SERVICE)) {
                for (String accountName : systemUserBp.getAccountsForUser(user)) {
                    criteria.getGroups().add(accountName + CUSTOMER_SERVICE);
                }
            }
            criteria.getNotTaskStatus().add(Status.Created);
            criteria.getNotTaskStatus().add(Status.Ready);
        }
        criteria.getNotTaskStatus().add(Status.Exited);
        criteria.setShowCompleted(showCompleted);
        return criteria;
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public boolean claimTask(long taskId, String user) {
        TaskService client = null;
        try {
            client = this.getClient();
            client.claim(taskId, user);
            workflowLogBp.createWorkflowLog(taskId, user, TaskEvent.CLAIM);
            return true;
        }
        catch (Exception e) {
            logger.error("Unable to claim task id=" + taskId + " as user=" + user + " because of :" + e.toString());
            return false;
        }
    }

    public boolean exitTask(long taskId) {
        TaskService client = null;
        try {
            client = this.getClient();
            client.exit(taskId, TASK_ADMIN);
            workflowLogBp.createWorkflowLog(taskId, TASK_ADMIN, TaskEvent.EXIT);
            return true;
        }
        catch (Exception e) {
            logger
                    .error("Unable to exit task id=" + taskId + " as user=" + TASK_ADMIN + " because of :"
                            + e.toString());
            return false;
        }
    }

    public boolean suspendTask(long taskId, String user) {
        TaskService client = null;
        try {
            client = this.getClient();
            client.suspend(taskId, user);
            workflowLogBp.createWorkflowLog(taskId, user, TaskEvent.SUSPEND);
            return true;
        }
        catch (Exception e) {
            logger.error("Unable to suspend task id=" + taskId + " as user=" + user + " because of :" + e.toString());
            return false;
        }
    }

    public boolean forwardTask(long taskId, String user, String forwardToId) {
        TaskService client = null;
        try {
            client = this.getClient();
            client.forward(taskId, user, forwardToId);
            workflowLogBp.createWorkflowLog(taskId, user, forwardToId, TaskEvent.FORWARD);
            return true;
        }
        catch (Exception e) {
            logger.error("Unable to forward task id=" + taskId + " as user=" + user + " because of :" + e.toString());
            return false;
        }
    }

    public boolean forwardTaskAsAdmin(long taskId, String forwardToId) {
        return this.forwardTask(taskId, TASK_ADMIN, forwardToId);
    }

    public boolean releaseTask(long taskId, String user) {
        TaskService client = null;
        Task task = this.getTask(taskId);
        try {
            client = this.getClient();
            if (task != null && task.getTaskData().getStatus().equals(Status.InProgress)) {
                client.stop(taskId, user);
            }
            client.release(taskId, user);
            workflowLogBp.createWorkflowLog(taskId, user, TaskEvent.RELEASE);
            return true;
        }
        catch (Exception e) {
            logger.error("Unable to release task id=" + taskId + " as user=" + user + " because of :" + e.toString());
            return false;
        }
    }

    public boolean delegateTask(long taskId, String user, String toUser) {
        TaskService client = null;
        try {
            client = this.getClient();
            client.delegate(taskId, user, toUser);
            client.claim(taskId, toUser);
            workflowLogBp.createWorkflowLog(taskId, user, toUser, TaskEvent.DELEGATE);
            return true;
        }
        catch (Exception e) {
            logger.error("Unable to delegate task id=" + taskId + " as user=" + user + " because of :" + e.toString());
            return false;
        }
    }

    public boolean resumeTask(long taskId, String user) {
        TaskService client = null;
        try {
            client = this.getClient();
            client.resume(taskId, user);
            workflowLogBp.createWorkflowLog(taskId, user, TaskEvent.RESUME);
            return true;
        }
        catch (Exception e) {
            logger.error("Unable to resume task id=" + taskId + " as user=" + user + " because of :" + e.toString());
            return false;
        }
    }

    public boolean removeTask(long taskId, String user, TaskEvent event) {
        TaskService client = null;
        try {
            client = this.getClient();
            client.exit(taskId, user);
            workflowLogBp.createWorkflowLog(taskId, user, event);
            return true;
        }
        catch (Exception e) {
            logger.error("Unable to exit task id=" + taskId + " as user=" + user + " because of :" + e.toString());
            return false;
        }
    }

    public List<PhoenixTaskSummary> getBackOrderHumanTasks(CustomerOrder order) {
        return getOpenOrderTasks(order, "resolveBackorderedItems");

    }

    public List<PhoenixTaskSummary> getItemRejectionTasks(CustomerOrder order) {
        return getOpenOrderTasks(order, "resolveItemRejection");

    }

    public List<PhoenixTaskSummary> getOpenOrderTasks(CustomerOrder order) {
        return this.getOpenOrderTasks(order, null);
    }

    private List<PhoenixTaskSummary> getOpenOrderTasks(CustomerOrder order, String taskName) {
		PhoenixTaskSearchCriteria criteria = new PhoenixTaskSearchCriteria();
        criteria.setApdPo(order.getApdPo().getValue());
        criteria.setName(taskName);
        List<Status> closedStatuses = new ArrayList<>();
        closedStatuses.add(Status.Completed);
        closedStatuses.add(Status.Exited);
        closedStatuses.add(Status.Failed);
        closedStatuses.add(Status.Obsolete);
        criteria.setNotTaskStatus(closedStatuses);
        List<PhoenixTaskSummary> items = (List<PhoenixTaskSummary>) this.criteriaQuery(criteria, 0, 0);
        if (items == null || items.isEmpty()) {
            return new ArrayList<>();
        }
        return items;
	}

    //use the method on WorkflowService instead
    @Deprecated
    public void completeTask(long taskId, long processId, String user, Map<String, Object> params) {
        workflowService.completeTask(taskId, processId, user, params);
    }

    //use the method on WorkflowService instead
    @Deprecated
    public void completeTask(long taskId, long processId, CompleteTaskCommand.Type domainIdType, String user,
            Map<String, Object> params) {
        workflowService.completeTask(taskId, processId, domainIdType, user, params);
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public boolean startTask(long taskId, String user) {
        TaskService client = null;
        try {
            client = this.getClient();
            client.start(taskId, user);
            workflowLogBp.createWorkflowLog(taskId, user, TaskEvent.START);
            return true;
        }
        catch (Exception e) {
            logger.error("Unable to start task id=" + taskId + " as user=" + user + " because of :" + e.toString());
            return false;
        }
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public Task getTask(long taskId) {
        TaskService client = null;
        try {
            client = this.getClient();
            return client.getTaskById(taskId);
        }
        catch (Exception e) {
            logger.error("Unable to start task id=" + taskId + " because of :" + e.toString());
            return null;
        }
    }

    public Map<String, Object> getTaskContent(long taskId) {
        TaskService client = taskClientFactory.getTaskService();
        if (client == null) {
            logger.error("Unable to retrieve TaskService");
            return new HashMap<>();
        }
        return client.getTaskContent(taskId);
    }

    private TaskService getClient() throws Exception {
        TaskService client = taskClientFactory.getTaskService();
        if (client == null) {
            throw new Exception("Could not create task client");
        }
        else {
            return client;
        }
    }

    public PhoenixTaskSummary taskToSummary(Task task) {
        if (task == null) {
            return null;
        }
        long processInstanceId = 0;
        Status status = null;
        User actualOwner = null;
        User createdBy = null;
        Date createdOn = null;
        Date activationTime = null;
        Date expirationTime = null;
        String processId = null;
        long processSessionId = 0;
        String account = null;
        long parentId = 0;
        String deploymentId = null;
        if (task.getTaskData() != null) {
            processInstanceId = task.getTaskData().getProcessInstanceId();
            status = task.getTaskData().getStatus();
            actualOwner = task.getTaskData().getActualOwner();
            createdBy = task.getTaskData().getCreatedBy();
            createdOn = task.getTaskData().getCreatedOn();
            activationTime = task.getTaskData().getActivationTime();
            expirationTime = task.getTaskData().getExpirationTime();
            processId = task.getTaskData().getProcessId();
            processSessionId = task.getTaskData().getProcessSessionId();
            parentId = task.getTaskData().getParentId();
            deploymentId = task.getTaskData().getDeploymentId();
        }
        String name = null;
        if (task.getNames() != null && !task.getNames().isEmpty() && task.getNames().get(0) != null) {
            name = task.getNames().get(0).getText();
        }
        String subject = null;
        if (task.getSubjects() != null && !task.getSubjects().isEmpty() && task.getSubjects().get(0) != null) {
            subject = task.getSubjects().get(0).getText();
        }
        String description = null;
        if (task.getDescriptions() != null && !task.getDescriptions().isEmpty()
                && task.getDescriptions().get(0) != null) {
            description = task.getDescriptions().get(0).getText();
        }
        if (task.getPeopleAssignments() != null && task.getPeopleAssignments().getPotentialOwners() != null) {
            for (OrganizationalEntity entity : task.getPeopleAssignments().getBusinessAdministrators()) {
                if (entity != null && StringUtils.isNotBlank(entity.getId())
                        && entity.getId().endsWith(" Customer Service") && !entity.equals("APD Customer Service")) {
                    account = entity.getId().substring(0, entity.getId().indexOf(" Customer Service"));
                }
            }
        }
        PhoenixTaskSummary toReturn = new PhoenixTaskSummary(task.getId(), processInstanceId, name, subject,
                description, status, task.getPriority(), false, actualOwner, createdBy, createdOn, activationTime,
                expirationTime, processId, processSessionId, account, parentId, deploymentId);
        return toReturn;
    }

    public Task taskSummaryToTask(TaskSummary summary) {
        if (summary == null) {
            return null;
        }
        return this.getTask(summary.getId());
    }

    @SuppressWarnings("unchecked")
	private List<PhoenixTaskSummary> criteriaQuery(PhoenixTaskSearchCriteria criteria, int size, int offset) {
        ArrayList<PhoenixTaskSummary> toReturn = new ArrayList<PhoenixTaskSummary>();
        try {
            String hql = "SELECT distinct t " + this.getCriteriaQuery(criteria);
            Query query = em.createQuery(hql).setFirstResult(offset).setMaxResults(size);
            for (Task task : (List<Task>) query.getResultList()) {
                toReturn.add(this.taskToSummary(task));
            }
            return toReturn;
        }
        catch (Exception e) {
            logger.error("Could not get tasks for the criteria", e);
            return toReturn;
        }
    }

    @SuppressWarnings("unchecked")
	private long criteriaQueryQuantity(PhoenixTaskSearchCriteria criteria) {
        try {
            String hql = "SELECT count(distinct t.id) " + this.getCriteriaQuery(criteria);
            Query query = em.createQuery(hql).setMaxResults(1);
            return (Long) ((List<Long>) (query.getResultList())).get(0);
        }
        catch (Exception e) {
            logger.error("Could not get number of tasks for the criteria", e);
            return 0l;
        }
    }

    private String getCriteriaQuery(PhoenixTaskSearchCriteria criteria) {
        StringBuilder toReturn = new StringBuilder("FROM TaskImpl t ");
        toReturn
                .append("LEFT JOIN t.taskData AS compareData LEFT JOIN compareData.actualOwner AS compareOwner LEFT JOIN t.names AS compareNames "
                        + "LEFT JOIN t.descriptions AS compareDescriptions "
                        + "LEFT JOIN t.peopleAssignments.potentialOwners AS comparePotentialOwners ");
        toReturn.append("WHERE 1=1 ");
        if (StringUtils.isNotBlank(criteria.getApdPo())) {
            toReturn.append("AND compareDescriptions.shortText = '" + criteria.getApdPo() + "' ");
        }
        if (StringUtils.isNotBlank(criteria.getName())) {
            toReturn.append("AND compareNames.shortText = '" + criteria.getName() + "' ");
        }
        if (StringUtils.isNotBlank(criteria.getOwner()) && criteria.isUserRequired()) {
            toReturn.append("AND lower(compareOwner.id) = '" + criteria.getOwner().toLowerCase() + "' ");
        }
        if (criteria.getGroups() != null && !criteria.getGroups().isEmpty()) {
            toReturn.append("AND (comparePotentialOwners.id IN (");
            for (int i = 0; i < criteria.getGroups().size(); i++) {
                if (i != 0) {
                    toReturn.append(", ");
                }
                toReturn.append("'" + criteria.getGroups().get(i) + "'");
            }
            toReturn.append(") ");
            //if the group and owner are both specified, and the owner doesn't have a group that the task is in, displays the task anyway
            if (StringUtils.isNotBlank(criteria.getOwner())) {
                toReturn.append(" OR  lower(compareOwner.id) = '" + criteria.getOwner().toLowerCase() + "'");
            }
            toReturn.append(" ) ");
        }
        if (criteria.getTaskStatus() != null && !criteria.getTaskStatus().isEmpty()) {
            toReturn.append("AND compareData.status IN (");
            for (int i = 0; i < criteria.getTaskStatus().size(); i++) {
                if (i != 0) {
                    toReturn.append(", ");
                }
                toReturn.append("'" + criteria.getTaskStatus().get(i) + "'");
            }
            toReturn.append(") ");
        }
        if (criteria.getNotTaskStatus() != null && !criteria.getNotTaskStatus().isEmpty()) {
            toReturn.append("AND compareData.status NOT IN (");
            for (int i = 0; i < criteria.getNotTaskStatus().size(); i++) {
                if (i != 0) {
                    toReturn.append(", ");
                }
                toReturn.append("'" + criteria.getNotTaskStatus().get(i) + "'");
            }
            toReturn.append(") ");
        }
        toReturn.append("ORDER BY compareData.createdOn desc");
        return toReturn.toString();
    }

	@SuppressWarnings("unchecked")
	public List<String> getAllOrderTypes() {
        try {
            String hql = "SELECT distinct names.shortText from TaskImpl t JOIN t.names AS names";
            Query query = em.createQuery(hql);
            return (List<String>)query.getResultList();
        }
        catch (Exception e) {
            logger.error("Could not get tasks for the criteria", e);
            return new ArrayList<String>();
        }
    }
}
