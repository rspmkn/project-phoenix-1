/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.service.brms.core.impl;

import java.net.URL;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.ejb.Stateful;
import javax.enterprise.context.ApplicationScoped;

import org.drools.core.io.impl.UrlResource;
import org.kie.api.KieBase;
import org.kie.api.KieServices;
import org.kie.api.builder.KieModule;
import org.kie.api.builder.KieRepository;
import org.kie.api.runtime.KieContainer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.apd.phoenix.core.utility.TenantConfigRepository;
import com.apd.phoenix.service.brms.core.api.KieBaseFactory;
import com.apd.phoenix.service.workflow.WorkflowService;

@Stateful
@ApplicationScoped
public class KieBaseFactoryImpl implements KieBaseFactory {

    private static final Logger LOG = LoggerFactory.getLogger(KieBaseFactoryImpl.class);
    private static final String DEFAULT_URL = "http://localhost:8080/business-central";
    private static final Integer CACHE_SIZE = 5;
    private Properties guvnorProps = TenantConfigRepository.getInstance().getProperties("guvnor");
    private Map<String,KieBase> kieSessions = new HashMap<String,KieBase>();
    private List<String> keys = new LinkedList<String>();

    @Override
    public void rebuild() {
        this.rebuild(guvnorProps.getProperty("snapshot"));
    }

    @Override
    public void rebuild(String snapshot) {
        this.build(snapshot);
    }

    @Override
    public KieBase getKieBase() {
        return this.getKieBase(guvnorProps.getProperty("snapshot"));
    }

    @Override
    public KieBase getKieBase(String snapshot) {
    	if (WorkflowService.isSnapshotLegacy(snapshot)) {
    		//if this is a legacy snapshot (from before the major 3.0 changes), the latest
    		//snapshot is used instead
    		snapshot = guvnorProps.getProperty("snapshot");
    	}
        if (this.kieSessions.get(snapshot) == null) {
            this.build(snapshot);
        }

        return this.kieSessions.get(snapshot);
    }

    private void build(String snapshot) {
    	String APP_URL = guvnorProps.getProperty("url", DEFAULT_URL) + "/maven2/com/apd/phoenix/" + snapshot + "/phoenix-" + snapshot + ".jar";
		try {
			URL url = new URL(APP_URL);
			String USER =  guvnorProps.getProperty("username");
			String PASSWORD =  guvnorProps.getProperty("password");
			
			KieServices kieServices = KieServices.Factory.get();
			UrlResource urlResource = (UrlResource) kieServices.getResources().newUrlResource(url);
			urlResource.setUsername(USER);
			urlResource.setPassword(PASSWORD);
			urlResource.setBasicAuthentication("enabled");
			KieRepository kr = kieServices.getRepository();
			KieModule kModule = kr.addKieModule(kieServices.getResources().newInputStreamResource(urlResource.getInputStream()));
			KieContainer kContainer = kieServices.newKieContainer( kModule.getReleaseId() );
			
			KieBase kieBase = kContainer.getKieBase();
	    	
	        LOG.info("Building KieBase (" + snapshot + ")");

	        if (!kieSessions.containsKey(snapshot) && kieSessions.size() == CACHE_SIZE) {
	        	kieSessions.remove(keys.remove(0));
	        }
	        keys.add(snapshot);
	        this.kieSessions.put(snapshot, kieBase);

		} catch (Exception e) {
			LOG.error("Error opening BRMS URL: "+ APP_URL, e);
		}
		
    }

}
