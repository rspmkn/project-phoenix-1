package com.apd.phoenix.service.brms.core.workflow;

import java.util.Comparator;
import org.kie.api.task.model.TaskSummary;

public class TaskComparator implements Comparator<TaskSummary> {

    @Override
    public int compare(TaskSummary arg0, TaskSummary arg1) {
        if (arg0 == null || arg0.getCreatedOn() == null) {
            return 1;
        }
        if (arg1 == null || arg1.getCreatedOn() == null) {
            return -1;
        }
        return arg1.getCreatedOn().compareTo(arg0.getCreatedOn());
    }

}
