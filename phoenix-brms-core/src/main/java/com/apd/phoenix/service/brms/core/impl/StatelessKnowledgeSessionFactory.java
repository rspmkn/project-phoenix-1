package com.apd.phoenix.service.brms.core.impl;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import org.kie.api.KieBase;
import org.kie.api.runtime.StatelessKieSession;

import com.apd.phoenix.service.brms.core.api.KieBaseFactory;

@Stateless
@LocalBean
public class StatelessKnowledgeSessionFactory {

    @Inject
    KieBaseFactory knowledgeBaseFactory;

    public StatelessKieSession getStatelessKieSession() {
        KieBase kbase = knowledgeBaseFactory.getKieBase();
        return kbase.newStatelessKieSession();
    }
}
