package com.apd.phoenix.service.brms.core.impl;

import org.kie.api.task.TaskService;

public interface TaskServiceServerFactory {

    public TaskService createTaskServiceServer();
}
