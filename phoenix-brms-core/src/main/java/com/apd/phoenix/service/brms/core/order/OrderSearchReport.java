package com.apd.phoenix.service.brms.core.order;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.business.CustomerOrderBp;
import com.apd.phoenix.service.business.CustomerOrderSearchCriteria;
import com.apd.phoenix.service.business.SecurityContext;
import com.apd.phoenix.service.model.CustomerOrder;
import com.apd.phoenix.service.persistence.jpa.SystemUserDao;

@Stateless
@LocalBean
public class OrderSearchReport {

    private static final Logger LOGGER = LoggerFactory.getLogger(OrderSearchReport.class);

    public static final String VIEW_DATA_FOR_ALL_ACCOUNTS = "view data for all accounts";

    @Inject
    private CustomerOrderBp customerOrderBp;

    @Inject
    private OrderSearchReportBp orderSearchReportBp;

    @Inject
    private SecurityContext securityContext;

    @Inject
    private SystemUserDao systemUserDao;

    public List<String> getOrderSearchReport(CustomerOrderSearchCriteria criterion) {

        String user = securityContext.getUserLogin();
        if (!securityContext.hasPermission(VIEW_DATA_FOR_ALL_ACCOUNTS)) {
            criterion.setAccountWhiteList(systemUserDao.getAccountIds(user));
        }

        List<String> orderList = new ArrayList<String>();
        int resultsize = customerOrderBp.getCustomerOrdersCount(criterion);
        orderList
                .add("Order Name,APD Order,Customer Order,Open Tasks?,Placed On,Placed By,Order Status,Merchandise Total,Sales Tax Total,Shipping/Handling,Order Total");

        int start = 0;
        boolean flag = false;
        int size = 100;
        if (resultsize > 100) {
            for (int i = 1; i <= 1000; i++) {

                criterion.setStart(start);
                criterion.setPageSize(size);
                orderList.addAll(getCustomerOrdersAsList(criterion));
                if (flag)
                    break;

                if (start > resultsize) {
                    size = start - resultsize;
                    flag = true;
                }
                else {
                    start = start + size;
                }

            }
        }
        else {
            orderList.addAll(getCustomerOrdersAsList(criterion));
        }

        return orderList;

    }

    private List<String> getCustomerOrdersAsList(CustomerOrderSearchCriteria criterion) {
    	
    	List<String> toReturn = new ArrayList<>();
		List<CustomerOrder> customerOrders = orderSearchReportBp.getCustomerOrdersPaginated(criterion);
		StringBuilder line = null;
		
		for (CustomerOrder co : customerOrders) {
			line = new StringBuilder();
			line.append(co.getAccount() == null ? "" : co.getAccount().getName()).append(",");
			line.append(co.getApdPo() == null ? "" : co.getApdPo().getValue()).append(",");
			line.append(co.getCustomerPo() != null ? co.getCustomerPo().getValue() : co.getBlanketPo() == null ? "" : co.getBlanketPo().getValue()).append(",");
			line.append(orderSearchReportBp.hasOpenTasks(co) == "Y" ? "Y" : "N").append(",");
			line.append(co.getOrderDate() == null ? "" : new SimpleDateFormat("MM/dd/yyyy").format(co.getOrderDate())).append(",");
			line.append(co.getUser() == null ? "" : co.getUser().getLogin()).append(",");
			line.append(co.getStatus() == null ? "" : co.getStatus().getValue().replace(",", "")).append(",");
			line.append(co.getSubtotal() == null ? "" : (co.getSubtotal().compareTo(new BigDecimal(".001")) < 0 ? "" : co.getSubtotal() + "")).append(",");
			line.append(co.getMaximumTaxToCharge() == null ? "" : (co.getMaximumTaxToCharge().compareTo(new BigDecimal(".001")) < 0 ? "" : co.getMaximumTaxToCharge() + "")).append(",");
			line.append(co.getEstimatedShippingAmount() == null ? "" : (co.getEstimatedShippingAmount().compareTo(new BigDecimal(".001")) < 0 ? "" : co.getEstimatedShippingAmount() + "")).append(",");
			line.append(co.getOrderTotal() == null ? "" : (co.getOrderTotal().compareTo(new BigDecimal(".001")) < 0 ? "" : co.getOrderTotal() + ""));
			toReturn.add(line.toString());
		}		
		return toReturn;
	}}
