/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.service.brms.core.api;

import org.kie.api.KieBase;

public interface KieBaseFactory {

    public void rebuild();

    public void rebuild(String snapshot);

    public KieBase getKieBase();

    public KieBase getKieBase(String snapshot);
}
