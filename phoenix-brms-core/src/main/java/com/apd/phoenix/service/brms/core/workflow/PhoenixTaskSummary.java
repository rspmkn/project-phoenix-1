package com.apd.phoenix.service.brms.core.workflow;

import java.util.Date;
import org.kie.api.task.model.Status;
import org.kie.api.task.model.User;
import org.jbpm.services.task.query.TaskSummaryImpl;

public class PhoenixTaskSummary extends TaskSummaryImpl {

    public PhoenixTaskSummary(Long id, Long processInstanceId, String name, String subject, String description,
            Status status, int priority, boolean skippable, User actualOwner, User createdBy, Date createdOn,
            Date activationTime, Date expirationTime, String processId, long processSessionId, String account, 
            long parentId, String deploymentId) {
        super(id, name, subject, description, status, priority, skippable, getUserId(actualOwner), getUserId(createdBy),
                createdOn, activationTime, expirationTime, processId, processInstanceId, processSessionId, parentId, deploymentId);
        
        this.account = account;
    }

    private String account;

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }
    
    private static String getUserId(User user) {
    	return user != null ? user.getId() : null;
    }
}
