package com.apd.phoenix.service.brms.core.order;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import org.kie.api.task.model.Status;
import com.apd.phoenix.service.brms.core.workflow.PhoenixTaskSummary;
import com.apd.phoenix.service.brms.core.workflow.UserTaskService;
import com.apd.phoenix.service.business.CustomerOrderSearchCriteria;
import com.apd.phoenix.service.model.CustomerOrder;
import com.apd.phoenix.service.persistence.jpa.CustomerOrderDao;

@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
@Stateless
@LocalBean
public class OrderSearchReportBp {

    @Inject
    private UserTaskService userTaskService;

    @Inject
    private CustomerOrderDao customerOrderDao;

    public String hasOpenTasks(CustomerOrder customerOrder) {

        List<PhoenixTaskSummary> results = userTaskService.getOrderTasks(customerOrder);
        if (results != null && !results.isEmpty()) {
            for (PhoenixTaskSummary task : results) {
                if (!task.getStatus().equals(Status.Completed) && !task.getStatus().equals(Status.Error)
                        && !task.getStatus().equals(Status.Exited) && !task.getStatus().equals(Status.Failed)
                        && !task.getStatus().equals(Status.Obsolete)) {
                    return "Y";
                }
            }
        }
        return "N";
    }

    public List<CustomerOrder> getCustomerOrdersPaginated(CustomerOrderSearchCriteria criteria) {

        List<CustomerOrder> coList = customerOrderDao.getCustomerOrdersPaginated(criteria);

        if (coList == null) {
            coList = new ArrayList<CustomerOrder>();
        }

        return coList;
    }

}
