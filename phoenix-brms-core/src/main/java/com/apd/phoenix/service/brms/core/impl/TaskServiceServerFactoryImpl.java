package com.apd.phoenix.service.brms.core.impl;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.jbpm.services.task.HumanTaskConfigurator;
import org.kie.api.task.TaskService;

@Stateless
@LocalBean
public class TaskServiceServerFactoryImpl implements TaskServiceServerFactory {

    @PersistenceContext(unitName = "jbpm")
    private EntityManager em;
    
    @Inject
    private UserGroupCallbackImpl callback;

    @Override
    public TaskService createTaskServiceServer() {
    	return new HumanTaskConfigurator().entityManagerFactory(em.getEntityManagerFactory()).userGroupCallback(callback).getTaskService();
    }
}
