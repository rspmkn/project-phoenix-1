package com.apd.phoenix.service.brms.core.knowledge;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import org.kie.api.runtime.StatelessKieSession;

import com.apd.phoenix.service.brms.core.impl.StatelessKnowledgeSessionFactory;

@Stateless
@LocalBean
public class KnowledgeService {

    @Inject
    StatelessKnowledgeSessionFactory statelessKnowledgeSessionFactory;

    public void execute(Object fact) {
        StatelessKieSession session = statelessKnowledgeSessionFactory.getStatelessKieSession();
        session.execute(fact);
    }

    public void execute(Iterable facts) {
        StatelessKieSession session = statelessKnowledgeSessionFactory.getStatelessKieSession();
        session.execute(facts);
    }
}
