package com.apd.phoenix.service.brms.core.order;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.apd.phoenix.service.brms.core.knowledge.KnowledgeService;
import com.apd.phoenix.service.business.CredentialBp;
import com.apd.phoenix.service.business.CustomerOrderBp;
import com.apd.phoenix.service.message.api.MessageService;
import com.apd.phoenix.service.model.AccountXCredentialXUser;
import com.apd.phoenix.service.model.CustomerOrder;
import com.apd.phoenix.service.model.LineItem;
import com.apd.phoenix.service.model.LineItemStatus.LineItemStatusEnum;
import com.apd.phoenix.service.model.MessageMetadata.MessageType;
import com.apd.phoenix.service.model.Vendor;
import com.apd.phoenix.service.model.dto.LineItemDto;
import com.apd.phoenix.service.model.dto.PurchaseOrderDto;
import com.apd.phoenix.service.model.dto.VendorDto;
import com.apd.phoenix.service.model.factory.DtoFactory;
import com.apd.phoenix.service.model.factory.DtoFactory.ParsingException;
import com.apd.phoenix.service.order.NoUsscoAccountIdException;
import com.apd.phoenix.service.persistence.jpa.CredentialXVendorDao;
import com.apd.phoenix.service.persistence.jpa.SequenceDao;
import com.apd.phoenix.service.persistence.jpa.SequenceDao.Sequence;
import com.apd.phoenix.service.workflow.WorkflowService;

@Stateless
@LocalBean
public class PlaceOrderService {

    private static String FAKE_VENDOR = "Solr vendor";
    private static String TEST_EDI_VENDOR = WorkflowService.USSCO_VENDOR_NAME;
    private static String TEST_CXML_VENDOR = WorkflowService.STAPLES_VENDOR_NAME;
    private static final int LENGTH_OF_A_STANDARD_ZIP_CODE = 5;

    @Inject
    CustomerOrderBp customerOrderBp;

    @Inject
    CredentialXVendorDao credentialXVendorDao;

    @Inject
    CredentialBp credentialBp;

    @Inject
    SequenceDao sequenceDao;

    @Inject
    MessageService messageService;

    @Inject
    KnowledgeService knowledgeService;

    private static final Logger logger = LoggerFactory.getLogger(PlaceOrderService.class);

    public void sendOrder(CustomerOrder customerOrder){
    	if (!customerOrder.hasAllowedStatus()) {
    		logger.warn("System did not send order " + customerOrder.getApdPo() + " due to disallowed order status");
    		return;
    	}
        Set<LineItem> lineItems = customerOrder.getItems();
        List<Vendor> vendors = new ArrayList<>();
        for (LineItem lineItem : lineItems){
            Vendor vendor = lineItem.getVendor();
            if (!vendors.contains(vendor)){
                vendors.add(vendor);
            }
        }

        for (Vendor vendor : vendors){
                this.sendOrder(vendor, customerOrder);
        }
    }

    public void sendOrder(Vendor vendor, CustomerOrder customerOrder) {
        this.sendOrder(vendor, customerOrder, lineItemDefaultQuantities(customerOrder.getItems()), false, null);
    }

    public void sendRetryOrder(Vendor vendor, CustomerOrder customerOrder, Set<LineItem> failedItems, Integer pass) {

        this.sendOrder(vendor, customerOrder, lineItemDefaultQuantities(failedItems), true, pass);
    }

    private void sendOrder(Vendor vendor, CustomerOrder customerOrder, Map<LineItem, Integer> items, boolean isRetry,
            Integer pass) {
        if (vendor == null) {
            return; //If the vendor is null then we are processing a fee. Fees do not get sent anywhere
        }
        PurchaseOrderDto purchaseOrderDto;
        VendorDto vendorDto;
        try {
            purchaseOrderDto = DtoFactory.createPurchaseOrderDto(customerOrder);
            vendorDto = DtoFactory.createVendorDto(vendor);
        }
        catch (ParsingException e) {
            logger.error("Parsing exception while preparing an order for sending", e);
            return;
        }

        purchaseOrderDto.setVendor(vendorDto);

        AccountXCredentialXUser axcxu = customerOrderBp.getAXCXU(customerOrder);

        String accountId = null;

        if (axcxu == null || axcxu.getUsAccountNumber() == null || !vendor.getPartnerId().equals(WorkflowService.USSCO_VENDOR_NAME)) {
            accountId = credentialXVendorDao.getAccountIdByCredentialAndVendor(vendor, customerOrder.getCredential());
            purchaseOrderDto.setPartnerAccountId(accountId);
        }

        if (vendor.getPartnerId().equals(WorkflowService.USSCO_VENDOR_NAME) && axcxu != null) {
            String axcxuUsscoAccountNumber = customerOrderBp.getUsAccountNumber(customerOrder);
            if (axcxuUsscoAccountNumber != null && !axcxuUsscoAccountNumber.isEmpty()) {
                accountId = axcxuUsscoAccountNumber;
            }

            if (credentialBp.getCredetialProperty("needs usps/ussco workflow", customerOrder.getCredential()) != null) {
                String originalZip = purchaseOrderDto.getShipToAddressDto().getZip();
                //The zip route table only uses the first 5 digits of a zip code
                if (originalZip.length() > LENGTH_OF_A_STANDARD_ZIP_CODE) {
                    purchaseOrderDto.getShipToAddressDto()
                            .setZip(
                                    purchaseOrderDto.getShipToAddressDto().getZip().substring(0,
                                            LENGTH_OF_A_STANDARD_ZIP_CODE));
                }
                //Determine all possible accountids and facility codes using zip route table
                knowledgeService.execute(purchaseOrderDto.getShipToAddressDto());

                if (pass == null || pass == 1) {
                    //Set the correct accountId and facility code on the order
                    knowledgeService.execute(purchaseOrderDto);
                    if (StringUtils.isEmpty(purchaseOrderDto.getPartnerAccountId())) {
                        purchaseOrderDto.setPartnerAccountId(purchaseOrderDto.getShipToAddressDto()
                                .getDefaultUsscoAccountId());
                    }
                }
                else if (pass == 2) {
                    purchaseOrderDto.setPartnerAccountId(purchaseOrderDto.getShipToAddressDto()
                            .getSecondPassUsscoAccountId());
                }
                else if (pass == 3) {
                    purchaseOrderDto.setPartnerAccountId(purchaseOrderDto.getShipToAddressDto()
                            .getThirdPassUsscoAccountId());
                }
                purchaseOrderDto.getShipToAddressDto().setZip(originalZip);

            }
            else {
                purchaseOrderDto.setPartnerAccountId(accountId);
            }

            //Ussco NEEDS an accountId
            if (purchaseOrderDto.getPartnerAccountId() == null || purchaseOrderDto.getPartnerAccountId().isEmpty()) {
                throw new NoUsscoAccountIdException();
            }
        }

        purchaseOrderDto.setItems(new ArrayList<LineItemDto>());
        for (LineItem lineItem : items.keySet()) {
            if (vendor.equals(lineItem.getVendor()) &&
            //Don't send rejected items to vendors
                    (lineItem.getStatus() == null || !LineItemStatusEnum.REJECTED.getValue().equals(
                            lineItem.getStatus().getValue()))) {
                LineItemDto toAdd = DtoFactory.createLineItemDto(lineItem);
                if (items.get(lineItem) != null) {
                    toAdd.setQuantity(new BigInteger(String.valueOf(items.get(lineItem))));
                }
                purchaseOrderDto.getItems().add(toAdd);
            }
        }

        if (isRetry && vendor.getName().equals(WorkflowService.STAPLES_VENDOR_NAME)) {
            purchaseOrderDto.setCustomerPoNumber(calculateRetryPo(purchaseOrderDto.getCustomerPoNumber(), customerOrder
                    .getResendAttempts()));
        }

        MessageType messageType = null;

        if ((StringUtils.isNotBlank(purchaseOrderDto.getCredential().getVendorOrderUrl()) || StringUtils
                .isNotBlank(purchaseOrderDto.getCredential().getVendorPunchoutUrl()))
                && (purchaseOrderDto.getOrderType() == null || !"MANUAL".equalsIgnoreCase(purchaseOrderDto
                        .getOrderType().getValue()))) {
            purchaseOrderDto.setPartnerDestination(purchaseOrderDto.getCredential().getVendorOrderUrl());
            if (StringUtils.isBlank(purchaseOrderDto.getPartnerDestination())) {
                purchaseOrderDto.setPartnerDestination(purchaseOrderDto.getCredential().getVendorPunchoutUrl());
            }
            messageType = MessageType.CXML;
        }
        else {
            purchaseOrderDto.setPartnerDestination(vendor.getCommunicationDestination());
            setPartnerId(purchaseOrderDto, vendor);
            if (vendor.getCommunicationMessageType() == MessageType.EMAIL) {
                purchaseOrderDto.setRecipients(vendor.getCommunicationDestination());
            }
            messageType = vendor.getCommunicationMessageType();
        }

        purchaseOrderDto.setInterchangeId(String.valueOf(sequenceDao.nextVal(Sequence.INTERCHANGE_CONTROL_ID)));
        purchaseOrderDto.setGroupId(String.valueOf(sequenceDao.nextVal(Sequence.GROUP_CONTROL_ID)));
        purchaseOrderDto.setTransactionId(String.valueOf(sequenceDao.nextVal(Sequence.TRANSACTION_CONTROL_ID)));
        messageService.sendPurchaseOrder(purchaseOrderDto, messageType, isRetry);
    }

    private static void setPartnerId(PurchaseOrderDto purchaseOrderDto, Vendor vendor) {
        String vendorPartnerId = vendor.getPartnerId();
        if (vendorPartnerId.equals(FAKE_VENDOR) && vendor.getCommunicationMessageType() == MessageType.EDI) {
            purchaseOrderDto.setPartnerId(TEST_EDI_VENDOR);
            logger.error("Warning: placing an order for '" + purchaseOrderDto.getPartnerId() + "' instead of for '"
                    + FAKE_VENDOR + "'");
        }
        else if (vendorPartnerId.equals(FAKE_VENDOR) && vendor.getCommunicationMessageType() == MessageType.CXML) {
            purchaseOrderDto.setPartnerId(TEST_CXML_VENDOR);
            logger.error("Warning: placing an order for '" + purchaseOrderDto.getPartnerId() + "' instead of for '"
                    + FAKE_VENDOR + "'");
        }
        else {
            logger.info("Sending order for vendor " + vendorPartnerId + " via "
                    + vendor.getCommunicationMessageType().getLabel() + " to " + vendor.getCommunicationDestination());
            //Vendor Name will be lowercase for queue selector
            if (vendor.getCommunicationMessageType() == MessageType.EDI) {
                purchaseOrderDto.setPartnerId(vendorPartnerId.toLowerCase());
            }
            else {
                purchaseOrderDto.setPartnerId(vendorPartnerId);
            }
        }
    }

    /**
     * DO NOT USE THIS METHOD.  It bypasses critical business logic in the sendOrder methods of this class
     * @param vendorName 
     * @param co 
     */
    @Deprecated
    public void sendCXMLOrder(String vendorName, CustomerOrder co) {
        PurchaseOrderDto purchaseOrderDto;
        try {
            purchaseOrderDto = DtoFactory.createPurchaseOrderDto(co);
        }
        catch (ParsingException e) {
            logger.error("Parsing exception while preparing an order for sending", e);
            return;
        }
        logger.info("Sending CXML Order");
        if (StringUtils.isBlank(vendorName)) {
            logger
                    .warn("Vendor name not provided for outbound order.  The CXML translators may generate invalid CXML.  Make sure vendor name is set in the credential.");
        }
        VendorDto vendor = new VendorDto();
        vendor.setName(vendorName);
        purchaseOrderDto.setVendor(vendor);
        purchaseOrderDto.setPartnerDestination(co.getCredential().getVendorOrderUrl());
        if (StringUtils.isBlank(purchaseOrderDto.getPartnerDestination())) {
            purchaseOrderDto.setPartnerDestination(co.getCredential().getVendorPunchoutUrl());
        }
        purchaseOrderDto.setInterchangeId(String.valueOf(sequenceDao.nextVal(Sequence.INTERCHANGE_CONTROL_ID)));
        purchaseOrderDto.setGroupId(String.valueOf(sequenceDao.nextVal(Sequence.GROUP_CONTROL_ID)));
        purchaseOrderDto.setTransactionId(String.valueOf(sequenceDao.nextVal(Sequence.TRANSACTION_CONTROL_ID)));
        messageService.sendPurchaseOrder(purchaseOrderDto, MessageType.CXML);
    }

    public void resendItems(CustomerOrder co, Vendor vendor, Map<LineItem, Integer> list) {
        this.sendOrder(vendor, co, list, true, null);
    }

    private Map<LineItem, Integer> lineItemDefaultQuantities(Set<LineItem> items) {
        Map<LineItem, Integer> toReturn = new HashMap<LineItem, Integer>();
        for (LineItem item : items) {
            toReturn.put(item, item.getQuantity());
        }
        return toReturn;
    }

    private static String calculateRetryPo(String poNumber, Integer retryNumber) {
        if (retryNumber == null || retryNumber.intValue() == 0) {
            return poNumber;
        }
        return poNumber + "-" + retryNumber;
    }
}
