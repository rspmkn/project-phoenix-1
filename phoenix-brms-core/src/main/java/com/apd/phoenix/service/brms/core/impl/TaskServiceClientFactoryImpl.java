/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.apd.phoenix.service.brms.core.impl;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import org.kie.api.task.TaskService;

import com.apd.phoenix.service.brms.core.api.TaskServiceClientFactory;

@Stateless
@LocalBean
public class TaskServiceClientFactoryImpl implements TaskServiceClientFactory {

    @Inject
    TaskServiceServerFactoryImpl taskServiceServerFactory;

    public TaskService getTaskService() {
        return taskServiceServerFactory.createTaskServiceServer();
    }
}