package com.apd.phoenix.service.brms.core.workflow;

import java.util.ArrayList;
import java.util.List;

import org.kie.api.task.model.Status;

public class PhoenixTaskSearchCriteria {
	private String owner;
	private List<Status> taskStatus = new ArrayList<>();
	private List<Status> notTaskStatus = new ArrayList<>();
	private String name;
	private List<String> groups = new ArrayList<>();
	private String apdPo;
	private boolean userRequired = true;
	
	public String getOwner() {
		return owner;
	}
	public void setOwner(String owner) {
		this.owner = owner;
	}
	public List<Status> getTaskStatus() {
		return taskStatus;
	}
	public void setTaskStatus(List<Status> taskStatus) {
		this.taskStatus = taskStatus;
	}
	public List<Status> getNotTaskStatus() {
		return notTaskStatus;
	}
	public void setNotTaskStatus(List<Status> notTaskStatus) {
		this.notTaskStatus = notTaskStatus;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<String> getGroups() {
		return groups;
	}
	public void setGroups(List<String> groups) {
		this.groups = groups;
	}
	public String getApdPo() {
		return apdPo;
	}
	public void setApdPo(String apdPo) {
		this.apdPo = apdPo;
	}
	public void setShowCompleted(boolean showCompleted) {
		if (showCompleted && !this.taskStatus.isEmpty()) {
			this.taskStatus.add(Status.Completed);
		}
		else if (!showCompleted) {
			this.notTaskStatus.add(Status.Completed);
		}
	}
	public boolean isUserRequired() {
		return userRequired;
	}
	public void setUserRequired(boolean userRequired) {
		this.userRequired = userRequired;
	}
}
