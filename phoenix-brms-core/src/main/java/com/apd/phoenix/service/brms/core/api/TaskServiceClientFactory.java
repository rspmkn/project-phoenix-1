package com.apd.phoenix.service.brms.core.api;

import javax.ejb.Stateless;
import org.kie.api.task.TaskService;

@Stateless
public interface TaskServiceClientFactory {

    public TaskService getTaskService();

}
