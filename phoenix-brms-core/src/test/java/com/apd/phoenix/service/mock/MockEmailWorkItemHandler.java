package com.apd.phoenix.service.mock;

import java.util.Map;

import org.kie.api.runtime.process.WorkItem;
import org.kie.api.runtime.process.WorkItemHandler;
import org.kie.api.runtime.process.WorkItemManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MockEmailWorkItemHandler implements WorkItemHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(MockEmailWorkItemHandler.class);

    @Override
    public void abortWorkItem(WorkItem workItem, WorkItemManager workItemManager) {
    }

    @Override
    public void executeWorkItem(WorkItem workItem, WorkItemManager workItemManager) {
        LOGGER.info("Mocking Email Notification");
        for (Map.Entry<String, Object> entry : workItem.getParameters().entrySet()) {
            LOGGER.info("key: " + entry.getKey() + ", value: " + entry.getValue());
        }

        workItemManager.completeWorkItem(workItem.getId(), null);

    }

}
