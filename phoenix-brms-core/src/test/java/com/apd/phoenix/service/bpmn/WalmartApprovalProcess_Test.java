package com.apd.phoenix.service.bpmn;

import java.util.HashMap;

import org.jbpm.test.JbpmJUnitTestCase;
import org.junit.Before;
import org.junit.Test;
import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.process.ProcessInstance;
import org.kie.api.runtime.process.WorkItem;
import org.kie.api.runtime.process.WorkflowProcessInstance;

public class WalmartApprovalProcess_Test extends JbpmJUnitTestCase {

	private TestWorkItemHandler workItemHandler = null;
	private ProcessInstance processInstance = null;
	private KieSession ksession = null;

	private static boolean APPROVED = true;
	private static boolean DENIED = false;

	HashMap<String, Object> params = new HashMap<>();

	@Before
	public void setUp() throws Exception {
		super.setUp();

		ksession = createKnowledgeSession("WalmartOrderApproval.bpmn");

		workItemHandler = new TestWorkItemHandler();

		ksession.getWorkItemManager().registerWorkItemHandler("Human Task", workItemHandler);

		params.put("assignee1", "John Doe");
		params.put("assignee2", "Jane Doe");
		params.put("assignee3", "Tom Doe");
	}

	@Test
	public void allApproved() {
		processInstance = ksession.startProcess("com.apd.phoenix.workflow.WalmartOrderApproval", params);

		assertProcessInstanceActive(processInstance.getId(), ksession);

		// First Approval
		assertNodeTriggered(processInstance.getId(), "First Level Order Approval Task");

		assertEquals(params.get("assignee1"), getVariableValue("assignee1", processInstance.getId(), ksession));

		WorkItem workItem1 = workItemHandler.getWorkItem();

		assertNotNull(workItem1);

		HashMap<String, Object> completeParams = new HashMap<>();
		completeParams.put("outcome", APPROVED);

		ksession.getWorkItemManager().completeWorkItem(workItem1.getId(), completeParams);

		assertNodeTriggered(processInstance.getId(), "Was Order Approved");

		assertEquals(APPROVED, ((WorkflowProcessInstance) processInstance).getVariable("outcome"));

		// Second approval
		assertNodeTriggered(processInstance.getId(), "Second Level Order Approval Task");

		assertEquals(params.get("assignee2"), getVariableValue("assignee2", processInstance.getId(), ksession));

		WorkItem workItem2 = workItemHandler.getWorkItem();
		assertNotNull(workItem2);

		ksession.getWorkItemManager().completeWorkItem(workItem2.getId(), completeParams);

		assertNodeTriggered(processInstance.getId(), "Was Order Approved  - 2");

		assertEquals(APPROVED, ((WorkflowProcessInstance) processInstance).getVariable("outcome"));

		// Third Approval
		assertNodeTriggered(processInstance.getId(), "Third Level Order Approval Task");

		assertEquals(params.get("assignee3"), getVariableValue("assignee3", processInstance.getId(), ksession));

		WorkItem workItem3 = workItemHandler.getWorkItem();
		assertNotNull(workItem3);

		ksession.getWorkItemManager().completeWorkItem(workItem3.getId(), completeParams);

		assertEquals(APPROVED, ((WorkflowProcessInstance) processInstance).getVariable("outcome"));

		assertProcessInstanceCompleted(processInstance.getId(), ksession);

	}

	@Test
	public void firstApproverRejects() {
		processInstance = ksession.startProcess("com.apd.phoenix.workflow.WalmartOrderApproval", params);

		assertProcessInstanceActive(processInstance.getId(), ksession);

		assertNodeTriggered(processInstance.getId(), "First Level Order Approval Task");

		WorkItem workItem1 = workItemHandler.getWorkItem();

		assertNotNull(workItem1);

		HashMap<String, Object> completeParams = new HashMap<>();
		completeParams.put("outcome", DENIED);

		ksession.getWorkItemManager().completeWorkItem(workItem1.getId(), completeParams);

		assertNodeTriggered(processInstance.getId(), "Was Order Approved");

		assertEquals(DENIED, ((WorkflowProcessInstance) processInstance).getVariable("outcome"));

		assertProcessInstanceCompleted(processInstance.getId(), ksession);
	}

	@Test
	public void approveThenDeny() {
		processInstance = ksession.startProcess("com.apd.phoenix.workflow.WalmartOrderApproval", params);

		assertProcessInstanceActive(processInstance.getId(), ksession);

		// Approve
		assertNodeTriggered(processInstance.getId(), "First Level Order Approval Task");

		WorkItem workItem1 = workItemHandler.getWorkItem();

		assertNotNull(workItem1);

		HashMap<String, Object> completeParams = new HashMap<>();
		completeParams.put("outcome", APPROVED);

		ksession.getWorkItemManager().completeWorkItem(workItem1.getId(), completeParams);

		assertNodeTriggered(processInstance.getId(), "Was Order Approved");

		assertEquals(APPROVED, ((WorkflowProcessInstance) processInstance).getVariable("outcome"));
		// Reject
		assertNodeTriggered(processInstance.getId(), "Second Level Order Approval Task");

		WorkItem workItem2 = workItemHandler.getWorkItem();
		assertNotNull(workItem2);

		completeParams.clear();
		completeParams.put("outcome", DENIED);

		ksession.getWorkItemManager().completeWorkItem(workItem2.getId(), completeParams);

		assertNodeTriggered(processInstance.getId(), "Was Order Approved  - 2");

		assertEquals(DENIED, ((WorkflowProcessInstance) processInstance).getVariable("outcome"));

		assertProcessInstanceCompleted(processInstance.getId(), ksession);
	}

	@Test
	public void approveApproveDeny() {
		processInstance = ksession.startProcess("com.apd.phoenix.workflow.WalmartOrderApproval", params);

		assertProcessInstanceActive(processInstance.getId(), ksession);

		// First Approval
		assertNodeTriggered(processInstance.getId(), "First Level Order Approval Task");

		assertEquals(params.get("assignee1"), getVariableValue("assignee1", processInstance.getId(), ksession));

		WorkItem workItem1 = workItemHandler.getWorkItem();

		assertNotNull(workItem1);

		HashMap<String, Object> completeParams = new HashMap<>();
		completeParams.put("outcome", APPROVED);

		ksession.getWorkItemManager().completeWorkItem(workItem1.getId(), completeParams);

		assertNodeTriggered(processInstance.getId(), "Was Order Approved");

		assertEquals(APPROVED, ((WorkflowProcessInstance) processInstance).getVariable("outcome"));

		// Second approval
		assertNodeTriggered(processInstance.getId(), "Second Level Order Approval Task");

		assertEquals(params.get("assignee2"), getVariableValue("assignee2", processInstance.getId(), ksession));

		WorkItem workItem2 = workItemHandler.getWorkItem();
		assertNotNull(workItem2);

		ksession.getWorkItemManager().completeWorkItem(workItem2.getId(), completeParams);

		assertNodeTriggered(processInstance.getId(), "Was Order Approved  - 2");

		assertEquals(APPROVED, ((WorkflowProcessInstance) processInstance).getVariable("outcome"));

		// Reject
		assertNodeTriggered(processInstance.getId(), "Third Level Order Approval Task");

		WorkItem workItem3 = workItemHandler.getWorkItem();
		assertNotNull(workItem3);

		completeParams.clear();
		completeParams.put("outcome", DENIED);

		ksession.getWorkItemManager().completeWorkItem(workItem3.getId(), completeParams);

		assertEquals(DENIED, ((WorkflowProcessInstance) processInstance).getVariable("outcome"));

		assertProcessInstanceCompleted(processInstance.getId(), ksession);
	}
}