package com.apd.phoenix.service.brms.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.DependencyResolvers;
import org.jboss.shrinkwrap.resolver.api.maven.MavenDependencyResolver;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.kie.api.task.TaskService;
import org.kie.api.task.model.Status;
import org.kie.api.task.model.TaskSummary;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.apd.phoenix.service.brms.core.impl.TaskServiceClientFactoryImpl;
import com.apd.phoenix.service.brms.core.workflow.UserTaskService;
import com.apd.phoenix.service.executor.command.api.BRMSCommandCreator;
import com.apd.phoenix.service.executor.command.api.CompleteTaskCommand;
import com.apd.phoenix.service.executor.command.api.StartProcessCommand;

@RunWith(Arquillian.class)
public class ShippingManifestIT {

    private static final Logger LOGGER = LoggerFactory.getLogger(ShippingManifestIT.class);

    @Inject
    BRMSCommandCreator commandCreator;

    @Inject
    UserTaskService taskService;

    @Inject
    TaskServiceClientFactoryImpl taskClientFactory;

    @Deployment
    public static WebArchive createDeployment() {
        // Create a resolver that will get dependencies from the pom for use
        // it the tests.
        MavenDependencyResolver resolver = DependencyResolvers.use(MavenDependencyResolver.class).loadMetadataFromPom(
                "pom.xml");

        // Build and return a dummy deployment of the app for testing.
        WebArchive test = ShrinkWrap.create(WebArchive.class, "executor-test.war").addPackages(true,
                "com.apd.phoenix.service.executor", "com.apd.phoenix.service.brms.impl").addClasses()
                .addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml").addAsLibraries(
                        resolver.artifacts("org.jbpm:jbpm-human-task:5.3.1.BRMS").resolveAsFiles());

        // LOGGER.info(test.toString(true));
        return test;

    }

    @Before
    public void setUp() throws Exception {

    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void multipleVendors() throws InterruptedException {
    	Long orderId = new Long(1870);

    	
        
        Map<String, Object> processParams = new HashMap<String, Object>();
        List<String> vendors = new ArrayList<>();
    	List<String> vendorsInError = new ArrayList<>();
    	vendors.add("APD");
    	vendors.add("USSCO");
    	processParams.put("vendors", vendors);
    	processParams.put("retry", false);
    	processParams.put("vendorsInError", vendorsInError);
    	commandCreator.sendStartProcess(StartProcessCommand.Name.VENDOR_SHIPMENT_MANIFEST_RETRIEVAL, StartProcessCommand.Type.ORDER,
                orderId, processParams, null, StartProcessCommand.Callback.HELLOWORLD);
    	
    	Thread.sleep(20000);

        TaskService client = taskClientFactory.getTaskService();

        List<TaskSummary> tasks = client.getTasksAssignedAsPotentialOwner("admin", "en-UK");

        LOGGER.info("---------# of tasks available to admin=" + tasks.size());
        for (TaskSummary taskSummary : tasks) {
            if (taskSummary.getStatus().equals(Status.Ready)) {
                LOGGER.info("-----------claiming task=" + taskSummary.getId());
                client.claim(taskSummary.getId(), "admin");

                LOGGER.info("-----------starting task=" + taskSummary.getId());
                client.start(taskSummary.getId(), "admin");

                LOGGER.info("-----------completing task=" + taskSummary.getId());
                Map<String, Object> responseData = new HashMap<>();
                responseData.put("retry", true);
                //Attempt retry again
                commandCreator.completeTask(new Long(taskSummary.getId()), "admin", responseData,
                        CompleteTaskCommand.Type.ORDER, orderId);
                
            }
        }
        
        Thread.sleep(20000);
        
        tasks = client.getTasksAssignedAsPotentialOwner("admin", "en-UK");

        
        LOGGER.info("---------# of tasks available to admin=" + tasks.size());
        for (TaskSummary taskSummary : tasks) {
            if (taskSummary.getStatus().equals(Status.Ready)) {
                LOGGER.info("-----------claiming task=" + taskSummary.getId());
                client.claim(taskSummary.getId(), "admin");

                LOGGER.info("-----------starting task=" + taskSummary.getId());
                client.start(taskSummary.getId(), "admin");

                LOGGER.info("-----------completing task=" + taskSummary.getId());
                Map<String, Object> responseData = new HashMap<>();
                responseData.put("retry", false);
                //Do not attempt retry
                commandCreator.completeTask(new Long(taskSummary.getId()), "admin", responseData,
                        CompleteTaskCommand.Type.ORDER, orderId);
 
            }
        }
        Thread.sleep(2000);
        
       
        
    }

//    private ContentData mapToContentData(Map<String, Object> params) {
//        ContentData toReturn = new ContentData();
//        toReturn.setAccessType(AccessType.Inline);
//        toReturn.setContent(getByteArrayFromObject(params));
//        return toReturn;
//    }

//    private byte[] getByteArrayFromObject(Object obj) {
//        byte[] result = null;
//
//        try {
//            ByteArrayOutputStream baos = new ByteArrayOutputStream();
//            ObjectOutputStream oos = new ObjectOutputStream(baos);
//            oos.writeObject(obj);
//            oos.flush();
//            oos.close();
//            baos.close();
//            result = baos.toByteArray();
//        }
//        catch (IOException ioEx) {
//            LOGGER.error(ioEx.getMessage());
//        }
//        return result;
//    }
}
