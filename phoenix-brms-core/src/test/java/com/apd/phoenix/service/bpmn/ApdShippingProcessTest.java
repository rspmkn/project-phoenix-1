package com.apd.phoenix.service.bpmn;

import java.util.HashMap;
import java.util.Map;

import org.jbpm.test.JbpmJUnitTestCase;
import org.jbpm.workflow.instance.WorkflowProcessInstance;
import org.junit.Test;
import org.kie.api.KieBase;
import org.kie.api.definition.type.FactType;
import org.kie.api.io.ResourceType;
import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.process.ProcessInstance;
import org.kie.internal.KnowledgeBase;
import org.kie.internal.logger.KnowledgeRuntimeLogger;
import org.kie.internal.logger.KnowledgeRuntimeLoggerFactory;
import org.kie.internal.runtime.StatefulKnowledgeSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.apd.phoenix.service.model.Account;
import com.apd.phoenix.service.model.Vendor;

/**
 * This is a file to test a process.
 */
public class ApdShippingProcessTest extends JbpmJUnitTestCase {

    private static final Logger LOGGER = LoggerFactory.getLogger(ApdShippingProcessTest.class);

    @Test
    public void testUsscoPackageShippingProcess() {
        KieSession ksession = createKnowledgeSession("usscoPackageShipping.bpmn"); //knowledge session
        //TODO MULTI-143
        KnowledgeRuntimeLogger logger = KnowledgeRuntimeLoggerFactory.newConsoleLogger(null);
        ProcessInstance processInstance = ksession.startProcess("com.sample.bpmn.usscoValidation", null);
        assertProcessInstanceActive(processInstance.getId(), ksession);
        logger.close();
    }

    @Test
    public void testApdShippingProcess_ValidateOrder() {
        Map<String, ResourceType> resources = new HashMap<String, ResourceType>();
        resources.put("apdShipping.bpmn", ResourceType.BPMN2);
        resources.put("ShippingValidation.drl", ResourceType.DRL);
        KieBase kbase = null;
        try {
            kbase = createKnowledgeBase(resources);
        }
        catch (Exception e) {
            // TODO Auto-generated catch block
            LOGGER.error("An error occured:", e);
        }
        KieSession ksession = createKnowledgeSession(); //knowledge session
        KnowledgeRuntimeLogger logger = KnowledgeRuntimeLoggerFactory.newConsoleLogger(null);

        FactType customerOrderType = kbase.getFactType("Phoenix", "Order");
        Object apdOrder = null;
        try {
            apdOrder = customerOrderType.newInstance();
        }
        catch (InstantiationException e) {
            LOGGER.error("Cannot initiate the class", e);
        }
        catch (IllegalAccessException e) {
            LOGGER.error("Cannot access the class on the package", e);
        }
        createFacts(apdOrder, customerOrderType);
        ksession.insert(apdOrder);

        WorkflowProcessInstance processInstance = (WorkflowProcessInstance) ksession.startProcess(
                "com.sample.bpmn.apdShipping", null);
        assertProcessInstanceActive(processInstance.getId(), ksession);

        assertNodeTriggered(processInstance.getId(), "PopulateContext");

        //NOTE call fireAllRules or fireAllRules with number of rules to be fired
        ksession.fireAllRules(1);
        assertNodeTriggered(processInstance.getId(), "LookupZipCode");
        //NOTE Other API calls may be tested

        ksession.fireAllRules(1);
        assertNodeTriggered(processInstance.getId(), "CheckNSN");
        //NOTE Other API calls may be tested

        ksession.fireAllRules(1);
        assertNodeTriggered(processInstance.getId(), "CheckHazmat");
        //NOTE Other API calls may be tested

        ksession.fireAllRules(1);
        assertNodeTriggered(processInstance.getId(), "CheckOverWeightSize");
        //NOTE Other API calls may be tested

        ksession.fireAllRules(1);
        assertNodeTriggered(processInstance.getId(), "CheckOrderWeight");
        //NOTE Other API calls may be tested

        ksession.fireAllRules(1);
        assertNodeTriggered(processInstance.getId(), "LookupFacilityCode");
        //NOTE Other API calls may be tested

        ksession.fireAllRules(1);
        assertNodeTriggered(processInstance.getId(), "SendOrder");
        //NOTE Other API calls may be tested

        logger.close();
        ksession.dispose();
    }

    @Test
    public void testApdShippingProcess_LookupZipCode() {
        Map<String, ResourceType> resources = new HashMap<String, ResourceType>();
        resources.put("apdShipping.bpmn", ResourceType.BPMN2);
        resources.put("OrderValidation.drl", ResourceType.DRL);
        KieBase kbase = null;
        try {
            kbase = createKnowledgeBase(resources);
        }
        catch (Exception e) {
            // TODO Auto-generated catch block
            LOGGER.error("An error occured:", e);
        }
        KieSession ksession = createKnowledgeSession(); //knowledge session

        FactType customerOrderType = kbase.getFactType("Phoenix", "Order");
        Object apdOrder = null;
        try {
            apdOrder = customerOrderType.newInstance();
        }
        catch (InstantiationException e) {
            LOGGER.error("Cannot initiate the class", e);
        }
        catch (IllegalAccessException e) {
            LOGGER.error("Cannot access the class on the package", e);
        }
        createFacts(apdOrder, customerOrderType);
        ksession.insert(apdOrder);

    }

    private void createFacts(Object apdOrder, FactType factType) {
        Vendor vendor = new Vendor();
        vendor.setName("USSCO");
        Account rootAccount = new Account();
        rootAccount.setName("USPS");
        Account account = new Account();
        account.setRootAccount(rootAccount);
        factType.set(apdOrder, "name", "apd_mockup_order");
        factType.set(apdOrder, "vendor", vendor);
        factType.set(apdOrder, "customer", account);
    }
}