package com.apd.phoenix.bpmn.manifest;

import java.util.HashMap;
import java.util.Map;

import org.jbpm.workflow.instance.WorkflowProcessInstance;
import org.kie.api.event.process.DefaultProcessEventListener;
import org.kie.api.event.process.ProcessStartedEvent;
import org.kie.api.event.rule.DefaultAgendaEventListener;
import org.kie.api.io.Resource;
import org.kie.api.io.ResourceType;
import org.kie.internal.KnowledgeBase;
import org.kie.internal.KnowledgeBaseFactory;
import org.kie.internal.builder.KnowledgeBuilder;
import org.kie.internal.builder.KnowledgeBuilderError;
import org.kie.internal.builder.KnowledgeBuilderErrors;
import org.kie.internal.builder.KnowledgeBuilderFactory;
import org.kie.internal.io.ResourceFactory;
import org.kie.internal.logger.KnowledgeRuntimeLoggerFactory;
import org.kie.internal.runtime.StatefulKnowledgeSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.apd.phoenix.service.mock.MockCamelServiceWorkItemHandler;

public class ManifestMain {

    public static StatefulKnowledgeSession session;
    private static final Logger logger = LoggerFactory.getLogger(ManifestMain.class);

    /**
     * @param args
     * @throws InterruptedException 
     */
    public static void main(String[] args) throws InterruptedException {

        KnowledgeBuilder kbuilder = KnowledgeBuilderFactory.newKnowledgeBuilder();

        for (Map.Entry<Resource, ResourceType> entry : getResources().entrySet()) {
            kbuilder.add(entry.getKey(), entry.getValue());
        }

        if (kbuilder.hasErrors()) {
            KnowledgeBuilderErrors errors = kbuilder.getErrors();

            for (KnowledgeBuilderError error : errors) {
                logger.info(">>> Error:" + error.getMessage());

            }
            throw new IllegalStateException(">>> Knowledge couldn't be parsed! ");
        }

        KnowledgeBase kbase = KnowledgeBaseFactory.newKnowledgeBase();

        kbase.addKnowledgePackages(kbuilder.getKnowledgePackages());

        session = kbase.newStatefulKnowledgeSession();

        KnowledgeRuntimeLoggerFactory.newConsoleLogger(session);

        session.addEventListener(new DefaultAgendaEventListener() {

            @Override
            public void afterRuleFlowGroupActivated(org.kie.api.event.rule.RuleFlowGroupActivatedEvent event) {
                session.fireAllRules();
            }

        });

        session.addEventListener(new DefaultProcessEventListener() {

            @Override
            public void beforeProcessStarted(ProcessStartedEvent event) {
                session.insert(event.getProcessInstance());
            }

        });

        session.getWorkItemManager().registerWorkItemHandler("Camel Service", new MockCamelServiceWorkItemHandler());

        session.getWorkItemManager().registerWorkItemHandler("Notification Service",
                new MockCamelServiceWorkItemHandler());

        //        session.getWorkItemManager().registerWorkItemHandler("Timer Service", new TimerServiceWorkItemHandler());

        session.getWorkItemManager().registerWorkItemHandler("S3 Service", new MockS3StorageWorkItemHandler(6));

        WorkflowProcessInstance processInstance = (WorkflowProcessInstance) session
                .startProcess("phoenix.signal-event-test");
        //assertTrue(processInstance.getState() == ProcessInstance.STATE_ACTIVE);
        //session = restoreSession(session, true);
        // now signal process instance
        Thread.sleep(2000);
        session.signalEvent("MyEvent", "SomeValue", processInstance.getId());
        session.signalEvent("MyEvent", "SomeValue", processInstance.getId());
        Thread.sleep(2000);

    }

    public static Map<Resource, ResourceType> getResources() {
        Map<Resource, ResourceType> resources = new HashMap<Resource, ResourceType>();
        resources.put(ResourceFactory.newClassPathResource("brms/order/change-set.xml"), ResourceType.CHANGE_SET);

        return resources;
    }

}
