package com.apd.phoenix.bpmn.order;

import java.util.Map;

import org.kie.api.runtime.process.WorkItem;
import org.kie.api.runtime.process.WorkItemHandler;
import org.kie.api.runtime.process.WorkItemManager;

public class MockWorkItemHandler implements WorkItemHandler {

    private long workItemId;
    private WorkItemManager manager;

    @Override
    public void abortWorkItem(WorkItem arg0, WorkItemManager arg1) {
        // TODO Auto-generated method stub

    }

    @Override
    public void executeWorkItem(WorkItem workItem, WorkItemManager manager) {
        workItemId = workItem.getId();
        this.manager = manager;
    }

    public void completeWorkItem(Map<String, Object> results) {
        this.manager.completeWorkItem(workItemId, results);
    }

}
