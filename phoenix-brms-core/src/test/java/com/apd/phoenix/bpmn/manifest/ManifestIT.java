package com.apd.phoenix.bpmn.manifest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import junit.framework.Assert;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.DependencyResolvers;
import org.jboss.shrinkwrap.resolver.api.maven.MavenDependencyResolver;
import org.jbpm.workflow.instance.WorkflowProcessInstance;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.kie.api.event.process.DefaultProcessEventListener;
import org.kie.api.event.process.ProcessStartedEvent;
import org.kie.api.event.rule.DefaultAgendaEventListener;
import org.kie.api.io.Resource;
import org.kie.api.io.ResourceType;
import org.kie.internal.KnowledgeBase;
import org.kie.internal.KnowledgeBaseFactory;
import org.kie.internal.builder.KnowledgeBuilder;
import org.kie.internal.builder.KnowledgeBuilderError;
import org.kie.internal.builder.KnowledgeBuilderErrors;
import org.kie.internal.builder.KnowledgeBuilderFactory;
import org.kie.internal.io.ResourceFactory;
import org.kie.internal.logger.KnowledgeRuntimeLoggerFactory;
import org.kie.internal.runtime.StatefulKnowledgeSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.apd.phoenix.service.manifest.Manifest;
import com.apd.phoenix.service.manifest.S3StorageResponse;
import com.apd.phoenix.service.manifest.VendorManifestsResponse;
import com.apd.phoenix.service.mock.MockCamelServiceWorkItemHandler;

@RunWith(Arquillian.class)
public class ManifestIT {

    public static StatefulKnowledgeSession session;
    private static final Logger LOGGER = LoggerFactory.getLogger(ManifestIT.class);

    @Deployment
    public static WebArchive createDeployment() {
        // Create a resolver that will get dependencies from the pom for use
        // it the tests.
        MavenDependencyResolver resolver = DependencyResolvers.use(MavenDependencyResolver.class).loadMetadataFromPom(
                "pom.xml");

        // Build and return a dummy deployment of the app for testing.
        return ShrinkWrap.create(WebArchive.class, "test.war").addClasses(S3StorageResponse.class,
                VendorManifestsResponse.class, Manifest.class, MockFTPWorkItemHandler.class,
                MockS3StorageWorkItemHandler.class).addPackages(true, "com.apd.phoenix.service.camel")
                .addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml").addAsResource(
                        "brms/shipment-manifest/change-set.xml", "brms/shipment-manifest/change-set.xml")
                .addAsResource("brms/shipment-manifest/manifest-aggregation.bpmn",
                        "brms/shipment-manifest/manifest-aggregation.bpmn").addAsResource(
                        "brms/shipment-manifest/vendor-shipment-manifest.bpmn",
                        "brms/shipment-manifest/vendor-shipment-manifest.bpmn").addAsResource(
                        "brms/shipment-manifest/vendor-manifest-validation.dsl",
                        "brms/shipment-manifest/vendor-manifest-validation.dsl").addAsResource(
                        "brms/shipment-manifest/vendor-manifest-validation.dslr",
                        "brms/shipment-manifest/vendor-manifest-validation.dslr").addAsLibraries(
                        resolver.artifacts("org.hornetq:hornetq-core:2.2.11.Final", "org.jbpm:jbpm-bam:5.3.1.BRMS",
                                "org.jbpm:jbpm-human-task:5.3.1.BRMS", "org.jbpm:jbpm-bpmn2:5.3.1.BRMS",
                                "org.jbpm:jbpm-flow-builder:5.3.1.BRMS", "org.jbpm:jbpm-flow:5.3.1.BRMS",
                                "org.drools:knowledge-api:5.3.1.BRMS", "org.drools:drools-core:5.3.1.BRMS",
                                "org.drools:drools-compiler:5.3.1.BRMS",
                                "org.drools:drools-persistence-jpa:5.3.1.BRMS", "org.drools:drools-spring:5.3.1.BRMS",
                                "org.apache.camel:camel-core:2.11.0", "org.apache.camel:camel-stream:2.11.0")
                                .resolveAsFiles());

    }

    @Test
    public void testIsDeployed() {
        KnowledgeBuilder kbuilder = KnowledgeBuilderFactory.newKnowledgeBuilder();

        for (Map.Entry<Resource, ResourceType> entry : getResources().entrySet()) {
            kbuilder.add(entry.getKey(), entry.getValue());
        }

        if (kbuilder.hasErrors()) {
            KnowledgeBuilderErrors errors = kbuilder.getErrors();

            for (KnowledgeBuilderError error : errors) {
                LOGGER.info(">>> Error:" + error.getMessage());

            }
            throw new IllegalStateException(">>> Knowledge couldn't be parsed! ");
        }

        KnowledgeBase kbase = KnowledgeBaseFactory.newKnowledgeBase();

        kbase.addKnowledgePackages(kbuilder.getKnowledgePackages());

        session = kbase.newStatefulKnowledgeSession();

        KnowledgeRuntimeLoggerFactory.newConsoleLogger(session);

        session.addEventListener(new DefaultAgendaEventListener() {

            @Override
            public void afterRuleFlowGroupActivated(org.kie.api.event.rule.RuleFlowGroupActivatedEvent event) {
                LOGGER.info("Fire all rules");
                session.fireAllRules();
            }

        });

        session.addEventListener(new DefaultProcessEventListener() {

            @Override
            public void beforeProcessStarted(ProcessStartedEvent event) {
                session.insert(event.getProcessInstance());
            }

        });

        session.getWorkItemManager().registerWorkItemHandler("Camel Service", new MockCamelServiceWorkItemHandler());

        session.getWorkItemManager().registerWorkItemHandler("Notification Service", new MockCamelServiceWorkItemHandler());

//        session.getWorkItemManager().registerWorkItemHandler("Timer Service", new TimerServiceWorkItemHandler());
        
        session.getWorkItemManager().registerWorkItemHandler("S3 Service", new MockS3StorageWorkItemHandler(6));
        
        VendorManifestsResponse vmr = new VendorManifestsResponse();
        Manifest manifest1 = new Manifest();
        manifest1.setValid(false);
        manifest1.setContent("Hello world");
        List<Manifest> manifests = new ArrayList<>();
        manifests.add(manifest1);
        vmr.setManifests(manifests);
        session.getWorkItemManager().registerWorkItemHandler("FTP Service", new MockFTPWorkItemHandler(vmr));

        Map<String, Object> inputVariables = new HashMap<String, Object>();
        inputVariables.put("count", 0);
        inputVariables.put("vendor1", "APD");
        inputVariables.put("vendor2", "USSCO");
        //Default settings
        inputVariables.put("delay", 1000);
        WorkflowProcessInstance processInstance = (WorkflowProcessInstance) session.startProcess(
                "manifest-aggregation", inputVariables);

        try {
            Thread.sleep(20000);
        }
        catch (InterruptedException e) {
            LOGGER.error("An error occured:", e);
        }

        Assert.assertEquals(WorkflowProcessInstance.STATE_COMPLETED, processInstance.getState());

    }

    public static Map<Resource, ResourceType> getResources() {
        Map<Resource, ResourceType> resources = new HashMap<Resource, ResourceType>();
        resources.put(ResourceFactory.newClassPathResource("brms/shipment-manifest/change-set.xml"),
                ResourceType.CHANGE_SET);

        return resources;
    }
}
