package com.apd.phoenix.bpmn.order;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.kie.api.io.Resource;
import org.kie.api.io.ResourceType;
import org.kie.api.runtime.process.ProcessInstance;
import org.kie.api.task.TaskService;
import org.kie.api.task.model.TaskSummary;
import org.kie.internal.io.ResourceFactory;
import org.kie.internal.task.api.model.AccessType;
import org.kie.internal.task.api.model.ContentData;

import com.apd.phoenix.bpmn.BrmsBaseTest;
import com.apd.phoenix.service.model.Address;
import com.apd.phoenix.service.model.CustomerOrder;
import com.apd.phoenix.service.model.Shipment;

public class OrderRejectedShipToTest extends BrmsBaseTest {

    private TaskService taskService;

    private Address badAddress, goodAddress;

    private String actorId = "admin";

    public OrderRejectedShipToTest() {
        super(true);
    }

    @Override
	protected Map<Resource, ResourceType> getResources() {
		Map<Resource, ResourceType> resources = new HashMap<>();
		resources.put( ResourceFactory.newClassPathResource("brms/order-rejected-shipto/change-set.xml"), ResourceType.CHANGE_SET);
		return resources;
	}

    @Before
    public void setUp() throws Exception {
        super.setUp();

        this.taskService = this.getTaskService(this.session);

        //        this.session.getWorkItemManager().registerWorkItemHandler("Request ShipTo Service",
        //                new RequestShipToServiceWorkItemHandler());

        this.badAddress = new Address();
        this.badAddress.setCity("In a");
        this.badAddress.setState("Galaxy");
        this.badAddress.setLine1("Far far away");
        this.badAddress.setZip("-1");

        this.goodAddress = new Address();
        this.goodAddress.setCity("Charlotte");
        this.goodAddress.setState("NC");
        this.goodAddress.setLine1("555 Arrow Dr.");
        this.goodAddress.setZip("55555");
    }

    @Test
    public void usingExistingAddress() {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("order", this.createCustomerOrder(true));
        params.put("newAddress", goodAddress);

        ProcessInstance processInstance = session.startProcess("order-rejected-shipto", params);

        this.assertProcessInstanceActive(processInstance.getId(), session);
        
        List<TaskSummary> taskList = taskService.getTasksAssignedAsPotentialOwner(actorId, "en-UK");
        Assert.assertEquals(taskList.size(), 1);
        
        TaskSummary taskSummary = taskList.get(0);
        
        taskService.claim(taskSummary.getId(), actorId);
        taskService.start(taskSummary.getId(), actorId);
        
        boolean needNewAddress = false;
        
        Map<String, Object> completeParams = new HashMap<>();
        completeParams.put("need", needNewAddress);
//        ContentData content = new ContentData();
//        content.setAccessType(AccessType.Inline);
//        content.setContent(getByteArrayFromObject(completeParams));

        taskService.complete(taskSummary.getId(), actorId, completeParams);
        
        this.assertProcessInstanceCompleted(processInstance.getId(), session);
    }

    @Test
    public void addNewAddress(){
    	

        Map<String, Object> params = new HashMap<String, Object>();
        params.put("order", this.createCustomerOrder(false));
        params.put("newAddress", goodAddress);

        ProcessInstance processInstance = session.startProcess("order-rejected-shipto", params);

        this.assertProcessInstanceActive(processInstance.getId(), session);
        
        List<TaskSummary> taskList = taskService.getTasksAssignedAsPotentialOwner(actorId, "en-UK");
        Assert.assertEquals(taskList.size(), 1);       
        TaskSummary taskSummary = taskList.get(0);       
        taskService.claim(taskSummary.getId(), actorId);
        taskService.start(taskSummary.getId(), actorId);       
        boolean needNewAddress = true;      
        Map<String, Object> completeParams = new HashMap<>();
        completeParams.put("need", needNewAddress);
//        ContentData content = new ContentData();
//        content.setAccessType(AccessType.Inline);
//        content.setContent(getByteArrayFromObject(completeParams));
        taskService.complete(taskSummary.getId(), actorId, completeParams);       
        
        this.assertNodeTriggered(processInstance.getId(), "Request ShipTo Service");      
        
        taskList = taskService.getTasksAssignedAsPotentialOwner(actorId, "en-UK");
        Assert.assertEquals(taskList.size(), 1);
        taskSummary = taskList.get(0);    
        taskService.claim(taskSummary.getId(), actorId);
        taskService.start(taskSummary.getId(), actorId);
        //This is where the acutal implementation of the taskservice UI will call 
        //out to a BP and persist the approval of the new shipto
        taskService.complete(taskSummary.getId(), actorId, null);
        
        this.assertProcessInstanceCompleted(processInstance.getId(), session);
    }

    private CustomerOrder createCustomerOrder(boolean hasGoodAddresses) {
        int numberOfShipments = 2;
        CustomerOrder customerOrder = new CustomerOrder();
        Set<Shipment> shipments = new HashSet<Shipment>();
        Shipment shipment;
        for (int i = 0; i < numberOfShipments; i++) {
            shipment = new Shipment();
            shipment.setShipFromAddress((hasGoodAddresses ? this.goodAddress : this.badAddress));
            shipments.add(shipment);
        }
        customerOrder.setShipments(shipments);
        return customerOrder;
    }

    public static byte[] getByteArrayFromObject(Object obj) {
        byte[] result = null;

        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ObjectOutputStream oos = new ObjectOutputStream(baos);
            oos.writeObject(obj);
            oos.flush();
            oos.close();
            baos.close();
            result = baos.toByteArray();
        }
        catch (IOException ioEx) {
        }
        return result;
    }

}
