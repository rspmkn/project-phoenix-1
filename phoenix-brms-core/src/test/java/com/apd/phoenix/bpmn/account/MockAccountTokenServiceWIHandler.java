package com.apd.phoenix.bpmn.account;

import java.util.HashMap;
import java.util.Map;

import org.kie.api.runtime.process.WorkItem;
import org.kie.api.runtime.process.WorkItemHandler;
import org.kie.api.runtime.process.WorkItemManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.apd.phoenix.service.user.AccountUpdateApprovalService;

public class MockAccountTokenServiceWIHandler implements WorkItemHandler {

    AccountUpdateApprovalService updateService;

    private static final Logger LOGGER = LoggerFactory.getLogger(MockAccountTokenServiceWIHandler.class);

    public MockAccountTokenServiceWIHandler(AccountUpdateApprovalService updateService) {
        this.updateService = updateService;
    }

    @Override
    public void abortWorkItem(WorkItem workItem, WorkItemManager manager) {
        throw new UnsupportedOperationException("This method has not been implemented");
    }

    @Override
    public void executeWorkItem(WorkItem workItem, WorkItemManager manager) {
        String token = updateService.generateToken(workItem.getProcessInstanceId());
        Map<String, Object> outputVariables = new HashMap<>();
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Generated Token: " + token);
        }
        outputVariables.put("token", token);
        manager.completeWorkItem(workItem.getId(), outputVariables);
    }
}
