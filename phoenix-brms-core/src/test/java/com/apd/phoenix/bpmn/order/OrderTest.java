package com.apd.phoenix.bpmn.order;

import java.util.HashMap;
import java.util.Map;

import junit.framework.Assert;

import org.jbpm.workflow.instance.WorkflowProcessInstance;
import org.junit.Before;
import org.junit.Test;
import org.kie.api.io.Resource;
import org.kie.api.io.ResourceType;
import org.kie.api.runtime.process.ProcessInstance;
import org.kie.internal.io.ResourceFactory;

import com.apd.phoenix.bpmn.BrmsBaseTest;

public class OrderTest extends BrmsBaseTest {

    @Override
	protected Map<Resource, ResourceType> getResources() {
		Map<Resource, ResourceType> resources = new HashMap<>();
		resources.put( ResourceFactory.newClassPathResource("brms/order/change-set.xml"), ResourceType.CHANGE_SET);
		return resources;
	}

    @Before
    public void setup() {
    	//TODO MULTI-143
//        SyncWSHumanTaskHandler humanTaskHandler = new SyncWSHumanTaskHandler();
//        session.getWorkItemManager().registerWorkItemHandler("Human Task", humanTaskHandler);
    }

    @Test
	public void verifyOrderValidationTriggered() throws InterruptedException {
		Map<String, Object> inputVariables = new HashMap<>();

        inputVariables.put("isManual", false);
        inputVariables.put("isOrderComplete", false);
		
		WorkflowProcessInstance processInstance = (WorkflowProcessInstance) session.startProcess("phoenix.standard-order", inputVariables);
		
		//State still active since it is an adhoc process
		Assert.assertTrue(processInstance.getState() == ProcessInstance.STATE_ACTIVE);
		
		assertNodeTriggered(processInstance.getId(), "Validate Order","Criteria Based Order Approval","Cost Center Order Approval","Vendor Communication");
		
	}

    @Test
    public void verifyManualOrderTriggered() {
    	Map<String, Object> inputVariables = new HashMap<>();

        inputVariables.put("isManual", true);
        inputVariables.put("isOrderComplete", false);
		
		WorkflowProcessInstance processInstance = (WorkflowProcessInstance) session.startProcess("phoenix.standard-order", inputVariables);
		
		//State still active since it is an adhoc process
		Assert.assertTrue(processInstance.getState() == ProcessInstance.STATE_ACTIVE);
		assertNodeNotTriggered(processInstance.getId(), "Validate Order","Criteria Based Order Approval","Cost Center Order Approval");
		assertNodeTriggered(processInstance.getId(), "Vendor Communication");
    }

    @Test
    public void completedOrder() throws InterruptedException {
    	Map<String, Object> inputVariables = new HashMap<>();

        inputVariables.put("isManual", false);
        inputVariables.put("isOrderComplete", true);
		
		WorkflowProcessInstance processInstance = (WorkflowProcessInstance) session.startProcess("phoenix.standard-order", inputVariables);
		
		//State still active since it is an adhoc process
		Assert.assertTrue(processInstance.getState() == ProcessInstance.STATE_ACTIVE);
		session.signalEvent("edi810", null);
		Thread.sleep(1000);
		Assert.assertTrue(processInstance.getState() == ProcessInstance.STATE_COMPLETED);
    }

    @Test
    public void edi855Triggered() throws InterruptedException {
    	Map<String, Object> inputVariables = new HashMap<>();

        inputVariables.put("isManual", false);
        inputVariables.put("isOrderComplete", false);
		
		WorkflowProcessInstance processInstance = (WorkflowProcessInstance) session.startProcess("phoenix.standard-order", inputVariables);
		
		//State still active since it is an adhoc process
		Assert.assertTrue(processInstance.getState() == ProcessInstance.STATE_ACTIVE);
		session.signalEvent("edi855", null);
		Thread.sleep(1000);
		assertNodeTriggered(processInstance.getId(), "Receive Vendor Acknowledgment");
    }

    @Test
    public void edi856Triggered() throws InterruptedException {
    	Map<String, Object> inputVariables = new HashMap<>();

        inputVariables.put("isManual", false);
        inputVariables.put("isOrderComplete", false);
		
		WorkflowProcessInstance processInstance = (WorkflowProcessInstance) session.startProcess("phoenix.standard-order", inputVariables);
		
		//State still active since it is an adhoc process
		Assert.assertTrue(processInstance.getState() == ProcessInstance.STATE_ACTIVE);
		session.signalEvent("edi856", null);
		Thread.sleep(1000);
		assertNodeTriggered(processInstance.getId(), "Vendor Ships Items","Invoice Customer");
    }

    @Test
    public void edi810Triggered() throws InterruptedException {
    	Map<String, Object> inputVariables = new HashMap<>();

        inputVariables.put("isManual", false);
        inputVariables.put("isOrderComplete", false);
		
		WorkflowProcessInstance processInstance = (WorkflowProcessInstance) session.startProcess("phoenix.standard-order", inputVariables);
		
		//State still active since it is an adhoc process
		Assert.assertTrue(processInstance.getState() == ProcessInstance.STATE_ACTIVE);
		session.signalEvent("edi810", null);
		Thread.sleep(1000);
		assertNodeTriggered(processInstance.getId(), "Vendor Invoices");
    }
}
