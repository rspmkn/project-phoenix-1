package com.apd.phoenix.bpmn.order;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import junit.framework.Assert;

import org.jbpm.workflow.instance.WorkflowProcessInstance;
import org.junit.Before;
import org.kie.api.io.Resource;
import org.kie.api.io.ResourceType;
import org.kie.api.task.TaskService;
import org.kie.api.task.model.TaskSummary;
import org.kie.internal.io.ResourceFactory;

import com.apd.phoenix.bpmn.BrmsBaseTest;
import com.apd.phoenix.service.model.CustomerOrder;
import com.apd.phoenix.service.model.LineItem;

public class LineItemRejectedTest extends BrmsBaseTest {

    private TaskService taskService;

    public LineItemRejectedTest() {
        super(true);
    }

    @Override
	protected Map<Resource, ResourceType> getResources() {
		Map<Resource, ResourceType> resources = new HashMap<>();
		resources.put( ResourceFactory.newClassPathResource("brms/line-item-rejected/change-set.xml"), ResourceType.CHANGE_SET);
		return resources;
	}

    @Before
    public void setUp() throws Exception {
        super.setUp();
        taskService = this.getTaskService(this.session);
    }

    public void cannotEditOrder() {
    	String actorId = "admin";
    	Map<String, Object> processParams = new HashMap<String, Object>();
        processParams.put("order", new CustomerOrder());
        processParams.put("rejectedItem", new LineItem());
        WorkflowProcessInstance processInstance = (WorkflowProcessInstance) session.startProcess(
                "line-item-rejected", processParams);
        
        List<TaskSummary> taskList = taskService.getTasksAssignedAsPotentialOwner(actorId, "en-UK");
        Assert.assertEquals(taskList.size(), 1);
        
        TaskSummary task = taskList.get(0);
        taskService.claim(task.getId(), actorId);
        taskService.start(task.getId(), actorId);
        LineItem replacement = new LineItem();
        boolean canEditOrder = false;
        Map<String, Object> completeParams = new HashMap<>();
        completeParams.put("replacementItem", replacement);
        completeParams.put("orderReplacement", canEditOrder);
//        ContentData content = new ContentData();
//        content.setAccessType(AccessType.Inline);
//        content.setContent(getByteArrayFromObject(completeParams));

        taskService.complete(task.getId(), actorId, completeParams);
        
        this.assertProcessInstanceCompleted(processInstance.getId(), session);
    }

    public void canEditOrder(){
    	String actorId = "admin";
    	Map<String, Object> processParams = new HashMap<String, Object>();
        processParams.put("order", new CustomerOrder());
        processParams.put("rejectedItem", new LineItem());
//        session.getWorkItemManager().registerWorkItemHandler("Replace Item on Order", new ReplaceItemOnOrderServiceWorkItemHandler());
        WorkflowProcessInstance processInstance = (WorkflowProcessInstance) session.startProcess(
                "line-item-rejected", processParams);
        
        List<TaskSummary> taskList = taskService.getTasksAssignedAsPotentialOwner(actorId, "en-UK");
        Assert.assertEquals(taskList.size(), 1);
        
        TaskSummary task = taskList.get(0);
        
        taskService.claim(task.getId(), actorId);
        taskService.start(task.getId(), actorId);
        
        LineItem replacement = new LineItem();
        boolean canEditOrder = true;
        
        Map<String, Object> completeParams = new HashMap<>();
        completeParams.put("replacementItem", replacement);
        completeParams.put("orderReplacement", canEditOrder);
//        ContentData content = new ContentData();
//        content.setAccessType(AccessType.Inline);
//        content.setContent(getByteArrayFromObject(completeParams));

        taskService.complete(task.getId(), actorId, completeParams);
        
        this.assertNodeTriggered(processInstance.getId(), "Replace Item on Order");
        
        taskList = taskService.getTasksAssignedAsPotentialOwner(actorId, "en-UK");
        Assert.assertEquals(taskList.size(), 1);
        
        task = taskList.get(0);
        
        taskService.claim(task.getId(), actorId);
        taskService.start(task.getId(), actorId);
        taskService.complete(task.getId(), actorId, null);
        
        this.assertProcessInstanceCompleted(processInstance.getId(), session);
    }

    public static byte[] getByteArrayFromObject(Object obj) {
        byte[] result = null;

        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ObjectOutputStream oos = new ObjectOutputStream(baos);
            oos.writeObject(obj);
            oos.flush();
            oos.close();
            baos.close();
            result = baos.toByteArray();
        }
        catch (IOException ioEx) {
        }
        return result;
    }
}
