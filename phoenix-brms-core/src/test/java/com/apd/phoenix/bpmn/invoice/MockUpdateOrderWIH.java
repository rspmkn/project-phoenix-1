package com.apd.phoenix.bpmn.invoice;

import java.util.Map;

import org.kie.api.runtime.process.WorkItem;
import org.kie.api.runtime.process.WorkItemHandler;
import org.kie.api.runtime.process.WorkItemManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.apd.phoenix.service.invoice.CustomerOrderUpdate;
import com.apd.phoenix.service.invoice.MessageLineItem;
import com.apd.phoenix.service.model.CustomerOrder;
import com.apd.phoenix.service.model.LineItem;

public class MockUpdateOrderWIH implements WorkItemHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(WorkItemHandler.class);

    @Override
    public void abortWorkItem(WorkItem workItem, WorkItemManager manager) {
        manager.abortWorkItem(workItem.getId());
    }

    @Override
    public void executeWorkItem(WorkItem workItem, WorkItemManager manager) {
        LOGGER.info("Updating order items");

        for (Map.Entry<String, Object> entry : workItem.getParameters().entrySet()) {
            LOGGER.info("key: " + entry.getKey() + ", value: " + entry.getValue());
            if (entry.getKey().equals("customerOrder")) {
                LOGGER.info("**Getting item sku and status in customer order**");
                CustomerOrder customerOrder = (CustomerOrder) entry.getValue();
                for (LineItem li : customerOrder.getItems()) {
                    LOGGER.info("sku: " + li.getStatus() + ", status: " + li.getApdSku());
                }
            }
            if (entry.getKey().equals("customerOrderUpdate")) {
                LOGGER.info("**Getting item sku and status in customer order update**");
                CustomerOrderUpdate cou = (CustomerOrderUpdate) entry.getValue();
                for (MessageLineItem li : cou.getItems()) {
                    LOGGER.info("sku: " + li.getStatus() + ", status: " + li.getApdSku());
                }
            }
        }
        manager.completeWorkItem(workItem.getId(), null);
    }

}
