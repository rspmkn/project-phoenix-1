package com.apd.phoenix.integration.ws.rest.xcbl.v3_5;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.rest.BaseRestEndpoint;
import com.perfect.liteenvelope.MessageAcknowledgement;

@Stateless
@Path("/osn")
public class OSNOrderEndpoint extends BaseRestEndpoint {

    protected static final Logger LOG = LoggerFactory.getLogger(OSNOrderEndpoint.class);

    @Inject
    private OSNReceiverService osnReceiverService;

    /**
     * Send the XCBL Order document (wrapped in an OSNLiteEnvelope) to the batch/integration service for further processing
     * @param order 
     *
     * @param entity the entity
     * @return the response
     */
    @POST
    @Consumes("text/xml")
    @Produces("application/xml")
    public Response placeOrder(String order) {
        Response response = null;
        try {
            if (LOG.isDebugEnabled()) {
                LOG.debug("OSNOrderEndpoint payload received: " + order);
            }
            MessageAcknowledgement messageAck = osnReceiverService.processOSNOrder(order);
            if (messageAck != null) {
                response = Response.ok(messageAck).build();
            }
            else {
                response = Response.ok().build();
            }
        }
        catch (Exception e) {
            response = Response.serverError().build();
        }
        return response != null ? response : Response.serverError().build();
    }
}
