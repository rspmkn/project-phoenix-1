package com.apd.phoenix.integration.ws.rest.xcbl.v3_5;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.Serializable;
import java.util.Properties;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;
import javax.ejb.Singleton;
import javax.inject.Inject;
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.ObjectMessage;
import javax.jms.Queue;
import javax.jms.Session;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.camel.component.ActiveMQComponent;
import org.apache.activemq.pool.PooledConnectionFactory;
import org.apache.camel.CamelContext;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.component.cdi.CdiCamelContext;
import org.apache.camel.component.jms.JmsConfiguration;
import org.apache.camel.impl.ExplicitCamelContextNameStrategy;
import org.apache.commons.lang.StringUtils;
import org.apacheextras.camel.jboss.JBossPackageScanClassResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import rrn.org_xcbl.schemas.xcbl.v3_5.xcbl35.Order;
import rrn.org_xcbl.schemas.xcbl.v3_5.xcbl35.OSNOrder;
import com.apd.phoenix.core.utility.TenantConfigRepository;
import com.apd.phoenix.integration.model.xcbl.v3_5.XCBLFactory;
import com.apd.phoenix.service.persistence.jpa.SystemUserDao;
import com.apd.phoenix.service.persistence.multitenancy.impl.CurrentTenantIdentifierResolverImpl;
import com.perfect.liteenvelope.MessageAcknowledgement;

@Singleton
public class OSNReceiverServiceCamelImpl implements OSNReceiverService {

    private static Logger LOGGER = LoggerFactory.getLogger(OSNReceiverServiceCamelImpl.class);

    public static final String CAMEL_CONTEXT_NAME_OSN_RECEIVER = "OsnReceiverService";

    @Inject
    private CdiCamelContext camelContext;

    @Resource(mappedName = "java:/activemq/xcbl-inbound")
    private Queue inboundXcblQueue;

    @Resource(mappedName = "java:/activemq/ConnectionFactory")
    protected ConnectionFactory connectionFactory;

    private ObjectMessage createXCBLJMSMessage(Session session, Serializable xcblPayload, String partnerId) {
        ObjectMessage objectMessage = null;
        try {
            objectMessage = session.createObjectMessage(xcblPayload);
            objectMessage.setStringProperty("partnerId", partnerId);
            objectMessage.setLongProperty("tenantId", CurrentTenantIdentifierResolverImpl.getCurrentTenant());
        }
        catch (JMSException e) {
            LOGGER.error(e.toString());
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("Stack trace", e);
            }
        }
        return objectMessage;
    }

    private ProducerTemplate producerTemplate;

    @Inject
    SystemUserDao userDao;

    @Override
    public MessageAcknowledgement processOSNOrder(String initialPayload) {
        String partnerId;

        OSNOrder osnOrder = unmarshallOrderXML(initialPayload);

        String accountCode = osnOrder.getOrder().getOrderHeader().getOrderReferences().getAccountCode().getReference()
                .getRefNum();

        if (StringUtils.startsWithIgnoreCase(accountCode, "ngc")) {
            partnerId = "ngc";
        }
        else if (StringUtils.startsWithIgnoreCase(accountCode, "hii")) {
            partnerId = "hii";
        }
        else {
            partnerId = "NONE";
        }

        try {
            Connection connection = connectionFactory.createConnection();
            try {
                Session msession = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
                MessageProducer messageProducer = msession.createProducer(inboundXcblQueue);
                ObjectMessage message = createXCBLJMSMessage(msession, initialPayload, partnerId);
                message.setStringProperty("envelopeId", osnOrder.getEnvelope().getDocAttributes()
                        .getIdentificationAttributes().getEnvid());
                message.setStringProperty("conversationId", osnOrder.getEnvelope().getDocAttributes()
                        .getIdentificationAttributes().getConvid());
                message.setStringProperty("partnerId", partnerId);
                messageProducer.send(message);
            }
            catch (Exception e) {
                LOGGER.error(e.toString());
                if (LOGGER.isDebugEnabled()) {
                    LOGGER.debug("Stack trace", e);
                }
            }
            if (connection != null) {
                connection.close();
            }
        }
        catch (Exception e) {
            LOGGER.error(e.toString());
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("Stack trace", e);
            }
        }
        return XCBLFactory.createOSNMessageAcknowledgement(osnOrder.getEnvelope().getDocAttributes()
                .getIdentificationAttributes().getEnvid());
    }

    @PostConstruct
    private void init() throws Exception {
        camelContext.setPackageScanClassResolver(new JBossPackageScanClassResolver());
        camelContext.setNameStrategy(new ExplicitCamelContextNameStrategy(CAMEL_CONTEXT_NAME_OSN_RECEIVER));
        producerTemplate = camelContext.createProducerTemplate();

        camelContext.start();
    }

    @PreDestroy
    private void destroy() throws Exception {
        producerTemplate.stop();
        camelContext.stop();
    }

    public void setupAmq(CamelContext context, String jmsBrokerUrl) {
        Properties amqProperties = TenantConfigRepository.getInstance().getProperties("amq");
        ActiveMQConnectionFactory activeMQConnectionFactory = new ActiveMQConnectionFactory();
        activeMQConnectionFactory.setBrokerURL(jmsBrokerUrl);
        PooledConnectionFactory pooledConnectionFactory = new PooledConnectionFactory();
        pooledConnectionFactory.setConnectionFactory(activeMQConnectionFactory);
        pooledConnectionFactory.setCreateConnectionOnStartup(true);
        pooledConnectionFactory.setMaxConnections(Integer.parseInt(amqProperties.getProperty(getClass().getSimpleName()
                + ".maxConnections", "2")));
        pooledConnectionFactory.start();
        JmsConfiguration jmsConfiguration = new JmsConfiguration();
        jmsConfiguration.setConnectionFactory(pooledConnectionFactory);
        jmsConfiguration.setConcurrentConsumers(Integer.parseInt(amqProperties.getProperty(getClass().getSimpleName()
                + ".concurrentConsumers", "2")));
        ActiveMQComponent activeMQComponent = new ActiveMQComponent();
        activeMQComponent.setConfiguration(jmsConfiguration);
        camelContext.addComponent("activemq", activeMQComponent);
    }

    public static OSNOrder unmarshallOrderXML(String orderXML) {
    	OSNOrder osnOrder = null;
	    try (ByteArrayInputStream inputStream = new ByteArrayInputStream(orderXML.getBytes())) {
	    	JAXBContext osnContext = JAXBContext.newInstance(OSNOrder.class);
	        XMLInputFactory inputFactory = XMLInputFactory.newInstance();
	        XMLEventReader reader = inputFactory.createXMLEventReader(inputStream);
	        Unmarshaller jaxbUnmarshaller = osnContext.createUnmarshaller();
	        osnOrder = (OSNOrder) jaxbUnmarshaller.unmarshal(reader);
	        
	    } catch (JAXBException | XMLStreamException | IOException ex) {
	        LOGGER.error("Error parsing xml", ex);
	    } catch (Exception ex) {
	            LOGGER.error(ex.getMessage(), ex);
	    }
	    return osnOrder;
    }

    @Override
    public void processXCBLOrder(String initialPayload) {

        try {
            Connection connection = connectionFactory.createConnection();
            try {
                Session msession = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
                MessageProducer messageProducer = msession.createProducer(inboundXcblQueue);
                ObjectMessage message = createXCBLJMSMessage(msession, initialPayload, "hubwoo");
                messageProducer.send(message);
            }
            catch (Exception e) {
                LOGGER.error(e.toString());
                if (LOGGER.isDebugEnabled()) {
                    LOGGER.debug("Stack trace", e);
                }
            }
            if (connection != null) {
                connection.close();
            }
        }
        catch (Exception e) {
            LOGGER.error(e.toString());
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("Stack trace", e);
            }
        }
    }
}
