package com.apd.phoenix.integration.ws.rest;

import java.util.Properties;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.core.utility.TenantConfigRepository;
import com.apd.phoenix.service.business.CustomerOrderRequestBp;
import com.apd.phoenix.service.persistence.multitenancy.impl.CurrentTenantIdentifierResolverImpl;
import com.apd.phoenix.service.rest.BaseRestEndpoint;
import com.apd.phoenix.service.workflow.RemoteTaskService;

@Path("/workflow/order")
@Stateless
public class WorkflowRestService extends BaseRestEndpoint {

    private static final Logger LOG = LoggerFactory.getLogger(WorkflowRestService.class);

    @Inject
    private RemoteTaskService remoteTaskService;

    @Inject
    private CustomerOrderRequestBp orderRequestBp;

    @GET
    @Path("/approve/{user}/{token}")
    public Response approve(@PathParam("user") String user, @PathParam("token") String token,
            @QueryParam("action") String action, @QueryParam("id") String id) {
        String response;
        try {
            remoteTaskService.completeApprovalTask(user, orderRequestBp.getCustomerOrderByToken(token), true);
            response = "<html><body><p>The order has been approved and submitted for processing. Thank you.</p></body></html>";

        }
        catch (Exception e) {
            LOG.info("Error approving task", e);
            response = "<html><body><p>Thank you, we are processing your request.</p><p>You will receive an email confirmation once your request has been processed. "
                    + "Please contact Customer Service at "
                    + this.getTenantPropertyByName("customer_service_email")
                    + " if you do not receive a confirmation email</p></body></html>";
            return Response.status(500).entity(response).type(MediaType.TEXT_HTML).build();
        }
        return Response.ok(response, MediaType.TEXT_HTML).build();
    }

    @GET
    @Path("/deny/{user}/{token}")
    public Response deny(@PathParam("user") String user, @PathParam("token") String token,
            @QueryParam("action") String action, @QueryParam("id") String id) {
        String response;
        try {
            remoteTaskService.completeApprovalTask(user, orderRequestBp.getCustomerOrderByToken(token), false);
            response = "<html><body><p>The order has been denied and will be cancelled.</p></body></html>";

        }
        catch (Exception e) {
            LOG.info("Error denying task", e);
            response = "<html><body><p>Thank you, we are processing your request.</p><p>You will receive an email confirmation once your request has been processed. "
                    + "Please contact Customer Service at "
                    + this.getTenantPropertyByName("customer_service_email")
                    + " if you do not receive a confirmation email</p></body></html>";
            return Response.status(500).entity(response).type(MediaType.TEXT_HTML).build();
        }
        return Response.ok(response, MediaType.TEXT_HTML).build();
    }

    private Properties getTenantProperties() {
        return TenantConfigRepository.getInstance().getPropertiesByTenantId(
                CurrentTenantIdentifierResolverImpl.getCurrentTenant(), "tenant");
    }

    private String getTenantPropertyByName(String propertyName) {
        return getTenantProperties().getProperty(propertyName);
    }
}
