package com.apd.phoenix.timer.ussco;

import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import com.apd.phoenix.timer.api.AbstractTimerServiceActivator;
import com.apd.phoenix.timer.api.AbstractTimerServiceBean;

@Singleton
@Startup
public class UsscoCsvCreationServiceActivator extends AbstractTimerServiceActivator {

    @EJB
    private UsscoCsvCreationServiceBean service;

    @Override
    protected AbstractTimerServiceBean getServiceBean() {
        return service;
    }
}
