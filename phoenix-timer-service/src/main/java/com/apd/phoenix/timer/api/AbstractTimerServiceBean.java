package com.apd.phoenix.timer.api;

import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import javax.annotation.Resource;
import javax.ejb.ScheduleExpression;
import javax.ejb.SessionContext;
import javax.ejb.Timeout;
import javax.ejb.Timer;
import javax.ejb.TimerConfig;
import javax.ejb.TimerService;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import org.jboss.as.server.ServerEnvironment;
import org.jboss.msc.inject.Injector;
import org.jboss.msc.service.Service;
import org.jboss.msc.service.ServiceName;
import org.jboss.msc.service.StartContext;
import org.jboss.msc.service.StartException;
import org.jboss.msc.service.StopContext;
import org.jboss.msc.value.InjectedValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.core.utility.TenantConfigRepository;
import com.apd.phoenix.service.persistence.multitenancy.impl.CurrentTenantIdentifierResolverImpl;

public abstract class AbstractTimerServiceBean implements Service<String> {

    @Resource
    private SessionContext sessionContext;

    private static final Logger LOGGER = LoggerFactory.getLogger(AbstractTimerServiceBean.class);

    private final AtomicBoolean started = new AtomicBoolean(false);

    private String nodeName;

    private final InjectedValue<ServerEnvironment> env = new InjectedValue<ServerEnvironment>();

    public abstract ServiceName getServiceName();

    public abstract ScheduleExpression getScheduleExpression();

    public abstract void execute(Timer timer, String tenant);

    public Injector<ServerEnvironment> getEnvInjector() {
        return this.env;
    }

    /**
     * @return the name of the server node
     */
    public String getValue() throws IllegalStateException, IllegalArgumentException {
        if (!started.get()) {
            throw new IllegalStateException("The service '" + this.getClass().getName() + "' is not ready!");
        }
        return this.nodeName;
    }

    public void start(StartContext arg0) throws StartException {
        if (!started.compareAndSet(false, true)) {
            throw new StartException("The service is still started!");
        }
        LOGGER.info("Start service '" + this.getClass().getName() + "'");
        this.nodeName = this.env.getValue().getNodeName();

        //starts timer
        timerService.createCalendarTimer(getScheduleExpression(), new TimerConfig(this.getTimerName(), false));
    }

    @Timeout
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public void timeout(Timer timer) {
        Set<String> tenants = TenantConfigRepository.getInstance().getTenantNames();
        for (String tenantName : tenants) {
            CurrentTenantIdentifierResolverImpl.setCurrentTenant(TenantConfigRepository.getInstance()
                    .getTenantIdByName(tenantName));
            sessionContext.getBusinessObject(this.getClass()).execute(timer, tenantName);
        }
    }

    public void stop(StopContext arg0) {
    }

    @Resource
    private TimerService timerService;

    private String getTimerName() {
        return "timer for " + this.nodeName;
    }

}
