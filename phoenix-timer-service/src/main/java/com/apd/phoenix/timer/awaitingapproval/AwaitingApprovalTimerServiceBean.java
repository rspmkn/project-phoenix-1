/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.timer.awaitingapproval;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.ejb.ScheduleExpression;
import javax.ejb.Singleton;
import javax.ejb.Timer;
import javax.inject.Inject;
import javax.inject.Named;
import org.jboss.ejb3.annotation.TransactionTimeout;
import org.jboss.msc.service.ServiceName;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.business.CustomerOrderBp;
import com.apd.phoenix.service.business.EmailFactoryBp;
import com.apd.phoenix.service.email.impl.ApproveOrderReminderEmailTemplate;
import com.apd.phoenix.service.message.api.Message;
import com.apd.phoenix.service.message.api.MessageService;
import com.apd.phoenix.service.message.impl.DenyOrderMessageSender;
import com.apd.phoenix.service.model.CustomerOrder;
import com.apd.phoenix.service.model.OrderLog.EventType;
import com.apd.phoenix.timer.api.AbstractTimerServiceBean;

/**
 *
 * @author nreidelb
 */
@Named
@Singleton
public class AwaitingApprovalTimerServiceBean extends AbstractTimerServiceBean {

    private static final Logger LOGGER = LoggerFactory.getLogger(AwaitingApprovalTimerServiceBean.class);

    private static final int TIMEOUT_IN_HOURS = 12;

    @Inject
    CustomerOrderBp customerOrderBp;

    @Inject
    private DenyOrderMessageSender messageSender;

    @Inject
    EmailFactoryBp emailFactoryBp;

    @Inject
    MessageService messageService;

    @Override
    public ServiceName getServiceName() {
        return ServiceName.JBOSS.append("quickstart", "awaitingApprovalTimerService", "singleton");
    }

    @Override
    public ScheduleExpression getScheduleExpression() {
        ScheduleExpression se = new ScheduleExpression();
        // Run at 3:00AM
        se.hour("3").minute("0").second("0");
        return se;
    }

    public void sendApproveOrderReminder(CustomerOrder order) {
        order = customerOrderBp.findById(order.getId(), CustomerOrder.class);
        List<Message> emails = emailFactoryBp.createOrderEmails(order.getId(), null, EventType.APPROVE_ORDER_REMINDER);
        for (Message message : emails) {
            if (message != null) {
                messageService.sendOrderMessage(order, message, EventType.APPROVE_ORDER_REMINDER);
            }
            else {
                LOGGER.error("Tried send an email with a null message");
            }
        }

    }

    @Override
    @TransactionTimeout(value = (60 * 60 * TIMEOUT_IN_HOURS))
    public void execute(Timer timer, String tenant) {

        Calendar orderDenialCalendar = Calendar.getInstance();
        orderDenialCalendar.add(Calendar.DAY_OF_YEAR,
                -ApproveOrderReminderEmailTemplate.DAYS_UNTIL_ORDER_IS_DENIED_AUTOMATICALLY);

        Calendar orderStopDenyingCalendar = Calendar.getInstance();
        orderStopDenyingCalendar.add(Calendar.DAY_OF_YEAR,
                -ApproveOrderReminderEmailTemplate.DAYS_UNTIL_ORDER_STOPS_TRYING_TO_DENY);

        List<CustomerOrder> ordersToRemind = customerOrderBp.checkLongWaitingApprovalOrders(orderDenialCalendar
                .getTime(), new Date());

        List<CustomerOrder> ordersToDeny = customerOrderBp.checkLongWaitingApprovalOrders(orderStopDenyingCalendar
                .getTime(), orderDenialCalendar.getTime());

        LOGGER.info("found: " + ordersToRemind.size() + " Orders Pending Approval");

        LOGGER.info("found: " + ordersToDeny.size() + " long-waiting Orders Pending Approval to deny");

        for (CustomerOrder order : ordersToRemind) {
            sendApproveOrderReminder(order);
        }

        for (CustomerOrder order : ordersToDeny) {
            messageSender.sendDenyOrder(order);
        }
    }
}
