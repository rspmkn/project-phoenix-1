package com.apd.phoenix.timer.apreport;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import javax.ejb.ScheduleExpression;
import javax.ejb.Singleton;
import javax.ejb.Timer;
import javax.inject.Inject;
import javax.inject.Named;
import org.jboss.msc.service.ServiceName;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.core.utility.TenantConfigRepository;
import com.apd.phoenix.service.email.api.Attachment;
import com.apd.phoenix.service.email.api.Attachment.MimeType;
import com.apd.phoenix.service.message.impl.EmailMessageSender;
import com.apd.phoenix.service.persistence.multitenancy.impl.CurrentTenantIdentifierResolverImpl;
import com.apd.phoenix.service.report.ReportService;
import com.apd.phoenix.timer.api.AbstractTimerServiceBean;

@Singleton
@Named
public class InvoiceReportTimerServiceBean extends AbstractTimerServiceBean {

    private static final Logger LOGGER = LoggerFactory.getLogger(InvoiceReportTimerServiceBean.class);

    @Inject
    private EmailMessageSender emailMessageSender;

    @Inject
    private ReportService reportService;

    @Override
    public ServiceName getServiceName() {
        return ServiceName.JBOSS.append("InvoiceReport", "poll", "hasingleton");
    }

    @Override
    public ScheduleExpression getScheduleExpression() {
        ScheduleExpression se = new ScheduleExpression();
        se.hour("2").minute("0").second("0");
        return se;
    }

    @Override
    public void execute(Timer timer, String tenant) {
        List<Attachment> apAttachments = new ArrayList<Attachment>();
        Attachment apReport = new Attachment();
        apReport.setFileName("ap-report" + ".csv");
        apReport.setMimeType(MimeType.csv);
        InputStream apis = reportService.generateApReport();
        if (apis != null) {
            apReport.setContent(apis);
            apAttachments.add(apReport);
        }
        Properties internalEmailProperties = TenantConfigRepository.getInstance().getPropertiesByTenantId(
                CurrentTenantIdentifierResolverImpl.getCurrentTenant(), "tenant");
        emailMessageSender.sendSimpleMessage(internalEmailProperties.getProperty("internal.emails.noreply"),
                internalEmailProperties.getProperty("internal.emails.invoice.ap.report"), "AP Report",
                "Please see the Attached AP Report.", apAttachments);

        List<Attachment> arAttachments = new ArrayList<Attachment>();
        Attachment arReport = new Attachment();
        arReport.setFileName("ar-report" + ".csv");
        arReport.setMimeType(MimeType.csv);
        InputStream aris = reportService.generateArReport();
        if (aris != null) {
            arReport.setContent(aris);
            arAttachments.add(arReport);
        }
        emailMessageSender.sendSimpleMessage(internalEmailProperties.getProperty("internal.emails.noreply"),
                internalEmailProperties.getProperty("internal.emails.invoice.ar.report"), "AR Report",
                "Please see the Attached AR Report.", arAttachments);

        List<Attachment> dailyChargeReportAttachments = new ArrayList<Attachment>();
        Attachment dailyChargeReport = new Attachment();
        dailyChargeReport.setFileName("daily-charge-report" + ".csv");
        dailyChargeReport.setMimeType(MimeType.csv);
        InputStream dcis = reportService.generateDailyChargeReport();
        if (dcis != null) {
            dailyChargeReport.setContent(dcis);
            dailyChargeReportAttachments.add(dailyChargeReport);
        }
        emailMessageSender.sendSimpleMessage(internalEmailProperties.getProperty("internal.emails.noreply"),
                internalEmailProperties.getProperty("internal.emails.invoice.daily.charge.report"), "Charge Report",
                "Please see the Attached Daily Charge Report.", dailyChargeReportAttachments);
    }
}
