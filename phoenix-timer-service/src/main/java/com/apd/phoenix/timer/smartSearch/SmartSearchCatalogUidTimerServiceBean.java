package com.apd.phoenix.timer.smartSearch;

import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;
import javax.ejb.AccessTimeout;
import javax.ejb.ScheduleExpression;
import javax.ejb.Singleton;
import javax.ejb.Timer;
import javax.inject.Inject;
import javax.inject.Named;
import org.jboss.ejb3.annotation.TransactionTimeout;
import org.jboss.msc.service.ServiceName;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.business.CatalogBp;
import com.apd.phoenix.service.business.ListHashToUidBp;
import com.apd.phoenix.service.model.Catalog;
import com.apd.phoenix.service.model.ListHashToUid;
import com.apd.phoenix.service.search.integration.client.backend.SmartSearchInsertMessageEntryPoint;
import com.apd.phoenix.timer.api.AbstractTimerServiceBean;

@Singleton
@Named
@TransactionTimeout(1200)
public class SmartSearchCatalogUidTimerServiceBean extends AbstractTimerServiceBean {

    private static final Logger LOGGER = LoggerFactory.getLogger(SmartSearchCatalogUidTimerServiceBean.class);

    @Inject
    private CatalogBp catalogBp;

    @Inject
    private ListHashToUidBp uidBp;

    @Inject
    private SmartSearchInsertMessageEntryPoint entryPoint;

    @Override
    public ServiceName getServiceName() {
        return ServiceName.JBOSS.append("SmartSearchCatalogUid", "poll", "hasingleton");
    }

    @Override
    public ScheduleExpression getScheduleExpression() {
        ScheduleExpression se = new ScheduleExpression();
        // normal value        
        se.hour("*").minute("30").second("0");
        //test value
        //se.hour("*").minute("*").second("0");
        return se;
    }

    /**
     * This method takes a hash and the current UID, and returns what should be the new UID
     * 
     * @param hash
     * @param currentUid
     * @return new UID for catalog
     */
    private ListHashToUid getNewUid(Long hash, ListHashToUid currentUid) {
        //if the hash matches the current UID, the current UID is correct
        if (currentUid != null && currentUid.getHash() != null && currentUid.getHash() == hash) {
            return currentUid;
        }

        //if a UID is found whose hash matches this hash, returns that UID
        ListHashToUid searchUid = uidBp.searchByHash(hash);
        if (searchUid != null) {
            return searchUid;
        }

        //if there is no catalog whose UID is this UID and whose hash matches 
        //this UID's hash, sets this UID's hash to the new hash
        if (currentUid != null && currentUid.getId() != null) {
            boolean canChangeHash = true;
            Catalog searchCatalog = new Catalog();
            searchCatalog.setListUid(new ListHashToUid());
            searchCatalog.getListUid().setId(currentUid.getId());
            List<Catalog> searchResults = catalogBp.searchByExactExample(searchCatalog, 0, 0);
            Catalog coreSearchCatalog = new Catalog();
            coreSearchCatalog.setListUid(new ListHashToUid());
            coreSearchCatalog.getListUid().setId(currentUid.getId());
            List<Catalog> coreSearchResults = catalogBp.searchByExactExample(coreSearchCatalog, 0, 0);
            for (Catalog result : searchResults) {
                if (result.getItemHash() == currentUid.getHash()) {
                    canChangeHash = false;
                }
            }
            for (Catalog result : coreSearchResults) {
                if (result.getItemHash() == currentUid.getHash()) {
                    canChangeHash = false;
                }
            }
            if (canChangeHash) {
                currentUid.setHash(hash);
                return uidBp.update(currentUid);
            }
        }

        //finally, if none of the above is true, creates a new UID
        return createNewUid(hash);
    }

    private ListHashToUid createNewUid(Long hash) {
        ListHashToUid newUid = new ListHashToUid();
        newUid.setCreationDate(new Date());
        newUid.setHash(hash);
        return uidBp.update(newUid);
    }

    @Override
    @AccessTimeout(unit = TimeUnit.HOURS, value = 2)
    public void execute(Timer timer, String tenant) {
        LOGGER.info("Getting catalogs with incorrect UIDs");
        List<Catalog> catalogsWithWrongUids = catalogBp.getCatalogsWithWrongListUid();
        LOGGER.info("{} catalogs with wrong UIDs", catalogsWithWrongUids.size());
        //for each catalog with non-matching UIDs (due to a hash change, from an item insert/delete)
        for (Catalog catalog : catalogsWithWrongUids) {
            //gets the new UID for both the regular and core items
            ListHashToUid newListUid = this.getNewUid(catalog.getItemHash(), catalog.getListUid());
            if (newListUid != null && uidBp.isListReady(newListUid)) {
                catalog.setListUid(newListUid);
                catalog = catalogBp.update(catalog);
            }
            ListHashToUid newCoreListUid = this.getNewUid(catalog.getCoreItemHash(), catalog.getCoreListUid());
            if (newCoreListUid != null && uidBp.isListReady(newCoreListUid)) {
                catalog.setCoreListUid(newCoreListUid);
                catalog = catalogBp.update(catalog);
            }
            //then reindexes the catalog
            entryPoint.scheduleRequest(new CatalogBp.SmartSearchInsertMessage(catalog.getId(), newListUid.getId(),
                    newCoreListUid.getId()));
        }
        uidBp.deleteUnusedUids();
        //TODO: remove unused Smart Search lists here
    }
}
