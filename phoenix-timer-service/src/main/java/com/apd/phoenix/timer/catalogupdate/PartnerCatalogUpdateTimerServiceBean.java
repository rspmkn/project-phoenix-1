/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.timer.catalogupdate;

import java.util.logging.Logger;
import javax.ejb.ScheduleExpression;
import javax.ejb.Singleton;
import javax.ejb.Timer;
import javax.inject.Inject;
import javax.inject.Named;
import org.jboss.msc.service.ServiceName;
import com.apd.phoenix.service.utility.PartnerCatalogUpdateService;
import com.apd.phoenix.timer.api.AbstractTimerServiceBean;
import org.jboss.ejb3.annotation.TransactionTimeout;

/**
 *
 * @author nreidelb
 */
@Named
@Singleton
public class PartnerCatalogUpdateTimerServiceBean extends AbstractTimerServiceBean {

    private static final Logger LOGGER = Logger
            .getLogger(PartnerCatalogUpdateTimerServiceBean.class.getCanonicalName());

    private static final int TIMEOUT_IN_HOURS = 12;

    @Inject
    private PartnerCatalogUpdateService catalogUpdateService;

    @Override
    public ServiceName getServiceName() {
        return ServiceName.JBOSS.append("quickstart", "catalogTimerService", "singleton");
    }

    @Override
    public ScheduleExpression getScheduleExpression() {
        ScheduleExpression se = new ScheduleExpression();
        //Run at 1:00AM
        se.hour("1").minute("0").second("0");
        return se;
    }

    @Override
    @TransactionTimeout(value = (60 * 60 * TIMEOUT_IN_HOURS))
    public void execute(Timer timer, String tenant) {
        LOGGER.info("Do nothing");
        //catalogUpdateService.sendCatalogUpdate();
    }
}
