/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.timer.orderstuck;

import java.util.List;
import java.util.logging.Logger;
import javax.ejb.ScheduleExpression;
import javax.ejb.Singleton;
import javax.ejb.Timer;
import javax.inject.Inject;
import javax.inject.Named;
import org.jboss.msc.service.ServiceName;
import com.apd.phoenix.service.business.CustomerOrderBp;
import com.apd.phoenix.service.model.CustomerOrder;
import com.apd.phoenix.service.workflow.RemoteTaskService;
import com.apd.phoenix.service.workflow.WorkflowService;
import com.apd.phoenix.timer.api.AbstractTimerServiceBean;

@Named
@Singleton
public class OrderStuckTimerServiceBean extends AbstractTimerServiceBean {

    private static final Logger LOGGER = Logger.getLogger(OrderStuckTimerServiceBean.class.getCanonicalName());
    //Amount of time (hrs) an order is stuck before triggering a task
    private static final int THRESHOLD = 4;
    //The Max age that an order is waiting threshold+frequency
    private static final int FREQUENCY = 1;

    @Inject
    private CustomerOrderBp customerOrderBp;

    @Inject
    private WorkflowService workflowService;

    @Inject
    private RemoteTaskService remoteTaskService;

    @Override
    public ServiceName getServiceName() {
        return ServiceName.JBOSS.append("orderstuck", "ha", "singleton");
    }

    @Override
    public ScheduleExpression getScheduleExpression() {
        ScheduleExpression se = new ScheduleExpression();
        se.hour("*").minute("0").second("0");
        return se;
    }

    @Override
    public void execute(Timer timer, String tenant) {
        for (CustomerOrder customerOrder : customerOrderBp.checkStuckOrders(THRESHOLD, FREQUENCY)) {
            boolean stuckOrderTaskExists = false;
            List<Long> stuckOrderTasks = remoteTaskService.getTasks(customerOrder, RemoteTaskService.STUCK_ORDER_TASK);
            stuckOrderTaskExists = (stuckOrderTasks != null && !stuckOrderTasks.isEmpty());
            if (!stuckOrderTaskExists) {
                workflowService.processOrderStuck(customerOrder);
            }
        }
    }
}
