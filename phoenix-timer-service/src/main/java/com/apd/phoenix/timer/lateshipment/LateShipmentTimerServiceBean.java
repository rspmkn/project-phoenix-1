package com.apd.phoenix.timer.lateshipment;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.ScheduleExpression;
import javax.ejb.Singleton;
import javax.ejb.Timer;
import javax.inject.Inject;
import javax.inject.Named;
import org.jboss.msc.service.ServiceName;
import com.apd.phoenix.service.business.CustomerOrderBp;
import com.apd.phoenix.service.business.EmailFactoryBp;
import com.apd.phoenix.service.message.api.Message;
import com.apd.phoenix.service.message.api.MessageService;
import com.apd.phoenix.service.model.CustomerOrder;
import com.apd.phoenix.service.model.OrderLog.EventType;
import com.apd.phoenix.timer.api.AbstractTimerServiceBean;

/**
 *
 * @author anicholson
 */
@Singleton
@Named
public class LateShipmentTimerServiceBean extends AbstractTimerServiceBean {

    private static final Logger LOGGER = Logger.getLogger(LateShipmentTimerServiceBean.class.getCanonicalName());

    @Inject
    private CustomerOrderBp customerOrderBp;

    @Inject
    private EmailFactoryBp emailFactoryBp;

    @Inject
    private MessageService messageService;

    @Override
    public ServiceName getServiceName() {
        return ServiceName.JBOSS.append("lateshipment", "poll", "hasingleton");
    }

    @Override
    public ScheduleExpression getScheduleExpression() {
        ScheduleExpression se = new ScheduleExpression();
        se.hour("23").minute("55").second("0");
        return se;
    }

    @Override
    public void execute(Timer timer, String tenant) {
        LOGGER.log(Level.INFO, "Checking for late shipments....");
        List<CustomerOrder> lateOrders = customerOrderBp.getLateOrdersFromToday();
        if (!lateOrders.isEmpty()) {
            LOGGER.log(Level.INFO, "{0} orders late", lateOrders.size());
            for (CustomerOrder order : lateOrders) {
                Long orderId = order.getId();
                Object content = order;
                EventType type = EventType.LATE_SHIPMENT_NOTICE;
                List<Message> messages = emailFactoryBp.createOrderEmails(orderId, content, type);
                for (Message message : messages) {
                    messageService.sendMessage(message, type);
                }
            }
        }
    }
}
