package com.apd.phoenix.timer.smartSearch;

import java.util.List;
import java.util.concurrent.TimeUnit;
import javax.ejb.AccessTimeout;
import javax.ejb.ScheduleExpression;
import javax.ejb.Singleton;
import javax.ejb.Timer;
import javax.inject.Inject;
import javax.inject.Named;
import org.jboss.ejb3.annotation.TransactionTimeout;
import org.jboss.msc.service.ServiceName;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.business.CatalogXItemBp;
import com.apd.phoenix.service.message.impl.InternalMessageSender;
import com.apd.phoenix.service.model.CatalogXItem;
import com.apd.phoenix.service.model.listener.SolrEntityListener.SolrDocumentAction;
import com.apd.phoenix.timer.api.AbstractTimerServiceBean;

@Singleton
@Named
@TransactionTimeout(1200)
public class SmartSearchCatalogUpdateTimerServiceBean extends AbstractTimerServiceBean {

    private static final Logger LOGGER = LoggerFactory.getLogger(SmartSearchCatalogUpdateTimerServiceBean.class);

    @Inject
    CatalogXItemBp catalogXItemBp;

    @Inject
    InternalMessageSender internalMessageSender;

    @Override
    public ServiceName getServiceName() {
        return ServiceName.JBOSS.append("SmartSearchCatalogUpdate", "poll", "hasingleton");
    }

    @Override
    public ScheduleExpression getScheduleExpression() {
        ScheduleExpression se = new ScheduleExpression();
        // normal value        
        se.hour("*").minute("40").second("0");
        //test value
        //se.hour("*").minute("*").second("0");
        return se;
    }

    @Override
    @AccessTimeout(unit = TimeUnit.HOURS, value = 2)
    public void execute(Timer timer, String tenant) {
        LOGGER.info("Checking for SmartSearch Items whose core/contract status has changed");
        List<CatalogXItem> newlyActiveItems = catalogXItemBp.findSmartSearchItemsInNewlyActiveCatalog();
        LOGGER.info("Found " + newlyActiveItems.size() + " items in newly active smart search catalogs");
        for (CatalogXItem item : newlyActiveItems) {
            internalMessageSender.sendSmartSearchItemUpdate(item, SolrDocumentAction.CATXI_UPDATE);
        }

        List<CatalogXItem> noLongerCoreItems = catalogXItemBp.findSmartSearchItemsInNewlyInactiveCatalog();
        LOGGER.info("Found " + noLongerCoreItems.size() + " items in newly inactive smart search catalogs");
        for (CatalogXItem item : noLongerCoreItems) {
            internalMessageSender.sendSmartSearchItemUpdate(item, SolrDocumentAction.CATXI_DELETE);
        }
    }
}