package com.apd.phoenix.timer.smartSearch;

import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import com.apd.phoenix.timer.api.AbstractTimerServiceActivator;
import com.apd.phoenix.timer.api.AbstractTimerServiceBean;

@Startup
@Singleton
public class SmartSearchCatalogHashTimerServiceActivator extends AbstractTimerServiceActivator {

    @EJB
    SmartSearchCatalogHashTimerServiceBean bean;

    @Override
    protected AbstractTimerServiceBean getServiceBean() {
        return bean;
    }

}
