package com.apd.phoenix.timer.apreport;

import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.timer.api.AbstractTimerServiceActivator;
import com.apd.phoenix.timer.api.AbstractTimerServiceBean;

@Startup
@Singleton
public class InvoiceReportTimerServiceActivator extends AbstractTimerServiceActivator {

    private static final Logger LOGGER = LoggerFactory.getLogger(InvoiceReportTimerServiceActivator.class);

    @EJB
    private InvoiceReportTimerServiceBean service;

    @Override
    protected AbstractTimerServiceBean getServiceBean() {
        return service;
    }
}
