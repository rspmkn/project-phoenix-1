package com.apd.phoenix.timer.catalog;

import javax.ejb.ScheduleExpression;
import javax.ejb.Singleton;
import javax.ejb.Timer;
import javax.inject.Inject;
import javax.inject.Named;
import org.jboss.msc.service.ServiceName;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.core.utility.TenantConfigRepository;
import com.apd.phoenix.service.utility.SendInternalHttpRequest;
import com.apd.phoenix.timer.api.AbstractTimerServiceBean;

/**
 * @author <a href="mailto:wfink@redhat.com">Wolf-Dieter Fink</a>
 */
@Named
@Singleton
public class CatalogProcessCsvTimerServiceBean extends AbstractTimerServiceBean {

    private static final Logger LOGGER = LoggerFactory.getLogger(CatalogProcessCsvTimerServiceBean.class);

    @Inject
    TenantConfigRepository tenantConfigRepository;

    @Override
    public ServiceName getServiceName() {
        return ServiceName.JBOSS.append("catalogprocesscsv", "ha", "singleton");
    }

    @Override
    public ScheduleExpression getScheduleExpression() {

        ScheduleExpression se = new ScheduleExpression();
        se.hour(tenantConfigRepository.getProperty("catalog.upload.integration", "csvTimerHour")).minute(
                tenantConfigRepository.getProperty("catalog.upload.integration", "csvTimerMinute")).second(
                tenantConfigRepository.getProperty("catalog.upload.integration", "csvTimerSecond"));

        return se;
    }

    @Override
    public void execute(Timer timer, String tenant) {
        LOGGER.info("CSV Processing Timeout started!");
        SendInternalHttpRequest.sendRequest(tenantConfigRepository.getProperty("catalog.upload.integration",
                "executeScheduledActionsUrl"));
        LOGGER.info("CSV Processing Timeout ended!");
    }
}