package com.apd.phoenix.timer.taxupload;

import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import com.apd.phoenix.timer.api.AbstractTimerServiceActivator;
import com.apd.phoenix.timer.api.AbstractTimerServiceBean;

@Startup
@Singleton
public class TaxUploadReportTimerServiceActivator extends AbstractTimerServiceActivator {

    @EJB
    private TaxUploadReportTimerServiceBean service;

    @Override
    protected AbstractTimerServiceBean getServiceBean() {
        return service;
    }
}
