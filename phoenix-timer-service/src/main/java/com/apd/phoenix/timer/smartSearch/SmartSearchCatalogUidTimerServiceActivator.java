package com.apd.phoenix.timer.smartSearch;

import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import com.apd.phoenix.timer.api.AbstractTimerServiceActivator;
import com.apd.phoenix.timer.api.AbstractTimerServiceBean;

@Startup
@Singleton
public class SmartSearchCatalogUidTimerServiceActivator extends AbstractTimerServiceActivator {

    @EJB
    SmartSearchCatalogUidTimerServiceBean bean;

    @Override
    protected AbstractTimerServiceBean getServiceBean() {
        return bean;
    }

}
