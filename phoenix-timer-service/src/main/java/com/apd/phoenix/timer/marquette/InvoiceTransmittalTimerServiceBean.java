package com.apd.phoenix.timer.marquette;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import javax.ejb.AccessTimeout;
import javax.ejb.ScheduleExpression;
import javax.ejb.Singleton;
import javax.ejb.Timer;
import javax.inject.Inject;
import javax.inject.Named;
import org.jboss.ejb3.annotation.TransactionTimeout;
import org.jboss.msc.service.ServiceName;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.core.utility.TenantConfigRepository;
import com.apd.phoenix.service.business.InvoiceBp;
import com.apd.phoenix.service.message.api.MessageService;
import com.apd.phoenix.service.model.CustomerCreditInvoice;
import com.apd.phoenix.service.model.CustomerInvoice;
import com.apd.phoenix.service.model.dto.CustomerCreditInvoiceDto;
import com.apd.phoenix.service.model.dto.InvoiceDto;
import com.apd.phoenix.service.model.factory.DtoFactory;
import com.apd.phoenix.service.model.factory.DtoFactory.ParsingException;
import com.apd.phoenix.service.persistence.multitenancy.impl.CurrentTenantIdentifierResolverImpl;
import com.apd.phoenix.timer.api.AbstractTimerServiceBean;

@Singleton
@Named
@TransactionTimeout(1200)
public class InvoiceTransmittalTimerServiceBean extends AbstractTimerServiceBean {

    private static final Logger LOGGER = LoggerFactory.getLogger(InvoiceTransmittalTimerServiceBean.class);

    @Inject
    private InvoiceBp invoiceBp;

    @Inject
    private MessageService messageService;

    @Override
    public ServiceName getServiceName() {
        return ServiceName.JBOSS.append("InvoiceTransmittal", "poll", "hasingleton");
    }

    @Override
    public ScheduleExpression getScheduleExpression() {
        ScheduleExpression se = new ScheduleExpression();
        // normal value

        se.hour("0").minute("5").second("0");
        //test value
        //se.hour("*").minute("*").second("0");
        return se;
    }

    @Override
    @AccessTimeout(unit = TimeUnit.HOURS, value = 2)
    public void execute(Timer timer, String tenant) {
    	Long tenantId = CurrentTenantIdentifierResolverImpl.getCurrentTenant();
    	
    	boolean isMarquetteTenant = TenantConfigRepository.getInstance().getPropertiesByTenantId(tenantId, "tenant").getProperty("marquette_enabled").equalsIgnoreCase("true");
    	
    	if(isMarquetteTenant){
	    	List<CustomerInvoice> invoices = invoiceBp.findYesterdaysMarquetteInvoices();
	        LOGGER.info("Found " + invoices.size() + " debit invoices.");
	        List<InvoiceDto> dtos = new ArrayList<>();
	        for (CustomerInvoice invoice : invoices) {
	        	//Need to create a dto without all the unneeded item and account address data which would make the message too large
	            dtos.add(DtoFactory.createMarquetteStreamlinedCustInvoiceDto(invoice));
	        }
	        messageService.sendMarquetteInvoice(dtos, Boolean.FALSE);
	        List<CustomerCreditInvoice> creditinvoices = invoiceBp.findYesterdaysMarquetteCreditInvoices();
	        LOGGER.info("Found " + creditinvoices.size() + " credit invoices.");
	        List<InvoiceDto> creditDtos = new ArrayList<>();
	        for (CustomerCreditInvoice invoice : creditinvoices) {
	            try {
	            	//Ensure all amounts are negative for the reports
	            	CustomerCreditInvoiceDto customerCreditInvoiceDto = DtoFactory.createMaquetteStreamlinedCustomerCreditInvoiceDto(invoice);
	            	if(customerCreditInvoiceDto.getAmount() != null){
	            		customerCreditInvoiceDto.setAmount(customerCreditInvoiceDto.getAmount().abs().negate());
	            	} else {
	            		LOGGER.error("Null amount on credit invoice dto " + customerCreditInvoiceDto.getInvoiceNumber());
	            	}
					creditDtos.add(customerCreditInvoiceDto);
	            }
	            catch (ParsingException e) {
	                LOGGER.error("PARSE EXCEPTION");
	            }
	        }
	        messageService.sendMarquetteInvoice(creditDtos, Boolean.TRUE);
    	}else{
    		LOGGER.info("Tenant with ID: " + tenantId + " does not currently support integration with marquette.");
    	}
    }
}
