package com.apd.phoenix.timer.taxupload;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Properties;
import javax.ejb.ScheduleExpression;
import javax.ejb.Singleton;
import javax.ejb.Timer;
import javax.inject.Inject;
import javax.inject.Named;
import org.apache.commons.lang.StringEscapeUtils;
import org.jboss.msc.service.ServiceName;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.core.utility.TenantConfigRepository;
import com.apd.phoenix.service.business.EntityComparator;
import com.apd.phoenix.service.business.TaxRateLogBp;
import com.apd.phoenix.service.email.api.Attachment;
import com.apd.phoenix.service.email.api.Attachment.MimeType;
import com.apd.phoenix.service.message.impl.EmailMessageSender;
import com.apd.phoenix.service.model.TaxRateLog;
import com.apd.phoenix.service.persistence.multitenancy.impl.CurrentTenantIdentifierResolverImpl;
import com.apd.phoenix.timer.api.AbstractTimerServiceBean;

@Singleton
@Named
public class TaxUploadReportTimerServiceBean extends AbstractTimerServiceBean {

    private static final Logger LOGGER = LoggerFactory.getLogger(TaxUploadReportTimerServiceBean.class);

    @Inject
    private EmailMessageSender emailMessageSender;

    @Inject
    private TaxRateLogBp logBp;

    @Override
    public ServiceName getServiceName() {
        return ServiceName.JBOSS.append("taxupload", "poll", "hasingleton");
    }

    @Override
    public ScheduleExpression getScheduleExpression() {
        ScheduleExpression se = new ScheduleExpression();
        se.hour("0").minute("30").second("0");
        return se;
    }

    @Override    
    public void execute(Timer timer, String tenant) {
    	LOGGER.info("Generating tax upload report");
    	List<TaxRateLog> logs = logBp.findAll(TaxRateLog.class, 0, 0);
    	if (logs == null || logs.isEmpty()) {
    		LOGGER.info("No tax uploads today, not sending email");
    		return;
    	}
    	else {
    		LOGGER.info("Sending email for " + logs.size() + " tax uploads");
    	}
    	Collections.sort(logs, new EntityComparator());
    	
    	//generates and stores the CSV
    	String path = "/var/tmp/"+tenant+"taxReport.csv";
    	File tempFile = new File(path);
    	if (tempFile.exists()) {
    		tempFile.delete();
    	}
    	try (
	    		FileWriter fw = new FileWriter(tempFile.getAbsoluteFile());
	    		BufferedWriter bw = new BufferedWriter(fw);
    		) {
    		tempFile.createNewFile();
    		bw.write("type,action,status,zip,identifier,timestamp");
			bw.write("\n");
	    	for (TaxRateLog log : logs) {
	    		if (log.getType() != null) {
	    			bw.write(log.getType().name());
	    		}
    			bw.write(",");
	    		if (log.getAction() != null) {
	    			bw.write(log.getAction().name());
	    		}
    			bw.write(",");
	    		if (log.getStatus() != null) {
	    			bw.write(log.getStatus().name());
	    		}
    			bw.write(",");
	    		if (log.getZip() != null) {
	    			bw.write(log.getZip());
	    		}
    			bw.write(",");
	    		if (log.getIdentifier() != null) {
	    			bw.write(log.getIdentifier());
	    		}
    			bw.write(",");
	    		if (log.getTimestamp() != null) {
	    			bw.write(StringEscapeUtils.escapeCsv(log.getTimestamp().toString()));
	    		}
    			bw.write("\n");
	    	}
    	}
    	catch (Exception e) {
    		LOGGER.error("Problem storing CSV for tax report", e);
    	}
    	
    	//now that the CSV is generated, create email and attach it
    	try (
    			FileInputStream stream = new FileInputStream(tempFile)
    		) {
        	Attachment report = new Attachment();
        	report.setFileName("report.csv");
        	report.setMimeType(MimeType.pdf);
	        if (stream != null) {
	        	report.setContent(stream);
	        }
	        List<Attachment> attachments = new ArrayList<>();
	        attachments.add(report);

	        Properties tenantProperties = TenantConfigRepository.getInstance().getPropertiesByTenantId(CurrentTenantIdentifierResolverImpl.getCurrentTenant(), "tenant");
	    	emailMessageSender.sendSimpleMessage(tenantProperties.getProperty("internal.emails.noreply"), tenantProperties.getProperty("internal.emails.tax.upload"),
	                "Tax Upload Report", "Attached is the status of yesterday's tax uploads.", attachments);
    	}
    	catch (Exception e) {
    		LOGGER.error("Problem retrieving CSV for tax report", e);
    	}
    	for (TaxRateLog log : logs) {
    		logBp.delete(log.getId(), TaxRateLog.class);
    	}
    }
}
