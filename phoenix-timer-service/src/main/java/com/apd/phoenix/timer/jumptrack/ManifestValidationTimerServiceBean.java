/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.timer.jumptrack;

import javax.ejb.ScheduleExpression;
import javax.ejb.Singleton;
import javax.ejb.Timer;
import javax.inject.Inject;
import javax.inject.Named;
import org.jboss.msc.service.ServiceName;
import com.apd.phoenix.core.utility.TenantConfigRepository;
import com.apd.phoenix.service.integration.manifest.service.api.ShipManifestService;
import com.apd.phoenix.timer.api.AbstractTimerServiceBean;

/**
 *
 * @author rhc
 */
@Named
@Singleton
public class ManifestValidationTimerServiceBean extends AbstractTimerServiceBean {

    @Inject
    private ShipManifestService shipManifestService;

    @Inject
    TenantConfigRepository tenantConfigRepository;

    @Override
    public ServiceName getServiceName() {
        return ServiceName.JBOSS.append("jumptrack-manifest-validation", "ha", "singleton");
    }

    @Override
    public ScheduleExpression getScheduleExpression() {
        //starts timer
        ScheduleExpression se = new ScheduleExpression();

        se.dayOfMonth(tenantConfigRepository.getProperty("jumptrack.integration", "manifestValidationTimerDayOfMonth"))
                .hour(tenantConfigRepository.getProperty("jumptrack.integration", "manifestValidationTimerHour"))
                .minute(tenantConfigRepository.getProperty("jumptrack.integration", "manifestValidationTimerMin"))
                .second(tenantConfigRepository.getProperty("jumptrack.integration", "manifestValidationTimerSec"));

        return se;
    }

    @Override
    public void execute(Timer timer, String tenant) {
        shipManifestService.verifyUsscoManifests();
    }
}
