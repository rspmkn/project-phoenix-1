package com.apd.phoenix.timer.catalog;

import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.timer.api.AbstractTimerServiceActivator;
import com.apd.phoenix.timer.api.AbstractTimerServiceBean;

/**
 * A Singleton EJB to create the SingletonService during startup.
 * 
 * @author <a href="mailto:wfink@redhat.com">Wolf-Dieter Fink</a>
 */
@Singleton
@Startup
public class CatalogReindexTimerServiceActivator extends AbstractTimerServiceActivator {

    private static final Logger LOGGER = LoggerFactory.getLogger(CatalogReindexTimerServiceActivator.class);

    @EJB
    private CatalogReindexTimerServiceBean service;

    @Override
    protected AbstractTimerServiceBean getServiceBean() {
        return service;
    }
}