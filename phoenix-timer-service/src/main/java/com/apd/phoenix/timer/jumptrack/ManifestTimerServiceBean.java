/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.timer.jumptrack;

import java.util.Properties;
import javax.ejb.ScheduleExpression;
import javax.ejb.Singleton;
import javax.ejb.Timer;
import javax.inject.Inject;
import javax.inject.Named;
import org.jboss.msc.service.ServiceName;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.core.utility.TenantConfigRepository;
import com.apd.phoenix.service.integration.manifest.service.api.ShipManifestService;
import com.apd.phoenix.timer.api.AbstractTimerServiceBean;

/**
 *
 * @author rhc
 */
@Named
@Singleton
public class ManifestTimerServiceBean extends AbstractTimerServiceBean {

    private static final Logger LOG = LoggerFactory.getLogger(ManifestTimerServiceBean.class);

    @Inject
    private ShipManifestService shipManifestService;

    @Inject
    TenantConfigRepository tenantConfigRepository;

    @Override
    public ServiceName getServiceName() {
        return ServiceName.JBOSS.append("jumptrack-manifest", "ha", "singleton");
    }

    @Override
    public ScheduleExpression getScheduleExpression() {

        //starts timer
        ScheduleExpression se = new ScheduleExpression();

        se.dayOfMonth(tenantConfigRepository.getProperty("jumptrack.integration", "manifestTimerDayOfMonth")).hour(
                tenantConfigRepository.getProperty("jumptrack.integration", "manifestTimerHour")).minute(
                tenantConfigRepository.getProperty("jumptrack.integration", "manifestTimerMin")).second(
                tenantConfigRepository.getProperty("jumptrack.integration", "manifestTimerSec"));

        return se;
    }

    @Override
    public void execute(Timer timer, String tenant) {
        try {
            shipManifestService.sendManifestToJumptrack();
        }
        catch (Exception e) {
            LOG.error("Error when processing manifests", e);
        }
    }
}
