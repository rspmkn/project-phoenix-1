package com.apd.phoenix.timer.workflowdelay;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.annotation.Resource;
import javax.ejb.ScheduleExpression;
import javax.ejb.Singleton;
import javax.ejb.Timer;
import javax.ejb.TimerService;
import javax.inject.Inject;
import javax.inject.Named;
import org.apache.commons.lang.StringUtils;
import org.jboss.msc.service.ServiceName;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.business.CommentBp;
import com.apd.phoenix.service.business.OrderProcessLookupBp;
import com.apd.phoenix.service.business.PendingWorkflowCommandBp;
import com.apd.phoenix.service.executor.api.Executor;
import com.apd.phoenix.service.model.CustomerOrder;
import com.apd.phoenix.service.model.PendingWorkflowCommand;
import com.apd.phoenix.timer.api.AbstractTimerServiceBean;

@Named
@Singleton
public class DelayedTimerServiceBean extends AbstractTimerServiceBean {

    @Resource
    TimerService timerService;

    @Inject
    Executor executor;

    @Inject
    PendingWorkflowCommandBp pendingWorkflowCommandBp;

    @Inject
    private OrderProcessLookupBp processLookupBp;

    @Inject
    private CommentBp commentBp;

    private static final Logger LOGGER = LoggerFactory.getLogger(DelayedTimerServiceBean.class);

    @Override
    public void execute(Timer timer, String tenant) {
        List<PendingWorkflowCommand> expiredCommands = pendingWorkflowCommandBp.getExpiredCommands(new Date());
        Set<String> startedProcesses = new HashSet<>();
        for (PendingWorkflowCommand pwCommand : expiredCommands) {
        	//if multiple commands refer to the same process (with the "id" field), it executes one at a time
        	//this is to prevent optimistic lock exceptions and dirty state when one process is being updated 
        	//in multiple places.
        	String processId = null;
        	if (pwCommand != null && pwCommand.getCtx() != null) {
        		processId = pwCommand.getCtx().getData("id").toString();
        	}
        	if (StringUtils.isBlank(processId) || !startedProcesses.contains(processId)) {
                executor.scheduleRequest(pwCommand.getCommandName(), pwCommand.getCtx());
                pwCommand.setExecuted(true);
                pendingWorkflowCommandBp.update(pwCommand);
                startedProcesses.add(processId);
                String logMessage = "Starting workflow command " + pwCommand.getId();
                LOGGER.info(logMessage);
                try {
	                CustomerOrder order = processLookupBp.getOrderFromProcessId(Long.parseLong(processId));
	                commentBp.addSystemComment(order, logMessage);
                }
                catch (NullPointerException|NumberFormatException e) {
                	LOGGER.info("Command wasn't an order command, not creating comment");
                }
        	}
        }
    }

    @Override
    public ServiceName getServiceName() {
        return ServiceName.JBOSS.append("workflow-timer", "ha", "singleton");
    }

    @Override
    public ScheduleExpression getScheduleExpression() {
        ScheduleExpression se = new ScheduleExpression();
        se.hour("*").minute("*").second("0");
        return se;
    }

}
