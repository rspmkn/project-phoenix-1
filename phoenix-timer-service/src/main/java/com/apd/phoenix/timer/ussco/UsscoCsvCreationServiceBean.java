package com.apd.phoenix.timer.ussco;

import java.util.Date;
import java.util.Properties;
import javax.ejb.ScheduleExpression;
import javax.ejb.Singleton;
import javax.ejb.Timer;
import javax.inject.Inject;
import javax.inject.Named;
import org.jboss.ejb3.annotation.TransactionTimeout;
import org.jboss.msc.service.ServiceName;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.core.utility.TenantConfigRepository;
import com.apd.phoenix.service.business.CatalogBp;
import com.apd.phoenix.service.business.CatalogBpNoTransaction;
import com.apd.phoenix.service.model.Catalog;
import com.apd.phoenix.service.persistence.multitenancy.impl.CurrentTenantIdentifierResolverImpl;
import com.apd.phoenix.timer.api.AbstractTimerServiceBean;

@Named
@Singleton
public class UsscoCsvCreationServiceBean extends AbstractTimerServiceBean {

    private static final Logger LOGGER = LoggerFactory.getLogger(UsscoCsvCreationServiceBean.class);

    @Inject
    private CatalogBp catalogBp;

    @Inject
    private CatalogBpNoTransaction catalogBpNoTransaction;

    @TransactionTimeout(value = 3600)
    public void execute(Timer timer, String tenant) {
        LOGGER.debug("Checking uploaded USSCO XML items");
        CurrentTenantIdentifierResolverImpl.setCurrentTenant(TenantConfigRepository.getInstance().getTenantIdByName(
                tenant));
        Catalog catalog = catalogBp.storeXmlCatalogFile();
        if (catalog != null) {
            catalogBpNoTransaction.setScheduledAction(catalog, new Date());
        }
        LOGGER.debug("Finished scheduling uploaded USSCO XML items");
    }

    @Override
    public ServiceName getServiceName() {
        return ServiceName.JBOSS.append("ussco-csv-creation", "ha", "singleton");
    }

    @Override
    public ScheduleExpression getScheduleExpression() {

        Properties properties = TenantConfigRepository.getInstance().getProperties("catalog.upload.integration");

        ScheduleExpression se = new ScheduleExpression();
        se.hour(properties.getProperty("usscoXmlTimerHour")).minute(properties.getProperty("usscoXmlTimerMinute"))
                .second(properties.getProperty("usscoXmlTimerSecond"));
        return se;
    }
}
