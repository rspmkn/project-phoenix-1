package com.apd.phoenix.timer.smartSearch;

import java.util.List;
import java.util.concurrent.TimeUnit;
import javax.ejb.AccessTimeout;
import javax.ejb.ScheduleExpression;
import javax.ejb.Singleton;
import javax.ejb.Timer;
import javax.inject.Inject;
import javax.inject.Named;
import org.jboss.ejb3.annotation.TransactionTimeout;
import org.jboss.msc.service.ServiceName;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.business.CatalogBp;
import com.apd.phoenix.timer.api.AbstractTimerServiceBean;

@Singleton
@Named
@TransactionTimeout(1200)
public class SmartSearchCatalogHashTimerServiceBean extends AbstractTimerServiceBean {

    private static final Logger LOGGER = LoggerFactory.getLogger(SmartSearchCatalogHashTimerServiceBean.class);

    @Inject
    private CatalogBp catalogBp;

    @Override
    public ServiceName getServiceName() {
        return ServiceName.JBOSS.append("SmartSearchCatalogHash", "poll", "hasingleton");
    }

    @Override
    public ScheduleExpression getScheduleExpression() {
        ScheduleExpression se = new ScheduleExpression();
        // normal value
        se.hour("*").minute("20").second("0");
        //test value
        //se.hour("*").minute("*").second("0");
        return se;
    }

    @Override
    @AccessTimeout(unit = TimeUnit.HOURS, value = 2)
    public void execute(Timer timer, String tenant) {
        LOGGER.info("Getting catalogs whose hashes might have changed");
        List<Long> catalogsToRehash = catalogBp.getCatalogsToRehash();
        LOGGER.info("{} catalogs to rehash", catalogsToRehash.size());
        for (Long catalogId : catalogsToRehash) {
            catalogBp.setNewHashOnCatalog(catalogId);
        }
    }
}
