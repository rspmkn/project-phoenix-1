package com.apd.phoenix.service.rest.shipto;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.business.CustomerOrderRequestBp;
import com.apd.phoenix.service.executor.command.api.BRMSCommandCreator;
import com.apd.phoenix.service.executor.command.api.SignalCommand;
import com.apd.phoenix.service.model.CustomerOrder;

//TODO: this is in the internal "phoenix-rest-service" project, but it is publicly accessible
//through an email URL (see RequestShipToWorkItemHandler), so this should be changed to a
//POST endpoint, with a GET endpoint added to phoenix-integration-ws to call the internal
//POST endpoint.
@Path("/shiptos")
@Stateless
public class ShipToService {

    private static final Logger LOG = LoggerFactory.getLogger(ShipToService.class);

    @Inject
    CustomerOrderRequestBp customerOrderRequestBp;

    @Inject
    BRMSCommandCreator brmsCommandCreator;

    @GET
    @Path("/{token}/workflow")
    public Response approveShipTo(@PathParam("token") String token, @QueryParam("action") String action,
            @QueryParam("id") String id) {

        CustomerOrder customerOrder = customerOrderRequestBp.getCustomerOrderByToken(token);

        Long orderId = customerOrder.getId();

        LOG.info("Approving ship to for order: {}", orderId);

        Long wiid;
        String response;

        try {
            wiid = Long.parseLong(id, 10);

            if (customerOrder != null && !customerOrder.hasAllowedStatus()) {
                LOG.warn("An attempt was made to place an order with the INVALIDATED or MANUALLY_RESOLVED status");
                if (customerOrder.getApdPo() != null && StringUtils.isNotBlank(customerOrder.getApdPo().getValue())) {
                    LOG.warn("Disallowed PO: " + customerOrder.getApdPo().getValue());
                }
            }
            else {
                brmsCommandCreator.sendCompleteWorkItem(SignalCommand.Type.ORDER, orderId, wiid, null, null, null);
            }
            response = "<html><body><p>The order has been approved and submitted for processing. Thank you.</p></body></html>";

        }
        catch (Exception e) {
            LOG.info("Error creating long");
            response = "<html><body><p>There was an error processing your request.</p></body></html>";
            return Response.status(500).entity(response).type(MediaType.TEXT_HTML).build();
        }

        return Response.ok(response).build();
    }

    @GET
    @Path("/test")
    public Response testResponse() {
        String response = "<html><body><p>This is a test</p></body></html>";
        return Response.ok(response).build();
    }
}
