package com.apd.phoenix.service.rest.workflow;

import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.model.dto.AdvanceShipmentNoticeDto;
import com.apd.phoenix.service.model.dto.POAcknowledgementDto;
import com.apd.phoenix.service.model.dto.PurchaseOrderDto;
import com.apd.phoenix.service.model.dto.TransactionMetadataDto;
import com.apd.phoenix.service.model.dto.VendorCreditInvoiceDto;
import com.apd.phoenix.service.model.dto.VendorInvoiceDto;
import com.apd.phoenix.service.persistence.jpa.SequenceDao;
import com.apd.phoenix.service.persistence.jpa.SequenceDao.Sequence;
import com.apd.phoenix.service.workflow.WorkflowService;

@Path("/workflow/order")
@Stateless
public class OrderWorkflowService {

    private static final Logger LOG = LoggerFactory.getLogger(OrderWorkflowService.class);

    @Inject
    private WorkflowService workflowService;

    @Inject
    private SequenceDao sequenceDao;

    @Path("/transaction")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getTransaction() {

        LOG.info("getTransaction called");
        long transactionId = sequenceDao.nextVal(Sequence.TRANSACTION_CONTROL_ID);
        long groupId = sequenceDao.nextVal(Sequence.GROUP_CONTROL_ID);
        long interchangeId = sequenceDao.nextVal(Sequence.INTERCHANGE_CONTROL_ID);
        TransactionMetadataDto transactionMetadataDto = new TransactionMetadataDto();
        transactionMetadataDto.setTransactionId(transactionId);
        transactionMetadataDto.setGroupId(groupId);
        transactionMetadataDto.setInterchangeId(interchangeId);
        return Response.ok(transactionMetadataDto).build();
    }

    @Path("/purchase-order-request")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response purchaseOrderRequests(List<PurchaseOrderDto> purchaseOrderDtos) {

        LOG.info("recieving purchaseOrderDto");
        for (PurchaseOrderDto purchaseOrderDto : purchaseOrderDtos) {
            workflowService.processOrder(purchaseOrderDto);
        }
        return Response.ok().build();
    }

    @Path("/invoice")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response invoice(VendorInvoiceDto invoiceDto) {

        LOG.info("Sending invoiceDto.");
        workflowService.processInvoice(invoiceDto);
        return Response.ok().build();
    }

    @POST
    @Path("/creditInvoice")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response createInvoice(VendorCreditInvoiceDto invoiceDto) {

        LOG.info("Sending creditInvoiceDto.");
        workflowService.processVendorCredit(invoiceDto);
        return Response.ok().build();
    }

    @Path("/po-acknowledgement")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response pOAcknowledgement(POAcknowledgementDto poAcknowledgementDto) {

        LOG.info("Sending po ack to workflow");
        workflowService.processOrderAcknowledgement(poAcknowledgementDto);
        return Response.ok().build();
    }

    @Path("/advanced-shipment-notice")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response advancedShipment(AdvanceShipmentNoticeDto advanceShipmentNoticeDto) {

        LOG.info("Sending po ack to workflow");
        workflowService.processAdvancedShipmentNotice(advanceShipmentNoticeDto);
        return Response.ok().build();
    }
}
