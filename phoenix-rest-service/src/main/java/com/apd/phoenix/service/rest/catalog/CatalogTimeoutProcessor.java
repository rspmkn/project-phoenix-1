package com.apd.phoenix.service.rest.catalog;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import com.apd.phoenix.service.business.CatalogBpNoTransaction;

@Stateless
@Path("/catalogTimeouts")
public class CatalogTimeoutProcessor {

    @Inject
    private CatalogBpNoTransaction catalogBp;

    /**
     * This method is a rest endpoint that calls CatalogBpNoTransaction to execute all
     * scheduled catalog CSV actions
     * 
     * @param catalogChanges
     * @return
     */
    @POST
    @Consumes("*/*")
    @Produces("application/xml")
    @Path("/executeScheduledActions")
    public Response executeScheduledActions() {
        catalogBp.executeScheduledActions();
        return Response.ok().build();
    }

    /**
     * This method is a rest endpoint that starts the scheduled catalog CSV generation
     * 
     * @param catalogChanges
     * @return
     */
    @POST
    @Consumes("*/*")
    @Produces("application/xml")
    @Path("/scheduledCsvRegeneration")
    public Response scheduledCsvRegeneration() {
        catalogBp.regenerateScheduledCatalogCsvs();
        catalogBp.regenerateScheduledSmartOci();
        return Response.ok().build();
    }

    /**
     * This method is a rest endpoint that starts the scheduled catalog reindexing
     * 
     * @param catalogChanges
     * @return
     */
    @POST
    @Consumes("*/*")
    @Produces("application/xml")
    @Path("/scheduledReindex")
    public Response scheduledReindex() {
        catalogBp.reindexScheduledCatalogs();
        return Response.ok().build();
    }
}
