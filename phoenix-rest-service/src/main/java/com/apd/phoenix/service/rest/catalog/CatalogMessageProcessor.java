package com.apd.phoenix.service.rest.catalog;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import com.apd.phoenix.service.business.CatalogBp;
import com.apd.phoenix.service.model.Catalog;
import com.apd.phoenix.service.persistence.multitenancy.impl.CurrentTenantIdentifierResolverImpl;

@Stateless
@Path("/catalogMessages")
public class CatalogMessageProcessor {

    @Inject
    private CatalogBp catalogBp;

    /**
     * This method is a rest endpoint that takes a list of catalog changes and processes them. 
     * This is a rest endpoint, rather than a bean, since Camel .bean() endpoints don't respect 
     * transaction annotations (such as the one on the syncItem methods in ItemBp and CatalogXItemBp).
     * 
     * @param catalogChanges
     * @return
     */
    @POST
    @Consumes("*/*")
    @Produces("application/xml")
    @Path("/smartSearchInsert")
    public Response smartSearchInsert(CatalogBp.SmartSearchInsertMessage insertMessage) {
        Catalog catalog = null;
        if (insertMessage.getCatalogId() != null) {
            catalog = this.catalogBp.findById(insertMessage.getCatalogId(), Catalog.class);
        }
        if (catalog != null && insertMessage.getListName() != null && insertMessage.getCoreListName() != null) {
            this.catalogBp.processSmartSearchList(catalog, insertMessage.getListName(),
                    insertMessage.getCoreListName(), CurrentTenantIdentifierResolverImpl.getCurrentTenant());
        }
        return Response.ok().build();
    }
}
