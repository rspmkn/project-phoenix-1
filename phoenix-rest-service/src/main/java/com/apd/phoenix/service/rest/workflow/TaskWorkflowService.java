package com.apd.phoenix.service.rest.workflow;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.apache.commons.lang.StringUtils;
import org.kie.api.task.model.Status;
import org.kie.api.task.model.Task;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.brms.core.workflow.PhoenixTaskSummary;
import com.apd.phoenix.service.brms.core.workflow.UserTaskService;
import com.apd.phoenix.service.business.CustomerOrderBp;
import com.apd.phoenix.service.model.CustomerOrder;
import com.apd.phoenix.service.utility.SendInternalHttpRequest;
import com.apd.phoenix.service.workflow.RemoteTaskService.RemoteTaskSummary;

@Path("/workflow/task")
@Stateless
public class TaskWorkflowService {

    private static final Logger LOG = LoggerFactory.getLogger(TaskWorkflowService.class);

    @Inject
    private UserTaskService userTaskService;

    @Inject
    private CustomerOrderBp orderBp;

    @Path("/assigned/{user}")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAssigned(@PathParam("user") String user) {
    	LOG.info("getAssigned tasks called");
    	if (StringUtils.isBlank(user)) {
    		return Response.ok().build();
    	}
    	List<PhoenixTaskSummary> tasks = userTaskService.getAssignedTasks(user, false);
    	List<RemoteTaskSummary> toReturn = new ArrayList<>();
    	for (PhoenixTaskSummary task : tasks) {
    		RemoteTaskSummary newSummary = new RemoteTaskSummary();
    		newSummary.setId(task.getId());
    		newSummary.setName(task.getName());
    		if (task.getActualOwner() != null) {
    			newSummary.setOwner(task.getActualOwner().getId());
    		}
    		newSummary.setDescription(task.getDescription());
    		newSummary.setCreationDate(task.getCreatedOn());
    		boolean canBeWorked = (task.getStatus() != null && (task.getStatus().equals(Status.Created) || task.getStatus().equals(Status.InProgress) || task.getStatus().equals(Status.Ready) || task.getStatus().equals(Status.Reserved)));
    		newSummary.setCanBeWorked(canBeWorked);
    		toReturn.add(newSummary);
    	}
    	return Response.ok(toReturn).build();
    }

    @Path("/taskContent/{taskId}")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response getTaskContent(@PathParam("taskId") Long taskId) {
        LOG.info("getTaskContent called");
        if (taskId == null) {
            return Response.ok().build();
        }
        return Response.ok(userTaskService.getTaskContent(taskId)).build();
    }

    @Path("/orderTasks/{apdPo}/{taskName}")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response getOrderTasks(@PathParam("apdPo") String apdPo, @PathParam("taskName") String taskName) {
    	LOG.info("getOrderTasks called");
    	if (StringUtils.isBlank(apdPo) || StringUtils.isBlank(taskName)) {
    		return Response.ok().build();
    	}
		CustomerOrder order = orderBp.searchByApdPo(apdPo);
		List<Long> toReturn = new ArrayList<>();
		for (PhoenixTaskSummary task : userTaskService.getOrderTasks(order)) {
			if (task != null && StringUtils.isNotBlank(task.getName()) && task.getName().equals(taskName)) {
				toReturn.add(task.getId());
			}
		}
    	return Response.ok(toReturn).build();
    }

    /**
     * This method takes a task and a user, and ensures that the task is assigned to the user and 
     * has the correct status, before being completed.
     * 
     * @param taskId
     * @param user
     * @return
     */
    @Path("/startStatus/{taskId}/{user}")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response startStatus(@PathParam("taskId") Long taskId, @PathParam("user") String user) {
        LOG.info("startStatus called");
        Task task = userTaskService.getTask(taskId);
        if (task.getTaskData().getStatus() == Status.Suspended) {
            LOG.info("---------resuming as " + user);
            userTaskService.claimTask(taskId, user);
            task = userTaskService.getTask(taskId);
        }
        if (task.getTaskData().getStatus() == Status.Ready || task.getTaskData().getStatus() == Status.Reserved) {
            //Only attempt to start the task if it can be started
            LOG.info("---------starting as " + user);
            userTaskService.startTask(taskId, user);
            task = userTaskService.getTask(taskId);
        }
        return Response.ok().build();
    }
}
