package com.apd.phoenix.service.payment.ws.client.authorize.capture;

import com.apd.phoenix.service.payment.ws.AuthorizeAndCaptureParams;
import com.apd.phoenix.service.payment.ws.AvsResponse;
import com.apd.phoenix.service.payment.ws.CardSecurityCodeIndicator;
import com.apd.phoenix.service.payment.ws.CreditCardTransaction;
import com.apd.phoenix.service.payment.ws.CreditCardTransactionAuthorizationResult;
import com.apd.phoenix.service.payment.ws.CreditCardTransactionCreditCard;
import com.apd.phoenix.service.payment.ws.ICreditCardTransactionAuthorizeAndCaptureApplicationFaultFaultFaultMessage;
import com.apd.phoenix.service.payment.ws.StoredCardIdentifier;
import com.apd.phoenix.service.payment.ws.client.PaymentGatewayBaseTest;
import static com.apd.phoenix.service.payment.ws.client.PaymentGatewayTestConstants.validMasterCardNumber;
import static com.apd.phoenix.service.payment.ws.client.PaymentGatewayTestConstants.validVisaCardNumber;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import static org.testng.Assert.assertEquals;
import org.testng.annotations.Test;

/**
 *
 * @author dnorris
 */
public class AddressVerificationTest extends PaymentGatewayBaseTest {

    private static final Logger logger = LoggerFactory.getLogger(AddressVerificationTest.class);

    @Test(dependsOnMethods = { "addCustomerToVault" })
    public void avsDenial() {
        StoredCardIdentifier invalidAVSCard = getInvalidAVSCardIdentifier(validVisaCardNumber);
        CreditCardTransactionAuthorizationResult result = AVSDenialAuthorizationRequestTest(invalidAVSCard);
        assertEquals(result.getAddressAvsResponse(), AvsResponse.NOT_MATCHED);
    }

    @Test(dependsOnMethods = { "addCustomerToVault" })
    public void avsMatched() {
        StoredCardIdentifier card = getStoredCardIdentifier(validMasterCardNumber);
        CreditCardTransactionAuthorizationResult result = validCardAuthorizeAndCaptureTest(card);
        assertEquals(result.getAddressAvsResponse(), AvsResponse.MATCHED);
    }

    public CreditCardTransactionAuthorizationResult validCardAuthorizeAndCaptureTest(StoredCardIdentifier creditCard) {
        try {
            AuthorizeAndCaptureParams params = new AuthorizeAndCaptureParams();
            CreditCardTransaction creditCardTransaction = createValidCreditCardTransaction(creditCard);
            CreditCardTransactionCreditCard cardMetaData = factory.createCreditCardTransactionCreditCard();
            cardMetaData.setCardSecurityCodeIndicator(CardSecurityCodeIndicator.PROVIDED);
            cardMetaData.setCardSecurityCode(factory.createCreditCardTransactionCreditCardCardSecurityCode("124"));
            creditCardTransaction.setCreditCard(factory.createCreditCardTransactionCreditCard1(cardMetaData));
            params.setCreditCardTransaction(factory
                    .createAuthorizeAndCaptureParamsCreditCardTransaction(creditCardTransaction));
            params.setTerminalIdentifier(factory.createAuthorizeAndCaptureParamsTerminalIdentifier(terminalIdentifier));
            CreditCardTransactionAuthorizationResult result = port.authorizeAndCapture(clientCredentials, params);
            if (!result.isSucceeded()) {
                if (result.getValidationFailures().getValue() != null) {
                    List<String> failureReasons = result.getValidationFailures().getValue().getString();
                    for (String failureDetail : failureReasons) {
                        logger.error(failureDetail);
                    }
                }
            }
            return result;
        }
        catch (ICreditCardTransactionAuthorizeAndCaptureApplicationFaultFaultFaultMessage ex) {
            logger.error(null, ex);
        }
        return null;
    }

    public CreditCardTransactionAuthorizationResult AVSDenialAuthorizationRequestTest(
            StoredCardIdentifier invalidAddressCard) {
        try {
            AuthorizeAndCaptureParams params = new AuthorizeAndCaptureParams();
            CreditCardTransaction creditCardTransaction = createValidCreditCardTransaction(invalidAddressCard);

            CreditCardTransactionCreditCard cardMetaData = factory.createCreditCardTransactionCreditCard();
            cardMetaData.setCardSecurityCodeIndicator(CardSecurityCodeIndicator.PROVIDED);
            cardMetaData.setCardSecurityCode(factory.createCreditCardTransactionCreditCardCardSecurityCode("124"));
            creditCardTransaction.setCreditCard(factory.createCreditCardTransactionCreditCard1(cardMetaData));
            params.setCreditCardTransaction(factory
                    .createAuthorizeAndCaptureParamsCreditCardTransaction(creditCardTransaction));
            params.setTerminalIdentifier(factory.createAuthorizeAndCaptureParamsTerminalIdentifier(terminalIdentifier));
            CreditCardTransactionAuthorizationResult result = port.authorizeAndCapture(clientCredentials, params);
            return result;
        }
        catch (ICreditCardTransactionAuthorizeAndCaptureApplicationFaultFaultFaultMessage ex) {
            logger.error(null, ex);
        }
        return null;
    }

}
