package com.apd.phoenix.service.payment.ws.client;

import java.io.IOException;
import java.io.InputStream;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PaymentGatewayTestPropertiesLoader {

    private static Logger logger = LoggerFactory.getLogger(PaymentGatewayTestPropertiesLoader.class);
    private PropertiesConfiguration paymentGatewayProperties;
    private static final PaymentGatewayTestPropertiesLoader singleton = new PaymentGatewayTestPropertiesLoader();

    public static PaymentGatewayTestPropertiesLoader getInstance() {
        return singleton;
    }

    private PaymentGatewayTestPropertiesLoader() {
        
        try (InputStream is = PaymentGatewayTestPropertiesLoader.class.getClassLoader().getResourceAsStream(
                "properties/TestPaymentGateway.properties")) {
            paymentGatewayProperties = new PropertiesConfiguration();
            paymentGatewayProperties.load(is);
        }
        catch (ConfigurationException | IOException ex) {
            logger.error("Could not load properties file", ex);
        }
    }

    public PropertiesConfiguration getProperties() {
        return paymentGatewayProperties;
    }
}
