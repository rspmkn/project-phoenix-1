/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.service.payment.ws.client.capture;

import com.apd.phoenix.service.payment.ws.AuthorizeParams;
import com.apd.phoenix.service.payment.ws.CaptureParams;
import com.apd.phoenix.service.payment.ws.CreditCardTransaction;
import com.apd.phoenix.service.payment.ws.CreditCardTransactionAuthorizationResult;
import com.apd.phoenix.service.payment.ws.CreditCardTransactionCreditCard;
import com.apd.phoenix.service.payment.ws.CreditCardTransactionResult;
import com.apd.phoenix.service.payment.ws.ICreditCardTransactionAuthorizeApplicationFaultFaultFaultMessage;
import com.apd.phoenix.service.payment.ws.ICreditCardTransactionCaptureApplicationFaultFaultFaultMessage;
import com.apd.phoenix.service.payment.ws.StoredCardIdentifier;
import com.apd.phoenix.service.payment.ws.client.PaymentGatewayBaseTest;
import static com.apd.phoenix.service.payment.ws.client.PaymentGatewayTestConstants.validAmericanExpressNumber;
import static com.apd.phoenix.service.payment.ws.client.PaymentGatewayTestConstants.validDinersCardNumber;
import static com.apd.phoenix.service.payment.ws.client.PaymentGatewayTestConstants.validDiscoverCardNumber;
import static com.apd.phoenix.service.payment.ws.client.PaymentGatewayTestConstants.validJCBNumber;
import static com.apd.phoenix.service.payment.ws.client.PaymentGatewayTestConstants.validMasterCardNumber;
import static com.apd.phoenix.service.payment.ws.client.PaymentGatewayTestConstants.validVisaCardNumber;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import static org.testng.Assert.assertTrue;
import org.testng.annotations.Test;

/**
 *
 * @author dnorris
 */
public class CaptureTests extends PaymentGatewayBaseTest {

    private static final Logger logger = LoggerFactory.getLogger(CaptureTests.class);
    protected CreditCardTransaction creditCardTransaction;
    protected StoredCardIdentifier creditCard;

    @Test(dependsOnMethods = { "addCustomerToVault" })
    public void americanExpress() {
        StoredCardIdentifier card = getStoredCardIdentifier(validAmericanExpressNumber);
        assertTrue(validCapture(card).isSucceeded());
    }

    @Test(dependsOnMethods = { "addCustomerToVault" })
    public void jcb() {
        StoredCardIdentifier card = getStoredCardIdentifier(validJCBNumber);
        assertTrue(validCapture(card).isSucceeded());
    }

    @Test(dependsOnMethods = { "addCustomerToVault" })
    public void discover() {
        StoredCardIdentifier card = getStoredCardIdentifier(validDiscoverCardNumber);
        assertTrue(validCapture(card).isSucceeded());
    }

    @Test(dependsOnMethods = { "addCustomerToVault" })
    public void diners() {
        StoredCardIdentifier card = getStoredCardIdentifier(validDinersCardNumber);
        assertTrue(validCapture(card).isSucceeded());
    }

    @Test(dependsOnMethods = { "addCustomerToVault" })
    public void masterCard() {
        StoredCardIdentifier card = getStoredCardIdentifier(validMasterCardNumber);
        assertTrue(validCapture(card).isSucceeded());
    }

    @Test(dependsOnMethods = { "addCustomerToVault" })
    public void visa() {
        StoredCardIdentifier card = getStoredCardIdentifier(validVisaCardNumber);
        assertTrue(validCapture(card).isSucceeded());
    }

    public void preAuthorization(StoredCardIdentifier creditCard) {
        try {
            AuthorizeParams params = factory.createAuthorizeParams();
            creditCardTransaction = createValidCreditCardTransaction(creditCard);
            params.setCreditCardTransaction(factory.createAuthorizeParamsCreditCardTransaction(creditCardTransaction));
            params.setTerminalIdentifier(factory.createAuthorizeParamsTerminalIdentifier(terminalIdentifier));
            CreditCardTransactionAuthorizationResult result = port.authorize(clientCredentials, params);

            if (!result.isSucceeded()) {
                if (result.getValidationFailures().getValue() != null) {
                    List<String> failureReasons = result.getValidationFailures().getValue().getString();
                    for (String failureDetail : failureReasons) {
                        logger.error(failureDetail);
                    }
                }
            }
            assertTrue(result.isSucceeded());
        }
        catch (ICreditCardTransactionAuthorizeApplicationFaultFaultFaultMessage ex) {
            logger.error("ICreditCardTransactionVoidApplicationFaultFaultFaultMessage caught", ex);
        }
    }

    public CreditCardTransactionResult validCapture(StoredCardIdentifier creditCard) {
        try {
            preAuthorization(creditCard);
            CaptureParams params = new CaptureParams();
            params.setCreditCardTransaction(factory
                    .createAuthorizeAndCaptureParamsCreditCardTransaction(creditCardTransaction));
            params.setTerminalIdentifier(factory.createAuthorizeAndCaptureParamsTerminalIdentifier(terminalIdentifier));
            CreditCardTransactionResult result = port.capture(clientCredentials, params);
            if (!result.isSucceeded()) {
                if (result.getValidationFailures().getValue() != null) {
                    List<String> failureReasons = result.getValidationFailures().getValue().getString();
                    for (String failureDetail : failureReasons) {
                        logger.error(failureDetail);
                    }
                }
            }
            return result;
        }
        catch (ICreditCardTransactionCaptureApplicationFaultFaultFaultMessage ex) {
            logger.error(null, ex);
        }
        return null;
    }
}
