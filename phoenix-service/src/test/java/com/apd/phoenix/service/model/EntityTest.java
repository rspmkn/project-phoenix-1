package com.apd.phoenix.service.model;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.JavaArchive;

public class EntityTest {

    @Deployment
    public static JavaArchive createDeployment() {
        return ShrinkWrap.create(JavaArchive.class, "test.jar").addPackage("com.apd.phoenix.service.model")
                .addAsManifestResource("test-persistence.xml", "persistence.xml").addAsManifestResource(
                        EmptyAsset.INSTANCE, "beans.xml");
    }
}
