package com.apd.phoenix.service.payment.ws.client;

import com.apd.phoenix.service.card.management.AddStoredCardParams;
import com.apd.phoenix.service.card.management.Address;
import com.apd.phoenix.service.card.management.Contact;
import com.apd.phoenix.service.card.management.CountryCode;
import com.apd.phoenix.service.card.management.CreateCustomerParams;
import com.apd.phoenix.service.card.management.CreditCardManagementFailureReason;
import com.apd.phoenix.service.card.management.CreditCardManagementResult;
import com.apd.phoenix.service.card.management.CreditCardManagementService;
import com.apd.phoenix.service.card.management.Customer;
import com.apd.phoenix.service.card.management.CustomerIdentifier;
import com.apd.phoenix.service.card.management.DeleteCustomerParams;
import com.apd.phoenix.service.card.management.GetTokenForCardNumberParams;
import com.apd.phoenix.service.card.management.ICreditCardManagementAddStoredCreditCardApplicationFaultFaultFaultMessage;
import com.apd.phoenix.service.card.management.ICreditCardManagementCreateCustomerApplicationFaultFaultFaultMessage;
import com.apd.phoenix.service.card.management.ICreditCardManagementGetTokenForCardNumberApplicationFaultFaultFaultMessage;
import com.apd.phoenix.service.card.management.LocationIdentifier;
import com.apd.phoenix.service.card.management.StoredCreditCard;
import com.apd.phoenix.service.card.management.TokenCreditCardManagementResult;
import com.apd.phoenix.service.payment.ws.ArrayOfLineItemDetail;
import com.apd.phoenix.service.payment.ws.ClientCredentials;
import com.apd.phoenix.service.payment.ws.CreditCardTransaction;
import com.apd.phoenix.service.payment.ws.CreditCardTransactionService;
import com.apd.phoenix.service.payment.ws.CurrencyCode;
import com.apd.phoenix.service.payment.ws.ICreditCardTransaction;
import com.apd.phoenix.service.payment.ws.LineItemDetailPurchasing;
import com.apd.phoenix.service.payment.ws.ObjectFactory;
import com.apd.phoenix.service.payment.ws.StoredCardIdentifier;
import com.apd.phoenix.service.payment.ws.TerminalIdentifier;
import static com.apd.phoenix.service.payment.ws.client.PaymentGatewayObjectFactory.createXMLGregorianCalendar;
import static com.apd.phoenix.service.payment.ws.client.PaymentGatewayTestConstants.CUSTOMER_CODE;
import static com.apd.phoenix.service.payment.ws.client.PaymentGatewayTestConstants.clientCode;
import static com.apd.phoenix.service.payment.ws.client.PaymentGatewayTestConstants.locationCode;
import static com.apd.phoenix.service.payment.ws.client.PaymentGatewayTestConstants.merchantCode;
import static com.apd.phoenix.service.payment.ws.client.PaymentGatewayTestConstants.password;
import static com.apd.phoenix.service.payment.ws.client.PaymentGatewayTestConstants.terminalCode;
import static com.apd.phoenix.service.payment.ws.client.PaymentGatewayTestConstants.totalAmount;
import static com.apd.phoenix.service.payment.ws.client.PaymentGatewayTestConstants.username;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertTrue;
import org.testng.annotations.BeforeGroups;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

/**
 *
 * @author dnorris
 */
public class PaymentGatewayBaseTest {

    private static final Logger logger = LoggerFactory.getLogger(PaymentGatewayBaseTest.class);

    private static final String DEFAULT_SHIP_FROM_POSTAL_CODE = "28273";

    private CreditCardTransactionService ss;

    protected ICreditCardTransaction port;
    protected com.apd.phoenix.service.card.management.ICreditCardManagement cardManagementPort;
    protected static final ObjectFactory factory = new ObjectFactory();
    protected CreditCardManagementService cardManagementService;
    protected static final com.apd.phoenix.service.card.management.ObjectFactory cardManagerFactory = new com.apd.phoenix.service.card.management.ObjectFactory();
    protected ClientCredentials clientCredentials;
    protected com.apd.phoenix.service.card.management.ClientCredentials cardManagementClientCredentials;
    protected TerminalIdentifier terminalIdentifier;
    protected LocationIdentifier locationIdentifier;
    protected Customer customer;
    protected CreateCustomerParams customerParams;

    @BeforeSuite
    public void setup() {
        ss = new CreditCardTransactionService();
        port = ss.getCreditCardTransationService();
        initClientCredentials();
        initTerminalIdentifier();
        cardManagementService = new CreditCardManagementService();
        cardManagementPort = cardManagementService.getCreditCardManagementService();
        initCardManagementClientCredentials();
        initLocationIdentifier();
        initCustomer();
        initCustomerParams();
    }

    @BeforeGroups(groups = { "initializationGroup" })
    public void cleanUpBeforeTest() throws Exception {
        CustomerIdentifier customerIdentifier = cardManagerFactory.createCustomerIdentifier();
        customerIdentifier.setCustomerCode(cardManagerFactory.createCustomerIdentifierCustomerCode(CUSTOMER_CODE));
        customerIdentifier.setMerchantCode(cardManagerFactory.createCustomerIdentifierMerchantCode(merchantCode));
        customerIdentifier.setLocationCode(cardManagerFactory.createCustomerIdentifierLocationCode(locationCode));
        DeleteCustomerParams deleteCustomerParams = cardManagerFactory.createDeleteCustomerParams();
        deleteCustomerParams.setCustomerIdentifier(cardManagerFactory
                .createDeleteCustomerParamsCustomerIdentifier(customerIdentifier));
        CreditCardManagementResult result = cardManagementPort.deleteCustomer(cardManagementClientCredentials,
                deleteCustomerParams);
        assertNotNull(result);
    }

    @Test(groups = { "initializationGroup" })
    public void addCustomerToVault() throws ICreditCardManagementCreateCustomerApplicationFaultFaultFaultMessage {
        CreditCardManagementResult result = cardManagementPort.createCustomer(cardManagementClientCredentials,
                customerParams);
        if (!result.getSucceeded()) {
            if (result.getValidationFailures().getValue() != null) {
                List<String> failureReasons = result.getValidationFailures().getValue().getString();
                for (String failureDetail : failureReasons) {
                    logger.error(failureDetail);
                }
            }
        }
        assertTrue(result.getSucceeded());
    }

    private void initCardManagementClientCredentials() {
        cardManagementClientCredentials = cardManagerFactory.createClientCredentials();
        cardManagementClientCredentials.setClientCode(cardManagerFactory.createClientCredentialsClientCode(clientCode));
        cardManagementClientCredentials.setUserName(cardManagerFactory.createClientCredentialsUserName(username));
        cardManagementClientCredentials.setPassword(cardManagerFactory.createClientCredentialsPassword(password));
    }

    private void initLocationIdentifier() {
        locationIdentifier = cardManagerFactory.createLocationIdentifier();
        locationIdentifier.setLocationCode(cardManagerFactory.createLocationIdentifierLocationCode(locationCode));
        locationIdentifier.setMerchantCode(cardManagerFactory.createLocationIdentifierMerchantCode(merchantCode));
    }

    private void initClientCredentials() {
        clientCredentials = PaymentGatewayObjectFactory.createClientCredentials(clientCode, username, password);
    }

    private void initTerminalIdentifier() {
        terminalIdentifier = PaymentGatewayObjectFactory.createTerminalIdentifier(locationCode, merchantCode,
                terminalCode);
    }

    protected void initCustomer() {
        customer = cardManagerFactory.createCustomer();
        customer.setBillingAddress(cardManagerFactory.createCustomerBillingAddress(getValidAddress()));
        customer.setName(cardManagerFactory.createCustomerName("Bob's Hardware"));
        customer.setCode(cardManagerFactory.createCustomerCode(CUSTOMER_CODE));
    }

    protected void initCustomerParams() {
        customerParams = cardManagerFactory.createCreateCustomerParams();
        customerParams.setLocationIdentifier(cardManagerFactory
                .createCreateCustomerParamsLocationIdentifier(locationIdentifier));
        customerParams.setCustomer(cardManagerFactory.createCreateCustomerParamsCustomer(customer));
    }

    protected StoredCardIdentifier getStoredCardIdentifier(String cardNumber) {
        StoredCreditCard storedCreditCard = createStoredCreditCard(cardNumber);

        StoredCardIdentifier storedCardIdentifier = factory.createStoredCardIdentifier();
        storedCardIdentifier.setCustomerCode(factory.createStoredCardIdentifierCustomerCode(CUSTOMER_CODE));
        storedCardIdentifier.setToken(factory.createStoredCardIdentifierToken(storeOrRetrieveCard(storedCreditCard)));
        return storedCardIdentifier;
    }

    protected StoredCardIdentifier getInvalidAVSCardIdentifier(String cardNumber) {
        StoredCreditCard storedCreditCard = createInvalidAddressStoredCreditCard(cardNumber);

        StoredCardIdentifier storedCardIdentifier = factory.createStoredCardIdentifier();
        storedCardIdentifier.setCustomerCode(factory.createStoredCardIdentifierCustomerCode(CUSTOMER_CODE));
        storedCardIdentifier.setToken(factory.createStoredCardIdentifierToken(storeOrRetrieveCard(storedCreditCard)));
        return storedCardIdentifier;
    }

    private String storeOrRetrieveCard(StoredCreditCard storedCreditCard) {
        String vaultToken = null;
        try {
            logger.debug("Attempting to Store Card in Vault");
            AddStoredCardParams addStoredCardParams;
            addStoredCardParams = cardManagerFactory.createAddStoredCardParams();
            CustomerIdentifier customerIdentifier = cardManagerFactory.createCustomerIdentifier();
            customerIdentifier.setCustomerCode(cardManagerFactory.createCustomerIdentifierCustomerCode(CUSTOMER_CODE));
            customerIdentifier.setMerchantCode(cardManagerFactory.createCustomerIdentifierMerchantCode(merchantCode));
            customerIdentifier.setLocationCode(cardManagerFactory.createCustomerIdentifierLocationCode(locationCode));

            addStoredCardParams.setCustomerIdentifier(cardManagerFactory
                    .createAddStoredCardParamsCustomerIdentifier(customerIdentifier));
            addStoredCardParams.setCreditCard(cardManagerFactory.createAddStoredCardParamsCreditCard(storedCreditCard));
            TokenCreditCardManagementResult result = cardManagementPort.addStoredCreditCard(
                    cardManagementClientCredentials, addStoredCardParams);
            if (!result.getSucceeded()) {
                if (result.getFailureReason().equals(CreditCardManagementFailureReason.CARD_NUMBER_IN_USE)) {
                    logger.debug("Card is already stored in vault.");
                    vaultToken = getTokenForExistingCard(storedCreditCard.getCardAccountNumber().getValue());
                }
                else {
                    if (result.getValidationFailures().getValue() != null) {
                        List<String> failureReasons = result.getValidationFailures().getValue().getString();
                        for (String failureDetail : failureReasons) {
                            logger
                                    .error("Error in soap exchange with payment gateway. FAILURE REASON:"
                                            + failureDetail);
                        }
                    }
                }
            }
            else {
                logger.debug("Card successfully stored in vault.");
                vaultToken = result.getToken().getValue();
            }
        }
        catch (ICreditCardManagementAddStoredCreditCardApplicationFaultFaultFaultMessage ex) {
            logger.error(null, ex);
        }
        return vaultToken;
    }

    private CustomerIdentifier createCustomeridentifier(String customerCode) {
        CustomerIdentifier customerIdentifier = cardManagerFactory.createCustomerIdentifier();
        customerIdentifier = cardManagerFactory.createCustomerIdentifier();
        customerIdentifier.setCustomerCode(cardManagerFactory.createCustomerIdentifierCustomerCode(customerCode));
        customerIdentifier.setLocationCode(cardManagerFactory.createCustomerIdentifierLocationCode(locationCode));
        customerIdentifier.setMerchantCode(cardManagerFactory.createCustomerIdentifierMerchantCode(merchantCode));
        return customerIdentifier;
    }

    private String getTokenForExistingCard(String cardNumber) {
        String vaultToken = null;
        try {
            logger.debug("Retrieving token for existing card from the vault.");
            GetTokenForCardNumberParams getParams = createGetTokenForCardNumberParams(cardNumber);
            TokenCreditCardManagementResult result = cardManagementPort.getTokenForCardNumber(
                    cardManagementClientCredentials, getParams);
            if (!result.getSucceeded()) {
                logger.error("Unable to retrieve token from the vault.");
                if (result.getValidationFailures().getValue() != null) {
                    List<String> failureReasons = result.getValidationFailures().getValue().getString();
                    for (String failureDetail : failureReasons) {
                        logger.error("Error in soap exchange with payment gateway. FAILURE REASON:" + failureDetail);
                    }
                }
            }
            else {
                logger.debug("Token retrieved successfully from the vault.");
                vaultToken = result.getToken().getValue();
            }
        }
        catch (ICreditCardManagementGetTokenForCardNumberApplicationFaultFaultFaultMessage ex) {
            logger.error(null, ex);
        }
        return vaultToken;
    }

    private GetTokenForCardNumberParams createGetTokenForCardNumberParams(String cardNumber) {
        GetTokenForCardNumberParams getParams = cardManagerFactory.createGetTokenForCardNumberParams();
        getParams.setAccountNumber(cardManagerFactory.createGetTokenForCardNumberParamsAccountNumber(cardNumber));
        return getParams;
    }

    private StoredCreditCard createInvalidAddressStoredCreditCard(String cardNumber) {
        StoredCreditCard storedCreditCard;
        storedCreditCard = cardManagerFactory.createStoredCreditCard();
        String validAccountNumber = cardNumber;
        storedCreditCard.setCardAccountNumber(cardManagerFactory.createCreditCardCardAccountNumber(validAccountNumber));
        Address billingAddress = getInvalidAddress();
        storedCreditCard.setBillingAddress(cardManagerFactory.createCreditCardBillingAddress(billingAddress));
        Contact contact = getValidContact();
        storedCreditCard.setCardholder(cardManagerFactory.createCreditCardCardholder(contact));
        storedCreditCard.setExpirationMonth(4);
        storedCreditCard.setExpirationYear(2014);
        String nameOnCard = "John Smith";
        storedCreditCard.setNameOnCard(cardManagerFactory.createCreditCardNameOnCard(nameOnCard));
        return storedCreditCard;
    }

    private StoredCreditCard createStoredCreditCard(String cardNumber) {
        StoredCreditCard storedCreditCard;
        storedCreditCard = cardManagerFactory.createStoredCreditCard();
        String validAccountNumber = cardNumber;
        storedCreditCard.setCardAccountNumber(cardManagerFactory.createCreditCardCardAccountNumber(validAccountNumber));
        Address billingAddress = getValidAddress();
        storedCreditCard.setBillingAddress(cardManagerFactory.createCreditCardBillingAddress(billingAddress));
        Contact contact = getValidContact();
        storedCreditCard.setCardholder(cardManagerFactory.createCreditCardCardholder(contact));
        storedCreditCard.setExpirationMonth(4);
        storedCreditCard.setExpirationYear(2014);
        String nameOnCard = "John Smith";
        storedCreditCard.setNameOnCard(cardManagerFactory.createCreditCardNameOnCard(nameOnCard));
        return storedCreditCard;
    }

    protected Contact getValidContact() {
        String firstname = "John";
        String lastName = "Smith";
        Contact contact = cardManagerFactory.createContact();
        contact.setFirstName(cardManagerFactory.createContactFirstName(firstname));
        contact.setLastName(cardManagerFactory.createContactLastName(lastName));
        return contact;
    }

    protected Address getValidAddress() {
        Address address = cardManagerFactory.createAddress();
        address.setAddressLine1(cardManagerFactory.createAddressAddressLine1("5123 Test St."));
        address.setCountryCode(CountryCode.NONE);
        address.setPostalCode(cardManagerFactory.createAddressPostalCode("32456"));
        return address;
    }

    protected Address getInvalidAddress() {
        Address address = cardManagerFactory.createAddress();
        address.setAddressLine1(cardManagerFactory.createAddressAddressLine1("14151 Test St."));
        address.setCountryCode(CountryCode.NONE);
        address.setPostalCode(cardManagerFactory.createAddressPostalCode("20151"));
        return address;
    }

    protected CreditCardTransaction createValidCreditCardTransaction(StoredCardIdentifier storedCardIdentifier) {
        return createCreditCardTransaction(new Date(), storedCardIdentifier);
    }

    protected CreditCardTransaction createCreditCardTransaction(Date invoiceDate,
            StoredCardIdentifier storedCardIdentifier) {
        CreditCardTransaction creditCardTransaction = factory.createCreditCardTransaction();
        creditCardTransaction.setStoredCardIdentifier(factory
                .createCreditCardTransactionStoredCardIdentifier(storedCardIdentifier));
        creditCardTransaction.setCurrencyCode(CurrencyCode.US_DOLLARS);
        creditCardTransaction.setTotalAmount(totalAmount);
        creditCardTransaction.setTransactionKey(factory.createCreditCardTransactionTransactionKey(invoiceDate
                .toString()
                + Math.random()));
        String customerRef = "PONUMBER1234567";
        creditCardTransaction.setCustomerReferenceValue(factory
                .createCreditCardTransactionCustomerReferenceValue(customerRef));
        creditCardTransaction.setInvoiceNumber(factory.createCreditCardTransactionInvoiceNumber(customerRef));
        String shipFromZip = DEFAULT_SHIP_FROM_POSTAL_CODE;
        //TODO initialize ship from zipcode with externalized property value
        creditCardTransaction = createTransactionLineItemDetails(creditCardTransaction);
        return creditCardTransaction;
    }

    private CreditCardTransaction createTransactionLineItemDetails(CreditCardTransaction creditCardTransaction) {
        ArrayOfLineItemDetail arrayOfLineItemDetail = factory.createArrayOfLineItemDetail();
        LineItemDetailPurchasing detail = factory.createLineItemDetailPurchasing();
        detail.setLineNumber(new Short("1"));
        detail.setProductCode(factory.createLineItemDetailPurchasingProductCode("APD-SKU-123"));
        detail.setCommodityCode(factory.createLineItemDetailPurchasingCommodityCode("10140000"));
        detail.setQuantity(new BigDecimal("1"));
        detail.setTaxAmount(BigDecimal.ZERO);
        detail.setTotalAmount(totalAmount);
        detail.setDescription(factory.createLineItemDetailPurchasingDescription("APD Custom Item"));
        detail.setUnitOfMeasure(factory.createLineItemDetailPurchasingUnitOfMeasure("EA"));
        detail.setItemAmount(totalAmount);
        detail.setDiscountAmount(BigDecimal.ZERO);
        arrayOfLineItemDetail.getLineItemDetail().add(detail);
        creditCardTransaction.setLineDetail(factory.createCreditCardTransactionLineDetail(arrayOfLineItemDetail));
        return creditCardTransaction;
    }
}
