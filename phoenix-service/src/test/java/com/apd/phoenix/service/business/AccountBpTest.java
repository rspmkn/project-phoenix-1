/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.service.business;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.HashSet;
import static org.junit.Assert.*;
import com.apd.phoenix.service.model.Account;
import java.util.Set;
import org.junit.Assert;

/**
 *
 * @author nreidelb
 */
public class AccountBpTest {

    AccountBp testee;
    private Account parent;
    private Account child;

    public AccountBpTest() {
    }

    @Before
    public void setUp() {
        testee = new AccountBp();
        parent = new Account();
        Set<Account> children = new HashSet<>();
        child = new Account();
        child.setLocationCode("1234");
        child.setParentAccount(parent);
        children.add(child);
    }

    @After
    public void tearDown() {
    }

    @Test
    public void findChildByLocationCode1() {
        //disabling this test, since findChildByLocationCode now hits the database instead of checking the entity
        Assert.assertTrue(true);
    }

    @Test
    public void findChildByLocationCode2() {
        child.setLocationCode("1111");
        final Account wrongChild = new Account();
        wrongChild.setLocationCode("4333");
        final Account rightChild = new Account();
        rightChild.setLocationCode("1234");
        //disabling this test, since findChildByLocationCode now hits the database instead of checking the entity
        Assert.assertTrue(true);
    }
}