package com.apd.phoenix.service.aws;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.UUID;
import javax.inject.Inject;
import junit.framework.Assert;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.DependencyResolvers;
import org.jboss.shrinkwrap.resolver.api.maven.MavenDependencyResolver;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.storage.api.StorageObject;
import com.apd.phoenix.service.storage.api.StorageObjectRequest;
import com.apd.phoenix.service.storage.api.StorageService;

@RunWith(Arquillian.class)
public class AWSS3ServiceIT {

    private static final Logger LOGGER = LoggerFactory.getLogger(AWSS3ServiceIT.class);

    @Inject
    StorageService storageService;

    private static final String BUCKET_NAME = "apd-phoenix-notifications-dev-20130916";

    @Deployment
    public static WebArchive createDeployment() {
        // Create a resolver that will get dependencies from the pom for use
        // it the tests.
        MavenDependencyResolver resolver = DependencyResolvers.use(MavenDependencyResolver.class).loadMetadataFromPom(
                "pom.xml");

        // Build and return a dummy deployment of the app for testing.
        return ShrinkWrap.create(WebArchive.class, "test.war").addClasses().addPackages(true,
                "com.apd.phoenix.service.storage").addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml")
                .addAsLibraries(resolver.artifacts("com.amazonaws:aws-java-sdk:1.5.7").resolveAsFiles());

    }

    @Test
    public void retrieveAnObject() throws IOException {
        //Create the object
        File testFile = createSampleFile();
        InputStream ips = new FileInputStream(testFile);
        StorageObject createObject = new StorageObject();
        createObject.setPath("test/sample-" + UUID.randomUUID().toString() + ".txt");
        createObject.setContent(ips);
        createObject.getAttributes().put("bucketName", BUCKET_NAME);
        createObject.setContentLength(testFile.length());
        createObject.setContentType("text/plain");
        Assert.assertTrue(storageService.createObject(createObject));

        LOGGER.info("Getting an object from S3");
        StorageObjectRequest getRequest = new StorageObjectRequest();
        getRequest.setPath(createObject.getPath());
        getRequest.getAttributes().put("bucketName", BUCKET_NAME);
        StorageObject retrievedObject = storageService.retrieveObject(getRequest);
        Assert.assertNotNull(retrievedObject);

        byte fileContent[] = new byte[(int) retrievedObject.getContentLength()];
        try {
            retrievedObject.getContent().read(fileContent);
        }
        catch (IOException e) {
            // TODO Auto-generated catch block
            LOGGER.error("An error occured:", e);
        }

        LOGGER.info("Contents:\n{}", new String(fileContent));

        //Delete the object
        StorageObjectRequest deleteRequest = new StorageObjectRequest();
        deleteRequest.setPath(retrievedObject.getPath());
        deleteRequest.getAttributes().put("bucketName", BUCKET_NAME);
        Assert.assertTrue(storageService.deleteObject(deleteRequest));

    }

    @Test
    public void createAndDeleteAnObject() throws IOException {
        File testFile = createSampleFile();
        InputStream ips = new FileInputStream(testFile);
        StorageObject object = new StorageObject();
        object.setPath("test/sample-" + UUID.randomUUID().toString() + ".txt");
        object.setContent(ips);
        object.getAttributes().put("bucketName", BUCKET_NAME);
        object.setContentLength(testFile.length());
        object.setContentType("text/plain");
        Assert.assertTrue(storageService.createObject(object));

        //Attempt to delete the new file
        StorageObjectRequest request = new StorageObjectRequest();
        request.setPath(object.getPath());
        request.getAttributes().put("bucketName", BUCKET_NAME);
        Assert.assertTrue(storageService.deleteObject(request));
    }

    @Test
    public void updateAnObject() throws IOException {
        File testFile = createSampleFile();
        InputStream ips = new FileInputStream(testFile);
        StorageObject object = new StorageObject();
        object.setPath("test/sample-" + UUID.randomUUID().toString() + ".txt");
        object.setContent(ips);
        object.getAttributes().put("bucketName", BUCKET_NAME);
        object.setContentLength(testFile.length());
        object.setContentType("text/plain");
        Assert.assertTrue(storageService.createObject(object));

        //Attempt to update the new file
        File testFile2 = createSampleFile();
        InputStream ips2 = new FileInputStream(testFile2);
        object.setContent(ips2);
        Assert.assertTrue(storageService.updateObject(object));
    }

    private static File createSampleFile() throws IOException {
        File file = File.createTempFile("aws-java-sdk-", ".txt");
        file.deleteOnExit();

        Writer writer = new OutputStreamWriter(new FileOutputStream(file));
        writer.write("********Sample File*******\n");
        writer.write("abcdefghijklmnopqrstuvwxyz\n");
        writer.write("01234567890112345678901234\n");
        writer.write("!@#$%^&*()-=[]{};':',.<>/?\n");
        writer.write("01234567890112345678901234\n");
        writer.write("abcdefghijklmnopqrstuvwxyz\n");
        String randomString = UUID.randomUUID().toString();
        LOGGER.info(randomString);
        writer.write(randomString);
        writer.close();

        return file;
    }
}
