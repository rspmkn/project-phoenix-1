/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.service.payment.ws.client.authorize.capture;

import com.apd.phoenix.service.payment.ws.AuthorizeAndCaptureParams;
import com.apd.phoenix.service.payment.ws.CardSecurityCodeIndicator;
import com.apd.phoenix.service.payment.ws.CreditCardTransaction;
import com.apd.phoenix.service.payment.ws.CreditCardTransactionAuthorizationResult;
import com.apd.phoenix.service.payment.ws.CreditCardTransactionCreditCard;
import com.apd.phoenix.service.payment.ws.CreditCardTransactionResult;
import com.apd.phoenix.service.payment.ws.ICreditCardTransactionAuthorizeAndCaptureApplicationFaultFaultFaultMessage;
import com.apd.phoenix.service.payment.ws.StoredCardIdentifier;
import com.apd.phoenix.service.payment.ws.client.PaymentGatewayBaseTest;
import static com.apd.phoenix.service.payment.ws.client.PaymentGatewayTestConstants.validAmericanExpressNumber;
import static com.apd.phoenix.service.payment.ws.client.PaymentGatewayTestConstants.validDinersCardNumber;
import static com.apd.phoenix.service.payment.ws.client.PaymentGatewayTestConstants.validDiscoverCardNumber;
import static com.apd.phoenix.service.payment.ws.client.PaymentGatewayTestConstants.validJCBNumber;
import static com.apd.phoenix.service.payment.ws.client.PaymentGatewayTestConstants.validMasterCardNumber;
import static com.apd.phoenix.service.payment.ws.client.PaymentGatewayTestConstants.validVisaCardNumber;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import static org.testng.Assert.assertTrue;
import org.testng.annotations.Test;

/**
 *
 * @author dnorris
 */
public class AthorizeAndCaptureTests extends PaymentGatewayBaseTest {

    private static final Logger logger = LoggerFactory.getLogger(AthorizeAndCaptureTests.class);

    @Test(dependsOnMethods = { "addCustomerToVault" })
    public void americanExpress() {
        StoredCardIdentifier card = getStoredCardIdentifier(validAmericanExpressNumber);
        assertTrue(validCardAuthorizeAndCaptureTest(card).isSucceeded());
    }

    @Test(dependsOnMethods = { "addCustomerToVault" })
    public void jcb() {
        StoredCardIdentifier card = getStoredCardIdentifier(validJCBNumber);
        assertTrue(validCardAuthorizeAndCaptureTest(card).isSucceeded());
    }

    @Test(dependsOnMethods = { "addCustomerToVault" })
    public void discover() {
        StoredCardIdentifier card = getStoredCardIdentifier(validDiscoverCardNumber);
        assertTrue(validCardAuthorizeAndCaptureTest(card).isSucceeded());
    }

    @Test(dependsOnMethods = { "addCustomerToVault" })
    public void diners() {
        StoredCardIdentifier card = getStoredCardIdentifier(validDinersCardNumber);
        assertTrue(validCardAuthorizeAndCaptureTest(card).isSucceeded());
    }

    @Test(dependsOnMethods = { "addCustomerToVault" })
    public void masterCard() {
        StoredCardIdentifier card = getStoredCardIdentifier(validMasterCardNumber);
        assertTrue(validCardAuthorizeAndCaptureTest(card).isSucceeded());
    }

    @Test(dependsOnMethods = { "addCustomerToVault" })
    public void visa() {
        StoredCardIdentifier card = getStoredCardIdentifier(validVisaCardNumber);
        assertTrue(validCardAuthorizeAndCaptureTest(card).isSucceeded());
    }

    public CreditCardTransactionResult validCardAuthorizeAndCaptureTest(StoredCardIdentifier creditCard) {
        try {
            AuthorizeAndCaptureParams params = new AuthorizeAndCaptureParams();
            CreditCardTransaction creditCardTransaction = createValidCreditCardTransaction(creditCard);
            CreditCardTransactionCreditCard cardForCvv = factory.createCreditCardTransactionCreditCard();
            cardForCvv.setCardSecurityCodeIndicator(CardSecurityCodeIndicator.PROVIDED);
            cardForCvv.setCardSecurityCode(factory.createCreditCardTransactionCreditCardCardSecurityCode("124"));
            creditCardTransaction.setCreditCard(factory.createCreditCardTransactionCreditCard1(cardForCvv));
            params.setCreditCardTransaction(factory
                    .createAuthorizeAndCaptureParamsCreditCardTransaction(creditCardTransaction));
            params.setTerminalIdentifier(factory.createAuthorizeAndCaptureParamsTerminalIdentifier(terminalIdentifier));
            CreditCardTransactionAuthorizationResult result = port.authorizeAndCapture(clientCredentials, params);
            if (!result.isSucceeded()) {
                if (result.getValidationFailures().getValue() != null) {
                    List<String> failureReasons = result.getValidationFailures().getValue().getString();
                    for (String failureDetail : failureReasons) {
                        logger.error(failureDetail);
                    }
                }
            }
            return result;
        }
        catch (ICreditCardTransactionAuthorizeAndCaptureApplicationFaultFaultFaultMessage ex) {
            logger.error(null, ex);
        }
        return null;
    }

}
