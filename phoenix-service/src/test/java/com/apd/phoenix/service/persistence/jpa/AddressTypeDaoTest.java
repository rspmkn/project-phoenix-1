package com.apd.phoenix.service.persistence.jpa;

import javax.inject.Inject;
import org.jboss.arquillian.junit.Arquillian;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.model.AddressType;

@RunWith(Arquillian.class)
public class AddressTypeDaoTest extends DaoTest {

    private static final Logger LOG = LoggerFactory.getLogger(AddressTypeDaoTest.class);

    @Inject
    private AddressTypeDao addressTypeDao;

    @Test
    public void testIsDeployed() {
        Assert.assertNotNull(addressTypeDao);
    }

    @Test
    public void testCreateAddressType() {

        String name = "NewAddressType";

        AddressType addressType = new AddressType();
        addressType.setName(name);

        LOG.info("Testing the creation of a new AddressType with name: " + name);
        addressTypeDao.create(addressType);
        AddressType persistedAddressType = addressTypeDao.findById(addressType.getId(), AddressType.class);

        Assert.assertEquals(persistedAddressType.getName(), name);
    }
}
