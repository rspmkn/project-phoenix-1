package com.apd.phoenix.service.solr.integration;

import com.apd.phoenix.service.business.CatalogBp;
import com.apd.phoenix.service.business.CatalogXItemBp;
import com.apd.phoenix.service.business.HierarchyNodeBp;
import com.apd.phoenix.service.business.ItemBp;
import com.apd.phoenix.service.business.ManufacturerBp;
import com.apd.phoenix.service.business.SkuBp;
import com.apd.phoenix.service.business.SkuTypeBp;
import com.apd.phoenix.service.business.UnitOfMeasureBp;
import com.apd.phoenix.service.business.VendorBp;
import com.apd.phoenix.service.solr.IndexingException;
import com.apd.phoenix.service.model.Catalog;
import com.apd.phoenix.service.model.CatalogXItem;
import com.apd.phoenix.service.model.HierarchyNode;
import com.apd.phoenix.service.model.Item;
import com.apd.phoenix.service.model.Manufacturer;
import com.apd.phoenix.service.model.Sku;
import com.apd.phoenix.service.model.SkuType;
import com.apd.phoenix.service.model.UnitOfMeasure;
import com.apd.phoenix.service.model.Vendor;
import com.apd.phoenix.service.solr.SolrServiceBean;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;
import javax.ejb.EJB;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import junit.framework.Assert;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServer;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;
import org.hibernate.Query;
import org.hibernate.Session;
import org.jboss.arquillian.junit.Arquillian;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author dnorris
 */
@RunWith(Arquillian.class)
public class CatalogXItemEntityListenerTest extends BaseArquillianTest {

    private static final Logger logger = LoggerFactory.getLogger(CatalogXItemEntityListenerTest.class);
    private static final String TEST_UNIT_OF_MEASURE = "Unique Test Unit of Measure";
    private static final String TEST_VENDOR_NAME = "Unique Test Vendor";
    private static final String TEST_CATALOG_NAME = "Unique Test Catalog";
    private static final String TEST_ITEM_NAME = "Solr Test Item";
    private static final String TEST_ITEM_DESCRIPTION = "Solr Test Item";
    private static final String TEST_MANUFACTURER_NAME = "Solr Manufacturer";
    @Inject
    private CatalogXItemBp catXItemBp;
    @Inject
    private CatalogBp catalogBp;
    @Inject
    private ItemBp itemBp;
    @Inject
    private UnitOfMeasureBp unitOfMeasureBp;
    @Inject
    private VendorBp vendorBp;
    @Inject
    private ManufacturerBp manufacturerBp;
    @Inject
    private HierarchyNodeBp hierarchyNodeBp;
    @Inject
    private SkuBp skuBp;
    @Inject
    private SkuTypeBp skuTypeBp;
    @PersistenceContext(unitName = "Phoenix")
    private EntityManager entityManager;
    @EJB
    private SolrServiceBean solrService;
    private CatalogXItem catXItem;

    @Before
    public void createCatalogXItem() throws Exception {
        try {
            insertData();
            logger.info("Sleeping for 2 seconds to wait for mdb");
            Thread.sleep(20000);
        }
        catch (Exception ex) {
            logger.error(null, ex);
            clearData();
        }
    }

    @Test
    public void validateCreateDeleteSolrDocument() throws InterruptedException, IOException {
        SolrServer solrServer = solrService.getSolrServer();
        SolrQuery query = new SolrQuery();
        query.add("q", "id:" + catXItem.getId());
        SolrDocument solrDocument;
        try {
            QueryResponse response = solrServer.query(query);
            SolrDocumentList searchResultDocuments = response.getResults();
            if (searchResultDocuments.size() != 1) {
                throw new IllegalStateException("Expected 1 result, actually: " + searchResultDocuments.size()
                        + " for query: " + query + " and id: " + catXItem.getId());
            }
            else {
                solrDocument = searchResultDocuments.get(0);
                Assert.assertNotNull(solrDocument);
                logger.info("Solr Document confirmed as successfully created");
                logger.info("Deleting the catalogXItem");
                catXItemBp.delete(catXItem.getId(), CatalogXItem.class);
                logger.info("Sleeping for 2 seconds to wait for MDB event");
                Thread.sleep(2000);
                logger.info("Confirming the solr document is no longer indexed");
                response = solrServer.query(query);
                searchResultDocuments = response.getResults();
                Assert.assertEquals(searchResultDocuments.size(), 0);
                logger.info("Removal Confirmation Complete");
            }
        }
        catch (SolrServerException e) {
            throw new IndexingException("Exception search indexing item: " + catXItem.getId(), e);
        }
    }

    @After
    public void cleanUpData() throws Exception {
        clearData();
    }

    private void clearData() throws Exception {
        logger.info("Clearing out old data");
        try {
            deleteTestSku();
            deleteTestCatalogXItem();
            deleteTestItem();
            deleteTestCatalog();
            deleteTestManufacturer();
            deleteTestVendor();
            deleteTestUnitOfMeasure();
            deleteTestHierarchyNode();
        }
        catch (Exception ex) {
            logger.error(null, ex);
        }
    }

    private void deleteTestSku() {
        logger.info("Start Deleting Sku");
        Sku sku = getTestSku();
        Session session = (Session) entityManager.getDelegate();
        String query = "delete from sku where value = :skuValue";
        final Query itemQuery = session.createSQLQuery(query);
        itemQuery.setParameter("skuValue", sku.getValue());
        itemQuery.executeUpdate();
        logger.info("Completed Deleting Sku");
    }

    private void deleteTestVendor() {
        logger.info("Start Deleting Vendor");
        Vendor vendor = getTestVendor();
        Session session = (Session) entityManager.getDelegate();
        String query = "delete from vendor where name = :vendorName";
        final Query itemQuery = session.createSQLQuery(query);
        itemQuery.setParameter("vendorName", vendor.getName());
        itemQuery.executeUpdate();
        logger.info("Completed Deleting Vendor");
    }

    private void deleteTestUnitOfMeasure() {
        logger.info("Start Deleting UnitOfMeasure");
        UnitOfMeasure uom = getTestUnitOfMeasure();
        Session session = (Session) entityManager.getDelegate();
        String query = "delete from UnitOfMeasure where name = :uomName";
        final Query itemQuery = session.createSQLQuery(query);
        itemQuery.setParameter("uomName", uom.getName());
        itemQuery.executeUpdate();
        logger.info("Completed Deleting UnitOfMeasure");
    }

    private void deleteTestHierarchyNode() {
        logger.info("Start Deleting HierarchyNode");
        HierarchyNode hierarchyNode = getTestHierarchyNode();
        Session session = (Session) entityManager.getDelegate();
        String query = "delete from HierarchyNode where description = :description and indexedpaths = :indexedPaths";
        final Query itemQuery = session.createSQLQuery(query);
        itemQuery.setParameter("description", hierarchyNode.getDescription());
        itemQuery.setParameter("indexedPaths", hierarchyNode.getIndexedPaths());
        itemQuery.executeUpdate();
        logger.info("Completed Deleting HierarchyNode");
    }

    private void deleteTestManufacturer() {
        logger.info("Start Deleting Manufacturer");
        Manufacturer manufacturer = getTestManufacturer();
        Session session = (Session) entityManager.getDelegate();
        String query = "delete from Manufacturer where name = :manufacturerName";
        final Query itemQuery = session.createSQLQuery(query);
        itemQuery.setParameter("manufacturerName", manufacturer.getName());
        itemQuery.executeUpdate();
        logger.info("Completed Deleting Manufacturer");
    }

    private void deleteTestCatalog() {
        logger.info("Start Deleting Catalog");
        Catalog catalog = getTestCatalog();
        Session session = (Session) entityManager.getDelegate();
        String query = "delete from Catalog where name = :catalogName";
        final Query itemQuery = session.createSQLQuery(query);
        itemQuery.setParameter("catalogName", catalog.getName());
        itemQuery.executeUpdate();
        logger.info("Completed Deleting Catalog");
    }

    private void deleteTestItem() {
        logger.info("Start Deleting Item");
        Item item = getTestItem();
        Session session = (Session) entityManager.getDelegate();
        String query = "delete from Item where name = :itemName and description = :itemDescription";
        final Query itemQuery = session.createSQLQuery(query);
        itemQuery.setParameter("itemName", item.getName());
        itemQuery.setParameter("itemDescription", item.getDescription());
        itemQuery.executeUpdate();
        logger.info("Completed Deleting Item");
    }

    private void deleteTestCatalogXItem() {
        logger.info("Start Deleting CatalogXItem");
        Session session = (Session) entityManager.getDelegate();
        if (catXItem != null) {
            String query = "delete from CatalogXItem where id = :catxItemID";
            final Query itemQuery = session.createSQLQuery(query);
            itemQuery.setParameter("catxItemID", catXItem.getId());
            itemQuery.executeUpdate();
        }
        else {
            Item item = null;
            List<Item> items = itemBp.searchByExactExample(getTestItem(), 0, 0);
            if (!items.isEmpty()) {
                item = items.get(0);
            }
            String query = "delete from CatalogXItem c where c.item_id = :itemID";
            final Query itemQuery = session.createSQLQuery(query);
            itemQuery.setParameter("itemID", item.getId());
            itemQuery.executeUpdate();
        }
        logger.info("Completed Deleting CatalogXItem");
    }

    private void insertData() throws InterruptedException {
        logger.info("Inserting new data.");
        Vendor vendor = getTestVendor();
        vendor = vendorBp.create(vendor);
        Catalog catalog = getTestCatalog();
        catalog.setVendor(vendor);
        catalog = catalogBp.create(catalog);
        UnitOfMeasure unitOfMeasure = getTestUnitOfMeasure();
        unitOfMeasure = unitOfMeasureBp.create(unitOfMeasure);
        Manufacturer manufacturer = getTestManufacturer();
        manufacturerBp.create(manufacturer);
        HierarchyNode hierarchyNode = getTestHierarchyNode();
        hierarchyNodeBp.create(hierarchyNode);
        Item item = getTestItem();
        item.setVendorCatalog(catalog);
        item.setUnitOfMeasure(unitOfMeasure);
        item.setManufacturer(manufacturer);
        item.setHierarchyNode(hierarchyNode);
        item = itemBp.create(item);
        Sku sku = getTestSku();
        sku.setItem(item);
        skuBp.create(sku);
        catXItem = getTestCatXItemStub(item);
        catXItem.setCatalog(catalog);
        logger.info("Adding CatalogXItem.");
        catXItem.setCatalog(catalog);
        catXItem = catXItemBp.create(catXItem);
    }

    private Vendor getTestVendor() {
        Vendor vendor = new Vendor();
        vendor.setName(TEST_VENDOR_NAME);
        return vendor;
    }

    private Catalog getTestCatalog() {
        Catalog catalog = new Catalog();
        catalog.setName(TEST_CATALOG_NAME);
        return catalog;
    }

    private UnitOfMeasure getTestUnitOfMeasure() {
        UnitOfMeasure unitOfMeasure = new UnitOfMeasure();
        unitOfMeasure.setName(TEST_UNIT_OF_MEASURE);
        return unitOfMeasure;
    }

    private Manufacturer getTestManufacturer() {
        Manufacturer manufacturer = new Manufacturer();
        manufacturer.setName(TEST_MANUFACTURER_NAME);
        return manufacturer;
    }

    private Item getTestItem() {
        Item item = new Item();
        item.setName(TEST_ITEM_NAME);
        item.setDescription(TEST_ITEM_DESCRIPTION);
        return item;
    }

    private CatalogXItem getTestCatXItemStub(Item item) {
        CatalogXItem catXItem = new CatalogXItem();
        catXItem.setItem(item);
        catXItem.setPrice(new BigDecimal(1.99));
        return catXItem;
    }

    private HierarchyNode getTestHierarchyNode() {
        HierarchyNode hierarchyNode = new HierarchyNode();
        hierarchyNode.setPublicId(new Long(9999));
        hierarchyNode.setDescription("Solr Ovens");
        hierarchyNode.setIndexedPaths("!Breakroom and Janitorial!Food & Beverage Service!Appliances!Solr Ovens");
        return hierarchyNode;
    }

    private Sku getTestSku() {

        Sku sku = new Sku();
        sku.setType(getTestSkuType());
        sku.setValue("APD-123456789");
        return sku;
    }

    private SkuType getTestSkuType() {
        SkuType skuType = new SkuType();
        skuType.setName("APD");
        List<SkuType> skuTypes = skuTypeBp.searchByExample(skuType, 0, 0);
        if (!skuTypes.isEmpty()) {
            skuType = skuTypes.get(0);
        }
        else {
            skuType = skuTypeBp.create(skuType);
        }
        return skuType;
    }
}
