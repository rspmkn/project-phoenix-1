/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.service.payment.ws.client;

import java.math.BigDecimal;
import java.math.RoundingMode;
import org.apache.commons.configuration.PropertiesConfiguration;

/**
 *
 * @author dnorris
 */
public class PaymentGatewayTestConstants {

    private static final PropertiesConfiguration props = PaymentGatewayTestPropertiesLoader.getInstance()
            .getProperties();
    public static final String clientCode = props.getString("testClientCode");
    public static final String username = props.getString("testUserName");
    public static final String password = props.getString("testPassword");
    public static final String locationCode = props.getString("testLocationCode");
    public static final String merchantCode = props.getString("testMerchantCode");
    public static final String terminalCode = props.getString("testTerminalCode");
    public static final BigDecimal totalAmount = new BigDecimal(9.99).setScale(2, RoundingMode.CEILING);
    public static final String CUSTOMER_CODE = "BobsHardware";
    public static final String validVisaCardNumber = "4111111111111111";
    public static final String validMasterCardNumber = "5584312967483092";
    public static final String validAmericanExpressNumber = "370824782591981";
    public static final String validJCBNumber = "3530111333300000";
    public static final String validDiscoverCardNumber = "6011375146854740";
    public static final String validDinersCardNumber = "365948383996583";
}
