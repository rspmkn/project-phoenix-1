/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.service.payment.ws.client.authorization;

import com.apd.phoenix.service.payment.ws.AuthorizeParams;
import com.apd.phoenix.service.payment.ws.CreditCardTransaction;
import com.apd.phoenix.service.payment.ws.CreditCardTransactionResult;
import com.apd.phoenix.service.payment.ws.ICreditCardTransactionAuthorizeApplicationFaultFaultFaultMessage;
import com.apd.phoenix.service.payment.ws.StoredCardIdentifier;
import com.apd.phoenix.service.payment.ws.client.PaymentGatewayBaseTest;
import static com.apd.phoenix.service.payment.ws.client.PaymentGatewayTestConstants.*;
import java.util.List;
import static org.junit.Assert.assertTrue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.Test;

/**
 *
 * @author dnorris
 */
public class AuthorizationTests extends PaymentGatewayBaseTest {

    private static final Logger logger = LoggerFactory.getLogger(AuthorizationTests.class);

    @Test(dependsOnMethods = { "addCustomerToVault" })
    public void americanExpress() {
        StoredCardIdentifier card = getStoredCardIdentifier(validAmericanExpressNumber);
        assertTrue(validAuthorizationRequestTest(card).isSucceeded());
    }

    @Test(dependsOnMethods = { "addCustomerToVault" })
    public void jcb() {
        StoredCardIdentifier card = getStoredCardIdentifier(validJCBNumber);
        assertTrue(validAuthorizationRequestTest(card).isSucceeded());
    }

    @Test(dependsOnMethods = { "addCustomerToVault" })
    public void discover() {
        StoredCardIdentifier card = getStoredCardIdentifier(validDiscoverCardNumber);
        assertTrue(validAuthorizationRequestTest(card).isSucceeded());
    }

    @Test(dependsOnMethods = { "addCustomerToVault" })
    public void diners() {
        StoredCardIdentifier card = getStoredCardIdentifier(validDinersCardNumber);
        assertTrue(validAuthorizationRequestTest(card).isSucceeded());
    }

    @Test(dependsOnMethods = { "addCustomerToVault" })
    public void masterCard() {
        StoredCardIdentifier card = getStoredCardIdentifier(validMasterCardNumber);
        assertTrue(validAuthorizationRequestTest(card).isSucceeded());
    }

    @Test(dependsOnMethods = { "addCustomerToVault" })
    public void visa() {
        StoredCardIdentifier card = getStoredCardIdentifier(validVisaCardNumber);
        assertTrue(validAuthorizationRequestTest(card).isSucceeded());
    }

    public CreditCardTransactionResult validAuthorizationRequestTest(StoredCardIdentifier creditCard) {
        try {
            AuthorizeParams params = factory.createAuthorizeParams();
            CreditCardTransaction creditCardTransaction = createValidCreditCardTransaction(creditCard);
            params.setCreditCardTransaction(factory.createAuthorizeParamsCreditCardTransaction(creditCardTransaction));
            params.setTerminalIdentifier(factory.createAuthorizeParamsTerminalIdentifier(terminalIdentifier));
            CreditCardTransactionResult result = port.authorize(clientCredentials, params);
            if (!result.isSucceeded()) {
                if (result.getValidationFailures().getValue() != null) {
                    List<String> failureReasons = result.getValidationFailures().getValue().getString();
                    for (String failureDetail : failureReasons) {
                        logger.error(failureDetail);
                    }
                }
            }
            return result;
        }
        catch (ICreditCardTransactionAuthorizeApplicationFaultFaultFaultMessage ex) {
            logger.error("ICreditCardTransactionVoidApplicationFaultFaultFaultMessage caught", ex);
        }
        return null;
    }

    //    @Test
    //    public void AVSDenialAuthorizationRequestTest() {
    //        try {
    //            AuthorizeParams params = factory.createAuthorizeParams();
    //            creditCard.getBillingAddress().getValue().setAddressLine1(
    //                    factory.createAddressAddressLine1("14151 Test St."));
    //            creditCard.getBillingAddress().getValue().setPostalCode(factory.createAddressPostalCode("20151"));
    //            CreditCardTransaction creditCardTransaction = createValidCreditCardTransaction(creditCard);
    //            params.setCreditCardTransaction(factory.createAuthorizeParamsCreditCardTransaction(creditCardTransaction));
    //            params.setTerminalIdentifier(factory.createAuthorizeParamsTerminalIdentifier(terminalIdentifier));
    //            CreditCardTransactionAuthorizationResult result = port.authorize(clientCredentials, params);
    //            Assert.assertEquals(result.getAddressAvsResponse(), AvsResponse.NOT_MATCHED);
    //        }
    //        catch (ICreditCardTransactionAuthorizeApplicationFaultFaultFaultMessage ex) {
    //            logger.error("ICreditCardTransactionVoidApplicationFaultFaultFaultMessage caught", ex);
    //        }
    //    }
    //
    //    @Test
    //    public void CVVDenialAuthorizationRequestTest() {
    //        if (!creditCard.getCardType().equals(CreditCardTypes.AMERICAN_EXPRESS)) {
    //            try {
    //
    //                AuthorizeParams params = factory.createAuthorizeParams();
    //                creditCard.setCardSecurityCode(factory.createCreditCardTransactionCreditCardCardSecurityCode("123"));
    //                CreditCardTransaction creditCardTransaction = createValidCreditCardTransaction(creditCard);
    //                params.setCreditCardTransaction(factory
    //                        .createAuthorizeParamsCreditCardTransaction(creditCardTransaction));
    //                params.setTerminalIdentifier(factory.createAuthorizeParamsTerminalIdentifier(terminalIdentifier));
    //                CreditCardTransactionAuthorizationResult result = port.authorize(clientCredentials, params);
    //                Assert.assertEquals(result.getCardSecurityCodeResponse(), CardSecurityCodeResponse.NOT_MATCHED);
    //
    //            }
    //            catch (ICreditCardTransactionAuthorizeApplicationFaultFaultFaultMessage ex) {
    //                logger.error("ICreditCardTransactionVoidApplicationFaultFaultFaultMessage caught", ex);
    //            }
    //        }
    //    }
}
