package com.apd.phoenix.service.message;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;
import javax.inject.Inject;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import org.antlr.stringtemplate.StringTemplate;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.DependencyResolvers;
import org.jboss.shrinkwrap.resolver.api.maven.MavenDependencyResolver;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.business.AbstractBp;
import com.apd.phoenix.service.business.AddressBp;
import com.apd.phoenix.service.business.Bp;
import com.apd.phoenix.service.business.CustomerOrderBp;
import com.apd.phoenix.service.business.OrderLogBp;
import com.apd.phoenix.service.business.OrderStatusBp;
import com.apd.phoenix.service.business.PoNumberBp;
import com.apd.phoenix.service.business.ProcessBp;
import com.apd.phoenix.service.email.api.EmailService;
import com.apd.phoenix.service.invoice.MessageLineItem;
import com.apd.phoenix.service.message.api.Message;
import com.apd.phoenix.service.message.api.MessageService;
import com.apd.phoenix.service.message.api.MessageUtils;
import com.apd.phoenix.service.model.Address;
import com.apd.phoenix.service.model.CustomerOrder;
import com.apd.phoenix.service.model.MessageMetadata;
import com.apd.phoenix.service.model.MiscShipTo;
import com.apd.phoenix.service.model.OrderLog.EventType;
import com.apd.phoenix.service.persistence.jpa.AbstractDao;
import com.apd.phoenix.service.persistence.jpa.AddressDao;
import com.apd.phoenix.service.persistence.jpa.CustomerOrderDao;
import com.apd.phoenix.service.persistence.jpa.Dao;
import com.apd.phoenix.service.persistence.jpa.MessageMetadataDao;
import com.apd.phoenix.service.persistence.jpa.OrderLogDao;
import com.apd.phoenix.service.persistence.jpa.OrderStatusDao;
import com.apd.phoenix.service.persistence.jpa.PoNumberDao;
import com.apd.phoenix.service.persistence.jpa.ProcessDao;

@RunWith(Arquillian.class)
public class MessageServiceIT {

    private static final Logger LOG = LoggerFactory.getLogger(MessageServiceIT.class);

    @Inject
    CustomerOrderDao customerOrderDao;

    @Inject
    MessageService messageService;

    @Inject
    MessageUtils messageUtils;

    @Inject
    EmailService emailService;

    @Deployment
    public static WebArchive createDeployment() {
        // Create a resolver that will get dependencies from the pom for use
        // it the tests.
        MavenDependencyResolver resolver = DependencyResolvers.use(MavenDependencyResolver.class).loadMetadataFromPom(
                "pom.xml");

        // Build and return a dummy deployment of the app for testing.
        WebArchive artifact = ShrinkWrap.create(WebArchive.class, "test.war").addAsResource(
                "arquillian-ds/persistence.xml", "META-INF/persistence.xml")

        .addClasses(CustomerOrderDao.class, AbstractDao.class, AddressDao.class, ProcessDao.class,
                MessageMetadataDao.class, OrderLogDao.class, PoNumberDao.class, OrderStatusDao.class, Dao.class,
                OrderStatusBp.class, OrderLogBp.class, ProcessBp.class, AddressBp.class, CustomerOrderBp.class,
                AbstractBp.class, Bp.class, PoNumberBp.class, MessageLineItem.class).addPackages(true,
                "com.apd.phoenix.service.email", "com.apd.phoenix.service.message", "com.apd.phoenix.service.storage",
                "com.apd.phoenix.service.model").addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml").addAsLibraries(
                resolver.artifacts("com.amazonaws:aws-java-sdk:1.5.7", "commons-io:commons-io:2.1",
                        "org.apache.camel:camel-core:2.11.0", "org.apache.camel:camel-stream:2.11.0").resolveAsFiles());
        // LOG.info(artifact.toString(true));
        return artifact;

    }

    @Test
	public void rejectedShipToEmail() throws IOException, MessagingException {
		Long customerOrderId = new Long(1905);
		String shipToId = "1234ABCD";
		CustomerOrder customerOrder = customerOrderDao.findById(
				customerOrderId, CustomerOrder.class);
		Address address = new Address();
		address.setShipToId(shipToId);
		address.setMiscShipTo(new MiscShipTo());
		address.getMiscShipTo().setAddressId(shipToId);
		customerOrder.setAddress(address);
		// customerOrder = customerOrderDao.update(customerOrder);
		
		
		
		String from = "customerservice@phoenixordering.com";
		String recipient = "jeckstei@redhat.com";
		String subject = "New ShipTo IDRequest: " + shipToId;
		String emailBody = prepareEmailMessage("1055941",
				"APDxml (APD/Northrop)", "1234ABCD", "APD/Northrop Grumman",
				"NGCO Sector", "9020 JUNCTION DR", "ANNAPOLIS JUNCTION", "MD",
				"20701");
		//InputStream is = new ByteArrayInputStream(emailBody.getBytes());
		Set<String> recipients = new HashSet<>();
		recipients.add(recipient);
		MimeMessage message = emailService.prepareRawMessage(from, null, null, recipients, null, subject, emailBody, null);
		
		//TODO: Fix so it doesn't load all into memory
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		message.writeTo(bos);
		byte[] mimeMessage = bos.toByteArray();
		ByteArrayInputStream ips = new ByteArrayInputStream(mimeMessage);
	
		Message messageToSend = messageUtils.createMessage(customerOrder, ips, mimeMessage.length, MessageMetadata.MessageType.EMAIL, recipient, null, null);
		messageService.sendMessage(messageToSend, EventType.RESENT_NOTFICATION);
	}

    public String prepareEmailMessage(String masterAccountNumber, String billTo, String shipToId, String customerName,
            String line1, String line2, String city, String state, String zip) {
        StringTemplate contentTemplate = new StringTemplate(
                "<p>Within Master Acct # $masterAccountNumber$ with the Bill To '$billTo$',<br />"
                        + "please create the new ShipTo ID $shipToId$ for an $customerName$ order.</p>"
                        + "<p>$shipToId$ should be created as: </p>" + "<p>" + "$line1$<br />" + "$line2$<br />"
                        + "$city$, $state$ $zip$" + "</p>"
                        + "<p>Please inform the APD/NGC Order Team once the new ShipTo ID has been created<br />"
                        + "by replying to this email or clicking here to release the waiting order.</p>");

        contentTemplate.setAttribute("masterAccountNumber", masterAccountNumber);
        contentTemplate.setAttribute("billTo", billTo);
        contentTemplate.setAttribute("shipToId", shipToId);
        contentTemplate.setAttribute("customerName", customerName);
        contentTemplate.setAttribute("line1", line1);
        contentTemplate.setAttribute("line2", line2);
        contentTemplate.setAttribute("city", city);
        contentTemplate.setAttribute("state", state);
        contentTemplate.setAttribute("zip", zip);

        return contentTemplate.toString();
    }
}
