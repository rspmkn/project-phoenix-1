package com.apd.phoenix.service.email;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import javax.inject.Inject;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.DependencyResolvers;
import org.jboss.shrinkwrap.resolver.api.maven.MavenDependencyResolver;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.email.api.Attachment;
import com.apd.phoenix.service.email.api.EmailService;

@RunWith(Arquillian.class)
public class EmailServiceIT {

    private static final Logger LOG = LoggerFactory.getLogger(EmailServiceIT.class);

    @Inject
    EmailService emailService;

    @Deployment
    public static WebArchive createDeployment() {
        // Create a resolver that will get dependencies from the pom for use
        // it the tests.
        MavenDependencyResolver resolver = DependencyResolvers.use(MavenDependencyResolver.class).loadMetadataFromPom(
                "pom.xml");

        // Build and return a dummy deployment of the app for testing.
        WebArchive artifact = ShrinkWrap.create(WebArchive.class, "test.war").addClasses().addPackages(true,
                "com.apd.phoenix.service.email").addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml").addAsLibraries(
                resolver.artifacts("com.amazonaws:aws-java-sdk:1.5.7", "commons-io:commons-io:2.1").resolveAsFiles());
        LOG.info(artifact.toString(true));
        return artifact;

    }

    @Test
    public void sendTestEmail() throws IOException {
        LOG.info("Sending text only email");
        Set<String> recipients = new HashSet<>();
        List<Attachment> attachments = new ArrayList<>();
        //receipiants.add("success@simulator.amazonses.com");
        recipients.add("jeckstei@redhat.com");

        File testFile2 = createSampleFile();
        InputStream ips = new FileInputStream(testFile2);

        //Add an attachment
        Attachment attachment1 = new Attachment();
        attachment1.setContent(ips);
        attachment1.setFileName("text.txt");
        attachment1.setMimeType(Attachment.MimeType.txt);

        attachments.add(attachment1);
        emailService.sendEmail("customerservice@phoenixordering.com", recipients, "This is the subject",
                "Here is the body", attachments);
    }

    private static File createSampleFile() throws IOException {
        File file = File.createTempFile("aws-java-sdk-", ".txt");
        file.deleteOnExit();

        Writer writer = new OutputStreamWriter(new FileOutputStream(file));
        writer.write("********Sample File*******\n");
        writer.write("abcdefghijklmnopqrstuvwxyz\n");
        writer.write("01234567890112345678901234\n");
        writer.write("!@#$%^&*()-=[]{};':',.<>/?\n");
        writer.write("01234567890112345678901234\n");
        writer.write("abcdefghijklmnopqrstuvwxyz\n");
        String randomString = UUID.randomUUID().toString();
        LOG.info(randomString);
        writer.write(randomString);
        writer.close();

        return file;
    }
}
