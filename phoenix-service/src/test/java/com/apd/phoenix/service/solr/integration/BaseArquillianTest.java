package com.apd.phoenix.service.solr.integration;

import com.apd.phoenix.service.business.CatalogBp;
import com.apd.phoenix.service.business.CatalogXItemBp;
import com.apd.phoenix.service.business.exception.OrderException;
import com.apd.phoenix.service.email.api.EmailTemplate;
import com.apd.phoenix.service.email.impl.OrderAcknowledgmentEmailTemplate;
import com.apd.phoenix.service.message.api.MessageUtils;
import com.apd.phoenix.service.message.impl.AWSMessageUtils;
import com.apd.phoenix.service.model.AccountXCredentialXUser;
import com.apd.phoenix.service.model.dto.VendorInvoiceDto;
import com.apd.phoenix.service.model.dto.error.EdiDocumentErrorsDto;
import com.apd.phoenix.service.model.factory.DtoFactory;
import com.apd.phoenix.service.model.listener.CatalogXItemListener;
import com.apd.phoenix.service.payment.ws.CreditCardTransactionResult;
import com.apd.phoenix.service.payment.ws.TransactionType;
import com.apd.phoenix.service.payment.ws.client.PaymentGatewayService;
import com.apd.phoenix.service.persistence.jpa.Dao;
import com.apd.phoenix.service.report.ReportService;
import com.apd.phoenix.service.solr.SolrServiceBean;
import com.apd.phoenix.service.storage.api.StorageService;
import com.apd.phoenix.service.storage.impl.StorageServiceS3Impl;
import com.apd.phoenix.service.utility.CsvExportService;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.DependencyResolvers;
import org.jboss.shrinkwrap.resolver.api.maven.MavenDependencyResolver;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author dnorris
 */
public class BaseArquillianTest {

    @Deployment
    public static Archive<?> createDeployment() {
        MavenDependencyResolver resolver = DependencyResolvers.use(MavenDependencyResolver.class).loadMetadataFromPom(
                "pom.xml");
        return ShrinkWrap.create(WebArchive.class, "test.war").addPackage(CatalogXItemListener.class.getPackage())
                .addPackage(CatalogXItemBp.class.getPackage()).addPackage(Dao.class.getPackage()).addPackage(
                        VendorInvoiceDto.class.getPackage()).addPackage(AccountXCredentialXUser.class.getPackage())
                .addPackage(CsvExportService.class.getPackage()).addPackage(SolrServiceBean.class.getPackage())
                .addClass(OrderException.class).addClass(TransactionType.class).addClass(MessageUtils.class)
                .addPackage(EmailTemplate.class.getPackage()).addPackage(
                        OrderAcknowledgmentEmailTemplate.class.getPackage()).addPackage(
                        ReportService.class.getPackage()).addPackage(DtoFactory.class.getPackage()).addPackage(
                        MessageUtils.class.getPackage()).addPackage(EdiDocumentErrorsDto.class.getPackage())
                .addPackage(StorageService.class.getPackage()).addPackage(PaymentGatewayService.class.getPackage())
                .addPackage(CatalogBp.class.getPackage()).addPackage(AWSMessageUtils.class.getPackage()).addPackage(
                        StorageServiceS3Impl.class.getPackage()).addPackage(
                        CreditCardTransactionResult.class.getPackage()).addClass(BaseArquillianTest.class)
                .addAsLibraries(
                        resolver.artifacts("org.jbpm:jbpm-human-task").exclusion("org.hornetq:hornetq-core")
                                .resolveAsFiles()).addAsLibraries(
                        resolver.artifacts("org.apache.solr:solr-solrj").resolveAsFiles()).addAsLibraries(
                        resolver.artifact("com.apd:phoenix-core").resolveAsFiles()).addAsLibraries(
                        resolver.artifact("com.apd:phoenix-paymentservice-model").resolveAsFiles()).addAsLibraries(
                        resolver.artifact("com.apd:phoenix-solr").resolveAsFiles()).addAsLibraries(
                        resolver.artifact("com.amazonaws:aws-java-sdk").resolveAsFiles())

                .addAsResource("test-persistence.xml", "META-INF/persistence.xml").addAsWebInfResource(
                        EmptyAsset.INSTANCE, "beans.xml");
    }
}
