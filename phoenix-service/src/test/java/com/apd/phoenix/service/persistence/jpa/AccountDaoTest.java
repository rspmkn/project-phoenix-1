package com.apd.phoenix.service.persistence.jpa;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.inject.Inject;
import org.jboss.arquillian.junit.Arquillian;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.model.Account;

@RunWith(Arquillian.class)
public class AccountDaoTest extends DaoTest {

    private static final Logger LOG = LoggerFactory.getLogger(AccountDaoTest.class);

    private static final String[] ACCOUNT_NAMES = { "Walmart", "Office Depot", "Office Max" };

    static {
        cleanDBQueries = new String[] { "delete from Account" };
    }

    @Inject
    private AccountDao accountDao;

    //    private void insertData() throws Exception {
    //        utx.begin();
    //        em.joinTransaction();
    //        LOG.info("Inserting new Account records for current test...");
    //        for (String name : ACCOUNT_NAMES) {
    //            Account account = new Account();
    //            account.setName(name);
    //            em.persist(account);
    //        }
    //        utx.commit();
    //        
    //        // clear the persistence context (first-level cache)
    //        em.clear();
    //    }

    @Test
    public void shouldFindAllAccountsUsingJpqlQuery() throws Exception {

        LOG.info("Inserting new Account records for current test...");
        for (String name : ACCOUNT_NAMES) {
            Account account = new Account();
            account.setName(name);
            accountDao.create(account);
        }

        // given
        String fetchAllAccountsInJpql = "select a from Account a order by a.id";

        // when
        LOG.info("Selecting all Account records (using JPQL)...");
        List<Account> accounts = em.createQuery(fetchAllAccountsInJpql, Account.class).getResultList();

        // then
        LOG.info("Found " + accounts.size() + " accounts (using JPQL):");
        assertContainsAllAccounts(accounts);
    }

    private static void assertContainsAllAccounts(Collection<Account> retrievedAccounts) {
        Assert.assertEquals(ACCOUNT_NAMES.length, retrievedAccounts.size());
        final Set<String> retrievedAccountNames = new HashSet<String>();
        for (Account account : retrievedAccounts) {
            LOG.info("* " + account);
            retrievedAccountNames.add(account.getName());
        }
        Assert.assertTrue(retrievedAccountNames.containsAll(Arrays.asList(ACCOUNT_NAMES)));
    }
}
