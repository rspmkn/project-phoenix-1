package com.apd.phoenix.service.persistence.jpa;

import javax.inject.Inject;
import org.jboss.arquillian.junit.Arquillian;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.model.LineItemStatus;
import com.apd.phoenix.service.model.MockDataFactory.LineItemStatusFactory;
import com.apd.phoenix.service.model.MockDataFactory.OrderStatusFactory;
import com.apd.phoenix.service.model.OrderStatus;

@RunWith(Arquillian.class)
public class OrderLogDaoTest extends DaoTest {

    private static final Logger LOG = LoggerFactory.getLogger(OrderLogDaoTest.class);

    //    @Inject
    //    private EventTypeDao eventTypeDao;

    @Inject
    private OrderStatusDao orderStatusDao;

    @Inject
    private LineItemStatusDao lineItemStatusDao;

    @Inject
    private CustomerOrderDao orderDao;

    @Inject
    private AddressTypeDao orderLogDao;

    @Test
    public void testIsDeployed() {
        //Assert.assertNotNull(eventTypeDao);
        Assert.assertNotNull(orderStatusDao);
        Assert.assertNotNull(orderDao);
        Assert.assertNotNull(orderLogDao);
    }

    @Test
    public void testGetAllOrderLogsUsingJpqlQuery() throws Exception {

        LOG.info("Inserting data for test...");
        /*
        EventType quickOrderAckSentToCustomer = eventTypeDao.create(EventTypeFactory
                .stub(EventTypeEnum.QUICK_ORDER_ACK_RECEIVED.toString()));
        EventType orderSentToVendor = eventTypeDao.create(EventTypeFactory.stub(EventTypeEnum.ORDER_SENT.toString()));
        EventType quickOrderAckReceivedFromVendor = eventTypeDao.create(EventTypeFactory
                .stub(EventTypeEnum.QUICK_ORDER_ACK_RECEIVED.toString()));
        EventType orderAckReceivedFromVendor = eventTypeDao.create(EventTypeFactory
                .stub(EventTypeEnum.ORDER_ACK_RECEIVED.toString()));
        EventType lineItemOrdered = eventTypeDao.create(EventTypeFactory
                .stub(EventTypeEnum.LINEITEM_ORDERED.toString()));
        EventType lineItemBackOrdered = eventTypeDao.create(EventTypeFactory.stub(EventTypeEnum.LINEITEM_BACKORDERED
                .toString()));
         */
        OrderStatus ordered = orderStatusDao.create(OrderStatusFactory.stub(OrderStatusEnum.ORDERED.toString()));
        OrderStatus orderSent = orderStatusDao.create(OrderStatusFactory
                .stub(OrderStatusEnum.SENT_TO_VENDOR.toString()));
        OrderStatus orderAccepted = orderStatusDao.create(OrderStatusFactory.stub(OrderStatusEnum.ACCEPTED_BY_VENDOR
                .toString()));
        LineItemStatus itemOrdered = lineItemStatusDao.create(LineItemStatusFactory.stub(LineItemStatusEnum.ORDERED
                .toString()));
        LineItemStatus itemBackOrdered = lineItemStatusDao.create(LineItemStatusFactory
                .stub(LineItemStatusEnum.BACKORDERED.toString()));

        //        // given
        //        String fetchAllAccountsInJpql = "select a from Account a order by a.id";
        //
        //        // when
        //        LOG.info("Selecting all Account records (using JPQL)...");
        //        List<Account> accounts = em.createQuery(fetchAllAccountsInJpql, Account.class).getResultList();
        //
        //        // then
        //        LOG.info("Found " + accounts.size() + " accounts (using JPQL):");
        //       // assertContainsAllAccounts(accounts);
    }

    @Test
    public void testGetAllByOrderId() {

        //    	addressTypeDao.create(addressType);
        //        AddressType persistedAddressType = addressTypeDao.findById(addressType.getId(), AddressType.class);

        //       Assert.assertEquals(persistedAddressType.getName(), name);
    }
}
