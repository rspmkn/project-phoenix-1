package com.apd.phoenix.service.model;

import javax.inject.Inject;
import org.jboss.arquillian.junit.Arquillian;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(Arquillian.class)
public class CashoutPageTest extends EntityTest {

    @Inject
    private CashoutPage cashoutpage;

    @Test
    public void testIsDeployed() {
        Assert.assertNotNull(cashoutpage);
    }
}
