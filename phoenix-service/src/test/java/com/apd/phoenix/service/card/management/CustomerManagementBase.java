/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.service.card.management;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author dnorris
 */
public class CustomerManagementBase extends CardManagerBaseTestNG {

    private static final Logger logger = LoggerFactory.getLogger(CustomerManagementBase.class);

    protected CreateCustomerParams firstCustomerParams;
    protected CreateCustomerParams secondCustomerParams;
    protected Customer firstCustomer;
    protected Customer secondCustomer;

    protected void cleanUpBeforeTest() throws Exception {
        CustomerIdentifier customerIdentifier = factory.createCustomerIdentifier();
        customerIdentifier.setCustomerCode(factory.createCustomerIdentifierCustomerCode(CUSTOMER_CODE_1));
        customerIdentifier.setMerchantCode(factory.createCustomerIdentifierMerchantCode(merchantCode));
        customerIdentifier.setLocationCode(factory.createCustomerIdentifierLocationCode(locationCode));
        DeleteCustomerParams deleteCustomerParams = factory.createDeleteCustomerParams();
        deleteCustomerParams.setCustomerIdentifier(factory
                .createDeleteCustomerParamsCustomerIdentifier(customerIdentifier));
        port.deleteCustomer(clientCredentials, deleteCustomerParams);
        customerIdentifier = factory.createCustomerIdentifier();
        customerIdentifier.setCustomerCode(factory.createCustomerIdentifierCustomerCode(CUSTOMER_CODE_2));
        customerIdentifier.setMerchantCode(factory.createCustomerIdentifierMerchantCode(merchantCode));
        customerIdentifier.setLocationCode(factory.createCustomerIdentifierLocationCode(locationCode));
        deleteCustomerParams = factory.createDeleteCustomerParams();
        deleteCustomerParams.setCustomerIdentifier(factory
                .createDeleteCustomerParamsCustomerIdentifier(customerIdentifier));
        port.deleteCustomer(clientCredentials, deleteCustomerParams);
    }

    protected void initFirstTestCustomer() {
        firstCustomer = factory.createCustomer();
        firstCustomer.setBillingAddress(factory.createCustomerBillingAddress(createValidAddress()));
        firstCustomer.setName(factory.createCustomerName("Bob's Hardware"));
        firstCustomer.setCode(factory.createCustomerCode(CUSTOMER_CODE_1));
    }

    protected void initFirstCustomerParams() {
        firstCustomerParams = factory.createCreateCustomerParams();
        firstCustomerParams.setLocationIdentifier(factory
                .createCreateCustomerParamsLocationIdentifier(locationIdentifier));
        firstCustomerParams.setCustomer(factory.createCreateCustomerParamsCustomer(firstCustomer));
    }

    protected void initSecondTestCustomer() {
        secondCustomer = factory.createCustomer();
        secondCustomer.setBillingAddress(factory.createCustomerBillingAddress(createValidAddress()));
        secondCustomer.setName(factory.createCustomerName("John Doe"));
        secondCustomer.setCode(factory.createCustomerCode(CUSTOMER_CODE_2));
    }

    protected void initSecondCustomerParams() {
        secondCustomerParams = factory.createCreateCustomerParams();
        secondCustomerParams.setLocationIdentifier(factory
                .createCreateCustomerParamsLocationIdentifier(locationIdentifier));
        secondCustomerParams.setCustomer(factory.createCreateCustomerParamsCustomer(secondCustomer));
    }

}
