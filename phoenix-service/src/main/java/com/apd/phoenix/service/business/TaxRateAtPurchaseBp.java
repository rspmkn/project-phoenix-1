package com.apd.phoenix.service.business;

import java.util.HashSet;
import java.util.Set;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import com.apd.phoenix.service.model.CustomerOrder;
import com.apd.phoenix.service.model.TaxRate;
import com.apd.phoenix.service.model.TaxRateAtPurchase;
import com.apd.phoenix.service.model.TaxType.TaxTypeEnum;
import com.apd.phoenix.service.persistence.jpa.TaxRateAtPurchaseDao;

/**
 * This class provides business process methods for TaxRateAtPurchase.
 * 
 * @author RHC
 *
 */
@Stateless
@LocalBean
public class TaxRateAtPurchaseBp extends AbstractBp<TaxRateAtPurchase> {

    @Inject
    private TaxTypeBp taxTypeBp;

    /**
     * This method is injected with the correct Dao for initialization
     * 
     * @param dao
     */
    @Inject
    public void initDao(TaxRateAtPurchaseDao dao) {
        this.dao = dao;
    }

    public Set<TaxRateAtPurchase> getRateSet(TaxRate rate, CustomerOrder order) {
    	Set<TaxRateAtPurchase> toReturn = new HashSet<>();
    	if (rate == null) {
    		return toReturn;
    	}
    	for (TaxTypeEnum taxTypeEnum : TaxTypeEnum.values()) {
    		TaxRateAtPurchase toAdd = new TaxRateAtPurchase();
    		toAdd.setValue(rate.getFromTaxType(taxTypeEnum));
    		toAdd.setType(taxTypeBp.getByName(taxTypeEnum.getDbValue()));
    		if (toAdd.getValue() != null) {
	    		toReturn.add(toAdd);
    		}
    	}
    	return toReturn;
    }
}
