package com.apd.phoenix.service.model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Version;
import org.hibernate.annotations.Index;

@Entity
public class SyncItemResultSummary implements Serializable, com.apd.phoenix.service.model.Entity {

    /** The id. */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id = null;

    /** The version. */
    @Version
    @Column(name = "version", nullable = false)
    private int version = 0;

    //the number of items created by the upload process
    private int created = 0;
    //the number of items updated by the upload process
    private int updated = 0;
    //the number of items discontinued by the upload process
    private int discontinued = 0;
    //the number of items that failed to be changed/created
    private int failures = 0;
    //the number of errors for all the items
    //(an item with three errors will increment "failures" once, and "errors" three times)
    private int errors = 0;
    //the number of warnings for all the items
    private int warnings = 0;
    //the number of warnings for all the items
    private int zeroPrice = 0;

    @Index(name = "sirs_corrId")
    private String correlationId;
    
    @Column
    private int total = 0;
    
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "summary")
    private Set<SyncItemResult> results = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public void setCreated(int created) {
        this.created = created;
    }

    public void setUpdated(int updated) {
        this.updated = updated;
    }

    public void setDiscontinued(int discontinued) {
        this.discontinued = discontinued;
    }

    public void setFailures(int failures) {
        this.failures = failures;
    }

    public void setErrors(int errors) {
        this.errors = errors;
    }

    public void setWarnings(int warnings) {
        this.warnings = warnings;
    }

    public void setZeroPrice(int zeroPrice) {
        this.zeroPrice = zeroPrice;
    }

    public int getCreated() {
        return created;
    }

    public void incrementCreated() {
        this.created++;
    }

    public int getUpdated() {
        return updated;
    }

    public void incrementUpdated() {
        this.updated++;
    }

    public int getDiscontinued() {
        return discontinued;
    }

    public void incrementDiscontinued() {
        this.discontinued++;
    }

    public int getFailures() {
        return failures;
    }

    public void incrementFailures() {
        this.failures++;
    }

    public int getErrors() {
        return errors;
    }

    public void incrementErrors() {
        this.errors++;
    }

    public int getWarnings() {
        return warnings;
    }

    public void incrementWarnings() {
        this.warnings++;
    }

    public int getZeroPrice() {
        return zeroPrice;
    }

    public void incrementZeroPrice() {
        this.zeroPrice++;
    }

    public void setValues(Object[] values) {
        this.created = ((Long) values[0]).intValue();
        this.discontinued = ((Long) values[1]).intValue();
        this.errors = ((Long) values[2]).intValue();
        this.failures = ((Long) values[3]).intValue();
        this.updated = ((Long) values[4]).intValue();
        this.warnings = ((Long) values[5]).intValue();
        this.zeroPrice = ((Long) values[6]).intValue();
    }

    public String getCorrelationId() {
        return correlationId;
    }

    public void setCorrelationId(String correlationId) {
        this.correlationId = correlationId;
    }

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public Set<SyncItemResult> getResults() {
		return results;
	}

	public void setResults(Set<SyncItemResult> results) {
		this.results = results;
	}
}
