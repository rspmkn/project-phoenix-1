/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.service.business;

import com.apd.phoenix.service.model.PhoneNumber;
import com.apd.phoenix.service.persistence.jpa.PhoneNumberDao;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import org.apache.commons.lang.StringUtils;

/**
 *
 * @author dnorris
 */
@Stateless
@LocalBean
public class PhoneNumberBp extends AbstractBp<PhoneNumber> {

    private PhoneNumberDao dao;

    /**
     * This method is injected with the correct Dao for initialization
     *
     * @param dao
     */
    @Inject
    public void initDao(PhoneNumberDao dao) {
        super.initAbstract(dao);
        this.dao = dao;
    }

    public void setReadableNumber(PhoneNumber number, String readableNumber) {
        String phoneWithoutArea = "";
        if (readableNumber.startsWith("(")) {
            number.setCountryCode("1");
            number.setAreaCode(readableNumber.substring(1, 4));
            phoneWithoutArea = readableNumber.substring(6);
        }
        else {
            number.setCountryCode(readableNumber.substring(0, 1));
            number.setAreaCode(readableNumber.substring(2, 5));
            phoneWithoutArea = readableNumber.substring(6);
        }
        number.setExchange(phoneWithoutArea.substring(0, 3));
        number.setLineNumber(phoneWithoutArea.substring(4, 8));
    }

    public String getReadableNumber(PhoneNumber number) {
        if (number == null || StringUtils.isBlank(number.getCountryCode()) || StringUtils.isBlank(number.getAreaCode())
                || StringUtils.isBlank(number.getExchange()) || StringUtils.isBlank(number.getLineNumber())) {
            return null;
        }
        return number.getCountryCode().concat("-").concat(number.getAreaCode()).concat("-")
                .concat(number.getExchange()).concat("-").concat(number.getLineNumber());
    }

    public String getReadableNumberRegex() {
        return "^((\\([0-9][0-9][0-9]\\) )|([0-9]\\-[0-9][0-9][0-9]\\-))[0-9][0-9][0-9]\\-[0-9][0-9][0-9][0-9]$";
    }
}
