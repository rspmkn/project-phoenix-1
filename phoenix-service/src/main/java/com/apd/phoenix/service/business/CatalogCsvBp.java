package com.apd.phoenix.service.business;

import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import com.apd.phoenix.service.model.CatalogCsv;
import com.apd.phoenix.service.persistence.jpa.CatalogCsvDao;

@Stateless
@LocalBean
public class CatalogCsvBp extends AbstractBp<CatalogCsv> {

    /**
     * This method is injected with the correct Dao for initialization
     * 
     * @param dao
     */
    @Inject
    public void initDao(CatalogCsvDao dao) {
        this.dao = dao;
    }

    public boolean existsByCorrelationId(String correlationId) {
        return ((CatalogCsvDao) dao).existsByCorrelationId(correlationId);
    }

    public CatalogCsv getByCorrelationId(String correlationId) {
        return ((CatalogCsvDao) dao).getByCorrelationId(correlationId);
    }

    public Long getTotalItems(String correlationId) {
        return ((CatalogCsvDao) dao).getTotalItems(correlationId);
    }

    public void removeRecords(String correlationId) {
        ((CatalogCsvDao) dao).removeRecords(correlationId);
    }

    public List<String> getCorrelationId(long catalogId) {
        return ((CatalogCsvDao) dao).getCorrelationId(catalogId);
    }

    public boolean isFinished(String correlationId) {
        CatalogCsv catalogCsv = getByCorrelationId(correlationId);
        if (catalogCsv != null) {
            return (catalogCsv.getTotalItems().compareTo(catalogCsv.getProcessedItems().longValue()) == 0);
        }
        else {
            return false;
        }
    }

    public void wrapUp(String correlationId) {

    }
}
