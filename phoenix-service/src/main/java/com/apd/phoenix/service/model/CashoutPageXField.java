/*
 * 
 */
package com.apd.phoenix.service.model;

import java.io.Serializable;
import java.util.Comparator;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Version;
import javax.xml.bind.annotation.XmlRootElement;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import com.apd.phoenix.service.business.EntityComparator.GenericComparator;

/**
 * The Class CashoutPageXFieldType.
 */
@Entity
@XmlRootElement
@Audited
public class CashoutPageXField implements Serializable, com.apd.phoenix.service.model.Entity {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -7648310635316378909L;

    /** The id. */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id = null;

    /** The version. */
    @Version
    @Column(name = "version", nullable = false)
    private int version = 0;

    /** The required. */
    @Column(nullable = false)
    private boolean required;

    /** The label. */
    @Column
    private String label;

    /** The required. */
    @Column(nullable = false)
    private boolean updatable = true;

    /** The default value. */
    @Column
    private String defaultValue;

    @Column
    private String inboundMapping;

    @ManyToOne(fetch = FetchType.EAGER)
    @NotAudited
    private MessageMapping mapping;

    @Column
    private Boolean includeLabelInMapping;

    /** The field type. */
    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    @Fetch(FetchMode.JOIN)
    @NotAudited
    private Field field;

    /**
     * Gets the id.
     *
     * @return the id
     */
    public Long getId() {
        return this.id;
    }

    /**
     * Sets the id.
     *
     * @param id the new id
     */
    public void setId(final Long id) {
        this.id = id;
    }

    /**
     * Gets the version.
     *
     * @return the version
     */
    public int getVersion() {
        return this.version;
    }

    /**
     * Sets the version.
     *
     * @param version the new version
     */
    public void setVersion(final int version) {
        this.version = version;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        if (id != null) {
            return id.equals(((CashoutPageXField) that).id);
        }
        return super.equals(that);
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        if (id != null) {
            return id.hashCode();
        }
        return super.hashCode();
    }

    /**
     * Gets the required.
     *
     * @return the required
     */
    public boolean getRequired() {
        return this.required;
    }

    /**
     * Sets the required.
     *
     * @param required the new required
     */
    public void setRequired(final boolean required) {
        this.required = required;
    }

    /**
     * Gets the label.
     *
     * @return the label
     */
    public String getLabel() {
        return this.label;
    }

    /**
     * Sets the label.
     *
     * @param label the new label
     */
    public void setLabel(final String label) {
        this.label = label;
    }

    /**
     * Gets whether the field is updatable.
     *
     * @return updatable
     */
    public boolean getUpdatable() {
        return this.updatable;
    }

    /**
     * Sets whether the field is updatable.
     *
     * @param updatable
     */
    public void setUpdatable(final boolean updatable) {
        this.updatable = updatable;
    }

    /**
     * Gets the default value.
     *
     * @return the default value
     */
    public String getDefaultValue() {
        return this.defaultValue;
    }

    /**
     * Sets the default value.
     *
     * @param defaultValue the new default value
     */
    public void setDefaultValue(final String defaultValue) {
        this.defaultValue = defaultValue;
    }

    public String getInboundMapping() {
        return this.inboundMapping;
    }

    public void setInboundMapping(final String inboundMapping) {
        this.inboundMapping = inboundMapping;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "CashoutPageXFieldType [version=" + version + ", required=" + required + ", defaultValue="
                + defaultValue + "]";
    }

    /**
     * Gets the field.
     *
     * @return the field
     */
    public Field getField() {
        return this.field;
    }

    /**
     * Sets the field.
     *
     * @param field the new field
     */
    public void setField(final Field field) {
        this.field = field;
    }

    public MessageMapping getMapping() {
        return this.mapping;
    }

    public void setMapping(final MessageMapping mapping) {
        this.mapping = mapping;
    }

    public Boolean getIncludeLabelInMapping() {
        return this.includeLabelInMapping;
    }

    public void setIncludeLabelInMapping(Boolean includeLabel) {
        this.includeLabelInMapping = includeLabel;
    }

    public static class CashoutPageXFieldComparator implements Comparator<CashoutPageXField> {

        @Override
        public int compare(CashoutPageXField field0, CashoutPageXField field1) {

            if (field0.getField() != null && field1.getField() != null) {
                int result = field0.getField().getName().compareTo(field1.getField().getName());
                if (result == 0 && field0.getMapping() != null && field1.getMapping() != null) {
                    result = field0.getMapping().getName().compareTo(field1.getMapping().getName());
                }
                return result;
            }
            if (field0.getField() != null) {
                return -1;
            }
            if (field1.getField() != null) {
                return 1;
            }
            return (new GenericComparator()).compare(field0, field1);
        }
    }
}