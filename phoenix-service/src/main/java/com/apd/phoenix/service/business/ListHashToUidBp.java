package com.apd.phoenix.service.business;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import com.apd.phoenix.service.model.ListHashToUid;
import com.apd.phoenix.service.persistence.jpa.ListHashToUidDao;

@Stateless
@LocalBean
public class ListHashToUidBp extends AbstractBp<ListHashToUid> {

    private static final int HOURS_UNTIL_LIST_IS_READY = 12;

    /**
     * This method is injected with the correct Dao for initialization
     * 
     * @param dao
     */
    @Inject
    public void initDao(ListHashToUidDao dao) {
        this.dao = dao;
    }

    /**
     * Determines if the list is ready to be used. Returns true if the creation date of 
     * the list is more than "HOURS_UNTIL_LIST_IS_READY" hours before now, if the date 
     * is null sets the creation date to "now" and returns false, and returns false in 
     * all other cases.
     * 
     * @param listUid
     * @return
     */
    public boolean isListReady(ListHashToUid listUid) {
        if (listUid != null) {
            if (listUid.getCreationDate() == null) {
                listUid.setCreationDate(new Date());
                this.update(listUid);
                return false;
            }
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(listUid.getCreationDate());
            calendar.add(Calendar.HOUR, HOURS_UNTIL_LIST_IS_READY);
            return calendar.getTime().before(new Date());
        }
        return false;
    }

    public ListHashToUid searchByHash(Long hash) {
        if (hash == null) {
            return null;
        }

        ListHashToUid searchUid = new ListHashToUid();
        searchUid.setHash(hash);
        List<ListHashToUid> results = this.searchByExactExample(searchUid, 0, 0);
        if (results != null && !results.isEmpty()) {
            return results.get(0);
        }
        return null;
    }

    public void deleteUnusedUids() {
        ((ListHashToUidDao) this.dao).deleteUnusedUids();
    }

    public List<String> getAllUids() {
    	List<String> toReturn = new ArrayList<>();
    	for (Long uid : ((ListHashToUidDao)this.dao).getAllUids()) {
    		toReturn.add(uid + "");
    	}
    	return toReturn;
    }
}
