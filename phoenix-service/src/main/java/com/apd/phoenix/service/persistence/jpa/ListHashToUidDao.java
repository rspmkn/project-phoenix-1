package com.apd.phoenix.service.persistence.jpa;

import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.Query;
import com.apd.phoenix.service.model.ListHashToUid;

/**
 * ListHashToUid DAO stub
 * 
 * @author RHC
 *
 */
@Stateless
@LocalBean
public class ListHashToUidDao extends AbstractDao<ListHashToUid> {

    public void deleteUnusedUids() {
        String hql = "DELETE FROM ListHashToUid AS uid WHERE "
                + "NOT EXISTS (SELECT cat.id FROM Catalog AS cat WHERE cat.listUid.id = uid.id) AND "
                + "NOT EXISTS (SELECT cat.id FROM Catalog AS cat WHERE cat.coreListUid.id = uid.id) AND "
                + "NOT EXISTS (SELECT cat.id FROM Catalog AS cat WHERE cat.itemHash = uid.hash) AND "
                + "NOT EXISTS (SELECT cat.id FROM Catalog AS cat WHERE cat.coreItemHash = uid.hash))";
        Query query = entityManager.createQuery(hql);
        query.executeUpdate();
    }

    @SuppressWarnings("unchecked")
    public List<Long> getAllUids() {

        String hql = "SELECT ID FROM ListHashToUid";
        Query query = entityManager.createQuery(hql);
        return query.getResultList();
    }
}
