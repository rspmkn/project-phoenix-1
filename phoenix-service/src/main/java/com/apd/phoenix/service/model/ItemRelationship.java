/*
 * 
 */
package com.apd.phoenix.service.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.xml.bind.annotation.XmlRootElement;
import org.hibernate.envers.Audited;

/**
 * The Class ItemRelationship.
 */
@Table(name = "ITEM_ITEM")
@Entity
@XmlRootElement
public class ItemRelationship implements Serializable, com.apd.phoenix.service.model.Entity {

    private static final long serialVersionUID = -3136838418461602685L;

    /** The id. */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id = null;

    /** The version. */
    @Version
    @Column(name = "version", nullable = false)
    private int version = 0;

    @Column(nullable = false)
    private int position;

    @Column(nullable = false)
    private String relationship;

    @ManyToOne(optional = false)
    @JoinColumn(name = "ITEM_ID")
    private Item original;

    @ManyToOne(optional = false)
    @JoinColumn(name = "SIMILARITEMS_ID")
    private Item relative;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public String getRelationship() {
        return relationship;
    }

    public void setRelationship(String relationship) {
        this.relationship = relationship;
    }

    public Item getOriginal() {
        return original;
    }

    public void setOriginal(Item original) {
        this.original = original;
    }

    public Item getRelative() {
        return relative;
    }

    public void setRelative(Item relative) {
        this.relative = relative;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        if (id != null) {
            return id.equals(((ItemRelationship) that).id);
        }
        return super.equals(that);
    }

}