package com.apd.phoenix.service.model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.Version;
import org.hibernate.annotations.IndexColumn;
import org.hibernate.envers.Audited;
/**
 *
 * @author dnorris
 */
@Entity
@Audited
public class CxmlConfiguration implements Serializable, com.apd.phoenix.service.model.Entity{
    
    private static final long serialVersionUID = -8172093234881252694L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id = null;
    @Version
    @Column(name = "version", nullable = false)
    private int version = 0;
    
    //Used to identify this conifiguration for search purposes
    @Column(unique = true)
    private String name;

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinTable(name="to_credential_table")
    @IndexColumn(base = 1, name = "tnr")
    private Set<CxmlCredential> toCredentials = new HashSet<>();
    
    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinTable(name="from_credential_table")
    @IndexColumn(base = 1, name = "fnr")
    private Set<CxmlCredential> fromCredentials = new HashSet<>();
    
    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinTable(name="sender_credential_table")
    @IndexColumn(base = 1, name = "snr")
    private Set<CxmlCredential> senderCredentials = new HashSet<>();

    @Column
    private String senderSharedSecret;

    @Column //e.g. for novant /cXML/Request/OrderRequest/OrderRequestHeader/Extrinsic[@name='Requesting Location Code']/text()
    private String systemUserXpathExpression;
    
    @Column(name="SYSTEMUSERXPATHEXPDEFRES")
    private String systemUserXpathExpressionDefaultResult;
    
    @Column
    private String systemUserPrefix;
    
    @Column 
    private String credentialNameXpathExpression;
    
    @Column
    private String credentialNamePrefix;
    
    @Enumerated(EnumType.STRING)
    private DeploymentMode deploymentMode = DeploymentMode.TEST;
    
    public enum DeploymentMode {

        TEST("test"),
        PRODUCTION("production");

        private final String label;

        private DeploymentMode(String label) {
            this.label = label;
        }

        public String getLabel() {
            return label;
        }
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSenderSharedSecret() {
        return senderSharedSecret;
    }

    public void setSenderSharedSecret(String senderSharedSecret) {
        this.senderSharedSecret = senderSharedSecret;
    }

    public String getSystemUserXpathExpression() {
        return systemUserXpathExpression;
    }

    public void setSystemUserXpathExpression(String systemUserXpathExpression) {
        this.systemUserXpathExpression = systemUserXpathExpression;
    }

    public DeploymentMode getDeploymentMode() {
        return deploymentMode;
    }

    public void setDeploymentMode(DeploymentMode deploymentMode) {
        this.deploymentMode = deploymentMode;
    }

    public Set<CxmlCredential> getToCredentials() {
        return toCredentials;
    }

    public void setToCredentials(Set<CxmlCredential> toCredentials) {
        this.toCredentials = toCredentials;
    }

    public Set<CxmlCredential> getFromCredentials() {
        return fromCredentials;
    }

    public void setFromCredentials(Set<CxmlCredential> fromCredentials) {
        this.fromCredentials = fromCredentials;
    }

    public Set<CxmlCredential> getSenderCredentials() {
        return senderCredentials;
    }

    public void setSenderCredentials(Set<CxmlCredential> senderCredentials) {
        this.senderCredentials = senderCredentials;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCredentialNameXpathExpression() {
        return credentialNameXpathExpression;
    }

    public void setCredentialNameXpathExpression(String credentialNameXpathExpression) {
        this.credentialNameXpathExpression = credentialNameXpathExpression;
    }

	public String getCredentialNamePrefix() {
		return credentialNamePrefix;
	}

	public void setCredentialNamePrefix(String credentialNamePrefix) {
		this.credentialNamePrefix = credentialNamePrefix;
	}

	public String getSystemUserXpathExpressionDefaultResult() {
		return systemUserXpathExpressionDefaultResult;
	}

	public void setSystemUserXpathExpressionDefaultResult(
			String systemUserXpathExpressionDefaultResult) {
		this.systemUserXpathExpressionDefaultResult = systemUserXpathExpressionDefaultResult;
	}

	public String getSystemUserPrefix() {
		return systemUserPrefix;
	}

	public void setSystemUserPrefix(String systemUserPrefix) {
		this.systemUserPrefix = systemUserPrefix;
	}
    
}
