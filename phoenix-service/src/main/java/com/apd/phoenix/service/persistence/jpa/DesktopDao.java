package com.apd.phoenix.service.persistence.jpa;

import com.apd.phoenix.service.model.AccountXCredentialXUser;
import com.apd.phoenix.service.model.Desktop;
import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.Query;

/**
 *
 * @author anicholson
 */
@Stateless
@LocalBean
public class DesktopDao extends AbstractDao<Desktop> {

    /**
     * Takes an AccountXCredentialXUser, and returns a List of Strings (desktops). If the 
     * AccountXCredentialXUser has at least one desktop, returns those desktops.
     * 
     * @param axcxu
     * @return
     */
    public List<Desktop> desktopsForCashout(AccountXCredentialXUser axcxu) {
        String hql = "SELECT desktops FROM AccountXCredentialXUser AS axcxu LEFT JOIN "
                + "axcxu.desktops AS desktops WHERE axcxu.id = :axcxuId";
        //Creates the query
        Query query = entityManager.createQuery(hql);
        query.setParameter("axcxuId", axcxu.getId());
        setItemsReturned(query, 0, 0);
        return query.getResultList();
    }
}
