package com.apd.phoenix.service.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Version;
import javax.xml.bind.annotation.XmlRootElement;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.envers.Audited;

/**
 * The Class PhoneNumber.
 */
@Entity
@XmlRootElement
@Audited
public class PhoneNumber implements Serializable, com.apd.phoenix.service.model.Entity {

    private static final long serialVersionUID = 3575072595407040302L;

    /** The area code. */
    @Column
    private String areaCode;

    /** The country code. */
    @Column
    private String countryCode;

    /** The exchange. */
    @Column
    private String exchange;

    /** The id. */
    @Id
    private @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false, nullable = false)
    Long id = null;

    /** The line number. */
    @Column
    private String lineNumber;

    /** The extension. */
    @Column
    private String extension;

    @Column
    private String value;

    /** The type. */
    @ManyToOne(fetch = FetchType.EAGER)
    @Fetch(FetchMode.JOIN)
    private PhoneNumberType type;

    /** The version. */
    @Version
    private @Column(name = "version", nullable = false)
    int version = 0;

    @ManyToOne
    private Person person;

    @ManyToOne
    private Account account;

    @ManyToOne
    private Vendor vendor;

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        if (id != null) {
            return id.equals(((PhoneNumber) that).id);
        }
        return super.equals(that);
    }

    /**
     * Gets the area code.
     *
     * @return the area code
     */
    public String getAreaCode() {
        return this.areaCode == null ? "" : this.areaCode;
    }

    /**
     * Gets the country code.
     *
     * @return the country code
     */
    public String getCountryCode() {
        return this.countryCode == null ? "" : this.countryCode;
    }

    /**
     * Gets the exchange.
     *
     * @return the exchange
     */
    public String getExchange() {
        return this.exchange == null ? "" : this.exchange;
    }

    /**
     * Gets the id.
     *
     * @return the id
     */
    public Long getId() {
        return this.id;
    }

    /**
     * Gets the line number.
     *
     * @return the line number
     */
    public String getLineNumber() {
        return this.lineNumber == null ? "" : this.lineNumber;
    }

    /**
     * Gets the extension.
     *
     * @return the extension
     */
    public String getExtension() {
        return this.extension == null ? "" : this.extension;
    }

    /**
     * Gets the type.
     *
     * @return the type
     */
    public PhoneNumberType getType() {
        return this.type;
    }

    /**
     * Gets the version.
     *
     * @return the version
     */
    public int getVersion() {
        return this.version;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        if (id != null) {
            return id.hashCode();
        }
        return super.hashCode();
    }

    /**
     * Sets the area code.
     *
     * @param areaCode the new area code
     */
    public void setAreaCode(final String areaCode) {
        this.areaCode = areaCode;
    }

    /**
     * Sets the country code.
     *
     * @param countryCode the new country code
     */
    public void setCountryCode(final String countryCode) {
        this.countryCode = countryCode;
    }

    /**
     * Sets the exchange.
     *
     * @param exchange the new exchange
     */
    public void setExchange(final String exchange) {
        this.exchange = exchange;
    }

    /**
     * Sets the id.
     *
     * @param id the new id
     */
    public void setId(final Long id) {
        this.id = id;
    }

    /**
     * Sets the line number.
     *
     * @param lineNumber the new line number
     */
    public void setLineNumber(final String lineNumber) {
        this.lineNumber = lineNumber;
    }

    /**
     * Sets the extension.
     *
     * @param extension the new extension
     */
    public void setExtension(final String extension) {
        this.extension = extension;
    }

    /**
     * Sets the type.
     *
     * @param type the new type
     */
    public void setType(final PhoneNumberType type) {
        this.type = type;
    }

    /**
     * Sets the version.
     *
     * @param version the new version
     */
    public void setVersion(final int version) {
        this.version = version;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        String result = "";
        if (countryCode != null && !countryCode.trim().isEmpty())
            result += countryCode;
        if (areaCode != null && !areaCode.trim().isEmpty())
            result += " " + areaCode;
        if (exchange != null && !exchange.trim().isEmpty())
            result += " " + exchange;
        if (lineNumber != null && !lineNumber.trim().isEmpty())
            result += " " + lineNumber;
        if (extension != null && !extension.trim().isEmpty())
            result += " " + extension;
        return result;
    }

    public Person getPerson() {
        return this.person;
    }

    public void setPerson(final Person person) {
        this.person = person;
    }

    public Account getAccount() {
        return this.account;
    }

    public void setAccount(final Account account) {
        this.account = account;
    }

    /**
     * @return the value
     */
    public String getValue() {
        if (this.value != null && !this.value.trim().equals("")) {
            return value;
        }
        String aggregate = "";

        if (!this.getExchange().equals("") && !this.getLineNumber().equals("")) {
            if (!this.getAreaCode().equals("")) {
                if (!this.getCountryCode().equals("")) {
                    aggregate += "+" + this.getCountryCode() + " ";
                }
                aggregate += "(" + this.getAreaCode() + ") ";
            }
            aggregate += this.getExchange() + "-" + this.getLineNumber();
            if (!this.getExtension().equals("")) {
                aggregate += " x" + this.getExtension();
            }
        }
        return aggregate;
    }

    /**
     * @param value the value to set
     */
    public void setValue(String value) {
        this.value = value;
    }

    public Vendor getVendor() {
        return this.vendor;
    }

    public void setVendor(final Vendor vendor) {
        this.vendor = vendor;
    }
}