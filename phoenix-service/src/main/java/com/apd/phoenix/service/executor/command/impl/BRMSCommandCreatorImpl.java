package com.apd.phoenix.service.executor.command.impl;

import java.util.List;
import java.util.Map;
import java.util.UUID;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import com.apd.phoenix.service.executor.ExecutorServiceEntryPoint;
import com.apd.phoenix.service.executor.api.CommandContext;
import com.apd.phoenix.service.executor.command.api.BRMSCommandCreator;
import com.apd.phoenix.service.executor.command.api.Command;
import com.apd.phoenix.service.executor.command.api.Command.Callback;
import com.apd.phoenix.service.executor.command.api.Command.Type;
import com.apd.phoenix.service.executor.command.api.CommandConstants;
import com.apd.phoenix.service.executor.command.api.SignalCommand;
import com.apd.phoenix.service.executor.command.api.StartProcessCommand;

@Stateless
@LocalBean
public class BRMSCommandCreatorImpl implements BRMSCommandCreator {

    @Inject
    protected ExecutorServiceEntryPoint executor;

    @Override
    public void sendStartProcess(StartProcessCommand.Name processName, Command.Type type, Long bussinessKey,
            Map<String, Object> processParams, List<Object> facts, Command.Callback callback) {
        executor.init();
        CommandContext context = new CommandContext();
        context.setData("processName", processName.toString());
        context.setData("commandKey", UUID.randomUUID().toString());
        if (callback != null) {
            context.setData("callbacks", callback.toString());
        }
        context.setData("idType", type.toString());
        context.setData("id", bussinessKey);
        context.setData("processParams", processParams);
        context.setData("facts", facts);
        executor.scheduleRequest(CommandConstants.START_PROCESS_COMMAND_BEAN, context);
        executor.destroy();
    }

    @Override
    public void sendSignalEvent(SignalCommand.Name eventName, Object message, Command.Type type, Long bussinessKey,
            List<Object> facts, Command.Callback callback) {
        executor.init();
        CommandContext context = new CommandContext();
        context.setData("commandKey", UUID.randomUUID().toString());
        if (callback != null) {
            context.setData("callbacks", callback.toString());
        }
        context.setData("idType", type.toString());
        context.setData("id", bussinessKey);
        context.setData("type", eventName.toString());
        context.setData("event", message);
        context.setData("facts", facts);
        executor.scheduleRequest(CommandConstants.SIGNAL_EVENT_COMMAND_BEAN, context);
        executor.destroy();
    }

    @Override
    public void sendCompleteWorkItem(Type type, Long bussinessKey, Long workItemID, Map<String, Object> responseData,
            List<Object> facts, Callback callback) {
        executor.init();
        CommandContext context = new CommandContext();

        context.setData("commandKey", UUID.randomUUID().toString());
        if (callback != null) {
            context.setData("callbacks", callback.toString());
        }
        context.setData("idType", type.toString());
        context.setData("id", bussinessKey);
        context.setData("workItemID", workItemID);
        context.setData("responseData", responseData);
        context.setData("facts", facts);
        executor.scheduleRequest(CommandConstants.COMPLETE_WI_COMMAND_BEAN, context);
        executor.destroy();
    }

    @Override
    public void completeTask(Long taskId, String userId, Map<String, Object> params, Command.Type type,
            Long bussinessKey) {
        executor.init();
        CommandContext context = new CommandContext();
        context.setData("commandKey", UUID.randomUUID().toString());
        context.setData("idType", type.toString());
        context.setData("id", bussinessKey);
        context.setData("contentDataMap", params);
        context.setData("taskId", taskId);
        context.setData("userId", userId);
        context.setData("facts", null);
        executor.scheduleRequest(CommandConstants.COMPLETE_TASK_COMMAND_BEAN, context);
        executor.destroy();
    }

}
