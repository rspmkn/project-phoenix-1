package com.apd.phoenix.service.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

/**
 * This entity contains the information relevant for an Inventory Location details.
 * 
 * @author FWS-Muthu
 * 
 */
@Entity
public class InventoryLocation implements Serializable, com.apd.phoenix.service.model.Entity {

    private static final long serialVersionUID = 7185662040258275545L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id = null;

    @Column
    private String locationName;

    @Column
    private String defaultLocation;

    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    private InventorySite inventorySite;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLocationName() {
        return locationName;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }

    public String getDefaultLocation() {
        return defaultLocation;
    }

    public void setDefaultLocation(String defaultLocation) {
        this.defaultLocation = defaultLocation;
    }

    public InventorySite getInventorySite() {
        return inventorySite;
    }

    public void setInventorySite(InventorySite inventorySite) {
        this.inventorySite = inventorySite;
    }

}