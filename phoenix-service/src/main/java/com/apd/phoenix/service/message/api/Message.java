package com.apd.phoenix.service.message.api;

import java.io.InputStream;
import com.apd.phoenix.service.model.MessageMetadata;

public class Message {

    private MessageMetadata metadata;
    private InputStream content;

    public MessageMetadata getMetadata() {
        return metadata;
    }

    public void setMetadata(MessageMetadata metadata) {
        this.metadata = metadata;
    }

    public InputStream getContent() {
        return content;
    }

    public void setContent(InputStream content) {
        this.content = content;
    }

}
