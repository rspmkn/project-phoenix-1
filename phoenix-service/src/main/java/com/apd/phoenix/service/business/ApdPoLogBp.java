package com.apd.phoenix.service.business;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import com.apd.phoenix.service.model.ApdPoLog;
import com.apd.phoenix.service.model.PoNumber;
import com.apd.phoenix.service.persistence.jpa.ApdPoLogDao;

@Stateless
@LocalBean
public class ApdPoLogBp extends AbstractBp<ApdPoLog> {

    @Inject
    private PoNumberTypeBp poNumberTypeBp;

    @Inject
    public void initDao(ApdPoLogDao dao) {
        this.dao = dao;
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public PoNumber createLogAndApdPo(String apdPo) {
        PoNumber poNumber = new PoNumber();
        poNumber.setValue(apdPo);
        poNumber.setType(poNumberTypeBp.getApdPoType());
        ApdPoLog log = new ApdPoLog();
        log.setApdPo(poNumber);
        return this.update(log).getApdPo();
    }
}