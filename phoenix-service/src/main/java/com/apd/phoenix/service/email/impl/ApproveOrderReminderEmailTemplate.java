package com.apd.phoenix.service.email.impl;

import java.io.IOException;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import javax.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.core.StringEscape;
import com.apd.phoenix.service.business.CustomerOrderBp;
import com.apd.phoenix.service.business.CustomerOrderRequestBp;
import com.apd.phoenix.service.email.api.Attachment;
import com.apd.phoenix.service.email.api.EmailTemplate;
import com.apd.phoenix.service.email.api.NoAttachmentContentException;
import com.apd.phoenix.service.model.CustomerOrder;
import com.apd.phoenix.service.model.dto.PurchaseOrderDto;
import freemarker.template.TemplateException;

public class ApproveOrderReminderEmailTemplate extends EmailTemplate<PurchaseOrderDto> {

    private static final String TEMPLATE = "approve.order.reminder";

    public static final int DAYS_UNTIL_ORDER_IS_DENIED_AUTOMATICALLY = 14;

    public static final int DAYS_UNTIL_ORDER_STOPS_TRYING_TO_DENY = 21;

    @Inject
    CustomerOrderRequestBp customerOrderRequestBp;

    @Inject
    CustomerOrderBp customerOrderBp;

    private static final Logger LOGGER = LoggerFactory.getLogger(ApproveOrderReminderEmailTemplate.class);

    @Override
	public String createBody(PurchaseOrderDto purchaseOrderDto) throws IOException,
			TemplateException {
		Map<String, Object> params = new HashMap<>();
		
		params.put("orderDate", purchaseOrderDto.getOrderDate());
		
		params.put("billingAddress", this.getSafeAddress(purchaseOrderDto.lookupBillToAddressDto()));
		params.put("shippingAddress", this.getSafeAddress(purchaseOrderDto.getShipToAddressDto()));
		
		LOGGER.info("Getting token");
		CustomerOrder customerOrder = customerOrderBp.searchByApdPo(purchaseOrderDto.getApdPoNumber());
		String token="";
		if (customerOrder != null){
			token = customerOrderRequestBp.createRequestToken(customerOrder);
		}
		String approver = customerOrderBp.getApprover(customerOrder);
		String authority = this.getProperty("tenant", "default_public_domain",  "http://localhost:8080");
		LOGGER.info("Token {}",token);
		
		params.put("approveUrl",authority + "/ws/workflow/order/approve/" + StringEscape.escapeForUrl(approver) + "/" + token);
		params.put("denyUrl",authority + "/ws/workflow/order/deny/" + StringEscape.escapeForUrl(approver) + "/" + token);
		
		params.put("merchandiseTotal", purchaseOrderDto.getSubTotal());	
		params.put("shippingTotal", purchaseOrderDto.getEstimatedShippingAmount());	
		params.put("taxTotal", purchaseOrderDto.getMaximumTaxToCharge());	
		params.put("orderTotal", purchaseOrderDto.getOrderTotal());
		params.put("items", purchaseOrderDto.getItems());
		
		Calendar dueDate = Calendar.getInstance();
		dueDate.setTime(customerOrderBp.getDateRequestedApproval(customerOrder));
		dueDate.add(Calendar.DAY_OF_YEAR, DAYS_UNTIL_ORDER_IS_DENIED_AUTOMATICALLY);
		Format formatter = new SimpleDateFormat("yyyy-MM-dd");
		params.put("dueDate", formatter.format(dueDate.getTime()));
		
		
		if (customerOrder != null && customerOrder.getUser() != null && customerOrder.getUser().getPerson() != null) {
			params.put("userName", customerOrder.getUser().getPerson().getFormalName());
			params.put("userEmail", customerOrder.getUser().getPerson().getEmail());
		}
		
		return this.create(params);
	}

    @Override
    public String getTemplate() {
        return ApproveOrderReminderEmailTemplate.TEMPLATE;
    }

    @Override
    public Attachment createAttachment(PurchaseOrderDto content) throws NoAttachmentContentException {
        return null;
    }

}
