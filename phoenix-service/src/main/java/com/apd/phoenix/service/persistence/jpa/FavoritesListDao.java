package com.apd.phoenix.service.persistence.jpa;

import com.apd.phoenix.service.model.CatalogXItem;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import com.apd.phoenix.service.model.FavoritesList;
import com.google.common.primitives.Ints;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Query;
import org.hibernate.Session;

@Stateless
@LocalBean
public class FavoritesListDao extends AbstractDao<FavoritesList> {

    private static final String ITEM_SET_HQL = "SELECT catalogXItem FROM CatalogXItem catalogXItem "
            + "WHERE (catalogXItem.id IN (SELECT userItems.id FROM FavoritesList f JOIN f.userItems AS userItems WHERE f.id = :favoritesListID) "
            + "OR catalogXItem.id IN (SELECT item.id FROM CatalogXItem item JOIN item.favorites AS list where list.id = :favoritesListID)) "
            + "AND COALESCE((SELECT availability.value FROM CatalogXItem item JOIN item.properties AS availability "
            + "WHERE item.id = catalogXItem.id AND availability.type.name = 'availability'), 'yes') != 'no' "
            + "ORDER BY catalogXItem.id";

    @SuppressWarnings("unchecked")
	public Set<CatalogXItem> getInitializedSet(Long favoritesListID) {
        //Creates the query
        Query query = entityManager.createQuery(ITEM_SET_HQL);
        query.setParameter("favoritesListID", favoritesListID);
        Set<CatalogXItem> toReturn = new HashSet<>(query.getResultList());
        return toReturn;
    }

    @SuppressWarnings("unchecked")
	public Set<CatalogXItem> getInitializedSet(Long favoritesListID, int start, int quantity) {
        //Creates the query
        Query query = entityManager.createQuery(ITEM_SET_HQL);
        query.setParameter("favoritesListID", favoritesListID);
        setItemsReturned(query, start, quantity);
        Set<CatalogXItem> toReturn = new HashSet<>(query.getResultList());
        return toReturn;
    }

    public int getListItemCount(Long favoritesListID) {
        String hql = "SELECT count(catalogXItem) FROM FavoritesList f " + "LEFT JOIN f.userItems as catalogXItem "
                + "WHERE f.id = :favoritesListID";
        String companyHql = "SELECT count(catalogXItem) FROM CatalogXItem catalogXItem "
                + "JOIN catalogXItem.favorites as favorites " + "WHERE favorites.id = :favoritesListID";
        //Creates the query
        Query query = entityManager.createQuery(hql);
        query.setParameter("favoritesListID", favoritesListID);
        Long count = (Long) query.getSingleResult();
        Query companyQuery = entityManager.createQuery(companyHql);
        companyQuery.setParameter("favoritesListID", favoritesListID);
        Long companyCount = (Long) companyQuery.getSingleResult();
        return Ints.checkedCast(count + companyCount);
    }

    public FavoritesList getHydratedFavoritesList(FavoritesList list) {
        if (list == null) {
            return null;
        }
        String hql = "SELECT list FROM FavoritesList list LEFT JOIN FETCH list.userItems WHERE list=:list";
        Query query = entityManager.createQuery(hql);
        query.setParameter("list", list);
        FavoritesList toReturn = (FavoritesList) query.getSingleResult();
        return toReturn;
    }

    public void removeEmptyCompanyLists(Long catalogId) {
        if (catalogId == null) {
            return;
        }
        Session session = (Session) entityManager.getDelegate();
        String hql = "DELETE FROM FavoritesList WHERE id not in ("
                + "    SELECT FAVXCXI.FAVORITESLIST_ID FROM CatalogXItem c "
                + "    JOIN comp_favlist_catxitem AS FAVXCXI ON FAVXCXI.ITEMS_ID = C.ID "
                + "    WHERE c.catalog_id = :catalogId) " + "AND catalog_id = :catalogId";
        org.hibernate.Query emptyFavoritesDeleteQuery = session.createSQLQuery(hql);
        emptyFavoritesDeleteQuery.setParameter("catalogId", catalogId);
        emptyFavoritesDeleteQuery.executeUpdate();
        return;
    }

}
