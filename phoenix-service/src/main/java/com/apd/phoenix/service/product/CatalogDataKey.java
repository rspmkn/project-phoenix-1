package com.apd.phoenix.service.product;

import java.io.Serializable;

/**
 * Presentation layer wrapper for storing product data pulled from solr results
 * or db directly
 *
 * @author RHC
 */
public class CatalogDataKey implements Serializable {

    private static final long serialVersionUID = 1L;

    private long customerCatalogId;
    private String searchType;
    private long tenantId;

    /**
     * This constructor should not be used in application code.
     */
    @Deprecated
    public CatalogDataKey() {
    }

    public CatalogDataKey(long customerCatalogId, String searchType, long tenantId) {
        this.customerCatalogId = customerCatalogId;
        this.searchType = searchType;
        this.tenantId = tenantId;
    }

    public long getCustomerCatalogId() {
        return customerCatalogId;
    }

    public void setCustomerCatalogId(long customerCatalogId) {
        this.customerCatalogId = customerCatalogId;
    }

    public String getSearchType() {
        return searchType;
    }

    public void setSearchType(String searchType) {
        this.searchType = searchType;
    }

    public long getTenantId() {
        return tenantId;
    }

    public void setTenantId(long tenantId) {
        this.tenantId = tenantId;
    }

}
