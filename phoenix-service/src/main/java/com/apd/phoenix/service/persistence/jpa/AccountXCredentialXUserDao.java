package com.apd.phoenix.service.persistence.jpa;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.Query;
import org.apache.commons.lang.StringUtils;
import com.apd.phoenix.service.model.AccountXCredentialXUser;
import com.apd.phoenix.service.model.FavoritesList;
import com.apd.phoenix.service.model.SystemUser;
import com.google.common.primitives.Ints;

/**
 * AccountXCredentialXUser DAO stub
 *
 * @author RHC
 *
 */
@Stateless
@LocalBean
public class AccountXCredentialXUserDao extends AbstractDao<AccountXCredentialXUser> {

    /**
     * Takes a user, and returns all of the accountXCredentialXUsers for that user. The only value that is fetched on axcxu is the credential, which is
     * displayed in the dropdown.
     *
     * @param user
     * @return
     */
    @SuppressWarnings("unchecked")
    public List<AccountXCredentialXUser> getAllAXCXU(SystemUser user) {
        String hql = "SELECT DISTINCT toReturn FROM AccountXCredentialXUser toReturn JOIN FETCH toReturn.user AS user "
                + "JOIN FETCH toReturn.credential AS cred JOIN FETCH toReturn.account AS acc LEFT JOIN FETCH "
                + "toReturn.allowedCostCenters LEFT JOIN FETCH cred.ips LEFT JOIN FETCH acc.ips "
                + "WHERE user.id = :userId AND toReturn.active = true";
        //Creates the query
        Query query = entityManager.createQuery(hql);
        query.setParameter("userId", user.getId());
        return query.getResultList();
    }

    @SuppressWarnings("unchecked")
    public List<AccountXCredentialXUser> getAllAXCXUByLogin(String login) {
        String hql = "SELECT DISTINCT toReturn FROM AccountXCredentialXUser toReturn JOIN FETCH toReturn.user AS user "
                + "JOIN FETCH toReturn.credential AS cred JOIN FETCH toReturn.account AS acc LEFT JOIN FETCH "
                + "toReturn.allowedCostCenters WHERE lower(user.login) = :login AND toReturn.active = true";
        //Creates the query
        Query query = entityManager.createQuery(hql);
        query.setParameter("login", login.toLowerCase());
        return query.getResultList();
    }

    /**
     * Returns a single AccountXCredentialXUser which represents the association between a customer Account, the credential used by that account, and 
     * the generic SystemUser that is created for that account (should be only one generic user for any given account or sub account).  This is primarily 
     * used to authorize and validate inbound procurement platform orders that provide only the business key for a child/sub account in Phoenix (e.g. the sector code 
     * provided in shipto portion of the inbound XCBL Order documents for NGC and HII, which uniquely identifies a "Sector" child account under the root
     * Northrup Grumman customer account)  
     * 
     * @param assignedAccountId
     * @return
     */
    public AccountXCredentialXUser findGenericAXCXUByAssignedAccountId(String assignedAccountId) {
        String hql = "SELECT DISTINCT toReturn FROM AccountXCredentialXUser toReturn JOIN FETCH toReturn.user AS user "
                + "JOIN FETCH toReturn.credential AS cred JOIN FETCH toReturn.account AS acc "
                + "WHERE user.concurrentAccess = TRUE AND acc.apdAssignedAccountId = :accountId AND toReturn.active = true";
        //Creates the query
        Query query = entityManager.createQuery(hql);
        query.setParameter("accountId", assignedAccountId);

        return getSingleResultOrNull(query);
    }

    /**
     * Takes an ID, and returns the AccountXCredentialXUser with that ID, fetching the credential.
     *
     * @param id
     * @return
     */
    public AccountXCredentialXUser getAXCXUForConverter(Long id) {
        String hql = "SELECT toReturn FROM AccountXCredentialXUser toReturn "
                + "JOIN FETCH toReturn.credential WHERE toReturn.id = :credId";
        //Creates the query
        Query query = entityManager.createQuery(hql);
        query.setParameter("credId", id);
        return (AccountXCredentialXUser) query.getSingleResult();
    }

    public int getFavoritesListCount(Long accountXCredentialXUserId) {
        String hql = "SELECT count(toReturn) FROM AccountXCredentialXUser a " + "LEFT JOIN a.favorites AS toReturn "
                + "WHERE a.id = :accountXCredentialXUserId";
        //Creates the query
        Query query = entityManager.createQuery(hql);
        query.setParameter("accountXCredentialXUserId", accountXCredentialXUserId);
        Long count = (Long) query.getSingleResult();
        return Ints.checkedCast(count);
    }

    public AccountXCredentialXUser getUserForFavoritesManagement(Long accountXCredentialXUserId) {
        String hql = "SELECT toReturn FROM AccountXCredentialXUser toReturn "
                + "LEFT JOIN FETCH toReturn.favorites WHERE toReturn.id = :accountXCredentialXUserId";
        //Creates the query
        Query query = entityManager.createQuery(hql);
        query.setParameter("accountXCredentialXUserId", accountXCredentialXUserId);
        return (AccountXCredentialXUser) query.getSingleResult();
    }

    @SuppressWarnings("unchecked")
    public List<String> getFavoritesListNames(Long accountXCredentialXUserId) {
        String hql = "SELECT toReturn.name FROM AccountXCredentialXUser a " + "JOIN a.favorites as toReturn "
                + "WHERE a.id = :accountXCredentialXUserId";
        //Creates the query
        Query query = entityManager.createQuery(hql);
        query.setParameter("accountXCredentialXUserId", accountXCredentialXUserId);
        return query.getResultList();
    }

    /**
     * Takes an ID, and returns the FavoritesLists associated with that ID, fetching the credential.
     *
     * @param accountXCredentialXUserId
     * @param start 
     * @param quantity 
     * @return List<FavoritesList>
     */
    @SuppressWarnings("unchecked")
    public List<FavoritesList> getInitializedFavoritesLists(Long accountXCredentialXUserId, int start, int quantity) {
        String hql = "SELECT DISTINCT toReturn FROM AccountXCredentialXUser a " + "LEFT JOIN a.favorites AS toReturn "
                + "WHERE a.id = :accountXCredentialXUserId ORDER BY UPPER(toReturn.name)";
        //Creates the query
        Query query = entityManager.createQuery(hql);
        query.setParameter("accountXCredentialXUserId", accountXCredentialXUserId);
        setItemsReturned(query, start, quantity);
        return query.getResultList();
    }

    /**
     * Takes an ID, and returns the FavoritesLists associated with that ID, fetching the credential.
     *
     * @param accountXCredentialXUserId
     * @return List<FavoritesList>
     */
    public List<FavoritesList> getInitializedFavoritesLists(long accountXCredentialXUserId) {
        return getInitializedFavoritesLists(accountXCredentialXUserId, 0, 0);
    }

    public List<FavoritesList> getUnInitializedFavoritesLists(Long accountXCredentialXUserId){
        String hql = "SELECT DISTINCT a FROM AccountXCredentialXUser a "
                + "LEFT JOIN FETCH a.favorites "
                + " WHERE a.id = :accountXCredentialXUserId";
        //Creates the query
        Query query = entityManager.createQuery(hql);
        query.setParameter("accountXCredentialXUserId", accountXCredentialXUserId);
        AccountXCredentialXUser user = (AccountXCredentialXUser) query.getSingleResult();
        return new ArrayList<>(user.getFavorites());
	}

    public FavoritesList getFavoritesListByNameAndCredential(String name, AccountXCredentialXUser axcxu) {
        if (StringUtils.isBlank(name) || axcxu == null) {
            return null;
        }
        String hql = "SELECT toReturn FROM AccountXCredentialXUser a " + "LEFT JOIN a.favorites AS toReturn "
                + " WHERE a.id = :accountXCredentialXUserId AND toReturn.name = :favoritesName";
        //Creates the query
        Query query = entityManager.createQuery(hql);
        query.setParameter("accountXCredentialXUserId", axcxu.getId());
        query.setParameter("favoritesName", name);
        FavoritesList list = (FavoritesList) getSingleResultOrNull(query);
        return list;
    }

    /**
     * Takes an ID, and returns a AccountXCredentialXUser with data loaded for order creation
     *
     * @param accountXCredentialXUserId
     * @return 
     */
    public AccountXCredentialXUser getAccountXCredentialXUserForBrowse(Long accountXCredentialXUserId) {
        String hql = "SELECT a FROM AccountXCredentialXUser a "
                + "JOIN FETCH a.credential AS cred LEFT JOIN FETCH cred.catalog JOIN FETCH a.account AS acc JOIN FETCH a.user AS user "
                + "LEFT JOIN FETCH cred.properties LEFT JOIN FETCH cred.roles LEFT JOIN FETCH cred.permissions LEFT JOIN FETCH cred.ips JOIN FETCH user.person "
                + "LEFT JOIN FETCH acc.properties LEFT JOIN FETCH acc.ips LEFT JOIN FETCH acc.iconUrls WHERE a.id = :accountXCredentialXUserId";
        //Creates the query
        Query query = entityManager.createQuery(hql);
        query.setParameter("accountXCredentialXUserId", accountXCredentialXUserId);
        setItemsReturned(query, 0, 0);
        return (AccountXCredentialXUser) query.getSingleResult();
    }

}
