/*
 * 
 */
package com.apd.phoenix.service.model.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;

public class ItemDto implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 6095045718901321751L;
    public static final String UNSPSC_CLASSIFICATION_NAME = "unspsc";

    private UnitOfMeasureDto unitOfMeasure;

    private String name;

    private String searchTerms;

    private String description;

    private ItemDto abilityOneSubstitute;

    private Set<ItemImageDto> itemImages;

    private Map<String, String> properties = new HashMap<>();
    
    private Map<String, String> classifications;
    
    private Map<String,String> specifications;
    
    private List<SkuDto> skus = new ArrayList<>();
    
    private String manufacturerName;
    
    private String commodityCode;
    
    private BigDecimal itemWeight; 

    public ItemDto() {
        this.classifications = new HashMap<>();
    }

    private Set<ItemClassificationDto> itemClassifications = new HashSet<>();

    public UnitOfMeasureDto getUnitOfMeasure() {
        return unitOfMeasure;
    }

    public void setUnitOfMeasure(UnitOfMeasureDto unitOfMeasure) {
        this.unitOfMeasure = unitOfMeasure;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSearchTerms() {
        return searchTerms;
    }

    public void setSearchTerms(String searchTerms) {
        this.searchTerms = searchTerms;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ItemDto getAbilityOneSubstitute() {
        return abilityOneSubstitute;
    }

    public void setAbilityOneSubstitute(ItemDto abilityOneSubstitute) {
        this.abilityOneSubstitute = abilityOneSubstitute;
    }

    /**
     * @return the itemImages
     */
    public Set<ItemImageDto> getItemImages() {
        return itemImages;
    }

    /**
     * @param itemImages
     *            the itemImages to set
     */
    public void setItemImages(Set<ItemImageDto> itemImages) {
        this.itemImages = itemImages;
    }

    /**
     * @return the properties
     */
    public Map<String, String> getProperties() {
        return properties;
    }

    /**
     * @param properties the properties to set
     */
    public void setProperties(Map<String, String> properties) {
        this.properties = properties;
    }

    /**
     * @return the skus
     */
    public List<SkuDto> getSkus() {
        return skus;
    }

    /**
     * @param skus the skus to set
     */
    public void setSkus(List<SkuDto> skus) {
        this.skus = skus;
    }
    
    public String lookupApdSku(){
        return lookupSpecificSku("dealer");
    }
    
    public String lookupManufacturerSku(){
        return lookupSpecificSku("manufacturer");
    }
    public String lookupVendorSku(){
        return lookupSpecificSku("vendor");
    }
                                               
    public String lookupCustomerSku(){
        return lookupSpecificSku("customer");
    }
    
    public String lookupSupplierSku(){
        return lookupSpecificSku("supplier");
    }

    private String lookupSpecificSku(String skuType) {
        if(skus!=null){
            for(SkuDto dto:skus){
                if(dto.getType().getName().equals(skuType)){
                    return dto.getValue();
                }
            }
        }
        return null;
    }

    /**
     * @return the manufacturerName
     */
    public String getManufacturerName() {
        return manufacturerName;
    }

    /**
     * @param manufacturerName the manufacturerName to set
     */
    public void setManufacturerName(String manufacturerName) {
        this.manufacturerName = manufacturerName;
    }
    
    public String lookupUnspcClassification(){
        String toReturn = getCommodityCode();
        if (StringUtils.isEmpty(toReturn)) {
            for (String type : getClassifications().keySet()) {
                if (type.toLowerCase().equals(UNSPSC_CLASSIFICATION_NAME)) {
                    toReturn = getClassifications().get(type);
                    return toReturn;
                }
            }
        }
        return toReturn; 
   }

    /**
     * @return the commodityCode
     */
    public String getCommodityCode() {
        return commodityCode;
    }

    /**
     * @param commodityCode the commodityCode to set
     */
    public void setCommodityCode(String commodityCode) {
        this.commodityCode = commodityCode;
    }

    /**
     * @return the classifications
     */
    public Map<String, String> getClassifications() {
        return classifications;
    }

    /**
     * @param classifications the classifications to set
     */
    public void setClassifications(Map<String, String> classifications) {
        this.classifications = classifications;
    }
    

    public Set<ItemClassificationDto> getItemClassifications() {
        return itemClassifications;
    }

    public void setItemClassifications(Set<ItemClassificationDto> itemClassifications) {
        this.itemClassifications = itemClassifications;
    }

    /**
     * @return the specifications
     */
    public Map<String,String> getSpecifications() {
        return specifications;
    }

    /**
     * @param specifications the specifications to set
     */
    public void setSpecifications(Map<String,String> specifications) {
        this.specifications = specifications;
    }

	public BigDecimal getItemWeight() {
		return itemWeight;
	}

	public void setItemWeight(BigDecimal itemWeight) {
		this.itemWeight = itemWeight;
	}

}
