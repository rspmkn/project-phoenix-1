package com.apd.phoenix.service.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.xml.bind.annotation.XmlRootElement;
import org.hibernate.annotations.Index;

@Entity
@XmlRootElement
public class Brand {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false, nullable = false)
    private long id;

    @Column
    @Index(name = "BRAND_ID_IX")
    private String usscoId;

    @Column
    @Index(name = "BRAND_NAME_IX")
    private String name;

    @Column
    private String logo;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getUsscoId() {
        return usscoId;
    }

    public void setUsscoId(String usscoId) {
        this.usscoId = usscoId;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object object) {
        if (object instanceof Brand) {
            Brand brand = (Brand) object;
            if (((this.getName() == null && brand.getName() == null) || (this.getName().equals(brand.getName())))
                    && ((this.getUsscoId() == null && brand.getUsscoId() == null) || (this.getUsscoId().equals(brand
                            .getUsscoId())))
                    && ((this.getLogo() == null && brand.getLogo() == null) || (this.getLogo().equals(brand.getLogo())))) {
                return true;
            }
        }
        return false;
    }
}
