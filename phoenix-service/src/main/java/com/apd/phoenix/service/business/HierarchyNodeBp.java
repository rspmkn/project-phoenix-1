package com.apd.phoenix.service.business;

import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.jms.JMSException;
import javax.naming.NamingException;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.model.HierarchyNode;
import com.apd.phoenix.service.model.Item;
import com.apd.phoenix.service.model.listener.ItemListener;
import com.apd.phoenix.service.persistence.jpa.HierarchyNodeDao;
import com.apd.phoenix.service.persistence.jpa.ItemDao;

/**
 * This class provides business process methods for HierarchyNode.
 * 
 * @author RHC
 * 
 */
@Stateless
@LocalBean
public class HierarchyNodeBp extends AbstractBp<HierarchyNode> {

    private static final Logger LOGGER = LoggerFactory.getLogger(HierarchyNodeBp.class);

    private HierarchyNodeDao dao;

    @Inject
    private ItemDao itemDao;

    /**
     * This method is injected with the correct Dao for initialization
     * 
     * @param dao
     */
    @Inject
    public void initDao(HierarchyNodeDao dao) {
        super.initAbstract(dao);
        this.dao = dao;
    }

    public List<HierarchyNode> getChildren(Long id) {
        HierarchyNode test = dao.findById(id, HierarchyNode.class);
        return this.dao.getChildren(test);
    }

    public HierarchyNode update(HierarchyNode hierarchyNode) {

        if (hierarchyNode.getId() == null) {
            if (hierarchyNode.getParent() != null) {
                hierarchyNode.setIndexedPaths(hierarchyNode.getParent().getIndexedPaths() + "!"
                        + hierarchyNode.getDescription());
            }
            else {
                hierarchyNode.setIndexedPaths("!" + hierarchyNode.getDescription());
            }
            hierarchyNode = super.update(hierarchyNode);
        }
        else {
            HierarchyNode originalHierarchyNode = this.dao.findById(hierarchyNode.getId(), HierarchyNode.class);
            if (originalHierarchyNode != null
                    && !originalHierarchyNode.getDescription().equals(hierarchyNode.getDescription())) {
                updateIndexedPaths(hierarchyNode, hierarchyNode.getDescription(), originalHierarchyNode
                        .getDescription());
            }
            else if (StringUtils.isNotEmpty(hierarchyNode.getType()) && originalHierarchyNode != null
                    && originalHierarchyNode.getType() == null ? true : !originalHierarchyNode.getType().equals(
                    hierarchyNode.getType())) {
                super.update(hierarchyNode);
            }
        }
        return dao.findById(hierarchyNode.getId(), HierarchyNode.class);

    }

    private void updateIndexedPaths(HierarchyNode hierarchyNode, String newDesc, String oldDesc) {

        List<HierarchyNode> hierarchyNodes = this.dao.getChildren(hierarchyNode);

        StringBuilder str = new StringBuilder(hierarchyNode.getIndexedPaths());
        int startIndex = str.indexOf(oldDesc);
        int length = oldDesc.length();
        if (startIndex >= 0) {
            String indexedPaths = str.replace(startIndex, startIndex + length, newDesc).toString();
            hierarchyNode.setIndexedPaths(indexedPaths);
            super.update(hierarchyNode);
            List<Item> items = itemDao.findItemsByHierarchyNode(hierarchyNode.getId());
            for (Item item : items) {
            	try {
            		ItemListener.addUpdateSolrDocument(item.getId());                
            	} catch (NamingException | JMSException e) {
					if (LOGGER.isDebugEnabled()) {
						LOGGER.debug("Error sending item update", e);
					}
				}
            }
        }

        for (HierarchyNode node : hierarchyNodes) {
            updateIndexedPaths(node, newDesc, oldDesc);
        }
    }
}
