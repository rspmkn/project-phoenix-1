package com.apd.phoenix.service.model;

import java.io.Serializable;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Version;
import org.hibernate.annotations.Index;

/**
 * This entity lists which list hash maps to list UID name
 * 
 * @author RHC
 *
 */
@Entity
@Cacheable
public class PendingSmartSearchDeletion implements Serializable, com.apd.phoenix.service.model.Entity {

    /**
     * 
     */
    private static final long serialVersionUID = 2521322791832072056L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id = null;
    @Version
    @Column(name = "version", nullable = false)
    private int version = 0;

    @ManyToOne(fetch = FetchType.EAGER)
    @Index(name = "PENDINGDELETION_CATALOG_IDX")
    private Catalog catalog;

    @ManyToOne(fetch = FetchType.EAGER)
    @Index(name = "PENDINGDELETION_ITEM_IDX")
    private Item item;

    @Column
    private Long hash;

    public Long getId() {
        return this.id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public int getVersion() {
        return this.version;
    }

    public void setVersion(final int version) {
        this.version = version;
    }

    public Catalog getCatalog() {
        return catalog;
    }

    public void setCatalog(Catalog catalog) {
        this.catalog = catalog;
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    public Long getHash() {
        return hash;
    }

    public void setHash(Long hash) {
        this.hash = hash;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        if (id != null) {
            return id.equals(((PendingSmartSearchDeletion) that).id);
        }
        return super.equals(that);
    }

    @Override
    public int hashCode() {
        if (id != null) {
            return id.hashCode();
        }
        return super.hashCode();
    }

    @Override
    public String toString() {
        String result = "";
        if (id != null) {
            result += id;
        }
        return result;
    }

}
