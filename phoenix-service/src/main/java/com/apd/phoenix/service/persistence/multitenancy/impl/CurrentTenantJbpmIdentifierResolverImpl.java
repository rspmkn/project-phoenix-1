package com.apd.phoenix.service.persistence.multitenancy.impl;

import java.io.Serializable;
import org.hibernate.context.spi.CurrentTenantIdentifierResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.core.utility.TenantConfigRepository;

public class CurrentTenantJbpmIdentifierResolverImpl implements Serializable, CurrentTenantIdentifierResolver {

    private static Logger LOG = LoggerFactory.getLogger(CurrentTenantJbpmIdentifierResolverImpl.class);

    /**
     * 
     */
    private static final long serialVersionUID = 7931804618647016263L;

    @Override
    public String resolveCurrentTenantIdentifier() {
        String tenantSchema = null;
        Long tenantId = CurrentTenantIdentifierResolverImpl.getCurrentTenant();
        LOG.debug("Current tenant id is " + tenantId);
        try {
            tenantSchema = TenantConfigRepository.getInstance().getTenantPropertiesById(tenantId).getJbpmSchemaName();
        }
        catch (final Exception e) {
            LOG.error("Error finding current tenant.", e);
        }
        LOG.debug("Current Tenant Schema is {}", tenantSchema);
        return tenantSchema;
    }

    @Override
    public boolean validateExistingCurrentSessions() {
        return true;
    }

}
