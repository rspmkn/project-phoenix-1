package com.apd.phoenix.service.model;

import java.io.Serializable;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Version;
import org.hibernate.envers.Audited;

/**
 * This entity stores the different types of properties that an Account has. It
 * has a one-to-many relationship with AccountProperty. Properties that every
 * Account must have include customer service name, customer service phone
 * number, customer service email address, and whether to charge the restocking
 * fee associated with a catalog.
 * 
 * @author RHC
 * 
 */
@Entity
@Cacheable
@Audited
public class AccountPropertyType implements Serializable, com.apd.phoenix.service.model.Entity {

    private static final long serialVersionUID = 6524579316864702558L;

    /**
     * This is an enum listing the different properties that are associated with Account.
     * 
     * @author RHC
     *
     */
    public enum AccountPropertyTypeEnum {
        ACCOUNT_ID("Accounting ID"), BILLTO_NAME("BillTo Name"), CAN_EDIT_PO("Can Edit Customer PO"), HIDE_USER_REGISTRATION_LINK(
                "hide user registration link"), INVOICE_BATCH("Invoice Batch"), MIN_ORDER_AMT("minimum order amount"), MIN_ORDER_FEE(
                "minimum order fee"), SALESPERSON("Salesperson"), SHIPTO_ID_PREFIX("authorized shipto prefix"), SUBDOMAIN(
                "Subdomain"), USER_REG_DOMAIN("user registration domain");

        String value;

        private AccountPropertyTypeEnum() {
            this.value = this.name();
        }

        private AccountPropertyTypeEnum(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id = null;
    @Version
    @Column(name = "version", nullable = false)
    private int version = 0;

    @Column(nullable = false, unique = true)
    private String name;

    public Long getId() {
        return this.id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public int getVersion() {
        return this.version;
    }

    public void setVersion(final int version) {
        this.version = version;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        if (id != null) {
            return id.equals(((AccountPropertyType) that).id);
        }
        return super.equals(that);
    }

    @Override
    public int hashCode() {
        if (id != null) {
            return id.hashCode();
        }
        return super.hashCode();
    }

    public String getName() {
        return this.name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        String result = "";
        if (name != null && !name.trim().isEmpty()) {
            result += name;
        }
        return result;
    }
}
