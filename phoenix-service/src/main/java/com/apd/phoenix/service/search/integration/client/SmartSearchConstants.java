package com.apd.phoenix.service.search.integration.client;

public class SmartSearchConstants {

    public static final String IS_USSCO_CATALOG = "isUsscoCatalog";
    public static final String DEALER_KEYWORD_DELIMETER = "@$";
    public static final String VENDOR_NAME_SMARTSEARCH_INDICATOR = "DealerIndicator01";
}
