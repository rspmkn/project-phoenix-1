package com.apd.phoenix.service.product;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import com.apd.phoenix.service.model.Manufacturer;

/**
 * Information from the Catalog required at login
 */
public class CatalogData implements Serializable {

    private static final long serialVersionUID = 8234507956520434046L;

    private Serializable categoryData;
    private Collection<Manufacturer> matchbookManufacturers;
    private ArrayList<FavoritesListInfo> companyFavoritesLists;

    public Serializable getCategoryData() {
        return categoryData;
    }

    public void setCategoryData(Serializable categoryData) {
        this.categoryData = categoryData;
    }

    public Collection<Manufacturer> getMatchbookManufacturers() {
        return matchbookManufacturers;
    }

    public void setMatchbookManufacturers(Collection<Manufacturer> matchbookManufacturers) {
        this.matchbookManufacturers = matchbookManufacturers;
    }

    public ArrayList<FavoritesListInfo> getCompanyFavoritesLists() {
        return companyFavoritesLists;
    }

    public void setCompanyFavoritesLists(ArrayList<FavoritesListInfo> companyFavoritesLists) {
        this.companyFavoritesLists = companyFavoritesLists;
    }

    public static class FavoritesListInfo implements Serializable {

        private static final long serialVersionUID = -2885613826637006239L;

        private Long id;

        private String name;

        public Long getId() {
            return id;
        }

        public void setId(Long id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

    }
}
