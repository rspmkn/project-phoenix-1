package com.apd.phoenix.service.business;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import com.apd.phoenix.service.model.ServiceReason;
import com.apd.phoenix.service.persistence.jpa.ServiceReasonDao;

/**
 * This class provides business process methods for ContactMethod.
 * 
 * @author RHC
 *
 */
@Stateless
@LocalBean
public class ServiceReasonBp extends AbstractBp<ServiceReason> {

    @Inject
    ServiceReasonDao serviceReasonDao;

    /**
     * This method is injected with the correct Dao for initialization
     * 
     * @param dao
     */
    @Inject
    public void initDao(ServiceReasonDao dao) {
        this.dao = dao;
    }

}
