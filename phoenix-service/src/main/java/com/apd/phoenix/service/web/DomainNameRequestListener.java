package com.apd.phoenix.service.web;

import javax.faces.context.FacesContext;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class is configured to be called before the RESTORE_VIEW phase of any request, setting the
 * tenant ID from the domain. This is used in conjunction with DomainNameRequestFilter, which
 * is a web filter that sets the tenant ID. The listener isn't called for non-JSF calls (such
 * as rest requests), and the filter isn't called if the user isn't authenticated (like on the 
 * login pages), so both are necessary.
 * 
 * @author RHC
 *
 */
public class DomainNameRequestListener implements PhaseListener {

    private static final long serialVersionUID = 4707726469552725398L;

    private static Logger LOG = LoggerFactory.getLogger(DomainNameRequestListener.class);

    @Override
    public PhaseId getPhaseId() {
        return PhaseId.RESTORE_VIEW;
    }

    /**
     * Called to set the tenant schema before the restore view phase, since the webfilters
     * aren't called before login
     */
    @Override
    public void beforePhase(PhaseEvent event) {
        LOG.debug("Calling lifecycle listener to set domain");
        HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext()
                .getRequest();
        DomainNameTenantLogic.setTenantAndGetDomain(request);
    }

    @Override
    public void afterPhase(PhaseEvent event) {
        //do nothing
    }

}