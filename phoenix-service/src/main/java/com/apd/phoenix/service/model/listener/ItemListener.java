package com.apd.phoenix.service.model.listener;

import java.util.Date;
import javax.jms.JMSException;
import javax.naming.NamingException;
import javax.persistence.PostPersist;
import javax.persistence.PostRemove;
import javax.persistence.PostUpdate;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import org.apache.commons.lang.StringUtils;
import com.apd.phoenix.service.model.Item;

/**
 *
 * @author RHC
 */
public class ItemListener extends SolrEntityListener {

    @PrePersist
    @PreUpdate
    public void preUpdateEventListener(Item entity) {
        entity.setLastModified(new Date());
        try {
            String dealerSku = entity.getSku("dealer");
            String vendorSku = entity.getSku("vendor");
            if (StringUtils.isNotBlank(dealerSku)) {
                entity.setDealerSku(dealerSku);
            }
            if (StringUtils.isNotBlank(vendorSku)) {
                entity.setVendorSku(vendorSku);
            }
        }
        catch (Exception e) {
            //catching Lazy Init exceptions, in this case don't do anything
            //these values shouldn't change except when the item is created,
            //so we don't expect there to be problems if this fails to 
            //execute when an item is updated.
            logger.debug("Exception when trying to populate item values on persist or update");
        }
    }

    @PostPersist
    @PostUpdate
    public void addUpdateItem(final Item entity) throws NamingException, JMSException {
        if (entity.isSendUpdateMessage()) {
            doSolrDocumentAction(entity.getId(), SolrDocumentAction.ITEM_UPDATE);
        }
        entity.setSendUpdateMessage(false);
    }

    public static void addUpdateSolrDocument(final Long id) throws NamingException, JMSException {
        doSolrDocumentAction(id, SolrDocumentAction.ITEM_UPDATE);
    }

    @PostRemove
    public void deleteItem(final Item entity) throws JMSException, NamingException {
        doSolrDocumentAction(entity.getId(), SolrDocumentAction.ITEM_DELETE);
    }
}
