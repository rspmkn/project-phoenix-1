package com.apd.phoenix.service.invoice;

import java.math.BigDecimal;

public class BilledLineItem extends MessageLineItem {

    /**
     * 
     */
    private static final long serialVersionUID = -2305889413054550048L;

    public BilledLineItem() {
        super();
    }

    public BilledLineItem(int quantity, String vendorSku, String apdSku, BigDecimal price, String status) {
        super(quantity, vendorSku, apdSku, price, status);

    }

}
