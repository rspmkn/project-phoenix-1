package com.apd.phoenix.service.business;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import javax.ejb.Asynchronous;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import com.apd.phoenix.service.email.api.Attachment;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.message.impl.EmailMessageSender;
import com.apd.phoenix.service.model.PasswordResetEntry;
import com.apd.phoenix.service.model.SystemUser;
import com.apd.phoenix.service.persistence.jpa.PasswordResetEntryDao;
import com.apd.phoenix.service.persistence.multitenancy.impl.CurrentTenantIdentifierResolverImpl;
import com.apd.phoenix.core.EncryptionUtils;
import com.apd.phoenix.core.StringEscape;
import com.apd.phoenix.core.utility.TenantConfigRepository;

/**
 * This class provides business process methods for PasswordResetEntry.
 * 
 * @author RHC
 * 
 */
@Stateless
@LocalBean
public class PasswordResetEntryBp extends AbstractBp<PasswordResetEntry> {

    //the number of hours that a user can change their password
    private static final int PASSWORD_RESET_PERIOD = 24;
    private PasswordResetEntryDao dao;
    private static final Logger logger = LoggerFactory.getLogger(PasswordResetEntryBp.class);

    @Inject
    private EmailMessageSender messageSender;

    /**
     * This method is injected with the correct Dao for initialization
     * 
     * @param dao
     */
    @Inject
    public void initDao(PasswordResetEntryDao dao) {
        super.initAbstract(dao);
        this.dao = dao;
    }

    /**
     * This method takes the generated key of a password reset entry and the provided key on a password reset email, 
     * and returns the user whose password should be reset. If no user can be reset, returns null.
     * 
     * @param generatedKey - the generated identifier of the entry
     * @param emailKey - the key on the link in the password reset email
     * @return The user to have their password reset, or null if no such user.
     */
    public SystemUser userToReset(String generatedKey, String emailKey) {
        if (generatedKey == null || generatedKey.isEmpty() || emailKey == null || emailKey.isEmpty()) {
            return null;
        }

        //fetches the passwordResetEntry, based on the generated key
        PasswordResetEntry toCheck = this.findByGeneratedKey(generatedKey);

        //if no entry is found, returns null
        if (toCheck == null) {
            return null;
        }

        //if the password reset entry has expired, returns null
        if (toCheck.getExpirationDate().before(new Date())) {
            return null;
        }

        //generates a new hash, based on the given key and the found entry's salt
        String submittedHash = EncryptionUtils.hash(emailKey, toCheck.getSalt());

        //returns the user if the generated hash matches the hash of the found entry
        if (submittedHash.equals(toCheck.getHash())) {
            return toCheck.getUser();
        }
        else {
            return null;
        }
    }

    /**
     * This method takes a String login, and creates a password reset entity, returning whether the reset email was 
     * successfully sent.
     * 
     * @param user - System User Object
     * @return The true if the email was sent successfully, false otherwise.
     * @throws Exception
     */
    //tenant ID has to be set manually on asynchronous EJB methods, since
    //the tenant ID is stored in a static threadlocal variable, and 
    //@Asynchronous calls occur in a different thread.
    @Asynchronous
    public void scheduleReset(SystemUser user, String resetUrl, Long tenantId) throws Exception {
        CurrentTenantIdentifierResolverImpl.setCurrentTenant(tenantId);

        String userLogin = user.getLogin();

        //finds any password reset entry for this user, and removes them
        PasswordResetEntry searchEntry = new PasswordResetEntry();
        searchEntry.setUser(new SystemUser());
        searchEntry.getUser().setLogin(userLogin);
        List<PasswordResetEntry> searchEntryList = this.searchByExactExample(searchEntry, 0, 0);
        for (PasswordResetEntry entry : searchEntryList) {
            this.delete(entry.getId(), PasswordResetEntry.class);
        }

        //gets the information necessary for the reset email
        String email = user.getPerson().getEmail();
        String emailKey = EncryptionUtils.randomAlphanumeric();

        //creates the new PasswordResetEntry, storing the user, the salt, and the hash
        PasswordResetEntry newEntry = new PasswordResetEntry();
        newEntry.setUser(user);
        newEntry.setSalt(EncryptionUtils.salt());
        newEntry.setHash(EncryptionUtils.hash(emailKey, newEntry.getSalt()));

        //repeatedly generates new keys until one is found that hasn't been used.
        String generatedKey;
        searchEntry = new PasswordResetEntry();
        do {
            generatedKey = EncryptionUtils.randomAlphanumeric();
            searchEntry.setGeneratedKey(generatedKey);
        }
        while (this.searchByExactExample(searchEntry, 0, 0).size() != 0);
        //sets the generated key of the new entry to the found unique key
        newEntry.setGeneratedKey(generatedKey);

        //generates an expiration date for the day after the scheduling takes place
        Calendar c = Calendar.getInstance();
        c.setTime(new Date());
        c.add(Calendar.HOUR_OF_DAY, PASSWORD_RESET_PERIOD);
        //sets the expiration date to the day after
        newEntry.setExpirationDate(c.getTime());

        String noReplyAddress = TenantConfigRepository.getInstance().getPropertiesByTenantId(
                CurrentTenantIdentifierResolverImpl.getCurrentTenant(), "tenant")
                .getProperty("internal.emails.noreply");

        //Email Sender
        String body = resetUrl + "?genKey=" + StringEscape.escapeForUrl(generatedKey) + "&emailKey="
                + StringEscape.escapeForUrl(emailKey);

        messageSender.sendSimpleMessage(noReplyAddress, email, "Password Reset", body, new ArrayList<Attachment>());

        logger.info("***SUCCESS: Email Sent to User: " + user.getLogin() + " ***");
        logger.info("***SUCCESS: User Email value: " + user.getPerson().getEmail() + " ***");
        //creates the new PasswordResetEntry entity
        this.update(newEntry);
    }

    /**
     * This method takes a generated key, and returns the associated PasswordResetEntry
     * 
     * @param generatedKey - the generated key for the entry
     * @return The PasswordResetEntry entity with the specified key; null if there is no match.
     */
    public PasswordResetEntry findByGeneratedKey(String generatedKey) {
        //searches for a PasswordResetEntry whose generated key is generatedKey
        PasswordResetEntry search = new PasswordResetEntry();
        search.setGeneratedKey(generatedKey);
        List<PasswordResetEntry> searchList = this.dao.searchByExactExample(search, 0, 0);

        //if the result list is empty, there is no such entry, and returns null
        if (searchList.size() == 0) {
            return null;
        }

        //returns the result
        return searchList.get(0);
    }
}
