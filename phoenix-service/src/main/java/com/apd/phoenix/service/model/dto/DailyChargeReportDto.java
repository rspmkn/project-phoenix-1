/*
 * 
 */
package com.apd.phoenix.service.model.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class DailyChargeReportDto implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -4654385736068703930L;

    private String accountName;
    private String userName;
    private String custPr;
    private String custPo;
    private String apdPo;
    private String tranType;
    private String transNum;
    private Date ccExpire;
    private Boolean success;
    private String processor = "3dsi"; //TODO:  Currently hardcoded because they only have one processor and is not represented in datamodel.
    private String authNum;
    private String trackNum;
    private Integer lineItems;
    private BigDecimal transAmount;
    private BigDecimal salesTax;
    private BigDecimal shipping;
    private BigDecimal merch;
    private Date transDate;

    public DailyChargeReportDto(String accountName, String userName, String custPo, String apdPo, Date ccExpire,
            Boolean success, String trackNum, BigDecimal transAmount, BigDecimal salesTax, BigDecimal shipping,
            BigDecimal merch, Date transDate, String tranType, Integer lineItems) {
        this.accountName = accountName;
        this.userName = userName;
        this.custPo = custPo;
        this.apdPo = apdPo;
        this.tranType = tranType;
        this.ccExpire = ccExpire;
        this.success = success;
        this.trackNum = trackNum;
        this.lineItems = lineItems;
        this.transAmount = transAmount;
        this.salesTax = salesTax;
        this.shipping = shipping;
        this.merch = merch;
        this.transDate = transDate;
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getCustPr() {
        return custPr;
    }

    public void setCustPr(String custPr) {
        this.custPr = custPr;
    }

    public String getCustPo() {
        return custPo;
    }

    public void setCustPo(String custPo) {
        this.custPo = custPo;
    }

    public String getApdPo() {
        return apdPo;
    }

    public void setApdPo(String apdPo) {
        this.apdPo = apdPo;
    }

    public String getTranType() {
        return tranType;
    }

    public void setTranType(String tranType) {
        this.tranType = tranType;
    }

    public String getTransNum() {
        return transNum;
    }

    public void setTransNum(String transNum) {
        this.transNum = transNum;
    }

    @Deprecated
    public String getCcNum() {
        return null;
    }

    @Deprecated
    public void setCcNum(String ccNum) {
        //do nothing
    }

    public Date getCcExpire() {
        return ccExpire;
    }

    public void setCcExpire(Date ccExpire) {
        this.ccExpire = ccExpire;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public String getProcessor() {
        return processor;
    }

    public void setProcessor(String processor) {
        this.processor = processor;
    }

    public String getAuthNum() {
        return authNum;
    }

    public void setAuthNum(String authNum) {
        this.authNum = authNum;
    }

    public String getTrackNum() {
        return trackNum;
    }

    public void setTrackNum(String trackNum) {
        this.trackNum = trackNum;
    }

    public Integer getLineItems() {
        return lineItems;
    }

    public void setLineItems(Integer lineItems) {
        this.lineItems = lineItems;
    }

    public BigDecimal getTransAmount() {
        return transAmount;
    }

    public void setTransAmount(BigDecimal transAmount) {
        this.transAmount = transAmount;
    }

    public BigDecimal getSalesTax() {
        return salesTax;
    }

    public void setSalesTax(BigDecimal salesTax) {
        this.salesTax = salesTax;
    }

    public BigDecimal getShipping() {
        return shipping;
    }

    public void setShipping(BigDecimal shipping) {
        this.shipping = shipping;
    }

    public BigDecimal getMerch() {
        return merch;
    }

    public void setMerch(BigDecimal merch) {
        this.merch = merch;
    }

    public Date getTransDate() {
        return transDate;
    }

    public void setTransDate(Date transDate) {
        this.transDate = transDate;
    }

    public String getSuccessAsString() {
        if (this.success != null && this.success) {
            return "APPROVED";
        }
        else {
            return "FAILED";
        }
    }
}