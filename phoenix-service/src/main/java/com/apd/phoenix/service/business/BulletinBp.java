package com.apd.phoenix.service.business;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import com.apd.phoenix.service.model.AccountXCredentialXUser;
import com.apd.phoenix.service.model.Bulletin;
import com.apd.phoenix.service.model.SystemUser;
import com.apd.phoenix.service.persistence.jpa.BulletinDao;

@Stateless
@LocalBean
public class BulletinBp extends AbstractBp<Bulletin> {

    @Inject
    BulletinDao issueLogDao;

    @Inject
    public void initDao(BulletinDao dao) {
        this.dao = dao;
    }

    /**
     * This method returns the set of bulletins that are associated with the given AccountXCredentialXUser, 
     * that have not yet expired.
     * 
     * @param account
     * @param credential
     * @param user
     * @return
     */
    public List<Bulletin> getBulletinsToDisplay(AccountXCredentialXUser axcxu) {
        return ((BulletinDao) this.dao).getBulletinsToDisplay(axcxu);
    }

    /**
     * This method returns the set of bulletins that are associated with the User, 
     * that have not yet expired.
     * 
     * @param account
     * @param credential
     * @param user
     * @return
     */
    public List<Bulletin> getBulletinsToDisplay(SystemUser user) {
        return ((BulletinDao) this.dao).getBulletinsToDisplay(user);
    }

    public List<Bulletin> getAllGlobalBulletinsForAdministration() {
        BulletinDao bulletinDao = (BulletinDao) this.dao;
        List<Bulletin> bulletinsToReturn = bulletinDao.getCSRBulletins();
        bulletinsToReturn.addAll(bulletinDao.getGlobalBulletins());
        return bulletinsToReturn;
    }

    public List<Bulletin> getCSRBulletins() {
        List<Bulletin> csrBulletins = ((BulletinDao) this.dao).getCSRBulletins();
        if (csrBulletins == null) {
            return new ArrayList<Bulletin>();
        }
        return csrBulletins;
    }
}
