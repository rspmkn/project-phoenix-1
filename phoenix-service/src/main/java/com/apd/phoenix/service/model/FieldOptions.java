package com.apd.phoenix.service.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Version;
import org.apache.commons.lang.StringUtils;

/**
 * This entity is used to store the different values that can be selected for a multiple-choice field. So, 
 * if one of the fields on the cashout page is a dropdown, this entity is used to store the entries in the 
 * dropdown.
 * 
 * @author RHC
 *
 */
@Entity
public class FieldOptions implements Serializable, com.apd.phoenix.service.model.Entity {

    private static final long serialVersionUID = 6440524527198763346L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id = null;
    @Version
    @Column(name = "version", nullable = false)
    private int version = 0;

    @Column
    private String value;

    public Long getId() {
        return this.id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public int getVersion() {
        return this.version;
    }

    public void setVersion(final int version) {
        this.version = version;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        if (id != null) {
            return id.equals(((FieldOptions) that).id);
        }
        return super.equals(that);
    }

    @Override
    public int hashCode() {
        if (id != null) {
            return id.hashCode();
        }
        return super.hashCode();
    }

    public String getValue() {
        return this.value;
    }

    public void setValue(final String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        String result = "";
        if (StringUtils.isNotBlank(value)) {
            result += value;
        }
        return result;
    }
}