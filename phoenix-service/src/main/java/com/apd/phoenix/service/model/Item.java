/*
 * 
 */
package com.apd.phoenix.service.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.persistence.Version;
import javax.xml.bind.annotation.XmlRootElement;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.Index;
import com.apd.phoenix.service.model.listener.ItemListener;

/**
 * The Class Item.
 */
@Entity
@EntityListeners(ItemListener.class)
@XmlRootElement
public class Item implements Serializable, com.apd.phoenix.service.model.Entity {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 6095045718901321751L;

    /** The id. */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id = null;

    /** The version. */
    @Version
    @Column(name = "version", nullable = false)
    private int version = 0;

    @OneToMany(mappedBy = "item", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<Sku> skus = new HashSet<Sku>();

    @OneToMany(mappedBy = "item", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<CustomerCost> customerCosts = new HashSet<CustomerCost>();

    @OneToMany(mappedBy = "item", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<ItemSpecification> itemSpecifications = new HashSet<ItemSpecification>();

    @OneToMany(mappedBy = "item", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<ItemImage> itemImages = new HashSet<ItemImage>();

    @ManyToOne(fetch = FetchType.EAGER, cascade = { CascadeType.DETACH, CascadeType.PERSIST, CascadeType.REFRESH,
            CascadeType.MERGE })
    @Index(name = "ITEM_ITEM_IX")
    private Item replacement;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private UnitOfMeasure unitOfMeasure;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private Catalog vendorCatalog;

    @ManyToOne(fetch = FetchType.LAZY)
    private HierarchyNode hierarchyNode;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "original")
    private Set<ItemRelationship> similarItems;

    /** The item name. */
    @Column(name = "name", nullable = false)
    private String name;

    /** The item dealer SKU. Added to item to speed lookup, it's also in the list of item SKUs. */
    @Column(name = "dealerSku")
    @Index(name = "ITEM_DEALERSKU_IX")
    private String dealerSku;

    /** The item name. Added to item to speed lookup, it's also in the list of item SKUs. */
    @Column(name = "vendorSku")
    @Index(name = "ITEM_VENDORSKU_IX")
    private String vendorSku;

    @Column(length = 4000)
    private String searchTerms;

    @Column(length = 4000)
    private String protectedSearchTerms;

    /** The item name. */
    @Column(name = "description", nullable = false, length = 4000)
    private String description;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
    @JoinColumn(name = "item_id")
    @Index(name = "ITEM_ID_IX")
    private Set<ItemXItemPropertyType> properties = new HashSet<ItemXItemPropertyType>();

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
    @JoinColumn(name = "item_id")
    @Index(name = "ITEM_SELLPT_ID_IX")
    private Set<ItemSellingPoint> sellingPoints = new HashSet<ItemSellingPoint>();

    @ManyToOne(fetch = FetchType.LAZY)
    private Item abilityOneSubstitute;

    @Column
    private String commodityCode;

    @ManyToMany(fetch = FetchType.LAZY, cascade = { CascadeType.PERSIST, CascadeType.MERGE, CascadeType.DETACH,
            CascadeType.REFRESH })
    private Set<Matchbook> matchbook = new HashSet<Matchbook>();

    @Enumerated(EnumType.STRING)
    private ItemStatus status;

    @Column
    private Boolean externalCatalogRelevant;

    @Column
    private Boolean externalCatalogChange;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "lastModified", nullable = false)
    private Date lastModified = new Date();

    @Column
    private Integer multiple;

    @Column
    private Integer minimum;

    @Column
    private BigDecimal itemWeight;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "lastCostChangeDate")
    private Date lastCostChangeDate = new Date();

    //Mapping of customer catalogs to their price categories
    @ElementCollection
    private Map<Long, String> priceCategories = new HashMap<Long, String>();

    @ElementCollection
    private Map<Long, String> smartSearchCustomerSkus = new HashMap<Long, String>();

    @Column(length = 1000)
    private String smartSearchKeywords;

    @Column
    private String smartSearchDealerDescription;

    @Column
    private Boolean isFeaturedItem;

    @Column
    private String forUseWith;

    @Column
    private String cartridgeNumber;

    @Column
    private String lotSerial;

    @Temporal(TemporalType.TIMESTAMP)
    @Column
    private Date expiry;

    @Transient
    private boolean sendUpdateMessage = true;

    @Column
    private String protectedCommodityCode;

    /**
     * Gets the id.
     *
     * @return the id
     */
    public Long getId() {
        return this.id;
    }

    /**
     * Sets the id.
     *
     * @param id the new id
     */
    public void setId(final Long id) {
        this.id = id;
    }

    /**
     * Gets the version.
     *
     * @return the version
     */
    public int getVersion() {
        return this.version;
    }

    /**
     * Sets the version.
     *
     * @param version the new version
     */
    public void setVersion(final int version) {
        this.version = version;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        String result = getClass().getSimpleName() + " ";
        if (id != null)
            result += "id: " + id;
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        if (id != null) {
            return id.equals(((Item) that).id);
        }
        return super.equals(that);
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        if (id != null) {
            return id.hashCode();
        }
        return super.hashCode();
    }

    /** The item category. */
    @ManyToOne(fetch = FetchType.EAGER)
    @Fetch(FetchMode.JOIN)
    private ItemCategory itemCategory;

    /**
     * Gets the item category.
     *
     * @return the item category
     */
    public ItemCategory getItemCategory() {
        return this.itemCategory;
    }

    /**
     * Sets the item category.
     *
     * @param itemCategory the new item category
     */
    public void setItemCategory(final ItemCategory itemCategory) {
        this.itemCategory = itemCategory;
    }

    public HierarchyNode getHierarchyNode() {
        return this.hierarchyNode;
    }

    public void setHierarchyNode(final HierarchyNode hierarchyNode) {
        this.hierarchyNode = hierarchyNode;
    }

    /** The manufacturer. */
    @ManyToOne(fetch = FetchType.LAZY)
    private Manufacturer manufacturer;

    @ManyToOne(fetch = FetchType.EAGER, cascade = { CascadeType.PERSIST, CascadeType.MERGE, CascadeType.DETACH,
            CascadeType.REFRESH })
    private Brand brand;

    /**
     * Gets the manufacturer.
     *
     * @return the manufacturer
     */
    public Manufacturer getManufacturer() {
        return this.manufacturer;
    }

    /**
     * Sets the manufacturer.
     *
     * @param manufacturer the new manufacturer
     */
    public void setManufacturer(final Manufacturer manufacturer) {
        this.manufacturer = manufacturer;
    }

    @OneToMany(mappedBy = "item", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<ItemClassification> itemClassifications = new HashSet<ItemClassification>();

    /**
     * Gets the item classification.
     *
     * @return the item classification
     */
    public Set<ItemClassification> getItemClassifications() {
        return this.itemClassifications;
    }

    /**
     * Sets the item classification.
     *
     * @param itemClassification the new item classification
     */
    public void setItemClassifications(final Set<ItemClassification> itemClassifications) {
        this.itemClassifications = itemClassifications;
    }

    public Set<Sku> getSkus() {
        return this.skus;
    }

    public void setSkus(final Set<Sku> skus) {
        this.skus = skus;
    }

    public Set<CustomerCost> getCustomerCosts() {
        return this.customerCosts;
    }

    public void setCustomerCosts(final Set<CustomerCost> customerCosts) {
        this.customerCosts = customerCosts;
    }

    public Item getReplacement() {
        return this.replacement;
    }

    public void setReplacement(final Item replacement) {
        this.replacement = replacement;
    }

    public UnitOfMeasure getUnitOfMeasure() {
        return unitOfMeasure;
    }

    public void setUnitOfMeasure(UnitOfMeasure unitOfMeasure) {
        this.unitOfMeasure = unitOfMeasure;
    }

    public Catalog getVendorCatalog() {
        return vendorCatalog;
    }

    public void setVendorCatalog(Catalog vendorCatalog) {
        this.vendorCatalog = vendorCatalog;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDealerSku() {
        return dealerSku;
    }

    public void setDealerSku(String dealerSku) {
        this.dealerSku = dealerSku;
    }

    public String getVendorSku() {
        return vendorSku;
    }

    public void setVendorSku(String vendorSku) {
        this.vendorSku = vendorSku;
    }

    public String getSearchTerms() {
        return this.searchTerms;
    }

    public void setSearchTerms(String searchTerms) {
        this.searchTerms = searchTerms;
    }

    public String getProtectedSearchTerms() {
        return this.protectedSearchTerms;
    }

    public void setProtectedSearchTerms(String protectedSearchTerms) {
        this.protectedSearchTerms = protectedSearchTerms;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    //Use itemBp.addOrModifyItemProperty or getPropertiesReadOnly
    @Deprecated
    public Set<ItemXItemPropertyType> getProperties() {
        return this.properties;
    }

    //Do not modify properties retrieved with this method or add to this list except with itemBp.addOrModifyItemProperty
    public Set<ItemXItemPropertyType> getPropertiesReadOnly() {
        return getProperties();
    }

    //Use itemBp.addOrModifyItemProperty or getPropertiesReadOnly
    @Deprecated
    public void setProperties(final Set<ItemXItemPropertyType> properties) {
        this.properties = properties;
    }

    //Use itemBp.addOrModifyItemProperty to change properties
    public ItemXItemPropertyType getPropertyReadOnly(String propertyName) {
        for (ItemXItemPropertyType property : this.getPropertiesReadOnly()) {
            if (property.getType().getName().equals(propertyName)) {
                return property;
            }
        }
        return null;
    }

    public Set<ItemSellingPoint> getSellingPoints() {
        return sellingPoints;
    }

    public void setSellingPoints(Set<ItemSellingPoint> sellingPoints) {
        this.sellingPoints = sellingPoints;
    }

    public Item getAbilityOneSubstitute() {
        return this.abilityOneSubstitute;
    }

    public void setAbilityOneSubstitute(final Item abilityOneSubstitute) {
        this.abilityOneSubstitute = abilityOneSubstitute;
    }

    public Set<ItemSpecification> getItemSpecifications() {
        return itemSpecifications;
    }

    public void setItemSpecifications(final Set<ItemSpecification> itemSpecifications) {
        this.itemSpecifications = itemSpecifications;
    }

    public Set<ItemImage> getItemImages() {
        return itemImages;
    }

    public void setItemImages(final Set<ItemImage> itemImages) {
        this.itemImages = itemImages;
    }

    public void setSpecifications(List<ItemSpecification> specifications) {
        this.getItemSpecifications().clear();
        int sequence = 0;
        List<String> specificationsAdded = new ArrayList<String>();
        for (ItemSpecification specification : specifications) {
            specification.setSequence(sequence);
            sequence++;
            ItemSpecification newSpecification = new ItemSpecification();
            newSpecification.setName(specification.getName());
            newSpecification.setPriority(specification.getPriority());
            newSpecification.setSequence(specification.getSequence());
            newSpecification.setValue(specification.getValue());
            newSpecification.setItem(this);
            if (!specificationsAdded.contains(newSpecification.getName())) {
                this.getItemSpecifications().add(newSpecification);
            }
            specificationsAdded.add(newSpecification.getName());
        }
    }

    public Map<String, String> getSpecificationsMap() {
    	Map<String, String> toReturn = new HashMap<>();
    	for (ItemSpecification specification : this.getItemSpecifications()) {
    		toReturn.put(specification.getName(), specification.getValue());
    	}
    	return toReturn;
    }

    public List<ItemSpecification> getSpecifications() {
        List<ItemSpecification> toReturn = new ArrayList<ItemSpecification>();
        for (ItemSpecification specification : this.getItemSpecifications()) {
            toReturn.add(specification);
        }
        return toReturn;
    }

    public void setClassifications(Map<ItemClassificationType, String> classes) {
        this.getItemClassifications().clear();
        List<ItemClassificationType> classificationsAdded = new ArrayList<ItemClassificationType>();
        for (ItemClassificationType type : classes.keySet()) {
            ItemClassification toAdd = new ItemClassification();
            toAdd.setItemClassificationType(type);
            toAdd.setValue(classes.get(type));
            if (!classificationsAdded.contains(type)) {
                this.getItemClassifications().add(toAdd);
            }
            toAdd.setItem(this);
            classificationsAdded.add(type);
        }
    }

    public Map<ItemClassificationType, String> getClassifications() {
    	Map<ItemClassificationType, String> toReturn = new HashMap<>();
    	for (ItemClassification classification : this.getItemClassifications()) {
    		toReturn.put(classification.getItemClassificationType(), classification.getValue());
    	}
    	return toReturn;
    }

    public Map<String, String> getClassificationStringMap() {
        Map<String, String> toReturn = new HashMap<>();
        for (ItemClassification classification : this.getItemClassifications()) {
            toReturn.put(classification.getItemClassificationType().getName(), classification.getValue());
        }
        return toReturn;
    }

    public String getSku(String skuType) {
        for (Sku s : this.getSkus()) {
            if (s.getType().getName().equals(skuType)) {
                return s.getValue();
            }
        }
        return null;
    }

    public String getCommodityCode() {
        return commodityCode;
    }

    public void setCommodityCode(String commodityCode) {
        this.commodityCode = commodityCode;
    }

    public Set<ItemRelationship> getSimilarItems() {
        return similarItems;
    }

    public void setSimilarItems(Set<ItemRelationship> similarItems) {
        this.similarItems = similarItems;
    }

    public Set<Matchbook> getMatchbook() {
        return matchbook;
    }

    public void setMatchbook(Set<Matchbook> matchbook) {
        this.matchbook = matchbook;
    }

    public ItemStatus getStatus() {
        return status;
    }

    public void setStatus(ItemStatus status) {
        this.status = status;
    }

    /**
     * @return the externalCatalogRelevant
     */
    public Boolean getExternalCatalogRelevant() {
        return externalCatalogRelevant;
    }

    /**
     * @param externalCatalogRelevant the externalCatalogRelevant to set
     */
    public void setExternalCatalogRelevant(Boolean externalCatalogRelevant) {
        this.externalCatalogRelevant = externalCatalogRelevant;
    }

    /**
     * @return the externalCatalogChange
     */
    public Boolean getExternalCatalogChange() {
        return externalCatalogChange;
    }

    /**
     * @param externalCatalogChange the externalCatalogChange to set
     */
    public void setExternalCatalogChange(Boolean externalCatalogChange) {
        this.externalCatalogChange = externalCatalogChange;
    }

    public enum ItemStatus {
        AVAILABLE, CONTACT_REQUIRED, DISCONTINUED;
    }

    public Date getLastModified() {
        return lastModified;
    }

    public void setLastModified(Date lastModified) {
        this.lastModified = lastModified;
    }

    public Integer getMultiple() {
        return multiple;
    }

    public void setMultiple(Integer multiple) {
        this.multiple = multiple;
    }

    public Integer getMinimum() {
        return minimum;
    }

    public void setMinimum(Integer minimum) {
        this.minimum = minimum;
    }

    public BigDecimal getItemWeight() {
        return itemWeight;
    }

    public void setItemWeight(BigDecimal itemWeight) {
        this.itemWeight = itemWeight;
    }

    public Date getLastCostChangeDate() {
        return lastCostChangeDate;
    }

    public void setLastCostChangeDate(Date lastCostChangeDate) {
        this.lastCostChangeDate = lastCostChangeDate;
    }

    public Map<Long, String> getPriceCategories() {
        return priceCategories;
    }

    public void setPriceCategories(Map<Long, String> priceCategories) {
        this.priceCategories = priceCategories;
    }

    public void setSmartSearchKeywords(String keywords) {
        this.smartSearchKeywords = keywords;
    }

    public String getSmartSearchKeywords() {
        return smartSearchKeywords;
    }

    public Map<Long, String> getSmartSearchCustomerSkus() {
        return smartSearchCustomerSkus;
    }

    public void setSmartSearchCustomerSkus(Map<Long, String> smartSearchCustomerSkus) {
        this.smartSearchCustomerSkus = smartSearchCustomerSkus;
    }

    public String getSmartSearchDealerDescription() {
        return smartSearchDealerDescription;
    }

    public void setSmartSearchDealerDescription(String smartSearchDealerDescription) {
        this.smartSearchDealerDescription = smartSearchDealerDescription;
    }

    public Brand getBrand() {
        return brand;
    }

    public void setBrand(Brand brand) {
        this.brand = brand;
    }

    public Boolean getIsFeaturedItem() {
        return isFeaturedItem;
    }

    public void setIsFeaturedItem(Boolean isFeaturedItem) {
        this.isFeaturedItem = isFeaturedItem;
    }

    public String getForUseWith() {
        return forUseWith;
    }

    public void setForUseWith(String forUseWith) {
        this.forUseWith = forUseWith;
    }

    public String getCartridgeNumber() {
        return cartridgeNumber;
    }

    public void setCartridgeNumber(String cartridgeNumber) {
        this.cartridgeNumber = cartridgeNumber;
    }

    public String getLotSerial() {
        return lotSerial;
    }

    public void setLotSerial(String lotSerial) {
        this.lotSerial = lotSerial;
    }

    public Date getExpiry() {
        return expiry;
    }

    public void setExpiry(Date expiry) {
        this.expiry = expiry;
    }

    public boolean isSendUpdateMessage() {
        return sendUpdateMessage;
    }

    public void setSendUpdateMessage(boolean sendUpdateMessage) {
        this.sendUpdateMessage = sendUpdateMessage;
    }

    public String getProtectedCommodityCode() {
        return protectedCommodityCode;
    }

    public void setProtectedCommodityCode(String protectedCommodityCode) {
        this.protectedCommodityCode = protectedCommodityCode;
    }

}