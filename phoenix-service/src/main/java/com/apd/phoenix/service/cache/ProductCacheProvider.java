package com.apd.phoenix.service.cache;

import javax.annotation.PreDestroy;
import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.enterprise.context.ApplicationScoped;
import com.apd.phoenix.service.product.Product;
import com.apd.phoenix.service.product.ProductKey;

/**
 * This class extends AbstractCacheProvider, using ProductKey and Product as the Key,Value pair.
 * 
 * @author RHC
 *
 */
@ApplicationScoped
@Lock(LockType.READ)
public class ProductCacheProvider extends AbstractCacheProvider<ProductKey, Product> {

    private static final String PRODUCT_CACHE_NAME = cacheProperties.getProperty("productCache.name");

    @Override
    protected String getCacheName() {
        return PRODUCT_CACHE_NAME;
    }

    @PreDestroy
    public void cleanup() {
        super.cleanup();
    }

}
