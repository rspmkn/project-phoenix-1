package com.apd.phoenix.service.business;

import java.util.Properties;
import javax.ejb.LocalBean;
import javax.ejb.Singleton;
import com.apd.phoenix.core.utility.PropertiesLoader;

/**
 * This class provides business process methods for Addresses.
 * 
 * @author RHC
 *
 */
@Singleton
@LocalBean
@Deprecated
public class SingletonPropertiesLoader {

    public static final Properties PROJECT_STATE = PropertiesLoader.getAsProperties("project.state");

    public static final String DEFAULT_TENANT = "apd";

    public static String getTenant() {
        return PROJECT_STATE.getProperty("tenant", DEFAULT_TENANT);
    }
}
