package com.apd.phoenix.service.persistence.jpa;

import java.util.Date;
import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.Query;
import com.apd.phoenix.service.model.ShipManifestLog;

@Stateless
@LocalBean
public class ShipManifestLogDao extends AbstractDao<ShipManifestLog> {

    @SuppressWarnings("unchecked")
    public List<ShipManifestLog> getTodaysManifestLogs() {
        String hql = "FROM ShipManifestLog AS sml WHERE day(sml.dateProcessed)=day(:today) AND month(sml.dateProcessed)=month(:today) AND year(sml.dateProcessed)=year(:today)";
        Query query = entityManager.createQuery(hql);
        query.setParameter("today", new Date());
        List<ShipManifestLog> results = query.getResultList();
        if (results.isEmpty()) {
            return null;
        }
        return results;
    }
}
