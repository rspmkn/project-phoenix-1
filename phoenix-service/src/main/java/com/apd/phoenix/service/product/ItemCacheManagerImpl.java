package com.apd.phoenix.service.product;

import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.core.utility.TenantConfigRepository;
import com.apd.phoenix.service.business.ItemBp;
import com.apd.phoenix.service.cache.ItemCacheProvider;
import com.apd.phoenix.service.model.Catalog;
import com.apd.phoenix.service.model.CatalogXItem;
import com.apd.phoenix.service.model.Item;
import com.apd.phoenix.service.persistence.jpa.CatalogXItemDao;
import com.apd.phoenix.service.persistence.jpa.ItemDao;
import com.apd.phoenix.service.persistence.multitenancy.impl.CurrentTenantIdentifierResolverImpl;

/**
 * Concrete class for ItemCacheManager
 * 
 * @author RHC
 *
 */
@Stateless
public class ItemCacheManagerImpl implements ItemCacheManager {

    private static final Logger logger = LoggerFactory.getLogger(ItemCacheManagerImpl.class);

    @Inject
    private ItemCacheProvider cacheProvider;

    @Inject
    private ItemDao itemDao;

    @Inject
    private CatalogXItemDao catalogXItemDao;

    @Override
    public Item getVendorItem(String dealerSku, String vendorName) {
        if (StringUtils.isBlank(dealerSku) || StringUtils.isBlank(vendorName)) {
            return null;
        }
        return this.getVendorItemBySkuType(dealerSku, vendorName, ItemBp.APD_SKUTYPE);
    }

    @Override
    public Item getVendorItemByVendorSku(String vendorSku, String vendorName) {
        if (StringUtils.isBlank(vendorSku) || StringUtils.isBlank(vendorName)) {
            return null;
        }
        return this.getVendorItemBySkuType(vendorSku, vendorName, ItemBp.VENDOR_SKUTYPE);
    }

    private Item getVendorItemBySkuType(String sku, String vendorName, String skuType) {
        ItemKey key = getItemKey(sku, vendorName, null);
        ItemValue value = this.getFromCache(key);
        if (value != null && value.getId() != null) {
            return itemDao.findById(value.getId(), Item.class);
        }
        else {
            Item searchItem = this.itemDao.searchItem(sku, skuType, vendorName);
            List<Item> searchList = this.itemDao.searchByExactExample(searchItem, 0, 0);
            if (searchList != null && !searchList.isEmpty()) {
                Item toReturn = searchList.get(0);
                this.putVendorItem(toReturn);
                return toReturn;
            }
            return null;
        }
    }

    @Override
    public void putVendorItem(Long itemId) {
        if (!useItemCache() || itemId == null) {
            return;
        }
        this.putVendorItem(this.itemDao.findById(itemId, Item.class));
    }

    private void putVendorItem(Item item) {
        if (!useItemCache() || item == null || item.getId() == null) {
            return;
        }
        ItemKey key = this.getDealerSkuItemKey(item);
        if (key == null) {
            return;
        }
        this.putInCache(key, new ItemValue(item.getId()));
        key = this.getVendorSkuItemKey(item);
        if (key == null) {
            return;
        }
        this.putInCache(key, new ItemValue(item.getId()));
    }

    @Override
    public CatalogXItem getCustomerItem(String dealerSku, String vendorName, Long catalogId) {
        if (StringUtils.isBlank(dealerSku) || StringUtils.isBlank(vendorName) || catalogId == null) {
            return null;
        }
        ItemKey key = getItemKey(dealerSku, vendorName, catalogId);
        ItemValue value = this.getFromCache(key);
        if (value != null && value.getId() != null) {
            return catalogXItemDao.findById(value.getId(), CatalogXItem.class);
        }
        else {
            CatalogXItem searchItem = new CatalogXItem();
            Item vendorItem = this.getVendorItem(dealerSku, vendorName);
            if (vendorItem == null) {
                return null;
            }
            searchItem.setItem(new Item());
            searchItem.getItem().setId(vendorItem.getId());
            searchItem.setCatalog(new Catalog());
            searchItem.getCatalog().setId(catalogId);
            List<CatalogXItem> searchList = this.catalogXItemDao.searchByExactExample(searchItem, 0, 0);
            if (searchList != null && !searchList.isEmpty()) {
                CatalogXItem toReturn = searchList.get(0);
                if (toReturn == null) {
                    return null;
                }
                this.putCustomerItem(toReturn);
                return toReturn;
            }
            return null;
        }
    }

    @Override
    public void putCustomerItem(CatalogXItem item) {
        if (!this.useItemCache() || item == null) {
            return;
        }
        ItemKey key = this.getItemKey(item);
        if (key == null) {
            return;
        }
        this.putInCache(key, new ItemValue(item.getId()));
    }

    private ItemKey getItemKey(CatalogXItem item) {
        ItemKey toReturn = this.getDealerSkuItemKey(item.getItem());
        toReturn.setCustomerCatalogId(item.getCatalog().getId());
        return toReturn;
    }

    private ItemKey getDealerSkuItemKey(Item item) {
        return this.getItemKey(item, ItemBp.APD_SKUTYPE);
    }

    private ItemKey getVendorSkuItemKey(Item item) {
        return this.getItemKey(item, ItemBp.VENDOR_SKUTYPE);
    }

    private ItemKey getItemKey(Item item, String skuType) {
        if (item != null) {
            String dealerSku = item.getSku(skuType);
            String vendorName = null;
            if (item.getVendorCatalog() != null && item.getVendorCatalog().getVendor() != null) {
                vendorName = item.getVendorCatalog().getVendor().getName();
            }
            if (StringUtils.isNotBlank(dealerSku) && StringUtils.isNotBlank(vendorName)) {
                return this.getItemKey(dealerSku, vendorName, null);
            }
        }
        return null;
    }

    private ItemKey getItemKey(String sku, String vendorName, Long catalogId) {
        return new ItemKey(sku, vendorName, catalogId, CurrentTenantIdentifierResolverImpl.getCurrentTenant());
    }

    private ItemValue getFromCache(ItemKey key) {
        if (!useItemCache()) {
            return null;
        }
        try {
            return cacheProvider.getCache().get(key);
        }
        catch (Exception e) {
            logger.warn("Exception getting item data from cache, turn on debug for stack trace");
            if (logger.isDebugEnabled()) {
                logger.debug("Stack trace", e);
            }
        }
        return null;
    }

    private void putInCache(ItemKey key, ItemValue value) {
        if (!useItemCache()) {
            return;
        }
        try {
            cacheProvider.getCache().putAsync(key, value);
        }
        catch (Exception e) {
            logger.warn("Exception putting item data into cache, turn on debug for stack trace");
            if (logger.isDebugEnabled()) {
                logger.debug("Stack trace", e);
            }
        }
    }

    private boolean useItemCache() {
        return "true".equals(TenantConfigRepository.getInstance().getProperty("cache", "useItemCache"));
    }
}
