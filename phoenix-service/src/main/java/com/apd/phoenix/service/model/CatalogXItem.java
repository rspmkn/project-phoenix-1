/*
 * 
 */
package com.apd.phoenix.service.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;
import javax.xml.bind.annotation.XmlRootElement;
import org.apache.commons.lang.StringUtils;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.Index;
import org.hibernate.envers.AuditJoinTable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.model.listener.CatalogXItemListener;

/**
 * The Class CatalogXItem.
 * Warning: some fields are accessed not through their getters in com.apd.phoenix.service.integration.solr.CatalogXItemUpdateBean
 */
@Entity
@EntityListeners(CatalogXItemListener.class)
@XmlRootElement
@Table(uniqueConstraints = @UniqueConstraint(columnNames = { "catalog_id", "item_id" }))
public class CatalogXItem implements Serializable, com.apd.phoenix.service.model.Entity {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -4888332150918131281L;
    public static final String AVAILABILITY_PROPERTY = "availability";

    /** The id. */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id = null;

    /** The version. */
    @Version
    @Column(name = "version", nullable = false)
    private int version = 0;

    /** The core item start date. */
    @Temporal(TemporalType.DATE)
    private Date coreItemStartDate;

    /** The core item expiration date. */
    @Temporal(TemporalType.DATE)
    private Date coreItemExpirationDate;

    /** The substitute item. */
    @ManyToOne(fetch = FetchType.LAZY)
    private CatalogXItem substituteItem;

    @Column
    private String customerSkuString;

    @ManyToOne(optional = false)
    @JoinColumn(name = "catalog_id")
    private Catalog catalog;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
    @JoinColumn(name = "catalogxitem_id")
    @Index(name = "CATALOGXITEM_ID_IX")
    @AuditJoinTable(name = "cxicxixipt_AUD")
    private Set<CatalogXItemXItemPropertyType> properties = new HashSet<CatalogXItemXItemPropertyType>();

    /** The pricing type. */
    @ManyToOne(fetch = FetchType.EAGER)
    @Fetch(FetchMode.JOIN)
    private PricingType pricingType;

    /** The name. */
    @Column
    private String pricingParameter;

    @Column(name = "price", nullable = false)
    private BigDecimal price = BigDecimal.ZERO;

    /** The item. */
    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    @Fetch(FetchMode.JOIN)
    @Index(name = "CXI_ITEM_IDX")
    private Item item;

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable(name = "comp_favlist_catxitem", joinColumns = { @JoinColumn(name = "items_id") }, inverseJoinColumns = { @JoinColumn(name = "favoriteslist_id") })
    private Set<FavoritesList> favorites = new HashSet<FavoritesList>();

    @ManyToMany(mappedBy = "userItems", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private Set<FavoritesList> userFavorites = new HashSet<FavoritesList>();

    @OneToMany(mappedBy = "catalogXItem", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
    private Set<CartItem> cartItem = new HashSet<CartItem>();

    //Properties to store for customers who use externally hosted catalogs
    @Column
    private Boolean modified;

    @Column
    private Boolean added;

    @Column
    private Boolean deleted;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "lastModified", nullable = false)
    private Date lastModified = new Date();

    //comma-separated list of catalog IDs where this item shouldn't be displayed, since 
    //these are child catalogs with items that override this item
    @Column
    private String overrideCatalogs;

    @Column
    private Boolean isFeaturedItem;

    @Column
    private Boolean mostPopular;

    @ManyToOne(fetch = FetchType.LAZY)
    private UnitOfMeasure customerUnitOfmeasure;

    @Column
    private String solrOverrideDescription;

    @Transient
    private boolean sendUpdateMessage = true;

    private static final Logger LOG = LoggerFactory.getLogger(CatalogXItem.class);

    /**
     * Gets the id.
     *
     * @return the id
     */
    public Long getId() {
        return this.id;
    }

    /**
     * Sets the id.
     *
     * @param id the new id
     */
    public void setId(final Long id) {
        this.id = id;
    }

    /**
     * Gets the version.
     *
     * @return the version
     */
    public int getVersion() {
        return this.version;
    }

    /**
     * Sets the version.
     *
     * @param version the new version
     */
    public void setVersion(final int version) {
        this.version = version;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        if (id != null) {
            return id.equals(((CatalogXItem) that).id);
        }
        return super.equals(that);
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        if (id != null) {
            return id.hashCode();
        }
        return super.hashCode();
    }

    /**
     * Gets the core item start date.
     *
     * @return the core item start date
     */
    public Date getCoreItemStartDate() {
        return this.coreItemStartDate;
    }

    /**
     * Sets the core item start date.
     *
     * @param coreItemStartDate the new core item start date
     */
    public void setCoreItemStartDate(final Date coreItemStartDate) {
        this.coreItemStartDate = coreItemStartDate;
    }

    /**
     * Gets the core item expiration date.
     *
     * @return the core item expiration date
     */
    public Date getCoreItemExpirationDate() {
        return this.coreItemExpirationDate;
    }

    /**
     * Sets the core item expiration date.
     *
     * @param coreItemExpirationDate the new core item expiration date
     */
    public void setCoreItemExpirationDate(final Date coreItemExpirationDate) {
        this.coreItemExpirationDate = coreItemExpirationDate;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "CatalogXItem [version=" + version + ", coreItemStartDate=" + coreItemStartDate
                + ", coreItemExpirationDate=" + coreItemExpirationDate + "]";
    }

    /**
     * Gets the item.
     *
     * @return the item
     */
    public Item getItem() {
        return this.item;
    }

    /**
     * Sets the item.
     *
     * @param item the new item
     */
    public void setItem(final Item item) {
        this.item = item;
    }

    /**
     * Gets the pricing type.
     *
     * @return the pricing type
     */
    public PricingType getPricingType() {
        return this.pricingType;
    }

    /**
     * Sets the pricing type.
     *
     * @param pricingType the new pricing type
     */
    public void setPricingType(final PricingType pricingType) {
        this.pricingType = pricingType;
    }

    /**
     * Gets the pricing parameter.
     *
     * @return the pricing parameter
     */
    public String getPricingParameter() {
        return this.pricingParameter;
    }

    /**
     * Sets the pricing parameter.
     *
     * @param pricingParameter the new pricing parameter
     */
    public void setPricingParameter(final String pricingParameter) {
        this.pricingParameter = pricingParameter;
    }

    /**
     * Gets the substitute item.
     *
     * @return the substitute item
     */
    public CatalogXItem getSubstituteItem() {
        return this.substituteItem;
    }

    /**
     * Sets the substitute item.
     *
     * @param substituteItem the new substitute item
     */
    public void setSubstituteItem(final CatalogXItem substituteItem) {
        this.substituteItem = substituteItem;
    }

    public String getCustomerSkuString() {
        return this.customerSkuString;
    }

    public void setCustomerSkuString(final String customerSkuString) {
        this.customerSkuString = customerSkuString;
    }

    public Sku getCustomerSku() {
        if (StringUtils.isBlank(this.customerSkuString)) {
            return null;
        }
        Sku toReturn = new Sku();
        toReturn.setValue(this.customerSkuString);
        return toReturn;
    }

    public void setCustomerSku(final Sku customerSku) {
        if (customerSku != null) {
            this.customerSkuString = customerSku.getValue();
        }
        else {
            this.customerSkuString = null;
        }
    }

    public Catalog getCatalog() {
        return this.catalog;
    }

    public void setCatalog(final Catalog catalog) {
        this.catalog = catalog;
    }

    public Set<CatalogXItemXItemPropertyType> getProperties() {
        return this.properties;
    }

    public void setProperties(final Set<CatalogXItemXItemPropertyType> properties) {
        this.properties = properties;
    }

    public CatalogXItemXItemPropertyType getProperty(String propertyName) {
        for (CatalogXItemXItemPropertyType property : this.getProperties()) {
            if (property.getType().getName().equals(propertyName)) {
                return property;
            }
        }
        return null;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Set<FavoritesList> getFavorites() {
        return favorites;
    }

    public void setFavorites(Set<FavoritesList> favorites) {
        this.favorites = favorites;
    }

    /**
     * This method is deprecated; there could be an unknown number of entities in the set
     * 
     * @return
     */
    @Deprecated
    public Set<FavoritesList> getUserFavorites() {
        return userFavorites;
    }

    /**
     * This method is deprecated; there could be an unknown number of entities in the set
     * 
     * @return
     */
    @Deprecated
    public void setUserFavorites(Set<FavoritesList> companyFavorites) {
        this.userFavorites = companyFavorites;
    }

    /**
     * This method is deprecated; there could be an unknown number of entities in the set
     * 
     * @return
     */
    @Deprecated
    public Set<CartItem> getCartItem() {
        return cartItem;
    }

    /**
     * This method is deprecated; there could be an unknown number of entities in the set
     * 
     * @return
     */
    @Deprecated
    public void setCartItem(Set<CartItem> cartItem) {
        this.cartItem = cartItem;
    }

    public BigDecimal getApdCost() {
        for (ItemXItemPropertyType property : this.getItem().getPropertiesReadOnly()) {
            if (property.getType().getName().equals("APDcost")) {
                try {
                    return new BigDecimal(property.getValue());
                }
                catch (NumberFormatException e) {
                    LOG.error("Could not format apd Cost: " + property.getValue() + ". Defaulting to zero.");
                }
            }
        }
        return BigDecimal.ZERO;
    }

    /**
     * @return the modified
     */
    public Boolean getModified() {
        return modified;
    }

    /**
     * @param modified the modified to set
     */
    public void setModified(Boolean modified) {
        this.modified = modified;
    }

    /**
     * @return the added
     */
    public Boolean getAdded() {
        return added;
    }

    /**
     * @param added the added to set
     */
    public void setAdded(Boolean added) {
        this.added = added;
    }

    /**
     * @return the deleted
     */
    public Boolean getDeleted() {
        return deleted;
    }

    /**
     * @param deleted the deleted to set
     */
    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public Date getLastModified() {
        return lastModified;
    }

    public void setLastModified(Date lastModified) {
        this.lastModified = lastModified;
    }

    public String getOverrideCatalogs() {
        return overrideCatalogs;
    }

    public void setOverrideCatalogs(String overrideCatalogs) {
        this.overrideCatalogs = overrideCatalogs;
    }

    public static class DeleteItemMessage implements Serializable {

        private Long id;
        private Long itemId;
        private Long catalogId;

        public DeleteItemMessage(CatalogXItem item) {
            id = item.getId();
            itemId = item.getItem().getId();
            catalogId = item.getCatalog().getId();
        }

        public DeleteItemMessage(Long id) {
            this.id = id;
            itemId = null;
            catalogId = null;
        }

        public Long getId() {
            return id;
        }

        public Long getItemId() {
            return itemId;
        }

        public Long getCatalogId() {
            return catalogId;
        }

        @Override
        public String toString() {
            return id + "";
        }
    }

    public static class UpdateSmartSearchMessage implements Serializable {

        private Long id;
        private Long listName;
        private Long coreListName;

        public UpdateSmartSearchMessage(Long id, Long listName, Long coreListName) {
            this.id = id;
            this.listName = listName;
            this.coreListName = coreListName;
        }

        public Long getId() {
            return id;
        }

        public Long getListName() {
            return listName;
        }

        public Long getCoreListName() {
            return coreListName;
        }

        @Override
        public String toString() {
            return id + "";
        }
    }

    public static class DeleteSmartSearchMessage implements Serializable {

        private Long itemId;
        private Long listName;

        public DeleteSmartSearchMessage(Long id, Long listName) {
            this.itemId = id;
            this.listName = listName;
        }

        public Long getItemId() {
            return itemId;
        }

        public Long getListName() {
            return listName;
        }

        @Override
        public String toString() {
            return itemId + "";
        }
    }

    public Boolean getIsFeaturedItem() {
        return isFeaturedItem;
    }

    public void setIsFeaturedItem(Boolean isFeaturedItem) {
        this.isFeaturedItem = isFeaturedItem;
    }

    public Boolean getMostPopular() {
        return mostPopular;
    }

    public void setMostPopular(Boolean mostPopular) {
        this.mostPopular = mostPopular;
    }

    public UnitOfMeasure getCustomerUnitOfmeasure() {
        return customerUnitOfmeasure;
    }

    public void setCustomerUnitOfmeasure(UnitOfMeasure customerUnitOfmeasure) {
        this.customerUnitOfmeasure = customerUnitOfmeasure;
    }

    public String getSolrOverrideDescription() {
        return solrOverrideDescription;
    }

    public void setSolrOverrideDescription(String solrOverrideDescription) {
        this.solrOverrideDescription = solrOverrideDescription;
    }

    public boolean isSendUpdateMessage() {
        return sendUpdateMessage;
    }

    public void setSendUpdateMessage(boolean sendUpdateMessage) {
        this.sendUpdateMessage = sendUpdateMessage;
    }

}