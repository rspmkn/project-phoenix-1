package com.apd.phoenix.service.model;

import java.util.Comparator;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Version;
import org.apache.commons.lang.StringUtils;
import org.hibernate.annotations.Index;
import com.apd.phoenix.service.business.EntityComparator.GenericComparator;

@Entity
public class TopLevelHierarchyXCatalog implements Comparable<TopLevelHierarchyXCatalog> {

    /** The version. */
    @Version
    private @Column(name = "version", nullable = false)
    int version = 0;

    @Id
    private @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false, nullable = false)
    Long id = null;

    @ManyToOne(fetch = FetchType.LAZY)
    @Index(name = "TLHXC_CAT_IX")
    private Catalog catalog;

    @Column(nullable = false)
    private Integer ordering;

    @Column(nullable = false)
    private String description;

    public Integer getOrdering() {
        return ordering;
    }

    public void setOrdering(Integer order) {
        this.ordering = order;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public int compareTo(TopLevelHierarchyXCatalog o) {
        return ordering.compareTo(o.getOrdering());
    }

    public Catalog getCatalog() {
        return catalog;
    }

    public void setCatalog(Catalog catalog) {
        this.catalog = catalog;
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof TopLevelHierarchyXCatalog) {
            TopLevelHierarchyXCatalog topLevelHierarchyXCatalog = (TopLevelHierarchyXCatalog) o;
            if (topLevelHierarchyXCatalog.id != null) {
                if (topLevelHierarchyXCatalog.id != this.id) {
                    return false;
                }
            }
            if (topLevelHierarchyXCatalog.description != null) {
                if (topLevelHierarchyXCatalog.catalog != null) {
                    return topLevelHierarchyXCatalog.description == this.description
                            && topLevelHierarchyXCatalog.catalog == this.catalog;
                }
            }
        }
        return false;

    }

    public static class TopLevelHierarchyComparator implements Comparator<TopLevelHierarchyXCatalog> {

        @Override
        public int compare(TopLevelHierarchyXCatalog log0, TopLevelHierarchyXCatalog log1) {
            if ((log0 == null || log0.getOrdering() == null) && (log1 == null || log1.getOrdering() == null)) {
                if (log0 != null && StringUtils.isNotBlank(log0.getDescription()) && log1 != null
                        && StringUtils.isNotBlank(log1.getDescription())) {
                    return log0.getDescription().compareTo(log1.getDescription());
                }
                return 0;
            }
            if (log0 == null || log0.getOrdering() == null) {
                return -1;
            }
            if (log1 == null || log1.getOrdering() == null) {
                return 1;
            }
            return log0.getOrdering() - log1.getOrdering();
        }
    }

}
