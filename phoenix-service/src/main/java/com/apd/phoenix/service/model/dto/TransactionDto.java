package com.apd.phoenix.service.model.dto;

import java.io.Serializable;

public class TransactionDto implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1966814693741117212L;

    public enum TransactionOperation {
        NEW("new"), UPDATE("update"), DELETE("delete");

        private String value;

        private TransactionOperation(String value) {
            this.value = value;
        }

        public String getValue() {
            return this.value;
        }

        public CXMLOperation getCXML() {
            switch (this) {
                case NEW:
                    return CXMLOperation.NEW;
                case UPDATE:
                    return CXMLOperation.UPDATE;
                case DELETE:
                    return CXMLOperation.DELETE;
                default:
                    return CXMLOperation.NEW;
            }
        }

        public X12TransactionPurpose getX12() throws UnsupportedOperationException {
            switch (this) {
                case NEW:
                    return X12TransactionPurpose.ORIGINAL;
                case UPDATE:
                    throw new UnsupportedOperationException();
                case DELETE:
                    throw new UnsupportedOperationException();
                default:
                    return X12TransactionPurpose.ORIGINAL;
            }
        }

        public XCBLTransactionPurpose getXCBL() throws UnsupportedOperationException {
            switch (this) {
                case NEW:
                    return XCBLTransactionPurpose.Original;
                case UPDATE:
                    throw new UnsupportedOperationException();
                case DELETE:
                    throw new UnsupportedOperationException();
                default:
                    return XCBLTransactionPurpose.Original;
            }
        }
    }

    // Reference values for xml document actions (e.g. OrderRequestHeader.type)
    public enum CXMLOperation {
        NEW("new"), UPDATE("update"), DELETE("delete");

        private String value;

        private CXMLOperation(String value) {
            this.value = value;
        }

        public String getValue() {
            return this.value;
        }

        public TransactionOperation getCanonicalModel() throws UnsupportedOperationException {
            switch (this) {
                case NEW:
                    return TransactionOperation.NEW;
                case UPDATE:
                    throw new UnsupportedOperationException();
                case DELETE:
                    return TransactionOperation.DELETE;
                default:
                    return TransactionOperation.NEW;
            }
        }
    }

    // Reference values for transaction set purpose code
    public enum X12TransactionPurpose {
        ORIGINAL("00");

        private String value;

        private X12TransactionPurpose(String value) {
            this.value = value;
        }

        public String getValue() {
            return this.value;
        }

        public TransactionOperation getCanonicalModel() throws UnsupportedOperationException {
            switch (this) {
                case ORIGINAL:
                    return TransactionOperation.NEW;
                default:
                    return TransactionOperation.NEW;
            }
        }
    }

    // Reference values for XCBL transaction purpose code (e.g. Order.orderHeader.purpose.purposeCoded)
    public enum XCBLTransactionPurpose {
        Original("Original"), Change("Change"), Cancellation("Cancellation");

        private String value;

        private XCBLTransactionPurpose(String value) {
            this.value = value;
        }

        public String getValue() {
            return this.value;
        }

        public TransactionOperation getCanonicalModel() throws UnsupportedOperationException {
            switch (this) {
                case Original:
                    return TransactionOperation.NEW;
                case Change:
                    throw new UnsupportedOperationException();
                case Cancellation:
                    throw new UnsupportedOperationException();
                default:
                    return TransactionOperation.NEW;
            }
        }
    }

    /**
     * specifies the purpose or type of operation associated with the transaction
     */
    private TransactionOperation transactionOperation;

    private String interchangeId;

    private String transactionGroupId;

    private String transactionId;

    public TransactionDto() {
    }

    public TransactionDto(TransactionDto toCopy) {
        this.transactionGroupId = toCopy.transactionGroupId;
        this.interchangeId = toCopy.interchangeId;
        this.transactionId = toCopy.transactionId;
    }

    public TransactionOperation getTransactionOperation() {
        return transactionOperation;
    }

    public void setTransactionOperation(TransactionOperation transactionOperation) {
        this.transactionOperation = transactionOperation;
    }

    public String getInterchangeId() {
        return interchangeId;
    }

    public void setInterchangeId(String interchangeId) {
        this.interchangeId = interchangeId;
    }

    public String getGroupId() {
        return transactionGroupId;
    }

    public void setGroupId(String groupId) {
        this.transactionGroupId = groupId;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }
}
