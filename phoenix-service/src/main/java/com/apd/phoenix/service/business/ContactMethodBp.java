package com.apd.phoenix.service.business;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import com.apd.phoenix.service.model.ContactMethod;
import com.apd.phoenix.service.persistence.jpa.ContactMethodDao;

/**
 * This class provides business process methods for ContactMethod.
 * 
 * @author RHC
 *
 */
@Stateless
@LocalBean
public class ContactMethodBp extends AbstractBp<ContactMethod> {

    @Inject
    ContactMethodDao contactMethodDao;

    /**
     * This method is injected with the correct Dao for initialization
     * 
     * @param dao
     */
    @Inject
    public void initDao(ContactMethodDao dao) {
        this.dao = dao;
    }

}
