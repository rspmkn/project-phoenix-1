package com.apd.phoenix.service.model.dto;

import java.io.Serializable;
import java.util.List;

public class StopNotificationResponseDto implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private String success;

    private List<String> errors;

    private List<StopNoticeReportDto> results;

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public List<String> getErrors() {
        return errors;
    }

    public void setErrors(List<String> errors) {
        this.errors = errors;
    }

    public List<StopNoticeReportDto> getResults() {
        return results;
    }

    public void setResults(List<StopNoticeReportDto> results) {
        this.results = results;
    }

}
