package com.apd.phoenix.service.business;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import com.apd.phoenix.service.model.HomePageCarouselDisplay;
import com.apd.phoenix.service.persistence.jpa.HomePageCarouselDisplayDao;

@Stateless
@LocalBean
public class HomePageCarouselBp extends AbstractBp<HomePageCarouselDisplay> {

    @Inject
    public void initDao(HomePageCarouselDisplayDao dao) {
        this.dao = dao;
    }

}
