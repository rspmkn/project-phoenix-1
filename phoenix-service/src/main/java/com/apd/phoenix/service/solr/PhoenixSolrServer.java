package com.apd.phoenix.service.solr;

import java.net.MalformedURLException;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.CloudSolrServer;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.params.SolrParams;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PhoenixSolrServer extends CloudSolrServer {

    private static final Logger LOG = LoggerFactory.getLogger(PhoenixSolrServer.class);
    private static final long serialVersionUID = 1L;

    public PhoenixSolrServer(String zkHost) throws MalformedURLException {
        super(zkHost);
    }

    @Override
    public QueryResponse query(SolrParams params) throws SolrServerException {
        try {
            return super.query(params);
        }
        catch (Exception e) {
            LOG.error("Error reaching Solr server, turn in debug logging to view stack trace");
            if (LOG.isDebugEnabled()) {
                LOG.debug("stack trace", e);
            }
            //update this for PHOEN-3496
            return new QueryResponse();
        }
    }

}