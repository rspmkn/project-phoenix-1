/**
 * 
 */
package com.apd.phoenix.service.workflow;

import java.io.Serializable;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.core.utility.TenantConfigRepository;
import com.apd.phoenix.service.model.CustomerOrder;
import com.apd.phoenix.service.utility.SendInternalHttpRequest;
import com.google.gson.Gson;

/**
 * @author Red Hat Consulting - Red Hat, Inc.
 *
 */
@Stateless
@LocalBean
public class RemoteTaskService {

    private static final Logger LOG = LoggerFactory.getLogger(RemoteTaskService.class);

    public static final String STUCK_ORDER_TASK = "reviewStuckOrder";

    public static final String APPROVE_ORDER_TASK = "approveOrder";

    @Inject
    private WorkflowService workflowService;

    public void completeApprovalTask(String user, CustomerOrder customerOrder, boolean approve) {
    	List<Long> approverTaskList = this.getTasks(customerOrder, APPROVE_ORDER_TASK);
        //if null or out of range, exception will be caught, and error shown to user making rest call
    	long taskId = approverTaskList.get(0);
        LOG.info("Approving order, via task: {} {}", customerOrder.getId(), taskId);
        Map<String, Object> params = new HashMap<>();
        params.put("approved", approve);
        
        workflowService.completeTask(taskId, customerOrder.getId(), user, params);
    }

    public List<RemoteTaskSummary> getAssignedTasks(String owner) {
        try {
            String uri = getRemoteTaskProperties().getProperty("assignedTasksUrl") + "/"
                    + URLEncoder.encode(owner, "UTF-8");
            String response = SendInternalHttpRequest.sendRequest(uri, new HashMap<String, String>(), true);
            if (StringUtils.isNotBlank(response)) {
                Gson gson = new Gson();
                List<Map<String, Object>> resultList = gson.fromJson(response, List.class);
                List<RemoteTaskSummary> toReturn = new ArrayList<>();
                for (Map<String, Object> resultMap : resultList) {
                	if (resultMap != null) {
                		toReturn.add(new RemoteTaskSummary(resultMap));
                	}
                }
                return toReturn;
            }
        }
        catch (Exception e) {
            LOG.error("Exception getting tasks", e);
        }
        return null;
    }

    public Map<String, Object> getTaskContent(Long id) {
        try {
            String uri = getRemoteTaskProperties().getProperty("taskContentUrl") + "/"
                    + URLEncoder.encode(id.toString(), "UTF-8");
            String response = SendInternalHttpRequest.sendRequest(uri, new HashMap<String, String>(), true);
            if (StringUtils.isNotBlank(response)) {
                Gson gson = new Gson();
                return gson.fromJson(response, Map.class);
            }
        }
        catch (Exception e) {
            LOG.error("Exception getting task content", e);
        }
        return null;
    }

    public List<Long> getTasks(CustomerOrder order, String name) {
        try {
            String uri = getRemoteTaskProperties().getProperty("orderTasksUrl") + "/"
                    + URLEncoder.encode(order.getApdPo().getValue(), "UTF-8") + "/" + URLEncoder.encode(name, "UTF-8");
            String response = SendInternalHttpRequest.sendRequest(uri, new HashMap<String, String>(), true);
            if (StringUtils.isNotBlank(response)) {
                Gson gson = new Gson();
                List<Double> toConvert = gson.fromJson(response, List.class);
                List<Long> toReturn = new ArrayList<>();
                for (Double val : toConvert) {
                	if (val != null) {
                		toReturn.add(val.longValue());
                	}
                }
                return toReturn;
            }
        }
        catch (Exception e) {
            LOG.error("Exception getting task content", e);
        }
        return null;
    }

    public static class RemoteTaskSummary implements Serializable {

        private static final long serialVersionUID = 1L;

        private Long id;
        private String name;
        private String owner;
        private String description;
        private Date creationDate;
        private Boolean canBeWorked;

        public RemoteTaskSummary() {
        }

        public RemoteTaskSummary(Map<String, Object> inputMap) {
            this.id = ((Double) inputMap.get("id")).longValue();
            this.name = (String) inputMap.get("name");
            this.owner = (String) inputMap.get("owner");
            this.description = (String) inputMap.get("description");
            this.creationDate = new Date(((Double) inputMap.get("creationDate")).longValue());
            this.canBeWorked = (Boolean) inputMap.get("canBeWorked");
        }

        public Long getId() {
            return id;
        }

        public void setId(Long id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getOwner() {
            return owner;
        }

        public void setOwner(String owner) {
            this.owner = owner;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public Date getCreationDate() {
            return creationDate;
        }

        public void setCreationDate(Date creationDate) {
            this.creationDate = creationDate;
        }

        public Boolean getCanBeWorked() {
            return canBeWorked;
        }

        public void setCanBeWorked(Boolean canBeWorked) {
            this.canBeWorked = canBeWorked;
        }
    }

    private Properties getRemoteTaskProperties() {
        return TenantConfigRepository.getInstance().getProperties("remote.task");
    }
}
