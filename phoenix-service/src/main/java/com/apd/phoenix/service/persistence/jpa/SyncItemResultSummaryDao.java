package com.apd.phoenix.service.persistence.jpa;

import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.Query;
import com.apd.phoenix.service.model.SyncItemResultSummary;

@Stateless
@LocalBean
public class SyncItemResultSummaryDao extends AbstractDao<SyncItemResultSummary> {

    public Long getCount(String correlationId) {
        String hql = "SELECT sum(s.total) FROM SyncItemResultSummary s WHERE s.correlationId = :correlationId";
        Query query = entityManager.createQuery(hql);
        query.setParameter("correlationId", correlationId);
        Long toReturn = (Long) query.getSingleResult();
        if (toReturn == null) {
            return 0l;
        }
        return toReturn;
    }

    public SyncItemResultSummary getAggregatedSummary(String correlationId) {
        String hql = "SELECT sum(s.created), sum(s.discontinued), sum(s.errors), sum(s.failures), sum(s.updated), sum(s.warnings), sum(zeroPrice) FROM SyncItemResultSummary s WHERE s.correlationId = :correlationId";
        Query query = entityManager.createQuery(hql);
        query.setParameter("correlationId", correlationId);
        List<Object[]> results = (List<Object[]>) query.getResultList();
        if (results != null && !results.isEmpty()) {
            SyncItemResultSummary toReturn = new SyncItemResultSummary();
            toReturn.setValues(results.get(0));
            return toReturn;
        }
        else {
            return null;
        }
    }

    public void removeRecords(String correlationId) {
        String hql = "DELETE FROM SyncItemResultSummary WHERE correlationId = :correlationId";
        Query query = entityManager.createQuery(hql);
        query.setParameter("correlationId", correlationId);
        query.executeUpdate();
    }

    @Override
    public SyncItemResultSummary update(SyncItemResultSummary syncItemResult) {
        throw new UnsupportedOperationException("SyncItemResultSummary cannot be updated");
    }

}
