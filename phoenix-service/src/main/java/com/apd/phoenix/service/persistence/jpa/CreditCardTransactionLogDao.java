/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.service.persistence.jpa;

import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.Query;
import com.apd.phoenix.service.model.CreditCardTransactionLog;
import com.apd.phoenix.service.model.LineItemXShipment;

/**
 *
 * @author dnorris
 */
@Stateless
@LocalBean
public class CreditCardTransactionLogDao extends AbstractDao<CreditCardTransactionLog> {

    public CreditCardTransactionLog hydrateCardTransactionLogForReport(long id) {
        String hql = "SELECT log FROM CreditCardTransactionLog as log" + " LEFT JOIN log.lineItemXShipments"
                + " LEFT JOIN FETCH log.customerOrder as customerOrder"
                + " LEFT JOIN FETCH customerOrder.address as address"
                + " LEFT JOIN FETCH log.failureDetails WHERE log.id = :id";
        Query query = entityManager.createQuery(hql);
        query.setParameter("id", id);
        return (CreditCardTransactionLog) query.getSingleResult();
    }

    public List<LineItemXShipment> getHydratedLineItemXShipments(long id) {
        String hql = "SELECT log FROM CreditCardTransactionLog as log"
                + " LEFT JOIN FETCH log.lineItemXShipments as shipments LEFT JOIN shipments.lineItem"
                + " WHERE log.id = :id ORDER BY shipments.id";
        Query query = entityManager.createQuery(hql);
        query.setParameter("id", id);
        final CreditCardTransactionLog log = (CreditCardTransactionLog) query.getSingleResult();
        return log.getLineItemXShipments();

    }
}
