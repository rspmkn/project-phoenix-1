package com.apd.phoenix.service.message.impl;

import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.jms.JMSException;
import javax.jms.ObjectMessage;
import javax.jms.Queue;
import javax.jms.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.message.api.Message;
import com.apd.phoenix.service.message.api.MessageSender;
import com.apd.phoenix.service.model.OrderLog.EventType;
import com.apd.phoenix.service.model.dto.POAcknowledgementDto;
import com.apd.phoenix.service.persistence.jpa.SequenceDao.Sequence;
import com.apd.phoenix.service.persistence.multitenancy.impl.CurrentTenantIdentifierResolverImpl;
import com.apd.phoenix.service.utility.XcblMessageUtils;

@Stateless
@LocalBean
public class XCBLMessageSender extends MessageSender {

    private static final Logger LOGGER = LoggerFactory.getLogger(XCBLMessageSender.class);

    @Inject
    private XcblMessageUtils xcblMessageUtils;

    @Resource(mappedName = "java:/activemq/xcbl-outbound-order-response")
    private Queue outboundXCBLOrderAckQueue;

    @PostConstruct
    public void setupQueues() {
        this.outboundOrderAckQueue = outboundXCBLOrderAckQueue;
    }

    @Override
    public boolean sendMessage(Message message, EventType evenType) {
        LOGGER.info("Sending XCBL Message");
        logMessage(message, LOGGER);
        return true;
    }

    private ObjectMessage createXCBLJMSMessage(Session session, Serializable xcblMessage, String partnerId,
            String partnerDestination) {
        ObjectMessage objectMessage = null;
        try {
            objectMessage = session.createObjectMessage(xcblMessage);
            objectMessage.setStringProperty("partnerId", partnerId);
            objectMessage.setStringProperty("interchangeId", xcblMessageUtils.generateEnvelopeId());
            objectMessage.setStringProperty("groupId", sequenceDao.nextVal(Sequence.GROUP_CONTROL_ID) + "");
            objectMessage.setStringProperty("transactionId", sequenceDao.nextVal(Sequence.TRANSACTION_CONTROL_ID) + "");
            objectMessage.setStringProperty("destination", partnerDestination);
            objectMessage.setLongProperty("tenantId", CurrentTenantIdentifierResolverImpl.getCurrentTenant());
        }
        catch (JMSException e) {
            LOGGER.error(e.toString());
        }
        return objectMessage;
    }

    @Override
    public javax.jms.Message createPOAcknowledgementMessage(Session session, POAcknowledgementDto poAcknowledgementDto,
            Boolean resend) {
        return createXCBLJMSMessage(session, poAcknowledgementDto, poAcknowledgementDto.getPartnerId(),
                poAcknowledgementDto.getPurchaseOrderDto().getPartnerDestination());
    }

}
