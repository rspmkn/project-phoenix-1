/*
 * 
 */
package com.apd.phoenix.service.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Version;
import javax.xml.bind.annotation.XmlRootElement;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.Index;

/**
 * The Class ItemPackaging.
 */
@Entity
@XmlRootElement
public class ItemPackaging implements Serializable, com.apd.phoenix.service.model.Entity {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1279298453702225375L;

    /** The id. */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id = null;

    /** The version. */
    @Version
    @Column(name = "version", nullable = false)
    private int version = 0;

    /**
     * Gets the id.
     *
     * @return the id
     */
    public Long getId() {
        return this.id;
    }

    /**
     * Sets the id.
     *
     * @param id the new id
     */
    public void setId(final Long id) {
        this.id = id;
    }

    /**
     * Gets the version.
     *
     * @return the version
     */
    public int getVersion() {
        return this.version;
    }

    /**
     * Sets the version.
     *
     * @param version the new version
     */
    public void setVersion(final int version) {
        this.version = version;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        if (id != null) {
            return id.equals(((ItemPackaging) that).id);
        }
        return super.equals(that);
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        if (id != null) {
            return id.hashCode();
        }
        return super.hashCode();
    }

    /** The packaging dimension. */
    @ManyToOne(fetch = FetchType.EAGER)
    @Fetch(FetchMode.JOIN)
    private PackagingDimension packagingDimension;

    /**
     * Gets the packaging dimension.
     *
     * @return the packaging dimension
     */
    public PackagingDimension getPackagingDimension() {
        return this.packagingDimension;
    }

    /**
     * Sets the packaging dimension.
     *
     * @param packagingDimension the new packaging dimension
     */
    public void setPackagingDimension(final PackagingDimension packagingDimension) {
        this.packagingDimension = packagingDimension;
    }

    /** The packaging type. */
    @ManyToOne(fetch = FetchType.EAGER)
    @Fetch(FetchMode.JOIN)
    private PackagingType packagingType;

    /**
     * Gets the packaging type.
     *
     * @return the packaging type
     */
    public PackagingType getPackagingType() {
        return this.packagingType;
    }

    /**
     * Sets the packaging type.
     *
     * @param packagingType the new packaging type
     */
    public void setPackagingType(final PackagingType packagingType) {
        this.packagingType = packagingType;
    }

    /** The item. */
    @ManyToOne(fetch = FetchType.LAZY)
    @Index(name = "IP_ITEM_IX")
    private Item item;

    /**
     * Gets the item.
     *
     * @return the item
     */
    public Item getItem() {
        return this.item;
    }

    /**
     * Sets the item.
     *
     * @param item the new item
     */
    public void setItem(final Item item) {
        this.item = item;
    }
}