package com.apd.phoenix.service.model;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import org.hibernate.annotations.Index;
import org.hibernate.envers.Audited;
import com.apd.phoenix.service.business.IpSubdomainRestriction;

/**
 * This entity is used to store the information for a bulletin message. It has a many-to-many relationship 
 * with SystemUser, Account, and Credential. 
 * 
 * When an administrator chooses to associate a bulletin with an Account, Credential or User, a relationship 
 * is established between the bulletin and that entity. Then, when a user logs in using that account or 
 * credential, they will be shown the bulletin, unless it has expired.
 * 
 * @author RHC
 *
 */
@Entity
@Audited
@DiscriminatorValue(value = "LOGIN")
public class LoginCarouselDisplay extends CarouselDisplay {

    /**
     * 
     */
    private static final long serialVersionUID = -4635253889219112998L;

    @Index(name = "CAROUSEL_SUBDOMAIN_IDX")
    @Column(nullable = true)
    private String subdomain = IpSubdomainRestriction.GENERIC_SUBDOMAIN;

    public String getSubdomain() {
        return subdomain;
    }

    public void setSubdomain(String subdomain) {
        this.subdomain = subdomain;
    }

}