package com.apd.phoenix.service.rest.cxf;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriBuilder;
import org.apache.commons.lang.StringUtils;
import org.apache.cxf.jaxrs.impl.MetadataMap;
import com.apd.phoenix.service.business.CatalogBp;
import com.apd.phoenix.service.business.CatalogBp.SyncAction;
import com.apd.phoenix.service.business.ItemBp;
import com.apd.phoenix.service.business.ItemBp.NoVendorCatalogSpecifiedException;
import com.apd.phoenix.service.business.ItemRollbackException;
import com.apd.phoenix.service.model.Catalog;
import com.apd.phoenix.service.model.CsvUploadedItem;
import com.apd.phoenix.service.model.CsvUploadedItemField;
import com.apd.phoenix.service.model.Item;
import com.apd.phoenix.service.model.SyncItemResult;
import com.apd.phoenix.service.model.SyncItemResultSummary;
import com.apd.phoenix.service.model.dto.ItemRelationshipDto;
import com.apd.phoenix.service.persistence.jpa.CsvUploadedItemDao;

@Stateless
@Path("/items")
public class ItemSyncEndpoint {//implements ItemSyncService {

    private static final int ORACLE_MAX_STRING_LENGTH = 4000;

    public static final String ORIGINAL_ITEM_RELATIONSHIP = "original";
    public static final String VENDOR_NAME_ITEM_RELATIONSHIP = "vendor name";

    @Inject
    ItemBp itemBp;

    @Inject
    CatalogBp catalogBp;

    @Inject
    private CsvUploadedItemDao csvUploadedItemDao;

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces("application/xml")
    @Path("/xmlUpdate")
    public Response xmlUpdate(MetadataMap<String, String> itemData) {

        CsvUploadedItem toCreate = new CsvUploadedItem();
        for (Entry<String, List<String>> entry : itemData.entrySet()) {
            if (entry != null && entry.getValue() != null && !entry.getValue().isEmpty()
                    && StringUtils.isNotBlank(entry.getValue().get(0))
                    && entry.getValue().get(0).length() < ORACLE_MAX_STRING_LENGTH
                    && StringUtils.isNotBlank(entry.getKey())) {
                CsvUploadedItemField field = new CsvUploadedItemField();
                field.setKey(entry.getKey());
                field.setValue(entry.getValue().get(0));
                toCreate.getCsvUploadedItemFields().add(field);
            }
        }
        this.csvUploadedItemDao.create(toCreate);
        return Response.created(
                UriBuilder.fromResource(ItemSyncEndpoint.class).path(
                        String.valueOf((itemData.get("vendor sku").get(0)))).build()).build();
    }

    /**
     * Find by id.
     *
     * @param id the id
     * @return the response
     */
    @GET
    @Path("/{id:[0-9][0-9]*}")
    @Produces("application/xml")
    public Response findById(@PathParam("id") Long id) {
        Item item = this.itemBp.findById(id, Item.class);
        if (item == null) {
            return Response.status(Status.NOT_FOUND).build();
        }
        return Response.ok(item).build();
    }

    /**
     * Search for item by APD SKU and Vendor Name.
     * @param apdSku 
     * @param vendorName 
     *
     * @return the item
     */
    @GET
    @Produces("application/xml")
    public Response findBySkuAndVendor(@QueryParam("apdsku") String apdSku, @QueryParam("vendor") String vendorName) {
        final Item item = this.itemBp.searchItem(apdSku, vendorName);
        if (item == null) {
            return Response.status(Status.NOT_FOUND).build();
        }
        return Response.ok(item).build();
    }

    /**
     * Update.
     *
     * @param id the id
     * @param item 
     * @return the response
     */
    @PUT
    @Path("/{id:[0-9][0-9]*}")
    @Consumes("application/xml")
    public Response update(@PathParam("id") Long id, Item item) {
        this.itemBp.update(item);
        return Response.noContent().build();
    }

    /**
     * Replaces the item.
     * @param itemData 
     * @return the response
     */
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces("application/xml")
    @Path("/link")
    public Response setRelationship(MetadataMap<String, ItemRelationshipDto> itemData) {

        List<ItemRelationshipDto> original = itemData.remove(ORIGINAL_ITEM_RELATIONSHIP);
        itemBp.setRelationships(original.get(0).getSku(), original.get(0).getVendorName(), itemData);
        return Response.ok().build();
    }
}
