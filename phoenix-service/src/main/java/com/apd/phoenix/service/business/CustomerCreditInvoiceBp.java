package com.apd.phoenix.service.business;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.model.CustomerCreditInvoice;
import com.apd.phoenix.service.model.CustomerOrder;
import com.apd.phoenix.service.model.Invoice;
import com.apd.phoenix.service.model.LineItem;
import com.apd.phoenix.service.model.LineItemXReturn;
import com.apd.phoenix.service.model.LineItemXShipmentXTaxType;
import com.apd.phoenix.service.model.ReturnOrder;
import com.apd.phoenix.service.model.dto.CustomerCreditInvoiceDto;
import com.apd.phoenix.service.model.dto.ReturnOrderDto;
import com.apd.phoenix.service.persistence.jpa.CustomerCreditInvoiceDao;

/**
 * 
 * @author RHC
 *
 */
@Stateless
@LocalBean
public class CustomerCreditInvoiceBp extends AbstractBp<CustomerCreditInvoice> {

    private static final Logger logger = LoggerFactory.getLogger(CustomerCreditInvoiceBp.class);

    @Inject
    ReturnOrderBp returnOrderBp;

    @Inject
    CustomerOrderBp customerOrderBp;

    @Inject
    InvoiceBp invoiceBp;

    @Inject
    LineItemXReturnBp lineItemXReturnBp;

    /**
     * This method is injected with the correct Dao for initialization
     * 
     * @param dao
     */
    @Inject
    public void initDao(CustomerCreditInvoiceDao dao) {
        this.dao = dao;
    }

    public CustomerCreditInvoice credit(ReturnOrderDto returnOrderDto) {
        CustomerCreditInvoice customerCreditInvoice = new CustomerCreditInvoice();
        ReturnOrder returnOrder = returnOrderBp.getUnreconciledReturnOrder(returnOrderDto);
        if (returnOrder == null) {
            logger.error("Tried to credit a return that does not exist!!!!");
            return null;
        }
        customerCreditInvoice.setInvoiceNumber(customerOrderBp.getNextInvoiceNumber(returnOrder.getOrder()));
        customerCreditInvoice.setReturnOrder(returnOrder);
        customerCreditInvoice.setActualItems(new ArrayList<LineItem>());
        customerCreditInvoice.setDate(new Date());
        returnOrderBp.reconcile(returnOrder);
        for (LineItemXReturn lixr : returnOrder.getItems()) {
            lixr = lineItemXReturnBp.checkForNullReturnShipping(lixr);
            customerCreditInvoice.getActualItems().add(lixr.getLineItem());
        }
        customerCreditInvoice.setAmount(returnOrderBp.getReturnTotal(returnOrder));
        //customerInvoice.setBillingTypeCode(billingTypeCode); //TODO: Determine what this is/how it is set

        customerCreditInvoice = this.create(customerCreditInvoice);
        return customerCreditInvoice;
    }

    public CustomerCreditInvoice retrieveCustomerCreditInvoice(CustomerCreditInvoiceDto customerCreditInvoiceDto) {
        Invoice search = new Invoice();
        search.setInvoiceNumber(customerCreditInvoiceDto.getInvoiceNumber());
        List<Invoice> results = invoiceBp.searchByExample(search, 0, 1);
        if (results != null && !results.isEmpty()) {
            return (CustomerCreditInvoice) results.get(0);
        }
        return null;
    }

    public List<CustomerCreditInvoice> getCreditInvoicesForOrder(CustomerOrder order) {
        return ((CustomerCreditInvoiceDao) dao).getCreditInvoicesForOrder(order);
    }
}
