package com.apd.phoenix.service.business;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import com.apd.phoenix.service.model.SyncItemResult;
import com.apd.phoenix.service.persistence.jpa.SyncItemResultDao;

/**
 * This class provides business process methods for Addresses.
 * 
 * @author RHC
 *
 */
@Stateless
@LocalBean
public class SyncItemResultBp extends AbstractBp<SyncItemResult> {

    /**
     * This method is injected with the correct Dao for initialization
     * 
     * @param dao
     */
    @Inject
    public void initDao(SyncItemResultDao dao) {
        this.dao = dao;
    }

    public List<SyncItemResult> getResults(String correlationId) {
        return ((SyncItemResultDao) dao).getResults(correlationId);
    }

    public void removeRecords(String correlationId) {
        ((SyncItemResultDao) dao).removeRecords(correlationId);
    }

    public List<Long> getResults(String correlationId, int batch) {
        return ((SyncItemResultDao) dao).getResults(correlationId, batch);
    }

    public List<SyncItemResult> getByIds(List<Long> ids) {
        return ((SyncItemResultDao) dao).getByIds(ids);
    }

    public void detach(SyncItemResult syncItemResult) {
        ((SyncItemResultDao) dao).detach(syncItemResult);
    }

    public long getCount(String correlationId) {
        return ((SyncItemResultDao) dao).getCount(correlationId);
    }

    public List<String> csvRows(List<Long> ids) {
        List<SyncItemResult> results = this.getByIds(ids);
        List<String> toReturn = new ArrayList<String>();
        for (SyncItemResult result : results) {
            toReturn.add(csvRow(result));
        }
        return toReturn;
    }

    private String csvRow(SyncItemResult syncItemResult) {
        StringBuilder sb = new StringBuilder();

        if (StringUtils.isNotBlank(syncItemResult.getResultVendor())) {
            sb.append(syncItemResult.getResultVendor());
        }
        sb.append(",");
        if (StringUtils.isNotBlank(syncItemResult.getResultApdSku())) {
            sb.append(syncItemResult.getResultApdSku());
        }
        sb.append(",");
        sb.append(StringEscapeUtils.escapeCsv(StringUtils.defaultIfBlank(syncItemResult.getErrors(), "")));
        sb.append(",");
        sb.append(StringEscapeUtils.escapeCsv(StringUtils.defaultIfBlank(syncItemResult.getWarnings(), "")));
        return sb.toString();
    }
}
