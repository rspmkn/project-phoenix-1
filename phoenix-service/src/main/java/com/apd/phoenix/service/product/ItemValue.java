package com.apd.phoenix.service.product;

import java.io.Serializable;

/**
 *
 * @author RHC
 */
public class ItemValue implements Serializable {

    private static final long serialVersionUID = -8996124365679230150L;

    private Long id;

    /**
     * This constructor should not be used in application code.
     */
    @Deprecated
    public ItemValue() {
    }

    public ItemValue(Long id) {
        this.setId(id);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

}
