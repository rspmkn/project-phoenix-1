package com.apd.phoenix.service.business;

import java.util.HashSet;
import java.util.Set;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import com.apd.phoenix.service.model.Permission;
import com.apd.phoenix.service.model.SystemUser;
import com.apd.phoenix.service.persistence.jpa.PermissionDao;

@Stateless
@LocalBean
public class PermissionBp extends AbstractBp<Permission> {

    /**
     * This method is injected with the correct Dao for initialization
     * 
     * @param dao
     */
    @Inject
    public void initDao(PermissionDao dao) {
        this.dao = dao;
    }

    /**
     * Change the specified Permission's name to a different one.
     * 
     * @param permission
     * @param newName
     */
    public void renamePermission(Permission permission, String newName) {
        permission.setName(newName);
        this.dao.update(permission);
    }

    /**
     * Change the specified Permission's description to a different one.
     * 
     * @param permission
     * @param newDescription
     */
    public void changePermissionDescription(Permission permission, String newDescription) {
        permission.setDescription(newDescription);
        this.dao.update(permission);
    }

    /**
     * Retrieve a list of Users who possess a specified permission.
     * 
     * @param permission
     * @return
     */
    @SuppressWarnings("static-method")
    public Set<SystemUser> retrieveUsers(Permission permission) {
        //TODO: rewrite
        return new HashSet<SystemUser>();
    }
}
