package com.apd.phoenix.service.business;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import com.apd.phoenix.service.model.ItemSpecification;
import com.apd.phoenix.service.persistence.jpa.ItemSpecificationDao;

@Stateless
@LocalBean
public class ItemSpecificationBp extends AbstractBp<ItemSpecification> {

    /**
     * This method is injected with the correct Dao for initialization
     * 
     * @param dao
     */
    @Inject
    public void initDao(ItemSpecificationDao dao) {
        this.dao = dao;
    }
}
