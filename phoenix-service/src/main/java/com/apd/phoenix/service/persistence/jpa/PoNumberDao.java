package com.apd.phoenix.service.persistence.jpa;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import com.apd.phoenix.service.model.AccountXCredentialXUser;
import com.apd.phoenix.service.model.PoNumber;
import javax.persistence.Query;

/**
 * Purchase Order DAO stub
 * 
 * @author RHC
 *
 */
@Stateless
@LocalBean
public class PoNumberDao extends AbstractDao<PoNumber> {

    @SuppressWarnings("unchecked")
    public PoNumber getPoNumber(String value) {
        String hql = "SELECT p FROM PoNumber AS p WHERE p.value=:value";
        Query query = entityManager.createQuery(hql);
        query.setParameter("value", value);
        query.setMaxResults(1);
        List<PoNumber> result = query.getResultList();
        if (result == null || result.size() == 0) {
            return null;
        }
        else {
            return result.get(0);
        }
    }

    public List<PoNumber> poNumbersForCashout(AccountXCredentialXUser axcxu) {
        List<Object> cred = poResults(axcxu.getId(), "cred");
        List<Object> account = poResults(axcxu.getId(), "account");
        List<PoNumber> toReturn = new ArrayList<>();
        for (Object obj : cred) {
        	if (obj != null) {
        		toReturn.add((PoNumber)obj);
        	}
        }
        for (Object obj : account) {
        	if (obj != null) {
        		toReturn.add((PoNumber)obj);
        	}
        }
        return toReturn;
    }

    @SuppressWarnings("unchecked")
    private List<Object> poResults(Long axcxuId, String accountOrCred) {
        String hql = "SELECT " + accountOrCred + "Pos FROM AccountXCredentialXUser AS axcxu LEFT JOIN "
                + "axcxu.credential.blanketPos AS credPos LEFT JOIN axcxu.account.blanketPos AS accountPos "
                + "WHERE axcxu.id = :axcxuId";
        //Creates the query
        Query query = entityManager.createQuery(hql);
        query.setParameter("axcxuId", axcxuId);

        setItemsReturned(query, 0, 0);
        return query.getResultList();
    }

    public void detach(PoNumber po) {
        if (this.entityManager.contains(po)) {
            this.entityManager.detach(po);
        }
    }
}
