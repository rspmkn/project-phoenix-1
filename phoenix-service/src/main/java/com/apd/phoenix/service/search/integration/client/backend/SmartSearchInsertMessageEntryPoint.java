package com.apd.phoenix.service.search.integration.client.backend;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.MessageProducer;
import javax.jms.ObjectMessage;
import javax.jms.Queue;
import javax.jms.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.business.CatalogBp;
import com.apd.phoenix.service.persistence.multitenancy.impl.CurrentTenantIdentifierResolverImpl;

@Stateless
public class SmartSearchInsertMessageEntryPoint {

    private static final Logger LOGGER = LoggerFactory.getLogger(SmartSearchInsertMessageEntryPoint.class);

    @Resource(mappedName = "java:/activemq/ConnectionFactory")
    private ConnectionFactory connectionFactory;

    @Resource(mappedName = "java:/activemq/smartsearch-insert")
    private Queue queue;

    public void scheduleRequest(CatalogBp.SmartSearchInsertMessage changes) {
        Connection connection = null;
        try {
            connection = connectionFactory.createConnection();
            Session msession = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            MessageProducer messageProducer = msession.createProducer(queue);
            ObjectMessage message = msession.createObjectMessage();
            message.setObject(changes);
            message.setLongProperty("tenantId", CurrentTenantIdentifierResolverImpl.getCurrentTenant());
            messageProducer.send(message);
        }
        catch (Exception e) {
            LOGGER.error("An error occured:", e);
            ;
        }
        finally {
            try {
                connection.close();
            }
            catch (Exception e) {
                LOGGER.error("Could not close connection", e);
            }
        }
    }
}
