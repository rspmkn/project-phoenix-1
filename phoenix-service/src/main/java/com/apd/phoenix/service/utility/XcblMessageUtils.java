package com.apd.phoenix.service.utility;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;
import javax.enterprise.context.Dependent;

@Dependent
public class XcblMessageUtils {

    public static final String[] XCBL_ORDER_DATE_FORMATS = new String[] { "yyyy-MM-dd'T'hh:mm:ss",
            "yyyy-MM-dd'T'hh:mm:ssXXX" };

    public static String generateTimeStamp() {
        return generateTimeStamp(new Date());
    }

    public static String generateTimeStamp(Date date) {
        SimpleDateFormat xcblDateFormat = new SimpleDateFormat(XCBL_ORDER_DATE_FORMATS[0]);
        return xcblDateFormat.format(date);
    }

    public String generateEnvelopeId() {
        return UUID.randomUUID().toString();
    }
}
