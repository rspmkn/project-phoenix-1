package com.apd.phoenix.service.product;

import java.io.Serializable;

/**
 * Key for looking up customer or vendor item based on SKU, vendor, and catalog
 *
 * @author RHC
 */
public class ItemKey implements Serializable {

    private static final long serialVersionUID = 1241128746819792090L;

    private String sku;
    private String vendorName;
    private Long customerCatalogId;
    private Long tenantId;

    /**
     * This constructor should not be used in application code.
     */
    @Deprecated
    public ItemKey() {
    }

    public ItemKey(String sku, String vendorName, Long customerCatalogId, Long tenantId) {
        this.sku = sku;
        this.vendorName = vendorName;
        this.customerCatalogId = customerCatalogId;
        this.setTenantId(tenantId);
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public String getVendorName() {
        return vendorName;
    }

    public void setVendorName(String vendorName) {
        this.vendorName = vendorName;
    }

    public Long getCustomerCatalogId() {
        return customerCatalogId;
    }

    public void setCustomerCatalogId(Long customerCatalogId) {
        this.customerCatalogId = customerCatalogId;
    }

    public Long getTenantId() {
        return tenantId;
    }

    public void setTenantId(Long tenantId) {
        this.tenantId = tenantId;
    }

}
