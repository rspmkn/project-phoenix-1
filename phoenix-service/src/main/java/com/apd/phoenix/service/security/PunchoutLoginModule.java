package com.apd.phoenix.service.security;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.security.auth.login.LoginException;
import javax.sql.DataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author dnorris
 */
public class PunchoutLoginModule extends AbstractMultiTenantLoginModule {

    private String removeExpiredSessionQuery = "delete from {SCHEMA}.PUNCHOUTSESSION where inittime < NOW() - INTERVAL '24' hour";
    private static final Logger logger = LoggerFactory.getLogger(PunchoutLoginModule.class);

    @Override
    public boolean login() throws LoginException {
    	removeExpiredSessionQuery = addTenantSchemaSwitchToQuery(removeExpiredSessionQuery, currentTenantSchema);
    	
        try (Connection conn = ((DataSource) (new InitialContext()).lookup(dsJndiName)).getConnection()) {
            PreparedStatement removeExpiredSessions = conn.prepareStatement(removeExpiredSessionQuery);
            removeExpiredSessions.execute();          
            boolean loginSucceeded = super.login();
            return loginSucceeded;
        } catch (NamingException ex) {
            LoginException le = new LoginException("Error looking up DataSource from: " + dsJndiName);
            logger.error("Error looking up DataSource from: " + dsJndiName, le);
            le.initCause(ex);
            throw le;
        } catch (SQLException ex) {
            LoginException le = new LoginException("removeExpiredSessions query failed");
            le.initCause(ex);
            logger.error("removeExpiredSessions query failed", le);
            throw le;
        }
    }
}
