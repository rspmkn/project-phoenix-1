package com.apd.phoenix.service.utility;

import java.math.BigDecimal;
import java.math.BigInteger;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.model.CatalogXItem;
import com.apd.phoenix.service.model.Item;
import com.ussco.ws.ecatalog.catalog._1.FilterType;
import com.ussco.ws.ecatalog.catalog._1.FilterValueType;
import com.ussco.ws.ecatalog.catalog._1.IdentificationType;
import com.ussco.ws.ecatalog.catalog._1.ItemResultStyle;
import com.ussco.ws.ecatalog.catalog._1.ListNameType;
import com.ussco.ws.ecatalog.catalog._1.MatchType;
import com.ussco.ws.ecatalog.catalog._1.SortType;
import com.ussco.ws.ecatalog.catalog._1.StatusType;

public class SmartSearchUtils {

    private static final Logger LOG = LoggerFactory.getLogger(SmartSearchUtils.class);

    public enum PriceCategory {
        //TODO: refactor this, per PHOEN-5374
        ZERO_TO_TWENTY_FIVE("0", BigDecimal.ZERO, new BigDecimal("25")), TWENTY_FIVE_TO_FIFTY("1",
                new BigDecimal("25"), new BigDecimal("50")), FIFTY_TO_ONE_HUNDRED("2", new BigDecimal("50"),
                new BigDecimal("100")), ONE_HUNDRED_TO_TWO_HUNDRED("3", new BigDecimal("100"), new BigDecimal("200")), TWO_HUNDRED_PLUS(
                "4", new BigDecimal("200"), null);

        PriceCategory(String code, BigDecimal lower, BigDecimal upper) {
            this.code = code;
            this.lowerBound = lower;
            this.upperBound = upper;
        }

        private BigDecimal upperBound;
        private BigDecimal lowerBound;
        private String code;

        public boolean priceWithingCategory(BigDecimal price) {
            if (this.upperBound == null) {
                return price.compareTo(lowerBound) >= 0;
            }
            else {
                return price.compareTo(upperBound) < 0 && price.compareTo(lowerBound) >= 0;
            }
        }

        public String getCode() {
            return code;
        }

    }

    public static String getCategoryCodeFromPrice(BigDecimal price) {
        //TODO: refactor this, per PHOEN-5374
        for (PriceCategory cateogry : PriceCategory.values()) {
            if (cateogry.priceWithingCategory(price)) {
                return cateogry.getCode();
            }
        }
        LOG.error("Could not find price category for price " + price.toPlainString());
        return null;
    }

    public enum SortCode {

        BestMatch("BM", "Best Match"), BrandAscending("BA", "Brand Ascending"), BrandDescending("BD",
                "Brand Descending"), MostPopular("MP", "Most Popular"), ItemAscending("IA", "Item Ascending"), ItemDescending(
                "ID", "Item Descending");

        private final String label;
        private final String description;

        private SortCode(String label, String description) {
            this.label = label;
            this.description = description;
        }

        public String getLabel() {
            return label;
        }

        public String getDescription() {
            return description;
        }
    }

    public enum ItemIndicator {

        NonReturnable("NonReturnable"), Recycled("Recycled"), AssemblyRequired("AssemblyRequired"), MinorityVendor(
                "Minority Vendor"), MSDS("MSDS"), RelatedItemsAvailable("RelatedItemsAvailable"), SmallPackage(
                "SmallPackage"), Stocked("Stocked"), ValuePack("ValuePack"), Warranty("Warranty"), ContractItem(
                "ContractItem"), Green("Green");

        private final String label;

        private ItemIndicator(String label) {
            this.label = label;
        }

        public String getLabel() {
            return label;
        }
    }

    public static IdentificationType buildIdentificationType(String sessionId, String userAgent) {
        IdentificationType toReturn = new IdentificationType();
        toReturn.setUserAgent(userAgent);
        toReturn.setSessionId(sessionId);
        return toReturn;
    }

    public static ListNameType getIncExcList(String listName, String incExcType) {
        ListNameType listNameType = new ListNameType();
        listNameType.setIncExcType(incExcType);
        listNameType.setListName(listName);

        StatusType statusType = new StatusType();
        statusType.setStatusCode("");
        statusType.setStatusMessage("");
        listNameType.setResultStatus(statusType);

        return listNameType;
    }

    public static ItemResultStyle getItemResultStyle(String style) {
        ItemResultStyle itemResultStyle = new ItemResultStyle();
        itemResultStyle.setItemResultStyle(style);
        return itemResultStyle;
    }

    public static FilterType getFilterType(String value, String filterStyle, BigInteger sequence) {
        return getFilterType(value, filterStyle, filterStyle, sequence);
    }

    public static FilterType getFilterType(String value, String filterStyle, String description, BigInteger sequence) {
        return getFilterType(value, filterStyle, description, sequence, "Standard");
    }

    public static FilterType getFilterType(String value, String filterStyle, String description, BigInteger sequence,
            String keywordInterface) {

        FilterType filterType = new FilterType();
        filterType.setFilterStyle(filterStyle);
        filterType.setFilterDescription(description);
        filterType.setCrossReference(MatchType.EXT);
        filterType.setSequence(sequence);
        filterType.setKeywordInterface(keywordInterface);

        FilterValueType filterValueType = new FilterValueType();
        filterValueType.setValue(value);
        filterType.getFilterValue().add(filterValueType);

        return filterType;
    }

    public static SortType getSortType(String type) {
        SortType sortType = new SortType();
        sortType.setSortCode(type);
        return sortType;
    }

    public static Boolean isUsscoItem(CatalogXItem catalogXItem) {
        if (catalogXItem != null) {
            return isUsscoItem(catalogXItem.getItem());
        }
        return false;
    }

    public static Boolean isUsscoItem(Item item) {
        if (item != null && item.getVendorCatalog() != null && item.getVendorCatalog().getVendor() != null) {
            return "USSCO".equals(item.getVendorCatalog().getVendor().getPartnerId());
        }

        return false;
    }

}
