package com.apd.phoenix.service.business;

import java.util.List;
import java.util.Properties;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import com.apd.phoenix.core.utility.TenantConfigRepository;
import com.apd.phoenix.service.model.CustomerOrder;
import com.apd.phoenix.service.model.OrderBRMSRecord;
import com.apd.phoenix.service.persistence.jpa.OrderBRMSRecordDao;

@Stateless
@LocalBean
public class OrderBRMSRecordBp extends AbstractBp<OrderBRMSRecord> {

    private static String[] STALE_STATUS = {};

    @Inject
    private OrderProcessLookupBp orderProcessBp;

    /**
     * This method is injected with the correct Dao for initialization
     * 
     * @param dao
     */
    @Inject
    public void initDao(OrderBRMSRecordDao dao) {
        this.dao = dao;
    };

    public Boolean isActive(Long orderId) {
        return getFirst(((OrderBRMSRecordDao) dao).isActive(orderId));
    }

    public Integer getSessionId(Long orderId) {
        return getFirst(((OrderBRMSRecordDao) dao).getSessionId(orderId));
    }

    public OrderBRMSRecord getRecordByOrderId(Long orderId) {
        return getFirst(((OrderBRMSRecordDao) dao).getRecordByOrderId(orderId));
    }

    public OrderBRMSRecord getRecordBySessionId(Integer sessionId) {
        return getFirst(((OrderBRMSRecordDao) dao).getRecordBySessionId(sessionId));
    }

    public void setReplaceSession(Long orderId, boolean value) {
        ((OrderBRMSRecordDao) dao).setReplaceSession(orderId, value);
    }

    public Boolean getReplaceSession(Long orderId) {
        return getFirst(((OrderBRMSRecordDao) dao).getReplaceSession(orderId));
    }

    private <T> T getFirst(List<T> list) {
        if (list == null || list.size() == 0) {
            return null;
        }
        return list.get(0);
    }

    public void createRecord(Long orderId, Integer sessionId, Boolean active) {
        Properties versionProperties = TenantConfigRepository.getInstance().getProperties("guvnor");
        OrderBRMSRecord newRecord = new OrderBRMSRecord();
        newRecord.setActive(active);
        newRecord.setSessionId(sessionId);
        newRecord.setSnapshot(versionProperties.getProperty("snapshot"));
        newRecord.setEntryId(orderId);

        create(newRecord);
    }

    public void setActive(Long orderId, Boolean active) {
        ((OrderBRMSRecordDao) dao).setActive(orderId, active);
    }

    public void setProcessId(long id, long processId) {
        OrderBRMSRecord oldRecord = getRecordByOrderId(id);
        oldRecord = this.findAndLock(oldRecord.getId(), OrderBRMSRecord.class);
        oldRecord.setProcessId(processId);
        update(oldRecord);
    }

    public boolean isStale(long orderId) {
        CustomerOrder customerOrder = orderProcessBp.getOrderFromProcessId(orderId);
        String statusValue = customerOrder.getStatus().getValue();
        for (int i = 0; i < STALE_STATUS.length; i++) {
            if (statusValue.equals(STALE_STATUS[i])) {
                return true;
            }
        }
        return false;
    }

    public Long getProcessId(long orderId) {
        return getFirst(((OrderBRMSRecordDao) dao).getProcessId(orderId));
    }

    public String getSnapshot(long orderId) {
        return getFirst(((OrderBRMSRecordDao) dao).getSnapshot(orderId));
    }
}
