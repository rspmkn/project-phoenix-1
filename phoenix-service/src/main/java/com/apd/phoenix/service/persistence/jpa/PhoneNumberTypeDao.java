package com.apd.phoenix.service.persistence.jpa;

import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.model.PhoneNumberType;
import com.apd.phoenix.service.model.PhoneNumberType.PhoneNumberTypeEnum;

/**
 * PhoneNumberType DAO stub
 * 
 * @author RHC
 *
 */
@Stateless
@LocalBean
public class PhoneNumberTypeDao extends AbstractDao<PhoneNumberType> {

    private static final Logger LOG = LoggerFactory.getLogger(PhoneNumberTypeDao.class);

    public PhoneNumberType findByType(PhoneNumberTypeEnum phoneNumberTypeEnum) {
        String hql = "SELECT type FROM PhoneNumberType AS type WHERE type.name=:name";
        Query query = entityManager.createQuery(hql);
        query.setParameter("name", phoneNumberTypeEnum.getValue());
        List<PhoneNumberType> result = (List<PhoneNumberType>) query.getResultList();
        if (result == null || result.isEmpty()) {
            LOG.error("Phone number type does not exist for enum type " + phoneNumberTypeEnum.getValue());
            return null;
        }
        else {
            return result.get(0);
        }
    }
}
