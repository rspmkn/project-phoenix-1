package com.apd.phoenix.service.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * This entity contains the information relevant for an Inventory Transaction Type.
 * 
 * @author FWS-Muthu
 * 
 */
@Entity
public class InventoryTransactionTypes implements Serializable, com.apd.phoenix.service.model.Entity {

    private static final long serialVersionUID = 7185662040258275545L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id = null;

    @Column(nullable = false, unique = true)
    private Long transcationtypeid = null;

    @Column(nullable = false)
    private String transactiontype;

    @Column(nullable = false)
    private String description;

    public Long getId() {
        return this.id;
    }

    public Long getTranscationtypeid() {
        return transcationtypeid;
    }

    public void setTranscationtypeid(Long transcationtypeid) {
        this.transcationtypeid = transcationtypeid;
    }

    public String getTransactiontype() {
        return transactiontype;
    }

    public void setTransactiontype(String transactiontype) {
        this.transactiontype = transactiontype;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setId(Long id) {
        this.id = id;
    }

}