package com.apd.phoenix.service.business;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import com.apd.phoenix.service.model.GlobalSetting;
import com.apd.phoenix.service.persistence.jpa.GlobalSettingDao;

/**
 * This class provides business process methods for Process.
 * 
 * @author RHC
 *
 */
@Stateless
@LocalBean
public class GlobalSettingBp extends AbstractBp<GlobalSetting> {

    /**
     * This method is injected with the correct Dao for initialization
     * 
     * @param dao
     */
    @Inject
    public void initDao(GlobalSettingDao dao) {
        this.dao = dao;
    }
}
