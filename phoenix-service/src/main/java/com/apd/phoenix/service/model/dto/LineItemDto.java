package com.apd.phoenix.service.model.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.apd.phoenix.service.model.LineItem;

public class LineItemDto implements Serializable {
	
	private Set<ValidationErrorDto> validationErrorDtos = new HashSet<>();

    private static final long serialVersionUID = -5463754245386248718L;
    
    protected static final Logger LOGGER = LoggerFactory.getLogger(LineItemDto.class);

    private ItemDto item;

    private Long expectedItemId;

    private LineItemStatusDto status;

    private LineItemStatusDto newStatus;

    private String supplierPartId;
    
    private String buyerPartNumber;

    private String manufacturerPartId;

    private String apdSku;
    
    private String supplierPartAuxiliaryId;

    private BigInteger quantity;

    private BigInteger quantityShipped;

    private BigInteger quantityCancelled = BigInteger.ZERO;

    //applies to both invoices and charges
    private BigInteger quantityInvoicedToCustomer;
    
    private BigInteger quantityInvoicedByVendor;

    private Integer lineNumber;

    private Integer parentLineNumber;

    private BigDecimal cost;

    private BigDecimal unitPrice;

    private BigDecimal estimatedShippingAmount;

    private Boolean taxable;

    private String shortName;

    private String description;

    private Boolean core;

    private UnitOfMeasureDto unitOfMeasure;

    private String unspscClassification;

    private String manufacturerName;
    
    private String vendorName;

    private Integer leadTime;

    private SkuDto customerSku;

    private BigDecimal maximumTaxToCharge;

    private String purchaseComment;

    private List<String> vendorComments;

    private Boolean valid;
    
    private BigDecimal subTotal;
    
    private BigInteger backorderedQuantity;
    
    private List<String> customerComments;
    
    private String customerLineNumber;
    
    private Integer originalQuantity;
    
    private PurchaseOrderDto order;
    
    private Boolean fee;
    
    private String extraneousCustomerSku;
    
    private String globalTradeItemNumber;
    
    private String universalProductCode;
    
    private Boolean canBeRetried;
    
    private String customerExpectedUoM;
    
    private String globalTradeIdentificatonNumber;

    public ItemDto getItem() {
        return item;
    }

    public void setItem(ItemDto item) {
        this.item = item;
    }

    public String getSupplierPartId() {
        return supplierPartId;
    }

    public void setSupplierPartId(String supplierPartId) {
        this.supplierPartId = supplierPartId;
    }

    public String getManufacturerPartId() {
        return manufacturerPartId;
    }

    public void setManufacturerPartId(String manufacturerPartId) {
        this.manufacturerPartId = manufacturerPartId;
    }

    public String getApdSku() {
        return apdSku;
    }

    public void setApdSku(String apdSku) {
        this.apdSku = apdSku;
    }
    
    public String getSupplierPartIdAndSupplierPartAuxId() {
        String ids = this.getSupplierPartId();
        if (StringUtils.isNotBlank(this.getSupplierPartAuxiliaryId())) {
            ids += "|" + this.getSupplierPartAuxiliaryId();
            if (this.getOrder() != null && this.getOrder().getApdPoNumber() != null) {
            	ids += "|" + this.getOrder().getApdPoNumber();
            }
        }
        return ids;
    }

    public String getSupplierPartAuxiliaryId() {
        return supplierPartAuxiliaryId;
    }

    public void setSupplierPartAuxiliaryId(String supplierPartAuxiliaryId) {
        this.supplierPartAuxiliaryId = supplierPartAuxiliaryId;
    }

    public BigInteger getQuantity() {
        return quantity;
    }

    public void setQuantity(BigInteger quantity) {
        this.quantity = quantity;
    }

    public Integer getLineNumber() {
        return lineNumber;
    }

    public void setLineNumber(Integer lineNumber) {
        this.lineNumber = lineNumber;
    }

    public Integer getParentLineNumber() {
        return parentLineNumber;
    }

    public void setParentLineNumber(Integer parentLineNumber) {
        this.parentLineNumber = parentLineNumber;
    }

    public BigDecimal getCost() {
        return cost;
    }

    public void setCost(BigDecimal cost) {
        this.cost = cost;
    }

    public BigDecimal getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(BigDecimal unitPrice) {
        this.unitPrice = unitPrice;
    }

    public BigDecimal getEstimatedShippingAmount() {
        return estimatedShippingAmount;
    }

    public void setEstimatedShippingAmount(BigDecimal estimatedShippingAmount) {
        this.estimatedShippingAmount = estimatedShippingAmount;
    }

    public Boolean getTaxable() {
        return taxable;
    }

    public void setTaxable(Boolean taxable) {
        this.taxable = taxable;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getCore() {
        return core;
    }

    public void setCore(Boolean core) {
        this.core = core;
    }

    public UnitOfMeasureDto getUnitOfMeasure() {
        return unitOfMeasure;
    }

    public void setUnitOfMeasure(UnitOfMeasureDto unitOfMeasure) {
        this.unitOfMeasure = unitOfMeasure;
    }

    public String getUnspscClassification() {
        return unspscClassification;
    }

    public void setUnspscClassification(String unspscClassification) {
        this.unspscClassification = unspscClassification;
    }

    public String getManufacturerName() {
        return manufacturerName;
    }

    public void setManufacturerName(String manufacturerName) {
        this.manufacturerName = manufacturerName;
    }

    public Integer getLeadTime() {
        return leadTime;
    }

    public void setLeadTime(Integer leadTime) {
        this.leadTime = leadTime;
    }
    
    public SkuDto getCustomerSku() {
    	SkuDto custSku = null;
    	if (StringUtils.isNotBlank(this.getBuyerPartNumber())) {
	    	custSku = new SkuDto();
	    	SkuTypeDto skuType = new SkuTypeDto();
	    	skuType.setName("customer");
	    	custSku.setType(skuType);
	    	custSku.setValue(this.buyerPartNumber);
    	}
        return custSku;
    }

    public void setCustomerSku(SkuDto customerSku) {
    	if (customerSku != null && customerSku.getType() != null && customerSku.getType().getName().equals("customer")) {
    		this.buyerPartNumber = customerSku.getValue();
    	} else {
            if(customerSku!=null && !StringUtils.isEmpty(customerSku.getValue())){
    		LOGGER.warn("Unable to set customer Sku.  Provided Sku "+ customerSku.getValue() + " has invalid SkuType.");
            }
    	}
    }
    
    public SkuDto getVendorSku() {
    	SkuDto vendorSku = null;
    	if (StringUtils.isNotBlank(this.supplierPartId)) {
	    	vendorSku = new SkuDto();
	    	SkuTypeDto skuType = new SkuTypeDto();
	    	skuType.setName("vendor");
	    	vendorSku.setType(skuType);
	    	vendorSku.setValue(this.supplierPartId);
    	}
        return vendorSku;
    }

    public void setVendorSku(SkuDto vendorSku) {
    	if (vendorSku != null && vendorSku.getType() != null && vendorSku.getType().getName().equals("vendor")) {
    		this.supplierPartId = vendorSku.getValue();
    	} else {
    		LOGGER.warn("Unable to set vendor Sku.  Provided Sku is null or has invalid SkuType.");
    	}
    }

    public BigDecimal getMaximumTaxToCharge() {
        return maximumTaxToCharge;
    }

    public void setMaximumTaxToCharge(BigDecimal maximumTaxToCharge) {
        this.maximumTaxToCharge = maximumTaxToCharge;
    }

    public String getPurchaseComment() {
        return purchaseComment;
    }

    public void setPurchaseComment(String purchaseComment) {
        this.purchaseComment = purchaseComment;
    }

    public LineItemStatusDto getStatus() {
        return status;
    }

    public void setStatus(LineItemStatusDto status) {
        this.status = status;
    }

    public void setVendorComments(List<String> comments) {
        this.vendorComments = comments;
    }

    /**
     * @return the vendorComments
     */
    public List<String> getVendorComments() {
        return vendorComments;
    }

    public Boolean getValid() {
        return valid;
    }

    public void setValid(Boolean valid) {
        this.valid = valid;
    }

    public LineItemStatusDto getNewStatus() {
        return newStatus;
    }

    public void setNewStatus(LineItemStatusDto newStatus) {
        this.newStatus = newStatus;
    }

    public BigInteger getQuantityShipped() {
        return quantityShipped;
    }

    public void setQuantityShipped(BigInteger quantityShipped) {
        this.quantityShipped = quantityShipped;
    }

    public BigInteger getQuantityCancelled() {
        return quantityCancelled;
    }

    public void setQuantityCancelled(BigInteger quantityCancelled) {
        this.quantityCancelled = quantityCancelled;
    }

    public BigInteger getQuantityInvoicedToCustomer() {
        return quantityInvoicedToCustomer;
    }

    public void setQuantityInvoicedToCustomer(BigInteger quantityInvoicedToCustomer) {
        this.quantityInvoicedToCustomer = quantityInvoicedToCustomer;
    }

    public Long getExpectedItemId() {
        return expectedItemId;
    }

    public void setExpectedItemId(Long expectedItemId) {
        this.expectedItemId = expectedItemId;
    }

	public Set<ValidationErrorDto> getValidationErrorDtos() {
		return validationErrorDtos;
	}

	public void setValidationErrorDtos(Set<ValidationErrorDto> validationErrorDtos) {
		this.validationErrorDtos = validationErrorDtos;
	}

	public String getVendorName() {
		return vendorName;
	}

	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}
	
	public BigDecimal getSubTotal(){
		try {
			return unitPrice.multiply(BigDecimal.valueOf(quantity.longValue())).setScale(2, RoundingMode.CEILING);
		} catch (NullPointerException e){
			return BigDecimal.ZERO;
		}
	}
	
	public void setSubTotal(BigDecimal subTotal){
		this.subTotal = subTotal;
	}

	public String getBuyerPartNumber() {
		return buyerPartNumber;
	}

	public void setBuyerPartNumber(String buyerPartNumber) {
		this.buyerPartNumber = buyerPartNumber;
	}

	public BigInteger getBackorderedQuantity() {
		return backorderedQuantity;
	}

	public void setBackorderedQuantity(BigInteger backorderedQuantity) {
		this.backorderedQuantity = backorderedQuantity;
	}
        
        @Override
        public LineItemDto clone(){
            LineItemDto clone = new LineItemDto();
            clone.setApdSku(apdSku);
            clone.setBackorderedQuantity(backorderedQuantity);
            clone.setBuyerPartNumber(buyerPartNumber);
            clone.setCore(core);
            clone.setCost(cost);
            clone.setCustomerSku(customerSku);
            clone.setDescription(description);
            clone.setEstimatedShippingAmount(estimatedShippingAmount);
            clone.setMaximumTaxToCharge(maximumTaxToCharge);
            clone.setExpectedItemId(expectedItemId);
            clone.setItem(item);
            clone.setLeadTime(leadTime);
            clone.setLineNumber(lineNumber);
            clone.setManufacturerName(manufacturerName);
            clone.setManufacturerPartId(manufacturerPartId);
            clone.setNewStatus(newStatus);
            clone.setParentLineNumber(parentLineNumber);
            clone.setPurchaseComment(purchaseComment);
            clone.setQuantity(quantity);
            clone.setQuantityInvoicedToCustomer(quantityInvoicedToCustomer);
            clone.setQuantityInvoicedByVendor(quantityInvoicedByVendor);
            clone.setQuantityShipped(quantityShipped);
            clone.setShortName(shortName);
            clone.setStatus(status);
            clone.setSubTotal(subTotal);
            clone.setSupplierPartAuxiliaryId(supplierPartAuxiliaryId);
            clone.setSupplierPartId(supplierPartId);
            clone.setTaxable(taxable);
            clone.setUnitOfMeasure(unitOfMeasure);
            clone.setUnitPrice(unitPrice);
            clone.setUnspscClassification(unspscClassification);
            clone.setValid(valid);
            clone.setValidationErrorDtos(validationErrorDtos);            
            clone.setVendorComments(vendorComments);
            clone.setVendorName(vendorName);
            return clone;
        }

    /**
     * @return the customerComments
     */
    public List<String> getCustomerComments() {
        return customerComments;
    }

    /**
     * @param customerComments the customerComments to set
     */
    public void setCustomerComments(List<String> customerComments) {
        this.customerComments = customerComments;
    }

    /**
     * @return the customerLineNumber
     */
    public String getCustomerLineNumber() {
        return customerLineNumber;
    }

    /**
     * @param customerLineNumber the customerLineNumber to set
     */
    public void setCustomerLineNumber(String customerLineNumber) {
        this.customerLineNumber = customerLineNumber;
    }

    /**
     * @return the originalQuantity
     */
    public Integer getOriginalQuantity() {
        return originalQuantity;
    }

    /**
     * @param originalQuantity the originalQuantity to set
     */
    public void setOriginalQuantity(Integer originalQuantity) {
        this.originalQuantity = originalQuantity;
    }

	public PurchaseOrderDto getOrder() {
		return order;
	}

	public void setOrder(PurchaseOrderDto order) {
		this.order = order;
	}

	public Boolean getFee() {
		if (fee != null) {
			return fee;
		}
		return LineItem.FEE_ITEM_CATEGORY.equals(manufacturerName);
	}

	public void setFee(Boolean fee) {
		this.fee = fee;
	}

    /**
     * @return the quantityInvoicedByVendor
     */
    public BigInteger getQuantityInvoicedByVendor() {
        return quantityInvoicedByVendor;
    }

    /**
     * @param quantityInvoicedByVendor the quantityInvoicedByVendor to set
     */
    public void setQuantityInvoicedByVendor(BigInteger quantityInvoicedByVendor) {
        this.quantityInvoicedByVendor = quantityInvoicedByVendor;
    }
    
    /**
     * Legacy DTO method
     * 
     * @return
     */
    @Deprecated
    public BigInteger getQuantityInvoiced() {
    	return this.quantityInvoicedToCustomer;
    }
    
    /**
     * Legacy DTO method
     * 
     * @return
     */
    @Deprecated
    public void setQuantityInvoiced(BigInteger quantityInvoiced) {
    	this.quantityInvoicedToCustomer = quantityInvoiced;
    }

    /**
     * @return the extraneousCustomerSku
     */
    public String getExtraneousCustomerSku() {
        return extraneousCustomerSku;
    }

    /**
     * @param extraneousCustomerSku the extraneousCustomerSku to set
     */
    public void setExtraneousCustomerSku(String extraneousCustomerSku) {
        this.extraneousCustomerSku = extraneousCustomerSku;
    }

    /**
     * @return the globalTradeItemNumber
     */
    public String getGlobalTradeItemNumber() {
        return globalTradeItemNumber;
    }

    /**
     * @param globalTradeItemNumber the globalTradeItemNumber to set
     */
    public void setGlobalTradeItemNumber(String globalTradeItemNumber) {
        this.globalTradeItemNumber = globalTradeItemNumber;
    }

    /**
     * @return the universalProductCode
     */
    public String getUniversalProductCode() {
        return universalProductCode;
    }

    /**
     * @param universalProductCode the universalProductCode to set
     */
    public void setUniversalProductCode(String universalProductCode) {
        this.universalProductCode = universalProductCode;
    }
    
    public Boolean getCanBeRetried() {
		return canBeRetried;
	}

	public void setCanBeRetried(Boolean canBeRetried) {
		this.canBeRetried = canBeRetried;
	}

	public boolean matchesItemOnOrder(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
    	if (that instanceof LineItemDto) {
    		LineItemDto dtoThat = (LineItemDto) that;
    		boolean skuMatches = (this.apdSku != null && this.apdSku.equals(dtoThat.getApdSku()))
                    || (this.buyerPartNumber != null && this.buyerPartNumber.equals(dtoThat.getBuyerPartNumber())) 
    				|| (this.buyerPartNumber != null && this.buyerPartNumber.equals(dtoThat.getSupplierPartId())) 
    				|| (this.buyerPartNumber != null && this.buyerPartNumber.equals(dtoThat.getSupplierPartAuxiliaryId())) 
    				|| (this.buyerPartNumber != null && dtoThat.getCustomerSku() != null && this.buyerPartNumber.equals(dtoThat.getCustomerSku().getValue())) 
    				|| (this.supplierPartId != null && this.supplierPartId.equals(dtoThat.getSupplierPartId()))
    				|| (this.supplierPartId != null && this.supplierPartId.equals(dtoThat.getBuyerPartNumber())) 
    				|| (this.supplierPartId != null && this.supplierPartId.equals(dtoThat.getSupplierPartAuxiliaryId())) 
    				|| (this.supplierPartAuxiliaryId != null && this.supplierPartAuxiliaryId.equals(dtoThat.getBuyerPartNumber()))
    				|| (this.supplierPartAuxiliaryId != null && this.supplierPartAuxiliaryId.equals(dtoThat.getSupplierPartId()))
    				|| (this.customerSku != null && this.customerSku.getValue() != null && this.customerSku.getValue().equals(dtoThat.getBuyerPartNumber()))
    				|| (this.customerSku != null && this.customerSku.getValue() != null && dtoThat.getCustomerSku() != null && this.customerSku.getValue().equals(dtoThat.getCustomerSku().getValue())) 
    				|| (this.manufacturerPartId != null && this.manufacturerPartId.equals(dtoThat.getManufacturerPartId()));
    		return skuMatches && (dtoThat.getLineNumber() == null || this.getLineNumber() == null || dtoThat.getLineNumber().equals(this.getLineNumber()));
    	}
    	else if (that instanceof LineItem) {
    		return ((LineItem) that).matchesItemOnOrder(this);
    	}
    	return super.equals(that);
    }
    
    public String getCustomerExpectedUoM() {
		return customerExpectedUoM;
	}

	public void setCustomerExpectedUoM(String customerExpectedUoM) {
		this.customerExpectedUoM = customerExpectedUoM;
	}

	public String getGlobalTradeIdentificatonNumber() {
		return globalTradeIdentificatonNumber;
	}

	public void setGlobalTradeIdentificatonNumber(
			String globalTradeIdentificatonNumber) {
		this.globalTradeIdentificatonNumber = globalTradeIdentificatonNumber;
	}

	public static class LineItemDtoComparator implements Comparator<LineItemDto> {

		@Override
		public int compare(LineItemDto arg0, LineItemDto arg1) {
			return staticCompare(arg0, arg1);
		}
		
		public static int staticCompare(LineItemDto arg0, LineItemDto arg1) {
			boolean arg0IsNotNull = (arg0 != null && arg0.getLineNumber() != null);
			boolean arg1IsNotNull = (arg1 != null && arg1.getLineNumber() != null);
			if (arg0IsNotNull && arg1IsNotNull) {
				return arg0.getLineNumber() - arg1.getLineNumber();
			}
			else if (!arg0IsNotNull && arg1IsNotNull) {
				return -1;
			}
			else if (arg0IsNotNull && !arg1IsNotNull) {
				return 1;
			}
			else {
				return 0;
			}
		}
    	
    }

}
