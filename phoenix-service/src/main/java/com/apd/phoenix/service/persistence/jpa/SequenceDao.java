package com.apd.phoenix.service.persistence.jpa;

import java.math.BigInteger;
import java.util.Random;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import com.apd.phoenix.service.model.Account;

/**
 * Bean to get values in sequences
 */
@Stateless
@LocalBean
public class SequenceDao extends AbstractDao<Account> {

    @PersistenceContext(unitName = "Phoenix")
    protected EntityManager entityManager;

    private Random random = new Random();

    public static enum Sequence {
        //if you need to create additional sequences, add it here, and to sequences.sql
        USSCO_STOCKCHECK("stockcheck", new Long(99999)), TRANSACTION_KEY("transactionKey", null),
        //Message service transaction control id
        TRANSACTION_CONTROL_ID("transactionControlId", new Long(99999)),
        //Message service group header control id
        GROUP_CONTROL_ID("groupControlId", new Long(999999999)),
        //Message service interchange header control  id
        INTERCHANGE_CONTROL_ID("interchangeControlId", new Long(99999)),
        //APD order number generator
        APD_PO_GENERATOR("apdPo", null),
        //Ticket Number for Issue Logs
        ISSUE_LOG_TICKET_NUM("ticketNum", null);

        private final String label;

        //If the max value is null, then there is no max value
        private Long maxValue = null;

        private Sequence(String label, Long maxValue) {
            this.label = label;
            this.maxValue = maxValue;
        }

        private String getLabel() {
            return this.label;
        }

        private Long getMaxValue() {
            return this.maxValue;
        }
    }

    public Long nextVal(Sequence sequence) {
        String key = random.nextLong() + "";
        Query insertQuery = entityManager.createNativeQuery("INSERT INTO " + sequence.getLabel()
                + " (SEQUENCE_KEY) VALUES (:value)");
        insertQuery.setParameter("value", key);
        insertQuery.executeUpdate();
        Query q = entityManager.createNativeQuery("SELECT ID FROM " + sequence.getLabel()
                + " WHERE SEQUENCE_KEY = :value");
        q.setParameter("value", key);
        Long toReturn = ((BigInteger) q.getSingleResult()).longValue();
        Query deleteQuery = entityManager.createNativeQuery("DELETE FROM " + sequence.getLabel()
                + " WHERE SEQUENCE_KEY = :value");
        deleteQuery.setParameter("value", key);
        deleteQuery.executeUpdate();
        if (sequence.getMaxValue() != null) {
            toReturn = toReturn % sequence.getMaxValue();
        }
        return toReturn;
    }
}
