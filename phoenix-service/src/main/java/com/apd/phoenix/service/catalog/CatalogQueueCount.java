package com.apd.phoenix.service.catalog;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import javax.annotation.PostConstruct;
import javax.ejb.Stateless;
import org.apache.commons.codec.binary.Base64;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.eclipse.jetty.http.HttpHeaders;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.core.utility.TenantConfigRepository;
import com.google.gson.Gson;

/**
 * This class is used to get the number of messages on the catalog processing queues. Please note 
 * that the queue count can be inaccurate, and is intended for estimates only.
 * 
 * @author RHC
 *
 */
@Stateless
public class CatalogQueueCount {

    private static final Logger LOGGER = LoggerFactory.getLogger(CatalogQueueCount.class);
    private static final String BROKER_LIST_SPLITTER = ",";

    private String[] brokerList = {};
    private String brokerCredentials = "";
    private String brokerName = "";

    @PostConstruct
    public void postConstruct() {
        Properties uploadProperties = TenantConfigRepository.getInstance().getProperties("catalog.upload.integration");
        brokerList = uploadProperties.getProperty("jmsBrokerRestUrl").split(BROKER_LIST_SPLITTER);
        brokerCredentials = uploadProperties.getProperty("jmsBrokerRestCredentials");
        brokerName = uploadProperties.getProperty("jmsBrokerName");
    }

    public static enum CatalogQueue {
        CATALOG_UNPROCESSED("queue.catalog-unprocessed"), CATALOG_PROCESSED("queue.catalog-processed"), ATOMIC_UPDATE(
                "queue.solr-atomic-updateq-aggregated");

        private String queueName;

        private CatalogQueue(String queueName) {
            this.queueName = queueName;
        }

        public String getQueueName() {
            return this.queueName;
        }
    }

    public Integer getCatalogUnprocessedCount() {
        LOGGER.info("Getting catalog unprocessed count");
        return this.getQueueCount(CatalogQueue.CATALOG_UNPROCESSED);
    }

    public Integer getCatalogProcessedCount() {
        LOGGER.info("Getting catalog processed count");
        return this.getQueueCount(CatalogQueue.CATALOG_PROCESSED);
    }

    public Integer getAtomicUpdateCount() {
        LOGGER.info("Getting aggregated atomic update count");
        return this.getQueueCount(CatalogQueue.ATOMIC_UPDATE);
    }

    private Integer getQueueCount(CatalogQueue queue) {
        Integer toReturn = null;
        for (String brokerRestUrl : brokerList) {
            if (toReturn == null) {
                StringBuilder requestUrl = new StringBuilder();
                requestUrl.append(brokerRestUrl);
                requestUrl.append("/api/jolokia/read/org.apache.activemq:type=Broker,brokerName=");
                requestUrl.append(brokerName);
                requestUrl.append(",destinationType=Queue,destinationName=");
                requestUrl.append(queue.getQueueName());
                requestUrl.append("/QueueSize");
                String response = this.sendQueueRequest(requestUrl.toString());
                toReturn = this.parseResponse(response);
            }
        }
        if (toReturn == null) {
            LOGGER.warn("Could not get queue count");
        }
        else {
            LOGGER.info("Queue count: " + toReturn);
        }
        return toReturn;
    }

    private String sendQueueRequest(String requestUrl) {
    	String toReturn = null;
		HttpClient httpClient = new DefaultHttpClient();

		HttpGet httpGet = new HttpGet(requestUrl);

		String auth = brokerCredentials;
		byte[] encodedAuth = Base64.encodeBase64(auth.getBytes(Charset.forName("ISO-8859-1")));
		String authHeader = "Basic " + new String(encodedAuth);
		httpGet.setHeader(HttpHeaders.AUTHORIZATION, authHeader);
    	
    	try {
			HttpResponse response = httpClient.execute(httpGet);
			try (BufferedReader streamReader = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), "UTF-8"));) {

				StringBuilder builder = new StringBuilder();
				String nextLine;
				while ((nextLine = streamReader.readLine()) != null) {
					builder.append(nextLine);
				}
				toReturn = builder.toString();
			}
            //gets the response InputStream
		    EntityUtils.consume(response.getEntity());
		} catch (Exception e) {
			//this warning is only info-level since the broker could just be shut down, 
			//with the other broker being the master node
    		LOGGER.info("Exception when sending queue HTTP request, turn on debug for stack trace");
    		if (LOGGER.isDebugEnabled()) {
    			LOGGER.debug("Stack trace", e);
    		}
		}
		
		return toReturn;
	}

    private Integer parseResponse(String response) {
        try {
            Gson gson = new Gson();
            Map<String, Object> map = new HashMap<String, Object>();
            map = gson.fromJson(response, map.getClass());
            return ((Double) map.get("value")).intValue();
        }
        catch (Exception e) {
            LOGGER.info("Exception when parsing queue response, turn on debug for stack trace");
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("Stack trace", e);
            }
            return null;
        }
    }
}
