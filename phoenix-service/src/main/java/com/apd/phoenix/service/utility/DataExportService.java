package com.apd.phoenix.service.utility;

import java.io.File;
import java.util.List;

public interface DataExportService {

    /**
     * This method takes an Array of columns and a List of Arrays for the rows, and returns a file of the results.
     * 
     * @param columns
     * @param results
     * @return
     */
    public File getFile(String[] columns, List<Object[]> results);
}
