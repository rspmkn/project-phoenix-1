package com.apd.phoenix.service.message.impl;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import javax.annotation.Resource;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.jms.Connection;
import javax.jms.JMSException;
import javax.jms.MapMessage;
import javax.jms.MessageProducer;
import javax.jms.ObjectMessage;
import javax.jms.Queue;
import javax.jms.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.message.api.Message;
import com.apd.phoenix.service.message.api.MessageSender;
import com.apd.phoenix.service.model.OrderLog.EventType;
import com.apd.phoenix.service.model.dto.CatalogXItemDto;
import com.apd.phoenix.service.model.dto.CustomerCreditInvoiceDto;
import com.apd.phoenix.service.model.dto.CustomerInvoiceDto;
import com.apd.phoenix.service.model.dto.POAcknowledgementDto;
import com.apd.phoenix.service.model.dto.PurchaseOrderDto;
import com.apd.phoenix.service.model.dto.ShipmentDto;
import com.apd.phoenix.service.model.factory.DtoFactory;
import com.apd.phoenix.service.persistence.jpa.SequenceDao.Sequence;
import com.apd.phoenix.service.persistence.multitenancy.impl.CurrentTenantIdentifierResolverImpl;
import javax.annotation.PostConstruct;

@Stateless
@LocalBean
public class EDIMessageSender extends MessageSender {

    public static final String EDI_OUTBOUND_997_RESEND = "edi-outbound-997-resend";

    public static final String EDI_OUTBOUND_850 = "edi-outbound-850";

    public static final String EDI_OUTBOUND_855 = "edi-outbound-855";

    public static final String EDI_OUTBOUND_810 = "edi-outbound-810";

    public static final String EDI_OUTBOUND_856 = "edi-outbound-856";

    private static final Logger LOGGER = LoggerFactory.getLogger(EDIMessageSender.class);

    @Resource(mappedName = "java:/activemq/edi-outbound-832")
    private Queue outboundEdi832Queue;

    @Resource(mappedName = "java:/activemq/" + EDI_OUTBOUND_850)
    private Queue outboundEDIPurchaseOrderQueue;

    @Resource(mappedName = "java:/activemq/edi-outbound-850-raw")
    private Queue outboundEDIPurchaseOrderQueueResend;

    @Resource(mappedName = "java:/activemq/" + EDI_OUTBOUND_856)
    private Queue outboundEDIShipNoticeQueue;

    @Resource(mappedName = "java:/activemq/" + EDI_OUTBOUND_997_RESEND)
    private Queue outboundEDIFunctionalAckQueueResend;

    @Resource(mappedName = "java:/activemq/" + EDI_OUTBOUND_810)
    private Queue outboundEDIInvoiceQueue;

    @Resource(mappedName = "java:/activemq/edi-outbound-810C")
    private Queue outboundEDICreditInvoiceQueue;

    @Resource(mappedName = "java:/activemq/" + EDI_OUTBOUND_855)
    private Queue outboundEDIOrderAckQueue;

    @PostConstruct
    public void setupQueues() {
        this.outboundFunctionalAckQueueResend = outboundEDIFunctionalAckQueueResend;
        this.outboundInvoiceQueue = outboundEDIInvoiceQueue;
        this.outboundOrderAckQueue = outboundEDIOrderAckQueue;
        this.outboundPurchaseOrderQueue = outboundEDIPurchaseOrderQueue;
        this.outboundPurchaseOrderQueueResend = outboundEDIPurchaseOrderQueueResend;
        this.outboundShipNoticeQueue = outboundEDIShipNoticeQueue;
        this.outboundCreditInvoiceQueue = outboundEDICreditInvoiceQueue;
    }

    @Override
    public boolean sendMessage(Message message, EventType eventType) {
        LOGGER.info("Sending EDI Message");
        Queue outboundQueue = null;
        if (EventType.PURCHASE_ORDER_SENT == eventType) {
            outboundQueue = outboundPurchaseOrderQueueResend;
        }
        if (EventType.FUNCTIONAL_ACKNOWLEDGEMENT == eventType) {
            outboundQueue = outboundFunctionalAckQueueResend;
        }
        if (outboundQueue == null) {
            return false;
        }

        Connection connection = null;
        Session msession;
        MessageProducer messageProducer;

        if (message.getContent().markSupported()) {
            try {
                LOGGER.info("Reseting input stream since mark supported");
                message.getContent().reset();
            }
            catch (IOException e) {
                LOGGER.error(e.toString());
            }
        }
        else {
            LOGGER.debug("retrieving message from storage since mark is unsupported");
            message = messageUtils.retrieveMessage(message.getMetadata());
        }

        byte fileContent[] = new byte[(int) message.getMetadata().getContentLength()];
        try {
            message.getContent().read(fileContent);
        }
        catch (IOException e) {
            LOGGER.error(e.toString());
        }
        String content = new String(fileContent);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Message content: {}", content);
        }
        try {

            connection = connectionFactory.createConnection();

            msession = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            messageProducer = msession.createProducer(outboundQueue);
            MapMessage mapMessage = msession.createMapMessage();
            mapMessage.setString("senderId", "APD");
            mapMessage.setString("partnerId", "USSCO");
            mapMessage.setString("rawMessage", content);
            mapMessage.setStringProperty("partnerId", "USSCO");
            mapMessage.setStringProperty("interchangeId", sequenceDao.nextVal(Sequence.INTERCHANGE_CONTROL_ID) + "");
            mapMessage.setStringProperty("groupId", sequenceDao.nextVal(Sequence.GROUP_CONTROL_ID) + "");
            mapMessage.setStringProperty("transactionId", sequenceDao.nextVal(Sequence.TRANSACTION_CONTROL_ID) + "");
            mapMessage.setLongProperty("tenantId", CurrentTenantIdentifierResolverImpl.getCurrentTenant());
            messageProducer.send(mapMessage);
        }
        catch (Exception e) {
            LOGGER.error(e.toString());
        }

        try {
            if (connection != null) {
                connection.close();
            }
        }
        catch (JMSException e) {
            LOGGER.error(e.toString());
        }
        return true;
    }

    @Override
    public Message sendCreditInvoice(CustomerCreditInvoiceDto customerCreditInvoiceDto, Boolean resend) {
        LOGGER.info("Sending Credit Invoice via EDI");
        //Credit invoices are handled the same way as standard invoices but with negative quantities
        CustomerInvoiceDto invoiceDto = DtoFactory.createCustomerInvoiceDto(customerCreditInvoiceDto);
        return sendInvoice(invoiceDto, resend);
    }

    //ArrayList specified because it must be a serializable list
    public boolean sendCatalogUpdate(ArrayList<CatalogXItemDto> catalogXItemDtos, String partnerId) {
        LOGGER.info("Sending EDI Catalog Update");
        try {
            Connection connection = connectionFactory.createConnection();
            try {
                Session msession = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
                msession = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
                MessageProducer messageProducer = msession.createProducer(outboundEdi832Queue);

                ObjectMessage objectMessage = msession.createObjectMessage(catalogXItemDtos);
                objectMessage.setStringProperty("partnerId", partnerId);
                objectMessage.setLongProperty("tenantId", CurrentTenantIdentifierResolverImpl.getCurrentTenant());
                messageProducer.send(objectMessage);
            }
            catch (Exception e) {
                LOGGER.error("Error while connecting for EDI catalog update", e.toString());
            }
            if (connection != null) {
                connection.close();
            }
        }
        catch (Exception e) {
            LOGGER.error("Error while creating connection for EDI catalog update", e.toString());
        }
        return true;
    }

    private ObjectMessage createX12JMSMessage(Session session, Serializable ediMessage, String partnerId) {
        return createX12JMSMessage(session, ediMessage, partnerId, Boolean.FALSE);
    }

    private ObjectMessage createX12JMSMessage(Session session, Serializable ediMessage, String partnerId, Boolean resend) {
        ObjectMessage objectMessage = null;
        try {
            objectMessage = session.createObjectMessage(ediMessage);
            objectMessage.setStringProperty("partnerId", partnerId);
            objectMessage.setStringProperty("interchangeId", sequenceDao.nextVal(Sequence.INTERCHANGE_CONTROL_ID) + "");
            objectMessage.setStringProperty("groupId", sequenceDao.nextVal(Sequence.GROUP_CONTROL_ID) + "");
            objectMessage.setStringProperty("transactionId", sequenceDao.nextVal(Sequence.TRANSACTION_CONTROL_ID) + "");
            objectMessage.setBooleanProperty("resend", resend);
            objectMessage.setLongProperty("tenantId", CurrentTenantIdentifierResolverImpl.getCurrentTenant());
        }
        catch (JMSException e) {
            LOGGER.error(e.toString());
        }
        return objectMessage;
    }

    @Override
    public javax.jms.Message createPurchaseOrderMessage(Session session, PurchaseOrderDto purchaseOrderDto,
            Boolean resend) {
        return createX12JMSMessage(session, purchaseOrderDto, purchaseOrderDto.getPartnerId(), resend);
    }

    @Override
    public javax.jms.Message createPOAcknowledgementMessage(Session session, POAcknowledgementDto poAcknowledgmentDto,
            Boolean resend) {
        return createX12JMSMessage(session, poAcknowledgmentDto, poAcknowledgmentDto.getPartnerId(), resend);
    }

    @Override
    public javax.jms.Message createShipmentNoticeMessage(Session session, ShipmentDto shipmentDto, Boolean resend) {
        return createX12JMSMessage(session, shipmentDto, shipmentDto.getCustomerOrderDto().getPartnerId(), resend);
    }

    @Override
    public javax.jms.Message createInvoiceMessage(Session session, CustomerInvoiceDto invoiceDto, Boolean resend) {
        ObjectMessage objectMessage = null;
        try {
            objectMessage = session.createObjectMessage(invoiceDto);
            objectMessage.setStringProperty("ediVersion", invoiceDto.getEdiVersion());
            objectMessage.setStringProperty("interchangeId", sequenceDao.nextVal(Sequence.INTERCHANGE_CONTROL_ID) + "");
            objectMessage.setStringProperty("groupId", sequenceDao.nextVal(Sequence.GROUP_CONTROL_ID) + "");
            objectMessage.setStringProperty("transactionId", sequenceDao.nextVal(Sequence.TRANSACTION_CONTROL_ID) + "");
            objectMessage.setStringProperty("receiverCode", invoiceDto.getReceiverCode());
            objectMessage.setStringProperty("partnerId", invoiceDto.getPartnerId());
            objectMessage.setBooleanProperty("resend", resend);
            objectMessage.setLongProperty("tenantId", CurrentTenantIdentifierResolverImpl.getCurrentTenant());
        }
        catch (JMSException e) {
            LOGGER.error(e.toString());
        }
        return objectMessage;
    }

}
