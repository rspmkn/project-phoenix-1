package com.apd.phoenix.service.model.dto;

import java.io.Serializable;

public class PhoneNumberTypeDto implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 8796517484752774934L;

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
