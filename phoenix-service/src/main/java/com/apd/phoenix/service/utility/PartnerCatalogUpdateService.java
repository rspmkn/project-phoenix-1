/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.service.utility;

import com.apd.phoenix.service.business.CatalogXItemBp;
import com.apd.phoenix.service.message.impl.EDIMessageSender;
import com.apd.phoenix.service.model.dto.CatalogXItemDto;
import java.util.ArrayList;
import java.util.logging.Logger;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author nreidelb
 */
@Named
@LocalBean
@Stateless
public class PartnerCatalogUpdateService {

    private static final Logger LOGGER = Logger.getLogger(PartnerCatalogUpdateService.class.getCanonicalName());

    @Inject
    CatalogXItemBp catalogXItemBp;

    @Inject
    EDIMessageSender edIMessageSender;

    public void sendCatalogUpdate() {
        LOGGER.info("Starting Edi 832 catalog update process.");
        // Specified ArrayList because it must be a serializable list to be put on a JMS queue
        ArrayList<CatalogXItemDto> catalogXItemDtos = catalogXItemBp.getUSPSChangedItems();
        LOGGER.info("Completed Edi 832 catalog update process.");
        if (!catalogXItemDtos.isEmpty()) {
            edIMessageSender.sendCatalogUpdate(catalogXItemDtos, "usps");
        }
    }
}
