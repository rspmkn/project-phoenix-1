/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.service.email.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import com.apd.phoenix.service.email.api.Attachment;
import com.apd.phoenix.service.email.api.EmailTemplate;
import com.apd.phoenix.service.email.api.NoAttachmentContentException;
import com.apd.phoenix.service.model.dto.error.EdiDocumentErrorsDto;
import com.apd.phoenix.service.model.dto.error.EdiErrorDto;
import com.apd.phoenix.service.model.dto.error.ErrorDataDto;
import com.apd.phoenix.service.model.dto.error.RelevantNumberDto;
import freemarker.template.TemplateException;

/**
 *
 * @author nreidelb
 */
public class EdiErrorEmailTemplate extends EmailTemplate<EdiDocumentErrorsDto> {

    private static final String TEMPLATE = "edi.error";

    @Override
    public String createBody(EdiDocumentErrorsDto ediDocumentErrorsDto) throws IOException, TemplateException {
        Map<String, Object> params = new HashMap<>();
        //Sets any null fields to avoid npes in the email creation process
        if(ediDocumentErrorsDto.getRelevantNumberDto()==null){
            ediDocumentErrorsDto.setRelevantNumberDto(new ArrayList<RelevantNumberDto>());
        }
        if(ediDocumentErrorsDto.getEdiErrorDtos()==null){
            ediDocumentErrorsDto.setEdiErrorDtos(new ArrayList<EdiErrorDto>());
        }
        else{
            for(EdiErrorDto errorDto:ediDocumentErrorsDto.getEdiErrorDtos()){
                if(errorDto.getErrorDataDtos()==null){
                    errorDto.setErrorDataDtos(new ArrayList<ErrorDataDto>());
                }
                else{
                    for(ErrorDataDto errorData:errorDto.getErrorDataDtos()){
                        if(errorData.getErrorSpecifications()==null){
                            errorData.setErrorSpecifications(new ArrayList<String>());
                        }
                        if(errorData.getRecommendedActions()==null){
                            errorData.setRecommendedActions(new ArrayList<String>());
                        }
                    }
                }
            }
        }
        params.put("dto", ediDocumentErrorsDto);
        return this.create(params);
    }

    @Override
    public String getTemplate() {
        return EdiErrorEmailTemplate.TEMPLATE;
    }

    @Override
    public Attachment createAttachment(EdiDocumentErrorsDto ediDocumentErrorsDto) throws NoAttachmentContentException {
        return null;
    }

}
