/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.apd.phoenix.service.model;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToOne;
import org.hibernate.annotations.Index;

@Entity
@DiscriminatorValue(value = "CUSTOMER_INVOICE")
public class CustomerInvoice extends Invoice {

    @Index(name = "INVOICE_SHIPMENT_IDX")
    @OneToOne(fetch = FetchType.LAZY)
    private Shipment shipment;

    public Shipment getShipment() {
        return shipment;
    }

    public void setShipment(Shipment shipment) {
        this.shipment = shipment;
    }

}
