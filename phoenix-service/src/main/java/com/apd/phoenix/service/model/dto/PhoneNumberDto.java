package com.apd.phoenix.service.model.dto;

import java.io.Serializable;

public class PhoneNumberDto implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1461930588674068650L;

    private String areaCode;

    private String countryCode;

    private String exchange;

    private String lineNumber;

    private String extension;

    private String value;

    private PhoneNumberTypeDto phoneNumberTypeDto;

    public String getAreaCode() {
        return areaCode;
    }

    public void setAreaCode(String areaCode) {
        this.areaCode = areaCode;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getExchange() {
        return exchange;
    }

    public void setExchange(String exchange) {
        this.exchange = exchange;
    }

    public String getLineNumber() {
        return lineNumber;
    }

    public void setLineNumber(String lineNumber) {
        this.lineNumber = lineNumber;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public PhoneNumberTypeDto getPhoneNumberTypeDto() {
        return phoneNumberTypeDto;
    }

    public void setPhoneNumberTypeDto(PhoneNumberTypeDto phoneNumberTypeDto) {
        this.phoneNumberTypeDto = phoneNumberTypeDto;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

}
