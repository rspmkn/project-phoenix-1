package com.apd.phoenix.service.persistence.jpa;

import com.apd.phoenix.service.model.VendorInvoice;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;

/**
 * Session Bean implementation class VendorInvoiceDao
 */
@Stateless
@LocalBean
public class VendorInvoiceDao extends AbstractDao<VendorInvoice> {

}
