package com.apd.phoenix.service.email.impl;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import com.apd.phoenix.service.email.api.Attachment;
import com.apd.phoenix.service.email.api.EmailTemplate;
import com.apd.phoenix.service.model.dto.LineItemDto;
import com.apd.phoenix.service.model.dto.PurchaseOrderDto;
import com.apd.phoenix.service.model.dto.ValidationErrorDto;
import freemarker.template.TemplateException;

@Stateless
@LocalBean
public class NoUsscoAccountIdEmailTemplate extends EmailTemplate<PurchaseOrderDto> {

    private static final String TEMPLATE = "no.ussco.account";

    @Override
	public String createBody(PurchaseOrderDto purchaseOrderDto) throws IOException, TemplateException {
		Map<String, Object> params = new HashMap<>();
		
		params.put("apdPo", purchaseOrderDto.getApdPoNumber());
		
		return this.create(params);
	}

    public String getTemplate() {
        return TEMPLATE;
    }

    @Override
    public Attachment createAttachment(PurchaseOrderDto purchaseOrderDto) {
        // TODO Auto-generated method stub
        return null;
    }
}
