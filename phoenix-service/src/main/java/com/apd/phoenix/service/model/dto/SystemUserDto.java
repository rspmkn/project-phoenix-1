package com.apd.phoenix.service.model.dto;

import java.io.Serializable;

public class SystemUserDto implements Serializable {

    private static final long serialVersionUID = 4330973393804697278L;
    private String login;

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }
}
