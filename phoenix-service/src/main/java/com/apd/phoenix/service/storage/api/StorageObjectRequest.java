package com.apd.phoenix.service.storage.api;

import java.util.HashMap;
import java.util.Map;

public class StorageObjectRequest {

    private String path;
    private String contentType;
    private boolean isContentUrl = false;
    private Map<String, String> attributes;

    public StorageObjectRequest() {
		attributes = new HashMap<>();
	}

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public Map<String, String> getAttributes() {
        return attributes;
    }

    public void setAttributes(Map<String, String> attributes) {
        this.attributes = attributes;
    }

    public boolean isContentUrl() {
        return isContentUrl;
    }

    public void setContentUrl(boolean isContentUrl) {
        this.isContentUrl = isContentUrl;
    }

}
