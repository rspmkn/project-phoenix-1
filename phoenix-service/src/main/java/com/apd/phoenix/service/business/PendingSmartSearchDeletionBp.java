package com.apd.phoenix.service.business;

import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import com.apd.phoenix.service.model.Catalog;
import com.apd.phoenix.service.model.Item;
import com.apd.phoenix.service.model.PendingSmartSearchDeletion;
import com.apd.phoenix.service.persistence.jpa.PendingSmartSearchDeletionDao;

@Stateless
@LocalBean
public class PendingSmartSearchDeletionBp extends AbstractBp<PendingSmartSearchDeletion> {

    /**
     * This method is injected with the correct Dao for initialization
     * 
     * @param dao
     */
    @Inject
    public void initDao(PendingSmartSearchDeletionDao dao) {
        this.dao = dao;
    }

    public PendingSmartSearchDeletion create(Catalog catalog, Item item) {
        if (catalog != null && item != null) {
            PendingSmartSearchDeletion newEntry = new PendingSmartSearchDeletion();
            newEntry.setCatalog(catalog);
            newEntry.setItem(item);
            return this.update(newEntry);
        }
        return null;
    }

    public List<PendingSmartSearchDeletion> batchForJMS(Catalog input, int batch) {
        return ((PendingSmartSearchDeletionDao) this.dao).batchForJMS(input, batch);
    }

    public void deletePendingForCatalog(Catalog input) {
        ((PendingSmartSearchDeletionDao) this.dao).deletePendingForCatalog(input);
    }
}
