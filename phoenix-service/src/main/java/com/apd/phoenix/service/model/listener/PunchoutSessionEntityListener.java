package com.apd.phoenix.service.model.listener;

import java.util.Date;
import javax.persistence.PrePersist;
import com.apd.phoenix.service.model.PunchoutSession;

public class PunchoutSessionEntityListener {

    @PrePersist
    public void prePersistEventListener(PunchoutSession entity) {
        entity.setInitTime(new Date());
    }
}
