package com.apd.phoenix.service.business;

import static com.apd.phoenix.service.model.CredentialPropertyType.CredentialPropertyTypeEnum.CHARGE_SALES_TAX;
import static com.apd.phoenix.service.model.CredentialPropertyType.CredentialPropertyTypeEnum.TAX_EXEMPT_STATES;
import static com.apd.phoenix.service.model.CredentialPropertyType.CredentialPropertyTypeEnum.TAX_EXEMPT_ZIPCODES;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import org.apache.commons.lang.StringUtils;
import com.apd.phoenix.service.model.Account;
import com.apd.phoenix.service.model.Address;
import com.apd.phoenix.service.model.Bulletin;
import com.apd.phoenix.service.model.Credential;
import com.apd.phoenix.service.model.CredentialXCredentialPropertyType;
import com.apd.phoenix.service.model.CxmlCredential;
import com.apd.phoenix.service.model.HierarchyNode;
import com.apd.phoenix.service.model.IpAddress;
import com.apd.phoenix.service.model.NotificationLimit;
import com.apd.phoenix.service.model.NotificationProperties;
import com.apd.phoenix.service.model.OrderLog;
import com.apd.phoenix.service.model.Permission;
import com.apd.phoenix.service.model.PoNumber;
import com.apd.phoenix.service.model.ProcessConfiguration;
import com.apd.phoenix.service.model.ProcessVariable;
import com.apd.phoenix.service.model.Role;
import com.apd.phoenix.service.model.TaxRate;
import com.apd.phoenix.service.model.dto.CredentialDto;
import com.apd.phoenix.service.persistence.jpa.CredentialDao;
import com.apd.phoenix.service.utility.CredentialPropertyUtils;

/**
 * This class provides business process methods for Accounts.
 * 
 * @author RHC
 * 
 */
@Stateless
@LocalBean
public class CredentialBp extends AbstractBp<Credential> {

    private static final String FALSE = "false";

    private static final String NO = "no";

    @Inject
    private CxmlCredentialBp cxmlCredBp;

    @Inject
    private TaxRateBp taxRateBp;

    /**
     * This method is injected with the correct Dao and calls the initAbstract method in the superclass.
     * 
     * @param dao
     */
    @Inject
    public void init(CredentialDao dao) {
        super.initAbstract(dao);
    }

    /**
     * This method is injected with the correct Dao for initialization
     * 
     * @param dao
     */
    @Inject
    public void initDao(CredentialDao dao) {
        this.dao = dao;
    }

    /**
     * This method is used to add the Credential "toAdd" to the Account tree
     * containing the account "treeMember". If an order has been placed with the
     * credential, it will not be moved.
     * 
     * @param toAdd
     *            - the Credential to be added
     * @param treeMember
     *            - a member of the Account tree that toAdd will be added to
     */
    public void addToAccountTree(Credential toAdd, Account treeMember) {
        boolean hasOrders = ((CredentialDao) dao).hasOrders(toAdd);
        if (!hasOrders) {
            if (treeMember.getRootAccount() != null) {
                treeMember = treeMember.getRootAccount();
            }
            treeMember.getCredentials().add(toAdd);
            toAdd.setRootAccount(treeMember);
            dao.update(toAdd);
        }
    }

    /**
     * This method is used to determine whether a credential can be used.
     * Returns true if the credential is active, has a catalog, and either uses
     * a procurement platform or has a cashout page; returns false otherwise.
     * 
     * @param credential
     *            - the credential to be checked
     * @return whether the credential can be used to place an order
     */
    @SuppressWarnings("static-method")
    public boolean isUsable(Credential credential) {
        // TODO: rework
        // return credential.getActive() && credential.getCatalog() != null &&
        // (credential.getUseProcurement() || credential.getCashoutPage() !=
        // null);
        return true;
    }

    /**
     * This method is used to get the existing addresses for a credential, based
     * on the Account tree the Credential is linked to.
     * 
     * @param c
     * @return
     */
    @SuppressWarnings("static-method")
    public Set<Address> existingAddresses(Credential c) {
        return new HashSet<Address>(c.getRootAccount().getAddresses());
    }

    @Override
    public Credential cloneEntity(Credential toClone) {
        Credential toReturn = super.cloneEntity(toClone);
        toClone = this.eagerLoad(toClone);
        //Clears out the relationships that shouldn't be copied
        toReturn.setRootAccount(null);
        toReturn.setNotificationProperties(new HashSet<NotificationProperties>());
        toReturn.setBulletins(new HashSet<Bulletin>());
        toReturn.getBulletins().addAll(toClone.getBulletins());
        toReturn.setBlanketPos(new HashSet<PoNumber>());
        toReturn.getBlanketPos().addAll(toClone.getBlanketPos());
        toReturn.setIps(new HashSet<IpAddress>());
        for (IpAddress ip : toClone.getIps()) {
            IpAddress newIp = new IpAddress();
            newIp.setValue(ip.getValue());
            toReturn.getIps().add(newIp);
        }
        toReturn.setPaymentInformation(null);
        //copies the process configurations
        Set<ProcessConfiguration> newConfigs = new HashSet<ProcessConfiguration>();
        for (ProcessConfiguration config : toClone.getProcessConfigurations()) {
            ProcessConfiguration newConfig = new ProcessConfiguration();
            newConfig.setCredential(toReturn);
            newConfig.setProcess(config.getProcess());
            newConfig.setProcessVariables(new HashSet<ProcessVariable>());
            for (ProcessVariable variable : config.getProcessVariables()) {
                ProcessVariable newVariable = new ProcessVariable();
                newVariable.setName(variable.getName());
                newVariable.setValue(variable.getValue());
                newConfig.getProcessVariables().add(newVariable);
            }
            newConfigs.add(newConfig);
        }
        toReturn.setProcessConfigurations(newConfigs);
        //Persists copies of the properties
        Set<CredentialXCredentialPropertyType> newProperties = new HashSet<CredentialXCredentialPropertyType>();
        for (CredentialXCredentialPropertyType c : toClone.getProperties()) {
            CredentialXCredentialPropertyType newProperty = new CredentialXCredentialPropertyType();
            newProperty.setValue(c.getValue());
            newProperty.setType(c.getType());
            newProperties.add(newProperty);
        }
        toReturn.setProperties(newProperties);
        toReturn.setCommunicationMethod(toClone.getCommunicationMethod());
        toReturn.setOutgoingToCredentials(new HashSet<CxmlCredential>());
        for (CxmlCredential cxmlCred : toClone.getOutgoingToCredentials()) {
            toReturn.getOutgoingToCredentials().add(cxmlCredBp.cloneEntity(cxmlCred));
        }
        toReturn.setOutgoingFromCredentials(new HashSet<CxmlCredential>());
        for (CxmlCredential cxmlCred : toClone.getOutgoingFromCredentials()) {
            toReturn.getOutgoingFromCredentials().add(cxmlCredBp.cloneEntity(cxmlCred));
        }
        toReturn.setOutgoingSenderCredentials(new HashSet<CxmlCredential>());
        for (CxmlCredential cxmlCred : toClone.getOutgoingSenderCredentials()) {
            toReturn.getOutgoingSenderCredentials().add(cxmlCredBp.cloneEntity(cxmlCred));
        }
        toReturn.setOutgoingSenderSharedSecret(toClone.getOutgoingSenderSharedSecret());

        toReturn.setCustomerOutoingToCred(new HashSet<CxmlCredential>());
        for (CxmlCredential cxmlCred : toClone.getCustomerOutoingToCred()) {
            toReturn.getCustomerOutoingToCred().add(cxmlCredBp.cloneEntity(cxmlCred));
        }
        toReturn.setCustomerOutoingFromCred(new HashSet<CxmlCredential>());
        for (CxmlCredential cxmlCred : toClone.getCustomerOutoingFromCred()) {
            toReturn.getCustomerOutoingFromCred().add(cxmlCredBp.cloneEntity(cxmlCred));
        }
        toReturn.setCustomerOutoingSenderCred(new HashSet<CxmlCredential>());
        for (CxmlCredential cxmlCred : toClone.getCustomerOutoingSenderCred()) {
            toReturn.getCustomerOutoingSenderCred().add(cxmlCredBp.cloneEntity(cxmlCred));
        }
        toReturn.setCustomerOutgoingSharedSecret(toClone.getCustomerOutgoingSharedSecret());

        toReturn.setNotificationLimits(new HashSet<NotificationLimit>());
        for (NotificationLimit clonedLimit : toClone.getNotificationLimits()) {
            NotificationLimit newLimit = new NotificationLimit();
            newLimit.setEventType(clonedLimit.getEventType());
            newLimit.setLimitType(clonedLimit.getLimitType());
            toReturn.getNotificationLimits().add(newLimit);
        }
        toReturn.setVendorPunchoutUrl(toClone.getVendorPunchoutUrl());
        toReturn.setVendorOrderUrl(toClone.getVendorOrderUrl());
        toReturn.setVendorName(toClone.getVendorName());

        return toReturn;
    }

    /**
     * Takes an address and a credential, and returns true if the IP address can be used with the credential.
     * 
     * @param address
     * @param cred
     * @return
     */
    @SuppressWarnings("static-method")
    public boolean isValidIp(String address, Credential cred) {
        if (cred.getIps().size() == 0) {
            return true;
        }
        for (IpAddress toTest : cred.getIps()) {
            if (toTest.getValue().equals(address)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Takes an address and a credential, and returns true if the IP address can be used with the credential.
     * 
     * @param address
     * @param cred
     * @return
     */
    public boolean isValidIp(IpAddress address, Credential cred) {
        return isValidIp(address.getValue(), cred);
    }

    public List<HierarchyNode> getMenuItems(Credential credential) {
        return ((CredentialDao) dao).getMenuItems(credential);
    }

    @Override
    public Credential create(Credential credential) {
        Set<PoNumber> poSet = credential.getBlanketPos();
        credential.setBlanketPos(new HashSet<PoNumber>());
        Set<Bulletin> bulletinSet = credential.getBulletins();
        credential.setBulletins(new HashSet<Bulletin>());
        Set<Permission> permissionSet = credential.getPermissions();
        credential.setPermissions(new HashSet<Permission>());
        Set<Role> roleSet = credential.getRoles();
        credential.setRoles(new HashSet<Role>());
        credential = super.create(credential);
        credential.setBlanketPos(poSet);
        credential.setBulletins(bulletinSet);
        credential.setPermissions(permissionSet);
        credential.setRoles(roleSet);
        credential = super.update(credential);
        return credential;
    }

    public Credential loadCredentialForMaintenance(Credential credential) {
        return ((CredentialDao) dao).getCredentialForMaintenance(credential.getId());
    }

    public Credential retrieveCredential(CredentialDto credentialDto) {
        Credential search = new Credential();
        search.setName(credentialDto.getName());
        List<Credential> results = this.dao.searchByExactExample(search, 0, 0);
        if (results == null || results.isEmpty()) {
            return null;
        }
        else {
            return results.get(0);
        }
    }

    public String getCredetialProperty(String name, Credential credential) {
        for (CredentialXCredentialPropertyType cxcpt : credential.getProperties()) {
            if (cxcpt.getType().getName().equalsIgnoreCase(name)) {
                return cxcpt.getValue();
            }
        }
        return null;
    }

    public boolean isCredentialTaxExempt(Credential credential, String zip) {
        if (StringUtils.isBlank(zip)) {
            return true;
        }
        TaxRate rate = this.taxRateBp.getTaxRate(zip, null, null, null, null);
        if (rate == null || rate.getId() == null) {
            return true;
        }
        Credential cred = this.findById(credential.getId(), Credential.class);

        return CredentialPropertyUtils.contains(TAX_EXEMPT_STATES, rate.getState(), cred, true)
                || CredentialPropertyUtils.contains(TAX_EXEMPT_ZIPCODES, zip, cred, true)
                || CredentialPropertyUtils.contains(CHARGE_SALES_TAX, NO, cred, false)
                || CredentialPropertyUtils.contains(CHARGE_SALES_TAX, FALSE, cred, false);
    }

    public Credential findCredentialByDuns(String dunsNumber) {
        return ((CredentialDao) dao).findCredentialByDuns(dunsNumber);
    }

    public List<OrderLog.EventType> getEventTypesWithNotificationLimit(Long credentialId,
            List<NotificationLimit.LimitType> limitTypesToView) {
        return ((CredentialDao) dao).getEventTypesWithNotificationLimit(credentialId, limitTypesToView);
    }

    public Credential getCredentialByName(String name) {
        return ((CredentialDao) dao).getCredentialByName(name);
    }
}
