package com.apd.phoenix.service.email.impl;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.email.api.Attachment;
import com.apd.phoenix.service.email.api.EmailTemplate;
import com.apd.phoenix.service.email.api.NoAttachmentContentException;
import com.apd.phoenix.service.model.SystemUser;
import freemarker.template.TemplateException;

public class NewUserSetupEmailTemplate extends EmailTemplate<SystemUser> {

    private static final Logger LOGGER = LoggerFactory.getLogger(MarfieldUserSetupEmailTemplate.class);

    private static final String TEMPLATE = "new.user.setup";

    @Override
	public String createBody(SystemUser user) throws IOException,
			TemplateException {
		Map<String, Object> params = new HashMap<>();
        params.put("login", user.getLogin());
        params.put("fullName", user.getPerson().getFormalName());
        return this.create(params);
	}

    @Override
    public String getTemplate() {
        return TEMPLATE;
    }

    @Override
    public Attachment createAttachment(SystemUser email) throws NoAttachmentContentException {
        // TODO Auto-generated method stub
        return null;
    }
}
