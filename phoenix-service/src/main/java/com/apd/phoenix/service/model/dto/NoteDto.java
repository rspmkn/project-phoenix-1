package com.apd.phoenix.service.model.dto;

import java.io.Serializable;

public class NoteDto implements Serializable {

    private static final long serialVersionUID = 1L;
    private SegmentNoteDto segmentNoteDto;
    private ElementNoteDto elementNoteDto;

    public SegmentNoteDto getSegmentNoteDto() {
        return segmentNoteDto;
    }

    public void setSegmentNoteDto(SegmentNoteDto segmentNoteDto) {
        this.segmentNoteDto = segmentNoteDto;
    }

    public ElementNoteDto getElementNoteDto() {
        return elementNoteDto;
    }

    public void setElementNoteDto(ElementNoteDto elementNoteDto) {
        this.elementNoteDto = elementNoteDto;
    }

}
