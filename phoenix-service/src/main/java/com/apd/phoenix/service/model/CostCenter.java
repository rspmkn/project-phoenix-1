/*
 * 
 */
package com.apd.phoenix.service.model;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Version;
import javax.xml.bind.annotation.XmlRootElement;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.envers.Audited;

/**
 * The Class CostCenter.
 */
@Entity
@XmlRootElement
@Audited
public class CostCenter implements Serializable, com.apd.phoenix.service.model.Entity {

    private static final long serialVersionUID = 6155014814050730364L;

    /** The id. */
    @Id
    private @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false, nullable = false)
    Long id = null;

    /** The description. */
    @Column
    private String description;

    /** The name. */
    @Column
    private String name;

    /** The cost center number. */
    @Column(name = "ccNumber")
    private String number;

    /** The period. */
    @ManyToOne(fetch = FetchType.EAGER)
    @Fetch(FetchMode.JOIN)
    private PeriodType period;

    /** The spending limit. */
    @Column
    private BigDecimal spendingLimit;

    /** The total spent. */
    @Column
    private BigDecimal totalSpent;

    /** The version. */
    @Version
    private @Column(name = "version", nullable = false)
    int version = 0;

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        if (id != null) {
            return id.equals(((CostCenter) that).id);
        }
        return super.equals(that);
    }

    /**
     * Gets the description.
     * 
     * @return the description
     */
    public String getDescription() {
        return this.description;
    }

    /**
     * Gets the id.
     * 
     * @return the id
     */
    public Long getId() {
        return this.id;
    }

    /**
     * Gets the name.
     * 
     * @return the name
     */
    public String getName() {
        return this.name;
    }

    /**
     * Gets the cost center number.
     * 
     * @return the cost center number
     */
    public String getNumber() {
        return this.number;
    }

    /**
     * Gets the period.
     * 
     * @return the period
     */
    public PeriodType getPeriod() {
        return this.period;
    }

    /**
     * Gets the spending limit.
     * 
     * @return the spending limit
     */
    public BigDecimal getSpendingLimit() {
        return this.spendingLimit;
    }

    /**
     * Gets the total spent.
     * 
     * @return the total spent
     */
    public BigDecimal getTotalSpent() {
        return this.totalSpent;
    }

    /**
     * Gets the version.
     * 
     * @return the version
     */
    public int getVersion() {
        return this.version;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        if (id != null) {
            return id.hashCode();
        }
        return super.hashCode();
    }

    /**
     * Sets the description.
     * 
     * @param description
     *            the new description
     */
    public void setDescription(final String description) {
        this.description = description;
    }

    /**
     * Sets the id.
     * 
     * @param id
     *            the new id
     */
    public void setId(final Long id) {
        this.id = id;
    }

    /**
     * Sets the name.
     * 
     * @param name
     *            the new name
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * Sets the number.
     * 
     * @param number
     *            the new number
     */
    public void setNumber(final String number) {
        this.number = number;
    }

    /**
     * Sets the period.
     * 
     * @param period
     *            the new period
     */
    public void setPeriod(final PeriodType period) {
        this.period = period;
    }

    /**
     * Sets the spending limit.
     * 
     * @param spendingLimit
     *            the new spending limit
     */
    public void setSpendingLimit(final BigDecimal spendingLimit) {
        this.spendingLimit = spendingLimit;
    }

    /**
     * Sets the total spent.
     * 
     * @param totalSpent
     *            the new total spent
     */
    public void setTotalSpent(final BigDecimal totalSpent) {
        this.totalSpent = totalSpent;
    }

    /**
     * Sets the version.
     * 
     * @param version
     *            the new version
     */
    public void setVersion(final int version) {
        this.version = version;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        String result = "";
        if (number != null && !number.trim().isEmpty())
            result += number;
        if (name != null && !name.trim().isEmpty())
            result += " " + name;
        if (description != null && !description.trim().isEmpty())
            result += " " + description;
        if (totalSpent != null)
            result += " " + totalSpent.toPlainString();
        if (spendingLimit != null)
            result += " " + spendingLimit.toPlainString();
        return result;
    }
}