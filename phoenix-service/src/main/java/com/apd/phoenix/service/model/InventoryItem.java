package com.apd.phoenix.service.model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class InventoryItem implements Serializable, com.apd.phoenix.service.model.Entity {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    /** The id. */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id = null;

    @Column
    private Long onHandQty;

    @Column
    private Long lot;

    @Column
    private Long UPC;

    @Column
    private java.util.Date expiry;

    @Column
    String vendorSku;

    @Column
    String vendorName;

    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    private Item item;

    @OneToMany(mappedBy = "inventoryItem", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<InventoryTransaction> inventorytransactions = new HashSet<InventoryTransaction>();

    @OneToMany(mappedBy = "inventoryItem", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<SerialNumber> serialNumbers = new HashSet<SerialNumber>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getOnHandQty() {
        return onHandQty;
    }

    public void setOnHandQty(Long onHandQty) {
        this.onHandQty = onHandQty;
    }

    public String getVendorName() {
        return vendorName;
    }

    public void setVendorName(String vendorName) {
        this.vendorName = vendorName;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        InventoryItem other = (InventoryItem) obj;
        if (id == null) {
            if (other.id != null)
                return false;
        }
        else if (!id.equals(other.id))
            return false;
        return true;
    }

    public Set<InventoryTransaction> getInventorytransactions() {
        return inventorytransactions;
    }

    public void setInventorytransactions(Set<InventoryTransaction> inventorytransactions) {
        this.inventorytransactions = inventorytransactions;
    }

    public Long getLot() {
        return lot;
    }

    public void setLot(Long lot) {
        this.lot = lot;
    }

    public Long getUPC() {
        return UPC;
    }

    public void setUPC(Long uPC) {
        UPC = uPC;
    }

    public java.util.Date getExpiry() {
        return expiry;
    }

    public void setExpiry(java.util.Date expiry) {
        this.expiry = expiry;
    }

    public String getVendorSku() {
        return vendorSku;
    }

    public void setVendorSku(String vendorSku) {
        this.vendorSku = vendorSku;
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    public Set<SerialNumber> getSerialNumbers() {
        return serialNumbers;
    }

    public void setSerialNumbers(Set<SerialNumber> serialNumbers) {
        this.serialNumbers = serialNumbers;
    }

}
