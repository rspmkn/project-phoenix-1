package com.apd.phoenix.service.manifest;

public class Manifest {

    private boolean valid;
    private String content;

    public boolean isValid() {
        return valid;
    }

    public void setValid(boolean valid) {
        this.valid = valid;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

}
