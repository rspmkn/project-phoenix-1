package com.apd.phoenix.service.business;

import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import org.apache.commons.lang.StringUtils;
import com.apd.phoenix.service.model.Account;
import com.apd.phoenix.service.model.AccountXCredentialXUser;
import com.apd.phoenix.service.model.AssignedDepartment;
import com.apd.phoenix.service.model.Credential;
import com.apd.phoenix.service.model.Department;
import com.apd.phoenix.service.model.SystemUser;
import com.apd.phoenix.service.persistence.jpa.AssignedDepartmentDao;

/**
 * This class provides business process methods for AssignedDepartment.
 * 
 * @author RHC
 *
 */
@Stateless
@LocalBean
public class AssignedDepartmentBp extends AbstractBp<AssignedDepartment> {

    /**
     * This method is injected with the correct Dao for initialization
     * 
     * @param dao
     */
    @Inject
    public void initDao(AssignedDepartmentDao dao) {
        this.dao = dao;
    }

    public String getApprovalRecipients(Account account, Credential credential, SystemUser user, String department) {
        String toReturn = "";
        if (account == null || credential == null || user == null || StringUtils.isBlank(department)) {
            return toReturn;
        }
        AssignedDepartment searchDepartment = new AssignedDepartment();
        searchDepartment.setDepartment(new Department());
        searchDepartment.getDepartment().setName(department);
        searchDepartment.setAxcxu(new AccountXCredentialXUser());
        searchDepartment.getAxcxu().setAccount(new Account());
        searchDepartment.getAxcxu().getAccount().setId(account.getId());
        searchDepartment.getAxcxu().setCredential(new Credential());
        searchDepartment.getAxcxu().getCredential().setId(credential.getId());
        searchDepartment.getAxcxu().setUser(new SystemUser());
        searchDepartment.getAxcxu().getUser().setId(user.getId());
        List<AssignedDepartment> results = this.searchByExactExample(searchDepartment, 0, 0);
        for (AssignedDepartment assignedDepartment : results) {
            if (assignedDepartment != null && StringUtils.isNotBlank(assignedDepartment.getRecipientsEmails())) {
                if (toReturn.length() != 0) {
                    toReturn = toReturn.concat(",");
                }
                toReturn = toReturn.concat(assignedDepartment.getRecipientsEmails());
            }
        }
        return toReturn;
    }
}
