package com.apd.phoenix.service.persistence.jpa;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import com.apd.phoenix.service.model.HomePageCarouselDisplay;

@Stateless
@LocalBean
public class HomePageCarouselDisplayDao extends AbstractDao<HomePageCarouselDisplay> {

}
