package com.apd.phoenix.service.persistence.jpa;

import java.util.List;
import java.util.Properties;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.Query;
import com.apd.phoenix.core.utility.PropertiesLoader;
import com.apd.phoenix.service.model.Catalog;
import com.apd.phoenix.service.model.PendingSmartSearchDeletion;

/**
 * PendingSmartSearchDeletion DAO stub
 * 
 * @author RHC
 *
 */
@Stateless
@LocalBean
public class PendingSmartSearchDeletionDao extends AbstractDao<PendingSmartSearchDeletion> {

    private static final String JMS_PROPERTIES = "catalog.upload.integration";
    private static final String JMS_BATCH_SIZE = "100";

    @SuppressWarnings("unchecked")
    public List<PendingSmartSearchDeletion> batchForJMS(Catalog input, int batch) {
        Properties jmsProperties = PropertiesLoader.getAsProperties(JMS_PROPERTIES);
        int jmsBatchSize = Integer.parseInt(jmsProperties.getProperty("jmsBatchSize", JMS_BATCH_SIZE));
        String hql = "SELECT pending FROM PendingSmartSearchDeletion AS pending WHERE pending.catalog.id = :catalogId AND pending.hash = pending.catalog.itemHash";
        Query query = entityManager.createQuery(hql);
        query.setParameter("catalogId", input.getId());
        setItemsReturned(query, batch * jmsBatchSize, jmsBatchSize);
        return query.getResultList();
    }

    public void deletePendingForCatalog(Catalog input) {
        String hql = "DELETE FROM PendingSmartSearchDeletion AS pending WHERE pending.catalog.id = :catalogId AND pending.hash = pending.catalog.itemHash";
        Query query = entityManager.createQuery(hql);
        query.setParameter("catalogId", input.getId());
        query.executeUpdate();
    }
}
