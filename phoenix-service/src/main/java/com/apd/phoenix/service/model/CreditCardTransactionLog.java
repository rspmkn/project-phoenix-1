/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.service.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Index;

/**
 *
 * @author nreidelb
 */
@Entity
@DiscriminatorValue(value = "CREDITCARDTRANSACTION_LOG")
public class CreditCardTransactionLog extends OrderLog {

    @Column(name = "card_identifier")
    private String cardIdentifier;

    @Temporal(TemporalType.DATE)
    private Date cardExpriation;

    @Column(name = "name_on_card")
    private String nameOnCard;

    @Column
    private BigDecimal totalAmount;

    @Column
    private String transactionKey;

    @Column
    private String failureReason;

    @ElementCollection(targetClass = String.class)
    @CollectionTable(name = "cctranlog_faildetails", joinColumns = @JoinColumn(name = "cctranlog_id"))
    @Column(name = "failuredetails")
    List<String> failureDetails;

    @Column
    String transactionType;

    @NotNull
    @Index(name = "CCTRANLOG_SUCCESS_IDX")
    Boolean transactionSuccess = true;

    @Column
    String trackingNum;

    @Column
    BigDecimal transactionAmount;

    @Column
    BigDecimal salesTax;

    @Column
    BigDecimal freightAmount;

    @Column
    BigDecimal subtotal;

    @Temporal(TemporalType.TIMESTAMP)
    Date timestamp;

    @Column
    String apdPo;

    @Column
    String custPo;

    @NotNull
    Integer itemCount = 0;

    @ElementCollection(targetClass = String.class)
    @CollectionTable(name = "cctranlog_vendorList", joinColumns = @JoinColumn(name = "cctranlog_id"))
    @Column(name = "vendorList")
    List<String> vendorList;

    @ManyToMany(fetch = FetchType.LAZY)
    private List<LineItemXShipment> lineItemXShipments = new ArrayList<>();

    /** The return order. */
    @ManyToOne(fetch = FetchType.LAZY)
    @Index(name = "CCTRANLOG_RETURN_IDX")
    @JoinColumn(name = "creditcard_returnorder_id")
    private ReturnOrder returnOrder;

    @Column
    String authCode;

    /**
     * @return the totalAmount
     */
    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    /**
     * @param totalAmount the totalAmount to set
     */
    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    /**
     * @return the transactionKey
     */
    public String getTransactionKey() {
        return transactionKey;
    }

    /**
     * @param transactionKey the transactionKey to set
     */
    public void setTransactionKey(String transactionKey) {
        this.transactionKey = transactionKey;
    }

    public String getFailureReason() {
        return failureReason;
    }

    public void setFailureReason(String failureReason) {
        this.failureReason = failureReason;
    }

    public List<String> getFailureDetails() {
        return failureDetails;
    }

    public void setFailureDetails(List<String> failureDetails) {
        this.failureDetails = failureDetails;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    public boolean isTransactionSuccess() {
        return transactionSuccess;
    }

    public void setTransactionSuccess(boolean transactionSuccess) {
        this.transactionSuccess = transactionSuccess;
    }

    public String getTrackingNum() {
        return trackingNum;
    }

    public void setTrackingNum(String trackingNum) {
        this.trackingNum = trackingNum;
    }

    public BigDecimal getTransactionAmount() {
        return transactionAmount;
    }

    public void setTransactionAmount(BigDecimal transactionAmount) {
        this.transactionAmount = transactionAmount;
    }

    public BigDecimal getSalesTax() {
        return salesTax;
    }

    public void setSalesTax(BigDecimal salesTax) {
        this.salesTax = salesTax;
    }

    public BigDecimal getFreightAmount() {
        return freightAmount;
    }

    public void setFreightAmount(BigDecimal freightAmount) {
        this.freightAmount = freightAmount;
    }

    public BigDecimal getSubtotal() {
        return subtotal;
    }

    public void setSubtotal(BigDecimal subtotal) {
        this.subtotal = subtotal;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(final Date timestamp) {
        this.timestamp = timestamp;
    }

    public String getApdPo() {
        return apdPo;
    }

    public void setApdPo(String apdPo) {
        this.apdPo = apdPo;
    }

    public String getCustPo() {
        return custPo;
    }

    public void setCustPo(String custPo) {
        this.custPo = custPo;
    }

    public String getCardIdentifier() {
        return cardIdentifier;
    }

    public void setCardIdentifier(String cardIdentifier) {
        this.cardIdentifier = cardIdentifier;
    }

    public Date getCardExpriation() {
        return cardExpriation;
    }

    public void setCardExpriation(Date cardExpriation) {
        this.cardExpriation = cardExpriation;
    }

    public String getNameOnCard() {
        return nameOnCard;
    }

    public void setNameOnCard(String nameOnCard) {
        this.nameOnCard = nameOnCard;
    }

    public int getItemCount() {
        return itemCount;
    }

    public void setItemCount(int itemCount) {
        this.itemCount = itemCount;
    }

    public List<String> getVendorList() {
        return vendorList;
    }

    public void setVendorList(List<String> vendorList) {
        this.vendorList = vendorList;
    }

    /**
     * @return the lineItemXShipments
     */
    public List<LineItemXShipment> getLineItemXShipments() {
        return lineItemXShipments;
    }

    /**
     * @param lineItemXShipments the lineItemXShipments to set
     */
    public void setLineItemXShipments(List<LineItemXShipment> lineItemXShipments) {
        this.lineItemXShipments = lineItemXShipments;
    }

    public ReturnOrder getReturnOrder() {
		return returnOrder;
	}

	public void setReturnOrder(ReturnOrder returnOrder) {
		this.returnOrder = returnOrder;
	}

	public String getAuthCode() {
        return authCode;
    }

    public void setAuthCode(String authCode) {
        this.authCode = authCode;
    }
}
