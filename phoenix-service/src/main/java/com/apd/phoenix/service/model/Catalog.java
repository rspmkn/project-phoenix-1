package com.apd.phoenix.service.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.annotation.Nullable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.persistence.Version;
import javax.xml.bind.annotation.XmlRootElement;
import org.hibernate.annotations.Index;
import java.lang.Override;
import org.hibernate.envers.Audited;

/**
 * The Class Catalog.
 */
@Entity
@XmlRootElement
public class Catalog implements Serializable, com.apd.phoenix.service.model.Entity {

    private static final long serialVersionUID = -5478015932315341430L;

    public static enum SearchType {
        SOLR_SEARCH("Solr Search"), SMART_SEARCH("Smart Search");

        private final String label;

        private SearchType(String label) {
            this.label = label;
        }

        public String getLabel() {
            return this.label;
        }
    }

    /** The version. */
    @Version
    private @Column(name = "version", nullable = false)
    int version = 0;

    @Id
    private @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false, nullable = false)
    Long id = null;

    @Index(name = "CATALOG_NAME_IDX")
    @Column(name = "name", nullable = false, unique = true)
    private String name;

    /** The start date. */
    private @Temporal(TemporalType.DATE)
    Date startDate;

    /** The start date. */
    private @Temporal(TemporalType.DATE)
    Date changeDate;

    /** The expiration date. */
    private @Temporal(TemporalType.DATE)
    Date expirationDate;

    /** The expiration date. */
    @Audited
    private @Temporal(TemporalType.TIMESTAMP)
    Date lastProcessStart;

    /** The expiration date. */
    @Audited
    private @Temporal(TemporalType.TIMESTAMP)
    Date lastProcessEnd;

    @Column
    private Integer unprocessedQueueCount;

    @Column
    private Integer processedQueueCount;

    @ManyToOne(fetch = FetchType.LAZY)
    private Catalog parent;

    @ManyToOne(fetch = FetchType.LAZY)
    private Catalog replacement;

    /** The vendor. */
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "vendor_id")
    private Vendor vendor;

    /** The vendor. */
    @ManyToOne(fetch = FetchType.EAGER)
    private Account customer;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "catalog", orphanRemoval = true)
    private Set<FavoritesList> favorites = new HashSet<FavoritesList>();

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true, mappedBy = "catalog")
    private Set<TopLevelHierarchyXCatalog> topLevelHierarchies = new HashSet<TopLevelHierarchyXCatalog>();

    @Transient
    private List<TopLevelHierarchyXCatalog> orderedTopLevelHierarchies;

    @Column
    @Audited
    private Boolean upsableShippingRatePercent;

    @Column
    @Audited
    private BigDecimal upsableShippingRate;

    @Column
    private BigDecimal upsableShippingMinimum;

    @Column
    private BigDecimal upsableShippingMaximum;

    @Column
    @Audited
    private Boolean nonUpsableShippingRatePercent;

    @Column
    @Audited
    private BigDecimal nonUpsableShippingRate;

    @Column
    @Audited
    private BigDecimal nonUpsableShippingMinimum;

    @Column
    @Audited
    private BigDecimal nonUpsableShippingMaximum;

    @Column
    private Boolean changedItemRelevantFields;

    @Column
    private Boolean regenerateCsvNightly = false;

    @Temporal(TemporalType.DATE)
    private Date regenerateCsvDate;

    @Column
    private Boolean reindexNightly = false;

    @Temporal(TemporalType.DATE)
    private Date reindexDate;

    @Temporal(TemporalType.DATE)
    private Date regenerateSmartOciDate;

    @Enumerated(EnumType.STRING)
    private SearchType searchType;

    @Column
    private Long itemHash = null;

    @Column
    private Long coreItemHash = null;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private ListHashToUid listUid;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private ListHashToUid coreListUid;

    @Column
    private String marketingImageUrlTop;

    @Column
    private String marketingImageLinkUrlTop;

    @Column
    private String marketingImageUrlBottom;

    @Column
    private String marketingLinkUrlBottom;

    @OneToMany(mappedBy = "catalog", cascade = CascadeType.ALL)
    @Nullable
    private Set<CarouselDisplay> carouselDisplayItems = new HashSet<CarouselDisplay>();

    @Column
    private String processingCompleteEmails;

    @Column
    private Boolean diffCsvChecked = false;

    /**
     * @return the version
     */
    public int getVersion() {
        return version;
    }

    /**
     * @param version the version to set
     */
    public void setVersion(int version) {
        this.version = version;
    }

    @OneToMany(mappedBy = "parent", cascade = CascadeType.ALL)
    private Set<Catalog> children = new HashSet<Catalog>();

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the startDate
     */
    public Date getStartDate() {
        return startDate;
    }

    /**
     * @param startDate the startDate to set
     */
    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    /**
     * @return the expirationDate
     */
    public Date getExpirationDate() {
        return expirationDate;
    }

    /**
     * @param expirationDate the expirationDate to set
     */
    public void setExpirationDate(Date expirationDate) {
        this.expirationDate = expirationDate;
    }

    public Date getLastProcessStart() {
        return lastProcessStart;
    }

    public void setLastProcessStart(Date lastProcessStart) {
        this.lastProcessStart = lastProcessStart;
    }

    public Date getLastProcessEnd() {
        return lastProcessEnd;
    }

    public void setLastProcessEnd(Date lastProcessEnd) {
        this.lastProcessEnd = lastProcessEnd;
    }

    public Integer getUnprocessedQueueCount() {
        return unprocessedQueueCount;
    }

    public void setUnprocessedQueueCount(Integer unprocessedQueueCount) {
        this.unprocessedQueueCount = unprocessedQueueCount;
    }

    public Integer getProcessedQueueCount() {
        return processedQueueCount;
    }

    public void setProcessedQueueCount(Integer processedQueueCount) {
        this.processedQueueCount = processedQueueCount;
    }

    /**
     * @return the parent
     */
    public Catalog getParent() {
        return parent;
    }

    /**
     * @param parent the parent to set
     */
    public void setParent(Catalog parent) {
        this.parent = parent;
    }

    /**
     * @return the replacement
     */
    public Catalog getReplacement() {
        return replacement;
    }

    /**
     * @param replacement the replacement to set
     */
    public void setReplacement(Catalog replacement) {
        this.replacement = replacement;
    }

    /**
     * @return the vendor
     */
    public Vendor getVendor() {
        return vendor;
    }

    /**
     * @param vendor the vendor to set
     */
    public void setVendor(Vendor vendor) {
        this.vendor = vendor;
    }

    /**
     * @return the vendor
     */
    public Account getCustomer() {
        return this.customer;
    }

    /**
     * @param vendor the vendor to set
     */
    public void setCustomer(Account customer) {
        this.customer = customer;
    }

    /**
     * @return the children
     */
    public Set<Catalog> getChildren() {
        return children;
    }

    /**
     * @param children the children to set
     */
    public void setChildren(Set<Catalog> children) {
        this.children = children;
    }

    public Set<FavoritesList> getFavorites() {
        return favorites;
    }

    public void setFavorites(Set<FavoritesList> favorites) {
        this.favorites = favorites;
    }

    public Date getChangeDate() {
        return changeDate;
    }

    public void setChangeDate(Date changeDate) {
        this.changeDate = changeDate;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        if (id != null) {
            return id.equals(((Catalog) that).id);
        }
        return super.equals(that);
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        if (id != null) {
            return id.hashCode();
        }
        return super.hashCode();
    }

    public Boolean getUpsableShippingRatePercent() {
        return this.upsableShippingRatePercent;
    }

    public void setUpsableShippingRatePercent(final Boolean isPercent) {
        this.upsableShippingRatePercent = isPercent;
    }

    public BigDecimal getUpsableShippingRate() {
        return this.upsableShippingRate;
    }

    public void setUpsableShippingRate(final BigDecimal upsableShippingRate) {
        this.upsableShippingRate = upsableShippingRate;
    }

    public BigDecimal getUpsableShippingMinimum() {
        return this.upsableShippingMinimum;
    }

    public void setUpsableShippingMinimum(final BigDecimal upsableShippingMinimum) {
        this.upsableShippingMinimum = upsableShippingMinimum;
    }

    public BigDecimal getUpsableShippingMaximum() {
        return this.upsableShippingMaximum;
    }

    public void setUpsableShippingMaximum(final BigDecimal upsableShippingMaximum) {
        this.upsableShippingMaximum = upsableShippingMaximum;
    }

    public Boolean getNonUpsableShippingRatePercent() {
        return this.nonUpsableShippingRatePercent;
    }

    public void setNonUpsableShippingRatePercent(final Boolean isPercent) {
        this.nonUpsableShippingRatePercent = isPercent;
    }

    public BigDecimal getNonUpsableShippingRate() {
        return this.nonUpsableShippingRate;
    }

    public void setNonUpsableShippingRate(final BigDecimal nonUpsableShippingRate) {
        this.nonUpsableShippingRate = nonUpsableShippingRate;
    }

    public BigDecimal getNonUpsableShippingMinimum() {
        return this.nonUpsableShippingMinimum;
    }

    public void setNonUpsableShippingMinimum(final BigDecimal nonUpsableShippingMinimum) {
        this.nonUpsableShippingMinimum = nonUpsableShippingMinimum;
    }

    public BigDecimal getNonUpsableShippingMaximum() {
        return this.nonUpsableShippingMaximum;
    }

    public void setNonUpsableShippingMaximum(final BigDecimal nonUpsableShippingMaximum) {
        this.nonUpsableShippingMaximum = nonUpsableShippingMaximum;
    }

    @Override
    public String toString() {
        String result = getClass().getSimpleName() + " ";
        result += "serialVersionUID: " + serialVersionUID;
        if (name != null && !name.trim().isEmpty())
            result += ", name: " + name;
        return result;
    }

    /**
     * @return the changedItemRelevantFields
     */
    public Boolean getChangedItemRelevantFields() {
        return changedItemRelevantFields;
    }

    /**
     * @param changedItemRelevantFields the changedItemRelevantFields to set
     */
    public void setChangedItemRelevantFields(Boolean changedItemRelevantFields) {
        this.changedItemRelevantFields = changedItemRelevantFields;
    }

    public Boolean getRegenerateCsvNightly() {
        return regenerateCsvNightly;
    }

    public void setRegenerateCsvNightly(Boolean regenerateCsvNightly) {
        this.regenerateCsvNightly = regenerateCsvNightly;
    }

    public Date getRegenerateCsvDate() {
        return regenerateCsvDate;
    }

    public void setRegenerateCsvDate(Date regenerateCsvDate) {
        this.regenerateCsvDate = regenerateCsvDate;
    }

    public Boolean getReindexNightly() {
        return reindexNightly;
    }

    public void setReindexNightly(Boolean reindexNightly) {
        this.reindexNightly = reindexNightly;
    }

    public Date getReindexDate() {
        return reindexDate;
    }

    public void setReindexDate(Date reindexDate) {
        this.reindexDate = reindexDate;
    }

    public Date getRegenerateSmartOciDate() {
        return regenerateSmartOciDate;
    }

    public void setRegenerateSmartOciDate(Date regenerateSmartOciDate) {
        this.regenerateSmartOciDate = regenerateSmartOciDate;
    }

    public SearchType getSearchType() {
        return this.searchType;
    }

    public void setSearchType(final SearchType searchType) {
        this.searchType = searchType;
    }

    public Long getItemHash() {
        return itemHash;
    }

    public void setItemHash(Long itemHash) {
        this.itemHash = itemHash;
    }

    public ListHashToUid getListUid() {
        return listUid;
    }

    public void setListUid(ListHashToUid listUid) {
        this.listUid = listUid;
    }

    public Long getCoreItemHash() {
        return coreItemHash;
    }

    public void setCoreItemHash(Long coreItemHash) {
        this.coreItemHash = coreItemHash;
    }

    public ListHashToUid getCoreListUid() {
        return coreListUid;
    }

    public void setCoreListUid(ListHashToUid coreListUid) {
        this.coreListUid = coreListUid;
    }

    public Set<TopLevelHierarchyXCatalog> getTopLevelHierarchies() {
        return topLevelHierarchies;
    }

    public void setTopLevelTopLevelHierarchies(Set<TopLevelHierarchyXCatalog> topLevelHierarchyPriorities) {
        this.topLevelHierarchies = topLevelHierarchyPriorities;
    }

    public List<TopLevelHierarchyXCatalog> getOrderedTopLevelHierarchies() {
        if (orderedTopLevelHierarchies == null) {
            setOrderedTopLevelHierarchies(new ArrayList<TopLevelHierarchyXCatalog>(getTopLevelHierarchies()));
        }
        Collections.sort(orderedTopLevelHierarchies);
        return orderedTopLevelHierarchies;

    }

    public void setOrderedTopLevelHierarchies(List<TopLevelHierarchyXCatalog> orderedtopLevelHierarchies) {
        this.orderedTopLevelHierarchies = orderedtopLevelHierarchies;
    }

    public String getMarketingImageUrlTop() {
        return marketingImageUrlTop;
    }

    public void setMarketingImageUrlTop(String marketingImageUrlTop) {
        this.marketingImageUrlTop = marketingImageUrlTop;
    }

    public String getMarketingImageLinkUrlTop() {
        return marketingImageLinkUrlTop;
    }

    public void setMarketingImageLinkUrlTop(String marketingImageLinkUrlTop) {
        this.marketingImageLinkUrlTop = marketingImageLinkUrlTop;
    }

    public String getMarketingImageUrlBottom() {
        return marketingImageUrlBottom;
    }

    public void setMarketingImageUrlBottom(String marketingImageUrlBottom) {
        this.marketingImageUrlBottom = marketingImageUrlBottom;
    }

    public String getMarketingLinkUrlBottom() {
        return marketingLinkUrlBottom;
    }

    public void setMarketingLinkUrlBottom(String marketingLinkUrlBottom) {
        this.marketingLinkUrlBottom = marketingLinkUrlBottom;
    }

    public Set<CarouselDisplay> getCarouselDisplayItems() {
        return carouselDisplayItems;
    }

    public void setCarouselDisplayItems(Set<CarouselDisplay> carouselDisplayItems) {
        this.carouselDisplayItems = carouselDisplayItems;
    }

    public String getProcessingCompleteEmails() {
        return processingCompleteEmails;
    }

    public void setProcessingCompleteEmails(String processingCompleteEmails) {
        this.processingCompleteEmails = processingCompleteEmails;
    }

    public Boolean getDiffCsvChecked() {
        return diffCsvChecked;
    }

    public void setDiffCsvChecked(Boolean diffCsvChecked) {
        this.diffCsvChecked = diffCsvChecked;
    }
}
