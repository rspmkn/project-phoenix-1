package com.apd.phoenix.service.business;

import java.util.List;
import java.util.Set;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import org.apache.commons.lang.StringUtils;
import com.apd.phoenix.service.model.Permission;
import com.apd.phoenix.service.model.Role;
import com.apd.phoenix.service.model.RoleXUser;
import com.apd.phoenix.service.model.SystemUser;
import com.apd.phoenix.service.persistence.jpa.RoleDao;

/**
 * This class provides business process methods for Roles.
 * 
 * @author RHC
 * 
 */
@Stateless
@LocalBean
public class RoleBp extends AbstractBp<Role> {

    /**
     * This method is injected with the correct Dao for initialization
     * 
     * @param dao
     */
    @Inject
    public void initDao(RoleDao dao) {
        this.dao = dao;
    }

    /**
     * Adds an role as a child of another role. Adding a child fails silently if
     * the resulting acount graph would be cyclic.
     * 
     * @param parent
     *            - the parent role in the new relationship
     * @param child
     *            - the child role in the new relationship
     * @return the persisted, managed parent role
     */
    //	public Role addChildRole(Role parent, Role child) {
    //
    //		/*
    //		 * Iterate through ancestors of the parent role. If one of the ancestor
    //		 * roles has the same ID as the child role, then the graph would be
    //		 * cyclical, so the parent would be returned without adding the child.
    //		 */
    //		Role ancestor = parent;
    //		while (ancestor.getParentRole() != null) {
    //			if (ancestor.getId() == child.getId()) {
    //				return parent;
    //			}
    //			ancestor = ancestor.getParentRole();
    //		}
    //
    //		// Removes the child from the original parent, if there is one
    //		if (child.getParentRole() != null) {
    //			child.getParentRole().getChildren().remove(child);
    //			this.dao.update(child.getParentRole());
    //		}
    //
    //		// Adds the child to the parent
    //		parent.getChildren().add(child);
    //		child.setParentRole(parent);
    //
    //		// Sets the child's root role to the parent's root role
    //		if (parent.getRootRole() == null) {
    //			setRootRole(child, parent);
    //		} else {
    //			setRootRole(child, parent.getRootRole());
    //		}
    //
    //		this.dao.update(child);
    //		return this.dao.update(parent);
    //	}

    /**
     * Sets the root Role of this role and all of its sub-Roles. This method
     * does not implement business logic other than recursion through the role
     * tree, so it is not safe.
     * 
     * @param role
     *            - the role whose root role will be changed
     * @param root
     *            - the new root role
     */
    //	private void setRootRole(Role role, Role root) {
    //		role.setRootRole(root);
    //		for (Role child : role.getChildren()) {
    //			setRootRole(child, root);
    //		}
    //	}

    /**
     * Deletes the entity, determined by the id from the database.
     * 
     * @param id
     *            - the id of the entity to be deleted
     */
    @Override
    public void delete(Long id, Class<Role> type) {
        //		Role toDelete = this.dao.findById(id, type);
        //
        //		// Checks that the role has no children
        //		if ((toDelete.getChildren() != null && !toDelete.getChildren()
        //				.isEmpty())) {
        //			return;
        //		}

        this.dao.delete(id, type);
    }

    /**
     * Deactivates the given role, and all of its sub-roles.
     * 
     * @param role
     *            - the role to be deactivated
     */
    public void deactivate(Role role) {
        //		for (Role subRole : role.getChildren()) {
        //			deactivate(subRole);
        //		}
        role.setIsActive(false);
        this.dao.update(role);
    }

    /**
     * Activates the given role, unless its parent role isn't active. If
     * recursive is set to true, all of the sub-roles are activated as well.
     * 
     * @param role
     *            - the role to be activated
     * @param recursive
     *            - set to true if the sub-roles should be activated as well
     */
    public void activate(Role role, boolean recursive) {
        //		if (role.getParentRole() == null || role.getParentRole().getIsActive()) {
        role.setIsActive(true);
        //			if (recursive) {
        //				for (Role child : role.getChildren()) {
        //					activate(child, recursive);
        //				}
        //			}
        //		}
        this.dao.update(role);
    }

    /**
     * Get permissions attached to a given Role.
     * 
     * @param role
     * @return
     */
    @SuppressWarnings("static-method")
    public Set<Permission> getRolePermissions(Role role) {
        return role.getPermissions();
    }

    /**
     * Assigns a Permission to a specified Role.
     * 
     * @param permission
     * @param role
     */
    public void assignPermissionToRole(Permission permission, Role role) {
        Set<Permission> permissions = role.getPermissions();
        permissions.add(permission);
        role.setPermissions(permissions);
        this.dao.update(role);
    }

    /**
     * Removes a Permission from a specified Role.
     * 
     * @param permission
     * @param role
     */
    public void removePermissionFromRole(Permission permission, Role role) {
        Set<Permission> permissions = role.getPermissions();
        permissions.remove(permission);
        role.setPermissions(permissions);
        this.dao.update(role);
    }

    /**
     * Change the specified Role's name to a different one.
     * 
     * @param role
     * @param newName
     */
    public void renameRole(Role role, String newName) {
        role.setName(newName);
        this.dao.update(role);
    }

    public Role findRole(String name) {
        if (StringUtils.isNotBlank(name)) {
            Role searchRole = new Role();
            searchRole.setName(name);
            List<Role> results = this.searchByExactExample(searchRole, 0, 0);
            if (results != null && !results.isEmpty()) {
                return results.get(0);
            }
        }
        return null;
    }

    public SystemUser populateUserDefaults(SystemUser systemUser) {
        Role toAdd = findRole("User");
        if (toAdd != null) {
            RoleXUser roleXUser = new RoleXUser();
            roleXUser.setRole(toAdd);
            systemUser.getRoleXUsers().add(roleXUser);
        }
        else {
            LOG.error("Could not find User role, unable to populate default role.");
        }
        return systemUser;
    }

}
