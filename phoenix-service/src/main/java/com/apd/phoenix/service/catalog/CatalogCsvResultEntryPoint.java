package com.apd.phoenix.service.catalog;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.MessageProducer;
import javax.jms.ObjectMessage;
import javax.jms.Queue;
import javax.jms.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.persistence.multitenancy.impl.CurrentTenantIdentifierResolverImpl;

@Stateless
public class CatalogCsvResultEntryPoint {

    private static final Logger LOGGER = LoggerFactory.getLogger(CatalogCsvResultEntryPoint.class);

    @Resource(mappedName = "java:/activemq/ConnectionFactory")
    private ConnectionFactory connectionFactory;

    @Resource(mappedName = "java:/activemq/catalog-csv-rows")
    private Queue queue;

    public void scheduleRequest(CatalogCsvResult rows) {
        Connection connection = null;
        Session msession = null;
        MessageProducer messageProducer = null;
        try {
            connection = connectionFactory.createConnection();
            msession = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            messageProducer = msession.createProducer(queue);
            ObjectMessage message = msession.createObjectMessage();
            message.setObject(rows);
            message.setLongProperty("tenantId", CurrentTenantIdentifierResolverImpl.getCurrentTenant());
            messageProducer.send(message);
        }
        catch (Exception e) {
            LOGGER.error("An error occured:", e);
            ;
        }
        finally {
            try {
                connection.close();
            }
            catch (Exception e) {
                LOGGER.error("Could not close connection", e);
            }
        }
    }

}
