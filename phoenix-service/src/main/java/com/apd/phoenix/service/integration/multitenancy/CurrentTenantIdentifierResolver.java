package com.apd.phoenix.service.integration.multitenancy;

public interface CurrentTenantIdentifierResolver {

    public Long resolveCurrentTenantIdentifier();

}
