/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.apd.phoenix.service.business;

import com.apd.phoenix.service.persistence.jpa.DesktopDao;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;

/**
 *
 * @author anicholson
 */
@Stateless
@LocalBean
public class DesktopBp extends AbstractBp {

    @Inject
    public void initDao(DesktopDao dao) {
        this.dao = dao;
    }

}
