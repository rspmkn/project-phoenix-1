package com.apd.phoenix.service.business;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import org.apache.commons.lang.StringUtils;
import com.apd.phoenix.service.model.Brand;
import com.apd.phoenix.service.persistence.jpa.BrandDao;

@Stateless
@LocalBean
public class BrandBp extends AbstractBp<Brand> {

    @Inject
    public void initDao(BrandDao dao) {
        this.dao = dao;
    }

    public Brand findByUsscoId(String id) {
        if (!StringUtils.isEmpty(id)) {
            return ((BrandDao) this.dao).findByUsscoId(id);
        }
        return null;
    }

    public Brand findByBrandName(String brandName) {
        if (!StringUtils.isEmpty(brandName)) {
            return ((BrandDao) this.dao).findByBrandName(brandName);
        }
        return null;
    }

}
