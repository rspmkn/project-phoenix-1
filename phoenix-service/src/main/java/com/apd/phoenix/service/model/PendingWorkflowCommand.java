package com.apd.phoenix.service.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;
import org.hibernate.annotations.Index;
import org.hibernate.annotations.Type;
import com.apd.phoenix.service.executor.api.CommandContext;

/**
 * This class is used to specify different types of fields for the AddressField
 * entity. This allows the system to use common address fields such as city or
 * zip code, as well as uncommon ones like floor number.
 * 
 * @author RHC
 * 
 */
@Entity
public class PendingWorkflowCommand implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id = null;
    @Version
    @Column(name = "version", nullable = false)
    private int version = 0;

    @Index(name = "PWCOMMAND_EXPIRES_IDX")
    @Temporal(TemporalType.TIMESTAMP)
    private Date expires;

    @Column
    private String commandName;

    @Type(type = "serializable")
    @Column(length = 5000)
    private CommandContext ctx;

    @Index(name = "PWCOMMAND_EXECUTED_IDX")
    @Column(nullable = false)
    private boolean executed = false;

    public Date getExpires() {
        return expires;
    }

    public void setExpires(Date expires) {
        this.expires = expires;
    }

    public String getCommandName() {
        return commandName;
    }

    public void setCommandName(String commandName) {
        this.commandName = commandName;
    }

    public CommandContext getCtx() {
        return ctx;
    }

    public void setCtx(CommandContext ctx) {
        this.ctx = ctx;
    }

    public boolean isExecuted() {
        return executed;
    }

    public void setExecuted(boolean executed) {
        this.executed = executed;
    }

    public Long getId() {
        return this.id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public int getVersion() {
        return this.version;
    }

    public void setVersion(final int version) {
        this.version = version;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        if (id != null) {
            return id.equals(((PendingWorkflowCommand) that).id);
        }
        return super.equals(that);
    }

    @Override
    public int hashCode() {
        if (id != null) {
            return id.hashCode();
        }
        return super.hashCode();
    }
}
