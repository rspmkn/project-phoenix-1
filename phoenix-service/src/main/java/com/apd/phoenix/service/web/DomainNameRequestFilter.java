package com.apd.phoenix.service.web;

import java.io.IOException;
import java.io.Serializable;
import javax.inject.Inject;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.persistence.multitenancy.api.CurrentTenantDomain;

/**
 * A WebFilter used to intercept requests and detect the targeted domain name.  This is used to set the {@link CurrentTenantDomain}
 * for each request that comes in.  This way we always know which tenant the user belongs to. This is used in conjunction with 
 * DomainNameRequestListener, which is a phase listener that sets the tenant ID. The listener isn't called for non-JSF calls (such
 * as rest requests), and the filter isn't called if the user isn't authenticated (like on the login pages), so both are necessary.
 * @author Red Hat Consulting
 *
 */
@WebFilter
public class DomainNameRequestFilter implements Serializable, Filter {

    private static final long serialVersionUID = 2459022678664287400L;
    private static final Logger LOG = LoggerFactory.getLogger(DomainNameRequestFilter.class);

    @Inject
    private CurrentTenantDomain tenantDomain;

    public DomainNameRequestFilter() {
        super();
    }

    @Override
    public void destroy() {
        //Do nothing
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain) throws IOException,
            ServletException {
        LOG.debug("Calling servlet filter to set domain");

        final HttpServletRequest httpRequest = (HttpServletRequest) request;

        String domain = DomainNameTenantLogic.setTenantAndGetDomain(httpRequest);

        tenantDomain.setDomain(domain);

        filterChain.doFilter(request, response);

    }

    @Override
    public void init(FilterConfig arg0) throws ServletException {
        //Do nothing
    }

}
