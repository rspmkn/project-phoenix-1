package com.apd.phoenix.service.integration.multitenancy;

import java.util.concurrent.ConcurrentHashMap;
import org.jboss.weld.context.AbstractContext;
import org.jboss.weld.context.beanstore.BeanStore;
import org.jboss.weld.context.beanstore.ConcurrentHashMapBeanStore;

/**
 * Custom CDI context to hold a tenant id, currently non-functional
 * 
 * @author rmallred
 *
 */
public class TenantContext extends AbstractContext {

    private static CurrentTenantIdentifierResolver tenantResolver;

    private ConcurrentHashMap<Long, BeanStore> store;

    public static void setCurrentTenantIdentifierResolver(CurrentTenantIdentifierResolver tenantResolver) {
        TenantContext.tenantResolver = tenantResolver;
    }

    public TenantContext() {
        super(true);
        store = new ConcurrentHashMap<Long, BeanStore>();
    }

    public Class getScope() {
        return TenantScoped.class;
    }

    public boolean isActive() {
        return true;
        //return getCurrentTenantId() != null;
    }

    @Override
    protected BeanStore getBeanStore() {
        Long current = getCurrentTenantId();
        return getTenantStore(current);
    }

    private Long getCurrentTenantId() {
        if (TenantContext.tenantResolver == null)
            return null;

        return TenantContext.tenantResolver.resolveCurrentTenantIdentifier();
    }

    private BeanStore getTenantStore(Long tenant) {
        if (!store.containsKey(tenant)) {
            store.putIfAbsent(tenant, new ConcurrentHashMapBeanStore());
        }
        return store.get(tenant);
    }
}