package com.apd.phoenix.service.persistence.jpa;

import java.util.List;

/**
 * A generic interface for DAOs.
 * 
 * @author RHC
 *
 * @param <T>
 */
public interface Dao<T> {

    /**
     * Persists the entity t, adding it to the database.
     * 
     * @param t - the object to be persisted
     * @return the managed entity
     */
    public T create(T t);

    /**
     * Persists the entity t, adding it to the database. It doesn't return a managed version, but should be somewhat 
     * faster.
     * 
     * @param t - the object to be persisted
     * @return the managed entity
     */
    public void createNoRefresh(T t);

    /**
     * Given a database key id and the entity type to return, finds the entity in the database.
     * 
     * @param id - the id to search by
     * @param type - the class type of the entity to be returned
     * @return the entity found with the id, or null if it doesn't exist
     */
    public T findById(Long id, Class<T> type);

    /**
     * Deletes the entity, determined by the id from the database.
     * 
     * @param id - the id of the entity to be deleted
     * @param type - the class type of the entity to be deleted
     */
    public void delete(Long id, Class<T> type);

    /**
     * Given a modified entity that exists on the database, updates the database with the changed entity.
     * 
     * @param t - the entity to be updated
     * @return the updated, managed entity
     */
    public T update(T t);

    /**
     * Given a type, the starting index, and the number to return, returns an 
     * unfiltered List<T> of entities in this database table.
     * 
     * @param type - the class type of the entities to be returned
     * @param startIndex - the position of the first item to be returned
     * @param quantity - the number of items to return. If this is equal to 0, returns all items.
     * @return A List<T> containing a number of elements determined by the quantity field.
     */
    public List<T> findAll(Class<T> type, int startIndex, int quantity);

    /**
     * Given an example, the starting index, and the number to return, returns a List<T> of 
     * entities that match the selected entity. Any fields that are null will not be matched 
     * against.
     * <p/>
     * IMPORTANT: Persisted, managed entities CANNOT be used a search parameter. To search, create 
     * new objects, setting the String fields to the values to search on.
     * <p/>
     * Example: we want the first 10 SystemUsers assigned to Accounts where the root account's 
     * name contains the string "root". We would create the search example in the following way:
     * <p/>
     * <pre>
     * {@code
     * SystemUser search = new SystemUser();
     * Account root = new Account();
     * Account account = new Account();
     * root.setName("root");
     * account.setRootAccount(root);
     * search.getAccounts.addAccount(account);
     * searchByExample(search, 0, 10);
     * } </pre>
     * 
     * @param search - the entity to search by
     * @param startIndex - the position of the first item to be returned
     * @param quantity - the number of items to return. If this is equal to 0, returns all items.
     * @return The entities whose String fields contain the Strings in the search parameter.
     */
    public List<T> searchByExample(T search, int startIndex, int quantity);

    /**
     * Works like searchByExample, with the additional parameter of an entity for ordering. To order by a field, the 
     * value of that field must be set to an integer indicating precedence, followed by " asc" or " desc". So, for a 
     * Person, if we are to sort by lastname then by firstname, we would pass in a transient Person entity whose 
     * firstname is "2 desc" and lastname is "1 desc".
     * <p>
     * You can order over properties that an object has, such as ordering Persons alphabetically by the address with 
     * the ShipTo type, or ordering Items by the property with the APDcost type. In that case, you would build the 
     * ordering entity with a property whose value is "1 asc" whose type has a name "APDcost". The search is exact; if 
     * an item doesn't have a property with the type "APDcost", it won't be returned.
     * </p>
     * 
     * @param search
     * @param ordering
     * @param startIndex
     * @param quantity
     * @return
     */
    public List<T> searchByExample(T search, T ordering, int startIndex, int quantity);

    /**
     * Given an entity, the starting index, and the number to return, returns a List<T> of 
     * entities that exactly match the selected entity. Any fields that are null will not be 
     * matched against.
     * <p/>
     * IMPORTANT: Persisted, managed entities CANNOT be used a search parameter. To search, create 
     * new objects, setting the String fields to the values to search on.
     * <p/>
     * Example: we want the first 10 SystemUsers assigned to Accounts where the root account's 
     * name is "root". We would create the search example in the following way:
     * <p/>
     * <pre>
     * {@code
     * SystemUser search = new SystemUser();
     * Account root = new Account();
     * Account account = new Account();
     * root.setName("root");
     * account.setRootAccount(root);
     * search.getAccounts.addAccount(account);
     * searchByExactExample(search, 0, 10);
     * }</pre>
     * 
     * @param search - the entity to search by
     * @param startIndex - the position of the first item to be returned
     * @param quantity - the number of items to return. If this is equal to 0, returns all items.
     * @return The entities whose String fields exactly match the Strings in the search parameter.
     */
    public List<T> searchByExactExample(T search, int startIndex, int quantity);

    /**
     * Works like searchByExactExample, with the additional parameter of an entity for ordering. To order by a field, 
     * the value of that field must be set to an integer indicating precedence, followed by " asc" or " desc". So, for 
     * a Person, if we are to sort by lastname then by firstname, we would pass in a transient Person entity whose 
     * firstname is "2 desc" and lastname is "1 desc".
     * <p>
     * You can order over properties that an object has, such as ordering Persons alphabetically by the address with 
     * the ShipTo type, or ordering Items by the property with the APDcost type. In that case, you would build the 
     * ordering entity with a property whose value is "1 asc" whose type has a name "APDcost". The search is exact; if 
     * an item doesn't have a property with the type "APDcost", it won't be returned.
     * </p>
     * 
     * @param search
     * @param ordering
     * @param startIndex
     * @param quantity
     * @return
     */
    public List<T> searchByExactExample(T search, T ordering, int startIndex, int quantity);

    /**
     * Returns the number of items that will be found if a search with the given entity is done.
     * 
     * @param search - the entity to be searched by
     * @return The number of items that will be returned.
     */
    public Long resultQuantity(T search);

    /**
     * Takes in an entity, and returns an instance of that entity fully hydrated. If any changes have been made the 
     * the entity, those changes will be persisted, and eventually will be flushed to the database. If the entity 
     * passed in is managed, the passed in object will be the one hydrated and returned.
     * 
     * @param toLoad - the entity to load
     * @return The hydrated entity.
     */
    public T eagerLoad(T toLoad);

    /**
     *Check if the instance is a managed entity instance belonging to the current persistence context.
     * 
     *@param search - the entity to locate
     *@return Whether or not the entity is managed
     */
    public boolean isManaged(T search);

    /**
     * This method applies a write lock to an entity, which will be released when the transaction commits.
     * 
     * @param toLock
     */
    public T findAndLock(Long id, Class<T> type);

}
