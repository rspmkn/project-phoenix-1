package com.apd.phoenix.service.cache;

import javax.annotation.PreDestroy;
import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.enterprise.context.ApplicationScoped;
import com.apd.phoenix.service.product.CatalogData;
import com.apd.phoenix.service.product.CatalogDataKey;

/**
 * This class extends AbstractCacheProvider, using ProductKey and Product as the Key,Value pair.
 * 
 * @author RHC
 *
 */
@ApplicationScoped
@Lock(LockType.READ)
public class CatalogDataCacheProvider extends AbstractCacheProvider<CatalogDataKey, CatalogData> {

    private static final String CATALOG_DATA_CACHE_NAME = cacheProperties.getProperty("catalogDataCache.name");

    @Override
    protected String getCacheName() {
        return CATALOG_DATA_CACHE_NAME;
    }

    @PreDestroy
    public void cleanup() {
        super.cleanup();
    }

}
