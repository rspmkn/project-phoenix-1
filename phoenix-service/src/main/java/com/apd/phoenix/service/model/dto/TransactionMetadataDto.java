package com.apd.phoenix.service.model.dto;

import java.io.Serializable;

public class TransactionMetadataDto implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -4666738085046111636L;
    private long interchangeId;
    private long groupId;
    private long transactionId;

    public long getInterchangeId() {
        return interchangeId;
    }

    public void setInterchangeId(long interchangeId) {
        this.interchangeId = interchangeId;
    }

    public long getGroupId() {
        return groupId;
    }

    public void setGroupId(long groupId) {
        this.groupId = groupId;
    }

    public long getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(long transactionId) {
        this.transactionId = transactionId;
    }

}
