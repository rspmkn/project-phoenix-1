/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.service.model.dto;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

public class VendorCreditInvoiceDto extends VendorInvoiceDto {
    
    private static final long serialVersionUID=4439600474634073997L;
    
    //Return Authorization Number
    private String raNumber;

    private BigDecimal restockingFee;
    
    private Set<LineItemXReturnDto> items = new HashSet<>();

    /**
     * @return the raNumber
     */
    public String getRaNumber() {
        return raNumber;
    }

    /**
     * @param raNumber the raNumber to set
     */
    public void setRaNumber(String raNumber) {
        this.raNumber = raNumber;
    }

    /**
     * @return the restockingFee
     */
    public BigDecimal getRestockingFee() {
        return restockingFee;
    }

    /**
     * @param restockingFee the restockingFee to set
     */
    public void setRestockingFee(BigDecimal restockingFee) {
        this.restockingFee = restockingFee;
    }

    /**
     * @return the items
     */
    public Set<LineItemXReturnDto> getItems() {
        return items;
    }

    /**
     * @param items the items to set
     */
    public void setItems(Set<LineItemXReturnDto> items) {
        this.items = items;
    }

}
