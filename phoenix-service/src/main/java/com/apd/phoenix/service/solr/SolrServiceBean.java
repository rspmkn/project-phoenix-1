package com.apd.phoenix.service.solr;

import java.net.MalformedURLException;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.Singleton;
import javax.ejb.Startup;

import org.apache.solr.client.solrj.SolrServer;
import org.apache.solr.client.solrj.impl.CloudSolrServer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.apd.phoenix.core.utility.EcommercePropertiesLoader;
import com.apd.phoenix.core.utility.TenantConfigRepository;
import com.apd.phoenix.service.persistence.multitenancy.impl.CurrentTenantIdentifierResolverImpl;

@Startup
@Singleton
public class SolrServiceBean {
	
	private static final Logger LOG = LoggerFactory.getLogger(SolrServiceBean.class);

    //should be setup in EJB configuration.
    //TODO look into EJB ocnfiguration
    private final String zooKeeperNodeUrlList = EcommercePropertiesLoader.getInstance().getEcommerceProperties()
            .getString("zooKeeperUrlList");
    private Map<String, CloudSolrServer> solrServers = new HashMap<>();

    public SolrServer getSolrServer() {
        return solrServers.get(CurrentTenantIdentifierResolverImpl.getCurrentTenantSchema());
    }

    @PostConstruct
    private void setup() throws MalformedURLException {
        for (String tenant : TenantConfigRepository.getInstance().getTenantNames()) {
            String collection;
            String identifier;
        	identifier = TenantConfigRepository.getInstance().getTenantPropertiesByName(tenant).getSchemaName();
            collection = TenantConfigRepository.getInstance().getTenantPropertiesByName(tenant).getSolrCollection();
            //per: http://wiki.apache.org/solr/Solrj#Directly_adding_POJOs_to_Solr
            CloudSolrServer solrServer = new PhoenixSolrServer(zooKeeperNodeUrlList);
            solrServer.setDefaultCollection(collection);
            //     solrServer = new ConcurrentUpdateSolrServer(url, 1, 2);
            solrServers.put(identifier, solrServer);
            
        }
    }

    @PreDestroy
    private void destroy() {
        for (CloudSolrServer solrServer : solrServers.values()) {
            solrServer.shutdown();
        }
    }
}
