package com.apd.phoenix.service.model.dto;

import java.io.Serializable;
import java.math.BigInteger;

public class SegmentNoteDto implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private String segmentId;
    private BigInteger position;
    private String loopIndentifier;
    private BigInteger syntaxErrorCode;
    private String syntaxErrorDescription;

    public String getSegmentId() {
        return segmentId;
    }

    public void setSegmentId(String segmentId) {
        this.segmentId = segmentId;
    }

    public BigInteger getPosition() {
        return position;
    }

    public void setPosition(BigInteger position) {
        this.position = position;
    }

    public String getLoopIndentifier() {
        return loopIndentifier;
    }

    public void setLoopIndentifier(String loopIndentifier) {
        this.loopIndentifier = loopIndentifier;
    }

    public BigInteger getSyntaxErrorCode() {
        return syntaxErrorCode;
    }

    public void setSyntaxErrorCode(BigInteger syntaxErrorCode) {
        this.syntaxErrorCode = syntaxErrorCode;
    }

    public String getSyntaxErrorDescription() {
        return syntaxErrorDescription;
    }

    public void setSyntaxErrorDescription(String syntaxErrorDescription) {
        this.syntaxErrorDescription = syntaxErrorDescription;
    }

}
