package com.apd.phoenix.service.business;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import com.apd.phoenix.service.model.OrderType;
import com.apd.phoenix.service.persistence.jpa.OrderTypeDao;

/**
 * This class provides business process methods for OrderType.
 * 
 * @author RHC
 * 
 */
@Stateless
@LocalBean
public class OrderTypeBp extends AbstractBp<OrderType> {

    /**
     * This method is injected with the correct Dao for initialization
     * 
     * @param dao
     */
    @Inject
    public void initDao(OrderTypeDao dao) {
        this.dao = dao;
    }

    public OrderType getTypeFromValue(String value) {
        OrderType search = new OrderType();
        search.setValue(value);
        return searchByExample(search, 0, 1).get(0);
    }
}
