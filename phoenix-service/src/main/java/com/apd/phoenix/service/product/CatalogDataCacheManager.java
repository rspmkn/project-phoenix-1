package com.apd.phoenix.service.product;

import com.apd.phoenix.service.model.Catalog.SearchType;

/**
 * Interface for Product cache management
 * 
 * @author RHC
 *
 */
public interface CatalogDataCacheManager {

    /**
     * Takes a catalog and search type, and returns the data from the cache. If the data does 
     * not exist, returns null.
     * 
     * @param catalogId
     * @param searchType
     * @return
     */
    public CatalogData getCachedCatalogData(Long catalogId, SearchType searchType);

    /**
     * Takes a catalog, search type, and data, and stores the data in the cache.
     * 
     * @param catalogId
     * @param searchType
     * @param data
     */
    public void setCachedCatalogData(Long catalogId, SearchType searchType, CatalogData data);
}
