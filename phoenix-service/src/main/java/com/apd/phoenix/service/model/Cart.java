package com.apd.phoenix.service.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

@Entity
public class Cart implements Serializable, com.apd.phoenix.service.model.Entity {

    //the number of hours that a cart will be saved
    private static final int CART_INACTIVITY_PERIOD = 24;
    private static final long serialVersionUID = -249975824408702425L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id = null;
    @Version
    @Column(name = "version", nullable = false)
    private int version = 0;

    @Temporal(TemporalType.TIMESTAMP)
    private Date expirationDate;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @Fetch(FetchMode.JOIN)
    @JoinColumn(name = "cart_id")
    private Set<CartItem> cartItems = new HashSet<CartItem>();

    @ManyToOne(fetch = FetchType.LAZY)
    private AccountXCredentialXUser userCredential;

    @Column
    private BigDecimal estimatedShippingAmount;

    @Column
    private BigDecimal maximumTaxToCharge;

    public Long getId() {
        return this.id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public int getVersion() {
        return this.version;
    }

    public void setVersion(final int version) {
        this.version = version;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        if (id != null) {
            return id.equals(((Cart) that).id);
        }
        return super.equals(that);
    }

    @Override
    public int hashCode() {
        if (id != null) {
            return id.hashCode();
        }
        return super.hashCode();
    }

    public Date getExpirationDate() {
        return this.expirationDate;
    }

    public void setExpirationDate(final Date expirationDate) {
        this.expirationDate = expirationDate;
    }

    public Set<CartItem> getCartItems() {
        return this.cartItems;
    }

    public void setCartItems(final Set<CartItem> cartItems) {
        this.cartItems = cartItems;
    }

    public AccountXCredentialXUser getUserCredential() {
        return this.userCredential;
    }

    public void setUserCredential(final AccountXCredentialXUser userCredential) {
        this.userCredential = userCredential;
    }

    public BigDecimal getEstimatedShippingAmount() {
        return estimatedShippingAmount;
    }

    public void setEstimatedShippingAmount(BigDecimal estimatedShippingAmount) {
        this.estimatedShippingAmount = estimatedShippingAmount;
    }

    public BigDecimal getMaximumTaxToCharge() {
        return maximumTaxToCharge;
    }

    public void setMaximumTaxToCharge(BigDecimal maximumTaxToCharge) {
        this.maximumTaxToCharge = maximumTaxToCharge;
    }

    /**
     * This helper method sets the expiration date of the Cart to one day from when the method is called.
     */
    public void resetExpirationDate() {
        //generates an expiration date for the day after the scheduling takes place
        Calendar c = Calendar.getInstance();
        c.setTime(new Date());
        c.add(Calendar.HOUR_OF_DAY, CART_INACTIVITY_PERIOD);
        //sets the expiration date to the day after
        this.setExpirationDate(c.getTime());
    }
}