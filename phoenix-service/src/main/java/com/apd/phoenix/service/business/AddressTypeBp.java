package com.apd.phoenix.service.business;

import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import com.apd.phoenix.service.model.AddressType;
import com.apd.phoenix.service.persistence.jpa.AddressTypeDao;

/**
 * This class provides business process methods for CustomeCost.
 * 
 * @author RHC
 * 
 */
@Stateless
@LocalBean
public class AddressTypeBp extends AbstractBp<AddressType> {

    public static final String SHIPTO_ADDRESS_TYPE = "SHIPTO";

    /**
     * This method is injected with the correct Dao for initialization
     * 
     * @param dao
     */
    @Inject
    public void initDao(AddressTypeDao dao) {
        this.dao = dao;
    }

    public AddressType getByName(String name) {
        AddressType searchType = new AddressType();
        searchType.setName(name);
        List<AddressType> searchList = this.searchByExactExample(searchType, 0, 0);
        if (searchList.isEmpty()) {
            return null;
        }
        return searchList.get(0);
    }
}
