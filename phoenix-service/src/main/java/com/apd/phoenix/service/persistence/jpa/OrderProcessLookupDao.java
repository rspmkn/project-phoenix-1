package com.apd.phoenix.service.persistence.jpa;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import com.apd.phoenix.service.model.OrderProcessLookup;

/**
 * OrderProcessLookup DAO stub
 * 
 * @author RHC
 *
 */
@Stateless
@LocalBean
public class OrderProcessLookupDao extends AbstractDao<OrderProcessLookup> {
}
