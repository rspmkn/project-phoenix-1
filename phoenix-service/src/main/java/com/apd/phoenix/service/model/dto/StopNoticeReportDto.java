package com.apd.phoenix.service.model.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class StopNoticeReportDto implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private Date runTime;

    List<StopNotificationDto> stopNotificationDtos;

    public List<StopNotificationDto> getStopNotificationDtos() {
        return stopNotificationDtos;
    }

    public void setStopNotificationDtos(List<StopNotificationDto> stopNotificationDtos) {
        this.stopNotificationDtos = stopNotificationDtos;
    }

    public Date getRunTime() {
        return runTime;
    }

    public void setRunTime(Date runTime) {
        this.runTime = runTime;
    }

}
