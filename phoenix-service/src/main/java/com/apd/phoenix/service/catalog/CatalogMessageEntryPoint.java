package com.apd.phoenix.service.catalog;

import java.util.ArrayList;
import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.MessageProducer;
import javax.jms.ObjectMessage;
import javax.jms.Queue;
import javax.jms.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.persistence.multitenancy.impl.CurrentTenantIdentifierResolverImpl;

@Stateless
public class CatalogMessageEntryPoint {

    private static final Logger LOGGER = LoggerFactory.getLogger(CatalogMessageEntryPoint.class);

    @Resource(mappedName = "java:/activemq/ConnectionFactory")
    private ConnectionFactory connectionFactory;

    @Resource(mappedName = "java:/activemq/catalog-unprocessed")
    private Queue processQueue;

    @Resource(mappedName = "java:/activemq/catalog-check")
    private Queue checkQueue;

    public void scheduleRequest(ArrayList<CatalogChange> changes) {
    	if (changes == null || changes.isEmpty()) {
    		return;
    	}
    	ArrayList<CatalogChange> toProcess = new ArrayList<>();
    	ArrayList<CatalogChange> toCheck = new ArrayList<>();
    	for (CatalogChange change : changes) {
    		if (change.getPersistChanges() == null || change.getPersistChanges()) {
    			toProcess.add(change);
    		} else {
    			toCheck.add(change);
    		}
    	}
    	if (toProcess != null && !toProcess.isEmpty()) {
    		this.scheduleRequestForQueue(toProcess, processQueue);
    	}
    	if (toCheck != null && !toCheck.isEmpty()) {
    		this.scheduleRequestForQueue(toCheck, checkQueue);
    	}
    }

    private void scheduleRequestForQueue(ArrayList<CatalogChange> changes, Queue queue) {
        Connection connection = null;
        try {
            connection = connectionFactory.createConnection();
            Session msession = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            MessageProducer messageProducer = msession.createProducer(queue);
            ObjectMessage message = msession.createObjectMessage();
            message.setObject(changes);
            message.setLongProperty("tenantId", CurrentTenantIdentifierResolverImpl.getCurrentTenant());
            messageProducer.send(message);
        }
        catch (Exception e) {
            LOGGER.error("An error occured:", e);
            ;
        }
        finally {
            try {
                connection.close();
            }
            catch (Exception e) {
                LOGGER.error("Could not close connection", e);
            }
        }
    }

}
