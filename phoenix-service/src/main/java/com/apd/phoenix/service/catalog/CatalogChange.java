package com.apd.phoenix.service.catalog;

import java.io.Serializable;
import java.util.Map;
import com.apd.phoenix.service.business.CatalogBp.SyncAction;

public class CatalogChange implements Serializable {

    private static final long serialVersionUID = -4095490910018425343L;
    private Map<String, String> itemData;
    private SyncAction action;
    private Long catalogId;
    private Long oldCatalogId;
    private Boolean persistChanges;
    private boolean forCustomerCatalog;
    private String correlationId;
    private Long totalItems;
    private Map<String, Long> propertyTypes;
    private Map<String, String> pricingTypes;

    public CatalogChange(Map<String, String> itemData, SyncAction action, Long catalogId, Long oldCatalogId,
            Boolean persistChanges, String correlationId, Long totalItems, boolean forCustomerCatalog,
            Map<String, Long> propertyTypes, Map<String, String> pricingTypes) {
        super();
        this.itemData = itemData;
        this.action = action;
        this.catalogId = catalogId;
        this.oldCatalogId = oldCatalogId;
        this.persistChanges = persistChanges;
        this.correlationId = correlationId;
        this.totalItems = totalItems;
        this.forCustomerCatalog = forCustomerCatalog;
        this.propertyTypes = propertyTypes;
        this.pricingTypes = pricingTypes;
    }

    public CatalogChange(Map<String, String> itemData, SyncAction action, Long catalogId, Boolean persistChanges,
            String correlationId, Long totalItems, boolean forCustomerCatalog, Map<String, Long> propertyTypes,
            Map<String, String> pricingTypes) {
        super();
        this.itemData = itemData;
        this.action = action;
        this.catalogId = catalogId;
        this.persistChanges = persistChanges;
        this.correlationId = correlationId;
        this.totalItems = totalItems;
        this.forCustomerCatalog = forCustomerCatalog;
        this.propertyTypes = propertyTypes;
        this.pricingTypes = pricingTypes;
    }

    public CatalogChange() {
        super();
    }

    public Long getOldCatalogId() {
        return oldCatalogId;
    }

    public void setOldCatalogId(Long oldCatalogId) {
        this.oldCatalogId = oldCatalogId;
    }

    public Map<String, String> getItemData() {
        return itemData;
    }

    public void setItemData(Map<String, String> itemData) {
        this.itemData = itemData;
    }

    public SyncAction getAction() {
        return action;
    }

    public void setAction(SyncAction action) {
        this.action = action;
    }

    public Long getCatalogId() {
        return catalogId;
    }

    public void setCatalogId(Long catalogId) {
        this.catalogId = catalogId;
    }

    public Boolean getPersistChanges() {
        return persistChanges;
    }

    public void setPersistChanges(Boolean persistChanges) {
        this.persistChanges = persistChanges;
    }

    public String getCorrelationId() {
        return correlationId;
    }

    public void setCorrelationId(String correlationId) {
        this.correlationId = correlationId;
    }

    public Long getTotalItems() {
        return totalItems;
    }

    public void setTotalItems(Long totalItems) {
        this.totalItems = totalItems;
    }

    public boolean isForCustomerCatalog() {
        return forCustomerCatalog;
    }

    public void setForCustomerCatalog(boolean forCustomerCatalog) {
        this.forCustomerCatalog = forCustomerCatalog;
    }

    public Map<String, Long> getPropertyTypes() {
        return propertyTypes;
    }

    public void setPropertyTypes(Map<String, Long> propertyTypes) {
        this.propertyTypes = propertyTypes;
    }

    public Map<String, String> getPricingTypes() {
        return pricingTypes;
    }

    public void setPricingTypes(Map<String, String> pricingTypes) {
        this.pricingTypes = pricingTypes;
    }
}
