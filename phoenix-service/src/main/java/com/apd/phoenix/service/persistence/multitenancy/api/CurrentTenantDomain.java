package com.apd.phoenix.service.persistence.multitenancy.api;

public interface CurrentTenantDomain {

    public String getDomain();

    public void setDomain(String domain);

}
