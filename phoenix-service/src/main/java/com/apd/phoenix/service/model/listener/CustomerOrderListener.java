package com.apd.phoenix.service.model.listener;

import javax.persistence.PrePersist;
import com.apd.phoenix.service.model.CustomerOrder;

/**
 * Recalculates line numbers for all items on the order. 
 * 
 * @author RHC
 */
public class CustomerOrderListener {

    @PrePersist
    public void preUpdateEventListener(CustomerOrder entity) {
        entity.calculateLineNumbers();
    }
}
