package com.apd.phoenix.service.product;

import java.util.List;
import com.apd.phoenix.service.model.CatalogXItem;

/**
 * Interface for Product cache management
 * 
 * @author RHC
 *
 */
public interface ProductCacheManager {

    /**
     * Takes a CatalogXItem and returns the Product that matches. This checks the cache first. If it is not in the 
     * cache, the cache is updated. Note that the CatalogXItem entity should be managed, or you might get Lazy Init 
     * exceptions.
     * 
     * @param item - The item whose Product should be returned
     * @return The Product in the cache
     */
    public Product getProduct(CatalogXItem item);

    /**
     * Takes an APD SKU, Vendor name, and catalog ID, and returns the Product that matches. This checks the cache first; 
     * if it is not in the cache, the CatalogXItem is pulled from the database, and the cache is updated.
     * 
     * @param apdSku
     * @param vendorName
     * @param customerCatalogId
     * @return The specified CatalogXItem
     * @throws ProductNotFoundException If no Product exists with the specified APD SKU, vendor name, and customer catalog ID
     */
    public Product getProduct(String apdSku, String vendorName, List<Long> customerCatalogId)
            throws ProductNotFoundException;

    /**
     * Takes an APD SKU, Vendor name, and catalog ID, and returns the CatalogXItem that matches. This checks the cache first; 
     * if it is not in the cache, the item is pulled from the database, and the cache is updated.
     * 
     * @param apdSku
     * @param vendorName
     * @param customerCatalogId
     * @return The specified CatalogXItem
     * @throws ProductNotFoundException If no item exists with the specified APD SKU, vendor name, and customer catalog ID
     */
    public CatalogXItem getCatalogXItem(String apdSku, String vendorName, Long customerCatalogId)
            throws ProductNotFoundException;

    /**
     * Refreshes the Product in the cache for a CatalogXItem. Called when the item is updated. Note that the CatalogXItem 
     * entity should be managed, or you might get Lazy Init exceptions.
     * 
     * @param item - The item to update
     * @return The new Product that was inserted into the cache.
     */
    public Product updateCatalogXItem(CatalogXItem item);
}
