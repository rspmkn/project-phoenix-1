package com.apd.phoenix.service.executor.api;

public interface BRMSCommand {

    public ExecutionResults execute(CommandContext ctx) throws Exception;

    public void setup();

    public void tearDown();
}
