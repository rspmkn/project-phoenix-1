/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.service.business;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.HashSet;
import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import com.apd.phoenix.service.model.CustomerOrder;
import com.apd.phoenix.service.model.LineItemXShipment;
import com.apd.phoenix.service.model.Shipment;
import com.apd.phoenix.service.model.dto.LineItemXShipmentDto;
import com.apd.phoenix.service.model.dto.ShipmentDto;
import com.apd.phoenix.service.model.dto.error.RelevantNumberDto;
import com.apd.phoenix.service.persistence.jpa.ShipmentDao;
import org.apache.commons.lang.StringUtils;

/**
 *
 * @author nreidelb
 */
@Stateless
@LocalBean
public class ShipmentBp extends AbstractBp<Shipment> {

    public static final String PURCHASE__ORDER__NUMBER = "Purchase Order Number";
    public static final String SHIPMENT__ID = "Shipment Id";

    @Inject
    LineItemXShipmentBp lineItemXShipmentBp;

    @Inject
    LineItemBp lineItemBp;

    @Inject
    LineItemStatusBp lineItemStatusBp;

    @Inject
    CustomerOrderBp customerOrderBp;

    @Inject
    AddressBp addressBp;

    /**
     * This method is injected with the correct Dao for initialization
     * 
     * @param dao
     */
    @Inject
    public void initDao(ShipmentDao dao) {
        this.dao = dao;
    }

    public Shipment persistShipment(ShipmentDto shipmentDto) {

        boolean shouldCreateShipment = false;

        for (LineItemXShipmentDto lixsd : shipmentDto.getLineItemXShipments()) {
            if (lixsd.getValid() == true || lixsd.getQuantity().compareTo(BigInteger.ZERO) > 0) {
                shouldCreateShipment = true;
                break;
            }
        }

        if (!shouldCreateShipment) {
            return null;
        }

        CustomerOrder customerOrder = customerOrderBp.searchByApdPo(shipmentDto.getCustomerOrderDto()
                .retrieveApdPoNumber().getValue());

        Shipment shipment = this.create(new Shipment());
        shipment.setOrder(customerOrder);

        shipment.setItemXShipments(new HashSet<LineItemXShipment>());
        for (LineItemXShipmentDto lineItemXShipmentDto : shipmentDto.getLineItemXShipments()) {
            if (lineItemXShipmentDto.getValid()) {
                shipment.getItemXShipments().add(
                        lineItemXShipmentBp.persistLineItemXShipment(lineItemXShipmentDto, shipment));
            }
        }

        if (shipment.getItemXShipments().isEmpty()) {
            return null;
        }

        shipment.setDeliveredTime(shipmentDto.getShipTime());
        shipment.setTrackingNumber(shipmentDto.getTrackingNumber());

        shipment.setSubTotalPrice(BigDecimal.ZERO);
        shipment.setTotalFreightPrice(BigDecimal.ZERO);
        shipment.setTotalTaxPrice(BigDecimal.ZERO);
        for (LineItemXShipment lineItemXShipment : shipment.getItemXShipments()) {
            if (!lineItemXShipment.getPaid()) {
                shipment.setSubTotalPrice(shipment.getSubTotalPrice().add(lineItemXShipment.getSubTotalAmount()));
                shipment.setTotalFreightPrice(shipment.getTotalFreightPrice().add(lineItemXShipment.getShipping()));
                shipment.setTotalTaxPrice(shipment.getTotalTaxPrice().add(lineItemXShipment.getTotalTaxAmount()));
            }
        }
        shipment.setTotalShippingPrice(shipment.getSubTotalPrice().add(
                shipment.getTotalFreightPrice().add(shipment.getTotalTaxPrice())));
        if (customerOrder.getCredential().getDefaultShipFromAddress() != null && shipmentDto.getFromAddress() == null) {
            shipment.setShipFromAddress(addressBp
                    .cloneEntity(customerOrder.getCredential().getDefaultShipFromAddress()));
        }
        else if (shipmentDto.getFromAddress() != null) {
            shipment.setShipFromAddress(addressBp.create(shipmentDto.getFromAddress()));
        }

        shipment = this.update(shipment);

        return shipment;
    }

    public CustomerOrder getOrderFromShipmentDto(ShipmentDto shipmentDto) {
        if (shipmentDto != null && shipmentDto.getCustomerOrderDto() != null
                && shipmentDto.getCustomerOrderDto().retrieveApdPoNumber() != null
                && StringUtils.isNotBlank(shipmentDto.getCustomerOrderDto().retrieveApdPoNumber().getValue())) {
            return customerOrderBp.searchByApdPo(shipmentDto.getCustomerOrderDto().retrieveApdPoNumber().getValue());
        }
        return null;
    }

    public Shipment retrieveShipment(ShipmentDto shipmentDto) {
        if (shipmentDto == null || shipmentDto.getShipmentDatabaseKey() == null) {
            LOG.error("Null shipmentDto or database ID given when trying to retrieve shipment");
            return null;
        }
        return this.findById(shipmentDto.getShipmentDatabaseKey(), Shipment.class);
    }

    public Shipment searchShipmentByTrackingNumber(String value) {
        return ((ShipmentDao) dao).searchShipmentByTrackingNumber(value);
    }

    public CustomerOrder findCustomerOrderBy824RelevantNumber(CustomerOrder customerOrder, RelevantNumberDto number) {
        switch (number.getType()) {
            case PURCHASE__ORDER__NUMBER:
                customerOrder = customerOrderBp.searchByApdPo(number.getValue());
                break;
            case SHIPMENT__ID:
                Shipment shipment = searchShipmentByTrackingNumber(number.getValue());
                customerOrder = shipment.getOrder();
                break;
        }
        return customerOrder;
    }

}
