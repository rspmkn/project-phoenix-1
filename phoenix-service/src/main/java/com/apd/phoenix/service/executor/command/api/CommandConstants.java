package com.apd.phoenix.service.executor.command.api;

public interface CommandConstants {

    /* Types */
    public static final String ORDER_TYPE = "CustomerOrder";
    public static final String ACCOUNT_TYPE = "Account";
    public static final String USER_REQUEST_TYPE = "UserRequest";

    /* Callbacks */
    public static final String HELLOWORLD_CALLBACK = "HelloWorldCallback";

    /* Bean Names */
    public static final String START_PROCESS_COMMAND_BEAN = "StartProcessCommand";
    public static final String SIGNAL_EVENT_COMMAND_BEAN = "SignalEventCommand";
    public static final String COMPLETE_TASK_COMMAND_BEAN = "CompleteTaskCommand";
    public static final String COMPLETE_WI_COMMAND_BEAN = "CompleteWICommand";
}
