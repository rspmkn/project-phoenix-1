package com.apd.phoenix.service.executor.command.api;

public class StartProcessCommand extends Command {

    public enum Name {
        STANDARD_ORDER(StartProcessCommandConstants.STANDARD_ORDER_PROCCESS_ID), SIGNAL_TEST(
                StartProcessCommandConstants.SIGNAL_TEST_PROCESS_ID), INLINE_SIGNAL_TEST(
                StartProcessCommandConstants.INLINE_SIGNAL_TEST), VENDOR_SHIPMENT_MANIFEST_RETRIEVAL(
                StartProcessCommandConstants.VENDOR_SHIPMENT_MANIFEST_RETRIEVAL), ORDER_REJECTED_SHIPTO(
                StartProcessCommandConstants.ORDER_REJECTED_SHIPTO_PROCESS_ID), USER_CREATION(
                StartProcessCommandConstants.USER_CREATION_PROCESS_ID), ITEM_REQUEST(
                StartProcessCommandConstants.ITEM_REQUEST_PROCESS_ID), USER_MODIFICATION(
                StartProcessCommandConstants.USER_MODIFICATION_PROCESS_ID), RETURN_ORDER(
                StartProcessCommandConstants.RETURN_ORDER_PROCESS_ID), CREDIT_INVOICE(
                StartProcessCommandConstants.CREDIT_INVOICE_PROCESS_ID), OLD_CART(
                StartProcessCommandConstants.OLD_CART_ID), ORDER_STUCK(StartProcessCommandConstants.ORDER_STUCK_ID);

        private String name;

        private Name(String name) {
            this.name = name;
        }

        @Override
        public String toString() {
            return name;
        }
    }
}
