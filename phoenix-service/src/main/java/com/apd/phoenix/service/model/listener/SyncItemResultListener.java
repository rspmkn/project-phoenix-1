package com.apd.phoenix.service.model.listener;

import java.util.Set;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import org.apache.commons.lang.StringUtils;
import com.apd.phoenix.service.model.SyncItemResult;

/**
 * If the item is persisted in a different transaction from the order (i.e. the recalulateLineNumberOnPersist 
 * flag is true) then line numbers are recalculated on the order for all transient items (including this one).
 * 
 * @author RHC
 */
public class SyncItemResultListener {

    private static final int MAX_FIELD_LENGTH = 3000;

    @PrePersist
    @PreUpdate
    public void preUpdateEventListener(SyncItemResult entity) {
        entity.setErrors(getFieldValue(entity.getErrors(), entity.getErrorSet()));
        entity.setWarnings(getFieldValue(entity.getWarnings(), entity.getWarningSet()));
    }

    private String getFieldValue(String originalValue, Set<String> values) {
        if (StringUtils.isNotBlank(originalValue) || values == null || values.isEmpty()) {
            return originalValue;
        }
        StringBuilder result = new StringBuilder();
        for (String value : values) {
            if (result.length() != 0) {
                result.append("; ");
            }
            result.append(value);
        }
        if (result.length() > MAX_FIELD_LENGTH) {
            return result.substring(0, MAX_FIELD_LENGTH);
        }
        else {
            return result.toString();
        }
    }
}
