package com.apd.phoenix.service.email.api;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.util.Map;
import java.util.Properties;
import javax.inject.Inject;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.core.utility.PropertiesLoader;
import com.apd.phoenix.core.utility.TenantConfigRepository;
import com.apd.phoenix.service.business.EmailFactoryBp;
import com.apd.phoenix.service.business.SingletonPropertiesLoader;
import com.apd.phoenix.service.model.dto.AddressDto;
import com.apd.phoenix.service.persistence.multitenancy.impl.CurrentTenantIdentifierResolverImpl;
import com.apd.phoenix.service.report.ReportService;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;

public abstract class EmailTemplate<C> {

    @Inject
    protected ReportService reportService;

    private static final Logger logger = LoggerFactory.getLogger(EmailTemplate.class);

    protected static final String directory = "email/";

    protected static final String PDF_EXT = ".pdf";

    protected static final String LOGO = "logo";
    protected static final String FACEBOOK = "facebook";
    protected static final String GOOGLE_PLUS = "google.plus";
    protected static final String LINKEDIN = "linkedin";
    protected static final String WORDPRESS = "wordpress";
    protected static final String YOUTUBE = "youtube";
    protected static final String REMIT_TENANT_NAME = "csr.address.name";
    protected static final String REMIT_TENANT_STREET = "csr.address.street";
    protected static final String REMIT_TENANT_CITY = "csr.address.city";
    protected static final String REMIT_TENANT_STATE = "csr.address.state";
    protected static final String REMIT_TENANT_ZIP = "csr.address.zip";
    protected static final String REMIT_MARQUETTE_NAME = "remit.marquette.name";
    protected static final String REMIT_MARQUETTE_STREET = "remit.marquette.street";
    protected static final String REMIT_MARQUETTE_CITY_STATE = "remit.marquette.city.state";
    protected static final String TENANT_CUSTOMER_SERVICE_EMAIL = "csr.email";
    protected static final String TENANT_CUSTOMER_SERVICE_PHONE = "customer_service_phone_number";
    protected static final String AWS = "aws.integration";
    protected static final String DOMAIN = "content.service.secure.tenant.domain";
    protected static final String GENERIC = "generic";
    protected static final String MARQUETTE_PROPERTIES = "marquette.integration";

    public abstract String createBody(C content) throws IOException, TemplateException;

    public abstract Attachment createAttachment(C content) throws NoAttachmentContentException;

    public String getProperty(String key) {
        Properties templateProperties = PropertiesLoader.getAsProperties(EmailFactoryBp.class, this
                .getTemplateLocation());
        String tenant = SingletonPropertiesLoader.getTenant();
        String tenantKey = tenant + "." + key;
        if (templateProperties.containsKey(tenantKey)) {
            return templateProperties.getProperty(tenantKey);
        }
        return templateProperties.getProperty(key);
    }

    public String getFromAddress() {
        return getTenantProperties().getProperty("internal.emails.noreply");
    }

    protected abstract String getTemplate();

    protected String getTemplateLocation() {
        return directory + getTemplate();
    }

    protected String create(Map<String, Object> params) throws IOException, TemplateException {
        params.put("tenantLongName", getProperty("tenant", "csr.address.name"));
        params.put("tenantName", getTenantName());
        params.put("tenantCode", getProperty("tenant", "code"));

        String templateFile = getTemplate() + ".ftl";

        Configuration configuration = new Configuration();
        configuration.setClassForTemplateLoading(EmailTemplate.class, "/");
        Template freemarkerTemplate = configuration.getTemplate("/" + directory + templateFile);
        Writer writer = new StringWriter();
        freemarkerTemplate.process(params, writer);

        return writer.toString();
    }

    protected AddressDto getSafeAddress(AddressDto addressDto) {
        AddressDto toReturn;
        if (addressDto == null) {
            toReturn = new AddressDto();
        }
        else {
            toReturn = addressDto;
        }
        if (toReturn.getCity() == null) {
            toReturn.setCity("");
        }
        if (toReturn.getLine1() == null) {
            toReturn.setLine1("None");
        }
        if (toReturn.getState() == null) {
            toReturn.setState("");
        }
        if (toReturn.getZip() == null) {
            toReturn.setZip("");
        }
        return toReturn;
    }

    protected String getAddressFieldValue(String fieldLabelAndValue) {
        if (StringUtils.isBlank(fieldLabelAndValue)) {
            return null;
        }
        if (fieldLabelAndValue.contains("|")) {
            return fieldLabelAndValue.substring(fieldLabelAndValue.indexOf("|") + 1);
        }
        return fieldLabelAndValue;
    }

    protected String getAddressFieldLabel(String fieldLabelAndValue) {
        if (StringUtils.isBlank(fieldLabelAndValue)) {
            return null;
        }
        if (fieldLabelAndValue.contains("|")) {
            return fieldLabelAndValue.substring(0, fieldLabelAndValue.indexOf("|"));
        }
        return "";
    }

    protected Properties getTenantProperties() {
        return TenantConfigRepository.getInstance().getPropertiesByTenantId(
                CurrentTenantIdentifierResolverImpl.getCurrentTenant(), "tenant");
    }

    protected String getTenantPropertyByName(String propertyName) {
        return getTenantProperties().getProperty(propertyName);
    }

    private String getTenantName() {
        return TenantConfigRepository.getInstance().getTenantNameById(
                CurrentTenantIdentifierResolverImpl.getCurrentTenant());
    }

    protected String getProperty(String propFile, String property) {
        return TenantConfigRepository.getInstance().getProperty(getTenantName(), propFile, property);
    }

    protected String getProperty(String propFile, String property, String defaultValue) {
        return TenantConfigRepository.getInstance().getProperty(getTenantName(), propFile, property, defaultValue);
    }

    protected String getMediaIconURL(String icon) {
        if (LOGO.equals(icon)) {
            return this.getTenantPropertyByName(DOMAIN) + this.getTenantPropertyByName(LOGO);
        }
        return this.getTenantPropertyByName(DOMAIN) + this.getProperty(GENERIC, icon);
    }
}
