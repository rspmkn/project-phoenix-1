package com.apd.phoenix.service.rest;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

public abstract class BaseRestEndpoint {

    @Path("/ping")
    @GET
    public Response ping(Object object) {
        String response = this.getServiceName() + ": Service is available for use.";
        return Response.ok(response).build();
    }

    public String getServiceName() {
        return this.getClass().getSimpleName();
    }
}
