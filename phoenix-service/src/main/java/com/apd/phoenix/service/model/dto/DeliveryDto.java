package com.apd.phoenix.service.model.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class DeliveryDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String id;
	private String deliveryPo;
	private Type type;
	private List<DeliveryLineDto> deliveryLineDtos = new ArrayList<>();
	

	public String getId() {
		return id;
	}



	public void setId(String id) {
		this.id = id;
	}



	public String getDeliveryPo() {
		return deliveryPo;
	}



	public void setDeliveryPo(String deliveryPo) {
		this.deliveryPo = deliveryPo;
	}



	public Type getType() {
		return type;
	}



	public void setType(Type type) {
		this.type = type;
	}



	public List<DeliveryLineDto> getDeliveryLineDtos() {
		return deliveryLineDtos;
	}



	public void setDeliveryLineDtos(List<DeliveryLineDto> deliveryLineDtos) {
		this.deliveryLineDtos = deliveryLineDtos;
	}



	public enum Type {
		DELIVERY("DELIVERY");
		
		private final String label;

        private Type(String label) {
            this.label = label;
        }

        public String getLabel() {
            return this.label;
        }
	}
}
