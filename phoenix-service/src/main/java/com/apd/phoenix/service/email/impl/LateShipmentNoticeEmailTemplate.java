package com.apd.phoenix.service.email.impl;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import com.apd.phoenix.core.utility.PropertiesLoader;
import com.apd.phoenix.service.email.api.Attachment;
import com.apd.phoenix.service.email.api.EmailTemplate;
import com.apd.phoenix.service.email.api.NoAttachmentContentException;
import com.apd.phoenix.service.model.Address;
import com.apd.phoenix.service.model.CustomerOrder;
import com.apd.phoenix.service.model.LineItem;
import com.apd.phoenix.service.model.factory.DtoFactory;
import freemarker.template.TemplateException;
import org.apache.commons.lang.StringUtils;

/**
 *
 * @author anicholson
 */

@Stateless
@LocalBean
public class LateShipmentNoticeEmailTemplate extends EmailTemplate<CustomerOrder> {

    private static final String TEMPLATE = "late.shipment.notice";
    private static final String ATTACHMENT_PREFIX = "late-shipment-notice-";

    @Override
    public String createBody(CustomerOrder order) throws IOException, TemplateException {
        Map<String, Object> params = new HashMap<>();      
        Address shipmentAddress = order.getAddress();
        Address billingAddress = order.getPaymentInformation().getBillingAddress();
		
		if (order.getCustomerPo() != null) {
			params.put("customerPo", order.getCustomerPo().getValue());
		}
		else
		{
			params.put("customerPo", "N/A");
		}
		
		params.put("apdPo", order.getApdPo().getValue());
		params.put("billingAddress", this.getSafeAddress(DtoFactory.createAddressDto(billingAddress)));
		params.put("shippingAddress", this.getSafeAddress(DtoFactory.createAddressDto(shipmentAddress)));
		
		Set<LineItem> validItems = order.getItems();		
		params.put("items", validItems);
		params.put("orderDate", order.getOrderDate());
		params.put("csPhone", order.getCredential().getCsrPhone());
		
		
		/*Check to see if the customer order contains a desktop and department with the ship to address. If so, we need show those values in
		 * the email.*/
        String desktopString = null;
        String departmentString = null;
        String costCenter = null;
        String phoneNumber = null;
        if(order.getAddress() != null && order.getAddress().getMiscShipTo() != null)
        {
            if(StringUtils.isNotEmpty(order.getAddress().getMiscShipTo().getDesktop()))
            {
                desktopString =  order.getAddress().getMiscShipTo().getDesktop();
            }

            if(StringUtils.isNotEmpty(order.getAddress().getMiscShipTo().getDepartment()))
            {
                departmentString = order.getAddress().getMiscShipTo().getDepartment();
            }

            if(order.getAssignedCostCenter() != null && order.getAssignedCostCenter().getCostCenter() != null)
            {
            	costCenter = order.getAssignedCostCenter().getCostCenter().getName();
            }

            if(StringUtils.isNotEmpty(order.getAddress().getMiscShipTo().getRequesterPhone()))
            {
                phoneNumber = order.getAddress().getMiscShipTo().getRequesterPhone();
            }
        }

        params.put("desktop", desktopString);
        params.put("building", departmentString);
        params.put("costcenter", costCenter);
        params.put("phone", phoneNumber);
		
		//Logos
        params.put("apdLogo", this.getMediaIconURL(LOGO));
        params.put("facebookLogo", this.getMediaIconURL(FACEBOOK));
        params.put("googlePlusLogo", this.getMediaIconURL(GOOGLE_PLUS));
        params.put("linkedInLogo", this.getMediaIconURL(LINKEDIN));
        params.put("wordpressLogo", this.getMediaIconURL(WORDPRESS));
        params.put("youTubeLogo", this.getMediaIconURL(YOUTUBE));
        
        return this.create(params);
    }

    @Override
    public String getTemplate() {
        return TEMPLATE;
    }

    @Override
    public Attachment createAttachment(CustomerOrder order) throws NoAttachmentContentException {
        Attachment lateShipNotice = new Attachment();
        String apdPo = order.getApdPo().getValue();
        lateShipNotice.setFileName(ATTACHMENT_PREFIX + apdPo + PDF_EXT);
        lateShipNotice.setMimeType(Attachment.MimeType.pdf);
        InputStream is = reportService.generateLateShipmentNoticePdf(apdPo);
        if (is != null) {
            lateShipNotice.setContent(is);
        }
        else {
            throw new NoAttachmentContentException();
        }
        return lateShipNotice;
    }
}
