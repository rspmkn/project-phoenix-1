package com.apd.phoenix.service.message.impl;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.message.api.Message;
import com.apd.phoenix.service.message.api.MessageSender;
import com.apd.phoenix.service.model.OrderLog.EventType;

@Stateless
@LocalBean
public class OCIMessageSender extends MessageSender {

    private static final Logger LOGGER = LoggerFactory.getLogger(OCIMessageSender.class);

    @Override
    public boolean sendMessage(Message message, EventType eventType) {
        LOGGER.info("Sending OCI Message");
        logMessage(message, LOGGER);
        return true;
    }

}
