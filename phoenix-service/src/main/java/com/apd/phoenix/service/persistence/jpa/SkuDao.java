package com.apd.phoenix.service.persistence.jpa;

import java.io.File;
import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.Query;
import org.apache.commons.lang.StringUtils;
import com.apd.phoenix.service.model.Sku;
import com.apd.phoenix.service.utility.CsvExportService;

/**
 * Sku DAO stub
 * 
 * @author RHC
 *
 */
@Stateless
@LocalBean
public class SkuDao extends AbstractDao<Sku> {

    @Inject
    private CsvExportService resultsCsv;

    @SuppressWarnings("unchecked")
    public List<Sku> skuList(String catalogName, String skuType, String skuValue, String keyword, int start,
            int quantity) {
        String hql = "SELECT s FROM Sku AS s JOIN FETCH s.item item JOIN FETCH item.vendorCatalog "
                + getQueryConditions("s");
        //Creates the query
        Query query = entityManager.createQuery(hql);
        setParameters(query, catalogName, skuType, skuValue, keyword);
        setItemsReturned(query, start, quantity);
        return query.getResultList();
    }

    @SuppressWarnings("unchecked")
    public File skuCsv(String[] columns, String catalogName, String skuType, String skuValue, String keyword) {
        String hql = "SELECT " + getValuesToReturn("s") + " FROM Sku AS s " + getQueryConditions("s");
        //Creates the query
        Query query = entityManager.createQuery(hql);
        setParameters(query, catalogName, skuType, skuValue, keyword);
        return resultsCsv.getFile(columns, query.getResultList());
    }

    public int searchCount(String catalogName, String skuType, String skuValue, String keyword) {
        String hql = "SELECT count(s) FROM Sku AS s " + getQueryConditions("s");
        //Creates the query
        Query query = entityManager.createQuery(hql);
        setParameters(query, catalogName, skuType, skuValue, keyword);
        return Integer.valueOf(query.getSingleResult().toString());
    }

    private static void setParameters(Query query, String catalogName, String skuType, String skuValue, String keyword) {
        query.setParameter("catalogName", "%" + catalogName.toLowerCase() + "%");
        query.setParameter("skuType", "%" + skuType.toLowerCase() + "%");
        query.setParameter("skuValue", StringUtils.defaultIfBlank(skuValue, "%"));
        query.setParameter("keyword", "%" + keyword.toLowerCase() + "%");
    }

    private static String getQueryConditions(String sku) {
        return "WHERE lower(" + sku + ".item.vendorCatalog.name) LIKE :catalogName AND lower(" + sku
                + ".type.name) LIKE :skuType AND " + sku + ".value LIKE :skuValue AND ( lower(" + sku
                + ".item.name) LIKE :keyword OR lower(" + sku + ".item.description) LIKE :keyword OR lower(" + sku
                + ".item.searchTerms) LIKE :keyword)";
    }

    private static String getValuesToReturn(String sku) {
        return sku + ".item.vendorCatalog.name, " + sku + ".type.name, " + sku + ".value, " + sku + ".item.name";
    }

    @SuppressWarnings("unchecked")
    public List<Object[]> customerSkuList(String catalogName, String customer, String customerCatalogName,
            List<Long> customerCatalogIds, String skuType, String skuValue, String customerSkuValue, String keyword,
            int start, int quantity) {
        String hql = "SELECT c, s FROM CatalogXItem c, Sku s JOIN FETCH c.item item JOIN FETCH item.vendorCatalog JOIN FETCH c.catalog AS custCat JOIN FETCH custCat.customer "
                + customerGetQueryConditions("c", "s");
        //Creates the query
        Query query = entityManager.createQuery(hql);
        customerSetParameters(query, catalogName, customer, customerCatalogName, customerCatalogIds, skuType, skuValue,
                customerSkuValue, keyword);
        setItemsReturned(query, start, quantity);
        return query.getResultList();
    }

    @SuppressWarnings("unchecked")
    public File customerSkuCsv(String[] columns, String catalogName, String customer, String customerCatalogName,
            List<Long> customerCatalogIds, String skuType, String skuValue, String customerSkuValue, String keyword) {
        String hql = "SELECT " + customerGetValuesToReturn("c", "s") + " FROM CatalogXItem c, Sku s "
                + customerGetQueryConditions("c", "s");
        //Creates the query
        Query query = entityManager.createQuery(hql);
        customerSetParameters(query, catalogName, customer, customerCatalogName, customerCatalogIds, skuType, skuValue,
                customerSkuValue, keyword);
        return resultsCsv.getFile(columns, query.getResultList());
    }

    public int customerSearchCount(String catalogName, String customer, String customerCatalogName,
            List<Long> customerCatalogIds, String skuType, String skuValue, String customerSkuValue, String keyword) {
        String hql = "SELECT count(*) FROM CatalogXItem c, Sku s " + customerGetQueryConditions("c", "s");
        //Creates the query
        Query query = entityManager.createQuery(hql);
        customerSetParameters(query, catalogName, customer, customerCatalogName, customerCatalogIds, skuType, skuValue,
                customerSkuValue, keyword);
        return Integer.valueOf(query.getSingleResult().toString());
    }

    private static void customerSetParameters(Query query, String catalogName, String customer,
            String customerCatalogName, List<Long> customerCatalogIds, String skuType, String skuValue,
            String customerSkuValue, String keyword) {
        query.setParameter("catalogName", "%" + catalogName.toLowerCase() + "%");
        query.setParameter("cust", "%" + customer.toLowerCase() + "%");
        query.setParameter("custCat", "%" + customerCatalogName.toLowerCase() + "%");
        if (customerCatalogIds.isEmpty()) {
            customerCatalogIds.add((long) 0);
            query.setParameter("custCatIdsEmpty", true);
        }
        else {
            query.setParameter("custCatIdsEmpty", false);
        }
        query.setParameter("custCatIds", customerCatalogIds);
        query.setParameter("skuType", "%" + skuType.toLowerCase() + "%");
        query.setParameter("skuValue", StringUtils.defaultIfBlank(skuValue, "%"));
        query.setParameter("customerSkuValue", StringUtils.defaultIfBlank(customerSkuValue, "%"));
        query.setParameter("keyword", "%" + keyword.toLowerCase() + "%");
    }

    private static String customerGetQueryConditions(String catalogXItem, String sku) {
        return "WHERE lower(" + catalogXItem + ".item.vendorCatalog.name) LIKE :catalogName AND lower(" + sku
                + ".type.name) LIKE :skuType AND " + sku + ".value LIKE :skuValue AND ( lower(" + catalogXItem
                + ".item.name) LIKE :keyword OR lower(" + catalogXItem + ".item.description) LIKE :keyword OR lower("
                + catalogXItem + ".item.searchTerms) LIKE :keyword) AND lower(" + catalogXItem
                + ".catalog.name) LIKE :custCat AND (:custCatIdsEmpty = true OR " + catalogXItem
                + ".catalog.id IN (:custCatIds)) AND lower(" + catalogXItem + ".catalog.customer.name) LIKE :cust AND "
                + sku + ".item.id = " + catalogXItem + ".item.id " + "AND " + catalogXItem
                + ".customerSkuString LIKE :customerSkuValue";
    }

    private static String customerGetValuesToReturn(String catalogXItem, String sku) {
        return catalogXItem + ".item.vendorCatalog.name, " + catalogXItem + ".catalog.customer.name, " + catalogXItem
                + ".catalog.name, " + sku + ".type.name, " + sku + ".value, " + catalogXItem + ".customerSkuString, "
                + catalogXItem + ".item.name," + catalogXItem + ".price";
    }
}
