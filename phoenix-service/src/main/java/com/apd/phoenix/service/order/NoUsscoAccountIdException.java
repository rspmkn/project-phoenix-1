package com.apd.phoenix.service.order;

import javax.ejb.ApplicationException;

@ApplicationException(rollback = true)
public class NoUsscoAccountIdException extends RuntimeException {

    /**
     * 
     */
    private static final long serialVersionUID = -1258026121152714235L;

}
