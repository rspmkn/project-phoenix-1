package com.apd.phoenix.service.persistence.multitenancy.impl;

import java.sql.Connection;
import java.sql.SQLException;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import org.hibernate.HibernateException;
import org.hibernate.service.jdbc.connections.spi.MultiTenantConnectionProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class AbstractMultiTenantConnectionProviderImpl implements MultiTenantConnectionProvider {

    private static Logger LOG = LoggerFactory.getLogger(AbstractMultiTenantConnectionProviderImpl.class);

    /**
     * 
     */
    private static final long serialVersionUID = 4398081696642804794L;

    @Override
    public boolean isUnwrappableAs(@SuppressWarnings("rawtypes") Class unwrapClass) {
        return false;
    }

    @Override
    public <T> T unwrap(Class<T> unwrapClass) {
        return null;
    }

    @Override
    public Connection getAnyConnection() throws SQLException {
        return getDataSource().getConnection();
    }

    @Override
    public Connection getConnection(String tenantSchemaName) throws SQLException {
        final Connection connection = getAnyConnection();
        try {
            LOG.debug("Switching to schema: " + tenantSchemaName);

            if (!tenantSchemaName.equalsIgnoreCase(connection.getCatalog())) {
                LOG.debug("Setting catalog on connection!");
                connection.setCatalog(tenantSchemaName.toUpperCase());
            }
        }
        catch (SQLException e) {
            throw new HibernateException("Could not alter JDBC connection to specified schema [" + tenantSchemaName
                    + "]", e);
        }

        return connection;
    }

    @Override
    public void releaseAnyConnection(Connection connection) throws SQLException {
        connection.close();

    }

    @Override
    public void releaseConnection(String tenantIdentifier, Connection connection) throws SQLException {
        connection.close();

    }

    @Override
    public boolean supportsAggressiveRelease() {
        return true;
    }

    private DataSource getDataSource() {
        try {
            return (DataSource) (new InitialContext()).lookup(getDataSourceJNDIName());
        }
        catch (NamingException e) {
            throw new HibernateException("Unable to find datasource in JNDI", e);
        }
    }

    protected abstract String getDataSourceJNDIName();

}