package com.apd.phoenix.service.email.impl;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.core.StringEscape;
import com.apd.phoenix.core.utility.PropertiesLoader;
import com.apd.phoenix.service.business.AccountXCredentialXUserBp;
import com.apd.phoenix.service.business.CustomerOrderBp;
import com.apd.phoenix.service.business.CustomerOrderRequestBp;
import com.apd.phoenix.service.email.api.Attachment;
import com.apd.phoenix.service.email.api.EmailTemplate;
import com.apd.phoenix.service.model.AccountXCredentialXUser;
import com.apd.phoenix.service.model.AssignedDepartment;
import com.apd.phoenix.service.model.CustomerOrder;
import com.apd.phoenix.service.model.PhoneNumber;
import com.apd.phoenix.service.model.PhoneNumberType;
import com.apd.phoenix.service.model.dto.PurchaseOrderDto;
import freemarker.template.TemplateException;

@Stateless
@LocalBean
public class OrderApprovalNeededEmailTemplate extends EmailTemplate<PurchaseOrderDto> {

    @Inject
    CustomerOrderRequestBp customerOrderRequestBp;

    @Inject
    CustomerOrderBp customerOrderBp;

    @Inject
    AccountXCredentialXUserBp accountXCredentialXUserBp;

    private static final Logger LOGGER = LoggerFactory.getLogger(OrderApprovalNeededEmailTemplate.class);

    private static final String TEMPLATE = "order.approval.needed";

    @Override
	public String createBody(PurchaseOrderDto purchaseOrderDto) throws IOException, TemplateException {
		Map<String, Object> params = new HashMap<>();
		
		params.put("orderDate", purchaseOrderDto.getOrderDate());
		
		params.put("billingAddress", this.getSafeAddress(purchaseOrderDto.lookupBillToAddressDto()));
		params.put("shippingAddress", this.getSafeAddress(purchaseOrderDto.getShipToAddressDto()));
		
		LOGGER.info("Getting token");
		CustomerOrder customerOrder = customerOrderBp.searchByApdPo(purchaseOrderDto.getApdPoNumber());
		String token="";
		if (customerOrder != null){
			token = customerOrderRequestBp.createRequestToken(customerOrder);
		}
		String approver = customerOrderBp.getApprover(customerOrder);
		LOGGER.info("Token {}",token);
		
		String authority = this.getProperty("tenant", "default_public_domain", "http://localhost:8080");
		params.put("approveUrl",authority + "/ws/workflow/order/approve/" + StringEscape.escapeForUrl(approver) + "/" + token);
		params.put("denyUrl",authority + "/ws/workflow/order/deny/" + StringEscape.escapeForUrl(approver) + "/" + token);
		
		params.put("merchandiseTotal", purchaseOrderDto.getSubTotal());	
		params.put("shippingTotal", purchaseOrderDto.getEstimatedShippingAmount());	
		params.put("taxTotal", purchaseOrderDto.getMaximumTaxToCharge());	
		params.put("orderTotal", purchaseOrderDto.getOrderTotal());
		params.put("items", purchaseOrderDto.getItems());
		params.put("customerPO", purchaseOrderDto.getCustomerPoNumber());
				
		String userName = null;
		String userEmail = null;		
		if (customerOrder != null && customerOrder.getUser() != null) {
			  List<AccountXCredentialXUser> response = accountXCredentialXUserBp.getAllAXCXUByLogin(customerOrder.getUser().getLogin());
            if (response != null && response.size() > 0) {
          	  for (AccountXCredentialXUser cred : response) {
          		  for (AssignedDepartment dept : cred.getDepartments()) {
          			  userName = dept.getRecipientsNames();
          			  userEmail = dept.getRecipientsEmails();
          			  if(dept.getDepartment() != null && !StringUtils.isEmpty(dept.getDepartment().getName())) {
          				params.put("department", dept.getDepartment().getName());
          			  }
          		  }
          	  }
            }
            
            if(customerOrder.getAssignedCostCenter() != null && customerOrder.getAssignedCostCenter().getCostCenter() != null) {
            	params.put("costCenter", customerOrder.getAssignedCostCenter().getCostCenter().getName());
            }	
      		if(customerOrder.getAddress() != null && customerOrder.getAddress().getMiscShipTo() != null ) {
      			if(!StringUtils.isEmpty(customerOrder.getAddress().getMiscShipTo().getRequesterPhone())) {
      				params.put("userPhone",customerOrder.getAddress().getMiscShipTo().getRequesterPhone());
      			}
      			if(!StringUtils.isEmpty(customerOrder.getAddress().getMiscShipTo().getDepartment())) {
      				params.put("department", customerOrder.getAddress().getMiscShipTo().getDepartment());
      			}
      		}
			if(StringUtils.isEmpty(userName) && StringUtils.isEmpty(userEmail) && customerOrder.getUser().getPerson() != null) {
				userName = customerOrder.getUser().getPerson().getFormalName();
				userEmail = customerOrder.getUser().getPerson().getEmail();
				if(params.get("userPhone") == null) {
					for (PhoneNumber phone : customerOrder.getUser().getPerson().getPhoneNumbers()) {
						if(PhoneNumberType.PhoneNumberTypeEnum.WORK.equals(phone.getType())) {
							params.put("userPhone", phone.getLineNumber());
						}
					}
				}				
			}
			params.put("userName", userName);
			params.put("userEmail", userEmail);		
			
		}
		
		return this.create(params);
	}

    public String getTemplate() {
        return TEMPLATE;
    }

    @Override
    public Attachment createAttachment(PurchaseOrderDto purchaseOrderDto) {
        // TODO Auto-generated method stub
        return null;
    }
}
