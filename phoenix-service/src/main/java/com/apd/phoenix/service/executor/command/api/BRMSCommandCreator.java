package com.apd.phoenix.service.executor.command.api;

import java.util.List;
import java.util.Map;

public interface BRMSCommandCreator {

    /**
     * Send a start process command to the BRMS Service
     * @param processName The name of the workflow process
     * @param type The domain type
     * @param bussinessKey The domain objects business key
     * @param processParams The process params map
     * @param facts The list of facts
     * @param callback A callback command to call
     */
    public void sendStartProcess(StartProcessCommand.Name processName, Command.Type type, Long bussinessKey,
            Map<String, Object> processParams, List<Object> facts, Command.Callback callback);

    /**
     * Send a signal Event command to the BRMS Service
     * @param eventName The name of the signal
     * @param message The message to include with the signal
     * @param type The domain type
     * @param bussinessKey The domain objects business key
     * @param facts The list of facts
     * @param callback A callback command to call
     */
    public void sendSignalEvent(SignalCommand.Name eventName, Object message, Command.Type type, Long bussinessKey,
            List<Object> facts, Command.Callback callback);

    public void sendCompleteWorkItem(Command.Type type, Long bussinessKey, Long workItemID,
            Map<String, Object> responseData, List<Object> facts, Command.Callback callback);

    public void completeTask(Long taskId, String userId, Map<String, Object> params, Command.Type type,
            Long bussinessKey);
}
