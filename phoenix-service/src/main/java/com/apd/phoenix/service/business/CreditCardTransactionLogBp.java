/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.service.business;

import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import com.apd.phoenix.service.model.CreditCardTransactionLog;
import com.apd.phoenix.service.model.LineItemXShipment;
import com.apd.phoenix.service.payment.ws.CreditCardTransactionResult;
import com.apd.phoenix.service.payment.ws.client.TransactionType;
import com.apd.phoenix.service.persistence.jpa.CreditCardTransactionLogDao;

/**
 *
 * @author dnorris
 */
@Stateless
@LocalBean
public class CreditCardTransactionLogBp extends AbstractBp<CreditCardTransactionLog> {

    /**
     * This method is injected with the correct Dao for initialization
     * 
     * @param dao
     */
    @Inject
    public void initDao(CreditCardTransactionLogDao dao) {
        this.dao = dao;
    }

    public CreditCardTransactionLog hydrateCardTransactionLogForReport(long id) {
        return ((CreditCardTransactionLogDao) dao).hydrateCardTransactionLogForReport(id);
    }

    public List<LineItemXShipment> getHydratedLineItemXShipments(long id) {
        return ((CreditCardTransactionLogDao) dao).getHydratedLineItemXShipments(id);
    }

    public CreditCardTransactionLog getLogByType(CreditCardTransactionResult creditCardTransactionResult,
            TransactionType type) {
        return this.getLogByType(creditCardTransactionResult.getTransactionKey().getValue(), type);
    }

    public CreditCardTransactionLog getLogByType(String transactionKey, TransactionType type) {
        CreditCardTransactionLog search = new CreditCardTransactionLog();
        search.setTransactionKey(transactionKey);
        search.setTransactionType(type.getTransactionValue());
        List<CreditCardTransactionLog> results = searchByExactExample(search, 0, 1);
        if (results != null && !results.isEmpty()) {
            return results.get(0);
        }
        else {
            return null;
        }
    }

}
