package com.apd.phoenix.service.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Version;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.Index;

/**
 * This entity is used to store different properties for the Catalog-Item
 * relationship, such as whether a substitution should be forced. It has a
 * many-to-one relationship with CatalogXItem and a many-to-one relationship
 * with CatalogXItemPropertyType.
 * 
 * @author RHC
 * 
 */
@Entity
public class ItemXItemPropertyType implements Serializable, com.apd.phoenix.service.model.Entity {

    private static final long serialVersionUID = 55569939624762582L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id = null;
    @Version
    @Column(name = "version", nullable = false)
    private int version = 0;

    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    @Fetch(FetchMode.JOIN)
    @Index(name = "TYPE_ID_IX")
    private ItemPropertyType type;

    @Column(length = 4000)
    private String value;

    public Long getId() {
        return this.id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public int getVersion() {
        return this.version;
    }

    public void setVersion(final int version) {
        this.version = version;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        if (id != null) {
            return id.equals(((ItemXItemPropertyType) that).id);
        }
        return super.equals(that);
    }

    @Override
    public int hashCode() {
        if (id != null) {
            return id.hashCode();
        }
        return super.hashCode();
    }

    public ItemPropertyType getType() {
        return this.type;
    }

    public void setType(final ItemPropertyType type) {
        this.type = type;
    }

    public String getValue() {
        return this.value;
    }

    public void setValue(final String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        String result = getClass().getSimpleName() + " ";
        if (value != null && !value.trim().isEmpty())
            result += "value: " + value;
        return result;
    }

}