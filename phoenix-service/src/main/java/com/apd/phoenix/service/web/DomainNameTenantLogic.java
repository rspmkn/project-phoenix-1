package com.apd.phoenix.service.web;

import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.core.utility.TenantConfigRepository;
import com.apd.phoenix.service.business.IpSubdomainRestriction;
import com.apd.phoenix.service.persistence.multitenancy.impl.CurrentTenantIdentifierResolverImpl;

public class DomainNameTenantLogic {

    private static final String REST_CONTEXT_PATH = "/rest";
    private static final Logger LOG = LoggerFactory.getLogger(DomainNameTenantLogic.class);

    /**
     * Takes an HttpServletRequest, and uses it to set the tenant ID
     * 
     * @param request
     * @return
     */
    public static String setTenantAndGetDomain(HttpServletRequest request) {

        String domain = IpSubdomainRestriction.ipAndDomainInfo(request)[2];
        LOG.debug("Domain detected is {}", domain);
        boolean tenantFromDomain = false;

        try {
            CurrentTenantIdentifierResolverImpl.setCurrentTenant(TenantConfigRepository.getInstance()
                    .getTenantIdByDomain(domain));
            tenantFromDomain = true;
        }
        catch (NullPointerException e) {
            //do nothing, either error will be logged or tenant will be fetched from tenantId header
        }
        String tenantIdParameter = request.getHeader("tenantId");
        if (isInternal(request) && StringUtils.isNotBlank(tenantIdParameter)
                && StringUtils.isNumeric(tenantIdParameter)) {
            CurrentTenantIdentifierResolverImpl.setCurrentTenant(Long.parseLong(tenantIdParameter));
        }
        else if (!tenantFromDomain) {
            LOG.error("ERROR finding tenant for domain: " + domain);
        }

        return domain;
    }

    /**
     * Returns true if the request is to an internal REST endpoint (and therefore the "tenantId" header
     * can be trusted) and false otherwise.
     * 
     * @param request
     * @return
     */
    private static boolean isInternal(HttpServletRequest request) {
        return REST_CONTEXT_PATH.equals(request.getContextPath());
    }
}
