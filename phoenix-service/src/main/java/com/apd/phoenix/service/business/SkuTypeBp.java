package com.apd.phoenix.service.business;

import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import com.apd.phoenix.service.model.SkuType;
import com.apd.phoenix.service.persistence.jpa.SkuTypeDao;

/**
 * This class provides business process methods for SkuType.
 * 
 * @author RHC
 * 
 */
@Stateless
@LocalBean
public class SkuTypeBp extends AbstractBp<SkuType> {

    //TODO replace these static final Strings with SkuTypeEnum class
    public static final String SKUTYPE_APD = "dealer";
    public static final String SKUTYPE_CUSTOMER = "customer";
    public static final String SKUTYPE_VENDOR = "vendor";

    /**
     * This method is injected with the correct Dao for initialization
     * 
     * @param dao
     */
    @Inject
    public void initDao(SkuTypeDao dao) {
        this.dao = dao;
    }

    /**
     * Returns an APD SkuType
     * 
     * @return the SkuType
     */
    public SkuType getApdSkuType() {
        return getSkuType(SKUTYPE_APD);
    }

    /**
     * Returns a Customer SkuType
     * 
     * @return the SkuType
     */
    public SkuType getCustomerSkuType() {
        return getSkuType(SKUTYPE_CUSTOMER);
    }

    /**
     * Returns an Vendor SkuType
     * 
     * @return the SkuType
     */
    public SkuType getVendorSkuType() {
        return getSkuType(SKUTYPE_VENDOR);
    }

    /**
     * Returns a SkuType matching SkuType.name
     * @param name 
     * 
     * @return the SkuType
     */
    public SkuType getSkuType(String name) {
        SkuType searchType = new SkuType();
        searchType.setName(name);
        List<SkuType> results = this.searchByExactExample(searchType, 0, 1);
        if (results.size() == 0) {
            return null;
        }
        return results.get(0);
    }
}
