package com.apd.phoenix.service.persistence.jpa;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.Query;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.model.Account;
import com.apd.phoenix.service.model.CustomerOrder;
import com.apd.phoenix.service.model.CxmlConfiguration;
import com.apd.phoenix.service.model.CxmlCredential;

/**
 *
 * @author dnorris
 */
@Stateless
@LocalBean
public class CxmlConfigurationDao extends AbstractDao<CxmlConfiguration> {

    private static final Logger LOG = LoggerFactory.getLogger(CxmlConfigurationDao.class);

    public List<CxmlConfiguration> getCxmlConfigurationForAccount(Account account) {
        Long accountId = account.getId();
        if (account.getParentAccount() != null) {
            accountId = account.getParentAccount().getId();
        }
        String hql = "SELECT cxmlCred FROM Account AS acc " + "LEFT JOIN acc.cxmlConfiguration AS cxmlCred "
                + "WHERE acc.id=:accountId";
        Query query = entityManager.createQuery(hql);
        query.setParameter("accountId", accountId);
        return (List<CxmlConfiguration>) query.getResultList();
    }

    public Account getConfigurationFromCxmlHeaderData(Map<String, String> fromCredentialMap,
            Map<String, String> toCredentialMap, Map<String, String> senderCredentialMap, String senderSharedSecret) {
        if (fromCredentialMap == null || toCredentialMap == null || senderCredentialMap == null) {
            return null;
        }
        Query fromQuery = getCxmlCredentialQueryFromMap(fromCredentialMap);
        Query toQuery = getCxmlCredentialQueryFromMap(toCredentialMap);
        Query senderQuery = getCxmlCredentialQueryFromMap(senderCredentialMap);

        Query cxmlConfigurationQuery = entityManager
                .createQuery("SELECT c FROM CxmlConfiguration AS c WHERE EXISTS "
                        + "(SELECT cred FROM CxmlCredential AS cred where cred IN (:fromCredentials) and cred MEMBER OF c.fromCredentials) AND EXISTS "
                        + "(SELECT cred FROM CxmlCredential AS cred where cred IN (:toCredentials) and cred MEMBER OF c.toCredentials) AND EXISTS "
                        + "(SELECT cred FROM CxmlCredential AS cred where cred IN (:senderCredentials) and cred MEMBER OF c.senderCredentials"
                        + ") AND " + "c.senderSharedSecret = :senderSharedSecret");
        cxmlConfigurationQuery.setParameter("fromCredentials", fromQuery.getResultList());
        cxmlConfigurationQuery.setParameter("toCredentials", toQuery.getResultList());
        cxmlConfigurationQuery.setParameter("senderCredentials", senderQuery.getResultList());
        cxmlConfigurationQuery.setParameter("senderSharedSecret", senderSharedSecret);
        List<CxmlConfiguration> matchingCxmlConfigurations = cxmlConfigurationQuery.getResultList();
        CxmlConfiguration matchingCxmlConfiguration = null;
        if (matchingCxmlConfigurations == null || matchingCxmlConfigurations.isEmpty()) {
            return null;
        }
        else if (matchingCxmlConfigurations.size() > 1) {
            for (CxmlConfiguration cxmlConfiguration : matchingCxmlConfigurations) {

                Iterator<Entry<String, String>> fromIter = fromCredentialMap.entrySet().iterator();
                while (fromIter.hasNext()) {
                    Entry<String, String> kvp = fromIter.next();
                    boolean isInConfiguration = containsCxmlCredential(kvp.getKey(), kvp.getValue(), cxmlConfiguration
                            .getFromCredentials());
                    if (!isInConfiguration) {
                        LOG.debug("Configuration did not match");
                        matchingCxmlConfiguration = null;
                        break;
                    }
                    if (!fromIter.hasNext()) {
                        LOG.debug("Found matching configuration");
                        matchingCxmlConfiguration = cxmlConfiguration;
                    }
                }

                if (matchingCxmlConfiguration != null) {
                    break;
                }
            }
        }
        else {
            matchingCxmlConfiguration = matchingCxmlConfigurations.get(0);
        }

        Query accountQuery = entityManager
                .createQuery("select a from Account As a where a.cxmlConfiguration = :cxmlConfiguration");
        accountQuery.setParameter("cxmlConfiguration", matchingCxmlConfiguration);
        Account toReturn = (Account) accountQuery.getSingleResult();
        return toReturn;
    }

    private boolean containsCxmlCredential(String key, String value, Set<CxmlCredential> list) {
        for (CxmlCredential cxmlCredential : list) {
            if (StringUtils.isNotEmpty(cxmlCredential.getDomain()) && cxmlCredential.getDomain().equals(key)
                    && StringUtils.isNotEmpty(cxmlCredential.getIdentifier())
                    && cxmlCredential.getIdentifier().equals(value)) {
                return true;
            }
        }
        return false;

    }

    private Query getCxmlCredentialQueryFromMap(Map<String, String> credentialDataMap) {
        StringBuilder queryString = new StringBuilder("select c from CxmlCredential as c where ");
        Iterator<Entry<String, String>> iter = credentialDataMap.entrySet().iterator();
        int count = 0;
        while (iter.hasNext()) {
            iter.next();
            //append something
            String domainParamBase = "domain" + count;
            String identityParamBase = "identity" + count;
            queryString.append("( ");
            queryString.append("c.domain = :").append(domainParamBase);
            queryString.append(" ").append("and ");
            queryString.append("c.identifier = :").append(identityParamBase);
            queryString.append(")");
            //iter.remove();
            if (iter.hasNext()) {
                queryString.append(" or ");
            }
            count++;
        }
        Query query = entityManager.createQuery(queryString.toString());

        Iterator<Entry<String, String>> senderCredentialMapIterator = credentialDataMap.entrySet().iterator();
        int i = 0;
        while (senderCredentialMapIterator.hasNext()) {
            Entry<String, String> keyValuePair = senderCredentialMapIterator.next();
            String domainParam = "domain" + i;
            query.setParameter(domainParam, keyValuePair.getKey());
            String identityParam = "identity" + i;
            query.setParameter(identityParam, keyValuePair.getValue());
            i++;
        }
        return query;
    }
}
