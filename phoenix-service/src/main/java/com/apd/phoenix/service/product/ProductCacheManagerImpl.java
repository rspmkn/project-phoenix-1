package com.apd.phoenix.service.product;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;
import org.apache.commons.lang.StringUtils;
import org.infinispan.client.hotrod.RemoteCache;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.business.ItemBp;
import com.apd.phoenix.service.cache.CacheProviderException;
import com.apd.phoenix.service.cache.ProductCacheProvider;
import com.apd.phoenix.service.model.Catalog;
import com.apd.phoenix.service.model.CatalogXItem;
import com.apd.phoenix.service.model.Catalog.SearchType;
import com.apd.phoenix.service.persistence.jpa.CatalogXItemDao;
import com.apd.phoenix.service.persistence.multitenancy.impl.CurrentTenantIdentifierResolverImpl;

/**
 * Concrete class for ProductCacheManager
 * 
 * @author RHC
 *
 */
@Stateless
public class ProductCacheManagerImpl implements ProductCacheManager {

    private static final Logger logger = LoggerFactory.getLogger(ProductCacheManagerImpl.class);

    @Inject
    private ProductCacheProvider cacheProvider;

    @Inject
    private ItemBp itemBp;

    @Inject
    private CatalogXItemDao catalogXItemDao;

    @Override
    public Product getProduct(CatalogXItem item) {
        try {
            //first, checks if the item is cached
            ProductKey key = this.getProductKey(item);
            Product toReturn = this.getProduct(key.getSku(), key.getVendorName(), this.getCustomerCatalogIdAsList(key
                    .getCustomerCatalogId()));
            //if a cached Product exists, compares the ID to confirm that they are for the same item. 
            //if so, returns the Product.
            if (toReturn != null && toReturn.getId() != null && toReturn.getId().equals(item.getId())) {
                return toReturn;
            }
            else {
                //otherwise, if the item is not cached or it doesn't match, create a new cache entry for the item
                return this.updateCatalogXItem(item);
            }
        }
        catch (Exception e) {
            //if an exception occurs (possibly an NPE from missing fields), returns null
            logger.warn("Exception pulling CatalogXItem from cache, turn on debug for stack trace");
            if (logger.isDebugEnabled()) {
                logger.debug("Stack trace", e);
            }
            return null;
        }
    }

    @Override
    public Product getProduct(String apdSku, String vendorName, List<Long> customerCatalogId)
            throws ProductNotFoundException {
        //first, checks if the Product is cached
        Product toReturn = getProductFromCache(apdSku, vendorName, customerCatalogId);
        if (toReturn == null) {
            //if the Product is not cached, finds the CatalogXItem in the database
            CatalogXItem item = this.getCatalogXItemFromDatabase(apdSku, vendorName, customerCatalogId);
            if (item != null) {
                //once found, inserts into cache, and returns the Product
                return this.updateCatalogXItem(item);
            }
            else {
                //if not found, throw exception
                throw new ProductNotFoundException();
            }
        }
        //returns the Product fetched from the cache
        return toReturn;
    }

    @Override
    public CatalogXItem getCatalogXItem(String apdSku, String vendorName, Long customerCatalogId)
            throws ProductNotFoundException {
        List<Long> catalogIdList = this.getCustomerCatalogIdAsList(customerCatalogId);
        //first, gets Product from cache
        Product product = getProductFromCache(apdSku, vendorName, catalogIdList);
        if (product != null) {
            try {
                //if in cache, gets CatalogXItem
                CatalogXItem toReturn = catalogXItemDao.findById(product.getId(), CatalogXItem.class);
                ProductKey key = this.getProductKey(toReturn);
                //compares fetched CatalogXItem to parameters; if they match, the CatalogXItem is returned
                if (key.getSku().equals(apdSku) && key.getVendorName().equals(vendorName)
                        && key.getCustomerCatalogId() == (long) customerCatalogId) {
                    return toReturn;
                }
            }
            catch (Exception e) {
                //do nothing, item will be fetched from database
                logger.warn("Exception pulling CatalogXItem from cache, turn on debug for stack trace");
                if (logger.isDebugEnabled()) {
                    logger.debug("Stack trace", e);
                }
            }
        }
        //if no Product, or the Product doesn't match, or an exception occurs, fetches the CatalogXItem 
        //from the database.
        CatalogXItem item = this.getCatalogXItemFromDatabase(apdSku, vendorName, catalogIdList);
        if (item != null) {
            this.updateCatalogXItem(item);
            return item;
        }
        else {
            throw new ProductNotFoundException();
        }
    }

    @Override
    public Product updateCatalogXItem(CatalogXItem item) {
        Product cacheValue = new Product(item);
        try {
            if (this.shouldCacheItem(item)) {
                ProductKey cacheKey = this.getProductKey(item);
                cacheProvider.getCache().putAsync(cacheKey, cacheValue);
            }
            else if (logger.isDebugEnabled()) {
                logger.debug("Not inserting item into cache.");
            }
        }
        catch (Exception e) {
            logger.warn("Exception inserting CatalogXItem into cache, turn on debug for stack trace");
            if (logger.isDebugEnabled()) {
                logger.debug("Stack trace", e);
            }
        }
        return cacheValue;
    }

    private Product getProductFromCache(String apdSku, String vendorName, List<Long> customerCatalogIds) {
        try {
            RemoteCache<ProductKey, Product> cache = cacheProvider.getCache();
            long tenantId = CurrentTenantIdentifierResolverImpl.getCurrentTenant();
            for (Long customerCatalogId : customerCatalogIds) {
                ProductKey cacheKey = new ProductKey(apdSku, vendorName, (long) customerCatalogId, tenantId);
                Product cacheValue = cache.get(cacheKey);
                if (cacheValue != null) {
                    return cacheValue;
                }
            }
            return null;
        }
        catch (CacheProviderException e) {
            //if an error occurred, returns null; value will be fetched from database
            logger.warn("CacheProviderException occurred, turn on debug for stack trace");
            if (logger.isDebugEnabled()) {
                logger.debug("Stack trace", e);
            }
            return null;
        }
    }

    /**
     * This method takes customer item information, and fetches the customer information from the database. 
     * 
     * TODO: optimize the performance of this method, per PHOEN-4998
     * 
     * @param apdSku
     * @param vendorName
     * @param customerCatalogId
     * @return
     * @throws ProductNotFoundException
     */
    private CatalogXItem getCatalogXItemFromDatabase(String apdSku, String vendorName, List<Long> customerCatalogIds)
            throws ProductNotFoundException {
        if (logger.isDebugEnabled()) {
            logger.debug("Unable to fetch from cache, fetching from database instead");
        }
        if (StringUtils.isBlank(apdSku) || StringUtils.isBlank(vendorName) || customerCatalogIds == null
                || customerCatalogIds.isEmpty()) {
            throw new ProductNotFoundException();
        }
        for (Long customerCatalogId : customerCatalogIds) {
            if (customerCatalogId == null) {
                throw new ProductNotFoundException();
            }
            CatalogXItem searchCxi = new CatalogXItem();
            searchCxi.setCatalog(new Catalog());
            searchCxi.getCatalog().setId(customerCatalogId);
            searchCxi.setItem(itemBp.searchItem(apdSku, ItemBp.APD_SKUTYPE, vendorName));
            List<CatalogXItem> results = catalogXItemDao.searchByExactExample(searchCxi, 0, 0);
            if (results != null && !results.isEmpty()) {
                if (results.size() > 1) {
                    logger.warn("Catalog {} returned multiple results for item with APD sku {}", new Object[] {
                            customerCatalogId, apdSku });
                }
                return results.get(0);
            }
        }
        throw new ProductNotFoundException();
    }

    private ProductKey getProductKey(CatalogXItem item) {
        long tenantId = CurrentTenantIdentifierResolverImpl.getCurrentTenant();
        return new ProductKey(item.getItem().getSku(ItemBp.APD_SKUTYPE), item.getItem().getVendorCatalog().getVendor()
                .getName(), (long) item.getCatalog().getId(), tenantId);
    }

    private List<Long> getCustomerCatalogIdAsList(Long id) {
    	List<Long> toReturn = new ArrayList<>(1);
    	toReturn.add(id);
        return toReturn;
    }

    /**
     * Takes an item, and returns a boolean indicating whether that item should be cached. Currently, only caches if 
     * the item is a SmartSearch item. This will be changed as part of PHOEN-5093.
     * 
     * @param item
     * @return
     */
    private boolean shouldCacheItem(CatalogXItem item) {
        return SearchType.SMART_SEARCH.equals(item.getCatalog().getSearchType());
    }
}
