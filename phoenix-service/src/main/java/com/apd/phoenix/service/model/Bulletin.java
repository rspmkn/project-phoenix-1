package com.apd.phoenix.service.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;
import org.hibernate.envers.Audited;

/**
 * This entity is used to store the information for a bulletin message. It has a many-to-many relationship 
 * with SystemUser, Account, and Credential. 
 * 
 * When an administrator chooses to associate a bulletin with an Account, Credential or User, a relationship 
 * is established between the bulletin and that entity. Then, when a user logs in using that account or 
 * credential, they will be shown the bulletin, unless it has expired.
 * 
 * @author RHC
 *
 */
@Entity
@Audited
public class Bulletin implements Serializable, com.apd.phoenix.service.model.Entity {

    private static final long serialVersionUID = -367553876062662589L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id = null;
    @Version
    @Column(name = "version", nullable = false)
    private int version = 0;

    @Column
    private String name;

    @Lob
    @Basic(fetch = FetchType.EAGER)
    @Column(name = "MESSAGE")
    private String message;

    @Temporal(TemporalType.DATE)
    private Date expirationDate;

    @Column(nullable = false)
    private boolean global = false;

    @Column(nullable = false)
    private boolean csr = false;

    public Long getId() {
        return this.id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public int getVersion() {
        return this.version;
    }

    public void setVersion(final int version) {
        this.version = version;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        if (id != null) {
            return id.equals(((Bulletin) that).id);
        }
        return super.equals(that);
    }

    @Override
    public int hashCode() {
        if (id != null) {
            return id.hashCode();
        }
        return super.hashCode();
    }

    public String getName() {
        return this.name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getMessage() {
        return this.message;
    }

    public void setMessage(final String message) {
        this.message = message;
    }

    public Date getExpirationDate() {
        return this.expirationDate;
    }

    public void setExpirationDate(final Date expirationDate) {
        this.expirationDate = expirationDate;
    }

    public boolean isGlobal() {
        return global;
    }

    public void setGlobal(boolean global) {
        this.global = global;
    }

    @Override
    public String toString() {
        String result = getClass().getSimpleName() + " ";
        if (name != null && !name.trim().isEmpty())
            result += "name: " + name;
        if (message != null && !message.trim().isEmpty())
            result += ", message: " + message;
        return result;
    }

    public boolean getCsr() {
        return csr;
    }

    public void setCsr(boolean csr) {
        this.csr = csr;
    }
}