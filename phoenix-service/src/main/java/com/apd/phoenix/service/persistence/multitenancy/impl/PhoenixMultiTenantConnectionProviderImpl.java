package com.apd.phoenix.service.persistence.multitenancy.impl;

public class PhoenixMultiTenantConnectionProviderImpl extends AbstractMultiTenantConnectionProviderImpl {

    /**
     * 
     */
    private static final long serialVersionUID = 8769488588594230672L;
    private static final String dataSourceJNDIName = "java:jboss/datasources/Phoenix";

    @Override
    protected String getDataSourceJNDIName() {
        return dataSourceJNDIName;
    }

}
