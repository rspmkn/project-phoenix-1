/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.apd.phoenix.service.business;

import com.apd.phoenix.service.model.ValidationError;
import com.apd.phoenix.service.persistence.jpa.ValidationErrorDao;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;

@Stateless
@LocalBean
public class ValidationErrorBp extends AbstractBp<ValidationError> {

    private static final String SKU_NOT_FOUND = "SKU_NOT_FOUND";

    /**
     * This method is injected with the correct Dao for initialization
     * 
     * @param dao
     */
    @Inject
    public void initDao(ValidationErrorDao dao) {
        this.dao = dao;
    }

    public ValidationError getNoSkuFoundError() {
        ValidationError search = new ValidationError();
        search.setName(SKU_NOT_FOUND);
        return this.searchByExample(search, 0, 1).get(0);
    }

}
