package com.apd.phoenix.service.email.impl;

import java.io.IOException;
import java.io.InputStream;
import java.util.*;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;
import com.apd.phoenix.service.model.dto.LineItemDto;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.core.StringEscape;
import com.apd.phoenix.core.utility.PropertiesLoader;
import com.apd.phoenix.service.business.CustomerOrderBp;
import com.apd.phoenix.service.email.api.Attachment;
import com.apd.phoenix.service.email.api.Attachment.MimeType;
import com.apd.phoenix.service.email.api.EmailTemplate;
import com.apd.phoenix.service.email.api.NoAttachmentContentException;
import com.apd.phoenix.service.email.impl.OrderCanceledEmailTemplate.ItemCancelled;
import com.apd.phoenix.service.model.CredentialPropertyType.CredentialPropertyTypeEnum;
import com.apd.phoenix.service.model.CustomerOrder;
import com.apd.phoenix.service.model.LineItem;
import com.apd.phoenix.service.model.dto.CashoutPageXFieldDto;
import com.apd.phoenix.service.model.dto.DtoUtils;
import com.apd.phoenix.service.model.dto.POAcknowledgementDto;
import com.apd.phoenix.service.model.dto.PurchaseOrderDto;
import com.apd.phoenix.service.model.factory.DtoFactory;
import com.apd.phoenix.service.model.factory.DtoFactory.ParsingException;
import freemarker.template.TemplateException;

@Stateless
@LocalBean
public class OrderAcknowledgmentEmailTemplate extends EmailTemplate<POAcknowledgementDto> {

    private static final String USPS_PARTNER_ID = "usps";

    private static final Logger LOGGER = LoggerFactory.getLogger(OrderAcknowledgmentEmailTemplate.class);

    private static final String TEMPLATE = "order.acknowledgment";
    private static final String ATTACHMENT_PREFIX = "order-ack";
    private static final String MINIMUM_ORDER_SKU = "min_order_fee";

    @Inject
    private CustomerOrderBp customerOrderBp;

    @Override
    public String createBody(POAcknowledgementDto pOAcknowledgementDto) throws IOException, TemplateException {
        Map<String, Object> params = new HashMap<>();
        //Get the current state of the order:
        CustomerOrder customerOrder = customerOrderBp.searchByApdPo(pOAcknowledgementDto.getApdPo());
        PurchaseOrderDto purchaseOrderDto;
        try {
                purchaseOrderDto = DtoFactory.createPurchaseOrderDto(customerOrder);
        } catch (ParsingException e) {
                LOGGER.error("Could not create purchaseOrderDto from Customer order", e);
                return null;
        }

        params.put("skuOnEmailFlag", purchaseOrderDto.getCredential().getProperties().get(CredentialPropertyTypeEnum.SKU_ON_EMAIL.getValue()));
        params.put("orderDate", purchaseOrderDto.getOrderDate());
        params.put("apdPo", purchaseOrderDto.retrieveApdPoNumber().getValue());
        params.put("customerPo", ((purchaseOrderDto.retrieveCustomerPoNumber() != null && purchaseOrderDto.retrieveCustomerPoNumber().getValue() != null) 
        		? purchaseOrderDto.retrieveCustomerPoNumber().getValue(): "N/A"));
        params.put("blanketPo", purchaseOrderDto.retrieveBlanketPoNumber() == null ? null : purchaseOrderDto.retrieveBlanketPoNumber().getValue());
        params.put("billingAddress", this.getSafeAddress(purchaseOrderDto.lookupBillToAddressDto()));
        params.put("shippingAddress", this.getSafeAddress(purchaseOrderDto.getShipToAddressDto()));
        String csPhone = customerOrder.getCredential().getCsrPhone();
        params.put("csPhone", (csPhone == null ? "" : csPhone));

        params.put("merchandiseTotal", purchaseOrderDto.getSubTotalNoFees());	
        params.put("shippingTotal", purchaseOrderDto.getEstimatedShippingAmount());	
        params.put("taxTotal", purchaseOrderDto.getMaximumTaxToCharge());	
        params.put("orderTotal", (purchaseOrderDto.getSubTotal().add(purchaseOrderDto.getEstimatedShippingAmount().add(purchaseOrderDto.getMaximumTaxToCharge()))));
        List<ItemSubstitution> itemsList = new ArrayList<>();
        BigDecimal minimumOrderTotal = null;

        /*Need to iterate through the list of items and see if we can find a minimum order fee and extract it out of the list.*/
        for(LineItem itemEntity: customerOrder.getItems())
        {
        	LineItemDto item = DtoFactory.createLineItemDto(itemEntity);
            if(MINIMUM_ORDER_SKU.equalsIgnoreCase(item.getApdSku()))
            {
                //Extract the total minimum order fee from the list
                minimumOrderTotal = item.getSubTotal();
            }
            else
            {
                //Add Item to the List
            	ItemSubstitution toAdd = new ItemSubstitution(); 
				toAdd.setItem(item);
				toAdd.setOriginallyAddedSku(itemEntity.getOriginallyAddedSku());
				toAdd.setOriginallyAddedName(itemEntity.getOriginallyAddedName());
                itemsList.add(toAdd);
            }
        }

        //Get items in the list
        params.put("items", itemsList);
        params.put("minimumOrderFee", minimumOrderTotal);


        /*Check to see if the customer order contains a desktop and department with the ship to address. If so, we need show those values in
                 * the email.*/
        String desktopString = null;
        String departmentString = null;
        String costCenter = null;
        String phoneNumber = null;
        String mailStop = null;
        String pole = null;
        if(customerOrder.getAddress() != null && customerOrder.getAddress().getMiscShipTo() != null)
        {
            if(StringUtils.isNotEmpty(customerOrder.getAddress().getMiscShipTo().getDesktop()))
            {
                desktopString =  customerOrder.getAddress().getMiscShipTo().getDesktop();
            }

            if(StringUtils.isNotEmpty(customerOrder.getAddress().getMiscShipTo().getDepartment()))
            {
                departmentString = customerOrder.getAddress().getMiscShipTo().getDepartment();
            }

            if(customerOrder.getAssignedCostCenter() != null && customerOrder.getAssignedCostCenter().getCostCenter() != null)
            {
            	costCenter = customerOrder.getAssignedCostCenter().getCostCenter().getName();
            }

            if(StringUtils.isNotEmpty(customerOrder.getAddress().getMiscShipTo().getMailStop()))
            {
                mailStop = customerOrder.getAddress().getMiscShipTo().getMailStop();
            }

            if(StringUtils.isNotEmpty(customerOrder.getAddress().getMiscShipTo().getPol()))
            {
                pole = customerOrder.getAddress().getMiscShipTo().getPol();
            }

            if(StringUtils.isNotEmpty(customerOrder.getAddress().getMiscShipTo().getRequesterPhone()))
            {
                phoneNumber = customerOrder.getAddress().getMiscShipTo().getRequesterPhone();
            }
        }

        params.put("desktop", this.getAddressFieldValue(desktopString));
        String desktopLabel = "Desktop";
        if(customerOrder.getCredential()!=null && customerOrder.getCredential().getCashoutPage() != null){
            CashoutPageXFieldDto deskFieldDefn = DtoUtils.findCashoutPageField(customerOrder.getCredential(), "desktop");
            if (deskFieldDefn == null) {
            	deskFieldDefn = DtoUtils.findCashoutPageField(customerOrder.getCredential(), "desktop combobox");
            }
            if (deskFieldDefn == null) {
            	deskFieldDefn = DtoUtils.findCashoutPageField(customerOrder.getCredential(), "desktop free");
            }
        	if (deskFieldDefn != null) {
        		if (deskFieldDefn.isIncludeLabelInMapping() != null && deskFieldDefn.isIncludeLabelInMapping()) {
        			desktopLabel = this.getAddressFieldLabel(desktopString);
        		} else if (StringUtils.isNotBlank(deskFieldDefn.getLabel())) {
            		desktopLabel = deskFieldDefn.getLabel();
            	}
        	}
        }
        params.put("desktopLabel", desktopLabel);
        params.put("department", this.getAddressFieldValue(departmentString));
        String departmentLabel = "Department";
        if(customerOrder.getCredential()!=null && customerOrder.getCredential().getCashoutPage() != null){
            CashoutPageXFieldDto deptFieldDefn = DtoUtils.findCashoutPageField(customerOrder.getCredential(), "department");
            if (deptFieldDefn != null) {
	        	if (deptFieldDefn.isIncludeLabelInMapping() != null && deptFieldDefn.isIncludeLabelInMapping()) {
	        		departmentLabel = this.getAddressFieldLabel(departmentString);
	        	} else if (StringUtils.isNotBlank(deptFieldDefn.getLabel())) {
	        		departmentLabel = deptFieldDefn.getLabel();
	        	}
            }
        }
        params.put("departmentLabel", departmentLabel);
        params.put("costcenter", this.getAddressFieldValue(costCenter));
        params.put("costCenterLabel", StringUtils.isBlank(customerOrder.getAssignedCostCenterLabel()) ? customerOrder.getAssignedCostCenterLabel() : "Cost Center");
        params.put("mailStop", this.getAddressFieldValue(mailStop));
        params.put("mailStopLabel", StringEscape.getMappingLabel(mailStop, "Mail Stop"));
        params.put("pole", this.getAddressFieldValue(pole));
        params.put("poleLabel", StringEscape.getMappingLabel(pole, "Pole"));
        params.put("phone", this.getAddressFieldValue(phoneNumber));
        params.put("phoneLabel", StringEscape.getMappingLabel(phoneNumber, "Phone"));

        //Logos
        params.put("apdLogo", this.getMediaIconURL(LOGO));
        params.put("facebookLogo", this.getMediaIconURL(FACEBOOK));
        params.put("googlePlusLogo", this.getMediaIconURL(GOOGLE_PLUS));
        params.put("linkedInLogo", this.getMediaIconURL(LINKEDIN));
        params.put("wordpressLogo", this.getMediaIconURL(WORDPRESS));
        params.put("youTubeLogo", this.getMediaIconURL(YOUTUBE));

        params.put("usps", this.isUsps(customerOrder));

        return this.create(params);
    }

    public String getTemplate() {
        return TEMPLATE;
    }

    @Override
    public Attachment createAttachment(POAcknowledgementDto pOAcknowledgementDto) throws NoAttachmentContentException {
        String apdPo = pOAcknowledgementDto.getApdPo();
        CustomerOrder order = customerOrderBp.searchByApdPo(apdPo);
        boolean useUspsAttachment = this.isUsps(order);
        Attachment orderAck = new Attachment();
        orderAck.setFileName(ATTACHMENT_PREFIX + apdPo + PDF_EXT);
        orderAck.setMimeType(MimeType.pdf);
        InputStream is = useUspsAttachment ? reportService.generateUspsOrderAcknowledgmentPdf(apdPo) : reportService
                .generateOrderAcknowledgmentPdf(apdPo);
        if (is != null) {
            orderAck.setContent(is);
        }
        else {
            throw new NoAttachmentContentException();
        }

        return orderAck;
    }

    private Boolean isUsps(CustomerOrder order) {
        return (order != null && order.getCredential() != null
                && StringUtils.isNotBlank(order.getCredential().getPartnerId()) && order.getCredential().getPartnerId()
                .equalsIgnoreCase(USPS_PARTNER_ID));
    }

    public class ItemSubstitution {

        private LineItemDto item;
        private String originallyAddedSku;
        private String originallyAddedName;

        public LineItemDto getItem() {
            return item;
        }

        public void setItem(LineItemDto item) {
            this.item = item;
        }

        public String getOriginallyAddedSku() {
            return originallyAddedSku;
        }

        public String getOriginallyAddedName() {
            return originallyAddedName;
        }

        public void setOriginallyAddedSku(String originallyAddedSku) {
            this.originallyAddedSku = originallyAddedSku;
        }

        public void setOriginallyAddedName(String originallyAddedName) {
            this.originallyAddedName = originallyAddedName;
        }
    }
}
