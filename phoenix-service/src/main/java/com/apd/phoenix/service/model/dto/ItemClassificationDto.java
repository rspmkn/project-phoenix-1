package com.apd.phoenix.service.model.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ItemClassificationDto implements Serializable {

    private static final long serialVersionUID = -5156424826714232358L;
    private String type;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
