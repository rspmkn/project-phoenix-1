package com.apd.phoenix.service.model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;
import javax.persistence.Version;
import org.hibernate.annotations.Index;
import com.apd.phoenix.service.model.listener.SyncItemResultListener;

@Entity
@EntityListeners(SyncItemResultListener.class)
public class SyncItemResult implements Serializable, com.apd.phoenix.service.model.Entity {

    @Transient
    private Set<String> errorSet = new HashSet<String>();

    @Transient
    private Set<String> warningSet = new HashSet<String>();

    @Column
    private String resultApdSku;

    @Column
    private String resultVendor;

    @Index(name = "sir_corrId")
    @Column
    private String correlationId;

    @Column(length = 4000)
    private String errors = null;

    @Column(length = 4000)
    private String warnings = null;

    /** The id. */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id = null;

    /** The version. */
    @Version
    @Column(name = "version", nullable = false)
    private int version = 0;

    @Index(name = "sir_summary_idx")
    @ManyToOne(fetch = FetchType.EAGER)
    private SyncItemResultSummary summary;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public Set<String> getErrorSet() {
        return errorSet;
    }

    public void setErrorSet(Set<String> errorSet) {
        this.errorSet = errorSet;
    }

    public Set<String> getWarningSet() {
        return warningSet;
    }

    public void setWarningSet(Set<String> warningSet) {
        this.warningSet = warningSet;
    }

    public SyncItemResultSummary getSummary() {
        return summary;
    }

    public void setSummary(SyncItemResultSummary summary) {
        this.summary = summary;
    }

    public String getErrors() {
        return errors;
    }

    public void setErrors(String errors) {
        this.errors = errors;
    }

    public String getWarnings() {
        return warnings;
    }

    public void setWarnings(String warnings) {
        this.warnings = warnings;
    }

    public String getResultApdSku() {
        return resultApdSku;
    }

    public void setResultApdSku(String resultApdSku) {
        this.resultApdSku = resultApdSku;
    }

    public String getResultVendor() {
        return resultVendor;
    }

    public void setResultVendor(String resultVendor) {
        this.resultVendor = resultVendor;
    }

    public String getCorrelationId() {
        return correlationId;
    }

    public void setCorrelationId(String correlationId) {
        this.correlationId = correlationId;
    }
}
