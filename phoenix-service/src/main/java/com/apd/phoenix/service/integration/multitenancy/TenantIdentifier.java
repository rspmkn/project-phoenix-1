package com.apd.phoenix.service.integration.multitenancy;

import javax.enterprise.inject.Alternative;

@Alternative
public class TenantIdentifier {

    private Long tenantId;

    public void setTenant(Long tenantId) {
        this.tenantId = tenantId;
    }

    public Long getTenant() {
        return tenantId;
    }

}
