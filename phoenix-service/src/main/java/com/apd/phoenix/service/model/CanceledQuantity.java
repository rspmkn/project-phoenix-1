package com.apd.phoenix.service.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;
import org.hibernate.annotations.Index;

/**
 * This entity stores the values for a cancellation of an item.
 * 
 * @author RHC
 *
 */
@Entity
public class CanceledQuantity implements Serializable, com.apd.phoenix.service.model.Entity {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id = null;
    @Version
    @Column(name = "version")
    private int version = 0;

    @ManyToOne(fetch = FetchType.EAGER)
    @Index(name = "CANCELQUANTITY_LINEITEM_IDX")
    private LineItem lineItem;

    @Column(nullable = false)
    private BigInteger quantity;

    @Column
    private String reason;

    @Temporal(TemporalType.TIMESTAMP)
    @Index(name = "CANCEL_CANCELDATE_IDX")
    private Date cancelDate = new Date();

    public Long getId() {
        return this.id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public int getVersion() {
        return this.version;
    }

    public void setVersion(final int version) {
        this.version = version;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        if (id != null) {
            return id.equals(((CanceledQuantity) that).id);
        }
        return super.equals(that);
    }

    @Override
    public int hashCode() {
        if (id != null) {
            return id.hashCode();
        }
        return super.hashCode();
    }

    public LineItem getLineItem() {
        return this.lineItem;
    }

    public void setLineItem(final LineItem lineItem) {
        this.lineItem = lineItem;
    }

    public BigInteger getQuantity() {
        return this.quantity;
    }

    public void setQuantity(final BigInteger quantity) {
        this.quantity = quantity;
    }

    @Override
    public String toString() {
        String result = getClass().getSimpleName() + " ";
        result += "quantity: " + quantity;
        return result;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public Date getCancelDate() {
        return cancelDate;
    }

    public void setCancelDate(Date cancelDate) {
        this.cancelDate = cancelDate;
    }

    /**
     * This enum lists the different cancellation reasons.
     * 
     * @author RHC
     *
     */
    public enum CancelReason {
        CUSTOMER_CANCEL("Customer Cancel"), DISC_WO_REPLACEMENT("Disc without replacement"), DISC_WITH_REPLACEMENT(
                "Disc with replacement");

        private final String label;

        private CancelReason(String label) {
            this.label = label;
        }

        public String getLabel() {
            return this.label;
        }

    }

    public CancelReason[] getCancelReasons() {
        return CancelReason.values();
    }

    public BigDecimal getSubTotalAmount() {
        return lineItem.getUnitPrice().multiply(new BigDecimal(quantity));
    }

}
