/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.service.model.dto;

import java.util.Date;
import java.util.List;

/**
 *
 * @author nreidelb
 */
public class StopNotificationDto {

    private List<String> errors;
    private String success;
    private List<ShipmentDto> shipments;

    /**
     * @return the errors
     */
    public List<String> getErrors() {
        return errors;
    }

    /**
     * @param errors the errors to set
     */
    public void setErrors(List<String> errors) {
        this.errors = errors;
    }

    /**
     * @return the success
     */
    public String getSuccess() {
        return success;
    }

    /**
     * @param success the success to set
     */
    public void setSuccess(String success) {
        this.success = success;
    }

    /**
     * @return the shipments
     */
    public List<ShipmentDto> getShipments() {
        return shipments;
    }

    /**
     * @param shipments the shipments to set
     */
    public void setShipments(List<ShipmentDto> shipments) {
        this.shipments = shipments;
    }

}
