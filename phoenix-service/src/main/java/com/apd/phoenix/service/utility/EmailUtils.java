/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.apd.phoenix.service.utility;

import com.apd.phoenix.service.model.dto.DepartmentRequestDto;
import com.apd.phoenix.service.model.dto.UserModificationRequestDto;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author anicholson
 */
public class EmailUtils {

    private static final Logger logger = LoggerFactory.getLogger(EmailUtils.class);

    public static List<String> getFieldList(Object object) {
        if (!(object instanceof UserModificationRequestDto || object instanceof DepartmentRequestDto)) { //all DTOs in object structure
            return null;
        }
        List<String> fieldList = new ArrayList<>();
        Method[] methods = object.getClass().getDeclaredMethods();
        for (Method method : methods) {
            String name = method.getName();
            if (name.startsWith("get")) { //method is a getter
                String fieldName = name.replace("get", "");
                String formattedField = fieldName;
                int offset = 0;
                for (int j=1; j<fieldName.length(); j++) {
                    char c = fieldName.charAt(j);
                    if (Character.isUpperCase(c)) {
                            formattedField = formattedField.substring(0, j + offset) + " " + formattedField.substring(j + offset);
                            offset++;
                    }
                }
                try {
                    Object methodReturn = method.invoke(object, new Object[0]);
                    if (methodReturn instanceof Collection) {
                        for (Object o : (Collection) methodReturn) {
                            if (o instanceof DepartmentRequestDto) { //add to this if more DTOs are added to UserModificationRequestDto object
                                fieldList.add(formattedField + ": " + getFieldList(o));
                            }
                        }
                    } else if (methodReturn instanceof String) {
                        fieldList.add(formattedField + ": " + methodReturn);
                    } else {
                        if (methodReturn instanceof DepartmentRequestDto) { //add to this if more DTOs are added to UserModificationRequestDto object
                            fieldList.add(formattedField + ": " + getFieldList(methodReturn));
                        }
                    }
                } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
                    // TODO Auto-generated catch block
                    logger.error(e.getMessage(), e);
                }
            }
        }
        return fieldList;
    }

    public static String getUserIdFromUserModificationRequestDTO(Object object) {
        if (!(object instanceof UserModificationRequestDto)) {
            return null;
        }
        else {
            UserModificationRequestDto umrDTO = (UserModificationRequestDto) object;
            return umrDTO.getUserId();
        }
    }

}
