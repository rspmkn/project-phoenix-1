package com.apd.phoenix.service.persistence.jpa;

import java.util.Collections;
import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.Query;
import com.apd.phoenix.service.business.EntityComparator;
import com.apd.phoenix.service.model.WorkflowLog;

/**
 * WorkflowLog DAO stub
 * 
 * @author RHC
 *
 */
@Stateless
@LocalBean
public class WorkflowLogDao extends AbstractDao<WorkflowLog> {

    @SuppressWarnings("unchecked")
    public List<WorkflowLog> getLogsForTask(long taskId) {

        String hql = "SELECT DISTINCT log FROM WorkflowLog log WHERE log.taskId = :taskId";
        //Creates the query
        Query query = entityManager.createQuery(hql);
        query.setParameter("taskId", taskId);
        List<WorkflowLog> toReturn = query.getResultList();
        Collections.sort(toReturn, new EntityComparator());
        return toReturn;
    }
}
