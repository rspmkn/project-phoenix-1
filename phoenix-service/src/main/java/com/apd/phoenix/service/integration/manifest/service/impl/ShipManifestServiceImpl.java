package com.apd.phoenix.service.integration.manifest.service.impl;

import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.mail.internet.MimeMessage;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.apd.phoenix.core.utility.TenantConfigRepository;
import com.apd.phoenix.service.business.JumptrackStopConfigurationBp;
import com.apd.phoenix.service.business.ShipManifestBp;
import com.apd.phoenix.service.email.api.EmailService;
import com.apd.phoenix.service.integration.manifest.service.api.ShipManifestService;
import com.apd.phoenix.service.message.api.MessageService;
import com.apd.phoenix.service.model.Account;
import com.apd.phoenix.service.model.Address;
import com.apd.phoenix.service.model.CustomerOrder;
import com.apd.phoenix.service.model.MessageMetadata.MessageType;
import com.apd.phoenix.service.model.Person;
import com.apd.phoenix.service.model.PhoneNumber;
import com.apd.phoenix.service.model.ShipManifest;
import com.apd.phoenix.service.model.ShipManifest.ManifestType;
import com.apd.phoenix.service.model.ShipManifestLog;
import com.apd.phoenix.service.model.dto.DeliveryDto;
import com.apd.phoenix.service.model.dto.DeliveryDto.Type;
import com.apd.phoenix.service.model.dto.DeliveryLineDto;
import com.apd.phoenix.service.model.dto.DeliveryLineDto.UOM;
import com.apd.phoenix.service.model.dto.ManifestDto;
import com.apd.phoenix.service.model.dto.ManifestRequestDto;
import com.apd.phoenix.service.model.dto.RouteDto;
import com.apd.phoenix.service.model.dto.SiteDto;
import com.apd.phoenix.service.model.dto.StopDto;
import com.apd.phoenix.service.model.dto.StopNotificationRequestDto;
import com.apd.phoenix.service.persistence.jpa.ShipManifestLogDao;
import com.apd.phoenix.service.persistence.multitenancy.impl.CurrentTenantIdentifierResolverImpl;

@Stateless
public class ShipManifestServiceImpl implements ShipManifestService {

	private static final Logger LOG = LoggerFactory
			.getLogger(ShipManifestServiceImpl.class);

	@Inject
	ShipManifestBp shipManifestBp;

	@Inject
	EmailService emailService;

	@Inject
	MessageService messageService;
	
	@Inject
	ShipManifestLogDao shipManifestLogDao;
	
	@Inject
	private JumptrackStopConfigurationBp stopConfigBp;
    
    public boolean isUsscoFileNameAllowed(String filename) {
    	for(String allowedFilename : allowedUsscoFilenames()) {
    		if(filename.contains(allowedFilename)) {
    			return true;
    		}
    	}
    	return false;
    }

	public boolean sendStopNotificationRequest() {
		String jumptrackEnabled = TenantConfigRepository.getInstance().getProperty(CurrentTenantIdentifierResolverImpl.getCurrentTenant(), "tenant", "jumptrackEnabled");
        boolean isEnabled = Boolean.parseBoolean(jumptrackEnabled);
        if (!isEnabled) {
        	LOG.error("Jumptrack disabled");
        	return false;
        }
		StopNotificationRequestDto stopNotificationRequestDto = new StopNotificationRequestDto();
		stopNotificationRequestDto.setEncodeImage(true);
		stopNotificationRequestDto.setMax(BigInteger.valueOf(10));
		return messageService.sendJumptrackStopNotificationRequest(stopNotificationRequestDto, MessageType.XML);
	}

	/**
	 * Start the jumptrack aggregation and upload
	 */
	@Override
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public synchronized boolean sendManifestToJumptrack() {

        String region = TenantConfigRepository.getInstance().getProperty(CurrentTenantIdentifierResolverImpl.getCurrentTenant(), "tenant", "ship_manifest_delivery_region");
        String distribution = TenantConfigRepository.getInstance().getProperty(CurrentTenantIdentifierResolverImpl.getCurrentTenant(), "tenant", "ship_manifest_distribution_name");
		String jumptrackEnabled = TenantConfigRepository.getInstance().getProperty(CurrentTenantIdentifierResolverImpl.getCurrentTenant(), "tenant", "jumptrackEnabled");
        boolean isEnabled = Boolean.parseBoolean(jumptrackEnabled);
		
        if (!isEnabled) {
        	LOG.error("Jumptrack disabled");
        	return false;
        }
		Map<String, List<ShipManifest>> shipManifestsByZip = shipManifestBp
				.getNewShipmentManifestsByZip();

		List<ManifestDto> results = new ArrayList<>();
		for (String zip : shipManifestsByZip.keySet()) {
			// Manifest entry per zip
			ManifestDto manifestDto = new ManifestDto();
			RouteDto routeDto = new RouteDto();
			routeDto.setDistribution(distribution);
			routeDto.setRegion(region);
			routeDto.setRoute(zip);
			manifestDto.setRouteDto(routeDto);

			List<ShipManifest> shipManifests = shipManifestsByZip.get(zip);
			List<StopDto> stopDtosForZip = generateStopDtos(shipManifests);
			manifestDto.setStops(stopDtosForZip);
			results.add(manifestDto);
		}
		ManifestRequestDto manifestRequestDto = new ManifestRequestDto();
		manifestRequestDto.setManifests(results);
		for (String key : shipManifestsByZip.keySet()) {
			for (ShipManifest manifest : shipManifestsByZip.get(key)) {
				try {
					shipManifestBp.update(manifest);
				} catch (Exception e) {
					LOG.warn("Error saving jumptrack manifest. Turn on debug logging to view stack trace.");
					if (LOG.isDebugEnabled()) {
						LOG.debug("Stack trace", e);
					}
				}
			}
		}
		LOG.info("Sending to jumptrack {} manifests.", manifestRequestDto
				.getManifests().size());
		return messageService.sendJumptrackManifest(manifestRequestDto,
				MessageType.XML);
	}

	private List<StopDto> generateStopDtos(List<ShipManifest> shipManifests) {
		// Organize by APD Po
		Map<CustomerOrder, List<ShipManifest>> shipManifestsByOrder = new HashMap<>();
		for (ShipManifest shipManifest : shipManifests) {
			// Mark as complete
			shipManifest.setManifested(true);

			if (shipManifest.getCustomerOrder() != null
					&& shipManifest.getCustomerOrder().getApdPo() != null) {
				CustomerOrder order = shipManifest.getCustomerOrder();
				if (shipManifestsByOrder.get(order) == null) {
					List<ShipManifest> newList = new ArrayList<>();
					newList.add(shipManifest);
					shipManifestsByOrder.put(order, newList);
				} else {
					shipManifestsByOrder.get(order).add(shipManifest);
				}
			} else {
				LOG.error("Skipping manifest due to missing apd po");
			}
		}
		List<StopDto> stopDtos = new ArrayList<>();
		for (CustomerOrder order : shipManifestsByOrder.keySet()) {
			List<ShipManifest> shipManifestsByPo = shipManifestsByOrder
					.get(order);
			
			// Generate a stop per PO
			StopDto stopDto = generateStopDto(order, shipManifestsByPo);
			stopDtos.add(stopDto);
		}
		return stopDtos;
	}

	private StopDto generateStopDto(CustomerOrder customerOrder,
			List<ShipManifest> shipManifests) {
		StopDto stopDto = new StopDto();
		SiteDto siteDto = new SiteDto();
		Account account = customerOrder.getAccount();
		if (account != null) {
			siteDto.setAccount(account.getId().toString());
		}
		Address address = customerOrder.getAddress();
		siteDto.setName(customerOrder.getApdPo().getValue() + ", " + address.getCompany() + ", "
				+ address.getName());
		siteDto.setAddress1(address.getLine1());
		siteDto.setAddress2(address.getLine2());
		Person person = address.getPerson();
		String phone = "";
		if (person != null && person.getPhoneNumbers() != null
				&& person.getPhoneNumbers().size() > 0) {
			PhoneNumber phoneNumber = person.getPhoneNumbers().iterator()
					.next();
			phone = phoneNumber.getAreaCode() + phoneNumber.getLineNumber()
					+ ", " + phoneNumber.getExtension();
		}
		siteDto.setAddress3(address.getName() + ", " + phone);
		siteDto.setCity(address.getCity());
		siteDto.setState(address.getState());
		siteDto.setZip(address.getZip());
		stopDto.setSiteDto(siteDto);
		if (shipManifests != null && !shipManifests.isEmpty()) {
			stopDto.setDeliveryDate(stopConfigBp.getNextDeliveryDate(address.getZip(), shipManifests.get(0).getManifestDate()));
		}
		else {
			stopDto.setDeliveryDate(stopConfigBp.getNextDeliveryDate(address.getZip()));
		}

		// Delivery
		DeliveryDto deliveryDto = generateDeliveryDto(customerOrder,
				shipManifests);
		for (ShipManifest shipManifest : shipManifests) {
			shipManifest.setDeliveryDate(stopDto.getDeliveryDate());
			if (shipManifest.getManifestDate() == null) {
				shipManifest.setManifestDate(new Date());
			}
		}
		stopDto.setDeliveryDto(deliveryDto);
		return stopDto;
	}

	private DeliveryDto generateDeliveryDto(CustomerOrder customerOrder,
			List<ShipManifest> shipManifests) {
		DeliveryDto deliveryDto = new DeliveryDto();
		String poNumber = customerOrder.getApdPo().getValue();
		if (customerOrder.getCustomerPo() != null) {
			poNumber = customerOrder.getCustomerPo().getValue();
		}
		deliveryDto.setDeliveryPo(poNumber);
		deliveryDto.setType(Type.DELIVERY);
		int usscoBoxes = 0;
		int apdBoxes = 0;
		for (ShipManifest shipManifest : shipManifests) {
			if (StringUtils.isEmpty(deliveryDto.getId())) {
				deliveryDto.setId(shipManifest.getId().toString());
			}
			if (shipManifest.getType().equals(ManifestType.USSCO)) {
				usscoBoxes++;
			}
			if (shipManifest.getType().equals(ManifestType.APD)) {
				apdBoxes += shipManifest.getNumBoxes().intValue();
			}
		}
		// Build delivery line
		if (apdBoxes > 0) {
			DeliveryLineDto apdDeliveryLineDto = generateDeliveryLineDto(
					apdBoxes, ManifestType.APD);
			deliveryDto.getDeliveryLineDtos().add(apdDeliveryLineDto);
		}
		if (usscoBoxes > 0) {
			DeliveryLineDto usscoDeliveryLineDto = generateDeliveryLineDto(
					usscoBoxes, ManifestType.USSCO);
			deliveryDto.getDeliveryLineDtos().add(usscoDeliveryLineDto);
		}

		return deliveryDto;
	}

	private DeliveryLineDto generateDeliveryLineDto(int numberOfBoxes,
			ManifestType manifestType) {
		String lineName = TenantConfigRepository.getInstance().getProperty(CurrentTenantIdentifierResolverImpl.getCurrentTenant(), "tenant", "ship_manifest_delivery_line_name");
        String lineDescription = TenantConfigRepository.getInstance().getProperty(CurrentTenantIdentifierResolverImpl.getCurrentTenant(), "tenant", "ship_manifest_delivery_line_description");
        String usscoLineName = TenantConfigRepository.getInstance().getProperty(CurrentTenantIdentifierResolverImpl.getCurrentTenant(), "tenant", "ship_manifest_ussco_delivery_line_name");
        String usscoLineDescription = TenantConfigRepository.getInstance().getProperty(CurrentTenantIdentifierResolverImpl.getCurrentTenant(), "tenant", "ship_manifest_ussco_delivery_line_description");
		
        
		DeliveryLineDto deliveryLineDto = new DeliveryLineDto();
		if (manifestType.equals(ManifestType.APD)) {
			deliveryLineDto.setName(lineName);
			deliveryLineDto.setDescription(lineDescription);
		} else if (manifestType.equals(ManifestType.USSCO)) {
			deliveryLineDto.setName(usscoLineName);
			deliveryLineDto.setDescription(usscoLineDescription);
		}
		deliveryLineDto.setQtyTarget(BigInteger.valueOf(numberOfBoxes));
		deliveryLineDto.setUom(UOM.BOX);
		return deliveryLineDto;
	}

	public class Response {
		private boolean isValid;
		private List<String> messages = new ArrayList<>();

		public boolean isValid() {
			return isValid;
		}

		public void setValid(boolean isValid) {
			this.isValid = isValid;
		}

		public List<String> getMessages() {
			return messages;
		}

		public void setMessages(List<String> messages) {
			this.messages = messages;
		}

	}

	@Override
	public boolean verifyUsscoManifests() {
		List<String> allowedFileNames = allowedUsscoFilenames();
		String jumptrackEnabled = TenantConfigRepository.getInstance().getProperty(CurrentTenantIdentifierResolverImpl.getCurrentTenant(), "tenant", "jumptrackEnabled");
        boolean isEnabled = Boolean.parseBoolean(jumptrackEnabled);
        if (!isEnabled) {
        	LOG.error("Jumptrack disabled");
        	return false;
        }
		List<String> errors = new ArrayList<>();
		List<ShipManifestLog> shipManifestLogs = shipManifestLogDao.getTodaysManifestLogs();
		if(shipManifestLogs == null) {
			errors.add("[ERROR] No logs found for today");
		}else {
			if(shipManifestLogs.size() < allowedFileNames.size()) {
				errors.add("[ERROR] Missing manifest files, expected " + allowedFileNames.size() + " but found " + shipManifestLogs.size());
			}
			Map<String, Integer> fileCount = new HashMap<>();
			for(String fileName : allowedFileNames) {
				fileCount.put(fileName, new Integer(0));
			}
			for(ShipManifestLog shipManifestLog : shipManifestLogs) {
				for(String key : fileCount.keySet()) {
					if(shipManifestLog.getFilename().contains(key)) {
						Integer count = fileCount.get(key);
						fileCount.put(key, count+1);
						break;
					}
				}
				
			}
			
			for(String key : fileCount.keySet()) {
				if( fileCount.get(key) < 1 ) {
					errors.add("[ERROR] " + key + " was not found today.");
				}
				if( fileCount.get(key) > 1 ) {
					errors.add("[ERROR] " + key + " was processed more than once.");
				}
			}			
			
		}
		
		return sendManifestVerificationResults(errors, shipManifestLogs);
	}
	
	private boolean sendManifestVerificationResults(List<String> errors, List<ShipManifestLog> shipManifestLogs) {
		
	        LOG.info("Sending email for Ussco manifest verification report");
	        Properties usscoProperties = TenantConfigRepository.getInstance().getPropertiesByTenantId(CurrentTenantIdentifierResolverImpl.getCurrentTenant(),"ussco.integration");
	        Properties emailProperties = TenantConfigRepository.getInstance().getPropertiesByTenantId(CurrentTenantIdentifierResolverImpl.getCurrentTenant(), "tenant");
	        String from = emailProperties.getProperty("shipManifestReportFrom");
	        Set<String> recipients = new HashSet<>();
	        recipients.add(emailProperties.getProperty("shipManifestReportTo"));
	        Set<String> ccRecipients = new HashSet<>();
	        String cc = emailProperties.getProperty("shipManifestReportCC");
	        if(StringUtils.isNotEmpty(cc)) {
	        	ccRecipients.add(cc);
	        }
	        String subject = usscoProperties.getProperty("shipmanifest.email.subject");
	        
	        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
	        String emailBody = "<h1>Ussco Ship Manifest Validation Report</h1>"
	        		+ "<p>Attempted processing files on "+sdf.format(new Date())+"</p>";
	        emailBody += "<h3>Errors:</h3><p>";
	        for(String error : errors) {
	        	emailBody += "Error: '"+ error +"'<br />";
	        }
	        emailBody += "</p>";
	        emailBody += "<h3>All Processed Files:</h3><p>";
	        if(shipManifestLogs != null) {
		        for(ShipManifestLog shipManifestLog : shipManifestLogs) {
		        	emailBody += "Importing: '"+ shipManifestLog.getFilename() + "'<br />";
		        }
	        }
	        emailBody += "</p>";
	        MimeMessage message = emailService.prepareRawMessage(from, null, null, recipients, ccRecipients, subject, emailBody, null);
			
			return emailService.sendEmail(message);

	}
	
	private List<String> allowedUsscoFilenames() {

    	List<String> allowedFileNames = new ArrayList<>();
    	Properties usscoProperties = TenantConfigRepository.getInstance().getPropertiesByTenantId(CurrentTenantIdentifierResolverImpl.getCurrentTenant(),"ussco.integration");
        String allowedFilenames = usscoProperties.getProperty("allowedManifestFilenames");
    	String[] afl = allowedFilenames.split(",");
    	if(afl != null) {
    		Collections.addAll(allowedFileNames, afl);
    	}
    	return allowedFileNames;
	}

}
