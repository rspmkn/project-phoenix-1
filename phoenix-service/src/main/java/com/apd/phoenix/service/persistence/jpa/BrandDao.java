package com.apd.phoenix.service.persistence.jpa;

import java.util.List;
import com.apd.phoenix.service.model.Brand;

public class BrandDao extends AbstractDao<Brand> {

    public Brand findByUsscoId(String usscoId) {
        Brand searchBrand = new Brand();
        searchBrand.setUsscoId(usscoId);
        List<Brand> resultList = this.searchByExactExample(searchBrand, 0, 1);
        if (resultList != null && !resultList.isEmpty()) {
            return resultList.get(0);
        }
        return null;
    }

    public Brand findByBrandName(String brandName) {
        Brand searchBrand = new Brand();
        searchBrand.setName(brandName);
        List<Brand> resultList = this.searchByExactExample(searchBrand, 0, 1);
        if (resultList != null && !resultList.isEmpty()) {
            return resultList.get(0);
        }
        return null;
    }

}
