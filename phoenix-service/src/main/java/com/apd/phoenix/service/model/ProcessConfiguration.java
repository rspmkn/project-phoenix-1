package com.apd.phoenix.service.model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;

/**
 * This entity stores the association between Process, ProcessVariable, Account, and Credential.
 * 
 * @author RHC
 *
 */

@Entity
@Table(uniqueConstraints = { @UniqueConstraint(columnNames = { "id", "account_id" }),
        @UniqueConstraint(columnNames = { "id", "credential_id" }) })
public class ProcessConfiguration implements Serializable, com.apd.phoenix.service.model.Entity {

    private static final long serialVersionUID = -4823764053921612246L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id = null;
    @Version
    @Column(name = "version", nullable = false)
    private int version = 0;

    @ManyToOne(optional = true)
    private Account account;

    @ManyToOne(optional = false)
    private Process process;

    @ManyToOne(optional = true)
    private Credential credential;

    @OneToMany(mappedBy = "processConfiguration", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private Set<ProcessVariable> processVariables = new HashSet<ProcessVariable>();

    public Long getId() {
        return this.id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public int getVersion() {
        return this.version;
    }

    public void setVersion(final int version) {
        this.version = version;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        if (id != null) {
            return id.equals(((ProcessConfiguration) that).id);
        }
        return super.equals(that);
    }

    @Override
    public int hashCode() {
        if (id != null) {
            return id.hashCode();
        }
        return super.hashCode();
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public Process getProcess() {
        return process;
    }

    public void setProcess(Process process) {
        this.process = process;
    }

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public Set<ProcessVariable> getProcessVariables() {
        return processVariables;
    }

    public void setProcessVariables(Set<ProcessVariable> processVariables) {
        this.processVariables = processVariables;
    }

    /*	@Override
     public String toString()
     {
     String result = getClass().getSimpleName() + " ";
     if (name != null && !name.trim().isEmpty())
     result += "type: " + name;
     return result;
     }
     */
}