package com.apd.phoenix.service.message;

import java.math.BigDecimal;
import com.apd.phoenix.service.invoice.MessageLineItem;

public class AckLineItem extends MessageLineItem {

    /**
     * 
     */
    private static final long serialVersionUID = 6811955926241069194L;

    public AckLineItem(int quantity, String vendorSku, String apdSku, BigDecimal price, String status) {
        super(quantity, vendorSku, apdSku, price, status);
    }
}
