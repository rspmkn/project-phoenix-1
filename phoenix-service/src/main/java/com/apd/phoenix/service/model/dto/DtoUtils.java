/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.service.model.dto;

import java.util.Collection;
import org.apache.commons.lang.StringUtils;
import com.apd.phoenix.service.model.Credential;
import com.apd.phoenix.service.model.factory.DtoFactory;
import java.math.BigDecimal;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author rhc
 */
public class DtoUtils {

    private static final Logger LOG = LoggerFactory.getLogger(DtoUtils.class);

    public static final String ITEM_WEIGHT_SPECIFICATION_TYPE = "Item Weight";

    public static String findCashoutPageFieldLabel(Collection<CashoutPageXFieldDto> cashoutPageXFieldDto, String field) {
        if (StringUtils.isNotBlank(field)) {
            for (CashoutPageXFieldDto pageXField : cashoutPageXFieldDto) {
                if (field.equals(pageXField.getField())) {
                    return pageXField.getLabel();
                }
            }
        }
        return "";
    }

    public static CashoutPageXFieldDto findCashoutPageField(Collection<CashoutPageXFieldDto> cashoutPageXFieldDto,
            String field) {
        if (StringUtils.isNotBlank(field)) {
            for (CashoutPageXFieldDto pageXField : cashoutPageXFieldDto) {
                if (field.equals(pageXField.getField())) {
                    return pageXField;
                }
            }
        }
        return null;
    }

    public static CashoutPageXFieldDto findCashoutPageField(CredentialDto credentialDto, String field) {
        if (credentialDto != null && StringUtils.isNotBlank(field)) {
            return findCashoutPageField(credentialDto.getCashoutPageDto().getCashoutPageXFields(), field);
        }
        else {
            return null;
        }
    }

    public static CashoutPageXFieldDto findCashoutPageField(Credential credential, String field) {
        if (credential != null && StringUtils.isNotBlank(field)) {
            return findCashoutPageField(DtoFactory.createCredentialDto(credential), field);
        }
        else {
            return null;
        }
    }

    //In pounds
    public static BigDecimal calculateOrderWeight(PurchaseOrderDto purchaseOrderDto) {
        BigDecimal weight = BigDecimal.ZERO;
        for (LineItemDto item : purchaseOrderDto.getItems()) {
            final String description = item.getDescription() == null ? item.getDescription() : "";
            BigDecimal weightValue = null;
            if (item.getItem() != null && item.getItem().getItemWeight() != null) {
                weightValue = item.getItem().getItemWeight();
            }
            else if (item.getItem() != null && item.getItem().getSpecifications() != null
                    && item.getItem().getSpecifications().get(ITEM_WEIGHT_SPECIFICATION_TYPE) != null) {
                try {
                    //used for legacy orders, moving forward will use the ItemWeight field.
                    weightValue = new BigDecimal(item.getItem().getSpecifications().get(ITEM_WEIGHT_SPECIFICATION_TYPE));
                }
                catch (NumberFormatException exp) {
                    if (purchaseOrderDto.getVendor() != null && purchaseOrderDto.getVendor().getName().equals("USSCO")) {
                        LOG.debug("Error parsing weight of item " + description, exp);
                        //Sets the default weight to one pound to avoid calling the special 13oz workflow
                        //if all items have unrecognizable weights
                        weight = weight.add(BigDecimal.ONE);
                    }
                }
            }

            if (weightValue != null) {
                try {
                    weight = weight.add(new BigDecimal(item.getQuantity().toString()).multiply(weightValue));
                }
                catch (Exception e) {
                    //Weight is only used in buisness rules for ussco ordes
                    if (purchaseOrderDto.getVendor() != null && purchaseOrderDto.getVendor().getName().equals("USSCO")) {
                        LOG.debug("Error parsing weight of item " + description, e);
                        //Sets the default weight to one pound to avoid calling the special 13oz workflow
                        //if all items have unrecognizable weights
                        weight = weight.add(BigDecimal.ONE);
                    }
                    else {
                        LOG.debug("Error parsing weight of an item.");
                    }
                }
            }
            else {
                //Weight is only used in buisness rules for ussco ordes
                if (purchaseOrderDto.getVendor() != null && purchaseOrderDto.getVendor().getName().equals("USSCO")) {
                    LOG.error("USSCO item " + description + "does not have a valid weight");
                }
                else {
                    LOG.debug("Error parsing weight of an item");
                }
            }
        }
        return weight;
    }
}
