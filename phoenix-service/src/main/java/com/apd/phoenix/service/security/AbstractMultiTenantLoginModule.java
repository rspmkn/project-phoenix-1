package com.apd.phoenix.service.security;

import java.security.acl.Group;
import java.util.Map;
import java.util.regex.Pattern;
import javax.security.auth.Subject;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.login.LoginException;
import javax.security.jacc.PolicyContext;
import javax.security.jacc.PolicyContextException;
import javax.servlet.http.HttpServletRequest;
import org.jboss.security.auth.spi.DatabaseServerLoginModule;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.core.utility.TenantConfigRepository;
import com.apd.phoenix.service.business.IpSubdomainRestriction;

/**
 * This class extends {@link DatabaseServerLoginModule} but takes care of intercepting
 * the incoming HTTPRequest for each authentication attempt, and detecting the current
 * target domain and then takes care of choosing the correct schema for that domain using the
 * {@link TenantConfigRepository} to translate the domain to a schema name.
 * @author rmallred
 *
 */
public abstract class AbstractMultiTenantLoginModule extends DatabaseServerLoginModule {

    protected String currentTenantDomain;
    protected String currentTenantSchema;
    //{SCHEMA} will be replaced at runtime
    protected String loginInfoQuery = "SELECT PASSWORD FROM {SCHEMA}.SYSTEMUSER WHERE LOWER(LOGIN) = LOWER(?)";
    protected static final String[] ADDITIONAL_VALID_OPTIONS = { "loginInfoQuery" };
    private static Logger LOG = LoggerFactory.getLogger(AbstractMultiTenantLoginModule.class);

    @Override
    public void initialize(Subject subject, CallbackHandler callbackHandler, Map<String, ?> sharedState,
            Map<String, ?> options) {

        try {
            //need to get the domain here to determine the tenant
            HttpServletRequest request = (HttpServletRequest) PolicyContext
                    .getContext("javax.servlet.http.HttpServletRequest");
            currentTenantDomain = IpSubdomainRestriction.ipAndDomainInfo(request)[2];
            //translate the domain for the tenant into it's schema name
            currentTenantSchema = TenantConfigRepository.getInstance().getTenantSchemaByDomain(currentTenantDomain);

        }
        catch (PolicyContextException e) {
            LOG.error("Error reading incoming HTTPRequest during authentication.", e);
        }

        addValidOptions(ADDITIONAL_VALID_OPTIONS);
        Object tmp = options.get("loginInfoQuery");
        if (tmp != null) {
            loginInfoQuery = tmp.toString();
        }
        loginInfoQuery = addTenantSchemaSwitchToQuery(loginInfoQuery, currentTenantSchema);
        super.initialize(subject, callbackHandler, sharedState, options);
    }

    protected String addTenantSchemaSwitchToQuery(String query, String tenantSchemaName) {
        String newQuery = query.replaceAll(Pattern.quote("{SCHEMA}"), tenantSchemaName);
        return newQuery;
    }

    @Override
    public String getUsersPassword() throws LoginException {

        super.principalsQuery = addTenantSchemaSwitchToQuery(super.principalsQuery, currentTenantSchema);

        return super.getUsersPassword();

    }

    @Override
    protected Group[] getRoleSets() throws LoginException {

        super.rolesQuery = addTenantSchemaSwitchToQuery(super.rolesQuery, currentTenantSchema);

        return super.getRoleSets();
    }
}
