/*
 * 
 */
package com.apd.phoenix.service.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;
import javax.xml.bind.annotation.XmlRootElement;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.Index;
import com.apd.phoenix.service.business.EntityComparator;
import com.apd.phoenix.service.model.LineItemStatus.LineItemStatusEnum;
import com.apd.phoenix.service.model.OrderStatus.OrderStatusEnum;
import com.apd.phoenix.service.model.ReturnOrder.ReturnOrderStatus;
import com.apd.phoenix.service.model.listener.CustomerOrderListener;

/**
 * This entity contains the information relevant to a single order placed by a customer.
 * 
 * @author RHC
 *
 */
@Entity
@XmlRootElement
@EntityListeners(CustomerOrderListener.class)
public class CustomerOrder implements Serializable, com.apd.phoenix.service.model.Entity {

    private static final long serialVersionUID = 55305987977404446L;

    public static final String APD_PO_VALUE = "APD";
    public static final String BLANKET_PO_VALUE = "blanket";
    public static final String CUSTOMER_PO_VALUE = "Customer";
    private static final String ORDER_PLACED = "PLACED";
    private static final String CUSTOMER_PR_VALUE = "PR";

    /**
     * @return the requisitionName
     */
    public String getRequisitionName() {
        return requisitionName;
    }

    /**
     * @param requisitionName the requisitionName to set
     */
    public void setRequisitionName(String requisitionName) {
        this.requisitionName = requisitionName;
    }

    /**
     * @return the terms
     */
    public Credential.Terms getTerms() {
        return terms;
    }

    /**
     * @param terms the terms to set
     */
    public void setTerms(Credential.Terms terms) {
        this.terms = terms;
    }

    /**
     * @return the shipNotBeforeDate
     */
    public Date getShipNotBeforeDate() {
        return shipNotBeforeDate;
    }

    /**
     * @param shipNotBeforeDate the shipNotBeforeDate to set
     */
    public void setShipNotBeforeDate(Date shipNotBeforeDate) {
        this.shipNotBeforeDate = shipNotBeforeDate;
    }

    /**
     * @return the shipNoLaterDate
     */
    public Date getShipNoLaterDate() {
        return shipNoLaterDate;
    }

    /**
     * @param shipNoLaterDate the shipNoLaterDate to set
     */
    public void setShipNoLaterDate(Date shipNoLaterDate) {
        this.shipNoLaterDate = shipNoLaterDate;
    }

    /**
     * This enum lists the different allowed operations in the cXML specification.
     * 
     * @author RHC
     *
     */
    public enum PunchoutOrderOperation {
        create, inspect, edit;
    }

    public enum OrderOrigin {
        SPECIAL_ORDERS("Special Orders"), INTERNAL_ORDERS("Internal Orders");

        private final String label;

        private OrderOrigin(String label) {
            this.label = label;
        }

        public String getLabel() {
            return this.label;
        }
    }

    public enum MerchandiseTypeCode {
        ZUS0("ZUS0"), ZUS1("ZUS1"), ZUS2("ZUS2"), ZUS3("ZUS3"), ZUS4("ZUS4"), ZUS5("ZUS5"), ZUS6("ZUS6"), ZUS7("ZUS7"), ZUS8(
                "ZUS8"), ZUS9("ZUS9"), ZUSA("ZUSA");

        private String label;

        private MerchandiseTypeCode(String label) {
            this.label = label;
        }

        /**
         * @return the label
         */
        public String getLabel() {
            return label;
        }

        public static MerchandiseTypeCode fromString(String label) {
            for (MerchandiseTypeCode type : MerchandiseTypeCode.values()) {
                if (type.label.equals(label)) {
                    return type;
                }
            }
            return null;

        }
    }

    public enum CurrencyCode {
        USD("USD");

        private String label;

        private CurrencyCode(String label) {
            this.label = label;
        }

        /**
         * @return the label
         */
        public String getLabel() {
            return label;
        }

        public static CurrencyCode fromString(String label) {
            for (CurrencyCode code : CurrencyCode.values()) {
                if (code.label.equals(label)) {
                    return code;
                }
            }
            return null;

        }
    }

    /** The id. */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id = null;

    /** The version. */
    @Version
    @Column(name = "version", nullable = false)
    private int version = 0;

    @Column
    private String buyerCookie;

    @Column
    private String estimatedShipping;

    @Column
    private String paymentTransactionKey;

    @Column
    private BigDecimal estimatedShippingAmount;

    @Column
    private String requisitionName;

    @Column
    private BigDecimal maximumTaxToCharge;

    @Temporal(TemporalType.TIMESTAMP)
    @Index(name = "CUSTORDER_ORDERDATE_IDX")
    private Date orderDate;

    @ManyToOne(fetch = FetchType.EAGER)
    private OrderType type;

    @Column(nullable = false)
    private BigDecimal orderTotal;

    @ManyToOne(fetch = FetchType.EAGER)
    @Fetch(FetchMode.JOIN)
    private SystemUser user;

    @ManyToOne(fetch = FetchType.EAGER)
    @Fetch(FetchMode.JOIN)
    private Account account;

    @ManyToOne(fetch = FetchType.EAGER)
    @Fetch(FetchMode.JOIN)
    private Credential credential;

    @OneToMany(mappedBy = "customerOrder", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Set<OrderLog> orderLogs = new HashSet<OrderLog>();

    @OneToMany(mappedBy = "customerOrder", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Set<ServiceLog> serviceLogs = new HashSet<ServiceLog>();

    @OneToMany(mappedBy = "order", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
    private Set<Shipment> shipments = new HashSet<Shipment>();

    @ManyToOne(fetch = FetchType.EAGER)
    private OrderStatus status;

    @ManyToOne(fetch = FetchType.EAGER)
    private OrderStatus paymentStatus;

    @ManyToOne(fetch = FetchType.EAGER)
    private OrderStatus customerStatus;

    @ManyToMany(mappedBy = "orders", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @Fetch(FetchMode.SUBSELECT)
    private Set<PoNumber> poNumbers = new HashSet<PoNumber>();

    @ManyToOne(fetch = FetchType.LAZY)
    private CardInformation cardInformation;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private PaymentInformation paymentInformation;

    @OneToMany(mappedBy = "order", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Set<NotificationProperties> notificationProperties = new HashSet<NotificationProperties>();

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @Fetch(FetchMode.SUBSELECT)
    @JoinColumn(name = "order_id")
    private Set<Comment> comments = new HashSet<Comment>();

    @OneToMany(mappedBy = "parentOrder", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
    @Fetch(FetchMode.SUBSELECT)
    private Set<CustomerOrder> children = new HashSet<CustomerOrder>();

    @OneToMany(mappedBy = "order", fetch = FetchType.LAZY, cascade = { CascadeType.PERSIST, CascadeType.MERGE,
            CascadeType.REFRESH, CascadeType.DETACH })
    @Fetch(FetchMode.SUBSELECT)
    private Set<ReturnOrder> returnOrders = new HashSet<ReturnOrder>();

    @ManyToOne
    private CustomerOrder parentOrder;

    @ManyToOne(cascade = CascadeType.ALL)
    private AssignedCostCenter assignedCostCenter;

    @Column
    private String assignedCostCenterLabel;

    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private Address address;

    @OneToMany(mappedBy = "order", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<LineItem> items = new HashSet<LineItem>();

    @Enumerated(EnumType.STRING)
    private PunchoutOrderOperation operationAllowed;

    @Enumerated(EnumType.STRING)
    private OrderOrigin orderOrigin;

    @Column
    private boolean taxAssigned = true;

    @Temporal(TemporalType.DATE)
    private Date shipDate;

    @Temporal(TemporalType.DATE)
    private Date deliveryDate;

    @Temporal(TemporalType.DATE)
    private Date cancelDate;

    @Temporal(TemporalType.DATE)
    private Date requestedDeliveryDate;

    @Temporal(TemporalType.DATE)
    private Date shipNotBeforeDate;

    @Temporal(TemporalType.DATE)
    private Date shipNoLaterDate;

    @Enumerated(EnumType.STRING)
    private MerchandiseTypeCode merchandiseTypeCode;

    @Enumerated(EnumType.STRING)
    private CurrencyCode currencyCode;

    @Enumerated(EnumType.STRING)
    private Credential.Terms terms;

    @Column(length = 4000)
    private String orderExtrasArtifact;

    @Column(length = 4000)
    private String shipToExtrasArtifact;

    @Column(length = 4000)
    private String approvalComment;

    @Column
    private String apdSalesPerson;

    //determines whether the shipping method for EDI should be overridden with "will call"
    @Column(nullable = false)
    private boolean willCallEdiOverride = false;

    @Column(nullable = false)
    private boolean wrapAndLabel = false;

    @Column(nullable = false)
    private boolean adot = false;

    @Column
    private String orderPayloadId;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @Fetch(FetchMode.SUBSELECT)
    @JoinColumn(name = "order_id")
    private Set<OrderAttachment> attachments = new HashSet<OrderAttachment>();

    @Column
    private Integer customerRevisionNumber = 1;

    @Column
    private Date customerRevisionDate = new Date();

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
    @Fetch(FetchMode.SUBSELECT)
    @JoinColumn(name = "order_id")
    private Set<TaxRateAtPurchase> taxRatesAtPurchase = new HashSet<TaxRateAtPurchase>();

    @Column
    private Integer resendAttempts = 0;

    @Column(nullable = false)
    private Boolean passedInitialValidation = false;

    @Temporal(TemporalType.DATE)
    private Date expiryDate;

    public Date getLastShipDate() {
        Date toReturn = null;
        for (Shipment shipment : getShipments()) {
            if (toReturn == null || toReturn.before(shipment.getShipTime())) {
                toReturn = shipment.getShipTime();
            }
        }
        return toReturn;
    }

    /**
     * Gets the id.
     *
     * @return the id
     */
    public Long getId() {
        return this.id;
    }

    /**
     * Sets the id.
     *
     * @param id the new id
     */
    public void setId(final Long id) {
        this.id = id;
    }

    /**
     * Gets the version.
     *
     * @return the version
     */
    public int getVersion() {
        return this.version;
    }

    /**
     * Sets the version.
     *
     * @param version the new version
     */
    public void setVersion(final int version) {
        this.version = version;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        if (id != null) {
            return id.equals(((CustomerOrder) that).id);
        }
        return super.equals(that);
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        if (id != null) {
            return id.hashCode();
        }
        return super.hashCode();
    }

    /**
     * Gets the user.
     *
     * @return the user
     */
    public SystemUser getUser() {
        return this.user;
    }

    /**
     * Sets the user.
     *
     * @param user the new user
     */
    public void setUser(final SystemUser user) {
        this.user = user;
    }

    public CurrencyCode getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(CurrencyCode currencyCode) {
        this.currencyCode = currencyCode;
    }

    /**
     * Gets the account.
     *
     * @return the account
     */
    public Account getAccount() {
        return this.account;
    }

    /**
     * Sets the account.
     *
     * @param account the new account
     */
    public void setAccount(final Account account) {
        this.account = account;
    }

    /**
     * Gets the credential.
     *
     * @return the credential
     */
    public Credential getCredential() {
        return this.credential;
    }

    /**
     * Sets the credential.
     *
     * @param credential the new credential
     */
    public void setCredential(final Credential credential) {
        this.credential = credential;
    }

    public Set<Shipment> getShipments() {
        return this.shipments;
    }

    public void setShipments(final Set<Shipment> shipments) {
        this.shipments = shipments;
    }

    public OrderStatus getStatus() {
        return this.status;
    }

    public void setStatus(final OrderStatus status) {
        this.status = status;
    }

    public OrderStatus getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(OrderStatus paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public OrderStatus getCustomerStatus() {
        return customerStatus;
    }

    public void setCustomerStatus(OrderStatus customerStatus) {
        this.customerStatus = customerStatus;
    }

    public Set<PoNumber> getPoNumbers() {
        return this.poNumbers;
    }

    public void setPoNumbers(final Set<PoNumber> poNumbers) {
        this.poNumbers = poNumbers;
    }

    @Override
    public String toString() {
        String result = getClass().getSimpleName() + " ";
        return result;
    }

    //use order.getPaymentInformation.getCard()
    @Deprecated
    public CardInformation getCardInformation() {
        return this.cardInformation;
    }

    //use order.getPaymentInformation.setCard()
    @Deprecated
    public void setCardInformation(final CardInformation cardInformation) {
        this.cardInformation = cardInformation;
    }

    public PaymentInformation getPaymentInformation() {
        return this.paymentInformation;
    }

    public void setPaymentInformation(final PaymentInformation paymentInformation) {
        this.paymentInformation = paymentInformation;
    }

    public Set<NotificationProperties> getNotificationProperties() {
        return this.notificationProperties;
    }

    public void setNotificationProperties(final Set<NotificationProperties> notificationProperties) {
        this.notificationProperties = notificationProperties;
    }

    public Set<Comment> getComments() {
        return this.comments;
    }

    public void setComments(final Set<Comment> comments) {
        this.comments = comments;
    }

    public Set<CustomerOrder> getChildren() {
        return this.children;
    }

    public void setChildren(final Set<CustomerOrder> children) {
        this.children = children;
    }

    public CustomerOrder getParentOrder() {
        return this.parentOrder;
    }

    public void setParentOrder(final CustomerOrder parentOrder) {
        this.parentOrder = parentOrder;
    }

    public AssignedCostCenter getAssignedCostCenter() {
        return this.assignedCostCenter;
    }

    public void setAssignedCostCenter(final AssignedCostCenter assignedCostCenter) {
        this.assignedCostCenter = assignedCostCenter;
    }

    public String getAssignedCostCenterLabel() {
        return assignedCostCenterLabel;
    }

    public void setAssignedCostCenterLabel(String assignedCostCenterLabel) {
        this.assignedCostCenterLabel = assignedCostCenterLabel;
    }

    public Set<OrderLog> getOrderLogs() {
        return orderLogs;
    }

    public void setOrderLogs(Set<OrderLog> orderLogs) {
        this.orderLogs = orderLogs;
    }

    public Set<ServiceLog> getServiceLogs() {
        return serviceLogs;
    }

    public void setServiceLogs(Set<ServiceLog> serviceLogs) {
        this.serviceLogs = serviceLogs;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    /**
     * @Precondition
     * this.orderHistories has been loaded
     * @return
     */
    public Date getOrderPlacedDate() {
        Date toReturn = null;
        for (OrderLog orderLog : this.getOrderLogs()) {
            if (toReturn == null
                    || (orderLog.getOrderStatus() != null && orderLog.getOrderStatus().getValue().equals(ORDER_PLACED))) {
                toReturn = orderLog.getUpdateTimestamp();
            }
        }
        return toReturn;
    }

    /**
     * @return the APD PO number
     * @Precondition
     * this.poNumbers has been loaded
     */
    public PoNumber getApdPo() {
        return this.selectPoNumberByType(APD_PO_VALUE);
    }

    /**
     * @return the Customer PO number
     * @Precondition
     * this.poNumbers has been loaded
     */
    public PoNumber getCustomerPo() {
        return this.selectPoNumberByType(CUSTOMER_PO_VALUE);
    }

    /**
     * @return the Customer PR number
     * @Precondition
     * this.poNumbers has been loaded
     */
    public PoNumber getCustomerPr() {
        return this.selectPoNumberByType(CUSTOMER_PR_VALUE);
    }

    /**
     * @return the Blanket PO number
     * @Precondition
     * this.poNumbers has been loaded
     */
    public PoNumber getBlanketPo() {
        return this.selectPoNumberByType(BLANKET_PO_VALUE);
    }

    /**
     * This first checks if customer provided po number.  
     * If not, it returns a blanket po number if one is associated with the order.  
     * If not, the APD order number is returned.
     * @return the customer order reference PoNumber
     */
    public PoNumber getCustomerReferenceNumber() {
        PoNumber customerRef = getCustomerPo();
        if (customerRef == null) {
            customerRef = getBlanketPo();
        }
        if (customerRef == null) {
            customerRef = getApdPo();
        }
        return customerRef;
    }

    private PoNumber selectPoNumberByType(String type) {
        PoNumber toReturn = null;
        for (PoNumber poNumber : this.getPoNumbers()) {
            if (poNumber.getType().getName().equals(type)) {
                toReturn = poNumber;
            }
        }
        return toReturn;
    }

    public String getBuyerCookie() {
        return buyerCookie;
    }

    public void setBuyerCookie(String buyerCookie) {
        this.buyerCookie = buyerCookie;
    }

    public Date getRequestedDeliveryDate() {
        return requestedDeliveryDate;
    }

    public void setRequestedDeliveryDate(Date requestedDeliveryDate) {
        this.requestedDeliveryDate = requestedDeliveryDate;
    }

    public Set<LineItem> getItems() {
        return this.items;
    }

    public void setItems(final Set<LineItem> items) {
        this.items = items;
    }

    public Set<LineItem> getNonFeeItems() {
        Set<LineItem> nonFeeItems = new HashSet<LineItem>();
        if (this.items != null) {
            for (LineItem item : this.items) {
                if (!item.isFee()) {
                    nonFeeItems.add(item);
                }
            }
        }
        return nonFeeItems;
    }

    public PunchoutOrderOperation getOperationAllowed() {
        return this.operationAllowed;
    }

    public void setOperationAllowed(final PunchoutOrderOperation operationAllowed) {
        this.operationAllowed = operationAllowed;
    }

    public OrderOrigin getOrderOrigin() {
        return orderOrigin;
    }

    public void setOrderOrigin(OrderOrigin orderOrigin) {
        this.orderOrigin = orderOrigin;
    }

    public boolean isTaxAssigned() {
        return taxAssigned;
    }

    public void setTaxAssigned(boolean taxAssigned) {
        this.taxAssigned = taxAssigned;
    }

    public String getEstimatedShipping() {
        return estimatedShipping;
    }

    public void setEstimatedShipping(String estimatedShipping) {
        this.estimatedShipping = estimatedShipping;
    }

    public String getPaymentTransactionKey() {
        return paymentTransactionKey;
    }

    public void setPaymentTransactionKey(String paymentTransactionKey) {
        this.paymentTransactionKey = paymentTransactionKey;
    }

    public BigDecimal getEstimatedShippingAmount() {
        return estimatedShippingAmount;
    }

    public void setEstimatedShippingAmount(BigDecimal estimatedShippingAmount) {
        this.estimatedShippingAmount = estimatedShippingAmount;
    }

    public BigDecimal getCalculatedShippingAmount() {
        BigDecimal toReturn = BigDecimal.ZERO;
        for (LineItem item : this.getItems()) {
            BigDecimal alreadyShipped = item.getPaidShipping();
            //uses the calculated "already shipped" amount, if all items shipped 
            //or the already shipped is greater than the initial estimate
            if (item.getEstimatedShippingAmount() != null && item.getQuantityRemaining() != 0
                    && item.getEstimatedShippingAmount().compareTo(alreadyShipped) > 0) {
                toReturn = toReturn.add(item.getEstimatedShippingAmount());
            }
            else {
                toReturn = toReturn.add(alreadyShipped);
            }
        }
        return toReturn;
    }

    public BigDecimal getMaximumTaxToCharge() {
        return maximumTaxToCharge;
    }

    public void setMaximumTaxToCharge(BigDecimal maximumToCharge) {
        this.maximumTaxToCharge = maximumToCharge;
    }

    public BigDecimal getCalculatedTaxAmount() {
        BigDecimal toReturn = this.maximumTaxToCharge != null ? this.maximumTaxToCharge : BigDecimal.ZERO;
        for (LineItem item : this.getItems()) {
            BigDecimal alreadyTaxed = BigDecimal.ZERO;
            //as of this writing, the following calculations are unnecessary; the "alreadyTaxed" 
            //amount will always evaluate to the maximum tax to charge, if all of the items have been 
            //shipped. So, the operations a few lines down (where the maximum tax is subtracted, and
            //the already taxed is added) don't change toReturn. However, if later it becomes possible
            //to fully ship an item without fully taxing it, this code will support that action.
            for (LineItemXShipment lixs : item.getItemXShipments()) {
                if (lixs.getTotalTaxAmount() != null) {
                    alreadyTaxed = alreadyTaxed.add(lixs.getTotalTaxAmount());
                }
            }
            //uses the calculated "already taxed" amount, if all items shipped 
            if (item.getQuantityRemaining() == 0) {
                if (item.getMaximumTaxToCharge() != null) {
                    toReturn = (toReturn.subtract(item.getMaximumTaxToCharge())).add(alreadyTaxed);
                }
            }
        }
        for (ReturnOrder returnOrder : this.getReturnOrders()) {
            for (LineItemXReturn lixr : returnOrder.getItems()) {
                if (returnOrder.getStatus() != null && !returnOrder.getStatus().equals(ReturnOrderStatus.CLOSED)
                        && lixr.getTotalTaxAmount() != null) {
                    toReturn = toReturn.subtract(lixr.getTotalTaxAmount());
                }
            }
        }
        return toReturn;
    }

    public Date getShipDate() {
        return shipDate;
    }

    public void setShipDate(Date shipDate) {
        this.shipDate = shipDate;
    }

    public Date getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDate(Date deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    public Date getCancelDate() {
        return cancelDate;
    }

    public void setCancelDate(Date cancelDate) {
        this.cancelDate = cancelDate;
    }

    public BigDecimal getSubtotal() {
        BigDecimal toReturn = BigDecimal.ZERO;
        for (LineItem item : this.getItems()) {
            toReturn = toReturn.add(item.getTotalPrice());
        }
        return toReturn;
    }

    public BigDecimal getCalculatedSubtotal() {
        BigDecimal toReturn = BigDecimal.ZERO;
        for (LineItem item : this.getItems()) {
            if (item.getTotalPrice() != null) {
                toReturn = toReturn.add(item.getTotalPrice());
            }
        }
        for (ReturnOrder returnOrder : this.getReturnOrders()) {
            for (LineItemXReturn lixr : returnOrder.getItems()) {
                if (returnOrder.getStatus() != null && !returnOrder.getStatus().equals(ReturnOrderStatus.CLOSED)
                        && lixr.getSubTotalAmount() != null) {
                    toReturn = toReturn.subtract(lixr.getSubTotalAmount());
                }
            }
        }
        for (LineItem item : this.getItems()) {
            for (CanceledQuantity cancel : item.getCanceledQuantities()) {
                if (cancel.getSubTotalAmount() != null) {
                    toReturn = toReturn.subtract(cancel.getSubTotalAmount());
                }
            }
        }
        return toReturn;
    }

    public BigDecimal getSubtotalNoFees() {
        BigDecimal toReturn = BigDecimal.ZERO;
        for (LineItem item : this.getItems()) {
            if (!item.isFee()) {
                toReturn = toReturn.add(item.getTotalPrice());
            }
        }
        return toReturn;
    }

    public BigDecimal getCalculatedTotal() {
        return this.getCalculatedSubtotal().add(this.getCalculatedShippingAmount()).add(this.getCalculatedTaxAmount());
    }

    public BigDecimal getCalculatedPaidTotal() {
        BigDecimal toReturn = BigDecimal.ZERO;
        if (this.shipments != null) {
            for (Shipment shipment : this.shipments) {
                if (shipment != null && shipment.getItemXShipments() != null) {
                    for (LineItemXShipment lixs : shipment.getItemXShipments()) {
                        if (lixs != null && lixs.getPaid() != null && lixs.getPaid() && lixs.getTotalAmount() != null) {
                            toReturn = toReturn.add(lixs.getTotalAmount());
                        }
                    }
                }
            }
        }
        if (this.returnOrders != null) {
            for (ReturnOrder returnOrder : this.returnOrders) {
                if (returnOrder != null && returnOrder.getItems() != null
                        && ReturnOrderStatus.RECONCILED.equals(returnOrder.getStatus())) {
                    for (LineItemXReturn lixr : returnOrder.getItems()) {
                        if (lixr != null && lixr.getTotalAmount() != null) {
                            toReturn = toReturn.subtract(lixr.getTotalAmount());
                        }
                    }
                }
            }
        }
        return toReturn;
    }

    public Date getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(Date orderDate) {
        this.orderDate = orderDate;
    }

    public OrderType getType() {
        return type;
    }

    public void setType(OrderType type) {
        this.type = type;
    }

    public BigDecimal getOrderTotal() {
        return this.orderTotal;
    }

    public void setOrderTotal(BigDecimal orderTotal) {
        this.orderTotal = orderTotal;
    }

    /**
     * @return the merchandiseTypeCode
     */
    public MerchandiseTypeCode getMerchandiseTypeCode() {
        return merchandiseTypeCode;
    }

    /**
     * @param merchandiseTypeCode the merchandiseTypeCode to set
     */
    public void setMerchandiseTypeCode(MerchandiseTypeCode merchandiseTypeCode) {
        this.merchandiseTypeCode = merchandiseTypeCode;
    }

    public List<CreditCardTransactionLog> getCreditCardTransactionLogs(){
        List<CreditCardTransactionLog> transactions = new ArrayList<>();
        for(OrderLog log: getOrderLogs()){
            if(log instanceof CreditCardTransactionLog){
                transactions.add((CreditCardTransactionLog) log);
            }            
        }
        Collections.sort(transactions, new EntityComparator());
        return transactions;
    }

    public String getOrderExtrasArtifact() {
        return orderExtrasArtifact;
    }

    public void setOrderExtrasArtifact(String orderExtrasArtifact) {
        this.orderExtrasArtifact = orderExtrasArtifact;
    }

    public String getShipToExtrasArtifact() {
        return shipToExtrasArtifact;
    }

    public void setShipToExtrasArtifact(String shipToExtrasArtifact) {
        this.shipToExtrasArtifact = shipToExtrasArtifact;
    }

    public String getApdSalesPerson() {
        return apdSalesPerson;
    }

    public void setApdSalesPerson(String apdSalesPerson) {
        this.apdSalesPerson = apdSalesPerson;
    }

    public void refreshOrderTotal() {
        if (this.getEstimatedShippingAmount() == null) {
            this.estimatedShippingAmount = BigDecimal.ZERO;
        }
        if (this.getMaximumTaxToCharge() == null) {
            this.maximumTaxToCharge = BigDecimal.ZERO;
        }
        this.setOrderTotal(this.getEstimatedShippingAmount().add(this.getMaximumTaxToCharge().add(this.getSubtotal())));
    }

    public Set<ReturnOrder> getReturnOrders() {
        return returnOrders;
    }

    public void setReturnOrders(Set<ReturnOrder> returnOrders) {
        this.returnOrders = returnOrders;
    }

    public String getApprovalComment() {
        return approvalComment;
    }

    public void setApprovalComment(String approvalComment) {
        this.approvalComment = approvalComment;
    }

    public boolean isWillCallEdiOverride() {
        return willCallEdiOverride;
    }

    public void setWillCallEdiOverride(boolean willCallEdiOverride) {
        this.willCallEdiOverride = willCallEdiOverride;
    }

    public Set<OrderAttachment> getAttachments() {
        return attachments;
    }

    public void setAttachments(Set<OrderAttachment> attachments) {
        this.attachments = attachments;
    }

    public void addNotificationProperties(NotificationProperties notificationProperties) {
        if (this.getNotificationProperties() == null) {
            this.setNotificationProperties(new HashSet<NotificationProperties>());
        }
        notificationProperties.setOrder(this);
        this.getNotificationProperties().add(notificationProperties);
    }

    public String getOrderPayloadId() {
        return orderPayloadId;
    }

    public void setOrderPayloadId(String orderPayloadId) {
        this.orderPayloadId = orderPayloadId;
    }

    public boolean isWrapAndLabel() {
        return wrapAndLabel;
    }

    public void setWrapAndLabel(boolean wrapAndLabel) {
        this.wrapAndLabel = wrapAndLabel;
    }

    public boolean isAdot() {
        return adot;
    }

    public void setAdot(boolean adot) {
        this.adot = adot;
    }

    public Integer getCustomerRevisionNumber() {
        return customerRevisionNumber;
    }

    public void setCustomerRevisionNumber(Integer customerRevisionNumber) {
        this.customerRevisionNumber = customerRevisionNumber;
    }

    public Date getCustomerRevisionDate() {
        return customerRevisionDate;
    }

    public void setCustomerRevisionDate(Date customerRevisionDate) {
        this.customerRevisionDate = customerRevisionDate;
    }

    public Set<TaxRateAtPurchase> getTaxRatesAtPurchase() {
        return taxRatesAtPurchase;
    }

    public void setTaxRatesAtPurchase(Set<TaxRateAtPurchase> taxRatesAtPurchase) {
        this.taxRatesAtPurchase = taxRatesAtPurchase;
    }

    public Integer getResendAttempts() {
        return this.resendAttempts;
    }

    public void setResendAttempts(Integer resendAttempts) {
        this.resendAttempts = resendAttempts;
    }

    public String findLastOrderConfirmationId() {
        String lastOrderConfirmationId = null;
        Date mostRecentDate = null;
        for (OrderLog orderLog : this.getOrderLogs()) {
            //Only applies to integrated order messages
            if (orderLog.getMessageMetadata() != null
                    && (MessageMetadata.MessageType.CXML.equals(orderLog.getMessageMetadata().getMessageType())
                            || MessageMetadata.MessageType.EDI.equals(orderLog.getMessageMetadata().getMessageType()) || MessageMetadata.MessageType.XCBL
                            .equals(orderLog.getMessageMetadata().getMessageType()))) {
                switch (orderLog.getEventType()) {
                    case ORDER_ACKNOWLEDGEMENT:
                        if (mostRecentDate == null || mostRecentDate.before(orderLog.getUpdateTimestamp())) {
                            lastOrderConfirmationId = orderLog.getMessageMetadata().getInterchangeId();
                            mostRecentDate = orderLog.getUpdateTimestamp();
                        }
                        break;
                    case ORDER_ACKNOWLEDGEMENT_ERRORS:
                        if (mostRecentDate == null || mostRecentDate.before(orderLog.getUpdateTimestamp())) {
                            lastOrderConfirmationId = orderLog.getMessageMetadata().getInterchangeId();
                            mostRecentDate = orderLog.getUpdateTimestamp();
                        }
                        break;
                    default:
                        //Do nothing
                        break;
                }
            }
        }
        return lastOrderConfirmationId;
    }

    public BigDecimal getTotalRejected() {
        BigDecimal totalCancelled = BigDecimal.ZERO;
        for (LineItem item : this.getItems()) {

            if (item.getStatus() == null || !item.getStatus().getValue().equals(LineItemStatusEnum.REJECTED.getValue())) {
                totalCancelled.add(item.getUnitPrice().multiply(BigDecimal.valueOf(item.getQuantity())));
            }
        }
        return totalCancelled;
    }

    public BigDecimal getTotalCancelled() {
        BigDecimal totalCancelled = BigDecimal.ZERO;
        for (LineItem item : this.getItems()) {
            Integer cancelled = item.getQuantityCancelled();
            if (cancelled > 0) {
                totalCancelled.add(item.getUnitPrice().multiply(BigDecimal.valueOf(cancelled)));
            }
        }
        return totalCancelled;
    }

    public BigDecimal getTotalReturned() {
        BigDecimal totalReturned = BigDecimal.ZERO;
        for (LineItem item : this.getItems()) {
            Integer returned = item.getQuantityReturned();
            if (returned > 0) {
                totalReturned.add(item.getUnitPrice().multiply(BigDecimal.valueOf(returned)));
            }
        }
        return totalReturned;
    }

    public BigDecimal getTotalBackOrder() {
        BigDecimal totalBackOder = BigDecimal.ZERO;
        for (LineItem item : this.getItems()) {
            BigInteger backOder = item.getBackorderedQuantity();
            if (backOder != null && backOder.intValue() > 0) {
                totalBackOder.add(item.getUnitPrice().multiply(BigDecimal.valueOf(backOder.intValue())));
            }
        }
        return totalBackOder;
    }

    /**
     * Determines if an order should be allowed to be processed by the system. If this method returns FALSE for 
     * an order, then that order is "dead", and no actions will occur for it.
     * 
     * @return boolean indicating whether actions can be performed on the order
     */
    public boolean hasAllowedStatus() {
        return this.getStatus() == null
                || !(this.getStatus().getValue().equals(OrderStatusEnum.INVALIDATED.getValue())
                        || this.getStatus().getValue().equals(OrderStatusEnum.MANUALLY_RESOLVED.getValue()) || this
                        .getStatus().getValue().equals(OrderStatusEnum.CAPS.getValue()));
    }

    public int getMaxLineNumber() {
        int number = 0;
        for (LineItem item : this.getItems()) {
            if (item.getLineNumber() != null && item.getLineNumber() > number) {
                number = item.getLineNumber();
            }
        }
        return number;
    }

    /**
     * Called by entity listeners to calculate the order's line numbers when the order or line item is persisted.
     */
    public void calculateLineNumbers() {
    	List<LineItem> sortedList = new ArrayList<>();
    	sortedList.addAll(this.getItems());
    	Collections.sort(sortedList, new EntityComparator());
    	for (LineItem item : sortedList) {
    		if (item.getId() == null) {
    			item.setLineNumber(null);
    		}
    	}
    	for (LineItem item : sortedList) {
    		if (item.getId() == null) {
	    		item.setLineNumber(this.getMaxLineNumber() + 1);
	    		item.setRecalculateLineNumberOnPersist(false);
    		}
    	}
    }

    public Boolean getPassedInitialValidation() {
        return passedInitialValidation;
    }

    public void setPassedInitialValidation(Boolean passedInitialValidation) {
        this.passedInitialValidation = passedInitialValidation;
    }

    public Date getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(Date expiryDate) {
        this.expiryDate = expiryDate;
    }
}
