/*
 * 
 */
package com.apd.phoenix.service.model.dto;

import java.io.Serializable;

public class UnitOfMeasureDto implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -4654385736068703930L;

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}