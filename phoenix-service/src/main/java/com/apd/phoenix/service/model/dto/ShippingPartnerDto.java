package com.apd.phoenix.service.model.dto;

import java.io.Serializable;

public class ShippingPartnerDto implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1541011025192257575L;

    private String name;

    private String scacCode;

    private String duns;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getScacCode() {
        return scacCode;
    }

    public void setScacCode(String scacCode) {
        this.scacCode = scacCode;
    }

    public String getDuns() {
        return duns;
    }

    public void setDuns(String duns) {
        this.duns = duns;
    }

}
