package com.apd.phoenix.service.business;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import org.apache.commons.lang.StringUtils;
import com.apd.phoenix.service.model.LineItemXReturn;
import com.apd.phoenix.service.model.LineItemXShipment;
import com.apd.phoenix.service.model.CustomerOrder;
import com.apd.phoenix.service.model.ReturnOrder;
import com.apd.phoenix.service.model.ReturnOrder.ReturnOrderStatus;
import com.apd.phoenix.service.model.dto.ReturnOrderDto;
import com.apd.phoenix.service.persistence.jpa.ReturnOrderDao;

/**
 * This class provides business process methods for Addresses.
 * 
 * @author RHC
 *
 */
@Stateless
@LocalBean
public class ReturnOrderBp extends AbstractBp<ReturnOrder> {

    @Inject
    CustomerOrderBp customerOrderBp;

    @Inject
    LineItemXReturnBp lineItemXReturnBp;

    /**
     * This method is injected with the correct Dao for initialization
     * 
     * @param dao
     */
    @Inject
    public void initDao(ReturnOrderDao dao) {
        this.dao = dao;
    }

    public ReturnOrder getUnreconciledReturnOrder(ReturnOrderDto returnOrderDto) {
        if (returnOrderDto == null || StringUtils.isBlank(returnOrderDto.getRaNumber())
                || returnOrderDto.getOrder() == null || StringUtils.isBlank(returnOrderDto.getOrder().getApdPoNumber())) {
            return null;
        }
        CustomerOrder order = customerOrderBp.searchByApdPo(returnOrderDto.getOrder().getApdPoNumber());
        for (ReturnOrder returnOrder : order.getReturnOrders()) {
            if (returnOrderDto.getRaNumber().equals(returnOrder.getRaNumber())
                    && ReturnOrderStatus.UNRECONCILED.equals(returnOrder.getStatus())) {
                return returnOrder;
            }
        }
        return null;
    }

    public List<ReturnOrder> returnSearch(String raNumber, String poNumber, String login, Date startDate, Date endDate,
            Boolean closed, Boolean reconciled, Boolean unreconciled) {
        return ((ReturnOrderDao) this.dao).returnSearch(raNumber, poNumber, login, startDate, endDate, closed,
                reconciled, unreconciled);
    }

    public void detach(ReturnOrder toDetach) {
        ((ReturnOrderDao) this.dao).detach(toDetach);
    }

    public void reconcile(ReturnOrder order) {
        order.setReconciledDate(new Date());
        order.setStatus(ReturnOrderStatus.RECONCILED);
        this.update(order);
    }

    public BigDecimal getReturnTotal(ReturnOrder returnOrder) {
        BigDecimal quantity = BigDecimal.ZERO;
        if (returnOrder != null) {
            if (returnOrder.getRestockingFee() != null && returnOrder.getRestockingFee().getUnitPrice() != null) {
                quantity = quantity.subtract(returnOrder.getRestockingFee().getUnitPrice());
            }
            for (LineItemXReturn item : returnOrder.getItems()) {
                item = lineItemXReturnBp.checkForNullReturnShipping(item);
                quantity = quantity.add(item.getTotalAmount());
            }
        }
        return quantity;
    }

    public ReturnOrder hydrateForReturnDetails(ReturnOrder returnOrder) {
        returnOrder = this.dao.findById(returnOrder.getId(), ReturnOrder.class);
        for (LineItemXReturn returnItem : returnOrder.getItems()) {
            returnItem = lineItemXReturnBp.checkForNullReturnShipping(returnItem);
            if (returnItem.getLineItem() != null) {
                returnItem.getLineItem().getItemXShipments().size();
                returnItem.getLineItem().getItemXReturns().size();
                if (returnItem.getLineItem().getExpected() != null) {
                    returnItem.getLineItem().getExpected().toString();
                }
            }
        }
        if (returnOrder.getOrder() != null) {
            returnOrder.getOrder().getPoNumbers().size();
        }
        return returnOrder;

    }

}
