package com.apd.phoenix.service.persistence.jpa;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import com.apd.phoenix.service.model.TaxRate;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * TaxRate DAO stub
 *
 * @author RHC
 *
 */
@Stateless
@LocalBean
public class TaxRateDao extends AbstractDao<TaxRate> {

    private static final Logger logger = LoggerFactory.getLogger(TaxRateDao.class);

    public boolean isValidZipCode(String zipCode) {
        try {
            String hql = "SELECT toReturn from TaxRate toReturn where toReturn.zip=:zipCode";
            //Creates the query
            Query query = entityManager.createQuery(hql);
            query.setParameter("zipCode", zipCode);
            Set<TaxRate> toReturn = new HashSet<>(query.getResultList());
            return !toReturn.isEmpty();
        } catch (Exception ex) {
            logger.error("Error searching for matching TaxRates", ex);
            return false;
        }
    }
}
