package com.apd.phoenix.service.persistence.jpa;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.Query;
import javax.persistence.TemporalType;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.business.CustomerOrderSearchCriteria;
import com.apd.phoenix.service.model.Account;
import com.apd.phoenix.service.model.AccountXAccountPropertyType;
import com.apd.phoenix.service.model.CanceledQuantity;
import com.apd.phoenix.service.model.Comment;
import com.apd.phoenix.service.model.CustomerOrder;
import com.apd.phoenix.service.model.LineItem;
import com.apd.phoenix.service.model.LineItemStatus.LineItemStatusEnum;
import com.apd.phoenix.service.model.LineItemXReturn;
import com.apd.phoenix.service.model.LineItemXShipment;
import com.apd.phoenix.service.model.MessageMetadata;
import com.apd.phoenix.service.model.NotificationProperties;
import com.apd.phoenix.service.model.OrderLog;
import com.apd.phoenix.service.model.OrderAttachment;
import com.apd.phoenix.service.model.OrderStatus;
import com.apd.phoenix.service.model.OrderStatus.OrderStatusEnum;
import com.apd.phoenix.service.model.PaymentInformation;
import com.apd.phoenix.service.model.PoNumber;
import com.apd.phoenix.service.model.ReturnOrder;
import com.apd.phoenix.service.model.ServiceLog;
import com.apd.phoenix.service.model.Shipment;
import com.apd.phoenix.service.model.SystemUser;
import com.google.common.primitives.Ints;
import java.util.Collection;

/**
 * CustomerOrder DAO stub
 * 
 * @author RHC
 *
 */
@Stateless
@LocalBean
public class CustomerOrderDao extends AbstractDao<CustomerOrder> {

    private static final String DEFAULT_STATUS_VALUE = OrderStatusEnum.ORDERED.getValue();
    private static final Logger LOG = LoggerFactory.getLogger(CustomerOrderDao.class);

    @SuppressWarnings("unchecked")
    public List<CustomerOrder> getCustomerOrders(CustomerOrderSearchCriteria criteria) {
        Query query = createQuery(criteria, false);
        if (query != null) {
            final List<CustomerOrder> resultList = new ArrayList<>();
            List<Object[]> queryResults = query.getResultList();
            for (Object[] o : queryResults) {
                resultList.add((CustomerOrder)o[0]);
            }
            return hydrateOrders(resultList);
        }
        return null;
    }

    @SuppressWarnings("unchecked")
    public List<CustomerOrder> getCustomerOrdersPaginated(CustomerOrderSearchCriteria criterion) {
        Query query = createQuery(criterion, false);
        if (query != null) {
            setItemsReturned(query, criterion.getStart(), criterion.getPageSize());
            final List<CustomerOrder> resultList = new ArrayList<>();
            List<Object[]> queryResults = query.getResultList();
            for (Object[] o : queryResults) {
                resultList.add((CustomerOrder)o[0]);
            }
            return hydrateOrders(resultList);
        }
        else {
            return new ArrayList<CustomerOrder>();
        }
    }

    public List<CustomerOrder> hydrateOrders(List<CustomerOrder> customerOrders) {
        List<CustomerOrder> toReturn = new ArrayList<CustomerOrder>();
        for (CustomerOrder customerOrder : customerOrders) {
            toReturn.add(hydrateForOrderDetails(customerOrder));
        }
        return toReturn;
    }

    public int getCustomerOrdersCount(CustomerOrderSearchCriteria criteria) {
        Query query = createQuery(criteria, true);
        if (query != null) {
            Long numberOfEntries = (Long) query.getSingleResult();
            return Ints.checkedCast(numberOfEntries);
        }
        else {
            return 0;
        }
    }

    public CustomerOrder hydrateForOrderDetails(CustomerOrder toHydrate) {
        CustomerOrder toReturn = findById(toHydrate.getId(), CustomerOrder.class);
        if (toReturn.getUser() != null) {
            if (toReturn.getUser().getPerson() != null) {
                toReturn.getUser().getPerson().getFirstName();
            }
        }
        if (toReturn.getOrderLogs() != null) {
            Set<OrderLog> orderLogs = toReturn.getOrderLogs();
            for (OrderLog log : orderLogs) {
                if (log != null && log.getMessageMetadata() != null) {
                    log.getMessageMetadata();
                }
            }
        }

        if (toReturn.getUser() != null) {
            if (toReturn.getUser().getPerson() != null) {
                toReturn.getUser().getPerson().getFirstName();
            }
        }

        if (toReturn.getShipments() != null) {
            Set<Shipment> shipments = toReturn.getShipments();
            for (Shipment shipment : shipments) {
                if (shipment.getItemXShipments() != null) {
                    for (LineItemXShipment lixs : shipment.getItemXShipments()) {
                        lixs.toString();
                    }
                }
                if (shipment.getShippingPartner() != null) {
                    shipment.getShippingPartner().toString();
                }
            }
        }

        if (toReturn.getPoNumbers() != null) {
            Set<PoNumber> poNumbers = toReturn.getPoNumbers();
            poNumbers.size();
        }

        if (toReturn.getPaymentInformation() != null) {
            PaymentInformation paymentInformation = toReturn.getPaymentInformation();
            paymentInformation.toString();
        }

        if (toReturn.getNotificationProperties() != null) {
            Set<NotificationProperties> notificationProperties = toReturn.getNotificationProperties();
            notificationProperties.size();
        }

        if (toReturn.getComments() != null) {
            Set<Comment> comments = toReturn.getComments();
            for (Comment comment : comments) {
                if (comment.getUser() != null) {
                    comment.getUser().toString();
                }
            }
            comments.size();
        }

        if (toReturn.getAssignedCostCenter() != null) {
            toReturn.getAssignedCostCenter().toString();
        }

        if (toReturn.getAddress() != null) {
            toReturn.getAddress().toString();
        }

        if (toReturn.getItems() != null) {
            toReturn.getItems().size();
            for (LineItem item : toReturn.getItems()) {
                if (item.getItemXShipments() != null) {
                    item.getItemXShipments().size();
                    for (LineItemXShipment itemXShipment : item.getItemXShipments()) {
                        if (itemXShipment.getVendorComments() != null) {
                            itemXShipment.getVendorComments().size();
                        }
                    }
                }
                if (item.getItem() != null) {
                    item.getItem().toString();
                    if (item.getItem().getItemClassifications() != null) {
                        item.getItem().getItemClassifications().size();
                    }
                    if (item.getItem().getSkus() != null) {
                        item.getItem().getSkus().size();
                    }
                }
                if (item.getCanceledQuantities() != null) {
                    Set<CanceledQuantity> canceledQuantities = item.getCanceledQuantities();
                    for (CanceledQuantity canceledQuantity : canceledQuantities) {
                        canceledQuantity.getQuantity();
                    }
                }
                if (item.getItemXReturns() != null) {
                    for (LineItemXReturn itemXReturn : item.getItemXReturns()) {
                        itemXReturn.getQuantity();
                    }
                }
                if (item.getLogs() != null) {
                    item.getLogs().size();
                }
                if (item.getVendorComments() != null) {
                    item.getVendorComments().size();
                }
                if (item.getCustomerExpectedUnitOfMeasure() != null) {
                    item.getCustomerExpectedUnitOfMeasure().toString();
                }
                if (item.getUnitOfMeasure() != null) {
                    item.getUnitOfMeasure().toString();
                }
            }
        }

        if (toReturn.getAccount() != null) {
            Set<AccountXAccountPropertyType> properties = toReturn.getAccount().getProperties();
            if (properties != null) {
                properties.size();
            }
        }

        if (toReturn.getAccount() != null && toReturn.getAccount().getParentAccount() != null) {
            toReturn.getAccount().getParentAccount().toString();
        }

        if (toReturn.getAccount() != null && toReturn.getAccount().getRootAccount() != null) {
            toReturn.getAccount().getRootAccount().toString();
        }

        if (toReturn.getUser() != null) {
            toReturn.getUser().toString();
            if (toReturn.getUser().getPerson() != null) {
                toReturn.getUser().getPerson().toString();
            }
        }

        if (toReturn.getReturnOrders() != null) {
            toReturn.getReturnOrders().size();
            for (ReturnOrder returnOrder : toReturn.getReturnOrders()) {
                if (returnOrder.getItems() != null) {
                    returnOrder.getItems().size();
                }
                if (returnOrder.getReturnOrderLogs() != null) {
                    returnOrder.getReturnOrderLogs().size();
                }
            }
        }

        if (toReturn.getAttachments() != null) {
            toReturn.getAttachments().size();
            for (OrderAttachment file : toReturn.getAttachments()) {
                if (file != null && file.getUser() != null) {
                    file.getUser().toString();
                }
            }
        }

        if (toReturn.getServiceLogs() != null) {
            toReturn.getServiceLogs().size();
            for (ServiceLog log : toReturn.getServiceLogs()) {
                if (log != null && log.getComments() != null) {
                    log.getComments().size();
                }
            }
        }

        if (toReturn.getUser() != null && toReturn.getUser().getPerson() != null
                && toReturn.getUser().getPerson().getPhoneNumbers() != null) {
            toReturn.getUser().getPerson().getPhoneNumbers().size();
        }

        if (toReturn.getCredential() != null) {
            toReturn.getCredential().toString();
            if (toReturn.getCredential().getProperties() != null) {
                toReturn.getCredential().getProperties().size();
            }
        }

        return toReturn;
    }

    public CustomerOrder getForWorkflow(long orderId) {
        String hql = "SELECT customerOrder FROM CustomerOrder AS customerOrder LEFT JOIN FETCH customerOrder.paymentInformation "
                + "WHERE customerOrder.id=:id";
        Query query = entityManager.createQuery(hql);
        query.setParameter("id", orderId);
        return (CustomerOrder) query.getSingleResult();
    }

    @SuppressWarnings("unchecked")
    public List<CustomerOrder> getOrdersWithShipToId(String shipToId) {
        //Sanity check
        if (shipToId == null || shipToId.isEmpty()) {
            return null;
        }

        StringBuilder hql = new StringBuilder(
                "SELECT DISTINCT customerOrder FROM CustomerOrder AS customerOrder WHERE customerOrder.address.shipToId=:shipToId OR "
                        + "customerOrder.address.miscShipTo.addressId=:miscShipToId");
        Query query = entityManager.createQuery(hql.toString());
        query.setParameter("shipToId", shipToId);
        query.setParameter("miscShipToId", shipToId);
        return query.getResultList();
    }

    @SuppressWarnings("unchecked")
    public List<LineItem> getLineItemsOrderedBySku(Long id) {
        String hql = "SELECT lineItem FROM CustomerOrder AS custOrder " + "LEFT JOIN custOrder.items AS lineItem "
                + "WHERE custOrder.id=:id ORDER BY lineItem.customerSku";
        Query query = entityManager.createQuery(hql);
        query.setParameter("id", id);
        return query.getResultList();
    }

    //TODO refactor into two methods, one for count and one for actual query. Right now if account is true this returns a query with a result list
    //of CustomerOrders, if it's false the result list contains Object[]s (a CustomerOrder and a PoNumber. Probably best to return the result list 
    //and not the query
    private Query createQuery(CustomerOrderSearchCriteria criteria, boolean count){
    	String selectValue = "DISTINCT customerOrder";
    	if (count) {
    		selectValue = "count(" + selectValue + ")";
    	} else {
            selectValue += ", apdPoNumber.value";
        }
        StringBuilder hql = new StringBuilder("SELECT "+selectValue+" FROM CustomerOrder AS customerOrder"
        		+ " LEFT OUTER JOIN customerOrder.poNumbers AS poNumber"
                + " LEFT OUTER JOIN customerOrder.poNumbers AS apdPoNumber"
                + " LEFT OUTER JOIN customerOrder.orderLogs AS log"
                + " LEFT OUTER JOIN customerOrder.items AS item"
                + " LEFT OUTER JOIN customerOrder.credential AS credential"
                + " LEFT OUTER JOIN customerOrder.account AS account" 
                + " LEFT OUTER JOIN account.rootAccount AS rootAccount"
                + " LEFT OUTER JOIN customerOrder.user AS user"
                + " LEFT OUTER JOIN user.person AS person"
                + " LEFT OUTER JOIN customerOrder.shipments AS shipment"
                + " LEFT OUTER JOIN customerOrder.paymentInformation AS paymentInformation"
                + " LEFT OUTER JOIN shipment.apdInvoice AS invoice"
        		+ " LEFT OUTER JOIN customerOrder.address AS address");

        if (criteria.getSkuList() != null && criteria.getSkuList().size() > 0) {
            hql.append(" LEFT OUTER JOIN  customerOrder.items AS lineItem"
                    + " LEFT OUTER JOIN lineItem.item.skus AS sku");
        }

        hql.append(" WHERE 1=1");
        Map<String, Object> paramMap = new HashMap<>();
        hql.append(" AND apdPoNumber.type.name = :apd");
        paramMap.put("apd", "APD");
        if (criteria.getSelectedStartDate() != null) {
            hql.append(" AND log.updateTimestamp > :startDate");
            paramMap.put("startDate", criteria.getSelectedStartDate());
        }
        if (criteria.getExactEndDate() != null) {
            hql.append(" AND log.updateTimestamp < :exactEndDate");
            paramMap.put("exactEndDate", criteria.getExactEndDate());
        }
        if (criteria.getSelectedEndDate() != null) {
            hql.append(" AND ("
            		+ "(year(log.updateTimestamp) <=year(:endDate)) OR "
            		+ "(year(log.updateTimestamp) = year(:endDate) AND month(log.updateTimestamp) <=month(:endDate)) OR "
            		+ "(year(log.updateTimestamp) = year(:endDate) AND month(log.updateTimestamp) = month(:endDate) AND day(log.updateTimestamp) <= day(:endDate))"
            		+ ") ");
            paramMap.put("endDate", criteria.getSelectedEndDate());
        }
        //if the start date, end date, or the new status is not null, sets the status for the log
        if (criteria.getSelectedStartDate() != null || criteria.getSelectedEndDate() != null || criteria.getChangeStatus() != null) {
        	if (criteria.getChangeStatus() == null && criteria.getSelectedStatus() == null && criteria.getOrderStatusList() == null) {
        		//if the change status is null, and only a date range is specified, uses DEFAULT_STATUS_VALUE
	            hql.append(" AND log.orderStatus.value=:defaultStatusValue");
	            paramMap.put("defaultStatusValue", DEFAULT_STATUS_VALUE);
        	} else if(criteria.getChangeStatus() != null) {
        		//if the change status is not null, sets the status of the orderLog to the status
	            hql.append(" AND log.orderStatus.id=:changeStatusId");
	            paramMap.put("changeStatusId", criteria.getChangeStatus().getId());
        	}
        }
        if (criteria.getSelectedStatus() != null) {
            hql.append(" AND (customerOrder.status.id=:currentStatusId OR customerOrder.paymentStatus.id=:currentStatusId)");
            paramMap.put("currentStatusId", criteria.getSelectedStatus().getId());
        }
        if(criteria.getOrderStatusList() != null && criteria.getOrderStatusList().size() > 0) {
        	StringBuilder Ids = new StringBuilder();
        	String delim = "";
        	for(OrderStatus orderStatus : criteria.getOrderStatusList()) {
        		Ids.append(delim).append(orderStatus.getId().toString());
        		delim = ",";
        	}        	
        	 hql.append(" AND (customerOrder.status.id in(" +Ids.toString() +" ) OR customerOrder.paymentStatus.id in (" +Ids.toString() +" ))");        	 
        }
        if(criteria.getLineItemStatus() != null) {
            hql.append(" AND (item.status.id=:currentLineItemStatusId OR item.paymentStatus.id=:currentLineItemStatusId)");
            paramMap.put("currentLineItemStatusId", criteria.getLineItemStatus().getId());
        }
        if(criteria.getPaymentType() != null) {
        	hql.append(" AND paymentInformation.paymentType.id=:paymentTypeId");
            paramMap.put("paymentTypeId", criteria.getPaymentType().getId());
        }
        if (!StringUtils.isEmpty(criteria.getSelectedPO()) ) {
            hql.append(" AND upper(poNumber.value)=:poNumberValue");
            paramMap.put("poNumberValue", criteria.getSelectedPO().toUpperCase());
            if(!StringUtils.isEmpty(criteria.getPoType())){
            	hql.append(" AND poNumber.type.name = :poType");
                paramMap.put("poType", criteria.getPoType());
            }
        }
        if (criteria.getAccount() != null) {
            hql.append(" AND account=:account");
            paramMap.put("account", criteria.getAccount());
        }
        if (criteria.getCredential() != null) {
            hql.append(" AND credential=:credential");
            paramMap.put("credential", criteria.getCredential());
            if(!StringUtils.isEmpty(criteria.getPartnerId())){
            	hql.append(" AND credential.partnerId = :patnerId");
            	paramMap.put("partnerId", criteria.getPartnerId());
            }
        }
        if (!StringUtils.isEmpty(criteria.getUserId())) {
            hql.append(" AND lower(user.login)=:userLogin");
            paramMap.put("userLogin", criteria.getUserId().toLowerCase());
        }
        if (!StringUtils.isEmpty(criteria.getSalesPerson())) {
            hql.append(" AND address.miscShipTo.apdSalesPerson=:salesPerson");
            paramMap.put("salesPerson", criteria.getSalesPerson());
        }
        if (!StringUtils.isEmpty(criteria.getEmail())) {
            hql.append(" AND lower(person.email)=:email");
            paramMap.put("email", criteria.getEmail().toLowerCase());
        }
        if (criteria.getSkuList() != null && criteria.getSkuList().size() > 0) {
            hql.append(" AND upper(sku.value) in :skuList");
            paramMap.put("skuList", criteria.getSkuList());
        }
        if (criteria.getMatchPrice() != null && criteria.getMatchPrice().compareTo(BigDecimal.ZERO) != 0) {
        	hql.append(" AND (customerOrder.orderTotal=:matchPrice OR invoice.amount=:matchPrice OR log.totalAmount=:matchPrice)");
            paramMap.put("matchPrice", criteria.getMatchPrice());
        }
        if (criteria.getAccountWhiteList() != null) {
        	hql.append(" AND (account.id in (:accountWhiteList) or rootAccount.id in (:accountWhiteList))");
            paramMap.put("accountWhiteList", criteria.getAccountWhiteList());
        }
        if (!StringUtils.isBlank(criteria.getApdPo())) {
            hql.append(" AND lower(apdPoNumber.value)=:apdPo");
            paramMap.put("apdPo", criteria.getApdPo().toLowerCase());
        }
        hql.append(" ORDER BY apdPoNumber.value desc");
        if (paramMap.keySet().size() == 0) {
        	//no parameters set
        	return null;
        }

        Query query = entityManager.createQuery(hql.toString());
        LOG.trace("Searching for customer order using '{}'", hql.toString());
        
        for (String key : paramMap.keySet()) {
                if(key.equals("startDate") || key.equals("exactEndDate")){
                    query.setParameter(key, (Date)paramMap.get(key), TemporalType.TIMESTAMP);
                }
                else {
                    query.setParameter(key, paramMap.get(key));
                }
        }
        return query;
    }

    @SuppressWarnings("unchecked")
    public List<CustomerOrder> getOrdersWithShipDate(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        String hql = "SELECT customerOrder FROM CustomerOrder AS customerOrder"
                + " WHERE day(shipDate) = day(:date) AND month(shipDate) = month(:date) AND year(shipDate) = year(:date)";
        Query query = entityManager.createQuery(hql);
        query.setParameter("date", date);
        return query.getResultList();
    }

    public CustomerOrder hydrateLineItems(CustomerOrder order) {
        CustomerOrder toReturn = this.findById(order.getId(), CustomerOrder.class);
        if (toReturn.getItems() != null) {
            Set<LineItem> items = toReturn.getItems();
            for (LineItem item : items) {
                if (item.getItemXShipments() != null) {
                    Set<LineItemXShipment> itemslixs = item.getItemXShipments();
                    itemslixs.size();
                }
                if (item.getItem() != null) {
                    item.getItem().toString();
                }
            }
        }
        return toReturn;
    }

    public CustomerOrder loadQuotedOrder(String apdOrderNumber) {
        //Sanity check
        if (apdOrderNumber == null || apdOrderNumber.isEmpty()) {
            return null;
        }

        StringBuilder hql = new StringBuilder("SELECT DISTINCT customerOrder FROM CustomerOrder AS customerOrder"
                + " JOIN customerOrder.poNumbers AS poNumber" + " JOIN poNumber.type AS poType"
                + " WHERE poType.name='APD'" + " AND poNumber.value=:apdPoNumber");
        Query query = entityManager.createQuery(hql.toString());
        query.setParameter("apdPoNumber", apdOrderNumber);
        return getSingleResultOrNull(query);
    }

    public MessageMetadata getOriginalMetadataForOrder(CustomerOrder order) {
        String hql = "SELECT meta FROM OrderLog as log LEFT JOIN log.messageMetadata as meta "
                + "WHERE log.customerOrder.id=:orderId AND log.orderStatus.value='ORDERED'";
        Query query = entityManager.createQuery(hql);
        query.setParameter("orderId", order.getId());
        return (MessageMetadata) query.getSingleResult();
    }

    public Collection<CustomerOrder> getOrderByCustomerPo(String customerPo, Account account) {
        String hql = "SELECT DISTINCT customerOrder FROM CustomerOrder AS customerOrder"
                + " JOIN customerOrder.poNumbers AS poNumber" + " JOIN poNumber.type AS poType"
                + " WHERE poType.name = 'Customer' AND poNumber.value = :customerPo AND"
                + " customerOrder.account.id = :accountId";
        Query query = entityManager.createQuery(hql);
        query.setParameter("customerPo", customerPo);
        query.setParameter("accountId", account.getId());
        Collection<CustomerOrder> returnCollection = (Collection<CustomerOrder>) query.getResultList();
        if (returnCollection == null) {
            return new ArrayList<CustomerOrder>();
        }
        else {
            return returnCollection;
        }
    }

    public List<CustomerOrder> searchByApdPo(String apdPo) {
        String hql = "SELECT DISTINCT customerOrder FROM CustomerOrder AS customerOrder"
                + " JOIN customerOrder.poNumbers AS apdPoNumber "
                + " WHERE apdPoNumber.type.name = 'APD' AND UPPER(apdPoNumber.value) = :apdPo";
        Query query = entityManager.createQuery(hql);
        query.setParameter("apdPo", apdPo.toUpperCase());
        return (List<CustomerOrder>) query.getResultList();
    }

    public String getAccountName(String apdPo) {
        try {
            String hql = "SELECT customerOrder.account.name FROM CustomerOrder AS customerOrder"
                    + " JOIN customerOrder.poNumbers AS poNumber" + " JOIN poNumber.type AS poType"
                    + " WHERE poType.name = 'APD' AND poNumber.value = :apdPo";
            Query query = entityManager.createQuery(hql);
            query.setParameter("apdPo", apdPo);
            return (String) query.getSingleResult();
        }
        catch (Exception e) {
            LOG.warn("Couldn't get account name for APD PO " + apdPo + ", turn on debug logging to view stack trace");
            if (LOG.isDebugEnabled()) {
                LOG.debug("Stack trace", e);
            }
            return null;
        }
    }

    public List<CustomerOrder> getOrdersPendingApproval() {
        String hql = "SELECT customerOrder FROM CustomerOrder AS customerOrder LEFT JOIN customerOrder.status as status "
                + "WHERE status.value = :pendingApprovalEnumValue";
        Query query = entityManager.createQuery(hql);
        query.setParameter("pendingApprovalEnumValue", OrderStatusEnum.PENDING_APPROVAL.getValue());
        List<CustomerOrder> returnList = (List<CustomerOrder>) query.getResultList();
        if (returnList == null) {
            return new ArrayList<CustomerOrder>();
        }
        else {
            return returnList;
        }
    }

    public LineItem findLastTimeItemOrdered(SystemUser user, LineItem item) {
        String hql = "SELECT item FROM LineItem as item LEFT JOIN item.order AS customerOrder "
                + "LEFT JOIN item.logs as lineLog where "
                + "customerOrder.user.id = :userId AND item.item.id = :itemId AND NOT item.id = :lineItemId "
                + "AND lineLog.lineItemStatus.value=:placedStatus order by lineLog.updateTimestamp desc";
        Query query = entityManager.createQuery(hql);
        query.setParameter("userId", user.getId());
        query.setParameter("itemId", item.getItem().getId());
        query.setParameter("lineItemId", item.getId());
        query.setParameter("placedStatus", LineItemStatusEnum.CREATED.getValue());
        List<LineItem> returnList = (List<LineItem>) query.getResultList();
        if (returnList == null || returnList.isEmpty()) {
            return null;
        }
        else {
            return returnList.get(0);
        }
    }

}