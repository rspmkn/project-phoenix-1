package com.apd.phoenix.service.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * This entity contains the information relevant for an Inventory Site details.
 * 
 * @author FWS-Muthu
 * 
 */
@Entity
public class InventoryBinType implements Serializable, com.apd.phoenix.service.model.Entity {

    private static final long serialVersionUID = 7185662040258275545L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id = null;

    @Column(updatable = false, nullable = false)
    private Long inventoryBinTypeID;

    @Column
    private String binName;

    @Column
    private String description;

    @Column
    private boolean defaultBin;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getInventoryBinTypeID() {
        return inventoryBinTypeID;
    }

    public void setInventoryBinTypeID(Long inventoryBinTypeID) {
        this.inventoryBinTypeID = inventoryBinTypeID;
    }

    public String getBinName() {
        return binName;
    }

    public void setBinName(String binName) {
        this.binName = binName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isDefaultBin() {
        return defaultBin;
    }

    public void setDefaultBin(boolean defaultBin) {
        this.defaultBin = defaultBin;
    }

}