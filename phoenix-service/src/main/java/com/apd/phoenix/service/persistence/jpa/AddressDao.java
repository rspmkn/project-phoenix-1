package com.apd.phoenix.service.persistence.jpa;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.Query;
import com.apd.phoenix.service.model.Account;
import com.apd.phoenix.service.model.AccountXCredentialXUser;
import com.apd.phoenix.service.model.Address;
import com.apd.phoenix.service.model.dto.AddressDto;
import org.apache.commons.lang.StringUtils;

/**
 * Address DAO stub
 * 
 * @author RHC
 *
 */
@Stateless
@LocalBean
public class AddressDao extends AbstractDao<Address> {

    @SuppressWarnings("unchecked")
    public List<Address> getAddresses(Account account) {
        if (account == null) {
            return null;
        }

        String hql = "SELECT address FROM Account AS account JOIN account.addresses AS address WHERE account=:account";
        Query query = entityManager.createQuery(hql);
        query.setParameter("account", account);
        return query.getResultList();
    }

    @SuppressWarnings("unchecked")
    public List<Address> getAccountAddressesByZip(Account account, String zip) {
        if (account == null || zip == null) {
            return null;
        }

        String hql = "SELECT address FROM Account AS account JOIN account.addresses AS address "
                + "WHERE length(address.zip) >= 5 AND length(:zip) >= 5  AND SUBSTRING(address.zip, 0, 5) = SUBSTRING(:zip, 0, 5) AND account=:account";
        Query query = entityManager.createQuery(hql);
        query.setParameter("zip", zip.trim());
        query.setParameter("account", account);
        return query.getResultList();
    }

    /**
     * Takes an AccountXCredentialXUser and an address type, and returns a List of Addresses with that type. If the 
     * User has at least one address of that type, returns the addresses that the user has with that type; otherwise, 
     * returns the addresses that the account has with that type.
     * 
     * @param axcxu
     * @param addressType
     * @return
     */
    public List<Address> addressesForCashout(AccountXCredentialXUser axcxu, String addressType) {
        String hql = "SELECT axcxu FROM AccountXCredentialXUser AS axcxu LEFT JOIN FETCH axcxu.user AS user "
                + "LEFT JOIN FETCH user.person AS person LEFT JOIN FETCH person.addresses AS userAddresses "
                + "LEFT JOIN FETCH axcxu.account AS account LEFT JOIN FETCH account.addresses AS accountAddresses "
                + "LEFT JOIN FETCH userAddresses.type AS userAddType LEFT JOIN FETCH accountAddresses.type AS accountAddType "
                + "WHERE axcxu.id = :axcxuId";

        //Creates the query
        Query query = entityManager.createQuery(hql);
        query.setParameter("axcxuId", axcxu.getId());

        //there will be exactly one entry in the returned list, since exactly one AXCXU matches the ID
        AccountXCredentialXUser fetchedAxcxu = (AccountXCredentialXUser) query.getSingleResult();
        List<Address> toReturn = new ArrayList<Address>();
        //adds any user address with the given type
        for (Address address : fetchedAxcxu.getUser().getPerson().getAddresses()) {
            if (address.getType() != null && addressType.equals(address.getType().getName())) {
                toReturn.add(address);
            }
        }
        //if there are no user addresses with the type, adds any account addresses with the type
        if (toReturn.isEmpty()) {
            for (Address address : fetchedAxcxu.getAccount().getAddresses()) {
                if (address.getType() != null && addressType.equals(address.getType().getName())) {
                    toReturn.add(address);
                }
            }
        }
        return toReturn;
    }

    @SuppressWarnings("unchecked")
    public List<Address> getAddressesWithShipTo(String shipToId) {
        if (shipToId == null || shipToId.equals("")) {
            return null;
        }

        String hql = "SELECT address FROM Address AS address WHERE address.shipToId=:shipToId";
        Query query = entityManager.createQuery(hql);
        query.setParameter("shipToId", shipToId);
        return query.getResultList();
    }

    @SuppressWarnings("unchecked")
    public Address retrieveAddress(AddressDto shipToAddressDto, Account account) {
        if (shipToAddressDto == null || account == null) {
            return null;
        }
        String hql;
        Query query;
        //Try to match on the addressId some customers use to uniquely identify addresses
        if (shipToAddressDto.getMiscShipToDto() != null) {
            if (shipToAddressDto.getMiscShipToDto().getAddressId() != null) {
                hql = "SELECT address FROM Address AS address WHERE address.miscShipTo.addressId =:addressId AND"
                        + " address.account =:account";
                query = entityManager.createQuery(hql);
                query.setParameter("addressId", shipToAddressDto.getMiscShipToDto().getAddressId());
                query.setParameter("account", account);
                List<Address> results = query.getResultList();
                if (results != null && !results.isEmpty()) {
                    return results.get(0);
                }
            }
            if (shipToAddressDto.getMiscShipToDto().getGlnID() != null
            //Currently used for Walmart. Should use address from inbound order when avaliable.
                    && (StringUtils.isEmpty(shipToAddressDto.getLine1())
                            || StringUtils.isEmpty(shipToAddressDto.getCity()) || shipToAddressDto.getZip() == null)) {
                hql = "SELECT address FROM Address AS address WHERE address.miscShipTo.glnID =:glnID AND"
                        + " address.account =:account";
                query = entityManager.createQuery(hql);
                query.setParameter("glnID", shipToAddressDto.getMiscShipToDto().getGlnID());
                query.setParameter("account", account);
                List<Address> results = query.getResultList();
                if (results != null && !results.isEmpty()) {
                    return results.get(0);
                }
            }
        }
        if (shipToAddressDto.getLine1() != null && shipToAddressDto.getZip() != null) {
            String hqlStart = "SELECT address FROM Address AS address WHERE ";
            String hqlEnd = " length(address.zip) >= 5 AND length(:zip) >= 5 AND SUBSTRING(address.zip, 0, 5) = SUBSTRING(:zip, 0, 5) AND"
                    + " address.account =:account";
            String hqlExtra = " AND address.miscShipTo.usAccount =:usAccount";

            if (StringUtils.isEmpty(shipToAddressDto.getLine1())) {
                hql = hqlStart + " address.line1 is null AND" + hqlEnd;
                query = entityManager.createQuery(hql);
            }
            else {
                //adds to the WHERE clause, ensuring that each substring is in the line1 of the address
                hql = hqlStart + " UPPER(address.line1) = :value AND " + hqlEnd;
                //adds usAccount if it exists
                if (shipToAddressDto.getMiscShipToDto() != null
                        && !StringUtils.isEmpty(shipToAddressDto.getMiscShipToDto().getUsAccount())) {
                    hql = hql + hqlExtra;
                    query = entityManager.createQuery(hql);
                    query.setParameter("usAccount", shipToAddressDto.getMiscShipToDto().getUsAccount());
                }
                else {
                    query = entityManager.createQuery(hql);
                }
                query.setParameter("value", shipToAddressDto.getLine1().toUpperCase());
            }

            query.setParameter("zip", shipToAddressDto.getZip().trim());
            query.setParameter("account", account);

            List<Address> results = query.getResultList();
            if (results != null && !results.isEmpty()) {
                return results.get(0);
            }
        }
        return null;
    }

    public List<Address> getAddresses(Account account, int start, int addressPageSize) {
        String hql = "SELECT address FROM Address AS address WHERE address.account.id =:accountId";
        Query query = entityManager.createQuery(hql);
        query.setParameter("accountId", account.getId());
        query.setFirstResult(start);
        query.setMaxResults(addressPageSize);
        return (List<Address>) query.getResultList();
    }

    public Integer getNumberOfAddresses(Long accountId) {
        String hql = "SELECT count(address.id) FROM Address AS address WHERE address.account.id =:accountId";
        Query query = entityManager.createQuery(hql);
        query.setParameter("accountId", accountId);
        Long numberOfAddresses = (Long) query.getSingleResult();
        if (numberOfAddresses == null) {
            return null;
        }
        return numberOfAddresses.intValue();
    }

    public boolean isAddressAlreadyOnAccount(Long addressId, Long accountId) {
        String hql = "SELECT count(address.id) FROM Address AS address WHERE address.account.id =:accountId and address.id =:addressId";
        Query query = entityManager.createQuery(hql);
        query.setParameter("accountId", accountId);
        query.setParameter("addressId", addressId);
        Long numberOfAddresses = (Long) query.getSingleResult();
        return (numberOfAddresses != null && numberOfAddresses > 0);
    }
}
