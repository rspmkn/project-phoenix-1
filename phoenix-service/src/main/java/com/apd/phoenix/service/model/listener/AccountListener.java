package com.apd.phoenix.service.model.listener;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.MessageProducer;
import javax.jms.ObjectMessage;
import javax.jms.Session;
import javax.naming.InitialContext;
import javax.persistence.PostPersist;
import javax.persistence.PostUpdate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.model.Account;
import com.apd.phoenix.service.persistence.multitenancy.impl.CurrentTenantIdentifierResolverImpl;

/**
 *
 * @author RHC
 */
public class AccountListener {

    private static final Logger LOGGER = LoggerFactory.getLogger(AccountListener.class);

    private static final String connectionFactoryJndi = "java:/activemq/ConnectionFactory";
    private static final String queueJndi = "java:/activemq/account-lineage-generation";

    public static void updateListener(final Account entity) {
        Connection connection = null;
        try {
            InitialContext context = new InitialContext();
            ConnectionFactory connectionFactory = (ConnectionFactory) context.lookup(connectionFactoryJndi);
            Destination queue = (Destination) context.lookup(queueJndi);
            connection = connectionFactory.createConnection();
            Session msession = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            MessageProducer messageProducer = msession.createProducer(queue);
            ObjectMessage message = msession.createObjectMessage();
            message.setObject(entity.getId());
            message.setLongProperty("tenantId", CurrentTenantIdentifierResolverImpl.getCurrentTenant());
            messageProducer.send(message);
        }
        catch (Exception e) {
            LOGGER.error("An error occured:", e);
        }
        finally {
            try {
                if (connection != null) {
                    connection.close();
                }
            }
            catch (Exception e) {
                LOGGER.error("Could not close connection", e);
            }
        }
    }
}
