package com.apd.phoenix.service.persistence.jpa;

import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.Query;
import com.apd.phoenix.service.model.ZipPlusFour;

/**
 * ZipPlusFour DAO stub
 * 
 * @author RHC
 *
 */
@Stateless
@LocalBean
public class ZipPlusFourDao extends AbstractDao<ZipPlusFour> {

    @SuppressWarnings("unchecked")
    public String countyFipsFromValues(String zip, String plusFour) {
        String hql = "SELECT plusFour FROM ZipPlusFour AS plusFour WHERE zip LIKE :zip AND :plusFour BETWEEN"
                + " plusFour.low AND plusFour.hi";
        //Creates the query
        Query query = entityManager.createQuery(hql);
        query.setParameter("zip", zip);
        query.setParameter("plusFour", plusFour);
        List<Object> returnList = query.getResultList();
        if (returnList.isEmpty()) {
            return null;
        }
        else {
            return ((ZipPlusFour) returnList.get(0)).getCountyFips();
        }
    }
}
