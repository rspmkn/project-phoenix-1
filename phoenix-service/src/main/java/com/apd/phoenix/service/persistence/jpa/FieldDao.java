package com.apd.phoenix.service.persistence.jpa;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import com.apd.phoenix.service.model.Field;

/**
 * FieldType DAO stub
 * 
 * @author RHC
 *
 */
@Stateless
@LocalBean
public class FieldDao extends AbstractDao<Field> {
}
