package com.apd.phoenix.service.manifest;

import java.io.Serializable;
import java.util.List;

public class VendorManifestsResponse implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 3173676458837705154L;
    private List<Manifest> manifests;

    public List<Manifest> getManifests() {
        return manifests;
    }

    public void setManifests(List<Manifest> manifests) {
        this.manifests = manifests;
    }

}
