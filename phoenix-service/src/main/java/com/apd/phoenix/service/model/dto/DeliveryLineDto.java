package com.apd.phoenix.service.model.dto;

import java.io.Serializable;
import java.math.BigInteger;

public class DeliveryLineDto implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private String name;
    private UOM uom;
    private String description;
    private BigInteger qtyTarget;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public UOM getUom() {
        return uom;
    }

    public void setUom(UOM uom) {
        this.uom = uom;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BigInteger getQtyTarget() {
        return qtyTarget;
    }

    public void setQtyTarget(BigInteger qtyTarget) {
        this.qtyTarget = qtyTarget;
    }

    public enum UOM {
        BOX("BX");

        private final String label;

        private UOM(String label) {
            this.label = label;
        }

        public String getLabel() {
            return this.label;
        }
    }

}
