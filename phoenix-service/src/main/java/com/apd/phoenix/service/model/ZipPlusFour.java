package com.apd.phoenix.service.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Version;
import org.hibernate.annotations.Index;

@Entity
public class ZipPlusFour implements Serializable, com.apd.phoenix.service.model.Entity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id = null;
    @Version
    @Column(name = "version", nullable = false)
    private int version = 0;

    @Column
    private String hi;

    @Column
    private String low;

    @Index(name = "ZIP_IDX")
    @Column
    private String zip;

    @Index(name = "COUNTY_FIPS_IDX")
    @Column
    private String countyFips;

    public Long getId() {
        return this.id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public int getVersion() {
        return this.version;
    }

    public void setVersion(final int version) {
        this.version = version;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        if (id != null) {
            return id.equals(((ZipPlusFour) that).id);
        }
        return super.equals(that);
    }

    @Override
    public int hashCode() {
        if (id != null) {
            return id.hashCode();
        }
        return super.hashCode();
    }

    public String getHi() {
        return this.hi;
    }

    public void setHi(final String hi) {
        this.hi = hi;
    }

    public String getLow() {
        return this.low;
    }

    public void setLow(final String low) {
        this.low = low;
    }

    public String getZip() {
        return this.zip;
    }

    public void setZip(final String zip) {
        this.zip = zip;
    }

    public String getCountyFips() {
        return this.countyFips;
    }

    public void setCountyFips(final String countyFips) {
        this.countyFips = countyFips;
    }

    @Override
    public String toString() {
        String result = getClass().getSimpleName() + " ";
        if (hi != null && !hi.trim().isEmpty())
            result += "hi: " + hi;
        if (low != null && !low.trim().isEmpty())
            result += ", low: " + low;
        if (zip != null && !zip.trim().isEmpty())
            result += ", zip: " + zip;
        if (countyFips != null && !countyFips.trim().isEmpty())
            result += ", countyFips: " + countyFips;
        return result;
    }
}
