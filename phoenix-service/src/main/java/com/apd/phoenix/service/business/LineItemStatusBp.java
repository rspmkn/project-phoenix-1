/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.service.business;

import com.apd.phoenix.service.model.LineItemStatus;
import com.apd.phoenix.service.model.LineItemStatus.LineItemStatusEnum;
import com.apd.phoenix.service.model.OrderStatus;
import com.apd.phoenix.service.persistence.jpa.LineItemStatusDao;
import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;

/**
 *
 * @author RHC
 */
@Stateless
@LocalBean
public class LineItemStatusBp extends AbstractBp<LineItemStatus> {

    @Inject
    LineItemStatusDao lineItemStatusDao;

    /**
     * This method is injected with the correct Dao for initialization
     * 
     * @param dao
     */
    @Inject
    public void initDao(LineItemStatusDao dao) {
        this.dao = dao;
    }

    public LineItemStatus getStatusByValue(String status) {
        return lineItemStatusDao.getStatusByValue(status);
    }

    public LineItemStatus getStatus(LineItemStatusEnum status) {
        return this.getStatusByValue(status.getValue());
    }

    public List<LineItemStatus> getStatusList() {
        return lineItemStatusDao.findAll(LineItemStatus.class, 0, 0);
    }
}
