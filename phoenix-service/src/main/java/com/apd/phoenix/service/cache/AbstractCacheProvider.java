package com.apd.phoenix.service.cache;

import java.util.Properties;
import org.infinispan.client.hotrod.RemoteCache;
import org.infinispan.client.hotrod.RemoteCacheManager;
import org.infinispan.client.hotrod.configuration.Configuration;
import org.infinispan.client.hotrod.configuration.ConfigurationBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.core.utility.TenantConfigRepository;

/**
 * This is the superclass for cache providers. It provides the methods for obtaining the cache. 
 * Concrete classes need to specify the Key and Value class types, and extend the getCacheName() 
 * method.
 * 
 * @author RHC
 *
 * @param <K>
 * @param <V>
 */
public abstract class AbstractCacheProvider<K, V> {

    protected static final Logger logger = LoggerFactory.getLogger(AbstractCacheProvider.class);

    protected static final Properties cacheProperties = TenantConfigRepository.getInstance().getProperties("cache");

    private RemoteCacheManager cacheManager;

    /**
     * Returns the cache. If an error occurs, null will be returned instead.
     * 
     * @return
     */
    public RemoteCache<K, V> getCache() throws CacheProviderException {
        try {
            RemoteCache<K, V> toReturn = getCacheManager().getCache(this.getCacheName());
            toReturn.get(null);
            return toReturn;
        }
        catch (Exception e1) {
            logger.error("Exception when getting cache, turn on debug for stack trace");
            if (logger.isDebugEnabled()) {
                logger.debug("Stack trace", e1);
            }
            try {
                cacheManager.stop();
            }
            catch (Exception e2) {
                logger.warn("Unable to stop broken cache manager, turn on debug for stack trace");
                if (logger.isDebugEnabled()) {
                    logger.debug("Stack trace", e2);
                }
            }
            //setting to null, will be generated again with getCacheManager next time getCache is called
            cacheManager = null;
            throw new CacheProviderException();
        }
    }

    /**
     * Called by the PreDestroy method in child classes.
     */
    protected void cleanup() {
        if (cacheManager != null) {
            cacheManager.stop();
        }
    }

    private RemoteCacheManager getCacheManager() {
        if (cacheManager == null) {
            Configuration config = new ConfigurationBuilder().protocolVersion(
                    cacheProperties.getProperty("hotrodProtocolVersion")).addServers(
                    cacheProperties.getProperty("remoteCacheUrl")).build();
            cacheManager = new RemoteCacheManager(config, true);
        }
        return cacheManager;
    }

    /**
     * Returns the String name of the cache. Implemented by concrete class.
     * 
     * @return
     */
    protected abstract String getCacheName();

}
