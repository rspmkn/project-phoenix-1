package com.apd.phoenix.service.model;

import java.io.Serializable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

@Entity
public class VendorPo implements Serializable, com.apd.phoenix.service.model.Entity {

    /**
     */
    private static final long serialVersionUID = 1L;

    /** The id. */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id = null;

    @Column(nullable = false)
    private Integer quantity = 0;

    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private LineItem lineitem;

    @ManyToOne(fetch = FetchType.LAZY)
    private InventoryItem inventoryItem;

    @Column
    private String vendorSku;

    @Column
    private String manufactuerSku;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public LineItem getLineitem() {
        return lineitem;
    }

    public void setLineitem(LineItem lineitem) {
        this.lineitem = lineitem;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        VendorPo other = (VendorPo) obj;
        if (id == null) {
            if (other.id != null)
                return false;
        }
        else if (!id.equals(other.id))
            return false;
        return true;
    }

    public InventoryItem getInventoryItem() {
        return inventoryItem;
    }

    public void setInventoryItem(InventoryItem inventoryItem) {
        this.inventoryItem = inventoryItem;
    }

    public String getVendorSku() {
        return vendorSku;
    }

    public void setVendorSku(String vendorSku) {
        this.vendorSku = vendorSku;
    }

    public String getManufactuerSku() {
        return manufactuerSku;
    }

    public void setManufactuerSku(String manufactuerSku) {
        this.manufactuerSku = manufactuerSku;
    }

}
