package com.apd.phoenix.service.model;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import org.hibernate.envers.Audited;

@Entity
@Audited
@DiscriminatorValue(value = "HOME_PAGE")
public class HomePageCarouselDisplay extends CarouselDisplay {

    /**
     * 
     */
    private static final long serialVersionUID = -7228945805042538009L;

    //Hibernate does not support ManyToOne relationships on sublcasses, so the field is only accessable through gettes on this class
    public Catalog getCatalog() {
        return catalog;
    }

    public void setCatalog(Catalog catalog) {
        this.catalog = catalog;
    }
}
