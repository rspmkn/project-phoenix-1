package com.apd.phoenix.service.business;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import com.apd.phoenix.service.model.TaxRateLog;
import com.apd.phoenix.service.persistence.jpa.TaxRateLogDao;

@Stateless
@LocalBean
public class TaxRateLogBp extends AbstractBp<TaxRateLog> {

    /**
     * This method is injected with the correct Dao for initialization
     * 
     * @param dao
     */
    @Inject
    public void initDao(TaxRateLogDao dao) {
        this.dao = dao;
    }
}
