package com.apd.phoenix.service.email.impl;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.inject.Inject;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.business.AccountXCredentialXUserBp;
import com.apd.phoenix.service.business.CustomerOrderBp;
import com.apd.phoenix.service.email.api.Attachment;
import com.apd.phoenix.service.email.api.EmailTemplate;
import com.apd.phoenix.service.email.api.NoAttachmentContentException;
import com.apd.phoenix.service.email.impl.SalesQuoteEmailTemplate.ItemSubstitution;
import com.apd.phoenix.service.model.Account;
import com.apd.phoenix.service.model.AccountPropertyType;
import com.apd.phoenix.service.model.AccountXAccountPropertyType;
import com.apd.phoenix.service.model.AccountXCredentialXUser;
import com.apd.phoenix.service.model.AssignedDepartment;
import com.apd.phoenix.service.model.Comment;
import com.apd.phoenix.service.model.CustomerOrder;
import com.apd.phoenix.service.model.LineItem;
import com.apd.phoenix.service.model.CredentialPropertyType.CredentialPropertyTypeEnum;
import com.apd.phoenix.service.model.dto.AddressDto;
import com.apd.phoenix.service.model.dto.LineItemDto;
import com.apd.phoenix.service.model.dto.POAcknowledgementDto;
import com.apd.phoenix.service.model.dto.PurchaseOrderDto;
import com.apd.phoenix.service.model.factory.DtoFactory;
import com.apd.phoenix.service.model.factory.DtoFactory.ParsingException;
import freemarker.template.TemplateException;

public class ProFormaInvoiceEmailTemplate extends EmailTemplate<POAcknowledgementDto> {

    private static final Logger LOGGER = LoggerFactory.getLogger(OrderAcknowledgmentEmailTemplate.class);
    private static final String TEMPLATE = "proforma.invoice";
    private static final String MINIMUM_ORDER_SKU = "min_order_fee";

    @Inject
    private CustomerOrderBp customerOrderBp;

    @Inject
    AccountXCredentialXUserBp accountXCredentialXUserBp;

    @Override
	public String createBody(POAcknowledgementDto pOAcknowledgementDto) throws IOException,
			TemplateException {
		Map<String, Object> params = new HashMap<>();
        String phoneNumber = null;
        List<ItemSubstitution> itemsList = new ArrayList<>();
        BigDecimal minimumOrderTotal = null;
        BigDecimal orderTotal = new BigDecimal(0);
        Set<Comment> comments = null;
        String commentString = "";
        String billToAddress = "";
        String shipToAddress = "";
		String userName = "";
		String userEmail = "";
		String salesPerson = "";
        PurchaseOrderDto purchaseOrderDto;
        Account account = null;
        
        CustomerOrder customerOrder = customerOrderBp.searchByApdPo(pOAcknowledgementDto.getApdPo());
        comments = customerOrder.getComments();
        
        //get salesperson, if exists
        account = customerOrder.getAccount(); 
        for(AccountXAccountPropertyType apt: account.getProperties()){
        	if(apt.getType().getName() == "SALESPERSON"){
        		salesPerson = apt.getType().getName();
        		break;
        	}
        }
        
        //get order comments
        for(Comment comment: comments){
        	commentString += comment.getContent() + "<br>";
        }
        
        try {
                purchaseOrderDto = DtoFactory.createPurchaseOrderDto(customerOrder);
        } catch (ParsingException e) {
                LOGGER.error("Could not create purchaseOrderDto from Customer order", e);
                return null;
        }

		if (customerOrder != null && customerOrder.getUser() != null) {
			  List<AccountXCredentialXUser> response = accountXCredentialXUserBp.getAllAXCXUByLogin(customerOrder.getUser().getLogin());
			  if (response != null && response.size() > 0) {
		      	  for (AccountXCredentialXUser cred : response) {
		      		  for (AssignedDepartment dept : cred.getDepartments()) {
		      			  userName = dept.getRecipientsNames();
		      			  userEmail = dept.getRecipientsEmails();
		      			  if(dept.getDepartment() != null && !StringUtils.isEmpty(dept.getDepartment().getName())) {
		      				params.put("department", dept.getDepartment().getName());
		      			  }
		      		  }
		      	  }
	      	  }
		}

        /*Need to iterate through the list of items and see if we can find a minimum order fee and extract it out of the list.*/
        for(LineItem itemEntity: customerOrder.getItems())
        {
        	LineItemDto item = DtoFactory.createLineItemDto(itemEntity);
            if(MINIMUM_ORDER_SKU.equalsIgnoreCase(item.getApdSku()))
            {
                //Extract the total minimum order fee from the list
                minimumOrderTotal = item.getSubTotal();
            }
            else
            {
                //Add Item to the List
            	ItemSubstitution toAdd = new ItemSubstitution(); 
				toAdd.setItem(item);
				toAdd.setOriginallyAddedSku(itemEntity.getOriginallyAddedSku());
				toAdd.setOriginallyAddedName(itemEntity.getOriginallyAddedName());
                itemsList.add(toAdd);
                orderTotal.add(item.getSubTotal());
            }
        }
           
        if(customerOrder.getAddress() != null && customerOrder.getAddress().getMiscShipTo() != null)
        {
        	if(StringUtils.isNotEmpty(customerOrder.getAddress().getMiscShipTo().getRequesterPhone()))
            {
                phoneNumber = customerOrder.getAddress().getMiscShipTo().getRequesterPhone();
            }
        }
        
        //set date to order date, if null then set date to today's date
        if(purchaseOrderDto.getOrderDate() != null){
        	params.put("date",  purchaseOrderDto.getOrderDate());
        }else{
        	SimpleDateFormat sdf = new SimpleDateFormat("MMM d, yyyy HH:mm:ss a");        	
        	Calendar calendar = Calendar.getInstance();
        	params.put("date", sdf.format(calendar.getTime()));
        }
        
        //create billing address
        if(purchaseOrderDto.getPaymentInformationDto().getBillToAddress() != null){
        	AddressDto billTo = purchaseOrderDto.getPaymentInformationDto().getBillToAddress();
        	billToAddress = billTo.getName() + "<br>" +
		        			billTo.getLine1() + "<br>" + 
							billTo.getLine2() + "<br>" +
		        			billTo.getCity() + "<br>" + 
		        			billTo.getCity() + "<br>" +
		        			billTo.getZip() + "<br>" +
		        			billTo.getCountry();    	
        }
        
        //create shipping address
        if(purchaseOrderDto.getShipToAddressDto() != null){
        	AddressDto shipTo = purchaseOrderDto.getShipToAddressDto();
        	shipToAddress = shipTo.getName() + "<br>" +
        			shipTo.getLine1() + "<br>" + 
        			shipTo.getLine2() + "<br>" +
        			shipTo.getCity() + "<br>" + 
        			shipTo.getCity() + "<br>" +
        			shipTo.getZip() + "<br>" +
        			shipTo.getCountry();
        }
        
        params.put("items", itemsList);      
        params.put("skuOnEmailFlag", purchaseOrderDto.getCredential().getProperties().get(CredentialPropertyTypeEnum.SKU_ON_EMAIL.getValue()));
		params.put("quoteNumber", purchaseOrderDto.retrieveApdPoNumber().getValue());
		params.put("termsOfPayment", purchaseOrderDto.getCredential().getTerms());
		params.put("clientRef", purchaseOrderDto.getAccount().getSolomonCustomerID());
		params.put("billToAddress", billToAddress);
		params.put("shippingAddress", shipToAddress);
		params.put("client", purchaseOrderDto.getAccount().getName());
		params.put("contactPerson",userName);
		params.put("phoneNumber", phoneNumber);
		params.put("faxNumber", null);
		params.put("email", userEmail);
		params.put("salesManager", salesPerson);
		params.put("expirationDate", customerOrder.getExpiryDate());
		params.put("freight", customerOrder.getEstimatedShippingAmount());
		params.put("orderTotal", orderTotal.add((BigDecimal) params.get("freight")));
		params.put("comments", commentString);
		params.put("terms", customerOrder.getTerms());
		
		
		return this.create(params);
	}

    @Override
    public Attachment createAttachment(POAcknowledgementDto content) throws NoAttachmentContentException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    protected String getTemplate() {
        return TEMPLATE;
    }

    public class ItemSubstitution {

        private LineItemDto item;
        private String originallyAddedSku;
        private String originallyAddedName;

        public LineItemDto getItem() {
            return item;
        }

        public void setItem(LineItemDto item) {
            this.item = item;
        }

        public String getOriginallyAddedSku() {
            return originallyAddedSku;
        }

        public String getOriginallyAddedName() {
            return originallyAddedName;
        }

        public void setOriginallyAddedSku(String originallyAddedSku) {
            this.originallyAddedSku = originallyAddedSku;
        }

        public void setOriginallyAddedName(String originallyAddedName) {
            this.originallyAddedName = originallyAddedName;
        }

    }
}
