package com.apd.phoenix.service.integration.multitenancy;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.apache.camel.Exchange;
import com.apd.phoenix.service.persistence.multitenancy.impl.CurrentTenantIdentifierResolverImpl;

@ApplicationScoped
@Named("tenantProcessor")
public class TenantProcessor {

    @Inject
    private ThreadLocalTenantResolver tenantResolver;

    public void resolve(Exchange exchange) {
        //note: the tenantId can sometimes be null. Normally this isn't a problem,
        //since the messages might just need to be aggregated, but the resolver
        //should still be called, in order to overwrite the old tenant ID on the
        //thread, if any.
        Long tenantId = exchange.getProperty("tenantId", Long.class);
        CurrentTenantIdentifierResolverImpl.setCurrentTenant(tenantId);
        tenantResolver.setTenant(tenantId);
    }

}