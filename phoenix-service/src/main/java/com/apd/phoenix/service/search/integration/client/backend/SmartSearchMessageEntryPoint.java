package com.apd.phoenix.service.search.integration.client.backend;

import java.util.Map;
import java.util.Properties;
import javax.ejb.Stateless;
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.MessageProducer;
import javax.jms.ObjectMessage;
import javax.jms.Session;
import javax.naming.InitialContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.core.utility.TenantConfigRepository;
import com.apd.phoenix.service.persistence.multitenancy.impl.CurrentTenantIdentifierResolverImpl;
import com.apd.phoenix.service.search.integration.client.backend.SmartSearchCatalogMaintenanceMessage.SmartSearchCatalogMaintenanceMessageContent;
import com.apd.phoenix.service.search.integration.client.backend.SmartSearchCatalogMaintenanceMessage.SmartSearchMaintenanceAction;

@Stateless
public class SmartSearchMessageEntryPoint {

    private static final Logger LOGGER = LoggerFactory.getLogger(SmartSearchMessageEntryPoint.class);

    private static final Properties props = TenantConfigRepository.getInstance().getProperties("smart.search");
    private static final String connectionFactoryJndi = (String) props
            .get("smartsearchCatalogUpdateQueueConnectionJndi");
    private static final String queueJndi = (String) props.get("smartsearchCatalogUpdateQueueJndi");

    public void scheduleRequest(SmartSearchMaintenanceAction action,
            Map<String, Map<Long, SmartSearchCatalogMaintenanceMessageContent>> object) {
        Connection connection = null;
        try {
            InitialContext context = new InitialContext();
            ConnectionFactory connectionFactory = (ConnectionFactory) context.lookup(connectionFactoryJndi);
            Destination queue = (Destination) context.lookup(queueJndi);
            connection = connectionFactory.createConnection();
            Session msession = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            MessageProducer messageProducer = msession.createProducer(queue);
            ObjectMessage message = msession.createObjectMessage();
            message.setObject(new SmartSearchCatalogMaintenanceMessage(action, object));
            message.setStringProperty("action", action.toString());
            message.setLongProperty("tenantId", CurrentTenantIdentifierResolverImpl.getCurrentTenant());
            messageProducer.send(message);
        }
        catch (Exception e) {
            LOGGER.error("Could not schedule request for JMS", e);
        }
        finally {
            try {
                connection.close();
            }
            catch (Exception e) {
                LOGGER.error("Could not close connection for JMS", e);
            }
        }
    }
}
