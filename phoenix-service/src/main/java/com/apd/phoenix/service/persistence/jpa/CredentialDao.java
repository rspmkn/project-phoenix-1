package com.apd.phoenix.service.persistence.jpa;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.Query;
import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.model.Credential;
import com.apd.phoenix.service.model.HierarchyNode;
import com.apd.phoenix.service.model.NotificationLimit;
import com.apd.phoenix.service.model.OrderLog;

/**
 * Credential DAO stub
 * 
 * @author RHC
 *
 */
@Stateless
@LocalBean
public class CredentialDao extends AbstractDao<Credential> {

    private static final Logger LOGGER = LoggerFactory.getLogger(CredentialDao.class);

    /*
    select h.description from credential c 
    inner join sku on c.skutype_id=sku.type_id
    inner join hierarchynode h on sku.hierarchynode_id=h.id
    where c.id = 1 AND h.parent_id is NULL
     */
    @SuppressWarnings("unchecked")
    public List<HierarchyNode> getMenuItems(Credential credential) {
        if (credential != null) {

            String hql = "select h from HierarchyNode as h " + "inner join h.skuTypes as skuTypes "
                    + "where skuTypes.id = :skuTypeId";
            Query query = this.entityManager.createQuery(hql);
            query.setParameter("skuTypeId", credential.getSkuType().getId());

            return query.getResultList();
        }
        return new ArrayList<>();
    }

    public boolean hasOrders(Credential toAdd) {
        String hql = "SELECT count(customerOrder.id) FROM CustomerOrder AS customerOrder WHERE customerOrder.credential = :credential";
        Query query = this.entityManager.createQuery(hql);
        query.setParameter("credential", toAdd);
        Long count = (Long) query.getSingleResult();
        return (count != null && count > 0);
    }

    public Credential findCredentialByDuns(String duns) {
        String hql = "SELECT distinct(c) FROM Credential c JOIN FETCH c.properties AS fetchedProperties "
                + "LEFT JOIN c.properties AS credProperty JOIN credProperty.type as credPropertyType "
                + "WHERE credPropertyType.name = 'DUNS number (for ariba)' AND credProperty.value = :dunsNumber";
        //Creates the query
        Query query = entityManager.createQuery(hql);
        query.setParameter("dunsNumber", duns);
        return getSingleResultOrNull(query);
    }

    public List<OrderLog.EventType> getEventTypesWithNotificationLimit(Long credentialId, List<NotificationLimit.LimitType> limitTypesToView) {
        if (limitTypesToView == null || limitTypesToView.isEmpty()) { 
            return new ArrayList<>();
        }
        String hql = "select limit.eventType, c from Credential c join fetch c.notificationLimits as limit "
                + "where c.id = :id and limit.limitType in :limitTypesToView";
        Query query = entityManager.createQuery(hql);
        query.setParameter("id", credentialId);
        query.setParameter("limitTypesToView", limitTypesToView);
        List<Object[]> results = query.getResultList();
        List<OrderLog.EventType> toReturn = new ArrayList<>();
        for (Object[] o : results) {
            try {
                toReturn.add((OrderLog.EventType) o[0]);
            } catch (ClassCastException e) {
                LOGGER.error("Error in CredentialDao#getEventTypesWithNotificationLimit : result of query is not of the type expected", e);
                break;
            }
        }
        return toReturn;
    }

    public Credential getCredentialForMaintenance(Long credentialId) {

        Session session = entityManager.unwrap(Session.class);
        //session.enableFetchProfile("Credential-maintenance");
        Credential credential = (Credential) session.get(Credential.class, credentialId);
        loadCollection(credential.getBlanketPos());
        loadCollection(credential.getBulletins());
        loadAssociation(credential.getCashoutPage());
        loadAssociation(credential.getCatalog());
        loadAssociation(credential.getCommunicationMethod());
        loadCollection(credential.getCostCenters());
        loadCollection(credential.getCustomerOutoingFromCred());
        loadCollection(credential.getCustomerOutoingSenderCred());
        loadCollection(credential.getCustomerOutoingToCred());
        loadAssociation(credential.getDefaultShipFromAddress());
        loadCollection(credential.getDepartments());
        loadCollection(credential.getIps());
        loadCollection(credential.getNotificationLimits());
        loadCollection(credential.getNotificationProperties());
        loadCollection(credential.getOutgoingFromCredentials());
        loadCollection(credential.getOutgoingSenderCredentials());
        loadCollection(credential.getOutgoingToCredentials());
        loadAssociation(credential.getPaymentInformation());
        loadCollection(credential.getPermissions());
        loadCollection(credential.getProcessConfigurations());
        loadCollection(credential.getProperties());
        loadAssociation(credential.getPunchoutType());
        loadCollection(credential.getRoles());
        loadAssociation(credential.getRootAccount());
        loadAssociation(credential.getSkuType());
        loadAssociation(credential.getTerms());
        return credential;
    }

    private void loadCollection(Collection collection) {
        if (collection != null) {
            collection.size();
        }
    }

    private void loadAssociation(Object object) {
        if (object != null) {
            object.toString();
        }
    }

    public Credential getCredentialByName(String name) {
        String hql = "SELECT distinct(c) FROM Credential c " + "WHERE c.name=:name";
        //Creates the query
        Query query = entityManager.createQuery(hql);
        query.setParameter("name", name);
        return getSingleResultOrNull(query);
    }
}