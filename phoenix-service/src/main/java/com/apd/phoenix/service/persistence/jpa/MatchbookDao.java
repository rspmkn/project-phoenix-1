package com.apd.phoenix.service.persistence.jpa;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.Query;
import com.apd.phoenix.service.model.Manufacturer;
import com.apd.phoenix.service.model.Matchbook;

/**
 * Matchbook DAO 
 * 
 * @author RHC
 *
 */
@Stateless
@LocalBean
public class MatchbookDao extends AbstractDao<Matchbook> {

    @SuppressWarnings("unchecked")
	public Set<Manufacturer> matchbookManufacturersFromCatalog(List<Long> catalogIds) {
		Set<Manufacturer> setToReturn = new HashSet<>();
		
		for (Long catalogId : catalogIds) {

	        String hql = "SELECT DISTINCT matchbook.manufacturer FROM CatalogXItem cxi JOIN cxi.item.matchbook AS "
	        		+ "matchbook WHERE cxi.catalog.id = :catalogId AND cxi.id NOT IN (SELECT subselectItem.id FROM CatalogXItem subselectItem "
	        		+ "JOIN subselectItem.properties AS property WHERE subselectItem.catalog.id = :catalogId AND property.type.name "
	        		+ "= 'availability' AND property.value = 'no')";
	        //Creates the query
	        Query query = entityManager.createQuery(hql);
	        query.setParameter("catalogId", catalogId);

	        setToReturn.addAll((List<Manufacturer>) query.getResultList());
		}
		return setToReturn;
	}

    @SuppressWarnings("unchecked")
	public Set<Matchbook> searchByCatalogAndManufacturer(List<Long> catalogIds, Manufacturer manufacturer) {
		Set<Matchbook> setToReturn = new HashSet<>();
		
		for (Long catalogId : catalogIds) {

	        String hql = "SELECT DISTINCT matchbook FROM CatalogXItem cxi JOIN cxi.item.matchbook AS "
	        		+ "matchbook WHERE cxi.catalog.id = :catalogId AND matchbook.manufacturer.id = :manufacturerId AND cxi.id NOT IN "
	        		+ "(SELECT subselectItem.id FROM CatalogXItem subselectItem JOIN subselectItem.properties AS property WHERE subselectItem.catalog.id "
	        		+ "= :catalogId AND property.type.name = 'availability' AND property.value = 'no')";
	        //Creates the query
	        Query query = entityManager.createQuery(hql);
	        query.setParameter("catalogId", catalogId);
	        query.setParameter("manufacturerId", manufacturer.getId());

	        setToReturn.addAll((List<Matchbook>) query.getResultList());
		}
		return setToReturn;
	}

    public List<Matchbook> searchByModel(String model, Long catalogId) {

        String hql = "SELECT DISTINCT matchbook FROM CatalogXItem cxi JOIN cxi.item.matchbook AS "
                + "matchbook WHERE "
                + "cxi.catalog.id = :catalogId AND UPPER(matchbook.model) like :model AND "
                + "cxi.id NOT IN "
                + "(SELECT subselectItem.id FROM CatalogXItem subselectItem JOIN subselectItem.properties AS property WHERE subselectItem.catalog.id "
                + "= :catalogId AND property.type.name = 'availability' AND property.value = 'no')";
        //Creates the query
        Query query = entityManager.createQuery(hql);
        query.setParameter("model", model.toUpperCase());
        query.setParameter("catalogId", catalogId);

        return (List<Matchbook>) query.getResultList();
    }
}
