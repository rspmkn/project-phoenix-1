package com.apd.phoenix.service.model.listener;

import javax.persistence.PrePersist;
import com.apd.phoenix.service.model.LineItem;

/**
 * If the item is persisted in a different transaction from the order (i.e. the recalulateLineNumberOnPersist 
 * flag is true) then line numbers are recalculated on the order for all transient items (including this one).
 * 
 * @author RHC
 */
public class LineItemListener {

    @PrePersist
    public void preUpdateEventListener(LineItem entity) {
        if (entity.getOrder() != null && !entity.isRecalculateLineNumberOnPersist()) {
            entity.getOrder().calculateLineNumbers();
        }
        if (entity.getQuantity() != null && entity.getOriginalQuantity() == null) {
            entity.setOriginalQuantity(entity.getQuantity());
        }
    }
}
