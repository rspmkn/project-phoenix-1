/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.service.model.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 *
 * @author nreidelb
 */
public class LineItemXShipmentDto implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private BigInteger quantity;
    private BigDecimal shipping;
    private List<LineItemXShipmentXTaxTypeDto> lineItemXShipmentXTaxTypeDtos;
    private LineItemDto lineItemDto;
    private Boolean valid;
    private Boolean paid;
    private List<String> vendorComments;

    //an enumeration of the exact item license numbers of all the items delivered
    private List<String> licenses;
    //Quantity delivered as reported by jumptrack, if nothing went wrong, this should match quantity
    private int quantityDelivered;

    public LineItemXShipmentDto() {
        this.lineItemXShipmentXTaxTypeDtos = new ArrayList<>();
    }

    /**
     * @return the quantity
     */
    public BigInteger getQuantity() {
        return quantity;
    }

    /**
     * @param quantity the quantity to set
     */
    public void setQuantity(BigInteger quantity) {
        this.quantity = quantity;
    }

    /**
     * @return the shipping. If shipping is null, calculates it based on the appropriate default
     */
    public BigDecimal getShipping() {
        if (this.shipping == null) {
            this.shipping = this.getDefaultShippingAmount();
        }
        return shipping;
    }

    /**
     * @param shipping the shipping to set
     */
    public void setShipping(BigDecimal shipping) {
        this.shipping = shipping;
    }

    /**
     * @return the lineItemXShipmentXTaxTypeDtos
     */
    public List<LineItemXShipmentXTaxTypeDto> getLineItemXShipmentXTaxTypeDtos() {
        return lineItemXShipmentXTaxTypeDtos;
    }

    /**
     * @param lineItemXShipmentXTaxTypeDtos the lineItemXShipmentXTaxTypeDtos to set
     */
    public void setLineItemXShipmentXTaxTypeDtos(List<LineItemXShipmentXTaxTypeDto> lineItemXShipmentXTaxTypeDtos) {
        this.lineItemXShipmentXTaxTypeDtos = lineItemXShipmentXTaxTypeDtos;
    }

    /**
     * @return the lineItemDto
     */
    public LineItemDto getLineItemDto() {
        return lineItemDto;
    }

    /**
     * @param lineItemDto the lineItemDto to set
     */
    public void setLineItemDto(LineItemDto lineItemDto) {
        this.lineItemDto = lineItemDto;
    }

    public void setVendorComments(List<String> comments) {
        this.vendorComments = comments;
    }

    /**
     * @return the vendorComments
     */
    public List<String> getVendorComments() {
        return vendorComments;
    }

    public Boolean getValid() {
        return valid;
    }

    public void setValid(Boolean valid) {
        this.valid = valid;
    }

    public Boolean getPaid() {
        return paid;
    }

    public void setPaid(Boolean paid) {
        this.paid = paid;
    }

    public BigDecimal calculateSubTotal() {
        if (this.getLineItemDto().getUnitPrice() != null && this.getQuantity() != null) {
            return this.getLineItemDto().getUnitPrice().multiply(BigDecimal.valueOf(this.getQuantity().longValue()))
                    .setScale(2, RoundingMode.CEILING);
        }
        return null;
    }

    /**
     * Note: this method returns the default shipping amount, without setting any values on this DTO.
     * 
     * @return
     */
    @Deprecated
    public BigDecimal calculateTotalFreightAmount() {
        return this.getDefaultShippingAmount();
    }

    private BigDecimal getDefaultShippingAmount() {
        if (this.getLineItemDto().getEstimatedShippingAmount() != null && this.getLineItemDto().getQuantity() != null) {
            if (quantity.compareTo(BigInteger.valueOf(lineItemDto.getQuantity().longValue())) == 0) {
                return lineItemDto.getEstimatedShippingAmount().setScale(2, RoundingMode.CEILING);
            }
            else {
                BigDecimal originalQty = new BigDecimal(lineItemDto.getQuantity());
                BigDecimal qtyFraction = new BigDecimal(quantity).divide(originalQty, MathContext.DECIMAL128);
                BigDecimal estimatedShippingAmount = lineItemDto.getEstimatedShippingAmount().multiply(qtyFraction);
                estimatedShippingAmount = estimatedShippingAmount.setScale(2, RoundingMode.CEILING);
                return estimatedShippingAmount;
            }
        }
        return null;
    }

    public BigDecimal calculateTaxableAmount() {
        BigDecimal lineItemSubtotal = this.calculateSubTotal();
        if (lineItemSubtotal == null) {
            lineItemSubtotal = BigDecimal.ZERO;
        }
        BigDecimal freightTotal = this.getShipping();
        if (freightTotal == null) {
            freightTotal = BigDecimal.ZERO;
        }
        return lineItemSubtotal.add(freightTotal);
    }

    public BigDecimal calculateTotalTaxAmount() {
        BigDecimal toReturn = BigDecimal.ZERO;
        for (LineItemXShipmentXTaxTypeDto tax : this.lineItemXShipmentXTaxTypeDtos) {
            if (tax.getValue() != null) {
                toReturn = toReturn.add(tax.getValue());
            }
        }
        return toReturn;
    }

    /**
     * @return the licenses
     */
    public List<String> getLicenses() {
        return licenses;
    }

    /**
     * @param licenses the licenses to set
     */
    public void setLicenses(List<String> licenses) {
        this.licenses = licenses;
    }

    /**
     * @return the quantityDelivered
     */
    public int getQuantityDelivered() {
        return quantityDelivered;
    }

    /**
     * @param quantityDelivered the quantityDelivered to set
     */
    public void setQuantityDelivered(int quantityDelivered) {
        this.quantityDelivered = quantityDelivered;
    }

    public static class LineItemXShipmentDtoComparator implements Comparator<LineItemXShipmentDto> {

        @Override
        public int compare(LineItemXShipmentDto arg0, LineItemXShipmentDto arg1) {
            if (arg0 != null && arg1 != null) {
                return LineItemDto.LineItemDtoComparator.staticCompare(arg0.getLineItemDto(), arg1.getLineItemDto());
            }
            else if (arg0 == null && arg1 != null) {
                return -1;
            }
            else if (arg0 != null && arg1 == null) {
                return 1;
            }
            else {
                return 0;
            }
        }

    }

}
