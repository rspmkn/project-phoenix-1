/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.service.integration.utility;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author nreidelb
 */
@Deprecated
public class PropertiesLoader {

    private static final Logger LOG = LoggerFactory.getLogger(PropertiesLoader.class);

    public static Properties getAsProperties(String name) {
        Properties props = new Properties();
        String propertyHome = System.getProperty("phoenix.config.home");
        if (LOG.isDebugEnabled()) {
            LOG.debug("phoenix config home: {}", propertyHome);
        }
    
        String propsFile = propertyHome + name;
        if (!propsFile.endsWith(".properties")) {
            propsFile += ".properties";
        }
        try (InputStream inputStream = new FileInputStream(propsFile)) {
            props.load(inputStream);
        } catch (IOException ex) {
            LOG.error("Could not read properties file to inputStream.", ex);
        }

        return props;
    }
}
