package com.apd.phoenix.service.model.dto;

import java.io.Serializable;

public class PoNumberTypeDto implements Serializable {

    private static final long serialVersionUID = 5619039503536454325L;

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}