package com.apd.phoenix.service.business;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import com.apd.phoenix.service.model.PeriodType;
import com.apd.phoenix.service.persistence.jpa.PeriodTypeDao;

/**
 * This class provides business process methods for PeriodType.
 * 
 * @author RHC
 * 
 */
@Stateless
@LocalBean
public class PeriodTypeBp extends AbstractBp<PeriodType> {

    /**
     * This method is injected with the correct Dao for initialization
     * 
     * @param dao
     */
    @Inject
    public void initDao(PeriodTypeDao dao) {
        this.dao = dao;
    }
}
