/*
 * 
 */
package com.apd.phoenix.service.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Version;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * The Class PackagingDimension.
 */
@Entity
@XmlRootElement
public class PackagingDimension implements Serializable, com.apd.phoenix.service.model.Entity {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 7570973525457130296L;

    /** The id. */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id = null;

    /** The version. */
    @Version
    @Column(name = "version", nullable = false)
    private int version = 0;

    /** The dimension type. */
    @ManyToOne(fetch = FetchType.EAGER)
    private DimensionType dimensionType;

    /** The unit of measure. */
    @ManyToOne(fetch = FetchType.EAGER)
    private UnitOfMeasure unitOfMeasure;

    /** The value. */
    @Column
    private Double value;

    /**
     * Gets the id.
     *
     * @return the id
     */
    public Long getId() {
        return this.id;
    }

    /**
     * Sets the id.
     *
     * @param id the new id
     */
    public void setId(final Long id) {
        this.id = id;
    }

    /**
     * Gets the version.
     *
     * @return the version
     */
    public int getVersion() {
        return this.version;
    }

    /**
     * Sets the version.
     *
     * @param version the new version
     */
    public void setVersion(final int version) {
        this.version = version;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        if (id != null) {
            return id.equals(((PackagingDimension) that).id);
        }
        return super.equals(that);
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        if (id != null) {
            return id.hashCode();
        }
        return super.hashCode();
    }

    /**
     * Gets the dimension type.
     *
     * @return the dimension type
     */
    public DimensionType getDimensionType() {
        return this.dimensionType;
    }

    /**
     * Sets the dimension type.
     *
     * @param dimensionType the new dimension type
     */
    public void setDimensionType(final DimensionType dimensionType) {
        this.dimensionType = dimensionType;
    }

    /**
     * Gets the unit of measure.
     *
     * @return the unit of measure
     */
    public UnitOfMeasure getUnitOfMeasure() {
        return this.unitOfMeasure;
    }

    /**
     * Sets the unit of measure.
     *
     * @param unitOfMeasure the new unit of measure
     */
    public void setUnitOfMeasure(final UnitOfMeasure unitOfMeasure) {
        this.unitOfMeasure = unitOfMeasure;
    }

    /**
     * Gets the value.
     *
     * @return the value
     */
    public Double getValue() {
        return this.value;
    }

    /**
     * Sets the value.
     *
     * @param value the new value
     */
    public void setValue(final Double value) {
        this.value = value;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        String result = getClass().getSimpleName() + " ";
        if (value != null)
            result += "value: " + value;
        return result;
    }
}