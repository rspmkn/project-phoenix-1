package com.apd.phoenix.service.integration.multitenancy;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Default;
import javax.enterprise.inject.Produces;

@ApplicationScoped
public class ThreadLocalTenantResolver implements CurrentTenantIdentifierResolver {

    private static final ThreadLocal<Long> tenantId = new ThreadLocal<Long>();

    public Long resolveCurrentTenantIdentifier() {
        return tenantId.get();
    }

    @SuppressWarnings("static-access")
    public void setTenant(Long tenant) {
        this.tenantId.set(tenant);
    }

    @Produces
    @Default
    @TenantScoped
    public TenantIdentifier createTenantIdentity() {
        TenantIdentifier id = new TenantIdentifier();
        id.setTenant(resolveCurrentTenantIdentifier());
        return id;
    }

}