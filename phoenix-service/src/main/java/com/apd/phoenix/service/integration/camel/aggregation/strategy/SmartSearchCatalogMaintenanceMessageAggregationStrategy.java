package com.apd.phoenix.service.integration.camel.aggregation.strategy;

import java.util.HashMap;
import java.util.Map;
import org.apache.camel.Exchange;
import org.apache.camel.processor.aggregate.AggregationStrategy;
import com.apd.phoenix.service.search.integration.client.backend.SmartSearchCatalogMaintenanceMessage;
import com.apd.phoenix.service.search.integration.client.backend.SmartSearchCatalogMaintenanceMessage.SmartSearchCatalogMaintenanceMessageContent;

public class SmartSearchCatalogMaintenanceMessageAggregationStrategy implements AggregationStrategy {

    public Exchange aggregate(Exchange oldExchange, Exchange newExchange) {
        if (oldExchange != null) {
            SmartSearchCatalogMaintenanceMessage newBody = (SmartSearchCatalogMaintenanceMessage) newExchange.getIn()
                    .getBody();
            SmartSearchCatalogMaintenanceMessage oldBody = (SmartSearchCatalogMaintenanceMessage) oldExchange.getIn()
                    .getBody();
            for (Map.Entry<String, Map<Long, SmartSearchCatalogMaintenanceMessageContent>> entry : oldBody.getMessage()
                    .entrySet()) {
                String key = entry.getKey();
                Map<Long, SmartSearchCatalogMaintenanceMessageContent> value = entry.getValue();
                if (!newBody.getMessage().containsKey(key) && !value.isEmpty()) {
                    newBody.getMessage().put(key, new HashMap<Long, SmartSearchCatalogMaintenanceMessageContent>());
                }
                for (Map.Entry<Long, SmartSearchCatalogMaintenanceMessageContent> subEntry : value.entrySet()) {
                    newBody.getMessage().get(key).put(subEntry.getKey(), subEntry.getValue());
                }
            }
        }
        return newExchange;
    }
}