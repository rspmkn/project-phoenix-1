/*
 * To change this template, choose Tools | Templates and open the template in
 * the editor.
 */
package com.apd.phoenix.service.executor.entities;

/**
 *
 * @author salaboy
 */
public enum STATUS {
    QUEUED, DONE, CANCELLED, ERROR, RETRYING, RUNNING;

}
