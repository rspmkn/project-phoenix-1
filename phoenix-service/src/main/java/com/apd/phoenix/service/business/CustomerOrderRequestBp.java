package com.apd.phoenix.service.business;

import java.util.List;
import java.util.UUID;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import com.apd.phoenix.service.model.Address;
import com.apd.phoenix.service.model.CustomerOrder;
import com.apd.phoenix.service.model.CustomerOrderRequest;
import com.apd.phoenix.service.persistence.jpa.CustomerOrderRequestDao;

@Stateless
@LocalBean
public class CustomerOrderRequestBp extends AbstractBp<CustomerOrderRequest> {

    public String createRequestToken(CustomerOrder customerOrder) {
        CustomerOrderRequest srt = new CustomerOrderRequest();
        srt.setCustomerOrder(customerOrder);
        srt.setToken(UUID.randomUUID().toString());
        try {
            srt = dao.create(srt);
        }
        catch (Exception e) {
            //Try a new token
            srt.setToken(UUID.randomUUID().toString());
            srt = dao.create(srt);
        }

        return srt.getToken();
    }

    public CustomerOrder getCustomerOrderByToken(String token) {
        List<CustomerOrderRequest> response = ((CustomerOrderRequestDao) dao).getCustomerOrderRequestByToken(token);
        if (response == null || response.isEmpty()) {
            return null;
        }
        return response.get(0).getCustomerOrder();

    }

    @Inject
    public void initDao(CustomerOrderRequestDao dao) {
        this.dao = dao;
    }
}
