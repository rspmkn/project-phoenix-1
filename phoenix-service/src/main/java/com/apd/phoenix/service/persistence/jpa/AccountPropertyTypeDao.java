package com.apd.phoenix.service.persistence.jpa;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import com.apd.phoenix.service.model.AccountPropertyType;

/**
 * Session Bean implementation class AccountPropertyTypeDao
 */
@Stateless
@LocalBean
public class AccountPropertyTypeDao extends AbstractDao<AccountPropertyType> {

}
