/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.service.model.dto;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 *
 * @author nreidelb
 */
public class CatalogXItemDto implements Serializable {

    private static final long serialVersionUID = 8057421963383534104L;

    private ItemDto item;
    private BigDecimal price;
    private CatalogDto catalog;
    private Boolean modified;
    private Boolean added;
    private Boolean deleted;
    private UnitOfMeasureDto customerUom;

    /**
     * @return the item
     */
    public ItemDto getItem() {
        return item;
    }

    /**
     * @param item the item to set
     */
    public void setItem(ItemDto item) {
        this.item = item;
    }

    /**
     * @return the price
     */
    public BigDecimal getPrice() {
        return price;
    }

    /**
     * @param price the price to set
     */
    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    /**
     * @return the catalog
     */
    public CatalogDto getCatalog() {
        return catalog;
    }

    /**
     * @param catalog the catalog to set
     */
    public void setCatalog(CatalogDto catalog) {
        this.catalog = catalog;
    }

    /**
     * @return the modified
     */
    public Boolean getModified() {
        return modified;
    }

    /**
     * @param modified the modified to set
     */
    public void setModified(Boolean modified) {
        this.modified = modified;
    }

    /**
     * @return the added
     */
    public Boolean getAdded() {
        return added;
    }

    /**
     * @param added the added to set
     */
    public void setAdded(Boolean added) {
        this.added = added;
    }

    /**
     * @return the deleted
     */
    public Boolean getDeleted() {
        return deleted;
    }

    /**
     * @param deleted the deleted to set
     */
    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public UnitOfMeasureDto getCustomerUom() {
        return customerUom;
    }

    public void setCustomerUom(UnitOfMeasureDto customerUom) {
        this.customerUom = customerUom;
    }

}
