package com.apd.phoenix.service.business;

import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import com.apd.phoenix.service.model.Catalog;
import com.apd.phoenix.service.model.CatalogUpload;
import com.apd.phoenix.service.persistence.jpa.CatalogUploadDao;

/**
 * This class provides business process methods for Addresses.
 * 
 * @author RHC
 *
 */
@Stateless
@LocalBean
public class CatalogUploadBp extends AbstractBp<CatalogUpload> {

    @Inject
    SyncItemResultSummaryBp syncItemResultSummaryBp;

    /**
     * This method is injected with the correct Dao for initialization
     * 
     * @param dao
     */
    @Inject
    public void initDao(CatalogUploadDao dao) {
        this.dao = dao;
    }

    public boolean existsByCorrelationId(String correlationId) {
        return ((CatalogUploadDao) dao).existsByCorrelationId(correlationId);
    }

    public CatalogUpload getByCorrelationId(String correlationId) {
        return ((CatalogUploadDao) dao).getByCorrelationId(correlationId);
    }

    public Long getTotalItems(String correlationId) {
        return ((CatalogUploadDao) dao).getTotalItems(correlationId);
    }

    public List<String> getCorrelationIds(long catalogId) {
        return ((CatalogUploadDao) dao).getCorrelationIds(catalogId);
    }

    public void removeRecords(String correlationId) {
        ((CatalogUploadDao) dao).removeRecords(correlationId);
    }

    public String getCorrelationId(Catalog catalog) {
        if (catalog == null) {
            return "";
        }
        else {
            return ((CatalogUploadDao) dao).getCorrelationId(catalog);
        }
    }

    public List<String> getCorrelationIds() {
        return ((CatalogUploadDao) dao).getCorrelationIds();
    }

    public boolean needsFinishing(String correlationId) {
        CatalogUpload catalogUpload = getByCorrelationId(correlationId);
        if (catalogUpload != null) {
            return (!catalogUpload.isCompleted() && syncItemResultSummaryBp.getCount(correlationId).compareTo(
                    catalogUpload.getTotalItems()) == 0);
        }
        else {
            return false;
        }
    }

    public void markAsFinished(String correlationId) {
        ((CatalogUploadDao) dao).markAsFinished(correlationId);
    }
}
