package com.apd.phoenix.service.payment.ws.client;

import com.apd.phoenix.service.payment.ws.CreditCardTransactionCreditCard;

/**
 *
 * @author dnorris
 */
public class VerificationTransaction extends PaymentGatewayTransaction {

    private CreditCardTransactionCreditCard creditCard;

    public CreditCardTransactionCreditCard getCreditCard() {
        return creditCard;
    }

    public void setCreditCard(CreditCardTransactionCreditCard creditCard) {
        this.creditCard = creditCard;
    }

}
