package com.apd.phoenix.service.camel;

import java.util.Calendar;
import java.util.TimeZone;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Used to generate the RoutePolicy for delaying queue startup.
 * 
 * @author RHC
 *
 */
@Startup
@Singleton
public class SmartSearchCatalogDelayBean {

    private static final long TOTAL_DELAY_MILLI = 12600000l;
    private static final String SMART_SEARCH_TIMEZONE = "US/Central";
    private static final Logger LOG = LoggerFactory.getLogger(SmartSearchCatalogDelayBean.class);

    /**
     * This method returns a long indicating the number of milliseconds that the next message should be delayed.
     * This is necessary, since Smart Search can have downtimes between 10:30 PM and 1 AM central time each day.
     * If a message is on the queue during the downtime, the consumption is paused until the downtime is over.
     * 
     * @return
     */
    public long computeDelay() {
        Calendar oneAmToday = Calendar.getInstance();
        oneAmToday.setTimeZone(TimeZone.getTimeZone(SMART_SEARCH_TIMEZONE));
        oneAmToday.set(Calendar.HOUR_OF_DAY, 1);
        oneAmToday.set(Calendar.MINUTE, 0);
        oneAmToday.set(Calendar.SECOND, 0);

        Calendar tenThirtyPmToday = Calendar.getInstance();
        tenThirtyPmToday.setTimeZone(TimeZone.getTimeZone(SMART_SEARCH_TIMEZONE));
        tenThirtyPmToday.set(Calendar.HOUR_OF_DAY, 22);
        tenThirtyPmToday.set(Calendar.MINUTE, 30);
        tenThirtyPmToday.set(Calendar.SECOND, 0);

        Calendar now = Calendar.getInstance();

        if (now.getTimeInMillis() - tenThirtyPmToday.getTimeInMillis() > 0) {
            LOG.info("Delaying until after Smart Search downtime");
            return TOTAL_DELAY_MILLI - (now.getTimeInMillis() - tenThirtyPmToday.getTimeInMillis());
        }
        else if (oneAmToday.getTimeInMillis() - now.getTimeInMillis() > 0) {
            LOG.info("Delaying until after Smart Search downtime");
            return oneAmToday.getTimeInMillis() - now.getTimeInMillis();
        }
        else {
            return 0l;
        }
    }
}
