package com.apd.phoenix.service.persistence.jpa;

import java.math.BigDecimal;
import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.Query;
import javax.persistence.TemporalType;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;
import com.apd.phoenix.service.business.EntityComparator;
import com.apd.phoenix.service.model.CarouselDisplay;
import com.apd.phoenix.service.model.Catalog;
import com.apd.phoenix.service.model.Catalog.SearchType;
import com.apd.phoenix.service.model.FavoritesList;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Map;

/**
 * Catalog DAO stub
 *
 * @author RHC
 *
 */
@Stateless
@LocalBean
public class CatalogDao extends AbstractDao<Catalog> {

    private static final String USPS_CUSTOMER_NAME = "usps";

    private static final String CATALOG_ITEMS_HASH = "SELECT COALESCE(MOD(SUM(COALESCE(CXI.ITEM_ID, 0)), 1000000), 0) AS HASH FROM CATALOGXITEM CXI JOIN ITEMPROPERTYTYPE avail ON avail.NAME = 'availability' LEFT JOIN CATALOGXITEMXITEMPROPERTYTYPE availProp ON (availProp.CATALOGXITEM_ID = CXI.ID AND availProp.TYPE_ID = avail.id) WHERE (availProp.VALUE IS NULL OR availProp.value != 'no') AND cxi.CATALOG_ID = :catalogId ";

    private static final String CATALOG_CORE_ITEMS_HASH = CATALOG_ITEMS_HASH
            + "AND (CXI.COREITEMSTARTDATE IS NULL OR CXI.COREITEMSTARTDATE < :now) AND (CXI.COREITEMEXPIRATIONDATE IS NULL OR CXI.COREITEMEXPIRATIONDATE > :now) AND (CXI.COREITEMSTARTDATE IS NOT NULL OR CXI.COREITEMEXPIRATIONDATE IS NOT NULL)";

    @Override
    public Catalog update(Catalog catalog) {
        Catalog oldCatalog = null;
        if (catalog.getId() != null) {
            oldCatalog = findById(catalog.getId(), Catalog.class);
        }
        catalog = super.update(catalog);
        if (catalog.getCustomer() != null && oldCatalog != null) {
            if (USPS_CUSTOMER_NAME.equals(catalog.getCustomer().getApdAssignedAccountId())) {
                if (changedRelevantFields(catalog, oldCatalog)) {
                    catalog.setChangedItemRelevantFields(Boolean.TRUE);
                }
            }
        }
        return super.update(catalog);
    }

    public Catalog hydrateForSearchResults(Catalog toHydrate) {
        String hql = "SELECT catalog FROM Catalog AS catalog" + " LEFT JOIN FETCH catalog.customer"
                + " LEFT JOIN FETCH catalog.vendor " + " WHERE catalog.id = " + toHydrate.getId().toString();
        //Creates the query
        Query query = entityManager.createQuery(hql);
        setItemsReturned(query, 0, 0);

        return getSingleResultOrNull(query);
    }

    public List<FavoritesList> getFavoritesLists(Catalog catalog) {
    	List<FavoritesList> toReturn = new ArrayList<>();
        String hql = "SELECT DISTINCT catalog FROM Catalog catalog"
                + " LEFT JOIN FETCH catalog.favorites LEFT JOIN FETCH catalog.parent "
                + " where catalog.id = :catalogId ";
        while (catalog != null) {
	        //Creates the query
	        Query query = entityManager.createQuery(hql);
	        query.setParameter("catalogId", catalog.getId());
	        catalog = (Catalog) query.getSingleResult();
	        toReturn.addAll(catalog.getFavorites());
	        if (catalog.equals(catalog.getParent())) {
	        	break;
	        }
	        catalog = catalog.getParent();
        }
        Collections.sort(toReturn, new EntityComparator());
        return toReturn;
    }

    public long getCatalogSize(Catalog catalog) {
        String itemHql = "SELECT COUNT(DISTINCT item) FROM Item item WHERE item.vendorCatalog.id = :catalogId";
        //Creates the query
        Query itemQuery = entityManager.createQuery(itemHql);
        itemQuery.setParameter("catalogId", catalog.getId());
        long vendorQuantity = (long) itemQuery.getSingleResult();
        if (vendorQuantity != 0) {
            return vendorQuantity;
        }

        String customerHql = "SELECT COUNT(DISTINCT item) FROM CatalogXItem item WHERE item.catalog.id = :catalogId";
        //Creates the query
        Query customerQuery = entityManager.createQuery(customerHql);
        customerQuery.setParameter("catalogId", catalog.getId());
        long customerQuantity = (long) customerQuery.getSingleResult();
        return customerQuantity;
    }

    public Long getParentCatalogId(long catalogID) {
        String hql = "SELECT parent.id FROM Catalog cat JOIN cat.parent AS parent " + "WHERE cat.id = :catalogId";
        //Creates the query
        Query query = entityManager.createQuery(hql);
        query.setParameter("catalogId", catalogID);

        Long parentId = getSingleResultOrNull(query);
        if (parentId != null) {
            return parentId;
        }
        return null;
    }

    public Catalog hydrateCustomerName(Catalog catalog) {
        String hql = "SELECT cat FROM Catalog cat LEFT JOIN cat.customer " + "WHERE cat.id = :catalogId";
        //Creates the query
        Query query = entityManager.createQuery(hql);
        query.setParameter("catalogId", catalog.getId());

        return getSingleResultOrNull(query);
    }

    private boolean changedRelevantFields(Catalog catalog, Catalog oldCatalog) {
        return (oldCatalog == null || !datesMatch(catalog.getExpirationDate(), oldCatalog.getExpirationDate()) || !datesMatch(
                oldCatalog.getStartDate(), catalog.getStartDate()));

    }

    private boolean datesMatch(Date date1, Date date2) {
        return (date1 == null && date2 == null) || (date1.equals(date2));
    }

    @SuppressWarnings("unchecked")
    public List<Catalog> searchCustomerCatalogByName(String name) {
        String hql = "SELECT cat FROM Catalog cat JOIN cat.customer " + "WHERE LOWER(cat.name) LIKE :catalogName";
        //Creates the query
        Query query = entityManager.createQuery(hql);
        query.setParameter("catalogName", "%" + name.toLowerCase() + "%");

        return (List<Catalog>) query.getResultList();
    }

    public String vendorNameOfCatalog(Catalog vendorCatalog) {
        String hql = "SELECT cat.vendor.name FROM Catalog cat WHERE cat.id = :vendorCatalogId";
        //Creates the query
        Query query = entityManager.createQuery(hql);
        query.setParameter("vendorCatalogId", vendorCatalog.getId());

        return getSingleResultOrNull(query);
    }

    public List<Long> getParentCatalogIdList(Long catalogID) {
        List<Long> parentCatalogIDs = new ArrayList<>();
        while (catalogID != null) {
            parentCatalogIDs.add(catalogID);
            catalogID = this.getParentCatalogId(catalogID);
        }        
        return parentCatalogIDs;
    }

    public List<Long> getCatalogUidIdList(Long catalogID) {
        List<Long> uids = new ArrayList<>();
        while (catalogID != null) {
        	Catalog catalog = this.findById(catalogID, Catalog.class);
        	if (catalog.getListUid() != null) {
        		uids.add(catalog.getListUid().getId());
        	}
            catalogID = this.getParentCatalogId(catalogID);
        }        
        return uids;
    }

    public List<Long> getCatalogCoreUidIdList(Long catalogID) {
        List<Long> uids = new ArrayList<>();
        while (catalogID != null) {
        	Catalog catalog = this.findById(catalogID, Catalog.class);
        	if (catalog.getCoreListUid() != null) {
        		uids.add(catalog.getCoreListUid().getId());
        	}
            catalogID = this.getParentCatalogId(catalogID);
        }        
        return uids;
    }

    @SuppressWarnings("unchecked")
    public List<Catalog> getCatalogsToReindex() {
        String hql = "SELECT cat FROM Catalog cat WHERE cat.reindexNightly = true OR cat.reindexDate = :today";
        //Creates the query
        Query query = entityManager.createQuery(hql);
        query.setParameter("today", new Date(), TemporalType.DATE);

        return (List<Catalog>) query.getResultList();
    }

    @SuppressWarnings("unchecked")
    public List<Catalog> getCatalogsToRegenerateCsv() {
        String hql = "SELECT cat FROM Catalog cat WHERE cat.regenerateCsvNightly = true OR cat.regenerateCsvDate = :today";
        //Creates the query
        Query query = entityManager.createQuery(hql);
        query.setParameter("today", new Date(), TemporalType.DATE);

        return (List<Catalog>) query.getResultList();
    }

    @SuppressWarnings("unchecked")
    public List<Catalog> getSmartOciToRegenerate() {
        String hql = "SELECT cat FROM Catalog cat WHERE cat.regenerateSmartOciDate = :today";
        //Creates the query
        Query query = entityManager.createQuery(hql);
        query.setParameter("today", new Date(), TemporalType.DATE);

        return (List<Catalog>) query.getResultList();
    }

    @SuppressWarnings("unchecked")
    public List<Long> getCatalogsToRehash() {
        String hql = "SELECT cat.id FROM Catalog cat WHERE cat.searchType = :searchType";
        //Creates the query
        Query query = entityManager.createQuery(hql);
        query.setParameter("searchType", SearchType.SMART_SEARCH);
        return (List<Long>) query.getResultList();
    }

    public void setNewHashOnCatalog(Long catalogId) {

        Long itemHash = getHash(catalogId, CATALOG_ITEMS_HASH);
        Long coreItemHash = getHash(catalogId, CATALOG_CORE_ITEMS_HASH);

        String hql = "UPDATE Catalog catalog set itemHash = :itemHash, coreItemHash = :coreItemHash WHERE catalog.id = :catalogId";
        Query query = entityManager.createQuery(hql);
        query.setParameter("itemHash", itemHash != null ? itemHash : 0l);
        query.setParameter("coreItemHash", coreItemHash != null ? coreItemHash : 0l);
        query.setParameter("catalogId", catalogId);
        query.executeUpdate();

        String deletionHql = "UPDATE PendingSmartSearchDeletion pending set hash = :itemHash WHERE pending.catalog.id = :catalogId";
        Query deletionQuery = entityManager.createQuery(deletionHql);
        deletionQuery.setParameter("itemHash", itemHash != null ? itemHash : 0l);
        deletionQuery.setParameter("catalogId", catalogId);
        deletionQuery.executeUpdate();
    }

    @SuppressWarnings("unchecked")
    private Long getHash(Long catalogId, String query) {

        Session session = (Session) entityManager.getDelegate();

        final org.hibernate.Query itemQuery = session.createSQLQuery(query);
        itemQuery.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
        itemQuery.setParameter("catalogId", catalogId);
        if (query.contains(":now")) {
            itemQuery.setParameter("now", new Date());
        }

        Map<String, Object> result = (Map<String, Object>) itemQuery.uniqueResult();

        if (result != null && result.containsKey("HASH")) {
            return ((BigDecimal) result.get("HASH")).longValue();
        }
        return 0l;
    }

    @SuppressWarnings("unchecked")
    public List<Catalog> getCatalogsWithWrongListUid() {
        String hql = "SELECT cat FROM Catalog cat LEFT JOIN cat.listUid AS listUid LEFT JOIN cat.coreListUid AS coreListUid WHERE cat.searchType = :searchType AND (listUid is null OR listUid.hash != cat.itemHash OR coreListUid is null OR coreListUid.hash != cat.coreItemHash) AND cat.itemHash is not null AND cat.coreItemHash is not null";
        //Creates the query
        Query query = entityManager.createQuery(hql);
        query.setParameter("searchType", SearchType.SMART_SEARCH);
        return (List<Catalog>) query.getResultList();
    }

    public Catalog hydrateCarousel(Catalog catalog) {
        Catalog returnCatalog = findById(catalog.getId(), Catalog.class);
        for (CarouselDisplay item : returnCatalog.getCarouselDisplayItems()) {
            item.getBackgroundImageUrl();
        }
        return returnCatalog;
    }
}
