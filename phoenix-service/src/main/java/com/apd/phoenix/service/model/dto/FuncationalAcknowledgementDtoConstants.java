package com.apd.phoenix.service.model.dto;

public interface FuncationalAcknowledgementDtoConstants {

    public static final String ACCEPTED = "Accepted";
    public static final String ACCEPTED_WITH_ERRORS = "Accepted but contains errors";
    public static final String REJECTED = "Rejected";
}
