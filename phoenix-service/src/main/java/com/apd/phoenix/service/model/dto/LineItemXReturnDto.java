package com.apd.phoenix.service.model.dto;

import java.io.Serializable;
import java.math.BigInteger;

public class LineItemXReturnDto implements Serializable {

    private static final long serialVersionUID = -1562274724122823031L;

    private LineItemDto lineItemDto;
    private BigInteger quantity;

    public LineItemDto getLineItemDto() {
        return lineItemDto;
    }

    public void setLineItemDto(LineItemDto lineItemDto) {
        this.lineItemDto = lineItemDto;
    }

    public BigInteger getQuantity() {
        return quantity;
    }

    public void setQuantity(BigInteger quantity) {
        this.quantity = quantity;
    }
}
