package com.apd.phoenix.service.message.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.jms.Connection;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.ObjectMessage;
import javax.jms.Queue;
import javax.jms.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.message.api.Message;
import com.apd.phoenix.service.message.api.MessageSender;
import com.apd.phoenix.service.model.OrderLog.EventType;
import com.apd.phoenix.service.model.dto.InvoiceDto;
import com.apd.phoenix.service.model.dto.InvoiceWrapper;
import com.apd.phoenix.service.model.dto.PickDto;
import com.apd.phoenix.service.model.dto.PurchaseOrderDto;
import com.apd.phoenix.service.persistence.multitenancy.impl.CurrentTenantIdentifierResolverImpl;

@Stateless
@LocalBean
public class CSVMessageSender extends MessageSender {

    public static final String CSV_OUTBOUND_PO = "csv-outbound-po";

    private static final Logger LOGGER = LoggerFactory.getLogger(CSVMessageSender.class);

    @Resource(mappedName = "java:/activemq/csv-ship-confirmation")
    private Queue outboundCSVShipConfirmationQueue;

    @Resource(mappedName = "java:/activemq/" + CSV_OUTBOUND_PO)
    private Queue outboundCSVPurchaseOrderQueue;

    @Resource(mappedName = "java:/activemq/csv-outbound-marquette-customer-invoice")
    private Queue outboundMarquetteCustomerInvoiceQueue;

    @PostConstruct
    public void setupQueues() {
        this.outboundShipConfirmationQueue = outboundCSVShipConfirmationQueue;
        this.outboundPurchaseOrderQueue = outboundCSVPurchaseOrderQueue;
    }

    @SuppressWarnings("static-method")
    private ObjectMessage createCsvJmsMessage(Session session, Serializable csvMessage, String partnerId, String apdPo) {
        ObjectMessage objectMessage = null;
        try {
            objectMessage = session.createObjectMessage(csvMessage);
            if (partnerId != null) {
                objectMessage.setStringProperty("partnerId", partnerId);
            }
            if (apdPo != null) {
                objectMessage.setStringProperty("apdPo", apdPo);
            }
            objectMessage.setLongProperty("tenantId", CurrentTenantIdentifierResolverImpl.getCurrentTenant());
        }
        catch (JMSException e) {
            LOGGER.error(e.toString());
        }
        return objectMessage;
    }

    @Override
    public javax.jms.Message createPurchaseOrderMessage(Session session, PurchaseOrderDto poDto, Boolean resend) {
        return createCsvJmsMessage(session, poDto, poDto.getPartnerId(), poDto.getApdPoNumber());
    }

    @Override
    public javax.jms.Message createShipConfirmationMessage(Session session, List<PickDto> pickList) {
        ObjectMessage objectMessage = null;
        try {
            objectMessage = session.createObjectMessage(new ArrayList<PickDto>(pickList));
            objectMessage.setLongProperty("tenantId", CurrentTenantIdentifierResolverImpl.getCurrentTenant());
        }
        catch (JMSException e) {
            LOGGER.error(e.toString());
        }
        return objectMessage;
    }

    @Override
    public boolean sendMessage(Message message, EventType eventType) {
        return false;
    }

    @Override
    public boolean sendMarquetteInvoice(List<InvoiceDto> invoice, Boolean credit) {

        Queue outboundQueue = outboundMarquetteCustomerInvoiceQueue;
        Connection connection = null;
        Session msession;
        MessageProducer messageProducer;

        try {
            InvoiceWrapper customerInvoiceWrapper = new InvoiceWrapper();
            customerInvoiceWrapper.setDtos(invoice);

            connection = connectionFactory.createConnection();
            msession = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);

            ObjectMessage message = createCsvJmsMessage(msession, customerInvoiceWrapper, null, null);
            if (credit) {
                message.setStringProperty("credit", "CREDIT-");
            }
            else {
                message.setStringProperty("credit", "");
            }

            messageProducer = msession.createProducer(outboundQueue);
            messageProducer.send(message);
        }
        catch (Exception e) {
            LOGGER.error(e.toString());
        }

        try {
            if (connection != null) {
                connection.close();
            }
        }
        catch (JMSException e) {
            LOGGER.error(e.toString());
        }
        return true;
    }

}
