package com.apd.phoenix.service.catalog;

import java.util.ArrayList;
import java.util.HashSet;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.business.CatalogBp;
import com.apd.phoenix.service.business.CatalogBp.SyncAction;
import com.apd.phoenix.service.business.CatalogBpNoTransaction;
import com.apd.phoenix.service.business.CatalogUploadBp;
import com.apd.phoenix.service.business.SyncItemResultSummaryBp;
import com.apd.phoenix.service.model.Catalog;
import com.apd.phoenix.service.model.CatalogUpload;
import com.apd.phoenix.service.model.SyncItemResult;
import com.apd.phoenix.service.model.SyncItemResultSummary;

@Stateless
@LocalBean
public class CatalogResultAggregator {

    private static final Logger LOGGER = LoggerFactory.getLogger(CatalogResultAggregator.class);

    @Inject
    private CatalogUploadBp catalogUploadBp;

    @Inject
    private CatalogBp catalogBp;

    @Inject
    private SyncItemResultSummaryBp syncItemResultSummaryBp;

    @Inject
    private CatalogBpNoTransaction catalogBpNoTransaction;

    @Inject
    private CatalogCsvCompleteCheckEntryPoint entryPoint;

    public void process(SyncItemResultSummary summary, ArrayList<CatalogChangeResult> catalogChangeResults) {

        String correlationId = null;
        SyncAction action = null;
        if (catalogChangeResults != null && !catalogChangeResults.isEmpty()) {
            correlationId = catalogChangeResults.get(0).getCorrelationId();
            action = catalogChangeResults.get(0).getAction();
        }
        if (correlationId == null || action == null) {
            return;
        }

        Boolean newUpload = !catalogUploadBp.existsByCorrelationId(correlationId);

        summary.setCorrelationId(correlationId);
        summary.setTotal(catalogChangeResults.size());
        summary.setResults(new HashSet<SyncItemResult>());

        for (CatalogChangeResult catalogChangeResult : catalogChangeResults) {
            SyncItemResult syncItemResult = catalogChangeResult.getResult();
            summary.getResults().add(syncItemResult);
            syncItemResult.setSummary(summary);
            syncItemResult.setCorrelationId(correlationId);
            if (newUpload) {
                CatalogUpload catalogUpload = new CatalogUpload();
                catalogUpload.setCatalogId(catalogChangeResult.getCatalogId());
                catalogUpload.setOldCatalogId(catalogChangeResult.getOldCatalogId());
                catalogUpload.setCorrelationId(correlationId);
                catalogUpload.setProcessedItems(1L);
                catalogUpload.setAction(action);
                catalogUpload.setTotalItems(catalogChangeResult.getTotalItems());
                catalogUpload.setPersistChanges(catalogChangeResult.getPersistChanges());
                catalogUploadBp.create(catalogUpload);
                newUpload = false;
            }
        }

        summary = syncItemResultSummaryBp.create(summary);

        entryPoint.scheduleRequest(correlationId);
    }

    public void finished(String correlationId) {
        LOGGER.info("Aggregating for {}....", correlationId);
        CatalogUpload catalogUpload = catalogUploadBp.getByCorrelationId(correlationId);
        Catalog catalog = catalogBp.findById(catalogUpload.getCatalogId(), Catalog.class);
        if (catalog != null) {
            Catalog parent = catalog.getParent();
            SyncItemResultSummary summary = syncItemResultSummaryBp.getSummary(correlationId);
            catalogBpNoTransaction.postParseCsv(catalog, catalogUpload.getAction(), correlationId, summary, parent,
                    catalogUpload.getPersistChanges());
            if (catalogUpload.getPersistChanges()) {
                catalogBp.deleteDiffCsv(catalog);
            }
            catalog = catalogBp.update(catalog);
        }
        catalogUploadBp.markAsFinished(correlationId);
        LOGGER.info("Finished aggreagating for {}", correlationId);
    }
}
