package com.apd.phoenix.service.persistence.jpa;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.Query;
import com.apd.phoenix.service.model.Catalog;
import com.apd.phoenix.service.model.CatalogUpload;

@Stateless
public class CatalogUploadDao extends AbstractDao<CatalogUpload> {

    public boolean existsByCorrelationId(String correlationId) {
        String hql = "SELECT count(catalogUpload) FROM CatalogUpload catalogUpload WHERE catalogUpload.correlationId = :correlationId";
        Query query = entityManager.createQuery(hql);
        query.setParameter("correlationId", correlationId);
        Long results = (Long) query.getSingleResult();
        if (results != null && results > 0) {
            return true;
        }
        else {
            return false;
        }
    }

    public CatalogUpload getByCorrelationId(String correlationId) {
        String hql = "SELECT catalogUpload FROM CatalogUpload catalogUpload WHERE catalogUpload.correlationId = :correlationId";
        Query query = entityManager.createQuery(hql);
        query.setParameter("correlationId", correlationId);
        List<CatalogUpload> results = (List<CatalogUpload>) query.getResultList();
        if (results != null && !results.isEmpty()) {
            return results.get(0);
        }
        else {
            return null;
        }
    }

    public Long getTotalItems(String correlationId) {
        String hql = "SELECT catalogUpload.totalItems FROM CatalogUpload catalogUpload WHERE catalogUpload.correlationId = :correlationId";
        Query query = entityManager.createQuery(hql);
        query.setParameter("correlationId", correlationId);
        List<Long> results = (List<Long>) query.getResultList();
        if (results != null && !results.isEmpty()) {
            return results.get(0);
        }
        else {
            return null;
        }
    }

    public List<String> getCorrelationIds(long catalogId) {
        String hql = "SELECT distinct catalogUpload.correlationId FROM CatalogUpload catalogUpload WHERE catalogUpload.catalogId = :catalogId";
        Query query = entityManager.createQuery(hql);
        query.setParameter("catalogId", catalogId);
        return (List<String>) query.getResultList();
    }

    public void removeRecords(String correlationId) {
        String hql = "DELETE FROM CatalogUpload WHERE correlationId = :correlationId";
        Query query = entityManager.createQuery(hql);
        query.setParameter("correlationId", correlationId);
        query.executeUpdate();
    }

    public String getCorrelationId(Catalog catalog) {
        String hql = "SELECT catalogUpload.correlationId FROM CatalogUpload catalogUpload WHERE catalogUpload.catalogId = :catalogId";
        Query query = entityManager.createQuery(hql);
        query.setParameter("catalogId", catalog.getId());
        List<String> results = query.getResultList();
        if (results != null && !results.isEmpty()) {
            return results.get(0);
        }
        else {
            return null;
        }
    }

    public List<String> getCorrelationIds() {
        String hql = "SELECT DISTINCT(catalogUpload.correlationId) FROM CatalogUpload catalogUpload WHERE completed = false";
        Query query = entityManager.createQuery(hql);
        return (List<String>) query.getResultList();
    }

    public void markAsFinished(String correlationId) {
        String hql = "UPDATE CatalogUpload SET completed = true WHERE correlationId = :correlationId";
        Query query = entityManager.createQuery(hql);
        query.setParameter("correlationId", correlationId);
        query.executeUpdate();
    }

}
