package com.apd.phoenix.service.business;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import org.apache.commons.lang.StringUtils;
import com.apd.phoenix.service.model.Account;
import com.apd.phoenix.service.model.AccountPropertyType.AccountPropertyTypeEnum;
import com.apd.phoenix.service.model.AccountXAccountPropertyType;
import com.apd.phoenix.service.model.AccountXCredentialXUser;
import com.apd.phoenix.service.model.Address;
import com.apd.phoenix.service.model.AddressXAddressPropertyType;
import com.apd.phoenix.service.model.MiscShipTo;
import com.apd.phoenix.service.model.dto.AddressDto;
import com.apd.phoenix.service.model.dto.AddressUpdateDto;
import com.apd.phoenix.service.model.dto.MiscShipToDto;
import com.apd.phoenix.service.model.factory.DtoFactory;
import com.apd.phoenix.service.persistence.jpa.AddressDao;

/**
 * This class provides business process methods for Addresses.
 * 
 * @author RHC
 *
 */
@Stateless
@LocalBean
public class AddressBp extends AbstractBp<Address> {

    public static final String WALMART_STORES_NAME = "WAL-MART STORES, INC.";
    public static final String WALMART_DEPT_99_NAME = "Walmart Dept 99";
    public static final String WALMART_KITS_NAME = "WalMart Kits & Furniture";

    @Inject
    AddressDao addressDao;

    @Inject
    private AccountBp accountBp;

    @Inject
    private MiscShipToBp miscShipToBp;

    private String[] addressDtoFieldsToIgnore = { "companyName", "serialVersionUID" };

    private String[] miscShipToDtoFieldsToIgnore = { "serialVersionUID" };

    /**
     * This method is injected with the correct Dao for initialization
     * 
     * @param dao
     */
    @Inject
    public void initDao(AddressDao dao) {
        this.dao = dao;
    }

    /**
     * This method is used to make changes to an address. It can either change the address for all 
     * entities, or persist the address and leave all other instances the same.
     * 
     * @param updatedAddress
     * @param changeForEverything
     * @return
     */
    public Address update(Address updatedAddress, boolean changeForEverything) {
        if (changeForEverything) {
            return dao.update(updatedAddress);
        }
        else {
            return dao.create(cloneEntity(updatedAddress));
        }
    }

    @Override
    public Address cloneEntity(Address toClone) {
        Address toReturn = new Address();
        return cloneEntity(toClone, toReturn);
    }

    @Override
    public Address cloneEntity(Address source, Address target) {
        if (source == null) {
            return null;
        }
        //creates the clone, with an empty Set of fields
        super.cloneEntity(source, target);
        target.setAccount(null);
        target.setPerson(null);
        target.setVendor(null);

        target.setFields(new HashSet<AddressXAddressPropertyType>());
        //for each field in the cloned entity, copies it, sets its id to null, and adds to the clone
        for (AddressXAddressPropertyType f : source.getFields()) {
            AddressXAddressPropertyType copyField = new AddressXAddressPropertyType();
            copyField.setType(f.getType());
            copyField.setValue(f.getValue());
            copyField.setId(null);
            target.getFields().add(copyField);
        }

        if (source.getMiscShipTo() != null) {
            target.setMiscShipTo(miscShipToBp.cloneEntity(source.getMiscShipTo()));
        }

        //returns the target
        return target;
    }

    public List<Address> addressesForCashout(AccountXCredentialXUser axcxu, String addressType) {
        return ((AddressDao) dao).addressesForCashout(axcxu, addressType);
    }

    public String newShipToId(Address address, Account account) {
        String shipToIdPrefix = null;
        // first check for an account property
        if (account.getProperties() != null) {
            for (AccountXAccountPropertyType axapt : account.getProperties()) {
                if (axapt.getType().getName().equalsIgnoreCase(AccountPropertyTypeEnum.SHIPTO_ID_PREFIX.getValue())) {
                    shipToIdPrefix = axapt.getValue();
                }
            }
        }
        // Then check for an alpha code embedded in parentheses in the account name
        if (StringUtils.isBlank(shipToIdPrefix)) {
            if (account.getName() != null && account.getName().startsWith("(") && account.getName().contains(")")) {
                shipToIdPrefix = account.getName().substring(account.getName().indexOf("(") + 1,
                        account.getName().indexOf(")"));
            }
        }
        // finally check for sufficiently long prefix in the unique account id
        if (StringUtils.isBlank(shipToIdPrefix)) {
            shipToIdPrefix = account.getApdAssignedAccountId();
            int firstWhitespace = shipToIdPrefix.indexOf(" ");
            if (firstWhitespace > 2) {
                shipToIdPrefix = shipToIdPrefix.substring(0, firstWhitespace);
            }
        }

        shipToIdPrefix = StringUtils.deleteWhitespace(shipToIdPrefix);
        String shipToIdNumber = address.getId().toString();

        // limit the length of the shipto id (precautionary)
        if (shipToIdPrefix.length() + shipToIdNumber.length() > 10) {
            shipToIdNumber = shipToIdNumber.substring(shipToIdPrefix.length() + shipToIdNumber.length() - 10);
        }
        return shipToIdPrefix + shipToIdNumber;
    }

    public void updateIntegratedCustomerAddresses(AddressUpdateDto orgRelationshipsDto) {
        Account searchAccount = new Account();
        //Walmart needs to update 2 accounts
        if (orgRelationshipsDto.getCompanyName().equals(WALMART_STORES_NAME)) {
            orgRelationshipsDto.setCompanyName(WALMART_KITS_NAME);
            updateIntegratedCustomerAddresses(orgRelationshipsDto);
            orgRelationshipsDto.setCompanyName(WALMART_DEPT_99_NAME);
            updateIntegratedCustomerAddresses(orgRelationshipsDto);
        }
        searchAccount.setName(orgRelationshipsDto.getCompanyName());
        List<Account> accountResults = accountBp.searchByExactExample(searchAccount, 0, 0);
        if (accountResults.isEmpty()) {
            return;
        }
        Account account = accountResults.get(0);
        for (AddressDto addressDto : orgRelationshipsDto.getAddresses()) {
            Address toAdd = new Address();
            toAdd.setAccount(account);
            //it's companyName in the DTO, so won't get picked up with reflection. Setting manually.
            toAdd.setCompany(addressDto.getCompanyName());
            DtoFactory.clone(addressDto, toAdd, addressDtoFieldsToIgnore);
            if (addressDto.getMiscShipToDto() != null) {
                toAdd.setMiscShipTo(new MiscShipTo());
                DtoFactory.clone(addressDto.getMiscShipToDto(), toAdd.getMiscShipTo(), miscShipToDtoFieldsToIgnore);
            }
            account.getAddresses().add(toAdd);
        }
        accountBp.update(account);
    }

    public Address retrieve(AddressDto shipToAddressDto, Account account) {
        return ((AddressDao) dao).retrieveAddress(shipToAddressDto, account);
    }

    public Address create(AddressDto shipToAddressDto) {
        if (shipToAddressDto == null) {
            return null;
        }
        Address toReturn = new Address();
        toReturn.setCity(shipToAddressDto.getCity());
        toReturn.setCompany(shipToAddressDto.getCompanyName());
        toReturn.setCountry(shipToAddressDto.getCountry());
        toReturn.setCounty(shipToAddressDto.getCounty());
        toReturn.setGeoCode(shipToAddressDto.getGeoCode());
        toReturn.setLine1(shipToAddressDto.getLine1());
        toReturn.setLine2(shipToAddressDto.getLine2());
        toReturn.setName(shipToAddressDto.getName());
        toReturn.setPendingShipToAuthorization(shipToAddressDto.getPendingShipToAuthorization());
        toReturn.setState(shipToAddressDto.getState());
        toReturn.setZip(shipToAddressDto.getZip());
        if (shipToAddressDto.getMiscShipToDto() != null) {
            toReturn.setMiscShipTo(miscShipToBp.create(shipToAddressDto.getMiscShipToDto()));
        }
        return create(toReturn);
    }

    public static boolean areClones(Address a1, Address a2) {
        Set<String> fieldsNotCloned = new HashSet<>(Arrays.asList("getId", "getVendor", "getPerson", "getAccount"));
        Method[] a1Methods = a1.getClass().getMethods();
        Method[] a2Methods = a2.getClass().getMethods();
        List<Method> a1Getters = new ArrayList<>();
        List<Method> a2Getters = new ArrayList<>();
        for (Method m : a1Methods) {
            String methodName = m.getName();
            if (methodName.startsWith("get") || methodName.startsWith("is")) {
                a1Getters.add(m);
            }
        }        
        for (Method m : a2Methods) {
            String methodName = m.getName();
            if (methodName.startsWith("get") || methodName.startsWith("is")) {
                a2Getters.add(m);
            }
        }
        for (Method m1 : a1Getters) {
            for (Method m2 : a2Getters) {
                if (m1.getName().equals(m2.getName()) &&
                                !fieldsNotCloned.contains(m1.getName())) {
                    try {
                        if (m1.getName().equals("getMiscShipTo")) {
                            if (!AbstractBp.areClones(m1.invoke(m1, new Object[0]), m2.invoke(m2, new Object[0]))) {
                                return false;
                            }
                        } else {
                            if (!AbstractBp.fieldsEqual(m1.invoke(m1, new Object[0]), m2.invoke(m2, new Object[0]))) {
                                return false;
                            }
                        }
                    } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
                    }
                }
            }
        }
        return true;
    }

    public Address populateInboundData(Address shipTo, AddressDto shipToAddressDto) {
        if (shipTo.getMiscShipTo() == null) {
            shipTo.setMiscShipTo(new MiscShipTo());
        }
        if (shipToAddressDto.getMiscShipToDto() == null) {
            shipToAddressDto.setMiscShipToDto(new MiscShipToDto());
        }
        if (StringUtils.isEmpty(shipTo.getMiscShipTo().getRequesterName())) {
            shipTo.getMiscShipTo().setRequesterName(shipToAddressDto.getMiscShipToDto().getRequesterName());
        }
        if (StringUtils.isEmpty(shipTo.getMiscShipTo().getRequesterPhone())) {
            shipTo.getMiscShipTo().setRequesterPhone(shipToAddressDto.getMiscShipToDto().getRequesterPhone());
        }
        if (StringUtils.isEmpty(shipTo.getMiscShipTo().getDivision())) {
            shipTo.getMiscShipTo().setDivision(shipToAddressDto.getMiscShipToDto().getDivision());
        }
        if (StringUtils.isEmpty(shipTo.getMiscShipTo().getFinanceNumber())) {
            shipTo.getMiscShipTo().setFinanceNumber(shipToAddressDto.getMiscShipToDto().getFinanceNumber());
        }
        return shipTo;
    }

    public List<Address> getAddressesForAccountPagenated(long accountId, int start, int addressPageSize) {
        Account account = new Account();
        account.setId(accountId);
        return addressDao.getAddresses(account, start, addressPageSize);
    }

    public Integer numberOfAddresses(Long accountId) {
        return addressDao.getNumberOfAddresses(accountId);
    }

}
