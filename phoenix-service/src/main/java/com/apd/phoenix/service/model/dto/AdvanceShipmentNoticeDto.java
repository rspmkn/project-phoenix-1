package com.apd.phoenix.service.model.dto;

import java.io.Serializable;
import java.util.List;

/**
 * Transaction Dto for the Advanced Shipment Notifications
 * @author RHC
 *
 */
public class AdvanceShipmentNoticeDto implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -1299288227595319054L;

    private List<ShipmentDto> shipmentDtos;

    private String apdPo;

    private String customerPo;

    private String senderId;

    private String orderTransactionId;

    public String getApdPo() {
        return apdPo;
    }

    public void setApdPo(String apdPo) {
        this.apdPo = apdPo;
    }

    public List<ShipmentDto> getShipmentDtos() {
        return shipmentDtos;
    }

    public void setShipmentDtos(List<ShipmentDto> shipmentDtos) {
        this.shipmentDtos = shipmentDtos;
    }

    public String getCustomerPo() {
        return customerPo;
    }

    public void setCustomerPo(String customerPo) {
        this.customerPo = customerPo;
    }

    public String getSenderId() {
        return senderId;
    }

    public void setSenderId(String senderId) {
        this.senderId = senderId;
    }

    public String getOrderTransactionId() {
        return orderTransactionId;
    }

    public void setOrderTransactionId(String orderTransactionId) {
        this.orderTransactionId = orderTransactionId;
    }

}
