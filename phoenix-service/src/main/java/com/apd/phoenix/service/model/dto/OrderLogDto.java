package com.apd.phoenix.service.model.dto;

import java.io.Serializable;
import java.util.Date;
import com.apd.phoenix.service.order.OrderLogConstants;

public class OrderLogDto implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private PurchaseOrderDto customerOrder;

    private EventTypeDto eventType;

    private MessageMetadataDto messageMetadata;

    private Date updateTimestamp;

    public PurchaseOrderDto getCustomerOrder() {
        return customerOrder;
    }

    public void setCustomerOrder(PurchaseOrderDto customerOrder) {
        this.customerOrder = customerOrder;
    }

    public EventTypeDto getEventType() {
        return eventType;
    }

    public void setEventType(EventTypeDto eventType) {
        this.eventType = eventType;
    }

    public MessageMetadataDto getMessageMetadata() {
        return messageMetadata;
    }

    public void setMessageMetadata(MessageMetadataDto messageMetadata) {
        this.messageMetadata = messageMetadata;
    }

    public Date getUpdateTimestamp() {
        return updateTimestamp;
    }

    public void setUpdateTimestamp(Date updateTimestamp) {
        this.updateTimestamp = updateTimestamp;
    }

    public enum EventTypeDto {
        RESENT_NOTFICATION(OrderLogConstants.RESENT_NOTIFICATION), ORDER_ACKNOWLEDGEMENT(
                OrderLogConstants.ORDER_ACKNOWLEDGEMENT), ADVANCED_SHIPMENT_NOTICE(
                OrderLogConstants.ADVANCED_SHIPMENT_NOTICE), DECLINED(OrderLogConstants.DECLINED), @Deprecated
        /** Use VOUCHED instead **/INVOICED(OrderLogConstants.INVOICED), VOUCHED(OrderLogConstants.VOUCHED), ORDERED(
                OrderLogConstants.ORDERED), FUNCTIONAL_ACKNOWLEDGEMENT(OrderLogConstants.FUNCTIONAL_ACKNOWLEDGEMENT), PURCHASE_ORDER_SENT(
                OrderLogConstants.PURCHASE_ORDER_SENT), VENDOR_NOTIFICATION(OrderLogConstants.VENDOR_NOTIFICATION), CUSTOMER_NOTIFICATION(
                OrderLogConstants.CUSTOMER_NOTIFICATION), CUSTOMER_INVOICED(OrderLogConstants.CUSTOMER_INVOICED), TECH_NOTIFICATION(
                OrderLogConstants.TECH_NOTIFICATION), RETURNED(OrderLogConstants.RETURNED), ORDER_REQUEST(
                OrderLogConstants.ORDER_REQUEST), PUNCHOUT_SETUP_REQUEST(OrderLogConstants.PUNCHOUT_SETUP_REQUEST), PUNCHOUT_ORDER_MESSAGE(
                OrderLogConstants.PUNCHOUT_ORDER_MESSAGE), DELIVERY_SIGNATURE(OrderLogConstants.DELIVERY_SIGNATURE), DELIVERY_DRIVER_COMMENTS(
                OrderLogConstants.DELIVERY_DRIVER_COMMENTS), DELIVERY_DRIVER_PHOTO(
                OrderLogConstants.DELIVERY_DRIVER_PHOTO), PURCHASE_ORDER_PARTNER_RESPONSE(
                OrderLogConstants.PURCHASE_ORDER_PARTNER_RESPONSE), CREDIT_CARD_DECLINED(
                OrderLogConstants.CREDIT_CARD_DECLINED), CREDIT_CARD_RECEIPT(OrderLogConstants.CREDIT_CARD_RECEIPT), CREDIT_CARD_CREDIT(
                OrderLogConstants.CREDIT_CARD_CREDIT), INVOICE_PRE_ENCRYPTION(OrderLogConstants.INVOICE_PRE_ENCRYTPION), ORDER_ACKNOWLEDGEMENT_PRE_ENCRYPTION(
                OrderLogConstants.ORDER_ACKNOWLEDGEMENT_PRE_ENCRYPTION), SHIPMENT_NOTICE_PRE_ENCRYPTION(
                OrderLogConstants.SHIPMENT_NOTICE_PRE_ENCRYPTION), AP_CREDIT(OrderLogConstants.AP_CREDIT), PARTNER_REPORTED_ERRORS(
                OrderLogConstants.PARTNER_REPORTED_ERRORS), ORDER_ACKNOWLEDGEMENT_PRE_ENCRYPTION_RESEND(
                OrderLogConstants.ORDER_ACKNOWLEDGEMENT_PRE_ENCRYPTION_RESEND), ORDER_ACKNOWLEDGEMENT_RESEND(
                OrderLogConstants.ORDER_ACKNOWLEDGEMENT_RESEND), SHIPMENT_NOTICE_PRE_ENCRYPTION_RESEND(
                OrderLogConstants.SHIPMENT_NOTICE_PRE_ENCRYPTION_RESEND), ADVANCED_SHIPMENT_NOTICE_RESEND(
                OrderLogConstants.ADVANCED_SHIPMENT_NOTICE_RESEND), INVOICE_PRE_ENCRYPTION_RESEND(
                OrderLogConstants.INVOICE_PRE_ENCRYPTION_RESEND), CUSTOMER_INVOICED_RESEND(
                OrderLogConstants.CUSTOMER_INVOICED_RESEND);

        private final String label;

        private EventTypeDto(String label) {
            this.label = label;
        }

        public String getLabel() {
            return this.label;
        }
    }

}
