/*
 * 
 */
package com.apd.phoenix.service.model;

import java.io.Serializable;
import java.util.Comparator;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;
import javax.xml.bind.annotation.XmlRootElement;
import com.apd.phoenix.service.business.EntityComparator.GenericComparator;

/**
 * The Class Comment.
 */
@Entity
@XmlRootElement
public class WorkflowLog implements Serializable, com.apd.phoenix.service.model.Entity {

    private static final long serialVersionUID = 4341173508100638061L;

    /** The id. */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id = null;

    /** The version. */
    @Version
    @Column(name = "version", nullable = false)
    private int version = 0;

    /** The comment date. */
    @Column(nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date changeDate;

    @Column(nullable = false)
    private String csrUser;

    private String target;

    @Column(nullable = false)
    private long taskId;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private TaskEvent taskEvent;

    public static enum TaskEvent {
        CLAIM, EXIT, SUSPEND, FORWARD, RELEASE, DELEGATE, RESUME, START, COMPLETE, DUPLICATE, ALREADY_FIXED;
    }

    /**
     * Gets the id.
     *
     * @return the id
     */
    public Long getId() {
        return this.id;
    }

    /**
     * Sets the id.
     *
     * @param id the new id
     */
    public void setId(final Long id) {
        this.id = id;
    }

    /**
     * Gets the version.
     *
     * @return the version
     */
    public int getVersion() {
        return this.version;
    }

    /**
     * Sets the version.
     *
     * @param version the new version
     */
    public void setVersion(final int version) {
        this.version = version;
    }

    public Date getChangeDate() {
        return changeDate;
    }

    public void setChangeDate(Date changeDate) {
        this.changeDate = changeDate;
    }

    public String getCsrUser() {
        return csrUser;
    }

    public void setCsrUser(String csrUser) {
        this.csrUser = csrUser;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public long getTaskId() {
        return taskId;
    }

    public void setTaskId(long taskId) {
        this.taskId = taskId;
    }

    public TaskEvent getTaskEvent() {
        return taskEvent;
    }

    public void setTaskEvent(TaskEvent taskEvent) {
        this.taskEvent = taskEvent;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        if (id != null) {
            return id.equals(((WorkflowLog) that).id);
        }
        return super.equals(that);
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        if (id != null) {
            return id.hashCode();
        }
        return super.hashCode();
    }

    @Override
    public String toString() {
        String result = getClass().getSimpleName() + " ";
        if (this.id != null) {
            result = "User " + this.csrUser + " " + this.taskEvent.toString() + "'d task " + taskId;
        }
        return result;
    }

    public static class WorkflowLogComparator implements Comparator<WorkflowLog> {

        @Override
        public int compare(WorkflowLog log0, WorkflowLog log1) {

            if (log0.getChangeDate() != null && log1.getChangeDate() != null) {
                return log0.getChangeDate().compareTo(log1.getChangeDate());
            }
            return (new GenericComparator()).compare(log0, log1);
        }
    }
}