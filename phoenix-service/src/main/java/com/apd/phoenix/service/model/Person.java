package com.apd.phoenix.service.model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Version;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlTransient;
import org.hibernate.envers.Audited;

@Entity
@XmlAccessorType(XmlAccessType.FIELD)
@Audited
public class Person implements Serializable, com.apd.phoenix.service.model.Entity {

    private static final long serialVersionUID = 8350503723126208151L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id = null;
    @Version
    @Column(name = "version", nullable = false)
    private int version = 0;

    @OneToMany(mappedBy = "person", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
    @XmlTransient
    private Set<PhoneNumber> phoneNumbers = new HashSet<PhoneNumber>();

    @OneToMany(mappedBy = "person", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
    @XmlTransient
    private Set<Address> addresses = new HashSet<Address>();

    @Column
    private String firstName;

    @Column
    private String lastName;

    @Column
    private String email;

    @Column
    private String middleInitial;

    @Column
    private String prefix;

    @Column
    private String suffix;

    public Long getId() {
        return this.id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public int getVersion() {
        return this.version;
    }

    public void setVersion(final int version) {
        this.version = version;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        if (id != null) {
            return id.equals(((Person) that).id);
        }
        return super.equals(that);
    }

    @Override
    public int hashCode() {
        if (id != null) {
            return id.hashCode();
        }
        return super.hashCode();
    }

    public Set<PhoneNumber> getPhoneNumbers() {
        return this.phoneNumbers;
    }

    public void setPhoneNumbers(final Set<PhoneNumber> phoneNumbers) {
        this.phoneNumbers = phoneNumbers;
    }

    public Set<Address> getAddresses() {
        return this.addresses;
    }

    public void setAddresses(final Set<Address> addresses) {
        this.addresses = addresses;
    }

    public String getFirstName() {
        return this.firstName;
    }

    public void setFirstName(final String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return this.lastName;
    }

    public void setLastName(final String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(final String email) {
        this.email = email;
    }

    public String getMiddleInitial() {
        return this.middleInitial;
    }

    public void setMiddleInitial(final String middleInitial) {
        this.middleInitial = middleInitial;
    }

    public String getPrefix() {
        return this.prefix;
    }

    public void setPrefix(final String prefix) {
        this.prefix = prefix;
    }

    public String getSuffix() {
        return this.suffix;
    }

    public void setSuffix(final String suffix) {
        this.suffix = suffix;
    }

    @Override
    public String toString() {
        String result = "";
        if (firstName != null && !firstName.trim().isEmpty()) {
            result += firstName;
        }
        if (lastName != null && !lastName.trim().isEmpty()) {
            if (!result.trim().isEmpty()) {
                result += " ";
            }
            result += lastName;
        }
        return result;
    }

    public String getFormalName() {
        String result = "";
        if (prefix != null && !prefix.trim().isEmpty()) {
            result += prefix;
        }
        if (firstName != null && !firstName.trim().isEmpty()) {
            result += " " + firstName;
        }
        if (middleInitial != null && !middleInitial.trim().isEmpty()) {
            result += " " + middleInitial;
        }
        if (lastName != null && !lastName.trim().isEmpty()) {
            result += " " + lastName;
        }
        if (suffix != null && !suffix.trim().isEmpty()) {
            result += " " + suffix;
        }
        return result;
    }

}