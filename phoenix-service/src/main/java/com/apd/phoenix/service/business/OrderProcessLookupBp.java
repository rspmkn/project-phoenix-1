package com.apd.phoenix.service.business;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import com.apd.phoenix.service.model.CustomerOrder;
import com.apd.phoenix.service.model.OrderProcessLookup;
import com.apd.phoenix.service.persistence.jpa.CustomerOrderDao;
import com.apd.phoenix.service.persistence.jpa.OrderProcessLookupDao;

/**
 * This is used to generate unique process IDs for handling parallel workflow processes.
 */
@Stateless
@LocalBean
public class OrderProcessLookupBp extends AbstractBp<OrderProcessLookup> {

    @Inject
    CustomerOrderDao customerOrderDao;

    @Inject
    public void initDao(OrderProcessLookupDao dao) {
        this.dao = dao;
    }

    public long getUniqueProcessId(CustomerOrder order) {
        OrderProcessLookup process = new OrderProcessLookup();
        process.setCustomerOrder(order);
        process = this.update(process);
        return transformToNotMatchOrderId(process.getId());
    }

    /**
     * Given the ID of a process, returns the order that the process is assigned to.
     * 
     * @param id
     * @return
     */
    public CustomerOrder getOrderFromProcessId(Long id) {
        if (id == null) {
            LOG.error("Process ID is null!");
            return null;
        }
        //first, looks up the order process entity
        OrderProcessLookup process = this.findById(this.undoTransformToNotMatchOrderId(id), OrderProcessLookup.class);
        //if it's not null, returns the order on that entity
        if (process != null) {
            return process.getCustomerOrder();
        }
        //if it is null, then the order ID matches the ID of the process
        //so it looks for an order whose ID is the same as the process
        //this check will not be necessary after PHOEN-4493 is resolved, but should be 
        //left in for legacy orders.
        CustomerOrder order = customerOrderDao.findById(id, CustomerOrder.class);
        if (order == null) {
            //trying to find the orderprocesslookup without inverting the ID, for pre-MySQL migration orders
            process = this.findById(id, OrderProcessLookup.class);
            if (process != null) {
                LOG.debug("Legacy order process lookup performed");
                return process.getCustomerOrder();
            }
            LOG.warn("Could not find order with ID " + id);
        }
        return order;
    }

    /**
     * Takes a primary key for an OrderProcessLookup, and transforms it so that it will
     * not match any CustomerOrder ID, past or future.
     * 
     * THIS IS A HACK WHICH WILL BE UNNECESSARY WHEN PHOEN-4493 IS RESOLVED.
     * 
     * @param id
     * @return
     */
    private Long transformToNotMatchOrderId(Long id) {
        //multiplies the ID by -1, since all customer order IDs will be positive
        return id * -1;
    }

    /**
     * Undoes the operation in trnasformToNotMatchOrderId
     * 
     * THIS IS A HACK WHICH WILL BE UNNECESSARY WHEN PHOEN-4493 IS RESOLVED.
     * 
     * @param id
     * @return
     */
    private Long undoTransformToNotMatchOrderId(Long id) {
        //the inverse of multiplying by negative one is multiplying by negative one
        return id * -1;
    }
}
