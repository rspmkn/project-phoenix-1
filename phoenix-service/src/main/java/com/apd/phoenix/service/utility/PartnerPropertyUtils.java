package com.apd.phoenix.service.utility;

import java.util.List;
import java.util.Properties;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.core.utility.TenantConfigRepository;
import com.apd.phoenix.service.persistence.multitenancy.impl.CurrentTenantIdentifierResolverImpl;

public class PartnerPropertyUtils {

    protected static final Logger LOG = LoggerFactory.getLogger(PartnerPropertyUtils.class);

    public static String getRouteEndpointUri(String partnerId, String baseUriPropertyKey, Properties partnerProperties) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("baseUriPropertyKey: " + baseUriPropertyKey);
            String baseUri = partnerProperties.getProperty(baseUriPropertyKey);
            LOG.debug("baseUri" + (baseUri == null ? "null" : baseUri));
        }
        String partnerRouteUri = "";
        if (StringUtils.isNotBlank(baseUriPropertyKey)) {
            String baseUri = partnerProperties.getProperty(baseUriPropertyKey);
            if (StringUtils.isNotBlank(baseUri)) {
                if (baseUri.startsWith("direct:") || baseUri.startsWith("direct-vm:")) {
                    partnerRouteUri = baseUri
                            + partnerProperties.getProperty("camel.direct.uri.suffix", "_" + partnerId);
                }
                else if (baseUri.startsWith("activemq:")) {
                    partnerRouteUri = baseUri
                            + "?selector="
                            + partnerProperties.getProperty("camel.activemq.uri.message.selector", "partnerId%3D%27"
                                    + partnerId + "%27");
                }
                else {
                    partnerRouteUri = baseUri;
                }
            }
        }
        if (LOG.isDebugEnabled()) {
            LOG.debug("partnerRouteUri: {}", partnerRouteUri);
        }
        return partnerRouteUri;
    }

    public static String getNoPartnerRouteEndpointUri(String baseUriPropertyKey, List<String> outboundPartnerList,
            String commonPropertiesPath) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("baseUriPropertyKey: " + baseUriPropertyKey);
        }
        String noPartnerRouteUri = "";
        //first, sets the base URI 
        String baseUri = null;

        if (StringUtils.isNotBlank(baseUriPropertyKey)) {
            for (String partner : outboundPartnerList) {
                Properties partnerProperties = TenantConfigRepository.getInstance().getProperties(commonPropertiesPath);
                partnerProperties.putAll(TenantConfigRepository.getInstance().getPropertiesByTenantId(
                        CurrentTenantIdentifierResolverImpl.getCurrentTenant(), partner + ".integration"));
                String partnerBaseUri = partnerProperties.getProperty(baseUriPropertyKey);
                if (StringUtils.isNotBlank(baseUri) && !baseUri.equals(partnerBaseUri)) {
                    LOG.warn("Partners have different base URIs");
                    return noPartnerRouteUri;
                }
                baseUri = partnerBaseUri;
            }
            if (StringUtils.isNotBlank(baseUri) && !baseUri.startsWith("activemq:")) {
                LOG.warn("Non-activemq URI specified, no partner route only for queues: " + baseUri);
                return noPartnerRouteUri;
            }
            noPartnerRouteUri = baseUri + "?selector=";
            String selector = "";
            for (String partner : outboundPartnerList) {
                if (StringUtils.isNotBlank(selector)) {
                    selector = selector + "%20AND%20";
                }
                Properties partnerProperties = TenantConfigRepository.getInstance().getProperties(commonPropertiesPath);
                partnerProperties.putAll(TenantConfigRepository.getInstance().getPropertiesByTenantId(
                        CurrentTenantIdentifierResolverImpl.getCurrentTenant(), partner + ".integration"));
                selector = selector
                        + "NOT%20("
                        + partnerProperties.getProperty("camel.activemq.uri.message.selector", "partnerId%3D%27"
                                + partner + "%27") + ")";
            }
            noPartnerRouteUri = noPartnerRouteUri + selector;
        }
        if (LOG.isDebugEnabled()) {
            LOG.debug("partnerRouteUri: {}", noPartnerRouteUri);
        }
        return noPartnerRouteUri;
    }
}
