package com.apd.phoenix.service.persistence.jpa;

import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.Query;
import org.apache.commons.lang.StringUtils;
import com.apd.phoenix.service.model.Vendor;
import com.apd.phoenix.service.model.VendorPropertyType;

/**
 * Vendor DAO stub
 * 
 * @author RHC
 *
 */
@Stateless
@LocalBean
public class VendorDao extends AbstractDao<Vendor> {

    /**
     * Returns all of the available property
     * types for a vendor. This method uses the 
     * <code>getallVendorPropertyTypes</code> named query 
     * on the <code>VendorPropertyType</code> entity
     * @return 
     */
    @SuppressWarnings("unchecked")
    public List<VendorPropertyType> getAllPropertyTypes() {
        return entityManager.createNamedQuery("getallVendorPropertyTypes").getResultList();
    }

    public Vendor getByName(String name) {
        // Confirm search string is not blank
        if (StringUtils.isBlank(name)) {
            return null;
        }
        Query query = entityManager.createQuery("SELECT vendor FROM Vendor AS vendor WHERE vendor.name=:name");
        query.setParameter("name", name);

        return getSingleResultOrNull(query);
    }

    public Vendor findByNameCaseInsensitive(String name) {
        // Confirm search string is not blank
        if (StringUtils.isBlank(name)) {
            return null;
        }
        Query query = entityManager.createQuery("SELECT vendor FROM Vendor AS vendor WHERE UPPER(vendor.name)=:name");
        query.setParameter("name", name.toUpperCase());

        return getSingleResultOrNull(query);
    }
}
