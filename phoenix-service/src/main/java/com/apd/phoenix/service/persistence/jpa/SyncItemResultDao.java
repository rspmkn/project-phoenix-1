package com.apd.phoenix.service.persistence.jpa;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.Query;
import com.apd.phoenix.service.model.SyncItemResult;

@Stateless
@LocalBean
public class SyncItemResultDao extends AbstractDao<SyncItemResult> {

    private static final int BATCH_SIZE = 100;

    public List<SyncItemResult> getResults(String correlationId) {
        String hql = "SELECT s FROM SyncItemResult s WHERE s.correlationId = :correlationId";
        Query query = entityManager.createQuery(hql);
        query.setParameter("correlationId", correlationId);
        List<SyncItemResult> results = (List<SyncItemResult>) query.getResultList();
        if (results != null && !results.isEmpty()) {
            return results;
        }
        else {
            return null;
        }
    }

    public void removeRecords(String correlationId) {
        String sql = "DELETE FROM sir_errors WHERE sir_id  in (SELECT ID FROM SYNCITEMRESULT WHERE CORRELATIONID = :correlationId)";
        Query query = entityManager.createNativeQuery(sql);
        query.setParameter("correlationId", correlationId);
        query.executeUpdate();

        sql = "DELETE FROM sir_warnings WHERE sir_id  in (SELECT ID FROM SYNCITEMRESULT WHERE CORRELATIONID = :correlationId)";
        query = entityManager.createNativeQuery(sql);
        query.setParameter("correlationId", correlationId);
        query.executeUpdate();

        String hql = "DELETE FROM SyncItemResult WHERE correlationId = :correlationId";
        query = entityManager.createQuery(hql);
        query.setParameter("correlationId", correlationId);
        query.executeUpdate();
    }

    @Override
    public SyncItemResult update(SyncItemResult syncItemResult) {
        throw new UnsupportedOperationException("SyncItemResult cannot be updated");
    }

    public List<Long> getResults(String correlationId, int batch) {
        String hql = "SELECT s.id FROM SyncItemResult s WHERE s.correlationId = :correlationId";
        Query query = entityManager.createQuery(hql);
        query.setParameter("correlationId", correlationId);
        query.setFirstResult(batch * BATCH_SIZE);
        query.setMaxResults(BATCH_SIZE);
        return (List<Long>) query.getResultList();
    }

    public List<SyncItemResult> getByIds(List<Long> ids) {
    	if (ids == null || ids.isEmpty()) {
    		return new ArrayList<>();
    	}
        String hql = "SELECT s FROM SyncItemResult s WHERE s.id in :ids";
        Query query = entityManager.createQuery(hql);
        query.setParameter("ids", ids);
        return (List<SyncItemResult>) query.getResultList();
    }

    public Long getCount(String correlationId) {
        String hql = "SELECT count(*) FROM SyncItemResult WHERE correlationId = :correlationId";
        Query query = entityManager.createQuery(hql);
        query.setParameter("correlationId", correlationId);
        List<Long> results = (List<Long>) query.getResultList();
        if (results != null && !results.isEmpty()) {
            return results.get(0);
        }
        else {
            return null;
        }
    }

    public void detach(SyncItemResult syncItemResult) {
        this.entityManager.detach(syncItemResult);
    }

}
