package com.apd.phoenix.service.message.impl;

import java.io.InputStream;
import java.util.Date;
import java.util.Properties;
import java.util.UUID;
import javax.annotation.PostConstruct;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.core.utility.TenantConfigRepository;
import com.apd.phoenix.service.message.api.Message;
import com.apd.phoenix.service.message.api.MessageUtils;
import com.apd.phoenix.service.model.CustomerOrder;
import com.apd.phoenix.service.model.MessageMetadata;
import com.apd.phoenix.service.model.MessageMetadata.CommunicationType;
import com.apd.phoenix.service.model.MessageMetadata.MessageType;
import com.apd.phoenix.service.persistence.jpa.MessageMetadataDao;
import com.apd.phoenix.service.storage.api.StorageObject;
import com.apd.phoenix.service.storage.api.StorageObjectRequest;
import com.apd.phoenix.service.storage.api.StorageService;

@Stateless
@LocalBean
public class AWSMessageUtils implements MessageUtils {

    private static final Logger LOGGER = LoggerFactory.getLogger(AWSMessageUtils.class);
    @Inject
    StorageService storageService;

    Properties awsProperties;

    @Inject
    private MessageMetadataDao messageMetadataDao;

    @PostConstruct
    public void init() {
        awsProperties = TenantConfigRepository.getInstance().getProperties("aws.integration");
    }

    @Override
    public boolean createMessage(Message message) {
        LOGGER.info("Creating message, path: {}", message.getMetadata().getFilePath());
        StorageObject storageObject = new StorageObject();
        storageObject.setPath(message.getMetadata().getFilePath());
        storageObject.setContent(message.getContent());
        storageObject.getAttributes().put("bucketName", awsProperties.getProperty("message.service.bucketName"));
        storageObject.setContentLength(message.getMetadata().getContentLength());
        String contentType = "text/plain";
        if (message.getMetadata().getContentType() != null) {
            contentType = message.getMetadata().getContentType();
        }
        storageObject.setContentType(contentType);
        return storageService.createObject(storageObject);
    }

    @Override
    public Message retrieveMessage(MessageMetadata metadata) {
        if (metadata != null) {
            StorageObjectRequest request = new StorageObjectRequest();
            request.setPath(metadata.getFilePath());
            request.getAttributes().put("bucketName", awsProperties.getProperty("message.service.bucketName"));
            request.setContentUrl(metadata.isContentUrl());
            StorageObject retrievedObject = storageService.retrieveObject(request);
            Message message = new Message();
            message.setMetadata(metadata);
            if (retrievedObject != null) {
                metadata.setPresignedUrl(retrievedObject.getPresignedUrl());
                message.setContent(retrievedObject.getContent());
            }
            return message;
        }
        return null;
    }

    @Override
    public boolean updateMessage(Message message) {
        StorageObject storageObject = new StorageObject();
        storageObject.setPath(message.getMetadata().getFilePath());
        storageObject.setContent(message.getContent());
        storageObject.getAttributes().put("bucketName", awsProperties.getProperty("message.service.bucketName"));
        storageObject.setContentLength(message.getMetadata().getContentLength());
        String contentType = "text/plain";
        if (message.getMetadata().getContentType() != null) {
            contentType = message.getMetadata().getContentType();
        }
        storageObject.setContentType(contentType);
        return storageService.updateObject(storageObject);
    }

    @Override
    public boolean deleteMessage(MessageMetadata metadata) {
        StorageObjectRequest request = new StorageObjectRequest();
        request.setPath(metadata.getFilePath());
        request.getAttributes().put("bucketName", awsProperties.getProperty("message.service.bucketName"));
        return storageService.deleteObject(request);
    }

    @Override
    public Message createMessage(CustomerOrder customerOrder, InputStream content, long contentLength,
            MessageType messageType, String destination, String ccRecipients, String bccRecipients) {
        MessageMetadata metadata = new MessageMetadata();
        metadata.setMessageType(messageType);
        metadata.setMessageDate(new Date());
        if (customerOrder == null || customerOrder.getApdPo().getValue() == null) {
            LOGGER.error("Could not find apd po when creating message");
            metadata.setFilePath("unknownCustomerOrder/" + UUID.randomUUID().toString() + ".txt");
        }
        else {
            metadata.setFilePath(customerOrder.getApdPo().getValue() + "/" + UUID.randomUUID().toString() + ".txt");
        }
        metadata.setContentLength(contentLength);
        metadata.setDestination(destination);
        metadata.setCcRecipients(ccRecipients);
        metadata.setBccRecipients(bccRecipients);
        metadata.setCommunicationType(CommunicationType.OUTBOUND);

        metadata = messageMetadataDao.create(metadata);

        Message message = new Message();
        message.setMetadata(metadata);
        message.setContent(content);

        createMessage(message);
        return message;
    }

    @Override
    public Message createUserModificationMessage(InputStream content, long contentLength, MessageType messageType,
            String destination, String ccRecipients, String bccRecipients) {
        MessageMetadata metadata = new MessageMetadata();
        metadata.setMessageType(messageType);
        metadata.setMessageDate(new Date());

        metadata.setFilePath("userRegistration/" + UUID.randomUUID().toString() + ".txt");
        metadata.setContentLength(contentLength);
        metadata.setDestination(destination);
        metadata.setCcRecipients(ccRecipients);
        metadata.setBccRecipients(bccRecipients);
        metadata.setCommunicationType(CommunicationType.OUTBOUND);

        metadata = messageMetadataDao.create(metadata);

        Message message = new Message();
        message.setMetadata(metadata);
        message.setContent(content);

        createMessage(message);
        return message;
    }
}
