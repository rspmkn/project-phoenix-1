package com.apd.phoenix.service.persistence.jpa;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.Query;
import com.apd.phoenix.service.business.ItemBp;
import com.apd.phoenix.service.model.Catalog;
import com.apd.phoenix.service.model.CatalogXItem;
import com.apd.phoenix.service.model.Item;
import com.apd.phoenix.service.model.ItemClassificationType;
import com.apd.phoenix.service.model.ItemImage;
import com.apd.phoenix.service.model.ItemSellingPoint;
import com.apd.phoenix.service.model.ItemSpecification;
import com.apd.phoenix.service.model.ItemXItemPropertyType;
import com.apd.phoenix.service.model.Matchbook;
import com.apd.phoenix.service.model.Sku;
import com.apd.phoenix.service.model.SkuType;
import com.apd.phoenix.service.model.Vendor;

/**
 * Item DAO stub
 * 
 * @author RHC
 *
 */
@Stateless
@LocalBean
public class ItemDao extends AbstractDao<Item> {

    //Tunable parameter via heap size and catalog size
    private static final int BATCH_SIZE = 100;

    private static final String COST_PROPERTY_TYPE = "APDcost";

    @Inject
    CatalogXItemDao catalogXItemDao;

    @Override
    public Item update(Item item) {
        Item originalItem = findById(item.getId(), Item.class);
        if (Boolean.TRUE.equals(item.getExternalCatalogRelevant())) {
            if (itemChangedInExternalCatalogRelevantWay(item, originalItem)) {
                item.setExternalCatalogChange(Boolean.TRUE);
            }
        }
        if (checkIfItemPropertiesHaveUpdated(item, originalItem)) {
            item.setLastModified(new Date());
        }
        if (checkifItemSKUHaveUpdated(item, originalItem)) {
            item.setLastModified(new Date());
        }
        if (checkifItemCostHaveUpdated(item, originalItem)) {
            item.setLastCostChangeDate(new Date());
        }
        return super.update(item);
    }

    public void updateCatalogXItems(Item item) {
        String hql = "SELECT c FROM CatalogXItem c where c.item.id = :id AND c.catalog.customer.name = 'usps'";
        Query query = entityManager.createQuery(hql);
        query.setParameter("id", item.getId());
        List<CatalogXItem> items = query.getResultList();
        for (Iterator<CatalogXItem> it = items.iterator(); it.hasNext();) {
            CatalogXItem catalogXItem = it.next();
            catalogXItemDao.update(catalogXItem);
        }
    }

    /**
     * Takes a vendor id, and returns all items that are assigned to that vendor's current catalog.
     * @param vendorId 
     * 
     * @param vendorName - the id of the vendor 
     * @return
     */
    @SuppressWarnings("unchecked")
    public List<Item> getCurrentCatalogItems(Long vendorId) {
        String hql = "SELECT item FROM Item AS item WHERE item.vendorCatalog.vendor.id = :vendorId AND "
                + "item.vendorCatalog.replacement IS null";
        //Creates the query
        Query query = entityManager.createQuery(hql);
        query.setParameter("vendorId", vendorId);
        return query.getResultList();
    }

    public ArrayList<Long> batchForJMS(Catalog catalog, int batch) {
        String preHql = "SELECT item.id FROM Item AS item WHERE item.vendorCatalog.id = :id";
        Query preQuery = entityManager.createQuery(preHql);
        preQuery.setParameter("id", catalog.getId());
        setItemsReturned(preQuery, batch * BATCH_SIZE, BATCH_SIZE);
        return new ArrayList<Long>(preQuery.getResultList());
    }

    /**
     * Calls the entityManager refresh method.
     * 
     * @param item
     */
    public void refresh(Item item) {
        this.entityManager.refresh(item);
    }

    public void detach(Item item) {
        this.entityManager.detach(item);
    }

    private static String fullHydrateString() {
        return "SELECT DISTINCT item FROM Item AS item "
                + "LEFT JOIN FETCH item.manufacturer AS itemManufacturer LEFT JOIN FETCH item.skus LEFT JOIN FETCH "
                + "item.itemCategory LEFT JOIN FETCH item.unitOfMeasure LEFT JOIN FETCH item.vendorCatalog "
                + "AS catalog LEFT JOIN FETCH catalog.vendor AS vendor LEFT JOIN FETCH item.itemClassifications "
                + "AS itemClass LEFT JOIN FETCH itemClass.itemClassificationType LEFT JOIN FETCH item.itemImages LEFT JOIN FETCH "
                + "item.replacement AS rep LEFT JOIN FETCH rep.skus LEFT JOIN FETCH item.properties LEFT JOIN FETCH item.hierarchyNode "
                + "LEFT JOIN FETCH item.itemSpecifications AS spec "
                + "LEFT JOIN FETCH item.customerCosts AS cost LEFT JOIN FETCH cost.customer LEFT JOIN FETCH item.matchbook";
    }

    /**
     * Takes a vendor id, and returns all items that are assigned to that vendor's current catalog.
     * @param itemId 
     * 
     * @param vendorName - the id of the vendor 
     * @return
     */
    @SuppressWarnings("unchecked")
    public List<CatalogXItem> getCatalogXItemsForPricing(Long itemId) {
        String hql = "SELECT catalogXItem FROM CatalogXItem AS catalogXItem WHERE catalogXItem.item.id = :itemId";
        //Creates the query
        Query query = entityManager.createQuery(hql);
        query.setParameter("itemId", itemId);
        return query.getResultList();
    }

    @SuppressWarnings("unchecked")
    public List<Item> searchByApdSku(String value, Catalog catalog) {
        return searchBySku(value, "dealer", catalog);
    }

    private boolean itemChangedInExternalCatalogRelevantWay(Item item, Item originalItem) {
        Map<String, String> itemClassificationStringMap = item.getClassificationStringMap();
        Map<String, String> originalClassificationStringMap = originalItem.getClassificationStringMap();
        //fields usps needs for integration
        if (valuesMatch(item.getItemImages(), originalItem.getItemImages())
                && valuesMatch(item.getSku("dealer"), originalItem.getSku("dealer"))
                && valuesMatch(item.getName(), originalItem.getName())
                && valuesMatch(itemClassificationStringMap.get("UNSPC"), originalClassificationStringMap.get("UNSPC"))
                && valuesMatch(item.getPropertyReadOnly("list price"), originalItem.getPropertyReadOnly("list price"))
                && (itemClassificationStringMap.containsKey("WBE") == originalClassificationStringMap
                        .containsKey("WBE"))
                && (itemClassificationStringMap.containsKey("MBE") == originalClassificationStringMap
                        .containsKey("MBE"))
                && valuesMatch(itemClassificationStringMap.get("JWOD"), originalClassificationStringMap.get("JWOD"))
                && valuesMatch(item.getSku("manufacturer"), originalItem.getSku("manufacturer"))
                && valuesMatch(item.getManufacturer().getName(), originalItem.getManufacturer().getName())
                && valuesMatch(item.getDescription(), originalItem.getDescription())) {
            return false;

        }
        else {
            return true;
        }
    }

    private boolean valuesMatch(Object object1, Object object2) {
        if (object1 == null) {
            return object2 == null;
        }
        return object1.equals(object2);
    }

    public List<Item> searchBySku(String value, String type, Catalog catalog) {
        String hql = "SELECT item FROM CatalogXItem as catX INNER JOIN catX.item AS item INNER JOIN item.skus AS sku "
                + "INNER JOIN sku.type AS type WHERE sku.value = :sku AND type.name = :type "
                + "AND catX.catalog.id = :catalogId";
        Query query = entityManager.createQuery(hql);
        query.setParameter("sku", value);
        query.setParameter("type", type);
        query.setParameter("catalogId", catalog.getId());
        return query.getResultList();
    }

    private boolean checkIfItemPropertiesHaveUpdated(Item item, Item originalItem) {
        if (item.getPropertiesReadOnly() == null || originalItem.getPropertiesReadOnly() == null) {
            return false;
        }
        Set<ItemXItemPropertyType> itemProperties = item.getPropertiesReadOnly();
        Set<ItemXItemPropertyType> originalitemPropertieds = originalItem.getPropertiesReadOnly();
        for (ItemXItemPropertyType itemProperty : itemProperties) {
            for (ItemXItemPropertyType orginalItemProperty : originalitemPropertieds) {
                if (itemProperty.getType().getName().equals(orginalItemProperty.getType().getName())
                        && !itemProperty.getValue().equals(orginalItemProperty.getValue())) {
                    return true;
                }
            }
        }
        return false;
    }

    private boolean checkifItemSKUHaveUpdated(Item item, Item originalItem) {
        if (item.getSkus() == null || originalItem.getSkus() == null) {
            return false;
        }
        Set<Sku> skus = item.getSkus();
        Set<Sku> originalSKUs = originalItem.getSkus();
        for (Sku sku : skus) {
            for (Sku orginalSKU : originalSKUs) {
                if (sku.getType().getName().equals(orginalSKU.getType().getName())
                        && !sku.getValue().equals(orginalSKU.getValue())) {
                    return true;
                }
            }
        }
        return false;
    }

    public String getCurrentApdCost(Item item) {
        String hql = "SELECT prop.value FROM Item AS item LEFT JOIN item.properties as prop"
                + " WHERE item.id =:id and prop.type.name = :apdCost ";
        Query query = entityManager.createQuery(hql);
        query.setParameter("id", item.getId());
        query.setParameter("apdCost", ItemBp.APD_COST_PROPERTYTYPE);
        return (String) query.getSingleResult();
    }

    private boolean checkifItemCostHaveUpdated(Item item, Item originalItem) {
        if (item.getSkus() == null || originalItem.getSkus() == null) {
            return false;
        }

        Set<ItemXItemPropertyType> itemProperties = item.getPropertiesReadOnly();
        Set<ItemXItemPropertyType> originalitemPropertieds = originalItem.getPropertiesReadOnly();
        for (ItemXItemPropertyType itemProperty : itemProperties) {
            for (ItemXItemPropertyType orginalItemProperty : originalitemPropertieds) {
                if (itemProperty.getType().getName().equals(COST_PROPERTY_TYPE)
                        && itemProperty.getType().getName().equals(orginalItemProperty.getType().getName())
                        && !itemProperty.getValue().equals(orginalItemProperty.getValue())) {
                    return true;
                }
            }
        }
        return false;
    }

    @SuppressWarnings("unchecked")
    public List<Item> findItemsByHierarchyNode(Long hierarchyNode) {
        String hql = "SELECT item FROM Item AS item WHERE item.hierarchyNode.id = :hierarchyNode ";
        Query query = entityManager.createQuery(hql);
        query.setParameter("hierarchyNode", hierarchyNode);
        return query.getResultList();
    }

    public Item hydrateItem(Item item) {
        item = this.findById(item.getId(), Item.class);
        for (ItemClassificationType classification : item.getClassifications().keySet()) {
            if (item.getClassifications().get(classification) != null) {
                item.getClassifications().get(classification).toString();
            }
        }
        for (ItemSpecification specification : item.getSpecifications()) {
            specification.toString();
        }
        for (ItemImage image : item.getItemImages()) {
            image.toString();
        }
        for (ItemSellingPoint sellingPoint : item.getSellingPoints()) {
            sellingPoint.toString();
        }

        for (Matchbook matchbook : item.getMatchbook()) {
            matchbook.toString();
        }
        return item;
    }

    public Item searchItem(String sku, String skuType, String vendorName) {
        Item search = new Item();
        if (skuType.equals(ItemBp.APD_SKUTYPE)) {
            search.setDealerSku(sku);
        }
        else if (skuType.equals(ItemBp.VENDOR_SKUTYPE)) {
            search.setVendorSku(sku);
        }
        else {
            search.setSkus(new HashSet<Sku>());
            Sku searchSku = new Sku();
            searchSku.setType(new SkuType());
            searchSku.getType().setName(skuType);
            searchSku.setValue(sku);
            search.getSkus().add(searchSku);
        }
        search.setVendorCatalog(new Catalog());
        search.getVendorCatalog().setVendor(new Vendor());
        search.getVendorCatalog().getVendor().setName(vendorName);
        return search;
    }

    @SuppressWarnings("deprecation")
    @Override
    public Item eagerLoad(Item toLoad) {
        if (toLoad == null || toLoad.getId() == null) {
            return toLoad;
        }
        Item toReturn = this.findById(toLoad.getId(), Item.class);
        if (toReturn.getSkus() != null) {
            toReturn.getSkus().size();
        }
        if (toReturn.getCustomerCosts() != null) {
            toReturn.getCustomerCosts().size();
        }
        if (toReturn.getItemSpecifications() != null) {
            toReturn.getItemSpecifications().size();
        }
        if (toReturn.getItemImages() != null) {
            toReturn.getItemImages().size();
        }
        if (toReturn.getReplacement() != null) {
            toReturn.getReplacement().toString();
        }
        if (toReturn.getUnitOfMeasure() != null) {
            toReturn.getUnitOfMeasure().toString();
        }
        if (toReturn.getVendorCatalog() != null) {
            toReturn.getVendorCatalog().toString();
        }
        if (toReturn.getHierarchyNode() != null) {
            toReturn.getHierarchyNode().toString();
        }
        if (toReturn.getSimilarItems() != null) {
            toReturn.getSimilarItems().size();
        }
        if (toReturn.getProperties() != null) {
            toReturn.getProperties().size();
        }
        if (toReturn.getSellingPoints() != null) {
            toReturn.getSellingPoints().size();
        }
        if (toReturn.getAbilityOneSubstitute() != null) {
            toReturn.getAbilityOneSubstitute().toString();
        }
        if (toReturn.getMatchbook() != null) {
            toReturn.getMatchbook().size();
        }
        if (toReturn.getItemCategory() != null) {
            toReturn.getItemCategory().toString();
        }
        if (toReturn.getManufacturer() != null) {
            toReturn.getManufacturer().toString();
        }
        if (toReturn.getBrand() != null) {
            toReturn.getBrand().toString();
        }
        if (toReturn.getItemClassifications() != null) {
            toReturn.getItemClassifications().size();
        }
        return toReturn;
    }
}
