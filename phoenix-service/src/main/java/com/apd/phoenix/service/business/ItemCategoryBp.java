package com.apd.phoenix.service.business;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import com.apd.phoenix.service.model.ItemCategory;
import com.apd.phoenix.service.persistence.jpa.ItemCategoryDao;

/**
 * This class provides business process methods for ItemCategory.
 * 
 * @author RHC
 * 
 */
@Stateless
@LocalBean
public class ItemCategoryBp extends AbstractBp<ItemCategory> {

    /**
     * This method is injected with the correct Dao for initialization
     * 
     * @param dao
     */
    @Inject
    public void initDao(ItemCategoryDao dao) {
        this.dao = dao;
    }

    public ItemCategory getCategoryByName(String name) {
        return ((ItemCategoryDao) dao).getCategoryByName(name);
    }
}
