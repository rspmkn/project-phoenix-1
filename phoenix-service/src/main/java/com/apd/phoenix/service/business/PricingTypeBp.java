package com.apd.phoenix.service.business;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import org.apache.commons.lang.StringUtils;
import com.apd.phoenix.service.model.Catalog;
import com.apd.phoenix.service.model.CatalogXCategoryXPricingType;
import com.apd.phoenix.service.model.Item;
import com.apd.phoenix.service.model.ItemXItemPropertyType;
import com.apd.phoenix.service.model.PricingType;
import com.apd.phoenix.service.persistence.jpa.CatalogXCategoryXPricingTypeDao;
import com.apd.phoenix.service.persistence.jpa.PricingTypeDao;

/**
 * This class provides business process methods for PricingType.
 * 
 * @author RHC
 * 
 */
@Stateless
@LocalBean
public class PricingTypeBp extends AbstractBp<PricingType> {

    public static final String STREET_LESS_SHIPPING = "Street Less, Shipping Included";
    public static final String STREET_LESS = "Street Less";
    public static final String STREET_LESS_MARKDOWN_SHIPPING = "Street Less Markdown, Shipping Included";
    public static final String STREET_LESS_MARKDOWN = "Street Less Markdown";
    public static final String AF_LESS_SHIPPING = "AF Less, Shipping Included";
    public static final String AF_LESS = "AF Less";
    public static final String GSA_LESS_SHIPPING = "GSA Less, Shipping Included";
    public static final String GSA_LESS = "GSA Less";
    public static final String LIST_LESS_SHIPPING = "List Less Discount %, Floor at Margin %, Shipping Included";
    public static final String LIST_LESS = "List Less Discount %, Floor at Margin %";
    public static final String LIST_LESS_MARKDOWN_SHIPPING = "List Less Markdown %, Floor at Margin %, Shipping Included";
    public static final String LIST_LESS_MARKDOWN = "List Less Markdown %, Floor at Margin %";
    public static final String COST_PLUS_SHIPPING = "Cost Plus Margin %, Shipping Included";
    public static final String COST_PLUS = "Cost Plus Margin %";
    public static final String COST_PLUS_MARKUP_SHIPPING = "Cost Plus Markup %, Shipping Included";
    public static final String COST_PLUS_MARKUP = "Cost Plus Markup %";
    public static final String FIXED_PRICE = "Fixed Price";
    private static final String PARAM_DELIMETER = ",";
    private static final String STREET_PROPERTY_TYPE = "street";
    private static final String AFPRICE_PROPERTY_TYPE = "AFprice";
    private static final String GSAPRICE_PROPERTY_TYPE = "GSAprice";
    private static final String UPSABLE_PROPERTY_TYPE = "UPSable";
    private static final String LOWEST_CEILING = "0.99";
    private static final String LOWEST_FLOOR = LOWEST_CEILING;
    private static final String LIST_PROPERTY_TYPE = "list price";
    private static final String COST_PROPERTY_TYPE = "APDcost";
    private static final String BOOLEAN_FALSE = "N";
    private static final String BOOLEAN_TRUE = "Y";

    @Inject
    private CatalogXCategoryXPricingTypeDao catalogPricingTypeDao;
    @Inject
    private CustomerCostBp customerCostBp;

    /**
     * This method is injected with the correct Dao for initialization
     * 
     * @param dao
     */
    @Inject
    public void initDao(PricingTypeDao dao) {
        this.dao = dao;
    }

    /**
     * Takes an Item, a Catalog, and an override PricingType and Parameter, and returns the price for the Item.
     * 
     * @param item
     * @param catalog
     * @param overrideType
     * @param overrideParameter
     * @return
     * @throws PricingCalculationException
     */
    public PricingCalculationResult calculatePrice(Item item, Catalog catalog, Long customerId,
            PricingType overrideType, String overrideParameter) throws PricingCalculationException {
        //Sets the type and parameter to the values assigned to the catalogXItem
        PricingType pricingType = overrideType;
        String pricingParameter = overrideParameter;
        //If none assigned finds the new values based on the catalog and the item category
        if (pricingParameter == null || pricingType == null) {
            CatalogXCategoryXPricingType pricing = null;
            //this nullcheck should never be triggered, items always have a category
            if (item.getItemCategory() != null) {
                pricing = catalogPricingTypeDao.getPricing(catalog.getId(), item.getItemCategory().getId());
            }
            if (pricing != null) {
                pricingParameter = pricing.getParameter();
                pricingType = pricing.getType();
            }
            else {
                //None specified, and none found
                throw new PricingCalculationException();
            }
        }
        //with the correct type and parameter, calculates the price
        PricingCalculationResult toReturn = new PricingCalculationResult();
        toReturn.cost = getDecimalProperty(item.getPropertyReadOnly(COST_PROPERTY_TYPE));
        BigDecimal customerCost = customerCostBp.getItemCost(item.getId(), customerId);
        if (customerCost != null) {
            toReturn.cost = customerCost;
        }
        toReturn.price = getPrice(item, catalog, toReturn.cost, pricingType, pricingParameter).setScale(2,
                RoundingMode.HALF_UP);
        return toReturn;
    }

    /**
     * Takes the correct pricing type and parameter, as well as the item and the catalog, and calculates the price
     * 
     * @param item
     * @param catalog
     * @param type
     * @param pricingParameter
     * @return
     * @throws PricingCalculationException
     */
    private BigDecimal getPrice(Item item, Catalog catalog, BigDecimal cost, PricingType type, String pricingParameter)
            throws PricingCalculationException {
        if (pricingParameter == null) {
            throw new PricingCalculationException();
        }
        if (!pricingParameter.matches(type.getParameterRegEx())) {
            throw new PricingCalculationException();
        }
        boolean useCeiling = type.getUseCeiling();
        BigDecimal floor = cost.max(new BigDecimal(LOWEST_FLOOR));
        BigDecimal ceiling = getDecimalProperty(item.getPropertyReadOnly(LIST_PROPERTY_TYPE)).max(
                new BigDecimal(LOWEST_CEILING));
        if (!useCeiling) {
            ceiling = null;
        }
        BigDecimal floorPercent;
        BigDecimal margin;
        BigDecimal discount;
        BigDecimal markdown;
        BigDecimal markup;
        BigDecimal list;
        BigDecimal shipping;
        String[] params;
        switch (type.getName()) {
            case FIXED_PRICE:
                //dollar
                if (pricingParameter.startsWith("$")) {
                    pricingParameter = pricingParameter.substring(1);
                }
                return new BigDecimal(pricingParameter);
            case COST_PLUS:
                //cost / (1 - margin)
                margin = percentFromString(pricingParameter);
                return boundUnitPrice(cost.divide(BigDecimal.ONE.subtract(margin), 10, RoundingMode.HALF_UP), floor,
                        ceiling);
            case COST_PLUS_SHIPPING:
                //(cost + shipping) / (1 - margin)
                margin = percentFromString(pricingParameter);
                shipping = shipRate(cost, catalog, getBooleanProperty(item.getPropertyReadOnly(UPSABLE_PROPERTY_TYPE)));
                return boundUnitPrice((cost.add(shipping)).divide(BigDecimal.ONE.subtract(margin), 10,
                        RoundingMode.HALF_UP), floor, ceiling, shipping);
            case COST_PLUS_MARKUP:
                //cost * (1 + markup)
                markup = percentFromString(pricingParameter);
                return boundUnitPrice(cost.multiply(BigDecimal.ONE.add(markup)), floor, ceiling);
            case COST_PLUS_MARKUP_SHIPPING:
                //(cost + shipping) * (1 + margin)
                markup = percentFromString(pricingParameter);
                shipping = shipRate(cost, catalog, getBooleanProperty(item.getPropertyReadOnly(UPSABLE_PROPERTY_TYPE)));
                return boundUnitPrice((cost.add(shipping)).multiply(BigDecimal.ONE.subtract(markup)), floor, ceiling,
                        shipping);
            case LIST_LESS:
                //list / (1 + discount), with floor: cost / (1 - floor)
                params = pricingParameter.split(PARAM_DELIMETER);
                list = getDecimalProperty(item.getPropertyReadOnly(LIST_PROPERTY_TYPE));
                discount = percentFromString(params[0].trim());
                floorPercent = percentFromString(params[1].trim());
                //overrides floor, because it's calculated slightly differently for this pricing type
                floor = cost.divide(BigDecimal.ONE.subtract(floorPercent), 10, RoundingMode.HALF_UP);
                return boundUnitPrice(list.divide(BigDecimal.ONE.add(discount), 10, RoundingMode.HALF_UP), floor,
                        ceiling);
            case LIST_LESS_SHIPPING:
                //list / (1 + discount) + shipping, with floor: (cost + shipping) / (1 - floor)
                params = pricingParameter.split(PARAM_DELIMETER);
                list = getDecimalProperty(item.getPropertyReadOnly(LIST_PROPERTY_TYPE));
                discount = percentFromString(params[0].trim());
                floorPercent = percentFromString(params[1].trim());
                shipping = shipRate(cost, catalog, getBooleanProperty(item.getPropertyReadOnly(UPSABLE_PROPERTY_TYPE)));
                //overrides ceiling and floor, because they're calculated slightly differently for this pricing type
                if (useCeiling) {
                    ceiling = list.add(shipping);
                }
                floor = (cost.add(shipping)).divide(BigDecimal.ONE.subtract(floorPercent), 10, RoundingMode.HALF_UP);
                //doesn't pass in shipping, because it's included in the floor and ceiling already
                return boundUnitPrice(
                        list.divide(BigDecimal.ONE.add(discount), 10, RoundingMode.HALF_UP).add(shipping), floor,
                        ceiling);
            case LIST_LESS_MARKDOWN:
                //list * (1 - markdown), with floor: cost / (1 - floor)
                params = pricingParameter.split(PARAM_DELIMETER);
                list = getDecimalProperty(item.getPropertyReadOnly(LIST_PROPERTY_TYPE));
                markdown = percentFromString(params[0].trim());
                floorPercent = percentFromString(params[1].trim());
                //overrides floor, because it's calculated slightly differently for this pricing type
                floor = cost.divide(BigDecimal.ONE.subtract(floorPercent), 10, RoundingMode.HALF_UP);
                return boundUnitPrice(list.multiply(BigDecimal.ONE.subtract(markdown)), floor, ceiling);
            case LIST_LESS_MARKDOWN_SHIPPING:
                //list * (1 - markdown) + shipping, with floor: (cost + shipping) / (1 - floor)
                params = pricingParameter.split(PARAM_DELIMETER);
                list = getDecimalProperty(item.getPropertyReadOnly(LIST_PROPERTY_TYPE));
                markdown = percentFromString(params[0].trim());
                floorPercent = percentFromString(params[1].trim());
                shipping = shipRate(cost, catalog, getBooleanProperty(item.getPropertyReadOnly(UPSABLE_PROPERTY_TYPE)));
                //overrides ceiling and floor, because they're calculated slightly differently for this pricing type
                if (useCeiling) {
                    ceiling = list.add(shipping);
                }
                floor = (cost.add(shipping)).divide(BigDecimal.ONE.subtract(floorPercent), 10, RoundingMode.HALF_UP);
                //doesn't pass in shipping, because it's included in the floor and ceiling already
                return boundUnitPrice(list.multiply(BigDecimal.ONE.subtract(markdown)).add(shipping), floor, ceiling);
            case GSA_LESS:
                //gsa / (1 + discount)
                discount = percentFromString(pricingParameter);
                list = getDecimalProperty(item.getPropertyReadOnly(GSAPRICE_PROPERTY_TYPE));
                return list.divide(BigDecimal.ONE.add(discount), 10, RoundingMode.HALF_UP);
            case GSA_LESS_SHIPPING:
                //gsa / (1 + discount) + shipping
                discount = percentFromString(pricingParameter);
                list = getDecimalProperty(item.getPropertyReadOnly(GSAPRICE_PROPERTY_TYPE));
                shipping = shipRate(cost, catalog, getBooleanProperty(item.getPropertyReadOnly(UPSABLE_PROPERTY_TYPE)));
                return list.divide(BigDecimal.ONE.add(discount), 10, RoundingMode.HALF_UP).add(shipping);
            case AF_LESS:
                //af / (1 + discount)
                discount = percentFromString(pricingParameter);
                list = getDecimalProperty(item.getPropertyReadOnly(AFPRICE_PROPERTY_TYPE));
                return list.divide(BigDecimal.ONE.add(discount), 10, RoundingMode.HALF_UP);
            case AF_LESS_SHIPPING:
                //af / (1 + discount) + shipping
                discount = percentFromString(pricingParameter);
                list = getDecimalProperty(item.getPropertyReadOnly(AFPRICE_PROPERTY_TYPE));
                shipping = shipRate(cost, catalog, getBooleanProperty(item.getPropertyReadOnly(UPSABLE_PROPERTY_TYPE)));
                return list.divide(BigDecimal.ONE.add(discount), 10, RoundingMode.HALF_UP).add(shipping);
            case STREET_LESS:
                //street / (1 + discount)
                discount = percentFromString(pricingParameter);
                list = getDecimalProperty(item.getPropertyReadOnly(STREET_PROPERTY_TYPE));
                return boundUnitPrice(list.divide(BigDecimal.ONE.add(discount), 10, RoundingMode.HALF_UP), floor,
                        ceiling);
            case STREET_LESS_SHIPPING:
                //street / (1 + discount) + shipping
                discount = percentFromString(pricingParameter);
                list = getDecimalProperty(item.getPropertyReadOnly(STREET_PROPERTY_TYPE));
                shipping = shipRate(cost, catalog, getBooleanProperty(item.getPropertyReadOnly(UPSABLE_PROPERTY_TYPE)));
                return boundUnitPrice(
                        list.divide(BigDecimal.ONE.add(discount), 10, RoundingMode.HALF_UP).add(shipping), floor,
                        ceiling, shipping);
            case STREET_LESS_MARKDOWN:
                //street * (1 - markdown)
                markdown = percentFromString(pricingParameter);
                list = getDecimalProperty(item.getPropertyReadOnly(STREET_PROPERTY_TYPE));
                return boundUnitPrice(list.multiply(BigDecimal.ONE.subtract(markdown)), floor, ceiling);
            case STREET_LESS_MARKDOWN_SHIPPING:
                //street * (1 - markdown) + shipping
                markdown = percentFromString(pricingParameter);
                list = getDecimalProperty(item.getPropertyReadOnly(STREET_PROPERTY_TYPE));
                shipping = shipRate(cost, catalog, getBooleanProperty(item.getPropertyReadOnly(UPSABLE_PROPERTY_TYPE)));
                return boundUnitPrice(list.multiply(BigDecimal.ONE.subtract(markdown)).add(shipping), floor, ceiling,
                        shipping);
        }
        throw new PricingCalculationException();
    }

    private BigDecimal shipRate(BigDecimal cost, Catalog catalog, boolean upsAble) {
        BigDecimal rate = (upsAble ? catalog.getUpsableShippingRate() : catalog.getNonUpsableShippingRate());
        BigDecimal min = (upsAble ? catalog.getUpsableShippingMinimum() : catalog.getNonUpsableShippingMinimum());
        BigDecimal max = (upsAble ? catalog.getUpsableShippingMaximum() : catalog.getNonUpsableShippingMaximum());
        Boolean isPercent = (upsAble ? catalog.getUpsableShippingRatePercent() : catalog
                .getNonUpsableShippingRatePercent());
        if (rate == null) {
            rate = BigDecimal.ZERO;
        }
        if (isPercent == null) {
            isPercent = false;
        }
        BigDecimal calculatedRate = rate;
        if (isPercent) {
            calculatedRate = rate.multiply(cost).multiply(new BigDecimal("0.01"));
        }
        if (min != null && calculatedRate.compareTo(min) < 0) {
            calculatedRate = min;
        }
        if (max != null && calculatedRate.compareTo(max) > 0) {
            calculatedRate = max;
        }
        return calculatedRate;
    }

    private boolean getBooleanProperty(ItemXItemPropertyType property) throws PricingCalculationException {
        if (property == null || StringUtils.isBlank(property.getValue()) || property.getValue().equals(BOOLEAN_FALSE)) {
            return false;
        }
        else if (property.getValue().equals(BOOLEAN_TRUE)) {
            return true;
        }
        throw new PricingCalculationException();
    }

    private BigDecimal getDecimalProperty(ItemXItemPropertyType property) throws PricingCalculationException {
        if (property != null && StringUtils.isNotBlank(property.getValue())) {
            try {
                return new BigDecimal(property.getValue());
            }
            catch (NumberFormatException e) {
                throw new PricingCalculationException();
            }
        }
        throw new PricingCalculationException();
    }

    private BigDecimal percentFromString(String number) {
        boolean isPercent = false;
        if (number.endsWith("%")) {
            isPercent = true;
            number = number.substring(0, number.length() - 1);
        }
        BigDecimal toReturn = new BigDecimal(number);
        if (isPercent) {
            toReturn = toReturn.multiply(new BigDecimal("0.01"));
        }
        return toReturn;
    }

    private BigDecimal boundUnitPrice(BigDecimal unitPrice, BigDecimal floor, BigDecimal ceiling, BigDecimal shipping) {
        if (ceiling == null) {
            //only occurs if pricing type has no ceiling
            return floor.add(shipping).max(unitPrice);
        }
        else {
            return ceiling.add(shipping).min(floor.add(shipping).max(unitPrice));
        }
    }

    private BigDecimal boundUnitPrice(BigDecimal unitPrice, BigDecimal floor, BigDecimal ceiling) {
        return boundUnitPrice(unitPrice, floor, ceiling, BigDecimal.ZERO);
    }

    public class PricingCalculationException extends Exception {

        private static final long serialVersionUID = 1L;

        @Override
        public String getMessage() {
            return "Unable to calculate the price for this item.";
        }
    }

    public class PricingCalculationResult {

        private BigDecimal price;
        private BigDecimal cost;

        public BigDecimal getPrice() {
            return this.price;
        }

        public BigDecimal getCost() {
            return this.cost;
        }

        public BigDecimal getProfit() {
            if (this.price != null && this.cost != null) {
                return this.price.subtract(this.cost);
            }
            return BigDecimal.ZERO;
        }
    }

    public PricingType getTypeByName(String name) {
        if (StringUtils.isBlank(name)) {
            return null;
        }
        boolean useCeiling = true;
        if (name.endsWith(", No Ceiling")) {
            name = name.substring(0, name.indexOf(", No Ceiling"));
            useCeiling = false;
        }
        PricingType searchType = new PricingType();
        searchType.setName(name);
        List<PricingType> searchList = this.searchByExactExample(searchType, 0, 0);
        if (searchList.isEmpty()) {
            return null;
        }
        for (PricingType type : searchList) {
            if (type.getUseCeiling() == useCeiling) {
                return type;
            }
        }
        return searchList.get(0);
    }
}
