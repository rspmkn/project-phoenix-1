package com.apd.phoenix.service.business;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import com.apd.phoenix.service.model.SyncItemResultSummary;
import com.apd.phoenix.service.persistence.jpa.SyncItemResultSummaryDao;

/**
 * This class provides business process methods for Addresses.
 * 
 * @author RHC
 *
 */
@Stateless
@LocalBean
public class SyncItemResultSummaryBp extends AbstractBp<SyncItemResultSummary> {

    /**
     * This method is injected with the correct Dao for initialization
     * 
     * @param dao
     */
    @Inject
    public void initDao(SyncItemResultSummaryDao dao) {
        this.dao = dao;
    }

    public SyncItemResultSummary getSummary(String correlationId) {
        return ((SyncItemResultSummaryDao) dao).getAggregatedSummary(correlationId);
    }

    public Long getCount(String correlationId) {
        return ((SyncItemResultSummaryDao) dao).getCount(correlationId);
    }

    public void removeRecords(String correlationId) {
        ((SyncItemResultSummaryDao) dao).removeRecords(correlationId);
    }
}
