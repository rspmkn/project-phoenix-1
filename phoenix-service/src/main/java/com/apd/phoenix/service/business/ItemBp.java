package com.apd.phoenix.service.business;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.ws.rs.core.MultivaluedMap;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import com.apd.phoenix.service.business.CatalogBp.SyncAction;
import com.apd.phoenix.service.business.CatalogBp.SyncError;
import com.apd.phoenix.service.business.PricingTypeBp.PricingCalculationException;
import com.apd.phoenix.service.business.PricingTypeBp.PricingCalculationResult;
import com.apd.phoenix.service.model.Account;
import com.apd.phoenix.service.model.Brand;
import com.apd.phoenix.service.model.Catalog;
import com.apd.phoenix.service.model.CatalogXItem;
import com.apd.phoenix.service.model.CatalogXItemXItemPropertyType;
import com.apd.phoenix.service.model.CustomerCost;
import com.apd.phoenix.service.model.HierarchyNode;
import com.apd.phoenix.service.model.Item;
import com.apd.phoenix.service.model.Item.ItemStatus;
import com.apd.phoenix.service.model.ItemCategory;
import com.apd.phoenix.service.model.ItemClassificationType;
import com.apd.phoenix.service.model.ItemImage;
import com.apd.phoenix.service.model.ItemPropertyType;
import com.apd.phoenix.service.model.ItemRelationship;
import com.apd.phoenix.service.model.ItemSellingPoint;
import com.apd.phoenix.service.model.ItemSpecification;
import com.apd.phoenix.service.model.ItemXItemPropertyType;
import com.apd.phoenix.service.model.Manufacturer;
import com.apd.phoenix.service.model.Matchbook;
import com.apd.phoenix.service.model.Sku;
import com.apd.phoenix.service.model.SkuType;
import com.apd.phoenix.service.model.SyncItemResult;
import com.apd.phoenix.service.model.SyncItemResultSummary;
import com.apd.phoenix.service.model.UnitOfMeasure;
import com.apd.phoenix.service.model.dto.ItemDto;
import com.apd.phoenix.service.model.dto.ItemRelationshipDto;
import com.apd.phoenix.service.model.dto.SkuDto;
import com.apd.phoenix.service.persistence.jpa.CatalogDao;
import com.apd.phoenix.service.persistence.jpa.CatalogXItemDao;
import com.apd.phoenix.service.persistence.jpa.HierarchyNodeDao;
import com.apd.phoenix.service.persistence.jpa.ItemDao;
import com.apd.phoenix.service.persistence.jpa.SkuDao;
import com.apd.phoenix.service.persistence.jpa.UnitOfMeasureDao;
import com.apd.phoenix.service.product.ItemCacheManager;

@Stateless
@LocalBean
public class ItemBp extends AbstractBp<Item> {

    private static final String ITEM_WEIGHT_SPECIFICATION_NAME = "Item Weight";
    private static final String LAST_APD_COST = "Last APD Cost";
    public static final String NAME = "name";
    public static final String DESCRIPTION = "description";
    public static final String CATEGORY = "category";
    public static final String CLASSIFICATIONS = "classifications";
    public static final String HIERARCHY_HEADER = "hierarchy";
    public static final String MANUFACTURER = "manufacturer";
    public static final String SELLING_POINT = "selling points";
    public static final String MATCHBOOK = "matchbook";
    public static final String OVERWRITE_PROTECTED_SEARCH_TERMS = "overwrite protected search terms";
    public static final String PROTECTED_SEARCH_TERMS = "protected search terms";
    public static final String SEARCH_TERMS = "search terms";
    public static final String SKU_SUFFIX = " sku";
    public static final String SPECIFICATIONS = "specifications";
    public static final String UNIT_OF_MEASURE = "unit of measure";
    public static final String VENDOR_HEADER = "vendor";
    public static final String UNSPSC_CLASSIFICATIONTYPE = "unspsc";

    public static final String APD_SKUTYPE = "dealer";
    public static final String VENDOR_SKUTYPE = "vendor";
    public static final String CUSTOMER_SKUTYPE = "customer";
    public static final String APD_COST_PROPERTYTYPE = "APDcost";
    public static final String STREET_PRICE_PROPERTYTYPE = "street";
    public static final String LIST_PRICE_PROPERTYTYPE = "list price";
    public static final String CUSTOMER_PRICE_PROPERTYTYPE = "unitPrice";
    public static final String LAST_PRICE_PROPERTYTYPE = "last calculated price";
    public static final String LAST_PROFIT_PROPERTYTYPE = "last calculated profit";
    public static final String CURRENT_PROFIT_PROPERTYTYPE = "current profit";
    public static final String PRICE_CHANGE_DATE_PROPERTYTYPE = "last price change date";
    public static final String CUSTOMER_REPLACEMENT_VENDOR_PROPERTYTYPE = "customerReplacementVendor";
    public static final String CUSTOMER_REPLACEMENT_SKU_PROPERTYTYPE = "customerReplacementSku";
    public static final String PREVENT_REPLACEMENT_PROPERTYTYPE = "prevent replacement";
    public static final String SPECIAL_ORDER_PROPERTYTYPE = "specialOrder";
    public static final String CUSTOM_ORDER_PROPERTYPYPE = "customOrder";
    public static final String FIXED_PRICE_PRICINGTYPE = "Fixed Price";
    public static final String SMART_SEARCH_DEALER_DESCRIPTION = "smart search dealer description";

    public static final String IN_ROW_DELIMETER_SPLITTER = "\\|";
    public static final String IN_ROW_DELIMETER = "|";
    public static final String SUB_DELIMITER = "=";
    public static final String SUB_DELIMITER_HYPHEN = "-";
    public static final String DELIMETER = ",";

    private static final String YES = "Y";
    private static final String BLANK = "";
    private static final String NULL = "null";
    private static final String TRUE = "true";
    private static final String FALSE = "false";
    private static final String ZERO_DOLLARS_AND_CENTS = "0.00";

    private static final String REPLACEMENT_VENDOR = "replacementVendor";
    private static final String REPLACEMENT_SKU = "replacementSku";
    public static final String APD_SKU_HEADER = "dealer sku";
    private static final String ETS_SKU = "ets_sku";
    private static final String MINIMUM = "minimum";
    private static final String MULTIPLES = "multiples";
    public static final String CATALOG = "catalog";
    private static final String IMAGE_URLS = "image URLs";
    private static final String DISCONTINUED_TYPE = "discontinued";
    private static final String HIERARCHY_SUFFIX = " hierarchy";
    private static final String COST_SUFFIX = " cost";
    public static final String ITEM_WEIGHT = "item weight";
    private static final String DISCONTINUED_DATE_TYPE = "discontinuedDate";
    public static final String BRAND_ID = "brandId";
    public static final String BRAND_NAME = "brandName";
    private static final String FEATURED_PRODUCT = "featuredProduct";
    private static final String USE_WITH = "useWith";
    private static final String UPSELL_ITEMS = "upsellItems";
    private static final String DOWNSELL_UPSELL = "Downsell/Upsell";
    private static final String COMPANION_ITEMS = "companionItems";
    private static final String COMPANION_FOR_COMPANION = "Companion For/Companion";
    private static final String ACCESSORY_ITEMS = "accessoryItems";
    private static final String ACCESSORY_FOR_ACCESSORIES = "Accessory For/Accessories";
    private static final String HIERARCHY_TYPE = "type";
    public static final String CARTRIDGE_NUMBER = "cartridgeNumber";
    private static final String PROTECTED_COMMODITY_CODE = "protectedCommodityCode";

    public static final String[] KNOWN_COLUMNS = { VENDOR_HEADER, NAME, USE_WITH, DESCRIPTION,
            SMART_SEARCH_DEALER_DESCRIPTION, FEATURED_PRODUCT, CATEGORY, CATALOG, MANUFACTURER, BRAND_ID, BRAND_NAME,
            REPLACEMENT_VENDOR, REPLACEMENT_SKU, ETS_SKU, OVERWRITE_PROTECTED_SEARCH_TERMS, PROTECTED_SEARCH_TERMS,
            SEARCH_TERMS, SPECIFICATIONS, UPSELL_ITEMS, COMPANION_ITEMS, ACCESSORY_ITEMS, CLASSIFICATIONS,
            UNIT_OF_MEASURE, HIERARCHY_HEADER, HIERARCHY_TYPE, DISCONTINUED_TYPE, IMAGE_URLS, MATCHBOOK, SELLING_POINT,
            MULTIPLES, MINIMUM, ITEM_WEIGHT, CARTRIDGE_NUMBER, PROTECTED_COMMODITY_CODE };

    private SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");

    public Item searchById(Long id) {
        return (Item) ((ItemDao) dao).findById(id, Item.class);
    }

    public enum ClassificationTypeEnum {
        ASSEMBLY_CODE("Assembly_Code"), EPACPGCOMPLIANT_CODE("EPACPGCompliant_Code"), GREEN_INDICATOR("Green_Indicator"), GREEN_INFORMATION(
                "Green_Information"), MSDS_Indicator("MSDS_Indicator"), NON_RETURNABLE_CODE("Non_Returnable_Code"), OVERWEIGHT_OVERSIZE_INDICATOR(
                "OverweightOversize_Indicator"), RECYCLE_INDICATOR("Recycle_Indicator"), UNSPSC("UNSPSC"), WBE_INDICATOR(
                "WBE_Indicator");

        private final String description;

        private ClassificationTypeEnum(String description) {
            this.description = description;
        }

        public String getDescription() {
            return this.description;
        }
    }

    @Inject
    private UnitOfMeasureDao uomDao;

    @Inject
    private SkuDao skuDao;

    @Inject
    private CustomerCostBp customerCostBp;

    @Inject
    private ItemCacheManager itemCacheManager;

    /**
     * This method is injected with the correct Dao for initialization
     * 
     * @param dao
     */
    @Inject
    public void initDao(ItemDao dao) {
        this.dao = dao;
    }

    public void eagerLoadCustomerCosts(Set<CustomerCost> toLoad) {
        for (CustomerCost cost : toLoad) {
            customerCostBp.eagerLoad(cost);
        }
    }

    public void eagerLoadSkus(Set<Sku> toLoad) {
        for (Sku sku : toLoad) {
            skuDao.eagerLoad(sku);
        }
    }

    @Override
    public Item eagerLoad(Item toLoad) {
        return ((ItemDao) dao).eagerLoad(toLoad);
    }

    /**
     * Takes an item and an ordered list of property types to be stored, and returns the item as a row in a CSV file.
     * 
     * @param entry - the item to be stored in a CSV
     * @param propertyTypes - an ordered list of properties on the item.
     * @param skuTypes - an ordered list of sku types
     * @param customerList - an ordered list of customers
     * @return
     */
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public String csvRow(Item entry, List<ItemPropertyType> propertyTypes, List<SkuType> skuTypes,
            List<String> customerList) {
        if (entry == null) {
            return BLANK;
        }
        Map<String, String> propertyValues = new HashMap<>();
        Map<String, Sku> skuValues = new HashMap<>();
        Map<String, String> customerCosts = new HashMap<>();
        for (ItemXItemPropertyType property : entry.getPropertiesReadOnly()) {
            propertyValues.put(property.getType().getName(), property.getValue());
        }
        for (Sku sku : entry.getSkus()) {
            skuValues.put(sku.getType().getName(), sku);
        }
        for (CustomerCost cost : entry.getCustomerCosts()) {
            customerCosts.put(cost.getCustomer().getName(), String.valueOf(cost.getValue()));
        }
        StringBuilder outputBuffer = new StringBuilder();
        //Stores the name of the Vendor
        outputBuffer.append(StringEscapeUtils.escapeCsv(entry.getVendorCatalog().getVendor().getName()));
        outputBuffer.append(DELIMETER);
        //Stores the APD sku
        outputBuffer.append(StringEscapeUtils.escapeCsv(skuValues.get(APD_SKUTYPE).getValue()));
        outputBuffer.append(DELIMETER);
        //Stores hierarchy node of the item
        if (entry.getHierarchyNode() != null) {
            outputBuffer.append(StringEscapeUtils.escapeCsv(entry.getHierarchyNode().getPublicId().toString()));
        }
        outputBuffer.append(DELIMETER);
        //Stores item information
        outputBuffer.append(StringEscapeUtils.escapeCsv(entry.getName()));
        outputBuffer.append(DELIMETER);
        outputBuffer.append(StringEscapeUtils.escapeCsv(entry.getDescription()));
        outputBuffer.append(DELIMETER);
        if (StringUtils.isNotBlank(entry.getSmartSearchDealerDescription())) {
        	outputBuffer.append(StringEscapeUtils.escapeCsv(entry.getSmartSearchDealerDescription()));
        }
        outputBuffer.append(DELIMETER);
        if (entry.getItemCategory() != null) {
            outputBuffer.append(StringEscapeUtils.escapeCsv(entry.getItemCategory().getName()));
        }
        outputBuffer.append(DELIMETER);
        if (entry.getItemCategory() != null) {
            outputBuffer.append(StringEscapeUtils.escapeCsv(entry.getUnitOfMeasure().getName()));
        }
        outputBuffer.append(DELIMETER);
        StringBuffer imageString = new StringBuffer();
        for (ItemImage image : entry.getItemImages()) {
        	if (image.getPrimary() != null && image.getPrimary()) {
        		imageString.append(image.getImageUrl());
        	}
        }
        for (ItemImage image : entry.getItemImages()) {
        	if (image.getPrimary() == null || !image.getPrimary()) {
        		if (imageString.length() != 0) {
        			imageString.append(IN_ROW_DELIMETER);
        		}
        		imageString.append(image.getImageUrl());
        	}
        }
        outputBuffer.append(StringEscapeUtils.escapeCsv(imageString.toString()));
        outputBuffer.append(DELIMETER);
        if (ItemStatus.DISCONTINUED.equals(entry.getStatus())) {
        	outputBuffer.append(StringEscapeUtils.escapeCsv(TRUE));
        }
        outputBuffer.append(DELIMETER);
        //Stores the APD sku of the replacement item
        if (entry.getReplacement() != null) {
        	for (Sku sku : entry.getReplacement().getSkus()) {
        		if (sku.getType().getName().equals(APD_SKUTYPE)) {
                    outputBuffer.append(StringEscapeUtils.escapeCsv(sku.getValue()));
                    break;
        		}
        	}
        }
        outputBuffer.append(DELIMETER);
        if (entry.getManufacturer() != null) {
            outputBuffer.append(StringEscapeUtils.escapeCsv(entry.getManufacturer().getName()));
        }
        outputBuffer.append(DELIMETER);
        if (entry.getBrand() != null) {
            outputBuffer.append(StringEscapeUtils.escapeCsv(entry.getBrand().getName()));
        }
        
        outputBuffer.append(DELIMETER);
        if (StringUtils.isNotBlank(entry.getProtectedSearchTerms())) {
        	outputBuffer.append(StringEscapeUtils.escapeCsv(entry.getProtectedSearchTerms()));
        }
        
        outputBuffer.append(DELIMETER);
        if (StringUtils.isNotBlank(entry.getSearchTerms())) {
        	outputBuffer.append(StringEscapeUtils.escapeCsv(entry.getSearchTerms()));
        }
        
        outputBuffer.append(DELIMETER);
        if (entry.getMinimum() != null) {
            outputBuffer.append(StringEscapeUtils.escapeCsv(entry.getMinimum().toString()));
        }
        
        outputBuffer.append(DELIMETER);
        if (entry.getMultiple() != null) {
            outputBuffer.append(StringEscapeUtils.escapeCsv(entry.getMultiple().toString()));
        }
        
        outputBuffer.append(DELIMETER);
        if (entry.getLastCostChangeDate() != null) {
            outputBuffer.append(StringEscapeUtils.escapeCsv("  "+entry.getLastCostChangeDate().toString()+ " "));
        }
        
        //Stores the values for different property types
        for (ItemPropertyType type : propertyTypes) {
            outputBuffer.append(DELIMETER);
            if (propertyValues.containsKey(type.getName())) {
                outputBuffer.append(StringEscapeUtils.escapeCsv(propertyValues.get(type.getName())));
            }
        }
        outputBuffer.append(DELIMETER);
        StringBuilder classificationBuffer = new StringBuilder();
        Map<ItemClassificationType, String> classMap = entry.getClassifications();
        for (ItemClassificationType classType : classMap.keySet()) {
            if (classificationBuffer.length() != 0) {
                classificationBuffer.append(IN_ROW_DELIMETER);
            }
            classificationBuffer.append(classType.getName());
            classificationBuffer.append(SUB_DELIMITER);
            classificationBuffer.append(classMap.get(classType));
        }
        outputBuffer.append(StringEscapeUtils.escapeCsv(classificationBuffer.toString()));
        outputBuffer.append(DELIMETER);
        StringBuilder specificationBuffer = new StringBuilder();
        List<ItemSpecification> specMap = entry.getSpecifications();
        for (ItemSpecification spec : specMap) {
            if (specificationBuffer.length() != 0) {
                specificationBuffer.append(IN_ROW_DELIMETER);
            }
            specificationBuffer.append(spec.getName());
            specificationBuffer.append(SUB_DELIMITER);
            specificationBuffer.append(spec.getValue());
            specificationBuffer.append(SUB_DELIMITER);
            specificationBuffer.append(spec.getPriority());
        }
        outputBuffer.append(StringEscapeUtils.escapeCsv(specificationBuffer.toString()));
        //Stores the values for different SKU types
        for (SkuType type : skuTypes) {
            outputBuffer.append(DELIMETER);
            if (skuValues.containsKey(type.getName())) {
                outputBuffer.append(StringEscapeUtils.escapeCsv(skuValues.get(type.getName()).getValue()));
            }
        }
        //stores the different matchbooks
        outputBuffer.append(DELIMETER);
        StringBuilder matchbookBuffer = new StringBuilder();
        for (Matchbook matchbook : entry.getMatchbook()) {
        	if (matchbookBuffer.length() != 0) {
        		matchbookBuffer.append(IN_ROW_DELIMETER);
        	}
        	matchbookBuffer.append(matchbook.getModel());
        	matchbookBuffer.append(SUB_DELIMITER);
        	matchbookBuffer.append(matchbook.getManufacturer().getName());
        }
        outputBuffer.append(StringEscapeUtils.escapeCsv(matchbookBuffer.toString()));
        //Stores the costs for different customers
        for (String account : customerList) {
            outputBuffer.append(DELIMETER);
            if (customerCosts.containsKey(account)) {
                outputBuffer.append(StringEscapeUtils.escapeCsv(customerCosts.get(account)));
            }
        }

        return outputBuffer.toString();
    }

    @Inject
    private ItemCategoryBp categoryBp;

    @Inject
    private ManufacturerBp manufacturerBp;

    @Inject
    private SkuTypeBp skuTypeBp;

    @Inject
    private HierarchyNodeDao hierarchyNodeDao;

    @Inject
    private ItemPropertyTypeBp propertyTypeBp;

    @Inject
    private AccountBp accountBp;

    @Inject
    private PricingTypeBp pricingBp;

    @Inject
    private MatchbookBp matchbookBp;

    @Inject
    private BrandBp brandBp;

    public SyncItemResult checkItem(Map<String, String> itemData, Catalog catalog, SyncAction action,
            SyncItemResultSummary summary, Map<String, Long> propertyTypes) {
        SyncItemResult toReturn = new SyncItemResult();
        String dealerSku = itemData.get(APD_SKU_HEADER);
        if (StringUtils.isBlank(dealerSku)) {
            toReturn.getErrorSet().add(CatalogBp.SyncError.NO_APD_SKU.getLabel());
            summary.incrementErrors();
            dealerSku = APD_SKU_HEADER;
        }
        String vendorName = itemData.get(VENDOR_HEADER);
        if (StringUtils.isBlank(vendorName)) {
            toReturn.getErrorSet().add(CatalogBp.SyncError.NO_VENDOR.getLabel());
            summary.incrementErrors();
            vendorName = VENDOR_HEADER;
        }
        if (catalog.getVendor() == null || !vendorName.equals(catalog.getVendor().getName())) {
            toReturn.getErrorSet().add(CatalogBp.SyncError.INCORRECT_VENDOR.getLabel());
            summary.incrementErrors();
        }
        String catalogName = itemData.get(CATALOG);
        if (StringUtils.isBlank(catalogName)) {
            toReturn.getErrorSet().add(CatalogBp.SyncError.CATALOG_NOT_SPECIFIED.getLabel());
            summary.incrementErrors();
        }
        else if (!catalogName.equals(catalog.getName())) {
            toReturn.getErrorSet().add(CatalogBp.SyncError.CATALOG_INCORRECTLY_SPECIFIED.getLabel());
            summary.incrementErrors();
            //no check that existing catalog on the item matches, since that would hit the database
        }
        if (StringUtils.isBlank(itemData.get(UNIT_OF_MEASURE))) {
            toReturn.getWarningSet().add(CatalogBp.SyncError.UOM_NOT_ASSIGNED.getLabel());
            summary.incrementWarnings();
        }
        if (StringUtils.isBlank(itemData.get(ItemBp.VENDOR_SKUTYPE + ItemBp.SKU_SUFFIX))) {
            toReturn.getWarningSet().add(CatalogBp.SyncError.VENDOR_SKU_NOT_ASSIGNED.getLabel());
            summary.incrementWarnings();
        }
        String itemWeight = itemData.get(ItemBp.ITEM_WEIGHT);
        if (StringUtils.isBlank(itemWeight)) {
            toReturn.getWarningSet().add(CatalogBp.SyncError.ITEM_WEIGHT_NOT_FOUND.getLabel());
            summary.incrementWarnings();
        }
        else {
            try {
                new BigDecimal(itemWeight);
            }
            catch (NumberFormatException e) {
                toReturn.getErrorSet().add(CatalogBp.SyncError.NUMBER_FORMAT.getLabel() + ": " + ItemBp.ITEM_WEIGHT);
                summary.incrementErrors();
            }
        }
        if (StringUtils.isNotBlank(itemData.get(ItemBp.MINIMUM))) {
            try {
                new BigDecimal(itemData.get(ItemBp.MINIMUM));
            }
            catch (NumberFormatException e) {
                toReturn.getWarningSet().add(CatalogBp.SyncError.NUMBER_FORMAT.getLabel() + ": " + ItemBp.MINIMUM);
                summary.incrementWarnings();
            }
        }
        if (StringUtils.isNotBlank(itemData.get(ItemBp.MULTIPLES))) {
            try {
                new BigDecimal(itemData.get(ItemBp.MULTIPLES));
            }
            catch (NumberFormatException e) {
                toReturn.getWarningSet().add(CatalogBp.SyncError.NUMBER_FORMAT.getLabel() + ": " + ItemBp.MULTIPLES);
                summary.incrementWarnings();
            }
        }
        for (String key : itemData.keySet()) {
            if (StringUtils.isNotBlank(key) && StringUtils.isNotBlank(itemData.get(key))) {
                if (key.endsWith(ItemBp.COST_SUFFIX)) {
                    try {
                        new BigDecimal(itemData.get(key));
                    }
                    catch (NumberFormatException e) {
                        toReturn.getWarningSet().add(CatalogBp.SyncError.NUMBER_FORMAT.getLabel() + ": " + key);
                        summary.incrementWarnings();
                    }
                }
                if (!key.endsWith(HIERARCHY_SUFFIX) && !key.endsWith(ItemBp.COST_SUFFIX)
                        && !key.endsWith(ItemBp.SKU_SUFFIX) && !isKnownColumn(key, propertyTypes)) {
                    toReturn.getWarningSet().add(CatalogBp.SyncError.COLUMN_NOT_FOUND.getLabel() + ": " + key);
                    summary.incrementWarnings();
                }
            }
        }
        toReturn.setResultApdSku(dealerSku);
        toReturn.setResultVendor(vendorName);
        return toReturn;
    }

    private boolean isKnownColumn(String column, Map<String, Long> itemPropertyTypes) {
        for (String propertyType : itemPropertyTypes.keySet()) {
            if (column.equals(propertyType)) {
                return true;
            }
        }
        for (String knownColumn : KNOWN_COLUMNS) {
            if (column.equals(knownColumn)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Executes a synchronization operation on a catalog item.
     * @param itemData 
     * @param catalog 
     * @param action
     * @param summary
     * @return SyncItemResult wrapper object
     */
    public SyncItemResult syncItem(Map<String, String> itemData, Catalog catalog, SyncAction action,
            SyncItemResultSummary summary, Map<String, Long> propertyTypes) throws ItemRollbackException,
            ItemFailureException {
        try {
            if (catalog != null && catalog.getId() != null) {
                catalog = this.catalogDao.findById(catalog.getId(), Catalog.class);
            }
            return syncItemWithExceptions(itemData, catalog, action, summary, propertyTypes);
        }
        catch (Exception e) {
            if (e instanceof ItemRollbackException) {
                throw (ItemRollbackException) e;
            }
            else {
                ItemFailureException toThrow = new ItemFailureException();
                toThrow.setException(e);
                throw toThrow;
            }
        }
    }

    private SyncItemResult syncItemWithExceptions(Map<String,String> itemData, Catalog catalog, SyncAction action, SyncItemResultSummary summary, Map<String, Long> propertyTypes) throws ItemRollbackException {

    	//This class is a wrapper, storing the result as well as any warnings and errors that occur
        SyncItemResult toReturn = new SyncItemResult();
    	
    	Item result = this.getItemToSync(itemData, catalog, action, summary, toReturn);

        Map<String, Sku> skuMap = new HashMap<>();
        Map<String, CustomerCost> costMap = new HashMap<>();
        Map<String, ItemXItemPropertyType> propertyMap = new HashMap<>();
        //creates a map from the property type name, sku type name, or customer name to a property, sku, or customer
        for (Sku sku : result.getSkus()) {
            skuMap.put(sku.getType().getName(), sku);
        }
        for (CustomerCost cost : result.getCustomerCosts()) {
            costMap.put(cost.getCustomer().getName(), cost);
        }
        for (ItemXItemPropertyType property : result.getPropertiesReadOnly()) {
            propertyMap.put(property.getType().getName(), property);
        }
        //Iterates through each key
        for (String key : itemData.keySet()) {
            if (StringUtils.isBlank(itemData.get(key)) || key.equals(APD_SKU_HEADER) || key.equals(VENDOR_HEADER)) {
                //do nothing
            }
            else if (key.equals(NAME)) {
            	if (itemData.get(key).equals(NULL)) {
            		result.setName(null);
            		continue;
            	}
                result.setName(itemData.get(key));
            }
            else if (key.equals(USE_WITH)) {
            	if (itemData.get(key).equals(NULL)) {
            		result.setForUseWith(null);
            		continue;
            	}
                result.setForUseWith(itemData.get(key));
            }
            else if (key.equals(DESCRIPTION)) {
            	if (itemData.get(key).equals(NULL)) {
            		result.setDescription(null);
            		continue;
            	}
                result.setDescription(itemData.get(key));
            }
            else if (key.equals(SMART_SEARCH_DEALER_DESCRIPTION)) {
            	if (itemData.get(key).equals(NULL)) {
            		result.setSmartSearchDealerDescription(null);
            		continue;
            	}
                result.setSmartSearchDealerDescription(itemData.get(key));
            } else if (key.equals(FEATURED_PRODUCT)) {
            	if (TRUE.equalsIgnoreCase(itemData.get(key))) {
            		result.setIsFeaturedItem(true);
            	} else if(FALSE.equalsIgnoreCase(itemData.get(key))) {
            		result.setIsFeaturedItem(false);
            	} else {
            		result.setIsFeaturedItem(null);
            	}
            }
            else if (key.equals(CATEGORY) && !SyncAction.VENDOR_UPDATE.equals(action)) {
            	if (itemData.get(key).equals(NULL)) {
            		result.setItemCategory(null);
            		continue;
            	}
                //Stores the category of the item
                ItemCategory category = categoryBp.getCategoryByName(itemData.get(key));
                if (category != null) {
                    result.setItemCategory(category);
                }
                else {
                    toReturn.getWarningSet().add(CatalogBp.SyncError.CATEGORY_NOT_FOUND.getLabel());
                    summary.incrementWarnings();
                }
            }
            else if (key.equals(CATALOG)) {
            	if (!itemData.get(key).equals(catalog.getName())) {
                	//if the catalog being updated and the "catalog" column in the CSV don't match, 
                	//log an error
                	toReturn.getErrorSet().add(CatalogBp.SyncError.CATALOG_INCORRECTLY_SPECIFIED.getLabel());
                	summary.incrementErrors();
            	} else if (result.getVendorCatalog() == null) {
                	//if the updated catalog and the catalog column match, and the item has no catalog
                	//(because it's a new item), set the item's catalog to the catalog being updated
            		result.setVendorCatalog(catalog);
            	} else if (!itemData.get(key).equals(result.getVendorCatalog().getName())) {
                	//if the item has a catalog, and it doesn't match the catalog being updated, log
                	//an error
                	toReturn.getErrorSet().add(CatalogBp.SyncError.EXISTING_CATALOG_INCORRECT.getLabel());
                	summary.incrementErrors();
            	}
            	//if none of the above is triggered, then the catalog being updated matches the item's 
            	//catalog, and they both match the "catalog" column, so no change is necessary since
            	//the item is already on the catalog.
            }
            else if (key.equals(MANUFACTURER)) {
            	if (itemData.get(key).equals(NULL)) {
            		result.setManufacturer(null);
            		continue;
            	}
                //Stores the manufacturer of the item
                Manufacturer searchManufacturer = new Manufacturer();
                searchManufacturer.setName(itemData.get(key));
                List<Manufacturer> searchList = manufacturerBp.searchByExactExample(searchManufacturer, 0, 0);
                if (!searchList.isEmpty()) {
                    result.setManufacturer(searchList.get(0));
                }
                else {
                	Manufacturer newManufacturer = new Manufacturer();
                	newManufacturer.setName(itemData.get(key));
                	result.setManufacturer(manufacturerBp.create(newManufacturer));
                }
            }
            else if (key.equals(BRAND_ID)){
            	String brandId = itemData.get(key);
            	if (brandId.equals(NULL)) {
            		result.setBrand(null);
            	}
            	Brand brand = brandBp.findByUsscoId(brandId);
            	if(brand == null){
                    toReturn.getWarningSet().add(SyncError.BRAND_NOT_FOUND.getLabel() + brandId);
                    summary.incrementWarnings();
            	}
            	result.setBrand(brand);
            }
            else if (key.equals(BRAND_NAME)){
            	String brandName = itemData.get(key);            	
            	Brand brand = brandBp.findByBrandName(brandName);
            	if (brand != null) {
            		result.setBrand(brand);
            	} else {
            		Brand newBrand = new Brand();
            		newBrand.setName(brandName);
            		result.setBrand(newBrand);
            	}
            }
            else if (key.equals(REPLACEMENT_VENDOR)) {
                //do nothing; handled in the "REPLACEMENT_SKU" case
            }
            else if (key.equals(REPLACEMENT_SKU)) {
            	if (itemData.get(key).equals(NULL)) {
            		result.setReplacement(null);
            		continue;
            	}
            	//setting replacement SKU of an item
                if (itemData.get(key) != null) {
                    try {
                    	String replacementSku = itemData.get(key);
                    	String replacementVendor = catalog.getVendor().getName();
                    	if (itemData.containsKey(REPLACEMENT_VENDOR) && StringUtils.isNotBlank(itemData.get(REPLACEMENT_VENDOR))) {
                    		replacementVendor = itemData.get(REPLACEMENT_VENDOR);
                    	}
                    	result.setReplacement(this.searchByExactExample(searchItem(replacementSku, replacementVendor), 0, 0).get(0));
                    }
                    catch (Exception e) {
                        toReturn.getWarningSet().add(SyncError.ITEM_NOT_FOUND.getLabel());
                        summary.incrementWarnings();
                    }
                }
            }
            else if (key.equals(ETS_SKU)) {
            	if (itemData.get(key).equals(NULL)) {
            		result.setAbilityOneSubstitute(null);
            		continue;
            	}
            	//setting the ETS sku of an item (might be replaced by item relationship)
                if (itemData.get(key) != null) {
                    try {
                        this.setEtsItem(result.getSku(ItemBp.APD_SKUTYPE), itemData.get(key), catalog.getVendor().getName());
                    }
                    catch (ItemNotFoundException e) {
                        toReturn.getWarningSet().add(SyncError.ITEM_NOT_FOUND.getLabel());
                        summary.incrementWarnings();
                    }
                }
            }
            else if (key.endsWith(COST_SUFFIX)) {
                //finds the customer specified, then sets the customer cost for that customer
                String accountName = key.substring(0, key.length() - COST_SUFFIX.length());
                //If the cost exists, sets it to the new value
                if (costMap.containsKey(accountName)) {
                    try {
                    	if (itemData.get(key).equals(NULL)) {
                    		result.getCustomerCosts().remove(costMap.get(accountName));
                    		continue;
                    	}
                    	costMap.get(accountName).setValue(new BigDecimal(itemData.get(key)));
                    }
                    catch (Exception e) {
                        toReturn.getWarningSet().add(CatalogBp.SyncError.NUMBER_FORMAT.getLabel());
                        summary.incrementWarnings();
                    }
                }
                else if (!itemData.get(key).equals(NULL)) { //otherwise, creates a new CustomerCost object
                    Account searchAccount = new Account();
                    searchAccount.setName(key.substring(0, key.length() - COST_SUFFIX.length()));
                    List<Account> accountResults = accountBp.searchByExactExample(searchAccount, 0, 0);
                    if (accountResults.isEmpty()) {
                        toReturn.getWarningSet().add(CatalogBp.SyncError.ACCOUNT_NOT_FOUND.getLabel());
                        summary.incrementWarnings();
                    }
                    else {
                        CustomerCost newCost = new CustomerCost();
                        newCost.setCustomer(accountResults.get(0));
                        newCost.setItem(result);
                        try {
                            newCost.setValue(new BigDecimal(itemData.get(key)));
                            result.getCustomerCosts().add(newCost);
                        }
                        catch (Exception e) {
                            toReturn.getWarningSet().add(CatalogBp.SyncError.NUMBER_FORMAT.getLabel());
                            summary.incrementWarnings();
                        }
                    }
                }
            }
            else if (key.endsWith(SKU_SUFFIX)) {
                //Changes the value of the specified SKU
                String skuTypeName = key.substring(0, key.length() - SKU_SUFFIX.length());
                Sku currentSku;
                //if the sku exists already, changes the value
                if (skuMap.containsKey(skuTypeName)) {
                	if (itemData.get(key).equals(NULL)) {
                		result.getSkus().remove(skuMap.get(skuTypeName));
                		continue;
                	}
                    skuMap.get(skuTypeName).setValue(itemData.get(key));
                    currentSku = skuMap.get(skuTypeName);
                }
                else if (!itemData.get(key).equals(NULL)) { //otherwise, creates a new sku of that type 
                    SkuType skuTypeSearch = new SkuType();
                    skuTypeSearch.setName(skuTypeName);
                    List<SkuType> skuTypeList = skuTypeBp.searchByExactExample(skuTypeSearch, 0, 1);
                    if (skuTypeList.size() == 0) {
                        toReturn.getWarningSet().add(CatalogBp.SyncError.SKU_TYPE_NOT_FOUND.getLabel());
                        summary.incrementWarnings();
                        continue;
                    }
                    currentSku = new Sku();
                    currentSku.setValue(itemData.get(key));
                    currentSku.setType(skuTypeList.get(0));
                    currentSku.setItem(result);
                    result.getSkus().add(currentSku);
                }
            }
            else if (key.endsWith(HIERARCHY_SUFFIX)) {
                //do nothing; handled in the "sku" case
            }
            else if (key.equals(OVERWRITE_PROTECTED_SEARCH_TERMS)) {
                //do nothing; handled in the "PROTECTED_SEARCH_TERMS" case
            }
            else if (key.equals(PROTECTED_SEARCH_TERMS)) {
            	boolean overrideExisting = (itemData.get(OVERWRITE_PROTECTED_SEARCH_TERMS) != null 
            			&& itemData.get(OVERWRITE_PROTECTED_SEARCH_TERMS).equalsIgnoreCase(YES));
            	if (overrideExisting && itemData.get(key).equals(NULL)) {
            		result.setProtectedSearchTerms(null);
            		continue;
            	}
                //sets the item's search terms
        		//first adds all existing search terms
        		Set<String> terms = new HashSet<String>();
        		if (!overrideExisting && !StringUtils.isBlank(result.getProtectedSearchTerms())) {
	        		for (String s : result.getProtectedSearchTerms().split(IN_ROW_DELIMETER_SPLITTER)) {
	        			terms.add(s);
	        		}
        		}
        		//then adds the new search terms
        		for (String s : itemData.get(key).split(IN_ROW_DELIMETER_SPLITTER)) {
        			terms.add(s);
        		}
        		//the builds the string, and stores it
        		StringBuilder termString = new StringBuilder();
        		for (String s : terms) {
        			if (termString.length() > 0) {
        				termString.append(IN_ROW_DELIMETER);
        			}
        			termString.append(s);
        		}
        		result.setProtectedSearchTerms(termString.toString());
            }
            else if (key.equals(SEARCH_TERMS)) {
            	if (action != SyncAction.VENDOR_UPDATE) {
    				toReturn.getWarningSet().add(CatalogBp.SyncError.WRONG_SEARCH_TERMS.getLabel());
    				summary.incrementWarnings();
            	} else {
            		result.setSearchTerms(itemData.get(key));
            	}
            }
            else if (key.equals(SPECIFICATIONS)) {
            	if (itemData.get(key).equals(NULL)) {
            		result.setSpecifications(new ArrayList<ItemSpecification>());
            		continue;
            	}
            	String[] attributes = itemData.get(key).split(IN_ROW_DELIMETER_SPLITTER);
            	List<ItemSpecification> specificationMap = new ArrayList<ItemSpecification>();
            	for (String attribute : attributes) {
            		//for each bar-separated attribute in the specifications, gets the location of 
            		//the first delimeter (separating the key and the value) and the second 
            		//delimeter (if it exists, separating the value and the priority). 
            		int equalsLocation = attribute.indexOf(SUB_DELIMITER);
        			int indexOfPrioritySeparator = StringUtils.lastIndexOf(attribute, SUB_DELIMITER);
            		String attributeKey = BLANK;
            		String attributeValue = BLANK;
            		//if there is a first delimeter
            		if (equalsLocation > 0) {
            			attributeKey = attribute.substring(0, equalsLocation);
            			//if there is no second delimeter, or the only other delimeter found is 
            			//in the same position as the first, then there is no priority specified.
            			//adds a tailing delimeter with nothing after, indicating an empty priority
            			if (indexOfPrioritySeparator < 0 || indexOfPrioritySeparator == equalsLocation) {
            				attribute = attribute + SUB_DELIMITER;
            				indexOfPrioritySeparator = attribute.length() - 1;
            			}
            			Long priority = null;
            			//gets the string value of the priority (could be blank)
            			String priorityString = attribute.substring(indexOfPrioritySeparator +1, attribute.length());
            			if(StringUtils.isNotBlank(priorityString)){
	            			try{
	            				//tries to set the priority to a parsed Long of the priority string
	            				priority = new Long(priorityString);
	            			} catch (NumberFormatException e){
	            				//if the long can't be parsed, assumes that the priority is actually 
	            				//part of value of the specification
	            				toReturn.getWarningSet().add(CatalogBp.SyncError.NUMBER_FORMAT.getLabel());
	            				indexOfPrioritySeparator = attribute.length();
	            			}
            			}
            			attributeValue = attribute.substring(equalsLocation + 1, indexOfPrioritySeparator);
            			ItemSpecification property = new ItemSpecification();
            			property.setName(attributeKey);
            			property.setValue(attributeValue);
            			property.setPriority(priority);
            			specificationMap.add(property);
            			if (ITEM_WEIGHT_SPECIFICATION_NAME.equals(attributeKey)) {
	            			try{
	            				result.setItemWeight(new BigDecimal(attributeValue));
	            			} catch (NumberFormatException e){
	            				toReturn.getWarningSet().add(CatalogBp.SyncError.NUMBER_FORMAT.getLabel());
	            			}
            			}
            		}
        			
            	}
            	result.setSpecifications(specificationMap);
            }
            else if (key.equals(UPSELL_ITEMS)) {
            	String[] attributes = itemData.get(key).split(IN_ROW_DELIMETER_SPLITTER);
            	int position = 1;
            	for (String attribute : attributes) {
            		int equalsLocation = attribute.indexOf(SUB_DELIMITER_HYPHEN);
            		String relationshipVendorName = BLANK;
            		String relationshipSku = BLANK;
            		if (equalsLocation > 0) {            			
            			relationshipVendorName = attribute.substring(0, equalsLocation);
            			relationshipSku = attribute.substring(equalsLocation + 1, attribute.length());
            			setRelationships(result, DOWNSELL_UPSELL, relationshipSku, relationshipVendorName, position);
            			position++;
            		}
        			
            	}
            	
            }
            else if (key.equals(COMPANION_ITEMS)) {
            	String[] attributes = itemData.get(key).split(IN_ROW_DELIMETER_SPLITTER);
            	int position = 1;
            	for (String attribute : attributes) {
            		int equalsLocation = attribute.indexOf(SUB_DELIMITER_HYPHEN);
            		String relationshipVendorName = BLANK;
            		String relationshipSku = BLANK;
            		if (equalsLocation > 0) {            			
            			relationshipVendorName = attribute.substring(0, equalsLocation);
            			relationshipSku = attribute.substring(equalsLocation + 1, attribute.length());
            			setRelationships(result, COMPANION_FOR_COMPANION, relationshipSku, relationshipVendorName, position);
            			position++;
            		}
        			
            	}
            	
            }
            else if (key.equals(ACCESSORY_ITEMS)) {
            	String[] attributes = itemData.get(key).split(IN_ROW_DELIMETER_SPLITTER);
            	int position = 1;
            	for (String attribute : attributes) {
            		int equalsLocation = attribute.indexOf(SUB_DELIMITER_HYPHEN);
            		String relationshipVendorName = BLANK;
            		String relationshipSku = BLANK;
            		if (equalsLocation > 0) {            			
            			relationshipVendorName = attribute.substring(0, equalsLocation);
            			relationshipSku = attribute.substring(equalsLocation + 1, attribute.length());
            			setRelationships(result, ACCESSORY_FOR_ACCESSORIES, relationshipSku, relationshipVendorName, position);
            			position++;
            		}
        			
            	}
            	
            }            
            else if (key.equals(CLASSIFICATIONS)) {
            	if (itemData.get(key).equals(NULL)) {
            		result.setClassifications(new HashMap<ItemClassificationType, String>());
            		continue;
            	}
            	String[] attributes = itemData.get(key).split(IN_ROW_DELIMETER_SPLITTER);
            	Map<ItemClassificationType, String> classificationMap = new HashMap<ItemClassificationType, String>();
            	List<ItemClassificationType> itemClassTypes = classTypeBp.findAll(ItemClassificationType.class, 0, 0);
            	for (String attribute : attributes) {
            		int equalsLocation = attribute.indexOf(SUB_DELIMITER);
            		String attributeKey = BLANK;
            		String attributeValue = YES;
            		if (equalsLocation > 0) {            			
            			attributeKey = attribute.substring(0, equalsLocation);
            			attributeValue = attribute.substring(equalsLocation + 1, attribute.length());
            		} else {
            			continue;
            		}
            		boolean placedInMap = false;
        			for (ItemClassificationType classType : itemClassTypes) {
        				if (classType.getName().equals(attributeKey)) {
        					classificationMap.put(classType, attributeValue);
        					placedInMap = true;
        					if (classType.getName().toLowerCase().equals(UNSPSC_CLASSIFICATIONTYPE)) {
        						result.setCommodityCode(attributeValue);
        					}
        					break;
        				}
        			}
        			if (!placedInMap) {
        				toReturn.getWarningSet().add(CatalogBp.SyncError.CLASSIFICATION_NOT_FOUND.getLabel());
        				summary.incrementWarnings();
        			}
            	}
            	result.setClassifications(classificationMap);
            }
            else if (key.equals(UNIT_OF_MEASURE)) {
            	if (itemData.get(key).equals(NULL)) {
            		result.setUnitOfMeasure(null);
            		continue;
            	}
                //sets the item's unit of measure
                UnitOfMeasure searchUom = new UnitOfMeasure();
                searchUom.setName(itemData.get(key));
                List<UnitOfMeasure> uomList = uomDao.searchByExactExample(searchUom, 0, 0);
                if (!uomList.isEmpty()) {
                    result.setUnitOfMeasure(uomList.get(0));
                }
                else {
                    toReturn.getWarningSet().add(CatalogBp.SyncError.UOM_NOT_FOUND.getLabel());
                    summary.incrementErrors();
                }
            }
            else if (key.equals(HIERARCHY_HEADER)) {
            	if (itemData.get(key).equals(NULL)) {
            		result.setHierarchyNode(null);
            		continue;
            	}
            	if(StringUtils.isBlank(itemData.get(HIERARCHY_TYPE)) || itemData.get(HIERARCHY_TYPE).equals(NULL)) {
           		 //otherwise, warning is displayed
                   toReturn.getWarningSet().add(CatalogBp.SyncError.HIERARCHY_TYPE.getLabel());
                   summary.incrementWarnings();
            	}
            	//sets the item's hierarchy node 
                try {                	
	                	List<HierarchyNode> nodeList = 
	                			hierarchyNodeDao.searchByPublicIdAndType(Long.valueOf(itemData.get(HIERARCHY_HEADER)), itemData.get(HIERARCHY_TYPE));
	                if (!nodeList.isEmpty()) {
	                    result.setHierarchyNode(nodeList.get(0));
	                } else {
	                	 //otherwise, warning is displayed
	                    toReturn.getWarningSet().add(CatalogBp.SyncError.HIERARCHY_NOT_FOUND.getLabel());
	                    summary.incrementWarnings();
	                }
                } catch (NumberFormatException e) {
                	//the value entered wasn't a number, do nothing
                	 toReturn.getWarningSet().add(CatalogBp.SyncError.HIERARCHY_NOT_FOUND.getLabel());
	                    summary.incrementWarnings();
                }  
            } else if (key.equals(HIERARCHY_TYPE)) {
            	//do nothing its  used in HIERARCHY_HEADER
            }
            else if (key.equals(DISCONTINUED_TYPE)) {
                if (itemData.get(key).equalsIgnoreCase(TRUE) && !ItemStatus.DISCONTINUED.equals(result.getStatus())) {
                    //If the item is being set to discontinued and either the propery map doesn't have the 
                    //"discontinued" property or the value of the property isn't true, discontinues the item.
                    summary.incrementDiscontinued();
                	this.discontinue(result);
                }
                else if (itemData.get(key).equalsIgnoreCase(FALSE) && ItemStatus.DISCONTINUED.equals(result.getStatus())) {
                    //If a discontinued item has the discontinued property set to false, undiscontinues
            		this.unDiscontinue(result);
                } else if (itemData.get(key).equalsIgnoreCase(TRUE) && ItemStatus.DISCONTINUED.equals(result.getStatus())) {
                	 //do nothing; If the item is being set to discontinued and item property have discontinued"
                }
                else {
                    toReturn.getWarningSet().add(CatalogBp.SyncError.PROPERTY_VALUE_NOT_FOUND.getLabel());
                    summary.incrementWarnings();
                }
            }
            else if (key.equals(IMAGE_URLS)) {
            	if (itemData.get(key).equals(NULL)) {
            		itemData.put(key, BLANK);
            	}
            	String[] imageUrls = itemData.get(key).split(IN_ROW_DELIMETER_SPLITTER);
            	result.getItemImages().removeAll(result.getItemImages());
            	for (String imageUrl : imageUrls) {
            		ItemImage image = new ItemImage();
            		image.setImageUrl(imageUrl);
            		image.setItem(result);
            		//the first URL in the list is set as "primary"
            		image.setPrimary(result.getItemImages().isEmpty());
            		result.getItemImages().add(image);
            	}
            }
            else if (key.equals(MATCHBOOK)) {
            	if (itemData.get(key).equals(NULL)) {
            		result.getMatchbook().removeAll(result.getMatchbook());
            		continue;
            	}
            	//creates link to matchbook items, creating the items if necessary
            	String[] matchbookProducts = itemData.get(key).split(IN_ROW_DELIMETER_SPLITTER);
            	boolean foundAllMatchbooks = true;
            	for (String product : matchbookProducts) {
            		Matchbook matchbook = matchbookBp.findFromArray(product.split(SUB_DELIMITER));
            		if (matchbook != null) {
            			result.getMatchbook().add(matchbook);
            		} else {
            			foundAllMatchbooks = false;
            		}
            	}
            	if (!foundAllMatchbooks) {
                    toReturn.getWarningSet().add(CatalogBp.SyncError.MATCHBOOK_MANUFACTURER_NOT_FOUND.getLabel());
                    summary.incrementWarnings();
            	}
            }
            else if (key.equals(SELLING_POINT)) {
            	if (itemData.get(key).equals(NULL)) {
            		itemData.put(key, BLANK);
            	}
            	//creates selling points of the items
            	String[] sellingPoints = itemData.get(key).split(IN_ROW_DELIMETER_SPLITTER);
            	result.getSellingPoints().removeAll(result.getSellingPoints());
            	for (int i = 0; i < sellingPoints.length; i ++) {
            		ItemSellingPoint newSellingPoint = new ItemSellingPoint();
            		newSellingPoint.setValue(sellingPoints[i]);
            		newSellingPoint.setPosition(i + 1);
            		result.getSellingPoints().add(newSellingPoint);
            	}
            }
            else if (key.equals(MULTIPLES)) {
            	//sets the multiples that the item must be ordered in
            	try {
                	if (itemData.get(key).equals(NULL)) {
                		result.setMultiple(null);
                		continue;
                	}
            		result.setMultiple(Integer.parseInt(itemData.get(key)));
            	}
            	catch (NumberFormatException e) {
                    toReturn.getWarningSet().add(CatalogBp.SyncError.NUMBER_FORMAT.getLabel() + ": " + key);
                    summary.incrementWarnings();
            	}
            }
            else if (key.equals(MINIMUM)) {
            	//sets the minimum of the item that must be ordered
            	try {
                	if (itemData.get(key).equals(NULL)) {
                		result.setMinimum(null);
                		continue;
                	}
            		result.setMinimum(Integer.parseInt(itemData.get(key)));
            	}
            	catch (NumberFormatException e) {
                    toReturn.getWarningSet().add(CatalogBp.SyncError.NUMBER_FORMAT.getLabel() + ": " + key);
                    summary.incrementWarnings();
            	}
            }
            else if (key.equals(ITEM_WEIGHT)) {
            	try {
                	if (itemData.get(key).equals(NULL)) {
                		toReturn.getErrorSet().add(CatalogBp.SyncError.ITEM_WEIGHT_NOT_FOUND.getLabel() + ": " + key);
                        summary.incrementErrors();                		
                	}                	
            		result.setItemWeight(new BigDecimal(itemData.get(key)));
            	}
            	catch (NumberFormatException e) {
                    toReturn.getErrorSet().add(CatalogBp.SyncError.NUMBER_FORMAT.getLabel() + ": " + key);
                    summary.incrementErrors();
                    result.setItemWeight(new BigDecimal(0));
            	}
            } else if(key.equals(CARTRIDGE_NUMBER)){
            	result.setCartridgeNumber(itemData.get(key));
            } else if(key.equals(PROTECTED_COMMODITY_CODE)) {
            	result.setProtectedCommodityCode(itemData.get(key));
            } else {
                //This is reached for any header that doesn't match previous values, ie it's an item property.
                if (propertyMap.containsKey(key)) {
                	ItemXItemPropertyType toModify = propertyMap.get(key);
					if (itemData.get(key).equals(NULL)) {
                		removeProperty(result,toModify);
                		continue;
                	}
                    //If the property already exists, changes its value
					ItemXItemPropertyType newProperty = new ItemXItemPropertyType();
                    newProperty.setValue(itemData.get(key));
                    newProperty.setType(toModify.getType());
                    result = addOrModifyItemProperty(result, newProperty);
                }
                else if (!itemData.get(key).equals(NULL)) {
                    //otherwise, creates a new itemxitempropertytype.
                    ItemPropertyType newType = propertyTypeBp.getPropertyType(key, propertyTypes);
                    if (newType == null) {
                        toReturn.getWarningSet().add(CatalogBp.SyncError.COLUMN_NOT_FOUND.getLabel() + ": " + key);
                        summary.incrementWarnings();
                    }
                    else {
                        ItemXItemPropertyType newProperty = new ItemXItemPropertyType();
                        newProperty.setValue(itemData.get(key));
                        newProperty.setType(newType);
                        result = addOrModifyItemProperty(result, newProperty);
                    }
                }
            }
        }
        if (StringUtils.isBlank(itemData.get(CATALOG))) {
            toReturn.getErrorSet().add(CatalogBp.SyncError.CATALOG_NOT_SPECIFIED.getLabel());
            summary.incrementErrors();
        }
        
        this.setItemStatus(result);
        
        if (action != SyncAction.VENDOR_UPDATE) {
	        try {
	        	if(result.getVendorCatalog() == null){
	                toReturn.getErrorSet().add(CatalogBp.SyncError.NO_CATALOG_FOUND.getLabel());
	                summary.incrementErrors();
	            	//rolls back changes
	            	ItemRollbackException exception = new ItemRollbackException();
	            	exception.setResult(toReturn);
	            	throw exception;
	        	}
	        	this.calculateStreetPrice(result, propertyTypeBp.getPropertyType(STREET_PRICE_PROPERTYTYPE, propertyTypes));
	        }
	        catch (PricingCalculationException e) {
	        	toReturn.getErrorSet().add(CatalogBp.SyncError.CANT_CALCULATE_STREET.getLabel());
	            summary.incrementErrors();
	        }
        }
        //checks to see if a required field has been left blank on the item
        if (result.getItemCategory() == null && action != SyncAction.VENDOR_UPDATE) {
            toReturn.getErrorSet().add(CatalogBp.SyncError.CATEGORY_NOT_ASSIGNED.getLabel());
            summary.incrementErrors();
        } else if (result.getItemCategory() == null && action.equals(SyncAction.VENDOR_UPDATE)) {
        	//set category to "new item"
            ItemCategory newItemCategory = categoryBp.getCategoryByName("New Item");
            if (newItemCategory != null) {
                result.setItemCategory(newItemCategory);
            } else {
            	toReturn.getErrorSet().add(CatalogBp.SyncError.CATEGORY_NOT_ASSIGNED.getLabel());
                summary.incrementErrors();
            }
        }
        if (result.getUnitOfMeasure() == null) {
            toReturn.getErrorSet().add(CatalogBp.SyncError.UOM_NOT_ASSIGNED.getLabel());
            summary.incrementErrors();
        }
        if (result.getName() == null || result.getName().isEmpty()) {
            toReturn.getErrorSet().add(CatalogBp.SyncError.NAME_NOT_ASSIGNED.getLabel());
            summary.incrementErrors();
        }
        if (result.getDescription() == null || result.getDescription().isEmpty()) {
            toReturn.getErrorSet().add(CatalogBp.SyncError.DESCRIPTION_NOT_ASSIGNED.getLabel());
            summary.incrementErrors();
        }
        if (result.getItemWeight() == null) {
        	String weightSpecification = result.getSpecificationsMap().get(ITEM_WEIGHT_SPECIFICATION_NAME);
			if(!StringUtils.isEmpty(weightSpecification)){
				LOG.debug("Setting item weight to specification property vale " + weightSpecification);
				try {
					result.setItemWeight(new BigDecimal(weightSpecification));
				} catch (NumberFormatException e){
					LOG.error("Could not convert " + weightSpecification + " to big decimal.");
		            toReturn.getErrorSet().add(CatalogBp.SyncError.ITEM_WEIGHT_NOT_FOUND.getLabel());
		            summary.incrementErrors();
				}
			} else {
				toReturn.getErrorSet().add(CatalogBp.SyncError.ITEM_WEIGHT_NOT_FOUND.getLabel());
	            summary.incrementErrors();
			}
        }
        boolean hasVendorSku = false;
        for (Sku s : result.getSkus()) {
        	if (s.getType().getName().equals(VENDOR_SKUTYPE)) {
        		hasVendorSku = true;
        		break;
        	}
        }
        if (!hasVendorSku) {
            toReturn.getErrorSet().add(CatalogBp.SyncError.VENDOR_SKU_NOT_ASSIGNED.getLabel());
            summary.incrementErrors();
        }
        BigDecimal apdCost = BigDecimal.ZERO;
        boolean foundApdCost = false;
        if (action != SyncAction.VENDOR_UPDATE) {	        
	        for (ItemXItemPropertyType property : result.getPropertiesReadOnly()) {
	        	if (property.getType().getName().equals(APD_COST_PROPERTYTYPE) && property.getValue() != null) {
	        		foundApdCost = true;
	        		apdCost = new BigDecimal(property.getValue());
	        		break;
	        	}
	        }
	        if (!foundApdCost) {
	            toReturn.getErrorSet().add(CatalogBp.SyncError.NO_APD_COST.getLabel());
	            summary.incrementErrors();
	        }
        }
        
        boolean foundListPrice = false;
        BigDecimal listPrice = BigDecimal.ZERO;
        for (ItemXItemPropertyType property : result.getPropertiesReadOnly()) {
        	if (property.getType().getName().equals(LIST_PRICE_PROPERTYTYPE) && property.getValue() != null) {
        		foundListPrice = true;
        		listPrice = new BigDecimal(property.getValue());
        		break;
        	}
        }
        if (!foundListPrice) {
            toReturn.getErrorSet().add(CatalogBp.SyncError.NO_LIST_PRICE.getLabel());
            summary.incrementErrors();
        }
        
        //check to see apdcost should be less than list price
       if(foundApdCost && foundListPrice && apdCost.compareTo(listPrice) == 1) {    	   
		   toReturn.getErrorSet().add(CatalogBp.SyncError.APD_COST_GT_LIST_PRICE.getLabel());
           summary.incrementErrors();
       }
        
        //if changes should be made
        if (toReturn.getErrorSet().isEmpty()) {
            if (result.getId() != null) {
            	//if the entity will be updated, makes the update
                summary.incrementUpdated();
                this.update(result);
            }
            else {
            	//if it will be created, creates the entity
                summary.incrementCreated();
                this.createNoRefresh(result);
            }
        }
        else {
        	//rolls back changes
        	ItemRollbackException exception = new ItemRollbackException();
        	exception.setResult(toReturn);
        	throw exception;
        }
        return toReturn;
    }

    /**
     * Given several parameters, this method returns the item to update. It will return a non-null object. If a new item
     * should be created, the item will be empty; otherwise, an existing item will be returned. It is possible that the 
     * existing item will fail validation (including if the catalog it's on doesn't match the catalog on the CSV or 
     * the one being updated), but that is handled later in the syncItem method.
     * 
     * @param itemData
     * @param catalog
     * @param action
     * @param summary
     * @param toReturn
     * @return
     */
    private Item getItemToSync(Map<String, String> itemData, Catalog catalog, SyncAction action,
            SyncItemResultSummary summary, SyncItemResult toReturn) {
        //finds the APD sku and vendor, and finds the item
        String apdSku = itemData.get(APD_SKU_HEADER);
        String vendorName = itemData.get(VENDOR_HEADER);
        String vendorSku = itemData.get(ItemBp.VENDOR_SKUTYPE + ItemBp.SKU_SUFFIX);

        //occurs when updating based on USSCO XML. Then, only vendor SKU is specified.
        if (action.equals(SyncAction.VENDOR_UPDATE) && StringUtils.isEmpty(apdSku) && StringUtils.isNotBlank(vendorSku)
                && StringUtils.isNotBlank(vendorName)) {
            Item result = itemCacheManager.getVendorItemByVendorSku(vendorSku, vendorName);
            if (result != null) {
                apdSku = result.getSku(ItemBp.APD_SKUTYPE);
            }
            else {
                apdSku = vendorSku;
            }
        }

        if (!catalog.getVendor().getName().equals(vendorName)) {
            toReturn.getErrorSet().add(CatalogBp.SyncError.INCORRECT_VENDOR.getLabel());
            summary.incrementErrors();
        }

        if (StringUtils.isBlank(apdSku)) {
            toReturn.getErrorSet().add(CatalogBp.SyncError.NO_APD_SKU.getLabel());
            summary.incrementErrors();
        }

        toReturn.setResultApdSku(apdSku);
        toReturn.setResultVendor(vendorName);

        Item result = this.itemCacheManager.getVendorItem(apdSku, vendorName);
        //If the item doesn't exist, creates a new one
        if (result == null) {
            result = new Item();
            result.setName(BLANK);
            result.setDescription(BLANK);
            Sku newSku = new Sku();
            newSku.setItem(result);
            newSku.setType(skuTypeBp.getApdSkuType());
            newSku.setValue(apdSku);
            result.getSkus().add(newSku);
        }
        return result;
    }

    @Inject
    private CatalogXItemDao catalogXItemDao;

    @Inject
    private ItemClassificationTypeBp classTypeBp;

    /**
     * This method discontinues an Item
     * 
     * @param item
     */
    public void discontinue(Item item) {
        if (ItemStatus.DISCONTINUED.equals(item.getStatus())) {
            //already discontinued
            return;
        }
        item = setDiscontinuedDate((new Date()), item);
        item.setStatus(ItemStatus.DISCONTINUED);
        //if the item exists in the datamodel already, updates all customer items
        if (item.getId() != null) {
            //gets the list of customer items referring to this vendor item
            CatalogXItem searchCxi = new CatalogXItem();
            searchCxi.setItem(new Item());
            searchCxi.getItem().setId(item.getId());
            List<CatalogXItem> searchCxiList = catalogXItemDao.searchByExactExample(searchCxi, 0, 0);
            Item replacementItem = new Item();
            if (item.getReplacement() != null) {
                replacementItem = this.findById(item.getReplacement().getId(), Item.class);
            }
            //for each customer item
            for (CatalogXItem catalogXItem : searchCxiList) {
                CatalogXItem fetchedCatalogXItem = catalogXItemDao.findById(catalogXItem.getId(), CatalogXItem.class);
                //if the customer item can be replaced, and if a replacement exists, and if the pricing type isn't 
                //"Fixed Price", it is pointed to the replacement
                if (replacementItem.getId() != null
                        && replaceAllowedFromCatalogXItem(fetchedCatalogXItem)
                        && (fetchedCatalogXItem.getPricingType() == null || !fetchedCatalogXItem.getPricingType()
                                .getName().equals(FIXED_PRICE_PRICINGTYPE))) {
                    //if the replacement isn't in the customer catalog, creates it
                    if (replaceAllowedWithConstraints(replacementItem, fetchedCatalogXItem)) {
                        CatalogXItem replacement = new CatalogXItem();
                        replacement.setCatalog(fetchedCatalogXItem.getCatalog());
                        replacement.setItem(replacementItem);
                        replacement.setPricingType(fetchedCatalogXItem.getPricingType());
                        replacement.setPricingParameter(fetchedCatalogXItem.getPricingParameter());
                        Sku newCustomerSku = new Sku();
                        newCustomerSku.setValue(replacementItem.getSku(APD_SKUTYPE));
                        replacement.setCustomerSku(newCustomerSku);
                        try {
                            this.calculatePrice(replacement);
                            catalogXItemDao.create(replacement);
                        }
                        catch (PricingCalculationException e) {
                            LOG.debug("Price calculation exception when creating replacement customer item: "
                                    + replacementItem.getSku(APD_SKUTYPE));
                        }
                    }
                    //updates properties on original
                    this.setCatalogXItemProperty(fetchedCatalogXItem, CUSTOMER_REPLACEMENT_SKU_PROPERTYTYPE,
                            replacementItem.getSku(APD_SKUTYPE));
                    this.setCatalogXItemProperty(fetchedCatalogXItem, CUSTOMER_REPLACEMENT_VENDOR_PROPERTYTYPE,
                            replacementItem.getVendorCatalog().getVendor().getName());
                }
                catalogXItemDao.update(fetchedCatalogXItem);
            }
        }
    }

    /**
     * Takes a CatalogXItem, and returns true if the CatalogXItem allows replacement, based on its properties
     * 
     * @param catalogXItem
     * @return
     */
    private boolean replaceAllowedFromCatalogXItem(CatalogXItem catalogXItem) {
        for (CatalogXItemXItemPropertyType property : catalogXItem.getProperties()) {
            if (property.getType().getName().equals(PREVENT_REPLACEMENT_PROPERTYTYPE)
                    && property.getValue().equalsIgnoreCase(TRUE)) {
                return false;
            }
        }
        return true;
    }

    /**
     * Takes a CatalogXItem and a potential Item replacement, and determines whether making the replacement would violate 
     * database constraints. Specifically, the unique constraint on CATALOG_ID and ITEM_ID in the CATALOGXITEM table. That 
     * is, a vendor item can only occur once in a customer catalog.
     * 
     * @param replacement
     * @param catalogXItem
     * @return
     */
    private boolean replaceAllowedWithConstraints(Item replacement, CatalogXItem catalogXItem) {
        CatalogXItem constraintSearchItem = new CatalogXItem();
        constraintSearchItem.setCatalog(new Catalog());
        constraintSearchItem.setItem(new Item());
        constraintSearchItem.getCatalog().setId(catalogXItem.getCatalog().getId());
        constraintSearchItem.getItem().setId(replacement.getId());
        return catalogXItemDao.resultQuantity(constraintSearchItem) == 0;
    }

    private void setCatalogXItemProperty(CatalogXItem item, String propertyType, String value) {
        boolean foundProperty = false;
        for (CatalogXItemXItemPropertyType property : item.getProperties()) {
            if (property.getType().getName().equals(propertyType)) {
                property.setValue(value);
                foundProperty = true;
            }
        }
        if (!foundProperty) {
            ItemPropertyType availabilityType = propertyTypeBp.getPropertyType(propertyType);
            CatalogXItemXItemPropertyType newProperty = new CatalogXItemXItemPropertyType();
            newProperty.setType(availabilityType);
            newProperty.setValue(value);
            item.getProperties().add(newProperty);
        }
    }

    /**
     * This method un-discontinues an Item.
     * 
     * @param item
     */
    public void unDiscontinue(Item item) {
        //blanks out the current status
        item.setStatus(null);
        //creates a new status
        this.setItemStatus(item);
    }

    public void calculateStreetPrice(Item item) throws PricingCalculationException {
        ItemPropertyType streetPricePropertyType = propertyTypeBp.getPropertyType(STREET_PRICE_PROPERTYTYPE);
        this.calculateStreetPrice(item, streetPricePropertyType);
    }

    private void calculateStreetPrice(Item item, ItemPropertyType streetPricePropertyType)
            throws PricingCalculationException {
        BigDecimal price = pricingBp.calculatePrice(item, item.getVendorCatalog(), null, null, null).getPrice();
        for (ItemXItemPropertyType property : item.getPropertiesReadOnly()) {
            if (property.getType().getName().equals(STREET_PRICE_PROPERTYTYPE)) {
                property.setValue(price.toPlainString());
                item = addOrModifyItemProperty(item, property);
                return;
            }
        }
        if (streetPricePropertyType != null) {
            ItemXItemPropertyType newProperty = new ItemXItemPropertyType();
            newProperty.setType(streetPricePropertyType);
            newProperty.setValue(price.toPlainString());
            item = addOrModifyItemProperty(item, newProperty);
            return;
        }
    }

    public Item searchItem(String sku, String skuType, String vendorName) {
        return ((ItemDao) this.dao).searchItem(sku, skuType, vendorName);
    }

    /**
     * This is a utility method, it generates a transient entity with the APD sku and vendor name set. This 
     * uniquely identifies one item.
     * 
     * @param apdSku
     * @param vendorName
     * @return
     */
    public Item searchItem(String apdSku, String vendorName) {
        return searchItem(apdSku, APD_SKUTYPE, vendorName);
    }

    /**
     * Sets the AbilityOneSubsitute of the item.
     * 
     * @param skuOfOriginal
     * @param skuOfNew
     * @param vendor
     * @param result
     */
    public void setEtsItem(String skuOfOriginal, String skuOfNew, String vendor) throws ItemNotFoundException {
        List<Item> originalSearchList = this.searchByExactExample(searchItem(skuOfOriginal, vendor), 0, 0);
        List<Item> etsSearchList = this.searchByExactExample(searchItem(skuOfNew, vendor), 0, 0);
        if (originalSearchList.isEmpty() || etsSearchList.isEmpty()) {
            throw new ItemNotFoundException();
        }
        Item original = originalSearchList.get(0);
        Item etsItem = etsSearchList.get(0);
        original.setAbilityOneSubstitute(etsItem);
    }

    @Override
    public Item cloneEntity(Item t) {
        return this.writeItem(t, new Item());
    }

    /**
     * This method takes two Item objects, and writes the contents of the first one onto the second one, except for 
     * the APD sku and primary key, which are maintained.
     * <br /><br />
     * writeItem(new Item(), existingItem) clears all values from existingItem, except for the ID and APD sku. This 
     * is good for catalog replacements.
     * <br /><br />
     * writeItem(existingItem, new Item()) returns an item with all of the values of existingItem, except for the ID 
     * and APD sku. Used by the cloneEntity method.
     * 
     * @param original
     * @param toWrite
     * @return
     */
    public Item writeItem(Item original, Item toWrite) {
        toWrite.setHierarchyNode(original.getHierarchyNode());
        toWrite.setDescription(original.getDescription());
        toWrite.setItemCategory(original.getItemCategory());
        toWrite.setManufacturer(original.getManufacturer());
        toWrite.setName(original.getName());
        toWrite.setReplacement(original.getReplacement());
        toWrite.setVendorCatalog(original.getVendorCatalog());
        toWrite.setSearchTerms(original.getSearchTerms());
        toWrite.getCustomerCosts().removeAll(toWrite.getCustomerCosts());
        toWrite.setCustomerCosts(new HashSet<CustomerCost>());
        for (CustomerCost cost : original.getCustomerCosts()) {
            CustomerCost newCost = new CustomerCost();
            newCost.setCustomer(cost.getCustomer());
            newCost.setValue(cost.getValue());
            newCost.setItem(toWrite);
            toWrite.getCustomerCosts().add(newCost);
        }
        Iterator<Sku> skuIterator = toWrite.getSkus().iterator();
        while (skuIterator.hasNext()) {
            Sku sku = skuIterator.next();
            if (!sku.getType().getName().equals(APD_SKUTYPE)) {
                skuIterator.remove();
            }
        }
        for (Sku sku : original.getSkus()) {
            if (!sku.getType().getName().equals(APD_SKUTYPE)) {
                Sku newSku = new Sku();
                newSku.setType(sku.getType());
                newSku.setValue(sku.getValue());
                newSku.setItem(toWrite);
                toWrite.getSkus().add(newSku);
            }
        }
        toWrite.getProperties().removeAll(toWrite.getPropertiesReadOnly());
        for (ItemXItemPropertyType property : original.getPropertiesReadOnly()) {
            ItemXItemPropertyType newProperty = new ItemXItemPropertyType();
            newProperty.setType(property.getType());
            newProperty.setValue(property.getValue());
            toWrite = addOrModifyItemProperty(toWrite, newProperty);
        }
        return toWrite;
    }

    //This is where item property list modification should occur
    @SuppressWarnings("deprecation")
    public Item addOrModifyItemProperty(Item toModify, ItemXItemPropertyType property) {
        if (ItemBp.APD_COST_PROPERTYTYPE.equals(property.getType().getName())) {
            toModify = setCurrentApdCostToOldApdCost(toModify);
        }
        ItemXItemPropertyType currentProperty = toModify.getPropertyReadOnly(property.getType().getName());
        if (currentProperty != null) {
            //If the property exists on the item already, remove it to avoid a duplicate
            removeProperty(toModify, currentProperty);
        }
        toModify.getProperties().add(property);
        return toModify;
    }

    public List<Item> getCurrentCatalogItems(Long vendorId) {
        return ((ItemDao) this.dao).getCurrentCatalogItems(vendorId);
    }

    public ArrayList<Long> batchForJMS(Catalog catalog, int batch) {
        return ((ItemDao) this.dao).batchForJMS(catalog, batch);
    }

    public class ItemNotFoundException extends Exception {

        private static final long serialVersionUID = 2211212555758045254L;

    }

    public void refresh(Item item) {
        ((ItemDao) this.dao).refresh(item);
    }

    public void setRelationships(String vendorSku, String vendorName,
            MultivaluedMap<String, ItemRelationshipDto> relationshipData) {
        Item searchItem = this.searchItem(vendorSku, VENDOR_SKUTYPE, vendorName);
        List<Item> searchList = this.searchByExactExample(searchItem, 0, 0);
        if (searchList.isEmpty()) {
            return;
        }
        Item originalItem = this.findById(searchList.get(0).getId(), Item.class);
        for (String relationshipType : relationshipData.keySet()) {
            for (ItemRelationshipDto relationship : relationshipData.get(relationshipType)) {
                String relationshipSku = relationship.getSku();
                String relationshipVendorName = relationship.getVendorName();
                int relationshipPosition = relationship.getPosition();
                searchItem = this.searchItem(relationshipSku, VENDOR_SKUTYPE, relationshipVendorName);
                searchList = this.searchByExactExample(searchItem, 0, 0);
                if (!searchList.isEmpty()) {
                    addItemRelationship(originalItem, searchList.get(0), relationshipType, relationshipPosition);
                }
            }
        }
        this.update(originalItem);
    }

    public void setRelationships(Item originalItem, String relationshipType, String relationshipSku,
            String relationshipVendorName, int relationshipPosition) {

        Item searchItem = this.searchItem(relationshipSku, relationshipVendorName);
        List<Item> searchList = this.searchByExactExample(searchItem, 0, 0);
        if (!searchList.isEmpty()) {
            addItemRelationship(originalItem, searchList.get(0), relationshipType, relationshipPosition);
        }
        else {
            searchItem = this.searchItem(relationshipSku, VENDOR_SKUTYPE, relationshipVendorName);
            searchList = this.searchByExactExample(searchItem, 0, 0);
            if (!searchList.isEmpty()) {
                addItemRelationship(originalItem, searchList.get(0), relationshipType, relationshipPosition);
            }
        }

    }

    private void addItemRelationship(Item original, Item relative, String relationship, int position) {
        if (original.getSimilarItems() == null) {
            original.setSimilarItems(new HashSet<ItemRelationship>());
        }
        ItemRelationship newRelationship = new ItemRelationship();
        for (ItemRelationship testRelationship : original.getSimilarItems()) {
            if (testRelationship.getRelative().getId().equals(relative.getId())
                    && testRelationship.getRelationship().equals(relationship)) {
                newRelationship = testRelationship;
                break;
            }
        }
        newRelationship.setOriginal(original);
        newRelationship.setRelative(relative);
        newRelationship.setRelationship(relationship);
        newRelationship.setPosition(position);
        original.getSimilarItems().add(newRelationship);
    }

    public void setItemStatus(Item toSet) {
        if (ItemStatus.DISCONTINUED.equals(toSet.getStatus())) {
            return;
        }
        for (ItemXItemPropertyType property : toSet.getPropertiesReadOnly()) {
            if ((property.getType().getName().equals(SPECIAL_ORDER_PROPERTYTYPE) || property.getType().getName()
                    .equals(CUSTOM_ORDER_PROPERTYPYPE))
                    && property.getValue().equalsIgnoreCase(TRUE) && !toSet.getStatus().equals(ItemStatus.DISCONTINUED)) {
                toSet.setStatus(ItemStatus.CONTACT_REQUIRED);
                return;
            }
        }
        toSet.setStatus(ItemStatus.AVAILABLE);
    }

    public Item retrieveItem(ItemDto item, Catalog catalog) {
        String apdSku = BLANK;
        List<SkuDto> skuDtos = item.getSkus();
        for (SkuDto skuDto : skuDtos) {
            if (skuDto.getType().getName().equals(APD_SKUTYPE)) {
                apdSku = skuDto.getValue();
            }
        }
        List<Item> results = null;
        if (!StringUtils.isEmpty(apdSku)) {
            results = ((ItemDao) dao).searchByApdSku(apdSku, catalog);
        }
        if (results != null && !results.isEmpty()) {
            if (results.size() > 1) {
                LOG.error("More than one item in customer catalog: " + catalog.getName() + " with apd sku: " + apdSku);
            }
            return results.get(0);
        }
        else {
            return null;
        }
    }

    @Inject
    private CatalogDao catalogDao;

    /**
     * Calculates the price of a CatalogXItem
     * 
     * @param toCalculate
     * @throws PricingCalculationException 
     */
    public CatalogXItem calculatePrice(CatalogXItem toCalculate) throws PricingCalculationException {
        if (!this.catalogXItemDao.isManaged(toCalculate) && toCalculate.getId() != null) {
            toCalculate = catalogXItemDao.findById(toCalculate.getId(), CatalogXItem.class);
        }
        if (!this.dao.isManaged(toCalculate.getItem()) && toCalculate.getItem().getId() != null) {
            toCalculate.setItem(dao.findById(toCalculate.getItem().getId(), Item.class));
        }
        //don't send vendor item update messages if the item changes, only the customer item is updated
        toCalculate.getItem().setSendUpdateMessage(false);
        if (!this.catalogDao.isManaged(toCalculate.getCatalog()) && toCalculate.getCatalog().getId() != null) {
            toCalculate.setCatalog(catalogDao.findById(toCalculate.getCatalog().getId(), Catalog.class));
        }

        //gets the name of the catalog, to check if a customer-specific cost is applicable
        Long customerId = toCalculate.getCatalog().getCustomer().getId();
        if (toCalculate.getCatalog().getCustomer().getRootAccount() != null) {
            customerId = toCalculate.getCatalog().getCustomer().getRootAccount().getId();
        }
        //first, calculates the new price and profit
        PricingCalculationResult result = pricingBp.calculatePrice(toCalculate.getItem(), toCalculate.getCatalog(),
                customerId, toCalculate.getPricingType(), toCalculate.getPricingParameter());
        BigDecimal newPrice = result.getPrice();
        BigDecimal newProfit = result.getProfit();

        //then, compares the existing profit and price to the calculated profits and prices
        CatalogXItemXItemPropertyType currentProfitProperty = getCatalogXItemProperty(toCalculate,
                CURRENT_PROFIT_PROPERTYTYPE);

        if (toCalculate.getPrice() == null || StringUtils.isBlank(currentProfitProperty.getValue())
                || newPrice.compareTo(toCalculate.getPrice()) != 0
                || !newProfit.toString().equals(currentProfitProperty.getValue())) {

            //if there's a change in either, updates the price, profit, and last price change date
            CatalogXItemXItemPropertyType priceChangeProperty = getCatalogXItemProperty(toCalculate,
                    PRICE_CHANGE_DATE_PROPERTYTYPE);

            priceChangeProperty.setValue((new Date()).toString());

            CatalogXItemXItemPropertyType lastPriceProperty = getCatalogXItemProperty(toCalculate,
                    LAST_PRICE_PROPERTYTYPE);

            lastPriceProperty.setValue(toCalculate.getPrice() != null ? toCalculate.getPrice().toString()
                    : ZERO_DOLLARS_AND_CENTS);

            CatalogXItemXItemPropertyType lastProfitProperty = getCatalogXItemProperty(toCalculate,
                    LAST_PROFIT_PROPERTYTYPE);

            lastProfitProperty
                    .setValue(StringUtils.isNotBlank(currentProfitProperty.getValue()) ? currentProfitProperty
                            .getValue() : ZERO_DOLLARS_AND_CENTS);

            currentProfitProperty.setValue(newProfit.toString());

            toCalculate.setPrice(newPrice);
        }
        return toCalculate;
    }

    private CatalogXItemXItemPropertyType getCatalogXItemProperty(CatalogXItem item, String propertyType) {
        CatalogXItemXItemPropertyType toReturn = item.getProperty(propertyType);
        if (toReturn == null) {
            toReturn = new CatalogXItemXItemPropertyType();
            toReturn.setType(propertyTypeBp.getPropertyType(propertyType));
            item.getProperties().add(toReturn);
        }
        return toReturn;
    }

    public Item setDiscontinuedDate(Date discontinuedDate, Item item) {
        if (item.getPropertiesReadOnly() == null) {
            item.setProperties(new HashSet<ItemXItemPropertyType>());
        }
        ItemXItemPropertyType discontintuedDateProperty = item.getPropertyReadOnly(DISCONTINUED_DATE_TYPE);
        if (discontintuedDateProperty == null) {
            discontintuedDateProperty = new ItemXItemPropertyType();
            discontintuedDateProperty.setType(propertyTypeBp.getPropertyType(DISCONTINUED_DATE_TYPE));
        }
        discontintuedDateProperty.setValue(sdf.format(discontinuedDate));
        item = addOrModifyItemProperty(item, discontintuedDateProperty);
        return item;
    }

    //private method part of add property logic
    @SuppressWarnings("deprecation")
    private Item setCurrentApdCostToOldApdCost(Item item) {
        ItemXItemPropertyType apdCost = item.getPropertyReadOnly(APD_COST_PROPERTYTYPE);
        if (item.getId() == null || apdCost == null || StringUtils.isEmpty(apdCost.getValue())) {
            //Don't set last apd cost if there isn't one, or if the item hasn't been persisted yet
            return item;
        }
        else {
            ItemXItemPropertyType lastApdCost = item.getPropertyReadOnly(LAST_APD_COST);
            if (lastApdCost != null && !StringUtils.isEmpty(lastApdCost.getValue())) {
                item = removeProperty(item, lastApdCost);
            }
            lastApdCost = new ItemXItemPropertyType();
            ItemPropertyType propertyType = propertyTypeBp.getPropertyType(LAST_APD_COST);
            lastApdCost.setType(propertyType);
            //Grabs the current apdCost from the database in case the local object has already changed it
            lastApdCost.setValue(((ItemDao) this.dao).getCurrentApdCost(item));
            item.getProperties().add(lastApdCost);
            return item;
        }
    }

    //Propert way to remove an item property from an item
    @SuppressWarnings("deprecation")
    public Item removeProperty(Item item, ItemXItemPropertyType property) {
        item.getProperties().remove(property);
        return item;

    }

    public class NoVendorCatalogSpecifiedException extends RuntimeException {

    }

    public Item hydrateItem(Item item) {
        return ((ItemDao) this.dao).hydrateItem(item);
    }

}
