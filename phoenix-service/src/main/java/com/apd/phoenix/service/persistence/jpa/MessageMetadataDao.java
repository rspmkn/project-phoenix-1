package com.apd.phoenix.service.persistence.jpa;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.Query;
import com.apd.phoenix.service.model.CustomerOrder;
import com.apd.phoenix.service.model.MessageMetadata;
import com.apd.phoenix.service.model.MessageMetadata.MessageType;
import com.apd.phoenix.service.model.OrderLog.EventType;

@Stateless
@LocalBean
public class MessageMetadataDao extends AbstractDao<MessageMetadata> {

    @SuppressWarnings("unchecked")
    public List<CustomerOrder> getCustomerOrdersPlacedInLastYearByTransaction(String transactionId, String senderId) {
        String hql = "SELECT customerOrder FROM OrderLog AS orderLog "
                + "LEFT JOIN orderLog.messageMetadata AS messageMetadata "
                + "LEFT JOIN orderLog.customerOrder AS customerOrder "
                + "WHERE messageMetadata.transactionId=:transactionId AND messageMetadata.senderId=:senderId AND "
                + "customerOrder.orderDate > :oneYearAgo";
        Query query = entityManager.createQuery(hql);
        query.setParameter(("transactionId"), transactionId);
        query.setParameter(("senderId"), senderId);
        //Edi transaction numbers will repeat every few years, so only look for orders placed within the last year
        Calendar oneYearAgo = Calendar.getInstance();
        oneYearAgo.add(Calendar.YEAR, -1);
        Date oneYearAgoDate = oneYearAgo.getTime();
        query.setParameter("oneYearAgo", oneYearAgoDate);
        List<CustomerOrder> results = query.getResultList();
        if (results.isEmpty()) {
            return null;
        }
        return results;
    }

    @SuppressWarnings("unchecked")
    public CustomerOrder getCOByTransactionAndInterchangeId(String transactionId, String senderId, String interchangeId) {
        String hql = "SELECT customerOrder FROM OrderLog AS orderLog "
                + "LEFT JOIN orderLog.messageMetadata AS messageMetadata "
                + "LEFT JOIN orderLog.customerOrder AS customerOrder "
                + "WHERE messageMetadata.transactionId=:transactionId AND messageMetadata.interchangeId=:interchangeId AND messageMetadata.senderId=:senderId";
        Query query = entityManager.createQuery(hql);
        query.setParameter(("transactionId"), transactionId);
        query.setParameter("interchangeId", interchangeId);
        query.setParameter(("senderId"), senderId);
        List<CustomerOrder> results = query.getResultList();
        if (results.isEmpty()) {
            return null;
        }
        return results.get(0);
    }

    @SuppressWarnings("unchecked")
    public MessageMetadata getMessageMetadataByTransactionAndSenderIdAndApdPo(String transactionId, String senderId,
            String apdPo) {
        String hql = "SELECT messageMetadata FROM OrderLog AS orderLog "
                + "LEFT JOIN orderLog.messageMetadata AS messageMetadata "
                + "LEFT JOIN orderLog.customerOrder AS customerOrder "
                + "LEFT JOIN customerOrder.poNumbers AS poNumber "
                + "WHERE messageMetadata.transactionId=:transactionId AND poNumber.value=:apdPo AND messageMetadata.senderId=:senderId";
        Query query = entityManager.createQuery(hql);
        query.setParameter(("transactionId"), transactionId);
        query.setParameter("apdPo", apdPo);
        query.setParameter(("senderId"), senderId);
        List<MessageMetadata> results = query.getResultList();
        if (results.isEmpty()) {
            return null;
        }
        return results.get(0);
    }

    @SuppressWarnings("unchecked")
    public List<MessageMetadata> getMessageExchangeByOrderNumberAndEvent(String apdPoNumber, EventType eventType,
            MessageType messageType, boolean returnMostRecentExchangeOnly) {
        String hql = "SELECT messageMetadata FROM OrderLog AS orderLog "
                + "JOIN orderLog.messageMetadata AS messageMetadata " + "JOIN orderLog.customerOrder AS customerOrder "
                + "JOIN customerOrder.poNumbers AS poNumber "
                + "WHERE poNumber.value=:apdPo AND orderLog.eventType=:eventType ";

        if (messageType != null) {
            hql += "AND messageMetadata.messageType=:messageType ";
        }
        if (returnMostRecentExchangeOnly) {
            hql += "ORDER BY messageMetadata.interchangeId desc";
        }
        Query query = entityManager.createQuery(hql);
        query.setParameter(("apdPo"), apdPoNumber);
        query.setParameter(("eventType"), eventType);
        if (messageType != null) {
            query.setParameter("messageType", messageType);
        }
        if (returnMostRecentExchangeOnly) {
            query.setMaxResults(1);
        }
        return query.getResultList();
    }

    public MessageMetadata getLatestMessageExchangeByOrderNumberAndEvent(String apdPoNumber, EventType eventType,
            MessageType messageType) {
        List<MessageMetadata> results = getMessageExchangeByOrderNumberAndEvent(apdPoNumber, eventType, messageType,
                true);
        if (results.isEmpty()) {
            return null;
        }
        return results.get(0);
    }

    public List<CustomerOrder> getCustomerOrdersPlacedInLastYearByGroupControlNumber(String groupControlNumber,
            String senderId) {
        String hql = "SELECT customerOrder FROM OrderLog AS orderLog "
                + "LEFT JOIN orderLog.messageMetadata AS messageMetadata "
                + "LEFT JOIN orderLog.customerOrder AS customerOrder "
                + "WHERE messageMetadata.groupId=:groupControlNumber AND messageMetadata.senderId=:senderId AND "
                + "customerOrder.orderDate > :oneYearAgo";
        Query query = entityManager.createQuery(hql);
        query.setParameter(("groupControlNumber"), groupControlNumber);
        query.setParameter(("senderId"), senderId);
        //Edi control numbers will repeat every few years, so only look for orders placed within the last year
        Calendar oneYearAgo = Calendar.getInstance();
        oneYearAgo.add(Calendar.YEAR, -1);
        Date oneYearAgoDate = oneYearAgo.getTime();
        query.setParameter("oneYearAgo", oneYearAgoDate);
        List<CustomerOrder> results = query.getResultList();
        if (results.isEmpty()) {
            return null;
        }
        return results;
    }
}
