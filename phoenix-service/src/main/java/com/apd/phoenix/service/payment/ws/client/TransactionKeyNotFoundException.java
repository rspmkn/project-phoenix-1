/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.service.payment.ws.client;

public class TransactionKeyNotFoundException extends Exception {

    public TransactionKeyNotFoundException(String message) {
        super(message);
    }

    public TransactionKeyNotFoundException(String message, Throwable throwable) {
        super(message, throwable);
    }
}