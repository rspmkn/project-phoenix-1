package com.apd.phoenix.service.business;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.jms.JMSException;
import javax.naming.NamingException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.core.utility.TenantConfigRepository;
import com.apd.phoenix.service.business.CatalogBp.SyncAction;
import com.apd.phoenix.service.catalog.CatalogQueueCount;
import com.apd.phoenix.service.model.Account;
import com.apd.phoenix.service.model.Catalog;
import com.apd.phoenix.service.model.CatalogXCategoryXPricingType;
import com.apd.phoenix.service.model.CatalogXItem;
import com.apd.phoenix.service.model.ItemCategory;
import com.apd.phoenix.service.model.Vendor;
import com.apd.phoenix.service.model.listener.CatalogXItemListener;
import com.apd.phoenix.service.persistence.jpa.CatalogXCategoryXPricingTypeDao;
import com.apd.phoenix.service.persistence.multitenancy.impl.CurrentTenantIdentifierResolverImpl;

@Stateless
@LocalBean
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
public class CatalogBpLoadTest extends AbstractBp<Catalog> {

    private static final Logger LOGGER = LoggerFactory.getLogger(CatalogBpLoadTest.class);

    //determines how often to check queues while waiting for processing to complete.
    //lower frequency will result in more accurate time estimates, but will slightly
    //increase load on brokers
    private static final long CHECK_FREQUENCY_SECONDS = 2l;

    private static final long INITIAL_WAIT_SECONDS = 10l;

    private static final String VENDOR_CATALOG_NAME = "Load Test Vendor Catalog";

    private static final String CUSTOMER_CATALOG_NAME = "Load Test Customer Catalog";

    private static final String VENDOR_NAME = "Load Test Vendor";

    private static final String CUSTOMER_NAME = "Load Test Customer";

    private static final String DO_NOT_USE_PREFIX = "[DO NOT USE]";

    @Inject
    private CatalogBp catalogBp;

    @Inject
    private CatalogBpNoTransaction catalogBpNoTransaction;

    @Inject
    private VendorBp vendorBp;

    @Inject
    private AccountBp accountBp;

    @Inject
    private ItemCategoryBp categoryBp;

    @Inject
    private PricingTypeBp pricingTypeBp;

    @Inject
    private CatalogXCategoryXPricingTypeDao categoryXPricingDao;

    @Inject
    private CatalogXItemBp catalogXItemBp;

    @Inject
    private CatalogQueueCount queueCount;

    /**
     * This method runs some load test CSVs, then removes the generated catalogs. This allows a user
     * to perform replicatable tests of catalog processing performance.
     */
    public long loadTest() {

        if ("true".equals(TenantConfigRepository.getInstance().getProperty("project.state", "isProductionEnvironment"))) {
            LOGGER.warn("Load tests should only be performed in lower environments, skipping test!");
            return 0l;
        }

        LOGGER.info("Setting up load test");
        this.cleanup();
        this.waitUntilProcessingComplete();

        LOGGER.info("Beginning load test");
        long startTimeMillis = System.currentTimeMillis();

        Vendor testVendor = new Vendor();
        testVendor.setName(VENDOR_NAME);
        testVendor.setCommunicationDestination("test");
        Account testCustomer = new Account();
        testCustomer.setName(CUSTOMER_NAME);
        testCustomer.setSolomonCustomerId("test");
        testCustomer.setUseLocationCode(false);
        testVendor = vendorBp.create(testVendor);
        testCustomer = accountBp.create(testCustomer);
        Catalog testVendorCatalog = new Catalog();
        testVendorCatalog.setVendor(testVendor);
        testVendorCatalog.setName(VENDOR_CATALOG_NAME);
        testVendorCatalog = catalogBp.update(testVendorCatalog);
        for (ItemCategory category : categoryBp.findAll(ItemCategory.class, 0, 0)) {
            CatalogXCategoryXPricingType newPricing = new CatalogXCategoryXPricingType();
            newPricing.setCatalog(testVendorCatalog);
            newPricing.setCategory(category);
            newPricing.setType(pricingTypeBp.getTypeByName(PricingTypeBp.COST_PLUS));
            newPricing.setParameter("25%");
            categoryXPricingDao.create(newPricing);
        }
        Catalog testCustomerCatalog = new Catalog();
        testCustomerCatalog.setCustomer(testCustomer);
        testCustomerCatalog.setName(CUSTOMER_CATALOG_NAME);
        testCustomerCatalog = catalogBp.update(testCustomerCatalog);
        for (ItemCategory category : categoryBp.findAll(ItemCategory.class, 0, 0)) {
            CatalogXCategoryXPricingType newPricing = new CatalogXCategoryXPricingType();
            newPricing.setCatalog(testCustomerCatalog);
            newPricing.setCategory(category);
            newPricing.setType(pricingTypeBp.getTypeByName(PricingTypeBp.COST_PLUS));
            newPricing.setParameter("25%");
            categoryXPricingDao.create(newPricing);
        }

        try {
            this.catalogBp.setTempCsv(catalogBp.getSpecifiedCsv("vendorLoadTest.csv"), "0", SyncAction.NEW);
            this.catalogBp.setDiffCsv("0", testVendorCatalog, SyncAction.NEW);
            this.catalogBp.setTempCsv(catalogBp.getSpecifiedCsv("customerLoadTest.csv"), "1", SyncAction.NEW);
            this.catalogBp.setDiffCsv("1", testCustomerCatalog, SyncAction.NEW);
        }
        catch (IOException e) {
            LOGGER.error("Error", e);
        }

        catalogBpNoTransaction.changeCatalog(testVendorCatalog, true, true);
        this.waitUntilProcessingComplete();
        catalogBpNoTransaction.changeCatalog(testCustomerCatalog, true, true);
        this.waitUntilProcessingComplete();
        catalogBp.setCatalogCsv(testCustomerCatalog, CurrentTenantIdentifierResolverImpl.getCurrentTenant());
        this.waitUntilProcessingComplete();
        this.catalogBp.reindexCatalog(testVendorCatalog, CurrentTenantIdentifierResolverImpl.getCurrentTenant());
        this.waitUntilProcessingComplete();

        long testTime = System.currentTimeMillis() - startTimeMillis;
        LOGGER.info("Load test complete, total time (in millis): " + testTime);

        this.cleanup();

        LOGGER.info("Done");

        return testTime;
    }

    /**
     * Waits until catalogs are done processing. Note that this implementation can result in 
     * inexact time measurements (checks occur every CHECK_FREQUENCY_SECONDS seconds).
     * 
     * @param testVendorCatalog
     * @param testCustomerCatalog
     */
    private void waitUntilProcessingComplete() {
        try {
            Thread.sleep(INITIAL_WAIT_SECONDS * 1000l);
        }
        catch (InterruptedException e) {
            LOGGER.error("Error", e);
        }
        boolean isComplete = false;
        do {
            try {
                Thread.sleep(CHECK_FREQUENCY_SECONDS * 1000l);
            }
            catch (InterruptedException e) {
                LOGGER.error("Error", e);
            }
            isComplete = queueCount.getCatalogProcessedCount() == 0 && queueCount.getCatalogUnprocessedCount() == 0
                    && queueCount.getAtomicUpdateCount() == 0;
        }
        while (!isComplete);
    }

    private void cleanup() {
        LOGGER.info("Cleaning up, this can take some time");

        Catalog searchCatalog = new Catalog();
        searchCatalog.setName(DO_NOT_USE_PREFIX + CUSTOMER_CATALOG_NAME);
        //inexact search for catalogs run previously - they will have slightly
        //different names because of the appended "previousRuns" value
        List<Catalog> searchList = catalogBp.searchByExample(searchCatalog, 0, 0);
        int previousRuns = searchList != null ? searchList.size() : 0;

        searchCatalog = new Catalog();
        searchCatalog.setName(CUSTOMER_CATALOG_NAME);
        searchList = catalogBp.searchByExactExample(searchCatalog, 0, 0);
        if (searchList != null && !searchList.isEmpty()) {
            this.disableCatalog(searchList.get(0), previousRuns);
        }
        searchCatalog = new Catalog();
        searchCatalog.setName(VENDOR_CATALOG_NAME);
        searchList = catalogBp.searchByExactExample(searchCatalog, 0, 0);
        if (searchList != null && !searchList.isEmpty()) {
            this.disableCatalog(searchList.get(0), previousRuns);
        }
        Account testCustomer = accountBp.findByName(CUSTOMER_NAME);
        Vendor testVendor = vendorBp.findByName(VENDOR_NAME);
        if (testCustomer != null) {
            testCustomer.setName(DO_NOT_USE_PREFIX + CUSTOMER_NAME + " " + previousRuns);
            accountBp.update(testCustomer);
        }
        if (testVendor != null) {
            testVendor.setName(DO_NOT_USE_PREFIX + VENDOR_NAME + " " + previousRuns);
            testVendor.setExpirationDate(new Date());
            vendorBp.update(testVendor);
        }
    }

    /**
     * This method takes a catalog, and disables it.
     * 
     * @param catalog
     */
    private void disableCatalog(Catalog catalog, int previousRuns) {
        catalog.setName(DO_NOT_USE_PREFIX + catalog.getName() + " " + previousRuns);
        catalog.setExpirationDate(new Date());
        catalog = catalogBp.update(catalog);
        //delete customer items in Solr
        int batchSize = 1;
        int batch = 0;
        while (batchSize > 0) {
            List<CatalogXItem> batchList = catalogXItemBp.batchForPrice(catalog, batch);
            batchSize = batchList.size();
            for (CatalogXItem item : batchList) {
            	try {
					CatalogXItemListener.addDeleteSolrDocument(item);
				} catch (JMSException | NamingException e) {
					LOGGER.error("Exception", e);
				}
            }
            batch++;
        }
    }
}
