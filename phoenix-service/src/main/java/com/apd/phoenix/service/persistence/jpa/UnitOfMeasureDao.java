package com.apd.phoenix.service.persistence.jpa;

import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.Query;
import org.apache.commons.lang.StringUtils;
import com.apd.phoenix.service.model.LineItemStatus;
import com.apd.phoenix.service.model.UnitOfMeasure;

/**
 * UnitOfMeasure DAO stub
 * 
 * @author RHC
 *
 */
@Stateless
@LocalBean
public class UnitOfMeasureDao extends AbstractDao<UnitOfMeasure> {

    public UnitOfMeasure getByName(String name) {
        // Confirm search string is not blank
        if (StringUtils.isBlank(name)) {
            return null;
        }
        Query query = entityManager.createQuery("SELECT uom FROM UnitOfMeasure AS uom WHERE uom.name=:name");
        query.setParameter("name", name);

        return getSingleResultOrNull(query);
    }
}
