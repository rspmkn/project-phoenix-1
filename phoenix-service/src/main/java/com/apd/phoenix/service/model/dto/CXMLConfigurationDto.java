/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.service.model.dto;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author nreidelb
 */
public class CXMLConfigurationDto implements Serializable{
    private static final long serialVersionUID = 3619523902269499L;
    private String name;
    private String senderSharedSecret;
    private Set<CXMLCredentialDto> senderCredentials = new HashSet<>();
    private Set<CXMLCredentialDto> fromCredentials = new HashSet<>();
    private Set<CXMLCredentialDto> toCredentials = new HashSet<>();

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the senderSharedSecret
     */
    public String getSenderSharedSecret() {
        return senderSharedSecret;
    }

    /**
     * @param senderSharedSecret the senderSharedSecret to set
     */
    public void setSenderSharedSecret(String senderSharedSecret) {
        this.senderSharedSecret = senderSharedSecret;
    }

	public Set<CXMLCredentialDto> getSenderCredentials() {
		return senderCredentials;
	}

	public void setSenderCredentials(Set<CXMLCredentialDto> senderCredentials) {
		this.senderCredentials = senderCredentials;
	}

	public Set<CXMLCredentialDto> getFromCredentials() {
		return fromCredentials;
	}

	public void setFromCredentials(Set<CXMLCredentialDto> fromCredentials) {
		this.fromCredentials = fromCredentials;
	}

	public Set<CXMLCredentialDto> getToCredentials() {
		return toCredentials;
	}

	public void setToCredentials(Set<CXMLCredentialDto> toCredentials) {
		this.toCredentials = toCredentials;
	}


}
