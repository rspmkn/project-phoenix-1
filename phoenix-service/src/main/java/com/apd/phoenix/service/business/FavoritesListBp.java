package com.apd.phoenix.service.business;

import com.apd.phoenix.service.model.CatalogXItem;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import com.apd.phoenix.service.model.FavoritesList;
import com.apd.phoenix.service.persistence.jpa.FavoritesListDao;
import java.util.List;
import java.util.Set;

@Stateless
@LocalBean
public class FavoritesListBp extends AbstractBp<FavoritesList> {

    /**
     * This method is injected with the correct Dao for initialization
     *
     * @param dao
     */
    @Inject
    public void initDao(FavoritesListDao dao) {
        this.dao = dao;
    }

    public Set<CatalogXItem> getInitializedSet(Long id) {
        return ((FavoritesListDao) dao).getInitializedSet(id);
    }

    public Set<CatalogXItem> getInitializedSet(Long id, int start, int quantity) {
        return ((FavoritesListDao) dao).getInitializedSet(id, start, quantity);
    }

    public int getListItemCount(Long id) {
        return ((FavoritesListDao) dao).getListItemCount(id);
    }

    public FavoritesList getHydratedFavoritesList(FavoritesList list) {
        return ((FavoritesListDao) dao).getHydratedFavoritesList(list);
    }

    public void removeEmptyCompanyLists(List<Long> catalogIds) {
        for (Long catalogId : catalogIds) {
            ((FavoritesListDao) dao).removeEmptyCompanyLists(catalogId);
        }
    }
}
