package com.apd.phoenix.service.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Version;
import org.hibernate.annotations.Index;
import com.apd.phoenix.service.business.CatalogBp.SyncAction;

@Entity
public class CatalogUpload {

    @Index(name = "cu_corrId")
    private String correlationId;
    private Long catalogId;
    private Long oldCatalogId;
    private Long totalItems;
    private Long processedItems;
    private boolean completed = false;

    private SyncAction action;
    private Boolean persistChanges;

    /** The id. */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id = null;

    /** The version. */
    @Version
    @Column(name = "version", nullable = false)
    private int version = 0;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public String getCorrelationId() {
        return correlationId;
    }

    public void setCorrelationId(String correlationId) {
        this.correlationId = correlationId;
    }

    public Long getCatalogId() {
        return catalogId;
    }

    public void setCatalogId(Long catalogId) {
        this.catalogId = catalogId;
    }

    public Long getTotalItems() {
        return totalItems;
    }

    public void setTotalItems(Long totalItems) {
        this.totalItems = totalItems;
    }

    public Long getProcessedItems() {
        return processedItems;
    }

    public void setProcessedItems(Long processedItems) {
        this.processedItems = processedItems;
    }

    public Long getOldCatalogId() {
        return oldCatalogId;
    }

    public void setOldCatalogId(Long oldCatalogId) {
        this.oldCatalogId = oldCatalogId;
    }

    public SyncAction getAction() {
        return action;
    }

    public void setAction(SyncAction action) {
        this.action = action;
    }

    public Boolean getPersistChanges() {
        return persistChanges;
    }

    public void setPersistChanges(Boolean persistChanges) {
        this.persistChanges = persistChanges;
    }

    public boolean isCompleted() {
        return completed;
    }

    public void setCompleted(boolean completed) {
        this.completed = completed;
    }
}
