/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.service.model.dto;

import java.io.Serializable;
import java.util.List;

/**
 *
 * @author nreidelb
 */
public class ManifestDto implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 6391980550809752851L;
    private List<StopDto> stops;
    private RouteDto routeDto;

    /**
     * @return the routeDto
     */
    public RouteDto getRouteDto() {
        return routeDto;
    }

    /**
     * @param routeDto the routeDto to set
     */
    public void setRouteDto(RouteDto routeDto) {
        this.routeDto = routeDto;
    }

    /**
     * @return the stops
     */
    public List<StopDto> getStops() {
        return stops;
    }

    /**
     * @param stops the stops to set
     */
    public void setStops(List<StopDto> stops) {
        this.stops = stops;
    }
}
