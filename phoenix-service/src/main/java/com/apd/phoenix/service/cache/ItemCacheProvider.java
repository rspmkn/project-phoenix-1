package com.apd.phoenix.service.cache;

import javax.annotation.PreDestroy;
import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.enterprise.context.ApplicationScoped;
import com.apd.phoenix.service.product.ItemKey;
import com.apd.phoenix.service.product.ItemValue;

/**
 * This class extends AbstractCacheProvider, using ProductKey and Product as the Key,Value pair.
 * 
 * @author RHC
 *
 */
@ApplicationScoped
@Lock(LockType.READ)
public class ItemCacheProvider extends AbstractCacheProvider<ItemKey, ItemValue> {

    private static final String ITEM_CACHE_NAME = cacheProperties.getProperty("itemCache.name");

    @Override
    protected String getCacheName() {
        return ITEM_CACHE_NAME;
    }

    @PreDestroy
    public void cleanup() {
        super.cleanup();
    }

}
