package com.apd.phoenix.service.persistence.jpa;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.Query;
import javax.persistence.TemporalType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.model.CredentialPropertyType.CredentialPropertyTypeEnum;
import com.apd.phoenix.service.model.CustomerCreditInvoice;
import com.apd.phoenix.service.model.CustomerInvoice;
import com.apd.phoenix.service.model.Invoice;

/**
 * Session Bean implementation class InvoiceDao
 */
@Stateless
@LocalBean
public class InvoiceDao extends AbstractDao<Invoice> {

    private static final Logger LOGGER = LoggerFactory.getLogger(InvoiceDao.class);

    public List<CustomerInvoice> findYesterdaysMarquetteInvoices() {
        String hql = "SELECT i FROM CustomerInvoice as i LEFT JOIN  i.shipment.order.credential as c where i.createdDate >= :dayStart AND i.createdDate <= :dayEnd "
                + "AND (0 < (SELECT count(*) FROM c.properties as p WHERE (p.type.name = :marquetteCredentialTypeConstant"
                + " OR p.type.name = :marquetteAPDCredentialTypeConstant) AND p.value = 'yes'))";
        Query query = entityManager.createQuery(hql);
        Calendar dayStart = Calendar.getInstance();
        dayStart.add(Calendar.DATE, -1);
        dayStart.set(Calendar.HOUR_OF_DAY, 0);
        dayStart.set(Calendar.MINUTE, 0);
        Calendar dayEnd = Calendar.getInstance();
        dayEnd.set(Calendar.HOUR_OF_DAY, 0);
        dayEnd.set(Calendar.MINUTE, 0);
        query.setParameter("dayStart", dayStart.getTime(), TemporalType.DATE);
        query.setParameter("dayEnd", dayEnd.getTime(), TemporalType.DATE);
        query.setParameter("marquetteCredentialTypeConstant", CredentialPropertyTypeEnum.ORDERS_PLACED_UNDER_MARQUETTE
                .getValue());
        query.setParameter("marquetteAPDCredentialTypeConstant",
                CredentialPropertyTypeEnum.ORDERS_PLACED_UNDER_APD_MARQUETTE.getValue());
        LOGGER.info("Finding Invoices Between " + dayStart.getTime() + " and " + dayEnd.getTime());
        List<CustomerInvoice> invoices = (List<CustomerInvoice>) query.getResultList();
        if (invoices == null) {
            return new ArrayList<CustomerInvoice>();
        }
        return invoices;
    }

    public List<CustomerCreditInvoice> findYesterdaysMarquetteCreditInvoices() {
        String hql = "SELECT i FROM CustomerCreditInvoice as i LEFT JOIN  i.returnOrder.order.credential as c where i.createdDate >= :dayStart AND i.createdDate <= :dayEnd "
                + "AND (0 < (SELECT count(*) FROM c.properties as p WHERE (p.type.name = :marquetteCredentialTypeConstant"
                + " OR p.type.name = :marquetteAPDCredentialTypeConstant) AND p.value = 'yes'))";
        Query query = entityManager.createQuery(hql);
        Calendar dayStart = Calendar.getInstance();
        dayStart.add(Calendar.DATE, -1);
        dayStart.set(Calendar.HOUR_OF_DAY, 0);
        dayStart.set(Calendar.MINUTE, 0);
        Calendar dayEnd = Calendar.getInstance();
        dayEnd.set(Calendar.HOUR_OF_DAY, 0);
        dayEnd.set(Calendar.MINUTE, 0);
        query.setParameter("dayStart", dayStart.getTime(), TemporalType.DATE);
        query.setParameter("dayEnd", dayEnd.getTime(), TemporalType.DATE);
        query.setParameter("marquetteCredentialTypeConstant", CredentialPropertyTypeEnum.ORDERS_PLACED_UNDER_MARQUETTE
                .getValue());
        query.setParameter("marquetteAPDCredentialTypeConstant",
                CredentialPropertyTypeEnum.ORDERS_PLACED_UNDER_APD_MARQUETTE.getValue());
        LOGGER.info("Finding Invoices Between " + dayStart.getTime() + " and " + dayEnd.getTime());
        List<CustomerCreditInvoice> invoices = (List<CustomerCreditInvoice>) query.getResultList();
        if (invoices == null) {
            return new ArrayList<CustomerCreditInvoice>();
        }
        return invoices;
    }

    public List<Invoice> findCustomerInvoiceByInvoiceNumber(String invoiceNumber) {
        String hql = "SELECT i FROM Invoice as i WHERE i.invoiceNumber=:invoiceNumber";
        Query query = entityManager.createQuery(hql);
        query.setParameter("invoiceNumber", invoiceNumber);
        return (List<Invoice>) query.getResultList();
    }

}