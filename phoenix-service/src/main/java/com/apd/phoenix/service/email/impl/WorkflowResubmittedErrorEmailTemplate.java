/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.service.email.impl;

import com.apd.phoenix.service.email.api.Attachment;
import com.apd.phoenix.service.email.api.EmailTemplate;
import com.apd.phoenix.service.email.api.NoAttachmentContentException;
import freemarker.template.TemplateException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author RHC
 */
public class WorkflowResubmittedErrorEmailTemplate extends EmailTemplate<String> {

    private static final String TEMPLATE = "workflow.resubmitted.error";

    @Override
    public String createBody(String apdPo) throws IOException, TemplateException {
         Map<String, Object> params = new HashMap<>();
         params.put("apdPo", apdPo);
         return this.create(params);
    }

    @Override
    public String getTemplate() {
        return TEMPLATE;
    }

    @Override
    public Attachment createAttachment(String content) throws NoAttachmentContentException {
        return null;
    }
}
