package com.apd.phoenix.service.business;

import java.sql.Date;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import com.apd.phoenix.service.model.RoleXUser;
import com.apd.phoenix.service.persistence.jpa.RoleXUserDao;

@Stateless
@LocalBean
public class RoleXUserBp extends AbstractBp<RoleXUser> {

    /**
     * This method is injected with the correct Dao for initialization
     * 
     * @param dao
     */
    @Inject
    public void initDao(RoleXUserDao dao) {
        this.dao = dao;
    }

    /**
     * Assigns a start date between a User and Role.
     * 
     * @param roleXUser
     * @param date
     */
    public void assignStartDate(RoleXUser roleXUser, Date date) {
        roleXUser.setStartDate(date);
        this.dao.update(roleXUser);
    }

    /**
     * Assigns a end date between a User and Role.
     * 
     * @param roleXUser
     * @param date
     */
    public void assignEndDate(RoleXUser roleXUser, Date date) {
        roleXUser.setStartDate(date);
        this.dao.update(roleXUser);
    }

    /**
     * Assigns a Role to a User.
     * 
     * @param roleXUser
     */
    public void assignRoleToUser(RoleXUser roleXUser) {
        this.dao.create(roleXUser);
    }

    /**
     * Removes a Role from a User.
     * 
     * @param roleXUser
     */
    public void removeRoleFromUser(RoleXUser roleXUser) {
        this.dao.delete(roleXUser.getId(), RoleXUser.class);
    }
}
