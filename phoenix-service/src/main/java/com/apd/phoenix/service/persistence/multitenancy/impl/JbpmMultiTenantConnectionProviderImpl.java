package com.apd.phoenix.service.persistence.multitenancy.impl;

public class JbpmMultiTenantConnectionProviderImpl extends AbstractMultiTenantConnectionProviderImpl {

    /**
     * 
     */
    private static final long serialVersionUID = 4103441658879091639L;
    private static final String dataSourceJNDIName = "java:jboss/datasources/jbpmDS";

    @Override
    protected String getDataSourceJNDIName() {
        return dataSourceJNDIName;
    }

}
