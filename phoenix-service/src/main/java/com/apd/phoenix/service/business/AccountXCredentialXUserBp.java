package com.apd.phoenix.service.business;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import com.apd.phoenix.service.persistence.jpa.AccountXCredentialXUserDao;
import com.apd.phoenix.service.model.AccountXCredentialXUser;
import com.apd.phoenix.service.model.Bulletin;
import com.apd.phoenix.service.model.FavoritesList;
import com.apd.phoenix.service.model.SystemUser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class provides business process methods for AccountXCredentialXUsers.
 *
 * @author RHC
 *
 */
@Stateless
@LocalBean
public class AccountXCredentialXUserBp extends AbstractBp<AccountXCredentialXUser> {

    private static final Logger logger = LoggerFactory.getLogger(AccountXCredentialXUserBp.class);

    /**
     * This method is injected with the correct Dao for initialization
     *
     * @param dao
     */
    @Inject
    public void initDao(AccountXCredentialXUserDao dao) {
        this.dao = dao;
    }

    /**
     * Given an AccountXCredentialXUser entity, this method returns all
     * bulletins associated with that Account, Credential, and User that should
     * be displayed.
     *
     * @param input - the AccountXCredentialXUser entity to get bulletins for
     * @return A Set of bulletins that should be displayed.
     */
    @SuppressWarnings("static-method")
    public Set<Bulletin> getBulletins(AccountXCredentialXUser input) {
        Set<Bulletin> toReturn = new HashSet<Bulletin>();
        Date currentDate = new Date();

        if (input.getUser() != null) {
            for (Bulletin b : input.getUser().getBulletins()) {
                if (currentDate.after(b.getExpirationDate()) && !toReturn.contains(b)) {
                    toReturn.add(b);
                }
            }
        }

        if (input.getCredential() != null) {
            for (Bulletin b : input.getCredential().getBulletins()) {
                if (currentDate.after(b.getExpirationDate()) && !toReturn.contains(b)) {
                    toReturn.add(b);
                }
            }
        }

        if (input.getAccount() != null) {
            for (Bulletin b : input.getAccount().getBulletins()) {
                if (currentDate.after(b.getExpirationDate()) && !toReturn.contains(b)) {
                    toReturn.add(b);
                }
            }
        }

        return toReturn;
    }

    /**
     * Takes a user, and returns all of the accountXCredentialXUsers for that
     * user. The only value that is fetched on axcxu is the credential, which is
     * displayed in the dropdown.
     *
     * @param user
     * @return
     */
    public List<AccountXCredentialXUser> getAllAXCXU(SystemUser user) {
        return ((AccountXCredentialXUserDao) dao).getAllAXCXU(user);
    }

    public List<AccountXCredentialXUser> getAllAXCXUByLogin(String login) {
        return ((AccountXCredentialXUserDao) dao).getAllAXCXUByLogin(login);
    }

    /**
     * Takes an ID, and returns the AccountXCredentialXUser with that ID,
     * fetching the credential.
     *
     * @param id
     * @return
     */
    public AccountXCredentialXUser getAXCXUForConverter(Long id) {
        return ((AccountXCredentialXUserDao) dao).getAXCXUForConverter(id);
    }

    public void addFavoritesList(AccountXCredentialXUser user, Set<FavoritesList> userFavoritesLists) {
        logger.info("TEST :)");
        this.dao.update(user);

    }

    public AccountXCredentialXUser getUserForFavoritesManagement(Long id) {
        return ((AccountXCredentialXUserDao) dao).getUserForFavoritesManagement(id);
    }

    public int getFavoritesListCount(Long id) {
        return ((AccountXCredentialXUserDao) dao).getFavoritesListCount(id);
    }

    public List<String> getFavoritesListNames(Long id) {
        return ((AccountXCredentialXUserDao) dao).getFavoritesListNames(id);
    }

    /**
     * Takes an ID, and returns a List<FavoritesList> 
     *
     * @param id
     * @return AccountXCredentialXUser
     */
    public List<FavoritesList> getInitializedFavoritesLists(Long id, int start, int quantity) {
        return ((AccountXCredentialXUserDao) dao).getInitializedFavoritesLists(id, start, quantity);
    }

    /**
     * Takes an ID, and returns a List<FavoritesList> 
     *
     * @param id
     * @return AccountXCredentialXUser
     */
    public List<FavoritesList> getInitializedFavoritesLists(long id) {
        return ((AccountXCredentialXUserDao) dao).getInitializedFavoritesLists(id);
    }

    /**
     * Takes an ID, and returns an unpopulated List<FavoritesList>
     *
     * @param id
     * @return AccountXCredentialXUser
     */
    public List<FavoritesList> getUnInitializedFavoritesLists(Long id) {
        return ((AccountXCredentialXUserDao) dao).getUnInitializedFavoritesLists(id);
    }

    public FavoritesList getFavoritesListByNameAndCredential(String name, AccountXCredentialXUser axcxu) {
        return ((AccountXCredentialXUserDao) dao).getFavoritesListByNameAndCredential(name, axcxu);
    }

    /**
     * Takes an ID, and returns a AccountXCredentialXUser with data loaded for order creation
     *
     * @param id
     * @return AccountXCredentialXUser
     */
    public AccountXCredentialXUser getAccountXCredentialXUserForBrowse(Long id) {
        return ((AccountXCredentialXUserDao) dao).getAccountXCredentialXUserForBrowse(id);
    }

    public AccountXCredentialXUser getGenericAXCXUForAccount(String accountId) {
        return ((AccountXCredentialXUserDao) dao).findGenericAXCXUByAssignedAccountId(accountId);
    }

}
