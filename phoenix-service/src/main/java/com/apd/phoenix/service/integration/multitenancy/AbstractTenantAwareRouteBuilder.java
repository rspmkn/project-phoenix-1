package com.apd.phoenix.service.integration.multitenancy;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import javax.inject.Inject;
import org.apache.camel.Exchange;
import org.apache.camel.builder.RouteBuilder;
import com.apd.phoenix.core.utility.TenantConfigRepository;

/**
 * An abstract class for all of our RouteBuilder implemenations to extend which
 * encapsulates the logic necessary to pull the tenantID from the message header and set it
 * into the CurrentTenantIdentifierResolverImpl so that our persistence layer will
 * use the appropriate datasource for all database interactions which occur during
 * the execution of the route.
 * @author rmallred
 *
 */
public abstract class AbstractTenantAwareRouteBuilder extends RouteBuilder {

    private static final String BEAN_PREFIX = "bean";
    private static final String MOCK_PREFIX = "mock";

    private String destinationPrefix = BEAN_PREFIX;

    private Long routeTenantId = null;

    private boolean routeTenantIdSpecified = false;

    @Inject
    private ThreadLocalTenantResolver resolver;

    @Override
    public void configure() throws Exception {

        TenantContext.setCurrentTenantIdentifierResolver(resolver);

        //this ensures that all messages from ActiveMQ have their tenantId property
        //set to the value of the tenantId message header
        interceptFrom("activemq:*").to(destinationPrefix + ":activemqTenantExtractor");
        interceptFrom("direct:*").to(destinationPrefix + ":activemqTenantExtractor");
        //this ensures that all messages from FTP have their tenantId property set
        //to the value of this RouteBuilder's ftpTenantId field.
        interceptFrom("ftp:*").setHeader("tenantId", constant(getTenantId())).setProperty("tenantId",
                constant(getTenantId())).to(destinationPrefix + ":activemqTenantExtractor");
        //this ensures that all messages being sent to FTP endpoints that should be tenant aware (using getEndpointName
        //on TenantFtpEndpointResolver) are sent to the tenant-specific endpoints.
        interceptSendToEndpoint(TenantFtpEndpointResolver.FTP_PREFIX + "*").skipSendToOriginalEndpoint().dynamicRouter(
                method(TenantFtpEndpointResolver.class, "getEndpoint"));
        intercept().to(destinationPrefix + ":tenantProcessor");

        this.configureRouteImplementation();
    }

    public abstract void configureRouteImplementation() throws Exception;

    /**
     * Used to determine if this is a test route, or if the messages should go to an actual EJB.
     * 
     * @param test
     */
    public void setTest(boolean test) {
        if (test) {
            destinationPrefix = MOCK_PREFIX;
        }
        else {
            destinationPrefix = BEAN_PREFIX;
        }
    }

    /**
     * By default, the tenant aware route builder will assume that all inbound FTP messages 
     * are intended for the APD tenant. To change this assumption for a route, create a route
     * for each tenant, and for each routebuilder, call this method, setting the tenant ID. 
     * 
     * @param ftpTenantId
     */
    public void setRouteTenantId(Long ftpTenantId) {
        this.routeTenantId = ftpTenantId;
        this.routeTenantIdSpecified = true;
    }

    /**
     * Gets the FTP tenant ID specified for this route. If none is specified, it is assumed
     * that FTP messages are intended for the APD tenant.
     * 
     * @return the FTP tenant ID
     */
    private Long getTenantId() {
        if (!isRouteTenantIdSpecified()) {
            routeTenantId = TenantConfigRepository.getInstance().getGenericTenantId();
        }
        return routeTenantId;
    }

    /**
     * Returns true if the FTP tenant ID has been explicitly set, false otherwise
     * 
     * @return
     */
    private boolean isRouteTenantIdSpecified() {
        return routeTenantIdSpecified;
    }

    /**
     * Takes the partner ID and the tenant ID, and returns the queue selector for this route. Used instead
     * of specifying the selector in the integration configuration.
     * 
     * @param partnerId
     * @param tenantId
     * @return
     */
    protected String getQueueSelectorParameter(String partnerId) {
        try {
            return "selector="
                    + URLEncoder.encode("(partnerId='" + partnerId.toLowerCase() + "') OR (partnerId='"
                            + partnerId.toUpperCase() + "')", "UTF-8");
        }
        catch (UnsupportedEncodingException e) {
            log.error("Error setting selector", e);
            return "";
        }
    }

}
