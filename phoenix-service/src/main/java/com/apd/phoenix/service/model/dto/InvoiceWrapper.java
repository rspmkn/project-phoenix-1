package com.apd.phoenix.service.model.dto;

import java.io.Serializable;
import java.util.List;
import com.apd.phoenix.service.model.dto.CustomerInvoiceDto;

public class InvoiceWrapper implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -6520612478152794291L;
    private List<InvoiceDto> dtos;

    public List<InvoiceDto> getDtos() {
        return dtos;
    }

    public void setDtos(List<InvoiceDto> dtos) {
        this.dtos = dtos;
    }

}
