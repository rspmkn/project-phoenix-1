/*
 * 
 */
package com.apd.phoenix.service.model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Version;
import javax.xml.bind.annotation.XmlRootElement;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

/**
 * The Class SkuType.
 */
@Entity
@Cacheable
@XmlRootElement
@Audited
public class SkuType implements Serializable, com.apd.phoenix.service.model.Entity {

    public enum ValidSkuType {
        DEALER("dealer"), CUSTOMER("customer"), VENDOR("vendor"), MANUFACTURER("manufactuer"), SEARCH("serach");

        private String name;

        private ValidSkuType(String nameInput) {
            this.name = nameInput;
        }

        public String getName() {
            return name;
        }

    }

    private static final long serialVersionUID = -4538026968267703561L;

    /** The credentials. */
    private @OneToMany(mappedBy = "skuType")
    Set<Credential> credentials = new HashSet<Credential>();

    /** The id. */
    @Id
    private @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false, nullable = false)
    Long id = null;

    /** The name. */
    @Column(nullable = false, unique = true)
    private String name;

    /** The version. */
    @Version
    private @Column(name = "version", nullable = false)
    int version = 0;

    @ManyToMany(fetch = FetchType.LAZY)
    @NotAudited
    private Set<HierarchyNode> hierarchyNodes = new HashSet<HierarchyNode>();

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        if (id != null) {
            return id.equals(((SkuType) that).id);
        }
        return super.equals(that);
    }

    /**
     * Gets the credentials.
     *
     * @return the credentials
     */
    public Set<Credential> getCredentials() {
        return this.credentials;
    }

    /**
     * Gets the id.
     *
     * @return the id
     */
    public Long getId() {
        return this.id;
    }

    /**
     * Gets the name.
     *
     * @return the name
     */
    public String getName() {
        return this.name;
    }

    /**
     * Gets the version.
     *
     * @return the version
     */
    public int getVersion() {
        return this.version;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        if (id != null) {
            return id.hashCode();
        }
        return super.hashCode();
    }

    /**
     * Sets the credentials.
     *
     * @param credentials the new credentials
     */
    public void setCredentials(final Set<Credential> credentials) {
        this.credentials = credentials;
    }

    /**
     * Sets the id.
     *
     * @param id the new id
     */
    public void setId(final Long id) {
        this.id = id;
    }

    /**
     * Sets the name.
     *
     * @param name the new name
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * Sets the version.
     *
     * @param version the new version
     */
    public void setVersion(final int version) {
        this.version = version;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        String result = "";
        if (name != null && !name.trim().isEmpty())
            result += name;
        return result;
    }

    public Set<HierarchyNode> getHierarchyNodes() {
        return hierarchyNodes;
    }

    public void setHierarchyNodes(Set<HierarchyNode> hierarchyNodes) {
        this.hierarchyNodes = hierarchyNodes;
    }

}
