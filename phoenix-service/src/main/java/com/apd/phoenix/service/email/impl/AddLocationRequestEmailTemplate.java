/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.apd.phoenix.service.email.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import com.apd.phoenix.service.email.api.Attachment;
import com.apd.phoenix.service.email.api.EmailTemplate;
import com.apd.phoenix.service.email.api.NoAttachmentContentException;
import com.apd.phoenix.service.model.dto.UserModificationRequestDto;
import com.apd.phoenix.service.utility.EmailUtils;
import freemarker.template.TemplateException;

/**
 *
 * @author kwashington
 */
@Stateless
@LocalBean
public class AddLocationRequestEmailTemplate extends EmailTemplate<Object> {

    private static final String TEMPLATE = "add.location.request";

    @Override
    public String createBody(Object content) throws IOException, TemplateException {
        Map<String, Object> params = new HashMap<>();
        
        UserModificationRequestDto umrDTO = (UserModificationRequestDto) content;

        params.put("requestType", umrDTO.getRequestType());
        params.put("userId", umrDTO.getUserId());
        params.put("firstName", umrDTO.getFirstName());
        params.put("lastName", umrDTO.getLastName());
        params.put("email", umrDTO.getEmail());
        params.put("phone", umrDTO.getPhone());
        params.put("ext", umrDTO.getExt());
        
        params.put("sAddress1", umrDTO.getSAddress1());
        params.put("sAddress2", umrDTO.getSAddress2());
        params.put("sCity", umrDTO.getSCity());
        params.put("sState", umrDTO.getSState());
        params.put("sZip", umrDTO.getSZip());
        
        //CHS Only
        params.put("sDesktop", umrDTO.getSDesktop());
        params.put("sDepartment", umrDTO.getSDepartment());
        params.put("sCostCenter", umrDTO.getSCostCenter());
        
        params.put("sComments", umrDTO.getSComments());
        
        
        return this.create(params);
    }

    @Override
    public String getTemplate() {
        return TEMPLATE;
    }

    @Override
    public Attachment createAttachment(Object content) throws NoAttachmentContentException {
        return null;
    }

}
