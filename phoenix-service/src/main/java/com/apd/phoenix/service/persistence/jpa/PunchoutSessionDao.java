/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.service.persistence.jpa;

import java.util.Date;
import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.Query;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.model.PunchoutSession;

/**
 *
 * @author dnorris
 */
@Stateless
@LocalBean
public class PunchoutSessionDao extends AbstractDao<PunchoutSession> {

    private static final Logger LOGGER = LoggerFactory.getLogger(PunchoutSessionDao.class);

    @SuppressWarnings("unchecked")
    public List<PunchoutSession> findOpenPunchoutSessionsByExample(PunchoutSession search) {

        boolean includePO = search.getCustomerOrder() != null && search.getCustomerOrder().getApdPo() != null
                && StringUtils.isNotBlank(search.getCustomerOrder().getApdPo().getValue());

        String hql = "select p from PunchoutSession as p JOIN p.customerOrder as custOrder JOIN custOrder.poNumbers AS apdPoNumber "
                + "WHERE p.operation=:operation AND p.endTime is null";

        if (includePO) {
            hql += " AND apdPoNumber.value=:orderId AND apdPoNumber.type.name=:poNumberType";
        }
        if (StringUtils.isNotBlank(search.getBuyerCookie())) {
            hql += " AND p.buyerCookie=:buyerCookie";
        }

        Query query = entityManager.createQuery(hql);
        query.setParameter("operation", search.getOperation());

        if (includePO) {
            query.setParameter("poNumberType", "APD");
            query.setParameter("orderId", search.getCustomerOrder().getApdPo().getValue());
        }
        if (StringUtils.isNotBlank(search.getBuyerCookie())) {
            query.setParameter("buyerCookie", search.getBuyerCookie());
        }
        return query.getResultList();
    }

    public void endBuyerCookieSessions(String buyerCookie) {
        String hql = "update PunchoutSession p set p.endTime=:date where p.buyerCookie=:buyerCookie";
        Query query = entityManager.createQuery(hql);
        query.setParameter("date", new Date());
        query.setParameter("buyerCookie", buyerCookie);
        int updatedEntities = query.executeUpdate();
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Ended " + updatedEntities + " punchout sessions with buyer cookie " + buyerCookie);
        }
    }
}
