/*
 * 
 */
package com.apd.phoenix.service.model;

import java.io.Serializable;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Version;
import javax.xml.bind.annotation.XmlRootElement;
import org.hibernate.envers.Audited;

/**
 * The Class PricingType.
 */
@Entity
@Cacheable
@XmlRootElement
@Audited
public class PricingType implements Serializable, com.apd.phoenix.service.model.Entity {

    private static final long serialVersionUID = 4076218089915097739L;

    private static final String STREET_LESS = "Street Less";
    private static final String STREET_LESS_SHIPPING = "Street Less, Shipping Included";

    /** The id. */
    @Id
    private @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false, nullable = false)
    Long id = null;

    /** The name. */
    @Column
    private String name;

    /** The name. */
    @Column(nullable = false)
    private String parameterRegEx;

    /** The version. */
    @Version
    private @Column(name = "version", nullable = false)
    int version = 0;

    @Column(nullable = false)
    private boolean useCeiling = false;

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        if (id != null) {
            return id.equals(((PricingType) that).id);
        }
        return super.equals(that);
    }

    /**
     * Gets the id.
     *
     * @return the id
     */
    public Long getId() {
        return this.id;
    }

    /**
     * Gets the name.
     *
     * @return the name
     */
    public String getName() {
        return this.name;
    }

    /**
     * Gets the version.
     *
     * @return the version
     */
    public int getVersion() {
        return this.version;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        if (id != null) {
            return id.hashCode();
        }
        return super.hashCode();
    }

    /**
     * Sets the id.
     *
     * @param id the new id
     */
    public void setId(final Long id) {
        this.id = id;
    }

    /**
     * Sets the name.
     *
     * @param name the new name
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * Sets the version.
     *
     * @param version the new version
     */
    public void setVersion(final int version) {
        this.version = version;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        String result = "";
        if (name != null && !name.trim().isEmpty())
            result += name;
        return result;
    }

    public String getParameterRegEx() {
        return this.parameterRegEx;
    }

    public void setParameterRegEx(final String parameterRegEx) {
        this.parameterRegEx = parameterRegEx;
    }

    public boolean getIsDisabledForVendorCatalogs() {

        if (STREET_LESS.equals(this.name) || STREET_LESS_SHIPPING.equals(this.name)) {
            return true;
        }
        else {
            return false;
        }
    }

    public boolean getUseCeiling() {
        return this.useCeiling;
    }

    public void setUseCeiling(final boolean useCeiling) {
        this.useCeiling = useCeiling;
    }

}
