package com.apd.phoenix.service.product;

import javax.ejb.Stateless;
import javax.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.cache.CacheProviderException;
import com.apd.phoenix.service.cache.CatalogDataCacheProvider;
import com.apd.phoenix.service.model.Catalog.SearchType;
import com.apd.phoenix.service.persistence.multitenancy.impl.CurrentTenantIdentifierResolverImpl;

/**
 * Concrete class for ProductCacheManager
 * 
 * @author RHC
 *
 */
@Stateless
public class CatalogDataCacheManagerImpl implements CatalogDataCacheManager {

    private static final Logger logger = LoggerFactory.getLogger(CatalogDataCacheManagerImpl.class);

    @Inject
    private CatalogDataCacheProvider cacheProvider;

    @Override
    public CatalogData getCachedCatalogData(Long catalogId, SearchType searchType) {
        if (catalogId == null) {
            return null;
        }
        if (searchType == null) {
            searchType = SearchType.SOLR_SEARCH;
        }
        try {
            long tenantId = CurrentTenantIdentifierResolverImpl.getCurrentTenant();
            return cacheProvider.getCache().get(new CatalogDataKey((long) catalogId, searchType.toString(), tenantId));
        }
        catch (CacheProviderException e) {
            logger.warn("CacheProviderException occurred, turn on debug for stack trace");
            if (logger.isDebugEnabled()) {
                logger.debug("Stack trace", e);
            }
            return null;
        }
    }

    @Override
    public void setCachedCatalogData(Long catalogId, SearchType searchType, CatalogData data) {
        if (catalogId == null) {
            return;
        }
        if (searchType == null) {
            searchType = SearchType.SOLR_SEARCH;
        }
        try {
            long tenantId = CurrentTenantIdentifierResolverImpl.getCurrentTenant();
            cacheProvider.getCache().put(new CatalogDataKey((long) catalogId, searchType.toString(), tenantId), data);
        }
        catch (CacheProviderException e) {
            logger.warn("CacheProviderException occurred, turn on debug for stack trace");
            if (logger.isDebugEnabled()) {
                logger.debug("Stack trace", e);
            }
        }
    }
}
