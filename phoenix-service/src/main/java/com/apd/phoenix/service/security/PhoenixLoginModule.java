package com.apd.phoenix.service.security;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.security.auth.login.FailedLoginException;
import javax.security.auth.login.LoginException;
import javax.security.jacc.PolicyContext;
import javax.security.jacc.PolicyContextException;
import javax.servlet.http.HttpServletRequest;
import javax.sql.DataSource;
import com.apd.phoenix.core.EncryptionUtils;
import com.apd.phoenix.service.business.IpSubdomainRestriction;
import com.apd.phoenix.service.business.SingletonPropertiesLoader;

public class PhoenixLoginModule extends AbstractMultiTenantLoginModule {

    private static final String SUBDOMAIN_TYPE_NAME = "Subdomain";
    private static final int FAILED_LOGINS_PER_DAY = 5;
    //{SCHEMA} will be replaced at runtime with appropriate tenant schema name.
    private String setDateQuery = "update {SCHEMA}.SYSTEMUSER set loginAttempts=?, "
            + "lastLoginAttemptDate=? where lower(login)=lower(?) AND isactive = 1";
    private String successfulLoginQuery = "update {SCHEMA}.SYSTEMUSER set loginAttempts=0, "
            + "lastLoginAttemptDate=null, lastLoginDate=? where lower(login)=lower(?) AND isactive = 1";

    /**
     * Private method, reusing code from the getUsersPassword method in DatabaseServerLoginModule:
     * 
     * http://www.docjar.com/html/api/org/jboss/security/auth/spi/DatabaseServerLoginModule.java.html
     * 
     * @param username
     * @return
     * @throws LoginException
     */
    private String getSalt(String username) throws LoginException {
        String salt = null;
        try (
    			Connection conn = ((DataSource) (new InitialContext()).lookup(dsJndiName)).getConnection();
    			PreparedStatement statement = conn.prepareStatement(loginInfoQuery);
        		ResultSet rs = getResultsWithUsername(username, statement)
        	) {
            //gets the information from the query
            if (rs.next() == false) {
                throw new FailedLoginException("No matching username found in Principals");
            }
            salt = rs.getString(1);
        }
        catch (NamingException ex) {
            LoginException le = new LoginException("Error looking up DataSource from: " + dsJndiName);
            le.initCause(ex);
            throw le;
        }
        catch (SQLException ex) {
            LoginException le = new LoginException("Query failed");
            le.initCause(ex);
            throw le;
        }
        return salt;
    }

    /**
     * Based on a username, returns the resultset containing the salt, login attempts, and the date of the last 
     * attempt for that user.
     * 
     * @param username
     * @param statement
     * @return
     * @throws SQLException
     * @throws NamingException
     */
    private ResultSet getResultsWithUsername(String username, PreparedStatement statement) throws SQLException,
            NamingException {
        String ipAddress = "invalid_address";
        String subdomain = IpSubdomainRestriction.GENERIC_SUBDOMAIN;
        try {
            HttpServletRequest request = (HttpServletRequest) PolicyContext
                    .getContext("javax.servlet.http.HttpServletRequest");
            String[] addressAndSubdomain = IpSubdomainRestriction.ipAndDomainInfo(request);
            ipAddress = addressAndSubdomain[0];
            subdomain = addressAndSubdomain[1];
        }
        catch (PolicyContextException ex) {
        }
        statement.setString(1, username);
        statement.setString(2, SUBDOMAIN_TYPE_NAME);
        statement.setString(3, subdomain);
        statement.setString(4, ipAddress);
        statement.setString(5, subdomain);
        String defaultSubdomain = IpSubdomainRestriction.GENERIC_SUBDOMAIN;
        if (!SingletonPropertiesLoader.getTenant().equals(SingletonPropertiesLoader.DEFAULT_TENANT)) {
            defaultSubdomain = SingletonPropertiesLoader.getTenant();
        }
        statement.setString(6, defaultSubdomain);
        return statement.executeQuery();
    }

    /**
     * Overrides the method in UsernamePasswordLoginModule; calls the original method, but then uses the 
     * EncryptionUtils hashing algorithm.
     * 
     * http://grepcode.com/file/repository.jboss.org/nexus/content/repositories/releases/org.jboss.security/jbosssx-bare/2.0.2.Beta3/org/jboss/security/auth/spi/UsernamePasswordLoginModule.java
     * http://docs.jboss.org/jbossas/javadoc/4.0.2/org/jboss/security/auth/spi/UsernamePasswordLoginModule.java.html
     */
    @Override
    protected String createPasswordHash(String username, String password, String digestOption) throws LoginException {
        super.createPasswordHash(username, password, digestOption);
        String passwordHash = EncryptionUtils.hash(password, this.getSalt(username));
        return passwordHash;
    }

    /**
     * Overrides the method in UsernamePasswordLoginModule. First, it obtains the number of login attempts, and the 
     * date of the last attempt; if the number is greater than the limit and the date is today, a LoginException is 
     * thrown. If the attempts are acceptable, then it performs the superclass's login method, hashing the password 
     * with the createPasswordHash method. If the passwords don't match, then the date of the last attempt is set to 
     * today and the number of attempts is incremented.
     * <br /><br />
     * For every login attempt, there is one read action. If there have been too many attempts today, or if the user 
     * isn't found, then there will be no more calls. Otherwise, there is one more read and one write action.
     */
    @Override
    public boolean login() throws LoginException {
    	//Update our queries to replace {SCHEMA} with the appropriate tenant schema name
    	successfulLoginQuery = addTenantSchemaSwitchToQuery(successfulLoginQuery, currentTenantSchema);
    	setDateQuery = addTenantSchemaSwitchToQuery(setDateQuery, currentTenantSchema);
    	try (
        		Connection conn = ((DataSource) (new InitialContext()).lookup(dsJndiName)).getConnection();
        		PreparedStatement successfulLoginStatement = conn.prepareStatement(successfulLoginQuery);
        		PreparedStatement updateCountStatement = conn.prepareStatement(setDateQuery)
        	) {
        	//gets the logins and dates. This call is the first read action, and is always performed
        	//if the number of attempts today is greater than a limit, or the user does not exist,
        	//an exception is thrown
    		String username = getUsernameAndPassword()[0];
    		Integer logins = checkLoginAttempts(username);
        	try {
        		//executes the superclass's login method. This calls createPasswordHash, which calls 
        		//getSalt, which executes the second read action
        		//if the hashes don't match, an exception is thrown, going to the catch block below
        		boolean toReturn = super.login();
        		//clears the user's login attempt tally
        		//this is the write action that occurs if the user exists
        		successfulLoginStatement.setDate(1, new java.sql.Date(java.util.Calendar.getInstance().getTime().getTime()));
        		successfulLoginStatement.setString(2, username);
        		successfulLoginStatement.execute();
    	        return toReturn;
        	} catch (LoginException ex) {
                //sets the date to today, and the counter to increment by 1
                updateCountStatement.setInt(1, logins + 1);
                updateCountStatement.setDate(2, new java.sql.Date(java.util.Calendar.getInstance().getTime().getTime()));
                updateCountStatement.setString(3, username);
                //writes the new date and number of attempts. this is the write action if the password is incorrect.
                updateCountStatement.execute();
                throw ex;
        	}
        } 
        catch (NamingException ex) {
            LoginException le = new LoginException("Error looking up DataSource from: " + dsJndiName);
            le.initCause(ex);
            throw le;
        }
        catch (SQLException ex) {
            LoginException le = new LoginException("Query failed");
            le.initCause(ex);
            throw le;
        }
    }

    /**
     * This method takes a username, and checks if the user should be allowed to log in, based on the number of 
     * attempts and the day of the last attempt. If the user can log in, returns the number of login attempts today.
     * 
     * @param username
     * @return
     * @throws LoginException
     * @throws SQLException
     * @throws NamingException
     */
    private Integer checkLoginAttempts(String username) throws LoginException, SQLException, NamingException {
        try (
	        	Connection conn = ((DataSource) (new InitialContext()).lookup(dsJndiName)).getConnection();
	            PreparedStatement statement = conn.prepareStatement(loginInfoQuery);
	        	ResultSet rs = getResultsWithUsername(username, statement);
	        ) {
            //gets the information from the query
            if (rs.next() == false) {
                throw new FailedLoginException("No matching username found in Principals");
            }
            Integer logins = rs.getInt(2);
            Date loginDate = rs.getDate(3);

            //finds a date 24 hours from the current system time
            Calendar cal = Calendar.getInstance();
            cal.add(Calendar.DATE, -1);

            //tests that the last day that logins were attempted wasn't today, or that the number of logins 
            //attempted is less than a limit.
            if (loginDate != null && cal.getTime().before(loginDate) && logins >= FAILED_LOGINS_PER_DAY) {
                throw new FailedLoginException("Too many attempts have been made today");
            }
            else if (loginDate == null || cal.getTime().after(loginDate)) {
                //if the last login date isn't today, sets the number of logins to zero
                //this is because there have been zero login attempts today
                logins = new Integer(0);
            }
            return logins;
        }
        catch (Exception ex) {
        	throw new FailedLoginException("Error getting login attempts!");
        }
    }
}
