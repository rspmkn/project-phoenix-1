/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.service.model;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;

/**
 *
 * @author rhc
 */
@javax.persistence.Entity
@DiscriminatorValue(value = "EXCEPTION_ORDER_LOG")
public class ExceptionOrderLog extends OrderLog {

    //this database column is also used by ChangeOrderLog
    @Column(length = 3000)
    private String description;

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }
}
