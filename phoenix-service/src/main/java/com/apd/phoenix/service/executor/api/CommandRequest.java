package com.apd.phoenix.service.executor.api;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * This class is a request pojo to attempt to run a command in the
 * executor service.
 * @author RHC
 *
 */
public class CommandRequest implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 4013337859129960053L;
    private String commandKey;
    private String commandName;
    private List<String> callbacks;
    private int retries;

    private Map<String, Object> requestData;

    public String getCommandKey() {
        return commandKey;
    }

    public void setCommandKey(String commandKey) {
        this.commandKey = commandKey;
    }

    public String getCommandName() {
        return commandName;
    }

    public void setCommandName(String commandName) {
        this.commandName = commandName;
    }

    public Map<String, Object> getRequestData() {
        return requestData;
    }

    public void setRequestData(Map<String, Object> requestData) {
        this.requestData = requestData;
    }

    public List<String> getCallbacks() {
        return callbacks;
    }

    public void setCallbacks(List<String> callbacks) {
        this.callbacks = callbacks;
    }

    public int getRetries() {
        return retries;
    }

    public void setRetries(int retries) {
        this.retries = retries;
    }

}
