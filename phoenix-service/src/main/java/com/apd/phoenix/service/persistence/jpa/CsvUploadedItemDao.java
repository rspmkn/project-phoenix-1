package com.apd.phoenix.service.persistence.jpa;

import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.Query;
import com.apd.phoenix.service.model.CsvUploadedItem;

/**
 * CSV Item DAO
 * 
 * @author RHC
 *
 */
@Stateless
@LocalBean
public class CsvUploadedItemDao extends AbstractDao<CsvUploadedItem> {

    private static final int BATCH_SIZE = 100;

    @SuppressWarnings("unchecked")
    public List<CsvUploadedItem> uploadedItems() {
        String hql = "SELECT uploadedItem FROM CsvUploadedItem uploadedItem";

        //Creates the query
        Query query = entityManager.createQuery(hql);

        this.setItemsReturned(query, 0, BATCH_SIZE);

        return (List<CsvUploadedItem>) query.getResultList();
    }

    @SuppressWarnings("unchecked")
    public List<String> uploadedItemFields() {
        String hql = "SELECT DISTINCT field.key FROM CsvUploadedItemField field";

        //Creates the query
        Query query = entityManager.createQuery(hql);
        return (List<String>) query.getResultList();
    }
}
