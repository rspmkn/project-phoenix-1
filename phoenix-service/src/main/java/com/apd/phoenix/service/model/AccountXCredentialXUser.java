/*
 * 
 */
package com.apd.phoenix.service.model;

import java.io.Serializable;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Version;
import javax.xml.bind.annotation.XmlRootElement;
import org.apache.commons.lang.StringUtils;
import org.hibernate.annotations.FetchProfile;
import org.hibernate.envers.Audited;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.AuditJoinTable;
import org.hibernate.envers.NotAudited;
import com.apd.phoenix.service.business.EntityComparator.GenericComparator;

/**
 * The Class AccountXCredentialXUser.
 */
@Entity
@FetchProfile(name = "AccountXCredentialXUser-for-browse", fetchOverrides = {
        @FetchProfile.FetchOverride(entity = AccountXCredentialXUser.class, association = "credential", mode = org.hibernate.annotations.FetchMode.JOIN),
        @FetchProfile.FetchOverride(entity = AccountXCredentialXUser.class, association = "account", mode = org.hibernate.annotations.FetchMode.JOIN),
        @FetchProfile.FetchOverride(entity = AccountXCredentialXUser.class, association = "user", mode = org.hibernate.annotations.FetchMode.JOIN),
        @FetchProfile.FetchOverride(entity = Credential.class, association = "catalog", mode = org.hibernate.annotations.FetchMode.JOIN),
        @FetchProfile.FetchOverride(entity = Credential.class, association = "properties", mode = org.hibernate.annotations.FetchMode.JOIN),
        @FetchProfile.FetchOverride(entity = Credential.class, association = "roles", mode = org.hibernate.annotations.FetchMode.JOIN),
        @FetchProfile.FetchOverride(entity = Credential.class, association = "permissions", mode = org.hibernate.annotations.FetchMode.JOIN), })
@XmlRootElement
@Audited
@AuditTable(value = "axcxu_aud")
public class AccountXCredentialXUser implements Serializable, com.apd.phoenix.service.model.Entity {

    private static final long serialVersionUID = -691379309983398748L;

    /** The id. */
    @Id
    private @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false, nullable = false)
    Long id = null;

    /** The version. */
    @Version
    private @Column(name = "version", nullable = false)
    int version = 0;

    @ManyToOne(fetch = FetchType.EAGER)
    private PaymentInformation paymentInformation;

    @OneToMany(mappedBy = "axcxu", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<AssignedCostCenter> allowedCostCenters = new HashSet<AssignedCostCenter>();

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "axcxu_id")
    @AuditJoinTable(name = "axcxu_f_AUD")
    @NotAudited
    private Set<FavoritesList> favorites = new HashSet<FavoritesList>();

    @OneToMany(mappedBy = "axcxu", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<AssignedDepartment> departments = new HashSet<AssignedDepartment>();

    @JoinTable(name = "AccXCredXUser_Desktops")
    @ManyToMany(fetch = FetchType.LAZY)
    private Set<Desktop> desktops = new HashSet<Desktop>();

    @Column
    private String usAccountNumber;

    @Column(nullable = false)
    private boolean active = true;

    @Column
    private String name;

    @Column
    private String credentialSelectionImage;

    /**
     * Gets the id.
     *
     * @return the id
     */
    public Long getId() {
        return this.id;
    }

    /**
     * Sets the id.
     *
     * @param id the new id
     */
    public void setId(final Long id) {
        this.id = id;
    }

    /**
     * Gets the version.
     *
     * @return the version
     */
    public int getVersion() {
        return this.version;
    }

    /**
     * Sets the version.
     *
     * @param version the new version
     */
    public void setVersion(final int version) {
        this.version = version;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        if (id != null) {
            return id.equals(((AccountXCredentialXUser) that).id);
        }
        return super.equals(that);
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        if (id != null) {
            return id.hashCode();
        }
        return super.hashCode();
    }

    /** The account. */
    @ManyToOne(fetch = FetchType.EAGER)
    private Account account;

    /**
     * Gets the account.
     *
     * @return the account
     */
    public Account getAccount() {
        return this.account;
    }

    /**
     * Sets the account.
     *
     * @param account the new account
     */
    public void setAccount(final Account account) {
        this.account = account;
    }

    /** The credential. */
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "cred_id")
    private Credential credential;

    /**
     * Gets the credential.
     *
     * @return the credential
     */
    public Credential getCredential() {
        return this.credential;
    }

    /**
     * Sets the credential.
     *
     * @param credential the new credential
     */
    public void setCredential(final Credential credential) {
        this.credential = credential;
    }

    /** The user. */
    @ManyToOne(fetch = FetchType.EAGER)
    private SystemUser user;

    /**
     * Gets the user.
     *
     * @return the user
     */
    public SystemUser getUser() {
        return this.user;
    }

    /**
     * Sets the user.
     *
     * @param user the new user
     */
    public void setUser(final SystemUser user) {
        this.user = user;
    }

    /** The address. */
    @OneToOne(fetch = FetchType.EAGER)
    private Address address;

    /**
     * Gets the address.
     *
     * @return the address
     */
    public Address getAddress() {
        return this.address;
    }

    /**
     * Sets the address.
     *
     * @param address the address
     */
    public void setAddress(final Address address) {
        this.address = address;
    }

    /** The punch level. */
    @Column
    private String punchLevel;

    /**
     * Gets the punch level.
     *
     * @return the punch level
     */
    public String getPunchLevel() {
        return this.punchLevel;
    }

    /**
     * Sets the punch level.
     *
     * @param punchLevel the new punch level
     */
    public void setPunchLevel(final String punchLevel) {
        this.punchLevel = punchLevel;
    }

    /** The user. */
    @ManyToOne(fetch = FetchType.LAZY)
    private SystemUser approverUser;

    /**
     * Gets the user.
     *
     * @return the user
     */
    public SystemUser getApproverUser() {
        return this.approverUser;
    }

    /**
     * Sets the user.
     *
     * @param approverUser the new approverUser
     */
    public void setApproverUser(final SystemUser approverUser) {
        this.approverUser = approverUser;
    }

    @OneToMany(mappedBy = "axcxu", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<LimitApproval> limitApprovals = new HashSet<LimitApproval>();

    public Set<LimitApproval> getLimitApprovals() {
        return limitApprovals;
    }

    public void setLimitApprovals(Set<LimitApproval> limitApproval) {
        this.limitApprovals = limitApproval;
    }

    public PaymentInformation getPaymentInformation() {
        return this.paymentInformation;
    }

    public void setPaymentInformation(final PaymentInformation paymentInformation) {
        this.paymentInformation = paymentInformation;
    }

    public void newAddress() {
        this.address = new Address();
    }

    public Set<AssignedCostCenter> getAllowedCostCenters() {
        return this.allowedCostCenters;
    }

    public void setAllowedCostCenters(final Set<AssignedCostCenter> allowedCostCenters) {
        this.allowedCostCenters = allowedCostCenters;
    }

    public Set<FavoritesList> getFavorites() {
        return this.favorites;
    }

    public void setFavorites(final Set<FavoritesList> favorites) {
        this.favorites = favorites;
    }

    public Set<AssignedDepartment> getDepartments() {
        return this.departments;
    }

    public void setDepartments(final Set<AssignedDepartment> departments) {
        this.departments = departments;
    }

    public String getUsAccountNumber() {
        return this.usAccountNumber;
    }

    public void setUsAccountNumber(final String usAccountNumber) {
        this.usAccountNumber = usAccountNumber;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public Set<Desktop> getDesktops() {
        return desktops;
    }

    public void setDesktops(Set<Desktop> desktops) {
        this.desktops = desktops;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCredentialSelectionImage() {
        return credentialSelectionImage;
    }

    public void setCredentialSelectionImage(String credentialSelectionImage) {
        this.credentialSelectionImage = credentialSelectionImage;
    }

    public String getLabel() {
        if (StringUtils.isNotBlank(name)) {
            return name;
        }
        else if (this.credential != null) {
            return credential.getName();
        }
        else {
            return null;
        }
    }

    @Override
    public String toString() {
        String result = getClass().getSimpleName() + " ";
        result += "serialVersionUID: " + serialVersionUID;
        if (usAccountNumber != null && !usAccountNumber.trim().isEmpty())
            result += ", usAccountNumber: " + usAccountNumber;
        if (punchLevel != null && !punchLevel.trim().isEmpty())
            result += ", punchLevel: " + punchLevel;
        return result;
    }

    public static class AxcxuComparator implements Comparator<AccountXCredentialXUser> {

        @Override
        public int compare(AccountXCredentialXUser arg0, AccountXCredentialXUser arg1) {
            if (arg0 != null && StringUtils.isNotBlank(arg0.getLabel()) && arg1 != null
                    && StringUtils.isNotBlank(arg1.getLabel())) {
                return arg0.getLabel().compareTo(arg1.getLabel());
            }
            return (new GenericComparator()).compare(arg0, arg1);
        }
    }
}
