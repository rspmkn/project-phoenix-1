package com.apd.phoenix.service.business;

import java.util.List;
import java.util.Properties;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import com.apd.phoenix.core.utility.PropertiesLoader;
import com.apd.phoenix.service.model.Account;
import com.apd.phoenix.service.model.AccountBRMSRecord;
import com.apd.phoenix.service.persistence.jpa.AccountBRMSRecordDao;

@Stateless
@LocalBean
public class AccountBRMSRecordBp extends AbstractBp<AccountBRMSRecord> {

    /**
     * This method is injected with the correct Dao for initialization
     * 
     * @param dao
     */
    @Inject
    public void initDao(AccountBRMSRecordDao dao) {
        this.dao = dao;
    }

    public Boolean isActive(Long accountId) {
        return getFirst(((AccountBRMSRecordDao) dao).isActive(accountId));
    }

    public Integer getSessionId(Long accountId) {
        return getFirst(((AccountBRMSRecordDao) dao).getSessionId(accountId));
    }

    public AccountBRMSRecord getRecordByAccountId(Long accountId) {
        return getFirst(((AccountBRMSRecordDao) dao).getRecordByAccountId(accountId));
    }

    public AccountBRMSRecord getRecordBySessionId(Long sessionId) {
        return getFirst(((AccountBRMSRecordDao) dao).getRecordBySessionId(sessionId));
    }

    private <T> T getFirst(List<T> list) {
        if (list == null || list.size() == 0) {
            return null;
        }
        return list.get(0);
    }

    public void createRecord(Long accountId, Integer sessionId, Boolean active) {
        Properties versionProperties = PropertiesLoader.getAsProperties("guvnor");
        AccountBRMSRecord newRecord = new AccountBRMSRecord();
        newRecord.setActive(active);
        newRecord.setSessionId(sessionId);
        newRecord.setSnapshot(versionProperties.getProperty("snapshot"));
        Account justId = new Account();
        justId.setId(accountId);
        newRecord.setAccount(justId);

        create(newRecord);
    }

    public void setActive(Long accountId, Boolean active) {
        AccountBRMSRecord oldRecord = getRecordByAccountId(accountId);
        oldRecord.setActive(active);
        update(oldRecord);
    }

    public void setProcessId(long accountId, long processId) {
        AccountBRMSRecord oldRecord = getRecordByAccountId(accountId);
        oldRecord.setProcessId(processId);
        update(oldRecord);
    }

    public boolean isStale(long accountId) {
        //TODO: Figure out how to determine if an account's processId is stale and implement
        return false;
    }

    public long getProcessId(long accountId) {
        return getFirst(((AccountBRMSRecordDao) dao).getProcessId(accountId));
    }

    public String getSnapshot(long accountId) {
        return getFirst(((AccountBRMSRecordDao) dao).getSnapshot(accountId));
    }
}
