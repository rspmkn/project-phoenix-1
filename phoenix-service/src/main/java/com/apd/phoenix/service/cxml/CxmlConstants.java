/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.service.cxml;

/**
 *
 * @author dnorris
 */
public final class CxmlConstants {

    private CxmlConstants() {
        // restrict instantiation
    }

    public static final String USER_AGENT = "Phoenix APD 1.0 Alpha";
    public static final String XML_LANG = "en";
    public static final String DEFAULT_CURRENCY = "USD";
    public static final String DEFAULT_OPERATION_ALLOWED = "edit";
    public static final int CXML_STATUS_OK = 200;
    public static final int CXML_STATUS_BAD_REQUEST = 400;
    public static final int CXML_STATUS_UNAUTHORIZED = 401;
    public static final int CXML_STATUS_NOT_ACCEPTABLE = 406;
    public static final int CXML_STATUS_PRECONDITION_FAILED = 412;
    public static final int CXML_STATUS_NOT_IMPLEMENTED = 450;
    public static final int CXML_STATUS_INTERNAL_ERROR = 500;
    public static final int CXML_STATUS_SUPPLIER_UNAVAILABLE = 550;
    public static final int CXML_STATUS_TEMPORARY_SERVER_ERROR = 560;
}
