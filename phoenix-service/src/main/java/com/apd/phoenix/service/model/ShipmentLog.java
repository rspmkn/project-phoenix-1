/**
 * 
 */
package com.apd.phoenix.service.model;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;

@Entity
@DiscriminatorValue(value = "SHIPMENT_LOG")
public class ShipmentLog extends OrderLog {

    private static final long serialVersionUID = -6056260041898052847L;
    /** The customer order. */
    @ManyToOne(fetch = FetchType.LAZY)
    private Shipment shipment;

    /**
     * @return the shipment
     */
    public Shipment getShipment() {
        return shipment;
    }

    /**
     * @param shipment the shipment to set
     */
    public void setShipment(final Shipment shipment) {
        this.shipment = shipment;
    }
}
