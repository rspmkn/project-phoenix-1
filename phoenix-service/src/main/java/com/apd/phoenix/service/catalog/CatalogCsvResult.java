package com.apd.phoenix.service.catalog;

import java.io.Serializable;
import java.util.ArrayList;
import com.apd.phoenix.service.catalog.CatalogCsvMessage.CatalogCsvTypeEnum;

public class CatalogCsvResult implements Serializable {

    private ArrayList<String> rows;
    private String correlationId;
    private String header;
    private long catalogId;
    private Long totalItems;
    private CatalogCsvTypeEnum catalogCsvType;

    public ArrayList<String> getRows() {
        return rows;
    }

    public void setRows(ArrayList<String> rows) {
        this.rows = rows;
    }

    public String getCorrelationId() {
        return correlationId;
    }

    public void setCorrelationId(String correlationId) {
        this.correlationId = correlationId;
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public long getCatalogId() {
        return catalogId;
    }

    public void setCatalogId(long catalogId) {
        this.catalogId = catalogId;
    }

    public Long getTotalItems() {
        return totalItems;
    }

    public void setTotalItems(Long totalItems) {
        this.totalItems = totalItems;
    }

    public CatalogCsvTypeEnum getCatalogCsvType() {
        return catalogCsvType;
    }

    public void setCatalogCsvType(CatalogCsvTypeEnum catalogCsvType) {
        this.catalogCsvType = catalogCsvType;
    }

}
