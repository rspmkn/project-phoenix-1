/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.apd.phoenix.service.email.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import com.apd.phoenix.service.email.api.Attachment;
import com.apd.phoenix.service.email.api.EmailTemplate;
import com.apd.phoenix.service.email.api.NoAttachmentContentException;
import com.apd.phoenix.service.model.dto.UserModificationRequestDto;
import com.apd.phoenix.service.utility.EmailUtils;
import freemarker.template.TemplateException;

/**
 *
 * @author anicholson
 */
@Stateless
@LocalBean
public class UserRequestCsrEmailTemplate extends EmailTemplate<Object> {

    private static final String TEMPLATE = "user.request.csr";

    @Override
    public String createBody(Object content) throws IOException, TemplateException {
    	
        Map<String, Object> params = new HashMap<>();
        List<String> fieldList = EmailUtils.getFieldList(content);
        params.put("fieldList", fieldList);
        
        return this.create(params);
    }

    @Override
    public String getTemplate() {
        return TEMPLATE;
    }

    @Override
    public Attachment createAttachment(Object content) throws NoAttachmentContentException {
        return null;
    }

}
