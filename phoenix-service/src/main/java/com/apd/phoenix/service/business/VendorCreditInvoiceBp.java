package com.apd.phoenix.service.business;

import com.apd.phoenix.service.business.CustomerOrderBp.NonUniqueCustomerPoException;
import java.util.ArrayList;
import java.util.Date;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import com.apd.phoenix.service.model.Credential;
import com.apd.phoenix.service.model.LineItem;
import com.apd.phoenix.service.model.ReturnOrder.ReturnOrderStatus;
import com.apd.phoenix.service.model.VendorCreditInvoice;
import com.apd.phoenix.service.model.dto.LineItemXReturnDto;
import com.apd.phoenix.service.model.dto.VendorCreditInvoiceDto;
import com.apd.phoenix.service.persistence.jpa.VendorCreditInvoiceDao;
import com.apd.phoenix.service.utility.ErrorEmailService;

/**
 * 
 * @author RHC
 *
 */
@Stateless
@LocalBean
public class VendorCreditInvoiceBp extends AbstractBp<VendorCreditInvoice> {

    @Inject
    CustomerOrderBp customerOrderBp;

    @Inject
    LineItemBp lineItemBp;

    @Inject
    ErrorEmailService errorEmailService;

    /**
     * This method is injected with the correct Dao for initialization
     * 
     * @param dao
     */
    @Inject
    public void initDao(VendorCreditInvoiceDao dao) {
        this.dao = dao;
    }

    public VendorCreditInvoice persistVendorCreditInvoice(VendorCreditInvoiceDto vendorCreditInvoiceDto) {
        try {
            VendorCreditInvoice vendorCreditInvoice = new VendorCreditInvoice();
            vendorCreditInvoice.setCustomerOrder(customerOrderBp.createOrUpdateCustomerOrder(vendorCreditInvoiceDto
                    .getCustomerOrderDto()));
            for (LineItemXReturnDto lixrd : vendorCreditInvoiceDto.getItems()) {
                //if valid
                if (lixrd != null && lixrd.getLineItemDto() != null && lixrd.getQuantity() != null) {
                    Credential cred = vendorCreditInvoice.getCustomerOrder() != null ? vendorCreditInvoice
                            .getCustomerOrder().getCredential() : null;
                    LineItem item = lineItemBp.createLineItem(lixrd.getLineItemDto(), cred);
                    item.setQuantity(lixrd.getQuantity().intValue());
                    item = lineItemBp.update(item);
                    if (vendorCreditInvoice.getActualItems() == null) {
                        vendorCreditInvoice.setActualItems(new ArrayList<LineItem>());
                    }
                    vendorCreditInvoice.getActualItems().add(item);
                }
            }

            vendorCreditInvoice.setApprovalDate(new Date());
            vendorCreditInvoice.setAmount(vendorCreditInvoiceDto.getAmount());

            vendorCreditInvoice.setBillingTypeCode(vendorCreditInvoiceDto.getBillingTypeCode());
            vendorCreditInvoice.setDate(new Date());
            vendorCreditInvoice.setInvoiceDate(vendorCreditInvoiceDto.getInvoiceDate());
            vendorCreditInvoice.setDueDate(vendorCreditInvoiceDto.getDueDate());
            vendorCreditInvoice.setInvoiceNumber(vendorCreditInvoiceDto.getInvoiceNumber());

            vendorCreditInvoice.setRaNumber(vendorCreditInvoiceDto.getRaNumber());
            vendorCreditInvoice.setRestockingFee(vendorCreditInvoiceDto.getRestockingFee());
            vendorCreditInvoice.setSpecialInstructions(new ArrayList<String>());
            if (vendorCreditInvoiceDto.getSpecalInstructions() != null) {
                for (String specialInstruction : vendorCreditInvoiceDto.getSpecalInstructions()) {
                    vendorCreditInvoice.getSpecialInstructions().add(specialInstruction);
                }
            }
            vendorCreditInvoice.setStatus(ReturnOrderStatus.UNRECONCILED);
            return create(vendorCreditInvoice);
        }
        catch (NonUniqueCustomerPoException e) {
            errorEmailService.sendDuplicateCustomerPOErrorEmail(e.getOrder(), e.getCustomerPo());
        }
        return null;
    }
}
