package com.apd.phoenix.service.product;

import java.io.Serializable;

/**
 * Presentation layer wrapper for storing product data pulled from solr results
 * or db directly
 *
 * @author RHC
 */
public class ProductKey implements Serializable {

    private static final long serialVersionUID = 1L;

    private String sku;
    private String vendorName;
    private long customerCatalogId;
    private long tenantId;

    /**
     * This constructor should not be used in application code.
     */
    @Deprecated
    public ProductKey() {
    }

    public ProductKey(String sku, String vendorName, long customerCatalogId, long tenantId) {
        this.sku = sku;
        this.vendorName = vendorName;
        this.customerCatalogId = customerCatalogId;
        this.setTenantId(tenantId);
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public String getVendorName() {
        return vendorName;
    }

    public void setVendorName(String vendorName) {
        this.vendorName = vendorName;
    }

    public long getCustomerCatalogId() {
        return customerCatalogId;
    }

    public void setCustomerCatalogId(long customerCatalogId) {
        this.customerCatalogId = customerCatalogId;
    }

    public long getTenantId() {
        return tenantId;
    }

    public void setTenantId(long tenantId) {
        this.tenantId = tenantId;
    }

}
