package com.apd.phoenix.service.business;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.UUID;
import javax.annotation.PostConstruct;
import javax.ejb.Asynchronous;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.jms.JMSException;
import javax.naming.NamingException;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringEscapeUtils;
import org.jboss.ejb3.annotation.TransactionTimeout;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.core.utility.TenantConfigRepository;
import com.apd.phoenix.service.catalog.CatalogCsvMessage;
import com.apd.phoenix.service.catalog.CatalogCsvMessage.CatalogCsvTypeEnum;
import com.apd.phoenix.service.catalog.CatalogCsvMessageEntryPoint;
import com.apd.phoenix.service.catalog.CatalogCsvPurge;
import com.apd.phoenix.service.catalog.CatalogQueueCount;
import com.apd.phoenix.service.catalog.CatalogUploadPurge;
import com.apd.phoenix.service.model.Account;
import com.apd.phoenix.service.model.Catalog;
import com.apd.phoenix.service.model.Credential;
import com.apd.phoenix.service.model.CsvUploadedItem;
import com.apd.phoenix.service.model.CsvUploadedItemField;
import com.apd.phoenix.service.model.FavoritesList;
import com.apd.phoenix.service.model.ItemPropertyType;
import com.apd.phoenix.service.model.PendingSmartSearchDeletion;
import com.apd.phoenix.service.model.SkuType;
import com.apd.phoenix.service.model.SyncItemResultSummary;
import com.apd.phoenix.service.model.listener.CatalogXItemListener;
import com.apd.phoenix.service.model.listener.ItemListener;
import com.apd.phoenix.service.persistence.jpa.CatalogDao;
import com.apd.phoenix.service.persistence.jpa.CredentialDao;
import com.apd.phoenix.service.persistence.jpa.CsvUploadedItemDao;
import com.apd.phoenix.service.persistence.jpa.ItemPropertyTypeDao;
import com.apd.phoenix.service.persistence.jpa.SkuTypeDao;
import com.apd.phoenix.service.persistence.multitenancy.impl.CurrentTenantIdentifierResolverImpl;
import com.apd.phoenix.service.product.CatalogDataCacheManager;
import com.apd.phoenix.service.storage.api.StorageObject;
import com.apd.phoenix.service.storage.api.StorageObjectRequest;
import com.apd.phoenix.service.storage.api.StorageService;

@Stateless
@LocalBean
public class CatalogBp extends AbstractBp<Catalog> {

    @Inject
    private CatalogXItemBp catalogXItemBp;

    @Inject
    private SkuTypeDao skuTypeDao;

    @Inject
    private ItemPropertyTypeDao propertyTypeDao;

    @Inject
    private ItemBp itemBp;

    @Inject
    private StorageService storageService;

    @Inject
    private SyncItemResultBp syncItemResultBp;

    @Inject
    private CatalogUploadPurge catalogUploadPurge;

    @Inject
    private CatalogCsvPurge catalogCsvPurge;

    @Inject
    private CredentialDao credentialDao;

    @Inject
    private PendingSmartSearchDeletionBp deletionBp;

    @Inject
    private CatalogDataCacheManager catalogDataCacheManager;

    @Inject
    private CatalogQueueCount queueCount;

    private static final Logger LOGGER = LoggerFactory.getLogger(CatalogBp.class);

    private Properties awsProperties;

    @PostConstruct
    public void init() {
        awsProperties = TenantConfigRepository.getInstance().getProperties("aws.integration");
    }

    public enum SyncAction {
        NEW("NEW", "Create new catalog"), UPDATE("UPDATE", "Update this catalog"), VENDOR_UPDATE("VENDOR_UPDATE",
                "Create/update item master data "), CREATE_DIFF("CREATE_DIFF", "Create new catalog based on this one");

        private final String description;
        private final String header;

        private SyncAction(String header, String description) {
            this.header = header;
            this.description = description;
        }

        public String getDescription() {
            return this.description;
        }

        public String getHeader() {
            return this.header;
        }
    }

    public enum SyncError {
        ITEM_NOT_FOUND("An item with the given vendor name and SKU could not be found"), ACCOUNT_NOT_FOUND(
                "An account with the given name could not be found"), SKU_TYPE_NOT_FOUND(
                "A sku type with the given name could not be found"), PRICING_TYPE_NOT_FOUND(
                "A pricing type with the given name could not be found"), COLUMN_NOT_FOUND(
                "The specified column could not be found"), PROPERTY_VALUE_NOT_FOUND(
                "The property cannot be assigned the given value"), NOT_YET_CREATED(
                "An item with the given vendor name and SKU has not been created yet"), NUMBER_FORMAT(
                "A number was not formatted correctly"), DATE_FORMAT(
                "A date was not formatted correctly! Must be MM/DD/YYYY"), CATEGORY_NOT_FOUND(
                "The category could not be found"), CATEGORY_NOT_ASSIGNED(
                "You need to specify a category for the item, with the \"category\" column"), HIERARCHY_NOT_FOUND(
                "No hierarchy node with that ID was found"), UOM_NOT_FOUND(
                "No unit of measure with that name was found"), CLASSIFICATION_NOT_FOUND(
                "No item classification with that name was found"), UOM_NOT_ASSIGNED(
                "No unit of measure was specified, with the \"unit of measure\" column"), NAME_NOT_ASSIGNED(
                "You need to specify a name for the item, with the \"name\" column"), DESCRIPTION_NOT_ASSIGNED(
                "You need to specify a description for the item, with the \"description\" column"), BAD_PRICING_PARAMETER(
                "The value in the \"pricingParameter\" column isn't formatted correctly"), NO_APD_COST(
                "No APD cost was specified, with the \"APDcost\" column"), NO_LIST_PRICE(
                "No list price was specified, with the \"list price\" column"), CANT_CALCULATE_PRICE(
                "Unable to calculate the unit price for this customer catalog item"), CANT_CALCULATE_STREET(
                "Unable to calculate the street price for this vendor catalog item"), MATCHBOOK_MANUFACTURER_NOT_FOUND(
                "Could not find the manufacturer for one of the matchbook products"), CATALOG_INCORRECTLY_SPECIFIED(
                "The value in the \"catalog\" column must be the name of the catalog the item is being assigned to"), CATALOG_NOT_SPECIFIED(
                "You need to specify the catalog of the item, with the \"catalog\" column"), EXISTING_CATALOG_INCORRECT(
                "This item is already assigned to a different catalog"), VENDOR_SKU_NOT_ASSIGNED(
                "No vendor SKU was specified, with the \"vendor sku\" column"), CUSTOMER_SKU_NOT_ASSIGNED(
                "No customer SKU was specified, with the \"customerSku\" column"), DIFFERENT_VENDOR(
                "This item belongs to a different Vendor"), UNEXPECTED_ERROR("An unexpected error occurred: "), DELETED(
                "This item has been deleted!"), APD_COST_GT_LIST_PRICE("The APD cost is greater than list price"), ITEM_WEIGHT_NOT_FOUND(
                "No item weight was specified, with the \"item weight\" column"), DUPLICATE_CUSTOMER_SKU(
                "The customer SKU already exists on this catalog or a parent catalog"), INCORRECT_VENDOR(
                "The vendor specified in the \"vendor\" column is different than the vendor on the catalog"), NO_APD_SKU(
                "No Dealer SKU was specified"), NO_VENDOR("No vendor was specified"), NO_CATALOG_FOUND(
                "Could not locate catalog, please ensure that it what specified correctly on the CSV"), HIERARCHY_TYPE(
                "You need to specify hierarchy type"), BRAND_NOT_FOUND("Brand not found with ID "), WRONG_SEARCH_TERMS(
                "Attempted to set search terms outside of ETL processing. Use \"protected search terms\" instead.");

        private final String label;

        private SyncError(String label) {
            this.label = label;
        }

        public String getLabel() {
            return this.label;
        }
    }

    /**
     * This method is injected with the correct Dao for initialization
     * 
     * @param dao
     */
    @Inject
    public void initDao(CatalogDao dao) {
        this.dao = dao;
    }

    public boolean isDiffCsvExists(Catalog catalog) {
        return this.isFileExists(this.getDiffCsvPath(catalog));
    }

    public InputStream getDiffCsv(Catalog catalog) {
        return this.getFile(this.getDiffCsvPath(catalog));
    }

    public void setDiffCsv(String convoNumber, Catalog catalog, SyncAction action) throws IOException {
        File tempFile = getTempCsv(convoNumber);
        if (!tempFile.exists()) {
            return;
        }
        this.setFile(this.getDiffCsvPath(catalog), new FileInputStream(tempFile), tempFile.length());
    }

    @Inject
    private CsvUploadedItemDao csvUploadedItemDao;

    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public Catalog storeXmlCatalogFile() {
    	Catalog toReturn = null;
    	LOGGER.info("Starting scheduling of USSCO XML CSV");
    	File tempFile = getTempCsv("xml-temp-csv");
    	if (tempFile.exists()) {
    		tempFile.delete();
    	}
    	try (
	    		FileWriter fw = new FileWriter(tempFile.getAbsoluteFile());
	    		BufferedWriter bw = new BufferedWriter(fw);
    		) {
    		tempFile.createNewFile();
	    	List<String> linesToWrite;
	    	List<String> headers = csvUploadedItemDao.uploadedItemFields();
	    	if (headers.isEmpty()) {
	    		LOGGER.info("No USSCO XML items have been scheduled.");
	    		return toReturn;
	    	}
	    	bw.write(SyncAction.VENDOR_UPDATE.getHeader());
	    	bw.write("\n");
	    	for (int i = 0; i < headers.size(); i++) {
	    		bw.write(StringEscapeUtils.escapeCsv(headers.get(i)));
	    		if (i != headers.size() - 1) {
	    			bw.write(",");
	    		}
	    	}
	    	bw.write("\n");
	    	int numberOfLines = 0;
	    	do {
	    		linesToWrite = this.xmlCsvLinesToWrite(headers);
	    		for (String s : linesToWrite) {
	    			bw.write(s);
	    			bw.write("\n");
	    			numberOfLines++;
	    		}
	    	} while (!linesToWrite.isEmpty());
	    	LOGGER.info("USSCO XML has {} lines", numberOfLines);
    	}
    	catch (Exception e) {
    		LOGGER.error("Problem storing CSV from XML upload", e);
    		return toReturn;
    	}
    	try (
    			FileInputStream stream = new FileInputStream(tempFile)
    		) {
    		Catalog searchCatalog = new Catalog();
    		searchCatalog.setName("USSCO Vendor Catalog");
    		toReturn = this.searchByExactExample(searchCatalog, 0, 0).get(0);
    		this.setDiffCsv(stream, tempFile.length(), toReturn, SyncAction.VENDOR_UPDATE);
    		tempFile.delete();
    	}
    	catch (Exception e) {
    		LOGGER.error("Problem reading CSV from XML upload", e);
    	}
    	LOGGER.info("Completed scheduling of USSCO XML CSV");
    	return toReturn;
    }

    private List<String> xmlCsvLinesToWrite(List<String> headers) {
    	List<String> toReturn = new ArrayList<>();
    	List<CsvUploadedItem> items = csvUploadedItemDao.uploadedItems();
    	for (CsvUploadedItem item : items) {
    		Map<String, String> keyValueMap = new HashMap<>();
    		for (CsvUploadedItemField field : item.getCsvUploadedItemFields()) {
    			keyValueMap.put(field.getKey(), field.getValue());
    		}
    		StringBuilder row = new StringBuilder();
    		boolean firstEntry = true;
    		for (String header : headers) {
    			if (!firstEntry) {
    				row.append(",");
    			}
    			if (keyValueMap.containsKey(header)) {
    				row.append(StringEscapeUtils.escapeCsv(keyValueMap.get(header)));
    			}
				firstEntry = false;
    		}
    		toReturn.add(row.toString());
    		csvUploadedItemDao.delete(item.getId(), CsvUploadedItem.class);
    	}
    	return toReturn;
    }

    public void setDiffCsv(InputStream stream, long length, Catalog catalog, SyncAction action) throws IOException {
        this.setFile(this.getDiffCsvPath(catalog), stream, length);
    }

    public void deleteDiffCsv(Catalog catalog) {
        StorageObjectRequest request = new StorageObjectRequest();
        request.setPath(this.getDiffCsvPath(catalog));
        request.getAttributes().put("bucketName", awsProperties.getProperty("message.service.bucketName"));
        storageService.deleteObject(request);
    }

    public File getTempCsv(String convoNumber) {
        return new File("/var/tmp/" + convoNumber + "-" + CurrentTenantIdentifierResolverImpl.getCurrentTenant()
                + ".csv");
    }

    public void setTempCsv(InputStream fileStream, String convoNumber, SyncAction action) throws IOException {
	    String filename = "/var/tmp/" + convoNumber + "-" + CurrentTenantIdentifierResolverImpl.getCurrentTenant() + ".csv";
	    File tempDirectory = new File("/var/tmp");
	    File file = new File(filename);
	    if (!tempDirectory.exists()) {
	        tempDirectory.mkdir();
	    }
	    if (file.exists()) {
	        file.delete();
	    }
	    try (
	    		FileWriter fw = new FileWriter(file.getAbsoluteFile());
	    		BufferedWriter bw = new BufferedWriter(fw);
	    	) {
		    file.createNewFile();
		    bw.write(action.getHeader());
		    bw.write("\n");
		    IOUtils.copy(fileStream, bw);
	    }
    }

    public void deleteTempCsv(String convoNumber) {
        File tempCsv = this.getTempCsv(convoNumber);
        if (tempCsv.exists()) {
            tempCsv.delete();
        }
    }

    public boolean isCatalogCsvExists(Catalog catalog) {
        return this.isFileExists(this.getCatalogCsvPath(catalog));
    }

    public InputStream getCatalogCsv(Catalog catalog) {
        return this.getFile(this.getCatalogCsvPath(catalog));
    }

    //tenant ID has to be set manually on asynchronous EJB methods, since
    //the tenant ID is stored in a static threadlocal variable, and 
    //@Asynchronous calls occur in a different thread.
    @Asynchronous
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public void setCatalogCsv(Catalog input, Long tenantId) {
        CurrentTenantIdentifierResolverImpl.setCurrentTenant(tenantId);
        LOGGER.info("purging records for catalogid={}", input.getId());
        input = eagerLoad(input);
        input.setLastProcessStart(new Date());
        this.update(input);
        catalogUploadPurge.purgeOldRecords(input.getId());
        catalogCsvPurge.purgeOldRecords(input.getId());
        LOGGER.info("*************SETTING CATALOG CSV");
        if (input.getVendor() == null) {
            generateCustomerCatalogCsv(input);
        }
        else {
            generateVendorCatalogCsv(input);
        }

        input = this.findById(input.getId(), Catalog.class);
        input.setProcessedQueueCount(queueCount.getCatalogProcessedCount());
        this.update(input);
        LOGGER.info("*************ENDING CATALOG CSV");
    }

    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    private void generateCustomerCatalogCsv(Catalog input) {
    	StringBuilder sb = new StringBuilder();
    	sb.append("vendor,dealer sku,discontinued,coreItemStartDate,coreItemExpirationDate,catalog,customerSku,customerUom,current price,pricingType,pricingParameter,favorites lists,solrOverrideDescription,category,APD cost,list price,street,");
        List<ItemPropertyType> propertyTypeList = propertyTypeDao.findAll(ItemPropertyType.class, 0, 0);
        for (ItemPropertyType type : propertyTypeList) {
            sb.append(type.getName());
            sb.append(",");
        }
        sb.append("\n");
        int batch = 0;
        String correlationId = UUID.randomUUID().toString();
        List<Long> batchList = catalogXItemBp.batchForJMS(input, batch);
        Long catalogSize = getCatalogSize(input);
        while (batchList.size() > 0) {
        	CatalogCsvMessage message = new CatalogCsvMessage();
        	message.setCatalogId(input.getId());
        	message.setCorrelationId(correlationId);
        	message.setPropertyTypeList(new ArrayList<>(propertyTypeList));
        	message.setHeader(sb.toString());
        	message.setIds(new ArrayList<>(batchList));
        	message.setTotalItems(catalogSize);
        	message.setCatalogCsvType(CatalogCsvTypeEnum.CUSTOMER_CATALOG_CSV);
        	catalogCsvMessageEntryPoint.scheduleRequest(message);
            batch++;
            batchList = catalogXItemBp.batchForJMS(input, batch);
        }
    }

    @Inject
    private CustomerCostBp costBp;

    @Inject
    CatalogCsvMessageEntryPoint catalogCsvMessageEntryPoint;

    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    private void generateVendorCatalogCsv(Catalog input) {
    	StringBuilder sb = new StringBuilder();
        sb.append("vendor,dealer sku,hierarchy,name,description,smart search dealer description,category,unit of measure,image URLs,discontinued,replacementSku,manufacturer,brandName,protected search terms,search terms,minimum,multiples,lastCostChangeDate,");
        ArrayList<ItemPropertyType> propertyTypeList = new ArrayList<>(propertyTypeDao.findAll(ItemPropertyType.class, 0, 0));
        ArrayList<SkuType> skuTypeList = new ArrayList<>(skuTypeDao.findAll(SkuType.class, 0, 0));
        ArrayList<String> customerList = new ArrayList<String>();
        Set<String> customerSet = new HashSet<String>();
        for (ItemPropertyType type : propertyTypeList) {
            sb.append(type.getName());
            sb.append(",");
        }
        sb.append("classifications,specifications,");
        SkuType toRemove = null;
        for (SkuType type : skuTypeList) {
            if (!type.getName().equals("dealer")) {
                sb.append(type.getName());
                sb.append(" sku,");
            }
            else {
                toRemove = type;
            }
        } 
        sb.append("matchbook,");
        skuTypeList.remove(toRemove);
        for (Account account : costBp.costList(input)) {
            if (!customerSet.contains(account.getName())) {
                sb.append(account.getName());
                sb.append(" cost,");
                customerSet.add(account.getName());
                customerList.add(account.getName());
            }
        }
        sb.append("\n");
        LOGGER.info("*********CALLING itemBp.batchForJms");
        String header = sb.toString();
        int batch = 0;
        Long catalogSize = getCatalogSize(input);
        String correlationId = UUID.randomUUID().toString();
        ArrayList<Long> batchList = itemBp.batchForJMS(input, batch);
        while (batchList.size() > 0) {
            CatalogCsvMessage message = new CatalogCsvMessage();
            message.setCustomerList(customerList);
            message.setPropertyTypeList(propertyTypeList);
            message.setSkuTypeList(skuTypeList);
            message.setIds(batchList);
            message.setCorrelationId(correlationId);
            message.setHeader(header);
            message.setCatalogId(input.getId());
            message.setTotalItems(catalogSize);
            message.setCatalogCsvType(CatalogCsvTypeEnum.VENDOR_CATALOG);
            catalogCsvMessageEntryPoint.scheduleRequest(message);
            batch++;
            batchList = itemBp.batchForJMS(input, batch);
        }
    }

    public boolean isReportCsvExists(Catalog catalog) {
        return this.isFileExists(this.getReportCsvPath(catalog));
    }

    public InputStream getReportCsv(Catalog catalog) {
        return this.getFile(this.getReportCsvPath(catalog));
    }

    @TransactionTimeout(value = 6000)
    public void setReportCsv(Catalog catalog, String correlationId, SyncItemResultSummary summary) {
        StringBuffer sb = new StringBuffer();

        if (summary.getCreated() != 0) {
            sb.append("Created," + summary.getCreated() + "\n");
        }
        if (summary.getUpdated() != 0) {
            sb.append("Updated," + summary.getUpdated() + "\n");
        }
        if (summary.getFailures() != 0) {
            sb.append("Failures," + summary.getFailures() + "\n");
        }
        if (summary.getDiscontinued() != 0) {
        	sb.append("Discontinued," + summary.getDiscontinued() + "\n");
        }
        if (summary.getZeroPrice() != 0) {
        	sb.append("Zero price items," + summary.getZeroPrice() + "\n");
        }
        if (summary.getWarnings() != 0) {
        	sb.append("Warnings," + summary.getWarnings() + "\n");
        }
        if (summary.getErrors() != 0) {
        	sb.append("Errors," + summary.getErrors() + "\n");
        }
        sb.append("vendor,sku,errors,warnings\n");
        //Batch the SyncItemResults

        int batch = 0;
        List<Long> batchList = syncItemResultBp.getResults(correlationId, batch);
        Long count = syncItemResultBp.getCount(correlationId);

        while (batchList.size() > 0) {
        	CatalogCsvMessage message = new CatalogCsvMessage();
        	message.setCatalogId(catalog.getId());
        	message.setCorrelationId(correlationId);
        	message.setHeader(sb.toString());
        	message.setIds(new ArrayList<>(batchList));
        	message.setTotalItems(count);
        	message.setCatalogCsvType(CatalogCsvTypeEnum.REPORT_CSV);
        	catalogCsvMessageEntryPoint.scheduleRequest(message);
            batch++;
            batchList = syncItemResultBp.getResults(correlationId, batch);
        }
    }

    public void uploadCatalogCsv(Catalog catalog, CatalogCsvTypeEnum type, InputStream value, long length) {
        String path = null;
        switch (type) {
            case CUSTOMER_CATALOG_CSV:
            case VENDOR_CATALOG:
                path = this.getCatalogCsvPath(catalog);
                break;
            case SMART_OCI_CATALOG_CSV:
                path = this.getSmartOciPath(catalog);
                break;
            case REPORT_CSV:
                path = this.getReportCsvPath(catalog);
                break;
        }
        this.setFile(path, value, length);
    }

    public boolean isUploadScheduleExists() {
        return this.isFileExists("catalogs/schedule" + "-" + CurrentTenantIdentifierResolverImpl.getCurrentTenant()
                + ".csv");
    }

    public InputStream getUploadSchedule() {
        return this
                .getFile("catalogs/schedule" + "-" + CurrentTenantIdentifierResolverImpl.getCurrentTenant() + ".csv");
    }

    public void setUploadSchedule(List<String> lines) {
        String filename = "/var/tmp/schedule" + "-" + CurrentTenantIdentifierResolverImpl.getCurrentTenant() + ".csv";
        File tempDirectory = new File("/var/tmp");
        File file = new File(filename);
        if (!tempDirectory.exists()) {
            tempDirectory.mkdir();
        }
        if (file.exists()) {
            file.delete();
        }
        try (
                FileWriter fw = new FileWriter(file.getAbsoluteFile());
                BufferedWriter bw = new BufferedWriter(fw);
            ) {
        	for (String line : lines) {
        		bw.write(line);
        		bw.write("\n");
        	}
        	bw.close();
        	fw.close();
        	
        	this.setFile("catalogs/schedule" + "-" + CurrentTenantIdentifierResolverImpl.getCurrentTenant() + ".csv", new FileInputStream(file), file.length());
            
        }
        catch (Exception e) {
            LOGGER.error("An error occured:", e);;
        }
    }

    private boolean isFileExists(String path) {
        try {
            StorageObjectRequest request = new StorageObjectRequest();
            request.setPath(path);
            request.getAttributes().put("bucketName", awsProperties.getProperty("message.service.bucketName"));
            StorageObject object = storageService.retrieveObject(request);
            if (object == null) {
            	return false;
            }
            try (InputStream stream = object.getContent()) {
            	stream.close();
            }
            catch (Exception e) {
            	//do nothing
            }
            return true;
        }
        catch (Exception e) {
        	LOGGER.error("Problem getting the storage object: " + path, e);
            return false;
        }
    }

    private InputStream getFile(String path) {
        StorageObjectRequest request = new StorageObjectRequest();
        request.setPath(path);
        request.getAttributes().put("bucketName", awsProperties.getProperty("message.service.bucketName"));
        StorageObject object = storageService.retrieveObject(request);
        if (object == null) {
            return null;
        }
        return object.getContent();
    }

    private void setFile(String path, InputStream value, long length) {
        StorageObject storageObject = new StorageObject();
        storageObject.setPath(path);
        storageObject.setContent(value);
        storageObject.getAttributes().put("bucketName", awsProperties.getProperty("message.service.bucketName"));
        storageObject.setContentLength(length);
        storageObject.setContentType("text/plain");
        if (this.isFileExists(path)) {
            storageService.updateObject(storageObject);
        }
        else {
            storageService.createObject(storageObject);
        }
    }

    private String getDiffCsvPath(Catalog catalog) {
        return "catalogs/" + catalog.getId().hashCode() + "-" + CurrentTenantIdentifierResolverImpl.getCurrentTenant()
                + "-diff.csv";
    }

    private String getCatalogCsvPath(Catalog catalog) {
        return "catalogs/" + catalog.getId().hashCode() + "-" + CurrentTenantIdentifierResolverImpl.getCurrentTenant()
                + ".csv";
    }

    private String getReportCsvPath(Catalog catalog) {
        return "catalogs/" + catalog.getId().hashCode() + "-" + CurrentTenantIdentifierResolverImpl.getCurrentTenant()
                + "-report.csv";
    }

    public InputStream getVendorTemplate() {
        //TODO: create template
        return this.getFile("catalogs/vendorTemplate.csv");
    }

    public InputStream getCustomerTemplate() {
        //TODO: create template
        return this.getFile("catalogs/customerTemplate.csv");
    }

    public InputStream getSpecifiedCsv(String catalogCsvName) {
        return this.getFile("catalogs/" + catalogCsvName);
    }

    public List<FavoritesList> getFavoritesLists(long credentialId) {
        return ((CatalogDao) dao).getFavoritesLists(this.credentialDao.findById(credentialId, Credential.class)
                .getCatalog());
    }

    public List<FavoritesList> getFavoritesLists(Catalog catalog) {
        return ((CatalogDao) dao).getFavoritesLists(catalog);
    }

    public long getCatalogSize(Catalog catalog) {
        return ((CatalogDao) dao).getCatalogSize(catalog);
    }

    public Catalog hydrateCustomerName(Catalog catalog) {
        return ((CatalogDao) dao).hydrateCustomerName(catalog);
    }

    public List<Long> getParentCatalogIdList(long catalogID) {
        return ((CatalogDao) dao).getParentCatalogIdList(catalogID);
    }

    public List<Long> getCatalogUidIdList(long catalogID) {
        return ((CatalogDao) dao).getCatalogUidIdList(catalogID);
    }

    public List<Long> getCatalogCoreUidIdList(long catalogID) {
        return ((CatalogDao) dao).getCatalogCoreUidIdList(catalogID);
    }

    //tenant ID has to be set manually on asynchronous EJB methods, since
    //the tenant ID is stored in a static threadlocal variable, and 
    //@Asynchronous calls occur in a different thread.
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    @Asynchronous
    public void reindexCatalog(Catalog catalog, Long tenantId) {
    	CurrentTenantIdentifierResolverImpl.setCurrentTenant(tenantId);
    	if (catalog == null || catalog.getId() == null) {
    		LOGGER.warn("Null catalog atomically updated");
    		return;
    	}
        int batch = 0;
        List<Long> batchList = new ArrayList<Long>();
        if (catalog.getVendor() == null) {
        	batchList = catalogXItemBp.batchForJMS(catalog, batch);
        } else {
        	batchList = itemBp.batchForJMS(catalog, batch);
        }
        while (batchList.size() > 0) {
        	for (Long itemId : batchList) {
        		try {
        			if (catalog.getVendor() == null) {
        				CatalogXItemListener.addUpdateSolrDocument(itemId);
        			} else {
        				ItemListener.addUpdateSolrDocument(itemId);
        			}
				} catch (NamingException | JMSException e) {
					if (LOGGER.isDebugEnabled()) {
						LOGGER.debug("Error sending item update", e);
					}
				}
        	}
        	batch++;
            if (catalog.getVendor() == null) {
            	batchList = catalogXItemBp.batchForJMS(catalog, batch);
            } else {
            	batchList = itemBp.batchForJMS(catalog, batch);
            }
        }
    }

    //tenant ID has to be set manually on asynchronous EJB methods, since
    //the tenant ID is stored in a static threadlocal variable, and 
    //@Asynchronous calls occur in a different thread.
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    @Asynchronous
    public void refreshCache(Long tenantId) {
    	CurrentTenantIdentifierResolverImpl.setCurrentTenant(tenantId);
        int batch = 0;
        List<Long> batchList = new ArrayList<Long>();
        batchList = catalogXItemBp.batchAllForJMS(batch);
        while (batchList.size() > 0) {
        	for (Long itemId : batchList) {
        		try {
        			CatalogXItemListener.addUpdateSolrDocument(itemId);
				} catch (NamingException | JMSException e) {
					if (LOGGER.isDebugEnabled()) {
						LOGGER.debug("Error sending item update", e);
					}
				}
        	}
        	batch++;
            batchList = catalogXItemBp.batchAllForJMS(batch);
        }
    }

    //tenant ID has to be set manually on asynchronous EJB methods, since
    //the tenant ID is stored in a static threadlocal variable, and 
    //@Asynchronous calls occur in a different thread.
    @Asynchronous
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public void setSmartOci(Catalog input, Long tenantId) {
        CurrentTenantIdentifierResolverImpl.setCurrentTenant(tenantId);
        LOGGER.info("Generating Smart OCI for catalogid={}", input.getId());
        input = eagerLoad(input);
        input.setLastProcessStart(new Date());
        this.update(input);

        generateSmartOci(input);

        input = this.findById(input.getId(), Catalog.class);
        input.setProcessedQueueCount(queueCount.getCatalogProcessedCount());
        this.update(input);

        LOGGER.info("Done generating Smart OCI");
    }

    public void generateSmartOci(Catalog input) {
    	StringBuilder sb = new StringBuilder();
    	sb.append("#ItemNumber,NEW_ITEM-VENDORMAT,NEW_ITEM-DESCRIPTION,NEW_ITEM-MATGROUP,NEW_ITEM-UNIT,NEW_ITEM-PRICE,NEW_ITEM-CURRENCY,NEW_ITEM-MANUFACTMAT,NEW_ITEM-MANUFACTCODE,NEW_ITEM-LONGTEXT,NEW_ITEM-IMAGE,");
    	sb.append("APD_PART_NUM (FIXED):1,STAPLES_REF_NUM (FIXED):2");
        ArrayList<ItemPropertyType> propertyTypeList = new ArrayList<>(propertyTypeDao.findAll(ItemPropertyType.class, 0, 0));
        ArrayList<SkuType> skuTypeList = new ArrayList<>(skuTypeDao.findAll(SkuType.class, 0, 0));
                      
        sb.append("\n");
        LOGGER.info("Calling catalogXItemBp.batchForJMS by batch Id's ");
        String header = sb.toString();
        int batch = 0;
        Long catalogSize = getCatalogSize(input);
        String correlationId = UUID.randomUUID().toString();
        List<Long> batchList = catalogXItemBp.batchForJMS(input, batch);
        while (batchList.size() > 0) {
            CatalogCsvMessage message = new CatalogCsvMessage();
            message.setPropertyTypeList(propertyTypeList);
            message.setSkuTypeList(skuTypeList);
            message.setIds(new ArrayList<>(batchList));
            message.setCorrelationId(correlationId);
            message.setHeader(header);
            message.setCatalogId(input.getId());
            message.setTotalItems(catalogSize);
            message.setCatalogCsvType(CatalogCsvTypeEnum.SMART_OCI_CATALOG_CSV);
            catalogCsvMessageEntryPoint.scheduleRequest(message);
            batch++;
            batchList = catalogXItemBp.batchForJMS(input, batch);
        }
        LOGGER.info("Done catalogXItemBp.batchForJMS by batch Id's ");
    }

    public boolean isSmartOciExists(Catalog catalog) {
        return this.isFileExists(this.getSmartOciPath(catalog));
    }

    private String getSmartOciPath(Catalog catalog) {
        return "catalogs/" + catalog.getId().hashCode() + "-" + CurrentTenantIdentifierResolverImpl.getCurrentTenant()
                + "-SmartOci.csv";
    }

    public InputStream getSmartOci(Catalog catalog) {
        return this.getFile(this.getSmartOciPath(catalog));
    }

    public List<Long> getCatalogsToRehash() {
        return ((CatalogDao) dao).getCatalogsToRehash();
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public void setNewHashOnCatalog(Long catalogId) {
        ((CatalogDao) dao).setNewHashOnCatalog(catalogId);
    }

    public List<Catalog> getCatalogsWithWrongListUid() {
        return ((CatalogDao) dao).getCatalogsWithWrongListUid();
    }

    public Catalog update(Catalog toUpdate) {
        Catalog toReturn = super.update(toUpdate);
        //Clears the cache
        if (toReturn != null && toReturn.getId() != null) {
            catalogDataCacheManager.setCachedCatalogData(toReturn.getId(), toReturn.getSearchType(), null);
        }
        return toReturn;
    }

    //tenant ID has to be set manually on asynchronous EJB methods, since
    //the tenant ID is stored in a static threadlocal variable, and 
    //@Asynchronous calls occur in a different thread.
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    @Asynchronous
    public void processSmartSearchList(Catalog catalog, Long listName, Long coreListName, Long tenantId) {
    	CurrentTenantIdentifierResolverImpl.setCurrentTenant(tenantId);
    	if (catalog == null || catalog.getId() == null) {
    		LOGGER.warn("Null catalog atomically updated");
    		return;
    	}
        int batch = 0;
        List<Long> batchList = catalogXItemBp.batchForJMS(catalog, batch);
        while (batchList.size() > 0) {
        	for (Long itemId : batchList) {
        		try {
        			CatalogXItemListener.addInsertInSmartsearch(itemId, listName, coreListName);
				} catch (NamingException | JMSException e) {
					if (LOGGER.isDebugEnabled()) {
						LOGGER.debug("Error sending item update", e);
					}
				}
        	}
        	batch++;
            batchList = catalogXItemBp.batchForJMS(catalog, batch);
        }
        int deletionBatch = 0;
        List<PendingSmartSearchDeletion> deletionBatchList = deletionBp.batchForJMS(catalog, deletionBatch);
        while (deletionBatchList.size() > 0) {
        	for (PendingSmartSearchDeletion deletion : deletionBatchList) {
        		try {
        			CatalogXItemListener.addDeleteInSmartsearch(deletion.getItem().getId(), listName);
				} catch (NamingException | JMSException e) {
					if (LOGGER.isDebugEnabled()) {
						LOGGER.debug("Error sending item update", e);
					}
				}
        	}
        	deletionBatch++;
        	deletionBatchList = deletionBp.batchForJMS(catalog, deletionBatch);
        }
        deletionBp.deletePendingForCatalog(catalog);
    }

    public static class SmartSearchInsertMessage implements Serializable {

        private static final long serialVersionUID = -7196900627374656668L;
        private Long catalogId;
        private Long listName;
        private Long coreListName;

        public SmartSearchInsertMessage() {
        }

        public SmartSearchInsertMessage(Long catalogId, Long listName, Long coreListName) {
            this.catalogId = catalogId;
            this.listName = listName;
            this.coreListName = coreListName;
        }

        public Long getCatalogId() {
            return catalogId;
        }

        public void setCatalogId(Long catalogId) {
            this.catalogId = catalogId;
        }

        public Long getListName() {
            return listName;
        }

        public void setListName(Long listName) {
            this.listName = listName;
        }

        public Long getCoreListName() {
            return coreListName;
        }

        public void setCoreListName(Long coreListName) {
            this.coreListName = coreListName;
        }
    }

    public Catalog hydrateCarousel(Catalog catalog) {
        return ((CatalogDao) this.dao).hydrateCarousel(catalog);
    }
}
