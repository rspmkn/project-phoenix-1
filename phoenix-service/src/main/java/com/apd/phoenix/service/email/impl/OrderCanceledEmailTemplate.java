package com.apd.phoenix.service.email.impl;

import static com.apd.phoenix.service.model.CredentialPropertyType.CredentialPropertyTypeEnum.USPS_USSCO_ADDITIONAL_PROCESSING;
import java.io.IOException;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import org.apache.commons.lang.StringUtils;
import com.apd.phoenix.service.business.CredentialBp;
import com.apd.phoenix.service.business.CustomerOrderBp;
import com.apd.phoenix.service.email.api.Attachment;
import com.apd.phoenix.service.email.api.EmailTemplate;
import com.apd.phoenix.service.model.CustomerOrder;
import com.apd.phoenix.service.model.LineItem;
import com.apd.phoenix.service.model.dto.LineItemDto;
import com.apd.phoenix.service.model.dto.LineItemXShipmentDto;
import com.apd.phoenix.service.model.dto.PurchaseOrderDto;
import com.apd.phoenix.service.model.dto.ShipmentDto;
import freemarker.template.TemplateException;

@Stateless
@LocalBean
public class OrderCanceledEmailTemplate extends EmailTemplate<Object> {

    private static final String TEMPLATE = "order.canceled";

    @Inject
    private CustomerOrderBp customerOrderBp;

    @Inject
    private CredentialBp credentialBp;

    @Override
	public String createBody(Object content) throws IOException, TemplateException {
    	PurchaseOrderDto purchaseOrderDto = null;
    	//for legacy orders, a purchaseOrderDto is the content type. Newer orders use a shipmentDto, indicating which items are cancelled
    	if (content instanceof PurchaseOrderDto) {
    		purchaseOrderDto = (PurchaseOrderDto) content;
    	}
    	else if (content instanceof ShipmentDto) {
    		purchaseOrderDto = ((ShipmentDto) content).getCustomerOrderDto();
    	}
		Map<String, Object> params = new HashMap<>();
		
		params.put("orderDate", purchaseOrderDto.getOrderDate());
		if(purchaseOrderDto.getShipToAddressDto() != null && purchaseOrderDto.getShipToAddressDto().getMiscShipToDto() != null && StringUtils.isNotBlank(purchaseOrderDto.getShipToAddressDto().getMiscShipToDto().getRequesterName())){
			params.put("requesterName", purchaseOrderDto.getShipToAddressDto().getMiscShipToDto().getRequesterName()); 
		}
		else{
			params.put("requesterName", purchaseOrderDto.getSystemUserDto().getLogin());
		}
		
		List<ItemCancelled> itemList = new ArrayList<>();
    	//pulls every item off the order, if only a purchase order is given
		if (content instanceof PurchaseOrderDto) {
			for (LineItemDto item : purchaseOrderDto.getItems()){
				if (item.getQuantityCancelled() != null && item.getQuantityCancelled().compareTo(BigInteger.ZERO) > 0){
					ItemCancelled toAdd = new ItemCancelled(); 
					toAdd.setItem(item);
					toAdd.setQuantity(item.getQuantityCancelled());
					itemList.add(toAdd);
				}
			}
		}
		//if a shipmentDto is given, only displays the items on that shipmentDto
		else if (content instanceof ShipmentDto) {
			for (LineItemXShipmentDto item : ((ShipmentDto) content).getLineItemXShipments()) {
				ItemCancelled toAdd = new ItemCancelled(); 
				toAdd.setItem(item.getLineItemDto());
				toAdd.setQuantity(item.getQuantity());
				itemList.add(toAdd);
			}
		}
		params.put("cancelledItems", itemList);
		
		CustomerOrder order = customerOrderBp.searchByApdPo(purchaseOrderDto.getApdPoNumber());

		params.put("csrEmail", StringUtils.defaultIfBlank(order.getCredential().getCsrEmail(), this.getTenantPropertyByName(TENANT_CUSTOMER_SERVICE_EMAIL)));
		params.put("csrPhone", StringUtils.defaultIfBlank(order.getCredential().getCsrPhone(), this.getTenantPropertyByName(TENANT_CUSTOMER_SERVICE_PHONE)));

		params.put("customerPo", order.getCustomerPo() != null ? order.getCustomerPo().getValue() : null);

		params.put("uspsIrEmail", isUspsOrder(order) && isAllItemsRejected(order, itemList));
		
		return this.create(params);
	}

    public String getTemplate() {
        return TEMPLATE;
    }

    @Override
    public Attachment createAttachment(Object content) {
        // no attachment for cancel emails
        return null;
    }

    private boolean isUspsOrder(CustomerOrder order) {
        return !StringUtils.isEmpty(credentialBp.getCredetialProperty(USPS_USSCO_ADDITIONAL_PROCESSING.getValue(),
                order.getCredential()));
    }

    private boolean isAllItemsRejected(CustomerOrder order, List<ItemCancelled> itemList) {
        boolean toReturn = true;
        for (LineItem item : order.getItems()) {
            for (ItemCancelled itemCancelled : itemList) {
                if (item.matchesItemOnOrder(itemCancelled.getItem()) && item.getHasBeenRejected() != null
                        && !item.getHasBeenRejected()) {
                    toReturn = false;
                }
            }
        }
        return toReturn;
    }

    public class ItemCancelled {

        private LineItemDto item;
        private BigInteger quantity;

        public LineItemDto getItem() {
            return item;
        }

        public void setItem(LineItemDto item) {
            this.item = item;
        }

        public BigInteger getQuantity() {
            return quantity;
        }

        public void setQuantity(BigInteger quantity) {
            this.quantity = quantity;
        }
    }

}
