package com.apd.phoenix.service.business;

import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import com.apd.phoenix.service.model.CustomerCreditInvoice;
import com.apd.phoenix.service.model.CustomerInvoice;
import com.apd.phoenix.service.model.Invoice;
import com.apd.phoenix.service.persistence.jpa.InvoiceDao;

/**
 * This class provides business process methods for Addresses.
 * 
 * @author RHC
 *
 */
@Stateless
@LocalBean
public class InvoiceBp extends AbstractBp<Invoice> {

    @Inject
    public void initDao(InvoiceDao dao) {
        this.dao = dao;
    }

    public List<CustomerInvoice> findYesterdaysMarquetteInvoices() {
        return ((InvoiceDao) this.dao).findYesterdaysMarquetteInvoices();
    }

    public List<CustomerCreditInvoice> findYesterdaysMarquetteCreditInvoices() {
        return ((InvoiceDao) this.dao).findYesterdaysMarquetteCreditInvoices();
    }

    public List<Invoice> findCustomerInvoiceByInvoiceNumber(String invoiceNumber) {
        return ((InvoiceDao) this.dao).findCustomerInvoiceByInvoiceNumber(invoiceNumber);
    }

}
