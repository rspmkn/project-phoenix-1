package com.apd.phoenix.service.business;

import java.util.Collections;
import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import com.apd.phoenix.service.model.MilitaryZip;
import com.apd.phoenix.service.model.TaxRateLog;
import com.apd.phoenix.service.model.ZipPlusFour;
import com.apd.phoenix.service.model.TaxRateLog.TaxRateLogAction;
import com.apd.phoenix.service.model.TaxRateLog.TaxRateLogStatus;
import com.apd.phoenix.service.model.TaxRateLog.TaxRateLogType;
import com.apd.phoenix.service.persistence.jpa.MilitaryZipDao;

/**
 * This class provides business process methods for MilitaryZip.
 * 
 * @author RHC
 * 
 */
@Stateless
@LocalBean
public class MilitaryZipBp extends AbstractBp<MilitaryZip> {

    @Inject
    private TaxRateLogBp logBp;

    /**
     * This method is injected with the correct Dao for initialization
     * 
     * @param dao
     */
    @Inject
    public void initDao(MilitaryZipDao dao) {
        this.dao = dao;
    }

    public void createMilitaryZips(List<MilitaryZip> zips) {
        for (MilitaryZip o : zips) {
            if (o == null) {
                continue;
            }
            try {
                this.create(o);
                logBp.create(new TaxRateLog(TaxRateLogType.MILITARY_ZIP, TaxRateLogAction.INSERT,
                        TaxRateLogStatus.SUCCESS, o.getZip(), o.getUspsCity()));
            }
            catch (Exception e) {
                LOG.error("Error when adding military zip, enable debug logging to view stack trace");
                if (LOG.isDebugEnabled()) {
                    LOG.debug("Stack trace", e);
                }
                logBp.create(new TaxRateLog(TaxRateLogType.MILITARY_ZIP, TaxRateLogAction.INSERT,
                        TaxRateLogStatus.FAILURE, o.getZip(), o.getUspsCity()));
            }
        }
    }

    public void deleteMilitaryZip(MilitaryZip zip) {
        MilitaryZip searchZip = new MilitaryZip();
        searchZip.setZip(zip.getZip());
        searchZip.setCity(zip.getCity());
        searchZip.setCounty(zip.getCounty());
        searchZip.setState(zip.getState());
        searchZip.setUspsCity(zip.getUspsCity());
        List<MilitaryZip> searchList = this.searchByExactExample(searchZip, 0, 0);
        for (MilitaryZip foundZip : searchList) {
            try {
                this.delete(foundZip.getId(), MilitaryZip.class);
                logBp.create(new TaxRateLog(TaxRateLogType.MILITARY_ZIP, TaxRateLogAction.DELETE,
                        TaxRateLogStatus.SUCCESS, zip.getZip(), zip.getUspsCity()));
            }
            catch (Exception e) {
                LOG.error("Error when deleting military zip, enable debug logging to view stack trace");
                if (LOG.isDebugEnabled()) {
                    LOG.debug("Stack trace", e);
                }
                logBp.create(new TaxRateLog(TaxRateLogType.MILITARY_ZIP, TaxRateLogAction.DELETE,
                        TaxRateLogStatus.FAILURE, zip.getZip(), zip.getUspsCity()));
            }
        }
    }

    public List<MilitaryZip> getMilitaryZips(String zip) {
        MilitaryZip searchZip = new MilitaryZip();
        searchZip.setZip(zip);
        List<MilitaryZip> toReturn = this.searchByExactExample(searchZip, 0, 0);

        Collections.sort(toReturn, new EntityComparator());

        return toReturn;
    }
}
