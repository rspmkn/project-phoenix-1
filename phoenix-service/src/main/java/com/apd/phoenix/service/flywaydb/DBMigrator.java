package com.apd.phoenix.service.flywaydb;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Set;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.EJBException;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.sql.DataSource;
import org.flywaydb.core.Flyway;
import org.flywaydb.core.api.MigrationInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.core.utility.TenantConfigRepository;

@Startup
@Singleton
@TransactionManagement(TransactionManagementType.BEAN)
public class DBMigrator {

    @Resource(name = "java:jboss/datasources/Phoenix")
    DataSource ds;

    private static final Logger LOG = LoggerFactory.getLogger(DBMigrator.class);

    private static final String PHOENIX_LOCATION = "database/migrations";

    private static final String JBPM_LOCATION = "database/jbpm-migrations";

    @PostConstruct
    private void onStartup() {
        if (ds == null) {
            LOG.error("no datasource found to execute the db migrations!");
            throw new EJBException("no datasource found to execute the db migrations!");
        }

        Set<String> tenants = TenantConfigRepository.getInstance().getTenantNames();

        for (String tenant : tenants) {
            String phoenixSchema = TenantConfigRepository.getInstance().getTenantPropertiesByName(tenant)
                    .getSchemaName();
            this.migrateTenant(phoenixSchema.toUpperCase(), PHOENIX_LOCATION);

            String jbpmSchema = TenantConfigRepository.getInstance().getTenantPropertiesByName(tenant)
                    .getJbpmSchemaName();
            this.migrateTenant(jbpmSchema.toUpperCase(), JBPM_LOCATION);
        }
    }

    private void migrateTenant(String schema, String location) {
    	
    	String maintenanceUser = TenantConfigRepository.getInstance().getProperty("project.state", "dbConnectionUsername");
    	
    	LOG.info("Obtaining database lock for check and (if necessary) migration of schema " + schema);
    	try (Connection connection = ds.getConnection()) {
    		try {
	    		connection.setAutoCommit(false);
	    		PreparedStatement lockStatement = connection.prepareStatement("LOCK TABLES " + maintenanceUser + ".MIGRATION_LOCK WRITE");
	    		lockStatement.execute();
	
	            Flyway flyway = new Flyway();
	            flyway.setDataSource(ds);
	            flyway.setLocations(location);
	            flyway.setSchemas(schema);
	            flyway.setBaselineOnMigrate(true);
	            flyway.setOutOfOrder(true);
	            for (MigrationInfo i : flyway.info().all()) {
	                LOG.info("migrate task: " + i.getVersion() + " : " + i.getDescription() + " from file: " + i.getScript());
	            }
	            flyway.migrate();
	            connection.commit();
    		} catch (Exception e) {
    			LOG.error("Error migrating database", e);
    		}
    		PreparedStatement unlockStatement = connection.prepareStatement("UNLOCK TABLES");
    		unlockStatement.execute();
    	} catch (SQLException e) {
    		LOG.error("Error getting connection", e);
		}
    }
}
