/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.service.model.dto;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author nreidelb
 */
public class StopDto implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -3123676769160992039L;

    private Date deliveryDate;
    private SiteDto siteDto;
    private DeliveryDto deliveryDto;

    public Date getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDate(Date deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    public SiteDto getSiteDto() {
        return siteDto;
    }

    public void setSiteDto(SiteDto siteDto) {
        this.siteDto = siteDto;
    }

    public DeliveryDto getDeliveryDto() {
        return deliveryDto;
    }

    public void setDeliveryDto(DeliveryDto deliveryDto) {
        this.deliveryDto = deliveryDto;
    }

}
