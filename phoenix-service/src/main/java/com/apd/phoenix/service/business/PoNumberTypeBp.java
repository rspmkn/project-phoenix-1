package com.apd.phoenix.service.business;

import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import com.apd.phoenix.service.model.PoNumberType;
import com.apd.phoenix.service.persistence.jpa.PoNumberTypeDao;

/**
 * This class provides business process methods for Addresses.
 * 
 * @author RHC
 *
 */
@Stateless
@LocalBean
public class PoNumberTypeBp extends AbstractBp<PoNumberType> {

    public static final String APD_PO_TYPE = "APD";
    public static final String CUSTOMER_PO_TYPE = "Customer";

    /**
     * This method is injected with the correct Dao for initialization
     * 
     * @param dao
     */
    @Inject
    public void initDao(PoNumberTypeDao dao) {
        this.dao = dao;
    }

    public PoNumberType searchByName(String name) {
        PoNumberType search = new PoNumberType();
        search.setName(name);
        List<PoNumberType> results = dao.searchByExample(search, 0, 0);
        if (results == null || results.isEmpty()) {
            return null;
        }
        else {
            return results.get(0);
        }
    }

    public PoNumberType getApdPoType() {
        return this.searchByName(APD_PO_TYPE);
    }

    public PoNumberType getCustomerPoType() {
        return this.searchByName(CUSTOMER_PO_TYPE);
    }
}
