package com.apd.phoenix.service.business;

import java.sql.Date;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import com.apd.phoenix.service.model.PermissionXUser;
import com.apd.phoenix.service.persistence.jpa.PermissionXUserDao;

@Stateless
@LocalBean
public class PermissionXUserBp extends AbstractBp<PermissionXUser> {

    /**
     * This method is injected with the correct Dao for initialization
     * 
     * @param dao
     */
    @Inject
    public void initDao(PermissionXUserDao dao) {
        this.dao = dao;
    }

    /**
     * Assigns a start date between a User and Permission.
     * 
     * @param permissionXUser
     * @param date
     */
    public void assignStartDate(PermissionXUser permissionXUser, Date date) {
        permissionXUser.setStartDate(date);
        this.dao.update(permissionXUser);
    }

    /**
     * Assigns a end date between a User and Permission.
     * 
     * @param permissionXUser
     * @param date
     */
    public void assignEndDate(PermissionXUser permissionXUser, Date date) {
        permissionXUser.setStartDate(date);
        this.dao.update(permissionXUser);
    }

    /**
     * Assigns a Permission to a User.
     * 
     * @param permissionXUser
     * @param user
     */
    public void assignPermissionToUser(PermissionXUser permissionXUser) {
        this.dao.create(permissionXUser);
    }

    /**
     * Removes a Permission from a Role.
     * 
     * @param permissionXUser
     */
    public void removePermissionFromUser(PermissionXUser permissionXUser) {
        this.dao.delete(permissionXUser.getId(), PermissionXUser.class);
    }

}
