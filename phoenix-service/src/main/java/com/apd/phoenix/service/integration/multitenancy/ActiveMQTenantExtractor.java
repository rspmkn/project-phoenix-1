package com.apd.phoenix.service.integration.multitenancy;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import org.apache.camel.Exchange;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@ApplicationScoped
@Named("activemqTenantExtractor")
public class ActiveMQTenantExtractor {

    Logger LOG = LoggerFactory.getLogger(ActiveMQTenantExtractor.class);

    public void extractTenantId(Exchange exchange) {
        Long tenantId = (Long) exchange.getIn().getHeader("tenantId");
        LOG.debug("Message Header contains tenantID: " + tenantId);
        if (tenantId == null) {
            LOG.warn("------------SETTING tenantId to default APD tenant");
            tenantId = 1l;
        }
        exchange.setProperty("tenantId", tenantId);
    }

}