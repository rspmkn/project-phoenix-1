/*
 * 
 */
package com.apd.phoenix.service.model.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.commons.lang.StringUtils;

public class PurchaseOrderDto extends TransactionDto implements Serializable {

    private static final long serialVersionUID = -7756095313247698734L;

    private String partnerAccountId;

    private String partnerId;

    private String partnerDestination;

    private CredentialDto credential;

    private BigDecimal estimatedShippingAmount;

    private BigDecimal maximumTaxToCharge;

    private Date orderDate;

    private Date shipDate;

    private Date requestedDeliveryDate;

    private Date shipNotBeforeDate;

    private Date shipNoLaterDate;

    private String buyerCookie;

    private String currencyCode;

    private BigDecimal orderTotal;

    private BigDecimal taxTotal;

    private TermsOfSaleDto termsOfSaleDto;

    private AccountDto account;

    private List<LineItemDto> items = new ArrayList<LineItemDto>();

    private PaymentInformationDto paymentInformationDto;

    private List<PoNumberDto> poNumbers = new ArrayList<PoNumberDto>();

    private OrderTypeDto orderType; // 'NE = New Order, 'OS' = Will Call Order
    //private String releaseNumber;
    //private String contractNumber;

    private OrderStatusDto orderStatus;

    private RoutingCarrierDetailsDto carrierDetails;

    private AddressDto shipToAddressDto;

    private List<ReferenceNumberDto> refNumbers = new ArrayList<ReferenceNumberDto>();

    // Code designating which USSCo facility to ship from for USPS shipments in EDI 850
    private String shipFrom;

    private String newStatus;

    private BigDecimal subTotal;

    private String recipients;

    private VendorDto vendor;

    private String merchandiseTypeCode;

    private List<String> comments;

    //deprecated with 1.3.1661
    @Deprecated
    private BigDecimal totalWeight;

    private Date cancelDate;

    private Date deliveryDate;

    private SystemUserDto systemUserDto;

    private Boolean eligibleForRetry;

    private String requisitionName;

    private String approvalComment;

    private boolean willCallEdiOverride = false;

    private boolean wrapAndLabel = false;

    private boolean adot = false;

    private CredentialTermsDto terms;

    private boolean request = false;

    private String operationAllowed;

    private String lastOrderConfirmationId;

    private String orderPayloadId;

    private String status;

    private String AccountLocationCode;

    private String customerRevisionNumber;

    private Date customerRevisionDate;

    private String assignedCostCenter;

    private String assignedCostCenterLabel;

    private BigDecimal orderWeight;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * For edi and cxml order creation it is unclear whether or not an order will 
     * make it through the process, but we still want to log that order's creation,
     * so we create an empty order with just the credential and account information.
     * This flag is used to signal that a dto should update this empty order.
     **/
    private Boolean updateEmptyOrder = false;

    public BigDecimal getEstimatedShippingAmount() {
        return estimatedShippingAmount;
    }

    public void setEstimatedShippingAmount(BigDecimal estimatedShippingAmount) {
        this.estimatedShippingAmount = estimatedShippingAmount;
    }

    public OrderStatusDto getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(OrderStatusDto orderStatus) {
        this.orderStatus = orderStatus;
    }

    public BigDecimal getMaximumTaxToCharge() {
        return maximumTaxToCharge;
    }

    public void setMaximumTaxToCharge(BigDecimal maximumTaxToCharge) {
        this.maximumTaxToCharge = maximumTaxToCharge;
    }

    public String getBuyerCookie() {
        return buyerCookie;
    }

    public void setBuyerCookie(String buyerCookie) {
        this.buyerCookie = buyerCookie;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public void setOrderTotal(BigDecimal orderTotal) {
        this.orderTotal = orderTotal;
    }

    public Date getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(Date orderDate) {
        this.orderDate = orderDate;
    }

    public BigDecimal getOrderTotal() {
        return orderTotal;
    }

    public AccountDto getAccount() {
        return account;
    }

    public void setAccount(AccountDto account) {
        this.account = account;
    }

    public List<LineItemDto> getItems() {
        return items;
    }

    public void setItems(List<LineItemDto> items) {
        this.items = items;
    }

    public List<PoNumberDto> getPoNumbers() {
        return poNumbers;
    }

    public void setPoNumbers(List<PoNumberDto> poNumbers) {
        this.poNumbers = poNumbers;
    }

    /**
     * @return the paymentInformationDto
     */
    public PaymentInformationDto getPaymentInformationDto() {
        return paymentInformationDto;
    }

    /**
     * @param paymentInformationDto the paymentInformationDto to set
     */
    public void setPaymentInformationDto(PaymentInformationDto paymentInformationDto) {
        this.paymentInformationDto = paymentInformationDto;
    }

    /*
     * Convenience methods to retrieve/update specific order reference numbers
     */
    public PoNumberDto retrieveApdPoNumber() {
        return this.retrievePoNumberByType(PoNumberDto.APD);
    }

    public PoNumberDto retrieveCustomerPoNumber() {
        return this.retrievePoNumberByType(PoNumberDto.CUSTOMER);
    }

    public PoNumberDto retrieveBlanketPoNumber() {
        return this.retrievePoNumberByType(PoNumberDto.BLANKET);
    }

    private PoNumberDto retrievePoNumberByType(String type) {
        PoNumberDto toReturn = null;
        if (this.getPoNumbers() != null) {
            for (PoNumberDto poNumber : this.getPoNumbers()) {
                if (poNumber.getType().equals(type)) {
                    toReturn = poNumber;
                }
            }
        }
        return toReturn;
    }

    public OrderTypeDto getOrderType() {
        return orderType;
    }

    public void setOrderType(OrderTypeDto orderType) {
        this.orderType = orderType;
    }

    public RoutingCarrierDetailsDto getCarrierDetails() {
        return carrierDetails;
    }

    public void setCarrierDetails(RoutingCarrierDetailsDto carrierDetails) {
        this.carrierDetails = carrierDetails;
    }

    public AddressDto getShipToAddressDto() {
        return shipToAddressDto;
    }

    public void setShipToAddressDto(AddressDto shipToAddressDto) {
        this.shipToAddressDto = shipToAddressDto;
    }

    @Deprecated
    public AddressDto lookupBillToAddressDto() {
        if (this.getPaymentInformationDto() != null) {
            return this.getPaymentInformationDto().getBillToAddress();
        }
        else {
            return null;
        }
    }

    @Deprecated
    public final void changeBillToAddressDto(AddressDto billToAddressDto) {
        if (this.paymentInformationDto == null) {
            this.paymentInformationDto = new PaymentInformationDto();
        }
        this.getPaymentInformationDto().setBillToAddress(billToAddressDto);
    }

    public void setApdPoNumber(String poNumber) {
        PoNumberDto poNumberDto = new PoNumberDto();
        poNumberDto.setType(PoNumberDto.APD);
        poNumberDto.setValue(poNumber);
        this.setPoNumberByType(poNumberDto);
    }

    public String getApdPoNumber() {
        if (this.retrieveApdPoNumber() != null) {
            return this.retrieveApdPoNumber().getValue();
        }
        return null;
    }

    public void setCustomerPoNumber(String poNumber) {
        PoNumberDto poNumberDto = new PoNumberDto();
        poNumberDto.setType(PoNumberDto.CUSTOMER);
        poNumberDto.setValue(poNumber);
        this.setPoNumberByType(poNumberDto);
    }

    public String getCustomerPoNumber() {
        if (this.retrieveCustomerPoNumber() != null) {
            return this.retrieveCustomerPoNumber().getValue();
        }
        return null;
    }

    public void setBlanketPoNumber(String poNumber) {
        PoNumberDto poNumberDto = new PoNumberDto();
        poNumberDto.setType(PoNumberDto.BLANKET);
        poNumberDto.setValue(poNumber);
        this.setPoNumberByType(poNumberDto);
    }

    public String getNewStatus() {
        return newStatus;
    }

    public void setNewStatus(String newStatus) {
        this.newStatus = newStatus;
    }

    private void setPoNumberByType(PoNumberDto newPoNumber) {

        PoNumberDto currentValue = null;
        if (this.getPoNumbers() == null && newPoNumber != null && StringUtils.isNotBlank(newPoNumber.getValue())) {
            this.poNumbers = new ArrayList<PoNumberDto>();
        }
        if (this.getPoNumbers() != null && newPoNumber != null) {
            for (PoNumberDto poNumber : this.getPoNumbers()) {
                if (poNumber.getType().equals(newPoNumber.getType())) {
                    currentValue = poNumber;
                }
            }
            // New value
            if (newPoNumber != null && StringUtils.isNotBlank(newPoNumber.getValue())) {
                if (currentValue == null) {
                    this.getPoNumbers().add(newPoNumber);
                }
                else {
                    currentValue = newPoNumber;
                }
            } // Unset value or removal 
            else {
                if (currentValue != null) {
                    this.getPoNumbers().remove(currentValue);
                }
            }
        }
    }

    public List<ReferenceNumberDto> getRefNumbers() {
        return refNumbers;
    }

    public void setRefNumbers(List<ReferenceNumberDto> refNumbers) {
        this.refNumbers = refNumbers;
    }

    /**
     * @return the credential
     */
    public CredentialDto getCredential() {
        return credential;
    }

    /**
     * @param credential the credential to set
     */
    public void setCredential(CredentialDto credential) {
        this.credential = credential;
    }

    // NOTE: these properties have been modified in our transaction DTO's to use Strings to accommodate alphanumeric id's 
    // and to be consistent with MessageMetadata properties.
    //
    //    public long getInterchangeId() {
    //        return interchangeId;
    //    }
    //
    //    public void setInterchangeId(long interchangeId) {
    //        this.interchangeId = interchangeId;
    //    }
    //
    //    public long getGroupId() {
    //        return groupId;
    //    }
    //
    //    public void setGroupId(long groupId) {
    //        this.groupId = groupId;
    //    }
    //
    //    public long getTransactionId() {
    //        return transactionId;
    //    }
    //
    //    public void setTransactionId(long transactionId) {
    //        this.transactionId = transactionId;
    //    }

    public String getPartnerId() {
        return partnerId;
    }

    public void setPartnerId(String partnerId) {
        this.partnerId = partnerId;
    }

    /**
     * @return the shipFrom
     */
    public String getShipFrom() {
        return shipFrom;
    }

    /**
     * @param shipFrom the shipFrom to set
     */
    public void setShipFrom(String shipFrom) {
        this.shipFrom = shipFrom;
    }

    public BigDecimal getSubTotal() {
        BigDecimal toReturn = BigDecimal.ZERO;
        for (LineItemDto lineItemDto : this.getItems()) {
            toReturn = toReturn.add(lineItemDto.getSubTotal());
        }
        return toReturn;
    }

    public BigDecimal getSubTotalNoFees() {
        BigDecimal toReturn = BigDecimal.ZERO;
        for (LineItemDto lineItemDto : this.getItems()) {
            if (!lineItemDto.getFee()) {
                toReturn = toReturn.add(lineItemDto.getSubTotal());
            }
        }
        return toReturn;
    }

    public BigDecimal getSubTotalFeesOnly() {
        BigDecimal toReturn = BigDecimal.ZERO;
        for (LineItemDto lineItemDto : this.getItems()) {
            if (lineItemDto.getFee()) {
                toReturn = toReturn.add(lineItemDto.getSubTotal());
            }
        }
        return toReturn;
    }

    public void setSubTotal(BigDecimal subTotal) {
        this.subTotal = subTotal;
    }

    public String getPartnerAccountId() {
        return partnerAccountId;
    }

    public void setPartnerAccountId(String partnerAccountId) {
        this.partnerAccountId = partnerAccountId;
    }

    public String getRecipients() {
        return recipients;
    }

    public void setRecipients(String recipients) {
        this.recipients = recipients;
    }

    public VendorDto getVendor() {
        return vendor;
    }

    public void setVendor(VendorDto vendor) {
        this.vendor = vendor;
    }

    public Date getShipDate() {
        return shipDate;
    }

    public void setShipDate(Date shipDate) {
        this.shipDate = shipDate;
    }

    /**
     * @return the merchandiseTypeCode
     */
    public String getMerchandiseTypeCode() {
        return merchandiseTypeCode;
    }

    /**
     * @param merchandiseTypeCode the merchandiseTypeCode to set
     */
    public void setMerchandiseTypeCode(String merchandiseTypeCode) {
        this.merchandiseTypeCode = merchandiseTypeCode;
    }

    /**
     * @return the comments
     */
    public List<String> getComments() {
        return comments;
    }

    /**
     * @param comments the comments to set
     */
    public void setComments(List<String> comments) {
        this.comments = comments;
    }

    public PurchaseOrderDto() {
    }

    public PurchaseOrderDto(PurchaseOrderDto toCopy) {
        this.account = toCopy.account;
        this.carrierDetails = toCopy.carrierDetails;
        this.comments = toCopy.comments;
        this.credential = toCopy.credential;
        this.estimatedShippingAmount = toCopy.estimatedShippingAmount;
        this.maximumTaxToCharge = toCopy.maximumTaxToCharge;
        this.items = toCopy.getItems();
        this.merchandiseTypeCode = toCopy.merchandiseTypeCode;
        this.newStatus = toCopy.newStatus;
        this.orderDate = toCopy.orderDate;
        this.orderTotal = toCopy.orderTotal;
        this.orderType = toCopy.orderType;
        this.partnerAccountId = toCopy.partnerAccountId;
        this.partnerId = toCopy.partnerAccountId;
        this.paymentInformationDto = toCopy.paymentInformationDto;
        this.poNumbers = toCopy.poNumbers;
        this.recipients = toCopy.recipients;
        this.refNumbers = toCopy.refNumbers;
        this.shipDate = toCopy.shipDate;
        this.shipFrom = toCopy.shipFrom;
        if (toCopy.shipToAddressDto != null) {
            this.shipToAddressDto = new AddressDto(toCopy.shipToAddressDto);
        }
        else {
            this.shipToAddressDto = new AddressDto();
        }
        this.subTotal = toCopy.subTotal;
        this.terms = toCopy.terms;
        this.vendor = toCopy.vendor;
    }

    /**
     * @return the requestedDeliveryDate
     */
    public Date getRequestedDeliveryDate() {
        return requestedDeliveryDate;
    }

    /**
     * @param requestedDeliveryDate the requestedDeliveryDate to set
     */
    public void setRequestedDeliveryDate(Date requestedDeliveryDate) {
        this.requestedDeliveryDate = requestedDeliveryDate;
    }

    /**
     * @return the cancelDate
     */
    public Date getCancelDate() {
        return cancelDate;
    }

    /**
     * @param cancelDate the cancelDate to set
     */
    public void setCancelDate(Date cancelDate) {
        this.cancelDate = cancelDate;
    }

    /**
     * @return the deliveryDate
     */
    public Date getDeliveryDate() {
        return deliveryDate;
    }

    /**
     * @param deliveryDate the deliveryDate to set
     */
    public void setDeliveryDate(Date deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    /**
     * @return the partnerDestination
     */
    public String getPartnerDestination() {
        return partnerDestination;
    }

    /**
     * @param partnerDestination the partnerDestination to set
     */
    public void setPartnerDestination(String partnerDestination) {
        this.partnerDestination = partnerDestination;
    }

    public SystemUserDto getSystemUserDto() {
        return systemUserDto;
    }

    public void setSystemUserDto(SystemUserDto systemUserDto) {
        this.systemUserDto = systemUserDto;
    }

    //deprecated with 1.3.1661
    @Deprecated
    public BigDecimal getTotalWeight() {
        return totalWeight;
    }

    //deprecated with 1.3.1661
    @Deprecated
    public void setTotalWeight(BigDecimal totalWeight) {
        this.totalWeight = totalWeight;
    }

    public Boolean getEligibleForRetry() {
        return eligibleForRetry;
    }

    public void setEligibleForRetry(Boolean eligibleForRetry) {
        this.eligibleForRetry = eligibleForRetry;
    }

    public TermsOfSaleDto getTermsOfSaleDto() {
        return termsOfSaleDto;
    }

    public void setTermsOfSaleDto(TermsOfSaleDto termsOfSaleDto) {
        this.termsOfSaleDto = termsOfSaleDto;
    }

    public BigDecimal getTaxTotal() {
        return taxTotal;
    }

    public void setTaxTotal(BigDecimal taxTotal) {
        this.taxTotal = taxTotal;
    }

    public String getApprovalComment() {
        return approvalComment;
    }

    public void setApprovalComment(String approvalComment) {
        this.approvalComment = approvalComment;
    }

    /**
     * @return the requisitionName
     */
    public String getRequisitionName() {
        return requisitionName;
    }

    /**
     * @param requisitionName the requisitionName to set
     */
    public void setRequisitionName(String requisitionName) {
        this.requisitionName = requisitionName;
    }

    public boolean isWillCallEdiOverride() {
        return willCallEdiOverride;
    }

    public void setWillCallEdiOverride(boolean willCallEdiOverride) {
        this.willCallEdiOverride = willCallEdiOverride;
    }

    /**
     * @return the terms
     */
    public CredentialTermsDto getTerms() {
        return terms;
    }

    /**
     * @param terms the terms to set
     */
    public void setTerms(CredentialTermsDto terms) {
        this.terms = terms;
    }

    public boolean isRequest() {
        return request;
    }

    public void setRequest(boolean request) {
        this.request = request;
    }

    public String getOperationAllowed() {
        return operationAllowed;
    }

    public void setOperationAllowed(String operationAllowed) {
        this.operationAllowed = operationAllowed;
    }

    public Boolean isUpdateEmptyOrder() {
        return updateEmptyOrder;
    }

    public void setUpdateEmptyOrder(Boolean updateEmptyOrder) {
        this.updateEmptyOrder = updateEmptyOrder;
    }

    public String getLastOrderConfirmationId() {
        return lastOrderConfirmationId;
    }

    public void setLastOrderConfirmationId(String lastOrderConfirmationId) {
        this.lastOrderConfirmationId = lastOrderConfirmationId;
    }

    /**
     * @return the AccountLocationCode
     */
    public String getAccountLocationCode() {
        return AccountLocationCode;
    }

    /**
     * @param AccountLocationCode the AccountLocationCode to set
     */
    public void setAccountLocationCode(String AccountLocationCode) {
        this.AccountLocationCode = AccountLocationCode;
    }

    public boolean isWrapAndLabel() {
        return wrapAndLabel;
    }

    public void setWrapAndLabel(boolean wrapAndLabel) {
        this.wrapAndLabel = wrapAndLabel;
    }

    public boolean isAdot() {
        return adot;
    }

    public void setAdot(boolean adot) {
        this.adot = adot;
    }

    public String getOrderPayloadId() {
        return orderPayloadId;
    }

    public void setOrderPayloadId(String orderPayloadId) {
        this.orderPayloadId = orderPayloadId;
    }

    public String getCustomerRevisionNumber() {
        return customerRevisionNumber;
    }

    public void setCustomerRevisionNumber(String customerRevisionNumber) {
        this.customerRevisionNumber = customerRevisionNumber;
    }

    public Date getCustomerRevisionDate() {
        return customerRevisionDate;
    }

    public void setCustomerRevisionDate(Date customerRevisionDate) {
        this.customerRevisionDate = customerRevisionDate;
    }

    public String getAssignedCostCenter() {
        return assignedCostCenter;
    }

    public void setAssignedCostCenter(String assignedCostCenter) {
        this.assignedCostCenter = assignedCostCenter;
    }

    public String getAssignedCostCenterLabel() {
        return assignedCostCenterLabel;
    }

    public void setAssignedCostCenterLabel(String assignedCostCenterLabel) {
        this.assignedCostCenterLabel = assignedCostCenterLabel;
    }

    public BigDecimal getOrderWeight() {
        return orderWeight;
    }

    public void setOrderWeight(BigDecimal orderWeight) {
        this.orderWeight = orderWeight;
    }

    /**
     * @return the shipNotBeforeDate
     */
    public Date getShipNotBeforeDate() {
        return shipNotBeforeDate;
    }

    /**
     * @param shipNotBeforeDate the shipNotBeforeDate to set
     */
    public void setShipNotBeforeDate(Date shipNotBeforeDate) {
        this.shipNotBeforeDate = shipNotBeforeDate;
    }

    /**
     * @return the shipNoLaterDate
     */
    public Date getShipNoLaterDate() {
        return shipNoLaterDate;
    }

    /**
     * @param shipNoLaterDate the shipNoLaterDate to set
     */
    public void setShipNoLaterDate(Date shipNoLaterDate) {
        this.shipNoLaterDate = shipNoLaterDate;
    }

}
