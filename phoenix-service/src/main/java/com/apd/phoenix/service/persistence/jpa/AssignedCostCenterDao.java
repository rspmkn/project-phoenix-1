package com.apd.phoenix.service.persistence.jpa;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.Query;
import com.apd.phoenix.service.model.AccountXCredentialXUser;
import com.apd.phoenix.service.model.AssignedCostCenter;

/**
 * AssignedCostCenter DAO stub
 * 
 * @author RHC
 *
 */
@Stateless
@LocalBean
public class AssignedCostCenterDao extends AbstractDao<AssignedCostCenter> {

    /**
     * Takes an AccountXCredentialXUser, and returns a List of Cost Centers. If the 
     * AccountXCredentialXUser has at least one cost center, returns those cost centers; otherwise, 
     * returns the cost centers associated with the Credential.
     * 
     * @param axcxu
     * @return
     */
    public List<AssignedCostCenter> costCentersForCashout(AccountXCredentialXUser axcxu) {
        List<Object> user = ccResults(axcxu.getId(), "user");
        //TODO: get AssignedCostCenters from Credentials, then update the query, 
        //and call using "cred" instead of "user" like with the method 
        //departmentsForCashout on DepartmentDao
        List<Object> cred = ccResults(axcxu.getId(), "user");

        
        List<AssignedCostCenter> toReturn = new ArrayList<>();
        //toReturn.addAll((List<AssignedCostCenter>)(List<?>)cred);
        
        if (user.size() != 0 && user.get(0) != null) {
        	for (Object obj : user) {
        		toReturn.add((AssignedCostCenter) obj);
        	}
        }
        else if (cred.size() != 0 && cred.get(0) != null) {
        	for (Object obj : cred) {
        		toReturn.add((AssignedCostCenter) obj);
        	}
        }
        
        return toReturn;
    }

    @SuppressWarnings("unchecked")
    private List<Object> ccResults(Long axcxuId, String userOrCred) {
        String hql = "SELECT " + userOrCred + "CCs FROM AccountXCredentialXUser AS axcxu LEFT JOIN "
                + "axcxu.allowedCostCenters AS userCCs " + "WHERE axcxu.id = :axcxuId AND axcxu.active = true AND "
                + userOrCred + "CCs.active = true";
        //Creates the query
        Query query = entityManager.createQuery(hql);
        query.setParameter("axcxuId", axcxuId);

        setItemsReturned(query, 0, 0);
        return query.getResultList();
    }
}
