package com.apd.phoenix.service.model.dto;

import java.io.Serializable;
import java.math.BigInteger;

public class ShipManifestDto implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -4625148572790247316L;

    private BigInteger manifestId;

    private PickDto pickDto;

    private BigInteger numberOfBoxes;

    public BigInteger getNumberOfBoxes() {
        return numberOfBoxes;
    }

    public void setNumberOfBoxes(BigInteger numberOfBoxes) {
        this.numberOfBoxes = numberOfBoxes;
    }

    public BigInteger getManifestId() {
        return manifestId;
    }

    public void setManifestId(BigInteger manifestId) {
        this.manifestId = manifestId;
    }

    public PickDto getPickDto() {
        return pickDto;
    }

    public void setPickDto(PickDto pickDto) {
        this.pickDto = pickDto;
    }

}
