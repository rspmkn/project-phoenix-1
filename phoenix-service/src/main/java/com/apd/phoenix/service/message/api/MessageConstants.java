package com.apd.phoenix.service.message.api;

public interface MessageConstants {

    public static final String EDI = "EDI";
    public static final String CXML = "CXML";
    public static final String XCBL = "XCBL";
    public static final String EMAIL = "EMAIL";
    public static final String OCI = "OCI";
    public static final String CSV = "CSV";

    public static final String OUTBOUND = "OUTBOUND";
    public static final String INBOUND = "INBOUND";
    public static final String XML = "XML";
    public static final String IMAGE = "IMAGE";
    public static final String TEXT = "TEXT";
}
