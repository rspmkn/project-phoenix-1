package com.apd.phoenix.service.persistence.multitenancy.impl;

import java.io.Serializable;
import org.hibernate.context.spi.CurrentTenantIdentifierResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.core.utility.TenantConfigRepository;

public class CurrentTenantIdentifierResolverImpl implements Serializable, CurrentTenantIdentifierResolver {

    private static final long serialVersionUID = 827666997195687623L;

    private static final ThreadLocal<Long> tenantId = new ThreadLocal<Long>();

    private static final Logger LOG = LoggerFactory.getLogger(CurrentTenantIdentifierResolverImpl.class);

    @Override
    public String resolveCurrentTenantIdentifier() {
        return CurrentTenantIdentifierResolverImpl.getCurrentTenantSchema();
    }

    public static String getCurrentTenantSchema() {
        String tenantSchema = null;
        if (LOG.isDebugEnabled()) {
            LOG.debug("Current tenant id is " + tenantId.get());
        }
        try {
            tenantSchema = TenantConfigRepository.getInstance().getTenantPropertiesById(tenantId.get()).getSchemaName();
        }
        catch (final Exception e) {
            LOG.error("Error finding current tenant.", e);
        }
        LOG.debug("Current Tenant Schema is {}", tenantSchema);
        return tenantSchema;
    }

    public static void setCurrentTenant(Long theTenantId) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("Setting the current tenant ID to " + theTenantId);
            if (theTenantId == null && LOG.isDebugEnabled()) {
                //note that if the ID is null, the current tenant ID is still updated.
                //This ensures that the previous tenant ID is cleared. If the null ID
                //is used, the hibernate schema resolver should throw an exception.
                LOG.debug("Null ID, stack trace", new Throwable());
            }
        }
        tenantId.set(theTenantId);
    }

    public static Long getCurrentTenant() {
        return tenantId.get();
    }

    @Override
    public boolean validateExistingCurrentSessions() {
        return true;
    }

}
