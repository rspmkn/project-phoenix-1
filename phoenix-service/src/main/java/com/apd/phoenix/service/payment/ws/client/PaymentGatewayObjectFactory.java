package com.apd.phoenix.service.payment.ws.client;

import static com.apd.phoenix.service.model.CredentialPropertyType.CredentialPropertyTypeEnum.*;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Set;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.core.utility.TenantConfigRepository;
import com.apd.phoenix.service.model.CardInformation;
import com.apd.phoenix.service.model.CardInformation.CvvIndicator;
import com.apd.phoenix.service.model.Credential;
import com.apd.phoenix.service.model.CustomerOrder;
import com.apd.phoenix.service.model.LineItem;
import com.apd.phoenix.service.model.LineItemXReturn;
import com.apd.phoenix.service.model.LineItemXShipment;
import com.apd.phoenix.service.model.PaymentInformation;
import com.apd.phoenix.service.model.Person;
import com.apd.phoenix.service.model.PhoneNumber;
import com.apd.phoenix.service.model.PoNumber;
import com.apd.phoenix.service.model.Shipment;
import com.apd.phoenix.service.payment.ws.AdditionalAmounts;
import com.apd.phoenix.service.payment.ws.Address;
import com.apd.phoenix.service.payment.ws.ArrayOfLineItemDetail;
import com.apd.phoenix.service.payment.ws.AuthorizeAndCaptureParams;
import com.apd.phoenix.service.payment.ws.AuthorizeParams;
import com.apd.phoenix.service.payment.ws.CaptureParams;
import com.apd.phoenix.service.payment.ws.CardSecurityCodeIndicator;
import com.apd.phoenix.service.payment.ws.CardVerificationParams;
import com.apd.phoenix.service.payment.ws.ClientCredentials;
import com.apd.phoenix.service.payment.ws.Contact;
import com.apd.phoenix.service.payment.ws.CountryCode;
import com.apd.phoenix.service.payment.ws.CreditCardTransaction;
import com.apd.phoenix.service.payment.ws.CreditCardTransactionCreditCard;
import com.apd.phoenix.service.payment.ws.CreditCardTypes;
import com.apd.phoenix.service.payment.ws.CreditParams;
import com.apd.phoenix.service.payment.ws.CurrencyCode;
import com.apd.phoenix.service.payment.ws.Email;
import com.apd.phoenix.service.payment.ws.GetTransactionStatusParams;
import com.apd.phoenix.service.payment.ws.LineItemDetailPurchasing;
import com.apd.phoenix.service.payment.ws.ObjectFactory;
import com.apd.phoenix.service.payment.ws.StateProvinceCode;
import com.apd.phoenix.service.payment.ws.StoredCardIdentifier;
import com.apd.phoenix.service.payment.ws.TerminalIdentifier;
import com.apd.phoenix.service.persistence.multitenancy.impl.CurrentTenantIdentifierResolverImpl;
import com.apd.phoenix.service.utility.CredentialPropertyUtils;

/**
 *
 * @author dnorris
 */
public class PaymentGatewayObjectFactory {

    private static final Logger logger = LoggerFactory.getLogger(PaymentGatewayObjectFactory.class);
    private static final ObjectFactory factory = new ObjectFactory();

    private static final String CARD_VAULT_CUSTOMER_CODE = "3dsi_customer_code";
    //TODO initialize default ship from zipcode with externalized property value
    private static final String DEFAULT_SHIP_FROM_POSTAL_CODE = "csr.address.zip";
    public static final String DEFAULT_UNSPSC_CODE = "44120000";
    public static final int LENGTH_OF_UNSPSC_CATEGORY_TREE_CODE = 4;
    public static final String FOUR_ZEROES_TO_PAD_COMMODITY_CODE = "0000";
    public static final int STANDARD_LENGTH_OF_COMMODITY_CODE = 8;
    public static final int PRODUCT_CODE_LENGTH = 49;

    static ClientCredentials createClientCredentials(String clientCode, String username, String password) {
        ClientCredentials clientCredentials = factory.createClientCredentials();
        clientCredentials.setClientCode(factory.createClientCredentialsClientCode(clientCode));
        clientCredentials.setUserName(factory.createClientCredentialsUserName(username));
        clientCredentials.setPassword(factory.createClientCredentialsPassword(password));
        return clientCredentials;
    }

    static TerminalIdentifier createTerminalIdentifier(String locationCode, String merchantCode, String terminalCode) {
        TerminalIdentifier terminalIdentifier = factory.createTerminalIdentifier();
        terminalIdentifier.setLocationCode(factory.createTerminalIdentifierLocationCode(locationCode));
        terminalIdentifier.setMerchantCode(factory.createTerminalIdentifierMerchantCode(merchantCode));
        terminalIdentifier.setTerminalCode(factory.createTerminalIdentifierTerminalCode(terminalCode));
        return terminalIdentifier;
    }

    static AuthorizeParams createAuthorizeParams(TerminalIdentifier terminalIdentifier,
            CreditCardTransaction creditCardTransaction) {
        AuthorizeParams params = factory.createAuthorizeParams();
        params.setTerminalIdentifier(factory.createAuthorizeParamsTerminalIdentifier(terminalIdentifier));
        params.setCreditCardTransaction(factory.createAuthorizeParamsCreditCardTransaction(creditCardTransaction));
        return params;
    }

    static AuthorizeAndCaptureParams createAuthorizeAndCaptureParams(TerminalIdentifier terminalIdentifier,
            CreditCardTransaction creditCardTransaction) {
        AuthorizeAndCaptureParams params = factory.createAuthorizeAndCaptureParams();
        params.setTerminalIdentifier(factory.createAuthorizeAndCaptureParamsTerminalIdentifier(terminalIdentifier));
        params.setCreditCardTransaction(factory
                .createAuthorizeAndCaptureParamsCreditCardTransaction(creditCardTransaction));
        return params;
    }

    static CaptureParams createCaptureParams(TerminalIdentifier terminalIdentifier,
            CreditCardTransaction creditCardTransaction) {
        CaptureParams params = factory.createCaptureParams();
        params.setCreditCardTransaction(factory.createCaptureParamsCreditCardTransaction(creditCardTransaction));
        params.setTerminalIdentifier(factory.createCaptureParamsTerminalIdentifier(terminalIdentifier));
        return params;
    }

    static CreditParams createCreditParams(TerminalIdentifier terminalIdentifier,
            CreditCardTransaction creditCardTransaction) {
        CreditParams params = factory.createCreditParams();
        params.setCreditCardTransaction(factory.createCreditParamsCreditCardTransaction(creditCardTransaction));
        params.setTerminalIdentifier(factory.createCreditParamsTerminalIdentifier(terminalIdentifier));
        return params;
    }

    static CardVerificationParams createVerificationParams(TerminalIdentifier terminalIdentifier,
            CreditCardTransactionCreditCard creditCard) {
        CardVerificationParams params = factory.createCardVerificationParams();
        params.setCreditCard(factory.createCardVerificationParamsCreditCard(creditCard));
        params.setTerminalIdentifier(factory.createCreditParamsTerminalIdentifier(terminalIdentifier));
        return params;
    }

    static GetTransactionStatusParams createGetTransactionStatusParams(TerminalIdentifier terminalIdentifier,
            String transactionId) {
        GetTransactionStatusParams params = factory.createGetTransactionStatusParams();
        params.setOriginalTransactionKey(factory.createOriginalTransactionParamsOriginalTransactionKey(transactionId));
        params.setTerminalIdentifier(factory.createCaptureParamsTerminalIdentifier(terminalIdentifier));
        return params;
    }

    static XMLGregorianCalendar createXMLGregorianCalendar(Date date) {
        XMLGregorianCalendar xmlDate = null;
        try {
            GregorianCalendar c = new GregorianCalendar();
            c.setTime(date);
            xmlDate = DatatypeFactory.newInstance().newXMLGregorianCalendar(c);

        }
        catch (DatatypeConfigurationException ex) {
            logger.error("Could not convert date");
        }
        return xmlDate;
    }

    static LineItemDetailPurchasing createLineItemDetail(LineItemXShipment lineItemXShipment, Short lineNumber,
            Credential credential) {
        LineItem lineItem = lineItemXShipment.getLineItem();
        LineItemDetailPurchasing detail = factory.createLineItemDetailPurchasing();
        detail.setLineNumber(lineNumber);
        detail.setProductCode(factory.createLineItemDetailPurchasingProductCode(getProductCode(lineItem, credential)));
        //ensure comodity code is in 8 digit format
        if (StringUtils.isNotBlank(lineItem.getUnspscClassification())
                && lineItem.getUnspscClassification().length() == LENGTH_OF_UNSPSC_CATEGORY_TREE_CODE) {
            lineItem.setUnspscClassification(lineItem.getUnspscClassification() + FOUR_ZEROES_TO_PAD_COMMODITY_CODE);
        }
        if (StringUtils.isBlank(lineItem.getUnspscClassification())
                || lineItem.getUnspscClassification().length() != STANDARD_LENGTH_OF_COMMODITY_CODE) {
            logger.debug("Unexpected UNSPC Commodity Code: "
                    + StringUtils.defaultIfBlank(lineItem.getUnspscClassification(), "EMPTY STRING"));
        }
        detail.setCommodityCode(factory.createLineItemDetailPurchasingCommodityCode(StringUtils.defaultIfBlank(lineItem
                .getUnspscClassification(), DEFAULT_UNSPSC_CODE)));
        detail.setQuantity(new BigDecimal(lineItemXShipment.getQuantity()));
        String description = StringUtils.defaultIfBlank(lineItem.getShortName(), "No description");
        description = StringUtils.substring(description, 0, 99);
        detail.setDescription(factory.createLineItemDetailPurchasingDescription(description));
        String uom = StringUtils.defaultIfBlank(lineItem.getUnitOfMeasure().getName(), "EA");
        detail.setUnitOfMeasure(factory.createLineItemDetailPurchasingUnitOfMeasure(uom));
        detail.setItemAmount(lineItem.getUnitPrice());
        detail.setTaxAmount(lineItemXShipment.getTotalTaxAmount().setScale(2, RoundingMode.CEILING));
        detail.setTotalAmount(lineItemXShipment.getSubTotalAmount().setScale(2, RoundingMode.CEILING));
        return detail;
    }

    static LineItemDetailPurchasing createReturnLineItemDetail(LineItemXReturn lineItemXReturn, Short lineNumber,
            Credential credential) {
        LineItem lineItem = lineItemXReturn.getLineItem();
        LineItemDetailPurchasing detail = factory.createLineItemDetailPurchasing();
        detail.setLineNumber(lineNumber);
        detail.setProductCode(factory.createLineItemDetailPurchasingProductCode(getProductCode(lineItem, credential)));
        detail
                .setCommodityCode(factory.createLineItemDetailPurchasingCommodityCode(lineItem
                        .getUnspscClassification()));
        detail.setQuantity(new BigDecimal(lineItemXReturn.getQuantity()));
        String description = StringUtils.defaultIfBlank(lineItem.getShortName(), "No description");
        description = StringUtils.substring(description, 0, 99);
        detail.setDescription(factory.createLineItemDetailPurchasingDescription(description));
        String uom = StringUtils.defaultIfBlank(lineItem.getUnitOfMeasure().getName(), "EA");
        detail.setUnitOfMeasure(factory.createLineItemDetailPurchasingUnitOfMeasure(uom));
        detail.setItemAmount(lineItem.getUnitPrice());
        detail.setTaxAmount(lineItemXReturn.getTotalTaxAmount());
        detail.setTotalAmount(lineItemXReturn.getSubTotalAmount());
        return detail;
    }

    static LineItemDetailPurchasing createLineItemDetail(LineItem lineItem, Short lineNumber, Credential credential) {
        LineItemDetailPurchasing detail = factory.createLineItemDetailPurchasing();
        detail.setLineNumber(lineNumber);
        detail.setProductCode(factory.createLineItemDetailPurchasingProductCode(getProductCode(lineItem, credential)));
        if (StringUtils.isNotBlank(lineItem.getUnspscClassification())) {
            detail.setCommodityCode(factory.createLineItemDetailPurchasingCommodityCode(lineItem
                    .getUnspscClassification()));
        }
        detail.setQuantity(new BigDecimal(lineItem.getQuantity()));
        String description = StringUtils.defaultIfBlank(lineItem.getShortName(), "No description");
        description = StringUtils.substring(description, 0, 99);
        detail.setDescription(factory.createLineItemDetailPurchasingDescription(description));

        String uom = StringUtils.defaultIfBlank(lineItem.getUnitOfMeasure().getName(), "EA");
        detail.setUnitOfMeasure(factory.createLineItemDetailPurchasingUnitOfMeasure(uom));
        detail.setItemAmount(lineItem.getUnitPrice());
        detail.setTaxAmount(lineItem.getMaximumTaxToCharge());
        detail.setTotalAmount(lineItem.getTotalPrice());
        return detail;
    }

    static Address createAddress(com.apd.phoenix.service.model.Address address) {
        if (address == null) {
            return null;
        }
        Address toReturn = factory.createAddress();
        if (StringUtils.isNotBlank(address.getLine1())) {
            toReturn.setAddressLine1(factory.createAddressAddressLine1(address.getLine1()));
        }
        if (StringUtils.isNotBlank(address.getLine2())) {
            toReturn.setAddressLine2(factory.createAddressAddressLine2(address.getLine2()));
        }
        if (StringUtils.isNotBlank(address.getCity())) {
            toReturn.setCity(factory.createAddressCity(address.getCity()));
        }
        if (StringUtils.isNotBlank(address.getCountry())) {
            toReturn.setCountryCode(CountryCode.fromValue(address.getCountry()));
        }
        if (StringUtils.isNotBlank(address.getZip())) {
            toReturn.setPostalCode(factory.createAddressPostalCode(address.getZip()));
        }
        if (StringUtils.isNotBlank(address.getState())) {
            toReturn.setStateProvinceCode(StateProvinceCode.fromValue(address.getState()));
        }
        return toReturn;
    }

    static Contact createContact(Person person) {
        Contact contact = factory.createContact();
        if (StringUtils.isNotBlank(person.getEmail())) {
            Email email = factory.createEmail();
            email.setAddress(factory.createEmailAddress(person.getEmail()));
            contact.setEmail(factory.createContactEmail(email));
        }
        if (StringUtils.isNotBlank(person.getFirstName())) {
            contact.setFirstName(factory.createContactFirstName(person.getFirstName()));
        }
        if (StringUtils.isNotBlank(person.getMiddleInitial())) {
            contact.setMiddleName(factory.createContactMiddleName(person.getMiddleInitial()));
        }
        if (StringUtils.isNotBlank(person.getLastName())) {
            contact.setLastName(factory.createContactLastName(person.getLastName()));
        }
        if (!person.getPhoneNumbers().isEmpty()) {
            for (PhoneNumber phoneNumber : person.getPhoneNumbers()) {
                String number = phoneNumber.toString();
                contact.setPhone(factory.createContactPhone(number));
                break;
            }
        }
        return contact;
    }

    static void initBillingAddress(CustomerOrder customerOrder, PaymentGatewayTransaction transaction) {
        if (customerOrder.getPaymentInformation() != null) {
            if (customerOrder.getPaymentInformation().getBillingAddress() != null) {
                transaction.setBillingAddress(customerOrder.getPaymentInformation().getBillingAddress());
            }
        }
    }

    static void initPaymentDetails(CustomerOrder customerOrder, PaymentGatewayTransaction transaction) {
        transaction.setTotalFreightAmount(customerOrder.getEstimatedShippingAmount());
        transaction.setSalesTaxAmount(customerOrder.getMaximumTaxToCharge());
        transaction.setSubTotalAmount(customerOrder.getSubtotal());
        transaction.setTotalAmount(customerOrder.getOrderTotal());
    }

    static void initPaymentDetails(SplitCharge splitCharge, PaymentGatewayTransaction transaction) {
        transaction.setTotalFreightAmount(splitCharge.getTotalFreightPrice());
        transaction.setSalesTaxAmount(splitCharge.getTotalTaxPrice());
        transaction.setSubTotalAmount(splitCharge.getSubTotalPrice());
        transaction.setTotalAmount(splitCharge.getTotalPrice());
    }

    static void initStoredCardIdentifier(PaymentGatewayTransaction transaction, PaymentInformation paymentInformation) {
        StoredCardIdentifier storedCardIdentifier = factory.createStoredCardIdentifier();
        CardInformation cardInformation = paymentInformation.getCard();
        storedCardIdentifier.setCustomerCode(factory
                .createStoredCardIdentifierCustomerCode(getTenantPropertyByName(CARD_VAULT_CUSTOMER_CODE)));
        storedCardIdentifier.setToken(factory.createStoredCardIdentifierToken(cardInformation.getCardVaultToken()));
        transaction.setStoredCardIdentifier(storedCardIdentifier);
        transaction.setStoredCardToken(cardInformation.getCardVaultToken());
    }

    //NOTE: For card verification only
    static void initCreditCard(VerificationTransaction transaction, PaymentInformation paymentInformation, String cvv) {
        CreditCardTransactionCreditCard creditCard = new CreditCardTransactionCreditCard();
        CardInformation cardInformation = paymentInformation.getCard();

        if (StringUtils.isNotBlank(cardInformation.getNameOnCard())) {
            String nameOnCard = cardInformation.getNameOnCard();
            creditCard.setNameOnCard(factory.createCreditCardNameOnCard(nameOnCard));
        }
        if (transaction.getBillingAddress() != null) {
            Address billingAddress = transaction.getBillingAddress();
            creditCard.setBillingAddress(factory.createCreditCardBillingAddress(billingAddress));
        }
        if (cardInformation.getExpiration() != null) {
            XMLGregorianCalendar expirationDate = PaymentGatewayObjectFactory
                    .createXMLGregorianCalendar(cardInformation.getExpiration());
            creditCard.setExpirationMonth(expirationDate.getMonth());
            creditCard.setExpirationYear(expirationDate.getYear());
        }
        if (StringUtils.isNotBlank(cardInformation.getNumber())) {
            String cardAccountNumber = cardInformation.getNumber();
            creditCard.setCardAccountNumber(factory.createCreditCardCardAccountNumber(cardAccountNumber));
        }
        else {
            throw new IllegalStateException("Card Number is Required for verification");
        }
        if (paymentInformation.getContact() != null) {
            Person person = paymentInformation.getContact();
            Contact contact = PaymentGatewayObjectFactory.createContact(person);
            creditCard.setCardholder(factory.createCreditCardCardholder(contact));
        }
        //the next block of code determines what the CVV indicator should be
        if (StringUtils.isNotBlank(cvv)) {
            //if the CVV exists and is not null, sets the indicator to "Provided"
            creditCard.setCardSecurityCode(factory.createCreditCardTransactionCreditCardCardSecurityCode(cvv));
            creditCard.setCardSecurityCodeIndicator(CardSecurityCodeIndicator.PROVIDED);
        }
        else if (cardInformation.getCvvIndicator() != null
                && !cardInformation.getCvvIndicator().equals(CvvIndicator.PROVIDED)) {
            //if the CVV doesn't exist, uses the indicator specified on the card information, 
            //unless that indicator is null or "Provided"
            creditCard.setCardSecurityCodeIndicator(cardInformation.getCvvIndicator().getXmlValue());
        }
        else {
            //if the above conditions don't apply, sets the indicator to "NotProvided"
            creditCard.setCardSecurityCodeIndicator(CardSecurityCodeIndicator.NOT_PROVIDED);
        }
        creditCard.setCardType(CreditCardTypes.NONE);
        transaction.setCreditCard(creditCard);
    }

    static void initLineItemDetails(CustomerOrder customerOrder, PaymentGatewayTransaction transaction,
            Credential credential) {
        ArrayOfLineItemDetail arrayOfLineItemDetail = factory.createArrayOfLineItemDetail();
        short lineNumber = 1;
        for (LineItem lineItem : customerOrder.getItems()) {
            LineItemDetailPurchasing detail = createLineItemDetail(lineItem, lineNumber, credential);
            arrayOfLineItemDetail.getLineItemDetail().add(detail);
            lineNumber++;
        }
        transaction.setArrayOfLineItemDetail(arrayOfLineItemDetail);
    }

    static void initLineItemDetails(Set<LineItemXShipment> contents, PaymentGatewayTransaction transaction,
            Credential credential) {
        ArrayOfLineItemDetail arrayOfLineItemDetail = factory.createArrayOfLineItemDetail();
        short lineNumber = 1;
        for (LineItemXShipment lineItemXShipment : contents) {
            LineItemDetailPurchasing detail = createLineItemDetail(lineItemXShipment, lineNumber, credential);
            arrayOfLineItemDetail.getLineItemDetail().add(detail);
            lineNumber++;
        }
        transaction.setArrayOfLineItemDetail(arrayOfLineItemDetail);
    }

    static void initReturnLineItemDetails(Set<LineItemXReturn> contents, PaymentGatewayTransaction transaction,
            Credential credential) {
        ArrayOfLineItemDetail arrayOfLineItemDetail = factory.createArrayOfLineItemDetail();
        short lineNumber = 1;
        for (LineItemXReturn lineItemXReturn : contents) {
            LineItemDetailPurchasing detail = createReturnLineItemDetail(lineItemXReturn, lineNumber, credential);
            arrayOfLineItemDetail.getLineItemDetail().add(detail);
            lineNumber++;
        }
        transaction.setArrayOfLineItemDetail(arrayOfLineItemDetail);
    }

    static void initAdditionalAmounts(PaymentGatewayTransaction transaction) {
        AdditionalAmounts additionalAmounts = new AdditionalAmounts();
        additionalAmounts.setFreightAmount(transaction.getTotalFreightAmount());
        additionalAmounts.setSalesTaxAmount(transaction.getSalesTaxAmount());
        transaction.setAdditionalAmounts(additionalAmounts);
    }

    static void initVerificationCreditCardTransaction(CustomerOrder customerOrder, VerificationTransaction transaction) {
        CreditCardTransaction creditCardTransaction = factory.createCreditCardTransaction();
        creditCardTransaction
                .setCreditCard(factory.createCreditCardTransactionCreditCard1(transaction.getCreditCard()));
        creditCardTransaction.setCurrencyCode(CurrencyCode.US_DOLLARS);
        // For 3DSI transactions, invoice # and customer reference value are both synonymous with customer po number
        PoNumber customerRef = customerOrder.getCustomerReferenceNumber();
        creditCardTransaction.setCustomerReferenceValue(factory
                .createCreditCardTransactionCustomerReferenceValue(customerRef.getValue()));
        creditCardTransaction
                .setInvoiceNumber(factory.createCreditCardTransactionInvoiceNumber(customerRef.getValue()));
        String shipFromZip = transaction.getShipFromZip();
        if (StringUtils.isBlank(shipFromZip)) {
            shipFromZip = getTenantPropertyByName(DEFAULT_SHIP_FROM_POSTAL_CODE);
        }
        creditCardTransaction.setShipFromPostalCode(factory.createCreditCardTransactionShipFromPostalCode(shipFromZip));
        creditCardTransaction.setAdditionalAmounts(factory.createCreditCardTransactionAdditionalAmounts(transaction
                .getAdditionalAmounts()));
        creditCardTransaction.setLineDetail(factory.createCreditCardTransactionLineDetail(transaction
                .getArrayOfLineItemDetail()));
        creditCardTransaction.setTotalAmount(transaction.getTotalAmount());
        transaction.setCreditCardTransaction(creditCardTransaction);
    }

    static void initCreditCardTransaction(CustomerOrder customerOrder, PaymentGatewayTransaction transaction) {
        CreditCardTransaction creditCardTransaction = factory.createCreditCardTransaction();
        creditCardTransaction.setStoredCardIdentifier(factory
                .createCreditCardTransactionStoredCardIdentifier(transaction.getStoredCardIdentifier()));
        creditCardTransaction.setCurrencyCode(CurrencyCode.US_DOLLARS);
        // For 3DSI transactions, invoice # and customer reference value are both synonymous with customer po number
        PoNumber customerRef = customerOrder.getCustomerReferenceNumber();
        creditCardTransaction.setCustomerReferenceValue(factory
                .createCreditCardTransactionCustomerReferenceValue(customerRef.getValue()));
        creditCardTransaction
                .setInvoiceNumber(factory.createCreditCardTransactionInvoiceNumber(customerRef.getValue()));
        String shipFromZip = transaction.getShipFromZip();
        if (StringUtils.isBlank(shipFromZip)) {
            shipFromZip = getTenantPropertyByName(DEFAULT_SHIP_FROM_POSTAL_CODE);
        }
        creditCardTransaction.setShipFromPostalCode(factory.createCreditCardTransactionShipFromPostalCode(shipFromZip));
        creditCardTransaction.setAdditionalAmounts(factory.createCreditCardTransactionAdditionalAmounts(transaction
                .getAdditionalAmounts()));
        creditCardTransaction.setLineDetail(factory.createCreditCardTransactionLineDetail(transaction
                .getArrayOfLineItemDetail()));
        creditCardTransaction.setTotalAmount(transaction.getTotalAmount());
        transaction.setCreditCardTransaction(creditCardTransaction);
    }

    static void initPoInformation(CustomerOrder customerOrder, PaymentGatewayTransaction transaction) {
        transaction.setApdPoNumber(customerOrder.getApdPo().getValue());
        if (customerOrder.getCustomerPo() != null) {
            transaction.setCustomerPoNumber(customerOrder.getCustomerPo().getValue());
        }
    }

    //Object Factory Methods
    static PaymentGatewayTransaction createPaymentGatewayTransaction(CustomerOrder customerOrder) {
        PaymentGatewayTransaction transaction = new PaymentGatewayTransaction();
        transaction.setCustomerOrder(customerOrder);
        initBillingAddress(customerOrder, transaction);
        initPoInformation(customerOrder, transaction);
        initPaymentDetails(customerOrder, transaction);
        initStoredCardIdentifier(transaction, customerOrder.getPaymentInformation());
        initLineItemDetails(customerOrder, transaction, customerOrder.getCredential());
        initAdditionalAmounts(transaction);
        initCreditCardTransaction(customerOrder, transaction);
        initCardIdentifier(customerOrder, transaction);
        initNameOnCard(customerOrder, transaction);
        return transaction;
    }

    static VerificationTransaction createVerificationTransaction(CustomerOrder customerOrder) {
        return createVerificationTransaction(customerOrder, null);
    }

    static VerificationTransaction createVerificationTransaction(CustomerOrder customerOrder, String cvv) {
        VerificationTransaction transaction = new VerificationTransaction();
        transaction.setCustomerOrder(customerOrder);
        initBillingAddress(customerOrder, transaction);
        initPoInformation(customerOrder, transaction);
        initPaymentDetails(customerOrder, transaction);
        initCreditCard(transaction, customerOrder.getPaymentInformation(), cvv);
        initLineItemDetails(customerOrder, transaction, customerOrder.getCredential());
        initAdditionalAmounts(transaction);
        initVerificationCreditCardTransaction(customerOrder, transaction);
        initNameOnCard(customerOrder, transaction);
        return transaction;
    }

    static PaymentGatewayTransaction createPaymentGatewayTransaction(CustomerOrder customerOrder,
            Set<LineItemXShipment> contents) {
        PaymentGatewayTransaction transaction = new PaymentGatewayTransaction();
        transaction.setCustomerOrder(customerOrder);
        SplitCharge splitCharge = SplitCharge.createSplitCharge(contents);
        initBillingAddress(customerOrder, transaction);
        initPoInformation(customerOrder, transaction);
        initPaymentDetails(splitCharge, transaction);
        initStoredCardIdentifier(transaction, customerOrder.getPaymentInformation());
        initLineItemDetails(contents, transaction, customerOrder.getCredential());
        initTrackingNumber(contents, transaction);
        initAdditionalAmounts(transaction);
        initCreditCardTransaction(customerOrder, transaction);
        initCardIdentifier(customerOrder, transaction);
        initNameOnCard(customerOrder, transaction);
        return transaction;
    }

    static PaymentGatewayTransaction createReturnPaymentGatewayTransaction(CustomerOrder customerOrder,
            Set<LineItemXReturn> contents) {
        PaymentGatewayTransaction transaction = new PaymentGatewayTransaction();
        transaction.setCustomerOrder(customerOrder);
        SplitCharge splitCharge = SplitCharge.createReturnSplitCharge(contents);
        initBillingAddress(customerOrder, transaction);
        initPoInformation(customerOrder, transaction);
        initPaymentDetails(splitCharge, transaction);
        initStoredCardIdentifier(transaction, customerOrder.getPaymentInformation());
        initReturnLineItemDetails(contents, transaction, customerOrder.getCredential());
        initAdditionalAmounts(transaction);
        initCreditCardTransaction(customerOrder, transaction);
        initCardIdentifier(customerOrder, transaction);
        initNameOnCard(customerOrder, transaction);
        return transaction;
    }

    static void initNameOnCard(CustomerOrder customerOrder, PaymentGatewayTransaction transaction) {
        if (customerOrder.getPaymentInformation() != null && customerOrder.getPaymentInformation().getCard() != null) {
            CardInformation cardInformation = customerOrder.getPaymentInformation().getCard();
            if (StringUtils.isNotBlank(cardInformation.getNameOnCard())) {
                transaction.setNameOnCard(cardInformation.getNameOnCard());
            }
        }
    }

    static void initCardIdentifier(CustomerOrder customerOrder, PaymentGatewayTransaction transaction) {
        if (customerOrder.getPaymentInformation() != null && customerOrder.getPaymentInformation().getCard() != null) {
            CardInformation cardInformation = customerOrder.getPaymentInformation().getCard();
            if (StringUtils.isNotBlank(cardInformation.getIdentifier())) {
                transaction.setCardIdentifier(cardInformation.getIdentifier());
            }
        }
    }

    static void initTrackingNumber(Set<LineItemXShipment> contents, PaymentGatewayTransaction transaction) {
        String trackingNumber = "";
        String shipFromZip = "";
        for (LineItemXShipment lineItemXShipment : contents) {
            if (lineItemXShipment.getShipments() != null) {
                Shipment shipment = lineItemXShipment.getShipments();
                if (StringUtils.isNotBlank(shipment.getTrackingNumber())) {
                    trackingNumber = lineItemXShipment.getShipments().getTrackingNumber();
                }
                if (shipment.getShipFromAddress() != null
                        && StringUtils.isNotBlank(shipment.getShipFromAddress().getZip())) {
                    shipFromZip = shipment.getShipFromAddress().getZip();
                }
            }
            break;
        }

        transaction.setTrackingNumber(trackingNumber);
        transaction.setShipFromZip(shipFromZip);
    }

    private static String getProductCode(LineItem item, Credential credential) {
        String toReturn;
        if (CredentialPropertyUtils.equals(SKU_ON_SHOPPING_CART, "S", credential)
                && StringUtils.isNotBlank(item.getSupplierPartId())) {
            toReturn = item.getSupplierPartId();
        }
        else if (CredentialPropertyUtils.equals(SKU_ON_SHOPPING_CART, "A", credential)
                && StringUtils.isNotBlank(item.getSupplierPartAuxiliaryId())) {
            toReturn = item.getSupplierPartAuxiliaryId();
        }
        else if (CredentialPropertyUtils.equals(SKU_ON_SHOPPING_CART, "B", credential)
                && StringUtils.isNotBlank(item.getVendorMat())) {
            toReturn = item.getVendorMat();
        }
        else if (item.getCustomerSku() != null && StringUtils.isNotBlank(item.getCustomerSku().getValue())) {
            toReturn = item.getCustomerSku().getValue();
        }
        else {
            toReturn = item.getApdSku();
        }
        if (!StringUtils.isBlank(toReturn) && toReturn.length() > PRODUCT_CODE_LENGTH) {
            toReturn = toReturn.substring(0, PRODUCT_CODE_LENGTH);
        }
        return toReturn;
    }

    private static String getTenantPropertyByName(String propertyName) {
        return TenantConfigRepository.getInstance().getProperty(getTenantName(), "tenant", propertyName);
    }

    private static String getTenantName() {
        return TenantConfigRepository.getInstance().getTenantNameById(
                CurrentTenantIdentifierResolverImpl.getCurrentTenant());
    }
}
