/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.service.email.impl;

import com.apd.phoenix.service.email.api.Attachment;
import com.apd.phoenix.service.email.api.EmailTemplate;
import com.apd.phoenix.service.email.api.NoAttachmentContentException;
import com.apd.phoenix.service.model.dto.EdiDocumentDto;
import freemarker.template.TemplateException;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.lang.StringUtils;

/**
 *
 * @author nreidelb
 */
public class EdiOrderNotFoundEmailTemplate extends EmailTemplate<EdiDocumentDto> {

    private static final String TEMPLATE = "edi.order.not.found";

    @Override
    public String createBody(EdiDocumentDto ediDocumentErrorsDto) throws IOException, TemplateException {
        Map<String, Object> params = new HashMap<>();
        params.put("dto", ediDocumentErrorsDto);
        return this.create(params);
    }

    @Override
    public String getTemplate() {
        return EdiOrderNotFoundEmailTemplate.TEMPLATE;
    }

    @Override
    public Attachment createAttachment(EdiDocumentDto content) throws NoAttachmentContentException {
        String apdPo = content.getApdPo();
        Attachment poNotice = new Attachment();
        poNotice.setFileName("orderNotFound" + (StringUtils.isNotEmpty(apdPo) ? apdPo : "") + ".txt");
        poNotice.setMimeType(Attachment.MimeType.asc);
        InputStream is = new ByteArrayInputStream(content.getRawMessage().getBytes(StandardCharsets.UTF_8));
        poNotice.setContent(is);
        return poNotice;
    }
}
