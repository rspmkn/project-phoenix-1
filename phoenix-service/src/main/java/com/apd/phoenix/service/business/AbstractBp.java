package com.apd.phoenix.service.business;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import com.apd.phoenix.service.persistence.jpa.Dao;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * The abstract implementation of the Bp interface.
 * 
 * @author RHC
 * 
 * @param <T>
 */
public abstract class AbstractBp<T> implements Bp<T> {

    protected Dao<T> dao;

    protected static Log LOG = LogFactory.getLog(AbstractBp.class);

    /**
     * Initializes the AbstractBp superclass. Takes the concrete Dao instance that should be used. 
     * 
     * @param dao - The Data Access Object that will be used
     */
    protected void initAbstract(Dao<T> dao) {
        this.dao = dao;
    }

    /**
     * Given a database key id, finds the entity in the database.
     * 
     * @param id
     *            - the id to search by
     * @return the entity found with the id, or null if it doesn't exist
     */
    @Override
    public T findById(Long id, Class<T> type) {
        return this.dao.findById(id, type);
    }

    /**
     * Given a modified entity that exists on the database, updates the database
     * with the changed entity.
     * 
     * @param t
     *            - the entity to be updated
     * @return the updated, managed entity
     */
    @Override
    public T update(T toUpdate) {
        return this.dao.update(toUpdate);
    }

    /**
     * Deletes the entity, determined by the id from the database.
     * 
     * @param id
     *            - the id of the entity to be deleted
     */
    @Override
    public void delete(Long id, Class<T> type) {
        this.dao.delete(id, type);
    }

    @Override
    public List<T> searchByExample(T search, int startIndex, int quantity) {
        return this.dao.searchByExample(search, startIndex, quantity);
    }

    @Override
    public List<T> searchByExample(T search, T ordering, int startIndex, int quantity) {
        return this.dao.searchByExample(search, ordering, startIndex, quantity);
    }

    @Override
    public List<T> searchByExactExample(T search, int startIndex, int quantity) {
        return this.dao.searchByExactExample(search, startIndex, quantity);
    }

    @Override
    public List<T> searchByExactExample(T search, T ordering, int startIndex, int quantity) {
        return this.dao.searchByExactExample(search, ordering, startIndex, quantity);
    }

    /**
     * Returns the number of items that will be found if a search with the given
     * entity is done.
     * 
     * @param search
     *            - the entity to be searched by
     * @return The number of items that will be returned.
     */
    @Override
    public long resultQuantity(T search) {
        return this.dao.resultQuantity(search);
    }

    /**
     * Given a type, the starting index, and the number to return, returns an
     * unfiltered List<T> of entities in this database table.
     * 
     * @param type
     *            - the class type of the entities to be returned
     * @param startIndex
     *            - the position of the first item to be returned
     * @param quantity
     *            - the number of items to return. If this is equal to 0,
     *            returns all items.
     * @return A List<T> containing a number of elements determined by the
     *         quantity field.
     */
    @Override
    public List<T> findAll(Class<T> type, int startIndex, int quantity) {
        return this.dao.findAll(type, startIndex, quantity);
    }

    /**
     * Persists the entity t, adding it to the database.
     * 
     * @param t
     *            - the object to be persisted
     * @return the managed entity
     */
    @Override
    public T create(T t) {
        return this.dao.create(t);
    }

    /**
     * This method takes an instance of an object, and returns a clone of the object.
     * 
     * @param toClone - the object to be cloned
     * @return A managed instance of the cloned object.
     */
    @SuppressWarnings("unchecked")
    public T cloneEntity(T toClone) {
        if (toClone == null) {
            return null;
        }
        try {
            //Constructs a new instance of the entity, that will be persisted
            Constructor<T> struct = (Constructor<T>) toClone.getClass().getDeclaredConstructor();
            T toReturn = struct.newInstance();

            return cloneEntity(toClone, toReturn);

        }
        catch (Exception e) {
            LOG.error(e);
            return null;
        }
    }

    /**
     * This method takes a source and a target, and copies the source onto the target.
     * 
     * @param source - the object to be cloned
     * @param target - the target object
     * @return A managed instance of the cloned object.
     */
    public T cloneEntity(T source, T target) {
        if (source == null || target == null) {
            return null;
        }

        try {
            //Makes the current toClone instance managed, to handle lazy loading.
            source = dao.eagerLoad(source);

            Method[] allMethods = source.getClass().getMethods();

            //Copies the properties from the entity to be cloned to the entity to be returned
            PropertyUtils.copyProperties(target, source);

            //finds the method named "setId"
            Method idSetter = target.getClass().getMethod("setId", Long.class);

            //uses the method to set the id to null
            if (idSetter != null) {
                idSetter.invoke(target, (Object) null);
            }

            for (int i = 0; i < allMethods.length; i++) {
                Method getter = allMethods[i];
                if (getter.getName().substring(0, 3).equals("get")
                        && getter.getReturnType().getName().contains("java.util.Set")) {
                    for (int j = 0; j < allMethods.length; j++) {
                        Method setter = allMethods[j];
                        if (setter.getName().substring(0, 3).equals("set")
                                && setter.getName().substring(3).equals(getter.getName().substring(3))) {
                            Object[] emptyParams = new Object[0];
                            //handle lazy loading
                            ((Set<?>) getter.invoke(source, emptyParams)).size();
                            setter.invoke(target, new HashSet<Object>((Set<?>) getter.invoke(source, emptyParams)));
                        }
                    }
                }
            }

            //returns the entity
            return target;

        }
        catch (Exception e) {
            LOG.error(e);
            return null;
        }
    }

    public static boolean areClones(Object o1, Object o2) {
        Method[] o1Methods = o1.getClass().getMethods();
        Method[] o2Methods = o2.getClass().getMethods();
        List<Method> o1Getters = new ArrayList<>();
        List<Method> o2Getters = new ArrayList<>();
        for (Method m : o1Methods) {
            String methodName = m.getName();
            if (methodName.startsWith("get") || methodName.startsWith("is")) {
                o1Getters.add(m);
            }
        }        
        for (Method m : o2Methods) {
            String methodName = m.getName();
            if (methodName.startsWith("get") || methodName.startsWith("is")) {
                o2Getters.add(m);
            }
        }
        for (Method m1 : o1Getters) {
            for (Method m2 : o2Getters) {
                if (m1.getName().equals(m2.getName()) && !m1.getName().equals("getId")) {
                    try {
                        if (!fieldsEqual(m1.invoke(o1, new Object[0]), m2.invoke(o2, new Object[0]))) {
                            return false;
                        }
                    } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
                        LOG.error(ex);
                    }
                }
            }
        }
        return true;
    }

    public static boolean fieldsEqual(Object o1, Object o2) {
        if (o1 == null) {
            return o2 == null;
        }
        else {
            return o1.equals(o2);
        }
    }

    /**
     * Provided with the CSV format file, this method bulk create the entities
     * 
     * @param inputStream
     *            CSV file input stream
     * @param type
     *            Type of entities to be bulk created
     */
    public Set<T> createEntityFromCSV(InputStream inputStream, Class<T> type) {
        Set<T> createdEntities = new HashSet<T>();
        try {

            List<String> lines = IOUtils.readLines(inputStream);
            HashMap<String, Method> entityFoundMethods = new HashMap<String, Method>();

            if (lines.size() > 0) {
                String[] entityValues = null;
                String header = lines.get(0);
                String[] fields = header.split(",");
                Class<?> entityClassDef = Class.forName(type.getName());
                Method[] entityClassMethods = entityClassDef.getMethods();
                Method setterMethod = null;
                Constructor<T> struct = type.getDeclaredConstructor();
                for (int index = 1; index < lines.size(); index++) {
                    T entity = struct.newInstance();
                    entityValues = lines.get(index).split(",");
                    for (int i = 0; i < fields.length; i++) {
                        if (entityFoundMethods.isEmpty() || !entityFoundMethods.containsKey(fields[i])) {
                            for (Method method : entityClassMethods) {
                                if (method.getName().equalsIgnoreCase("set" + fields[i])) {
                                    setterMethod = method;
                                    entityFoundMethods.put(fields[i], setterMethod);
                                    break;
                                }
                            }
                        }
                        else {
                            setterMethod = entityFoundMethods.get(fields[i]);
                        }
                        setEntityValue(entity, setterMethod, entityValues[i]);
                    }
                    entity = this.create(entity);
                    createdEntities.add(entity);
                }
            }
        }
        catch (IOException ex) {
            LOG.error(ex);
        }
        catch (ClassNotFoundException ex) {
            LOG.error(ex);
        }
        catch (Exception ex) {
            LOG.error(ex);
        }
        return createdEntities;
    }

    /**
     * Set entity field value based on field type
     * 
     * @param entity
     *            Entity to be created
     * @param setterMethod
     *            Entity setter method
     * @param fieldValue
     *            Entity field value
     * @throws Exception
     */
    private void setEntityValue(T entity, Method setterMethod, String fieldValue) throws Exception {

        Field[] fields = entity.getClass().getDeclaredFields();
        Class<?> fieldClass = null;
        Object[] args = new Object[1];

        try {
            for (Field field : fields) {
                if (StringUtils.replace(setterMethod.getName(), "set", "").equalsIgnoreCase(field.getName())) {
                    fieldClass = field.getType();
                    break;
                }
            }

            if (fieldClass != null) {
                if (fieldClass == String.class) {
                    args[0] = fieldValue;
                }
                else if (fieldClass == Integer.class) {
                    args[0] = Integer.valueOf(fieldValue);
                }
                else if (fieldClass == Long.class) {
                    args[0] = Long.valueOf(fieldValue);
                }
                else if (fieldClass == java.util.Date.class) {
                    java.text.SimpleDateFormat bartDateFormat = new java.text.SimpleDateFormat("MM/dd/yyyy");
                    args[0] = bartDateFormat.parseObject(fieldValue);
                }
                else {
                }
                setterMethod.setAccessible(true);
                setterMethod.invoke(entity, args);
            }
        }
        catch (Exception ex) {
            LOG.error(ex);
            throw ex;
        }
    }

    @Override
    public T eagerLoad(T toLoad) {
        return this.dao.eagerLoad(toLoad);
    }

    @Override
    public void createNoRefresh(T toCreate) {
        this.dao.createNoRefresh(toCreate);
    }

    @Override
    public T findAndLock(Long id, Class<T> type) {
        return this.dao.findAndLock(id, type);
    }

    //Used to load a specific field of a hibernate object manually while the session is open
    protected void load(Object toLoad) {
        if (toLoad == null) {
            return;
        }
        if (toLoad instanceof List) {
            ((List) toLoad).size();

        }
        else {
            toLoad.toString();
        }
    }
}
