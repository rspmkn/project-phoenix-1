package com.apd.phoenix.service.persistence.jpa;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.Query;
import org.apache.commons.lang.StringUtils;
import com.apd.phoenix.service.model.HierarchyNode;

/**
 * HierarchyNode DAO stub
 * 
 * @author RHC
 *
 */
@Stateless
@LocalBean
public class HierarchyNodeDao extends AbstractDao<HierarchyNode> {

    @SuppressWarnings("unchecked")
    public List<HierarchyNode> getChildren(HierarchyNode hierarchyNode) {
        if (hierarchyNode != null) {
            //TODO:Check the skutype?
            String hql = "from HierarchyNode h where h.parent = :node";
            Query query = entityManager.createQuery(hql);
            query.setParameter("node", hierarchyNode);
            List<HierarchyNode> results = query.getResultList();
            return results;
        }
        return new ArrayList<HierarchyNode>();
    }

    public List<String> getTopLevelCategory(String description) {
        List<String> results = new ArrayList<String>();
        if (!StringUtils.isEmpty(description)) {
            String hql = "select distinct h.description from HierarchyNode h "
                    + "where h.parent is null and lower(h.description) like ?1";
            Query query = entityManager.createQuery(hql);
            query.setParameter(1, "%" + description.trim().toLowerCase() + "%");
            results = query.getResultList();
        }
        return results;
    }

    @SuppressWarnings("unchecked")
    public List<HierarchyNode> searchByPublicIdAndType(Long publicId, String type) {
        String hql = "select h from HierarchyNode h where h.publicId = :publicId and h.type = :type";
        Query query = entityManager.createQuery(hql);
        query.setParameter("publicId", publicId);
        query.setParameter("type", type);
        List<HierarchyNode> results = query.getResultList();
        return results;
    }
}
