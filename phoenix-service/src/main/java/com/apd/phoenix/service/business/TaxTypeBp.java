package com.apd.phoenix.service.business;

import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import com.apd.phoenix.service.model.TaxType;
import com.apd.phoenix.service.persistence.jpa.TaxTypeDao;

/**
 * This class provides business process methods for TaxType.
 * 
 * @author RHC
 *
 */
@Stateless
@LocalBean
public class TaxTypeBp extends AbstractBp<TaxType> {

    /**
     * This method is injected with the correct Dao for initialization
     * 
     * @param dao
     */
    @Inject
    public void initDao(TaxTypeDao dao) {
        this.dao = dao;
    }

    public TaxType getByName(String name) {
        TaxType searchType = new TaxType();
        searchType.setName(name);
        List<TaxType> typeList = this.searchByExactExample(searchType, 0, 0);
        if (typeList.isEmpty()) {
            return null;
        }
        return typeList.get(0);
    }

}
