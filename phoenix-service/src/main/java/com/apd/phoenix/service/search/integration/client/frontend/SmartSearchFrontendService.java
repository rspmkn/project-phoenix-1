package com.apd.phoenix.service.search.integration.client.frontend;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import javax.annotation.PostConstruct;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.core.utility.TenantConfigRepository;
import com.apd.phoenix.service.business.VendorBp;
import com.apd.phoenix.service.model.Manufacturer;
import com.apd.phoenix.service.model.Vendor;
import com.apd.phoenix.service.persistence.multitenancy.impl.CurrentTenantIdentifierResolverImpl;
import com.apd.phoenix.service.product.Product;
import com.apd.phoenix.service.product.ProductCacheManager;
import com.apd.phoenix.service.product.ProductNotFoundException;
import com.apd.phoenix.service.search.integration.client.SmartSearchConstants;
import com.apd.phoenix.service.utility.SmartSearchUtils;
import com.ussco.ws.ecatalog.catalog._1.ECatalogInterface;
import com.ussco.ws.ecatalog.catalog._1.ECatalogService;
import com.ussco.ws.ecatalog.catalog._1.FilterListType;
import com.ussco.ws.ecatalog.catalog._1.FilterType;
import com.ussco.ws.ecatalog.catalog._1.FilterValueType;
import com.ussco.ws.ecatalog.catalog._1.GetItemsRequest;
import com.ussco.ws.ecatalog.catalog._1.GetItemsResponse;
import com.ussco.ws.ecatalog.catalog._1.GetRelatedItemRequest;
import com.ussco.ws.ecatalog.catalog._1.GetRelatedItemResponse;
import com.ussco.ws.ecatalog.catalog._1.GetSearchRequest;
import com.ussco.ws.ecatalog.catalog._1.GetSearchResponse;
import com.ussco.ws.ecatalog.catalog._1.GetSuppliesFinderRequest;
import com.ussco.ws.ecatalog.catalog._1.GetSuppliesFinderResponse;
import com.ussco.ws.ecatalog.catalog._1.ItemCrossReferenceType;
import com.ussco.ws.ecatalog.catalog._1.ItemListType;
import com.ussco.ws.ecatalog.catalog._1.ItemReferenceFilterListType;
import com.ussco.ws.ecatalog.catalog._1.ItemType;
import com.ussco.ws.ecatalog.catalog._1.ItemsEntryType;
import com.ussco.ws.ecatalog.catalog._1.ItemsRequestType;
import com.ussco.ws.ecatalog.catalog._1.ListNameType;
import com.ussco.ws.ecatalog.catalog._1.RelatedItemRequestType;
import com.ussco.ws.ecatalog.catalog._1.RequestHeaderType;
import com.ussco.ws.ecatalog.catalog._1.SearchRequestType;
import com.ussco.ws.ecatalog.catalog._1.SortType;
import com.ussco.ws.ecatalog.catalog._1.SuppliesFinderRequestType;

@Stateless
@LocalBean
public class SmartSearchFrontendService {

    public static final String DEFAULT_VENDOR_NAME = "USSCO";

    private static final Logger logger = LoggerFactory.getLogger(SmartSearchFrontendService.class);

    private static final Properties commonProps = TenantConfigRepository.getInstance().getProperties("smart.search");
    private static final String serviceUrl = (String) commonProps.get("serviceUrl");
    private static final String DEFAULT_SORT_TYPE = "BM"; //best match used by default

    public static final int SORT_BY_PRICE_MAX_QUANTITY = Integer.valueOf((String) commonProps
            .get("sortByPriceQuantity"));

    @Inject
    private ProductCacheManager cacheManager;

    @Inject
    private VendorBp vendorBp;

    @PostConstruct
    public void init() {

    }

    private GetSearchRequest buildEmptyGetSearchObject(String sessionId, String userAgent, List<Long> listIds,
            List<Long> coreListIds) {
        if (StringUtils.isEmpty(sessionId)) {
            logger.error("Session id is null, will not be able to reach smartSearch");
        }
        GetSearchRequest searchRequest = new GetSearchRequest();
        searchRequest.setRequestHeader(this.getRequestHeader(sessionId, userAgent, listIds, coreListIds));
        searchRequest.getRequestHeader().setResultStyle(SmartSearchUtils.getItemResultStyle("List"));
        searchRequest.getRequestHeader().setCatNavDepth(2);
        searchRequest.setSearchRequest(new SearchRequestType());
        searchRequest.getSearchRequest().setSearchFilters(new FilterListType());
        return searchRequest;
    }

    public GetSearchResponse searchListItemsByKeyword(String sessionId, String userAgent, List<Long> listIds,
            List<Long> coreListIds, List<FilterType> filters, BigInteger page, BigInteger itemSize) {
        return this.searchListItemsByKeyword(sessionId, userAgent, listIds, coreListIds, filters, DEFAULT_SORT_TYPE,
                page, itemSize, true, null);
    }

    public GetSearchResponse searchListItemsByKeyword(String sessionId, String userAgent, List<Long> listIds, List<Long> coreListIds,
            List<FilterType> filters, String sortType, BigInteger page, BigInteger itemSize, boolean showNonCore,
            List<Long> customerCatalogId) {
    	//Create tenant-specific connection to SS
    	ECatalogInterface port = this.createTenantSpecificPortToSS();
    	
        //these values are populated if the ordering of items on the page is determined using sort by price
        List<Product> productsOnPage = null;
        Map<Integer, String> indexToPartNumber = new HashMap<>();
        Map<Integer, String> indexToPartType = new HashMap<>();
        Map<Product, Integer> productToIndex = new HashMap<>();
        if (sortType.equals("PA") || sortType.equals("PD")) {
            //if one of the price sorting methods is used, begins the sort by price process
        	//if not using sort by price, this conditional is ignored
            GetSearchRequest sortProducts = this.buildSearchRequestWithoutPageData(sessionId, userAgent, listIds, coreListIds,
                    filters, showNonCore);
            sortProducts.getSearchRequest().setSearchSort(SmartSearchUtils.getSortType(DEFAULT_SORT_TYPE));
            sortProducts.getSearchRequest().setStartAtResult(BigInteger.ONE);
            sortProducts.getSearchRequest().setBufferSize(new BigInteger(SORT_BY_PRICE_MAX_QUANTITY + ""));
            //builds the search query and sends the request, for the full buffer of items
            GetSearchResponse result = port.getSearch(sortProducts);
            //if the total results is still less than the sort by price quantity
            if (result.getSearchResponse().getItemPage().getTotalResults().intValue() <= SORT_BY_PRICE_MAX_QUANTITY) {
                for (int index = 0; index < result.getSearchResponse().getItemPage().getItems().getItem().size(); index++) {
                	ItemType item = result.getSearchResponse().getItemPage().getItems().getItem().get(index);
                    if (StringUtils.isNotBlank(item.getDealerItemNumber())) {
                    	indexToPartNumber.put(index, item.getDealerItemNumber());
                    	indexToPartType.put(index, "DealerItemNumber");
                    }
                    else {
                    	indexToPartNumber.put(index, item.getItemNumber());
                    	indexToPartType.put(index, "UnitedItemNumber");
                    }
                }
                //fetches the products for each of the skus
                List<Product> products = new ArrayList<Product>(indexToPartType.keySet().size());
                List<String> foundVendors = new ArrayList<String>();
                foundVendors.add(DEFAULT_VENDOR_NAME);
                for (Integer index : indexToPartNumber.keySet()) {
                	String partNumber = indexToPartNumber.get(index);
                    String sku = SmartSearchFrontendService.skuFromDealerPart(partNumber);
                    String vendor = vendorFromDealerPart(partNumber, foundVendors);
                    Product toAdd = null;
                    try {
                    	toAdd = cacheManager.getProduct(sku, vendor, customerCatalogId);
                    }
                    catch (ProductNotFoundException e) {
                        if (logger.isDebugEnabled()) {
                            logger.debug("Item not found " + sku);
                        }
                        //for Products that were not found, the APD SKU will store the part number from smart search
                        toAdd = new Product();
                        toAdd.setApdSku(partNumber);
                    }
                    products.add(toAdd);
                    productToIndex.put(toAdd, index);
                }
                //sorts the products
                Collections.sort(products, new ProductComparator(sortType.equals("PA")));
                //and uses the sorted products to filter the products on the page
                productsOnPage = products.subList(page.intValue() - 1, Math.min(page.intValue() - 1
                        + itemSize.intValue(), products.size()));
            }
            sortType = DEFAULT_SORT_TYPE;
        }

        GetSearchRequest searchRequest = this.buildSearchRequestWithoutPageData(sessionId, userAgent, listIds, coreListIds,
                filters, showNonCore);

        //this conditional block is ignored if not using sort by price.
        if (productsOnPage != null && !productsOnPage.isEmpty()) {
            searchRequest.getSearchRequest().setItemReferenceFilters(new ItemReferenceFilterListType());
            page = BigInteger.ZERO;
            for (int i = 0; i < productsOnPage.size(); i++) {
                Product product = productsOnPage.get(i);
                ItemCrossReferenceType item = new ItemCrossReferenceType();
                item.setSequence(new BigInteger("" + i));
                item.setReferenceType("DealerItemNumber");
                item.setItemNumber(indexToPartNumber.get(productToIndex.get(product)));
                item.setReferenceType(indexToPartType.get(productToIndex.get(product)));
                searchRequest.getSearchRequest().getItemReferenceFilters().getItemReference().add(item);
            }
        }

        searchRequest.getSearchRequest().setSearchSort(SmartSearchUtils.getSortType(sortType));
        searchRequest.getSearchRequest().setStartAtResult(page);
        searchRequest.getSearchRequest().setBufferSize(itemSize);
        GetSearchResponse result = port.getSearch(searchRequest);
        return result;
    }

    private GetSearchRequest buildSearchRequestWithoutPageData(String sessionId, String userAgent, List<Long> listIds,
            List<Long> coreListIds, List<FilterType> filters, boolean showNonCore) {
        GetSearchRequest searchRequest = buildEmptyGetSearchObject(sessionId, userAgent, listIds, coreListIds);
        if (!showNonCore) {
            for (ListNameType list : searchRequest.getRequestHeader().getContractList().getItemList()) {
                list.setIncExcType("SurfacingPlusFilter");
            }
        }
        searchRequest.getSearchRequest().getSearchFilters().getFilter().addAll(filters);
        if (filters == null || filters.isEmpty()) {
            searchRequest.getSearchRequest().setSearchFilters(null);
        }
        return searchRequest;
    }

    public GetRelatedItemResponse getRelatedItems(String sessionId, String userAgent, String vendorSku,
            List<Long> listIds, List<Long> coreListIds) {
        //Create tenant-specific connection to SS
        ECatalogInterface port = this.createTenantSpecificPortToSS();

        GetRelatedItemRequest relatedItemRequest = new GetRelatedItemRequest();
        relatedItemRequest.setRequestHeader(this.getRequestHeader(sessionId, userAgent, listIds, coreListIds));
        relatedItemRequest.setRelatedItemRequest(new RelatedItemRequestType());
        relatedItemRequest.getRelatedItemRequest().getItemNumber().add(vendorSku);
        return port.getRelatedItems(relatedItemRequest);
    }

    public GetSuppliesFinderResponse getMatchbooks(String sessionId, String userAgent, List<Long> listIds,
            List<Long> coreListIds, Manufacturer manufacturer) {

        ECatalogInterface port = this.createTenantSpecificPortToSS();

        GetSuppliesFinderRequest suppliesFinderRequest = buildBasicSuppliesFinderRequest(listIds, coreListIds,
                sessionId, userAgent);
        FilterType filterType = buildSuppliesFinderFilter();
        filterType.setSequence(BigInteger.ONE);
        FilterValueType filterValueType = new FilterValueType();
        filterValueType.setDescription(manufacturer.getName());
        filterValueType.setValue(manufacturer.getSmartSearchCode());
        filterValueType.setSequence(BigInteger.ONE);
        filterType.getFilterValue().add(filterValueType);
        suppliesFinderRequest.getSuppliesFinderRequest().getSearchFilters().getFilter().add(filterType);

        return port.getSuppliesFinder(suppliesFinderRequest);
    }

    public GetSuppliesFinderResponse matchbookManufactuerersFromCatalog(List<Long> listIds, List<Long> coreListIds,
            String sessionId, String userAgent) {

        ECatalogInterface port = this.createTenantSpecificPortToSS();

        GetSuppliesFinderRequest suppliesFinderRequest = buildBasicSuppliesFinderRequest(listIds, coreListIds,
                sessionId, userAgent);
        FilterType filterType = buildSuppliesFinderFilter();
        filterType.setSequence(BigInteger.ONE);
        FilterValueType filterValueType = new FilterValueType();
        filterValueType.setDescription("Supplies Finder Entrance Page");
        filterValueType.setValue("");
        filterValueType.setSequence(BigInteger.ONE);
        filterType.getFilterValue().add(filterValueType);
        suppliesFinderRequest.getSuppliesFinderRequest().getSearchFilters().getFilter().add(filterType);

        return port.getSuppliesFinder(suppliesFinderRequest);
    }

    public GetItemsResponse getItem(String sessionId, String userAgent, List<String> vendorAndApdSkuList,
            List<Long> listIds, List<Long> coreListIds) {

        ECatalogInterface port = this.createTenantSpecificPortToSS();

        GetItemsRequest itemsRequest = new GetItemsRequest();
        itemsRequest.setRequestHeader(this.getRequestHeader(sessionId, userAgent, listIds, coreListIds));

        itemsRequest.getRequestHeader().setResultStyle(SmartSearchUtils.getItemResultStyle("Detail"));
        itemsRequest.setItemsRequest(new ItemsRequestType());
        for (String vendorAndApdSku : vendorAndApdSkuList) {
            ItemsEntryType itemEntry = new ItemsEntryType();
            itemEntry.setItemReference(new ItemCrossReferenceType());
            itemEntry.getItemReference().setReferenceType("DealerItemNumber");
            itemEntry.getItemReference().setSequence(
                    new BigInteger("" + itemsRequest.getItemsRequest().getItemEntry().size()));
            itemEntry.getItemReference().setItemNumber(vendorAndApdSku);
            itemsRequest.getItemsRequest().getItemEntry().add(itemEntry);
        }
        return port.getItems(itemsRequest);
    }

    private GetSuppliesFinderRequest buildBasicSuppliesFinderRequest(List<Long> listIds, List<Long> coreListIds,
            String sessionId, String userAgent) {
        GetSuppliesFinderRequest suppliesFinderRequest = new GetSuppliesFinderRequest();
        suppliesFinderRequest.setRequestHeader(getRequestHeader(sessionId, userAgent, listIds, coreListIds));

        suppliesFinderRequest.setSuppliesFinderRequest(new SuppliesFinderRequestType());
        suppliesFinderRequest.getSuppliesFinderRequest().setSearchFilters(new FilterListType());
        SortType sortType = new SortType();
        sortType.setSortCode("BM");
        suppliesFinderRequest.getSuppliesFinderRequest().setSearchSort(sortType);
        suppliesFinderRequest.getSuppliesFinderRequest().setStartAtResult(BigInteger.ONE);
        suppliesFinderRequest.getSuppliesFinderRequest().setBufferSize(BigInteger.TEN);
        return suppliesFinderRequest;
    }

    private RequestHeaderType getRequestHeader(String sessionId, String userAgent, List<Long> listIds,
            List<Long> coreListIds) {
        RequestHeaderType toReturn = new RequestHeaderType();
        toReturn.setIdentificationRequest(SmartSearchUtils.buildIdentificationType(sessionId, userAgent));
        if (!listIds.isEmpty()) {
            toReturn.setIncExcList(new ItemListType());
            for (Long listId : listIds) {
                toReturn.getIncExcList().getItemList().add(
                        SmartSearchUtils.getIncExcList(listId.toString(), "Inclusion"));
            }
        }
        if (!coreListIds.isEmpty()) {
            toReturn.setContractList(new ItemListType());
            for (Long coreListId : coreListIds) {
                toReturn.getContractList().getItemList().add(
                        SmartSearchUtils.getIncExcList(coreListId.toString(), "SurfacingPlusFilter"));
            }
        }
        return toReturn;
    }

    private FilterType buildSuppliesFinderFilter() {
        FilterType filterType = new FilterType();
        filterType.setKeywordInterface("SuppliesFinder");
        filterType.setFilterStyle("SuppliesFinderBrand");
        filterType.setFilterDescription("SuppliesFinderBrand");
        return filterType;
    }

    private class ProductComparator implements Comparator<Product> {

        private boolean ascending = true;

        private ProductComparator(boolean ascending) {
            this.ascending = ascending;
        }

        @Override
        public int compare(Product arg0, Product arg1) {
            if ((arg0 == null || arg0.getPrice() == null) && (arg1 == null || arg1.getPrice() == null)) {
                return 0;
            }
            if (arg0 == null || arg0.getPrice() == null) {
                return 1;
            }
            if (arg1 == null || arg1.getPrice() == null) {
                return -1;
            }
            return (ascending ? 1 : -1) * arg0.getPrice().compareTo(arg1.getPrice());
        }
    }

    /**
     * This method takes the dealer part number specified on a Smart Search item, and the APD SKU of the item.
     * 
     * The reason this is used is because the dealer part number encodes both the vendor name and the APD SKU, to guarantee
     * uniqueness across catalogs.
     * 
     * @param dealerPartNumber
     * @return
     */
    public static String skuFromDealerPart(String dealerPartNumber) {
        if (StringUtils.isBlank(dealerPartNumber)) {
            return dealerPartNumber;
        }
        int index = StringUtils.lastIndexOf(dealerPartNumber, SmartSearchConstants.DEALER_KEYWORD_DELIMETER);
        if (index > 0) {
            return dealerPartNumber.substring(0, index);
        }
        else {
            return dealerPartNumber;
        }
    }

    private String vendorFromDealerPart(String dealerPartNumber, List<String> foundVendors) {
        String apdSku = skuFromDealerPart(dealerPartNumber);
        if (StringUtils.isBlank(dealerPartNumber) || StringUtils.isBlank(apdSku) || apdSku.equals(dealerPartNumber)) {
            return DEFAULT_VENDOR_NAME;
        }
        int index = apdSku.length() + SmartSearchConstants.DEALER_KEYWORD_DELIMETER.length();
        if (index < dealerPartNumber.length()) {
            String caseInsensitiveVendorName = dealerPartNumber.substring(index);
            for (String foundVendor : foundVendors) {
                if (foundVendor.equalsIgnoreCase(caseInsensitiveVendorName)) {
                    return foundVendor;
                }
            }
            Vendor vendor = this.vendorBp.findByNameCaseInsensitive(caseInsensitiveVendorName);
            if (vendor != null && StringUtils.isNotBlank(vendor.getName())) {
                foundVendors.add(vendor.getName());
                return vendor.getName();
            }
        }
        return DEFAULT_VENDOR_NAME;
    }

    private ECatalogInterface createTenantSpecificPortToSS() {
        //Create tenant-specific connection to SS
        ECatalogService ss = new ECatalogService();
        String username = TenantConfigRepository.getInstance().getProperty(
                CurrentTenantIdentifierResolverImpl.getCurrentTenant(), "tenant", "smart_search_user");
        String password = TenantConfigRepository.getInstance().getProperty(
                CurrentTenantIdentifierResolverImpl.getCurrentTenant(), "tenant", "smart_search_password");
        ECatalogInterface port = ss.getECatalogService(serviceUrl, username, password);
        return port;
    }

}
