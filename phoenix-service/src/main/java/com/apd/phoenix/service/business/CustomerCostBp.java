package com.apd.phoenix.service.business;

import java.math.BigDecimal;
import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import com.apd.phoenix.service.model.Account;
import com.apd.phoenix.service.model.Catalog;
import com.apd.phoenix.service.model.CustomerCost;
import com.apd.phoenix.service.persistence.jpa.CustomerCostDao;

/**
 * This class provides business process methods for CustomeCost.
 * 
 * @author RHC
 * 
 */
@Stateless
@LocalBean
public class CustomerCostBp extends AbstractBp<CustomerCost> {

    /**
     * This method is injected with the correct Dao for initialization
     * 
     * @param dao
     */
    @Inject
    public void initDao(CustomerCostDao dao) {
        this.dao = dao;
    }

    public List<Account> costList(Catalog catalog) {
        return ((CustomerCostDao) dao).costList(catalog);
    }

    public BigDecimal getItemCost(Long itemId, Long customerId) {
        if (itemId == null || customerId == null) {
            return null;
        }
        return ((CustomerCostDao) dao).getItemCost(itemId, customerId);
    }
}
