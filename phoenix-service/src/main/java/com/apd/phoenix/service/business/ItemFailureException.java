package com.apd.phoenix.service.business;

import javax.ejb.ApplicationException;
import com.apd.phoenix.service.model.SyncItemResult;

//This annotation is used to ensure that the following exception will not be logged, but will still cause the transaction to be set as rollback
@ApplicationException(rollback = true)
public class ItemFailureException extends RuntimeException {

    private static final long serialVersionUID = 7494904950613590529L;

    private Exception exception;

    public Exception getException() {
        return exception;
    }

    public void setException(Exception exception) {
        this.exception = exception;
    }
}
