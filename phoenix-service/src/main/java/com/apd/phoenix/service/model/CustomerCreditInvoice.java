/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.apd.phoenix.service.model;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToOne;
import org.hibernate.annotations.Index;

@Entity
@DiscriminatorValue(value = "CUSTOMER_CREDIT_INVOICE")
public class CustomerCreditInvoice extends Invoice {

    private static final long serialVersionUID = -8180038493863369830L;

    @Index(name = "INVOICE_RETURNORDER_IDX")
    @OneToOne(fetch = FetchType.LAZY)
    private ReturnOrder returnOrder;

    public ReturnOrder getReturnOrder() {
        return returnOrder;
    }

    public void setReturnOrder(ReturnOrder returnOrder) {
        this.returnOrder = returnOrder;
    }

}
