package com.apd.phoenix.service.business;

import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import com.apd.phoenix.service.model.CustomerOrder;
import com.apd.phoenix.service.model.MessageMetadata;
import com.apd.phoenix.service.persistence.jpa.MessageMetadataDao;

@Stateless
@LocalBean
public class MessageMetadataBp extends AbstractBp<MessageMetadata> {

    /**
     * This method is injected with the correct Dao for initialization
     * 
     * @param dao
     */
    @Inject
    public void initDao(MessageMetadataDao dao) {
        this.dao = dao;
    }

    public List<CustomerOrder> getCustomerOrdersPlacedInLastYearByTransaction(String transactionId, String senderId) {
        return ((MessageMetadataDao) dao).getCustomerOrdersPlacedInLastYearByTransaction(transactionId, senderId);
    }

    public List<CustomerOrder> getCustomerOrdersPlacedInLastYearByGroupControlNumber(String groupControlNumber,
            String senderId) {
        return ((MessageMetadataDao) dao).getCustomerOrdersPlacedInLastYearByGroupControlNumber(groupControlNumber,
                senderId);

    }
}
