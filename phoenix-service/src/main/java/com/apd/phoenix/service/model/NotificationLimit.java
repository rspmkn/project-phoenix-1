package com.apd.phoenix.service.model;

import java.io.Serializable;
import java.util.Comparator;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Version;
import com.apd.phoenix.service.business.EntityComparator.GenericComparator;
import com.apd.phoenix.service.model.OrderLog.EventType;

/**
 * This entity has a limit type and event type, specifying which emails should be sent for a given event.
 * 
 * @author RHC
 *
 */
@Entity
public class NotificationLimit implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id = null;
    @Version
    @Column(name = "version", nullable = false)
    private int version = 0;

    @Enumerated(EnumType.STRING)
    private LimitType limitType;

    @Enumerated(EnumType.STRING)
    private EventType eventType;

    public Long getId() {
        return this.id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public int getVersion() {
        return this.version;
    }

    public void setVersion(final int version) {
        this.version = version;
    }

    public LimitType getLimitType() {
        return limitType;
    }

    public void setLimitType(LimitType limitType) {
        this.limitType = limitType;
    }

    public EventType getEventType() {
        return eventType;
    }

    public void setEventType(EventType eventType) {
        this.eventType = eventType;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        if (id != null) {
            return id.equals(((NotificationLimit) that).id);
        }
        return super.equals(that);
    }

    @Override
    public int hashCode() {
        if (id != null) {
            return id.hashCode();
        }
        return super.hashCode();
    }

    public static enum LimitType {
        ALL, CC_ONLY, BCC_ONLY, NONE
    }

    public static class NotificationLimitComparator implements Comparator<NotificationLimit> {

        @Override
        public int compare(NotificationLimit limit0, NotificationLimit limit1) {

            if (limit0.getEventType() != null && limit1.getEventType() != null) {
                return limit0.getEventType().name().compareTo(limit1.getEventType().name());
            }
            if (limit0.getEventType() != null) {
                return -1;
            }
            if (limit1.getEventType() != null) {
                return 1;
            }
            return (new GenericComparator()).compare(limit0, limit1);
        }
    }
}