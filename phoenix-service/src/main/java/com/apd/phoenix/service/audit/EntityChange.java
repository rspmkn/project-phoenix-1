package com.apd.phoenix.service.audit;

import java.util.Date;
import java.util.Map;

public class EntityChange {

    private Date timestamp;
    private String username;
    private Map<String, String[]> entityDifferences;
    private long primaryKey;

    public long getPrimaryKey() {
        return primaryKey;
    }

    public void setPrimaryKey(long primaryKey) {
        this.primaryKey = primaryKey;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Map<String, String[]> getEntityDifferences() {
        return entityDifferences;
    }

    public void setEntityDifferences(Map<String, String[]> entityDifferences) {
        this.entityDifferences = entityDifferences;
    }

}
