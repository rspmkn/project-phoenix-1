package com.apd.phoenix.service.model.listener;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Properties;
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.ObjectMessage;
import javax.jms.Session;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.core.utility.TenantConfigRepository;
import com.apd.phoenix.service.persistence.multitenancy.impl.CurrentTenantIdentifierResolverImpl;
import com.apd.phoenix.service.solr.IndexingException;

public class SolrEntityListener {

    static final Logger logger = LoggerFactory.getLogger(SolrEntityListener.class);

    private static final Properties catalogProperties = TenantConfigRepository.getInstance().getProperties(
            "catalog.upload.integration");
    private static final String connectionFactoryJndi = catalogProperties
            .getProperty("solrItemUpdateQueueConnectionJndi");
    private static final String queueJndi = catalogProperties.getProperty("solrItemUpdateQueueJndi");

    protected static void doSolrDocumentAction(Serializable content, SolrDocumentAction action) throws NamingException,
            JMSException {
        ArrayList<Serializable> contents = new ArrayList<Serializable>();
        contents.add(content);
        doSolrDocumentActions(contents, action);
    }

    protected static void doSolrDocumentActions(ArrayList<Serializable> contents, SolrDocumentAction action)
            throws NamingException, JMSException {
        //Send to solr queue
        InitialContext context = new InitialContext();
        ConnectionFactory connectionFactory = (ConnectionFactory) context.lookup(connectionFactoryJndi);
        Destination itemQueue = (Destination) context.lookup(queueJndi);
        Connection connection = connectionFactory.createConnection();
        try {
            if (logger.isDebugEnabled()) {
                logger.debug("Sending " + action.name() + " message");
            }
            Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            MessageProducer messageProducer = session.createProducer(itemQueue);
            for (Serializable content : contents) {
                SolrDocumentMessage message = new SolrDocumentMessage();
                message.setContent(content);
                message.setAction(action);
                ObjectMessage objectMessage = session.createObjectMessage(message);
                objectMessage.setLongProperty("tenantId", CurrentTenantIdentifierResolverImpl.getCurrentTenant());
                messageProducer.send(objectMessage);
            }
        }
        catch (JMSException ex) {
            throw new IndexingException("Exception during solr message creation: " + contents, ex);
        }
        finally {
            try {
                connection.close();
            }
            catch (Exception e) {
                logger.error("Could not close connection", e);
            }
        }

        if (logger.isTraceEnabled()) {
            logger.trace("Object " + action.name() + "D. " + contents);
        }
    }

    public enum SolrDocumentAction {
        CATXI_INSERT, CATXI_UPDATE, CATXI_DELETE, CATXI_OVERRIDES_RECALC, CATXI_CACHE, ITEM_UPDATE, ITEM_DELETE, CATXI_REPRICE, SMARTSEARCH_LIST_INSERT, SMARTSEARCH_LIST_REMOVE;
    }

    public SolrEntityListener() {
        super();
    }

    public static class SolrDocumentMessage implements Serializable {

        private static final long serialVersionUID = 1L;
        private Serializable content;
        private SolrDocumentAction action;

        public SolrDocumentAction getAction() {
            return action;
        }

        public void setAction(SolrDocumentAction action) {
            this.action = action;
        }

        public Serializable getContent() {
            return content;
        }

        public void setContent(Serializable content) {
            this.content = content;
        }
    }
}