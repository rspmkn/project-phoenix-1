package com.apd.phoenix.service.message.api;

import java.io.InputStream;
import com.apd.phoenix.service.model.CustomerOrder;
import com.apd.phoenix.service.model.MessageMetadata;
import com.apd.phoenix.service.model.MessageMetadata.MessageType;

public interface MessageUtils {

    public boolean createMessage(Message message);

    public Message retrieveMessage(MessageMetadata metadata);

    public boolean updateMessage(Message message);

    public boolean deleteMessage(MessageMetadata metadata);

    public Message createMessage(CustomerOrder order, InputStream content, long contentLength, MessageType messageType,
            String destination, String ccRecipients, String bccRecipients);

    public Message createUserModificationMessage(InputStream content, long contentLength, MessageType messageType,
            String destination, String ccRecipients, String bccRecipients);
}
