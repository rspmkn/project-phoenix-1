/*
 * 
 */
package com.apd.phoenix.service.model;

import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import com.apd.phoenix.service.business.EntityComparator.GenericComparator;
import javax.persistence.Version;
import javax.xml.bind.annotation.XmlRootElement;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.envers.Audited;

/**
 * The Class ServiceLog.
 */
@Entity
@Table(name = "ISSUE_LOG")
public class IssueLog extends ContactLog {

    @Temporal(TemporalType.TIMESTAMP)
    private Date scheduledFollowUp;

    @Temporal(TemporalType.TIMESTAMP)
    private Date expectedResolution;

    @Column(length = 1000)
    private String issueDescription;

    @Enumerated(EnumType.STRING)
    private IssueCategory issueCategory;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "issuelog_id")
    private Set<ContactLogComment> comments = new HashSet<ContactLogComment>();

    @ManyToOne(fetch = FetchType.LAZY)
    private SystemUser reporter;

    public enum IssueCategory {
        ORDER_CANCELLATION("Cancellation of order"), CATALOG_REQUEST("Catalog Request"), DELIVERY_ISSUES(
                "Delivery Issues"), GENERAL_INQUIRIES("General Inquiries"), INVOICE_REQUEST("Invoice request"), ITEM_SEARCH_ASSISTANCE(
                "Item Search Assistance"), LOGIN_ASSISTANCE("Log In/Password Assistance"), ORDER_ISSUE("Order Issue"), PRODUCT_INQUIRY(
                "Product Inquiry"), RETURN_VENDOR_ERR("Return : Vendor Error"), RETURN_CUST_ERR(
                "Return: Customer Error"), RETURN_DAMAGED("Return: Damaged"), RETURN_DEFECTIVE("Return: Defective"), RETURN_QUALITY_ISSUES(
                "Return: Quality Issues"), RETURNING_CALL("Returning a call"), SPECIAL_ORDER("Special Order"), TRACKING_BACKORDERED(
                "Tracking: backordered"), TRACKING_MISSING_ITEMS("Tracking: missing Items"), TRACKING_NOT_RECEIVED(
                "Tracking: ordered, but not received"), TRAINING("Training"), TRANSFER("Transfer"), DELIVERY_CONFIRMATION(
                "Delivery Confirmation"), SYSTEM_ISSUE("System Issue");

        private String label;

        private IssueCategory(String label) {
            this.label = label;
        }

        public String getLabel() {
            return this.label;
        }
    }

    public Date getScheduledFollowUp() {
        return scheduledFollowUp;
    }

    public void setScheduledFollowUp(Date scheduledFollowUp) {
        this.scheduledFollowUp = scheduledFollowUp;
    }

    public Date getExpectedResolution() {
        return expectedResolution;
    }

    public void setExpectedResolution(Date expectedResolution) {
        this.expectedResolution = expectedResolution;
    }

    public String getIssueDescription() {
        return issueDescription;
    }

    public void setIssueDescription(String issueDescription) {
        this.issueDescription = issueDescription;
    }

    public IssueCategory getIssueCategory() {
        return issueCategory;
    }

    public void setIssueCategory(IssueCategory issueCategory) {
        this.issueCategory = issueCategory;
    }

    public Set<ContactLogComment> getComments() {
        return comments;
    }

    public void setComments(Set<ContactLogComment> comments) {
        this.comments = comments;
    }

    public SystemUser getReporter() {
        return reporter;
    }

    public void setReporter(SystemUser reporter) {
        this.reporter = reporter;
    }

    public static class IssueLogComparator implements Comparator<IssueLog> {

        @Override
        public int compare(IssueLog log0, IssueLog log1) {

            if (log0.getOpenedDate() != null && log1.getOpenedDate() != null) {
                return log0.getOpenedDate().compareTo(log1.getOpenedDate());
            }
            return (new GenericComparator()).compare(log0, log1);
        }
    }
}
