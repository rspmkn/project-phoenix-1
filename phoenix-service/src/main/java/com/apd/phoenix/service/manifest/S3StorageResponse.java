package com.apd.phoenix.service.manifest;

import java.io.Serializable;

public class S3StorageResponse implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 975963555677586756L;
    private int numberOfFiles;

    public int getNumberOfFiles() {
        return numberOfFiles;
    }

    public void setNumberOfFiles(int numberOfFiles) {
        this.numberOfFiles = numberOfFiles;
    }

}
