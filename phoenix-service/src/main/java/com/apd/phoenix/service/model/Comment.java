/*
 * 
 */
package com.apd.phoenix.service.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;
import javax.xml.bind.annotation.XmlRootElement;
import org.hibernate.annotations.Check;

/**
 * The Class Comment.
 */
@Entity(name = "com.apd.phoenix.service.model.Comment")
@Table(name = "SystemComment")
@XmlRootElement
@Check(constraints = "(MADEBYSYSTEM = 1) OR (USER_ID IS NOT NULL)")
public class Comment implements Serializable, com.apd.phoenix.service.model.Entity {

    private static final long serialVersionUID = -8898614583519924838L;

    /** The id. */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id = null;

    /** The version. */
    @Version
    @Column(name = "version", nullable = false)
    private int version = 0;

    /** The user. */
    @ManyToOne(fetch = FetchType.LAZY)
    private SystemUser user;

    /** The comment date. */
    @Temporal(TemporalType.TIMESTAMP)
    private Date commentDate;

    @Column(length = 4000)
    private String content;

    @Column
    private String fileUrl;

    @ManyToOne
    private Shipment shipment;

    @Column
    private Boolean madeBySystem = false;

    /**
     * Gets the id.
     *
     * @return the id
     */
    public Long getId() {
        return this.id;
    }

    /**
     * Sets the id.
     *
     * @param id the new id
     */
    public void setId(final Long id) {
        this.id = id;
    }

    /**
     * Gets the version.
     *
     * @return the version
     */
    public int getVersion() {
        return this.version;
    }

    /**
     * Sets the version.
     *
     * @param version the new version
     */
    public void setVersion(final int version) {
        this.version = version;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        if (id != null) {
            return id.equals(((Comment) that).id);
        }
        return super.equals(that);
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        if (id != null) {
            return id.hashCode();
        }
        return super.hashCode();
    }

    /**
     * Gets the user.
     *
     * @return the user
     */
    public SystemUser getUser() {
        return this.user;
    }

    /**
     * Sets the user.
     *
     * @param user the new user
     */
    public void setUser(final SystemUser user) {
        this.user = user;
    }

    /**
     * Gets the comment date.
     *
     * @return the comment date
     */
    public Date getCommentDate() {
        return this.commentDate;
    }

    /**
     * Sets the comment date.
     *
     * @param commentDate the new comment date
     */
    public void setCommentDate(final Date commentDate) {
        this.commentDate = commentDate;
    }

    public String getContent() {
        return this.content;
    }

    public void setContent(final String content) {
        this.content = content;
    }

    public String getFileUrl() {
        return this.fileUrl;
    }

    public void setFileUrl(final String fileUrl) {
        this.fileUrl = fileUrl;
    }

    @Override
    public String toString() {
        String result = getClass().getSimpleName() + " ";
        if (content != null && !content.trim().isEmpty())
            result += "content: " + content;
        if (fileUrl != null && !fileUrl.trim().isEmpty())
            result += ", fileUrl: " + fileUrl;
        return result;
    }

    public Shipment getShipment() {
        return this.shipment;
    }

    public void setShipment(final Shipment shipment) {
        this.shipment = shipment;
    }

    public Boolean getMadeBySystem() {
        return madeBySystem;
    }

    public void setMadeBySystem(Boolean madeBySystem) {
        this.madeBySystem = madeBySystem;
    }
}