package com.apd.phoenix.service.integration.multitenancy;

import java.util.Map;
import org.apache.camel.Exchange;
import org.apache.camel.Properties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.core.utility.TenantConfigRepository;

/**
 * This class is used to resolve tenant-specific FTP destinations. It does this by generating endpoints that can be 
 * read from intercepted messages to pull the correct property from tenant-specific configurations.
 * 
 * @author RHC
 *
 */
public class TenantFtpEndpointResolver {

    private static final Logger LOG = LoggerFactory.getLogger(TenantFtpEndpointResolver.class);

    //prefix for generated endpoints, which are not valid FTP but can be used to specify properties
    public static final String FTP_PREFIX = "ftp://tenantAwareFtpDestination-";
    private static final String FTP_ENDPOINT_DELIMETER = "-tenantAwareFtpDelimeter-";
    private static final String TENANT_ID_FIELD = "tenantId";

    /**
     * Takes a message, and returns the correct endpoint. It does this by checking the original,
     * invalid endpoint, and pulls the correct endpoint from tenant-specific properties.
     * 
     * @param body
     * @param properties
     * @return
     */
    public String getEndpoint(Object body, @Properties Map<String, Object> properties) {
        String endpoint = properties.get(Exchange.TO_ENDPOINT).toString();
        if (LOG.isDebugEnabled()) {
            LOG.debug("Processing message with endpoint " + endpoint);
        }
        if (endpoint == null || !endpoint.startsWith(FTP_PREFIX)) {
            //occurs if the endpoint does not start with the FTP prefix, in which case it's dropped
            return null;
        }
        try {
            Object tenantObject = properties.get(TENANT_ID_FIELD);
            String tenantName = TenantConfigRepository.getInstance().getTenantNameById(
                    new Long(tenantObject.toString()));
            String[] endpointStringArray = endpoint.toString().split(FTP_PREFIX)[1].split(FTP_ENDPOINT_DELIMETER);
            String partnerId = endpointStringArray[0];
            String propertyValue = endpointStringArray[1];
            String toReturn = TenantConfigRepository.getInstance().getProperty(tenantName, partnerId + ".integration",
                    propertyValue);
            if (!toReturn.startsWith(FTP_PREFIX)) {
                return toReturn;
            }
        }
        catch (Exception e) {
            LOG.error("Error generating tenant-specific FTP endpoint, turn on debug for stack trace");
            if (LOG.isDebugEnabled()) {
                LOG.debug("Error generating correct endpoint from object " + endpoint, e);
            }
        }
        return null;
    }

    /**
     * Takes a partner ID and the name of a property, and returns an FTP endpoint. Messages sent
     * to that endpoint will look up the tenant-specific parter property specified, so that tenant-
     * aware FTP routing works.
     * 
     * Note that this should NOT be used on inbound FTP processing, but adding this to outbound FTP
     * processing will not cause problems, even if that outbound processing is tenant-agnostic (such
     * as for APD-only integration).
     * 
     * @param partnerId
     * @param propertyValue
     * @return
     */
    public static String getEndpointName(String partnerId, String propertyValue) {
        return FTP_PREFIX + partnerId + FTP_ENDPOINT_DELIMETER + propertyValue;
    }

}
