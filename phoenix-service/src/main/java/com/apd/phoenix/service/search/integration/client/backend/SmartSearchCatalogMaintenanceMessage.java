package com.apd.phoenix.service.search.integration.client.backend;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Map;
import com.apd.phoenix.service.model.CatalogXItem;
import com.apd.phoenix.service.model.Item;
import com.apd.phoenix.service.model.SkuType;
import com.apd.phoenix.service.search.integration.client.SmartSearchConstants;
import com.apd.phoenix.service.utility.SmartSearchUtils;

public class SmartSearchCatalogMaintenanceMessage implements Serializable {

    private static final long serialVersionUID = 5622164793754229011L;

    private SmartSearchMaintenanceAction action;
    private Map<String, Map<Long, SmartSearchCatalogMaintenanceMessageContent>> message;

    public SmartSearchCatalogMaintenanceMessage(SmartSearchMaintenanceAction action,
            Map<String, Map<Long, SmartSearchCatalogMaintenanceMessageContent>> message) {
        this.action = action;
        this.message = message;
    }

    public SmartSearchMaintenanceAction getAction() {
        return action;
    }

    public void setAction(SmartSearchMaintenanceAction action) {
        this.action = action;
    }

    public Map<String, Map<Long, SmartSearchCatalogMaintenanceMessageContent>> getMessage() {
        return message;
    }

    public void setMessage(Map<String, Map<Long, SmartSearchCatalogMaintenanceMessageContent>> message) {
        this.message = message;
    }

    public enum SmartSearchMaintenanceAction {
        UPDATE_ITEM, REMOVE_LIST, INSERT_LIST;
    }

    public static class SmartSearchCatalogMaintenanceMessageContent implements Serializable {

        private static final long serialVersionUID = 2622164793754229011L;

        private Timestamp coreItemStartDate;
        private Timestamp coreItemExpirationDate;
        private Boolean usscoCatalog = false;
        private String apdSku;
        private String vendorName;
        private String vendorSku;
        private String smartSearchKeywords;
        private String name;
        private String uomName;
        private String searchTerms;
        private String itemStatus;
        private String hierarchyDescription;
        private String imageUrl;
        private Long coreListName;
        private String dealerDescription;

        public SmartSearchCatalogMaintenanceMessageContent(Map<String, Object> itemProperties) {
            if (itemProperties.get(SmartSearchConstants.IS_USSCO_CATALOG) != null) {
                usscoCatalog = (Boolean) itemProperties.get(SmartSearchConstants.IS_USSCO_CATALOG);
            }
            coreItemStartDate = (Timestamp) itemProperties.get("CORESTARTDATE");
            coreItemExpirationDate = (Timestamp) itemProperties.get("COREEXPIREDATE");
            apdSku = (String) itemProperties.get(SkuType.ValidSkuType.DEALER.getName());
            vendorName = (String) itemProperties.get("VENDORNAME");
            vendorSku = (String) itemProperties.get(SkuType.ValidSkuType.VENDOR.getName());
            smartSearchKeywords = (String) itemProperties.get("SMART_SEARCH_KEYWORDS");
            name = (String) itemProperties.get("NAME");
            uomName = (String) itemProperties.get("UOM_NAME");
            if (!usscoCatalog) {
                searchTerms = (String) itemProperties.get("PROTECTEDSEARCHTERMS");
            }
            itemStatus = (String) itemProperties.get("ITEM_STATUS");
            hierarchyDescription = (String) itemProperties.get("HIERARCHY_DESCRIPTION");
            imageUrl = (String) itemProperties.get("IMAGEURL");
            dealerDescription = (String) itemProperties.get("DEALER_DESCRIPTION");
        }

        public SmartSearchCatalogMaintenanceMessageContent(CatalogXItem item, Long coreListName) {
            this.parseItem(item.getItem());
            if (item.getCoreItemStartDate() != null) {
                coreItemStartDate = new Timestamp(item.getCoreItemStartDate().getTime());
            }
            if (item.getCoreItemExpirationDate() != null) {
                coreItemExpirationDate = new Timestamp(item.getCoreItemExpirationDate().getTime());
            }
            this.coreListName = coreListName;
        }

        public SmartSearchCatalogMaintenanceMessageContent(Item item) {
            this.parseItem(item);
        }

        private void parseItem(Item item) {
            usscoCatalog = SmartSearchUtils.isUsscoItem(item);
            apdSku = item.getSku(SkuType.ValidSkuType.DEALER.getName());
            vendorName = item.getVendorCatalog().getVendor().getName();
            vendorSku = item.getSku(SkuType.ValidSkuType.VENDOR.getName());
        }

        public Timestamp getCoreItemStartDate() {
            return coreItemStartDate;
        }

        public void setCoreItemStartDate(Timestamp coreItemStartDate) {
            this.coreItemStartDate = coreItemStartDate;
        }

        public Timestamp getCoreItemExpirationDate() {
            return coreItemExpirationDate;
        }

        public void setCoreItemExpirationDate(Timestamp coreItemExpirationDate) {
            this.coreItemExpirationDate = coreItemExpirationDate;
        }

        public Boolean getUsscoCatalog() {
            return usscoCatalog;
        }

        public void setUsscoCatalog(Boolean usscoCatalog) {
            this.usscoCatalog = usscoCatalog;
        }

        public String getApdSku() {
            return apdSku;
        }

        public void setApdSku(String apdSku) {
            this.apdSku = apdSku;
        }

        public String getVendorName() {
            return vendorName;
        }

        public void setVendorName(String vendorName) {
            this.vendorName = vendorName;
        }

        public String getVendorSku() {
            return vendorSku;
        }

        public void setVendorSku(String vendorSku) {
            this.vendorSku = vendorSku;
        }

        public String getSmartSearchKeywords() {
            return smartSearchKeywords;
        }

        public void setSmartSearchKeywords(String smartSearchKeywords) {
            this.smartSearchKeywords = smartSearchKeywords;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getUomName() {
            return uomName;
        }

        public void setUomName(String uomName) {
            this.uomName = uomName;
        }

        public String getSearchTerms() {
            return searchTerms;
        }

        public void setSearchTerms(String searchTerms) {
            this.searchTerms = searchTerms;
        }

        public String getItemStatus() {
            return itemStatus;
        }

        public void setItemStatus(String itemStatus) {
            this.itemStatus = itemStatus;
        }

        public String getHierarchyDescription() {
            return hierarchyDescription;
        }

        public void setHierarchyDescription(String hierarchyDescription) {
            this.hierarchyDescription = hierarchyDescription;
        }

        public String getImageUrl() {
            return imageUrl;
        }

        public void setImageUrl(String imageUrl) {
            this.imageUrl = imageUrl;
        }

        public Long getCoreListName() {
            return coreListName;
        }

        public void setCoreListName(Long coreListName) {
            this.coreListName = coreListName;
        }

        public String getDealerDescription() {
            return dealerDescription;
        }

        public void setDealerDescription(String dealerDescription) {
            this.dealerDescription = dealerDescription;
        }

    }
}
