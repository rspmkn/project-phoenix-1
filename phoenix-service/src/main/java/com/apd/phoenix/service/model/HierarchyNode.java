package com.apd.phoenix.service.model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Version;

@Entity
public class HierarchyNode implements Serializable, com.apd.phoenix.service.model.Entity {

    //This is the delimeter separating the different paths in the field
    //NOTE: If this changes, change it in hierarchynode_update_path.sql
    private static final String DELIMETER = "   ";

    //This is the delimeter separating different nodes in a single path
    //NOTE: If this changes, change it in hierarchynode_update_path.sql
    private static final String PATH_DELIMETER = "!";

    private static final long serialVersionUID = -2622201312405236709L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id = null;
    @Version
    @Column(name = "version", nullable = false)
    private Integer version;

    @OneToMany(mappedBy = "parent", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
    private Set<HierarchyNode> children = new HashSet<HierarchyNode>();
    @ManyToOne
    private HierarchyNode parent;

    @ManyToMany(mappedBy = "hierarchyNodes", fetch = FetchType.LAZY)
    private Set<SkuType> skuTypes = new HashSet<SkuType>();

    @Column
    private String description;

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "publicid", updatable = false, nullable = false)
    private Long publicId = null;

    @Column(length = 4000)
    private String indexedPaths;

    @Column
    private String type;

    public Long getId() {
        return this.id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public Long getPublicId() {
        return this.publicId;
    }

    public void setPublicId(final Long publicId) {
        this.publicId = publicId;
    }

    public Integer getVersion() {
        return this.version;
    }

    public void setVersion(final Integer version) {
        this.version = version;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        if (id != null) {
            return id.equals(((HierarchyNode) that).id);
        }
        return super.equals(that);
    }

    @Override
    public int hashCode() {
        if (id != null) {
            return id.hashCode();
        }
        return super.hashCode();
    }

    public Set<HierarchyNode> getChildren() {
        return this.children;
    }

    public void setChildren(final Set<HierarchyNode> children) {
        this.children = children;
    }

    public HierarchyNode getParent() {
        return this.parent;
    }

    public void setParent(final HierarchyNode parent) {
        this.parent = parent;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    public Set<SkuType> getSkuTypes() {
        return skuTypes;
    }

    public void setSkuTypes(Set<SkuType> skuTypes) {
        this.skuTypes = skuTypes;
    }

    public String getIndexedPaths() {
        return this.indexedPaths;
    }

    public void setIndexedPaths(final String paths) {
        this.indexedPaths = paths;
    }

    @Override
    public String toString() {
        String result = getClass().getSimpleName() + " ";
        result += "serialVersionUID: " + serialVersionUID;
        if (description != null && !description.trim().isEmpty())
            result += ", description: " + description;
        if (publicId != null)
            result += ", publicId: " + publicId;
        if (indexedPaths != null && !indexedPaths.trim().isEmpty())
            result += ", indexedPaths: " + indexedPaths;
        return result;
    }

    /**
     * This method updates the indexedPaths of the entire tree that a HierarchyNode is associated with.
     * <br /><br />
     * For more details, see: http://wiki.apache.org/solr/HierarchicalFaceting
     */
    public void updateTree() {
        HierarchyNode currentNode = this;
        while (currentNode.getParent() != null) {
            currentNode = currentNode.getParent();
        }
        currentNode.updateChildrenPaths();
    }

    /**
     * This method recursively updates the descendants of a node in the tree, based on depth-first search.
     */
    private void updateChildrenPaths() {
        this.setIndexedPaths(this.generatePath());
        for (HierarchyNode child : this.getChildren()) {
            child.updateChildrenPaths();
        }
    }

    private String generatePath() {
        return PATH_DELIMETER + this.getDescription();
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}