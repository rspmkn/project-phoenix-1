/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.service.business;

import java.util.Date;
import java.util.List;
import javax.ejb.Asynchronous;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import com.apd.phoenix.core.EncryptionUtils;
import com.apd.phoenix.service.model.AccountXCredentialXUser;
import com.apd.phoenix.service.model.CustomerOrder;
import com.apd.phoenix.service.model.PunchoutSession;
import com.apd.phoenix.service.model.CustomerOrder.PunchoutOrderOperation;
import com.apd.phoenix.service.persistence.jpa.PunchoutSessionDao;
import com.apd.phoenix.service.persistence.multitenancy.impl.CurrentTenantIdentifierResolverImpl;

/**
 * @author RH
 */
@Stateless
@LocalBean
public class PunchoutSessionBp extends AbstractBp<PunchoutSession> {

    @Inject
    CustomerOrderBp customerOrderBp;

    @Inject
    public void initDao(PunchoutSessionDao dao) {
        this.dao = dao;
    }

    public PunchoutSession createPunchoutSession(PunchoutSession punchoutSession) {
        return createPunchoutSession(punchoutSession, punchoutSession.getCustomerOrder());
    }

    //tenant ID has to be set manually on asynchronous EJB methods, since
    //the tenant ID is stored in a static threadlocal variable, and 
    //@Asynchronous calls occur in a different thread.
    @Asynchronous
    public void createPunchoutSession(String buyerCookie, PunchoutOrderOperation operation,
            AccountXCredentialXUser axcxu, Long tenantId) {
        CurrentTenantIdentifierResolverImpl.setCurrentTenant(tenantId);
        PunchoutSession punchoutSession = new PunchoutSession();

        punchoutSession.setBuyerCookie(buyerCookie);
        punchoutSession.setSystemUserLoginName(axcxu.getUser().getLogin());

        punchoutSession.setOperation(operation);

        punchoutSession.setAccntCredUser(axcxu);
        try {
            punchoutSession = this.createPunchoutSession(punchoutSession);
        }
        catch (Exception ex) {
            String errorMessage = "Error persisting PunchoutSession entity";
            LOG.error(errorMessage, ex);
        }
    }

    public PunchoutSession createPunchoutSession(PunchoutSession punchoutSession, CustomerOrder customerOrder) {

        String punchoutSessionToken = EncryptionUtils.randomAlphanumeric();
        punchoutSession.setSessionToken(punchoutSessionToken);

        if (customerOrder == null) {
            customerOrder = customerOrderBp.createPunchoutOrder(punchoutSession);
            punchoutSession.setCustomerOrder(customerOrder);
        }

        return dao.update(punchoutSession);
    }

    public PunchoutSession endPunchoutSession(PunchoutSession punchoutSession) {
        punchoutSession.setEndTime(new Date());
        return dao.update(punchoutSession);
    }

    public void endBuyerCookieSessions(String buyerCookie) {
        ((PunchoutSessionDao) dao).endBuyerCookieSessions(buyerCookie);
    }

    public Long getSearchResultCount(PunchoutSession punchoutSessionSearch) {
        return dao.resultQuantity(punchoutSessionSearch);
    }

    public List<PunchoutSession> getOpenPunchoutSessions(PunchoutSession punchoutSessionSearch) {
        return ((PunchoutSessionDao) dao).findOpenPunchoutSessionsByExample(punchoutSessionSearch);
    }
}
