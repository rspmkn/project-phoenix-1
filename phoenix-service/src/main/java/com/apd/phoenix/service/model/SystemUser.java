/*
 * 
 */
package com.apd.phoenix.service.model;

import java.io.Serializable;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import org.apache.commons.lang.StringUtils;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.Index;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import com.apd.phoenix.service.business.EntityComparator.GenericComparator;
import com.apd.phoenix.service.model.Catalog.SearchType;

/**
 * The Class SystemUser.
 */
@Entity
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@Audited
public class SystemUser implements Serializable, com.apd.phoenix.service.model.Entity {

    private static final long serialVersionUID = -8384115335625999929L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id = null;
    @Version
    @Column(name = "version", nullable = false)
    private int version = 0;

    /** The accounts. */
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "SystemUser_Account", joinColumns = { @JoinColumn(name = "users_id") }, inverseJoinColumns = { @JoinColumn(name = "accounts_id") })
    @XmlTransient
    private Set<Account> accounts = new HashSet<Account>();

    /** The change password. */
    @Column(nullable = false)
    private boolean changePassword = true;

    @Temporal(TemporalType.DATE)
    private Date passwordCreationDate = new Date();

    /** The concurrent access. */
    @Column(nullable = false)
    private boolean concurrentAccess;

    @Column(nullable = false)
    private boolean isActive = true;

    @Column
    private Integer loginAttempts;

    @Temporal(TemporalType.DATE)
    private Date lastLoginAttemptDate;

    @Column
    private Integer creditCardAttempts;

    @Temporal(TemporalType.TIMESTAMP)
    private Date lastCreditCardAttemptDate;

    @Temporal(TemporalType.TIMESTAMP)
    private Date previousCreditCardAttemptDate;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private PaymentInformation paymentInformation;

    public Long getId() {
        return this.id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public int getVersion() {
        return this.version;
    }

    public void setVersion(final int version) {
        this.version = version;
    }

    /** The login. */
    @Column(nullable = false, unique = true)
    @Index(name = "LOGIN_INDEX")
    private String login;

    /** The bulletin. */
    @ManyToMany(fetch = FetchType.LAZY, cascade = { CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH,
            CascadeType.DETACH })
    @XmlTransient
    private Set<Bulletin> bulletins = new HashSet<Bulletin>();

    /** The password. */
    @Column
    private String password;

    /** The password salt. */
    @Column
    private String salt;

    /** The permissions. */
    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "user_id")
    @XmlTransient
    @NotAudited
    private Set<PermissionXUser> permissionXUsers = new HashSet<PermissionXUser>();

    /** The Roles. */
    private @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "user_id")
    @XmlTransient
    Set<RoleXUser> roleXUsers = new HashSet<RoleXUser>();

    @Column
    private String status;

    @Temporal(TemporalType.DATE)
    private Date creationDate = new Date();

    @Temporal(TemporalType.DATE)
    private Date lastLoginDate;

    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL, optional = false)
    @Fetch(FetchMode.JOIN)
    private Person person = new Person();

    @Enumerated(EnumType.STRING)
    private CatalogSelectionType catalogSelectionType;

    /**
     * Gets the accounts.
     *
     * @return the accounts
     */
    public Set<Account> getAccounts() {
        return this.accounts;
    }

    /**
     * Gets the change password.
     *
     * @return the change password
     */
    public boolean getChangePassword() {
        return this.changePassword;
    }

    /**
     * Gets the concurrent access.
     *
     * @return the concurrent access
     */
    public boolean getConcurrentAccess() {
        return this.concurrentAccess;
    }

    /**
     * Gets the login.
     *
     * @return the login
     */
    public String getLogin() {
        return this.login;
    }

    /**
     * Get the bulletin
     * 
     * @return the bulletin
     */
    public Set<Bulletin> getBulletins() {
        return this.bulletins;
    }

    /**
     * Gets the password.
     *
     * @return the password
     */
    public String getPassword() {
        return this.password;
    }

    /**
     * Gets the permissions.
     *
     * @return the permissions
     */
    public Set<PermissionXUser> getPermissionXUsers() {
        return this.permissionXUsers;
    }

    /**
     * Gets the roles.
     *
     * @return the roles
     */
    public Set<RoleXUser> getRoleXUsers() {
        return this.roleXUsers;
    }

    /**
     * Sets the accounts.
     *
     * @param accounts the new accounts
     */
    public void setAccounts(final Set<Account> accounts) {
        this.accounts = accounts;
    }

    /**
     * Sets the change password.
     *
     * @param changePassword the new change password
     */
    public void setChangePassword(final boolean changePassword) {

        this.changePassword = changePassword;
    }

    /**
     * Sets the concurrent access.
     *
     * @param concurrentAccess the new concurrent access
     */
    public void setConcurrentAccess(final boolean concurrentAccess) {
        this.concurrentAccess = concurrentAccess;
    }

    /**
     * Sets the login.
     *
     * @param login the new login
     */
    public void setLogin(final String login) {
        this.login = login;
    }

    /**
     * Sets the bulletin
     * 
     * @param bulletin
     */
    public void setBulletins(final Set<Bulletin> bulletins) {
        this.bulletins = bulletins;
    }

    /**
     * Sets the password.
     *
     * @param password the new password
     */
    public void setPassword(final String password) {
        this.password = password;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    /**
     * Sets the permissions.
     *
     * @param permissions the new permissions
     */
    public void setPermissionXUsers(final Set<PermissionXUser> permissionXUsers) {
        this.permissionXUsers = permissionXUsers;
    }

    /**
     * Sets the roles.
     *
     * @param Roles the new roles
     */
    public void setRoleXUsers(final Set<RoleXUser> roleXUsers) {
        this.roleXUsers = roleXUsers;
    }

    public Date getPasswordCreationDate() {
        return passwordCreationDate;
    }

    public void setPasswordCreationDate(Date passwordCreationDate) {
        this.passwordCreationDate = passwordCreationDate;
    }

    public String getStatus() {
        return this.status;
    }

    public void setStatus(final String status) {
        this.status = status;
    }

    public Date getCreationDate() {
        return this.creationDate;
    }

    public void setCreationDate(final Date creationDate) {
        this.creationDate = creationDate;
    }

    public Date getLastLoginDate() {
        return this.lastLoginDate;
    }

    public void setLastLoginDate(final Date lastLoginDate) {
        this.lastLoginDate = lastLoginDate;
    }

    public Person getPerson() {
        return this.person;
    }

    public void setPerson(final Person person) {
        this.person = person;
    }

    public CatalogSelectionType getCatalogSelectionType() {
        return catalogSelectionType;
    }

    public void setCatalogSelectionType(CatalogSelectionType catalogSelectionType) {
        this.catalogSelectionType = catalogSelectionType;
    }

    public boolean getIsActive() {
        return this.isActive;
    }

    public void setIsActive(final boolean isActive) {
        this.isActive = isActive;
    }

    public PaymentInformation getPaymentInformation() {
        return this.paymentInformation;
    }

    public void setPaymentInformation(final PaymentInformation paymentInformation) {
        this.paymentInformation = paymentInformation;
    }

    public Integer getLoginAttempts() {
        return loginAttempts;
    }

    public void setLoginAttempts(Integer loginAttempts) {
        this.loginAttempts = loginAttempts;
    }

    public Date getLastLoginAttemptDate() {
        return lastLoginAttemptDate;
    }

    public void setLastLoginAttemptDate(Date lastLoginAttemptDate) {
        this.lastLoginAttemptDate = lastLoginAttemptDate;
    }

    public Date getPreviousCreditCardAttemptDate() {
        return previousCreditCardAttemptDate;
    }

    public void setPreviousCreditCardAttemptDate(Date previousCreditCardAttemptDate) {
        this.previousCreditCardAttemptDate = previousCreditCardAttemptDate;
    }

    public Integer getCreditCardAttempts() {
        return creditCardAttempts;
    }

    public void setCreditCardAttempts(Integer creditCardAttempts) {
        this.creditCardAttempts = creditCardAttempts;
    }

    public Date getLastCreditCardAttemptDate() {
        return lastCreditCardAttemptDate;
    }

    public void setLastCreditCardAttemptDate(Date lastCreditCardAttemptDate) {
        this.lastCreditCardAttemptDate = lastCreditCardAttemptDate;
    }

    @Override
    public String toString() {
        String result = "";
        if (login != null && !login.trim().isEmpty()) {
            result += login;
        }
        return result + " (" + this.person.toString() + ")";
    }

    public static class SystemUserComparator implements Comparator<SystemUser> {

        @Override
        public int compare(SystemUser arg0, SystemUser arg1) {
            if (!StringUtils.isBlank(arg0.getLogin()) && !StringUtils.isBlank(arg1.getLogin())) {
                return arg0.getLogin().compareTo(arg1.getLogin());
            }
            return (new GenericComparator()).compare(arg0, arg1);
        }
    }

    public static enum CatalogSelectionType {
        TENANT_DEFAULT("Use Default"), DROPDOWN("Dropdown with credential name"), TILES("Tiles with credential images");

        private final String label;

        private CatalogSelectionType(String label) {
            this.label = label;
        }

        public String getLabel() {
            return this.label;
        }
    }

}
