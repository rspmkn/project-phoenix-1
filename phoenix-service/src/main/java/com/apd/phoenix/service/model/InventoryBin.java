package com.apd.phoenix.service.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

/**
 * This entity contains the information relevant for an Inventory Bin details.
 * 
 * @author FWS-Muthu
 * 
 */
@Entity
public class InventoryBin implements Serializable, com.apd.phoenix.service.model.Entity {

    private static final long serialVersionUID = 7185662040258275545L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id = null;

    @Column
    private Long onHandQuantity;

    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    private InventoryLocation inventoryLocation;

    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    private InventoryItem inventoryitem;

    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    private InventoryBinType inventoryBinType;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getOnHandQuantity() {
        return onHandQuantity;
    }

    public void setOnHandQuantity(Long onHandQuantity) {
        this.onHandQuantity = onHandQuantity;
    }

    public InventoryLocation getInventoryLocation() {
        return inventoryLocation;
    }

    public void setInventoryLocation(InventoryLocation inventoryLocation) {
        this.inventoryLocation = inventoryLocation;
    }

    public InventoryItem getInventoryitem() {
        return inventoryitem;
    }

    public void setInventoryitem(InventoryItem inventoryitem) {
        this.inventoryitem = inventoryitem;
    }

    public InventoryBinType getInventoryBinType() {
        return inventoryBinType;
    }

    public void setInventoryBinType(InventoryBinType inventoryBinType) {
        this.inventoryBinType = inventoryBinType;
    }

}