package com.apd.phoenix.service.utility;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.lang.StringUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.persistence.multitenancy.impl.CurrentTenantIdentifierResolverImpl;

public class SendInternalHttpRequest {

    private static final Logger logger = LoggerFactory.getLogger(SendInternalHttpRequest.class);

    public static void sendRequest(String requestUrl) {
        sendRequest(requestUrl, new HashMap<String, String>());
    }

    public static void sendRequest(String requestUrl, Map<String, String> parameters) {
        sendRequest(requestUrl, parameters, false);
    }

    public static String sendRequest(String requestUrl, Map<String, String> parameters, boolean returnResponse) {
    	String toReturn = null;
		HttpClient httpClient = new DefaultHttpClient();
    	
		try {
			HttpPost httpPost = new HttpPost(requestUrl);
	    	//sets the parameters for the request
	    	if (parameters != null) {
		    	for (String key : parameters.keySet()) {
		    		if (StringUtils.isNotBlank(key) && StringUtils.isNotBlank(parameters.get(key))) {
		    			httpPost.setHeader(key, parameters.get(key));
		    		}
		    	}
	    	}

	    	httpPost.setHeader("tenantId", CurrentTenantIdentifierResolverImpl.getCurrentTenant().toString());
	    	
	    	try {
				HttpResponse response = httpClient.execute(httpPost);
				if (returnResponse) {
					try (BufferedReader streamReader = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), "UTF-8"));) {

						StringBuilder builder = new StringBuilder();
						String nextLine;
						while ((nextLine = streamReader.readLine()) != null) {
							builder.append(nextLine);
						}
						toReturn = builder.toString();
					}
				}
	            //gets the response InputStream
			    EntityUtils.consume(response.getEntity());
			} catch (Exception e) {
				logger.error("Error sending HTTP reqeust", e);
			}
		} catch (Exception e) {
			logger.error("Error generating HTTP reqeust", e);
		}
		
		return toReturn;
	}
}
