package com.apd.phoenix.service.business;

import java.io.Serializable;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.Set;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.core.utility.TenantConfigRepository;
import com.apd.phoenix.service.model.Account;
import com.apd.phoenix.service.model.AccountXAccountPropertyType;
import com.apd.phoenix.service.model.AccountXCredentialXUser;
import com.apd.phoenix.service.model.IpAddress;
import com.apd.phoenix.service.model.SystemUser;

/**
 * This class provides business process methods for Addresses.
 * 
 * @author RHC
 *
 */
@Stateless
@LocalBean
public class IpSubdomainRestriction implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 4929262047580383468L;
    private static final String QA_SUBDOMAIN_SUFFIX = "-qa";
    private static final String DEV_SUBDOMAIN_SUFFIX = "-dev";
    private static final Logger logger = LoggerFactory.getLogger(IpSubdomainRestriction.class);
    private static final String SUBDOMAIN_PROPERTY_TYPE = "Subdomain";
    public static final String GENERIC_SUBDOMAIN = "www";

    @Inject
    private SystemUserBp userBp;

    @Inject
    private AccountBp accountBp;

    /**
     * This method takes a managed Account, an HttpServletRequest, and a boolean flag, and returns a boolean 
     * indicating whether the account is accessible from the servlet request. The flag indicates whether the 
     * function should return true if no subdomain is specified on the account, when IP address validation 
     * has passed.
     * 
     * @param account
     * @param request
     * @param allowWhenNoSubdomain
     * @return
     */
    private boolean isAccountValid(Account account, HttpServletRequest request, boolean allowWhenNoSubdomain,
            boolean punchout) {
        String[] addressAndSubdomain = ipAndDomainInfo(request);
        String ipAddress = addressAndSubdomain[0];
        String subdomain = addressAndSubdomain[1];
        account.getIps().size();
        account.getProperties().size();
        boolean containsIp = false;
        for (IpAddress address : account.getIps()) {
            if (address.getValue().equals(ipAddress)) {
                containsIp = true;
                break;
            }
        }
        if (!containsIp && account.getIps().size() != 0) {
            return false;
        }
        for (AccountXAccountPropertyType property : account.getProperties()) {
            if (property.getType().getName().equals(SUBDOMAIN_PROPERTY_TYPE)) {
                if (property.getValue().equals(subdomain)) {
                    return true;
                }
                else if (!property.getValue().equals(GENERIC_SUBDOMAIN) && !punchout) {
                    if (logger.isDebugEnabled()) {
                        logger.debug("Login failed due to insufficient subdomain permissions.");
                    }
                    return false;
                }
            }
        }
        return allowWhenNoSubdomain;
    }

    public boolean isAccXCredXUserValid(AccountXCredentialXUser axcxu, HttpServletRequest request, boolean punchout) {
        String ip = ipAndDomainInfo(request)[0];
        boolean isAccountValid = this.isAccountValid(axcxu.getAccount(), request, true, punchout);
        boolean isCredentialValid = axcxu.getCredential().getIps().size() == 0;
        for (IpAddress testIp : axcxu.getCredential().getIps()) {
            if (testIp.getValue().equals(ip)) {
                isCredentialValid = true;
                break;
            }
        }
        if (logger.isDebugEnabled()) {
            if (!isAccountValid || !isCredentialValid) {
                logger.debug("Login failed, credential or account is not valid. : isAccountValid=" + isAccountValid
                        + " isCredentialValid= " + isCredentialValid);
            }
        }
        return isAccountValid && isCredentialValid;
    }

    /**
     * This method takes a user and a servlet request, and determines if that user should be able to log in.
     * 
     * This uses the same algorith as the PhoenixLoginModule.
     * 
     * @param user
     * @param request
     * @return
     */
    public boolean isUserValid(SystemUser user, HttpServletRequest request, boolean punchout) {
        //if the subdomain is the generic subdomain, returns true
        if (GENERIC_SUBDOMAIN.equals(ipAndDomainInfo(request)[1])) {
            return true;
        }
        user = this.userBp.findById(user.getId(), SystemUser.class);
        //otherwise, iterates through the user's accounts. If one of the accounts has that subdomain 
        //assigned, returns true.
        for (Account account : this.accountBp.getAccountsForLoginValidation(user)) {
            if (this.isAccountValid(account, request, false, punchout)) {
                return true;
            }
        }
        //otherwise, returns false
        return false;
    }

    /**
     * This static method returns the ip address and the "raw" subdomain, ie if the user goes to 
     * not-a-valid-domain.apd.com, returns "not-a-valid-domain". The benefit of this method is that it doesn't use any 
     * injection, so it can be called by anything (like the LoginModule).
     * 
     * @param request
     * @return
     */
    public static String[] ipAndDomainInfo(HttpServletRequest request) {
        String[] toReturn = new String[3];
        String ipAddress = "invalid_address";
        String subdomain = GENERIC_SUBDOMAIN;
        String hostName = "invalid";
        try {
            ipAddress = request.getRemoteAddr();
            URL url = new URL(request.getRequestURL().toString());
            hostName = url.getHost();

            Set<String> tenantNames = TenantConfigRepository.getInstance().getTenantNames();

            for (String tenant : tenantNames) {

                List<String> tenantDomains = TenantConfigRepository.getInstance().getTenantDomainsById(
                        TenantConfigRepository.getInstance().getTenantIdByName(tenant));
                boolean hostNameFound = false;

                for (String baseDomain : tenantDomains) {
                    int subdomainEndIndex = hostName.indexOf("." + baseDomain);
                    //walmart.apdmarketplace.com will return "walmart" as the subdomain
                    if (subdomainEndIndex != -1) {
                        subdomain = hostName.substring(0, subdomainEndIndex);
                        hostName = baseDomain;
                        hostNameFound = true;
                        break;
                    }
                }
                if (hostNameFound) {
                    break;
                }

            }

            if (subdomain.endsWith(QA_SUBDOMAIN_SUFFIX)) {
                subdomain = subdomain.substring(0, subdomain.length() - QA_SUBDOMAIN_SUFFIX.length());
            }

            if (subdomain.endsWith(DEV_SUBDOMAIN_SUFFIX)) {
                subdomain = subdomain.substring(0, subdomain.length() - DEV_SUBDOMAIN_SUFFIX.length());
            }

            //www.walmart.apdmarketplace.com will return "walmart" as the subdomain
            if (subdomain.startsWith(GENERIC_SUBDOMAIN) && subdomain.length() > GENERIC_SUBDOMAIN.length()) {
                subdomain = subdomain.substring(GENERIC_SUBDOMAIN.length() + 1, subdomain.length());
            }

            //dev.apdmarketplace.com or qa.apdmarketplace.com or apdmarketplace.com returns "www" as the subdomain
            if (StringUtils.isBlank(subdomain) || subdomain.equals("qa") || subdomain.equals("dev")) {
                //if the user is visiting the dev or qa pages, uses "www" as the subdomain
                subdomain = GENERIC_SUBDOMAIN;
            }
        }
        catch (MalformedURLException ex) {
            logger.error("Invalid URL detected!", ex);
        }

        toReturn[0] = ipAddress;
        toReturn[1] = subdomain;
        toReturn[2] = hostName;

        return toReturn;
    }
}
