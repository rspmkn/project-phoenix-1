package com.apd.phoenix.service.email.impl;

import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.*;
import java.sql.Date;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;
import com.apd.phoenix.service.model.dto.LineItemDto;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.core.StringEscape;
import com.apd.phoenix.core.utility.PropertiesLoader;
import com.apd.phoenix.service.business.AccountXCredentialXUserBp;
import com.apd.phoenix.service.business.CustomerOrderBp;
import com.apd.phoenix.service.email.api.Attachment;
import com.apd.phoenix.service.email.api.Attachment.MimeType;
import com.apd.phoenix.service.email.api.EmailTemplate;
import com.apd.phoenix.service.email.api.NoAttachmentContentException;
import com.apd.phoenix.service.email.impl.OrderCanceledEmailTemplate.ItemCancelled;
import com.apd.phoenix.service.model.CredentialPropertyType.CredentialPropertyTypeEnum;
import com.apd.phoenix.service.model.AccountXCredentialXUser;
import com.apd.phoenix.service.model.AssignedDepartment;
import com.apd.phoenix.service.model.Comment;
import com.apd.phoenix.service.model.CustomerOrder;
import com.apd.phoenix.service.model.LineItem;
import com.apd.phoenix.service.model.PhoneNumber;
import com.apd.phoenix.service.model.PhoneNumberType;
import com.apd.phoenix.service.model.dto.CashoutPageXFieldDto;
import com.apd.phoenix.service.model.dto.DtoUtils;
import com.apd.phoenix.service.model.dto.POAcknowledgementDto;
import com.apd.phoenix.service.model.dto.PurchaseOrderDto;
import com.apd.phoenix.service.model.factory.DtoFactory;
import com.apd.phoenix.service.model.factory.DtoFactory.ParsingException;
import freemarker.template.TemplateException;
import freemarker.template.TemplateException;

public class SalesQuoteEmailTemplate extends EmailTemplate<POAcknowledgementDto> {

    private static final Logger LOGGER = LoggerFactory.getLogger(OrderAcknowledgmentEmailTemplate.class);
    private static final String TEMPLATE = "sales.quote";
    private static final String MINIMUM_ORDER_SKU = "min_order_fee";

    @Inject
    private CustomerOrderBp customerOrderBp;

    @Inject
    AccountXCredentialXUserBp accountXCredentialXUserBp;

    @Override
	public String createBody(POAcknowledgementDto pOAcknowledgementDto) throws IOException,TemplateException {
    	
		Map<String, Object> params = new HashMap<>();
        String phoneNumber = null;
        List<ItemSubstitution> itemsList = new ArrayList<>();
        BigDecimal minimumOrderTotal = null;
        Set<Comment> comments = null;
        String commentString = "";
        PurchaseOrderDto purchaseOrderDto;
		String userName = "";
		String userEmail = "";
		
        CustomerOrder customerOrder = customerOrderBp.searchByApdPo(pOAcknowledgementDto.getApdPo());
        comments = customerOrder.getComments();

        
        for(Comment comment: comments){
        	commentString += comment.getContent() + "<br>";
        }
        
        try {
                purchaseOrderDto = DtoFactory.createPurchaseOrderDto(customerOrder);
        } catch (ParsingException e) {
                LOGGER.error("Could not create purchaseOrderDto from Customer order", e);
                return null;
        }
        
		if (customerOrder != null && customerOrder.getUser() != null) {
			  List<AccountXCredentialXUser> response = accountXCredentialXUserBp.getAllAXCXUByLogin(customerOrder.getUser().getLogin());
          if (response != null && response.size() > 0) {
        	  for (AccountXCredentialXUser cred : response) {
        		  for (AssignedDepartment dept : cred.getDepartments()) {
        			  userName = dept.getRecipientsNames();
        			  userEmail = dept.getRecipientsEmails();
        			  if(dept.getDepartment() != null && !StringUtils.isEmpty(dept.getDepartment().getName())) {
        				params.put("department", dept.getDepartment().getName());
        			  }
        		  }
        	  }
          }
          
          if(customerOrder.getAssignedCostCenter() != null && customerOrder.getAssignedCostCenter().getCostCenter() != null) {
          	params.put("costCenter", customerOrder.getAssignedCostCenter().getCostCenter().getName());
          }	
    		if(customerOrder.getAddress() != null && customerOrder.getAddress().getMiscShipTo() != null ) {
    			if(!StringUtils.isEmpty(customerOrder.getAddress().getMiscShipTo().getRequesterPhone())) {
    				params.put("phoneNumber",customerOrder.getAddress().getMiscShipTo().getRequesterPhone());
    			}
    			if(!StringUtils.isEmpty(customerOrder.getAddress().getMiscShipTo().getDepartment())) {
    				params.put("phoneNumber", customerOrder.getAddress().getMiscShipTo().getDepartment());
    			}
    		}
			if(StringUtils.isEmpty(userName) && StringUtils.isEmpty(userEmail) && customerOrder.getUser().getPerson() != null) {
				userName = customerOrder.getUser().getPerson().getFormalName();
				userEmail = customerOrder.getUser().getPerson().getEmail();
				if(params.get("phoneNumber") == null) {
					for (PhoneNumber phone : customerOrder.getUser().getPerson().getPhoneNumbers()) {
						if(PhoneNumberType.PhoneNumberTypeEnum.WORK.equals(phone.getType())) {
							params.put("phoneNumber", phone.getLineNumber());
						}
					}
				}				
			}
			params.put("contactPerson", userName);
			params.put("email", userEmail);		
			
		}

        /*Need to iterate through the list of items and see if we can find a minimum order fee and extract it out of the list.*/
        for(LineItem itemEntity: customerOrder.getItems())
        {
        	LineItemDto item = DtoFactory.createLineItemDto(itemEntity);
            if(MINIMUM_ORDER_SKU.equalsIgnoreCase(item.getApdSku()))
            {
                //Extract the total minimum order fee from the list
                minimumOrderTotal = item.getSubTotal();
            }
            else
            {
                //Add Item to the List
            	ItemSubstitution toAdd = new ItemSubstitution(); 
				toAdd.setItem(item);
				toAdd.setOriginallyAddedSku(itemEntity.getOriginallyAddedSku());
				toAdd.setOriginallyAddedName(itemEntity.getOriginallyAddedName());
                itemsList.add(toAdd);
            }
        }
               
        if(customerOrder.getAddress() != null && customerOrder.getAddress().getMiscShipTo() != null)
        {
        	if(StringUtils.isNotEmpty(customerOrder.getAddress().getMiscShipTo().getRequesterPhone()))
            {
                phoneNumber = customerOrder.getAddress().getMiscShipTo().getRequesterPhone();
            }
        }
        
        if(purchaseOrderDto.getOrderDate() != null){
        	params.put("date",  purchaseOrderDto.getOrderDate());
        }else{
        	SimpleDateFormat sdf = new SimpleDateFormat("Mmm d, yyyy HH:mm:ss");        	
        	Calendar calendar = Calendar.getInstance();
        	params.put("date", sdf.format(calendar.getTime()));
        }
        
		if (customerOrder != null && customerOrder.getUser() != null) {
			List<AccountXCredentialXUser> response = accountXCredentialXUserBp.getAllAXCXUByLogin(customerOrder.getUser().getLogin());
			  if (response != null && response.size() > 0) {
		      	  for (AccountXCredentialXUser cred : response) {
		      		  for (AssignedDepartment dept : cred.getDepartments()) {
		      			  userName = dept.getRecipientsNames();
		      			  userEmail = dept.getRecipientsEmails();
		      			  if(dept.getDepartment() != null && !StringUtils.isEmpty(dept.getDepartment().getName())) {
		      				params.put("department", dept.getDepartment().getName());
		      			  }
		      		  }
		      	  }
	      	  }
		}
        
        params.put("items", itemsList);
        params.put("skuOnEmailFlag", purchaseOrderDto.getCredential().getProperties().get(CredentialPropertyTypeEnum.SKU_ON_EMAIL.getValue()));
		params.put("quoteNumber", purchaseOrderDto.retrieveApdPoNumber().getValue());
		params.put("termsOfPayment", purchaseOrderDto.getCredential().getTerms());
		params.put("clientRef", purchaseOrderDto.getAccount().getSolomonCustomerID());
		params.put("client", purchaseOrderDto.getAccount().getName());
		params.put("contactPerson", userName);
		params.put("phoneNumber", this.getAddressFieldValue(phoneNumber));
		params.put("faxNumber",this.getAddressFieldValue(phoneNumber));
		params.put("email", userEmail);
		params.put("comments", commentString);
		
		return this.create(params);
	}

    @Override
    protected String getTemplate() {
        return TEMPLATE;
    }

    @Override
    public Attachment createAttachment(POAcknowledgementDto content) throws NoAttachmentContentException {
        // TODO Auto-generated method stub
        return null;
    }

    public class ItemSubstitution {

        private LineItemDto item;
        private String originallyAddedSku;
        private String originallyAddedName;

        public LineItemDto getItem() {
            return item;
        }

        public void setItem(LineItemDto item) {
            this.item = item;
        }

        public String getOriginallyAddedSku() {
            return originallyAddedSku;
        }

        public String getOriginallyAddedName() {
            return originallyAddedName;
        }

        public void setOriginallyAddedSku(String originallyAddedSku) {
            this.originallyAddedSku = originallyAddedSku;
        }

        public void setOriginallyAddedName(String originallyAddedName) {
            this.originallyAddedName = originallyAddedName;
        }

    }

}
