package com.apd.phoenix.service.executor.jms.impl;

import java.util.List;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import com.apd.phoenix.service.executor.api.ExecutorQueryService;
import com.apd.phoenix.service.executor.entities.ErrorInfo;
import com.apd.phoenix.service.executor.entities.RequestInfo;

@Stateless
@TransactionManagement(value = TransactionManagementType.CONTAINER)
@TransactionAttribute(value = TransactionAttributeType.REQUIRED)
public class ExecutorQueryServiceImpl implements ExecutorQueryService {

    @Override
    public List<RequestInfo> getQueuedRequests() {
        throw new UnsupportedOperationException("This features is not implemented");
    }

    @Override
    public List<RequestInfo> getExecutedRequests() {
        throw new UnsupportedOperationException("This features is not implemented");
    }

    @Override
    public List<RequestInfo> getInErrorRequests() {
        throw new UnsupportedOperationException("This features is not implemented");
    }

    @Override
    public List<RequestInfo> getCancelledRequests() {
        throw new UnsupportedOperationException("This features is not implemented");
    }

    @Override
    public List<ErrorInfo> getAllErrors() {
        throw new UnsupportedOperationException("This features is not implemented");
    }

    @Override
    public List<RequestInfo> getAllRequests() {
        throw new UnsupportedOperationException("This features is not implemented");
    }

}
