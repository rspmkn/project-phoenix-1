package com.apd.phoenix.service.product;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.apd.phoenix.core.StringEscape;
import com.apd.phoenix.service.model.CatalogXItem;
import com.apd.phoenix.service.model.CatalogXItemXItemPropertyType;
import com.apd.phoenix.service.model.Item;
import com.apd.phoenix.service.model.Item.ItemStatus;
import com.apd.phoenix.service.model.ItemImage;
import com.apd.phoenix.service.model.Matchbook;

/**
 * Presentation layer wrapper for storing product data pulled from solr results
 * or db directly
 *
 * @author dcnorris
 */
public class Product implements Serializable {

	public static final int MAXIMUM_NUMBER_OF_FOR_USE_WITH_TO_DISPLAY = 10;

	private static final long serialVersionUID = 1L;

	private Long id;
    private String name;
    private String description;
    private String customerSku;
    private String apdSku;
    private String manufacturerSku;
    private String vendorSku;
    private String vendorName;
    private Map<String, String> skuMap = new HashMap<>();
    private String manufacturerName;
    private String mainImageUrl;
    private List<String> imageUrls = new ArrayList<>();
    private Map<String, String> largerImagesMap = new HashMap<>();
    private Map<String, String> iconMap = new HashMap<>();
    private Map<String, String> properties = new HashMap<>();
    private BigDecimal price;
    private String units;
    private Date coreItemStartDate;
    private Date coreItemExpirationDate;
    public static final String DETAILS_PAGE_BASE_URL = "/shopping/ecommerce/product/productDetails.xhtml";
    private String status;
    private Integer multiple;
    private Integer minimum;
    private String customerReplacementSku;
    private String customerReplacementVendor;
    private String specialOrder;
    private String customOrder;
    private List<String> sellingPoints;
    private String brandName;
    private String brandImage;
    private String forModelNumbers;
    private String warranty;
    private Map<String, String> specifications;
    
    public Product(){}
     
    public Product(CatalogXItem cxItem){
        this.setId(cxItem.getId());
        Item item = cxItem.getItem();
        this.setName(item.getName());
        this.setDescription(item.getDescription());
        if (item.getBrand() != null) {
        	this.setBrandName(item.getBrand().getName());
        }
        if (cxItem.getCustomerSku() != null) {
        	this.setCustomerSku(cxItem.getCustomerSku().getValue());
        }
        this.setApdSku(item.getSku("dealer"));
        this.setManufacturerSku(item.getSku("manufacturer"));
        this.setVendorSku(item.getSku("vendor"));
        if (item.getVendorCatalog().getVendor() != null) {
        	this.setVendorName(item.getVendorCatalog().getVendor().getName());
        }
        if (item.getManufacturer() != null) {
        	this.setManufacturerName(item.getManufacturer().getName());
        }
        //If there are no images add a placeholder image
        this.setImageUrls(new ArrayList<String>());
        for (ItemImage i : item.getItemImages()) {
            this.getImageUrls().add(i.getImageUrl());
        }        
        for (String url : imageUrls) {
        	this.setMainImageUrl(url);
            break;
        }
        
        //note: icon map will not be set
        this.setProperties(new HashMap<String, String>());
        for (CatalogXItemXItemPropertyType ixipt : cxItem.getProperties()) {
            this.getProperties().put(ixipt.getType().getName(), ixipt.getValue());
        }
        this.setPrice(cxItem.getPrice());
        if(cxItem.getCustomerUnitOfmeasure() == null || StringUtils.isEmpty(cxItem.getCustomerUnitOfmeasure().getName())){
        	this.setUnits(item.getUnitOfMeasure().getName());
        } else {
        	this.setUnits(cxItem.getCustomerUnitOfmeasure().getName());
        }
        this.setCoreItemStartDate(cxItem.getCoreItemStartDate());
        this.setCoreItemExpirationDate(cxItem.getCoreItemExpirationDate());
        if (item.getStatus() == null) {
            this.setStatus(ItemStatus.AVAILABLE.name());
        }
        else {
            this.setStatus(item.getStatus().name());
        }
        this.setMinimum(item.getMinimum());
        this.setMultiple(item.getMultiple());
        this.setForModelNumbers(item.getForUseWith());
    }

	public void setForUseWithFromItem(Item item) {
		if(item == null){
			return;
		}
		if(StringUtils.isEmpty(item.getForUseWith())){
        	String forUseWith = "";
          	int count = 0; 
             if(item.getMatchbook() != null){       	
             	for(Matchbook matchbooks: item.getMatchbook()){
                 	if(count > MAXIMUM_NUMBER_OF_FOR_USE_WITH_TO_DISPLAY){
                 		break;
                 	}
             		if(!StringUtils.isEmpty(matchbooks.getModel())){
             			forUseWith = forUseWith + "," + matchbooks.getModel();
             			count++;
             		}
             	}
             }
             if(count > 0){
            	 forUseWith = forUseWith.substring(1);
             }
             this.setForModelNumbers(forUseWith);
        } else {
        	this.setForModelNumbers(item.getForUseWith());
        }
	}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Map<String, String> getSkuMap() {
        return skuMap;
    }

    public void setSkuMap(HashMap<String, String> skuMap) {
        this.skuMap = skuMap;
    }

    public String getManufacturerName() {
        return manufacturerName;
    }

    public void setManufacturerName(String manufacturerName) {
        this.manufacturerName = manufacturerName;
    }

    public String getMainImageUrl() {
        return mainImageUrl;
    }

    public void setMainImageUrl(String mainImageUrl) {
        this.mainImageUrl = mainImageUrl;
    }

    public List<String> getImageUrls() {
        return imageUrls;
    }

    public void setImageUrls(List<String> imageUrls) {
        this.imageUrls = imageUrls;
    }

    public Map<String, String> getLargerImagesMap() {
		return largerImagesMap;
	}

	public void setLargerImagesMap(Map<String, String> largerImagesMap) {
		this.largerImagesMap = largerImagesMap;
	}

	public String getCustomerSku() {
        return customerSku;
    }

    public void setCustomerSku(String customerSku) {
        this.customerSku = customerSku;
    }

    public String getUnits() {
        return units;
    }

    public void setUnits(String units) {
        this.units = units;
    }

    public String getApdSku() {
        return apdSku;
    }

    public void setApdSku(String apdSku) {
        this.apdSku = apdSku;
    }

    public String getManufacturerSku() {
        return manufacturerSku;
    }

    public void setManufacturerSku(String manufacturerSku) {
        this.manufacturerSku = manufacturerSku;
    }

    public String getVendorSku() {
        return vendorSku;
    }

    public void setVendorSku(String vendorSku) {
        this.vendorSku = vendorSku;
    }

    public String getVendorName() {
        return vendorName;
    }

    public void setVendorName(String vendorName) {
        this.vendorName = vendorName;
    }

    public Map<String, String> getIconMap() {
        return iconMap;
    }

    public void setIconMap(Map<String, String> iconMap) {
        this.iconMap = iconMap;
    }

    public Date getCoreItemStartDate() {
        return coreItemStartDate;
    }

    public void setCoreItemStartDate(Date coreItemStartDate) {
        this.coreItemStartDate = coreItemStartDate;
    }

    public Date getCoreItemExpirationDate() {
        return coreItemExpirationDate;
    }

    public void setCoreItemExpirationDate(Date coreItemExpirationDate) {
        this.coreItemExpirationDate = coreItemExpirationDate;
    }

    public Map<String, String> getProperties() {
        return properties;
    }

    public void setProperties(Map<String, String> properties) {
        this.properties = properties;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDetailsLink() {
        StringBuilder url = new StringBuilder(DETAILS_PAGE_BASE_URL);
        if (StringUtils.isBlank(apdSku) || StringUtils.isBlank(vendorName)) {
        	return url.toString();
        }
        url.append("?");
        url.append("apdSku=");
        url.append(StringEscape.escapeForUrl(apdSku));
        url.append("&");
        url.append("vendorName=");
        url.append(StringEscape.escapeForUrl(vendorName));
        return url.toString();
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (that instanceof Product) {
            that = (Product) that;
            if (this.getId() != null) {
                return this.getId().equals(((Product) that).getId());
            }
            return super.equals(that);
        }
        else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        if (id != null) {
            return id.hashCode();
        }
        return super.hashCode();
    }

    public boolean isCoreItem() {
        if (coreItemStartDate == null || coreItemExpirationDate == null) {
            return false;
        }
        Date date = new Date();
        if (coreItemStartDate.before(date) && coreItemExpirationDate.after(date)) {
            return true;
        }
        return false;
    }

    public static String getDETAILS_PAGE_BASE_URL() {
        return DETAILS_PAGE_BASE_URL;
    }

    public Integer getMultiple() {
        return multiple;
    }

    public void setMultiple(Integer multiple) {
        this.multiple = multiple;
    }

    public Integer getMinimum() {
        return minimum;
    }

    public void setMinimum(Integer minimum) {
        this.minimum = minimum;
    }

    public String getCustomerReplacementSku() {
        return customerReplacementSku;
    }

    public void setCustomerReplacementSku(String customerReplacementSku) {
        this.customerReplacementSku = customerReplacementSku;
    }

    public String getCustomerReplacementVendor() {
        return customerReplacementVendor;
    }

    public void setCustomerReplacementVendor(String customerReplacementVendor) {
        this.customerReplacementVendor = customerReplacementVendor;
    }

    public String getSpecialOrder() {
        return specialOrder;
    }

    public void setSpecialOrder(String specialOrder) {
        this.specialOrder = specialOrder;
    }

    public String getCustomOrder() {
        return customOrder;
    }

    public void setCustomOrder(String customOrder) {
        this.customOrder = customOrder;
    }

    /**
     * Returns the customerReplacementSku specified on the CatalogXItem if one exists. If not,
     * returns the APD sku of the item specified by item.replacement.
     * @return 
     */
    public String getReplacementSku() {
        String toReturn = null;
        if (properties != null) {
            toReturn = properties.get("customerReplacementSku");
        }
        if (StringUtils.isBlank(toReturn)) {
            toReturn = customerReplacementSku;
        }
        return toReturn;
    }

    /**
     * Returns the customerReplacementVendor specified on the CatalogXItem if one exists. If not,
     * returns the vendor of the item specified by item.replacement.
     * @return 
     */
    public String getReplacementVendor() {
        String toReturn = null;
        if (properties != null) {
            toReturn = properties.get("customerReplacementVendor");
        }
        if (StringUtils.isBlank(toReturn)) {
            toReturn = customerReplacementVendor;
        }
        return toReturn;
    }

	public List<String> getSellingPoints() {
		return sellingPoints;
	}

	public void setSellingPoints(List<String> sellingPoints) {
		this.sellingPoints = sellingPoints;
	}

	public String getBrandImage() {
		return brandImage;
	}

	public void setBrandImage(String brandImage) {
		this.brandImage = brandImage;
	}

	public String getForModelNumbers() {
		return forModelNumbers;
	}

	public void setForModelNumbers(String forModelNumbers) {
		this.forModelNumbers = forModelNumbers;
	}

	public String getWarranty() {
		return warranty;
	}

	public void setWarranty(String warranty) {
		this.warranty = warranty;
	}

	public Map<String, String> getSpecifications() {
		return specifications;
	}

	public void setSpecifications(Map<String, String> specifications) {
		this.specifications = specifications;
	}

	public String getBrandName() {
		return brandName;
	}

	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}

	
}
