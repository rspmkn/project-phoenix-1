package com.apd.phoenix.service.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class InventoryTransaction implements Serializable, com.apd.phoenix.service.model.Entity {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    /** The id. */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id = null;

    @Column
    String transactionType;

    @Temporal(TemporalType.DATE)
    private Date transactionDate;

    @Column(nullable = false)
    private BigDecimal transactionAmount = BigDecimal.ZERO;

    @Column
    private String user;

    @Column
    private Long shipmentNumber;

    @Column
    private String returnAuthorization;

    @Column
    private Long quantity;

    @Column
    private Long fromBinOnHandQty;

    @Column
    private String comments;

    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    private InventoryBinType fromBinType;

    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    private InventoryBinType toBinType;

    @ManyToOne(fetch = FetchType.EAGER)
    private InventoryItem inventoryItem;

    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    private InventoryTransactionTypes inventoryTransactionTypes;

    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    private InventoryReasons inventoryReasonTypes;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    public Date getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(Date transactionDate) {
        this.transactionDate = transactionDate;
    }

    public BigDecimal getTransactionAmount() {
        return transactionAmount;
    }

    public void setTransactionAmount(BigDecimal transactionAmount) {
        this.transactionAmount = transactionAmount;
    }

    public InventoryItem getInventoryItem() {
        return inventoryItem;
    }

    public void setInventoryItem(InventoryItem inventoryItem) {
        this.inventoryItem = inventoryItem;
    }

    public InventoryTransactionTypes getInventoryTransactionTypes() {
        return inventoryTransactionTypes;
    }

    public void setInventoryTransactionTypes(InventoryTransactionTypes inventoryTransactionTypes) {
        this.inventoryTransactionTypes = inventoryTransactionTypes;
    }

    public InventoryReasons getInventoryReasonTypes() {
        return inventoryReasonTypes;
    }

    public void setInventoryReasonTypes(InventoryReasons inventoryReasonTypes) {
        this.inventoryReasonTypes = inventoryReasonTypes;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        InventoryTransaction other = (InventoryTransaction) obj;
        if (id == null) {
            if (other.id != null)
                return false;
        }
        else if (!id.equals(other.id))
            return false;
        return true;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public Long getShipmentNumber() {
        return shipmentNumber;
    }

    public void setShipmentNumber(Long shipmentNumber) {
        this.shipmentNumber = shipmentNumber;
    }

    public String getReturnAuthorization() {
        return returnAuthorization;
    }

    public void setReturnAuthorization(String returnAuthorization) {
        this.returnAuthorization = returnAuthorization;
    }

    public Long getQuantity() {
        return quantity;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }

    public Long getFromBinOnHandQty() {
        return fromBinOnHandQty;
    }

    public void setFromBinOnHandQty(Long fromBinOnHandQty) {
        this.fromBinOnHandQty = fromBinOnHandQty;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public InventoryBinType getFromBinType() {
        return fromBinType;
    }

    public void setFromBinType(InventoryBinType fromBinType) {
        this.fromBinType = fromBinType;
    }

    public InventoryBinType getToBinType() {
        return toBinType;
    }

    public void setToBinType(InventoryBinType toBinType) {
        this.toBinType = toBinType;
    }

}
