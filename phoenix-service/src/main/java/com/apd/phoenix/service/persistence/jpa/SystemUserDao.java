package com.apd.phoenix.service.persistence.jpa;

import java.util.Date;
import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.Query;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.model.Account;
import com.apd.phoenix.service.model.Person;
import com.apd.phoenix.service.model.Role;
import com.apd.phoenix.service.model.SystemUser;

/**
 * SystemUser DAO stub
 * 
 * @author RHC
 * 
 */
@Stateless
@LocalBean
public class SystemUserDao extends AbstractDao<SystemUser> {

    private static final Logger LOGGER = LoggerFactory.getLogger(SystemUserDao.class);

    @SuppressWarnings("unchecked")
    public List<Role> getRoles(String user) {
        if (user == null) {
            return null;
        }

        String hql = "SELECT role FROM SystemUser as systemUser "
                + "JOIN systemUser.roleXUsers AS roleXUser "
                + "JOIN roleXUser.role AS role "
                + "WHERE LOWER(systemUser.login)=:user AND (roleXUser.startDate is null "
                + "OR roleXUser.startDate < :date) AND (roleXUser.expirationDate is null OR roleXUser.expirationDate > :date)";
        Query query = entityManager.createQuery(hql);
        query.setParameter("user", user.toLowerCase());
        query.setParameter("date", new Date());
        return query.getResultList();
    }

    @SuppressWarnings("unchecked")
    public List<Account> getAccounts(String user) {
        if (StringUtils.isBlank(user)) {
            return null;
        }

        String hql = "SELECT DISTINCT COALESCE(rootAccount, account) FROM SystemUser as systemUser "
                + "JOIN systemUser.accounts AS account " + "LEFT JOIN account.rootAccount AS rootAccount "
                + "WHERE LOWER(systemUser.login)=:user";
        Query query = entityManager.createQuery(hql);
        query.setParameter("user", user.toLowerCase());
        return query.getResultList();
    }

    @SuppressWarnings("unchecked")
    public List<Long> getAccountIds(String user) {
        if (StringUtils.isBlank(user)) {
            return null;
        }

        String hql = "SELECT DISTINCT COALESCE(rootAccount.id, account.id) FROM SystemUser as systemUser "
                + "LEFT OUTER JOIN systemUser.accounts AS account "
                + "LEFT OUTER JOIN account.rootAccount AS rootAccount " + "WHERE LOWER(systemUser.login)=:user";
        Query query = entityManager.createQuery(hql);
        query.setParameter("user", user.toLowerCase());
        return query.getResultList();
    }

    public List<SystemUser> getSystemUserByLogin(String login) {
        return getSystemUserByLogin(login, null);
    }

    /**
     * Finds the SystemUser based on login, case-insensitive
     * @param login 
     * @return 
     */
    @SuppressWarnings("unchecked")
    public List<SystemUser> getSystemUserByLogin(String login, List<Long> accountWhiteList) {
        String hql = "SELECT systemUser FROM SystemUser as systemUser LEFT JOIN systemUser.accounts AS account"
                + " LEFT JOIN account.rootAccount AS rootAccount WHERE LOWER(systemUser.login)=:login ";
        if (accountWhiteList != null) {
            hql = hql + "AND (account.id in (:accountWhiteList) or rootAccount.id in (:accountWhiteList))";
        }
        Query query = entityManager.createQuery(hql);
        query.setParameter("login", login == null ? "" : login.toLowerCase());
        if (accountWhiteList != null) {
            query.setParameter("accountWhiteList", accountWhiteList);
        }
        return query.getResultList();
    }

    @Override
    public SystemUser eagerLoad(SystemUser toLoad) {
        try {
            if (toLoad.getId() == null) {
                //transient entity passed in
                return toLoad;
            }
            StringBuffer queryString = new StringBuffer();
            queryString.append("SELECT S FROM SystemUser S ");

            queryString.append(joinQuery("S", SystemUser.class));

            queryString.append(joinQuery("S_person", Person.class));

            queryString.append("WHERE S.id = :userId");

            Query query = entityManager.createQuery(queryString.toString(), SystemUser.class);
            query.setParameter("userId", toLoad.getId());
            setItemsReturned(query, 0, 0);
            SystemUser loaded = (SystemUser) query.getResultList().get(0);

            //merges the passed in entity to the persistence context
            if (!entityManager.contains(loaded)) {
                loaded = entityManager.merge(loaded);
            }
            return loaded;
        }
        catch (Exception e) {
            LOGGER.error("An error occured:", e);
            //occurs when there's a problem calling the methods
            return toLoad;
        }
    }

    @SuppressWarnings("unchecked")
    public List<SystemUser> filteredUserList(String firstName, String lastName, String login, String email,
            List<Long> accountWhiteList) {
        String selectValue = "DISTINCT user";

        StringBuilder hql = new StringBuilder("SELECT " + selectValue + " FROM SystemUser AS user"
                + " LEFT OUTER JOIN user.accounts AS account" + " LEFT OUTER JOIN account.rootAccount AS rootAccount"
                + " LEFT OUTER JOIN FETCH user.person AS person");

        hql.append(" WHERE 1=1");
        if (accountWhiteList != null) {
            hql.append(" AND (account.id in (:accountWhiteList) or rootAccount.id in (:accountWhiteList))");
        }
        if (StringUtils.isNotBlank(firstName)) {
            hql.append(" AND upper(person.firstName) like :firstName");
        }
        if (StringUtils.isNotBlank(lastName)) {
            hql.append(" AND upper(person.lastName) like :lastName");
        }
        if (StringUtils.isNotBlank(login)) {
            hql.append(" AND upper(user.login) like :login");
        }
        if (StringUtils.isNotBlank(email)) {
            hql.append(" AND upper(person.email) like :email");
        }
        //hql.append(" ORDER BY person.lastName asc");

        Query query = entityManager.createQuery(hql.toString());
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Searching for systemuser using '{}'", hql.toString());
        }

        if (accountWhiteList != null) {
            query.setParameter("accountWhiteList", accountWhiteList);
        }

        if (StringUtils.isNotBlank(firstName)) {
            query.setParameter("firstName", "%" + firstName.toUpperCase() + "%");
        }
        if (StringUtils.isNotBlank(lastName)) {
            query.setParameter("lastName", "%" + lastName.toUpperCase() + "%");
        }
        if (StringUtils.isNotBlank(login)) {
            query.setParameter("login", "%" + login.toUpperCase() + "%");
        }
        if (StringUtils.isNotBlank(email)) {
            query.setParameter("email", "%" + email.toUpperCase() + "%");
        }

        final List<SystemUser> resultList = query.getResultList();
        return resultList;
    }

    @SuppressWarnings("unchecked")
    public List<SystemUser> usersWithNoPassword() {
        String hql = "FROM SystemUser as systemUser WHERE systemUser.password IS NULL";
        Query query = entityManager.createQuery(hql);
        return query.getResultList();
    }
}
