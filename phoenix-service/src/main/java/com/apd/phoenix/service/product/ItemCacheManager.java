package com.apd.phoenix.service.product;

import com.apd.phoenix.service.model.CatalogXItem;
import com.apd.phoenix.service.model.Item;

/**
 * Interface for Product cache management
 * 
 * @author RHC
 *
 */
public interface ItemCacheManager {

    /**
     * Takes the dealer SKU and vendor name of a vendor item, and returns that item, fetching
     * from the database if the item isn't in the cache.
     * 
     * @param dealerSku
     * @param vendorName
     * @return
     */
    public Item getVendorItem(String dealerSku, String vendorName);

    /**
     * Takes the vendor SKU and vendor name of a vendor item, and returns that item, fetching
     * from the database if the item isn't in the cache.
     * 
     * @param dealerSku
     * @param vendorName
     * @return
     */
    public Item getVendorItemByVendorSku(String vendorSku, String vendorName);

    /**
     * Stores a vendor item in the cache. 
     * 
     * @param item
     */
    public void putVendorItem(Long itemId);

    /**
     * Takes the dealer SKU, vendor name, cand catalog ID of a customer item, and returns that 
     * item, fetching from the database if the item isn't in the cache.
     * 
     * @param dealerSku
     * @param vendorName
     * @param catalogId
     * @return
     */
    public CatalogXItem getCustomerItem(String dealerSku, String vendorName, Long catalogId);

    /**
     * Stores a customer item in the cache. Could result in lazy init exception if the item is
     * unmanaged.
     * 
     * @param item
     */
    public void putCustomerItem(CatalogXItem item);

}
