package com.apd.phoenix.service.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * This entity contains the information relevant for an Inventory Reasons Type.
 * 
 * @author FWS-Muthu
 * 
 */

@Entity
public class InventoryReasons implements Serializable, com.apd.phoenix.service.model.Entity {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id = null;

    @Column(nullable = false, unique = true)
    private Long invReasonTypeId = null;

    @Column(nullable = false)
    private String invReasonType;

    @Column(nullable = false)
    private String description;

    public Long getId() {
        return this.id;
    }

    public Long getInvReasonTypeId() {
        return invReasonTypeId;
    }

    public void setInvReasonTypeId(Long invReasonTypeId) {
        this.invReasonTypeId = invReasonTypeId;
    }

    public String getInvReasonType() {
        return invReasonType;
    }

    public void setInvReasonType(String invReasonType) {
        this.invReasonType = invReasonType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setId(Long id) {
        this.id = id;
    }

}