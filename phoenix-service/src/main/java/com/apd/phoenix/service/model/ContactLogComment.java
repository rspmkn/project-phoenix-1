package com.apd.phoenix.service.model;

import java.io.Serializable;
import java.util.Comparator;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;
import javax.xml.bind.annotation.XmlRootElement;
import org.hibernate.envers.Audited;
import com.apd.phoenix.service.business.EntityComparator.GenericComparator;

/**
 * This entity is used to store comments on contact logs.
 * 
 * @author RHC
 * 
 */
@Entity
@XmlRootElement
public class ContactLogComment implements Serializable, com.apd.phoenix.service.model.Entity {

    private static final long serialVersionUID = -4689678957420545936L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id = null;
    @Version
    @Column(name = "version", nullable = false)
    private int version = 0;

    @Column(nullable = false, length = 2000)
    private String content;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private SystemUser apdCsrRep;

    /** The comment date. */
    @Temporal(TemporalType.TIMESTAMP)
    private Date commentTimestamp;

    public Long getId() {
        return this.id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public int getVersion() {
        return this.version;
    }

    public void setVersion(final int version) {
        this.version = version;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public SystemUser getApdCsrRep() {
        return apdCsrRep;
    }

    public void setApdCsrRep(SystemUser apdCsrRep) {
        this.apdCsrRep = apdCsrRep;
    }

    public Date getCommentTimestamp() {
        return commentTimestamp;
    }

    public void setCommentTimestamp(Date commentTimestamp) {
        this.commentTimestamp = commentTimestamp;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        if (id != null) {
            return id.equals(((ContactLogComment) that).id);
        }
        return super.equals(that);
    }

    @Override
    public int hashCode() {
        if (id != null) {
            return id.hashCode();
        }
        return super.hashCode();
    }

    @Override
    public String toString() {
        String result = "";
        if (content != null && !content.trim().isEmpty()) {
            result += content;
        }
        return result;
    }

    public static class ContactLogCommentComparator implements Comparator<ContactLogComment> {

        @Override
        public int compare(ContactLogComment log0, ContactLogComment log1) {

            if (log0.getCommentTimestamp() != null && log1.getCommentTimestamp() != null) {
                return log0.getCommentTimestamp().compareTo(log1.getCommentTimestamp());
            }
            return (new GenericComparator()).compare(log0, log1);
        }
    }
}
