package com.apd.phoenix.service.persistence.multitenancy.impl;

import javax.ejb.LocalBean;
import javax.ejb.Stateful;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.persistence.multitenancy.api.CurrentTenantDomain;

/**
 * A simple RequestScoped object used to hold the current detected tenant domain information.
 * @author Red Hat Consulting
 *
 */
@Stateful
@Named
@RequestScoped
@LocalBean
public class CurrentTenantDomainImpl implements CurrentTenantDomain {

    private static Logger LOG = LoggerFactory.getLogger(CurrentTenantDomainImpl.class);

    private String domain;

    public CurrentTenantDomainImpl() {
        super();
        LOG.debug("Create TenantDomainImpl");
    }

    public CurrentTenantDomainImpl(String domain) {
        super();
        this.domain = domain;
    }

    @Override
    public String getDomain() {
        return domain;
    }

    @Override
    public void setDomain(String domain) {
        LOG.debug("Setting tenant domain to: {}", domain);
        this.domain = domain;
    }

    @Override
    public String toString() {
        return "CurrentTenantDomainImpl [domain=" + domain + "]";
    }

}