/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.service.model.dto;

import java.io.Serializable;
import java.util.Set;

/**
 *
 * @author nreidelb
 */
public class CashoutPageDto implements Serializable {

    private static final long serialVersionUID = 80303830220705380L;

    private Set<CashoutPageXFieldDto> cashoutPageXFields;

    /**
     * @return the cashoutPageXFields
     */
    public Set<CashoutPageXFieldDto> getCashoutPageXFields() {
        return cashoutPageXFields;
    }

    /**
     * @param cashoutPageXFields the cashoutPageXFields to set
     */
    public void setCashoutPageXFields(Set<CashoutPageXFieldDto> cashoutPageXFields) {
        this.cashoutPageXFields = cashoutPageXFields;
    }
}
