package com.apd.phoenix.service.message.impl;

import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.jms.JMSException;
import javax.jms.ObjectMessage;
import javax.jms.Queue;
import javax.jms.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.business.CredentialBp;
import com.apd.phoenix.service.business.CustomerOrderBp;
import com.apd.phoenix.service.message.api.Message;
import com.apd.phoenix.service.message.api.MessageSender;
import com.apd.phoenix.service.model.Credential;
import com.apd.phoenix.service.model.CxmlConfiguration.DeploymentMode;
import com.apd.phoenix.service.model.OrderLog.EventType;
import com.apd.phoenix.service.model.dto.CustomerCreditInvoiceDto;
import com.apd.phoenix.service.model.dto.CustomerInvoiceDto;
import com.apd.phoenix.service.model.dto.POAcknowledgementDto;
import com.apd.phoenix.service.model.dto.PurchaseOrderDto;
import com.apd.phoenix.service.model.dto.ShipmentDto;
import com.apd.phoenix.service.model.factory.DtoFactory;
import com.apd.phoenix.service.persistence.multitenancy.impl.CurrentTenantIdentifierResolverImpl;
import com.apd.phoenix.service.utility.CxmlMessageUtils;

@Stateless
@LocalBean
public class CXMLMessageSender extends MessageSender {

    private static final Logger LOGGER = LoggerFactory.getLogger(CXMLMessageSender.class);

    @Inject
    CxmlMessageUtils transactionUtil;

    @Inject
    CredentialBp credentialBp;

    @Inject
    CustomerOrderBp customerOrderBp;

    @Resource(mappedName = "java:/activemq/cxml-outbound-order-request")
    private Queue outboundCXMLPurchaseOrderQueue;

    @Resource(mappedName = "java:/activemq/cxml-outbound-confirmation-request")
    private Queue outboundCXMLOrderAckQueue;

    @Resource(mappedName = "java:/activemq/cxml-outbound-ship-notice-request")
    private Queue outboundCXMLShipNoticeQueue;

    @Resource(mappedName = "java:/activemq/cxml-outbound-invoice-request")
    private Queue outboundCXMLInvoiceQueue;

    @PostConstruct
    public void setupQueues() {
        this.outboundOrderAckQueue = outboundCXMLOrderAckQueue;
        this.outboundPurchaseOrderQueue = outboundCXMLPurchaseOrderQueue;
        this.outboundShipNoticeQueue = outboundCXMLShipNoticeQueue;
        this.outboundInvoiceQueue = outboundCXMLInvoiceQueue;
    }

    @Override
    public boolean sendMessage(Message message, EventType evenType) {
        LOGGER.info("Sending CXML Message");
        logMessage(message, LOGGER);
        return true;
    }

    private ObjectMessage createCxmlJMSMessage(Session session, Serializable cxmlMessage, String partnerId,
            String partnerDestination, DeploymentMode credentialDeploymentMode) {
        ObjectMessage objectMessage = null;
        String credentialDeploymentModeString = credentialDeploymentMode != null ? credentialDeploymentMode.getLabel()
                : DeploymentMode.PRODUCTION.getLabel();
        try {
            objectMessage = session.createObjectMessage(cxmlMessage);
            objectMessage.setStringProperty("partnerId", partnerId);
            objectMessage.setStringProperty("destination", partnerDestination);
            objectMessage.setStringProperty("interchangeId", transactionUtil.generatePayloadId());
            objectMessage.setStringProperty("transactionId", transactionUtil.generationTransactionId());
            objectMessage.setStringProperty("credentialDeploymentMode", credentialDeploymentModeString);
            objectMessage.setLongProperty("tenantId", CurrentTenantIdentifierResolverImpl.getCurrentTenant());
        }
        catch (Exception e) {
            LOGGER.error(e.toString());
        }
        return objectMessage;
    }

    @Override
    public javax.jms.Message createPurchaseOrderMessage(Session session, PurchaseOrderDto purchaseOrderDto,
            Boolean resend) {
        Credential credential = credentialBp.retrieveCredential(purchaseOrderDto.getCredential());
        return createCxmlJMSMessage(session, purchaseOrderDto, purchaseOrderDto.getPartnerId(), purchaseOrderDto
                .getPartnerDestination(), credential.getDeploymentMode());
    }

    @Override
    public javax.jms.Message createInvoiceMessage(Session session, CustomerInvoiceDto invoiceDto, Boolean resend) {
        Credential credential = credentialBp.retrieveCredential(invoiceDto.getShipment().getCustomerOrderDto()
                .getCredential());
        return createCxmlJMSMessage(session, invoiceDto, invoiceDto.getPartnerId(), credential.getInvoiceDestination(),
                credential.getDeploymentMode());
    }

    @Override
    public Message sendCreditInvoice(CustomerCreditInvoiceDto customerCreditInvoiceDto) {
        LOGGER.info("Sending Credit Invoice via cXML");
        CustomerInvoiceDto invoiceDto = DtoFactory.createCustomerInvoiceDto(customerCreditInvoiceDto);
        return sendInvoice(invoiceDto);
    }

    @Override
    public javax.jms.Message createPOAcknowledgementMessage(Session session, POAcknowledgementDto poAcknowledgementDto,
            Boolean resend) {
        Credential credential = credentialBp.retrieveCredential(poAcknowledgementDto.getPurchaseOrderDto()
                .getCredential());
        return createCxmlJMSMessage(session, poAcknowledgementDto, poAcknowledgementDto.getPartnerId(), credential
                .getPoAcknowledgementDestination(), credential.getDeploymentMode());
    }

    @Override
    public javax.jms.Message createShipmentNoticeMessage(Session session, ShipmentDto poDto, Boolean resend) {
        Credential credential = credentialBp.retrieveCredential(poDto.getCustomerOrderDto().getCredential());
        ObjectMessage message = createCxmlJMSMessage(session, poDto, poDto.getCustomerOrderDto().getPartnerId(),
                credential.getShipmentNotificationDestination(), credential.getDeploymentMode());

        try {
            message.setStringProperty("originalInterchangeId", poDto.getCustomerOrderDto().getOrderPayloadId());
        }
        catch (JMSException e) {
            LOGGER.error(e.getMessage());
        }
        return message;
    }

}
