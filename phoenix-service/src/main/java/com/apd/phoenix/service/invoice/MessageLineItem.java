package com.apd.phoenix.service.invoice;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * A line item generated from a EDI or cXML message.
 * @author RHC
 *
 */
public class MessageLineItem implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -2102398039209283085L;
    private int quantity;
    private String vendorSku;
    private String apdSku;
    private BigDecimal price;
    private String status;
    private boolean invalid;
    private List<String> errorMessages;

    public MessageLineItem() {
        errorMessages = new ArrayList<>();
        this.invalid = false;
    }

    public MessageLineItem(int quantity, String vendorSku, String apdSku, BigDecimal price, String status) {
        super();
        this.quantity = quantity;
        this.vendorSku = vendorSku;
        this.apdSku = apdSku;
        this.price = price;
        this.status = status;
        this.invalid = false;
        this.errorMessages = new ArrayList<>();
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getVendorSku() {
        return vendorSku;
    }

    public void setVendorSku(String vendorSku) {
        this.vendorSku = vendorSku;
    }

    public String getApdSku() {
        return apdSku;
    }

    public void setApdSku(String apdSku) {
        this.apdSku = apdSku;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public boolean isInvalid() {
        return invalid;
    }

    public void setInvalid(boolean invalid) {
        this.invalid = invalid;
    }

    public List<String> getErrorMessages() {
        return errorMessages;
    }

    public void setErrorMessages(List<String> errorMessages) {
        this.errorMessages = errorMessages;
    }
}
