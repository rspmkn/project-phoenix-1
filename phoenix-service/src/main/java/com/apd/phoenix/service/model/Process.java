package com.apd.phoenix.service.model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Version;

@NamedQueries( {
        @NamedQuery(name = "findProcessByAccountByOrder", query = "SELECT p FROM Process p JOIN p.processConfigurations pc JOIN pc.account acct WHERE pc.account IN "
                + "(SELECT o.account FROM CustomerOrder o WHERE o.id = :id)"),
        @NamedQuery(name = "findProcessByCredentialByOrder", query = "SELECT p FROM Process p JOIN p.processConfigurations pc JOIN pc.credential cred WHERE pc.credential IN "
                + "(SELECT o.credential FROM CustomerOrder o WHERE o.id = :id)")

//order.getcredential.getprocess.getpc.getpcv where order approval

//find the account and the credential, look if the credenital has a pc, if it does, loop through them
//and their associated pcv to determine which one is for order approval, if credential doesn't have one, then look 
//at account, if account doesn't have one, then nothing, else, start process with name and order

//take the order

})
@Entity
public class Process implements Serializable, com.apd.phoenix.service.model.Entity {

    private static final long serialVersionUID = -507139813085046649L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id = null;
    @Version
    @Column(name = "version", nullable = false)
    private int version = 0;

    @OneToMany(mappedBy = "process", cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.LAZY)
    private Set<ProcessConfiguration> processConfigurations = new HashSet<ProcessConfiguration>();

    @Column
    private String name;

    public Long getId() {
        return this.id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public int getVersion() {
        return this.version;
    }

    public void setVersion(final int version) {
        this.version = version;
    }

    public Set<ProcessConfiguration> getProcessConfigurations() {
        return this.processConfigurations;
    }

    public void setProcessConfiguration(final Set<ProcessConfiguration> processConfigurations) {
        this.processConfigurations = processConfigurations;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        if (id != null) {
            return id.equals(((Process) that).id);
        }
        return super.equals(that);
    }

    @Override
    public int hashCode() {
        if (id != null) {
            return id.hashCode();
        }
        return super.hashCode();
    }

    public String getName() {
        return this.name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        String result = getClass().getSimpleName() + " ";
        if (name != null && !name.trim().isEmpty())
            result += "name: " + name;
        return result;
    }
}