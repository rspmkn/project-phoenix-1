/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.service.model;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;

/**
 *
 * @author nreidelb
 */
@javax.persistence.Entity
@DiscriminatorValue(value = "CHANGE_ORDER_LOG")
public class ChangeOrderLog extends OrderLog {

    //Null indicates a system initiated change
    @Column
    private String changingUser;
    @Column(length = 3000)
    private String description;

    public ChangeOrderLog() {
    }

    /**
     * @return the changingUser
     */
    public String getChangingUser() {
        return changingUser;
    }

    /**
     * @param changingUser the changingUser to set
     */
    public void setChangingUser(String changingUser) {
        this.changingUser = changingUser;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

}
