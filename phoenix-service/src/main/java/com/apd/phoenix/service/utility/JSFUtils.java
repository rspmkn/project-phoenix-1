package com.apd.phoenix.service.utility;

import java.io.IOException;
import java.util.Iterator;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.security.jacc.PolicyContext;
import javax.security.jacc.PolicyContextException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class JSFUtils {

    public static void clearForm(String componentId) {
        FacesContext context = FacesContext.getCurrentInstance();
        UIComponent component = context.getViewRoot().findComponent(componentId);
        if (component != null) {
            clearInputChildren(component);
        }
    }

    private static void clearInputChildren(UIComponent component) {

        // If the component is a UIInput instance unset any existing or newly submitted value
        if (UIInput.class.isAssignableFrom(component.getClass())) {
            ((UIInput) component).resetValue();
        }
        else {
            // Clear values set for UIInput components contained by this component, if any
            Iterator<UIComponent> kids = component.getFacetsAndChildren();
            while (kids.hasNext()) {
                clearInputChildren(kids.next());
            }
        }
    }

    public static void redirect(String view) throws IOException, PolicyContextException {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        if (facesContext != null) {
            ExternalContext context = facesContext.getExternalContext();
            context.redirect(context.getRequestContextPath() + view);
        }
        else {
            HttpServletRequest httpRequest = (HttpServletRequest) PolicyContext
                    .getContext("javax.servlet.http.HttpServletRequest");
            HttpServletResponse httpResponse = (HttpServletResponse) PolicyContext
                    .getContext("javax.servlet.http.HttpServletResponse");
            httpResponse.sendRedirect(httpRequest.getContextPath() + view);
        }
    }

}
