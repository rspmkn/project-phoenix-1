/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.service.business;

import java.util.Date;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import org.apache.commons.lang.StringUtils;
import com.apd.phoenix.service.model.Account;
import com.apd.phoenix.service.model.Address;
import com.apd.phoenix.service.model.CardInformation;
import com.apd.phoenix.service.model.PaymentInformation;
import com.apd.phoenix.service.model.dto.PaymentInformationDto;
import com.apd.phoenix.service.persistence.jpa.PaymentInformationDao;

/**
 *
 * @author dnorris
 */
@Stateless
@LocalBean
public class PaymentInformationBp extends AbstractBp<PaymentInformation> {

    @Inject
    AddressBp addressBp;

    @Inject
    PaymentTypeBp paymentTypeBp;

    /**
     * This method is injected with the correct Dao for initialization
     * 
     * @param dao
     */
    @Inject
    public void initDao(PaymentInformationDao dao) {
        this.dao = dao;
    }

    public PaymentInformation createPaymentInformation(PaymentInformationDto paymentInformationDto, Account account) {
        //look for billing address on account by GlnID if relevant
        Address billAddress = null;
        if (paymentInformationDto.getBillToAddress().getMiscShipToDto() != null
                && !StringUtils.isEmpty(paymentInformationDto.getBillToAddress().getMiscShipToDto().getGlnID())) {
            final Address retrievedAddress = addressBp.retrieve(paymentInformationDto.getBillToAddress(), account);
            if (retrievedAddress != null) {
                billAddress = addressBp.cloneEntity(retrievedAddress);
            }
        }
        if (billAddress == null) {
            billAddress = addressBp.create(paymentInformationDto.getBillToAddress());
        }
        PaymentInformation paymentInformation = new PaymentInformation();
        paymentInformation.setBillingAddress(billAddress);
        String cardNumber = paymentInformationDto.getCardNumber();
        Date expirationDate = paymentInformationDto.getExpiration();
        String identity = paymentInformationDto.getIdentity();
        String name = paymentInformationDto.getNameOnCard();
        if (StringUtils.isNotBlank(cardNumber) && expirationDate != null && StringUtils.isNotBlank(identity)
                && StringUtils.isNotBlank(name)) {
            CardInformation card = new CardInformation();
            card.setNameOnCard(name);
            card.setExpiration(expirationDate);
            card.setIdentifier(identity);
            card.setNumber(cardNumber);
            paymentInformation.setCard(card);
        }
        paymentInformation.setPaymentType(paymentTypeBp.getTypeByValue(paymentInformationDto.getPaymentType()));

        return create(paymentInformation);
    }

}
