package com.apd.phoenix.service.model;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Version;
import org.hibernate.annotations.Index;

/**
 * This entity is used to store the cost for a specific customer for an item.
 * 
 * @author RHC
 *
 */
@Entity
public class CustomerCost implements Serializable, com.apd.phoenix.service.model.Entity {

    private static final long serialVersionUID = 770682068693037081L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id = null;
    @Version
    @Column(name = "version", nullable = false)
    private int version = 0;

    @ManyToOne(fetch = FetchType.LAZY)
    @Index(name = "CC_ITEM_IX")
    private Item item;

    @ManyToOne(fetch = FetchType.LAZY)
    private Account customer;

    @Column
    private BigDecimal value;

    public Long getId() {
        return this.id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public int getVersion() {
        return this.version;
    }

    public void setVersion(final int version) {
        this.version = version;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        if (id != null) {
            return id.equals(((CustomerCost) that).id);
        }
        return super.equals(that);
    }

    @Override
    public int hashCode() {
        if (id != null) {
            return id.hashCode();
        }
        return super.hashCode();
    }

    public Item getItem() {
        return this.item;
    }

    public void setItem(final Item item) {
        this.item = item;
    }

    public Account getCustomer() {
        return this.customer;
    }

    public void setCustomer(final Account customer) {
        this.customer = customer;
    }

    public BigDecimal getValue() {
        return this.value;
    }

    public void setValue(final BigDecimal value) {
        this.value = value;
    }

    @Override
    public String toString() {
        String result = getClass().getSimpleName() + " ";
        if (value != null)
            result += "value: " + value.toPlainString();
        return result;
    }
}