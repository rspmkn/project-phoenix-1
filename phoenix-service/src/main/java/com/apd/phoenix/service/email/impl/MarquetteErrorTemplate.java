package com.apd.phoenix.service.email.impl;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.email.api.Attachment;
import com.apd.phoenix.service.email.api.EmailTemplate;
import com.apd.phoenix.service.email.api.NoAttachmentContentException;
import com.apd.phoenix.service.model.dto.InvoiceDto;
import freemarker.template.TemplateException;

public class MarquetteErrorTemplate extends EmailTemplate<InvoiceDto> {

    private static final Logger LOGGER = LoggerFactory.getLogger(MarquetteErrorTemplate.class);
    private static final String TEMPLATE = "marquette.error.email";

    @Override
	public String createBody(InvoiceDto content) throws IOException,
			TemplateException {
		Map<String, Object> params = new HashMap<>();
		if(content != null){
			params.put("exception", content.getException());
	        params.put("invoiceNumber", content.getInvoiceNumber());
		}
        return this.create(params);
	}

    @Override
    public Attachment createAttachment(InvoiceDto content) throws NoAttachmentContentException {
        return null;
    }

    @Override
    protected String getTemplate() {
        return TEMPLATE;
    }

}
