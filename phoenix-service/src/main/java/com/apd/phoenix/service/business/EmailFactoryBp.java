package com.apd.phoenix.service.business;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.logging.Level;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.mail.Message.RecipientType;
import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.core.utility.PropertiesLoader;
import com.apd.phoenix.core.utility.TenantConfigRepository;
import com.apd.phoenix.service.email.api.Attachment;
import com.apd.phoenix.service.email.api.EmailService;
import com.apd.phoenix.service.email.api.EmailTemplate;
import com.apd.phoenix.service.email.api.NoAttachmentContentException;
import com.apd.phoenix.service.email.impl.AddLocationRequestEmailTemplate;
import com.apd.phoenix.service.email.impl.ApCreditEmailTemplate;
import com.apd.phoenix.service.email.impl.ApproveOrderReminderEmailTemplate;
import com.apd.phoenix.service.email.impl.BackorderNoticeEmailTemplate;
import com.apd.phoenix.service.email.impl.CreateUserRequestEmailTemplate;
import com.apd.phoenix.service.email.impl.CreateUserRequestorEmailTemplate;
import com.apd.phoenix.service.email.impl.CreditCardCreditEmailTemplate;
import com.apd.phoenix.service.email.impl.CreditCardDeclinedEmailTemplate;
import com.apd.phoenix.service.email.impl.CreditCardReceiptEmailTemplate;
import com.apd.phoenix.service.email.impl.CustomerCreditInvoiceEmailTemplate;
import com.apd.phoenix.service.email.impl.CustomerInvoiceEmailTemplate;
import com.apd.phoenix.service.email.impl.Edi864ErrorEmailTemplate;
import com.apd.phoenix.service.email.impl.EdiErrorEmailTemplate;
import com.apd.phoenix.service.email.impl.EdiOrderNotFoundEmailTemplate;
import com.apd.phoenix.service.email.impl.InvoiceErrorsEmailTemplate;
import com.apd.phoenix.service.email.impl.LateShipmentNoticeEmailTemplate;
import com.apd.phoenix.service.email.impl.MarfieldUserSetupEmailTemplate;
import com.apd.phoenix.service.email.impl.MarquetteErrorTemplate;
import com.apd.phoenix.service.email.impl.ModifyLocationRequestEmailTemplate;
import com.apd.phoenix.service.email.impl.ModifyUserRequestEmailTemplate;
import com.apd.phoenix.service.email.impl.NewUserSetupEmailTemplate;
import com.apd.phoenix.service.email.impl.OrderAcknowledgmentEmailTemplate;
import com.apd.phoenix.service.email.impl.OrderAcknowledgmentErrorsEmailTemplate;
import com.apd.phoenix.service.email.impl.OrderApprovalNeededEmailTemplate;
import com.apd.phoenix.service.email.impl.OrderCanceledEmailTemplate;
import com.apd.phoenix.service.email.impl.OrderDeniedEmailTemplate;
import com.apd.phoenix.service.email.impl.OrderFailedEmailTemplate;
import com.apd.phoenix.service.email.impl.ProFormaInvoiceEmailTemplate;
import com.apd.phoenix.service.email.impl.PurchaseOrderTemplate;
import com.apd.phoenix.service.email.impl.SalesQuoteEmailTemplate;
import com.apd.phoenix.service.email.impl.ShipmentNoticeEmailTemplate;
import com.apd.phoenix.service.email.impl.ShipmentNotificationErrorsEmailTemplate;
import com.apd.phoenix.service.email.impl.UserRequestCsrEmailTemplate;
import com.apd.phoenix.service.email.impl.DuplicateCustomerPoErrorEmailTemplate;
import com.apd.phoenix.service.email.impl.WorkflowResubmittedErrorEmailTemplate;
import com.apd.phoenix.service.message.api.Message;
import com.apd.phoenix.service.message.api.MessageUtils;
import com.apd.phoenix.service.model.Credential;
import com.apd.phoenix.service.model.CustomerOrder;
import com.apd.phoenix.service.model.MessageMetadata;
import com.apd.phoenix.service.model.SystemUser;
import com.apd.phoenix.service.model.MessageMetadata.MessageType;
import com.apd.phoenix.service.model.MessageObjectsWrapper;
import com.apd.phoenix.service.model.NotificationProperties;
import com.apd.phoenix.service.model.OrderLog.EventType;
import com.apd.phoenix.service.model.PoNumber;
import com.apd.phoenix.service.model.dto.EdiDocumentDto;
import com.apd.phoenix.service.model.dto.InvoiceDto;
import com.apd.phoenix.service.model.dto.MessageDto;
import com.apd.phoenix.service.model.dto.MessageMetadataDto.MessageObjectsWrapperDto;
import com.apd.phoenix.service.model.dto.PurchaseOrderDto;
import com.apd.phoenix.service.model.dto.UserModificationRequestDto;
import com.apd.phoenix.service.model.dto.error.EdiDocumentErrorsDto;
import com.apd.phoenix.service.model.factory.DtoFactory;
import com.apd.phoenix.service.model.factory.DtoFactory.ParsingException;
import com.apd.phoenix.service.persistence.multitenancy.impl.CurrentTenantIdentifierResolverImpl;
import freemarker.template.TemplateException;

@Stateless
@LocalBean
public class EmailFactoryBp {

    private static final String MARQUETTE_ERROR_RECIPIENT = "marquetteErrorRecipient";
    private static final Logger LOGGER = LoggerFactory.getLogger(EmailFactoryBp.class);
    private static final String EMAIL_PROPERTIES_SUBJECT = "subject";
    private static final String SUBJECT_LINE_CUST_PO = "Cust PO#";
    private static final String SUBJECT_LINE_APD_PO = " Order #";
    private static final String EMAIL_DELIMITER_REGEX = "[,; ]";
    private static final String USER_MODIFICATION_BCC_CONFIRMATION = "user.modification.bcc.confirmation";

    @Inject
    EmailService emailService;

    @Inject
    MessageUtils messageUtils;

    @Inject
    CustomerOrderBp customerOrderBp;

    @Inject
    private OrderLogBp orderLogBp;

    @Inject
    OrderAcknowledgmentEmailTemplate orderAcknowledgmentEmailTemplate;

    @Inject
    OrderDeniedEmailTemplate orderDeniedEmailTemplate;

    @Inject
    OrderApprovalNeededEmailTemplate orderApprovalNeededEmailTemplate;

    @Inject
    CustomerInvoiceEmailTemplate customerInvoiceEmailTemplate;

    @Inject
    CustomerCreditInvoiceEmailTemplate customerCreditInvoiceEmailTemplate;

    @Inject
    CreditCardReceiptEmailTemplate creditCardReceiptEmailTemplate;

    @Inject
    CreditCardCreditEmailTemplate creditCardCreditEmailTemplate;

    @Inject
    InvoiceErrorsEmailTemplate invoiceErrorsEmailTemplate;

    @Inject
    OrderAcknowledgmentErrorsEmailTemplate orderAcknowledgmentErrorsEmailTemplate;

    @Inject
    ShipmentNotificationErrorsEmailTemplate shipmentNotificationErrorsEmailTemplate;

    @Inject
    CreditCardDeclinedEmailTemplate creditCardDeclinedEmailTemplate;

    @Inject
    OrderCanceledEmailTemplate orderCanceledEmailTemplate;

    @Inject
    BackorderNoticeEmailTemplate backorderNoticeEmailTemplate;

    @Inject
    ShipmentNoticeEmailTemplate shipmentNoticeEmailTemplate;

    @Inject
    EdiErrorEmailTemplate ediErrorEmailTemplate;

    @Inject
    Edi864ErrorEmailTemplate edi864ErrorEmailTemplate;

    @Inject
    DuplicateCustomerPoErrorEmailTemplate duplicateCustomerPoErrorEmailTemplate;

    @Inject
    WorkflowResubmittedErrorEmailTemplate workflowResubmittedErrorEmailTemplate;

    @Inject
    LateShipmentNoticeEmailTemplate lateShipmentNoticeEmailTemplate;

    @Inject
    OrderFailedEmailTemplate orderFailedEmailTemplate;

    @Inject
    EdiOrderNotFoundEmailTemplate ediOrderNotFoundEmailTemplate;

    @Inject
    PurchaseOrderTemplate purchaseOrderEmailTemplate;

    @Inject
    ApCreditEmailTemplate apCreditEmailTemplate;

    @Inject
    ApproveOrderReminderEmailTemplate approveOrderReminderTemplate;

    @Inject
    SalesQuoteEmailTemplate salesQuoteEmailTemplate;

    @Inject
    ProFormaInvoiceEmailTemplate proFormaInvoiceEmailTemplate;

    private Message createOrderNotificationEmail(CustomerOrder order, EventType type, Set<String> notificationTo,
            Set<String> notificationCC, Set<String> notificationBCC, Object content) {
        EmailTemplate template;
        switch (type) {
            case ORDER_ACKNOWLEDGEMENT:
                template = orderAcknowledgmentEmailTemplate;
                break;
            case ORDER_DENIED:
                template = orderDeniedEmailTemplate;
                break;
            case PURCHASE_ORDER_SENT:
                template = purchaseOrderEmailTemplate;
                break;
            case ORDER_APPROVAL_NEEDED:
                template = orderApprovalNeededEmailTemplate;
                break;
            case CUSTOMER_INVOICED:
            case CUSTOMER_INVOICED_RESEND:
                template = customerInvoiceEmailTemplate;
                break;
            case CUSTOMER_CREDIT_INVOICED:
            case CUSTOMER_CREDIT_INVOICED_RESEND:
                template = customerCreditInvoiceEmailTemplate;
                break;
            case CREDIT_CARD_RECEIPT:
                template = creditCardReceiptEmailTemplate;
                break;
            case CREDIT_CARD_CREDIT:
                template = creditCardCreditEmailTemplate;
                break;
            case VOUCHER_ERRORS:
                template = invoiceErrorsEmailTemplate;
                break;
            case INVOICE_ERRORS:
                template = invoiceErrorsEmailTemplate;
                break;
            case ORDER_ACKNOWLEDGEMENT_ERRORS:
                template = orderAcknowledgmentErrorsEmailTemplate;
                break;
            case SHIPMENT_NOTIFICATION_ERRORS:
                template = shipmentNotificationErrorsEmailTemplate;
                break;
            case ORDER_CANCELED:
                template = orderCanceledEmailTemplate;
                break;
            case CREDIT_CARD_DECLINED:
                template = creditCardDeclinedEmailTemplate;
                break;
            case BACKORDERED_ITEM_NOTICE:
                template = backorderNoticeEmailTemplate;
                break;
            case SHIPMENT_NOTICE:
                template = shipmentNoticeEmailTemplate;
                break;
            case LATE_SHIPMENT_NOTICE:
                template = lateShipmentNoticeEmailTemplate;
                break;
            case ORDER_FAILED:
                template = orderFailedEmailTemplate;
                break;
            case AP_CREDIT:
                template = apCreditEmailTemplate;
                break;
            case APPROVE_ORDER_REMINDER:
                template = approveOrderReminderTemplate;
                break;
            case RFQ_SUBMITTED:
                template = salesQuoteEmailTemplate;
                break;
            case PROFORMA_INVOICE:
                template = proFormaInvoiceEmailTemplate;
                break;
            default:
                LOGGER.error("Could not find email template for event type " + type.toString()
                        + ". No message will be sent.");
                return null;
        }

        PoNumber apdPo = order.getApdPo();
        PoNumber customerPo = order.getCustomerPo();

        String from = template.getFromAddress();
        StringBuilder subjectBuilder = new StringBuilder(template.getProperty(EMAIL_PROPERTIES_SUBJECT) + " - ");
        if (customerPo != null && customerPo.getValue() != null) {
            subjectBuilder.append(SUBJECT_LINE_CUST_PO).append(customerPo.getValue()).append(",");
        }

        subjectBuilder.append(" ").append(this.getTenantPropertyByName("code") + SUBJECT_LINE_APD_PO).append(
                apdPo.getValue());
        String subject = subjectBuilder.toString();
        List<Attachment> attachments = new ArrayList<Attachment>();

        try {
            Attachment report = template.createAttachment(content);
            if (report != null) {
                attachments.add(report);
            }
        }
        catch (NoAttachmentContentException e) {
            LOGGER.warn("No attachment retrieved for email");
        }

        try {
            String emailBody = template.createBody(content);
            Message toReturn = createEmailMessage(from, notificationBCC, notificationTo, notificationCC, subject,
                    emailBody, attachments, order, type);

            if (toReturn != null && toReturn.getMetadata() != null && content != null && type != null && order != null
                    && order.getId() != null) {
                MessageObjectsWrapperDto objectsWrapper = new MessageObjectsWrapperDto();
                objectsWrapper.setContent(content);
                objectsWrapper.setEventName(type.getLabel());
                objectsWrapper.setOrderId(order.getId());
                toReturn.getMetadata().setObjectsWrapper(new MessageObjectsWrapper());
                toReturn.getMetadata().getObjectsWrapper().setData(objectsWrapper);
            }

            return toReturn;
        }
        catch (Exception e) {
            LOGGER.error("An error occured:", e);
        }
        return null;
    }

    private Message createEmailMessage(String from, Set<String> notificationBCC, Set<String> notificationTo, Set<String> notificationCC,
            String subject, String emailBody, List<Attachment> attachments, CustomerOrder order, EventType type)
            throws IOException, MessagingException {

    	Set<String> bcc = this.parseRecipients(notificationBCC);
        Set<String> to = this.parseRecipients(notificationTo);
        Set<String> cc = this.parseRecipients(notificationCC);
        
        MimeMessage message = emailService.prepareRawMessage(from, null, bcc, to, cc,
                subject, emailBody, attachments);

        //TODO: Fix so it doesn't load all into memory
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        message.writeTo(bos);
        byte[] mimeMessage = bos.toByteArray();
        ByteArrayInputStream ips = new ByteArrayInputStream(mimeMessage);
        Message messageToSend = null;
        if (order != null) {
            messageToSend = messageUtils.createMessage(order, ips, mimeMessage.length,
                    MessageMetadata.MessageType.EMAIL, concat(new ArrayList<>(to)), concat(new ArrayList<>(cc)), concat(new ArrayList<>(bcc)));
        }
        else {
            messageToSend = messageUtils.createUserModificationMessage(ips, mimeMessage.length,
                    MessageMetadata.MessageType.EMAIL, concat(new ArrayList<>(to)), concat(new ArrayList<>(cc)), concat(new ArrayList<>(bcc)));
        }
        if(attachments != null){
	        for (Attachment attachment : attachments) {
	        	messageToSend.getMetadata().setAttachmentName(attachment.getFileName());
	            this.messageMetadataBp.update(messageToSend.getMetadata());
	        	break;
	        }
        }
        return messageToSend;
    }

    private String concat(List<String> strings) {
        StringBuilder sb = new StringBuilder();
        if (strings != null) {
            for (String string : strings) {
                if (StringUtils.isNotBlank(string)) {
                    if (sb.length() != 0) {
                        sb.append(",");
                    }
                    sb.append(string);
                }
            }
        }
        return sb.toString();
    }

    public List<Message> createOrderEmails(long orderId, Object content, EventType eventType) {
    	List<Message> toReturn = new ArrayList<>();
        CustomerOrder customerOrder = customerOrderBp.findById(orderId, CustomerOrder.class);
        if (content == null) {
            try {
                content = DtoFactory.createPurchaseOrderDto(customerOrder);
            }
            catch (ParsingException e) {
                LOGGER.error("An error occured:", e);
            }
        }

        Set<NotificationProperties> notificationProperties = customerOrderBp.getNotificationPropertiesForEvent(customerOrder, eventType);
        Set<String> notificationTo = new HashSet<>();
        Set<String> notificationCC = new HashSet<>();
        Set<String> notificationBCC = new HashSet<>();
        if (notificationProperties != null) {
        	for (NotificationProperties prop : notificationProperties) {
        		if (prop.getAddressing() != null && StringUtils.isNotBlank(prop.getRecipients())) {
        			List<String> recipients = Arrays.asList(prop.getRecipients().split(","));
        			switch (prop.getAddressing()) {
        			case TO: notificationTo.addAll(recipients);
        				break;
        			case CC: notificationCC.addAll(recipients);
        				break;
        			case BCC: notificationBCC.addAll(recipients);
        			}
        		}
        	}
        }
        
        addInternalEmails(notificationTo, eventType);
        
        addCredentialEmail(notificationTo, eventType, customerOrder.getCredential());
        

        
        if (eventType.equals(EventType.ORDER_APPROVAL_NEEDED) || eventType.equals(EventType.APPROVE_ORDER_REMINDER)) {
        	notificationTo.add(customerOrderBp.getApproverEmail(customerOrder));
        }
        
        if (notificationTo.isEmpty()) {
            Properties tenantProperties = TenantConfigRepository.getInstance().getPropertiesByTenantId(CurrentTenantIdentifierResolverImpl.getCurrentTenant(), "tenant");
            for (String address : tenantProperties.getProperty("internal.emails.ses.failure").split(",")) {
            	notificationTo.add(address);
            }
        }
        
        toReturn.add(createOrderNotificationEmail(customerOrder, eventType, notificationTo, notificationCC, notificationBCC, content));
        return toReturn;
    }

    @Inject
    private MessageMetadataBp messageMetadataBp;

    public Message createOrderEmailToResend(Message message, String resendRecipients, boolean includeOriginalCC,
            boolean includeOriginalBCC) {

        Message messageCopy = new Message();
        if (message != null && message.getMetadata() != null && message.getMetadata().getObjectsWrapper() != null) {
            MessageObjectsWrapperDto wrapper = messageMetadataBp.findById(message.getMetadata().getId(),
                    MessageMetadata.class).getObjectsWrapper().getData();
            EventType event = orderLogBp.getFromDto(wrapper.getEventName());
            if (event != null) {
                List<Message> newMessages = this.createOrderEmails(wrapper.getOrderId(), wrapper.getContent(), event);
                if (newMessages.size() == 1) {
                    message = messageUtils.retrieveMessage(newMessages.get(0).getMetadata());
                }
            }
        }
        try (InputStream messageContent = message.getContent()) {
            MessageMetadata messageMetadataCopy = messageMetadataBp.cloneEntity(message.getMetadata());
            MimeMessage mimeMessage = null;
            mimeMessage = new MimeMessage(null, messageContent);
            mimeMessage.setRecipients(RecipientType.TO, resendRecipients);

            InternetAddress[] nullInetAddressArray = null;
            if (!includeOriginalCC || StringUtils.isBlank(messageMetadataCopy.getCcRecipients())) {
                mimeMessage.setRecipients(RecipientType.CC, nullInetAddressArray);
                messageMetadataCopy.setCcRecipients(null);
            }
            else {
                mimeMessage.setRecipients(RecipientType.CC, messageMetadataCopy.getCcRecipients());
            }
            if (!includeOriginalBCC || StringUtils.isBlank(messageMetadataCopy.getBccRecipients())) {
                mimeMessage.setRecipients(RecipientType.BCC, nullInetAddressArray);
                messageMetadataCopy.setBccRecipients(null);
            }
            else {
                mimeMessage.setRecipients(RecipientType.BCC, messageMetadataCopy.getBccRecipients());
            }

            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            mimeMessage.writeTo(bos);
            byte[] mimeMessageBytes = bos.toByteArray();
            ByteArrayInputStream ips = new ByteArrayInputStream(mimeMessageBytes);
            messageCopy.setContent(ips);
            messageMetadataCopy.setDestination(resendRecipients);

            messageCopy.setMetadata(messageMetadataCopy);

        }
        catch (MessagingException e) {
            LOGGER.error(e.toString());
        }
        catch (IOException e) {
            LOGGER.error(e.toString());
        }

        return messageCopy;
    }

    private void addInternalEmails(Set<String> people, EventType type) {
        Properties internalEmails = TenantConfigRepository.getInstance().getPropertiesByTenantId(
                CurrentTenantIdentifierResolverImpl.getCurrentTenant(), "tenant");
        if (type.equals(EventType.ORDER_CANCELED)) {
            people.add(internalEmails.getProperty("internal.emails.order.canceled"));
        }
        else if (type.equals(EventType.VOUCHER_ERRORS)) {
            people.add(internalEmails.getProperty("internal.emails.invoice.errors"));
        }
        else if (type.equals(EventType.ORDER_ACKNOWLEDGEMENT_ERRORS)) {
            people.add(internalEmails.getProperty("internal.emails.order.acknowledgment.errors"));
        }
        else if (type.equals(EventType.SHIPMENT_NOTIFICATION_ERRORS)) {
            people.add(internalEmails.getProperty("internal.emails.shipment.notification.errors"));
        }
        else if (type.equals(EventType.ORDER_FAILED)) {
            people.add(internalEmails.getProperty("internal.emails.order.failed"));
        }
        else if (type.equals(EventType.AP_CREDIT)) {
            people.add(internalEmails.getProperty("internal.emails.ap.credit"));
        }
    }

    private void addCredentialEmail(Set<String> people, EventType type, Credential credential) {

        if (type.equals(EventType.ADVANCED_SHIPMENT_NOTICE)
                && credential.getShipmentNotificationMessageType().equals(MessageType.EMAIL)) {
            people.add(credential.getShipmentNotificationDestination());
        }
        else if (type.equals(EventType.CUSTOMER_INVOICED)
                && credential.getInvoiceMessageType().equals(MessageType.EMAIL)) {
            people.add(credential.getInvoiceDestination());
        }
        else if (type.equals(EventType.CUSTOMER_CREDIT_INVOICED)
                && credential.getCreditInvoiceMessageType().equals(MessageType.EMAIL)) {
            people.add(credential.getCreditInvoiceDestination());
        }
        else if (type.equals(EventType.ORDER_ACKNOWLEDGEMENT)
                && credential.getPoAcknowledgementMessageType().equals(MessageType.EMAIL)) {
            people.add(credential.getPoAcknowledgementDestination());
        }
    }

    public Message createEDIErrorEmail(EdiDocumentErrorsDto errorDto, CustomerOrder order) throws IOException, TemplateException{
        try {
            String emailBody = ediErrorEmailTemplate.createBody(errorDto);
            Properties emailsLoader = TenantConfigRepository.getInstance().getPropertiesByTenantId(CurrentTenantIdentifierResolverImpl.getCurrentTenant(), "tenant");
            return createEmailMessage(emailBody, order,ediErrorEmailTemplate, emailsLoader.getProperty("internal.emails.edi.error"));
        }
        catch (IOException | TemplateException e) {
            LOGGER.error(e.getMessage());
        } catch (MessagingException ex) {
            java.util.logging.Logger.getLogger(EmailFactoryBp.class.getName()).log(Level.SEVERE, null, ex);
             }
        return null;
    }

    public Message createEDI864ErrorEmail(String body, String partnerId, String fileName) {
    	
		try {
			String emailBody = edi864ErrorEmailTemplate.createBody(body, partnerId,fileName);
	        Properties emailsLoader = TenantConfigRepository.getInstance().getPropertiesByTenantId(CurrentTenantIdentifierResolverImpl.getCurrentTenant(), "tenant");
			return createEmailMessage(emailBody, null, edi864ErrorEmailTemplate, emailsLoader.getProperty("internal.emails.edi.864.error"));
        }
        catch (IOException | TemplateException e) {
            LOGGER.error(e.getMessage());
        } catch (MessagingException ex) {
            java.util.logging.Logger.getLogger(EmailFactoryBp.class.getName()).log(Level.SEVERE, null, ex);
             }
        return null;
	}

    private Message createEmailMessage(String emailBody, CustomerOrder order, EmailTemplate template, String recipient)
            throws IOException, MessagingException {
        String from = template.getFromAddress();
        StringBuilder subjectBuilder = new StringBuilder(template.getProperty(EMAIL_PROPERTIES_SUBJECT) + " - ");
        Set<String> recipients = new HashSet<String>();
        recipients.add(recipient);
        return createEmailMessage(from, null, recipients, null, subjectBuilder.toString(), emailBody, null, order, null);
    }

    public Message createDuplicateCustomerPoEmail(CustomerOrder order, String duplicatePo) throws IOException, TemplateException{
        PurchaseOrderDto orderDto;
        try {
            orderDto = DtoFactory.createPurchaseOrderDto(order);
            orderDto.setCustomerPoNumber(duplicatePo);

            String from = duplicateCustomerPoErrorEmailTemplate.getFromAddress();
            StringBuilder subjectBuilder = new StringBuilder(duplicateCustomerPoErrorEmailTemplate.getProperty(EMAIL_PROPERTIES_SUBJECT)
                    + " - ");
            Properties emailsLoader = TenantConfigRepository.getInstance().getPropertiesByTenantId(CurrentTenantIdentifierResolverImpl.getCurrentTenant(), "tenant");
            String recipient = emailsLoader.getProperty("internal.emails.duplicate.cust.po");
            Set<String> recipients = new HashSet<>();
            recipients.add(recipient);
            try {
                String emailBody = duplicateCustomerPoErrorEmailTemplate.createBody(orderDto);
                return createEmailMessage(from, null, recipients, null, subjectBuilder.toString(), emailBody, null, order, null);
            }
            catch (IOException e) {
                LOGGER.error(e.getMessage());
            } catch (MessagingException ex) {
                java.util.logging.Logger.getLogger(EmailFactoryBp.class.getName()).log(Level.SEVERE, null, ex);
                 }
            return null;
        
        } catch (ParsingException ex) {
            java.util.logging.Logger.getLogger(EmailFactoryBp.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public Message createWorkflowResubmittedEmail(CustomerOrder order) throws IOException, TemplateException{
        String from = workflowResubmittedErrorEmailTemplate.getFromAddress();
        StringBuilder subjectBuilder = new StringBuilder(workflowResubmittedErrorEmailTemplate.getProperty(EMAIL_PROPERTIES_SUBJECT)
                + " - ");
        Properties emailsLoader = TenantConfigRepository.getInstance().getPropertiesByTenantId(CurrentTenantIdentifierResolverImpl.getCurrentTenant(), "tenant");
        String recipient = emailsLoader.getProperty("internal.emails.workflow.resubmitted.error");
        Set<String> recipients = new HashSet<>();
        recipients.add(recipient);
        try {
            String emailBody = workflowResubmittedErrorEmailTemplate.createBody(order.getApdPo().getValue());
            return createEmailMessage(from, null, recipients, null, subjectBuilder.toString(), emailBody, null, order, null);
        }
        catch (IOException e) {
            LOGGER.error(e.getMessage());
        } catch (MessagingException ex) {
            java.util.logging.Logger.getLogger(EmailFactoryBp.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public static final String USER_REQUEST_CREATE = "create user";
    public static final String USER_REQUEST_MODIFY = "modify user";
    public static final String LOCATION_REQUEST_CREATE = "create location";
    public static final String LOCATION_REQUEST_MODIFY = "modify location";

    @Inject
    private CreateUserRequestEmailTemplate createUserRequestEmailTemplate;
    @Inject
    private CreateUserRequestorEmailTemplate createUserRequestorEmailTemplate;
    @Inject
    private ModifyUserRequestEmailTemplate modifyUserRequestEmailTemplate;
    @Inject
    private AddLocationRequestEmailTemplate addLocationRequestEmailTemplate;
    @Inject
    private ModifyLocationRequestEmailTemplate modifyLocationRequestEmailTemplate;
    @Inject
    private UserRequestCsrEmailTemplate userRequestCsrEmailTemplate;
    @Inject
    private MarfieldUserSetupEmailTemplate marfieldUserSetupEmailTemplate;
    @Inject
    private MarquetteErrorTemplate marquetteErrorTemplate;
    @Inject
    private NewUserSetupEmailTemplate newUserSetupEmailTemplate;

    public List<Message> createUserModificationEmail(UserModificationRequestDto request) {
 
    	List<Message> messagesToSend = new ArrayList<Message>();
    	boolean requiresRequestorEmail =false;
        EmailTemplate template;
        String requestType = request.getRequestType();
        switch (requestType) {
            case USER_REQUEST_CREATE:
                template = createUserRequestEmailTemplate;
                requiresRequestorEmail = true;
                break;
            	
            case USER_REQUEST_MODIFY:
                template = modifyUserRequestEmailTemplate;
                break;
            case LOCATION_REQUEST_CREATE:
                template = addLocationRequestEmailTemplate;
                break;
            case LOCATION_REQUEST_MODIFY:
                template = modifyLocationRequestEmailTemplate;
                break;
            default:
                return null;
        }
        Properties internalEmails = PropertiesLoader.getAsProperties("internal.emails");
        String from = template.getFromAddress();
        String bcc = internalEmails.getProperty(USER_MODIFICATION_BCC_CONFIRMATION);
        Set<String> bccList = new HashSet<>();
        bccList.add(bcc);
        String subject = template.getProperty(EMAIL_PROPERTIES_SUBJECT);
        Set<String> recipList = new HashSet<>();
        Set<String> ccList = new HashSet<>();
        List<String> emailList = request.getEmailList();
        //add all email addresses associated with request
        
        if (emailList != null && emailList.size() > 0){
        	recipList.addAll(emailList);
        }else{
        	//Email list is empty, add minimum necessary emails individually
        	if (request.getInternalEmail() != null) {
                recipList.add(request.getInternalEmail());
            }
            
            if (request.getRequesterManagerEmail() != null) {
                recipList.add(request.getRequesterManagerEmail());
            }
        }
        
        try {
            String emailBody = template.createBody(request);
            messagesToSend.add(createEmailMessage(from, bccList, recipList,  ccList, subject, emailBody, null, null, null));
            if (requiresRequestorEmail){
            	messagesToSend.add(buildRequestorConfirmation(request));
            }
            return messagesToSend;
        }
        catch (Exception e) {
            // TODO Auto-generated catch block
            LOGGER.error("An error occured:", e);
        }
        return null;
    }

    public List<Message> createMarfieldSetupInfoEmail(SystemUser user){
    	List<Message> messagesToSend = new ArrayList<Message>();
    	
        Properties internalEmails = PropertiesLoader.getAsProperties("internal.emails");
        String from = marfieldUserSetupEmailTemplate.getFromAddress();
        String bcc = internalEmails.getProperty(USER_MODIFICATION_BCC_CONFIRMATION);
        Set<String> bccList = new HashSet<>();
        bccList.add(bcc);
        String subject = marfieldUserSetupEmailTemplate.getProperty(EMAIL_PROPERTIES_SUBJECT);
        Set<String> recipList = new HashSet<>();
        recipList.add(user.getPerson().getEmail());
        Set<String> ccList = new HashSet<>();
        
        try {
            String emailBody = marfieldUserSetupEmailTemplate.createBody(user);
            messagesToSend.add(createEmailMessage(from, bccList, recipList,  ccList, subject, emailBody, null, null, null));
            return messagesToSend;
        }
        catch (Exception e) {
            // TODO Auto-generated catch block
            LOGGER.error("An error occured:", e);
        }
        return null;
    }

    public List<Message> createUserSetupInfoEmail(SystemUser newUser) {
		List<Message> messagesToSend = new ArrayList<Message>();
    	
        Properties internalEmails = PropertiesLoader.getAsProperties("internal.emails");
        String from = newUserSetupEmailTemplate.getFromAddress();
        String bcc = internalEmails.getProperty(USER_MODIFICATION_BCC_CONFIRMATION);
        Set<String> bccList = new HashSet<>();
        bccList.add(bcc);
        String subject = newUserSetupEmailTemplate.getProperty(EMAIL_PROPERTIES_SUBJECT);
        Set<String> recipList = new HashSet<>();
        recipList.add(newUser.getPerson().getEmail());
        Set<String> ccList = new HashSet<>();
        
        try {
            String emailBody = newUserSetupEmailTemplate.createBody(newUser);
            messagesToSend.add(createEmailMessage(from, bccList, recipList,  ccList, subject, emailBody, null, null, null));
            return messagesToSend;
        }
        catch (Exception e) {
            // TODO Auto-generated catch block
            LOGGER.error("An error occured:", e);
        }
        return null;
	}

    public List<Message> createUserModificationEmailForCsr(UserModificationRequestDto request) {

    	List<Message> messagesToSend = new ArrayList<Message>();

        Set<String> recipList = new HashSet<>();
        recipList.add(request.getInternalEmail());
        
        try {
            Properties internalEmails = PropertiesLoader.getAsProperties("internal.emails");
            String bcc = internalEmails.getProperty(USER_MODIFICATION_BCC_CONFIRMATION);
            Set<String>bccList = new HashSet<>();
            bccList.add(bcc);
            String emailBody = userRequestCsrEmailTemplate.createBody(request);
            messagesToSend.add(createEmailMessage(userRequestCsrEmailTemplate.getFromAddress(), bccList, recipList,  new HashSet<String>(), 
            		userRequestCsrEmailTemplate.getProperty(EMAIL_PROPERTIES_SUBJECT), emailBody, null, null, null));
            return messagesToSend;
        }
        catch (Exception e) {
            LOGGER.error("An error occured:", e);
        }
        return null;
    }

    private Message buildRequestorConfirmation(UserModificationRequestDto request){
    	
    	CreateUserRequestorEmailTemplate template = createUserRequestorEmailTemplate;
        String from = template.getFromAddress();
        String subject = template.getProperty(EMAIL_PROPERTIES_SUBJECT);
        Set<String> recipList = new HashSet<>();
        
        if (request.getEmail() != null) {
            recipList.add(request.getEmail());
        }
        
        try{
        	String emailBody = template.createBody(request);
        	return createEmailMessage(from, null, recipList, null, subject, emailBody, null, null, null);
        }catch (Exception e){
        	LOGGER.error("An error occured:", e);
        }
    	return null;
    }

    public Set<String> parseRecipients(Set<String> recipients) {
		Set<String> recipientSet = new HashSet<>();
		if (recipients != null) {
			for (String recipient : recipients) {
				if (StringUtils.isNotBlank(recipient)) {
					recipientSet.addAll(Arrays.asList(recipient.split(EMAIL_DELIMITER_REGEX)));
				}
			}
		}
		return recipientSet;
    }

    public Message createEDIOrderNotFoundEmail(MessageDto messageDto, String token, String messageEventType) {
        String from = ediOrderNotFoundEmailTemplate.getFromAddress();
        StringBuilder subjectBuilder = new StringBuilder(ediOrderNotFoundEmailTemplate.getProperty(EMAIL_PROPERTIES_SUBJECT)
                + " - ");
        Properties emailsLoader = TenantConfigRepository.getInstance().getPropertiesByTenantId(CurrentTenantIdentifierResolverImpl.getCurrentTenant(), "tenant");
        String recipient = emailsLoader.getProperty("internal.emails.edi.order.not.found.error");
        Set<String> recipients = new HashSet<String>();
        recipients.add(recipient);
        EdiDocumentDto ediDocumentDto = new EdiDocumentDto();
        ediDocumentDto.setApdPo(token);
        ediDocumentDto.setRawMessage(messageDto.getContent());
        
        List<Attachment> attachments = new ArrayList<Attachment>();

        try {
            Attachment report = ediOrderNotFoundEmailTemplate.createAttachment(ediDocumentDto);
            if (report != null) {
                attachments.add(report);
            }
        }
        catch (NoAttachmentContentException e) {
            LOGGER.warn("No attachment retrieved for email");
        }
        try {
            String emailBody = ediOrderNotFoundEmailTemplate.createBody(ediDocumentDto);
            return createEmailMessage(from, null, recipients, null, subjectBuilder.toString(), emailBody, attachments, null, null);
        }
        catch (IOException | TemplateException e) {
            LOGGER.error(e.getMessage());
        } catch (MessagingException ex) {
            java.util.logging.Logger.getLogger(EmailFactoryBp.class.getName()).log(Level.SEVERE, null, ex);
             }
        return null;
    }

    public Message createSectorValidationFailedEmail(PurchaseOrderDto po, String partnerId) {
    	String emailBody = "<p>The order "+po.retrieveCustomerPoNumber().getValue()+" was sent with account name \""+po.getShipToAddressDto().getCompanyName()+"\" which is unknown and was rejected.</p>";
    	Properties emailsLoader = TenantConfigRepository.getInstance().getPropertiesByTenantId(CurrentTenantIdentifierResolverImpl.getCurrentTenant(), "tenant");
    	
    	String from = emailsLoader.getProperty("internal.emails.noreply");
		
		Set<String> recipients = new HashSet<>();
		if ("ngc".equals(partnerId)) {
			recipients.add("ngcreject@americanproduct.com");
		} else if ("hii".equals(partnerId)) {
			recipients.add("hiireject@americanproduct.com");
		} else {
			recipients.add(emailsLoader.getProperty("internal.emails.order.failed"));
		}
		String subject = "EXTERNAL:Account Name Failure";
		try {
            return createEmailMessage(from, null, recipients, null, subject, emailBody, null, null, null);
        }
        catch (IOException e) {
            LOGGER.error(e.getMessage());
        } catch (MessagingException ex) {
            java.util.logging.Logger.getLogger(EmailFactoryBp.class.getName()).log(Level.SEVERE, null, ex);
             }
        return null;
    }

    public Message createPurchaseOrderEmail(CustomerOrder order, PurchaseOrderDto purchaseOrderDto) {

        Set<String> recipientSet = new HashSet<>();
        String recipients = purchaseOrderDto.getRecipients();
        if(recipients != null) {
            String[] recipientList = recipients.split(",");
            for(String recipient:recipientList) {
                    recipientSet.add(recipient);	
            }
        }
        
        Set<String> ccSet = new HashSet<>();

        Properties emailsLoader = TenantConfigRepository.getInstance().getPropertiesByTenantId(CurrentTenantIdentifierResolverImpl.getCurrentTenant(), "tenant");
        
        String ccString = emailsLoader.getProperty("internal.emails.vendor.po.cc");
        
        if (StringUtils.isNotBlank(ccString)) {
        	for (String cc : ccString.split(",")) {
        		ccSet.add(cc);
        	}
        }

    	return this.createOrderNotificationEmail(order, EventType.PURCHASE_ORDER_SENT, recipientSet, 
    			new HashSet<String>(), new HashSet<String>(), purchaseOrderDto);
    }

    public Message createMarquetteErrorEmail(InvoiceDto invoice, String exception) {
		String from = marquetteErrorTemplate.getFromAddress();
        StringBuilder subjectBuilder = new StringBuilder(marquetteErrorTemplate.getProperty(EMAIL_PROPERTIES_SUBJECT)
                + " - ");
        String recipient = marquetteErrorTemplate.getProperty(MARQUETTE_ERROR_RECIPIENT);
        Set<String> recipients = new HashSet<String>();
        recipients.add(recipient);
        
        List<Attachment> attachments = new ArrayList<Attachment>();
        try {
        	invoice.setException(exception);
            String emailBody = marquetteErrorTemplate.createBody(invoice);
            return createEmailMessage(from, null, recipients, null, subjectBuilder.toString(), emailBody, attachments, null, null);
        }
        catch (IOException | TemplateException e) {
            LOGGER.error(e.getMessage());
        } catch (MessagingException ex) {
            java.util.logging.Logger.getLogger(EmailFactoryBp.class.getName()).log(Level.SEVERE, null, ex);
             }
        return null;
	}

    protected String getTenantPropertyByName(String propertyName) {
        return TenantConfigRepository.getInstance().getProperty(getTenantName(), "tenant", propertyName);
    }

    private String getTenantName() {
        return TenantConfigRepository.getInstance().getTenantNameById(
                CurrentTenantIdentifierResolverImpl.getCurrentTenant());
    }

    public List<Message> createRequestForQuoteEmails(long orderId, Object content, EventType eventType) {
    	List<Message> toReturn = new ArrayList<>();
        CustomerOrder customerOrder = customerOrderBp.findById(orderId, CustomerOrder.class);
        if (content == null) {
            try {
                content = DtoFactory.createPurchaseOrderDto(customerOrder);
            }
            catch (ParsingException e) {
                LOGGER.error("An error occured:", e);
            }
        }

        Set<NotificationProperties> notificationProperties = customerOrderBp.getNotificationPropertiesForEvent(customerOrder, eventType);
        Set<String> notificationTo = new HashSet<>();
        Set<String> notificationCC = new HashSet<>();
        Set<String> notificationBCC = new HashSet<>();
        if (notificationProperties != null) {
        	for (NotificationProperties prop : notificationProperties) {
        		if (prop.getAddressing() != null && StringUtils.isNotBlank(prop.getRecipients())) {
        			List<String> recipients = Arrays.asList(prop.getRecipients().split(","));
        			switch (prop.getAddressing()) {
        			case TO: notificationTo.addAll(recipients);
        				break;
        			case CC: notificationCC.addAll(recipients);
        				break;
        			case BCC: notificationBCC.addAll(recipients);
        			}
        		}
        	}
        }
        
        addInternalEmails(notificationTo, eventType);
        
        addCredentialEmail(notificationTo, eventType, customerOrder.getCredential());
                
        if (notificationTo.isEmpty()) {
            Properties tenantProperties = TenantConfigRepository.getInstance().getPropertiesByTenantId(CurrentTenantIdentifierResolverImpl.getCurrentTenant(), "tenant");
            for (String address : tenantProperties.getProperty("internal.emails.ses.failure").split(",")) {
            	notificationTo.add(address);
            }
        }
        
        toReturn.add(createOrderNotificationEmail(customerOrder, eventType, notificationTo, notificationCC, notificationBCC, content));
        return toReturn;
    }

    public List<Message> createProformaInvoiceEmails(long orderId, Object content, EventType eventType) {
    	List<Message> toReturn = new ArrayList<>();
        CustomerOrder customerOrder = customerOrderBp.findById(orderId, CustomerOrder.class);
        if (content == null) {
            try {
                content = DtoFactory.createPurchaseOrderDto(customerOrder);
            }
            catch (ParsingException e) {
                LOGGER.error("An error occured:", e);
            }
        }

        Set<NotificationProperties> notificationProperties = customerOrderBp.getNotificationPropertiesForEvent(customerOrder, eventType);
        Set<String> notificationTo = new HashSet<>();
        Set<String> notificationCC = new HashSet<>();
        Set<String> notificationBCC = new HashSet<>();
        if (notificationProperties != null) {
        	for (NotificationProperties prop : notificationProperties) {
        		if (prop.getAddressing() != null && StringUtils.isNotBlank(prop.getRecipients())) {
        			List<String> recipients = Arrays.asList(prop.getRecipients().split(","));
        			switch (prop.getAddressing()) {
        			case TO: notificationTo.addAll(recipients);
        				break;
        			case CC: notificationCC.addAll(recipients);
        				break;
        			case BCC: notificationBCC.addAll(recipients);
        			}
        		}
        	}
        }
        
        addInternalEmails(notificationTo, eventType);
        
        addCredentialEmail(notificationTo, eventType, customerOrder.getCredential());
                
        if (notificationTo.isEmpty()) {
            Properties tenantProperties = TenantConfigRepository.getInstance().getPropertiesByTenantId(CurrentTenantIdentifierResolverImpl.getCurrentTenant(), "tenant");
            for (String address : tenantProperties.getProperty("internal.emails.ses.failure").split(",")) {
            	notificationTo.add(address);
            }
        }
        
        toReturn.add(createOrderNotificationEmail(customerOrder, eventType, notificationTo, notificationCC, notificationBCC, content));
        return toReturn;
    }
}