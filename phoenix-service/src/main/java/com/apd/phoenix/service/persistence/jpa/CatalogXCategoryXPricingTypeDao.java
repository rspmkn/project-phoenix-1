package com.apd.phoenix.service.persistence.jpa;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.Query;
import com.apd.phoenix.service.model.CatalogXCategoryXPricingType;

/**
 * CatalogXCategoryXPricingType DAO stub
 * 
 * @author RHC
 *
 */
@Stateless
@LocalBean
public class CatalogXCategoryXPricingTypeDao extends AbstractDao<CatalogXCategoryXPricingType> {

    public CatalogXCategoryXPricingType getPricing(Long catalogId, Long categoryId) {
        if (catalogId == null || categoryId == null) {
            return null;
        }
        String hql = "SELECT catPricing FROM CatalogXCategoryXPricingType catPricing "
                + "WHERE catPricing.catalog.id = :catalogId " + "AND catPricing.category.id = :categoryId";
        //Creates the query
        Query query = entityManager.createQuery(hql);
        query.setParameter("catalogId", catalogId);
        query.setParameter("categoryId", categoryId);
        return getSingleResultOrNull(query);
    }
}
