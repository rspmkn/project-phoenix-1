package com.apd.phoenix.service.persistence.jpa;

import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.Query;
import com.apd.phoenix.service.model.PaymentType;

/**
 * Address DAO stub
 * 
 * @author RHC
 *
 */
@Stateless
@LocalBean
public class PaymentTypeDao extends AbstractDao<PaymentType> {

    public PaymentType getPaymentTypeById(Long id) { //TODO:  This should already exist as 'findById' inherited from AbstractDao.  This should be removed and references in other places of the code updated.  Same for any other DAOs and BPs where this method is unnecessarily duplicated.
        String hql = "SELECT s FROM PaymentType AS s WHERE s.id=:id";
        Query query = entityManager.createQuery(hql);
        query.setParameter("id", id);
        return (PaymentType) query.getSingleResult();
    }

    @SuppressWarnings("unchecked")
    public List<PaymentType> getPaymentTypeList() {
        String hql = "SELECT s FROM PaymentType AS s";
        Query query = entityManager.createQuery(hql);
        return query.getResultList();
    }

    @SuppressWarnings("unchecked")
    public List<PaymentType> getPaymentTypeByValue(String value) {
        //Sanity check
        if (value == null || value.equals("")) {
            return null;
        }

        StringBuilder hql = new StringBuilder(
                "SELECT paymentType FROM PaymentType AS paymentType WHERE paymentType.name=:value");

        Query query = entityManager.createQuery(hql.toString());
        query.setParameter("value", value);

        return query.getResultList();
    }
}
