package com.apd.phoenix.service.persistence.jpa;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.Query;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;
import com.apd.phoenix.core.utility.TenantConfigRepository;
import com.apd.phoenix.service.model.Catalog;
import com.apd.phoenix.service.model.Catalog.SearchType;
import com.apd.phoenix.service.model.CatalogXItem;
import com.apd.phoenix.service.model.CustomerOrder;
import com.apd.phoenix.service.model.Item;

/**
 * CatalogXItem DAO stub
 * 
 * @author RHC
 *
 */
@Stateless
@LocalBean
public class CatalogXItemDao extends AbstractDao<CatalogXItem> {

    private static final String JMS_BATCH_SIZE = "100";
    private static final String JMS_PROPERTIES = "catalog.upload.integration";
    private static final int PRICE_BATCH_SIZE = 100;
    public static final String USPS_APD_DESIGNATED_VENDOR_ID = "usps";

    @Override
    public CatalogXItem update(CatalogXItem catalogXItem) {
        if (catalogXItem.getCatalog().getCustomer() != null) {
            if (USPS_APD_DESIGNATED_VENDOR_ID.equals(catalogXItem.getCatalog().getCustomer().getApdAssignedAccountId())) {
                CatalogXItem oldCatalogXItem = findById(catalogXItem.getId(), CatalogXItem.class);
                if (changedRelevantFields(catalogXItem, oldCatalogXItem)) {
                    catalogXItem.setModified(Boolean.TRUE);
                }
            }
        }
        return super.update(catalogXItem);
    }

    @Override
    public CatalogXItem create(CatalogXItem catalogXItem) {
        if (USPS_APD_DESIGNATED_VENDOR_ID.equals(catalogXItem.getCatalog().getCustomer().getApdAssignedAccountId())) {
            catalogXItem.setAdded(Boolean.TRUE);
        }
        return super.create(catalogXItem);
    }

    @Override
    public void createNoRefresh(CatalogXItem catalogXItem) {
        if (USPS_APD_DESIGNATED_VENDOR_ID.equals(catalogXItem.getCatalog().getCustomer().getApdAssignedAccountId())) {
            catalogXItem.setAdded(Boolean.TRUE);
        }
        super.createNoRefresh(catalogXItem);
    }

    public void deleteQuietly(Long id, Class<CatalogXItem> aClass) {
        super.delete(id, aClass);
    }

    @SuppressWarnings("unchecked")
    public Map<CatalogXItem, String> getCatalogXItemsSimilarItems(CatalogXItem thisCXItem, int maxResults) {

    	Map<CatalogXItem, String> relationshipType = new HashMap<>();
    	Query test = entityManager.createQuery("SELECT relative.id, irl.relationship from Item i JOIN i.similarItems as irl JOIN irl.relative AS relative"
    			                               +" WHERE i.id = :itemId");
        test.setParameter("itemId", thisCXItem.getItem().getId());
    	
    	List<Object[]> results = test.getResultList();
    	Map<Long, String> relatedItems = new HashMap<>();
    	if (results.isEmpty()) {
    		return relationshipType;
    	} else {
    		for (Object[] result : results) {    			
    			relatedItems.put((Long)result[0], (String)result[1]);
    		}
    	}
        Query query = entityManager.createQuery("SELECT cxi FROM CatalogXItem cxi WHERE cxi.catalog.id = :catalogId AND cxi.item.id IN (:resultList)");
        query.setParameter("catalogId", thisCXItem.getCatalog().getId());
        query.setParameter("resultList", relatedItems.keySet());
        setItemsReturned(query, 0, maxResults);
        List<CatalogXItem> CatalogXItems = query.getResultList();
        for(CatalogXItem xItem : CatalogXItems) {
        	relationshipType.put(xItem, relatedItems.get(xItem.getItem().getId()));
        }
        return relationshipType;
    }

    @SuppressWarnings("unchecked")
    public List<Integer> getQuantitiesForOrder(CustomerOrder order) {
        StringBuilder hql = new StringBuilder(
                "SELECT lineItem.quantity FROM "
                        + "LineItem as lineItem INNER JOIN lineItem.order as order WHERE order.id=:orderId ORDER BY lineItem.customerSku");
        Query query = entityManager.createQuery(hql.toString());
        query.setParameter("orderId", order.getId());
        return query.getResultList();
    }

    public void refresh(CatalogXItem toRefresh) {
        this.entityManager.refresh(toRefresh);
    }

    public void detach(CatalogXItem item) {
        this.entityManager.detach(item);
    }

    @SuppressWarnings("unchecked")
    public List<CatalogXItem> batchForPrice(Catalog catalog, int batch) {
        String preHql = "SELECT cxi.id FROM CatalogXItem AS cxi WHERE cxi.catalog.id = :id";
        Query preQuery = entityManager.createQuery(preHql);
        preQuery.setParameter("id", catalog.getId());
        setItemsReturned(preQuery, batch * PRICE_BATCH_SIZE, PRICE_BATCH_SIZE);
        List<Double> idList = preQuery.getResultList();
        if (idList.size() > 0) {
	        String hql = "SELECT DISTINCT cxi FROM CatalogXItem AS cxi LEFT JOIN FETCH cxi.item AS item LEFT JOIN FETCH "
	        	+ "item.vendorCatalog LEFT JOIN FETCH item.properties LEFT JOIN FETCH cxi.catalog WHERE cxi.id IN (:idList)";
	        //Creates the query
	        Query query = entityManager.createQuery(hql);
	        query.setParameter("idList", idList);
	        return query.getResultList();
        }
        return new ArrayList<>();
    }

    @SuppressWarnings("unchecked")
    public List<Long> batchForJMS(Catalog input, int batch) {
        Properties jmsProperties = TenantConfigRepository.getInstance().getProperties(JMS_PROPERTIES);
        int jmsBatchSize = Integer.parseInt(jmsProperties.getProperty("jmsBatchSize", JMS_BATCH_SIZE));
        String preHql = "SELECT cxi.id FROM CatalogXItem AS cxi WHERE cxi.catalog.id = :id";
        Query preQuery = entityManager.createQuery(preHql);
        preQuery.setParameter("id", input.getId());
        setItemsReturned(preQuery, batch * jmsBatchSize, jmsBatchSize);
        return preQuery.getResultList();
    }

    @SuppressWarnings("unchecked")
    public List<Long> batchAllForJMS(int batch) {
        Properties jmsProperties = TenantConfigRepository.getInstance().getProperties(JMS_PROPERTIES);
        int jmsBatchSize = Integer.parseInt(jmsProperties.getProperty("allJmsBatchSize", JMS_BATCH_SIZE));
        String preHql = "SELECT cxi.id FROM CatalogXItem AS cxi";
        Query preQuery = entityManager.createQuery(preHql);
        setItemsReturned(preQuery, batch * jmsBatchSize, jmsBatchSize);
        return preQuery.getResultList();
    }

    //Updates the entity without showing that the item was modified, used to clear the modified field
    public void updateQuietly(CatalogXItem itemEntity) {
        super.update(itemEntity);
        entityManager.flush();
    }

    //price is only relevant field
    private boolean changedRelevantFields(CatalogXItem item, CatalogXItem oldCatalogXItem) {
        if (item.getPrice() == null) {
            return oldCatalogXItem.getPrice() == null;
        }
        else {
            return item.getPrice().equals(oldCatalogXItem.getPrice());
        }
    }

    @SuppressWarnings("unchecked")
    public List<Long> getUspsChangedItemsIds() {
        String hql = "SELECT cxi.id FROM CatalogXItem AS cxi JOIN cxi.item AS item "
                + "JOIN cxi.catalog AS cat WHERE cat.customer.apdAssignedAccountId = 'usps' "
                + "AND (coalesce(cxi.modified, false) = true OR coalesce(cxi.added, false) = true "
                + "OR coalesce(cxi.deleted, false) = true OR coalesce(item.externalCatalogChange,false) = true "
                + "OR coalesce(cat.changedItemRelevantFields,false) = true) "
                //Do not include items which were added and then removed before the customer updated their catalog
                + "AND NOT (coalesce(cxi.added, false) = true AND coalesce(cxi.deleted, false) = true)";
        Query query = entityManager.createQuery(hql);
        return query.getResultList();
    }

    public CatalogXItem getCatalogXItem(Item resultItem, long catalogId) {
        CatalogXItem catalogXItem = new CatalogXItem();
        catalogXItem.setItem(resultItem);
        catalogXItem.setCatalog(new Catalog());
        catalogXItem.getCatalog().setId(catalogId);
        List<CatalogXItem> catxItems = searchByExactExample(catalogXItem, 0, 0);
        if (catxItems.isEmpty()) {
            return null;
        }
        else {
            catalogXItem = catxItems.get(0);
            catalogXItem = this.findById(catalogXItem.getId(), CatalogXItem.class);
        }
        return catalogXItem;
    }

    public CatalogXItem searchByCustomerSku(String sku, Catalog catalog) {
        String hql = "SELECT catX FROM CatalogXItem AS catX "
                + "WHERE catX.customerSkuString = :sku AND catX.catalog.id = :catId";
        //Creates the query
        Query query = entityManager.createQuery(hql);
        query.setParameter("sku", sku);
        query.setParameter("catId", catalog.getId());
        return (CatalogXItem) getSingleResultOrNull(query);
    }

    @SuppressWarnings("unchecked")
    public List<Long> itemIdByCustomerSku(String sku, Long catalogId) {
    	
    	ArrayList<Long> toReturn = new ArrayList<>();

        Session session = (Session) entityManager.getDelegate();

        final org.hibernate.Query itemQuery = session.createSQLQuery("SELECT CXI.ITEM_ID AS ITEM_ID FROM CATALOGXITEM CXI WHERE CXI.CATALOG_ID = :catalogId AND CXI.CUSTOMERSKUSTRING = :sku");
        itemQuery.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
        itemQuery.setParameter("sku", sku);
        itemQuery.setParameter("catalogId", catalogId);

        List<Map<String, Object>> results = (List<Map<String, Object>>) itemQuery.list();

        if (results != null) {
        	for (Map<String, Object> result : results) {
        		if (result != null && result.get("ITEM_ID") != null) {
        			toReturn.add(((BigInteger) result.get("ITEM_ID")).longValue());
        		}
        	}
        }
        return toReturn;
    }

    public String getOverrideCatalogs(CatalogXItem item, List<Catalog> toCheck) {
    	if (item == null || item.getItem() == null || toCheck == null || toCheck.isEmpty()) {
    		return "";
    	}
        // get the hibernate session from the injected entity manager.
        Session session = (Session) entityManager.getDelegate();
        
        List<Long> idsToCheck = new ArrayList<>();
        for (Catalog catalog : toCheck) {
        	if (catalog != null && catalog.getId() != null) {
        		idsToCheck.add(catalog.getId());
        	}
        }

        String overridesQuery = "select "
                + "        overrideCxi.catalog_id AS OVERRIDES_ID "
                + "        from CatalogXItem overrideCxi "
                + "      	WHERE overrideCxi.item_id = :itemId AND overrideCxi.catalog_id in (:idsToCheck) ";
        final org.hibernate.Query itemQuery = session.createSQLQuery(overridesQuery);
        itemQuery.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
        itemQuery.setParameter("itemId", item.getItem().getId());
        itemQuery.setParameterList("idsToCheck", idsToCheck);

        List<Map<String, Object>> results = (List<Map<String, Object>>) itemQuery.list();

        if (results == null) {
            return "";
        }

        String toReturn = "";

        for (Map<String, Object> result : results) {
            String overridesIds = result.get("OVERRIDES_ID") == null ? "" : ((BigInteger) result.get("OVERRIDES_ID"))
                    .toString();
            if (StringUtils.isNotBlank(overridesIds)) {
                if (StringUtils.isNotBlank(toReturn)) {
                    toReturn = toReturn.concat(",");
                }
                toReturn = toReturn.concat(overridesIds);
            }
        }

        return toReturn;
    }

    public List<CatalogXItem> findSmartSearchItemsWhichBecomeCoreToday() {
        String hql = "select cxi from CatalogXItem as cxi where cxi.catalog.searchType = :smartSearchType "
                + " and cxi.coreItemStartDate > :yesterdayMidnight and cxi.coreItemStartDate < :tonightMidnight";
        Query query = entityManager.createQuery(hql);
        query.setParameter("smartSearchType", Catalog.SearchType.SMART_SEARCH);
        Calendar yesterdayMidnight = Calendar.getInstance();
        yesterdayMidnight.add(Calendar.DATE, -1);
        yesterdayMidnight.set(Calendar.HOUR_OF_DAY, 23);
        yesterdayMidnight.set(Calendar.MINUTE, 59);
        query.setParameter("yesterdayMidnight", yesterdayMidnight.getTime());

        Calendar tomorrowMidnight = Calendar.getInstance();
        tomorrowMidnight.add(Calendar.DATE, 1);
        tomorrowMidnight.set(Calendar.HOUR_OF_DAY, 0);
        tomorrowMidnight.set(Calendar.MINUTE, 0);
        query.setParameter("tonightMidnight", tomorrowMidnight.getTime());
        List<CatalogXItem> resultList = (List<CatalogXItem>) query.getResultList();
        if (resultList == null) {
            return new ArrayList<CatalogXItem>();
        }
        return resultList;
    }

    public List<CatalogXItem> findSmartSearchItemsWhichAreNoLongerCoreToday() {
        String hql = "select cxi from CatalogXItem as cxi where cxi.catalog.searchType = :smartSearchType "
                + " and cxi.coreItemExpirationDate > :yesterdayMidnight and cxi.coreItemExpirationDate < :tonightMidnight";
        Query query = entityManager.createQuery(hql);
        query.setParameter("smartSearchType", Catalog.SearchType.SMART_SEARCH);
        Calendar yesterdayMidnight = Calendar.getInstance();
        yesterdayMidnight.add(Calendar.DATE, -1);
        yesterdayMidnight.set(Calendar.HOUR_OF_DAY, 23);
        yesterdayMidnight.set(Calendar.MINUTE, 59);
        query.setParameter("yesterdayMidnight", yesterdayMidnight.getTime());

        Calendar tomorrowMidnight = Calendar.getInstance();
        tomorrowMidnight.add(Calendar.DATE, 1);
        tomorrowMidnight.set(Calendar.HOUR_OF_DAY, 0);
        tomorrowMidnight.set(Calendar.MINUTE, 0);
        query.setParameter("tonightMidnight", tomorrowMidnight.getTime());
        List<CatalogXItem> resultList = (List<CatalogXItem>) query.getResultList();
        if (resultList == null) {
            return new ArrayList<CatalogXItem>();
        }
        return resultList;
    }

    public List<CatalogXItem> getRandomCoreItems(Catalog catalog, Integer randomSort, Integer numberToReturn) {

        List<CatalogXItem> resultList = new ArrayList<CatalogXItem>();

        String baseQuery = "from catalogxitem where catalogxitem.catalog_id = :catalogId "
        		+ "and catalogxitem.coreitemstartdate < NOW()";
        String countSql = "select count(*) AS COUNT " + baseQuery;
        org.hibernate.Query countQuery = createFeaturedQuery(catalog, countSql);
        List<Map<String, Object>> countResults = (List<Map<String, Object>>) countQuery.list();
        BigInteger count = BigInteger.ZERO;
        for (Map<String, Object> result : countResults) {
        	if (result != null && result.get("COUNT") != null) {
        		count = ((BigInteger)result.get("COUNT"));
        	}
        }
        if (count.compareTo(BigInteger.ZERO) > 0) {

            String randomIdsSql = "select catalogxitem.id AS ID " + baseQuery + " order by RAND() LIMIT :numberToReturn";
            org.hibernate.Query randomIdsQuery = createFeaturedQuery(catalog, randomIdsSql);
            randomIdsQuery.setParameter("numberToReturn", numberToReturn);
            List<Map<String, Object>> randomIdsResults = (List<Map<String, Object>>) randomIdsQuery.list();
            
            ArrayList<Long> idList = new ArrayList<>();
            for (Map<String, Object> randomIdsResult : randomIdsResults) {
            	if (randomIdsResult != null && randomIdsResult.get("ID") != null) {
            		idList.add(((BigInteger)randomIdsResult.get("ID")).longValue());
            	}
            }

            if (idList != null && !idList.isEmpty()) {
	            String toReturnHql = "select cxi from CatalogXItem as cxi where cxi.id in (:idList)";
	            Query toReturnQuery = entityManager.createQuery(toReturnHql);
	            toReturnQuery.setParameter("idList", idList);
	            resultList = (List<CatalogXItem>) toReturnQuery.getResultList();
            }

        }
        return resultList;
    }

    private org.hibernate.Query createFeaturedQuery(Catalog catalog, String sql) {
        Session session = (Session) entityManager.getDelegate();
        org.hibernate.Query query = session.createSQLQuery(sql);
        query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
        query.setParameter("catalogId", catalog.getId());
        return query;
    }

    public List<CatalogXItem> findSmartSearchItemsInNewlyActiveCatalog() {
        String hql = "select cxi from CatalogXItem as cxi where cxi.catalog.searchType = :smartSearchType "
                + " and cxi.catalog.startDate > :yesterdayMidnight and cxi.catalog.startDate < :tonightMidnight";
        Query query = entityManager.createQuery(hql);
        query.setParameter("smartSearchType", Catalog.SearchType.SMART_SEARCH);
        Calendar yesterdayMidnight = Calendar.getInstance();
        yesterdayMidnight.add(Calendar.DATE, -1);
        yesterdayMidnight.set(Calendar.HOUR_OF_DAY, 23);
        yesterdayMidnight.set(Calendar.MINUTE, 59);
        query.setParameter("yesterdayMidnight", yesterdayMidnight.getTime());

        Calendar tomorrowMidnight = Calendar.getInstance();
        tomorrowMidnight.add(Calendar.DATE, 1);
        tomorrowMidnight.set(Calendar.HOUR_OF_DAY, 0);
        tomorrowMidnight.set(Calendar.MINUTE, 0);
        query.setParameter("tonightMidnight", tomorrowMidnight.getTime());
        List<CatalogXItem> resultList = (List<CatalogXItem>) query.getResultList();
        if (resultList == null) {
            return new ArrayList<CatalogXItem>();
        }
        return resultList;
    }

    public List<CatalogXItem> findSmartSearchItemsInNewlyInactiveCatalog() {
        String hql = "select cxi from CatalogXItem as cxi where cxi.catalog.searchType = :smartSearchType "
                + " and cxi.catalog.expirationDate > :yesterdayMidnight and cxi.catalog.expirationDate < :tonightMidnight";
        Query query = entityManager.createQuery(hql);
        query.setParameter("smartSearchType", Catalog.SearchType.SMART_SEARCH);
        Calendar yesterdayMidnight = Calendar.getInstance();
        yesterdayMidnight.add(Calendar.DATE, -1);
        yesterdayMidnight.set(Calendar.HOUR_OF_DAY, 23);
        yesterdayMidnight.set(Calendar.MINUTE, 59);
        query.setParameter("yesterdayMidnight", yesterdayMidnight.getTime());

        Calendar tomorrowMidnight = Calendar.getInstance();
        tomorrowMidnight.add(Calendar.DATE, 1);
        tomorrowMidnight.set(Calendar.HOUR_OF_DAY, 0);
        tomorrowMidnight.set(Calendar.MINUTE, 0);
        query.setParameter("tonightMidnight", tomorrowMidnight.getTime());
        List<CatalogXItem> resultList = (List<CatalogXItem>) query.getResultList();
        if (resultList == null) {
            return new ArrayList<CatalogXItem>();
        }
        return resultList;
    }

    @SuppressWarnings("unchecked")
    public CatalogInfo getCatalogInfoFromItemId(Long catalogXItemId) {
        String hql = "SELECT cxi.catalog.id, cxi.catalog.searchType FROM CatalogXItem as cxi where cxi.id = :catalogXItemId";
        //Creates the query
        Query query = entityManager.createQuery(hql);
        query.setParameter("catalogXItemId", catalogXItemId);
        List<Object[]> results = query.getResultList();
        if (results == null || results.isEmpty()) {
            return null;
        }
        CatalogInfo toReturn = new CatalogInfo();
        toReturn.setId((Long) results.get(0)[0]);
        toReturn.setSearchType((SearchType) results.get(0)[1]);
        return toReturn;
    }

    public static class CatalogInfo {

        private Long id;
        private SearchType searchType;

        public Long getId() {
            return id;
        }

        public void setId(Long id) {
            this.id = id;
        }

        public SearchType getSearchType() {
            return searchType;
        }

        public void setSearchType(SearchType searchType) {
            this.searchType = searchType;
        }
    }

    @Override
    public CatalogXItem eagerLoad(CatalogXItem toLoad) {
        if (toLoad == null || toLoad.getId() == null) {
            return toLoad;
        }
        CatalogXItem toReturn = this.findById(toLoad.getId(), CatalogXItem.class);
        if (toReturn.getProperties() != null) {
            toReturn.getProperties().size();
        }
        if (toReturn.getCartItem() != null) {
            toReturn.getCartItem().size();
        }
        if (toReturn.getSubstituteItem() != null) {
            toReturn.getSubstituteItem().toString();
        }
        if (toReturn.getCatalog() != null) {
            toReturn.getCatalog().toString();
        }
        if (toReturn.getPricingType() != null) {
            toReturn.getPricingType().toString();
        }
        if (toReturn.getItem() != null) {
            toReturn.getItem().toString();
        }
        if (toReturn.getFavorites() != null) {
            toReturn.getFavorites().size();
        }
        if (toReturn.getUserFavorites() != null) {
            toReturn.getUserFavorites().size();
        }
        if (toReturn.getCustomerUnitOfmeasure() != null) {
            toReturn.getCustomerUnitOfmeasure().toString();
        }
        return toReturn;
    }

}
