package com.apd.phoenix.service.persistence.jpa;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;

/**
 * Account DAO stub
 * 
 * @author RHC
 * @param <T> 
 *
 */
@Stateless
@LocalBean
public class GenericDao<T> extends AbstractDao<T> {
}
