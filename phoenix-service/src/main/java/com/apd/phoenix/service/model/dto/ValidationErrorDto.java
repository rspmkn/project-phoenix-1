package com.apd.phoenix.service.model.dto;

import java.io.Serializable;

public class ValidationErrorDto implements Serializable {

    private String error;

    public ValidationErrorDto(String value) {
        this.error = value;
    }

    public static ValidationErrorDto UNRECOGNIZED_SKU = new ValidationErrorDto("Unrecognized sku");
    public static ValidationErrorDto OVER_SHIPPED = new ValidationErrorDto("Quantity shipped is too high");
    public static ValidationErrorDto OVER_ACKED = new ValidationErrorDto("Incorrect quantity");
    public static ValidationErrorDto PRICE_ERROR = new ValidationErrorDto("There is a price error");
    public static ValidationErrorDto ADOT = new ValidationErrorDto("ADOT");
    public static ValidationErrorDto ITEM_QUANTITY_CHANGED = new ValidationErrorDto("Item quantity was changed");

    public String getError() {
        return this.error;
    }
}
