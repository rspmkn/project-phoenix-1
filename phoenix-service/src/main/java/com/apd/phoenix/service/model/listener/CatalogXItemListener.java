package com.apd.phoenix.service.model.listener;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map;
import javax.jms.JMSException;
import javax.naming.NamingException;
import javax.persistence.PostPersist;
import javax.persistence.PostRemove;
import javax.persistence.PostUpdate;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import org.apache.commons.lang.StringUtils;
import com.apd.phoenix.service.model.Catalog.SearchType;
import com.apd.phoenix.service.model.CatalogXItem;
import com.apd.phoenix.service.utility.SmartSearchUtils;

/**
 *
 * @author RHC
 */
public class CatalogXItemListener extends SolrEntityListener {

    @PrePersist
    @PreUpdate
    public void preUpdateEventListener(CatalogXItem entity) {
        entity.setLastModified(new Date());
        Map<Long, String> priceCategories = entity.getItem().getPriceCategories();
        priceCategories.put(entity.getCatalog().getId(), SmartSearchUtils.getCategoryCodeFromPrice(entity.getPrice()));
        //overwrite old customer sku if changed or add new one
        if (StringUtils.isNotBlank(entity.getCustomerSkuString())) {
            entity.getItem().getSmartSearchCustomerSkus().put(entity.getCatalog().getId(),
                    entity.getCustomerSkuString());
        }
        //Converts smartSerach keywords into string since item update is done with raw sql
        String smartSearchKeywords = "";
        for (Long catalogId : priceCategories.keySet()) {
            if (priceCategories.get(catalogId) != null) {
                smartSearchKeywords = smartSearchKeywords + catalogId.toString() + "-" + priceCategories.get(catalogId)
                        + " ";
            }
        }
        //Does not include catalog, since we cannot append the catalog id to each word the user enters
        for (String sku : new ArrayList<String>(entity.getItem().getSmartSearchCustomerSkus().values())) {
            smartSearchKeywords = smartSearchKeywords + sku + " ";
        }
        entity.getItem().setSmartSearchKeywords(smartSearchKeywords);
    }

    @PostPersist
    public void addInsertSolrDocument(final CatalogXItem entity) throws NamingException, JMSException {
        doSolrDocumentAction(entity.getId(), SolrDocumentAction.CATXI_INSERT);
    }

    @PostUpdate
    public void addUpdateSolrDocument(final CatalogXItem entity) throws NamingException, JMSException {
        if (entity.getProperty(CatalogXItem.AVAILABILITY_PROPERTY) != null
                && "no".equals(entity.getProperty(CatalogXItem.AVAILABILITY_PROPERTY).getValue())) {
            addDeleteSolrDocument(entity);
        }
        else if (entity.isSendUpdateMessage()) {
            addUpdateSolrDocument(entity.getId());
        }
        entity.setSendUpdateMessage(false);
    }

    public static void addUpdateSolrDocument(final Long id) throws NamingException, JMSException {
        doSolrDocumentAction(id, SolrDocumentAction.CATXI_UPDATE);
        doSolrDocumentAction(id, SolrDocumentAction.CATXI_CACHE);
    }

    @PostRemove
    public void deleteSolrDocument(final CatalogXItem entity) throws JMSException, NamingException {
        addDeleteSolrDocument(entity);
    }

    public static void addDeleteSolrDocument(final CatalogXItem entity) throws JMSException, NamingException {
        doSolrDocumentAction(new CatalogXItem.DeleteItemMessage(entity), SolrDocumentAction.CATXI_DELETE);
        if (SearchType.SMART_SEARCH.equals(entity.getCatalog().getSearchType())) {
            if (entity.getCoreItemStartDate() != null || entity.getCoreItemExpirationDate() != null) {
                addDeleteInSmartsearch(entity.getItem().getId(), entity.getCatalog().getCoreListUid().getId());
                addDeleteInSmartsearch(entity.getItem().getId(), entity.getCatalog().getListUid().getId());
            }
            else {
                addDeleteInSmartsearch(entity.getItem().getId(), entity.getCatalog().getListUid().getId());
            }
        }
    }

    public static void addRepriceSolrDocument(final ArrayList<Long> ids) throws NamingException, JMSException {
        if (ids != null && !ids.isEmpty()) {
            ArrayList<Serializable> message = new ArrayList<Serializable>(ids.size());
            message.addAll(ids);
            doSolrDocumentActions(message, SolrDocumentAction.CATXI_REPRICE);
        }
    }

    //called once to set up override list on all items, the lists should be up to date
    //moving forward, so this won't get called again.
    public static void addCacheRefresh(final Long id) throws NamingException, JMSException {
        doSolrDocumentAction(id, SolrDocumentAction.CATXI_CACHE);
    }

    //called once to set up override list on all items, the lists should be up to date
    //moving forward, so this won't get called again.
    public static void addCalculateOverrides(final Long id) throws NamingException, JMSException {
        doSolrDocumentAction(id, SolrDocumentAction.CATXI_OVERRIDES_RECALC);
    }

    public static void addInsertInSmartsearch(Long id, Long listName, Long coreListName) throws NamingException,
            JMSException {
        doSolrDocumentAction(new CatalogXItem.UpdateSmartSearchMessage(id, listName, coreListName),
                SolrDocumentAction.SMARTSEARCH_LIST_INSERT);
    }

    public static void addDeleteInSmartsearch(final Long id, Long listName) throws NamingException, JMSException {
        doSolrDocumentAction(new CatalogXItem.DeleteSmartSearchMessage(id, listName),
                SolrDocumentAction.SMARTSEARCH_LIST_REMOVE);
    }
}
