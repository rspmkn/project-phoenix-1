package com.apd.phoenix.service.business;

import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import com.apd.phoenix.service.model.OrderStatus;
import com.apd.phoenix.service.model.OrderStatus.OrderStatusEnum;
import com.apd.phoenix.service.persistence.jpa.OrderStatusDao;

@Stateless
@LocalBean
public class OrderStatusBp extends AbstractBp<OrderStatus> {

    @Inject
    OrderStatusDao orderStatusDao;

    @Inject
    public void initDao(OrderStatusDao dao) {
        this.dao = dao;
    }

    public List<OrderStatus> getStatusList() {
        return orderStatusDao.getStatusList();
    }

    public OrderStatus getOrderStatusByValue(String value) {
        return orderStatusDao.getOrderStatusByValue(value);
    }

    public OrderStatus getOrderStatus(OrderStatusEnum status) {
        return this.getOrderStatusByValue(status.getValue());
    }

}
