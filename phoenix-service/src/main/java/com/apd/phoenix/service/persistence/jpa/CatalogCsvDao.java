package com.apd.phoenix.service.persistence.jpa;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.Query;
import com.apd.phoenix.service.model.Catalog;
import com.apd.phoenix.service.model.CatalogCsv;
import com.apd.phoenix.service.model.CatalogUpload;

@Stateless
public class CatalogCsvDao extends AbstractDao<CatalogCsv> {

    public boolean existsByCorrelationId(String correlationId) {
        String hql = "SELECT count(catalogCsv) FROM CatalogCsv catalogCsv WHERE catalogCsv.correlationId = :correlationId";
        Query query = entityManager.createQuery(hql);
        query.setParameter("correlationId", correlationId);
        Long results = (Long) query.getSingleResult();
        if (results != null && results > 0) {
            return true;
        }
        else {
            return false;
        }
    }

    public CatalogCsv getByCorrelationId(String correlationId) {
        String hql = "SELECT catalogCsv FROM CatalogCsv catalogCsv WHERE catalogCsv.correlationId = :correlationId";
        Query query = entityManager.createQuery(hql);
        query.setParameter("correlationId", correlationId);
        List<CatalogCsv> results = (List<CatalogCsv>) query.getResultList();
        if (results != null && !results.isEmpty()) {
            return results.get(0);
        }
        else {
            return null;
        }
    }

    public Long getTotalItems(String correlationId) {
        String hql = "SELECT catalogCsv.totalItems FROM CatalogCsv catalogCsv WHERE catalogCsv.correlationId = :correlationId";
        Query query = entityManager.createQuery(hql);
        query.setParameter("correlationId", correlationId);
        List<Long> results = (List<Long>) query.getResultList();
        if (results != null && !results.isEmpty()) {
            return results.get(0);
        }
        else {
            return null;
        }
    }

    public void removeRecords(String correlationId) {
        String hql = "DELETE FROM CatalogCsv WHERE correlationId = :correlationId";
        Query query = entityManager.createQuery(hql);
        query.setParameter("correlationId", correlationId);
        query.executeUpdate();
    }

    public List<String> getCorrelationId(long catalogId) {
        String hql = "SELECT catalogCsv.correlationId FROM CatalogCsv catalogCsv WHERE catalogCsv.catalogId = :catalogId";
        Query query = entityManager.createQuery(hql);
        query.setParameter("catalogId", catalogId);
        return (List<String>) query.getResultList();
    }

}
