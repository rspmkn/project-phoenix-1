package com.apd.phoenix.service.model.listener;

import java.util.Date;
import javax.persistence.PrePersist;
import com.apd.phoenix.service.model.PunchoutSession;

public class PunchoutSessionListener {

    @SuppressWarnings("static-method")
    @PrePersist
    public void setDate(PunchoutSession punchoutSession) {
        punchoutSession.setInitTime((new Date()));
    }
}
