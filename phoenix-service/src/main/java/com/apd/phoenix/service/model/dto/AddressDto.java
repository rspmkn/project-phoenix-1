package com.apd.phoenix.service.model.dto;

import java.io.Serializable;

public class AddressDto implements Serializable {

    private static final long serialVersionUID = 4010126251619639131L;

    private String companyName;

    private String name;

    private String line1;

    private String line2;

    private String city;

    private String state;

    private String zip;

    private String shipToId;

    private Boolean pendingShipToAuthorization = Boolean.FALSE;

    private String country;

    private String geoCode;

    private String county;

    private boolean primary;

    private String defaultUsscoAccountId;

    private String secondPassUsscoAccountId;

    private String thirdPassUsscoAccountId;

    private MiscShipToDto miscShipToDto;

    private PersonDto personDto;

    // private ContactDto contactDto;

    public String getLine1() {
        return line1;
    }

    public void setLine1(String line1) {
        this.line1 = line1;
    }

    public String getLine2() {
        return line2;
    }

    public void setLine2(String line2) {
        this.line2 = line2;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public String getShipToId() {
        return shipToId;
    }

    public void setShipToId(String shipToId) {
        this.shipToId = shipToId;
    }

    public Boolean getPendingShipToAuthorization() {
        return pendingShipToAuthorization;
    }

    public void setPendingShipToAuthorization(Boolean pendingShipToAuthorization) {
        this.pendingShipToAuthorization = pendingShipToAuthorization;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getGeoCode() {
        return geoCode;
    }

    public void setGeoCode(String geoCode) {
        this.geoCode = geoCode;
    }

    public String getCounty() {
        return county;
    }

    public void setCounty(String county) {
        this.county = county;
    }

    public boolean isPrimary() {
        return primary;
    }

    public void setPrimary(boolean primary) {
        this.primary = primary;
    }

    /**
     * @return the miscShipToDto
     */
    public MiscShipToDto getMiscShipToDto() {
        return miscShipToDto;
    }

    /**
     * @param miscShipToDto the miscShipToDto to set
     */
    public void setMiscShipToDto(MiscShipToDto miscShipToDto) {
        this.miscShipToDto = miscShipToDto;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDefaultUsscoAccountId() {
        return defaultUsscoAccountId;
    }

    public void setDefaultUsscoAccountId(String defaultUsscoAccountId) {
        this.defaultUsscoAccountId = defaultUsscoAccountId;
    }

    public AddressDto() {

    }

    public AddressDto(AddressDto toCopy) {
        city = toCopy.city;
        companyName = toCopy.companyName;
        country = toCopy.country;
        county = toCopy.county;
        geoCode = toCopy.geoCode;
        line1 = toCopy.line1;
        line2 = toCopy.line2;
        if (toCopy.miscShipToDto != null) {
            miscShipToDto = new MiscShipToDto(toCopy.miscShipToDto);
        }
        else {
            miscShipToDto = new MiscShipToDto();
        }
        name = toCopy.name;
        pendingShipToAuthorization = toCopy.pendingShipToAuthorization;
        primary = toCopy.primary;
        shipToId = toCopy.shipToId;
        state = toCopy.state;
        zip = toCopy.zip;
    }

    /**
     * @return the personDto
     */
    public PersonDto getPersonDto() {
        return personDto;
    }

    /**
     * @param personDto the personDto to set
     */
    public void setPersonDto(PersonDto personDto) {
        this.personDto = personDto;
    }

    //    public ContactDto getContactDto() {
    //		return contactDto;
    //	}
    //
    //	public void setContactDto(ContactDto contactDto) {
    //		this.contactDto = contactDto;
    //	}

    public String lookupCountryName(String isoCode) {
        for (CountryISOCode code : CountryISOCode.values()) {
            if (code.toString().equalsIgnoreCase(isoCode) || code.getValue().equalsIgnoreCase(isoCode)) {
                return code.getValue();
            }
        }
        return null;
    }

    public String getSecondPassUsscoAccountId() {
        return secondPassUsscoAccountId;
    }

    public void setSecondPassUsscoAccountId(String secondPassUsscoAccountId) {
        this.secondPassUsscoAccountId = secondPassUsscoAccountId;
    }

    public String getThirdPassUsscoAccountId() {
        return thirdPassUsscoAccountId;
    }

    public void setThirdPassUsscoAccountId(String thirdPassUsscoAccountId) {
        this.thirdPassUsscoAccountId = thirdPassUsscoAccountId;
    }

    private enum CountryISOCode {
        US("United States");

        private String value;

        CountryISOCode(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }

}
