package com.apd.phoenix.service.search.integration.client.backend;

import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Random;
import javax.annotation.PostConstruct;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.core.utility.TenantConfigRepository;
import com.apd.phoenix.service.persistence.multitenancy.impl.CurrentTenantIdentifierResolverImpl;
import com.apd.phoenix.service.search.integration.client.SmartSearchConstants;
import com.apd.phoenix.service.search.integration.client.backend.SmartSearchCatalogMaintenanceMessage.SmartSearchCatalogMaintenanceMessageContent;
import com.ussco.ws.ecatalog.catalog._1.CategoryType;
import com.ussco.ws.ecatalog.catalog._1.ECatalogMDInterface;
import com.ussco.ws.ecatalog.catalog._1.ECatalogMDService;
import com.ussco.ws.ecatalog.catalog._1.GetCategoryRequest;
import com.ussco.ws.ecatalog.catalog._1.GetCategoryResponse;
import com.ussco.ws.ecatalog.catalog._1.ImageType;
import com.ussco.ws.ecatalog.catalog._1.ItemIndicatorType;
import com.ussco.ws.ecatalog.catalog._1.MDCategoryRequestType;
import com.ussco.ws.ecatalog.catalog._1.MDCustomItemType;
import com.ussco.ws.ecatalog.catalog._1.MDItemRequestType;
import com.ussco.ws.ecatalog.catalog._1.MDListRequestType;
import com.ussco.ws.ecatalog.catalog._1.MDListType;
import com.ussco.ws.ecatalog.catalog._1.MDListType.ListItem;
import com.ussco.ws.ecatalog.catalog._1.RemoveItemListRequest;
import com.ussco.ws.ecatalog.catalog._1.RemoveItemListResponse;
import com.ussco.ws.ecatalog.catalog._1.UpdateItemListRequest;
import com.ussco.ws.ecatalog.catalog._1.UpdateItemListResponse;
import com.ussco.ws.ecatalog.catalog._1.UpdateItemRequest;
import com.ussco.ws.ecatalog.catalog._1.UpdateItemResponse;

@Stateless
@LocalBean
public class SmartSearchCatalogMaintenanceService {

    private static final int KEYWORDS_MAX_LENGTH = 1000;

    private static final int UOM_MAX_LENGTH = 10;

    private static final int DESCRIPTION_MAX_LENGTH = 100;

    private static final BigInteger CATEGORY_MENU_DEPTH = new BigInteger("3");

    private static final Logger logger = LoggerFactory.getLogger(SmartSearchCatalogMaintenanceService.class);

    private static final Properties commonProps = TenantConfigRepository.getInstance().getProperties("smart.search");
    //Make these properties tenant-specific, as part of MULTI-106
    private static final String serviceUrl = (String) commonProps.getProperty("serviceUrlMasterData");

    public void updateItems(Map<String, Map<Long, SmartSearchCatalogMaintenanceMessageContent>> catalogXItems) {

        ECatalogMDInterface port = this.createTenantSpecificConnectionToSS();

        //TODO: add caching here, per PHOEN-5248
        GetCategoryResponse categories = null;

        for (Map.Entry<String, Map<Long, SmartSearchCatalogMaintenanceMessageContent>> entry : catalogXItems.entrySet()) {

            Map<Long, SmartSearchCatalogMaintenanceMessageContent> catalogXItemIds = entry.getValue();

            UpdateItemRequest updateItemRequest = new UpdateItemRequest();
            updateItemRequest.setItemRequest(new MDItemRequestType());

            for (Map.Entry<Long, SmartSearchCatalogMaintenanceMessageContent> entry1 : catalogXItemIds.entrySet()) {
                SmartSearchCatalogMaintenanceMessageContent itemProperties = entry1.getValue();

                if (itemProperties == null || !itemProperties.getUsscoCatalog()) {
                    if (categories == null) {
                        categories = this.getCategories("catalog-maintenance", "system", null);
                    }
                    updateItemRequest.getItemRequest().getItem().add(createCustomItem(itemProperties, categories));
                }
                else {
                    updateItemRequest.getItemRequest().getItem().add(augmentItem(itemProperties));
                }

            }

            if (updateItemRequest.getItemRequest().getItem().size() > 0) {
                UpdateItemResponse updateItemResponse = port.updateItem(updateItemRequest);
                logger.info("Custom/Augmented Items StatusCode {}", updateItemResponse.getItemResponse()
                        .getResultStatus().getStatusCode());
                logger.info("Custom/Augmented Items StatusMessage {}", updateItemResponse.getItemResponse()
                        .getResultStatus().getStatusMessage());
            }
        }

    }

    private MDCustomItemType augmentItem(SmartSearchCatalogMaintenanceMessageContent itemProperties) {

        MDCustomItemType mdCustomItemType = new MDCustomItemType();
        String apdSku = itemProperties.getApdSku();
        //note: the dealer item number is the APD SKU, followed by a delimeter, followed by vendor name
        //this is to ensure that dealer item numbers are unique 
        mdCustomItemType.setDealerItemNumber(getDealerPartNumber(apdSku, itemProperties.getVendorName()));
        mdCustomItemType.setItemNumber(itemProperties.getVendorSku());
        String smartSearchKeywords = apdSku + " " + itemProperties.getSmartSearchKeywords();
        mdCustomItemType.setDealerKeywords(smartSearchKeywords);
        String description = itemProperties.getDealerDescription();
        if (StringUtils.isNotBlank(description)) {
            mdCustomItemType.setDealerDescription(description.substring(0, Math.min(description.length(),
                    DESCRIPTION_MAX_LENGTH)));
        }
        return mdCustomItemType;
    }

    private MDCustomItemType createCustomItem(SmartSearchCatalogMaintenanceMessageContent itemProperties,
            GetCategoryResponse categories) {
        String vendor = itemProperties.getVendorName();

        MDCustomItemType mdCustomItemType = new MDCustomItemType();
        mdCustomItemType.setUpdateStyle("Replace");
        String apdSku = itemProperties.getApdSku();
        //note: the dealer item number is the APD SKU, followed by a delimeter, followed by vendor name
        //this is to ensure that dealer item numbers are unique 
        mdCustomItemType.setDealerItemNumber(getDealerPartNumber(apdSku, vendor));
        String description = itemProperties.getName();
        if (StringUtils.isNotBlank(description)) {
            mdCustomItemType.setDealerDescription(description.substring(0, Math.min(description.length(),
                    DESCRIPTION_MAX_LENGTH)));
        }
        String unitOfMeasure = itemProperties.getUomName();
        if (StringUtils.isNotBlank(unitOfMeasure)) {
            mdCustomItemType.setDealerUnitOfMeasure(unitOfMeasure.substring(0, Math.min(unitOfMeasure.length(),
                    UOM_MAX_LENGTH)));
        }
        String searchTerms = apdSku + " " + itemProperties.getSearchTerms() + " "
                + itemProperties.getSmartSearchKeywords();
        if (StringUtils.isNotBlank(searchTerms)) {
            mdCustomItemType.setDealerKeywords(searchTerms.substring(0, Math.min(searchTerms.length(),
                    KEYWORDS_MAX_LENGTH)));
        }
        if (itemProperties.getItemStatus() != null && "AVAILABLE".equalsIgnoreCase(itemProperties.getItemStatus())) {
            mdCustomItemType.setStockedItem("Y");
        }

        if (StringUtils.isNotBlank(vendor)) {
            ItemIndicatorType vendorItemIndicator = new ItemIndicatorType();
            vendorItemIndicator.setIndicatorType(SmartSearchConstants.VENDOR_NAME_SMARTSEARCH_INDICATOR);
            vendorItemIndicator.setIndicatorValue(vendor);
            mdCustomItemType.getItemIndicator().add(vendorItemIndicator);
        }

        String hierarchyName = itemProperties.getHierarchyDescription();
        String categoryId = null;

        if (categories != null && categories.getCategoryResponse() != null) {
            categoryId = this.getCategoryId(hierarchyName, categories.getCategoryResponse().getCategory());
        }

        mdCustomItemType.setCategoryId(categoryId);
        Random rand = new Random();
        int posRandInt = rand.nextInt(100000) + 1;
        mdCustomItemType.setSequence(posRandInt);
        String imageUrl = itemProperties.getImageUrl();
        if (StringUtils.isNotBlank(imageUrl)) {
            ImageType imageType = new ImageType();
            imageType.setImageURL(imageUrl);
            imageType.setImageType("DealerA");
            imageType.setImageHeight(new BigInteger("100"));
            imageType.setImageWidth(new BigInteger("100"));
            imageType.setImageTitle("Image");
            mdCustomItemType.getItemImage().add(imageType);
        }
        return mdCustomItemType;

    }

    private String getCategoryId(String hierarchyName, List<CategoryType> categoryList) {
        if (categoryList == null || categoryList.isEmpty() || StringUtils.isBlank(hierarchyName)) {
            return null;
        }
        for (CategoryType category : categoryList) {
            if (category != null) {
                if (hierarchyName.equalsIgnoreCase(category.getCategoryDescription())) {
                    return category.getCategoryId();
                }
                String idFromSubcategories = this.getCategoryId(hierarchyName, category.getCategory());
                if (StringUtils.isNotBlank(idFromSubcategories)) {
                    return idFromSubcategories;
                }
            }
        }
        return null;
    }

    /**
     * This method takes a set of smart search updates, inserts the items into lists, and ensures that the core
     * items are correct.
     * 
     * @param catalogXItems
     */
    public void insertItems(Map<String, Map<Long, SmartSearchCatalogMaintenanceMessageContent>> catalogXItems) {

    	ECatalogMDInterface port = this.createTenantSpecificConnectionToSS();
    	
        for (Map.Entry<String, Map<Long, SmartSearchCatalogMaintenanceMessageContent>> entry : catalogXItems.entrySet()) {
            String listName = entry.getKey();
            Map<Long, SmartSearchCatalogMaintenanceMessageContent> catalogXItemIds = entry.getValue();
            Map<Long, SmartSearchCatalogMaintenanceMessageContent> nonCoreItems = new HashMap<>();

            UpdateItemListRequest updateListRequest = new UpdateItemListRequest();
            updateListRequest.setItemListRequest(new MDListRequestType());

            MDListType list = new MDListType();
            list.setListName(listName);
            list.setUpdateStyle("Insert");

            MDListType coreList = new MDListType();
            coreList.setUpdateStyle("Insert");

            for (Map.Entry<Long, SmartSearchCatalogMaintenanceMessageContent> entry1 : catalogXItemIds.entrySet()) {
                SmartSearchCatalogMaintenanceMessageContent itemProperties = entry1.getValue();

                //each entry with the list name will have the same core list, so just pulls the core list 
                //name from the first item
                if (StringUtils.isBlank(coreList.getListName())) {
                	coreList.setListName(itemProperties.getCoreListName() + "");
                }

                ListItem listItem = createListItem(itemProperties);

                list.getListItem().add(listItem);

                Timestamp coreExpirationDate = itemProperties.getCoreItemExpirationDate();
                Timestamp coreStartDate = itemProperties.getCoreItemStartDate();
                Calendar coreExpirationCalendar = null;
                Calendar coreStartCalendar = null;
                if (coreExpirationDate != null) {
                    coreExpirationCalendar = org.apache.commons.lang.time.DateUtils.toCalendar(coreExpirationDate);
                }
                if (coreStartDate != null) {
                    coreStartCalendar = org.apache.commons.lang.time.DateUtils.toCalendar(coreStartDate);
                }
                if ((coreExpirationCalendar != null || coreStartCalendar != null)
                        && (coreStartCalendar == null || Calendar.getInstance().after(coreStartCalendar))
                        && (coreExpirationCalendar == null || Calendar.getInstance().before(coreExpirationCalendar))) {
                    coreList.getListItem().add(listItem);
                } else {
                	nonCoreItems.put(entry1.getKey(), itemProperties);
                }

            }

            updateListRequest.getItemListRequest().setList(list);
            UpdateItemListResponse updateListResponse = port.updateItemList(updateListRequest);
            logger.info("Update List StatusCode {}", updateListResponse.getItemListResponse().getResultStatus()
                    .getStatusCode());
            logger.info("Update List StatusMessage {}", updateListResponse.getItemListResponse().getResultStatus()
                    .getStatusMessage());
            if (!coreList.getListItem().isEmpty()) {
                logger.info("Updating core list");
                updateListRequest.getItemListRequest().setList(coreList);
                UpdateItemListResponse updateCoreListResponse = port.updateItemList(updateListRequest);
                logger.info("Core List Items StatusCode {}", updateCoreListResponse.getItemListResponse()
                        .getResultStatus().getStatusCode());
                logger.info("Core ListItems StatusMessage {}", updateCoreListResponse.getItemListResponse()
                        .getResultStatus().getStatusMessage());
            }

            if (StringUtils.isNotBlank(coreList.getListName())) {
	            Map<String, Map<Long, SmartSearchCatalogMaintenanceMessageContent>> nonCoreToRemove = new HashMap<>();
	            nonCoreToRemove.put(coreList.getListName(), nonCoreItems);
	            removeItems(nonCoreToRemove);
            }
        }
    }

    private ListItem createListItem(SmartSearchCatalogMaintenanceMessageContent itemProperties) {
        ListItem listItem = new ListItem();
        //Only one item number can be specified to lookup the item
        if (itemProperties == null || !itemProperties.getUsscoCatalog()) {
            //note: the dealer item number is the APD SKU, followed by a delimeter, followed by vendor name
            //this is to ensure that dealer item numbers are unique 
            listItem
                    .setDealerItemNumber(getDealerPartNumber(itemProperties.getApdSku(), itemProperties.getVendorName()));
        }
        else {
            //set vendor item number to uniquely identify item for smartSearch
            listItem.setItemNumber(itemProperties.getVendorSku());
        }

        return listItem;
    }

    public void removeItems(Map<String, Map<Long, SmartSearchCatalogMaintenanceMessageContent>> catalogXItems) {

        ECatalogMDInterface port = this.createTenantSpecificConnectionToSS();

        for (Map.Entry<String, Map<Long, SmartSearchCatalogMaintenanceMessageContent>> entry : catalogXItems.entrySet()) {
            String listName = entry.getKey();
            Map<Long, SmartSearchCatalogMaintenanceMessageContent> catalogXItemIds = entry.getValue();
            RemoveItemListRequest removeItemListRequest = new RemoveItemListRequest();
            removeItemListRequest.setItemListRequest(new MDListRequestType());

            MDListType list = new MDListType();
            list.setListName(listName);

            list.setUpdateStyle("Insert");

            for (Map.Entry<Long, SmartSearchCatalogMaintenanceMessageContent> entry1 : catalogXItemIds.entrySet()) {
                SmartSearchCatalogMaintenanceMessageContent itemProperties = entry1.getValue();
                ListItem listItem = new ListItem();
                if (itemProperties == null || !(Boolean) itemProperties.getUsscoCatalog()) {
                    //note: the dealer item number is the APD SKU, followed by a delimeter, followed by vendor name
                    //this is to ensure that dealer item numbers are unique 
                    listItem.setDealerItemNumber(getDealerPartNumber(itemProperties.getApdSku(), itemProperties
                            .getVendorName()));
                }
                else {
                    listItem.setItemNumber(itemProperties.getVendorSku());
                }
                list.getListItem().add(listItem);
            }
            removeItemListRequest.getItemListRequest().setList(list);
            RemoveItemListResponse removeItemListResponse = port.removeItemList(removeItemListRequest);
            logger.info("removeItems StatusCode {}", removeItemListResponse.getItemListResponse().getResultStatus()
                    .getStatusCode());
            logger.info("removeItems StatusMessage {}", removeItemListResponse.getItemListResponse().getResultStatus()
                    .getStatusMessage());
        }

    }

    public static String getDealerPartNumber(String apdSku, String vendorName) {
        return apdSku + SmartSearchConstants.DEALER_KEYWORD_DELIMETER + StringUtils.upperCase(vendorName);
    }

    public GetCategoryResponse getCategories(String sessionId, String userAgent, String listKey) {
        ECatalogMDInterface port = this.createTenantSpecificConnectionToSS();
        GetCategoryRequest categoryRequest = this.buildEmptyGetCategoryObject(sessionId);
        return port.getCategory(categoryRequest);
    }

    private GetCategoryRequest buildEmptyGetCategoryObject(String sessionId) {
        if (StringUtils.isEmpty(sessionId)) {
            logger.error("Session id is null, will not be able to reach smartSearch");
        }
        GetCategoryRequest categoryRequest = new GetCategoryRequest();
        categoryRequest.setCategoryRequest(new MDCategoryRequestType());
        categoryRequest.getCategoryRequest().setCategoryDepth(CATEGORY_MENU_DEPTH);
        return categoryRequest;
    }

    private ECatalogMDInterface createTenantSpecificConnectionToSS() {
        ECatalogMDService ss = new ECatalogMDService();
        String username = TenantConfigRepository.getInstance().getProperty(
                CurrentTenantIdentifierResolverImpl.getCurrentTenant(), "tenant", "smart_search_user");
        String password = TenantConfigRepository.getInstance().getProperty(
                CurrentTenantIdentifierResolverImpl.getCurrentTenant(), "tenant", "smart_search_password");
        ECatalogMDInterface port = ss.getECatalogMDService(serviceUrl, username, password);
        return port;
    }

}
