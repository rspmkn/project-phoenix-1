/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.service.persistence.jpa;

import com.apd.phoenix.service.model.ItemXItemPropertyType;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;

/**
 *
 * @author nreidelb
 */
@Stateless
@LocalBean
public class ItemXItemPropertyTypeDao extends AbstractDao<ItemXItemPropertyType> {

}
