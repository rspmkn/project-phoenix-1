package com.apd.phoenix.service.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.MathContext;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Version;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.Index;

@Entity
public class LineItemXShipment implements Serializable, com.apd.phoenix.service.model.Entity {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id = null;
    @Version
    @Column(name = "version", nullable = false)
    private int version = 0;

    @ManyToOne(fetch = FetchType.EAGER)
    @Fetch(FetchMode.JOIN)
    @Index(name = "LIXS_LINEITEM_IDX")
    private LineItem lineItem;

    @ManyToOne(fetch = FetchType.LAZY)
    @Index(name = "LIXS_SHIPMENT_IDX")
    private Shipment shipments;

    @Column(nullable = false)
    private BigInteger quantity;

    @Column
    private Boolean paid;

    @Column
    private BigDecimal shipping;

    @OneToMany(mappedBy = "lineItemXShipment", fetch = FetchType.EAGER)
    private Set<LineItemXShipmentXTaxType> taxes = new HashSet<LineItemXShipmentXTaxType>();

    //this column is used to track payment IDs, when there is a timeout while charging split charges
    @Column
    private String ccTransactionId;

    @ElementCollection
    @CollectionTable(name = "liXShipment_vendorComments", joinColumns = @JoinColumn(name = "liXShipment_id"))
    @Column(name = "vendorComment")
    private List<String> vendorComments;

    public Long getId() {
        return this.id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public int getVersion() {
        return this.version;
    }

    public void setVersion(final int version) {
        this.version = version;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        if (id != null) {
            return id.equals(((LineItemXShipment) that).id);
        }
        return super.equals(that);
    }

    @Override
    public int hashCode() {
        if (id != null) {
            return id.hashCode();
        }
        return super.hashCode();
    }

    public LineItem getLineItem() {
        return this.lineItem;
    }

    public void setLineItem(final LineItem lineItem) {
        this.lineItem = lineItem;
    }

    public Shipment getShipments() {
        return this.shipments;
    }

    public void setShipments(final Shipment shipments) {
        this.shipments = shipments;
    }

    public BigInteger getQuantity() {
        return this.quantity;
    }

    public void setQuantity(final BigInteger quantity) {
        this.quantity = quantity;
    }

    /**
     * 
     * @return the shipping. If shipping is null, calculates it based on the appropriate default
     */
    public BigDecimal getShipping() {
        if (this.shipping == null) {
            this.shipping = this.getDefaultShippingAmount();
        }
        return shipping;
    }

    public void setShipping(BigDecimal shipping) {
        this.shipping = shipping;
    }

    @Override
    public String toString() {
        String result = getClass().getSimpleName() + " ";
        result += "quantity: " + quantity;
        return result;
    }

    public Set<LineItemXShipmentXTaxType> getTaxes() {
        return taxes;
    }

    public void setTaxes(Set<LineItemXShipmentXTaxType> taxes) {
        this.taxes = taxes;
    }

    public String getCcTransactionId() {
        return ccTransactionId;
    }

    public void setCcTransactionId(String ccTransactionId) {
        this.ccTransactionId = ccTransactionId;
    }

    /**
     * @return the vendorComments
     */
    public List<String> getVendorComments() {
        return vendorComments;
    }

    /**
     * @param vendorComments the vendorComments to set
     */
    public void setVendorComments(List<String> vendorComments) {
        this.vendorComments = vendorComments;
    }

    public Boolean getPaid() {
        return paid;
    }

    public void setPaid(Boolean paid) {
        this.paid = paid;
    }

    /**
     * Note: this method returns the default shipping amount, without setting any values on this entity.
     * 
     * @return
     */
    @Deprecated
    public BigDecimal getTotalFreightAmount() {
        return this.getDefaultShippingAmount();
    }

    private BigDecimal getDefaultShippingAmount() {
        if (quantity.compareTo(BigInteger.valueOf(lineItem.getQuantity())) == 0) {
            return lineItem.getEstimatedShippingAmount();
        }
        else {
            BigDecimal originalQty = new BigDecimal(lineItem.getQuantity());
            BigDecimal qtyFraction = new BigDecimal(quantity).divide(originalQty, MathContext.DECIMAL128);
            BigDecimal estimatedShippingAmount = lineItem.getEstimatedShippingAmount().multiply(qtyFraction);
            return estimatedShippingAmount;
        }
    }

    public BigDecimal getTotalTaxAmount() {
        BigDecimal sum = BigDecimal.ZERO;
        for (LineItemXShipmentXTaxType tax : this.getTaxes()) {
            sum = sum.add(tax.getValue());
        }
        return sum;
    }

    public BigDecimal getSubTotalAmount() {
        if (quantity.compareTo(BigInteger.valueOf(lineItem.getQuantity())) == 0) {
            return lineItem.getTotalPrice();
        }
        else {
            BigDecimal subTotalAmount = lineItem.getUnitPrice().multiply(new BigDecimal(quantity));
            return subTotalAmount;
        }
    }

    public BigDecimal getTotalAmount() {
        BigDecimal totalShippingAmount = getShipping();
        BigDecimal totalTaxAmount = getTotalTaxAmount();
        BigDecimal subTotalAmount = getSubTotalAmount();
        BigDecimal totalAmount = totalShippingAmount.add(totalTaxAmount).add(subTotalAmount);
        return totalAmount;
    }
}
