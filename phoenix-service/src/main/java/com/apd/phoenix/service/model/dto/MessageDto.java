package com.apd.phoenix.service.model.dto;

import java.io.Serializable;

public class MessageDto implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -286762557983510523L;

    MessageMetadataDto messageMetadataDto;

    String content;

    private boolean isBase64Encoded = false;

    public MessageMetadataDto getMessageMetadataDto() {
        return messageMetadataDto;
    }

    public void setMessageMetadataDto(MessageMetadataDto messageMetadataDto) {
        this.messageMetadataDto = messageMetadataDto;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public boolean isBase64Encoded() {
        return isBase64Encoded;
    }

    public void setBase64Encoded(boolean isBase64Encoded) {
        this.isBase64Encoded = isBase64Encoded;
    }

}
