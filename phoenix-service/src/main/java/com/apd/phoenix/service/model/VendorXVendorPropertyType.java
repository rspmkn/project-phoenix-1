package com.apd.phoenix.service.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Version;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.envers.Audited;

@Entity
@Audited
public class VendorXVendorPropertyType implements Serializable, com.apd.phoenix.service.model.Entity {

    private static final long serialVersionUID = 1189381103132860019L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id = null;
    @Version
    @Column(name = "version", nullable = false)
    private int version = 0;

    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    @Fetch(FetchMode.JOIN)
    private VendorPropertyType type;

    //   @ManyToOne(fetch = FetchType.EAGER, optional = false)
    //   @Fetch(FetchMode.JOIN)
    //   private Vendor vendor;
    //   
    //   public Vendor getVendor() {
    //	return vendor;
    //}
    //
    //public void setVendor(Vendor vendor) {
    //	this.vendor = vendor;
    //}

    @Column
    private String value;

    public Long getId() {
        return this.id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public int getVersion() {
        return this.version;
    }

    public void setVersion(final int version) {
        this.version = version;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        if (id != null) {
            return id.equals(((VendorXVendorPropertyType) that).id);
        }
        return super.equals(that);
    }

    @Override
    public int hashCode() {
        if (id != null) {
            return id.hashCode();
        }
        return super.hashCode();
    }

    public VendorPropertyType getType() {
        return this.type;
    }

    public void setType(final VendorPropertyType type) {
        this.type = type;
    }

    public String getValue() {
        return this.value;
    }

    public void setValue(final String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        String result = getClass().getSimpleName() + " ";
        if (value != null && !value.trim().isEmpty())
            result += "value: " + value;
        return result;
    }
}