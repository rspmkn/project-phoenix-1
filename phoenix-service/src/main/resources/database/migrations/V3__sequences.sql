
CREATE TABLE `stockcheck` (
    `ID` bigint(20) NOT NULL AUTO_INCREMENT,
    `NAME` varchar(255) NOT NULL,
    PRIMARY KEY (`ID`) 
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `transactionKey` (
    `ID` bigint(20) NOT NULL AUTO_INCREMENT,
    `NAME` varchar(255) NOT NULL,
    PRIMARY KEY (`ID`) 
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `transactionControlId` (
    `ID` bigint(20) NOT NULL AUTO_INCREMENT,
    `NAME` varchar(255) NOT NULL,
    PRIMARY KEY (`ID`) 
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `groupControlId` (
    `ID` bigint(20) NOT NULL AUTO_INCREMENT,
    `NAME` varchar(255) NOT NULL,
    PRIMARY KEY (`ID`) 
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `interchangeControlId` (
    `ID` bigint(20) NOT NULL AUTO_INCREMENT,
    `NAME` varchar(255) NOT NULL,
    PRIMARY KEY (`ID`) 
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `apdPo` (
    `ID` bigint(20) NOT NULL AUTO_INCREMENT,
    `NAME` varchar(255) NOT NULL,
    PRIMARY KEY (`ID`) 
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `ticketNum` (
    `ID` bigint(20) NOT NULL AUTO_INCREMENT,
    `NAME` varchar(255) NOT NULL,
    PRIMARY KEY (`ID`) 
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
