	CREATE TABLE IF NOT EXISTS InventoryBinType (
         id bigint not null auto_increment,
          binName varchar(255),
          defaultBin boolean,
          description varchar(255),
          inventoryBinTypeID bigint not null,
          primary key (id)
    );
    
        CREATE TABLE IF NOT EXISTS InventorySite (
        id BIGINT NOT NULL AUTO_INCREMENT,
        attention VARCHAR(255),
        city VARCHAR(255),
        country VARCHAR(255),
        fax BIGINT,
        line1 VARCHAR(255),
        line2 VARCHAR(255),
        phone BIGINT,
        receivingHours VARCHAR(255),
        siteName VARCHAR(255),
        state VARCHAR(255),
        zip VARCHAR(255),
        PRIMARY KEY (id)
    );
    
    CREATE TABLE IF NOT EXISTS InventoryLocation (
        id BIGINT NOT NULL AUTO_INCREMENT,
        defaultLocation VARCHAR(255),
        locationName VARCHAR(255),
        inventorySite_id BIGINT NOT NULL,
        PRIMARY KEY (id)
    );

    CREATE TABLE IF NOT EXISTS InventoryBin (
        id BIGINT NOT NULL AUTO_INCREMENT,
        onHandQuantity BIGINT,
        inventoryBinType_id BIGINT NOT NULL,
        inventoryLocation_id BIGINT NOT NULL,
        inventoryitem_id BIGINT NOT NULL,
        PRIMARY KEY (id)
    );
     
	
   CREATE TABLE IF NOT EXISTS SerialNumber (
    id BIGINT NOT NULL AUTO_INCREMENT,
    serialNumber VARCHAR(255),
    inventoryItem_id BIGINT NOT NULL,
    PRIMARY KEY (id)
	);
	
	ALTER TABLE Inventoryitem
 	DROP COLUMN beginningQuantity,
 	DROP COLUMN customerSku,
 	DROP COLUMN endingQuantity;
 	
	ALTER TABLE `Inventoryitem` 	
	ADD COLUMN `vendorSku` VARCHAR(255),
	ADD COLUMN `upc` VARCHAR(255),
	ADD COLUMN `expiry` DATETIME,
	ADD COLUMN `lot` BIGINT,
	ADD COLUMN `onHandQty` BIGINT,
	ADD COLUMN `item_id` BIGINT NOT NULL;
	       
    ALTER TABLE `inventorytransaction` 
    ADD COLUMN `fromBinOnHandQty` BIGINT,
    ADD COLUMN `quantity` BIGINT,
    ADD COLUMN `shipmentNumber` VARCHAR(255),
    ADD COLUMN `returnAuthorization` VARCHAR(255),
    ADD COLUMN `user` VARCHAR(50),
    ADD COLUMN `comments` VARCHAR(255),
    ADD COLUMN `fromBinType_id` BIGINT NOT NULL,
    ADD COLUMN `toBinType_id` BIGINT NOT NULL;
    
    ALTER TABLE InventoryLocation 
        ADD INDEX FK4EED41118AF45977 (inventorySite_id), 
        ADD CONSTRAINT FK4EED41118AF45977 
        FOREIGN KEY (inventorySite_id) 
        REFERENCES InventorySite (id);
    
    ALTER TABLE InventoryBin 
    ADD INDEX FKA3C3722B8BE735F7 (inventoryitem_id), 
    ADD CONSTRAINT FKA3C3722B8BE735F7 
    FOREIGN KEY (inventoryitem_id) 
    REFERENCES InventoryItem (id);

    ALTER TABLE InventoryBin 
        ADD INDEX FKA3C3722BADFE8437 (inventoryLocation_id), 
        ADD CONSTRAINT FKA3C3722BADFE8437 
        FOREIGN KEY (inventoryLocation_id) 
        REFERENCES InventoryLocation (id);

 	alter table InventoryBin 
        add index FKA3C3722B63AB2FFD (inventoryBinType_id), 
        add constraint FKA3C3722B63AB2FFD 
        foreign key (inventoryBinType_id) 
        references InventoryBinType (id);

    ALTER TABLE InventoryItem 
        ADD INDEX FKD4AE2A6FD4B669BD (item_id), 
        ADD CONSTRAINT FKD4AE2A6FD4B669BD 
        FOREIGN KEY (item_id) 
        REFERENCES Item (id);

    alter table InventoryTransaction 
        add index FK2A3D6402441E8A5C (toBinType_id), 
        add constraint FK2A3D6402441E8A5C 
        foreign key (toBinType_id) 
        references InventoryBinType (id);

    alter table InventoryTransaction 
        add index FK2A3D6402289424CB (fromBinType_id), 
        add constraint FK2A3D6402289424CB 
        foreign key (fromBinType_id) 
        references InventoryBinType (id);
        
  	alter table SerialNumber 
          add index FKEC9D27D8BE735F7 (inventoryItem_id), 
          add constraint FKEC9D27D8BE735F7 
          foreign key (inventoryItem_id) 
          references InventoryItem (id);


 