
    create table AccountLineage (
        id bigint not null auto_increment,
        priority integer,
        version integer not null,
        account_id bigint,
        ancestorAccount_id bigint,
        primary key (id)
    ) ;

    create table AccountLineage_AUD (
        id bigint not null,
        REV bigint not null,
        REVTYPE tinyint,
        priority integer,
        account_id bigint,
        ancestorAccount_id bigint,
        primary key (id, REV)
    ) ;

    create index ACCOUNT_LINEAGE_IDX on AccountLineage (priority);

    alter table AccountLineage 
        add index FKF99CD1BE1976C0EA (ancestorAccount_id), 
        add constraint FKF99CD1BE1976C0EA 
        foreign key (ancestorAccount_id) 
        references Account (id);

    alter table AccountLineage 
        add index FKF99CD1BE185A22D7 (account_id), 
        add constraint FKF99CD1BE185A22D7 
        foreign key (account_id) 
        references Account (id);

    alter table AccountLineage_AUD 
        add index FK576DF08F75D1432 (REV), 
        add constraint FK576DF08F75D1432 
        foreign key (REV) 
        references REVINFO (REV);

/* Inserts account lineages into all accounts that are children of root accounts */
INSERT INTO ACCOUNTLINEAGE (PRIORITY, VERSION, ACCOUNT_ID, ANCESTORACCOUNT_ID) 
	SELECT 1, 0, THISACCOUNT.ID, THISACCOUNT.PARENTACCOUNT_ID 
			FROM ACCOUNT THISACCOUNT WHERE THISACCOUNT.ROOTACCOUNT_ID = THISACCOUNT.PARENTACCOUNT_ID;

DELIMITER $$
 
CREATE PROCEDURE create_account_lineages()
 BEGIN
 DECLARE previousCount  INT;
 DECLARE thisCount  INT;
 
 SET previousCount = 0;
 SET thisCount = (SELECT count(*) FROM ACCOUNTLINEAGE);
 
 WHILE previousCount < thisCount DO
 INSERT INTO ACCOUNTLINEAGE (PRIORITY, VERSION, ACCOUNT_ID, ANCESTORACCOUNT_ID) SELECT JOINEDLINEAGE.PRIORITY, 0, THISACCOUNT.ID, JOINEDLINEAGE.ANCESTORACCOUNT_ID FROM ACCOUNT THISACCOUNT JOIN ACCOUNTLINEAGE JOINEDLINEAGE ON JOINEDLINEAGE.ACCOUNT_ID = THISACCOUNT.PARENTACCOUNT_ID WHERE THISACCOUNT.ID NOT IN (SELECT ACCOUNT_ID FROM ACCOUNTLINEAGE);
 INSERT INTO ACCOUNTLINEAGE (PRIORITY, VERSION, ACCOUNT_ID, ANCESTORACCOUNT_ID) SELECT MAX(JOINEDLINEAGE.PRIORITY) + 1, 0, THISACCOUNT.ID, THISACCOUNT.PARENTACCOUNT_ID FROM ACCOUNT THISACCOUNT JOIN ACCOUNTLINEAGE JOINEDLINEAGE ON JOINEDLINEAGE.ACCOUNT_ID = THISACCOUNT.ID WHERE NOT EXISTS (SELECT ID FROM ACCOUNTLINEAGE WHERE ACCOUNTLINEAGE.ANCESTORACCOUNT_ID = THISACCOUNT.PARENTACCOUNT_ID) GROUP BY THISACCOUNT.ID, THISACCOUNT.PARENTACCOUNT_ID;
 SET previousCount = thisCount;
 SET thisCount := (SELECT count(*) FROM ACCOUNTLINEAGE);
 SELECT previousCount;
 SELECT thisCount;
 END WHILE;
 
 END$$
 
DELIMITER ;

CALL create_account_lineages();

