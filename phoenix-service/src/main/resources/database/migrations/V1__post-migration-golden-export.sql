-- MySQL dump 10.13  Distrib 5.6.19, for Linux (x86_64)
--
-- Host: apdebizdev-mysql.cyortqtf1ptj.us-east-1.rds.amazonaws.com    Database: empty_tenant
-- ------------------------------------------------------
-- Server version	5.6.27-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `account`
--

DROP TABLE IF EXISTS `account`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `account` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `APDASSIGNEDACCOUNTID` varchar(255) DEFAULT NULL,
  `CREATIONDATE` datetime DEFAULT NULL,
  `ISACTIVE` decimal(1,0) NOT NULL,
  `NAME` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `VERSION` bigint(20) DEFAULT NULL,
  `PARENTACCOUNT_ID` bigint(20) DEFAULT NULL,
  `PAYMENTINFORMATION_ID` bigint(20) DEFAULT NULL,
  `PRIMARYCONTACT_ID` bigint(20) DEFAULT NULL,
  `ROOTACCOUNT_ID` bigint(20) DEFAULT NULL,
  `CXMLCONFIGURATION_ID` bigint(20) DEFAULT NULL,
  `CSRPHONE` varchar(255) DEFAULT NULL,
  `CSREMAIL` varchar(255) DEFAULT NULL,
  `SOLOMONCUSTOMERID` varchar(255) NOT NULL,
  `USELOCATIONCODE` decimal(1,0) NOT NULL DEFAULT '0',
  `LOCATIONCODE` varchar(255) DEFAULT NULL,
  `SHOWGLOBALBULLETIN` decimal(1,0) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UC_ACCOUNT_1` (`NAME`),
  KEY `INDX_1` (`PARENTACCOUNT_ID`) USING BTREE,
  KEY `INDX_2` (`ROOTACCOUNT_ID`) USING BTREE,
  KEY `INDX_3` (`PAYMENTINFORMATION_ID`) USING BTREE,
  KEY `INDX_4` (`PRIMARYCONTACT_ID`) USING BTREE,
  KEY `USPS_IDX_3` (`APDASSIGNEDACCOUNTID`) USING BTREE,
  KEY `FK1D0C220DD0B0DA57` (`CXMLCONFIGURATION_ID`),
  CONSTRAINT `FK1D0C220D46F7094` FOREIGN KEY (`PRIMARYCONTACT_ID`) REFERENCES `person` (`ID`),
  CONSTRAINT `FK1D0C220D48B9DF01` FOREIGN KEY (`PARENTACCOUNT_ID`) REFERENCES `account` (`ID`),
  CONSTRAINT `FK1D0C220D7ED1037D` FOREIGN KEY (`PAYMENTINFORMATION_ID`) REFERENCES `paymentinformation` (`ID`),
  CONSTRAINT `FK1D0C220DD0B0DA57` FOREIGN KEY (`CXMLCONFIGURATION_ID`) REFERENCES `cxmlconfiguration` (`ID`),
  CONSTRAINT `FK1D0C220DE5957139` FOREIGN KEY (`ROOTACCOUNT_ID`) REFERENCES `account` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=6349716 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `account`
--

LOCK TABLES `account` WRITE;
/*!40000 ALTER TABLE `account` DISABLE KEYS */;
INSERT INTO `account` VALUES (55801,NULL,'2013-09-10 00:00:00',1,'American Product Distributors, Inc.',5,NULL,39301884,NULL,NULL,NULL,NULL,NULL,'000000',0,NULL,1),(60071,NULL,'2013-09-11 00:00:00',1,'APD Office Administration',2,55801,NULL,NULL,55801,NULL,NULL,NULL,'000000',0,NULL,1),(720323,NULL,'2013-09-26 00:00:00',1,'APD Departments ',0,55801,NULL,NULL,55801,NULL,NULL,NULL,'000000',0,NULL,1),(720329,NULL,'2013-09-26 00:00:00',1,'E-Commerce Department ',0,720323,NULL,NULL,55801,NULL,NULL,NULL,'000000',0,NULL,1),(720332,NULL,'2013-09-26 00:00:00',1,'Customer Service Department ',0,720323,NULL,NULL,55801,NULL,NULL,NULL,'000000',0,NULL,1),(720335,NULL,'2013-09-26 00:00:00',0,'Order Management Department ',0,720323,NULL,NULL,55801,NULL,NULL,NULL,'000000',0,NULL,1),(720338,NULL,'2013-09-26 00:00:00',1,'Exceptions Department ',0,720323,NULL,NULL,55801,NULL,NULL,NULL,'000000',0,NULL,1),(720341,NULL,'2013-09-26 00:00:00',1,'Technical Department ',0,720323,NULL,NULL,55801,NULL,NULL,NULL,'000000',0,NULL,1),(720344,NULL,'2013-09-26 00:00:00',1,'Human Resource Department ',0,720323,NULL,NULL,55801,NULL,NULL,NULL,'000000',0,NULL,1),(720348,NULL,'2013-09-27 00:00:00',1,'Accounting Department ',0,720323,NULL,NULL,55801,NULL,NULL,NULL,'000000',0,NULL,1),(720351,NULL,'2013-09-27 00:00:00',1,'Warehouse & Logistics Department ',0,720323,NULL,NULL,55801,NULL,NULL,NULL,'000000',0,NULL,1),(720354,NULL,'2013-09-27 00:00:00',1,'APD Executive Staff ',3,720323,134845721,NULL,55801,NULL,NULL,NULL,'000000',0,NULL,1),(720357,NULL,'2013-09-27 00:00:00',1,'Account Management Department ',0,720323,NULL,NULL,55801,NULL,NULL,NULL,'000000',0,NULL,1),(6323579,NULL,'2013-11-13 00:00:00',1,'Treasury & Revenues ',0,55801,NULL,NULL,55801,NULL,NULL,NULL,'000000',0,NULL,1),(6332215,NULL,'2013-11-13 00:00:00',1,'Government Contracting ',0,55801,NULL,NULL,55801,NULL,NULL,NULL,'000000',0,NULL,1),(6349715,NULL,'2013-11-13 00:00:00',1,'[Deprecated] APD Employee Purchasing Program ',2,55801,NULL,NULL,55801,NULL,NULL,NULL,'APD822NC',0,NULL,1);
/*!40000 ALTER TABLE `account` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `account_accountgroup`
--

DROP TABLE IF EXISTS `account_accountgroup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `account_accountgroup` (
  `ACCOUNTS_ID` bigint(20) NOT NULL DEFAULT '0',
  `GROUPS_ID` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ACCOUNTS_ID`,`GROUPS_ID`),
  KEY `FK2FA3E964236EB562` (`GROUPS_ID`),
  CONSTRAINT `FK2FA3E964236EB562` FOREIGN KEY (`GROUPS_ID`) REFERENCES `accountgroup` (`ID`),
  CONSTRAINT `FK2FA3E9647BEA6A7E` FOREIGN KEY (`ACCOUNTS_ID`) REFERENCES `account` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `account_accountgroup`
--

LOCK TABLES `account_accountgroup` WRITE;
/*!40000 ALTER TABLE `account_accountgroup` DISABLE KEYS */;
/*!40000 ALTER TABLE `account_accountgroup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `account_accountgroup_aud`
--

DROP TABLE IF EXISTS `account_accountgroup_aud`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `account_accountgroup_aud` (
  `REV` bigint(20) NOT NULL AUTO_INCREMENT,
  `ACCOUNTS_ID` bigint(20) NOT NULL DEFAULT '0',
  `GROUPS_ID` bigint(20) NOT NULL DEFAULT '0',
  `REVTYPE` decimal(3,0) DEFAULT NULL,
  PRIMARY KEY (`REV`,`ACCOUNTS_ID`,`GROUPS_ID`),
  CONSTRAINT `FK7F34C535DF74E053` FOREIGN KEY (`REV`) REFERENCES `revinfo` (`REV`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `account_accountgroup_aud`
--

LOCK TABLES `account_accountgroup_aud` WRITE;
/*!40000 ALTER TABLE `account_accountgroup_aud` DISABLE KEYS */;
/*!40000 ALTER TABLE `account_accountgroup_aud` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `account_aud`
--

DROP TABLE IF EXISTS `account_aud`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `account_aud` (
  `ID` bigint(20) NOT NULL DEFAULT '0',
  `REV` bigint(20) NOT NULL AUTO_INCREMENT,
  `REVTYPE` decimal(3,0) DEFAULT NULL,
  `APDASSIGNEDACCOUNTID` varchar(255) DEFAULT NULL,
  `CREATIONDATE` datetime DEFAULT NULL,
  `CSREMAIL` varchar(255) DEFAULT NULL,
  `CSRPHONE` varchar(255) DEFAULT NULL,
  `ISACTIVE` decimal(1,0) DEFAULT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `SOLOMONCUSTOMERID` varchar(255) DEFAULT NULL,
  `ALLOWEDPAYMENTTYPE_ID` bigint(20) DEFAULT NULL,
  `CXMLCONFIGURATION_ID` bigint(20) DEFAULT NULL,
  `PARENTACCOUNT_ID` bigint(20) DEFAULT NULL,
  `PAYMENTINFORMATION_ID` bigint(20) DEFAULT NULL,
  `PRIMARYCONTACT_ID` bigint(20) DEFAULT NULL,
  `ROOTACCOUNT_ID` bigint(20) DEFAULT NULL,
  `USELOCATIONCODE` decimal(1,0) DEFAULT '0',
  `LOCATIONCODE` varchar(255) DEFAULT NULL,
  `SHOWGLOBALBULLETIN` decimal(1,0) DEFAULT NULL,
  PRIMARY KEY (`ID`,`REV`),
  KEY `FK970E815EDF74E053` (`REV`),
  CONSTRAINT `FK970E815EDF74E053` FOREIGN KEY (`REV`) REFERENCES `revinfo` (`REV`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `account_aud`
--

LOCK TABLES `account_aud` WRITE;
/*!40000 ALTER TABLE `account_aud` DISABLE KEYS */;
/*!40000 ALTER TABLE `account_aud` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `account_bulletin`
--

DROP TABLE IF EXISTS `account_bulletin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `account_bulletin` (
  `ACCOUNT_ID` bigint(20) NOT NULL DEFAULT '0',
  `BULLETINS_ID` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ACCOUNT_ID`,`BULLETINS_ID`),
  KEY `FKB24DABB9367C0B98` (`BULLETINS_ID`),
  CONSTRAINT `FKB24DABB9185A22D7` FOREIGN KEY (`ACCOUNT_ID`) REFERENCES `account` (`ID`),
  CONSTRAINT `FKB24DABB9367C0B98` FOREIGN KEY (`BULLETINS_ID`) REFERENCES `bulletin` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `account_bulletin`
--

LOCK TABLES `account_bulletin` WRITE;
/*!40000 ALTER TABLE `account_bulletin` DISABLE KEYS */;
/*!40000 ALTER TABLE `account_bulletin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `account_bulletin_aud`
--

DROP TABLE IF EXISTS `account_bulletin_aud`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `account_bulletin_aud` (
  `REV` bigint(20) NOT NULL AUTO_INCREMENT,
  `ACCOUNT_ID` bigint(20) NOT NULL DEFAULT '0',
  `BULLETINS_ID` bigint(20) NOT NULL DEFAULT '0',
  `REVTYPE` decimal(3,0) DEFAULT NULL,
  PRIMARY KEY (`REV`,`ACCOUNT_ID`,`BULLETINS_ID`),
  CONSTRAINT `FK37DB550ADF74E053` FOREIGN KEY (`REV`) REFERENCES `revinfo` (`REV`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `account_bulletin_aud`
--

LOCK TABLES `account_bulletin_aud` WRITE;
/*!40000 ALTER TABLE `account_bulletin_aud` DISABLE KEYS */;
/*!40000 ALTER TABLE `account_bulletin_aud` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `account_ipaddress_aud`
--

DROP TABLE IF EXISTS `account_ipaddress_aud`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `account_ipaddress_aud` (
  `REV` bigint(20) NOT NULL AUTO_INCREMENT,
  `ACCOUNT_ID` bigint(20) NOT NULL DEFAULT '0',
  `ID` bigint(20) NOT NULL DEFAULT '0',
  `REVTYPE` decimal(3,0) DEFAULT NULL,
  PRIMARY KEY (`REV`,`ACCOUNT_ID`,`ID`),
  CONSTRAINT `FK5006654CDF74E053` FOREIGN KEY (`REV`) REFERENCES `revinfo` (`REV`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `account_ipaddress_aud`
--

LOCK TABLES `account_ipaddress_aud` WRITE;
/*!40000 ALTER TABLE `account_ipaddress_aud` DISABLE KEYS */;
/*!40000 ALTER TABLE `account_ipaddress_aud` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `account_ponumber`
--

DROP TABLE IF EXISTS `account_ponumber`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `account_ponumber` (
  `ACCOUNT_ID` bigint(20) NOT NULL DEFAULT '0',
  `BLANKETPOS_ID` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ACCOUNT_ID`,`BLANKETPOS_ID`),
  KEY `FKF0898C3ACE415F34` (`BLANKETPOS_ID`),
  CONSTRAINT `FKF0898C3A185A22D7` FOREIGN KEY (`ACCOUNT_ID`) REFERENCES `account` (`ID`),
  CONSTRAINT `FKF0898C3ACE415F34` FOREIGN KEY (`BLANKETPOS_ID`) REFERENCES `ponumber` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `account_ponumber`
--

LOCK TABLES `account_ponumber` WRITE;
/*!40000 ALTER TABLE `account_ponumber` DISABLE KEYS */;
/*!40000 ALTER TABLE `account_ponumber` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `accountbrmsrecord`
--

DROP TABLE IF EXISTS `accountbrmsrecord`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accountbrmsrecord` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `ACTIVE` decimal(1,0) DEFAULT NULL,
  `PROCESSID` bigint(20) DEFAULT NULL,
  `SESSIONID` bigint(20) DEFAULT NULL,
  `VERSION` bigint(20) DEFAULT NULL,
  `ACCOUNT_ID` bigint(20) DEFAULT NULL,
  `SNAPSHOT` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK9D4C7BB4185A22D7` (`ACCOUNT_ID`),
  CONSTRAINT `FK9D4C7BB4185A22D7` FOREIGN KEY (`ACCOUNT_ID`) REFERENCES `account` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `accountbrmsrecord`
--

LOCK TABLES `accountbrmsrecord` WRITE;
/*!40000 ALTER TABLE `accountbrmsrecord` DISABLE KEYS */;
/*!40000 ALTER TABLE `accountbrmsrecord` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `accountgroup`
--

DROP TABLE IF EXISTS `accountgroup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accountgroup` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(255) DEFAULT NULL,
  `VERSION` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `INDX_5` (`NAME`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=141028465 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `accountgroup`
--

LOCK TABLES `accountgroup` WRITE;
/*!40000 ALTER TABLE `accountgroup` DISABLE KEYS */;
INSERT INTO `accountgroup` VALUES (31280,'Clinic',0),(31281,'Hospital',0),(31282,'Commercial',0),(31283,'Government',0),(31284,'Novant',0),(31285,'CSS',0),(17706662,'CSS Clinic',0),(42877590,'CSS Corporate',0),(42877629,'CSS Outlying',0),(42878651,'CSS Metro Hospitals',1),(42878660,'Cintas',0),(42878662,'CSS CPN',0),(42878664,'CSSOutlying',0),(42878666,'General Accounting',0),(42878668,'NH Inpatient Care Specialists',0),(102588696,'CSS Regional Hospitals',1),(141028460,'Acute Care Hospitals',0),(141028462,'Corporate ',0),(141028464,'NMG',0);
/*!40000 ALTER TABLE `accountgroup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `accountgroup_aud`
--

DROP TABLE IF EXISTS `accountgroup_aud`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accountgroup_aud` (
  `ID` bigint(20) NOT NULL DEFAULT '0',
  `REV` bigint(20) NOT NULL AUTO_INCREMENT,
  `REVTYPE` decimal(3,0) DEFAULT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`,`REV`),
  KEY `FK1846C943DF74E053` (`REV`),
  CONSTRAINT `FK1846C943DF74E053` FOREIGN KEY (`REV`) REFERENCES `revinfo` (`REV`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `accountgroup_aud`
--

LOCK TABLES `accountgroup_aud` WRITE;
/*!40000 ALTER TABLE `accountgroup_aud` DISABLE KEYS */;
/*!40000 ALTER TABLE `accountgroup_aud` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `accountpropertytype`
--

DROP TABLE IF EXISTS `accountpropertytype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accountpropertytype` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `VERSION` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UC_ACCOUNTPROPERTYTYPE_1` (`NAME`)
) ENGINE=InnoDB AUTO_INCREMENT=141028753 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `accountpropertytype`
--

LOCK TABLES `accountpropertytype` WRITE;
/*!40000 ALTER TABLE `accountpropertytype` DISABLE KEYS */;
INSERT INTO `accountpropertytype` VALUES (31286,'BillTo Name',0),(31287,'Invoice Batch',0),(3966240,'Salesperson',0),(5875222,'Accounting ID',0),(13613632,'Can Edit Customer PO',0),(17709306,'minimum order amount',0),(17709307,'minimum order fee',0),(39033268,'Subdomain',0),(39304649,'user registration domain',0),(110066903,'hide user registration link',0),(118805681,'authorized shipto prefix',0),(124509649,'hide user password reset link',0),(141028752,'Region',0);
/*!40000 ALTER TABLE `accountpropertytype` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `accountpropertytype_aud`
--

DROP TABLE IF EXISTS `accountpropertytype_aud`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accountpropertytype_aud` (
  `ID` bigint(20) NOT NULL DEFAULT '0',
  `REV` bigint(20) NOT NULL AUTO_INCREMENT,
  `REVTYPE` decimal(3,0) DEFAULT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`,`REV`),
  KEY `FKEF0CD92DDF74E053` (`REV`),
  CONSTRAINT `FKEF0CD92DDF74E053` FOREIGN KEY (`REV`) REFERENCES `revinfo` (`REV`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `accountpropertytype_aud`
--

LOCK TABLES `accountpropertytype_aud` WRITE;
/*!40000 ALTER TABLE `accountpropertytype_aud` DISABLE KEYS */;
/*!40000 ALTER TABLE `accountpropertytype_aud` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `accountxaccountpropertytype`
--

DROP TABLE IF EXISTS `accountxaccountpropertytype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accountxaccountpropertytype` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `VALUE` varchar(255) DEFAULT NULL,
  `VERSION` bigint(20) DEFAULT NULL,
  `TYPE_ID` bigint(20) DEFAULT NULL,
  `ACCOUNT_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `INDX_6` (`ACCOUNT_ID`) USING BTREE,
  KEY `INDX_7` (`TYPE_ID`) USING BTREE,
  CONSTRAINT `FKF7E20391185A22D7` FOREIGN KEY (`ACCOUNT_ID`) REFERENCES `account` (`ID`),
  CONSTRAINT `FKF7E20391460EDA79` FOREIGN KEY (`TYPE_ID`) REFERENCES `accountpropertytype` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=135390198 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `accountxaccountpropertytype`
--

LOCK TABLES `accountxaccountpropertytype` WRITE;
/*!40000 ALTER TABLE `accountxaccountpropertytype` DISABLE KEYS */;
INSERT INTO `accountxaccountpropertytype` VALUES (6349718,'APD822NC',0,5875222,6349715),(135390197,'AME822NC',0,5875222,720354);
/*!40000 ALTER TABLE `accountxaccountpropertytype` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `accountxcredentialxuser`
--

DROP TABLE IF EXISTS `accountxcredentialxuser`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accountxcredentialxuser` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `PUNCHLEVEL` varchar(255) DEFAULT NULL,
  `USACCOUNTNUMBER` varchar(255) DEFAULT NULL,
  `VERSION` bigint(20) DEFAULT NULL,
  `ACCOUNT_ID` bigint(20) DEFAULT NULL,
  `ADDRESS_ID` bigint(20) DEFAULT NULL,
  `APPROVERUSER_ID` bigint(20) DEFAULT NULL,
  `CRED_ID` bigint(20) DEFAULT NULL,
  `PAYMENTINFORMATION_ID` bigint(20) DEFAULT NULL,
  `USER_ID` bigint(20) DEFAULT NULL,
  `ACTIVE` decimal(1,0) NOT NULL,
  `CREDENTIALSELECTIONIMAGE` varchar(255) DEFAULT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `ACCTXUSER_ACCOUNT_ID` (`ACCOUNT_ID`) USING BTREE,
  KEY `ACCTXUSER_ADDRESS_ID` (`ADDRESS_ID`) USING BTREE,
  KEY `ACCTXUSER_APPROVERUSER_ID` (`APPROVERUSER_ID`) USING BTREE,
  KEY `ACCTXUSER_CRED_ID` (`CRED_ID`) USING BTREE,
  KEY `ACCTXUSER_PAYMENTINFO_ID` (`PAYMENTINFORMATION_ID`) USING BTREE,
  KEY `ACCTXUSER_USER_ID` (`USER_ID`) USING BTREE,
  CONSTRAINT `FK34428EE1185A22D7` FOREIGN KEY (`ACCOUNT_ID`) REFERENCES `account` (`ID`),
  CONSTRAINT `FK34428EE145A534A6` FOREIGN KEY (`CRED_ID`) REFERENCES `credential` (`ID`),
  CONSTRAINT `FK34428EE14E9ECF4C` FOREIGN KEY (`USER_ID`) REFERENCES `systemuser` (`ID`),
  CONSTRAINT `FK34428EE17ED1037D` FOREIGN KEY (`PAYMENTINFORMATION_ID`) REFERENCES `paymentinformation` (`ID`),
  CONSTRAINT `FK34428EE199EEF977` FOREIGN KEY (`ADDRESS_ID`) REFERENCES `address` (`ID`),
  CONSTRAINT `FK34428EE1DC8230E7` FOREIGN KEY (`APPROVERUSER_ID`) REFERENCES `systemuser` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=14179141 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `accountxcredentialxuser`
--

LOCK TABLES `accountxcredentialxuser` WRITE;
/*!40000 ALTER TABLE `accountxcredentialxuser` DISABLE KEYS */;
INSERT INTO `accountxcredentialxuser` VALUES (719552,NULL,NULL,3,55801,NULL,NULL,60094,NULL,75494,0,NULL,NULL),(719699,NULL,NULL,0,60071,NULL,NULL,60094,NULL,719573,1,NULL,NULL),(720224,NULL,NULL,0,55801,NULL,NULL,60094,NULL,719662,1,NULL,NULL),(1438234,NULL,NULL,0,55801,NULL,NULL,60094,NULL,770070,1,NULL,NULL),(3966437,NULL,NULL,0,55801,NULL,NULL,3825168,NULL,3966428,1,NULL,NULL),(14178581,NULL,NULL,0,55801,NULL,NULL,60094,NULL,14178576,1,NULL,NULL),(14178616,NULL,NULL,0,720341,NULL,NULL,60094,NULL,14178611,1,NULL,NULL),(14179140,NULL,NULL,0,6349715,NULL,NULL,60094,NULL,719747,1,NULL,NULL);
/*!40000 ALTER TABLE `accountxcredentialxuser` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `accxcredxuser_ccenter`
--

DROP TABLE IF EXISTS `accxcredxuser_ccenter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accxcredxuser_ccenter` (
  `CURRENTCHARGE` decimal(19,2) DEFAULT NULL,
  `PENDINGCHARGE` decimal(19,2) DEFAULT NULL,
  `TOTALLIMIT` decimal(19,2) DEFAULT NULL,
  `VERSION` bigint(20) DEFAULT NULL,
  `ACCOUNTCREDENTIALUSERS_ID` bigint(20) DEFAULT NULL,
  `ALLOWEDCOSTCENTERS_ID` bigint(20) DEFAULT NULL,
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `ACTIVE` decimal(1,0) NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `SYS_C004413` (`ACCOUNTCREDENTIALUSERS_ID`,`ALLOWEDCOSTCENTERS_ID`) USING BTREE,
  KEY `FKCE242C77179FBA96` (`ALLOWEDCOSTCENTERS_ID`),
  CONSTRAINT `FKCE242C77179FBA96` FOREIGN KEY (`ALLOWEDCOSTCENTERS_ID`) REFERENCES `costcenter` (`ID`),
  CONSTRAINT `FKCE242C77FFE1BA34` FOREIGN KEY (`ACCOUNTCREDENTIALUSERS_ID`) REFERENCES `accountxcredentialxuser` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `accxcredxuser_ccenter`
--

LOCK TABLES `accxcredxuser_ccenter` WRITE;
/*!40000 ALTER TABLE `accxcredxuser_ccenter` DISABLE KEYS */;
/*!40000 ALTER TABLE `accxcredxuser_ccenter` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `accxcredxuser_ccenter_aud`
--

DROP TABLE IF EXISTS `accxcredxuser_ccenter_aud`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accxcredxuser_ccenter_aud` (
  `ID` bigint(20) NOT NULL DEFAULT '0',
  `REV` bigint(20) NOT NULL AUTO_INCREMENT,
  `REVTYPE` decimal(3,0) DEFAULT NULL,
  `CURRENTCHARGE` decimal(19,2) DEFAULT NULL,
  `PENDINGCHARGE` decimal(19,2) DEFAULT NULL,
  `TOTALLIMIT` decimal(19,2) DEFAULT NULL,
  `ACCOUNTCREDENTIALUSERS_ID` bigint(20) DEFAULT NULL,
  `ALLOWEDCOSTCENTERS_ID` bigint(20) DEFAULT NULL,
  `ACTIVE` decimal(1,0) DEFAULT NULL,
  PRIMARY KEY (`ID`,`REV`),
  KEY `FK8EE746C8DF74E053` (`REV`),
  CONSTRAINT `FK8EE746C8DF74E053` FOREIGN KEY (`REV`) REFERENCES `revinfo` (`REV`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `accxcredxuser_ccenter_aud`
--

LOCK TABLES `accxcredxuser_ccenter_aud` WRITE;
/*!40000 ALTER TABLE `accxcredxuser_ccenter_aud` DISABLE KEYS */;
/*!40000 ALTER TABLE `accxcredxuser_ccenter_aud` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `accxcredxuser_dept`
--

DROP TABLE IF EXISTS `accxcredxuser_dept`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accxcredxuser_dept` (
  `ACCOUNTXCREDENTIALXUSER_ID` bigint(20) DEFAULT NULL,
  `DEPARTMENTS_ID` bigint(20) DEFAULT NULL,
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `VERSION` bigint(20) DEFAULT NULL,
  `RECIPIENTSEMAILS` varchar(255) DEFAULT NULL,
  `RECIPIENTSNAMES` varchar(255) DEFAULT NULL,
  `ACTIVE` decimal(1,0) NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `SYS_C004416` (`ACCOUNTXCREDENTIALXUSER_ID`,`DEPARTMENTS_ID`) USING BTREE,
  KEY `FK44393CA6540E04AE` (`DEPARTMENTS_ID`),
  CONSTRAINT `FK44393CA6540E04AE` FOREIGN KEY (`DEPARTMENTS_ID`) REFERENCES `department` (`ID`),
  CONSTRAINT `FK44393CA661F26857` FOREIGN KEY (`ACCOUNTXCREDENTIALXUSER_ID`) REFERENCES `accountxcredentialxuser` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `accxcredxuser_dept`
--

LOCK TABLES `accxcredxuser_dept` WRITE;
/*!40000 ALTER TABLE `accxcredxuser_dept` DISABLE KEYS */;
/*!40000 ALTER TABLE `accxcredxuser_dept` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `accxcredxuser_dept_aud`
--

DROP TABLE IF EXISTS `accxcredxuser_dept_aud`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accxcredxuser_dept_aud` (
  `REV` bigint(20) NOT NULL AUTO_INCREMENT,
  `ACCOUNTXCREDENTIALXUSER_ID` bigint(20) DEFAULT NULL,
  `DEPARTMENTS_ID` bigint(20) DEFAULT NULL,
  `REVTYPE` decimal(3,0) DEFAULT NULL,
  `ID` bigint(20) NOT NULL DEFAULT '0',
  `RECIPIENTSEMAILS` varchar(255) DEFAULT NULL,
  `RECIPIENTSNAMES` varchar(255) DEFAULT NULL,
  `ACTIVE` decimal(1,0) DEFAULT NULL,
  PRIMARY KEY (`ID`,`REV`),
  KEY `FKD68AA777DF74E053` (`REV`),
  CONSTRAINT `FKD68AA777DF74E053` FOREIGN KEY (`REV`) REFERENCES `revinfo` (`REV`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `accxcredxuser_dept_aud`
--

LOCK TABLES `accxcredxuser_dept_aud` WRITE;
/*!40000 ALTER TABLE `accxcredxuser_dept_aud` DISABLE KEYS */;
/*!40000 ALTER TABLE `accxcredxuser_dept_aud` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `accxcredxuser_desktops`
--

DROP TABLE IF EXISTS `accxcredxuser_desktops`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accxcredxuser_desktops` (
  `ACCOUNTXCREDENTIALXUSER_ID` bigint(20) NOT NULL DEFAULT '0',
  `DESKTOPS_ID` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ACCOUNTXCREDENTIALXUSER_ID`,`DESKTOPS_ID`),
  KEY `FKDB3499B8377FE6DC` (`DESKTOPS_ID`),
  CONSTRAINT `FKDB3499B8377FE6DC` FOREIGN KEY (`DESKTOPS_ID`) REFERENCES `desktop` (`ID`),
  CONSTRAINT `FKDB3499B861F26857` FOREIGN KEY (`ACCOUNTXCREDENTIALXUSER_ID`) REFERENCES `accountxcredentialxuser` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `accxcredxuser_desktops`
--

LOCK TABLES `accxcredxuser_desktops` WRITE;
/*!40000 ALTER TABLE `accxcredxuser_desktops` DISABLE KEYS */;
/*!40000 ALTER TABLE `accxcredxuser_desktops` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `accxcredxuser_desktops_aud`
--

DROP TABLE IF EXISTS `accxcredxuser_desktops_aud`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accxcredxuser_desktops_aud` (
  `REV` bigint(20) NOT NULL AUTO_INCREMENT,
  `ACCOUNTXCREDENTIALXUSER_ID` bigint(20) NOT NULL DEFAULT '0',
  `DESKTOPS_ID` bigint(20) NOT NULL DEFAULT '0',
  `REVTYPE` decimal(3,0) DEFAULT NULL,
  PRIMARY KEY (`REV`,`ACCOUNTXCREDENTIALXUSER_ID`,`DESKTOPS_ID`),
  CONSTRAINT `FK978D2B8975D1432` FOREIGN KEY (`REV`) REFERENCES `revinfo` (`REV`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `accxcredxuser_desktops_aud`
--

LOCK TABLES `accxcredxuser_desktops_aud` WRITE;
/*!40000 ALTER TABLE `accxcredxuser_desktops_aud` DISABLE KEYS */;
/*!40000 ALTER TABLE `accxcredxuser_desktops_aud` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `actiontype`
--

DROP TABLE IF EXISTS `actiontype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `actiontype` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `DESCRIPTION` varchar(255) DEFAULT NULL,
  `NAME` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `VERSION` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UC_ACTIONTYPE_1` (`NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `actiontype`
--

LOCK TABLES `actiontype` WRITE;
/*!40000 ALTER TABLE `actiontype` DISABLE KEYS */;
/*!40000 ALTER TABLE `actiontype` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `actiontype_aud`
--

DROP TABLE IF EXISTS `actiontype_aud`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `actiontype_aud` (
  `ID` bigint(20) NOT NULL DEFAULT '0',
  `REV` bigint(20) NOT NULL AUTO_INCREMENT,
  `REVTYPE` decimal(3,0) DEFAULT NULL,
  `DESCRIPTION` varchar(255) DEFAULT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`,`REV`),
  KEY `FKA116A881DF74E053` (`REV`),
  CONSTRAINT `FKA116A881DF74E053` FOREIGN KEY (`REV`) REFERENCES `revinfo` (`REV`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `actiontype_aud`
--

LOCK TABLES `actiontype_aud` WRITE;
/*!40000 ALTER TABLE `actiontype_aud` DISABLE KEYS */;
/*!40000 ALTER TABLE `actiontype_aud` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `acxacpt_aud`
--

DROP TABLE IF EXISTS `acxacpt_aud`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `acxacpt_aud` (
  `ID` bigint(20) NOT NULL DEFAULT '0',
  `REV` bigint(20) NOT NULL AUTO_INCREMENT,
  `REVTYPE` decimal(3,0) DEFAULT NULL,
  `VALUE` varchar(255) DEFAULT NULL,
  `TYPE_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`,`REV`),
  KEY `FKFA2B070D75D1432` (`REV`),
  CONSTRAINT `FKFA2B070D75D1432` FOREIGN KEY (`REV`) REFERENCES `revinfo` (`REV`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `acxacpt_aud`
--

LOCK TABLES `acxacpt_aud` WRITE;
/*!40000 ALTER TABLE `acxacpt_aud` DISABLE KEYS */;
/*!40000 ALTER TABLE `acxacpt_aud` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `address`
--

DROP TABLE IF EXISTS `address`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `address` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `CITY` varchar(255) DEFAULT NULL,
  `COMPANY` varchar(255) DEFAULT NULL,
  `COUNTRY` varchar(255) DEFAULT NULL,
  `COUNTY` varchar(255) DEFAULT NULL,
  `GEOCODE` varchar(255) DEFAULT NULL,
  `LINE1` varchar(255) DEFAULT NULL,
  `LINE2` varchar(255) DEFAULT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `PENDINGSHIPTOAUTHORIZATION` decimal(1,0) DEFAULT NULL,
  `PRIMARY` decimal(1,0) DEFAULT NULL,
  `SHIPTOID` varchar(255) DEFAULT NULL,
  `STATE` varchar(255) DEFAULT NULL,
  `VERSION` bigint(20) DEFAULT NULL,
  `ZIP` varchar(255) DEFAULT NULL,
  `ACCOUNT_ID` bigint(20) DEFAULT NULL,
  `MISCSHIPTO_ID` bigint(20) DEFAULT NULL,
  `PERSON_ID` bigint(20) DEFAULT NULL,
  `TYPE_ID` bigint(20) DEFAULT NULL,
  `VENDOR_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `ADDRESS_PERSON_ID` (`PERSON_ID`) USING BTREE,
  KEY `ADDRESS_TYPE_ID` (`TYPE_ID`) USING BTREE,
  KEY `ADDRESS_VENDOR_ID` (`VENDOR_ID`) USING BTREE,
  KEY `INDX_8` (`ACCOUNT_ID`) USING BTREE,
  KEY `INDX_9` (`MISCSHIPTO_ID`) USING BTREE,
  CONSTRAINT `FK1ED033D4185A22D7` FOREIGN KEY (`ACCOUNT_ID`) REFERENCES `account` (`ID`),
  CONSTRAINT `FK1ED033D43E66BDBD` FOREIGN KEY (`PERSON_ID`) REFERENCES `person` (`ID`),
  CONSTRAINT `FK1ED033D46C357B4B` FOREIGN KEY (`TYPE_ID`) REFERENCES `addresstype` (`ID`),
  CONSTRAINT `FK1ED033D4D50C94DD` FOREIGN KEY (`VENDOR_ID`) REFERENCES `vendor` (`ID`),
  CONSTRAINT `FK1ED033D4E4B11BD` FOREIGN KEY (`MISCSHIPTO_ID`) REFERENCES `miscshipto` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `address`
--

LOCK TABLES `address` WRITE;
/*!40000 ALTER TABLE `address` DISABLE KEYS */;
/*!40000 ALTER TABLE `address` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `address_aud`
--

DROP TABLE IF EXISTS `address_aud`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `address_aud` (
  `ID` bigint(20) NOT NULL DEFAULT '0',
  `REV` bigint(20) NOT NULL AUTO_INCREMENT,
  `REVTYPE` decimal(3,0) DEFAULT NULL,
  `CITY` varchar(255) DEFAULT NULL,
  `COMPANY` varchar(255) DEFAULT NULL,
  `COUNTRY` varchar(255) DEFAULT NULL,
  `COUNTY` varchar(255) DEFAULT NULL,
  `GEOCODE` varchar(255) DEFAULT NULL,
  `LINE1` varchar(255) DEFAULT NULL,
  `LINE2` varchar(255) DEFAULT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `PENDINGSHIPTOAUTHORIZATION` decimal(1,0) DEFAULT NULL,
  `PRIMARY` decimal(1,0) DEFAULT NULL,
  `SHIPTOID` varchar(255) DEFAULT NULL,
  `STATE` varchar(255) DEFAULT NULL,
  `ZIP` varchar(255) DEFAULT NULL,
  `ACCOUNT_ID` bigint(20) DEFAULT NULL,
  `PERSON_ID` bigint(20) DEFAULT NULL,
  `TYPE_ID` bigint(20) DEFAULT NULL,
  `VENDOR_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`,`REV`),
  KEY `FK115657A5DF74E053` (`REV`),
  CONSTRAINT `FK115657A5DF74E053` FOREIGN KEY (`REV`) REFERENCES `revinfo` (`REV`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `address_aud`
--

LOCK TABLES `address_aud` WRITE;
/*!40000 ALTER TABLE `address_aud` DISABLE KEYS */;
/*!40000 ALTER TABLE `address_aud` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `addresspropertytype`
--

DROP TABLE IF EXISTS `addresspropertytype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `addresspropertytype` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(255) DEFAULT NULL,
  `VERSION` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=31276 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `addresspropertytype`
--

LOCK TABLES `addresspropertytype` WRITE;
/*!40000 ALTER TABLE `addresspropertytype` DISABLE KEYS */;
INSERT INTO `addresspropertytype` VALUES (31270,'APD EDI ID',0),(31271,'Airport Code',0),(31272,'Desktop',0),(31273,'Building/Department',0),(31274,'US Account Number',0),(31275,'Cost Center',0);
/*!40000 ALTER TABLE `addresspropertytype` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `addresspropertytype_aud`
--

DROP TABLE IF EXISTS `addresspropertytype_aud`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `addresspropertytype_aud` (
  `ID` bigint(20) NOT NULL DEFAULT '0',
  `REV` bigint(20) NOT NULL AUTO_INCREMENT,
  `REVTYPE` decimal(3,0) DEFAULT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`,`REV`),
  KEY `FKE49A7CF4DF74E053` (`REV`),
  CONSTRAINT `FKE49A7CF4DF74E053` FOREIGN KEY (`REV`) REFERENCES `revinfo` (`REV`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `addresspropertytype_aud`
--

LOCK TABLES `addresspropertytype_aud` WRITE;
/*!40000 ALTER TABLE `addresspropertytype_aud` DISABLE KEYS */;
/*!40000 ALTER TABLE `addresspropertytype_aud` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `addresstype`
--

DROP TABLE IF EXISTS `addresstype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `addresstype` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `VERSION` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UC_ADDRESSTYPE_1` (`NAME`)
) ENGINE=InnoDB AUTO_INCREMENT=31270 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `addresstype`
--

LOCK TABLES `addresstype` WRITE;
/*!40000 ALTER TABLE `addresstype` DISABLE KEYS */;
INSERT INTO `addresstype` VALUES (31266,'SHIPTO',0),(31267,'BILLTO',0),(31268,'Home',0),(31269,'Work',0);
/*!40000 ALTER TABLE `addresstype` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `addresstype_aud`
--

DROP TABLE IF EXISTS `addresstype_aud`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `addresstype_aud` (
  `ID` bigint(20) NOT NULL DEFAULT '0',
  `REV` bigint(20) NOT NULL AUTO_INCREMENT,
  `REVTYPE` decimal(3,0) DEFAULT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`,`REV`),
  KEY `FK952813FFDF74E053` (`REV`),
  CONSTRAINT `FK952813FFDF74E053` FOREIGN KEY (`REV`) REFERENCES `revinfo` (`REV`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `addresstype_aud`
--

LOCK TABLES `addresstype_aud` WRITE;
/*!40000 ALTER TABLE `addresstype_aud` DISABLE KEYS */;
/*!40000 ALTER TABLE `addresstype_aud` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `addressxaddresspropertytype`
--

DROP TABLE IF EXISTS `addressxaddresspropertytype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `addressxaddresspropertytype` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `VALUE` varchar(255) DEFAULT NULL,
  `VERSION` bigint(20) DEFAULT NULL,
  `ADDRESS_ID` bigint(20) DEFAULT NULL,
  `TYPE_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FKF9E70B1F99EEF977` (`ADDRESS_ID`),
  KEY `FKF9E70B1FDD1B79C0` (`TYPE_ID`),
  CONSTRAINT `FKF9E70B1F99EEF977` FOREIGN KEY (`ADDRESS_ID`) REFERENCES `address` (`ID`),
  CONSTRAINT `FKF9E70B1FDD1B79C0` FOREIGN KEY (`TYPE_ID`) REFERENCES `addresspropertytype` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `addressxaddresspropertytype`
--

LOCK TABLES `addressxaddresspropertytype` WRITE;
/*!40000 ALTER TABLE `addressxaddresspropertytype` DISABLE KEYS */;
/*!40000 ALTER TABLE `addressxaddresspropertytype` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `apdpolog`
--

DROP TABLE IF EXISTS `apdpolog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `apdpolog` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `CREATIONDATE` datetime DEFAULT NULL,
  `VERSION` bigint(20) DEFAULT NULL,
  `APDPO_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `CREATEDATE_APDPOLOG_IDX` (`CREATIONDATE`) USING BTREE,
  KEY `PO_APDPOLOG_IDX` (`APDPO_ID`) USING BTREE,
  CONSTRAINT `FK3425E7D05ECADFF1` FOREIGN KEY (`APDPO_ID`) REFERENCES `ponumber` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `apdpolog`
--

LOCK TABLES `apdpolog` WRITE;
/*!40000 ALTER TABLE `apdpolog` DISABLE KEYS */;
/*!40000 ALTER TABLE `apdpolog` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `attachmenttype`
--

DROP TABLE IF EXISTS `attachmenttype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `attachmenttype` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(255) DEFAULT NULL,
  `VERSION` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `attachmenttype`
--

LOCK TABLES `attachmenttype` WRITE;
/*!40000 ALTER TABLE `attachmenttype` DISABLE KEYS */;
/*!40000 ALTER TABLE `attachmenttype` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `attachmenttype_aud`
--

DROP TABLE IF EXISTS `attachmenttype_aud`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `attachmenttype_aud` (
  `ID` bigint(20) NOT NULL DEFAULT '0',
  `REV` bigint(20) NOT NULL AUTO_INCREMENT,
  `REVTYPE` decimal(3,0) DEFAULT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`,`REV`),
  KEY `FKFD3C366EDF74E053` (`REV`),
  CONSTRAINT `FKFD3C366EDF74E053` FOREIGN KEY (`REV`) REFERENCES `revinfo` (`REV`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `attachmenttype_aud`
--

LOCK TABLES `attachmenttype_aud` WRITE;
/*!40000 ALTER TABLE `attachmenttype_aud` DISABLE KEYS */;
/*!40000 ALTER TABLE `attachmenttype_aud` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `axapt_aud`
--

DROP TABLE IF EXISTS `axapt_aud`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `axapt_aud` (
  `ID` bigint(20) NOT NULL DEFAULT '0',
  `REV` bigint(20) NOT NULL AUTO_INCREMENT,
  `REVTYPE` decimal(3,0) DEFAULT NULL,
  `VALUE` varchar(255) DEFAULT NULL,
  `ADDRESS_ID` bigint(20) DEFAULT NULL,
  `TYPE_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`REV`,`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `axapt_aud`
--

LOCK TABLES `axapt_aud` WRITE;
/*!40000 ALTER TABLE `axapt_aud` DISABLE KEYS */;
/*!40000 ALTER TABLE `axapt_aud` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `axcxu_aud`
--

DROP TABLE IF EXISTS `axcxu_aud`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `axcxu_aud` (
  `ID` bigint(20) NOT NULL DEFAULT '0',
  `REV` bigint(20) NOT NULL AUTO_INCREMENT,
  `REVTYPE` decimal(3,0) DEFAULT NULL,
  `PUNCHLEVEL` varchar(255) DEFAULT NULL,
  `USACCOUNTNUMBER` varchar(255) DEFAULT NULL,
  `ACCOUNT_ID` bigint(20) DEFAULT NULL,
  `ADDRESS_ID` bigint(20) DEFAULT NULL,
  `APPROVERUSER_ID` bigint(20) DEFAULT NULL,
  `CRED_ID` bigint(20) DEFAULT NULL,
  `PAYMENTINFORMATION_ID` bigint(20) DEFAULT NULL,
  `USER_ID` bigint(20) DEFAULT NULL,
  `ACTIVE` decimal(1,0) DEFAULT NULL,
  `CREDENTIALSELECTIONIMAGE` varchar(255) DEFAULT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`,`REV`),
  KEY `FK27E6F47ADF74E053` (`REV`),
  CONSTRAINT `FK27E6F47ADF74E053` FOREIGN KEY (`REV`) REFERENCES `revinfo` (`REV`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `axcxu_aud`
--

LOCK TABLES `axcxu_aud` WRITE;
/*!40000 ALTER TABLE `axcxu_aud` DISABLE KEYS */;
/*!40000 ALTER TABLE `axcxu_aud` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `brand`
--

DROP TABLE IF EXISTS `brand`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `brand` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `LOGO` varchar(255) DEFAULT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `USSCOID` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `BRAND_ID_IX` (`USSCOID`) USING BTREE,
  KEY `BRAND_NAME_IX` (`NAME`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `brand`
--

LOCK TABLES `brand` WRITE;
/*!40000 ALTER TABLE `brand` DISABLE KEYS */;
/*!40000 ALTER TABLE `brand` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bulletin`
--

DROP TABLE IF EXISTS `bulletin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bulletin` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `EXPIRATIONDATE` datetime DEFAULT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `VERSION` bigint(20) DEFAULT NULL,
  `GLOBAL` decimal(1,0) NOT NULL,
  `MESSAGE` longtext,
  `CSR` decimal(1,0) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bulletin`
--

LOCK TABLES `bulletin` WRITE;
/*!40000 ALTER TABLE `bulletin` DISABLE KEYS */;
/*!40000 ALTER TABLE `bulletin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bulletin_aud`
--

DROP TABLE IF EXISTS `bulletin_aud`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bulletin_aud` (
  `ID` bigint(20) NOT NULL DEFAULT '0',
  `REV` bigint(20) NOT NULL AUTO_INCREMENT,
  `REVTYPE` decimal(3,0) DEFAULT NULL,
  `EXPIRATIONDATE` datetime DEFAULT NULL,
  `GLOBAL` decimal(1,0) DEFAULT NULL,
  `MESSAGE` longtext,
  `NAME` varchar(255) DEFAULT NULL,
  `CSR` decimal(1,0) DEFAULT NULL,
  PRIMARY KEY (`ID`,`REV`),
  KEY `FK51BD9018DF74E053` (`REV`),
  CONSTRAINT `FK51BD9018DF74E053` FOREIGN KEY (`REV`) REFERENCES `revinfo` (`REV`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bulletin_aud`
--

LOCK TABLES `bulletin_aud` WRITE;
/*!40000 ALTER TABLE `bulletin_aud` DISABLE KEYS */;
/*!40000 ALTER TABLE `bulletin_aud` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `canceledquantity`
--

DROP TABLE IF EXISTS `canceledquantity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `canceledquantity` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `QUANTITY` decimal(19,2) NOT NULL,
  `REASON` varchar(255) DEFAULT NULL,
  `VERSION` bigint(20) DEFAULT NULL,
  `LINEITEM_ID` bigint(20) DEFAULT NULL,
  `CANCELDATE` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `CANCELQUANTITY_LINEITEM_IDX` (`LINEITEM_ID`) USING BTREE,
  KEY `CANCEL_CANCELDATE_IDX` (`CANCELDATE`) USING BTREE,
  CONSTRAINT `FK3BD1E864B7777FDD` FOREIGN KEY (`LINEITEM_ID`) REFERENCES `lineitem` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `canceledquantity`
--

LOCK TABLES `canceledquantity` WRITE;
/*!40000 ALTER TABLE `canceledquantity` DISABLE KEYS */;
/*!40000 ALTER TABLE `canceledquantity` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cardinformation`
--

DROP TABLE IF EXISTS `cardinformation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cardinformation` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `DEBIT` decimal(1,0) NOT NULL,
  `EXPIRATION` datetime DEFAULT NULL,
  `IDENTIFIER` varchar(255) DEFAULT NULL,
  `NAME_ON_CARD` varchar(255) DEFAULT NULL,
  `VERSION` bigint(20) DEFAULT NULL,
  `CARDVAULTTOKEN` varchar(255) DEFAULT NULL,
  `GHOST` decimal(1,0) NOT NULL,
  `CARDTYPE` varchar(255) DEFAULT NULL,
  `CVVINDICATOR` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=142686136 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cardinformation`
--

LOCK TABLES `cardinformation` WRITE;
/*!40000 ALTER TABLE `cardinformation` DISABLE KEYS */;
INSERT INTO `cardinformation` VALUES (3966430,0,'2013-11-29 00:00:00',NULL,'Test User',0,'invalid',0,NULL,NULL),(3969674,0,NULL,NULL,NULL,0,'invalid',0,NULL,NULL),(111406654,0,'2025-06-02 00:00:00','XXXXXXXXXXXX0000','Boehringer Ingelheim Corp.',0,'invalid',1,NULL,NULL),(124463059,0,'2015-05-01 00:00:00','XXXXXXXXXXXX9078','J&J Maint/V.V ',1,'C100000010712797',1,NULL,NULL),(134487593,0,NULL,NULL,NULL,0,NULL,1,NULL,NULL),(142686129,0,NULL,NULL,NULL,0,NULL,1,NULL,NULL),(142686135,0,NULL,NULL,NULL,0,NULL,1,NULL,NULL);
/*!40000 ALTER TABLE `cardinformation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cardinformation_aud`
--

DROP TABLE IF EXISTS `cardinformation_aud`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cardinformation_aud` (
  `ID` bigint(20) NOT NULL DEFAULT '0',
  `REV` bigint(20) NOT NULL AUTO_INCREMENT,
  `REVTYPE` decimal(3,0) DEFAULT NULL,
  `CARDVAULTTOKEN` varchar(255) DEFAULT NULL,
  `DEBIT` decimal(1,0) DEFAULT NULL,
  `EXPIRATION` datetime DEFAULT NULL,
  `GHOST` decimal(1,0) DEFAULT NULL,
  `IDENTIFIER` varchar(255) DEFAULT NULL,
  `NAME_ON_CARD` varchar(255) DEFAULT NULL,
  `CARDTYPE` varchar(255) DEFAULT NULL,
  `CVVINDICATOR` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`,`REV`),
  KEY `FK69BCAFADDF74E053` (`REV`),
  CONSTRAINT `FK69BCAFADDF74E053` FOREIGN KEY (`REV`) REFERENCES `revinfo` (`REV`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cardinformation_aud`
--

LOCK TABLES `cardinformation_aud` WRITE;
/*!40000 ALTER TABLE `cardinformation_aud` DISABLE KEYS */;
/*!40000 ALTER TABLE `cardinformation_aud` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `carousel_display`
--

DROP TABLE IF EXISTS `carousel_display`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `carousel_display` (
  `FROM_CLASS` varchar(31) NOT NULL,
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `BACKGROUNDIMAGEURL` varchar(255) DEFAULT NULL,
  `BODY` varchar(2000) DEFAULT NULL,
  `HEADER` varchar(255) DEFAULT NULL,
  `LINKTEXT` varchar(255) DEFAULT NULL,
  `LINKURL` varchar(255) DEFAULT NULL,
  `VERSION` bigint(20) DEFAULT NULL,
  `SUBDOMAIN` varchar(255) DEFAULT NULL,
  `CATALOG_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `CAROUSEL_SUBDOMAIN_IDX` (`SUBDOMAIN`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `carousel_display`
--

LOCK TABLES `carousel_display` WRITE;
/*!40000 ALTER TABLE `carousel_display` DISABLE KEYS */;
/*!40000 ALTER TABLE `carousel_display` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `carousel_display_aud`
--

DROP TABLE IF EXISTS `carousel_display_aud`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `carousel_display_aud` (
  `FROM_CLASS` varchar(31) NOT NULL,
  `ID` bigint(20) NOT NULL DEFAULT '0',
  `REV` bigint(20) NOT NULL AUTO_INCREMENT,
  `REVTYPE` decimal(3,0) DEFAULT NULL,
  `BACKGROUNDIMAGEURL` varchar(255) DEFAULT NULL,
  `BODY` varchar(2000) DEFAULT NULL,
  `HEADER` varchar(255) DEFAULT NULL,
  `LINKTEXT` varchar(255) DEFAULT NULL,
  `LINKURL` varchar(255) DEFAULT NULL,
  `SUBDOMAIN` varchar(255) DEFAULT NULL,
  `CATALOG_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`,`REV`),
  KEY `FKE513A13475D1432` (`REV`),
  CONSTRAINT `FKE513A13475D1432` FOREIGN KEY (`REV`) REFERENCES `revinfo` (`REV`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `carousel_display_aud`
--

LOCK TABLES `carousel_display_aud` WRITE;
/*!40000 ALTER TABLE `carousel_display_aud` DISABLE KEYS */;
/*!40000 ALTER TABLE `carousel_display_aud` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cart`
--

DROP TABLE IF EXISTS `cart`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cart` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `ESTIMATEDSHIPPINGAMOUNT` decimal(19,2) DEFAULT NULL,
  `MAXIMUMTAXTOCHARGE` decimal(19,2) DEFAULT NULL,
  `EXPIRATIONDATE` datetime DEFAULT NULL,
  `VERSION` bigint(20) DEFAULT NULL,
  `USERCREDENTIAL_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK1FEF409ADA0556` (`USERCREDENTIAL_ID`),
  CONSTRAINT `FK1FEF409ADA0556` FOREIGN KEY (`USERCREDENTIAL_ID`) REFERENCES `accountxcredentialxuser` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cart`
--

LOCK TABLES `cart` WRITE;
/*!40000 ALTER TABLE `cart` DISABLE KEYS */;
/*!40000 ALTER TABLE `cart` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cartitem`
--

DROP TABLE IF EXISTS `cartitem`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cartitem` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `QUANTITY` bigint(20) DEFAULT NULL,
  `VERSION` bigint(20) DEFAULT NULL,
  `CATALOGXITEM_ID` bigint(20) DEFAULT NULL,
  `CART_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK4393E73779C529D` (`CART_ID`),
  KEY `FK4393E73D4DB665D` (`CATALOGXITEM_ID`),
  CONSTRAINT `FK4393E73779C529D` FOREIGN KEY (`CART_ID`) REFERENCES `cart` (`ID`),
  CONSTRAINT `FK4393E73D4DB665D` FOREIGN KEY (`CATALOGXITEM_ID`) REFERENCES `catalogxitem` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cartitem`
--

LOCK TABLES `cartitem` WRITE;
/*!40000 ALTER TABLE `cartitem` DISABLE KEYS */;
/*!40000 ALTER TABLE `cartitem` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cashoutpage`
--

DROP TABLE IF EXISTS `cashoutpage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cashoutpage` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `CANCELDATE` decimal(1,0) DEFAULT NULL,
  `DELIVERYDATE` decimal(1,0) DEFAULT NULL,
  `EXPECTEDDATE` decimal(1,0) DEFAULT NULL,
  `HIDECCNUMBER` decimal(1,0) DEFAULT NULL,
  `HIGHERPRICENOTICE` decimal(1,0) DEFAULT NULL,
  `MULTIPLESHIPTO` decimal(1,0) DEFAULT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `NOCARDACTION` varchar(255) DEFAULT NULL,
  `SERVICEDATE` decimal(1,0) DEFAULT NULL,
  `UPDATEUSERINFORMATION` decimal(1,0) DEFAULT NULL,
  `VERSION` bigint(20) DEFAULT NULL,
  `BILLTOADDRESSDISPLAYFORMAT` varchar(255) DEFAULT NULL,
  `SHIPTOADDRESSDISPLAYFORMAT` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=3825206 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cashoutpage`
--

LOCK TABLES `cashoutpage` WRITE;
/*!40000 ALTER TABLE `cashoutpage` DISABLE KEYS */;
INSERT INTO `cashoutpage` VALUES (1543988,1,0,0,0,1,1,'Placeholder Cashout Page (INVOICE)','INVOICE',0,1,11,NULL,NULL),(3825205,0,0,0,0,0,0,'Select Existing','USE_ASSIGNED',0,0,5,NULL,NULL);
/*!40000 ALTER TABLE `cashoutpage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cashoutpagexfield`
--

DROP TABLE IF EXISTS `cashoutpagexfield`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cashoutpagexfield` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `DEFAULTVALUE` varchar(255) DEFAULT NULL,
  `INCLUDELABELINMAPPING` decimal(1,0) DEFAULT NULL,
  `LABEL` varchar(255) DEFAULT NULL,
  `REQUIRED` decimal(1,0) NOT NULL,
  `VERSION` bigint(20) DEFAULT NULL,
  `FIELD_ID` bigint(20) DEFAULT NULL,
  `MAPPING_ID` bigint(20) DEFAULT NULL,
  `PAGE_ID` bigint(20) DEFAULT NULL,
  `INBOUNDMAPPING` varchar(255) DEFAULT NULL,
  `UPDATABLE` decimal(1,0) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `FKE48C344C71C12AF2` (`PAGE_ID`),
  KEY `FKE48C344C44537EF7` (`FIELD_ID`),
  KEY `FKE48C344CA5D99E16` (`MAPPING_ID`),
  CONSTRAINT `FKE48C344C44537EF7` FOREIGN KEY (`FIELD_ID`) REFERENCES `field` (`ID`),
  CONSTRAINT `FKE48C344C71C12AF2` FOREIGN KEY (`PAGE_ID`) REFERENCES `cashoutpage` (`ID`),
  CONSTRAINT `FKE48C344CA5D99E16` FOREIGN KEY (`MAPPING_ID`) REFERENCES `messagemapping` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=44250296 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cashoutpagexfield`
--

LOCK TABLES `cashoutpagexfield` WRITE;
/*!40000 ALTER TABLE `cashoutpagexfield` DISABLE KEYS */;
INSERT INTO `cashoutpagexfield` VALUES (1543989,NULL,NULL,'BILLING ADDRESS 2',0,2,30768,NULL,1543988,NULL,1),(1543990,NULL,0,'EMAIL ADDRESS',1,4,30760,NULL,1543988,NULL,1),(1543991,NULL,0,'SHIPPING ADDRESS 1',1,4,30740,NULL,1543988,NULL,1),(1543992,NULL,NULL,'SHIPPING ADDRESS 2',0,2,30741,NULL,1543988,NULL,1),(1543993,NULL,0,'SHIPPING CITY',1,4,30743,NULL,1543988,NULL,1),(1543994,NULL,0,'SHIPPING STATE',1,4,30745,NULL,1543988,NULL,1),(1543995,NULL,0,'BILLING ZIP',1,4,30771,NULL,1543988,NULL,1),(1543996,NULL,0,'BILLING CITY',1,4,30770,NULL,1543988,NULL,1),(1543997,NULL,0,'RECIPIENT\'S NAME',1,6,30738,NULL,1543988,NULL,1),(1543999,NULL,0,'COMPANY NAME',1,3,30737,NULL,1543988,NULL,1),(1544000,NULL,0,'BILLING STATE',1,4,30772,NULL,1543988,NULL,1),(1544001,NULL,0,'BILLING ADDRESS 1',1,4,30767,NULL,1543988,NULL,1),(1544002,NULL,0,'SHIPPING ZIP',1,4,30744,NULL,1543988,NULL,1),(3825206,NULL,NULL,'SELECT EXISTING BILLING ADDRESS',1,3,30777,NULL,3825205,NULL,1),(3825207,NULL,NULL,'SELECT EXISTING SHIPPING ADDRESS',1,3,30750,NULL,3825205,NULL,1),(3850165,NULL,NULL,'NAME',0,1,30738,NULL,3825205,NULL,1),(4136358,NULL,NULL,'PO NUMBER',1,1,30758,NULL,1543988,NULL,1),(7989197,NULL,0,'REQUESTOR\'S PHONE NUMBER',1,1,30739,NULL,1543988,NULL,1),(8000546,NULL,0,'BLDG/DEPT',0,0,30752,NULL,1543988,NULL,1),(8000547,NULL,0,'DESKTOP',0,0,30751,NULL,1543988,NULL,1),(44250294,NULL,0,'CHOOSE EXISTING SHIP TO',0,0,30750,NULL,1543988,NULL,1),(44250295,NULL,0,'CHOOSE EXISTING BILL TO',0,0,30777,NULL,1543988,NULL,1);
/*!40000 ALTER TABLE `cashoutpagexfield` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cashoutpagexfield_aud`
--

DROP TABLE IF EXISTS `cashoutpagexfield_aud`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cashoutpagexfield_aud` (
  `ID` bigint(20) NOT NULL DEFAULT '0',
  `REV` bigint(20) NOT NULL AUTO_INCREMENT,
  `REVTYPE` decimal(3,0) DEFAULT NULL,
  `DEFAULTVALUE` varchar(255) DEFAULT NULL,
  `INBOUNDMAPPING` varchar(255) DEFAULT NULL,
  `INCLUDELABELINMAPPING` decimal(1,0) DEFAULT NULL,
  `LABEL` varchar(255) DEFAULT NULL,
  `REQUIRED` decimal(1,0) DEFAULT NULL,
  `UPDATABLE` decimal(1,0) DEFAULT NULL,
  PRIMARY KEY (`ID`,`REV`),
  KEY `FK9FAD5C1DDF74E053` (`REV`),
  CONSTRAINT `FK9FAD5C1DDF74E053` FOREIGN KEY (`REV`) REFERENCES `revinfo` (`REV`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cashoutpagexfield_aud`
--

LOCK TABLES `cashoutpagexfield_aud` WRITE;
/*!40000 ALTER TABLE `cashoutpagexfield_aud` DISABLE KEYS */;
/*!40000 ALTER TABLE `cashoutpagexfield_aud` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalog`
--

DROP TABLE IF EXISTS `catalog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `catalog` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `CHANGEDATE` datetime DEFAULT NULL,
  `EXPIRATIONDATE` datetime DEFAULT NULL,
  `NAME` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `NONUPSABLESHIPPINGMAXIMUM` decimal(19,2) DEFAULT NULL,
  `NONUPSABLESHIPPINGMINIMUM` decimal(19,2) DEFAULT NULL,
  `NONUPSABLESHIPPINGRATE` decimal(19,2) DEFAULT NULL,
  `NONUPSABLESHIPPINGRATEPERCENT` decimal(1,0) DEFAULT NULL,
  `STARTDATE` datetime DEFAULT NULL,
  `UPSABLESHIPPINGMAXIMUM` decimal(19,2) DEFAULT NULL,
  `UPSABLESHIPPINGMINIMUM` decimal(19,2) DEFAULT NULL,
  `UPSABLESHIPPINGRATE` decimal(19,2) DEFAULT NULL,
  `UPSABLESHIPPINGRATEPERCENT` decimal(1,0) DEFAULT NULL,
  `VERSION` bigint(20) DEFAULT NULL,
  `CUSTOMER_ID` bigint(20) DEFAULT NULL,
  `PARENT_ID` bigint(20) DEFAULT NULL,
  `REPLACEMENT_ID` bigint(20) DEFAULT NULL,
  `VENDOR_ID` bigint(20) DEFAULT NULL,
  `LASTPROCESSEND` datetime DEFAULT NULL,
  `LASTPROCESSSTART` datetime DEFAULT NULL,
  `CHANGEDITEMRELEVANTFIELDS` decimal(1,0) DEFAULT NULL,
  `USELOCATIONCODE` decimal(1,0) DEFAULT NULL,
  `LOCATIONCODE` varchar(255) DEFAULT NULL,
  `REGENERATECSVDATE` datetime DEFAULT NULL,
  `REGENERATECSVNIGHTLY` decimal(1,0) DEFAULT NULL,
  `REINDEXDATE` datetime DEFAULT NULL,
  `REINDEXNIGHTLY` decimal(1,0) DEFAULT NULL,
  `REGENERATESMARTOCIDATE` datetime DEFAULT NULL,
  `SEARCHTYPE` varchar(255) DEFAULT NULL,
  `COREITEMHASH` bigint(20) DEFAULT NULL,
  `ITEMHASH` bigint(20) DEFAULT NULL,
  `CORELISTUID_ID` bigint(20) DEFAULT NULL,
  `LISTUID_ID` bigint(20) DEFAULT NULL,
  `MARKETINGIMAGELINKURLTOP` varchar(255) DEFAULT NULL,
  `MARKETINGIMAGEURLBOTTOM` varchar(255) DEFAULT NULL,
  `MARKETINGIMAGEURLTOP` varchar(255) DEFAULT NULL,
  `MARKETINGLINKURLBOTTOM` varchar(255) DEFAULT NULL,
  `PROCESSINGCOMPLETEEMAILS` varchar(255) DEFAULT NULL,
  `PROCESSEDQUEUECOUNT` bigint(20) DEFAULT NULL,
  `UNPROCESSEDQUEUECOUNT` bigint(20) DEFAULT NULL,
  `DIFFCSVCHECKED` decimal(1,0) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UC_CATALOG_1` (`NAME`),
  KEY `CATALOG_PARENT_IDX` (`PARENT_ID`) USING BTREE,
  KEY `USPS_IDX_1` (`CUSTOMER_ID`) USING BTREE,
  KEY `USPS_IDX_8` (`CHANGEDITEMRELEVANTFIELDS`) USING BTREE,
  KEY `FK8457F7F9A6F76EBE` (`REPLACEMENT_ID`),
  KEY `FK8457F7F96277CD4D` (`CORELISTUID_ID`),
  KEY `FK8457F7F9738CD86E` (`LISTUID_ID`),
  KEY `FK8457F7F9D50C94DD` (`VENDOR_ID`),
  CONSTRAINT `FK8457F7F92AEE8D26` FOREIGN KEY (`PARENT_ID`) REFERENCES `catalog` (`ID`),
  CONSTRAINT `FK8457F7F96277CD4D` FOREIGN KEY (`CORELISTUID_ID`) REFERENCES `listhashtouid` (`ID`),
  CONSTRAINT `FK8457F7F9738CD86E` FOREIGN KEY (`LISTUID_ID`) REFERENCES `listhashtouid` (`ID`),
  CONSTRAINT `FK8457F7F9A6F76EBE` FOREIGN KEY (`REPLACEMENT_ID`) REFERENCES `catalog` (`ID`),
  CONSTRAINT `FK8457F7F9D50C94DD` FOREIGN KEY (`VENDOR_ID`) REFERENCES `vendor` (`ID`),
  CONSTRAINT `FK8457F7F9DE9C8386` FOREIGN KEY (`CUSTOMER_ID`) REFERENCES `account` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `catalog`
--

LOCK TABLES `catalog` WRITE;
/*!40000 ALTER TABLE `catalog` DISABLE KEYS */;
/*!40000 ALTER TABLE `catalog` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalog_aud`
--

DROP TABLE IF EXISTS `catalog_aud`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `catalog_aud` (
  `ID` bigint(20) NOT NULL DEFAULT '0',
  `REV` bigint(20) NOT NULL AUTO_INCREMENT,
  `REVTYPE` decimal(3,0) DEFAULT NULL,
  `LASTPROCESSEND` datetime DEFAULT NULL,
  `LASTPROCESSSTART` datetime DEFAULT NULL,
  `NONUPSABLESHIPPINGMAXIMUM` decimal(19,2) DEFAULT NULL,
  `NONUPSABLESHIPPINGMINIMUM` decimal(19,2) DEFAULT NULL,
  `NONUPSABLESHIPPINGRATE` decimal(19,2) DEFAULT NULL,
  `NONUPSABLESHIPPINGRATEPERCENT` decimal(1,0) DEFAULT NULL,
  `UPSABLESHIPPINGRATE` decimal(19,2) DEFAULT NULL,
  `UPSABLESHIPPINGRATEPERCENT` decimal(1,0) DEFAULT NULL,
  PRIMARY KEY (`ID`,`REV`),
  KEY `FK2765814ADF74E053` (`REV`),
  CONSTRAINT `FK2765814ADF74E053` FOREIGN KEY (`REV`) REFERENCES `revinfo` (`REV`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `catalog_aud`
--

LOCK TABLES `catalog_aud` WRITE;
/*!40000 ALTER TABLE `catalog_aud` DISABLE KEYS */;
/*!40000 ALTER TABLE `catalog_aud` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalogcsv`
--

DROP TABLE IF EXISTS `catalogcsv`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `catalogcsv` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `CATALOGID` bigint(20) DEFAULT NULL,
  `CORRELATIONID` varchar(255) DEFAULT NULL,
  `PROCESSEDITEMS` bigint(20) DEFAULT NULL,
  `TOTALITEMS` bigint(20) DEFAULT NULL,
  `VERSION` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `CS_CORRID` (`CORRELATIONID`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `catalogcsv`
--

LOCK TABLES `catalogcsv` WRITE;
/*!40000 ALTER TABLE `catalogcsv` DISABLE KEYS */;
/*!40000 ALTER TABLE `catalogcsv` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalogupload`
--

DROP TABLE IF EXISTS `catalogupload`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `catalogupload` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `ACTION` bigint(20) DEFAULT NULL,
  `CATALOGID` bigint(20) DEFAULT NULL,
  `COMPLETED` decimal(1,0) NOT NULL,
  `CORRELATIONID` varchar(255) DEFAULT NULL,
  `OLDCATALOGID` bigint(20) DEFAULT NULL,
  `PERSISTCHANGES` decimal(1,0) DEFAULT NULL,
  `PROCESSEDITEMS` bigint(20) DEFAULT NULL,
  `TOTALITEMS` bigint(20) DEFAULT NULL,
  `VERSION` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `CU_CORRID` (`CORRELATIONID`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `catalogupload`
--

LOCK TABLES `catalogupload` WRITE;
/*!40000 ALTER TABLE `catalogupload` DISABLE KEYS */;
/*!40000 ALTER TABLE `catalogupload` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalogxcategoryxpricingtype`
--

DROP TABLE IF EXISTS `catalogxcategoryxpricingtype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `catalogxcategoryxpricingtype` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `PARAMETER` varchar(255) DEFAULT NULL,
  `VERSION` bigint(20) DEFAULT NULL,
  `CATALOG_ID` bigint(20) DEFAULT NULL,
  `CATEGORY_ID` bigint(20) DEFAULT NULL,
  `TYPE_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `CATXCATEGXPRIC_CATALOG` (`CATALOG_ID`) USING BTREE,
  KEY `CATXCATEGXPRIC_CATEGORY` (`CATEGORY_ID`) USING BTREE,
  KEY `FK7BA786C51B2C461D` (`TYPE_ID`),
  CONSTRAINT `FK7BA786C51B2C461D` FOREIGN KEY (`TYPE_ID`) REFERENCES `pricingtype` (`ID`),
  CONSTRAINT `FK7BA786C531B94B57` FOREIGN KEY (`CATALOG_ID`) REFERENCES `catalog` (`ID`),
  CONSTRAINT `FK7BA786C5B7BD2710` FOREIGN KEY (`CATEGORY_ID`) REFERENCES `itemcategory` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `catalogxcategoryxpricingtype`
--

LOCK TABLES `catalogxcategoryxpricingtype` WRITE;
/*!40000 ALTER TABLE `catalogxcategoryxpricingtype` DISABLE KEYS */;
/*!40000 ALTER TABLE `catalogxcategoryxpricingtype` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalogxitem`
--

DROP TABLE IF EXISTS `catalogxitem`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `catalogxitem` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `COREITEMEXPIRATIONDATE` datetime DEFAULT NULL,
  `COREITEMSTARTDATE` datetime DEFAULT NULL,
  `PRICE` decimal(19,2) NOT NULL,
  `PRICINGPARAMETER` varchar(255) DEFAULT NULL,
  `VERSION` bigint(20) DEFAULT NULL,
  `CATALOG_ID` bigint(20) DEFAULT NULL,
  `CUSTOMERSKU_ID` bigint(20) DEFAULT NULL,
  `ITEM_ID` bigint(20) DEFAULT NULL,
  `PRICINGTYPE_ID` bigint(20) DEFAULT NULL,
  `SUBSTITUTEITEM_ID` bigint(20) DEFAULT NULL,
  `ADDED` decimal(1,0) DEFAULT NULL,
  `DELETED` decimal(1,0) DEFAULT NULL,
  `MODIFIED` decimal(1,0) DEFAULT NULL,
  `LASTMODIFIED` datetime DEFAULT NULL,
  `OVERRIDECATALOGS` varchar(255) DEFAULT NULL,
  `ISFEATUREDITEM` decimal(1,0) DEFAULT NULL,
  `MOSTPOPULAR` decimal(1,0) DEFAULT NULL,
  `CUSTOMERUNITOFMEASURE_ID` bigint(20) DEFAULT NULL,
  `SOLROVERRIDEDESCRIPTION` varchar(255) DEFAULT NULL,
  `CUSTOMERSKUSTRING` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UK_CATALOGXITEM_1` (`CATALOG_ID`,`ITEM_ID`),
  KEY `CXI_CUSTSKUCATALOG_IDX` (`CUSTOMERSKUSTRING`,`CATALOG_ID`) USING BTREE,
  KEY `CXI_CUSTSKU_IDX` (`CUSTOMERSKUSTRING`) USING BTREE,
  KEY `CXI_ITEM_IDX` (`ITEM_ID`) USING BTREE,
  KEY `USPS_IDX_2` (`CATALOG_ID`) USING BTREE,
  KEY `USPS_IDX_4` (`MODIFIED`) USING BTREE,
  KEY `USPS_IDX_5` (`ADDED`) USING BTREE,
  KEY `USPS_IDX_6` (`DELETED`) USING BTREE,
  KEY `FKC4EC1F3220C000E2` (`SUBSTITUTEITEM_ID`),
  KEY `FKC4EC1F3291FB7B97` (`PRICINGTYPE_ID`),
  KEY `FKC4EC1F329260A0F5` (`CUSTOMERUNITOFMEASURE_ID`),
  KEY `FKC4EC1F32EA944B5` (`CUSTOMERSKU_ID`),
  CONSTRAINT `FKC4EC1F3220C000E2` FOREIGN KEY (`SUBSTITUTEITEM_ID`) REFERENCES `catalogxitem` (`ID`),
  CONSTRAINT `FKC4EC1F3231B94B57` FOREIGN KEY (`CATALOG_ID`) REFERENCES `catalog` (`ID`),
  CONSTRAINT `FKC4EC1F3291FB7B97` FOREIGN KEY (`PRICINGTYPE_ID`) REFERENCES `pricingtype` (`ID`),
  CONSTRAINT `FKC4EC1F329260A0F5` FOREIGN KEY (`CUSTOMERUNITOFMEASURE_ID`) REFERENCES `unitofmeasure` (`ID`),
  CONSTRAINT `FKC4EC1F32D4B669BD` FOREIGN KEY (`ITEM_ID`) REFERENCES `item` (`ID`),
  CONSTRAINT `FKC4EC1F32EA944B5` FOREIGN KEY (`CUSTOMERSKU_ID`) REFERENCES `sku` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `catalogxitem`
--

LOCK TABLES `catalogxitem` WRITE;
/*!40000 ALTER TABLE `catalogxitem` DISABLE KEYS */;
/*!40000 ALTER TABLE `catalogxitem` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalogxitemxitempropertytype`
--

DROP TABLE IF EXISTS `catalogxitemxitempropertytype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `catalogxitemxitempropertytype` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `VERSION` bigint(20) DEFAULT NULL,
  `TYPE_ID` bigint(20) DEFAULT NULL,
  `CATALOGXITEM_ID` bigint(20) DEFAULT NULL,
  `VALUE` varchar(4000) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `CATALOGXITEM_ID_IX` (`CATALOGXITEM_ID`) USING BTREE,
  KEY `FK846B38E878D89205` (`TYPE_ID`),
  CONSTRAINT `FK846B38E878D89205` FOREIGN KEY (`TYPE_ID`) REFERENCES `itempropertytype` (`ID`),
  CONSTRAINT `FK846B38E8D4DB665D` FOREIGN KEY (`CATALOGXITEM_ID`) REFERENCES `catalogxitem` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `catalogxitemxitempropertytype`
--

LOCK TABLES `catalogxitemxitempropertytype` WRITE;
/*!40000 ALTER TABLE `catalogxitemxitempropertytype` DISABLE KEYS */;
/*!40000 ALTER TABLE `catalogxitemxitempropertytype` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categorymarkup`
--

DROP TABLE IF EXISTS `categorymarkup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categorymarkup` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `MARKUPPERCENTAGE` decimal(19,2) DEFAULT NULL,
  `VERSION` bigint(20) DEFAULT NULL,
  `CATALOG_ID` bigint(20) DEFAULT NULL,
  `CATEGORY_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK16ACD4E631B94B57` (`CATALOG_ID`),
  KEY `FK16ACD4E6B7BD2710` (`CATEGORY_ID`),
  CONSTRAINT `FK16ACD4E631B94B57` FOREIGN KEY (`CATALOG_ID`) REFERENCES `catalog` (`ID`),
  CONSTRAINT `FK16ACD4E6B7BD2710` FOREIGN KEY (`CATEGORY_ID`) REFERENCES `itemcategory` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categorymarkup`
--

LOCK TABLES `categorymarkup` WRITE;
/*!40000 ALTER TABLE `categorymarkup` DISABLE KEYS */;
/*!40000 ALTER TABLE `categorymarkup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categorymarkup_aud`
--

DROP TABLE IF EXISTS `categorymarkup_aud`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categorymarkup_aud` (
  `ID` bigint(20) NOT NULL DEFAULT '0',
  `REV` bigint(20) NOT NULL AUTO_INCREMENT,
  `REVTYPE` decimal(3,0) DEFAULT NULL,
  `MARKUPPERCENTAGE` decimal(19,2) DEFAULT NULL,
  `CATALOG_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`,`REV`),
  KEY `FK98F81FB7DF74E053` (`REV`),
  CONSTRAINT `FK98F81FB7DF74E053` FOREIGN KEY (`REV`) REFERENCES `revinfo` (`REV`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categorymarkup_aud`
--

LOCK TABLES `categorymarkup_aud` WRITE;
/*!40000 ALTER TABLE `categorymarkup_aud` DISABLE KEYS */;
/*!40000 ALTER TABLE `categorymarkup_aud` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cctranlog_faildetails`
--

DROP TABLE IF EXISTS `cctranlog_faildetails`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cctranlog_faildetails` (
  `CCTRANLOG_ID` bigint(20) DEFAULT NULL,
  `FAILUREDETAILS` varchar(255) DEFAULT NULL,
  KEY `FK362E01FEEA1EA233` (`CCTRANLOG_ID`),
  CONSTRAINT `FK362E01FEEA1EA233` FOREIGN KEY (`CCTRANLOG_ID`) REFERENCES `order_log` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cctranlog_faildetails`
--

LOCK TABLES `cctranlog_faildetails` WRITE;
/*!40000 ALTER TABLE `cctranlog_faildetails` DISABLE KEYS */;
/*!40000 ALTER TABLE `cctranlog_faildetails` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cctranlog_vendorlist`
--

DROP TABLE IF EXISTS `cctranlog_vendorlist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cctranlog_vendorlist` (
  `CCTRANLOG_ID` bigint(20) DEFAULT NULL,
  `VENDORLIST` varchar(255) DEFAULT NULL,
  KEY `FK65DD662CEA1EA233` (`CCTRANLOG_ID`),
  CONSTRAINT `FK65DD662CEA1EA233` FOREIGN KEY (`CCTRANLOG_ID`) REFERENCES `order_log` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cctranlog_vendorlist`
--

LOCK TABLES `cctranlog_vendorlist` WRITE;
/*!40000 ALTER TABLE `cctranlog_vendorlist` DISABLE KEYS */;
/*!40000 ALTER TABLE `cctranlog_vendorlist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `classificationcode`
--

DROP TABLE IF EXISTS `classificationcode`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `classificationcode` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `VALUE` varchar(255) DEFAULT NULL,
  `VERSION` bigint(20) DEFAULT NULL,
  `CODETYPE_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `DEV_INDEX_4` (`CODETYPE_ID`) USING BTREE,
  CONSTRAINT `FKEB05AF133EED1BDD` FOREIGN KEY (`CODETYPE_ID`) REFERENCES `codetype` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `classificationcode`
--

LOCK TABLES `classificationcode` WRITE;
/*!40000 ALTER TABLE `classificationcode` DISABLE KEYS */;
/*!40000 ALTER TABLE `classificationcode` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `classificationnote`
--

DROP TABLE IF EXISTS `classificationnote`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `classificationnote` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `VERSION` bigint(20) DEFAULT NULL,
  `ITEMCLASSIFICATION_ID` bigint(20) DEFAULT NULL,
  `NOTETYPE_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FKEB0AB11815D522DD` (`ITEMCLASSIFICATION_ID`),
  KEY `FKEB0AB1187B0231BD` (`NOTETYPE_ID`),
  CONSTRAINT `FKEB0AB11815D522DD` FOREIGN KEY (`ITEMCLASSIFICATION_ID`) REFERENCES `itemclassification` (`ID`),
  CONSTRAINT `FKEB0AB1187B0231BD` FOREIGN KEY (`NOTETYPE_ID`) REFERENCES `notetype` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `classificationnote`
--

LOCK TABLES `classificationnote` WRITE;
/*!40000 ALTER TABLE `classificationnote` DISABLE KEYS */;
/*!40000 ALTER TABLE `classificationnote` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `codetype`
--

DROP TABLE IF EXISTS `codetype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `codetype` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(255) DEFAULT NULL,
  `VERSION` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `codetype`
--

LOCK TABLES `codetype` WRITE;
/*!40000 ALTER TABLE `codetype` DISABLE KEYS */;
/*!40000 ALTER TABLE `codetype` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `communicationmethod`
--

DROP TABLE IF EXISTS `communicationmethod`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `communicationmethod` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `VERSION` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UC_COMMUNICATIONMETHOD_1` (`NAME`)
) ENGINE=InnoDB AUTO_INCREMENT=1510047 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `communicationmethod`
--

LOCK TABLES `communicationmethod` WRITE;
/*!40000 ALTER TABLE `communicationmethod` DISABLE KEYS */;
INSERT INTO `communicationmethod` VALUES (31182,'EDI',0),(31183,'cXML',0),(31184,'xCBL',0),(1510046,'Manual',0);
/*!40000 ALTER TABLE `communicationmethod` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `communicationmethod_aud`
--

DROP TABLE IF EXISTS `communicationmethod_aud`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `communicationmethod_aud` (
  `ID` bigint(20) NOT NULL DEFAULT '0',
  `REV` bigint(20) NOT NULL AUTO_INCREMENT,
  `REVTYPE` decimal(3,0) DEFAULT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`,`REV`),
  KEY `FK570CD688DF74E053` (`REV`),
  CONSTRAINT `FK570CD688DF74E053` FOREIGN KEY (`REV`) REFERENCES `revinfo` (`REV`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `communicationmethod_aud`
--

LOCK TABLES `communicationmethod_aud` WRITE;
/*!40000 ALTER TABLE `communicationmethod_aud` DISABLE KEYS */;
/*!40000 ALTER TABLE `communicationmethod_aud` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `comp_favlist_catxitem`
--

DROP TABLE IF EXISTS `comp_favlist_catxitem`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comp_favlist_catxitem` (
  `ITEMS_ID` bigint(20) NOT NULL DEFAULT '0',
  `FAVORITESLIST_ID` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ITEMS_ID`,`FAVORITESLIST_ID`),
  KEY `FK3BD333EBE4345037` (`FAVORITESLIST_ID`),
  CONSTRAINT `FK3BD333EB34FADEEF` FOREIGN KEY (`ITEMS_ID`) REFERENCES `catalogxitem` (`ID`),
  CONSTRAINT `FK3BD333EBE4345037` FOREIGN KEY (`FAVORITESLIST_ID`) REFERENCES `favoriteslist` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comp_favlist_catxitem`
--

LOCK TABLES `comp_favlist_catxitem` WRITE;
/*!40000 ALTER TABLE `comp_favlist_catxitem` DISABLE KEYS */;
/*!40000 ALTER TABLE `comp_favlist_catxitem` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contactlogcomment`
--

DROP TABLE IF EXISTS `contactlogcomment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contactlogcomment` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `COMMENTTIMESTAMP` datetime DEFAULT NULL,
  `CONTENT` varchar(2000) NOT NULL,
  `VERSION` bigint(20) DEFAULT NULL,
  `APDCSRREP_ID` bigint(20) DEFAULT NULL,
  `SERVICELOG_ID` bigint(20) DEFAULT NULL,
  `ISSUELOG_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FKE766069B29E27F47` (`APDCSRREP_ID`),
  KEY `FKE766069B797AADDD` (`SERVICELOG_ID`),
  KEY `FKE766069BE92BED1D` (`ISSUELOG_ID`),
  CONSTRAINT `FKE766069B29E27F47` FOREIGN KEY (`APDCSRREP_ID`) REFERENCES `systemuser` (`ID`),
  CONSTRAINT `FKE766069B797AADDD` FOREIGN KEY (`SERVICELOG_ID`) REFERENCES `service_log` (`ID`),
  CONSTRAINT `FKE766069BE92BED1D` FOREIGN KEY (`ISSUELOG_ID`) REFERENCES `issue_log` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contactlogcomment`
--

LOCK TABLES `contactlogcomment` WRITE;
/*!40000 ALTER TABLE `contactlogcomment` DISABLE KEYS */;
/*!40000 ALTER TABLE `contactlogcomment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contactmethod`
--

DROP TABLE IF EXISTS `contactmethod`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contactmethod` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(255) DEFAULT NULL,
  `VERSION` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=13613658 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contactmethod`
--

LOCK TABLES `contactmethod` WRITE;
/*!40000 ALTER TABLE `contactmethod` DISABLE KEYS */;
INSERT INTO `contactmethod` VALUES (13613654,'Call',0),(13613655,'E-mail',0),(13613656,'Fax',0),(13613657,'Mail',0);
/*!40000 ALTER TABLE `contactmethod` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contactmethod_aud`
--

DROP TABLE IF EXISTS `contactmethod_aud`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contactmethod_aud` (
  `ID` bigint(20) NOT NULL DEFAULT '0',
  `REV` bigint(20) NOT NULL AUTO_INCREMENT,
  `REVTYPE` decimal(3,0) DEFAULT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`,`REV`),
  KEY `FKFC333E52DF74E053` (`REV`),
  CONSTRAINT `FKFC333E52DF74E053` FOREIGN KEY (`REV`) REFERENCES `revinfo` (`REV`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contactmethod_aud`
--

LOCK TABLES `contactmethod_aud` WRITE;
/*!40000 ALTER TABLE `contactmethod_aud` DISABLE KEYS */;
/*!40000 ALTER TABLE `contactmethod_aud` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `costcenter`
--

DROP TABLE IF EXISTS `costcenter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `costcenter` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `DESCRIPTION` varchar(255) DEFAULT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `CCNUMBER` varchar(255) DEFAULT NULL,
  `SPENDINGLIMIT` decimal(19,2) DEFAULT NULL,
  `TOTALSPENT` decimal(19,2) DEFAULT NULL,
  `VERSION` bigint(20) DEFAULT NULL,
  `PERIOD_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK537676A269C00797` (`PERIOD_ID`),
  CONSTRAINT `FK537676A269C00797` FOREIGN KEY (`PERIOD_ID`) REFERENCES `periodtype` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `costcenter`
--

LOCK TABLES `costcenter` WRITE;
/*!40000 ALTER TABLE `costcenter` DISABLE KEYS */;
/*!40000 ALTER TABLE `costcenter` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `costcenter_aud`
--

DROP TABLE IF EXISTS `costcenter_aud`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `costcenter_aud` (
  `ID` bigint(20) NOT NULL DEFAULT '0',
  `REV` bigint(20) NOT NULL AUTO_INCREMENT,
  `REVTYPE` decimal(3,0) DEFAULT NULL,
  `DESCRIPTION` varchar(255) DEFAULT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `CCNUMBER` varchar(255) DEFAULT NULL,
  `SPENDINGLIMIT` decimal(19,2) DEFAULT NULL,
  `TOTALSPENT` decimal(19,2) DEFAULT NULL,
  `PERIOD_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`,`REV`),
  KEY `FK30628373DF74E053` (`REV`),
  CONSTRAINT `FK30628373DF74E053` FOREIGN KEY (`REV`) REFERENCES `revinfo` (`REV`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `costcenter_aud`
--

LOCK TABLES `costcenter_aud` WRITE;
/*!40000 ALTER TABLE `costcenter_aud` DISABLE KEYS */;
/*!40000 ALTER TABLE `costcenter_aud` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `credential`
--

DROP TABLE IF EXISTS `credential`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `credential` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `ACTIVATIONDATE` datetime DEFAULT NULL,
  `AUTHORIZEADDRESS` decimal(1,0) NOT NULL,
  `EXPIRATIONDATE` datetime DEFAULT NULL,
  `NAME` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `STORECREDIT` decimal(19,2) DEFAULT NULL,
  `VERSION` bigint(20) DEFAULT NULL,
  `CASHOUTPAGE_ID` bigint(20) DEFAULT NULL,
  `CATALOG_ID` bigint(20) DEFAULT NULL,
  `COMMUNICATIONMETHOD_ID` bigint(20) DEFAULT NULL,
  `PAYMENTINFORMATION_ID` bigint(20) DEFAULT NULL,
  `ROOTACCOUNT_ID` bigint(20) DEFAULT NULL,
  `SKUTYPE_ID` bigint(20) DEFAULT NULL,
  `OUTGOINGSENDERSHAREDSECRET` varchar(255) DEFAULT NULL,
  `VENDORPUNCHOUTURL` varchar(255) DEFAULT NULL,
  `CIDEST` varchar(255) DEFAULT NULL,
  `CIMTYPE` varchar(255) DEFAULT NULL,
  `IDEST` varchar(255) DEFAULT NULL,
  `IMTYPE` varchar(255) DEFAULT NULL,
  `POACKDEST` varchar(255) DEFAULT NULL,
  `POACKMTYPE` varchar(255) DEFAULT NULL,
  `SNDEST` varchar(255) DEFAULT NULL,
  `SNMTYPE` varchar(255) DEFAULT NULL,
  `EDIRECCODE` varchar(255) DEFAULT NULL,
  `EDIVERSION` varchar(255) DEFAULT NULL,
  `PARTNERID` varchar(255) DEFAULT NULL,
  `WILLCALLEDIOVERRIDE` decimal(1,0) NOT NULL,
  `TERMS` varchar(255) DEFAULT NULL,
  `CSRPHONE` varchar(255) DEFAULT NULL,
  `CSREMAIL` varchar(255) DEFAULT NULL,
  `PUNCHOUTTYPE` varchar(255) DEFAULT NULL,
  `CUSTOMEROUTGOINGSHAREDSECRET` varchar(255) DEFAULT NULL,
  `DEFAULTPUNCHOUTZIPCODE` varchar(255) DEFAULT NULL,
  `REQUIREZIPCODEINPUT` decimal(1,0) NOT NULL,
  `WRAPANDLABEL` decimal(1,0) NOT NULL DEFAULT '0',
  `ADOT` decimal(1,0) NOT NULL DEFAULT '0',
  `RETURNCARTDOM` varchar(255) DEFAULT NULL,
  `RETURNCARTID` varchar(255) DEFAULT NULL,
  `VENDORNAME` varchar(255) DEFAULT NULL,
  `PUNCHOPALLOW` varchar(255) DEFAULT NULL,
  `DEFAULTSHIPFROMADDRESS_ID` bigint(20) DEFAULT NULL,
  `VENDORORDERURL` varchar(255) DEFAULT NULL,
  `DEPLOYMENTMODE` varchar(255) DEFAULT NULL,
  `CUSTOMFLENAME` varchar(255) DEFAULT NULL,
  `SPECIALFLENAME` varchar(255) DEFAULT NULL,
  `USRGUIDEFLENAME` varchar(255) DEFAULT NULL,
  `USER_ID` bigint(20) DEFAULT NULL,
  `CREDENTIALSELECTIONIMAGE` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UC_CREDENTIAL_1` (`NAME`),
  KEY `CRED_CASHOUTPAGE_ID` (`CASHOUTPAGE_ID`) USING BTREE,
  KEY `CRED_CATALOG_ID` (`CATALOG_ID`) USING BTREE,
  KEY `CRED_PAYMENTINFORMATION_ID` (`PAYMENTINFORMATION_ID`) USING BTREE,
  KEY `CRED_SKUTYPE_ID` (`SKUTYPE_ID`) USING BTREE,
  KEY `INDX_10` (`ROOTACCOUNT_ID`) USING BTREE,
  KEY `FK4E15C477B4B76DDE` (`DEFAULTSHIPFROMADDRESS_ID`),
  KEY `FK4E15C47762FD6737` (`COMMUNICATIONMETHOD_ID`),
  KEY `FK4E15C477DC8230E7` (`USER_ID`),
  CONSTRAINT `FK4E15C47731B94B57` FOREIGN KEY (`CATALOG_ID`) REFERENCES `catalog` (`ID`),
  CONSTRAINT `FK4E15C47762FD6737` FOREIGN KEY (`COMMUNICATIONMETHOD_ID`) REFERENCES `communicationmethod` (`ID`),
  CONSTRAINT `FK4E15C4777ED1037D` FOREIGN KEY (`PAYMENTINFORMATION_ID`) REFERENCES `paymentinformation` (`ID`),
  CONSTRAINT `FK4E15C477B4B76DDE` FOREIGN KEY (`DEFAULTSHIPFROMADDRESS_ID`) REFERENCES `address` (`ID`),
  CONSTRAINT `FK4E15C477BACFEB7` FOREIGN KEY (`SKUTYPE_ID`) REFERENCES `skutype` (`ID`),
  CONSTRAINT `FK4E15C477DC8230E7` FOREIGN KEY (`USER_ID`) REFERENCES `systemuser` (`ID`),
  CONSTRAINT `FK4E15C477DD089F57` FOREIGN KEY (`CASHOUTPAGE_ID`) REFERENCES `cashoutpage` (`ID`),
  CONSTRAINT `FK4E15C477E5957139` FOREIGN KEY (`ROOTACCOUNT_ID`) REFERENCES `account` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=75746449 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `credential`
--

LOCK TABLES `credential` WRITE;
/*!40000 ALTER TABLE `credential` DISABLE KEYS */;
INSERT INTO `credential` VALUES (60094,NULL,0,NULL,'APD Internal Orders',0.00,7,3825205,NULL,NULL,NULL,55801,31186,NULL,NULL,'ar@americanproduct.com','EMAIL','ar@americanproduct.com','EMAIL','sales@americanproduct.com','EMAIL','sales@americanproduct.com','EMAIL',NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(3825168,NULL,0,NULL,'Internal Orders',0.00,11,1543988,NULL,NULL,3969675,55801,31186,NULL,NULL,'ar@americanproduct.com','EMAIL','ar@americanproduct.com','EMAIL','sales@americanproduct.com','EMAIL','sales@americanproduct.com','EMAIL',NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(75746448,NULL,0,NULL,'Medwest Test',0.00,3,NULL,NULL,NULL,NULL,55801,31186,NULL,NULL,'ar@americanproduct.com','EMAIL','ar@americanproduct.com','EMAIL','sales@americanproduct.com','EMAIL','sales@americanproduct.com','EMAIL',NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,'28273',0,0,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `credential` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `credential_aud`
--

DROP TABLE IF EXISTS `credential_aud`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `credential_aud` (
  `ID` bigint(20) NOT NULL DEFAULT '0',
  `REV` bigint(20) NOT NULL AUTO_INCREMENT,
  `REVTYPE` decimal(3,0) DEFAULT NULL,
  `ACTIVATIONDATE` datetime DEFAULT NULL,
  `AUTHORIZEADDRESS` decimal(1,0) DEFAULT NULL,
  `CIDEST` varchar(255) DEFAULT NULL,
  `CIMTYPE` varchar(255) DEFAULT NULL,
  `CSREMAIL` varchar(255) DEFAULT NULL,
  `CSRPHONE` varchar(255) DEFAULT NULL,
  `CUSTOMEROUTGOINGSHAREDSECRET` varchar(255) DEFAULT NULL,
  `DEFAULTPUNCHOUTZIPCODE` varchar(255) DEFAULT NULL,
  `EDIRECCODE` varchar(255) DEFAULT NULL,
  `EDIVERSION` varchar(255) DEFAULT NULL,
  `EXPIRATIONDATE` datetime DEFAULT NULL,
  `IDEST` varchar(255) DEFAULT NULL,
  `IMTYPE` varchar(255) DEFAULT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `OUTGOINGSENDERSHAREDSECRET` varchar(255) DEFAULT NULL,
  `PARTNERID` varchar(255) DEFAULT NULL,
  `POACKDEST` varchar(255) DEFAULT NULL,
  `POACKMTYPE` varchar(255) DEFAULT NULL,
  `PUNCHOUTTYPE` varchar(255) DEFAULT NULL,
  `REQUIREZIPCODEINPUT` decimal(1,0) DEFAULT NULL,
  `SNDEST` varchar(255) DEFAULT NULL,
  `SNMTYPE` varchar(255) DEFAULT NULL,
  `STORECREDIT` decimal(19,2) DEFAULT NULL,
  `TERMS` varchar(255) DEFAULT NULL,
  `VENDORPUNCHOUTURL` varchar(255) DEFAULT NULL,
  `WILLCALLEDIOVERRIDE` decimal(1,0) DEFAULT NULL,
  `CATALOG_ID` bigint(20) DEFAULT NULL,
  `COMMUNICATIONMETHOD_ID` bigint(20) DEFAULT NULL,
  `PAYMENTINFORMATION_ID` bigint(20) DEFAULT NULL,
  `ROOTACCOUNT_ID` bigint(20) DEFAULT NULL,
  `SKUTYPE_ID` bigint(20) DEFAULT NULL,
  `WRAPANDLABEL` decimal(1,0) DEFAULT '0',
  `ADOT` decimal(1,0) DEFAULT '0',
  `RETURNCARTDOM` varchar(255) DEFAULT NULL,
  `RETURNCARTID` varchar(255) DEFAULT NULL,
  `VENDORNAME` varchar(255) DEFAULT NULL,
  `PUNCHOPALLOW` varchar(255) DEFAULT NULL,
  `DEFAULTSHIPFROMADDRESS_ID` bigint(20) DEFAULT NULL,
  `VENDORORDERURL` varchar(255) DEFAULT NULL,
  `DEPLOYMENTMODE` varchar(255) DEFAULT NULL,
  `CUSTOMFLENAME` varchar(255) DEFAULT NULL,
  `SPECIALFLENAME` varchar(255) DEFAULT NULL,
  `USRGUIDEFLENAME` varchar(255) DEFAULT NULL,
  `USER_ID` bigint(20) DEFAULT NULL,
  `CREDENTIALSELECTIONIMAGE` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`,`REV`),
  KEY `FKC4CDEC8DF74E053` (`REV`),
  CONSTRAINT `FKC4CDEC8DF74E053` FOREIGN KEY (`REV`) REFERENCES `revinfo` (`REV`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `credential_aud`
--

LOCK TABLES `credential_aud` WRITE;
/*!40000 ALTER TABLE `credential_aud` DISABLE KEYS */;
/*!40000 ALTER TABLE `credential_aud` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `credential_bulletin`
--

DROP TABLE IF EXISTS `credential_bulletin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `credential_bulletin` (
  `CREDENTIAL_ID` bigint(20) NOT NULL DEFAULT '0',
  `BULLETINS_ID` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`CREDENTIAL_ID`,`BULLETINS_ID`),
  KEY `FK3B63208F367C0B98` (`BULLETINS_ID`),
  CONSTRAINT `FK3B63208F367C0B98` FOREIGN KEY (`BULLETINS_ID`) REFERENCES `bulletin` (`ID`),
  CONSTRAINT `FK3B63208FAFD1BCFD` FOREIGN KEY (`CREDENTIAL_ID`) REFERENCES `credential` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `credential_bulletin`
--

LOCK TABLES `credential_bulletin` WRITE;
/*!40000 ALTER TABLE `credential_bulletin` DISABLE KEYS */;
/*!40000 ALTER TABLE `credential_bulletin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `credential_bulletin_aud`
--

DROP TABLE IF EXISTS `credential_bulletin_aud`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `credential_bulletin_aud` (
  `REV` bigint(20) NOT NULL AUTO_INCREMENT,
  `CREDENTIAL_ID` bigint(20) NOT NULL DEFAULT '0',
  `BULLETINS_ID` bigint(20) NOT NULL DEFAULT '0',
  `REVTYPE` decimal(3,0) DEFAULT NULL,
  PRIMARY KEY (`REV`,`CREDENTIAL_ID`,`BULLETINS_ID`),
  CONSTRAINT `FK9CDE6EE0DF74E053` FOREIGN KEY (`REV`) REFERENCES `revinfo` (`REV`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `credential_bulletin_aud`
--

LOCK TABLES `credential_bulletin_aud` WRITE;
/*!40000 ALTER TABLE `credential_bulletin_aud` DISABLE KEYS */;
/*!40000 ALTER TABLE `credential_bulletin_aud` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `credential_costcenter`
--

DROP TABLE IF EXISTS `credential_costcenter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `credential_costcenter` (
  `CREDENTIAL_ID` bigint(20) NOT NULL DEFAULT '0',
  `COSTCENTERS_ID` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`CREDENTIAL_ID`,`COSTCENTERS_ID`),
  KEY `FKAD50676A309BEEE` (`COSTCENTERS_ID`),
  CONSTRAINT `FKAD50676A309BEEE` FOREIGN KEY (`COSTCENTERS_ID`) REFERENCES `costcenter` (`ID`),
  CONSTRAINT `FKAD50676AAFD1BCFD` FOREIGN KEY (`CREDENTIAL_ID`) REFERENCES `credential` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `credential_costcenter`
--

LOCK TABLES `credential_costcenter` WRITE;
/*!40000 ALTER TABLE `credential_costcenter` DISABLE KEYS */;
/*!40000 ALTER TABLE `credential_costcenter` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `credential_costcenter_aud`
--

DROP TABLE IF EXISTS `credential_costcenter_aud`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `credential_costcenter_aud` (
  `REV` bigint(20) NOT NULL AUTO_INCREMENT,
  `CREDENTIAL_ID` bigint(20) NOT NULL DEFAULT '0',
  `COSTCENTERS_ID` bigint(20) NOT NULL DEFAULT '0',
  `REVTYPE` decimal(3,0) DEFAULT NULL,
  PRIMARY KEY (`REV`,`CREDENTIAL_ID`,`COSTCENTERS_ID`),
  CONSTRAINT `FK36C6D03BDF74E053` FOREIGN KEY (`REV`) REFERENCES `revinfo` (`REV`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `credential_costcenter_aud`
--

LOCK TABLES `credential_costcenter_aud` WRITE;
/*!40000 ALTER TABLE `credential_costcenter_aud` DISABLE KEYS */;
/*!40000 ALTER TABLE `credential_costcenter_aud` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `credential_department`
--

DROP TABLE IF EXISTS `credential_department`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `credential_department` (
  `CREDENTIAL_ID` bigint(20) NOT NULL DEFAULT '0',
  `DEPARTMENTS_ID` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`CREDENTIAL_ID`,`DEPARTMENTS_ID`),
  KEY `FK33A103A540E04AE` (`DEPARTMENTS_ID`),
  CONSTRAINT `FK33A103A540E04AE` FOREIGN KEY (`DEPARTMENTS_ID`) REFERENCES `department` (`ID`),
  CONSTRAINT `FK33A103AAFD1BCFD` FOREIGN KEY (`CREDENTIAL_ID`) REFERENCES `credential` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `credential_department`
--

LOCK TABLES `credential_department` WRITE;
/*!40000 ALTER TABLE `credential_department` DISABLE KEYS */;
/*!40000 ALTER TABLE `credential_department` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `credential_department_aud`
--

DROP TABLE IF EXISTS `credential_department_aud`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `credential_department_aud` (
  `REV` bigint(20) NOT NULL AUTO_INCREMENT,
  `CREDENTIAL_ID` bigint(20) NOT NULL DEFAULT '0',
  `DEPARTMENTS_ID` bigint(20) NOT NULL DEFAULT '0',
  `REVTYPE` decimal(3,0) DEFAULT NULL,
  PRIMARY KEY (`REV`,`CREDENTIAL_ID`,`DEPARTMENTS_ID`),
  CONSTRAINT `FKBB0F910BDF74E053` FOREIGN KEY (`REV`) REFERENCES `revinfo` (`REV`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `credential_department_aud`
--

LOCK TABLES `credential_department_aud` WRITE;
/*!40000 ALTER TABLE `credential_department_aud` DISABLE KEYS */;
/*!40000 ALTER TABLE `credential_department_aud` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `credential_limitapproval`
--

DROP TABLE IF EXISTS `credential_limitapproval`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `credential_limitapproval` (
  `CREDENTIAL_ID` bigint(20) DEFAULT NULL,
  `LIMITAPPROVAL_ID` bigint(20) DEFAULT NULL,
  KEY `FK20BF5536AFD1BCFD` (`CREDENTIAL_ID`),
  KEY `FK20BF5536C2092797` (`LIMITAPPROVAL_ID`),
  CONSTRAINT `FK20BF5536AFD1BCFD` FOREIGN KEY (`CREDENTIAL_ID`) REFERENCES `credential` (`ID`),
  CONSTRAINT `FK20BF5536C2092797` FOREIGN KEY (`LIMITAPPROVAL_ID`) REFERENCES `limitapproval` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `credential_limitapproval`
--

LOCK TABLES `credential_limitapproval` WRITE;
/*!40000 ALTER TABLE `credential_limitapproval` DISABLE KEYS */;
/*!40000 ALTER TABLE `credential_limitapproval` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `credential_limitapproval_aud`
--

DROP TABLE IF EXISTS `credential_limitapproval_aud`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `credential_limitapproval_aud` (
  `REV` bigint(20) NOT NULL AUTO_INCREMENT,
  `CREDENTIAL_ID` bigint(20) DEFAULT NULL,
  `LIMITAPPROVAL_ID` bigint(20) DEFAULT NULL,
  `REVTYPE` decimal(3,0) DEFAULT NULL,
  KEY `FK5A31F80775D1432` (`REV`),
  CONSTRAINT `FK5A31F80775D1432` FOREIGN KEY (`REV`) REFERENCES `revinfo` (`REV`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `credential_limitapproval_aud`
--

LOCK TABLES `credential_limitapproval_aud` WRITE;
/*!40000 ALTER TABLE `credential_limitapproval_aud` DISABLE KEYS */;
/*!40000 ALTER TABLE `credential_limitapproval_aud` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `credential_permission`
--

DROP TABLE IF EXISTS `credential_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `credential_permission` (
  `CREDENTIAL_ID` bigint(20) NOT NULL DEFAULT '0',
  `PERMISSIONS_ID` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`CREDENTIAL_ID`,`PERMISSIONS_ID`),
  KEY `FKB1D192B7BC8D8328` (`PERMISSIONS_ID`),
  CONSTRAINT `FKB1D192B7AFD1BCFD` FOREIGN KEY (`CREDENTIAL_ID`) REFERENCES `credential` (`ID`),
  CONSTRAINT `FKB1D192B7BC8D8328` FOREIGN KEY (`PERMISSIONS_ID`) REFERENCES `permission` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `credential_permission`
--

LOCK TABLES `credential_permission` WRITE;
/*!40000 ALTER TABLE `credential_permission` DISABLE KEYS */;
INSERT INTO `credential_permission` VALUES (60094,1509661),(60094,1509662),(3825168,1509662),(60094,1509663),(3825168,1509663),(3825168,1509666),(60094,1509667);
/*!40000 ALTER TABLE `credential_permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `credential_permission_aud`
--

DROP TABLE IF EXISTS `credential_permission_aud`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `credential_permission_aud` (
  `REV` bigint(20) NOT NULL AUTO_INCREMENT,
  `CREDENTIAL_ID` bigint(20) NOT NULL DEFAULT '0',
  `PERMISSIONS_ID` bigint(20) NOT NULL DEFAULT '0',
  `REVTYPE` decimal(3,0) DEFAULT NULL,
  PRIMARY KEY (`REV`,`CREDENTIAL_ID`,`PERMISSIONS_ID`),
  CONSTRAINT `FK74F78D08DF74E053` FOREIGN KEY (`REV`) REFERENCES `revinfo` (`REV`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `credential_permission_aud`
--

LOCK TABLES `credential_permission_aud` WRITE;
/*!40000 ALTER TABLE `credential_permission_aud` DISABLE KEYS */;
/*!40000 ALTER TABLE `credential_permission_aud` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `credential_ponumber`
--

DROP TABLE IF EXISTS `credential_ponumber`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `credential_ponumber` (
  `CREDENTIAL_ID` bigint(20) NOT NULL DEFAULT '0',
  `BLANKETPOS_ID` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`CREDENTIAL_ID`,`BLANKETPOS_ID`),
  KEY `FK799F0110CE415F34` (`BLANKETPOS_ID`),
  CONSTRAINT `FK799F0110AFD1BCFD` FOREIGN KEY (`CREDENTIAL_ID`) REFERENCES `credential` (`ID`),
  CONSTRAINT `FK799F0110CE415F34` FOREIGN KEY (`BLANKETPOS_ID`) REFERENCES `ponumber` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `credential_ponumber`
--

LOCK TABLES `credential_ponumber` WRITE;
/*!40000 ALTER TABLE `credential_ponumber` DISABLE KEYS */;
/*!40000 ALTER TABLE `credential_ponumber` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `credential_propertytype`
--

DROP TABLE IF EXISTS `credential_propertytype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `credential_propertytype` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `VALUE` varchar(255) DEFAULT NULL,
  `VERSION` bigint(20) DEFAULT NULL,
  `TYPE_ID` bigint(20) DEFAULT NULL,
  `CRED_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `C_P_CRED_ID` (`CRED_ID`) USING BTREE,
  KEY `C_P_TYPE_ID` (`TYPE_ID`) USING BTREE,
  CONSTRAINT `FK2165219745A534A6` FOREIGN KEY (`CRED_ID`) REFERENCES `credential` (`ID`),
  CONSTRAINT `FK21652197B66C2309` FOREIGN KEY (`TYPE_ID`) REFERENCES `credentialpropertytype` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `credential_propertytype`
--

LOCK TABLES `credential_propertytype` WRITE;
/*!40000 ALTER TABLE `credential_propertytype` DISABLE KEYS */;
/*!40000 ALTER TABLE `credential_propertytype` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `credential_propertytype_aud`
--

DROP TABLE IF EXISTS `credential_propertytype_aud`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `credential_propertytype_aud` (
  `ID` bigint(20) NOT NULL DEFAULT '0',
  `REV` bigint(20) NOT NULL AUTO_INCREMENT,
  `REVTYPE` decimal(3,0) DEFAULT NULL,
  `VALUE` varchar(255) DEFAULT NULL,
  `TYPE_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`,`REV`),
  KEY `FKC068ABE8DF74E053` (`REV`),
  CONSTRAINT `FKC068ABE8DF74E053` FOREIGN KEY (`REV`) REFERENCES `revinfo` (`REV`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `credential_propertytype_aud`
--

LOCK TABLES `credential_propertytype_aud` WRITE;
/*!40000 ALTER TABLE `credential_propertytype_aud` DISABLE KEYS */;
/*!40000 ALTER TABLE `credential_propertytype_aud` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `credential_role`
--

DROP TABLE IF EXISTS `credential_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `credential_role` (
  `CREDENTIAL_ID` bigint(20) NOT NULL DEFAULT '0',
  `ROLES_ID` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`CREDENTIAL_ID`,`ROLES_ID`),
  KEY `FK7D571B5E46C1C336` (`ROLES_ID`),
  CONSTRAINT `FK7D571B5E46C1C336` FOREIGN KEY (`ROLES_ID`) REFERENCES `role` (`ID`),
  CONSTRAINT `FK7D571B5EAFD1BCFD` FOREIGN KEY (`CREDENTIAL_ID`) REFERENCES `credential` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `credential_role`
--

LOCK TABLES `credential_role` WRITE;
/*!40000 ALTER TABLE `credential_role` DISABLE KEYS */;
/*!40000 ALTER TABLE `credential_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `credential_role_aud`
--

DROP TABLE IF EXISTS `credential_role_aud`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `credential_role_aud` (
  `REV` bigint(20) NOT NULL AUTO_INCREMENT,
  `CREDENTIAL_ID` bigint(20) NOT NULL DEFAULT '0',
  `ROLES_ID` bigint(20) NOT NULL DEFAULT '0',
  `REVTYPE` decimal(3,0) DEFAULT NULL,
  PRIMARY KEY (`REV`,`CREDENTIAL_ID`,`ROLES_ID`),
  CONSTRAINT `FK7BAA6A2FDF74E053` FOREIGN KEY (`REV`) REFERENCES `revinfo` (`REV`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `credential_role_aud`
--

LOCK TABLES `credential_role_aud` WRITE;
/*!40000 ALTER TABLE `credential_role_aud` DISABLE KEYS */;
/*!40000 ALTER TABLE `credential_role_aud` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `credentialpropertytype`
--

DROP TABLE IF EXISTS `credentialpropertytype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `credentialpropertytype` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `VERSION` bigint(20) DEFAULT NULL,
  `DEFAULTVALUE` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UC_CREDENTIALPROPERTYTYPE_1` (`NAME`)
) ENGINE=InnoDB AUTO_INCREMENT=156609547 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `credentialpropertytype`
--

LOCK TABLES `credentialpropertytype` WRITE;
/*!40000 ALTER TABLE `credentialpropertytype` DISABLE KEYS */;
INSERT INTO `credentialpropertytype` VALUES (31064,'[deprecated] information',0,NULL),(31065,'[deprecated] type',0,NULL),(31066,'session timeout',0,NULL),(31067,'return button text',0,NULL),(31068,'deployment',0,NULL),(31069,'[deprecated] vendor path prefix',0,NULL),(31070,'[deprecated] display name for cXML',0,NULL),(31071,'[deprecated] button label',0,NULL),(31072,'AR GL account',0,NULL),(31073,'AR subaccount',0,NULL),(31074,'contract number',0,NULL),(31075,'DUNS number (for ariba)',0,NULL),(31076,'[deprecated] incoming FROM identity',0,NULL),(31077,'[deprecated] punch/order HTTPS cert',0,NULL),(31078,'[deprecated] punchout URL',0,NULL),(31079,'[deprecated] suppliersetup URL',0,NULL),(31080,'[deprecated] punchoutexit URL',0,NULL),(31081,'[deprecated] orderrequest URL',0,NULL),(31082,'[deprecated] confirm URL',0,NULL),(31083,'[deprecated] invoice URL',0,NULL),(31084,'xCBL buyer MPID',0,NULL),(31085,'xCBL seller MPID',0,NULL),(31086,'[deprecated] shop again URL base',0,NULL),(31087,'[deprecated] HTTP root',0,NULL),(31088,'[deprecated] HTTPS root',0,NULL),(31089,'[deprecated] HTTP path',0,NULL),(31090,'[deprecated] HTTPS path',0,NULL),(31091,'[deprecated] image: header',0,NULL),(31092,'[deprecated] login frame template',0,NULL),(31093,'punch frame template',0,NULL),(31094,'[deprecated] from credential',0,NULL),(31095,'[deprecated] from punch identity',0,NULL),(31096,'[deprecated] from order identity',0,NULL),(31097,'[deprecated] to credential',0,NULL),(31098,'[deprecated] to punch identity',0,NULL),(31099,'[deprecated] to order identity',0,NULL),(31100,'[deprecated] sender credential',0,NULL),(31101,'[deprecated] sender credential punch user',0,NULL),(31102,'[deprecated] sender credential punch password',0,NULL),(31103,'[deprecated] sender credential order user',0,NULL),(31104,'[deprecated] sender credential order password',0,NULL),(31105,'[deprecated] credit card processor',0,NULL),(31106,'batch credit card transactions',0,NULL),(31107,'credit card merchant code',0,NULL),(31108,'[deprecated] vendor connection login',0,NULL),(31109,'[deprecated] vendor connection password',0,NULL),(31110,'[deprecated] vendor connection email',0,NULL),(31111,'email quotes as saved list',0,NULL),(31112,'merchandise discount',0,NULL),(31113,'discount error allowance',0,NULL),(31114,'cost of goods',0,NULL),(31115,'cc proc perc of order',0,NULL),(31116,'cc proc flat per order',0,NULL),(31117,'[deprecated] rev.net proc perc of order',0,NULL),(31118,'[deprecated] allow cXML CREATE operation',0,NULL),(31119,'[deprecated] allow cXML EDIT operation',0,NULL),(31120,'[deprecated] allow cXML INSPECT operation',0,NULL),(31121,'send PDF attachment',0,NULL),(31122,'send TXT attachment',0,NULL),(31123,'send buyer email to vendor',0,NULL),(31124,'[deprecated] send declines to',0,NULL),(31125,'email template',0,NULL),(31126,'SKU on emails',0,NULL),(31127,'allow additional emails',0,NULL),(31128,'cc payments',0,NULL),(31129,'cc payments allowed types',0,NULL),(31130,'invoice payments',0,NULL),(31131,'invoice number required',0,NULL),(31132,'use billing PO for invoices',0,NULL),(31133,'use default credit card number',0,NULL),(31134,'charge sales tax',0,'yes'),(31135,'state exemptions for sales tax',0,NULL),(31136,'zip code exceptions for sales tax',0,'none'),(31137,'use alternate shiptos',0,NULL),(31138,'use alternate billtos',0,NULL),(31139,'require authorized shiptos',0,NULL),(31140,'blank address 2/3 when authorized',0,NULL),(31141,'allow rejected items to ship',0,'yes'),(31142,'use last shipto',0,NULL),(31143,'use common units of measure',0,NULL),(31144,'use ISO units of measure',0,NULL),(31145,'admin orders: pick supplier',0,NULL),(31146,'protect shiptos',0,NULL),(31147,'[deprecated] concatenate item number to description',0,NULL),(31148,'allow attachments to orders',0,'no'),(31149,'[deprecated] prompt for zip code',0,NULL),(31150,'[deprecated] zip code to send for every punchout',0,NULL),(31151,'unscheduled service fee',0,NULL),(31152,'days to keep order on hold',0,NULL),(31153,'default order history timeframe',0,NULL),(31154,'[deprecated] require password change on prompt',0,NULL),(31155,'default address when adding user',0,NULL),(31156,'allow PO editing',0,'yes'),(31157,'[deprecated] send cXML item unit price',0,NULL),(31158,'use order comments',0,NULL),(31159,'retry refused order',0,NULL),(31160,'bulletin button text',0,NULL),(31161,'spending limit message',0,NULL),(31162,'EDI: ISA sender qualifier',0,NULL),(31163,'EDI: ISA sender ID',0,NULL),(31164,'EDI: ISA receiver qualifier',0,NULL),(31165,'EDI: ISA receiver ID',0,NULL),(31166,'EDI: GS sender ID',0,NULL),(31167,'EDI: GS receiver ID',0,NULL),(31168,'EDI: field separator',0,NULL),(31169,'EDI: record separator',0,NULL),(31170,'auto PO evaluation',0,NULL),(31171,'[deprecated] solomon vendor ID',0,NULL),(31172,'default invoice terms',0,NULL),(31173,'allowed invoice terms',0,NULL),(31174,'bank ABA number',0,NULL),(31175,'bank account number',0,NULL),(31176,'billto heading title',0,NULL),(31177,'shipto heading title',0,NULL),(31178,'multiple billto pull down heading',0,NULL),(31179,'multiple shipto pull down heading',0,NULL),(31180,'notes/comments',0,NULL),(31181,'shipping rate',0,NULL),(4147122,'US Account #',0,NULL),(4147123,'check all facilities (ADOT)',0,'yes'),(4147124,'zip code for stock check',0,NULL),(13613633,'APD sales person',1,'Nikki Bradley'),(17702743,'cost-price discrepancy threshold',0,'0.08'),(17702746,'needs usps/ussco workflow',0,NULL),(17702759,'allowed country codes',0,NULL),(17702760,'allow military zip codes',0,'no'),(17709296,'cxml send unit price',0,NULL),(17709297,'cxml default price margin',0,NULL),(17709298,'minimum order amount',0,'0.00'),(17709299,'minimum order fee',0,'0.00'),(17709300,'handling fee',0,NULL),(17709301,'route code',0,NULL),(17709304,'minimum order amount required',1,'0.00'),(17709308,'orders placed under marquette',0,NULL),(39304641,'fulfillment',0,'speed'),(39304642,'APD_AccountingID',0,NULL),(53419250,'default shipto email contact',0,NULL),(53419251,'set line item shipto on outbound order',0,NULL),(102556574,'orders placed under APD/Marquette',1,'no'),(102582462,'concatenate order item line # to description',0,NULL),(106360281,'EDI: order type identification',0,'ZUS1'),(106360282,'EDI: supplier number identification',0,'373030'),(106360283,'EDI: departmnet number identification',0,NULL),(110066902,'show search results summary',1,'no'),(115603866,'cc preauth strategy (full, one, none)',0,'full'),(116932954,'sku on shopping cart',0,'B'),(156609546,'smart search use dealer description',0,'false');
/*!40000 ALTER TABLE `credentialpropertytype` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `credentialpropertytype_aud`
--

DROP TABLE IF EXISTS `credentialpropertytype_aud`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `credentialpropertytype_aud` (
  `ID` bigint(20) NOT NULL DEFAULT '0',
  `REV` bigint(20) NOT NULL AUTO_INCREMENT,
  `REVTYPE` decimal(3,0) DEFAULT NULL,
  `DEFAULTVALUE` varchar(255) DEFAULT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`,`REV`),
  KEY `FKD2E86797DF74E053` (`REV`),
  CONSTRAINT `FKD2E86797DF74E053` FOREIGN KEY (`REV`) REFERENCES `revinfo` (`REV`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `credentialpropertytype_aud`
--

LOCK TABLES `credentialpropertytype_aud` WRITE;
/*!40000 ALTER TABLE `credentialpropertytype_aud` DISABLE KEYS */;
/*!40000 ALTER TABLE `credentialpropertytype_aud` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `credentialxvendor`
--

DROP TABLE IF EXISTS `credentialxvendor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `credentialxvendor` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `ACCOUNTID` varchar(255) NOT NULL,
  `VERSION` bigint(20) DEFAULT NULL,
  `CREDENTIAL_ID` bigint(20) DEFAULT NULL,
  `VENDOR_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `CXV_CREDENTIAL_ID` (`CREDENTIAL_ID`) USING BTREE,
  KEY `CXV_VENDOR_ID` (`VENDOR_ID`) USING BTREE,
  CONSTRAINT `FK17F98769AFD1BCFD` FOREIGN KEY (`CREDENTIAL_ID`) REFERENCES `credential` (`ID`),
  CONSTRAINT `FK17F98769D50C94DD` FOREIGN KEY (`VENDOR_ID`) REFERENCES `vendor` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `credentialxvendor`
--

LOCK TABLES `credentialxvendor` WRITE;
/*!40000 ALTER TABLE `credentialxvendor` DISABLE KEYS */;
/*!40000 ALTER TABLE `credentialxvendor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `credentialxvendor_aud`
--

DROP TABLE IF EXISTS `credentialxvendor_aud`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `credentialxvendor_aud` (
  `ID` bigint(20) NOT NULL DEFAULT '0',
  `REV` bigint(20) NOT NULL AUTO_INCREMENT,
  `REVTYPE` decimal(3,0) DEFAULT NULL,
  `ACCOUNTID` varchar(255) DEFAULT NULL,
  `CREDENTIAL_ID` bigint(20) DEFAULT NULL,
  `VENDOR_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`,`REV`),
  KEY `FKE7D1D8BADF74E053` (`REV`),
  CONSTRAINT `FKE7D1D8BADF74E053` FOREIGN KEY (`REV`) REFERENCES `revinfo` (`REV`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `credentialxvendor_aud`
--

LOCK TABLES `credentialxvendor_aud` WRITE;
/*!40000 ALTER TABLE `credentialxvendor_aud` DISABLE KEYS */;
/*!40000 ALTER TABLE `credentialxvendor_aud` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `csvuploadeditem`
--

DROP TABLE IF EXISTS `csvuploadeditem`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `csvuploadeditem` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `VERSION` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `csvuploadeditem`
--

LOCK TABLES `csvuploadeditem` WRITE;
/*!40000 ALTER TABLE `csvuploadeditem` DISABLE KEYS */;
/*!40000 ALTER TABLE `csvuploadeditem` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `csvuploadeditemfield`
--

DROP TABLE IF EXISTS `csvuploadeditemfield`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `csvuploadeditemfield` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `KEY` varchar(255) NOT NULL,
  `VALUE` varchar(4000) NOT NULL,
  `VERSION` bigint(20) DEFAULT NULL,
  `UPLOADEDITEM_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `UPLOADEDITEM_FIELD_IDX` (`UPLOADEDITEM_ID`) USING BTREE,
  CONSTRAINT `FK707855E14A59795D` FOREIGN KEY (`UPLOADEDITEM_ID`) REFERENCES `csvuploadeditem` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `csvuploadeditemfield`
--

LOCK TABLES `csvuploadeditemfield` WRITE;
/*!40000 ALTER TABLE `csvuploadeditemfield` DISABLE KEYS */;
/*!40000 ALTER TABLE `csvuploadeditemfield` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customercost`
--

DROP TABLE IF EXISTS `customercost`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customercost` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `VALUE` decimal(19,2) DEFAULT NULL,
  `VERSION` bigint(20) DEFAULT NULL,
  `CUSTOMER_ID` bigint(20) DEFAULT NULL,
  `ITEM_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `CC_ITEM_IX` (`ITEM_ID`) USING BTREE,
  KEY `CUSTCOST_CUSTOMER` (`CUSTOMER_ID`) USING BTREE,
  KEY `CUSTCOST_ITEMCUST_IDX` (`ITEM_ID`,`CUSTOMER_ID`) USING BTREE,
  CONSTRAINT `FK3F6DD8EBD4B669BD` FOREIGN KEY (`ITEM_ID`) REFERENCES `item` (`ID`),
  CONSTRAINT `FK3F6DD8EBDE9C8386` FOREIGN KEY (`CUSTOMER_ID`) REFERENCES `account` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customercost`
--

LOCK TABLES `customercost` WRITE;
/*!40000 ALTER TABLE `customercost` DISABLE KEYS */;
/*!40000 ALTER TABLE `customercost` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customerorder`
--

DROP TABLE IF EXISTS `customerorder`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customerorder` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `ESTIMATEDSHIPPING` varchar(255) DEFAULT NULL,
  `ESTIMATEDSHIPPINGAMOUNT` decimal(19,2) DEFAULT NULL,
  `MAXIMUMTAXTOCHARGE` decimal(19,2) DEFAULT NULL,
  `OPERATIONALLOWED` varchar(255) DEFAULT NULL,
  `ORDERDATE` datetime DEFAULT NULL,
  `ORDERTOTAL` decimal(19,2) NOT NULL,
  `PAYMENTTRANSACTIONKEY` varchar(255) DEFAULT NULL,
  `TAXASSIGNED` decimal(1,0) DEFAULT NULL,
  `VERSION` bigint(20) DEFAULT NULL,
  `ACCOUNT_ID` bigint(20) DEFAULT NULL,
  `ADDRESS_ID` bigint(20) DEFAULT NULL,
  `CARDINFORMATION_ID` bigint(20) DEFAULT NULL,
  `CREDENTIAL_ID` bigint(20) DEFAULT NULL,
  `PARENTORDER_ID` bigint(20) DEFAULT NULL,
  `PAYMENTINFORMATION_ID` bigint(20) DEFAULT NULL,
  `STATUS_ID` bigint(20) DEFAULT NULL,
  `TYPE_ID` bigint(20) DEFAULT NULL,
  `USER_ID` bigint(20) DEFAULT NULL,
  `SHIPDATE` datetime DEFAULT NULL,
  `DELIVERYDATE` datetime DEFAULT NULL,
  `CANCELDATE` datetime DEFAULT NULL,
  `SHIPTOEXTRASARTIFACT` varchar(4000) DEFAULT NULL,
  `ORDEREXTRASARTIFACT` varchar(4000) DEFAULT NULL,
  `MERCHANDISETYPECODE` varchar(255) DEFAULT NULL,
  `REQUESTEDDELIVERYDATE` datetime DEFAULT NULL,
  `BUYERCOOKIE` varchar(255) DEFAULT NULL,
  `CURRENCYCODE` varchar(255) DEFAULT NULL,
  `ASSIGNEDCOSTCENTER_ID` bigint(20) DEFAULT NULL,
  `CONTRACTNUMBER` varchar(255) DEFAULT NULL,
  `APDSALESPERSON` varchar(255) DEFAULT NULL,
  `APPROVALCOMMENT` varchar(4000) DEFAULT NULL,
  `WILLCALLEDIOVERRIDE` decimal(1,0) NOT NULL,
  `REQUISITIONNAME` varchar(255) DEFAULT NULL,
  `TERMS` varchar(255) DEFAULT NULL,
  `ORDERPAYLOADID` varchar(255) DEFAULT NULL,
  `ORDERORIGIN` varchar(50) DEFAULT NULL,
  `WRAPANDLABEL` decimal(1,0) NOT NULL DEFAULT '0',
  `ADOT` decimal(1,0) NOT NULL DEFAULT '0',
  `CUSTOMERREVISIONDATE` datetime DEFAULT NULL,
  `CUSTOMERREVISIONNUMBER` bigint(20) DEFAULT NULL,
  `ASSIGNEDCOSTCENTERLABEL` varchar(255) DEFAULT NULL,
  `RESENDATTEMPTS` bigint(20) DEFAULT NULL,
  `SHIPNOLATERDATE` datetime DEFAULT NULL,
  `SHIPNOTBEFOREDATE` datetime DEFAULT NULL,
  `PASSEDINITIALVALIDATION` decimal(1,0) NOT NULL,
  `PAYMENTSTATUS_ID` bigint(20) DEFAULT NULL,
  `CUSTOMERSTATUS_ID` bigint(20) DEFAULT NULL,
  `EXPIRYDATE` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `CUSTORDER_ORDERDATE_IDX` (`ORDERDATE`) USING BTREE,
  KEY `INDX_11` (`ACCOUNT_ID`) USING BTREE,
  KEY `INDX_12` (`USER_ID`) USING BTREE,
  KEY `INDX_13` (`PAYMENTINFORMATION_ID`) USING BTREE,
  KEY `INDX_14` (`ADDRESS_ID`) USING BTREE,
  KEY `INDX_15` (`CREDENTIAL_ID`) USING BTREE,
  KEY `INDX_16` (`STATUS_ID`) USING BTREE,
  KEY `INDX_17` (`TYPE_ID`) USING BTREE,
  KEY `INDX_18` (`ASSIGNEDCOSTCENTER_ID`) USING BTREE,
  KEY `FKAEF781F05F7CC357` (`CARDINFORMATION_ID`),
  KEY `FKAEF781F08B76AAE3` (`PARENTORDER_ID`),
  KEY `FKAEF781F0971F1FDF` (`PAYMENTSTATUS_ID`),
  KEY `FKAEF781F0EB7E507` (`CUSTOMERSTATUS_ID`),
  CONSTRAINT `FKAEF781F0185A22D7` FOREIGN KEY (`ACCOUNT_ID`) REFERENCES `account` (`ID`),
  CONSTRAINT `FKAEF781F04C0A74E5` FOREIGN KEY (`TYPE_ID`) REFERENCES `ordertype` (`ID`),
  CONSTRAINT `FKAEF781F04E9ECF4C` FOREIGN KEY (`USER_ID`) REFERENCES `systemuser` (`ID`),
  CONSTRAINT `FKAEF781F05F7CC357` FOREIGN KEY (`CARDINFORMATION_ID`) REFERENCES `cardinformation` (`ID`),
  CONSTRAINT `FKAEF781F07ED1037D` FOREIGN KEY (`PAYMENTINFORMATION_ID`) REFERENCES `paymentinformation` (`ID`),
  CONSTRAINT `FKAEF781F08B76AAE3` FOREIGN KEY (`PARENTORDER_ID`) REFERENCES `customerorder` (`ID`),
  CONSTRAINT `FKAEF781F0971F1FDF` FOREIGN KEY (`PAYMENTSTATUS_ID`) REFERENCES `orderstatus` (`ID`),
  CONSTRAINT `FKAEF781F099EEF977` FOREIGN KEY (`ADDRESS_ID`) REFERENCES `address` (`ID`),
  CONSTRAINT `FKAEF781F0AFD1BCFD` FOREIGN KEY (`CREDENTIAL_ID`) REFERENCES `credential` (`ID`),
  CONSTRAINT `FKAEF781F0D6EA6B25` FOREIGN KEY (`STATUS_ID`) REFERENCES `orderstatus` (`ID`),
  CONSTRAINT `FKAEF781F0EB7E507` FOREIGN KEY (`CUSTOMERSTATUS_ID`) REFERENCES `orderstatus` (`ID`),
  CONSTRAINT `PK_ASSIGNEDCOSTCENTER_ID` FOREIGN KEY (`ASSIGNEDCOSTCENTER_ID`) REFERENCES `accxcredxuser_ccenter` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customerorder`
--

LOCK TABLES `customerorder` WRITE;
/*!40000 ALTER TABLE `customerorder` DISABLE KEYS */;
/*!40000 ALTER TABLE `customerorder` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customerorder_taxtype`
--

DROP TABLE IF EXISTS `customerorder_taxtype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customerorder_taxtype` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `VALUE` decimal(19,6) NOT NULL,
  `VERSION` bigint(20) DEFAULT NULL,
  `ORDER_ID` bigint(20) DEFAULT NULL,
  `TYPE_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FKD872C6168E1EC39` (`ORDER_ID`),
  KEY `FKD872C6162B5202` (`TYPE_ID`),
  CONSTRAINT `FKD872C6162B5202` FOREIGN KEY (`TYPE_ID`) REFERENCES `taxtype` (`ID`),
  CONSTRAINT `FKD872C6168E1EC39` FOREIGN KEY (`ORDER_ID`) REFERENCES `customerorder` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customerorder_taxtype`
--

LOCK TABLES `customerorder_taxtype` WRITE;
/*!40000 ALTER TABLE `customerorder_taxtype` DISABLE KEYS */;
/*!40000 ALTER TABLE `customerorder_taxtype` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customerorderrequest`
--

DROP TABLE IF EXISTS `customerorderrequest`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customerorderrequest` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `TOKEN` varchar(255) NOT NULL,
  `VERSION` bigint(20) DEFAULT NULL,
  `CUSTOMERORDER_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UC_CUSTOMERORDERREQUEST_1` (`TOKEN`),
  KEY `FK5C66FA7F26CAE17` (`CUSTOMERORDER_ID`),
  CONSTRAINT `FK5C66FA7F26CAE17` FOREIGN KEY (`CUSTOMERORDER_ID`) REFERENCES `customerorder` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customerorderrequest`
--

LOCK TABLES `customerorderrequest` WRITE;
/*!40000 ALTER TABLE `customerorderrequest` DISABLE KEYS */;
/*!40000 ALTER TABLE `customerorderrequest` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customeroutfromcred`
--

DROP TABLE IF EXISTS `customeroutfromcred`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customeroutfromcred` (
  `CREDENTIAL_ID` bigint(20) NOT NULL DEFAULT '0',
  `CUSTOMEROUTOINGFROMCRED_ID` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`CREDENTIAL_ID`,`CUSTOMEROUTOINGFROMCRED_ID`),
  UNIQUE KEY `CUSTOMEROUTOINGFROMCRED_ID_UC` (`CUSTOMEROUTOINGFROMCRED_ID`),
  CONSTRAINT `FKFB1F76C8440D5C8D` FOREIGN KEY (`CUSTOMEROUTOINGFROMCRED_ID`) REFERENCES `cxmlcredential` (`ID`),
  CONSTRAINT `FKFB1F76C8AFD1BCFD` FOREIGN KEY (`CREDENTIAL_ID`) REFERENCES `credential` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customeroutfromcred`
--

LOCK TABLES `customeroutfromcred` WRITE;
/*!40000 ALTER TABLE `customeroutfromcred` DISABLE KEYS */;
/*!40000 ALTER TABLE `customeroutfromcred` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customeroutfromcred_aud`
--

DROP TABLE IF EXISTS `customeroutfromcred_aud`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customeroutfromcred_aud` (
  `REV` bigint(20) NOT NULL AUTO_INCREMENT,
  `CREDENTIAL_ID` bigint(20) NOT NULL DEFAULT '0',
  `CUSTOMEROUTOINGFROMCRED_ID` bigint(20) NOT NULL DEFAULT '0',
  `REVTYPE` decimal(3,0) DEFAULT NULL,
  PRIMARY KEY (`REV`,`CREDENTIAL_ID`,`CUSTOMEROUTOINGFROMCRED_ID`),
  CONSTRAINT `FKDDA30099DF74E053` FOREIGN KEY (`REV`) REFERENCES `revinfo` (`REV`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customeroutfromcred_aud`
--

LOCK TABLES `customeroutfromcred_aud` WRITE;
/*!40000 ALTER TABLE `customeroutfromcred_aud` DISABLE KEYS */;
/*!40000 ALTER TABLE `customeroutfromcred_aud` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customeroutsendercred`
--

DROP TABLE IF EXISTS `customeroutsendercred`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customeroutsendercred` (
  `CREDENTIAL_ID` bigint(20) NOT NULL DEFAULT '0',
  `CUSTOMEROUTOINGSENDERCRED_ID` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`CREDENTIAL_ID`,`CUSTOMEROUTOINGSENDERCRED_ID`),
  KEY `FK875A4FD3F111D462` (`CUSTOMEROUTOINGSENDERCRED_ID`),
  CONSTRAINT `FK875A4FD3AFD1BCFD` FOREIGN KEY (`CREDENTIAL_ID`) REFERENCES `credential` (`ID`),
  CONSTRAINT `FK875A4FD3F111D462` FOREIGN KEY (`CUSTOMEROUTOINGSENDERCRED_ID`) REFERENCES `cxmlcredential` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customeroutsendercred`
--

LOCK TABLES `customeroutsendercred` WRITE;
/*!40000 ALTER TABLE `customeroutsendercred` DISABLE KEYS */;
/*!40000 ALTER TABLE `customeroutsendercred` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customeroutsendercred_aud`
--

DROP TABLE IF EXISTS `customeroutsendercred_aud`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customeroutsendercred_aud` (
  `REV` bigint(20) NOT NULL AUTO_INCREMENT,
  `CREDENTIAL_ID` bigint(20) NOT NULL DEFAULT '0',
  `CUSTOMEROUTOINGSENDERCRED_ID` bigint(20) NOT NULL DEFAULT '0',
  `REVTYPE` decimal(3,0) DEFAULT NULL,
  PRIMARY KEY (`REV`,`CREDENTIAL_ID`,`CUSTOMEROUTOINGSENDERCRED_ID`),
  CONSTRAINT `FKAF645C24DF74E053` FOREIGN KEY (`REV`) REFERENCES `revinfo` (`REV`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customeroutsendercred_aud`
--

LOCK TABLES `customeroutsendercred_aud` WRITE;
/*!40000 ALTER TABLE `customeroutsendercred_aud` DISABLE KEYS */;
/*!40000 ALTER TABLE `customeroutsendercred_aud` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customerouttocred`
--

DROP TABLE IF EXISTS `customerouttocred`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customerouttocred` (
  `CREDENTIAL_ID` bigint(20) NOT NULL DEFAULT '0',
  `CUSTOMEROUTOINGTOCRED_ID` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`CREDENTIAL_ID`,`CUSTOMEROUTOINGTOCRED_ID`),
  UNIQUE KEY `SYS_C0022729` (`CUSTOMEROUTOINGTOCRED_ID`),
  CONSTRAINT `FKBB8919AFD1BCFD` FOREIGN KEY (`CREDENTIAL_ID`) REFERENCES `credential` (`ID`),
  CONSTRAINT `FKBB8919CA6ABBDC` FOREIGN KEY (`CUSTOMEROUTOINGTOCRED_ID`) REFERENCES `cxmlcredential` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customerouttocred`
--

LOCK TABLES `customerouttocred` WRITE;
/*!40000 ALTER TABLE `customerouttocred` DISABLE KEYS */;
/*!40000 ALTER TABLE `customerouttocred` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customerouttocred_aud`
--

DROP TABLE IF EXISTS `customerouttocred_aud`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customerouttocred_aud` (
  `REV` bigint(20) NOT NULL AUTO_INCREMENT,
  `CREDENTIAL_ID` bigint(20) NOT NULL DEFAULT '0',
  `CUSTOMEROUTOINGTOCRED_ID` bigint(20) NOT NULL DEFAULT '0',
  `REVTYPE` decimal(3,0) DEFAULT NULL,
  PRIMARY KEY (`REV`,`CREDENTIAL_ID`,`CUSTOMEROUTOINGTOCRED_ID`),
  CONSTRAINT `FKB75B826ADF74E053` FOREIGN KEY (`REV`) REFERENCES `revinfo` (`REV`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customerouttocred_aud`
--

LOCK TABLES `customerouttocred_aud` WRITE;
/*!40000 ALTER TABLE `customerouttocred_aud` DISABLE KEYS */;
/*!40000 ALTER TABLE `customerouttocred_aud` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customerspecificiconurl`
--

DROP TABLE IF EXISTS `customerspecificiconurl`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customerspecificiconurl` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `OVERRIDEICONURL` varchar(255) DEFAULT NULL,
  `VERSION` bigint(20) DEFAULT NULL,
  `CLASSIFICATIONTYPE_ID` bigint(20) DEFAULT NULL,
  `PROPERTYTYPE_ID` bigint(20) DEFAULT NULL,
  `ACCOUNT_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FKD7C87CC6185A22D7` (`ACCOUNT_ID`),
  KEY `FKD7C87CC68CC90C70` (`CLASSIFICATIONTYPE_ID`),
  KEY `FKD7C87CC6F5826A90` (`PROPERTYTYPE_ID`),
  CONSTRAINT `FKD7C87CC6185A22D7` FOREIGN KEY (`ACCOUNT_ID`) REFERENCES `account` (`ID`),
  CONSTRAINT `FKD7C87CC68CC90C70` FOREIGN KEY (`CLASSIFICATIONTYPE_ID`) REFERENCES `itemclassificationtype` (`ID`),
  CONSTRAINT `FKD7C87CC6F5826A90` FOREIGN KEY (`PROPERTYTYPE_ID`) REFERENCES `itempropertytype` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customerspecificiconurl`
--

LOCK TABLES `customerspecificiconurl` WRITE;
/*!40000 ALTER TABLE `customerspecificiconurl` DISABLE KEYS */;
/*!40000 ALTER TABLE `customerspecificiconurl` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cxcpt_aud`
--

DROP TABLE IF EXISTS `cxcpt_aud`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cxcpt_aud` (
  `REV` bigint(20) NOT NULL AUTO_INCREMENT,
  `CRED_ID` bigint(20) NOT NULL DEFAULT '0',
  `ID` bigint(20) NOT NULL DEFAULT '0',
  `REVTYPE` decimal(3,0) DEFAULT NULL,
  PRIMARY KEY (`REV`,`CRED_ID`,`ID`),
  CONSTRAINT `FK42BA79E3DF74E053` FOREIGN KEY (`REV`) REFERENCES `revinfo` (`REV`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cxcpt_aud`
--

LOCK TABLES `cxcpt_aud` WRITE;
/*!40000 ALTER TABLE `cxcpt_aud` DISABLE KEYS */;
/*!40000 ALTER TABLE `cxcpt_aud` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cxcxpt_aud`
--

DROP TABLE IF EXISTS `cxcxpt_aud`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cxcxpt_aud` (
  `ID` bigint(20) NOT NULL DEFAULT '0',
  `REV` bigint(20) NOT NULL AUTO_INCREMENT,
  `REVTYPE` decimal(3,0) DEFAULT NULL,
  `PARAMETER` varchar(255) DEFAULT NULL,
  `CATALOG_ID` bigint(20) DEFAULT NULL,
  `TYPE_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`,`REV`),
  KEY `FKB62881FFDF74E053` (`REV`),
  CONSTRAINT `FKB62881FFDF74E053` FOREIGN KEY (`REV`) REFERENCES `revinfo` (`REV`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cxcxpt_aud`
--

LOCK TABLES `cxcxpt_aud` WRITE;
/*!40000 ALTER TABLE `cxcxpt_aud` DISABLE KEYS */;
/*!40000 ALTER TABLE `cxcxpt_aud` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cxmlconfiguration`
--

DROP TABLE IF EXISTS `cxmlconfiguration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cxmlconfiguration` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `CREDENTIALIDXPATHEXPRESSION` varchar(255) DEFAULT NULL,
  `DEPLOYMENTMODE` varchar(255) DEFAULT NULL,
  `NAME` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `SENDERSHAREDSECRET` varchar(255) DEFAULT NULL,
  `SYSTEMUSERXPATHEXPRESSION` varchar(255) DEFAULT NULL,
  `VERSION` bigint(20) DEFAULT NULL,
  `CREDENTIALNAMEXPATHEXPRESSION` varchar(255) DEFAULT NULL,
  `SYSTEMUSERXPATHEXPDEFRES` varchar(255) DEFAULT NULL,
  `SYSTEMUSERPREFIX` varchar(255) DEFAULT NULL,
  `CREDENTIALNAMEPREFIX` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UC_CXMLCONFIGURATION` (`NAME`)
) ENGINE=InnoDB AUTO_INCREMENT=153624782 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cxmlconfiguration`
--

LOCK TABLES `cxmlconfiguration` WRITE;
/*!40000 ALTER TABLE `cxmlconfiguration` DISABLE KEYS */;
INSERT INTO `cxmlconfiguration` VALUES (17708586,NULL,'PRODUCTION','Cintas-TEST','94JJF4049','NOT APPLICABLE',9,NULL,'demo-cintas-user',NULL,NULL),(72500901,NULL,'PRODUCTION','Cintas-Ariba','F404994JJ','/cXML/Request/PunchOutSetupRequest/ShipTo/Address/@addressID',4,NULL,'cintas-default-customer','cintas-punchout-',NULL),(102567915,NULL,'PRODUCTION','Novant_Config','F404994JJ','NOT APPLICABLE',15,'/cXML/Request/PunchOutSetupRequest/SelectedItem/ItemID/SupplierPartID/text()','defaultNovantUser','novant-',NULL),(110038389,NULL,'PRODUCTION','BI-Customer','F404994JJ','/cXML/Request/PunchOutSetupRequest/ShipTo/Address/@addressID',7,NULL,'catalog_tester1','biusa-punchout-',NULL),(123726544,NULL,'TEST','ARIBA-BUYER-CXML-DEMO','94JJF4049','NOT APPLICABLE',8,'/cXML/Request/PunchOutSetupRequest/SelectedItem/ItemID/SupplierPartID/text()','catalog_tester',NULL,NULL),(141078347,NULL,'PRODUCTION','Novant_Config_TRAINING','94JJF4049','NOT APPLICABLE',4,NULL,NULL,NULL,'/cXML/Request/PunchOutSetupRequest/SelectedItem/ItemID/SupplierPartID/text()'),(142745206,NULL,'TEST','JNJ_ARIBA_Test','1difsca77spl1guy','NOT APPLICABLE',14,'/cXML/Request/PunchOutSetupRequest/SelectedItem/ItemID/SupplierPartID/text()','catalog_tester',NULL,NULL),(143975296,NULL,'TEST','FritoLay_config','fritolay2014-cxml','/cXML/Request/PunchOutSetupRequest/Extrinsic[@name=\'User\']/text()',0,NULL,NULL,NULL,NULL),(153624781,NULL,'TEST','Fritolay-ARIBATest','94JJF4049','NOT APPLICABLE',1,NULL,'demo-fritolay-user',NULL,NULL);
/*!40000 ALTER TABLE `cxmlconfiguration` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cxmlconfiguration_aud`
--

DROP TABLE IF EXISTS `cxmlconfiguration_aud`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cxmlconfiguration_aud` (
  `ID` bigint(20) NOT NULL DEFAULT '0',
  `REV` bigint(20) NOT NULL AUTO_INCREMENT,
  `REVTYPE` decimal(3,0) DEFAULT NULL,
  `CREDENTIALNAMEXPATHEXPRESSION` varchar(255) DEFAULT NULL,
  `DEPLOYMENTMODE` varchar(255) DEFAULT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `SENDERSHAREDSECRET` varchar(255) DEFAULT NULL,
  `SYSTEMUSERXPATHEXPRESSION` varchar(255) DEFAULT NULL,
  `SYSTEMUSERXPATHEXPDEFRES` varchar(255) DEFAULT NULL,
  `SYSTEMUSERPREFIX` varchar(255) DEFAULT NULL,
  `CREDENTIALNAMEPREFIX` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`,`REV`),
  KEY `FK5BD03933DF74E053` (`REV`),
  CONSTRAINT `FK5BD03933DF74E053` FOREIGN KEY (`REV`) REFERENCES `revinfo` (`REV`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cxmlconfiguration_aud`
--

LOCK TABLES `cxmlconfiguration_aud` WRITE;
/*!40000 ALTER TABLE `cxmlconfiguration_aud` DISABLE KEYS */;
/*!40000 ALTER TABLE `cxmlconfiguration_aud` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cxmlcredential`
--

DROP TABLE IF EXISTS `cxmlcredential`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cxmlcredential` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `DOMAIN` varchar(255) DEFAULT NULL,
  `IDENTIFIER` varchar(255) DEFAULT NULL,
  `VERSION` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=153624950 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cxmlcredential`
--

LOCK TABLES `cxmlcredential` WRITE;
/*!40000 ALTER TABLE `cxmlcredential` DISABLE KEYS */;
INSERT INTO `cxmlcredential` VALUES (17708587,'NetworkId','AN01011648990-T',0),(17708588,'AribaNetworkUserId','sysadmin@ariba.com',0),(17708593,'networkid','an01000049754-t',2),(53926612,'NetworkId','AN01000049754',1),(53926613,'NetworkId','AN01000049754',1),(53926614,'NetworkId','AN01011648990',1),(72500902,'NetworkId','AN01011648990',0),(72500903,'AribaNetworkUserId','sysadmin@ariba.com',0),(72500904,'sap','1208298',1),(72500905,'networkid','an01000049754',0),(72500906,'transactionnetworkid','an01000049754',0),(72500907,'buyersystemid','acm_16584290',1),(102567916,'duns','093778918',1),(102567917,'buyersystemid','855627',0),(102567919,'novantid','855627',0),(102567920,'privateid','855627',0),(102567921,'vendorid','855627',0),(102567922,'transactionnetworkid','an01000049754',1),(102567923,'networkid','an01000049754',1),(102568000,'NetworkId','AN01003699021',1),(102568001,'AribaNetworkUserId','sysadmin@ariba.com',0),(102573860,'NetworkId','AN01000049754',2),(102573861,'NetworkId','AN01003699021',1),(102573993,'NetworkId','AN01000049754',2),(102573994,'NetworkId','AN01003699021',1),(110038390,'NetworkId','AN01000000270',1),(110038391,'AribaNetworkUserId','sysadmin@ariba.com',0),(110038392,'NetworkID','AN01000049754',1),(110038393,'buyersystemid','60853',0),(110038394,'duns','093778918',1),(110038395,'partition-part1','0009064535',0),(110065229,'NetworkId','AN01000049754',2),(110065230,'NetworkId','AN01000049754',2),(110065231,'NetworkId','AN01000000270',2),(111336106,'duns','093778918',0),(111336107,'duns','093778918',0),(111336108,'NetworkId','AN01000000270',0),(116932765,'user','apdnorthrop-p',1),(116932766,'staples.com','apdnorthrop-p',1),(116932767,'user','staples.com',0),(119348129,'user','HUNTINGTONI',0),(119348130,'staples.com','HUNTINGTONI',0),(119348131,'user','staples.com',0),(119359613,'DUNS','759759759',0),(119359614,'APD_DOMAIN','TEMA/SUPER',0),(119359615,'DUNS','943879320',0),(119371029,'APD_DOMAIN','943879320',0),(119371030,'APD_DOMAIN','test',0),(119371031,'DUNS','943879320',0),(119614745,'user','HUNTINGTONI',0),(119614746,'staples.com','HUNTINGTONI',0),(119614747,'user','staples.com',0),(119789751,'user','apdnorthrop-p',0),(119789752,'staples.com','apdnorthrop-p',0),(119789753,'user','staples.com',0),(119873100,'DUNS','759759759',0),(119873101,'APD_DOMAIN','tema',0),(119873102,'DUNS','943879320',0),(119873444,'DUNS','759759759',0),(119873445,'APD_DOMAIN','tema',0),(119873446,'DUNS','943879320',0),(120745433,'user','apdnorthrop-p',0),(120745434,'staples.com','apdnorthrop-p',0),(120745435,'user','staples.com',0),(123725988,'NetworkID','AN01000049754-T',0),(123725989,'AribaNetworkUserId','sysadmin@ariba.com',0),(123725990,'NetworkID','AN01000002779-T',0),(123725991,'DUNS','093778918-T',0),(123726545,'NetworkID','AN01000002779-T',0),(123726546,'DUNS','093778918-T',0),(123726547,'AribaNetworkUserId','sysadmin@ariba.com',0),(123726548,'NetworkID','AN01000049754-T',0),(123897193,'user','HUNTINGTONI',0),(123897194,'staples.com','HUNTINGTONI',0),(123897195,'user','staples.com',0),(123921964,'user','HUNTINGTONI',0),(123921965,'staples.com','HUNTINGTONI',0),(123921966,'user','staples.com',0),(123924490,'user','apdnorthrop-p',0),(123924491,'staples.com','apdnorthrop-p',0),(123924492,'user','staples.com',0),(133421156,'NetworkID','AN01000049754-T',2),(133421157,'NetworkID','AN01000049754-T',2),(133421158,'NetworkID','AN01000002779-T',3),(133456850,'user','HUNTINGTONI',0),(133456851,'staples.com','HUNTINGTONI',0),(133456852,'user','staples.com',0),(133458294,'sap','1201258',0),(133458420,'transactionnetworkid','an01000049754-t',0),(133458774,'buyersystemid','1201258',0),(133502984,'AribaNetworkUserId','AN01011648990-T',0),(133589983,'user','apdnorthrop-p',0),(133589984,'staples.com','apdnorthrop-p',0),(133589985,'user','staples.com',0),(134403380,'user','HUNTINGTONI',0),(134403381,'staples.com','HUNTINGTONI',0),(134403382,'user','staples.com',0),(141052685,'privateid','855627',0),(141052686,'vendorid','855627',0),(141052687,'NetworkID','an01000049754-t',0),(141052688,'NetworkId','AN01003699021-T',0),(141066291,'vendorid','855627',0),(141066292,'privateid','855627',0),(141066293,'NetworkID','an01000049754-t',0),(141066294,'NetworkId','AN01003699021-T',0),(141078348,'NetworkId','AN01003699021-T',0),(141078349,'AribaNetworkUserId','sysadmin@ariba.com',0),(141078350,'vendorid','855627',0),(141078351,'privateid','855627',0),(141078352,'networkid','an01000049754-t',0),(141078353,'buyersystemid','855627',0),(141078354,'ustin','561789017',0),(141078355,'duns','093778918-t',0),(141078356,'novantid','855627',0),(141078357,'transactionnetworkid','an01000049754-t',1),(141273252,'user','apdnorthrop-p',0),(141273253,'staples.com','apdnorthrop-p',0),(141273254,'user','staples.com',0),(141277391,'user','apdnorthrop-p',0),(141277392,'staples.com','apdnorthrop-p',0),(141277393,'user','staples.com',0),(141293337,'user','apdnorthrop-p',0),(141293338,'staples.com','apdnorthrop-p',0),(141293339,'user','staples.com',0),(141302044,'user','apdnorthrop-p',0),(141302045,'staples.com','apdnorthrop-p',0),(141302046,'user','staples.com',0),(141550705,'APD_DOMAIN','943879320',0),(141550706,'APD_DOMAIN','test',0),(141550707,'DUNS','943879320',0),(142013378,'NetworkId','AN01000049754',0),(142013514,'NetworkId','AN01000049754',0),(142745208,'NetworkID','AN01000002779-T',4),(142745209,'AribaNetworkUserId','ariba-network-catalog-tester@ariba.com',3),(142745210,'NetworkID','AN01000110542-T',2),(142747037,'NetworkID','AN01000110542-T',2),(142747040,'NetworkID','AN01000002779-T',2),(142747576,'APD_DOMAIN','759759759',1),(142747577,'APD_DOMAIN','test',0),(142747578,'DUNS','943879320',0),(143975297,'DUNS','943879320',0),(143975298,'DUNS','943879320',0),(143975299,'AribaNetworkUserId','ariba-network-catalog-tester@ariba.com',0),(143975300,'DUNS','APD-TEST',0),(143980516,'DUNS','APD-TEST',0),(143980517,'DUNS','APD-TEST',0),(143980518,'DUNS','943879320',0),(143980562,'APD_DOMAIN','17713',0),(143980563,'NetworkID','APD-TEST',0),(143980564,'DUNS','marfield',0),(143981780,'APD_DOMAIN','17713',0),(143981781,'APD_DOMAIN','APDLIVE',3),(143981782,'DUNS','marfield',0),(145912845,'APD_DOMAIN','17713',0),(145912846,'APD_DOMAIN','APDLIVE',1),(145912847,'DUNS','marfield',0),(146245311,'APD_DOMAIN','17713',0),(146245312,'APD_DOMAIN','APDLIVE',1),(146245313,'DUNS','marfield',0),(146923049,'APD_DOMAIN','17713',0),(146923050,'APD_DOMAIN','APDLIVE',0),(146923051,'DUNS','marfield',0),(146929094,'NetworkID','AN01000049754-T',0),(146929095,'AribaNetworkUserId','sysadmin@ariba.com',0),(146929096,'DUNS','093778918-T',0),(146931577,'AribaNetworkUserId','ariba-network-catalog-tester@ariba.com',1),(146933093,'DUNS','609192091-T',0),(146935905,'DUNS','093778918-T',1),(147043975,'DUNS','759759759',0),(147043976,'APD_DOMAIN','TEMA/SUPER',0),(147043977,'DUNS','943879320',0),(148087819,'APD_DOMAIN','17713',0),(148087820,'APD_DOMAIN','APDLIVE',0),(148087821,'DUNS','marfield',0),(148389433,'DUNS','798798798',1),(148389435,'DUNS','107741050',2),(148398492,'APD_DOMAIN','apd',3),(153624782,'NetworkId','AN01015090455-T',0),(153624783,'AribaNetworkUserId','sysadmin@ariba.com',0),(153624784,'networkid','an01000049754-t',0),(153624882,'NetworkID','AN01000049754-T',1),(153624883,'NetworkID',' AN01000049754-T',1),(153624928,'APD_DOMAIN','17713',0),(153624929,'NetworkID','APD-TEST',0),(153624930,'DUNS','marfield',0),(153624948,'NetworkID','AN01000002779-T',0),(153624949,'AribaNetworkUserId','AN01015090455-T',0);
/*!40000 ALTER TABLE `cxmlcredential` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cxmlcredential_aud`
--

DROP TABLE IF EXISTS `cxmlcredential_aud`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cxmlcredential_aud` (
  `ID` bigint(20) NOT NULL DEFAULT '0',
  `REV` bigint(20) NOT NULL AUTO_INCREMENT,
  `REVTYPE` decimal(3,0) DEFAULT NULL,
  `DOMAIN` varchar(255) DEFAULT NULL,
  `IDENTIFIER` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`,`REV`),
  KEY `FK41C768BCDF74E053` (`REV`),
  CONSTRAINT `FK41C768BCDF74E053` FOREIGN KEY (`REV`) REFERENCES `revinfo` (`REV`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cxmlcredential_aud`
--

LOCK TABLES `cxmlcredential_aud` WRITE;
/*!40000 ALTER TABLE `cxmlcredential_aud` DISABLE KEYS */;
/*!40000 ALTER TABLE `cxmlcredential_aud` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `deliverysch_calendar`
--

DROP TABLE IF EXISTS `deliverysch_calendar`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `deliverysch_calendar` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `SCHEDULE_ID` bigint(20) DEFAULT NULL,
  `SCHEDULEDATE` datetime NOT NULL,
  `VERSION` bigint(20) DEFAULT NULL,
  UNIQUE KEY `DELIVERYSCH_CALENDAR_PK` (`ID`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `deliverysch_calendar`
--

LOCK TABLES `deliverysch_calendar` WRITE;
/*!40000 ALTER TABLE `deliverysch_calendar` DISABLE KEYS */;
/*!40000 ALTER TABLE `deliverysch_calendar` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `deliverysch_definition`
--

DROP TABLE IF EXISTS `deliverysch_definition`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `deliverysch_definition` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `DEFINITION_DESC` varchar(255) NOT NULL,
  `SCHEDULE_ID` bigint(20) DEFAULT NULL,
  `SCHEDULEDAY` decimal(1,0) NOT NULL,
  `VERSION` bigint(20) DEFAULT NULL,
  UNIQUE KEY `DELIVERYSCH_DEFINITION_PK` (`ID`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `deliverysch_definition`
--

LOCK TABLES `deliverysch_definition` WRITE;
/*!40000 ALTER TABLE `deliverysch_definition` DISABLE KEYS */;
/*!40000 ALTER TABLE `deliverysch_definition` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `deliverysch_exception`
--

DROP TABLE IF EXISTS `deliverysch_exception`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `deliverysch_exception` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `LOCATION_DESC` varchar(255) DEFAULT NULL,
  `DELIVERY_DESC` varchar(255) DEFAULT NULL,
  `COMMENTS` varchar(255) DEFAULT NULL,
  `SCHEDULE_ID` bigint(20) DEFAULT NULL,
  `SHIP_ADDR_LINE1` varchar(255) DEFAULT NULL,
  `SHIP_ADDR_LINE1_FLAG` decimal(1,0) NOT NULL,
  `SHIP_CITY` varchar(255) DEFAULT NULL,
  `SHIP_CITY_FLAG` decimal(1,0) NOT NULL,
  `SHIP_STATE` varchar(255) DEFAULT NULL,
  `SHIP_STATE_FLAG` decimal(1,0) NOT NULL,
  `SHIP_ZIP` varchar(255) DEFAULT NULL,
  `SHIP_ZIP_FLAG` decimal(1,0) NOT NULL,
  `ACCOUNT_ID` bigint(20) DEFAULT NULL,
  `ACCOUNT_ID_FLAG` decimal(1,0) NOT NULL,
  `ROOT_NAME` varchar(255) DEFAULT NULL,
  `ROOT_NAME_FLAG` decimal(1,0) NOT NULL,
  `PARENT_NAME` varchar(255) DEFAULT NULL,
  `PARENT_NAME_FLAG` decimal(1,0) NOT NULL,
  `ACCOUNT_NAME` varchar(255) DEFAULT NULL,
  `ACCOUNT_NAME_FLAG` decimal(1,0) NOT NULL,
  `VERSION` bigint(20) DEFAULT NULL,
  UNIQUE KEY `DELIVERYSCH_EXCEPTION_PK` (`ID`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `deliverysch_exception`
--

LOCK TABLES `deliverysch_exception` WRITE;
/*!40000 ALTER TABLE `deliverysch_exception` DISABLE KEYS */;
/*!40000 ALTER TABLE `deliverysch_exception` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `deliverysch_holiday`
--

DROP TABLE IF EXISTS `deliverysch_holiday`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `deliverysch_holiday` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `HOLIDAY_DESC` varchar(255) DEFAULT NULL,
  `HOLIDAYDATE` datetime DEFAULT NULL,
  `VERSION` bigint(20) DEFAULT NULL,
  UNIQUE KEY `DELIVERYSCH_HOLIDAY_PK` (`ID`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `deliverysch_holiday`
--

LOCK TABLES `deliverysch_holiday` WRITE;
/*!40000 ALTER TABLE `deliverysch_holiday` DISABLE KEYS */;
/*!40000 ALTER TABLE `deliverysch_holiday` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `department`
--

DROP TABLE IF EXISTS `department`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `department` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(255) DEFAULT NULL,
  `VERSION` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `department`
--

LOCK TABLES `department` WRITE;
/*!40000 ALTER TABLE `department` DISABLE KEYS */;
/*!40000 ALTER TABLE `department` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `department_aud`
--

DROP TABLE IF EXISTS `department_aud`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `department_aud` (
  `ID` bigint(20) NOT NULL DEFAULT '0',
  `REV` bigint(20) NOT NULL AUTO_INCREMENT,
  `REVTYPE` decimal(3,0) DEFAULT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`,`REV`),
  KEY `FKB4AB4443DF74E053` (`REV`),
  CONSTRAINT `FKB4AB4443DF74E053` FOREIGN KEY (`REV`) REFERENCES `revinfo` (`REV`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `department_aud`
--

LOCK TABLES `department_aud` WRITE;
/*!40000 ALTER TABLE `department_aud` DISABLE KEYS */;
/*!40000 ALTER TABLE `department_aud` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `descriptiontype`
--

DROP TABLE IF EXISTS `descriptiontype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `descriptiontype` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(255) DEFAULT NULL,
  `VERSION` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `descriptiontype`
--

LOCK TABLES `descriptiontype` WRITE;
/*!40000 ALTER TABLE `descriptiontype` DISABLE KEYS */;
/*!40000 ALTER TABLE `descriptiontype` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `desktop`
--

DROP TABLE IF EXISTS `desktop`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `desktop` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(255) DEFAULT NULL,
  `VERSION` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `desktop`
--

LOCK TABLES `desktop` WRITE;
/*!40000 ALTER TABLE `desktop` DISABLE KEYS */;
/*!40000 ALTER TABLE `desktop` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `desktop_aud`
--

DROP TABLE IF EXISTS `desktop_aud`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `desktop_aud` (
  `ID` bigint(20) NOT NULL DEFAULT '0',
  `REV` bigint(20) NOT NULL AUTO_INCREMENT,
  `REVTYPE` decimal(3,0) DEFAULT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`REV`,`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `desktop_aud`
--

LOCK TABLES `desktop_aud` WRITE;
/*!40000 ALTER TABLE `desktop_aud` DISABLE KEYS */;
/*!40000 ALTER TABLE `desktop_aud` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dimensiontype`
--

DROP TABLE IF EXISTS `dimensiontype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dimensiontype` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `DIMENSIONTYPENAME` varchar(255) DEFAULT NULL,
  `VERSION` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dimensiontype`
--

LOCK TABLES `dimensiontype` WRITE;
/*!40000 ALTER TABLE `dimensiontype` DISABLE KEYS */;
/*!40000 ALTER TABLE `dimensiontype` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `favoriteslist`
--

DROP TABLE IF EXISTS `favoriteslist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `favoriteslist` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `VERSION` bigint(20) DEFAULT NULL,
  `CATALOG_ID` bigint(20) DEFAULT NULL,
  `AXCXU_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UK_FAVORITESLIST_1` (`AXCXU_ID`,`NAME`,`CATALOG_ID`),
  KEY `FKFC5FBB7531B94B57` (`CATALOG_ID`),
  CONSTRAINT `FKFC5FBB7531B94B57` FOREIGN KEY (`CATALOG_ID`) REFERENCES `catalog` (`ID`),
  CONSTRAINT `FKFC5FBB7590DF4B4F` FOREIGN KEY (`AXCXU_ID`) REFERENCES `accountxcredentialxuser` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `favoriteslist`
--

LOCK TABLES `favoriteslist` WRITE;
/*!40000 ALTER TABLE `favoriteslist` DISABLE KEYS */;
/*!40000 ALTER TABLE `favoriteslist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `favoriteslist_catalogxitem`
--

DROP TABLE IF EXISTS `favoriteslist_catalogxitem`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `favoriteslist_catalogxitem` (
  `FAVORITESLIST_ID` bigint(20) NOT NULL DEFAULT '0',
  `ITEMS_ID` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`FAVORITESLIST_ID`,`ITEMS_ID`),
  KEY `FK229D1F9C34FADEEF` (`ITEMS_ID`),
  CONSTRAINT `FK229D1F9C34FADEEF` FOREIGN KEY (`ITEMS_ID`) REFERENCES `catalogxitem` (`ID`),
  CONSTRAINT `FK229D1F9CE4345037` FOREIGN KEY (`FAVORITESLIST_ID`) REFERENCES `favoriteslist` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `favoriteslist_catalogxitem`
--

LOCK TABLES `favoriteslist_catalogxitem` WRITE;
/*!40000 ALTER TABLE `favoriteslist_catalogxitem` DISABLE KEYS */;
/*!40000 ALTER TABLE `favoriteslist_catalogxitem` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `field`
--

DROP TABLE IF EXISTS `field`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `field` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `VERSION` bigint(20) DEFAULT NULL,
  `FIELDTYPE_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UC_FIELD_1` (`NAME`),
  KEY `FK40BB0DA532E0A57` (`FIELDTYPE_ID`),
  CONSTRAINT `FK40BB0DA532E0A57` FOREIGN KEY (`FIELDTYPE_ID`) REFERENCES `fieldtype` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=145917862 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `field`
--

LOCK TABLES `field` WRITE;
/*!40000 ALTER TABLE `field` DISABLE KEYS */;
INSERT INTO `field` VALUES (30737,'ship company',0,30736),(30738,'ship name',0,30731),(30739,'requester phone',1,30731),(30740,'ship address 1',0,30731),(30741,'ship address 2',0,30731),(30742,'ship address 3',0,30731),(30743,'ship city',0,30731),(30744,'ship zip',0,30731),(30745,'ship states',0,30732),(30746,'ship contiguous states [DEPRECATED]',0,30732),(30747,'ship north america',0,30732),(30748,'ship us only',0,30732),(30749,'ship us and canada',0,30732),(30750,'existing shipto address selection',0,30732),(30751,'desktop combobox',0,30731),(30752,'building or department',0,30732),(30753,'cost center',0,30732),(30754,'shipping method',1,30732),(30755,'ship extra 1',0,30731),(30756,'ship extra 2',0,30731),(30757,'ship extra 3',0,30731),(30758,'custom po number',0,30731),(30759,'blanket po number',0,30732),(30760,'email',0,30731),(30761,'credit card number',0,30731),(30762,'credit card expiration',0,30735),(30763,'credit card CVV',0,30731),(30764,'payment type selection',0,30732),(30767,'bill address 1',0,30731),(30768,'bill address 2',0,30731),(30769,'bill address 3',0,30731),(30770,'bill city',0,30731),(30771,'bill zip',0,30731),(30772,'bill states',0,30732),(30773,'bill contiguous states [DEPRECATED]',0,30732),(30774,'bill north america',0,30732),(30775,'bill us only',0,30732),(30776,'bill us and canada',0,30732),(30777,'existing billto address selection',0,30732),(30778,'bill extra 1',0,30731),(30779,'bill extra 2',0,30731),(30780,'bill extra 3',0,30731),(30781,'comments',0,30731),(30782,'line item comments',0,30731),(30783,'ship date',0,30735),(30784,'delivery date',0,30735),(30785,'cancel date',0,30735),(30786,'order origin',0,30732),(30787,'order type',0,30732),(30788,'contracting officer',0,30731),(30789,'shipping cost',0,30736),(30790,'handling cost',0,30736),(4147125,'bill name',0,30731),(4147126,'bill company',0,30731),(4494225,'credit card name on card',0,30731),(17702739,'building or department combobox',0,30732),(17702740,'building or department free',0,30732),(17702741,'cost center combobox',0,30732),(17702742,'cost center free',0,30732),(39304596,'account location code',0,30731),(39304608,'ship contiguous states',0,30731),(39304609,'bill contiguos states',0,30731),(39304610,'AddressID',0,30731),(39304611,'AddressID(base 36)',0,30731),(39304612,'Contact Name',0,30731),(39304613,'CarrierRouting',0,30731),(39304614,'DeliveryDate',0,30731),(39304615,'Dept',0,30731),(39304616,'Facility',0,30731),(39304617,'Fedex No',0,30731),(39304618,'LocationID',0,30731),(39304619,'Mailstop',0,30731),(39304620,'NOWL',0,30731),(39304621,'POL',0,30731),(39304622,'Header Comments',0,30731),(39304623,'USAccount',0,30731),(39304624,'OldRelease',0,30731),(39304625,'Order Email',0,30731),(39304626,'Street 2',0,30731),(39304627,'Street 3',0,30731),(39304628,'requestername',1,30731),(39304630,'apd po',0,30731),(39304631,'APD_AccountingID',0,30731),(39304632,'paymenttype',0,30731),(39304633,'Customer Sku',0,30731),(39304634,'PR No.',0,30731),(39304635,'Total Eaches',0,30731),(39304636,'No Charge Item',0,30731),(39304637,'Manufacturer Part Number',0,30731),(39304638,'fulfillment',0,30731),(39304639,'APD_InterchangeID',0,30731),(39304651,'County',0,30731),(39304652,'Taxable',0,30731),(116931579,'desktop',0,30731),(117385019,'desktop free',0,30731),(117443684,'credit card CVV indicator',0,30732),(145917861,'ship address type',1,30732);
/*!40000 ALTER TABLE `field` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fieldoptions`
--

DROP TABLE IF EXISTS `fieldoptions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fieldoptions` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `VALUE` varchar(255) DEFAULT NULL,
  `VERSION` bigint(20) DEFAULT NULL,
  `FIELD_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FKC6EDA46444537EF7` (`FIELD_ID`),
  CONSTRAINT `FKC6EDA46444537EF7` FOREIGN KEY (`FIELD_ID`) REFERENCES `field` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=145917864 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fieldoptions`
--

LOCK TABLES `fieldoptions` WRITE;
/*!40000 ALTER TABLE `fieldoptions` DISABLE KEYS */;
INSERT INTO `fieldoptions` VALUES (30765,'Credit Card',0,30764),(30766,'Invoice',0,30764),(30813,'US',0,30749),(30814,'CA',0,30749),(30815,'US',0,30748),(30816,'US',0,30747),(30817,'CA',0,30747),(30818,'MX',0,30747),(30819,'US',0,30776),(30820,'CA',0,30776),(30821,'US',0,30775),(30822,'US',0,30774),(30823,'CA',0,30774),(30824,'MX',0,30774),(30825,'AL',0,30745),(30826,'AK',0,30745),(30827,'AZ',0,30745),(30828,'AR',0,30745),(30829,'CA',0,30745),(30830,'CO',0,30745),(30831,'CT',0,30745),(30832,'DE',0,30745),(30833,'DC',0,30745),(30834,'FL',0,30745),(30835,'GA',0,30745),(30836,'HI',0,30745),(30837,'ID',0,30745),(30838,'IL',0,30745),(30839,'IN',0,30745),(30840,'IA',0,30745),(30841,'KS',0,30745),(30842,'KY',0,30745),(30843,'LA',0,30745),(30844,'ME',0,30745),(30845,'MD',0,30745),(30846,'MA',0,30745),(30847,'MI',0,30745),(30848,'MN',0,30745),(30849,'MS',0,30745),(30850,'MO',0,30745),(30851,'MT',0,30745),(30852,'NE',0,30745),(30853,'NV',0,30745),(30854,'NH',0,30745),(30855,'NJ',0,30745),(30856,'NM',0,30745),(30857,'NY',0,30745),(30858,'NC',0,30745),(30859,'ND',0,30745),(30860,'OH',0,30745),(30861,'OK',0,30745),(30862,'OR',0,30745),(30863,'PA',0,30745),(30864,'RI',0,30745),(30865,'SC',0,30745),(30866,'SD',0,30745),(30867,'TN',0,30745),(30868,'TX',0,30745),(30869,'UT',0,30745),(30870,'VT',0,30745),(30871,'VA',0,30745),(30872,'WA',0,30745),(30873,'WV',0,30745),(30874,'WI',0,30745),(30875,'WY',0,30745),(30876,'AL',0,30746),(30877,'AZ',0,30746),(30878,'AR',0,30746),(30879,'CA',0,30746),(30880,'CO',0,30746),(30881,'CT',0,30746),(30882,'DE',0,30746),(30883,'DC',0,30746),(30884,'FL',0,30746),(30885,'GA',0,30746),(30886,'ID',0,30746),(30887,'IL',0,30746),(30888,'IN',0,30746),(30889,'IA',0,30746),(30890,'KS',0,30746),(30891,'KY',0,30746),(30892,'LA',0,30746),(30893,'ME',0,30746),(30894,'MD',0,30746),(30895,'MA',0,30746),(30896,'MI',0,30746),(30897,'MN',0,30746),(30898,'MS',0,30746),(30899,'MO',0,30746),(30900,'MT',0,30746),(30901,'NE',0,30746),(30902,'NV',0,30746),(30903,'NH',0,30746),(30904,'NJ',0,30746),(30905,'NM',0,30746),(30906,'NY',0,30746),(30907,'NC',0,30746),(30908,'ND',0,30746),(30909,'OH',0,30746),(30910,'OK',0,30746),(30911,'OR',0,30746),(30912,'PA',0,30746),(30913,'RI',0,30746),(30914,'SC',0,30746),(30915,'SD',0,30746),(30916,'TN',0,30746),(30917,'TX',0,30746),(30918,'UT',0,30746),(30919,'VT',0,30746),(30920,'VA',0,30746),(30921,'WA',0,30746),(30922,'WV',0,30746),(30923,'WI',0,30746),(30924,'WY',0,30746),(30925,'AL',0,30772),(30926,'AK',0,30772),(30927,'AZ',0,30772),(30928,'AR',0,30772),(30929,'CA',0,30772),(30930,'CO',0,30772),(30931,'CT',0,30772),(30932,'DE',0,30772),(30933,'DC',0,30772),(30934,'FL',0,30772),(30935,'GA',0,30772),(30936,'HI',0,30772),(30937,'ID',0,30772),(30938,'IL',0,30772),(30939,'IN',0,30772),(30940,'IA',0,30772),(30941,'KS',0,30772),(30942,'KY',0,30772),(30943,'LA',0,30772),(30944,'ME',0,30772),(30945,'MD',0,30772),(30946,'MA',0,30772),(30947,'MI',0,30772),(30948,'MN',0,30772),(30949,'MS',0,30772),(30950,'MO',0,30772),(30951,'MT',0,30772),(30952,'NE',0,30772),(30953,'NV',0,30772),(30954,'NH',0,30772),(30955,'NJ',0,30772),(30956,'NM',0,30772),(30957,'NY',0,30772),(30958,'NC',0,30772),(30959,'ND',0,30772),(30960,'OH',0,30772),(30961,'OK',0,30772),(30962,'OR',0,30772),(30963,'PA',0,30772),(30964,'RI',0,30772),(30965,'SC',0,30772),(30966,'SD',0,30772),(30967,'TN',0,30772),(30968,'TX',0,30772),(30969,'UT',0,30772),(30970,'VT',0,30772),(30971,'VA',0,30772),(30972,'WA',0,30772),(30973,'WV',0,30772),(30974,'WI',0,30772),(30975,'WY',0,30772),(30976,'AL',0,30773),(30977,'AZ',0,30773),(30978,'AR',0,30773),(30979,'CA',0,30773),(30980,'CO',0,30773),(30981,'CT',0,30773),(30982,'DE',0,30773),(30983,'DC',0,30773),(30984,'FL',0,30773),(30985,'GA',0,30773),(30986,'ID',0,30773),(30987,'IL',0,30773),(30988,'IN',0,30773),(30989,'IA',0,30773),(30990,'KS',0,30773),(30991,'KY',0,30773),(30992,'LA',0,30773),(30993,'ME',0,30773),(30994,'MD',0,30773),(30995,'MA',0,30773),(30996,'MI',0,30773),(30997,'MN',0,30773),(30998,'MS',0,30773),(30999,'MO',0,30773),(31000,'MT',0,30773),(31001,'NE',0,30773),(31002,'NV',0,30773),(31003,'NH',0,30773),(31004,'NJ',0,30773),(31005,'NM',0,30773),(31006,'NY',0,30773),(31007,'NC',0,30773),(31008,'ND',0,30773),(31009,'OH',0,30773),(31010,'OK',0,30773),(31011,'OR',0,30773),(31012,'PA',0,30773),(31013,'RI',0,30773),(31014,'SC',0,30773),(31015,'SD',0,30773),(31016,'TN',0,30773),(31017,'TX',0,30773),(31018,'UT',0,30773),(31019,'VT',0,30773),(31020,'VA',0,30773),(31021,'WA',0,30773),(31022,'WV',0,30773),(31023,'WI',0,30773),(31024,'WY',0,30773),(31025,'USPS',0,NULL),(31026,'FedEx',0,NULL),(145915753,'UPS Next Day Air',0,30754),(145915754,'UPS',0,30754),(145915755,'UPS Second Day Air',0,30754),(145917862,'business',0,145917861),(145917863,'residence',0,145917861);
/*!40000 ALTER TABLE `fieldoptions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fieldtype`
--

DROP TABLE IF EXISTS `fieldtype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fieldtype` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `VERSION` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UC_FIELDTYPE_1` (`NAME`)
) ENGINE=InnoDB AUTO_INCREMENT=30737 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fieldtype`
--

LOCK TABLES `fieldtype` WRITE;
/*!40000 ALTER TABLE `fieldtype` DISABLE KEYS */;
INSERT INTO `fieldtype` VALUES (30731,'text',0),(30732,'dropdown',0),(30733,'radio',0),(30734,'checkbox',0),(30735,'calendar',0),(30736,'output',0);
/*!40000 ALTER TABLE `fieldtype` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `flatitemclassification`
--

DROP TABLE IF EXISTS `flatitemclassification`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `flatitemclassification` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `VALUE` varchar(255) DEFAULT NULL,
  `VERSION` bigint(20) DEFAULT NULL,
  `ITEM_ID` bigint(20) DEFAULT NULL,
  `ITEMCLASSTYPE_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FIC_ITEM_ID` (`ITEM_ID`) USING BTREE,
  KEY `FKBF6E98F250060531` (`ITEMCLASSTYPE_ID`),
  CONSTRAINT `FKBF6E98F250060531` FOREIGN KEY (`ITEMCLASSTYPE_ID`) REFERENCES `itemclassificationtype` (`ID`),
  CONSTRAINT `FKBF6E98F2D4B669BD` FOREIGN KEY (`ITEM_ID`) REFERENCES `item` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `flatitemclassification`
--

LOCK TABLES `flatitemclassification` WRITE;
/*!40000 ALTER TABLE `flatitemclassification` DISABLE KEYS */;
/*!40000 ALTER TABLE `flatitemclassification` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `flatitemspecification`
--

DROP TABLE IF EXISTS `flatitemspecification`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `flatitemspecification` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `VERSION` bigint(20) DEFAULT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `PRIORITY` bigint(20) DEFAULT NULL,
  `SEQUENCE` bigint(20) DEFAULT NULL,
  `VALUE` varchar(400) DEFAULT NULL,
  `ITEM_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FIS_ITEM_ID` (`ITEM_ID`) USING BTREE,
  CONSTRAINT `FK21CE8657D4B669BD` FOREIGN KEY (`ITEM_ID`) REFERENCES `item` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `flatitemspecification`
--

LOCK TABLES `flatitemspecification` WRITE;
/*!40000 ALTER TABLE `flatitemspecification` DISABLE KEYS */;
/*!40000 ALTER TABLE `flatitemspecification` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `from_credential_table`
--

DROP TABLE IF EXISTS `from_credential_table`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `from_credential_table` (
  `CXMLCONFIGURATION_ID` bigint(20) NOT NULL DEFAULT '0',
  `FROMCREDENTIALS_ID` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`CXMLCONFIGURATION_ID`,`FROMCREDENTIALS_ID`),
  UNIQUE KEY `SYS_C0022030` (`FROMCREDENTIALS_ID`),
  CONSTRAINT `FK5AAAFDDBD0B0DA57` FOREIGN KEY (`CXMLCONFIGURATION_ID`) REFERENCES `cxmlconfiguration` (`ID`),
  CONSTRAINT `FK5AAAFDDBE0E054D6` FOREIGN KEY (`FROMCREDENTIALS_ID`) REFERENCES `cxmlcredential` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `from_credential_table`
--

LOCK TABLES `from_credential_table` WRITE;
/*!40000 ALTER TABLE `from_credential_table` DISABLE KEYS */;
INSERT INTO `from_credential_table` VALUES (17708586,17708587),(72500901,72500902),(102567915,102568000),(110038389,110038390),(123726544,123726545),(123726544,123726546),(141078347,141078348),(142745206,142745208),(143975296,143975297),(142745206,146933093),(153624781,153624782);
/*!40000 ALTER TABLE `from_credential_table` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `from_credential_table_aud`
--

DROP TABLE IF EXISTS `from_credential_table_aud`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `from_credential_table_aud` (
  `REV` bigint(20) NOT NULL AUTO_INCREMENT,
  `CXMLCONFIGURATION_ID` bigint(20) NOT NULL DEFAULT '0',
  `FROMCREDENTIALS_ID` bigint(20) NOT NULL DEFAULT '0',
  `REVTYPE` decimal(3,0) DEFAULT NULL,
  PRIMARY KEY (`REV`,`CXMLCONFIGURATION_ID`,`FROMCREDENTIALS_ID`),
  CONSTRAINT `FKEF1EC62CDF74E053` FOREIGN KEY (`REV`) REFERENCES `revinfo` (`REV`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `from_credential_table_aud`
--

LOCK TABLES `from_credential_table_aud` WRITE;
/*!40000 ALTER TABLE `from_credential_table_aud` DISABLE KEYS */;
/*!40000 ALTER TABLE `from_credential_table_aud` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `globalsetting`
--

DROP TABLE IF EXISTS `globalsetting`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `globalsetting` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(255) DEFAULT NULL,
  `VALUE` varchar(255) DEFAULT NULL,
  `VERSION` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `globalsetting`
--

LOCK TABLES `globalsetting` WRITE;
/*!40000 ALTER TABLE `globalsetting` DISABLE KEYS */;
/*!40000 ALTER TABLE `globalsetting` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hierarchynode`
--

DROP TABLE IF EXISTS `hierarchynode`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hierarchynode` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `DESCRIPTION` varchar(255) DEFAULT NULL,
  `INDEXEDPATHS` varchar(4000) DEFAULT NULL,
  `PUBLICID` bigint(20) DEFAULT NULL,
  `VERSION` bigint(20) DEFAULT NULL,
  `PARENT_ID` bigint(20) DEFAULT NULL,
  `POPULARITY` bigint(20) DEFAULT NULL,
  `TYPE` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK6128C1B7ECE7DD24` (`PARENT_ID`),
  CONSTRAINT `FK6128C1B7ECE7DD24` FOREIGN KEY (`PARENT_ID`) REFERENCES `hierarchynode` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=132422195 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hierarchynode`
--

LOCK TABLES `hierarchynode` WRITE;
/*!40000 ALTER TABLE `hierarchynode` DISABLE KEYS */;
INSERT INTO `hierarchynode` VALUES (31307,'Breakroom and Janitorial','!Breakroom and Janitorial',10,0,NULL,NULL,NULL),(31308,'Furniture','!Furniture',9,0,NULL,NULL,NULL),(31309,'Office Supplies','!Office Supplies',11,0,NULL,NULL,NULL),(31310,'Technology','!Technology',12,0,NULL,NULL,NULL),(31311,'Facility Maintenance','!Breakroom and Janitorial!Facility Maintenance',38,0,31307,NULL,NULL),(31312,'Fans, Heaters & Humidifiers','!Breakroom and Janitorial!Fans, Heaters & Humidifiers',39,0,31307,NULL,NULL),(31313,'First Aid & Health Supplies','!Breakroom and Janitorial!First Aid & Health Supplies',40,0,31307,NULL,NULL),(31314,'Food & Beverage Service','!Breakroom and Janitorial!Food & Beverage Service',41,0,31307,NULL,NULL),(31315,'Janitorial & Breakroom Carts','!Breakroom and Janitorial!Janitorial & Breakroom Carts',42,0,31307,NULL,NULL),(31316,'Janitorial & Sanitation Supplies','!Breakroom and Janitorial!Janitorial & Sanitation Supplies',43,0,31307,NULL,NULL),(31317,'Personal Hygiene Products','!Breakroom and Janitorial!Personal Hygiene Products',44,0,31307,NULL,NULL),(31318,'Safety & Sanitary Wear','!Breakroom and Janitorial!Safety & Sanitary Wear',45,0,31307,NULL,NULL),(31319,'Towels & Tissue','!Breakroom and Janitorial!Towels & Tissue',46,0,31307,NULL,NULL),(31320,'Waste Receptacles & Liners','!Breakroom and Janitorial!Waste Receptacles & Liners',47,0,31307,NULL,NULL),(31321,'Cabinets, Racks & Shelves','!Furniture!Cabinets, Racks & Shelves',48,0,31308,NULL,NULL),(31322,'Carts & Stands','!Furniture!Carts & Stands',49,0,31308,NULL,NULL),(31323,'Casters','!Furniture!Casters',50,0,31308,NULL,NULL),(31324,'Chairs & Sofas','!Furniture!Chairs & Sofas',51,0,31308,NULL,NULL),(31325,'Desks & Workstations','!Furniture!Desks & Workstations',52,0,31308,NULL,NULL),(31326,'Furniture Locks','!Furniture!Furniture Locks',53,0,31308,NULL,NULL),(31327,'Panel Systems & Components','!Furniture!Panel Systems & Components',54,0,31308,NULL,NULL),(31328,'Room Accessories','!Furniture!Room Accessories',55,0,31308,NULL,NULL),(31329,'Tables','!Furniture!Tables',56,0,31308,NULL,NULL),(31330,'Binders & Binding Systems','!Office Supplies!Binders & Binding Systems',19,0,31309,NULL,NULL),(31331,'Calendars, Planners & Personal Organizers','!Office Supplies!Calendars, Planners & Personal Organizers',20,0,31309,NULL,NULL),(31332,'Carrying Cases','!Office Supplies!Carrying Cases',21,0,31309,NULL,NULL),(31333,'Classroom Teaching & Learning Materials','!Office Supplies!Classroom Teaching & Learning Materials',22,0,31309,NULL,NULL),(31334,'Crafts & Recreation Room Products','!Office Supplies!Crafts & Recreation Room Products',23,0,31309,NULL,NULL),(31335,'Cutting & Measuring Devices','!Office Supplies!Cutting & Measuring Devices',24,0,31309,NULL,NULL),(31336,'Desk Accessories & Workspace Organizers','!Office Supplies!Desk Accessories & Workspace Organizers',25,0,31309,NULL,NULL),(31337,'Envelopes, Mailers & Shipping Supplies','!Office Supplies!Envelopes, Mailers & Shipping Supplies',26,0,31309,NULL,NULL),(31338,'File Folders, Portable & Storage Box Files','!Office Supplies!File Folders, Portable & Storage Box Files',27,0,31309,NULL,NULL),(31339,'Forms, Recordkeeping & Reference Materials','!Office Supplies!Forms, Recordkeeping & Reference Materials',28,0,31309,NULL,NULL),(31340,'Labels, Indexes & Stamps','!Office Supplies!Labels, Indexes & Stamps',29,0,31309,NULL,NULL),(31341,'Money Handling Products','!Office Supplies!Money Handling Products',30,0,31309,NULL,NULL),(31342,'Office Accessories','!Office Supplies!Office Accessories',31,0,31309,NULL,NULL),(31343,'Office Equipment Cleaners','!Office Supplies!Office Equipment Cleaners',32,0,31309,NULL,NULL),(31344,'Paper & Printable Media','!Office Supplies!Paper & Printable Media',33,0,31309,NULL,NULL),(31345,'Presentation/Display & Scheduling Boards','!Office Supplies!Presentation/Display & Scheduling Boards',34,0,31309,NULL,NULL),(31346,'Staplers & Punches','!Office Supplies!Staplers & Punches',35,0,31309,NULL,NULL),(31347,'Tape, Adhesives & Fasteners','!Office Supplies!Tape, Adhesives & Fasteners',36,0,31309,NULL,NULL),(31348,'Writing & Correction Supplies','!Office Supplies!Writing & Correction Supplies',37,0,31309,NULL,NULL),(31349,'Audio Visual Equipment','!Technology!Audio Visual Equipment',13,0,31310,NULL,NULL),(31350,'Cables, Adapters & Power Products','!Technology!Cables, Adapters & Power Products',14,0,31310,NULL,NULL),(31351,'Computer Components, Peripherals & Accessories','!Technology!Computer Components, Peripherals & Accessories',15,0,31310,NULL,NULL),(31352,'Imaging Supplies and Accessories','!Technology!Imaging Supplies and Accessories',16,0,31310,NULL,NULL),(31353,'Office Machines','!Technology!Office Machines',17,0,31310,NULL,NULL),(31354,'Storage Media','!Technology!Storage Media',18,0,31310,NULL,NULL),(31355,'Baby Changing Stations','!Breakroom and Janitorial!Facility Maintenance!Baby Changing Stations',350,0,31311,NULL,NULL),(31356,'Batteries & Electrical Supplies','!Breakroom and Janitorial!Facility Maintenance!Batteries & Electrical Supplies',351,0,31311,NULL,NULL),(31357,'Building Safety & Security Products','!Breakroom and Janitorial!Facility Maintenance!Building Safety & Security Products',352,0,31311,NULL,NULL),(31358,'Dispensers','!Breakroom and Janitorial!Facility Maintenance!Dispensers',353,0,31311,NULL,NULL),(31359,'Furniture Mover Sliders','!Breakroom and Janitorial!Facility Maintenance!Furniture Mover Sliders',354,0,31311,NULL,NULL),(31360,'Maintenance Tools','!Breakroom and Janitorial!Facility Maintenance!Maintenance Tools',355,0,31311,NULL,NULL),(31361,'Mats & Antislip Tape','!Breakroom and Janitorial!Facility Maintenance!Mats & Antislip Tape',356,0,31311,NULL,NULL),(31362,'Plumbing Supplies','!Breakroom and Janitorial!Facility Maintenance!Plumbing Supplies',357,0,31311,NULL,NULL),(31363,'Step Stools & Ladders','!Breakroom and Janitorial!Facility Maintenance!Step Stools & Ladders',359,0,31311,NULL,NULL),(31364,'Air Purifier & Humidifier Filters','!Breakroom and Janitorial!Fans, Heaters & Humidifiers!Air Purifier & Humidifier Filters',344,0,31312,NULL,NULL),(31365,'Air Purifiers','!Breakroom and Janitorial!Fans, Heaters & Humidifiers!Air Purifiers',345,0,31312,NULL,NULL),(31366,'Automatic Hand Dryers','!Breakroom and Janitorial!Fans, Heaters & Humidifiers!Automatic Hand Dryers',346,0,31312,NULL,NULL),(31367,'Fans','!Breakroom and Janitorial!Fans, Heaters & Humidifiers!Fans',347,0,31312,NULL,NULL),(31368,'Heaters','!Breakroom and Janitorial!Fans, Heaters & Humidifiers!Heaters',348,0,31312,NULL,NULL),(31369,'Humidifiers','!Breakroom and Janitorial!Fans, Heaters & Humidifiers!Humidifiers',349,0,31312,NULL,NULL),(31370,'Antiseptics','!Breakroom and Janitorial!First Aid & Health Supplies!Antiseptics',322,0,31313,NULL,NULL),(31371,'Bandages, Pads & Wraps','!Breakroom and Janitorial!First Aid & Health Supplies!Bandages, Pads & Wraps',323,0,31313,NULL,NULL),(31372,'Blood Pressure Monitors','!Breakroom and Janitorial!First Aid & Health Supplies!Blood Pressure Monitors',324,0,31313,NULL,NULL),(31373,'Cold Packs','!Breakroom and Janitorial!First Aid & Health Supplies!Cold Packs',325,0,31313,NULL,NULL),(31374,'Cotton Balls & Swabs','!Breakroom and Janitorial!First Aid & Health Supplies!Cotton Balls & Swabs',326,0,31313,NULL,NULL),(31375,'Crutches','!Breakroom and Janitorial!First Aid & Health Supplies!Crutches',327,0,31313,NULL,NULL),(31376,'Exam Gloves','!Breakroom and Janitorial!First Aid & Health Supplies!Exam Gloves',328,0,31313,NULL,NULL),(31377,'Exam Room Equipment','!Breakroom and Janitorial!First Aid & Health Supplies!Exam Room Equipment',329,0,31313,NULL,NULL),(31378,'Exam Room ID Flag Systems','!Breakroom and Janitorial!First Aid & Health Supplies!Exam Room ID Flag Systems',330,0,31313,NULL,NULL),(31379,'Exam Table Sheets & Pillowcases','!Breakroom and Janitorial!First Aid & Health Supplies!Exam Table Sheets & Pillowcases',331,0,31313,NULL,NULL),(31380,'Eyewash Stations & Refills','!Breakroom and Janitorial!First Aid & Health Supplies!Eyewash Stations & Refills',332,0,31313,NULL,NULL),(31381,'First Aid Kits','!Breakroom and Janitorial!First Aid & Health Supplies!First Aid Kits',333,0,31313,NULL,NULL),(31382,'Gauze Sponges','!Breakroom and Janitorial!First Aid & Health Supplies!Gauze Sponges',334,0,31313,NULL,NULL),(31383,'Medical Wear','!Breakroom and Janitorial!First Aid & Health Supplies!Medical Wear',335,0,31313,NULL,NULL),(31384,'Medical/Surgical Tape','!Breakroom and Janitorial!First Aid & Health Supplies!Medical/Surgical Tape',336,0,31313,NULL,NULL),(31385,'Medicine','!Breakroom and Janitorial!First Aid & Health Supplies!Medicine',337,0,31313,NULL,NULL),(31386,'Ointments & Salves','!Breakroom and Janitorial!First Aid & Health Supplies!Ointments & Salves',338,0,31313,NULL,NULL),(31387,'Sharps Containers','!Breakroom and Janitorial!First Aid & Health Supplies!Sharps Containers',339,0,31313,NULL,NULL),(31388,'Stethoscopes','!Breakroom and Janitorial!First Aid & Health Supplies!Stethoscopes',340,0,31313,NULL,NULL),(31389,'Thermometers','!Breakroom and Janitorial!First Aid & Health Supplies!Thermometers',341,0,31313,NULL,NULL),(31390,'Tongue Depressors','!Breakroom and Janitorial!First Aid & Health Supplies!Tongue Depressors',342,0,31313,NULL,NULL),(31391,'Wheelchairs','!Breakroom and Janitorial!First Aid & Health Supplies!Wheelchairs',343,0,31313,NULL,NULL),(31392,'Appliances','!Breakroom and Janitorial!Food & Beverage Service!Appliances',319,0,31314,NULL,NULL),(31393,'Beverages & Snack Foods','!Breakroom and Janitorial!Food & Beverage Service!Beverages & Snack Foods',320,0,31314,NULL,NULL),(31394,'Food Service Supplies','!Breakroom and Janitorial!Food & Beverage Service!Food Service Supplies',321,0,31314,NULL,NULL),(31395,'Breakroom & Beverage Carts','!Breakroom and Janitorial!Janitorial & Breakroom Carts!Breakroom & Beverage Carts',311,0,31315,NULL,NULL),(31396,'Chair & Table Carts','!Breakroom and Janitorial!Janitorial & Breakroom Carts!Chair & Table Carts',312,0,31315,NULL,NULL),(31397,'Furniture Dollies','!Breakroom and Janitorial!Janitorial & Breakroom Carts!Furniture Dollies',313,0,31315,NULL,NULL),(31398,'Hand Trucks & Platform Carts','!Breakroom and Janitorial!Janitorial & Breakroom Carts!Hand Trucks & Platform Carts',314,0,31315,NULL,NULL),(31399,'Janitorial Carts','!Breakroom and Janitorial!Janitorial & Breakroom Carts!Janitorial Carts',315,0,31315,NULL,NULL),(31400,'Luggage Carts','!Breakroom and Janitorial!Janitorial & Breakroom Carts!Luggage Carts',316,0,31315,NULL,NULL),(31401,'Utility Carts','!Breakroom and Janitorial!Janitorial & Breakroom Carts!Utility Carts',317,0,31315,NULL,NULL),(31402,'Waste Receptacle Dollies','!Breakroom and Janitorial!Janitorial & Breakroom Carts!Waste Receptacle Dollies',318,0,31315,NULL,NULL),(31403,'Cleaning Products','!Breakroom and Janitorial!Janitorial & Sanitation Supplies!Cleaning Products',308,0,31316,NULL,NULL),(31404,'Cleaning Tools & Supplies','!Breakroom and Janitorial!Janitorial & Sanitation Supplies!Cleaning Tools & Supplies',309,0,31316,NULL,NULL),(31405,'Work Gloves','!Breakroom and Janitorial!Janitorial & Sanitation Supplies!Work Gloves',310,0,31316,NULL,NULL),(31406,'Diapers','!Breakroom and Janitorial!Personal Hygiene Products!Diapers',1404,0,31317,NULL,NULL),(31407,'Hand Cleaners','!Breakroom and Janitorial!Personal Hygiene Products!Hand Cleaners',301,0,31317,NULL,NULL),(31408,'Hand Lotion','!Breakroom and Janitorial!Personal Hygiene Products!Hand Lotion',302,0,31317,NULL,NULL),(31409,'Hand Wipes & Towelettes','!Breakroom and Janitorial!Personal Hygiene Products!Hand Wipes & Towelettes',303,0,31317,NULL,NULL),(31410,'Insect Repellant','!Breakroom and Janitorial!Personal Hygiene Products!Insect Repellant',304,0,31317,NULL,NULL),(31411,'Mouthwash','!Breakroom and Janitorial!Personal Hygiene Products!Mouthwash',305,0,31317,NULL,NULL),(31412,'Sanitary Napkins & Tampons','!Breakroom and Janitorial!Personal Hygiene Products!Sanitary Napkins & Tampons',306,0,31317,NULL,NULL),(31413,'Aprons & Coveralls','!Breakroom and Janitorial!Safety & Sanitary Wear!Aprons & Coveralls',289,0,31318,NULL,NULL),(31414,'Back Supports','!Breakroom and Janitorial!Safety & Sanitary Wear!Back Supports',290,0,31318,NULL,NULL),(31415,'Beard & Hair Nets','!Breakroom and Janitorial!Safety & Sanitary Wear!Beard & Hair Nets',291,0,31318,NULL,NULL),(31416,'Disposable Sanitary Gloves','!Breakroom and Janitorial!Safety & Sanitary Wear!Disposable Sanitary Gloves',292,0,31318,NULL,NULL),(31417,'Dust Masks & Respirators','!Breakroom and Janitorial!Safety & Sanitary Wear!Dust Masks & Respirators',293,0,31318,NULL,NULL),(31418,'Ear Plugs & Ear Muffs','!Breakroom and Janitorial!Safety & Sanitary Wear!Ear Plugs & Ear Muffs',294,0,31318,NULL,NULL),(31419,'Footwear','!Breakroom and Janitorial!Safety & Sanitary Wear!Footwear',295,0,31318,NULL,NULL),(31420,'Hardhats','!Breakroom and Janitorial!Safety & Sanitary Wear!Hardhats',296,0,31318,NULL,NULL),(31421,'Raingear','!Breakroom and Janitorial!Safety & Sanitary Wear!Raingear',297,0,31318,NULL,NULL),(31422,'Safety Glasses & Goggles','!Breakroom and Janitorial!Safety & Sanitary Wear!Safety Glasses & Goggles',298,0,31318,NULL,NULL),(31423,'Safety Vests','!Breakroom and Janitorial!Safety & Sanitary Wear!Safety Vests',299,0,31318,NULL,NULL),(31424,'Wrist Supports','!Breakroom and Janitorial!Safety & Sanitary Wear!Wrist Supports',300,0,31318,NULL,NULL),(31425,'Bathroom Tissue','!Breakroom and Janitorial!Towels & Tissue!Bathroom Tissue',283,0,31319,NULL,NULL),(31426,'Facial Tissue','!Breakroom and Janitorial!Towels & Tissue!Facial Tissue',284,0,31319,NULL,NULL),(31427,'Lens Cleaning Tissues','!Breakroom and Janitorial!Towels & Tissue!Lens Cleaning Tissues',286,0,31319,NULL,NULL),(31428,'Restroom Hand Towels','!Breakroom and Janitorial!Towels & Tissue!Restroom Hand Towels',287,0,31319,NULL,NULL),(31429,'Toilet Seat Covers','!Breakroom and Janitorial!Towels & Tissue!Toilet Seat Covers',288,0,31319,NULL,NULL),(31430,'Ash Receptacles','!Breakroom and Janitorial!Waste Receptacles & Liners!Ash Receptacles',275,0,31320,NULL,NULL),(31431,'Medical Waste Receptacles','!Breakroom and Janitorial!Waste Receptacles & Liners!Medical Waste Receptacles',276,0,31320,NULL,NULL),(31432,'Recycling Receptacles & Lids','!Breakroom and Janitorial!Waste Receptacles & Liners!Recycling Receptacles & Lids',277,0,31320,NULL,NULL),(31433,'Sanitary Napkin Receptacles','!Breakroom and Janitorial!Waste Receptacles & Liners!Sanitary Napkin Receptacles',278,0,31320,NULL,NULL),(31434,'Waste Receptacle Liners','!Breakroom and Janitorial!Waste Receptacles & Liners!Waste Receptacle Liners',280,0,31320,NULL,NULL),(31435,'Waste Receptacles & Lids','!Breakroom and Janitorial!Waste Receptacles & Liners!Waste Receptacles & Lids',281,0,31320,NULL,NULL),(31436,'Wastebaskets','!Breakroom and Janitorial!Waste Receptacles & Liners!Wastebaskets',282,0,31320,NULL,NULL),(31437,'Bookcases & Door Kits','!Furniture!Cabinets, Racks & Shelves!Bookcases & Door Kits',411,0,31321,NULL,NULL),(31438,'Coat Racks & Hangers','!Furniture!Cabinets, Racks & Shelves!Coat Racks & Hangers',412,0,31321,NULL,NULL),(31439,'Conference Cabinets','!Furniture!Cabinets, Racks & Shelves!Conference Cabinets',413,0,31321,NULL,NULL),(31440,'Display Racks & Cases','!Furniture!Cabinets, Racks & Shelves!Display Racks & Cases',415,0,31321,NULL,NULL),(31441,'File & Storage Cabinets','!Furniture!Cabinets, Racks & Shelves!File & Storage Cabinets',416,0,31321,NULL,NULL),(31442,'File & Storage Racks','!Furniture!Cabinets, Racks & Shelves!File & Storage Racks',417,0,31321,NULL,NULL),(31443,'Hutches/Stack-On Units','!Furniture!Cabinets, Racks & Shelves!Hutches/Stack-On Units',418,0,31321,NULL,NULL),(31444,'Safes','!Furniture!Cabinets, Racks & Shelves!Safes',419,0,31321,NULL,NULL),(31445,'Shelving','!Furniture!Cabinets, Racks & Shelves!Shelving',420,0,31321,NULL,NULL),(31446,'AV Carts & Stands','!Furniture!Carts & Stands!AV Carts & Stands',401,0,31322,NULL,NULL),(31447,'Book Carts','!Furniture!Carts & Stands!Book Carts',402,0,31322,NULL,NULL),(31448,'Cart/Stand Accessories','!Furniture!Carts & Stands!Cart/Stand Accessories',404,0,31322,NULL,NULL),(31449,'Chart Stands','!Furniture!Carts & Stands!Chart Stands',405,0,31322,NULL,NULL),(31450,'File Folder Carts','!Furniture!Carts & Stands!File Folder Carts',406,0,31322,NULL,NULL),(31451,'Lecterns & Podiums','!Furniture!Carts & Stands!Lecterns & Podiums',407,0,31322,NULL,NULL),(31452,'Mail Carts','!Furniture!Carts & Stands!Mail Carts',408,0,31322,NULL,NULL),(31453,'Printer/Office Machine Carts & Stands','!Furniture!Carts & Stands!Printer/Office Machine Carts & Stands',409,0,31322,NULL,NULL),(31454,'Storage Drawer Carts','!Furniture!Carts & Stands!Storage Drawer Carts',410,0,31322,NULL,NULL),(31455,'Chair Accessories','!Furniture!Chairs & Sofas!Chair Accessories',393,0,31324,NULL,NULL),(31456,'Classroom Chairs','!Furniture!Chairs & Sofas!Classroom Chairs',394,0,31324,NULL,NULL),(31457,'Executive & Task Chairs','!Furniture!Chairs & Sofas!Executive & Task Chairs',395,0,31324,NULL,NULL),(31458,'Folding Chairs','!Furniture!Chairs & Sofas!Folding Chairs',396,0,31324,NULL,NULL),(31459,'Reception & Guest Chairs','!Furniture!Chairs & Sofas!Reception & Guest Chairs',397,0,31324,NULL,NULL),(31460,'Sofas & Loveseats','!Furniture!Chairs & Sofas!Sofas & Loveseats',398,0,31324,NULL,NULL),(31461,'Stacking/Nesting Chairs','!Furniture!Chairs & Sofas!Stacking/Nesting Chairs',399,0,31324,NULL,NULL),(31462,'Stools','!Furniture!Chairs & Sofas!Stools',400,0,31324,NULL,NULL),(31463,'Bridges & Connectors','!Furniture!Desks & Workstations!Bridges & Connectors',384,0,31325,NULL,NULL),(31464,'Computer Workstations & Components','!Furniture!Desks & Workstations!Computer Workstations & Components',385,0,31325,NULL,NULL),(31465,'Credenzas','!Furniture!Desks & Workstations!Credenzas',386,0,31325,NULL,NULL),(31466,'Desk/Workstation Accessories','!Furniture!Desks & Workstations!Desk/Workstation Accessories',387,0,31325,NULL,NULL),(31467,'Desks','!Furniture!Desks & Workstations!Desks',388,0,31325,NULL,NULL),(31468,'Hutches & Stack-On Storage Units','!Furniture!Desks & Workstations!Hutches & Stack-On Storage Units',389,0,31325,NULL,NULL),(31469,'Mailroom Stations','!Furniture!Desks & Workstations!Mailroom Stations',390,0,31325,NULL,NULL),(31470,'Peninsulas','!Furniture!Desks & Workstations!Peninsulas',391,0,31325,NULL,NULL),(31471,'Returns','!Furniture!Desks & Workstations!Returns',392,0,31325,NULL,NULL),(31472,NULL,'!Furniture!Furniture Locks!',0,0,31326,NULL,NULL),(31473,'Paneling & Partitioning Hardware','!Furniture!Panel Systems & Components!Paneling & Partitioning Hardware',381,0,31327,NULL,NULL),(31474,'Panels & Partitions','!Furniture!Panel Systems & Components!Panels & Partitions',382,0,31327,NULL,NULL),(31475,'Privacy Screens/Study Carrels','!Furniture!Panel Systems & Components!Privacy Screens/Study Carrels',383,0,31327,NULL,NULL),(31476,'Clocks','!Furniture!Room Accessories!Clocks',368,0,31328,NULL,NULL),(31477,'Doorstops','!Furniture!Room Accessories!Doorstops',369,0,31328,NULL,NULL),(31478,'Frames & Plaques','!Furniture!Room Accessories!Frames & Plaques',370,0,31328,NULL,NULL),(31479,'Lamps & Lighting','!Furniture!Room Accessories!Lamps & Lighting',371,0,31328,NULL,NULL),(31480,'Motivational Prints','!Furniture!Room Accessories!Motivational Prints',373,0,31328,NULL,NULL),(31481,'Name Plates','!Furniture!Room Accessories!Name Plates',374,0,31328,NULL,NULL),(31482,'Planters & Artificial Greenery','!Furniture!Room Accessories!Planters & Artificial Greenery',375,0,31328,NULL,NULL),(31483,'Signs & Sign Holders','!Furniture!Room Accessories!Signs & Sign Holders',376,0,31328,NULL,NULL),(31484,'TV/Projector Mounts','!Furniture!Room Accessories!TV/Projector Mounts',377,0,31328,NULL,NULL),(31485,'Activity & Utility Tables','!Furniture!Tables!Activity & Utility Tables',360,0,31329,NULL,NULL),(31486,'Art & Drafting Tables','!Furniture!Tables!Art & Drafting Tables',361,0,31329,NULL,NULL),(31487,'Conference & Hospitality Tables','!Furniture!Tables!Conference & Hospitality Tables',362,0,31329,NULL,NULL),(31488,'Meeting/Training Room Tables','!Furniture!Tables!Meeting/Training Room Tables',363,0,31329,NULL,NULL),(31489,'Reception Room Tables','!Furniture!Tables!Reception Room Tables',364,0,31329,NULL,NULL),(31490,'Table Accessories','!Furniture!Tables!Table Accessories',366,0,31329,NULL,NULL),(31491,'Binder Accessories','!Office Supplies!Binders & Binding Systems!Binder Accessories',267,0,31330,NULL,NULL),(31492,'Binders','!Office Supplies!Binders & Binding Systems!Binders',268,0,31330,NULL,NULL),(31493,'Binding Systems Supplies','!Office Supplies!Binders & Binding Systems!Binding Systems Supplies',270,0,31330,NULL,NULL),(31494,'Presentation Books','!Office Supplies!Binders & Binding Systems!Presentation Books',272,0,31330,NULL,NULL),(31495,'Report Covers & Pocket Portfolios','!Office Supplies!Binders & Binding Systems!Report Covers & Pocket Portfolios',273,0,31330,NULL,NULL),(31496,'Sheet Protectors, Card & Photo Sleeves','!Office Supplies!Binders & Binding Systems!Sheet Protectors, Card & Photo Sleeves',274,0,31330,NULL,NULL),(31497,'Appointment Books/Planners','!Office Supplies!Calendars, Planners & Personal Organizers!Appointment Books/Planners',262,0,31331,NULL,NULL),(31498,'Desktop Calendars & Planners','!Office Supplies!Calendars, Planners & Personal Organizers!Desktop Calendars & Planners',263,0,31331,NULL,NULL),(31499,'Diaries','!Office Supplies!Calendars, Planners & Personal Organizers!Diaries',264,0,31331,NULL,NULL),(31500,'Personal Organizers','!Office Supplies!Calendars, Planners & Personal Organizers!Personal Organizers',265,0,31331,NULL,NULL),(31501,'Wall Calendars & Planners','!Office Supplies!Calendars, Planners & Personal Organizers!Wall Calendars & Planners',266,0,31331,NULL,NULL),(31502,'Art Portfolios','!Office Supplies!Carrying Cases!Art Portfolios',246,0,31332,NULL,NULL),(31503,'Board/Board Supplies Cases','!Office Supplies!Carrying Cases!Board/Board Supplies Cases',247,0,31332,NULL,NULL),(31504,'Business Cases','!Office Supplies!Carrying Cases!Business Cases',248,0,31332,NULL,NULL),(31505,'Camera Bags & Cases','!Office Supplies!Carrying Cases!Camera Bags & Cases',249,0,31332,NULL,NULL),(31506,'Easel Carrying Cases','!Office Supplies!Carrying Cases!Easel Carrying Cases',251,0,31332,NULL,NULL),(31507,'iPod Cases','!Office Supplies!Carrying Cases!iPod Cases',252,0,31332,NULL,NULL),(31508,'Notebook Computer Bags & Cases','!Office Supplies!Carrying Cases!Notebook Computer Bags & Cases',256,0,31332,NULL,NULL),(31509,'Pad Holders & Pad Portfolios','!Office Supplies!Carrying Cases!Pad Holders & Pad Portfolios',257,0,31332,NULL,NULL),(31510,'PDA Bags & Cases','!Office Supplies!Carrying Cases!PDA Bags & Cases',258,0,31332,NULL,NULL),(31511,'Wheeled Bags & Cases','!Office Supplies!Carrying Cases!Wheeled Bags & Cases',261,0,31332,NULL,NULL),(31512,'Class Records & Lesson Books','!Office Supplies!Classroom Teaching & Learning Materials!Class Records & Lesson Books',232,0,31333,NULL,NULL),(31513,'Math Materials','!Office Supplies!Classroom Teaching & Learning Materials!Math Materials',236,0,31333,NULL,NULL),(31514,'Pocket Charts','!Office Supplies!Classroom Teaching & Learning Materials!Pocket Charts',237,0,31333,NULL,NULL),(31515,'Pre-K How-To Aids','!Office Supplies!Classroom Teaching & Learning Materials!Pre-K How-To Aids',238,0,31333,NULL,NULL),(31516,'Reading & Writing Materials','!Office Supplies!Classroom Teaching & Learning Materials!Reading & Writing Materials',239,0,31333,NULL,NULL),(31517,'School Supplies Kits','!Office Supplies!Classroom Teaching & Learning Materials!School Supplies Kits',240,0,31333,NULL,NULL),(31518,'Social Studies Materials','!Office Supplies!Classroom Teaching & Learning Materials!Social Studies Materials',242,0,31333,NULL,NULL),(31519,'Teacher\'s Aids & Manuals','!Office Supplies!Classroom Teaching & Learning Materials!Teacher\'s Aids & Manuals',245,0,31333,NULL,NULL),(31520,'Arts & Crafts Supplies','!Office Supplies!Crafts & Recreation Room Products!Arts & Crafts Supplies',224,0,31334,NULL,NULL),(31521,'Balloons','!Office Supplies!Crafts & Recreation Room Products!Balloons',225,0,31334,NULL,NULL),(31522,'Classroom Decorations','!Office Supplies!Crafts & Recreation Room Products!Classroom Decorations',226,0,31334,NULL,NULL),(31523,'Dress-Up & Crafts Apparel','!Office Supplies!Crafts & Recreation Room Products!Dress-Up & Crafts Apparel',227,0,31334,NULL,NULL),(31524,'Idea Books/Activity Kits','!Office Supplies!Crafts & Recreation Room Products!Idea Books/Activity Kits',228,0,31334,NULL,NULL),(31525,'Physical Education Equipment','!Office Supplies!Crafts & Recreation Room Products!Physical Education Equipment',229,0,31334,NULL,NULL),(31526,'Playhouse/Playschool Equipment','!Office Supplies!Crafts & Recreation Room Products!Playhouse/Playschool Equipment',230,0,31334,NULL,NULL),(31527,'Stencils','!Office Supplies!Crafts & Recreation Room Products!Stencils',231,0,31334,NULL,NULL),(31528,'Craft Knives & Blades','!Office Supplies!Cutting & Measuring Devices!Craft Knives & Blades',216,0,31335,NULL,NULL),(31529,'Cutting Mats','!Office Supplies!Cutting & Measuring Devices!Cutting Mats',217,0,31335,NULL,NULL),(31530,'Letter Openers','!Office Supplies!Cutting & Measuring Devices!Letter Openers',218,0,31335,NULL,NULL),(31531,'Paper Roll Cutters','!Office Supplies!Cutting & Measuring Devices!Paper Roll Cutters',219,0,31335,NULL,NULL),(31532,'Paper Trimmers & Blades','!Office Supplies!Cutting & Measuring Devices!Paper Trimmers & Blades',220,0,31335,NULL,NULL),(31533,'Rulers & Tape Measures','!Office Supplies!Cutting & Measuring Devices!Rulers & Tape Measures',221,0,31335,NULL,NULL),(31534,'Scissors','!Office Supplies!Cutting & Measuring Devices!Scissors',222,0,31335,NULL,NULL),(31535,'Bookends & Book Racks','!Office Supplies!Desk Accessories & Workspace Organizers!Bookends & Book Racks',198,0,31336,NULL,NULL),(31536,'Card Files, Holders & Racks','!Office Supplies!Desk Accessories & Workspace Organizers!Card Files, Holders & Racks',199,0,31336,NULL,NULL),(31537,'Catalog & Reference Racks','!Office Supplies!Desk Accessories & Workspace Organizers!Catalog & Reference Racks',200,0,31336,NULL,NULL),(31538,'Copyholders','!Office Supplies!Desk Accessories & Workspace Organizers!Copyholders',202,0,31336,NULL,NULL),(31539,'Desk Pads & Refills','!Office Supplies!Desk Accessories & Workspace Organizers!Desk Pads & Refills',203,0,31336,NULL,NULL),(31540,'Desk Supplies Holders & Dispensers','!Office Supplies!Desk Accessories & Workspace Organizers!Desk Supplies Holders & Dispensers',204,0,31336,NULL,NULL),(31541,'Drawer Organizers','!Office Supplies!Desk Accessories & Workspace Organizers!Drawer Organizers',205,0,31336,NULL,NULL),(31542,'Letter Trays & Stacking Supports','!Office Supplies!Desk Accessories & Workspace Organizers!Letter Trays & Stacking Supports',207,0,31336,NULL,NULL),(31543,'Literature Display Racks & Holders','!Office Supplies!Desk Accessories & Workspace Organizers!Literature Display Racks & Holders',208,0,31336,NULL,NULL),(31544,'Media Organizers','!Office Supplies!Desk Accessories & Workspace Organizers!Media Organizers',209,0,31336,NULL,NULL),(31545,'Mouse Pads & Wrist Rests','!Office Supplies!Desk Accessories & Workspace Organizers!Mouse Pads & Wrist Rests',210,0,31336,NULL,NULL),(31546,'Platforms, Stands & Shelves','!Office Supplies!Desk Accessories & Workspace Organizers!Platforms, Stands & Shelves',211,0,31336,NULL,NULL),(31547,'Sorters','!Office Supplies!Desk Accessories & Workspace Organizers!Sorters',212,0,31336,NULL,NULL),(31548,'Storage Containers','!Office Supplies!Desk Accessories & Workspace Organizers!Storage Containers',214,0,31336,NULL,NULL),(31549,'Wall & Panel Organizers','!Office Supplies!Desk Accessories & Workspace Organizers!Wall & Panel Organizers',215,0,31336,NULL,NULL),(31550,'Bags & Sacks','!Office Supplies!Envelopes, Mailers & Shipping Supplies!Bags & Sacks',194,0,31337,NULL,NULL),(31551,'Envelopes','!Office Supplies!Envelopes, Mailers & Shipping Supplies!Envelopes',195,0,31337,NULL,NULL),(31552,'Mailers','!Office Supplies!Envelopes, Mailers & Shipping Supplies!Mailers',196,0,31337,NULL,NULL),(31553,'Packing Materials','!Office Supplies!Envelopes, Mailers & Shipping Supplies!Packing Materials',197,0,31337,NULL,NULL),(31554,'Expanding Files & Wallets','!Office Supplies!File Folders, Portable & Storage Box Files!Expanding Files & Wallets',187,0,31338,NULL,NULL),(31555,'File & Folder Accessories','!Office Supplies!File Folders, Portable & Storage Box Files!File & Folder Accessories',188,0,31338,NULL,NULL),(31556,'File Sorters','!Office Supplies!File Folders, Portable & Storage Box Files!File Sorters',189,0,31338,NULL,NULL),(31557,'Filing Envelopes','!Office Supplies!File Folders, Portable & Storage Box Files!Filing Envelopes',190,0,31338,NULL,NULL),(31558,'Folders','!Office Supplies!File Folders, Portable & Storage Box Files!Folders',191,0,31338,NULL,NULL),(31559,'Portable Files','!Office Supplies!File Folders, Portable & Storage Box Files!Portable Files',192,0,31338,NULL,NULL),(31560,'Record Storage Boxes','!Office Supplies!File Folders, Portable & Storage Box Files!Record Storage Boxes',193,0,31338,NULL,NULL),(31561,'Address Books','!Office Supplies!Forms, Recordkeeping & Reference Materials!Address Books',183,0,31339,NULL,NULL),(31562,'Clipboards & Forms Holders','!Office Supplies!Forms, Recordkeeping & Reference Materials!Clipboards & Forms Holders',184,0,31339,NULL,NULL),(31563,'Forms & Recordkeeping Systems','!Office Supplies!Forms, Recordkeeping & Reference Materials!Forms & Recordkeeping Systems',185,0,31339,NULL,NULL),(31564,'Reference Materials','!Office Supplies!Forms, Recordkeeping & Reference Materials!Reference Materials',186,0,31339,NULL,NULL),(31565,'Bookmarks','!Office Supplies!Labels, Indexes & Stamps!Bookmarks',173,0,31340,NULL,NULL),(31566,'Identification Badges','!Office Supplies!Labels, Indexes & Stamps!Identification Badges',175,0,31340,NULL,NULL),(31567,'Index Dividers, Tabs & File Guides','!Office Supplies!Labels, Indexes & Stamps!Index Dividers, Tabs & File Guides',176,0,31340,NULL,NULL),(31568,'Labels & Stickers','!Office Supplies!Labels, Indexes & Stamps!Labels & Stickers',177,0,31340,NULL,NULL),(31569,'Sign & Poster Kits','!Office Supplies!Labels, Indexes & Stamps!Sign & Poster Kits',179,0,31340,NULL,NULL),(31570,'Stamps & Stamp Supplies','!Office Supplies!Labels, Indexes & Stamps!Stamps & Stamp Supplies',180,0,31340,NULL,NULL),(31571,'Tags & Tickets','!Office Supplies!Labels, Indexes & Stamps!Tags & Tickets',181,0,31340,NULL,NULL),(31572,'Tape Flags','!Office Supplies!Labels, Indexes & Stamps!Tape Flags',182,0,31340,NULL,NULL),(31573,'Cash & Check Boxes','!Office Supplies!Money Handling Products!Cash & Check Boxes',163,0,31341,NULL,NULL),(31574,'Cash Bags','!Office Supplies!Money Handling Products!Cash Bags',164,0,31341,NULL,NULL),(31575,'Cash Counters','!Office Supplies!Money Handling Products!Cash Counters',165,0,31341,NULL,NULL),(31576,'Cash Trays','!Office Supplies!Money Handling Products!Cash Trays',167,0,31341,NULL,NULL),(31577,'Coin Handling','!Office Supplies!Money Handling Products!Coin Handling',168,0,31341,NULL,NULL),(31578,'Deposit Ticket Holders','!Office Supplies!Money Handling Products!Deposit Ticket Holders',169,0,31341,NULL,NULL),(31579,'Paper Currency & Check Handling','!Office Supplies!Money Handling Products!Paper Currency & Check Handling',170,0,31341,NULL,NULL),(31580,'Wrapper & Band Sorter Racks','!Office Supplies!Money Handling Products!Wrapper & Band Sorter Racks',172,0,31341,NULL,NULL),(31581,'Call Bells','!Office Supplies!Office Accessories!Call Bells',157,0,31342,NULL,NULL),(31582,'Finger Pads & Moisteners','!Office Supplies!Office Accessories!Finger Pads & Moisteners',158,0,31342,NULL,NULL),(31583,'Gavels','!Office Supplies!Office Accessories!Gavels',159,0,31342,NULL,NULL),(31584,'Magnifiers','!Office Supplies!Office Accessories!Magnifiers',160,0,31342,NULL,NULL),(31585,'Mail & Suggestion Boxes','!Office Supplies!Office Accessories!Mail & Suggestion Boxes',161,0,31342,NULL,NULL),(31586,'US & State Flags','!Office Supplies!Office Accessories!US & State Flags',162,0,31342,NULL,NULL),(31587,'Dry Erase Board Cleaners','!Office Supplies!Office Equipment Cleaners!Dry Erase Board Cleaners',155,0,31343,NULL,NULL),(31588,'Electronics Equipment Cleaners','!Office Supplies!Office Equipment Cleaners!Electronics Equipment Cleaners',156,0,31343,NULL,NULL),(31589,'Art Paper & Sketching Pads','!Office Supplies!Paper & Printable Media!Art Paper & Sketching Pads',143,0,31344,NULL,NULL),(31590,'Canvas & Fabric','!Office Supplies!Paper & Printable Media!Canvas & Fabric',144,0,31344,NULL,NULL),(31591,'Carbon & Carbonless Paper','!Office Supplies!Paper & Printable Media!Carbon & Carbonless Paper',145,0,31344,NULL,NULL),(31592,'Cards & Card Stock','!Office Supplies!Paper & Printable Media!Cards & Card Stock',146,0,31344,NULL,NULL),(31593,'Easel Pads & Flip Charts','!Office Supplies!Paper & Printable Media!Easel Pads & Flip Charts',148,0,31344,NULL,NULL),(31594,'Kraft Paper','!Office Supplies!Paper & Printable Media!Kraft Paper',149,0,31344,NULL,NULL),(31595,'Loose Paper','!Office Supplies!Paper & Printable Media!Loose Paper',150,0,31344,NULL,NULL),(31596,'Machine Paper Rolls','!Office Supplies!Paper & Printable Media!Machine Paper Rolls',151,0,31344,NULL,NULL),(31597,'Notebooks & Writing Pads','!Office Supplies!Paper & Printable Media!Notebooks & Writing Pads',152,0,31344,NULL,NULL),(31598,'Printer Paper','!Office Supplies!Paper & Printable Media!Printer Paper',153,0,31344,NULL,NULL),(31599,'Transparency Film & Frames','!Office Supplies!Paper & Printable Media!Transparency Film & Frames',154,0,31344,NULL,NULL),(31600,'Board Accessories','!Office Supplies!Presentation/Display & Scheduling Boards!Board Accessories',132,0,31345,NULL,NULL),(31601,'Bulletin Boards','!Office Supplies!Presentation/Display & Scheduling Boards!Bulletin Boards',133,0,31345,NULL,NULL),(31602,'Chalk Boards','!Office Supplies!Presentation/Display & Scheduling Boards!Chalk Boards',134,0,31345,NULL,NULL),(31603,'Changeable Letter Boards','!Office Supplies!Presentation/Display & Scheduling Boards!Changeable Letter Boards',135,0,31345,NULL,NULL),(31604,'Combination Boards','!Office Supplies!Presentation/Display & Scheduling Boards!Combination Boards',136,0,31345,NULL,NULL),(31605,'Display Board Systems','!Office Supplies!Presentation/Display & Scheduling Boards!Display Board Systems',138,0,31345,NULL,NULL),(31606,'Dry Erase Boards','!Office Supplies!Presentation/Display & Scheduling Boards!Dry Erase Boards',139,0,31345,NULL,NULL),(31607,'Easel Stands','!Office Supplies!Presentation/Display & Scheduling Boards!Easel Stands',140,0,31345,NULL,NULL),(31608,'Electronic Copyboards','!Office Supplies!Presentation/Display & Scheduling Boards!Electronic Copyboards',141,0,31345,NULL,NULL),(31609,'Planning Boards/Schedulers','!Office Supplies!Presentation/Display & Scheduling Boards!Planning Boards/Schedulers',142,0,31345,NULL,NULL),(31610,'Punch Replacement Parts','!Office Supplies!Staplers & Punches!Punch Replacement Parts',126,0,31346,NULL,NULL),(31611,'Punches','!Office Supplies!Staplers & Punches!Punches',128,0,31346,NULL,NULL),(31612,'Staple Removers','!Office Supplies!Staplers & Punches!Staple Removers',129,0,31346,NULL,NULL),(31613,'Staplers','!Office Supplies!Staplers & Punches!Staplers',130,0,31346,NULL,NULL),(31614,'Staples','!Office Supplies!Staplers & Punches!Staples',131,0,31346,NULL,NULL),(31615,'Adhesives & Glue','!Office Supplies!Tape, Adhesives & Fasteners!Adhesives & Glue',113,0,31347,NULL,NULL),(31616,'Clips, Clamps & Rings','!Office Supplies!Tape, Adhesives & Fasteners!Clips, Clamps & Rings',114,0,31347,NULL,NULL),(31617,'Glue Guns & Sticks','!Office Supplies!Tape, Adhesives & Fasteners!Glue Guns & Sticks',115,0,31347,NULL,NULL),(31618,'Paper Fasteners','!Office Supplies!Tape, Adhesives & Fasteners!Paper Fasteners',116,0,31347,NULL,NULL),(31619,'Pins & Tacks','!Office Supplies!Tape, Adhesives & Fasteners!Pins & Tacks',117,0,31347,NULL,NULL),(31620,'Rubber Bands','!Office Supplies!Tape, Adhesives & Fasteners!Rubber Bands',118,0,31347,NULL,NULL),(31621,'String & Twine','!Office Supplies!Tape, Adhesives & Fasteners!String & Twine',119,0,31347,NULL,NULL),(31622,'Tag Attacher Guns','!Office Supplies!Tape, Adhesives & Fasteners!Tag Attacher Guns',120,0,31347,NULL,NULL),(31623,'Tag Fasteners & Bag Seals','!Office Supplies!Tape, Adhesives & Fasteners!Tag Fasteners & Bag Seals',121,0,31347,NULL,NULL),(31624,'Tape','!Office Supplies!Tape, Adhesives & Fasteners!Tape',122,0,31347,NULL,NULL),(31625,'Tape Dispensers','!Office Supplies!Tape, Adhesives & Fasteners!Tape Dispensers',123,0,31347,NULL,NULL),(31626,'Velcro & Mounting Products','!Office Supplies!Tape, Adhesives & Fasteners!Velcro & Mounting Products',124,0,31347,NULL,NULL),(31627,'Chalk','!Office Supplies!Writing & Correction Supplies!Chalk',102,0,31348,NULL,NULL),(31628,'Crayons & China Markers','!Office Supplies!Writing & Correction Supplies!Crayons & China Markers',103,0,31348,NULL,NULL),(31629,'Erasers & Correction Products','!Office Supplies!Writing & Correction Supplies!Erasers & Correction Products',104,0,31348,NULL,NULL),(31630,'Highlighters','!Office Supplies!Writing & Correction Supplies!Highlighters',105,0,31348,NULL,NULL),(31631,'Markers','!Office Supplies!Writing & Correction Supplies!Markers',106,0,31348,NULL,NULL),(31632,'Multifunction Writing Instruments','!Office Supplies!Writing & Correction Supplies!Multifunction Writing Instruments',107,0,31348,NULL,NULL),(31633,'Pen/Pencil Sets & Desk Sets','!Office Supplies!Writing & Correction Supplies!Pen/Pencil Sets & Desk Sets',108,0,31348,NULL,NULL),(31634,'Pencil Sharpeners','!Office Supplies!Writing & Correction Supplies!Pencil Sharpeners',109,0,31348,NULL,NULL),(31635,'Pencils','!Office Supplies!Writing & Correction Supplies!Pencils',110,0,31348,NULL,NULL),(31636,'Pens & Refills','!Office Supplies!Writing & Correction Supplies!Pens & Refills',111,0,31348,NULL,NULL),(31637,'Writing Accessories','!Office Supplies!Writing & Correction Supplies!Writing Accessories',112,0,31348,NULL,NULL),(31638,'Audio & Video Cassettes & Tapes','!Technology!Audio Visual Equipment!Audio & Video Cassettes & Tapes',89,0,31349,NULL,NULL),(31639,'Cameras & Accessories','!Technology!Audio Visual Equipment!Cameras & Accessories',93,0,31349,NULL,NULL),(31640,'Players and Recorders','!Technology!Audio Visual Equipment!Players and Recorders',96,0,31349,NULL,NULL),(31641,'Presentation and Projection Equipment','!Technology!Audio Visual Equipment!Presentation and Projection Equipment',97,0,31349,NULL,NULL),(31642,'Telephone & Cellular Accessories','!Technology!Audio Visual Equipment!Telephone & Cellular Accessories',98,0,31349,NULL,NULL),(31643,'Telephones','!Technology!Audio Visual Equipment!Telephones',99,0,31349,NULL,NULL),(31644,'Cables and Adapters','!Technology!Cables, Adapters & Power Products!Cables and Adapters',86,0,31350,NULL,NULL),(31645,'Office Equipment Power Products','!Technology!Cables, Adapters & Power Products!Office Equipment Power Products',87,0,31350,NULL,NULL),(31646,'Computer Accessories','!Technology!Computer Components, Peripherals & Accessories!Computer Accessories',84,0,31351,NULL,NULL),(31647,'Computer Components & Peripherals','!Technology!Computer Components, Peripherals & Accessories!Computer Components & Peripherals',85,0,31351,NULL,NULL),(31648,'Imaging Machine Accessories','!Technology!Imaging Supplies and Accessories!Imaging Machine Accessories',82,0,31352,NULL,NULL),(31649,'Imaging Supplies','!Technology!Imaging Supplies and Accessories!Imaging Supplies',83,0,31352,NULL,NULL),(31650,'Automatic Letter Openers','!Technology!Office Machines!Automatic Letter Openers',61,0,31353,NULL,NULL),(31651,'Binding Machines','!Technology!Office Machines!Binding Machines',62,0,31353,NULL,NULL),(31652,'Calculators & Counters','!Technology!Office Machines!Calculators & Counters',63,0,31353,NULL,NULL),(31653,'Cash Registers','!Technology!Office Machines!Cash Registers',66,0,31353,NULL,NULL),(31654,'CD & DVD Duplicators','!Technology!Office Machines!CD & DVD Duplicators',67,0,31353,NULL,NULL),(31655,'Checkwriters','!Technology!Office Machines!Checkwriters',68,0,31353,NULL,NULL),(31656,'Copiers, Fax Machines & Printers','!Technology!Office Machines!Copiers, Fax Machines & Printers',69,0,31353,NULL,NULL),(31657,'Dictation Devices','!Technology!Office Machines!Dictation Devices',70,0,31353,NULL,NULL),(31658,'Electronic Reference Books','!Technology!Office Machines!Electronic Reference Books',72,0,31353,NULL,NULL),(31659,'Label Makers & Supplies','!Technology!Office Machines!Label Makers & Supplies',73,0,31353,NULL,NULL),(31660,'Laminators & Supplies','!Technology!Office Machines!Laminators & Supplies',75,0,31353,NULL,NULL),(31661,'Paper Handling Machines','!Technology!Office Machines!Paper Handling Machines',76,0,31353,NULL,NULL),(31662,'Postal/Shipping Scales','!Technology!Office Machines!Postal/Shipping Scales',77,0,31353,NULL,NULL),(31663,'Shredders & Data Destroyers','!Technology!Office Machines!Shredders & Data Destroyers',78,0,31353,NULL,NULL),(31664,'Time Clocks, Cards & Badges','!Technology!Office Machines!Time Clocks, Cards & Badges',79,0,31353,NULL,NULL),(31665,'Time/Date Document Stampers','!Technology!Office Machines!Time/Date Document Stampers',80,0,31353,NULL,NULL),(31666,'Typewriters and Printwheels','!Technology!Office Machines!Typewriters and Printwheels',81,0,31353,NULL,NULL),(31667,'CDs','!Technology!Storage Media!CDs',57,0,31354,NULL,NULL),(31668,'Data Tapes','!Technology!Storage Media!Data Tapes',58,0,31354,NULL,NULL),(31669,'Diskettes','!Technology!Storage Media!Diskettes',59,0,31354,NULL,NULL),(31670,'DVDs','!Technology!Storage Media!DVDs',60,0,31354,NULL,NULL),(31671,'1/2\' Data Tape Cartridges','!Technology!Storage Media!Data Tapes!1/2\' Data Tape Cartridges',421,0,31668,NULL,NULL),(31672,'4MM Data Tape (DAT, DDS)','!Technology!Storage Media!Data Tapes!4MM Data Tape (DAT, DDS)',422,0,31668,NULL,NULL),(31673,'5 1/4\' Data Tapes (QIC/SLR/MLR/Magnus)','!Technology!Storage Media!Data Tapes!5 1/4\' Data Tapes (QIC/SLR/MLR/Magnus)',423,0,31668,NULL,NULL),(31674,'8MM Data Tape','!Technology!Storage Media!Data Tapes!8MM Data Tape',424,0,31668,NULL,NULL),(31675,'AC/DC Power Adapters','!Technology!Cables, Adapters & Power Products!Office Equipment Power Products!AC/DC Power Adapters',515,0,31645,NULL,NULL),(31676,'Account/Recordkeeping Books','!Office Supplies!Forms, Recordkeeping & Reference Materials!Forms & Recordkeeping Systems!Account/Recordkeeping Books',748,0,31563,NULL,NULL),(31677,'Accounting Forms','!Office Supplies!Forms, Recordkeeping & Reference Materials!Forms & Recordkeeping Systems!Accounting Forms',749,0,31563,NULL,NULL),(31678,'Address & Multi-Use Labels','!Office Supplies!Labels, Indexes & Stamps!Labels & Stickers!Address & Multi-Use Labels',716,0,31568,NULL,NULL),(31679,'Adhesive Putty','!Office Supplies!Tape, Adhesives & Fasteners!Velcro & Mounting Products!Adhesive Putty',595,0,31626,NULL,NULL),(31680,'Agenda/\'To Do\' Pads','!Office Supplies!Forms, Recordkeeping & Reference Materials!Forms & Recordkeeping Systems!Agenda/\'To Do\' Pads',750,0,31563,NULL,NULL),(31681,'Air Fresheners & Deodorizers','!Breakroom and Janitorial!Janitorial & Sanitation Supplies!Cleaning Products!Air Fresheners & Deodorizers',992,0,31403,NULL,NULL),(31682,'All-Purpose Cleaners','!Breakroom and Janitorial!Janitorial & Sanitation Supplies!Cleaning Products!All-Purpose Cleaners',993,0,31403,NULL,NULL),(31683,'Antifatigue/Antistatic Mats','!Breakroom and Janitorial!Facility Maintenance!Mats & Antislip Tape!Antifatigue/Antistatic Mats',1040,0,31361,NULL,NULL),(31684,'Appointment Book/Planner Refills','!Office Supplies!Calendars, Planners & Personal Organizers!Appointment Books/Planners!Appointment Book/Planner Refills',930,0,31497,NULL,NULL),(31685,'Art & Drafting Tape','!Office Supplies!Tape, Adhesives & Fasteners!Tape!Art & Drafting Tape',601,0,31624,NULL,NULL),(31686,'Art Markers','!Office Supplies!Writing & Correction Supplies!Markers!Art Markers',585,0,31631,NULL,NULL),(31687,'Art Paper Rolls','!Office Supplies!Paper & Printable Media!Art Paper & Sketching Pads!Art Paper Rolls',674,0,31589,NULL,NULL),(31688,'Art Paper Sheets & Pads','!Office Supplies!Paper & Printable Media!Art Paper & Sketching Pads!Art Paper Sheets & Pads',675,0,31589,NULL,NULL),(31689,'Art Tissue & Crepe Paper','!Office Supplies!Paper & Printable Media!Art Paper & Sketching Pads!Art Tissue & Crepe Paper',676,0,31589,NULL,NULL),(31690,'Ash/Trash Receptacles','!Breakroom and Janitorial!Waste Receptacles & Liners!Waste Receptacles & Lids!Ash/Trash Receptacles',967,0,31435,NULL,NULL),(31691,'Atlases','!Office Supplies!Forms, Recordkeeping & Reference Materials!Reference Materials!Atlases',741,0,31564,NULL,NULL),(31692,'ATM, Credit/Debit & Financial Paper Rolls','!Office Supplies!Paper & Printable Media!Machine Paper Rolls!ATM, Credit/Debit & Financial Paper Rolls',647,0,31596,NULL,NULL),(31693,'Audio Cables & Adapters','!Technology!Cables, Adapters & Power Products!Cables and Adapters!Audio Cables & Adapters',518,0,31644,NULL,NULL),(31694,'Audio Cassettes','!Technology!Audio Visual Equipment!Audio & Video Cassettes & Tapes!Audio Cassettes',568,0,31638,NULL,NULL),(31695,'Audio Players & Recorders','!Technology!Audio Visual Equipment!Players and Recorders!Audio Players & Recorders',555,0,31640,NULL,NULL),(31696,'Award & Certificate Forms','!Office Supplies!Forms, Recordkeeping & Reference Materials!Forms & Recordkeeping Systems!Award & Certificate Forms',751,0,31563,NULL,NULL),(31697,'Back & Seat Cushions','!Furniture!Chairs & Sofas!Chair Accessories!Back & Seat Cushions',1131,0,31455,NULL,NULL),(31698,'Badge & Card Racks','!Office Supplies!Desk Accessories & Workspace Organizers!Card Files, Holders & Racks!Badge & Card Racks',861,0,31536,NULL,NULL),(31699,'Badge Holders & Inserts','!Office Supplies!Labels, Indexes & Stamps!Identification Badges!Badge Holders & Inserts',737,0,31566,NULL,NULL),(31700,'Ballpoint Pens','!Office Supplies!Writing & Correction Supplies!Pens & Refills!Ballpoint Pens',573,0,31636,NULL,NULL),(31701,'Bathroom Tissue Dispensers','!Breakroom and Janitorial!Facility Maintenance!Dispensers!Bathroom Tissue Dispensers',1054,0,31358,NULL,NULL),(31702,'Batteries & Chargers','!Breakroom and Janitorial!Facility Maintenance!Batteries & Electrical Supplies!Batteries & Chargers',1070,0,31356,NULL,NULL),(31703,'Bean Bags','!Office Supplies!Crafts & Recreation Room Products!Physical Education Equipment!Bean Bags',882,0,31525,NULL,NULL),(31704,'Bell Glides','!Furniture!Chairs & Sofas!Chair Accessories!Bell Glides',1132,0,31455,NULL,NULL),(31705,'Beverages','!Breakroom and Janitorial!Food & Beverage Service!Beverages & Snack Foods!Beverages',1026,0,31393,NULL,NULL),(31706,'Bill Counters','!Office Supplies!Money Handling Products!Paper Currency & Check Handling!Bill Counters',690,0,31579,NULL,NULL),(31707,'Binder Dividers','!Office Supplies!Binders & Binding Systems!Binder Accessories!Binder Dividers',957,0,31491,NULL,NULL),(31708,'Binder Insert Strips','!Office Supplies!Binders & Binding Systems!Binder Accessories!Binder Insert Strips',959,0,31491,NULL,NULL),(31709,'Binder Labels','!Office Supplies!Labels, Indexes & Stamps!Labels & Stickers!Binder Labels',717,0,31568,NULL,NULL),(31710,'Binding Bars','!Office Supplies!Binders & Binding Systems!Report Covers & Pocket Portfolios!Binding Bars',944,0,31495,NULL,NULL),(31711,'Binding Systems Combs & Spines','!Office Supplies!Binders & Binding Systems!Binding Systems Supplies!Binding Systems Combs & Spines',949,0,31493,NULL,NULL),(31712,'Binding Systems Covers & Paper','!Office Supplies!Binders & Binding Systems!Binding Systems Supplies!Binding Systems Covers & Paper',950,0,31493,NULL,NULL),(31713,'Binding Tape','!Office Supplies!Tape, Adhesives & Fasteners!Tape!Binding Tape',602,0,31624,NULL,NULL),(31714,'Board Accessory Kits','!Office Supplies!Presentation/Display & Scheduling Boards!Board Accessories!Board Accessory Kits',630,0,31600,NULL,NULL),(31715,'Board Characters & Shapes','!Office Supplies!Presentation/Display & Scheduling Boards!Board Accessories!Board Characters & Shapes',631,0,31600,NULL,NULL),(31716,'Book Racks','!Office Supplies!Desk Accessories & Workspace Organizers!Bookends & Book Racks!Book Racks',867,0,31535,NULL,NULL),(31717,'Book Repair/Preservation Tape','!Office Supplies!Tape, Adhesives & Fasteners!Tape!Book Repair/Preservation Tape',603,0,31624,NULL,NULL),(31718,'Book Stands','!Office Supplies!Desk Accessories & Workspace Organizers!Platforms, Stands & Shelves!Book Stands',824,0,31546,NULL,NULL),(31719,'Bookcase Doors','!Furniture!Cabinets, Racks & Shelves!Bookcases & Door Kits!Bookcase Doors',1169,0,31437,NULL,NULL),(31720,'Bookcases','!Furniture!Cabinets, Racks & Shelves!Bookcases & Door Kits!Bookcases',1170,0,31437,NULL,NULL),(31721,'Bookends','!Office Supplies!Desk Accessories & Workspace Organizers!Bookends & Book Racks!Bookends',868,0,31535,NULL,NULL),(31722,'Booklet & Catalog Envelopes','!Office Supplies!Envelopes, Mailers & Shipping Supplies!Envelopes!Booklet & Catalog Envelopes',794,0,31551,NULL,NULL),(31723,'Bowling Sets','!Office Supplies!Crafts & Recreation Room Products!Physical Education Equipment!Bowling Sets',883,0,31525,NULL,NULL),(31724,'Brooms','!Breakroom and Janitorial!Janitorial & Sanitation Supplies!Cleaning Tools & Supplies!Brooms',975,0,31404,NULL,NULL),(31725,'Brushes, Rollers & Sponges','!Office Supplies!Crafts & Recreation Room Products!Arts & Crafts Supplies!Brushes, Rollers & Sponges',899,0,31520,NULL,NULL),(31726,'Buckets & Wringers','!Breakroom and Janitorial!Janitorial & Sanitation Supplies!Cleaning Tools & Supplies!Buckets & Wringers',976,0,31404,NULL,NULL),(31727,'Business Card Books & Wallets','!Office Supplies!Desk Accessories & Workspace Organizers!Card Files, Holders & Racks!Business Card Books & Wallets',862,0,31536,NULL,NULL),(31728,'Business Card Holders','!Office Supplies!Desk Accessories & Workspace Organizers!Card Files, Holders & Racks!Business Card Holders',863,0,31536,NULL,NULL),(31729,'Business Cards','!Office Supplies!Paper & Printable Media!Cards & Card Stock!Business Cards',665,0,31592,NULL,NULL),(31730,'Business Letter Envelopes','!Office Supplies!Envelopes, Mailers & Shipping Supplies!Envelopes!Business Letter Envelopes',795,0,31551,NULL,NULL),(31731,'Business Notebooks & Planners','!Office Supplies!Paper & Printable Media!Notebooks & Writing Pads!Business Notebooks & Planners',644,0,31597,NULL,NULL),(31732,'Cabinet Accessories & Components','!Furniture!Cabinets, Racks & Shelves!File & Storage Cabinets!Cabinet Accessories & Components',1153,0,31441,NULL,NULL),(31733,'Cabinet-Style Sorters','!Office Supplies!Desk Accessories & Workspace Organizers!Sorters!Cabinet-Style Sorters',821,0,31547,NULL,NULL),(31734,'Cable & Screw Posts','!Office Supplies!Binders & Binding Systems!Binder Accessories!Cable & Screw Posts',963,0,31491,NULL,NULL),(31735,'Caddy Bags','!Breakroom and Janitorial!Janitorial & Sanitation Supplies!Cleaning Tools & Supplies!Caddy Bags',977,0,31404,NULL,NULL),(31736,'Calculator Stands','!Office Supplies!Desk Accessories & Workspace Organizers!Platforms, Stands & Shelves!Calculator Stands',825,0,31546,NULL,NULL),(31737,'Camcorder Tapes','!Technology!Audio Visual Equipment!Audio & Video Cassettes & Tapes!Camcorder Tapes',569,0,31638,NULL,NULL),(31738,'Camera Film','!Technology!Audio Visual Equipment!Cameras & Accessories!Camera Film',562,0,31639,NULL,NULL),(31739,'Camera Tripods','!Technology!Audio Visual Equipment!Cameras & Accessories!Camera Tripods',563,0,31639,NULL,NULL),(31740,'Candy & Gum','!Breakroom and Janitorial!Food & Beverage Service!Beverages & Snack Foods!Candy & Gum',1027,0,31393,NULL,NULL),(31741,'Carafes & Pitchers','!Breakroom and Janitorial!Food & Beverage Service!Food Service Supplies!Carafes & Pitchers',1010,0,31394,NULL,NULL),(31742,'Card File Boxes & Trays','!Office Supplies!Desk Accessories & Workspace Organizers!Card Files, Holders & Racks!Card File Boxes & Trays',864,0,31536,NULL,NULL),(31743,'Card Protectors & Refill Sleeves','!Office Supplies!Binders & Binding Systems!Sheet Protectors, Card & Photo Sleeves!Card Protectors & Refill Sleeves',937,0,31496,NULL,NULL),(31744,'Card Readers','!Technology!Computer Components, Peripherals & Accessories!Computer Components & Peripherals!Card Readers',488,0,31647,NULL,NULL),(31745,'Card/Cover Stock','!Office Supplies!Paper & Printable Media!Cards & Card Stock!Card/Cover Stock',666,0,31592,NULL,NULL),(31746,'Carpet & Fabric Cleaners','!Breakroom and Janitorial!Janitorial & Sanitation Supplies!Cleaning Products!Carpet & Fabric Cleaners',994,0,31403,NULL,NULL),(31747,'Carrying Files','!Office Supplies!File Folders, Portable & Storage Box Files!Portable Files!Carrying Files',771,0,31559,NULL,NULL),(31748,'Cash Register & POS Paper Rolls','!Office Supplies!Paper & Printable Media!Machine Paper Rolls!Cash Register & POS Paper Rolls',649,0,31596,NULL,NULL),(31749,'Cassette Recorders & Transcribers','!Technology!Office Machines!Dictation Devices!Cassette Recorders & Transcribers',449,0,31657,NULL,NULL),(31750,'Catalog & Reference Rack Accesssories','!Office Supplies!Desk Accessories & Workspace Organizers!Catalog & Reference Racks!Catalog & Reference Rack Accesssories',858,0,31537,NULL,NULL),(31751,'Catalog & Reference Racks','!Office Supplies!Desk Accessories & Workspace Organizers!Catalog & Reference Racks!Catalog & Reference Racks',859,0,31537,NULL,NULL),(31752,'CD/DVD Data Destroyers','!Technology!Office Machines!Shredders & Data Destroyers!CD/DVD Data Destroyers',439,0,31663,NULL,NULL),(31753,'Cellophane Wrap','!Office Supplies!Paper & Printable Media!Art Paper & Sketching Pads!Cellophane Wrap',677,0,31589,NULL,NULL),(31754,'Cellular Phone Holders & Cases','!Technology!Audio Visual Equipment!Telephone & Cellular Accessories!Cellular Phone Holders & Cases',539,0,31642,NULL,NULL),(31755,'Cellular Phone Power Adapters & Chargers','!Technology!Audio Visual Equipment!Telephone & Cellular Accessories!Cellular Phone Power Adapters & Chargers',540,0,31642,NULL,NULL),(31756,'Center/Pencil Drawers','!Furniture!Desks & Workstations!Desk/Workstation Accessories!Center/Pencil Drawers',1109,0,31466,NULL,NULL),(31757,'Chair Arms','!Furniture!Chairs & Sofas!Chair Accessories!Chair Arms',1133,0,31455,NULL,NULL),(31758,'Chair Desk Combos','!Furniture!Chairs & Sofas!Classroom Chairs!Chair Desk Combos',1128,0,31456,NULL,NULL),(31759,'Chair Mats','!Furniture!Chairs & Sofas!Chair Accessories!Chair Mats',1134,0,31455,NULL,NULL),(31760,'Chalk, Eraser & Marker Holders','!Office Supplies!Presentation/Display & Scheduling Boards!Board Accessories!Chalk, Eraser & Marker Holders',635,0,31600,NULL,NULL),(31761,'Children\'s Scissors','!Office Supplies!Cutting & Measuring Devices!Scissors!Children\'s Scissors',869,0,31534,NULL,NULL),(31762,'Clasp Envelopes','!Office Supplies!Envelopes, Mailers & Shipping Supplies!Envelopes!Clasp Envelopes',796,0,31551,NULL,NULL),(31763,'Classroom Desks','!Furniture!Desks & Workstations!Desks!Classroom Desks',1105,0,31467,NULL,NULL),(31764,'Classroom Paper','!Office Supplies!Paper & Printable Media!Loose Paper!Classroom Paper',654,0,31595,NULL,NULL),(31765,'Cleaning Brushes','!Breakroom and Janitorial!Janitorial & Sanitation Supplies!Cleaning Tools & Supplies!Cleaning Brushes',978,0,31404,NULL,NULL),(31766,'Clipboards','!Office Supplies!Forms, Recordkeeping & Reference Materials!Clipboards & Forms Holders!Clipboards',766,0,31562,NULL,NULL),(31767,'Coat Hangers','!Furniture!Cabinets, Racks & Shelves!Coat Racks & Hangers!Coat Hangers',1166,0,31438,NULL,NULL),(31768,'Coffee Filters','!Breakroom and Janitorial!Food & Beverage Service!Food Service Supplies!Coffee Filters',1011,0,31394,NULL,NULL),(31769,'Coffee Makers','!Breakroom and Janitorial!Food & Beverage Service!Appliances!Coffee Makers',1031,0,31392,NULL,NULL),(31770,'Coin Changers','!Office Supplies!Money Handling Products!Coin Handling!Coin Changers',695,0,31577,NULL,NULL),(31771,'Coin Counters','!Office Supplies!Money Handling Products!Coin Handling!Coin Counters',696,0,31577,NULL,NULL),(31772,'Coin Roll Wrappers','!Office Supplies!Money Handling Products!Coin Handling!Coin Roll Wrappers',697,0,31577,NULL,NULL),(31773,'Coin Sorters','!Office Supplies!Money Handling Products!Coin Handling!Coin Sorters',698,0,31577,NULL,NULL),(31774,'Coin Storage/Shipping Boxes','!Office Supplies!Money Handling Products!Coin Handling!Coin Storage/Shipping Boxes',699,0,31577,NULL,NULL),(31775,'Coin/Currency Bags','!Office Supplies!Money Handling Products!Cash Bags!Coin/Currency Bags',701,0,31574,NULL,NULL),(31776,'Color-Coding Labels','!Office Supplies!Labels, Indexes & Stamps!Labels & Stickers!Color-Coding Labels',719,0,31568,NULL,NULL),(31777,'Colored Paper','!Office Supplies!Paper & Printable Media!Loose Paper!Colored Paper',655,0,31595,NULL,NULL),(31778,'Commercial Shelving','!Furniture!Cabinets, Racks & Shelves!Shelving!Commercial Shelving',1141,0,31445,NULL,NULL),(31779,'Compressed Air Dusters','!Office Supplies!Office Equipment Cleaners!Electronics Equipment Cleaners!Compressed Air Dusters',684,0,31588,NULL,NULL),(31780,'Computer Cleaning Supplies','!Technology!Computer Components, Peripherals & Accessories!Computer Accessories!Computer Cleaning Supplies',2,0,31646,NULL,NULL),(31781,'Computer Equipment Dustcovers','!Technology!Computer Components, Peripherals & Accessories!Computer Accessories!Computer Equipment Dustcovers',498,0,31646,NULL,NULL),(31782,'Computer Locks & Security Cables','!Technology!Computer Components, Peripherals & Accessories!Computer Accessories!Computer Locks & Security Cables',499,0,31646,NULL,NULL),(31783,'Computer Tool Kits','!Technology!Computer Components, Peripherals & Accessories!Computer Accessories!Computer Tool Kits',500,0,31646,NULL,NULL),(31784,'Computer Vacs & Supplies','!Office Supplies!Office Equipment Cleaners!Electronics Equipment Cleaners!Computer Vacs & Supplies',685,0,31588,NULL,NULL),(31785,'Computer Workstation Components','!Furniture!Desks & Workstations!Computer Workstations & Components!Computer Workstation Components',1119,0,31464,NULL,NULL),(31786,'Computer Workstations','!Furniture!Desks & Workstations!Computer Workstations & Components!Computer Workstations',1120,0,31464,NULL,NULL),(31787,'Condiments','!Breakroom and Janitorial!Food & Beverage Service!Beverages & Snack Foods!Condiments',1028,0,31393,NULL,NULL),(31788,'Cones','!Office Supplies!Crafts & Recreation Room Products!Physical Education Equipment!Cones',884,0,31525,NULL,NULL),(31789,'Conference Telephones','!Technology!Audio Visual Equipment!Telephones!Conference Telephones',534,0,31643,NULL,NULL),(31790,'Construction Paper','!Office Supplies!Paper & Printable Media!Art Paper & Sketching Pads!Construction Paper',678,0,31589,NULL,NULL),(31791,'Copier, Fax & Laser Printer Supplies','!Technology!Imaging Supplies and Accessories!Imaging Supplies!Copier, Fax & Laser Printer Supplies',464,0,31649,NULL,NULL),(31792,'Copiers','!Technology!Office Machines!Copiers, Fax Machines & Printers!Copiers',452,0,31656,NULL,NULL),(31793,'Copy & Multipurpose Office Paper','!Office Supplies!Paper & Printable Media!Loose Paper!Copy & Multipurpose Office Paper',656,0,31595,NULL,NULL),(31794,'Correction Fluid & Pens','!Office Supplies!Writing & Correction Supplies!Erasers & Correction Products!Correction Fluid & Pens',592,0,31629,NULL,NULL),(31795,'Correction Tape & Film','!Office Supplies!Writing & Correction Supplies!Erasers & Correction Products!Correction Tape & Film',593,0,31629,NULL,NULL),(31796,'Counter Pens','!Office Supplies!Writing & Correction Supplies!Pens & Refills!Counter Pens',574,0,31636,NULL,NULL),(31797,'Counterfeit Bill Detectors','!Office Supplies!Money Handling Products!Paper Currency & Check Handling!Counterfeit Bill Detectors',693,0,31579,NULL,NULL),(31798,'CPU Holders','!Furniture!Desks & Workstations!Computer Workstations & Components!CPU Holders',1121,0,31464,NULL,NULL),(31799,'Craft Assembly/Decorating Materials','!Office Supplies!Crafts & Recreation Room Products!Arts & Crafts Supplies!Craft Assembly/Decorating Materials',901,0,31520,NULL,NULL),(31800,'Craft Knife Blades','!Office Supplies!Cutting & Measuring Devices!Craft Knives & Blades!Craft Knife Blades',879,0,31528,NULL,NULL),(31801,'Craft Knives','!Office Supplies!Cutting & Measuring Devices!Craft Knives & Blades!Craft Knives',880,0,31528,NULL,NULL),(31802,'Craft Supplies Dispensers & Containers','!Office Supplies!Crafts & Recreation Room Products!Arts & Crafts Supplies!Craft Supplies Dispensers & Containers',903,0,31520,NULL,NULL),(31803,'Cups & Glasses','!Breakroom and Janitorial!Food & Beverage Service!Food Service Supplies!Cups & Glasses',1012,0,31394,NULL,NULL),(31804,'Currency Bands & Straps','!Office Supplies!Money Handling Products!Paper Currency & Check Handling!Currency Bands & Straps',694,0,31579,NULL,NULL),(31805,'Cushioning Materials','!Office Supplies!Envelopes, Mailers & Shipping Supplies!Packing Materials!Cushioning Materials',786,0,31553,NULL,NULL),(31806,'Custom Order Stamps','!Office Supplies!Labels, Indexes & Stamps!Stamps & Stamp Supplies!Custom Order Stamps',706,0,31570,NULL,NULL),(31807,'Daily Appointment Books','!Office Supplies!Calendars, Planners & Personal Organizers!Appointment Books/Planners!Daily Appointment Books',931,0,31497,NULL,NULL),(31808,'Daily/Monthly Appointment Books','!Office Supplies!Calendars, Planners & Personal Organizers!Appointment Books/Planners!Daily/Monthly Appointment Books',932,0,31497,NULL,NULL),(31809,'Data Binders','!Office Supplies!Binders & Binding Systems!Binders!Data Binders',952,0,31492,NULL,NULL),(31810,'Data Cards','!Office Supplies!Paper & Printable Media!Cards & Card Stock!Data Cards',667,0,31592,NULL,NULL),(31811,'Data Drive/Tape Cleaning Cartridges','!Technology!Storage Media!Data Tapes!Data Drive/Tape Cleaning Cartridges',425,0,31668,NULL,NULL),(31812,'Daters','!Office Supplies!Labels, Indexes & Stamps!Stamps & Stamp Supplies!Daters',707,0,31570,NULL,NULL),(31813,'Design & Modeling Tools','!Office Supplies!Crafts & Recreation Room Products!Arts & Crafts Supplies!Design & Modeling Tools',904,0,31520,NULL,NULL),(31814,'Desk & Table Lamps','!Furniture!Room Accessories!Lamps & Lighting!Desk & Table Lamps',1087,0,31479,NULL,NULL),(31815,'Desk Calendar Bases','!Office Supplies!Desk Accessories & Workspace Organizers!Desk Supplies Holders & Dispensers!Desk Calendar Bases',844,0,31540,NULL,NULL),(31816,'Desk Calendar Sets & Refills','!Office Supplies!Calendars, Planners & Personal Organizers!Desktop Calendars & Planners!Desk Calendar Sets & Refills',927,0,31498,NULL,NULL),(31817,'Desk Media Files','!Office Supplies!Desk Accessories & Workspace Organizers!Media Organizers!Desk Media Files',834,0,31544,NULL,NULL),(31818,'Desk Pad Calendars & Refills','!Office Supplies!Calendars, Planners & Personal Organizers!Desktop Calendars & Planners!Desk Pad Calendars & Refills',928,0,31498,NULL,NULL),(31819,'Desk Pad Refills','!Office Supplies!Desk Accessories & Workspace Organizers!Desk Pads & Refills!Desk Pad Refills',856,0,31539,NULL,NULL),(31820,'Desk Pads','!Office Supplies!Desk Accessories & Workspace Organizers!Desk Pads & Refills!Desk Pads',857,0,31539,NULL,NULL),(31821,'Desk Supplies Organizers','!Office Supplies!Desk Accessories & Workspace Organizers!Desk Supplies Holders & Dispensers!Desk Supplies Organizers',846,0,31540,NULL,NULL),(31822,'Desktop Dry Erase Planners','!Office Supplies!Calendars, Planners & Personal Organizers!Desktop Calendars & Planners!Desktop Dry Erase Planners',929,0,31498,NULL,NULL),(31823,'Desktop Literature Display Racks','!Office Supplies!Desk Accessories & Workspace Organizers!Literature Display Racks & Holders!Desktop Literature Display Racks',838,0,31543,NULL,NULL),(31824,'Desktop/Off-Surface Shelves','!Office Supplies!Desk Accessories & Workspace Organizers!Platforms, Stands & Shelves!Desktop/Off-Surface Shelves',826,0,31546,NULL,NULL),(31825,'Desktop/Portable Dry-Erase Boards','!Office Supplies!Presentation/Display & Scheduling Boards!Dry Erase Boards!Desktop/Portable Dry-Erase Boards',622,0,31606,NULL,NULL),(31826,'Dictation Accessories','!Technology!Office Machines!Dictation Devices!Dictation Accessories',450,0,31657,NULL,NULL),(31827,'Digital Cameras & Accessories','!Technology!Audio Visual Equipment!Cameras & Accessories!Digital Cameras & Accessories',564,0,31639,NULL,NULL),(31828,'Digital Linear Tape(DLT)','!Technology!Storage Media!Data Tapes!Digital Linear Tape(DLT)',426,0,31668,NULL,NULL),(31829,'Digital Voice Recorders','!Technology!Office Machines!Dictation Devices!Digital Voice Recorders',451,0,31657,NULL,NULL),(31830,'Digital Writing Systems & Notepads','!Technology!Computer Components, Peripherals & Accessories!Computer Components & Peripherals!Digital Writing Systems & Notepads',489,0,31647,NULL,NULL),(31831,'Disc, Drive & Head Cleaners','!Technology!Computer Components, Peripherals & Accessories!Computer Accessories!Disc, Drive & Head Cleaners',501,0,31646,NULL,NULL),(31832,'Dish/Dishwasher Detergents','!Breakroom and Janitorial!Janitorial & Sanitation Supplies!Cleaning Products!Dish/Dishwasher Detergents',995,0,31403,NULL,NULL),(31833,'Disinfectant Cleaners','!Breakroom and Janitorial!Janitorial & Sanitation Supplies!Cleaning Products!Disinfectant Cleaners',996,0,31403,NULL,NULL),(31834,'Disk Drives & Memory','!Technology!Computer Components, Peripherals & Accessories!Computer Components & Peripherals!Disk Drives & Memory',490,0,31647,NULL,NULL),(31835,'Display Board/Booth Accessories','!Office Supplies!Presentation/Display & Scheduling Boards!Display Board Systems!Display Board/Booth Accessories',625,0,31605,NULL,NULL),(31836,'Display Boards/Booths','!Office Supplies!Presentation/Display & Scheduling Boards!Display Board Systems!Display Boards/Booths',626,0,31605,NULL,NULL),(31837,'Display Easel Binders','!Office Supplies!Binders & Binding Systems!Binders!Display Easel Binders',953,0,31492,NULL,NULL),(31838,'Disposable/Renewable Mats','!Breakroom and Janitorial!Facility Maintenance!Mats & Antislip Tape!Disposable/Renewable Mats',1042,0,31361,NULL,NULL),(31839,'Disposible Glove Dispensers','!Breakroom and Janitorial!Facility Maintenance!Dispensers!Disposible Glove Dispensers',1055,0,31358,NULL,NULL),(31840,'Docking Stations','!Technology!Computer Components, Peripherals & Accessories!Computer Components & Peripherals!Docking Stations',491,0,31647,NULL,NULL),(31841,'Door Chimes','!Breakroom and Janitorial!Facility Maintenance!Building Safety & Security Products!Door Chimes',1061,0,31357,NULL,NULL),(31842,'Drain Openers','!Breakroom and Janitorial!Janitorial & Sanitation Supplies!Cleaning Products!Drain Openers',997,0,31403,NULL,NULL),(31843,'Drawing Paper & Sketch Pads','!Office Supplies!Paper & Printable Media!Art Paper & Sketching Pads!Drawing Paper & Sketch Pads',679,0,31589,NULL,NULL),(31844,'Dry Erase Markers','!Office Supplies!Writing & Correction Supplies!Markers!Dry Erase Markers',586,0,31631,NULL,NULL),(31845,'Dry Erase Sheets','!Office Supplies!Paper & Printable Media!Loose Paper!Dry Erase Sheets',657,0,31595,NULL,NULL),(31846,'Duct Tape','!Office Supplies!Tape, Adhesives & Fasteners!Tape!Duct Tape',604,0,31624,NULL,NULL),(31847,'Duplex Units','!Technology!Imaging Supplies and Accessories!Imaging Machine Accessories!Duplex Units',475,0,31648,NULL,NULL),(31848,'Dust Masks','!Breakroom and Janitorial!Safety & Sanitary Wear!Dust Masks & Respirators!Dust Masks',972,0,31417,NULL,NULL),(31849,'Dusters','!Breakroom and Janitorial!Janitorial & Sanitation Supplies!Cleaning Tools & Supplies!Dusters',979,0,31404,NULL,NULL),(31850,'Dustpans','!Breakroom and Janitorial!Janitorial & Sanitation Supplies!Cleaning Tools & Supplies!Dustpans',980,0,31404,NULL,NULL),(31851,'Easel-Style Dry Erase Boards','!Office Supplies!Presentation/Display & Scheduling Boards!Dry Erase Boards!Easel-Style Dry Erase Boards',623,0,31606,NULL,NULL),(31852,'Electric/Battery-Operated Pencil Sharpeners','!Office Supplies!Writing & Correction Supplies!Pencil Sharpeners!Electric/Battery-Operated Pencil Sharpeners',583,0,31634,NULL,NULL),(31853,'Electric/Battery-Operated Staplers','!Office Supplies!Staplers & Punches!Staplers!Electric/Battery-Operated Staplers',617,0,31613,NULL,NULL),(31854,'Electronic Personal Organizers','!Technology!Computer Components, Peripherals & Accessories!Computer Components & Peripherals!Electronic Personal Organizers',492,0,31647,NULL,NULL),(31855,'Electronic Time Recorders','!Technology!Office Machines!Time Clocks, Cards & Badges!Electronic Time Recorders',435,0,31664,NULL,NULL),(31856,'Electronics Cleaning Kits','!Office Supplies!Office Equipment Cleaners!Electronics Equipment Cleaners!Electronics Cleaning Kits',687,0,31588,NULL,NULL),(31857,'Electronics Cleaning Wipes & Swabs','!Office Supplies!Office Equipment Cleaners!Electronics Equipment Cleaners!Electronics Cleaning Wipes & Swabs',688,0,31588,NULL,NULL),(31858,'Electronics Spray & Liquid Cleaners','!Office Supplies!Office Equipment Cleaners!Electronics Equipment Cleaners!Electronics Spray & Liquid Cleaners',689,0,31588,NULL,NULL),(31859,'Embossers','!Office Supplies!Labels, Indexes & Stamps!Stamps & Stamp Supplies!Embossers',708,0,31570,NULL,NULL),(31860,'Emergency Lighting','!Breakroom and Janitorial!Facility Maintenance!Building Safety & Security Products!Emergency Lighting',1062,0,31357,NULL,NULL),(31861,'Entrance Mats','!Breakroom and Janitorial!Facility Maintenance!Mats & Antislip Tape!Entrance Mats',1043,0,31361,NULL,NULL),(31862,'Equipment Storage','!Office Supplies!Crafts & Recreation Room Products!Physical Education Equipment!Equipment Storage',885,0,31525,NULL,NULL),(31863,'Erasers','!Office Supplies!Writing & Correction Supplies!Erasers & Correction Products!Erasers',594,0,31629,NULL,NULL),(31864,'Expansion & Jumbo Envelopes','!Office Supplies!Envelopes, Mailers & Shipping Supplies!Envelopes!Expansion & Jumbo Envelopes',798,0,31551,NULL,NULL),(31865,'Extension Poles','!Breakroom and Janitorial!Janitorial & Sanitation Supplies!Cleaning Tools & Supplies!Extension Poles',981,0,31404,NULL,NULL),(31866,'Fabric Repair Kits','!Breakroom and Janitorial!Janitorial & Sanitation Supplies!Cleaning Tools & Supplies!Fabric Repair Kits',982,0,31404,NULL,NULL),(31867,'Facility Waste Receptacles','!Breakroom and Janitorial!Waste Receptacles & Liners!Waste Receptacles & Lids!Facility Waste Receptacles',968,0,31435,NULL,NULL),(31868,'Fax Accessories','!Technology!Imaging Supplies and Accessories!Imaging Machine Accessories!Fax Accessories',476,0,31648,NULL,NULL),(31869,'Fax Paper Rolls','!Office Supplies!Paper & Printable Media!Machine Paper Rolls!Fax Paper Rolls',650,0,31596,NULL,NULL),(31870,'File Folder Inserts','!Office Supplies!File Folders, Portable & Storage Box Files!File & Folder Accessories!File Folder Inserts',780,0,31555,NULL,NULL),(31871,'File Folder Racks & Holders','!Office Supplies!File Folders, Portable & Storage Box Files!Portable Files!File Folder Racks & Holders',773,0,31559,NULL,NULL),(31872,'File Guides','!Office Supplies!Labels, Indexes & Stamps!Index Dividers, Tabs & File Guides!File Guides',734,0,31567,NULL,NULL),(31873,'File Jackets & File Pockets','!Office Supplies!File Folders, Portable & Storage Box Files!Folders!File Jackets & File Pockets',775,0,31558,NULL,NULL),(31874,'Filing Crates & Posting Tubs','!Office Supplies!File Folders, Portable & Storage Box Files!Portable Files!Filing Crates & Posting Tubs',774,0,31559,NULL,NULL),(31875,'Filing Labels','!Office Supplies!Labels, Indexes & Stamps!Labels & Stickers!Filing Labels',720,0,31568,NULL,NULL),(31876,'Film Cameras','!Technology!Audio Visual Equipment!Cameras & Accessories!Film Cameras',565,0,31639,NULL,NULL),(31877,'Film/Tape Dry Adhesives','!Office Supplies!Tape, Adhesives & Fasteners!Adhesives & Glue!Film/Tape Dry Adhesives',611,0,31615,NULL,NULL),(31878,'Financial Calculators','!Technology!Office Machines!Calculators & Counters!Financial Calculators',458,0,31652,NULL,NULL),(31879,'Fire Extinguishers','!Breakroom and Janitorial!Facility Maintenance!Building Safety & Security Products!Fire Extinguishers',1063,0,31357,NULL,NULL),(31880,'Firewire Cables & Cards','!Technology!Cables, Adapters & Power Products!Cables and Adapters!Firewire Cables & Cards',520,0,31644,NULL,NULL),(31881,'Flashlights & Lanterns','!Breakroom and Janitorial!Facility Maintenance!Batteries & Electrical Supplies!Flashlights & Lanterns',1071,0,31356,NULL,NULL),(31882,'Flat Files','!Furniture!Cabinets, Racks & Shelves!File & Storage Cabinets!Flat Files',1155,0,31441,NULL,NULL),(31883,'Flip Chart Markers','!Office Supplies!Writing & Correction Supplies!Markers!Flip Chart Markers',587,0,31631,NULL,NULL),(31884,'Floor Cleaners & Polishes','!Breakroom and Janitorial!Janitorial & Sanitation Supplies!Cleaning Products!Floor Cleaners & Polishes',998,0,31403,NULL,NULL),(31885,'Floor Lamps','!Furniture!Room Accessories!Lamps & Lighting!Floor Lamps',1089,0,31479,NULL,NULL),(31886,'Floor Machines & Vacuums','!Breakroom and Janitorial!Janitorial & Sanitation Supplies!Cleaning Tools & Supplies!Floor Machines & Vacuums',983,0,31404,NULL,NULL),(31887,'Floor Stand Coat Racks','!Furniture!Cabinets, Racks & Shelves!Coat Racks & Hangers!Floor Stand Coat Racks',1167,0,31438,NULL,NULL),(31888,'Folding Activity/Utility Tables','!Furniture!Tables!Activity & Utility Tables!Folding Activity/Utility Tables',1079,0,31485,NULL,NULL),(31889,'Food Bags & Wrap','!Breakroom and Janitorial!Food & Beverage Service!Food Service Supplies!Food Bags & Wrap',1013,0,31394,NULL,NULL),(31890,'Food Storage Containers','!Breakroom and Janitorial!Food & Beverage Service!Food Service Supplies!Food Storage Containers',1014,0,31394,NULL,NULL),(31891,'Food/Beverage Service Supplies Dispensers','!Breakroom and Janitorial!Food & Beverage Service!Food Service Supplies!Food/Beverage Service Supplies Dispensers',1015,0,31394,NULL,NULL),(31892,'Footrests','!Furniture!Chairs & Sofas!Chair Accessories!Footrests',1135,0,31455,NULL,NULL),(31893,'Forms & Check Envelopes','!Office Supplies!Envelopes, Mailers & Shipping Supplies!Envelopes!Forms & Check Envelopes',800,0,31551,NULL,NULL),(31894,'Fountain & Calligraphy Pens','!Office Supplies!Writing & Correction Supplies!Pens & Refills!Fountain & Calligraphy Pens',575,0,31636,NULL,NULL),(31895,'Fragrance/Deodorizer Dispensers','!Breakroom and Janitorial!Facility Maintenance!Dispensers!Fragrance/Deodorizer Dispensers',1056,0,31358,NULL,NULL),(31896,'Furniture Polish & Touch-up','!Breakroom and Janitorial!Janitorial & Sanitation Supplies!Cleaning Products!Furniture Polish & Touch-up',999,0,31403,NULL,NULL),(31897,'Ganging Hardware','!Furniture!Chairs & Sofas!Chair Accessories!Ganging Hardware',1136,0,31455,NULL,NULL),(31898,'Glass Cleaners','!Breakroom and Janitorial!Janitorial & Sanitation Supplies!Cleaning Products!Glass Cleaners',1000,0,31403,NULL,NULL),(31899,'Globes','!Office Supplies!Forms, Recordkeeping & Reference Materials!Reference Materials!Globes',744,0,31564,NULL,NULL),(31900,'Glue Sticks & Pens','!Office Supplies!Tape, Adhesives & Fasteners!Adhesives & Glue!Glue Sticks & Pens',612,0,31615,NULL,NULL),(31901,'Graphing & Scientific Calculators','!Technology!Office Machines!Calculators & Counters!Graphing & Scientific Calculators',459,0,31652,NULL,NULL),(31902,'Greeting Card Envelopes','!Office Supplies!Envelopes, Mailers & Shipping Supplies!Envelopes!Greeting Card Envelopes',801,0,31551,NULL,NULL),(31903,'Hand Letter Openers','!Office Supplies!Cutting & Measuring Devices!Letter Openers!Hand Letter Openers',878,0,31530,NULL,NULL),(31904,'Hand Tools','!Breakroom and Janitorial!Facility Maintenance!Maintenance Tools!Hand Tools',1046,0,31360,NULL,NULL),(31905,'Hanging Folder Bars & Frames','!Office Supplies!File Folders, Portable & Storage Box Files!File & Folder Accessories!Hanging Folder Bars & Frames',784,0,31555,NULL,NULL),(31906,'Hanging Folders & Interior Folders','!Office Supplies!File Folders, Portable & Storage Box Files!Folders!Hanging Folders & Interior Folders',776,0,31558,NULL,NULL),(31907,'Hanging Sheet File Racks','!Furniture!Cabinets, Racks & Shelves!File & Storage Racks!Hanging Sheet File Racks',1148,0,31442,NULL,NULL),(31908,'High-Back Swivel/Tilt Chairs','!Furniture!Chairs & Sofas!Executive & Task Chairs!High-Back Swivel/Tilt Chairs',1124,0,31457,NULL,NULL),(31909,'Human Resources Forms & Software','!Office Supplies!Forms, Recordkeeping & Reference Materials!Forms & Recordkeeping Systems!Human Resources Forms & Software',752,0,31563,NULL,NULL),(31910,'Hutches/Stack-On Units','!Furniture!Desks & Workstations!Hutches & Stack-On Storage Units!Hutches/Stack-On Units',1102,0,31468,NULL,NULL),(31911,'Ice Buckets & Coolers','!Breakroom and Janitorial!Food & Beverage Service!Food Service Supplies!Ice Buckets & Coolers',1016,0,31394,NULL,NULL),(31912,'ID Wristbands','!Office Supplies!Labels, Indexes & Stamps!Identification Badges!ID Wristbands',739,0,31566,NULL,NULL),(31913,'Illustration & Foam Boards','!Office Supplies!Crafts & Recreation Room Products!Arts & Crafts Supplies!Illustration & Foam Boards',906,0,31520,NULL,NULL),(31914,'Image Transfer Sheets','!Office Supplies!Paper & Printable Media!Loose Paper!Image Transfer Sheets',658,0,31595,NULL,NULL),(31915,'Index Cards','!Office Supplies!Paper & Printable Media!Cards & Card Stock!Index Cards',668,0,31592,NULL,NULL),(31916,'Index Dividers','!Office Supplies!Labels, Indexes & Stamps!Index Dividers, Tabs & File Guides!Index Dividers',735,0,31567,NULL,NULL),(31917,'Index Tabs & Tab Inserts','!Office Supplies!Labels, Indexes & Stamps!Index Dividers, Tabs & File Guides!Index Tabs & Tab Inserts',736,0,31567,NULL,NULL),(31918,'Industrial Steel/Wire Shelving','!Furniture!Cabinets, Racks & Shelves!Shelving!Industrial Steel/Wire Shelving',1143,0,31445,NULL,NULL),(31919,'Inflating Pumps & Needles','!Office Supplies!Crafts & Recreation Room Products!Physical Education Equipment!Inflating Pumps & Needles',888,0,31525,NULL,NULL),(31920,'Inkjet Printer Paper & Media','!Office Supplies!Paper & Printable Media!Printer Paper!Inkjet Printer Paper & Media',641,0,31598,NULL,NULL),(31921,'Inkjet Printer Supplies','!Technology!Imaging Supplies and Accessories!Imaging Supplies!Inkjet Printer Supplies',466,0,31649,NULL,NULL),(31922,'Insecticides','!Breakroom and Janitorial!Janitorial & Sanitation Supplies!Cleaning Products!Insecticides',1001,0,31403,NULL,NULL),(31923,'Instructor Chairs','!Furniture!Chairs & Sofas!Classroom Chairs!Instructor Chairs',1129,0,31456,NULL,NULL),(31924,'Interoffice/Routing Envelopes','!Office Supplies!Envelopes, Mailers & Shipping Supplies!Envelopes!Interoffice/Routing Envelopes',802,0,31551,NULL,NULL),(31925,'Jewel/Media Storage Cases','!Office Supplies!Desk Accessories & Workspace Organizers!Media Organizers!Jewel/Media Storage Cases',835,0,31544,NULL,NULL),(31926,'Jump Ropes','!Office Supplies!Crafts & Recreation Room Products!Physical Education Equipment!Jump Ropes',889,0,31525,NULL,NULL),(31927,'Key Control','!Breakroom and Janitorial!Facility Maintenance!Building Safety & Security Products!Key Control',1064,0,31357,NULL,NULL),(31928,'Keyboard & Mouse Combos','!Technology!Computer Components, Peripherals & Accessories!Computer Components & Peripherals!Keyboard & Mouse Combos',493,0,31647,NULL,NULL),(31929,'Keyboard, Monitor & Modem Cables & Adapters','!Technology!Cables, Adapters & Power Products!Cables and Adapters!Keyboard, Monitor & Modem Cables & Adapters',521,0,31644,NULL,NULL),(31930,'Keyboard/Mouse Drawers & Platforms','!Office Supplies!Desk Accessories & Workspace Organizers!Platforms, Stands & Shelves!Keyboard/Mouse Drawers & Platforms',827,0,31546,NULL,NULL),(31931,'Keyboards','!Technology!Computer Components, Peripherals & Accessories!Computer Components & Peripherals!Keyboards',494,0,31647,NULL,NULL),(31932,'Kitchen Roll Towels','!Breakroom and Janitorial!Food & Beverage Service!Food Service Supplies!Kitchen Roll Towels',1017,0,31394,NULL,NULL),(31933,'Label Applicators','!Office Supplies!Labels, Indexes & Stamps!Labels & Stickers!Label Applicators',721,0,31568,NULL,NULL),(31934,'Label Holders & Label Protectors','!Office Supplies!Labels, Indexes & Stamps!Labels & Stickers!Label Holders & Label Protectors',722,0,31568,NULL,NULL),(31935,'Label Maker Die-Cut & Continuous Labels','!Technology!Office Machines!Label Makers & Supplies!Label Maker Die-Cut & Continuous Labels',446,0,31659,NULL,NULL),(31936,'Label Maker Tapes & Ribbons','!Technology!Office Machines!Label Makers & Supplies!Label Maker Tapes & Ribbons',447,0,31659,NULL,NULL),(31937,'Label Makers','!Technology!Office Machines!Label Makers & Supplies!Label Makers',448,0,31659,NULL,NULL),(31938,'Labor Law Posters','!Office Supplies!Forms, Recordkeeping & Reference Materials!Reference Materials!Labor Law Posters',745,0,31564,NULL,NULL),(31939,'Laminating Machine Supplies','!Technology!Office Machines!Laminators & Supplies!Laminating Machine Supplies',444,0,31660,NULL,NULL),(31940,'Laminating Machines','!Technology!Office Machines!Laminators & Supplies!Laminating Machines',445,0,31660,NULL,NULL),(31941,'LAN Station Accessories','!Furniture!Cabinets, Racks & Shelves!Shelving!LAN Station Accessories',1144,0,31445,NULL,NULL),(31942,'Laser Printer Paper','!Office Supplies!Paper & Printable Media!Printer Paper!Laser Printer Paper',642,0,31598,NULL,NULL),(31943,'Lateral Files','!Furniture!Cabinets, Racks & Shelves!File & Storage Cabinets!Lateral Files',1156,0,31441,NULL,NULL),(31944,'Laundry Products','!Breakroom and Janitorial!Janitorial & Sanitation Supplies!Cleaning Products!Laundry Products',1002,0,31403,NULL,NULL),(31945,'Leads','!Office Supplies!Writing & Correction Supplies!Pencils!Leads',580,0,31635,NULL,NULL),(31946,'Ledger Posting Systems','!Office Supplies!Forms, Recordkeeping & Reference Materials!Forms & Recordkeeping Systems!Ledger Posting Systems',754,0,31563,NULL,NULL),(31947,'Legal Envelopes','!Office Supplies!Envelopes, Mailers & Shipping Supplies!Envelopes!Legal Envelopes',803,0,31551,NULL,NULL),(31948,'Legal Forms & Kits','!Office Supplies!Forms, Recordkeeping & Reference Materials!Forms & Recordkeeping Systems!Legal Forms & Kits',755,0,31563,NULL,NULL),(31949,'Letter Tray Supports','!Office Supplies!Desk Accessories & Workspace Organizers!Letter Trays & Stacking Supports!Letter Tray Supports',842,0,31542,NULL,NULL),(31950,'Letter Trays','!Office Supplies!Desk Accessories & Workspace Organizers!Letter Trays & Stacking Supports!Letter Trays',843,0,31542,NULL,NULL),(31951,'Light Bulbs','!Breakroom and Janitorial!Facility Maintenance!Batteries & Electrical Supplies!Light Bulbs',1072,0,31356,NULL,NULL),(31952,'Linear Tape Open(LTO)','!Technology!Storage Media!Data Tapes!Linear Tape Open(LTO)',427,0,31668,NULL,NULL),(31953,'Liquid Ink Highlighters','!Office Supplies!Writing & Correction Supplies!Highlighters!Liquid Ink Highlighters',591,0,31630,NULL,NULL),(31954,'Liquid/White Glues','!Office Supplies!Tape, Adhesives & Fasteners!Adhesives & Glue!Liquid/White Glues',613,0,31615,NULL,NULL),(31955,'Literature Display Holders','!Furniture!Cabinets, Racks & Shelves!Display Racks & Cases!Literature Display Holders',1165,0,31440,NULL,NULL),(31956,'Lockers & Storage Cabinets','!Furniture!Cabinets, Racks & Shelves!File & Storage Cabinets!Lockers & Storage Cabinets',1157,0,31441,NULL,NULL),(31957,'Looseleaf Binder Paper','!Office Supplies!Paper & Printable Media!Loose Paper!Looseleaf Binder Paper',661,0,31595,NULL,NULL),(31958,'Low-Back Swivel/Tilt Chairs','!Furniture!Chairs & Sofas!Executive & Task Chairs!Low-Back Swivel/Tilt Chairs',1125,0,31457,NULL,NULL),(31959,'Magazine Covers','!Office Supplies!Desk Accessories & Workspace Organizers!Literature Display Racks & Holders!Magazine Covers',839,0,31543,NULL,NULL),(31960,'Magazine Files','!Office Supplies!Desk Accessories & Workspace Organizers!Literature Display Racks & Holders!Magazine Files',840,0,31543,NULL,NULL),(31961,'Magnifier Lamps','!Furniture!Room Accessories!Lamps & Lighting!Magnifier Lamps',1091,0,31479,NULL,NULL),(31962,'Mail Bags & Transit Sacks','!Office Supplies!Envelopes, Mailers & Shipping Supplies!Bags & Sacks!Mail Bags & Transit Sacks',806,0,31550,NULL,NULL),(31963,'Mailer Envelopes','!Office Supplies!Envelopes, Mailers & Shipping Supplies!Mailers!Mailer Envelopes',790,0,31552,NULL,NULL),(31964,'Mailing Boxes & Shipping Cartons','!Office Supplies!Envelopes, Mailers & Shipping Supplies!Mailers!Mailing Boxes & Shipping Cartons',791,0,31552,NULL,NULL),(31965,'Mailing Tubes','!Office Supplies!Envelopes, Mailers & Shipping Supplies!Mailers!Mailing Tubes',792,0,31552,NULL,NULL),(31966,'Maintenance Kits','!Technology!Imaging Supplies and Accessories!Imaging Supplies!Maintenance Kits',467,0,31649,NULL,NULL),(31967,'Manual Pencil Sharpeners','!Office Supplies!Writing & Correction Supplies!Pencil Sharpeners!Manual Pencil Sharpeners',584,0,31634,NULL,NULL),(31968,'Manual Staplers','!Office Supplies!Staplers & Punches!Staplers!Manual Staplers',618,0,31613,NULL,NULL),(31969,'Manual Time Clocks','!Technology!Office Machines!Time Clocks, Cards & Badges!Manual Time Clocks',436,0,31664,NULL,NULL),(31970,'Maps','!Office Supplies!Forms, Recordkeeping & Reference Materials!Reference Materials!Maps',746,0,31564,NULL,NULL),(31971,'Masking Tape','!Office Supplies!Tape, Adhesives & Fasteners!Tape!Masking Tape',605,0,31624,NULL,NULL),(31972,'Mechanical Pencils','!Office Supplies!Writing & Correction Supplies!Pencils!Mechanical Pencils',581,0,31635,NULL,NULL),(31973,'Med/Lab Printer Rolls','!Office Supplies!Paper & Printable Media!Machine Paper Rolls!Med/Lab Printer Rolls',651,0,31596,NULL,NULL),(31974,'Media Labels & Case Inserts','!Office Supplies!Labels, Indexes & Stamps!Labels & Stickers!Media Labels & Case Inserts',725,0,31568,NULL,NULL),(31975,'Media Mailers','!Office Supplies!Envelopes, Mailers & Shipping Supplies!Mailers!Media Mailers',793,0,31552,NULL,NULL),(31976,'Media Sleeves/Panels','!Office Supplies!Desk Accessories & Workspace Organizers!Media Organizers!Media Sleeves/Panels',836,0,31544,NULL,NULL),(31977,'Media Wallets & Binders','!Office Supplies!Desk Accessories & Workspace Organizers!Media Organizers!Media Wallets & Binders',837,0,31544,NULL,NULL),(31978,'Media/Card File Cabinets','!Furniture!Cabinets, Racks & Shelves!File & Storage Cabinets!Media/Card File Cabinets',1158,0,31441,NULL,NULL),(31979,'Medical Labels','!Office Supplies!Labels, Indexes & Stamps!Labels & Stickers!Medical Labels',726,0,31568,NULL,NULL),(31980,'Medical/Insurance Forms','!Office Supplies!Forms, Recordkeeping & Reference Materials!Forms & Recordkeeping Systems!Medical/Insurance Forms',756,0,31563,NULL,NULL),(31981,'Memo Sheet Holders','!Office Supplies!Desk Accessories & Workspace Organizers!Desk Supplies Holders & Dispensers!Memo Sheet Holders',847,0,31540,NULL,NULL),(31982,'Memo Sheets','!Office Supplies!Paper & Printable Media!Loose Paper!Memo Sheets',662,0,31595,NULL,NULL),(31983,'Message & Symbol Stamps','!Office Supplies!Labels, Indexes & Stamps!Stamps & Stamp Supplies!Message & Symbol Stamps',710,0,31570,NULL,NULL),(31984,'Message Forms','!Office Supplies!Forms, Recordkeeping & Reference Materials!Forms & Recordkeeping Systems!Message Forms',757,0,31563,NULL,NULL),(31985,'Message Labels','!Office Supplies!Labels, Indexes & Stamps!Labels & Stickers!Message Labels',727,0,31568,NULL,NULL),(31986,'Metal Cleaners & Lubricants','!Breakroom and Janitorial!Janitorial & Sanitation Supplies!Cleaning Products!Metal Cleaners & Lubricants',1003,0,31403,NULL,NULL),(31987,'Mice & Trackballs','!Technology!Computer Components, Peripherals & Accessories!Computer Components & Peripherals!Mice & Trackballs',495,0,31647,NULL,NULL),(31988,'Microphones & Megaphones','!Technology!Audio Visual Equipment!Presentation and Projection Equipment!Microphones & Megaphones',547,0,31641,NULL,NULL),(31989,'Microwave Ovens','!Breakroom and Janitorial!Food & Beverage Service!Appliances!Microwave Ovens',1034,0,31392,NULL,NULL),(31990,'Mid-Back Swivel/Tilt Chairs','!Furniture!Chairs & Sofas!Executive & Task Chairs!Mid-Back Swivel/Tilt Chairs',1126,0,31457,NULL,NULL),(31991,'Mileage & Expense Forms','!Office Supplies!Forms, Recordkeeping & Reference Materials!Forms & Recordkeeping Systems!Mileage & Expense Forms',758,0,31563,NULL,NULL),(31992,'Modeling Materials','!Office Supplies!Crafts & Recreation Room Products!Arts & Crafts Supplies!Modeling Materials',907,0,31520,NULL,NULL),(31993,'Modesty Panels','!Furniture!Desks & Workstations!Desk/Workstation Accessories!Modesty Panels',1113,0,31466,NULL,NULL),(31994,'Money & Rent Receipts','!Office Supplies!Forms, Recordkeeping & Reference Materials!Forms & Recordkeeping Systems!Money & Rent Receipts',759,0,31563,NULL,NULL),(31995,'Monitor Arms & Stands','!Office Supplies!Desk Accessories & Workspace Organizers!Platforms, Stands & Shelves!Monitor Arms & Stands',828,0,31546,NULL,NULL),(31996,'Monitor Screen Filters','!Technology!Computer Components, Peripherals & Accessories!Computer Accessories!Monitor Screen Filters',505,0,31646,NULL,NULL),(31997,'Monitors','!Technology!Computer Components, Peripherals & Accessories!Computer Components & Peripherals!Monitors',496,0,31647,NULL,NULL),(31998,'Monthly Planners','!Office Supplies!Calendars, Planners & Personal Organizers!Appointment Books/Planners!Monthly Planners',933,0,31497,NULL,NULL),(31999,'Mops, Heads & Handles','!Breakroom and Janitorial!Janitorial & Sanitation Supplies!Cleaning Tools & Supplies!Mops, Heads & Handles',984,0,31404,NULL,NULL),(32000,'Mounting Tape','!Office Supplies!Tape, Adhesives & Fasteners!Velcro & Mounting Products!Mounting Tape',597,0,31626,NULL,NULL),(32001,'Mouse Pads','!Office Supplies!Desk Accessories & Workspace Organizers!Mouse Pads & Wrist Rests!Mouse Pads',832,0,31545,NULL,NULL),(32002,'Multi-hole Punches','!Office Supplies!Staplers & Punches!Punches!Multi-hole Punches',620,0,31611,NULL,NULL),(32003,'Multi-line Telephones','!Technology!Audio Visual Equipment!Telephones!Multi-line Telephones',535,0,31643,NULL,NULL),(32004,'Multifile/Combo Storage Cabinets','!Furniture!Cabinets, Racks & Shelves!File & Storage Cabinets!Multifile/Combo Storage Cabinets',1159,0,31441,NULL,NULL),(32005,'Multifunction Office Machines','!Technology!Office Machines!Copiers, Fax Machines & Printers!Multifunction Office Machines',454,0,31656,NULL,NULL),(32006,'Napkins','!Breakroom and Janitorial!Food & Beverage Service!Food Service Supplies!Napkins',1018,0,31394,NULL,NULL),(32007,'Networking Cables & Adapters','!Technology!Cables, Adapters & Power Products!Cables and Adapters!Networking Cables & Adapters',522,0,31644,NULL,NULL),(32008,'Networking Equipment','!Technology!Computer Components, Peripherals & Accessories!Computer Components & Peripherals!Networking Equipment',497,0,31647,NULL,NULL),(32009,'Non-Printing Calculators','!Technology!Office Machines!Calculators & Counters!Non-Printing Calculators',460,0,31652,NULL,NULL),(32010,'Nonfolding Activity/Utility Tables','!Furniture!Tables!Activity & Utility Tables!Nonfolding Activity/Utility Tables',1080,0,31485,NULL,NULL),(32011,'Note & Greeting Cards','!Office Supplies!Paper & Printable Media!Cards & Card Stock!Note & Greeting Cards',669,0,31592,NULL,NULL),(32012,'Notebook Computer Carry Cases','!Office Supplies!Carrying Cases!Notebook Computer Bags & Cases!Notebook Computer Carry Cases',917,0,31508,NULL,NULL),(32013,'Notebook Computer Lights','!Technology!Computer Components, Peripherals & Accessories!Computer Accessories!Notebook Computer Lights',509,0,31646,NULL,NULL),(32014,'Notebook Computer Stands','!Office Supplies!Desk Accessories & Workspace Organizers!Platforms, Stands & Shelves!Notebook Computer Stands',829,0,31546,NULL,NULL),(32015,'Numbering Machines & Pricemarkers','!Office Supplies!Labels, Indexes & Stamps!Stamps & Stamp Supplies!Numbering Machines & Pricemarkers',711,0,31570,NULL,NULL),(32016,'Office Desks','!Furniture!Desks & Workstations!Desks!Office Desks',1106,0,31467,NULL,NULL),(32017,'Office Machine Carts & Floor Stands','!Furniture!Carts & Stands!Printer/Office Machine Carts & Stands!Office Machine Carts & Floor Stands',1139,0,31453,NULL,NULL),(32018,'Office Tape Dispensers','!Office Supplies!Tape, Adhesives & Fasteners!Tape Dispensers!Office Tape Dispensers',599,0,31625,NULL,NULL),(32019,'Optical Disks','!Technology!Storage Media!Data Tapes!Optical Disks',428,0,31668,NULL,NULL),(32020,'Oven & Grill Cleaners','!Breakroom and Janitorial!Janitorial & Sanitation Supplies!Cleaning Products!Oven & Grill Cleaners',1004,0,31403,NULL,NULL),(32021,'Packaging Tape','!Office Supplies!Tape, Adhesives & Fasteners!Tape!Packaging Tape',606,0,31624,NULL,NULL),(32022,'Packaging Tape Dispensers','!Office Supplies!Tape, Adhesives & Fasteners!Tape Dispensers!Packaging Tape Dispensers',600,0,31625,NULL,NULL),(32023,'Packing List Envelopes','!Office Supplies!Envelopes, Mailers & Shipping Supplies!Envelopes!Packing List Envelopes',804,0,31551,NULL,NULL),(32024,'Pad Holders','!Office Supplies!Carrying Cases!Pad Holders & Pad Portfolios!Pad Holders',915,0,31509,NULL,NULL),(32025,'Padfolio Ring Binders','!Office Supplies!Carrying Cases!Pad Holders & Pad Portfolios!Padfolio Ring Binders',916,0,31509,NULL,NULL),(32026,'Padlocks','!Breakroom and Janitorial!Facility Maintenance!Building Safety & Security Products!Padlocks',1065,0,31357,NULL,NULL),(32027,'Paint Sets','!Office Supplies!Crafts & Recreation Room Products!Arts & Crafts Supplies!Paint Sets',908,0,31520,NULL,NULL),(32028,'Paints & Finishes','!Office Supplies!Crafts & Recreation Room Products!Arts & Crafts Supplies!Paints & Finishes',909,0,31520,NULL,NULL),(32029,'Paper & Plastic Bags','!Office Supplies!Envelopes, Mailers & Shipping Supplies!Bags & Sacks!Paper & Plastic Bags',807,0,31550,NULL,NULL),(32030,'Paper Clip Holders','!Office Supplies!Desk Accessories & Workspace Organizers!Desk Supplies Holders & Dispensers!Paper Clip Holders',849,0,31540,NULL,NULL),(32031,'Paper Crafts','!Office Supplies!Crafts & Recreation Room Products!Arts & Crafts Supplies!Paper Crafts',910,0,31520,NULL,NULL),(32032,'Paper Folding Machines','!Technology!Office Machines!Paper Handling Machines!Paper Folding Machines',442,0,31661,NULL,NULL),(32033,'Paper Hand Towel Dispensers','!Breakroom and Janitorial!Facility Maintenance!Dispensers!Paper Hand Towel Dispensers',1057,0,31358,NULL,NULL),(32034,'Paper Joggers','!Technology!Office Machines!Paper Handling Machines!Paper Joggers',443,0,31661,NULL,NULL),(32035,'Paper Wall Calendars','!Office Supplies!Calendars, Planners & Personal Organizers!Wall Calendars & Planners!Paper Wall Calendars',919,0,31501,NULL,NULL),(32036,'Parachutes','!Office Supplies!Crafts & Recreation Room Products!Physical Education Equipment!Parachutes',890,0,31525,NULL,NULL),(32037,'Parallel Cables & Adapters','!Technology!Cables, Adapters & Power Products!Cables and Adapters!Parallel Cables & Adapters',523,0,31644,NULL,NULL),(32038,'Partition Accessories','!Office Supplies!Desk Accessories & Workspace Organizers!Wall & Panel Organizers!Partition Accessories',808,0,31549,NULL,NULL),(32039,'Pay & Expense Envelopes','!Office Supplies!Envelopes, Mailers & Shipping Supplies!Envelopes!Pay & Expense Envelopes',805,0,31551,NULL,NULL),(32040,'PC Headsets','!Technology!Computer Components, Peripherals & Accessories!Computer Accessories!PC Headsets',511,0,31646,NULL,NULL),(32041,'PC Speakers','!Technology!Computer Components, Peripherals & Accessories!Computer Accessories!PC Speakers',512,0,31646,NULL,NULL),(32042,'Pedestal Files','!Furniture!Cabinets, Racks & Shelves!File & Storage Cabinets!Pedestal Files',1161,0,31441,NULL,NULL),(32043,'Pen Ink & Refills','!Office Supplies!Writing & Correction Supplies!Pens & Refills!Pen Ink & Refills',576,0,31636,NULL,NULL),(32044,'Pencil Holders','!Office Supplies!Desk Accessories & Workspace Organizers!Desk Supplies Holders & Dispensers!Pencil Holders',850,0,31540,NULL,NULL),(32045,'Permanent Markers','!Office Supplies!Writing & Correction Supplies!Markers!Permanent Markers',588,0,31631,NULL,NULL),(32046,'Personal Organizer Components','!Office Supplies!Calendars, Planners & Personal Organizers!Personal Organizers!Personal Organizer Components',923,0,31500,NULL,NULL),(32047,'Personal Organizer Starter Sets','!Office Supplies!Calendars, Planners & Personal Organizers!Personal Organizers!Personal Organizer Starter Sets',925,0,31500,NULL,NULL),(32048,'Photo/Transparency Sleeves','!Office Supplies!Binders & Binding Systems!Sheet Protectors, Card & Photo Sleeves!Photo/Transparency Sleeves',939,0,31496,NULL,NULL),(32049,'Physical Education Kits','!Office Supplies!Crafts & Recreation Room Products!Physical Education Equipment!Physical Education Kits',891,0,31525,NULL,NULL),(32050,'Picture/Document Frames','!Furniture!Room Accessories!Frames & Plaques!Picture/Document Frames',1093,0,31478,NULL,NULL),(32051,'Planner Covers & Accessories','!Office Supplies!Calendars, Planners & Personal Organizers!Appointment Books/Planners!Planner Covers & Accessories',934,0,31497,NULL,NULL),(32052,'Plaques','!Furniture!Room Accessories!Frames & Plaques!Plaques',1094,0,31478,NULL,NULL),(32053,'Plates & Bowls','!Breakroom and Janitorial!Food & Beverage Service!Food Service Supplies!Plates & Bowls',1019,0,31394,NULL,NULL),(32054,'Playground & Sports Balls','!Office Supplies!Crafts & Recreation Room Products!Physical Education Equipment!Playground & Sports Balls',892,0,31525,NULL,NULL),(32055,'Plungers','!Breakroom and Janitorial!Janitorial & Sanitation Supplies!Cleaning Tools & Supplies!Plungers',985,0,31404,NULL,NULL),(32056,'Pocket Portfolios','!Office Supplies!Binders & Binding Systems!Report Covers & Pocket Portfolios!Pocket Portfolios',945,0,31495,NULL,NULL),(32057,'Pocket Portfolios with Fasteners','!Office Supplies!Binders & Binding Systems!Report Covers & Pocket Portfolios!Pocket Portfolios with Fasteners',946,0,31495,NULL,NULL),(32058,'Pocket Protectors','!Office Supplies!Writing & Correction Supplies!Writing Accessories!Pocket Protectors',571,0,31637,NULL,NULL),(32059,'Pointers & Remote Controls','!Technology!Audio Visual Equipment!Presentation and Projection Equipment!Pointers & Remote Controls',548,0,31641,NULL,NULL),(32060,'Porous Point Pens','!Office Supplies!Writing & Correction Supplies!Pens & Refills!Porous Point Pens',577,0,31636,NULL,NULL),(32061,'Post Binders','!Office Supplies!Binders & Binding Systems!Binders!Post Binders',954,0,31492,NULL,NULL),(32062,'Postage Machine Ink','!Technology!Imaging Supplies and Accessories!Imaging Supplies!Postage Machine Ink',468,0,31649,NULL,NULL),(32063,'Postage Meter Labels','!Office Supplies!Labels, Indexes & Stamps!Labels & Stickers!Postage Meter Labels',729,0,31568,NULL,NULL),(32064,'Postage Stamp Dispensers','!Office Supplies!Desk Accessories & Workspace Organizers!Desk Supplies Holders & Dispensers!Postage Stamp Dispensers',851,0,31540,NULL,NULL),(32065,'Postcards','!Office Supplies!Paper & Printable Media!Cards & Card Stock!Postcards',670,0,31592,NULL,NULL),(32066,'Poster Board & Tagboard','!Office Supplies!Paper & Printable Media!Art Paper & Sketching Pads!Poster Board & Tagboard',683,0,31589,NULL,NULL),(32067,'Poster Board/Drying Racks','!Furniture!Cabinets, Racks & Shelves!File & Storage Racks!Poster Board/Drying Racks',1151,0,31442,NULL,NULL),(32068,'Power Cords & Accessories','!Breakroom and Janitorial!Facility Maintenance!Batteries & Electrical Supplies!Power Cords & Accessories',1073,0,31356,NULL,NULL),(32069,'Power Tools','!Breakroom and Janitorial!Facility Maintenance!Maintenance Tools!Power Tools',1047,0,31360,NULL,NULL),(32070,'Power Washers','!Breakroom and Janitorial!Janitorial & Sanitation Supplies!Cleaning Tools & Supplies!Power Washers',986,0,31404,NULL,NULL),(32071,'Pricemarker Labels','!Office Supplies!Labels, Indexes & Stamps!Labels & Stickers!Pricemarker Labels',730,0,31568,NULL,NULL),(32072,'Print Servers','!Technology!Imaging Supplies and Accessories!Imaging Machine Accessories!Print Servers',477,0,31648,NULL,NULL),(32073,'Printer Cleaner Units','!Technology!Imaging Supplies and Accessories!Imaging Machine Accessories!Printer Cleaner Units',478,0,31648,NULL,NULL),(32074,'Printer Cleaners','!Technology!Imaging Supplies and Accessories!Imaging Supplies!Printer Cleaners',469,0,31649,NULL,NULL),(32075,'Printer Desktop Stands','!Office Supplies!Desk Accessories & Workspace Organizers!Platforms, Stands & Shelves!Printer Desktop Stands',830,0,31546,NULL,NULL),(32076,'Printer Dustcovers','!Technology!Imaging Supplies and Accessories!Imaging Machine Accessories!Printer Dustcovers',479,0,31648,NULL,NULL),(32077,'Printer Memory Cards','!Technology!Imaging Supplies and Accessories!Imaging Machine Accessories!Printer Memory Cards',480,0,31648,NULL,NULL),(32078,'Printer Paper Handlers','!Technology!Imaging Supplies and Accessories!Imaging Machine Accessories!Printer Paper Handlers',481,0,31648,NULL,NULL),(32079,'Printers','!Technology!Office Machines!Copiers, Fax Machines & Printers!Printers',455,0,31656,NULL,NULL),(32080,'Printheads & Printbands','!Technology!Imaging Supplies and Accessories!Imaging Machine Accessories!Printheads & Printbands',482,0,31648,NULL,NULL),(32081,'Printing Calculators','!Technology!Office Machines!Calculators & Counters!Printing Calculators',462,0,31652,NULL,NULL),(32082,'Printing Software','!Technology!Imaging Supplies and Accessories!Imaging Machine Accessories!Printing Software',483,0,31648,NULL,NULL),(32083,'Printout Paper','!Office Supplies!Paper & Printable Media!Printer Paper!Printout Paper',643,0,31598,NULL,NULL),(32084,'Printwheels & Type Elements','!Technology!Office Machines!Typewriters and Printwheels!Printwheels & Type Elements',431,0,31666,NULL,NULL),(32085,'Project Folders & Sleeves','!Office Supplies!File Folders, Portable & Storage Box Files!Folders!Project Folders & Sleeves',777,0,31558,NULL,NULL),(32086,'Projection Screens','!Technology!Audio Visual Equipment!Presentation and Projection Equipment!Projection Screens',549,0,31641,NULL,NULL),(32087,'Projector Supplies Caddies','!Technology!Audio Visual Equipment!Presentation and Projection Equipment!Projector Supplies Caddies',551,0,31641,NULL,NULL),(32088,'Projectors & Lamps','!Technology!Audio Visual Equipment!Presentation and Projection Equipment!Projectors & Lamps',552,0,31641,NULL,NULL),(32089,'Public Address Systems','!Technology!Audio Visual Equipment!Presentation and Projection Equipment!Public Address Systems',553,0,31641,NULL,NULL),(32090,'Punchless Binders','!Office Supplies!Binders & Binding Systems!Binders!Punchless Binders',955,0,31492,NULL,NULL),(32091,'Puzzles','!Office Supplies!Crafts & Recreation Room Products!Arts & Crafts Supplies!Puzzles',912,0,31520,NULL,NULL),(32092,'Quadrille Sheets','!Office Supplies!Paper & Printable Media!Loose Paper!Quadrille Sheets',663,0,31595,NULL,NULL),(32093,'Recycling Receptacle Lids','!Breakroom and Janitorial!Waste Receptacles & Liners!Recycling Receptacles & Lids!Recycling Receptacle Lids',970,0,31432,NULL,NULL),(32094,'Recycling Receptacles','!Breakroom and Janitorial!Waste Receptacles & Liners!Recycling Receptacles & Lids!Recycling Receptacles',971,0,31432,NULL,NULL),(32095,'Reference Books','!Office Supplies!Forms, Recordkeeping & Reference Materials!Reference Materials!Reference Books',747,0,31564,NULL,NULL),(32096,'Refrigerators','!Breakroom and Janitorial!Food & Beverage Service!Appliances!Refrigerators',1035,0,31392,NULL,NULL),(32097,'Report Covers','!Office Supplies!Binders & Binding Systems!Report Covers & Pocket Portfolios!Report Covers',948,0,31495,NULL,NULL),(32098,'Requisitions & Purchase Orders','!Office Supplies!Forms, Recordkeeping & Reference Materials!Forms & Recordkeeping Systems!Requisitions & Purchase Orders',761,0,31563,NULL,NULL),(32099,'Respirator Cartridges & Filters','!Breakroom and Janitorial!Safety & Sanitary Wear!Dust Masks & Respirators!Respirator Cartridges & Filters',973,0,31417,NULL,NULL),(32100,'Respirators','!Breakroom and Janitorial!Safety & Sanitary Wear!Dust Masks & Respirators!Respirators',974,0,31417,NULL,NULL),(32101,'Retail Signs','!Furniture!Room Accessories!Signs & Sign Holders!Retail Signs',1083,0,31483,NULL,NULL),(32102,'Reversible/Erasable Wall Calendars','!Office Supplies!Calendars, Planners & Personal Organizers!Wall Calendars & Planners!Reversible/Erasable Wall Calendars',921,0,31501,NULL,NULL),(32103,'Ribbons','!Technology!Imaging Supplies and Accessories!Imaging Supplies!Ribbons',471,0,31649,NULL,NULL),(32104,'Ring Binders','!Office Supplies!Binders & Binding Systems!Binders!Ring Binders',956,0,31492,NULL,NULL),(32105,'Ring Toss','!Office Supplies!Crafts & Recreation Room Products!Physical Education Equipment!Ring Toss',893,0,31525,NULL,NULL),(32106,'Roll/Tube File Racks','!Furniture!Cabinets, Racks & Shelves!File & Storage Racks!Roll/Tube File Racks',1152,0,31442,NULL,NULL),(32107,'Rollerball Pens','!Office Supplies!Writing & Correction Supplies!Pens & Refills!Rollerball Pens',578,0,31636,NULL,NULL),(32108,'Room Signs','!Furniture!Room Accessories!Signs & Sign Holders!Room Signs',1084,0,31483,NULL,NULL),(32109,'Rotary Card Files','!Office Supplies!Desk Accessories & Workspace Organizers!Card Files, Holders & Racks!Rotary Card Files',865,0,31536,NULL,NULL),(32110,'Rotary Trimmers','!Office Supplies!Cutting & Measuring Devices!Paper Trimmers & Blades!Rotary Trimmers',874,0,31532,NULL,NULL),(32111,'Rotary/Slotted Cards','!Office Supplies!Paper & Printable Media!Cards & Card Stock!Rotary/Slotted Cards',671,0,31592,NULL,NULL),(32112,'Rubber Cement','!Office Supplies!Tape, Adhesives & Fasteners!Adhesives & Glue!Rubber Cement',614,0,31615,NULL,NULL),(32113,'Rulers & Yardsticks','!Office Supplies!Cutting & Measuring Devices!Rulers & Tape Measures!Rulers & Yardsticks',872,0,31533,NULL,NULL),(32114,'Safety Barriers & Crowd Control','!Breakroom and Janitorial!Facility Maintenance!Building Safety & Security Products!Safety Barriers & Crowd Control',1066,0,31357,NULL,NULL),(32115,'Sales & Invoice Forms','!Office Supplies!Forms, Recordkeeping & Reference Materials!Forms & Recordkeeping Systems!Sales & Invoice Forms',762,0,31563,NULL,NULL),(32116,'Sanitary Napkin/Tampon Dispensers','!Breakroom and Janitorial!Facility Maintenance!Dispensers!Sanitary Napkin/Tampon Dispensers',1058,0,31358,NULL,NULL),(32117,'Scanner Parts','!Technology!Imaging Supplies and Accessories!Imaging Supplies!Scanner Parts',472,0,31649,NULL,NULL),(32118,'Scanners','!Technology!Office Machines!Copiers, Fax Machines & Printers!Scanners',456,0,31656,NULL,NULL),(32119,'School & Lab Notebooks & Pads','!Office Supplies!Paper & Printable Media!Notebooks & Writing Pads!School & Lab Notebooks & Pads',645,0,31597,NULL,NULL),(32120,'Scissors & Shears','!Office Supplies!Cutting & Measuring Devices!Scissors!Scissors & Shears',870,0,31534,NULL,NULL),(32121,'Scooters','!Office Supplies!Crafts & Recreation Room Products!Physical Education Equipment!Scooters',894,0,31525,NULL,NULL),(32122,'Scrapers & Knives','!Breakroom and Janitorial!Facility Maintenance!Maintenance Tools!Scrapers & Knives',1048,0,31360,NULL,NULL),(32123,'Seals & Stickers','!Office Supplies!Labels, Indexes & Stamps!Labels & Stickers!Seals & Stickers',731,0,31568,NULL,NULL),(32124,'Security Mirrors','!Breakroom and Janitorial!Facility Maintenance!Building Safety & Security Products!Security Mirrors',1067,0,31357,NULL,NULL),(32125,'Self-Adhere Pockets','!Office Supplies!Binders & Binding Systems!Sheet Protectors, Card & Photo Sleeves!Self-Adhere Pockets',940,0,31496,NULL,NULL),(32126,'Self-Adhesive Badges','!Office Supplies!Labels, Indexes & Stamps!Identification Badges!Self-Adhesive Badges',740,0,31566,NULL,NULL),(32127,'Self-Seal Laminating Supplies','!Office Supplies!Binders & Binding Systems!Sheet Protectors, Card & Photo Sleeves!Self-Seal Laminating Supplies',941,0,31496,NULL,NULL),(32128,'Self-Stick Bulletin Boards','!Office Supplies!Presentation/Display & Scheduling Boards!Bulletin Boards!Self-Stick Bulletin Boards',628,0,31601,NULL,NULL),(32129,'Self-Stick Note Pad Holders','!Office Supplies!Desk Accessories & Workspace Organizers!Desk Supplies Holders & Dispensers!Self-Stick Note Pad Holders',853,0,31540,NULL,NULL),(32130,'Serial Cables & Adapters','!Technology!Cables, Adapters & Power Products!Cables and Adapters!Serial Cables & Adapters',525,0,31644,NULL,NULL),(32131,'Sheet & Hole Reinforcements','!Office Supplies!Binders & Binding Systems!Binder Accessories!Sheet & Hole Reinforcements',964,0,31491,NULL,NULL),(32132,'Sheet Lifters','!Office Supplies!Binders & Binding Systems!Binder Accessories!Sheet Lifters',965,0,31491,NULL,NULL),(32133,'Sheet Protectors & Refill Pages','!Office Supplies!Binders & Binding Systems!Sheet Protectors, Card & Photo Sleeves!Sheet Protectors & Refill Pages',942,0,31496,NULL,NULL),(32134,'Shelf Files','!Furniture!Cabinets, Racks & Shelves!File & Storage Cabinets!Shelf Files',1162,0,31441,NULL,NULL),(32135,'Shipping & Receiving Forms','!Office Supplies!Forms, Recordkeeping & Reference Materials!Forms & Recordkeeping Systems!Shipping & Receiving Forms',763,0,31563,NULL,NULL),(32136,'Shipping Label Tape','!Office Supplies!Tape, Adhesives & Fasteners!Tape!Shipping Label Tape',607,0,31624,NULL,NULL),(32137,'Shop Desks','!Furniture!Desks & Workstations!Desks!Shop Desks',1107,0,31467,NULL,NULL),(32138,'Shop/Job Ticket Holders','!Office Supplies!Binders & Binding Systems!Sheet Protectors, Card & Photo Sleeves!Shop/Job Ticket Holders',943,0,31496,NULL,NULL),(32139,'Shop/Job Ticket Holders','!Office Supplies!Desk Accessories & Workspace Organizers!Wall & Panel Organizers!Shop/Job Ticket Holders',810,0,31549,NULL,NULL),(32140,'Shredders','!Technology!Office Machines!Shredders & Data Destroyers!Shredders',440,0,31663,NULL,NULL),(32141,'Sign Holders','!Furniture!Room Accessories!Signs & Sign Holders!Sign Holders',1086,0,31483,NULL,NULL),(32142,'Single-hole Punches','!Office Supplies!Staplers & Punches!Punches!Single-hole Punches',621,0,31611,NULL,NULL),(32143,'Single-line Telephones','!Technology!Audio Visual Equipment!Telephones!Single-line Telephones',536,0,31643,NULL,NULL),(32144,'Sink, Tub & Tile Cleaners','!Breakroom and Janitorial!Janitorial & Sanitation Supplies!Cleaning Products!Sink, Tub & Tile Cleaners',1005,0,31403,NULL,NULL),(32145,'Small Parts Storage','!Breakroom and Janitorial!Facility Maintenance!Maintenance Tools!Small Parts Storage',1049,0,31360,NULL,NULL),(32146,'Smoke & CO Detectors','!Breakroom and Janitorial!Facility Maintenance!Building Safety & Security Products!Smoke & CO Detectors',1068,0,31357,NULL,NULL),(32147,'Snack Foods','!Breakroom and Janitorial!Food & Beverage Service!Beverages & Snack Foods!Snack Foods',1029,0,31393,NULL,NULL),(32148,'Snips','!Office Supplies!Cutting & Measuring Devices!Scissors!Snips',871,0,31534,NULL,NULL),(32149,'Soap Dispensers','!Breakroom and Janitorial!Facility Maintenance!Dispensers!Soap Dispensers',1059,0,31358,NULL,NULL),(32150,'Solid Ink Sticks','!Technology!Imaging Supplies and Accessories!Imaging Supplies!Solid Ink Sticks',473,0,31649,NULL,NULL),(32151,'Sorter Accessories','!Office Supplies!Desk Accessories & Workspace Organizers!Sorters!Sorter Accessories',822,0,31547,NULL,NULL),(32152,'Sorter Racks/Trays','!Office Supplies!Desk Accessories & Workspace Organizers!Sorters!Sorter Racks/Trays',823,0,31547,NULL,NULL),(32153,'Spills Sorbents','!Breakroom and Janitorial!Facility Maintenance!Building Safety & Security Products!Spills Sorbents',1069,0,31357,NULL,NULL),(32154,'Sponges & Scouring Pads','!Breakroom and Janitorial!Janitorial & Sanitation Supplies!Cleaning Tools & Supplies!Sponges & Scouring Pads',987,0,31404,NULL,NULL),(32155,'Spray Adhesives','!Office Supplies!Tape, Adhesives & Fasteners!Adhesives & Glue!Spray Adhesives',615,0,31615,NULL,NULL),(32156,'Sprayers','!Breakroom and Janitorial!Janitorial & Sanitation Supplies!Cleaning Tools & Supplies!Sprayers',988,0,31404,NULL,NULL),(32157,'Squeegees','!Breakroom and Janitorial!Janitorial & Sanitation Supplies!Cleaning Tools & Supplies!Squeegees',989,0,31404,NULL,NULL),(32158,'Stack-On Unit Accessories','!Furniture!Desks & Workstations!Hutches & Stack-On Storage Units!Stack-On Unit Accessories',1103,0,31468,NULL,NULL),(32159,'Stack/Guillotine Trimmers','!Office Supplies!Cutting & Measuring Devices!Paper Trimmers & Blades!Stack/Guillotine Trimmers',875,0,31532,NULL,NULL),(32160,'Stain/Graffiti Removers','!Breakroom and Janitorial!Janitorial & Sanitation Supplies!Cleaning Products!Stain/Graffiti Removers',1006,0,31403,NULL,NULL),(32161,'Stamp Kits','!Office Supplies!Labels, Indexes & Stamps!Stamps & Stamp Supplies!Stamp Kits',713,0,31570,NULL,NULL),(32162,'Stamp Pads & Ink','!Office Supplies!Labels, Indexes & Stamps!Stamps & Stamp Supplies!Stamp Pads & Ink',714,0,31570,NULL,NULL),(32163,'Stamp Racks & Trays','!Office Supplies!Labels, Indexes & Stamps!Stamps & Stamp Supplies!Stamp Racks & Trays',715,0,31570,NULL,NULL),(32164,'Staple Cartridges for Copiers & Printers','!Technology!Imaging Supplies and Accessories!Imaging Machine Accessories!Staple Cartridges for Copiers & Printers',484,0,31648,NULL,NULL),(32165,'Staple Guns','!Office Supplies!Staplers & Punches!Staplers!Staple Guns',619,0,31613,NULL,NULL),(32166,'Station Components','!Furniture!Desks & Workstations!Mailroom Stations!Station Components',1101,0,31469,NULL,NULL),(32167,'Stationery/Design Paper','!Office Supplies!Paper & Printable Media!Loose Paper!Stationery/Design Paper',664,0,31595,NULL,NULL),(32168,'Stopwatches & Timers','!Office Supplies!Crafts & Recreation Room Products!Physical Education Equipment!Stopwatches & Timers',895,0,31525,NULL,NULL),(32169,'Storage Box Accessories','!Office Supplies!File Folders, Portable & Storage Box Files!Record Storage Boxes!Storage Box Accessories',769,0,31560,NULL,NULL),(32170,'Storage Crates','!Office Supplies!Desk Accessories & Workspace Organizers!Storage Containers!Storage Crates',817,0,31548,NULL,NULL),(32171,'Storage File Boxes','!Office Supplies!File Folders, Portable & Storage Box Files!Record Storage Boxes!Storage File Boxes',770,0,31560,NULL,NULL),(32172,'Storage System Accessories','!Office Supplies!Desk Accessories & Workspace Organizers!Storage Containers!Storage System Accessories',820,0,31548,NULL,NULL),(32173,'Straight-Through Cables & Adapters','!Technology!Cables, Adapters & Power Products!Cables and Adapters!Straight-Through Cables & Adapters',526,0,31644,NULL,NULL),(32174,'Straws & Stirrers','!Breakroom and Janitorial!Food & Beverage Service!Food Service Supplies!Straws & Stirrers',1021,0,31394,NULL,NULL),(32175,'Stretch Film & Dispensers','!Office Supplies!Envelopes, Mailers & Shipping Supplies!Packing Materials!Stretch Film & Dispensers',789,0,31553,NULL,NULL),(32176,'Student Chairs','!Furniture!Chairs & Sofas!Classroom Chairs!Student Chairs',1130,0,31456,NULL,NULL),(32177,'Suggestion Box Cards','!Office Supplies!Paper & Printable Media!Cards & Card Stock!Suggestion Box Cards',672,0,31592,NULL,NULL),(32178,'Super Glue','!Office Supplies!Tape, Adhesives & Fasteners!Adhesives & Glue!Super Glue',616,0,31615,NULL,NULL),(32179,'Surface Protectors','!Furniture!Desks & Workstations!Desk/Workstation Accessories!Surface Protectors',1118,0,31466,NULL,NULL),(32180,'Surge Protectors','!Technology!Cables, Adapters & Power Products!Office Equipment Power Products!Surge Protectors',516,0,31645,NULL,NULL),(32181,'Switchboxes','!Technology!Cables, Adapters & Power Products!Cables and Adapters!Switchboxes',527,0,31644,NULL,NULL),(32182,'Table Covers','!Breakroom and Janitorial!Food & Beverage Service!Food Service Supplies!Table Covers',1022,0,31394,NULL,NULL),(32183,'Table Hardware','!Furniture!Tables!Table Accessories!Table Hardware',1078,0,31490,NULL,NULL),(32184,'Tableware','!Breakroom and Janitorial!Food & Beverage Service!Food Service Supplies!Tableware',1023,0,31394,NULL,NULL),(32185,'Tack Bulletin Boards','!Office Supplies!Presentation/Display & Scheduling Boards!Bulletin Boards!Tack Bulletin Boards',629,0,31601,NULL,NULL),(32186,'Tags','!Office Supplies!Labels, Indexes & Stamps!Tags & Tickets!Tags',704,0,31571,NULL,NULL),(32187,'Tally Counters','!Technology!Office Machines!Calculators & Counters!Tally Counters',463,0,31652,NULL,NULL),(32188,'Tap Water Filters','!Breakroom and Janitorial!Facility Maintenance!Plumbing Supplies!Tap Water Filters',1039,0,31362,NULL,NULL),(32189,'Tape Flag Dispensers','!Office Supplies!Desk Accessories & Workspace Organizers!Desk Supplies Holders & Dispensers!Tape Flag Dispensers',854,0,31540,NULL,NULL),(32190,'Tape Measures','!Office Supplies!Cutting & Measuring Devices!Rulers & Tape Measures!Tape Measures',873,0,31533,NULL,NULL),(32191,'Task/Operator Chairs','!Furniture!Chairs & Sofas!Executive & Task Chairs!Task/Operator Chairs',1127,0,31457,NULL,NULL),(32192,'Tax Forms','!Office Supplies!Forms, Recordkeeping & Reference Materials!Forms & Recordkeeping Systems!Tax Forms',764,0,31563,NULL,NULL),(32193,'Technical Drawing Supplies','!Office Supplies!Writing & Correction Supplies!Writing Accessories!Technical Drawing Supplies',572,0,31637,NULL,NULL),(32194,'Telephone & Cellular Headsets','!Technology!Audio Visual Equipment!Telephone & Cellular Accessories!Telephone & Cellular Headsets',541,0,31642,NULL,NULL),(32195,'Telephone Cords & Connectors','!Technology!Audio Visual Equipment!Telephone & Cellular Accessories!Telephone Cords & Connectors',543,0,31642,NULL,NULL),(32196,'Telephone Handsets','!Technology!Audio Visual Equipment!Telephone & Cellular Accessories!Telephone Handsets',544,0,31642,NULL,NULL),(32197,'Telephone Shoulder Rests','!Technology!Audio Visual Equipment!Telephone & Cellular Accessories!Telephone Shoulder Rests',545,0,31642,NULL,NULL),(32198,'Telephone Stands','!Office Supplies!Desk Accessories & Workspace Organizers!Platforms, Stands & Shelves!Telephone Stands',831,0,31546,NULL,NULL),(32199,'Tent Cards','!Office Supplies!Paper & Printable Media!Cards & Card Stock!Tent Cards',673,0,31592,NULL,NULL),(32200,'Thermal Supplies','!Technology!Imaging Supplies and Accessories!Imaging Supplies!Thermal Supplies',474,0,31649,NULL,NULL),(32201,'Tickets & Claim Checks','!Office Supplies!Labels, Indexes & Stamps!Tags & Tickets!Tickets & Claim Checks',705,0,31571,NULL,NULL),(32202,'Time Clock Cards & Badges','!Technology!Office Machines!Time Clocks, Cards & Badges!Time Clock Cards & Badges',437,0,31664,NULL,NULL),(32203,'Toasters & Toaster Ovens','!Breakroom and Janitorial!Food & Beverage Service!Appliances!Toasters & Toaster Ovens',1036,0,31392,NULL,NULL),(32204,'Toilet Bowl Cleaners','!Breakroom and Janitorial!Janitorial & Sanitation Supplies!Cleaning Products!Toilet Bowl Cleaners',1007,0,31403,NULL,NULL),(32205,'Toilet Seat Cover Dispensers','!Breakroom and Janitorial!Facility Maintenance!Dispensers!Toilet Seat Cover Dispensers',1060,0,31358,NULL,NULL),(32206,'Tool Storage','!Breakroom and Janitorial!Facility Maintenance!Maintenance Tools!Tool Storage',1052,0,31360,NULL,NULL),(32207,'Top Tab & End Tab Folders','!Office Supplies!File Folders, Portable & Storage Box Files!Folders!Top Tab & End Tab Folders',778,0,31558,NULL,NULL),(32208,'Transfer Units and Belts','!Technology!Imaging Supplies and Accessories!Imaging Machine Accessories!Transfer Units and Belts',485,0,31648,NULL,NULL),(32209,'Transparency Film','!Office Supplies!Paper & Printable Media!Transparency Film & Frames!Transparency Film',638,0,31599,NULL,NULL),(32210,'Transparency Markers','!Office Supplies!Writing & Correction Supplies!Markers!Transparency Markers',589,0,31631,NULL,NULL),(32211,'Transparent Tape','!Office Supplies!Tape, Adhesives & Fasteners!Tape!Transparent Tape',608,0,31624,NULL,NULL),(32212,'Travan Tape','!Technology!Storage Media!Data Tapes!Travan Tape',429,0,31668,NULL,NULL),(32213,'Trimmer Blades','!Office Supplies!Cutting & Measuring Devices!Paper Trimmers & Blades!Trimmer Blades',876,0,31532,NULL,NULL),(32214,'Two-Way Radios & Accessories','!Technology!Audio Visual Equipment!Telephones!Two-Way Radios & Accessories',537,0,31643,NULL,NULL),(32215,'Typewriters','!Technology!Office Machines!Typewriters and Printwheels!Typewriters',433,0,31666,NULL,NULL),(32216,'Under Cabinet Lighting','!Furniture!Room Accessories!Lamps & Lighting!Under Cabinet Lighting',1092,0,31479,NULL,NULL),(32217,'Uninterruptible Power Sources (UPS)','!Technology!Cables, Adapters & Power Products!Office Equipment Power Products!Uninterruptible Power Sources (UPS)',517,0,31645,NULL,NULL),(32218,'Urinal Screens & Deodorizers','!Breakroom and Janitorial!Janitorial & Sanitation Supplies!Cleaning Products!Urinal Screens & Deodorizers',1008,0,31403,NULL,NULL),(32219,'USB Cables & Adapters','!Technology!Cables, Adapters & Power Products!Cables and Adapters!USB Cables & Adapters',528,0,31644,NULL,NULL),(32220,'Velcro/Hook & Loop Fasteners','!Office Supplies!Tape, Adhesives & Fasteners!Velcro & Mounting Products!Velcro/Hook & Loop Fasteners',598,0,31626,NULL,NULL),(32221,'Vertical File Cabinets','!Furniture!Cabinets, Racks & Shelves!File & Storage Cabinets!Vertical File Cabinets',1163,0,31441,NULL,NULL),(32222,'VHS Video Cassettes','!Technology!Audio Visual Equipment!Audio & Video Cassettes & Tapes!VHS Video Cassettes',570,0,31638,NULL,NULL),(32223,'Video Cables & Adapters','!Technology!Cables, Adapters & Power Products!Cables and Adapters!Video Cables & Adapters',529,0,31644,NULL,NULL),(32224,'Wall-Mount Dry Erase Boards','!Office Supplies!Presentation/Display & Scheduling Boards!Dry Erase Boards!Wall-Mount Dry Erase Boards',624,0,31606,NULL,NULL),(32225,'Wall/Panel Coat Racks','!Furniture!Cabinets, Racks & Shelves!Coat Racks & Hangers!Wall/Panel Coat Racks',1168,0,31438,NULL,NULL),(32226,'Wall/Panel Hooks & Hangers','!Office Supplies!Desk Accessories & Workspace Organizers!Wall & Panel Organizers!Wall/Panel Hooks & Hangers',811,0,31549,NULL,NULL),(32227,'Wall/Panel Organizer Systems','!Office Supplies!Desk Accessories & Workspace Organizers!Wall & Panel Organizers!Wall/Panel Organizer Systems',812,0,31549,NULL,NULL),(32228,'Wall/Panel Pockets & Connectors','!Office Supplies!Desk Accessories & Workspace Organizers!Wall & Panel Organizers!Wall/Panel Pockets & Connectors',813,0,31549,NULL,NULL),(32229,'Warranties','!Technology!Imaging Supplies and Accessories!Imaging Machine Accessories!Warranties',486,0,31648,NULL,NULL),(32230,'Waste Receptacle Lids','!Breakroom and Janitorial!Waste Receptacles & Liners!Waste Receptacles & Lids!Waste Receptacle Lids',969,0,31435,NULL,NULL),(32231,'Waste Toner Collectors','!Technology!Imaging Supplies and Accessories!Imaging Machine Accessories!Waste Toner Collectors',487,0,31648,NULL,NULL),(32232,'Water Pitcher Filters','!Breakroom and Janitorial!Food & Beverage Service!Food Service Supplies!Water Pitcher Filters',1025,0,31394,NULL,NULL),(32233,'Web Cameras','!Technology!Audio Visual Equipment!Cameras & Accessories!Web Cameras',567,0,31639,NULL,NULL),(32234,'Weekly Appointment Books','!Office Supplies!Calendars, Planners & Personal Organizers!Appointment Books/Planners!Weekly Appointment Books',935,0,31497,NULL,NULL),(32235,'Weekly/Monthly Planners','!Office Supplies!Calendars, Planners & Personal Organizers!Appointment Books/Planners!Weekly/Monthly Planners',936,0,31497,NULL,NULL),(32236,'Whistles','!Office Supplies!Crafts & Recreation Room Products!Physical Education Equipment!Whistles',896,0,31525,NULL,NULL),(32237,'Window Cleaning Kits','!Breakroom and Janitorial!Janitorial & Sanitation Supplies!Cleaning Tools & Supplies!Window Cleaning Kits',990,0,31404,NULL,NULL),(32238,'Wipes & Cleaning Cloths','!Breakroom and Janitorial!Janitorial & Sanitation Supplies!Cleaning Tools & Supplies!Wipes & Cleaning Cloths',991,0,31404,NULL,NULL),(32239,'Wireless Adapters & Cards','!Technology!Cables, Adapters & Power Products!Cables and Adapters!Wireless Adapters & Cards',530,0,31644,NULL,NULL),(32240,'Woodcase Pencils','!Office Supplies!Writing & Correction Supplies!Pencils!Woodcase Pencils',582,0,31635,NULL,NULL),(32241,'Wrist Rests','!Office Supplies!Desk Accessories & Workspace Organizers!Mouse Pads & Wrist Rests!Wrist Rests',833,0,31545,NULL,NULL),(32242,'Write-On Tapes','!Office Supplies!Labels, Indexes & Stamps!Labels & Stickers!Write-On Tapes',733,0,31568,NULL,NULL),(32243,'Writing Pads','!Office Supplies!Paper & Printable Media!Notebooks & Writing Pads!Writing Pads',646,0,31597,NULL,NULL),(32244,'Zip?? & Jaz?? & REV?? Disks','!Technology!Storage Media!Data Tapes!Zip?? & Jaz?? & REV?? Disks',430,0,31668,NULL,NULL),(32245,'Confetti-/Cross-Cut Shredders','!Technology!Office Machines!Shredders & Data Destroyers!Shredders!Confetti-/Cross-Cut Shredders',1171,0,32140,NULL,NULL),(32246,'Shredder Supplies','!Technology!Office Machines!Shredders & Data Destroyers!Shredders!Shredder Supplies',1172,0,32140,NULL,NULL),(32247,'Strip-Cut Paper Shredders','!Technology!Office Machines!Shredders & Data Destroyers!Shredders!Strip-Cut Paper Shredders',1173,0,32140,NULL,NULL),(32248,'Dot Matrix Printers','!Technology!Office Machines!Copiers, Fax Machines & Printers!Printers!Dot Matrix Printers',1174,0,32079,NULL,NULL),(32249,'Inkjet  Printers','!Technology!Office Machines!Copiers, Fax Machines & Printers!Printers!Inkjet  Printers',1175,0,32079,NULL,NULL),(32250,'Inkjet Photo Printers','!Technology!Office Machines!Copiers, Fax Machines & Printers!Printers!Inkjet Photo Printers',1176,0,32079,NULL,NULL),(32251,'Laser Printers','!Technology!Office Machines!Copiers, Fax Machines & Printers!Printers!Laser Printers',1177,0,32079,NULL,NULL),(32252,'Solid Ink Printers','!Technology!Office Machines!Copiers, Fax Machines & Printers!Printers!Solid Ink Printers',1178,0,32079,NULL,NULL),(32253,'Thermal Fax/Printer Rolls','!Technology!Imaging Supplies and Accessories!Imaging Supplies!Thermal Supplies!Thermal Fax/Printer Rolls',1179,0,32200,NULL,NULL),(32254,'Thermal Ribbons & Cartridges','!Technology!Imaging Supplies and Accessories!Imaging Supplies!Thermal Supplies!Thermal Ribbons & Cartridges',1180,0,32200,NULL,NULL),(32255,'Calculator Ribbons and Ink Rollers','!Technology!Imaging Supplies and Accessories!Imaging Supplies!Ribbons!Calculator Ribbons and Ink Rollers',1181,0,32103,NULL,NULL),(32256,'Cash Register, POS & ATM Ribbons','!Technology!Imaging Supplies and Accessories!Imaging Supplies!Ribbons!Cash Register, POS & ATM Ribbons',1182,0,32103,NULL,NULL),(32257,'Printer Ribbons','!Technology!Imaging Supplies and Accessories!Imaging Supplies!Ribbons!Printer Ribbons',1183,0,32103,NULL,NULL),(32258,'Time Clock Ribbons','!Technology!Imaging Supplies and Accessories!Imaging Supplies!Ribbons!Time Clock Ribbons',1185,0,32103,NULL,NULL),(32259,'Typewriter Ribbons','!Technology!Imaging Supplies and Accessories!Imaging Supplies!Ribbons!Typewriter Ribbons',1186,0,32103,NULL,NULL),(32260,'Developers','!Technology!Imaging Supplies and Accessories!Imaging Supplies!Copier, Fax & Laser Printer Supplies!Developers',1187,0,31791,NULL,NULL),(32261,'Drums','!Technology!Imaging Supplies and Accessories!Imaging Supplies!Copier, Fax & Laser Printer Supplies!Drums',1188,0,31791,NULL,NULL),(32262,'Fusers','!Technology!Imaging Supplies and Accessories!Imaging Supplies!Copier, Fax & Laser Printer Supplies!Fusers',1189,0,31791,NULL,NULL),(32263,'Photoconductors','!Technology!Imaging Supplies and Accessories!Imaging Supplies!Copier, Fax & Laser Printer Supplies!Photoconductors',1190,0,31791,NULL,NULL),(32264,'Toner','!Technology!Imaging Supplies and Accessories!Imaging Supplies!Copier, Fax & Laser Printer Supplies!Toner',1191,0,31791,NULL,NULL),(32265,'Hubs','!Technology!Computer Components, Peripherals & Accessories!Computer Components & Peripherals!Networking Equipment!Hubs',1192,0,32008,NULL,NULL),(32266,'Modems','!Technology!Computer Components, Peripherals & Accessories!Computer Components & Peripherals!Networking Equipment!Modems',1193,0,32008,NULL,NULL),(32267,'Routers','!Technology!Computer Components, Peripherals & Accessories!Computer Components & Peripherals!Networking Equipment!Routers',1194,0,32008,NULL,NULL),(32268,'Switches','!Technology!Computer Components, Peripherals & Accessories!Computer Components & Peripherals!Networking Equipment!Switches',1195,0,32008,NULL,NULL),(32269,'PDA Power Products,Cables & Connectors','!Technology!Computer Components, Peripherals & Accessories!Computer Components & Peripherals!Electronic Personal Organizers!PDA Power Products,Cables & Connectors',1197,0,31854,NULL,NULL),(32270,'PDA Projection Lamp Modules','!Technology!Computer Components, Peripherals & Accessories!Computer Components & Peripherals!Electronic Personal Organizers!PDA Projection Lamp Modules',1198,0,31854,NULL,NULL),(32271,'PDA Sleeves & Overlays','!Technology!Computer Components, Peripherals & Accessories!Computer Components & Peripherals!Electronic Personal Organizers!PDA Sleeves & Overlays',1199,0,31854,NULL,NULL),(32272,'PDA Styli','!Technology!Computer Components, Peripherals & Accessories!Computer Components & Peripherals!Electronic Personal Organizers!PDA Styli',1200,0,31854,NULL,NULL),(32273,'PDAs/Electronic Organizers','!Technology!Computer Components, Peripherals & Accessories!Computer Components & Peripherals!Electronic Personal Organizers!PDAs/Electronic Organizers',1201,0,31854,NULL,NULL),(32274,'eXtreme Digital (XD) Cards','!Technology!Computer Components, Peripherals & Accessories!Computer Components & Peripherals!Disk Drives & Memory!eXtreme Digital (XD) Cards',1203,0,31834,NULL,NULL),(32275,'Flash Memory Cards','!Technology!Computer Components, Peripherals & Accessories!Computer Components & Peripherals!Disk Drives & Memory!Flash Memory Cards',1204,0,31834,NULL,NULL),(32276,'Hard Drives','!Technology!Computer Components, Peripherals & Accessories!Computer Components & Peripherals!Disk Drives & Memory!Hard Drives',1206,0,31834,NULL,NULL),(32277,'Memory Sticks & Memory Cards','!Technology!Computer Components, Peripherals & Accessories!Computer Components & Peripherals!Disk Drives & Memory!Memory Sticks & Memory Cards',1207,0,31834,NULL,NULL),(32278,'Secure Digital (SD) Cards','!Technology!Computer Components, Peripherals & Accessories!Computer Components & Peripherals!Disk Drives & Memory!Secure Digital (SD) Cards',1209,0,31834,NULL,NULL),(32279,'USB Drives','!Technology!Computer Components, Peripherals & Accessories!Computer Components & Peripherals!Disk Drives & Memory!USB Drives',1211,0,31834,NULL,NULL),(32280,'Two-Way Radio Accessories','!Technology!Audio Visual Equipment!Telephones!Two-Way Radios & Accessories!Two-Way Radio Accessories',1213,0,32214,NULL,NULL),(32281,'Two-Way Radios','!Technology!Audio Visual Equipment!Telephones!Two-Way Radios & Accessories!Two-Way Radios',1214,0,32214,NULL,NULL),(32282,'Cellular Phone Headsets','!Technology!Audio Visual Equipment!Telephone & Cellular Accessories!Telephone & Cellular Headsets!Cellular Phone Headsets',1215,0,32194,NULL,NULL),(32283,'Headset Accessories','!Technology!Audio Visual Equipment!Telephone & Cellular Accessories!Telephone & Cellular Headsets!Headset Accessories',1216,0,32194,NULL,NULL),(32284,'Headset Amplifiers','!Technology!Audio Visual Equipment!Telephone & Cellular Accessories!Telephone & Cellular Headsets!Headset Amplifiers',1217,0,32194,NULL,NULL),(32285,'Telephone Headsets','!Technology!Audio Visual Equipment!Telephone & Cellular Accessories!Telephone & Cellular Headsets!Telephone Headsets',1218,0,32194,NULL,NULL),(32286,'Digital/Multimedia Projectors','!Technology!Audio Visual Equipment!Presentation and Projection Equipment!Projectors & Lamps!Digital/Multimedia Projectors',1219,0,32088,NULL,NULL),(32287,'Overhead Projectors','!Technology!Audio Visual Equipment!Presentation and Projection Equipment!Projectors & Lamps!Overhead Projectors',1221,0,32088,NULL,NULL),(32288,'Projection Lamps','!Technology!Audio Visual Equipment!Presentation and Projection Equipment!Projectors & Lamps!Projection Lamps',1223,0,32088,NULL,NULL),(32289,'Digital Camera Accessories','!Technology!Audio Visual Equipment!Cameras & Accessories!Digital Cameras & Accessories!Digital Camera Accessories',1228,0,31827,NULL,NULL),(32290,'Digital Cameras','!Technology!Audio Visual Equipment!Cameras & Accessories!Digital Cameras & Accessories!Digital Cameras',1229,0,31827,NULL,NULL),(32291,'Compasses','!Office Supplies!Writing & Correction Supplies!Writing Accessories!Technical Drawing Supplies!Compasses',1232,0,32193,NULL,NULL),(32292,'Protractors','!Office Supplies!Writing & Correction Supplies!Writing Accessories!Technical Drawing Supplies!Protractors',1233,0,32193,NULL,NULL),(32293,'Templates','!Office Supplies!Writing & Correction Supplies!Writing Accessories!Technical Drawing Supplies!Templates',1235,0,32193,NULL,NULL),(32294,'Triangular Scales','!Office Supplies!Writing & Correction Supplies!Writing Accessories!Technical Drawing Supplies!Triangular Scales',1236,0,32193,NULL,NULL),(32295,'Gel Ink Rollerball Pens','!Office Supplies!Writing & Correction Supplies!Pens & Refills!Rollerball Pens!Gel Ink Rollerball Pens',1237,0,32107,NULL,NULL),(32296,'Liquid Ink Rollerball Pens','!Office Supplies!Writing & Correction Supplies!Pens & Refills!Rollerball Pens!Liquid Ink Rollerball Pens',1238,0,32107,NULL,NULL),(32297,'Bottled Ink','!Office Supplies!Writing & Correction Supplies!Pens & Refills!Pen Ink & Refills!Bottled Ink',1239,0,32043,NULL,NULL),(32298,'Pen Refills','!Office Supplies!Writing & Correction Supplies!Pens & Refills!Pen Ink & Refills!Pen Refills',1240,0,32043,NULL,NULL),(32299,'Retractable Ballpoint Pens','!Office Supplies!Writing & Correction Supplies!Pens & Refills!Ballpoint Pens!Retractable Ballpoint Pens',1241,0,31700,NULL,NULL),(32300,'Stick Ballpoint Pens','!Office Supplies!Writing & Correction Supplies!Pens & Refills!Ballpoint Pens!Stick Ballpoint Pens',1242,0,31700,NULL,NULL),(32301,'Colored Pencils','!Office Supplies!Writing & Correction Supplies!Pencils!Woodcase Pencils!Colored Pencils',1243,0,32240,NULL,NULL),(32302,'Lead Pencils','!Office Supplies!Writing & Correction Supplies!Pencils!Woodcase Pencils!Lead Pencils',1244,0,32240,NULL,NULL),(32303,'Board Erasers','!Office Supplies!Writing & Correction Supplies!Erasers & Correction Products!Erasers!Board Erasers',1245,0,31863,NULL,NULL),(32304,'Eraser Refills For Mechanical Pencils','!Office Supplies!Writing & Correction Supplies!Erasers & Correction Products!Erasers!Eraser Refills For Mechanical Pencils',1247,0,31863,NULL,NULL),(32305,'Pen & Pencil Erasers','!Office Supplies!Writing & Correction Supplies!Erasers & Correction Products!Erasers!Pen & Pencil Erasers',1248,0,31863,NULL,NULL),(32306,'Desktop Staplers','!Office Supplies!Staplers & Punches!Staplers!Manual Staplers!Desktop Staplers',1249,0,31968,NULL,NULL),(32307,'Heavy-Duty Staplers','!Office Supplies!Staplers & Punches!Staplers!Manual Staplers!Heavy-Duty Staplers',1250,0,31968,NULL,NULL),(32308,'Plier & Clipper Staplers','!Office Supplies!Staplers & Punches!Staplers!Manual Staplers!Plier & Clipper Staplers',1251,0,31968,NULL,NULL),(32309,'Electric/Battery-Operated Punches','!Office Supplies!Staplers & Punches!Punches!Multi-hole Punches!Electric/Battery-Operated Punches',1252,0,32002,NULL,NULL),(32310,'Manual Punches','!Office Supplies!Staplers & Punches!Punches!Multi-hole Punches!Manual Punches',1253,0,32002,NULL,NULL),(32311,'Inkjet Photo/Picture Paper','!Office Supplies!Paper & Printable Media!Printer Paper!Inkjet Printer Paper & Media!Inkjet Photo/Picture Paper',1254,0,31920,NULL,NULL),(32312,'Inkjet Printer Paper','!Office Supplies!Paper & Printable Media!Printer Paper!Inkjet Printer Paper & Media!Inkjet Printer Paper',1255,0,31920,NULL,NULL),(32313,'Wide Format Printer Rolls','!Office Supplies!Paper & Printable Media!Printer Paper!Inkjet Printer Paper & Media!Wide Format Printer Rolls',1256,0,31920,NULL,NULL),(32314,'Wide Format Printer Sheets','!Office Supplies!Paper & Printable Media!Printer Paper!Inkjet Printer Paper & Media!Wide Format Printer Sheets',1257,0,31920,NULL,NULL),(32315,'Letter & Legal Ruled Pads','!Office Supplies!Paper & Printable Media!Notebooks & Writing Pads!Writing Pads!Letter & Legal Ruled Pads',1258,0,32243,NULL,NULL),(32316,'Memo & Scratch Pads','!Office Supplies!Paper & Printable Media!Notebooks & Writing Pads!Writing Pads!Memo & Scratch Pads',1259,0,32243,NULL,NULL),(32317,'Self-Stick Note Pads','!Office Supplies!Paper & Printable Media!Notebooks & Writing Pads!Writing Pads!Self-Stick Note Pads',1260,0,32243,NULL,NULL),(32318,'Chart Tablets','!Office Supplies!Paper & Printable Media!Notebooks & Writing Pads!School & Lab Notebooks & Pads!Chart Tablets',1261,0,32119,NULL,NULL),(32319,'Composition Notebooks','!Office Supplies!Paper & Printable Media!Notebooks & Writing Pads!School & Lab Notebooks & Pads!Composition Notebooks',1262,0,32119,NULL,NULL),(32320,'Exam & Spelling Notebooks','!Office Supplies!Paper & Printable Media!Notebooks & Writing Pads!School & Lab Notebooks & Pads!Exam & Spelling Notebooks',1263,0,32119,NULL,NULL),(32321,'Lab, Quad & Data Pads & Books','!Office Supplies!Paper & Printable Media!Notebooks & Writing Pads!School & Lab Notebooks & Pads!Lab, Quad & Data Pads & Books',1264,0,32119,NULL,NULL),(32322,'Subject Notebooks','!Office Supplies!Paper & Printable Media!Notebooks & Writing Pads!School & Lab Notebooks & Pads!Subject Notebooks',1266,0,32119,NULL,NULL),(32323,'Teacher\'s Planning Notebooks','!Office Supplies!Paper & Printable Media!Notebooks & Writing Pads!School & Lab Notebooks & Pads!Teacher\'s Planning Notebooks',1267,0,32119,NULL,NULL),(32324,'Hardcover Executive Notebooks','!Office Supplies!Paper & Printable Media!Notebooks & Writing Pads!Business Notebooks & Planners!Hardcover Executive Notebooks',1268,0,31731,NULL,NULL),(32325,'Memo Books','!Office Supplies!Paper & Printable Media!Notebooks & Writing Pads!Business Notebooks & Planners!Memo Books',1269,0,31731,NULL,NULL),(32326,'Steno Notebooks','!Office Supplies!Paper & Printable Media!Notebooks & Writing Pads!Business Notebooks & Planners!Steno Notebooks',1270,0,31731,NULL,NULL),(32327,'Wirebound Notebooks','!Office Supplies!Paper & Printable Media!Notebooks & Writing Pads!Business Notebooks & Planners!Wirebound Notebooks',1272,0,31731,NULL,NULL),(32328,'Drawing & Sketch Paper','!Office Supplies!Paper & Printable Media!Art Paper & Sketching Pads!Drawing Paper & Sketch Pads!Drawing & Sketch Paper',1273,0,31843,NULL,NULL),(32329,'Tracing Paper','!Office Supplies!Paper & Printable Media!Art Paper & Sketching Pads!Drawing Paper & Sketch Pads!Tracing Paper',1275,0,31843,NULL,NULL),(32330,'Computer Vac Supplies','!Office Supplies!Office Equipment Cleaners!Electronics Equipment Cleaners!Computer Vacs & Supplies!Computer Vac Supplies',1276,0,31784,NULL,NULL),(32331,'Computer Vacuum/Blowers','!Office Supplies!Office Equipment Cleaners!Electronics Equipment Cleaners!Computer Vacs & Supplies!Computer Vacuum/Blowers',1277,0,31784,NULL,NULL),(32332,'Binder Label Holders','!Office Supplies!Labels, Indexes & Stamps!Labels & Stickers!Label Holders & Label Protectors!Binder Label Holders',1279,0,31934,NULL,NULL),(32333,'Label Protectors','!Office Supplies!Labels, Indexes & Stamps!Labels & Stickers!Label Holders & Label Protectors!Label Protectors',1280,0,31934,NULL,NULL),(32334,'Shelf/Magnetic Label Holders','!Office Supplies!Labels, Indexes & Stamps!Labels & Stickers!Label Holders & Label Protectors!Shelf/Magnetic Label Holders',1281,0,31934,NULL,NULL),(32335,'Alpha, Numeric, Month, Year Filing Labels','!Office Supplies!Labels, Indexes & Stamps!Labels & Stickers!Filing Labels!Alpha, Numeric, Month, Year Filing Labels',1282,0,31875,NULL,NULL),(32336,'File Folder Labels','!Office Supplies!Labels, Indexes & Stamps!Labels & Stickers!Filing Labels!File Folder Labels',1284,0,31875,NULL,NULL),(32337,'Filing System Refill Labels','!Office Supplies!Labels, Indexes & Stamps!Labels & Stickers!Filing Labels!Filing System Refill Labels',1285,0,31875,NULL,NULL),(32338,'Binder Punched Indexes','!Office Supplies!Labels, Indexes & Stamps!Index Dividers, Tabs & File Guides!Index Dividers!Binder Punched Indexes',1286,0,31916,NULL,NULL),(32339,'Unpunched Indexes','!Office Supplies!Labels, Indexes & Stamps!Index Dividers, Tabs & File Guides!Index Dividers!Unpunched Indexes',1289,0,31916,NULL,NULL),(32340,'Human Resources Forms','!Office Supplies!Forms, Recordkeeping & Reference Materials!Forms & Recordkeeping Systems!Human Resources Forms & Software!Human Resources Forms',1290,0,31909,NULL,NULL),(32341,'Human Resources Software','!Office Supplies!Forms, Recordkeeping & Reference Materials!Forms & Recordkeeping Systems!Human Resources Forms & Software!Human Resources Software',1291,0,31909,NULL,NULL),(32342,'Awards & Certificates','!Office Supplies!Forms, Recordkeeping & Reference Materials!Forms & Recordkeeping Systems!Award & Certificate Forms!Awards & Certificates',1295,0,31696,NULL,NULL),(32343,'Certificate Covers','!Office Supplies!Forms, Recordkeeping & Reference Materials!Forms & Recordkeeping Systems!Award & Certificate Forms!Certificate Covers',1296,0,31696,NULL,NULL),(32344,'Check Registers','!Office Supplies!Forms, Recordkeeping & Reference Materials!Forms & Recordkeeping Systems!Account/Recordkeeping Books!Check Registers',1298,0,31676,NULL,NULL),(32345,'Record/Account Books & Journals','!Office Supplies!Forms, Recordkeeping & Reference Materials!Forms & Recordkeeping Systems!Account/Recordkeeping Books!Record/Account Books & Journals',1300,0,31676,NULL,NULL),(32346,'Visitor Registers','!Office Supplies!Forms, Recordkeeping & Reference Materials!Forms & Recordkeeping Systems!Account/Recordkeeping Books!Visitor Registers',1301,0,31676,NULL,NULL),(32347,'Classification/Fastener Folders','!Office Supplies!File Folders, Portable & Storage Box Files!Folders!Top Tab & End Tab Folders!Classification/Fastener Folders',1302,0,32207,NULL,NULL),(32348,'File Folders','!Office Supplies!File Folders, Portable & Storage Box Files!Folders!Top Tab & End Tab Folders!File Folders',1303,0,32207,NULL,NULL),(32349,'Hanging Classification Folders','!Office Supplies!File Folders, Portable & Storage Box Files!Folders!Hanging Folders & Interior Folders!Hanging Classification Folders',1304,0,31906,NULL,NULL),(32350,'Hanging Folders/Jackets/Pockets','!Office Supplies!File Folders, Portable & Storage Box Files!Folders!Hanging Folders & Interior Folders!Hanging Folders/Jackets/Pockets',1305,0,31906,NULL,NULL),(32351,'Interior File Folders','!Office Supplies!File Folders, Portable & Storage Box Files!Folders!Hanging Folders & Interior Folders!Interior File Folders',1306,0,31906,NULL,NULL),(32352,'Conversion Jackets & Pockets','!Office Supplies!File Folders, Portable & Storage Box Files!Folders!File Jackets & File Pockets!Conversion Jackets & Pockets',1307,0,31873,NULL,NULL),(32353,'End Tab Jackets & Pockets','!Office Supplies!File Folders, Portable & Storage Box Files!Folders!File Jackets & File Pockets!End Tab Jackets & Pockets',1308,0,31873,NULL,NULL),(32354,'Specialty File Jackets & Pockets','!Office Supplies!File Folders, Portable & Storage Box Files!Folders!File Jackets & File Pockets!Specialty File Jackets & Pockets',1309,0,31873,NULL,NULL),(32355,'Top Tab Jackets & Pockets','!Office Supplies!File Folders, Portable & Storage Box Files!Folders!File Jackets & File Pockets!Top Tab Jackets & Pockets',1310,0,31873,NULL,NULL),(32356,'File Backs/Folder Dividers','!Office Supplies!File Folders, Portable & Storage Box Files!File & Folder Accessories!File Folder Inserts!File Backs/Folder Dividers',1312,0,31870,NULL,NULL),(32357,'File Folder Converters','!Office Supplies!File Folders, Portable & Storage Box Files!File & Folder Accessories!File Folder Inserts!File Folder Converters',1313,0,31870,NULL,NULL),(32358,'Stretch Film','!Office Supplies!Envelopes, Mailers & Shipping Supplies!Packing Materials!Stretch Film & Dispensers!Stretch Film',1314,0,32175,NULL,NULL),(32359,'Stretch Film Dispensers','!Office Supplies!Envelopes, Mailers & Shipping Supplies!Packing Materials!Stretch Film & Dispensers!Stretch Film Dispensers',1315,0,32175,NULL,NULL),(32360,'Wall/Panel File Pockets','!Office Supplies!Desk Accessories & Workspace Organizers!Wall & Panel Organizers!Wall/Panel Pockets & Connectors!Wall/Panel File Pockets',1316,0,32228,NULL,NULL),(32361,'Wall/Panel Pocket Connectors','!Office Supplies!Desk Accessories & Workspace Organizers!Wall & Panel Organizers!Wall/Panel Pockets & Connectors!Wall/Panel Pocket Connectors',1317,0,32228,NULL,NULL),(32362,'Monitor Arms','!Office Supplies!Desk Accessories & Workspace Organizers!Platforms, Stands & Shelves!Monitor Arms & Stands!Monitor Arms',1318,0,31995,NULL,NULL),(32363,'Monitor Risers & Stands','!Office Supplies!Desk Accessories & Workspace Organizers!Platforms, Stands & Shelves!Monitor Arms & Stands!Monitor Risers & Stands',1319,0,31995,NULL,NULL),(32364,'Daytimer Refills & Accessories','!Office Supplies!Calendars, Planners & Personal Organizers!Personal Organizers!Personal Organizer Components!Daytimer Refills & Accessories',1322,0,32046,NULL,NULL),(32365,'Franklin Covey Refills & Accessories','!Office Supplies!Calendars, Planners & Personal Organizers!Personal Organizers!Personal Organizer Components!Franklin Covey Refills & Accessories',1323,0,32046,NULL,NULL),(32366,'Personal Organizer Binders','!Office Supplies!Calendars, Planners & Personal Organizers!Personal Organizers!Personal Organizer Components!Personal Organizer Binders',1324,0,32046,NULL,NULL),(32367,'Portfolio/Case Ring Binders','!Office Supplies!Binders & Binding Systems!Binders!Ring Binders!Portfolio/Case Ring Binders',1327,0,32104,NULL,NULL),(32368,'Reference Ring Binders','!Office Supplies!Binders & Binding Systems!Binders!Ring Binders!Reference Ring Binders',1328,0,32104,NULL,NULL),(32369,'Specialty Ring Binders','!Office Supplies!Binders & Binding Systems!Binders!Ring Binders!Specialty Ring Binders',1329,0,32104,NULL,NULL),(32370,'View Binders','!Office Supplies!Binders & Binding Systems!Binders!Ring Binders!View Binders',1330,0,32104,NULL,NULL),(32371,'Binder Pockets & Sheet Dividers','!Office Supplies!Binders & Binding Systems!Binder Accessories!Binder Dividers!Binder Pockets & Sheet Dividers',1331,0,31707,NULL,NULL),(32372,'Dust Mops','!Breakroom and Janitorial!Janitorial & Sanitation Supplies!Cleaning Tools & Supplies!Mops, Heads & Handles!Dust Mops',1333,0,31999,NULL,NULL),(32373,'Wet Mops','!Breakroom and Janitorial!Janitorial & Sanitation Supplies!Cleaning Tools & Supplies!Mops, Heads & Handles!Wet Mops',1334,0,31999,NULL,NULL),(32374,'Floor Cleaning Machines','!Breakroom and Janitorial!Janitorial & Sanitation Supplies!Cleaning Tools & Supplies!Floor Machines & Vacuums!Floor Cleaning Machines',1335,0,31886,NULL,NULL),(32375,'Floor Cleaning Pads','!Breakroom and Janitorial!Janitorial & Sanitation Supplies!Cleaning Tools & Supplies!Floor Machines & Vacuums!Floor Cleaning Pads',1336,0,31886,NULL,NULL),(32376,'Sweepers','!Breakroom and Janitorial!Janitorial & Sanitation Supplies!Cleaning Tools & Supplies!Floor Machines & Vacuums!Sweepers',1337,0,31886,NULL,NULL),(32377,'Vacuum Cleaner Supplies','!Breakroom and Janitorial!Janitorial & Sanitation Supplies!Cleaning Tools & Supplies!Floor Machines & Vacuums!Vacuum Cleaner Supplies',1338,0,31886,NULL,NULL),(32378,'Vacuum Cleaners','!Breakroom and Janitorial!Janitorial & Sanitation Supplies!Cleaning Tools & Supplies!Floor Machines & Vacuums!Vacuum Cleaners',1339,0,31886,NULL,NULL),(32379,'Cold Drink Cups','!Breakroom and Janitorial!Food & Beverage Service!Food Service Supplies!Cups & Glasses!Cold Drink Cups',1340,0,31803,NULL,NULL),(32380,'Cup Lids','!Breakroom and Janitorial!Food & Beverage Service!Food Service Supplies!Cups & Glasses!Cup Lids',1341,0,31803,NULL,NULL),(32381,'Hot Drink Cups','!Breakroom and Janitorial!Food & Beverage Service!Food Service Supplies!Cups & Glasses!Hot Drink Cups',1343,0,31803,NULL,NULL),(32382,'Coffee','!Breakroom and Janitorial!Food & Beverage Service!Beverages & Snack Foods!Beverages!Coffee',1345,0,31705,NULL,NULL),(32383,'Creamers','!Breakroom and Janitorial!Food & Beverage Service!Beverages & Snack Foods!Beverages!Creamers',1346,0,31705,NULL,NULL),(32384,'Flavor Stix for Water','!Breakroom and Janitorial!Food & Beverage Service!Beverages & Snack Foods!Beverages!Flavor Stix for Water',1347,0,31705,NULL,NULL),(32385,'Hot Cocoa','!Breakroom and Janitorial!Food & Beverage Service!Beverages & Snack Foods!Beverages!Hot Cocoa',1348,0,31705,NULL,NULL),(32386,'Juices','!Breakroom and Janitorial!Food & Beverage Service!Beverages & Snack Foods!Beverages!Juices',1349,0,31705,NULL,NULL),(32387,'Milk','!Breakroom and Janitorial!Food & Beverage Service!Beverages & Snack Foods!Beverages!Milk',1350,0,31705,NULL,NULL),(32388,'Soft Drinks','!Breakroom and Janitorial!Food & Beverage Service!Beverages & Snack Foods!Beverages!Soft Drinks',1351,0,31705,NULL,NULL),(32389,'Soup','!Breakroom and Janitorial!Food & Beverage Service!Beverages & Snack Foods!Beverages!Soup',1352,0,31705,NULL,NULL),(32390,'Tea','!Breakroom and Janitorial!Food & Beverage Service!Beverages & Snack Foods!Beverages!Tea',1353,0,31705,NULL,NULL),(32391,'Water','!Breakroom and Janitorial!Food & Beverage Service!Beverages & Snack Foods!Beverages!Water',1354,0,31705,NULL,NULL),(32392,'Scraper & Utility Knife Blades','!Breakroom and Janitorial!Facility Maintenance!Maintenance Tools!Scrapers & Knives!Scraper & Utility Knife Blades',1355,0,32122,NULL,NULL),(32393,'Scrapers & Putty Knives','!Breakroom and Janitorial!Facility Maintenance!Maintenance Tools!Scrapers & Knives!Scrapers & Putty Knives',1356,0,32122,NULL,NULL),(32394,'Utility Knives','!Breakroom and Janitorial!Facility Maintenance!Maintenance Tools!Scrapers & Knives!Utility Knives',1357,0,32122,NULL,NULL),(32395,'Barricades','!Breakroom and Janitorial!Facility Maintenance!Building Safety & Security Products!Safety Barriers & Crowd Control!Barricades',1358,0,32114,NULL,NULL),(32396,'Safety Cones','!Breakroom and Janitorial!Facility Maintenance!Building Safety & Security Products!Safety Barriers & Crowd Control!Safety Cones',1359,0,32114,NULL,NULL),(32397,'Safety Tapes','!Breakroom and Janitorial!Facility Maintenance!Building Safety & Security Products!Safety Barriers & Crowd Control!Safety Tapes',1360,0,32114,NULL,NULL),(32398,'Stanchion Systems','!Breakroom and Janitorial!Facility Maintenance!Building Safety & Security Products!Safety Barriers & Crowd Control!Stanchion Systems',1361,0,32114,NULL,NULL),(32399,'Key Cabinets','!Breakroom and Janitorial!Facility Maintenance!Building Safety & Security Products!Key Control!Key Cabinets',1362,0,31927,NULL,NULL),(32400,'Key Cases','!Breakroom and Janitorial!Facility Maintenance!Building Safety & Security Products!Key Control!Key Cases',1363,0,31927,NULL,NULL),(32401,'Key Chains','!Breakroom and Janitorial!Facility Maintenance!Building Safety & Security Products!Key Control!Key Chains',1364,0,31927,NULL,NULL),(32402,'Key Racks & Trays','!Breakroom and Janitorial!Facility Maintenance!Building Safety & Security Products!Key Control!Key Racks & Trays',1365,0,31927,NULL,NULL),(32403,'Key Tags','!Breakroom and Janitorial!Facility Maintenance!Building Safety & Security Products!Key Control!Key Tags',1366,0,31927,NULL,NULL),(32404,'Cord Covers','!Breakroom and Janitorial!Facility Maintenance!Batteries & Electrical Supplies!Power Cords & Accessories!Cord Covers',1367,0,32068,NULL,NULL),(32405,'Cord/Cable Managers','!Breakroom and Janitorial!Facility Maintenance!Batteries & Electrical Supplies!Power Cords & Accessories!Cord/Cable Managers',1368,0,32068,NULL,NULL),(32406,'Extension Cords','!Breakroom and Janitorial!Facility Maintenance!Batteries & Electrical Supplies!Power Cords & Accessories!Extension Cords',1369,0,32068,NULL,NULL),(32407,'Plug Adapters','!Breakroom and Janitorial!Facility Maintenance!Batteries & Electrical Supplies!Power Cords & Accessories!Plug Adapters',1370,0,32068,NULL,NULL),(32408,'Power Strips & Receptacles','!Breakroom and Janitorial!Facility Maintenance!Batteries & Electrical Supplies!Power Cords & Accessories!Power Strips & Receptacles',1371,0,32068,NULL,NULL),(32409,'Batteries','!Breakroom and Janitorial!Facility Maintenance!Batteries & Electrical Supplies!Batteries & Chargers!Batteries',1372,0,31702,NULL,NULL),(32410,'Battery Chargers','!Breakroom and Janitorial!Facility Maintenance!Batteries & Electrical Supplies!Batteries & Chargers!Battery Chargers',1373,0,31702,NULL,NULL),(32411,'Student Desks','!Furniture!Desks & Workstations!Desks!Classroom Desks!Student Desks',1376,0,31763,NULL,NULL),(32412,'File Folder Shelf Files','!Furniture!Cabinets, Racks & Shelves!File & Storage Cabinets!Shelf Files!File Folder Shelf Files',1377,0,32134,NULL,NULL),(32413,'Shelf File Accessories','!Furniture!Cabinets, Racks & Shelves!File & Storage Cabinets!Shelf Files!Shelf File Accessories',1378,0,32134,NULL,NULL),(32414,'Lockers','!Furniture!Cabinets, Racks & Shelves!File & Storage Cabinets!Lockers & Storage Cabinets!Lockers',1379,0,31956,NULL,NULL),(32415,'Storage & Wardrobe Cabinets','!Furniture!Cabinets, Racks & Shelves!File & Storage Cabinets!Lockers & Storage Cabinets!Storage & Wardrobe Cabinets',1380,0,31956,NULL,NULL),(32416,'Literature Display Floor Stands','!Furniture!Cabinets, Racks & Shelves!Display Racks & Cases!Literature Display Holders!Literature Display Floor Stands',1383,0,31955,NULL,NULL),(32417,'Literature Display Wall Racks','!Furniture!Cabinets, Racks & Shelves!Display Racks & Cases!Literature Display Holders!Literature Display Wall Racks',1384,0,31955,NULL,NULL),(32418,'Outdoor Literature Display Boxes','!Furniture!Cabinets, Racks & Shelves!Display Racks & Cases!Literature Display Holders!Outdoor Literature Display Boxes',1385,0,31955,NULL,NULL),(32419,'Laser Printer Toner','!Technology!Imaging Supplies and Accessories!Imaging Supplies!Copier, Fax & Laser Printer Supplies!Toner!Laser Printer Toner',1387,0,32264,NULL,NULL),(32420,'Copier/Fax Toner','!Technology!Imaging Supplies and Accessories!Imaging Supplies!Copier, Fax & Laser Printer Supplies!Toner!Copier/Fax Toner',1386,0,32264,NULL,NULL),(32421,'Toner/Developer/Drum Cartridges','!Technology!Imaging Supplies and Accessories!Imaging Supplies!Copier, Fax & Laser Printer Supplies!Toner!Toner/Developer/Drum Cartridges',1388,0,32264,NULL,NULL),(32422,'Specialty Classification Folders','!Office Supplies!File Folders, Portable & Storage Box Files!Folders!Top Tab & End Tab Folders!Classification/Fastener Folders!Specialty Classification Folders',1395,0,32347,NULL,NULL),(32423,'Top Tab Classification Folders','!Office Supplies!File Folders, Portable & Storage Box Files!Folders!Top Tab & End Tab Folders!Classification/Fastener Folders!Top Tab Classification Folders',1396,0,32347,NULL,NULL),(32424,'End Tab Classification Folders','!Office Supplies!File Folders, Portable & Storage Box Files!Folders!Top Tab & End Tab Folders!Classification/Fastener Folders!End Tab Classification Folders',1394,0,32347,NULL,NULL),(32425,'Top Tab File Folders','!Office Supplies!File Folders, Portable & Storage Box Files!Folders!Top Tab & End Tab Folders!File Folders!Top Tab File Folders',1393,0,32348,NULL,NULL),(32426,'End Tab File Folders','!Office Supplies!File Folders, Portable & Storage Box Files!Folders!Top Tab & End Tab Folders!File Folders!End Tab File Folders',1392,0,32348,NULL,NULL),(32427,'Conversion File Folders','!Office Supplies!File Folders, Portable & Storage Box Files!Folders!Top Tab & End Tab Folders!File Folders!Conversion File Folders',1391,0,32348,NULL,NULL),(32573,'Appointment Book','!Appointment Book',123456789,0,NULL,NULL,NULL),(32574,'Box of Pencils','!Box of Pencils',22222222222,0,NULL,NULL,NULL),(32575,'Pack of Paper','!Pack of Paper',333333333,0,NULL,NULL,NULL),(5875888,'Maintenance, Repair and Operations','!Maintenance, Repair and Operations',1500,0,NULL,NULL,NULL),(5875889,'Abrasives','!Maintenance, Repair and Operations!Abrasives',1502,0,5875888,NULL,NULL),(5875890,'Coated Abrasives','!Maintenance, Repair and Operations!Abrasives!Coated Abrasives',1544,0,5875889,NULL,NULL),(5875891,'Coated Flap Wheel Abrasive Parts and Accessories','!Maintenance, Repair and Operations!Abrasives!Coated Abrasives!Coated Flap Wheel Abrasive Parts and Accessories',1981,0,5875890,NULL,NULL),(5875892,'Coated Flap Wheel Abrasives','!Maintenance, Repair and Operations!Abrasives!Coated Abrasives!Coated Flap Wheel Abrasives',1982,0,5875890,NULL,NULL),(5875893,'Coated Roll Abrasive Parts and Accessories','!Maintenance, Repair and Operations!Abrasives!Coated Abrasives!Coated Roll Abrasive Parts and Accessories',1983,0,5875890,NULL,NULL),(5875894,'Coated Roll Abrasives','!Maintenance, Repair and Operations!Abrasives!Coated Abrasives!Coated Roll Abrasives',1984,0,5875890,NULL,NULL),(5875895,'Coated Sheet Abrasives','!Maintenance, Repair and Operations!Abrasives!Coated Abrasives!Coated Sheet Abrasives',1985,0,5875890,NULL,NULL),(5875896,'Coated Sleeve Abrasive Parts and Accessories','!Maintenance, Repair and Operations!Abrasives!Coated Abrasives!Coated Sleeve Abrasive Parts and Accessories',1986,0,5875890,NULL,NULL),(5875897,'Coated Sleeve Abrasives','!Maintenance, Repair and Operations!Abrasives!Coated Abrasives!Coated Sleeve Abrasives',1987,0,5875890,NULL,NULL),(5875898,'Non Woven Abrasives','!Maintenance, Repair and Operations!Abrasives!Non Woven Abrasives',1545,0,5875889,NULL,NULL),(5875899,'Non Woven Belt Abrasives','!Maintenance, Repair and Operations!Abrasives!Non Woven Abrasives!Non Woven Belt Abrasives',1988,0,5875898,NULL,NULL),(5875900,'Non Woven Disc Abrasive Parts and Accessories','!Maintenance, Repair and Operations!Abrasives!Non Woven Abrasives!Non Woven Disc Abrasive Parts and Accessories',1989,0,5875898,NULL,NULL),(5875901,'Non Woven Disc Abrasives','!Maintenance, Repair and Operations!Abrasives!Non Woven Abrasives!Non Woven Disc Abrasives',1990,0,5875898,NULL,NULL),(5875902,'Bonded Abrasives','!Maintenance, Repair and Operations!Abrasives!Bonded Abrasives',1546,0,5875889,NULL,NULL),(5875903,'Bonded Abrasive Parts and Accessories','!Maintenance, Repair and Operations!Abrasives!Bonded Abrasives!Bonded Abrasive Parts and Accessories',1991,0,5875902,NULL,NULL),(5875904,'Non Woven Abrasives','!Maintenance, Repair and Operations!Abrasives!Non Woven Abrasives',1547,0,5875889,NULL,NULL),(5875905,'Non Woven Pad Abrasives','!Maintenance, Repair and Operations!Abrasives!Non Woven Abrasives!Non Woven Pad Abrasives',1992,0,5875904,NULL,NULL),(5875906,'Non Woven Roll Abrasive Parts and Accessories','!Maintenance, Repair and Operations!Abrasives!Non Woven Abrasives!Non Woven Roll Abrasive Parts and Accessories',1993,0,5875904,NULL,NULL),(5875907,'Non Woven Roll Abrasives','!Maintenance, Repair and Operations!Abrasives!Non Woven Abrasives!Non Woven Roll Abrasives',1994,0,5875904,NULL,NULL),(5875908,'Non Woven Sponge Abrasives','!Maintenance, Repair and Operations!Abrasives!Non Woven Abrasives!Non Woven Sponge Abrasives',1995,0,5875904,NULL,NULL),(5875909,'Non Woven Star Abrasives','!Maintenance, Repair and Operations!Abrasives!Non Woven Abrasives!Non Woven Star Abrasives',1996,0,5875904,NULL,NULL),(5875910,'Non Woven Wheel Abrasive Parts and Accessories','!Maintenance, Repair and Operations!Abrasives!Non Woven Abrasives!Non Woven Wheel Abrasive Parts and Accessories',1997,0,5875904,NULL,NULL),(5875911,'Non Woven Wheel Abrasives','!Maintenance, Repair and Operations!Abrasives!Non Woven Abrasives!Non Woven Wheel Abrasives',1998,0,5875904,NULL,NULL),(5875912,'Steel Wool','!Maintenance, Repair and Operations!Abrasives!Non Woven Abrasives!Steel Wool',1999,0,5875904,NULL,NULL),(5875913,'Wire Brush Abrasives','!Maintenance, Repair and Operations!Abrasives!Wire Brush Abrasives',1548,0,5875889,NULL,NULL),(5875914,'Cup Brushes','!Maintenance, Repair and Operations!Abrasives!Wire Brush Abrasives!Cup Brushes',2000,0,5875913,NULL,NULL),(5875915,'Disc Brushes','!Maintenance, Repair and Operations!Abrasives!Wire Brush Abrasives!Disc Brushes',2001,0,5875913,NULL,NULL),(5875916,'Bonded Abrasives','!Maintenance, Repair and Operations!Abrasives!Bonded Abrasives',1549,0,5875889,NULL,NULL),(5875917,'Resin Bonded Abrasives','!Maintenance, Repair and Operations!Abrasives!Bonded Abrasives!Resin Bonded Abrasives',2002,0,5875916,NULL,NULL),(5875918,'Wire Brush Abrasives','!Maintenance, Repair and Operations!Abrasives!Wire Brush Abrasives',1550,0,5875889,NULL,NULL),(5875919,'End Brushes','!Maintenance, Repair and Operations!Abrasives!Wire Brush Abrasives!End Brushes',2003,0,5875918,NULL,NULL),(5875920,'Scratch Brushes','!Maintenance, Repair and Operations!Abrasives!Wire Brush Abrasives!Scratch Brushes',2004,0,5875918,NULL,NULL),(5875921,'Tube Brush Parts and Accessories','!Maintenance, Repair and Operations!Abrasives!Wire Brush Abrasives!Tube Brush Parts and Accessories',2005,0,5875918,NULL,NULL),(5875922,'Tube Brushes','!Maintenance, Repair and Operations!Abrasives!Wire Brush Abrasives!Tube Brushes',2006,0,5875918,NULL,NULL),(5875923,'Wheel Brush Parts and Accessories','!Maintenance, Repair and Operations!Abrasives!Wire Brush Abrasives!Wheel Brush Parts and Accessories',2007,0,5875918,NULL,NULL),(5875924,'Wheel Brushes','!Maintenance, Repair and Operations!Abrasives!Wire Brush Abrasives!Wheel Brushes',2008,0,5875918,NULL,NULL),(5875925,'Bonded Abrasives','!Maintenance, Repair and Operations!Abrasives!Bonded Abrasives',1551,0,5875889,NULL,NULL),(5875926,'Vitrified Abrasives','!Maintenance, Repair and Operations!Abrasives!Bonded Abrasives!Vitrified Abrasives',2009,0,5875925,NULL,NULL),(5875927,'Coated Abrasives','!Maintenance, Repair and Operations!Abrasives!Coated Abrasives',1552,0,5875889,NULL,NULL),(5875928,'Coated Belt Abrasives','!Maintenance, Repair and Operations!Abrasives!Coated Abrasives!Coated Belt Abrasives',2010,0,5875927,NULL,NULL),(5875929,'Coated Disc Abrasive Parts and Accessories','!Maintenance, Repair and Operations!Abrasives!Coated Abrasives!Coated Disc Abrasive Parts and Accessories',2011,0,5875927,NULL,NULL),(5875930,'Coated Disc Abrasives','!Maintenance, Repair and Operations!Abrasives!Coated Abrasives!Coated Disc Abrasives',2012,0,5875927,NULL,NULL),(5875931,'Coated Flap Disc Abrasive Parts and Accessories','!Maintenance, Repair and Operations!Abrasives!Coated Abrasives!Coated Flap Disc Abrasive Parts and Accessories',2013,0,5875927,NULL,NULL),(5875932,'Coated Flap Disc Abrasives','!Maintenance, Repair and Operations!Abrasives!Coated Abrasives!Coated Flap Disc Abrasives',2014,0,5875927,NULL,NULL),(5875933,'Adhesives, Sealants and Tapes','!Maintenance, Repair and Operations!Adhesives, Sealants and Tapes',1503,0,5875888,NULL,NULL),(5875934,'Tape Products','!Maintenance, Repair and Operations!Adhesives, Sealants and Tapes!Tape Products',1554,0,5875933,NULL,NULL),(5875935,'Electrical Tapes','!Maintenance, Repair and Operations!Adhesives, Sealants and Tapes!Tape Products!Electrical Tapes',2016,0,5875934,NULL,NULL),(5875936,'Marking Tapes','!Maintenance, Repair and Operations!Adhesives, Sealants and Tapes!Tape Products!Marking Tapes',2017,0,5875934,NULL,NULL),(5875937,'Masking Tapes','!Maintenance, Repair and Operations!Adhesives, Sealants and Tapes!Tape Products!Masking Tapes',2018,0,5875934,NULL,NULL),(5875938,'Packaging Tape Dispensers','!Maintenance, Repair and Operations!Adhesives, Sealants and Tapes!Tape Products!Packaging Tape Dispensers',2019,0,5875934,NULL,NULL),(5875939,'Packaging Tapes','!Maintenance, Repair and Operations!Adhesives, Sealants and Tapes!Tape Products!Packaging Tapes',2020,0,5875934,NULL,NULL),(5875940,'Pipe Tapes','!Maintenance, Repair and Operations!Adhesives, Sealants and Tapes!Tape Products!Pipe Tapes',2021,0,5875934,NULL,NULL),(5875941,'Specialty Tapes','!Maintenance, Repair and Operations!Adhesives, Sealants and Tapes!Tape Products!Specialty Tapes',2022,0,5875934,NULL,NULL),(5875942,'Strapping Tapes','!Maintenance, Repair and Operations!Adhesives, Sealants and Tapes!Tape Products!Strapping Tapes',2023,0,5875934,NULL,NULL),(5875943,'Thread Seal Tapes','!Maintenance, Repair and Operations!Adhesives, Sealants and Tapes!Tape Products!Thread Seal Tapes',2024,0,5875934,NULL,NULL),(5875944,'Adhesive Products','!Maintenance, Repair and Operations!Adhesives, Sealants and Tapes!Adhesive Products',1555,0,5875933,NULL,NULL),(5875945,'Adhesive Product Accessories','!Maintenance, Repair and Operations!Adhesives, Sealants and Tapes!Adhesive Products!Adhesive Product Accessories',2025,0,5875944,NULL,NULL),(5875946,'Adhesives and Glues','!Maintenance, Repair and Operations!Adhesives, Sealants and Tapes!Adhesive Products!Adhesives and Glues',2026,0,5875944,NULL,NULL),(5875947,'Putty','!Maintenance, Repair and Operations!Adhesives, Sealants and Tapes!Adhesive Products!Putty',2027,0,5875944,NULL,NULL),(5875948,'Sealing Products','!Maintenance, Repair and Operations!Adhesives, Sealants and Tapes!Sealing Products',1556,0,5875933,NULL,NULL),(5875949,'Caulk Guns','!Maintenance, Repair and Operations!Adhesives, Sealants and Tapes!Sealing Products!Caulk Guns',2028,0,5875948,NULL,NULL),(5875950,'Retaining Compounds','!Maintenance, Repair and Operations!Adhesives, Sealants and Tapes!Sealing Products!Retaining Compounds',2029,0,5875948,NULL,NULL),(5875951,'Sealants','!Maintenance, Repair and Operations!Adhesives, Sealants and Tapes!Sealing Products!Sealants',2030,0,5875948,NULL,NULL),(5875952,'Threadlockers','!Maintenance, Repair and Operations!Adhesives, Sealants and Tapes!Sealing Products!Threadlockers',2031,0,5875948,NULL,NULL),(5875953,'Tape Products','!Maintenance, Repair and Operations!Adhesives, Sealants and Tapes!Tape Products',1557,0,5875933,NULL,NULL),(5875954,'Duct Tapes','!Maintenance, Repair and Operations!Adhesives, Sealants and Tapes!Tape Products!Duct Tapes',2032,0,5875953,NULL,NULL),(5875955,'Automotive Maintenance Products','!Maintenance, Repair and Operations!Automotive Maintenance Products',1504,0,5875888,NULL,NULL),(5875956,'Battery Chargers and Starting Systems','!Maintenance, Repair and Operations!Automotive Maintenance Products!Battery Chargers and Starting Systems',1558,0,5875955,NULL,NULL),(5875957,'Hose Benders','!Maintenance, Repair and Operations!Automotive Maintenance Products!Hose Benders',1559,0,5875955,NULL,NULL),(5875958,'Measuring Cans','!Maintenance, Repair and Operations!Automotive Maintenance Products!Measuring Cans',1560,0,5875955,NULL,NULL),(5875959,'Oil Drain Products','!Maintenance, Repair and Operations!Automotive Maintenance Products!Oil Drain Products',1561,0,5875955,NULL,NULL),(5875960,'Piston Compressors','!Maintenance, Repair and Operations!Automotive Maintenance Products!Piston Compressors',1562,0,5875955,NULL,NULL),(5875961,'Tire Gauges','!Maintenance, Repair and Operations!Automotive Maintenance Products!Tire Gauges',1563,0,5875955,NULL,NULL),(5875962,'Tire Sealers','!Maintenance, Repair and Operations!Automotive Maintenance Products!Tire Sealers',1564,0,5875955,NULL,NULL),(5875963,'Battery Clamps','!Maintenance, Repair and Operations!Automotive Maintenance Products!Battery Clamps',1565,0,5875955,NULL,NULL),(5875964,'Battery Post Cleaners','!Maintenance, Repair and Operations!Automotive Maintenance Products!Battery Post Cleaners',1566,0,5875955,NULL,NULL),(5875965,'Booster Cables','!Maintenance, Repair and Operations!Automotive Maintenance Products!Booster Cables',1567,0,5875955,NULL,NULL),(5875966,'Car Waxes, Polishes and Protectants','!Maintenance, Repair and Operations!Automotive Maintenance Products!Car Waxes, Polishes and Protectants',1568,0,5875955,NULL,NULL),(5875967,'Fiberglass Repair Kits','!Maintenance, Repair and Operations!Automotive Maintenance Products!Fiberglass Repair Kits',1569,0,5875955,NULL,NULL),(5875968,'Fuel Filters','!Maintenance, Repair and Operations!Automotive Maintenance Products!Fuel Filters',1570,0,5875955,NULL,NULL),(5875969,'Funnel Sets','!Maintenance, Repair and Operations!Automotive Maintenance Products!Funnel Sets',1571,0,5875955,NULL,NULL),(5875970,'Funnels','!Maintenance, Repair and Operations!Automotive Maintenance Products!Funnels',1572,0,5875955,NULL,NULL),(5875971,'Batteries','!Maintenance, Repair and Operations!Batteries',1505,0,5875888,NULL,NULL),(5875972,'Battery Chargers','!Maintenance, Repair and Operations!Batteries!Battery Chargers',1574,0,5875971,NULL,NULL),(5875973,'Chemicals, Lubricants and Paints','!Maintenance, Repair and Operations!Chemicals, Lubricants and Paints',1506,0,5875888,NULL,NULL),(5875974,'Leak Detectors and Penetrants','!Maintenance, Repair and Operations!Chemicals, Lubricants and Paints!Leak Detectors and Penetrants',1576,0,5875973,NULL,NULL),(5875975,'Surface Irregularity Testing Product Accessories','!Maintenance, Repair and Operations!Chemicals, Lubricants and Paints!Leak Detectors and Penetrants!Surface Irregularity Testing Product Accessories',2034,0,5875974,NULL,NULL),(5875976,'Lubricants and Penetrants','!Maintenance, Repair and Operations!Chemicals, Lubricants and Paints!Lubricants and Penetrants',1577,0,5875973,NULL,NULL),(5875977,'Anti-Seize Compounds','!Maintenance, Repair and Operations!Chemicals, Lubricants and Paints!Lubricants and Penetrants!Anti-Seize Compounds',2035,0,5875976,NULL,NULL),(5875978,'Belt Dressings','!Maintenance, Repair and Operations!Chemicals, Lubricants and Paints!Lubricants and Penetrants!Belt Dressings',2036,0,5875976,NULL,NULL),(5875979,'Chain and Cable Lubricants','!Maintenance, Repair and Operations!Chemicals, Lubricants and Paints!Lubricants and Penetrants!Chain and Cable Lubricants',2037,0,5875976,NULL,NULL),(5875980,'Corrosion Inhibitors','!Maintenance, Repair and Operations!Chemicals, Lubricants and Paints!Lubricants and Penetrants!Corrosion Inhibitors',2038,0,5875976,NULL,NULL),(5875981,'Drill Collar and Tool Joint Compounds','!Maintenance, Repair and Operations!Chemicals, Lubricants and Paints!Lubricants and Penetrants!Drill Collar and Tool Joint Compounds',2039,0,5875976,NULL,NULL),(5875982,'Dry Lubes','!Maintenance, Repair and Operations!Chemicals, Lubricants and Paints!Lubricants and Penetrants!Dry Lubes',2040,0,5875976,NULL,NULL),(5875983,'Fluids','!Maintenance, Repair and Operations!Chemicals, Lubricants and Paints!Lubricants and Penetrants!Fluids',2041,0,5875976,NULL,NULL),(5875984,'Grease','!Maintenance, Repair and Operations!Chemicals, Lubricants and Paints!Lubricants and Penetrants!Grease',2042,0,5875976,NULL,NULL),(5875985,'Grease Fittings','!Maintenance, Repair and Operations!Chemicals, Lubricants and Paints!Lubricants and Penetrants!Grease Fittings',2043,0,5875976,NULL,NULL),(5875986,'Cleaning Products','!Maintenance, Repair and Operations!Chemicals, Lubricants and Paints!Cleaning Products',1578,0,5875973,NULL,NULL),(5875987,'Cleaners and Degreasers','!Maintenance, Repair and Operations!Chemicals, Lubricants and Paints!Cleaning Products!Cleaners and Degreasers',2044,0,5875986,NULL,NULL),(5875988,'Lubricants and Penetrants','!Maintenance, Repair and Operations!Chemicals, Lubricants and Paints!Lubricants and Penetrants',1579,0,5875973,NULL,NULL),(5875989,'Grease Gun Accessories','!Maintenance, Repair and Operations!Chemicals, Lubricants and Paints!Lubricants and Penetrants!Grease Gun Accessories',2045,0,5875988,NULL,NULL),(5875990,'Grease Gun Hoses, Extensions and Adapters','!Maintenance, Repair and Operations!Chemicals, Lubricants and Paints!Lubricants and Penetrants!Grease Gun Hoses, Extensions and Adapters',2046,0,5875988,NULL,NULL),(5875991,'Grease Guns','!Maintenance, Repair and Operations!Chemicals, Lubricants and Paints!Lubricants and Penetrants!Grease Guns',2047,0,5875988,NULL,NULL),(5875992,'Lubricating Product Accessories','!Maintenance, Repair and Operations!Chemicals, Lubricants and Paints!Lubricants and Penetrants!Lubricating Product Accessories',2048,0,5875988,NULL,NULL),(5875993,'Multi-Purpose Lubricants','!Maintenance, Repair and Operations!Chemicals, Lubricants and Paints!Lubricants and Penetrants!Multi-Purpose Lubricants',2049,0,5875988,NULL,NULL),(5875994,'Oiler and Sprayer Extensions and Adapters','!Maintenance, Repair and Operations!Chemicals, Lubricants and Paints!Lubricants and Penetrants!Oiler and Sprayer Extensions and Adapters',2050,0,5875988,NULL,NULL),(5875995,'Oilers and Sprayers','!Maintenance, Repair and Operations!Chemicals, Lubricants and Paints!Lubricants and Penetrants!Oilers and Sprayers',2051,0,5875988,NULL,NULL),(5875996,'Oils','!Maintenance, Repair and Operations!Chemicals, Lubricants and Paints!Lubricants and Penetrants!Oils',2052,0,5875988,NULL,NULL),(5875997,'Silicone Lubricants','!Maintenance, Repair and Operations!Chemicals, Lubricants and Paints!Lubricants and Penetrants!Silicone Lubricants',2053,0,5875988,NULL,NULL),(5875998,'Specialty Lubricants','!Maintenance, Repair and Operations!Chemicals, Lubricants and Paints!Lubricants and Penetrants!Specialty Lubricants',2054,0,5875988,NULL,NULL),(5875999,'Cleaning Products','!Maintenance, Repair and Operations!Chemicals, Lubricants and Paints!Cleaning Products',1580,0,5875973,NULL,NULL),(5876000,'Cleaning Products Accessories','!Maintenance, Repair and Operations!Chemicals, Lubricants and Paints!Cleaning Products!Cleaning Products Accessories',2055,0,5875999,NULL,NULL),(5876001,'Paint and Paint Supplies','!Maintenance, Repair and Operations!Chemicals, Lubricants and Paints!Paint and Paint Supplies',1581,0,5875973,NULL,NULL),(5876002,'Coatings','!Maintenance, Repair and Operations!Chemicals, Lubricants and Paints!Paint and Paint Supplies!Coatings',2056,0,5876001,NULL,NULL),(5876003,'Drop Cloths','!Maintenance, Repair and Operations!Chemicals, Lubricants and Paints!Paint and Paint Supplies!Drop Cloths',2057,0,5876001,NULL,NULL),(5876004,'Inks','!Maintenance, Repair and Operations!Chemicals, Lubricants and Paints!Paint and Paint Supplies!Inks',2058,0,5876001,NULL,NULL),(5876005,'Paint Brushes','!Maintenance, Repair and Operations!Chemicals, Lubricants and Paints!Paint and Paint Supplies!Paint Brushes',2059,0,5876001,NULL,NULL),(5876006,'Paint Rollers and Covers','!Maintenance, Repair and Operations!Chemicals, Lubricants and Paints!Paint and Paint Supplies!Paint Rollers and Covers',2060,0,5876001,NULL,NULL),(5876007,'Paint Stripers','!Maintenance, Repair and Operations!Chemicals, Lubricants and Paints!Paint and Paint Supplies!Paint Stripers',2061,0,5876001,NULL,NULL),(5876008,'Paint Trays and Liners','!Maintenance, Repair and Operations!Chemicals, Lubricants and Paints!Paint and Paint Supplies!Paint Trays and Liners',2062,0,5876001,NULL,NULL),(5876009,'Paints','!Maintenance, Repair and Operations!Chemicals, Lubricants and Paints!Paint and Paint Supplies!Paints',2063,0,5876001,NULL,NULL),(5876010,'Spray Gun Parts and Accessories','!Maintenance, Repair and Operations!Chemicals, Lubricants and Paints!Paint and Paint Supplies!Spray Gun Parts and Accessories',2064,0,5876001,NULL,NULL),(5876011,'Spray Guns','!Maintenance, Repair and Operations!Chemicals, Lubricants and Paints!Paint and Paint Supplies!Spray Guns',2065,0,5876001,NULL,NULL),(5876012,'Insect and Weed Controlling Products','!Maintenance, Repair and Operations!Chemicals, Lubricants and Paints!Insect and Weed Controlling Products',1582,0,5875973,NULL,NULL),(5876013,'Insect Killers','!Maintenance, Repair and Operations!Chemicals, Lubricants and Paints!Insect and Weed Controlling Products!Insect Killers',2066,0,5876012,NULL,NULL),(5876014,'Paint and Paint Supplies','!Maintenance, Repair and Operations!Chemicals, Lubricants and Paints!Paint and Paint Supplies',1583,0,5875973,NULL,NULL),(5876015,'Thinners','!Maintenance, Repair and Operations!Chemicals, Lubricants and Paints!Paint and Paint Supplies!Thinners',2067,0,5876014,NULL,NULL),(5876016,'Specialty Products','!Maintenance, Repair and Operations!Chemicals, Lubricants and Paints!Specialty Products',1584,0,5875973,NULL,NULL),(5876017,'Coolants','!Maintenance, Repair and Operations!Chemicals, Lubricants and Paints!Specialty Products!Coolants',2068,0,5876016,NULL,NULL),(5876018,'De-Icers','!Maintenance, Repair and Operations!Chemicals, Lubricants and Paints!Specialty Products!De-Icers',2069,0,5876016,NULL,NULL),(5876019,'Finding Paste','!Maintenance, Repair and Operations!Chemicals, Lubricants and Paints!Specialty Products!Finding Paste',2070,0,5876016,NULL,NULL),(5876020,'Layout Fluids','!Maintenance, Repair and Operations!Chemicals, Lubricants and Paints!Specialty Products!Layout Fluids',2071,0,5876016,NULL,NULL),(5876021,'Insect and Weed Controlling Products','!Maintenance, Repair and Operations!Chemicals, Lubricants and Paints!Insect and Weed Controlling Products',1585,0,5875973,NULL,NULL),(5876022,'Weed Killers','!Maintenance, Repair and Operations!Chemicals, Lubricants and Paints!Insect and Weed Controlling Products!Weed Killers',2072,0,5876021,NULL,NULL),(5876023,'Leak Detectors and Penetrants','!Maintenance, Repair and Operations!Chemicals, Lubricants and Paints!Leak Detectors and Penetrants',1586,0,5875973,NULL,NULL),(5876024,'Leak Detectors','!Maintenance, Repair and Operations!Chemicals, Lubricants and Paints!Leak Detectors and Penetrants!Leak Detectors',2073,0,5876023,NULL,NULL),(5876025,'Magnetic Particles','!Maintenance, Repair and Operations!Chemicals, Lubricants and Paints!Leak Detectors and Penetrants!Magnetic Particles',2074,0,5876023,NULL,NULL),(5876026,'Penetrant Kits','!Maintenance, Repair and Operations!Chemicals, Lubricants and Paints!Leak Detectors and Penetrants!Penetrant Kits',2075,0,5876023,NULL,NULL),(5876027,'Penetrants','!Maintenance, Repair and Operations!Chemicals, Lubricants and Paints!Leak Detectors and Penetrants!Penetrants',2076,0,5876023,NULL,NULL),(5876028,'Communicating Products','!Maintenance, Repair and Operations!Communicating Products',1507,0,5875888,NULL,NULL),(5876029,'Two-Way Radio Accessories','!Maintenance, Repair and Operations!Communicating Products!Two-Way Radio Accessories',1587,0,5876028,NULL,NULL),(5876030,'Two-Way Radios','!Maintenance, Repair and Operations!Communicating Products!Two-Way Radios',1588,0,5876028,NULL,NULL),(5876031,'Covering Products','!Maintenance, Repair and Operations!Covering Products',1508,0,5875888,NULL,NULL),(5876032,'Plastic Sheeting','!Maintenance, Repair and Operations!Covering Products!Plastic Sheeting',1589,0,5876031,NULL,NULL),(5876033,'Tarp Accessories','!Maintenance, Repair and Operations!Covering Products!Tarp Accessories',1590,0,5876031,NULL,NULL),(5876034,'Grommets','!Maintenance, Repair and Operations!Covering Products!Tarp Accessories!Grommets',2078,0,5876033,NULL,NULL),(5876035,'Tarps','!Maintenance, Repair and Operations!Covering Products!Tarps',1591,0,5876031,NULL,NULL),(5876036,'Display Units','!Maintenance, Repair and Operations!Display Units',1509,0,5875888,NULL,NULL),(5876037,'Anchor Displays','!Maintenance, Repair and Operations!Display Units!Anchor Displays',1592,0,5876036,NULL,NULL),(5876038,'Display Accessories','!Maintenance, Repair and Operations!Display Units!Display Accessories',1593,0,5876036,NULL,NULL),(5876039,'Electrical and Lighting','!Maintenance, Repair and Operations!Electrical and Lighting',1510,0,5875888,NULL,NULL),(5876040,'Electrical Tools','!Maintenance, Repair and Operations!Electrical and Lighting!Electrical Tools',1595,0,5876039,NULL,NULL),(5876041,'Fish Tape Blowers','!Maintenance, Repair and Operations!Electrical and Lighting!Electrical Tools!Fish Tape Blowers',2080,0,5876040,NULL,NULL),(5876042,'Fish Tapes','!Maintenance, Repair and Operations!Electrical and Lighting!Electrical Tools!Fish Tapes',2081,0,5876040,NULL,NULL),(5876043,'Grips','!Maintenance, Repair and Operations!Electrical and Lighting!Electrical Tools!Grips',2082,0,5876040,NULL,NULL),(5876044,'Knockout Punch Parts and Accessories','!Maintenance, Repair and Operations!Electrical and Lighting!Electrical Tools!Knockout Punch Parts and Accessories',2083,0,5876040,NULL,NULL),(5876045,'Knockout Punches Hydraulic','!Maintenance, Repair and Operations!Electrical and Lighting!Electrical Tools!Knockout Punches Hydraulic',2084,0,5876040,NULL,NULL),(5876046,'Knockout Punches Manual','!Maintenance, Repair and Operations!Electrical and Lighting!Electrical Tools!Knockout Punches Manual',2085,0,5876040,NULL,NULL),(5876047,'Meters','!Maintenance, Repair and Operations!Electrical and Lighting!Electrical Tools!Meters',2086,0,5876040,NULL,NULL),(5876048,'Punchdown Tool Parts and Accessories','!Maintenance, Repair and Operations!Electrical and Lighting!Electrical Tools!Punchdown Tool Parts and Accessories',2087,0,5876040,NULL,NULL),(5876049,'Punchdown Tools','!Maintenance, Repair and Operations!Electrical and Lighting!Electrical Tools!Punchdown Tools',2088,0,5876040,NULL,NULL),(5876050,'Testers and Indicator Parts and Accessories','!Maintenance, Repair and Operations!Electrical and Lighting!Electrical Tools!Testers and Indicator Parts and Accessories',2089,0,5876040,NULL,NULL),(5876051,'Cable Protectors','!Maintenance, Repair and Operations!Electrical and Lighting!Cable Protectors',1596,0,5876039,NULL,NULL),(5876052,'Electrical Tools','!Maintenance, Repair and Operations!Electrical and Lighting!Electrical Tools',1597,0,5876039,NULL,NULL),(5876053,'Testers and Indicators','!Maintenance, Repair and Operations!Electrical and Lighting!Electrical Tools!Testers and Indicators',2091,0,5876052,NULL,NULL),(5876054,'Wire Pulling Grips','!Maintenance, Repair and Operations!Electrical and Lighting!Electrical Tools!Wire Pulling Grips',2092,0,5876052,NULL,NULL),(5876055,'Wire/Cable Carts and Dispensers','!Maintenance, Repair and Operations!Electrical and Lighting!Electrical Tools!Wire/Cable Carts and Dispensers',2093,0,5876052,NULL,NULL),(5876056,'Emergency Lights','!Maintenance, Repair and Operations!Electrical and Lighting!Emergency Lights',1598,0,5876039,NULL,NULL),(5876057,'Extension and Power Cord Accessories','!Maintenance, Repair and Operations!Electrical and Lighting!Extension and Power Cord Accessories',1599,0,5876039,NULL,NULL),(5876058,'Extension and Power Cords','!Maintenance, Repair and Operations!Electrical and Lighting!Extension and Power Cords',1600,0,5876039,NULL,NULL),(5876059,'Cord Cable Managers','!Maintenance, Repair and Operations!Electrical and Lighting!Extension and Power Cords!Cord Cable Managers',2095,0,5876058,NULL,NULL),(5876060,'Cord Covers','!Maintenance, Repair and Operations!Electrical and Lighting!Extension and Power Cords!Cord Covers',2096,0,5876058,NULL,NULL),(5876061,'Extension Cords','!Maintenance, Repair and Operations!Electrical and Lighting!Extension and Power Cords!Extension Cords',2097,0,5876058,NULL,NULL),(5876062,'Plug Adapters','!Maintenance, Repair and Operations!Electrical and Lighting!Extension and Power Cords!Plug Adapters',2098,0,5876058,NULL,NULL),(5876063,'Connectors, Terminals and Clips','!Maintenance, Repair and Operations!Electrical and Lighting!Connectors, Terminals and Clips',1601,0,5876039,NULL,NULL),(5876064,'Extension and Power Cords','!Maintenance, Repair and Operations!Electrical and Lighting!Extension and Power Cords',1602,0,5876039,NULL,NULL),(5876065,'Power Strips and Receptacles','!Maintenance, Repair and Operations!Electrical and Lighting!Extension and Power Cords!Power Strips and Receptacles',2100,0,5876064,NULL,NULL),(5876066,'Flashlight and Lantern Parts and Accessories','!Maintenance, Repair and Operations!Electrical and Lighting!Flashlight and Lantern Parts and Accessories',1603,0,5876039,NULL,NULL),(5876067,'Flashlights and Lanterns','!Maintenance, Repair and Operations!Electrical and Lighting!Flashlights and Lanterns',1604,0,5876039,NULL,NULL),(5876068,'Fuses','!Maintenance, Repair and Operations!Electrical and Lighting!Fuses',1605,0,5876039,NULL,NULL),(5876069,'Generators','!Maintenance, Repair and Operations!Electrical and Lighting!Generators',1606,0,5876039,NULL,NULL),(5876070,'Ground Fault Circuit Interrupters','!Maintenance, Repair and Operations!Electrical and Lighting!Ground Fault Circuit Interrupters',1607,0,5876039,NULL,NULL),(5876071,'Light Bulbs','!Maintenance, Repair and Operations!Electrical and Lighting!Light Bulbs',1608,0,5876039,NULL,NULL),(5876072,'Bulb Changing Kits','!Maintenance, Repair and Operations!Electrical and Lighting!Light Bulbs!Bulb Changing Kits',2102,0,5876071,NULL,NULL),(5876073,'Plugs and Receptacles','!Maintenance, Repair and Operations!Electrical and Lighting!Plugs and Receptacles',1609,0,5876039,NULL,NULL),(5876074,'Power Centers','!Maintenance, Repair and Operations!Electrical and Lighting!Power Centers',1610,0,5876039,NULL,NULL),(5876075,'Cord Reels','!Maintenance, Repair and Operations!Electrical and Lighting!Cord Reels',1611,0,5876039,NULL,NULL),(5876076,'Power Strips','!Maintenance, Repair and Operations!Electrical and Lighting!Power Strips',1612,0,5876039,NULL,NULL),(5876077,'Push Button Stations','!Maintenance, Repair and Operations!Electrical and Lighting!Push Button Stations',1613,0,5876039,NULL,NULL),(5876078,'Splice Kits','!Maintenance, Repair and Operations!Electrical and Lighting!Splice Kits',1614,0,5876039,NULL,NULL),(5876079,'Switches','!Maintenance, Repair and Operations!Electrical and Lighting!Switches',1615,0,5876039,NULL,NULL),(5876080,'Work Light Parts and Accessories','!Maintenance, Repair and Operations!Electrical and Lighting!Work Light Parts and Accessories',1616,0,5876039,NULL,NULL),(5876081,'Work Lights','!Maintenance, Repair and Operations!Electrical and Lighting!Work Lights',1617,0,5876039,NULL,NULL),(5876082,'Electrical Tools','!Maintenance, Repair and Operations!Electrical and Lighting!Electrical Tools',1618,0,5876039,NULL,NULL),(5876083,'Breakaways','!Maintenance, Repair and Operations!Electrical and Lighting!Electrical Tools!Breakaways',2104,0,5876082,NULL,NULL),(5876084,'Cable Puller Accessories','!Maintenance, Repair and Operations!Electrical and Lighting!Electrical Tools!Cable Puller Accessories',2105,0,5876082,NULL,NULL),(5876085,'Cable Pullers','!Maintenance, Repair and Operations!Electrical and Lighting!Electrical Tools!Cable Pullers',2106,0,5876082,NULL,NULL),(5876086,'Fish Tape Accessories','!Maintenance, Repair and Operations!Electrical and Lighting!Electrical Tools!Fish Tape Accessories',2107,0,5876082,NULL,NULL),(5876087,'Fasteners, Clamps and Straps','!Maintenance, Repair and Operations!Fasteners, Clamps and Straps',1511,0,5875888,NULL,NULL),(5876088,'Fastening Products','!Maintenance, Repair and Operations!Fasteners, Clamps and Straps!Fastening Products',1620,0,5876087,NULL,NULL),(5876089,'Clip, Ring and Pin Kits','!Maintenance, Repair and Operations!Fasteners, Clamps and Straps!Fastening Products!Clip, Ring and Pin Kits',2109,0,5876088,NULL,NULL),(5876090,'Clips, Rings and Pins','!Maintenance, Repair and Operations!Fasteners, Clamps and Straps!Fastening Products!Clips, Rings and Pins',2110,0,5876088,NULL,NULL),(5876091,'Screw Anchor Expanders','!Maintenance, Repair and Operations!Fasteners, Clamps and Straps!Fastening Products!Screw Anchor Expanders',2111,0,5876088,NULL,NULL),(5876092,'Screw Assortments','!Maintenance, Repair and Operations!Fasteners, Clamps and Straps!Fastening Products!Screw Assortments',2112,0,5876088,NULL,NULL),(5876093,'Screws','!Maintenance, Repair and Operations!Fasteners, Clamps and Straps!Fastening Products!Screws',2113,0,5876088,NULL,NULL),(5876094,'Velcro Fasteners','!Maintenance, Repair and Operations!Fasteners, Clamps and Straps!Fastening Products!Velcro Fasteners',2114,0,5876088,NULL,NULL),(5876095,'Strapping Products','!Maintenance, Repair and Operations!Fasteners, Clamps and Straps!Strapping Products',1621,0,5876087,NULL,NULL),(5876096,'Cable Tie Guns','!Maintenance, Repair and Operations!Fasteners, Clamps and Straps!Strapping Products!Cable Tie Guns',2115,0,5876095,NULL,NULL),(5876097,'Cable Ties','!Maintenance, Repair and Operations!Fasteners, Clamps and Straps!Strapping Products!Cable Ties',2116,0,5876095,NULL,NULL),(5876098,'Music Wire','!Maintenance, Repair and Operations!Fasteners, Clamps and Straps!Strapping Products!Music Wire',2117,0,5876095,NULL,NULL),(5876099,'Strap Dispensers','!Maintenance, Repair and Operations!Fasteners, Clamps and Straps!Strapping Products!Strap Dispensers',2118,0,5876095,NULL,NULL),(5876100,'Clamping Products','!Maintenance, Repair and Operations!Fasteners, Clamps and Straps!Clamping Products',1622,0,5876087,NULL,NULL),(5876101,'Bands','!Maintenance, Repair and Operations!Fasteners, Clamps and Straps!Clamping Products!Bands',2119,0,5876100,NULL,NULL),(5876102,'Strapping Products','!Maintenance, Repair and Operations!Fasteners, Clamps and Straps!Strapping Products',1623,0,5876087,NULL,NULL),(5876103,'Strap Machines','!Maintenance, Repair and Operations!Fasteners, Clamps and Straps!Strapping Products!Strap Machines',2120,0,5876102,NULL,NULL),(5876104,'Strap Tensioners and Adaptors','!Maintenance, Repair and Operations!Fasteners, Clamps and Straps!Strapping Products!Strap Tensioners and Adaptors',2121,0,5876102,NULL,NULL),(5876105,'Tie Wire','!Maintenance, Repair and Operations!Fasteners, Clamps and Straps!Strapping Products!Tie Wire',2122,0,5876102,NULL,NULL),(5876106,'Tie Wire Accessories','!Maintenance, Repair and Operations!Fasteners, Clamps and Straps!Strapping Products!Tie Wire Accessories',2123,0,5876102,NULL,NULL),(5876107,'Clamping Products','!Maintenance, Repair and Operations!Fasteners, Clamps and Straps!Clamping Products',1624,0,5876087,NULL,NULL),(5876108,'Brackets','!Maintenance, Repair and Operations!Fasteners, Clamps and Straps!Clamping Products!Brackets',2124,0,5876107,NULL,NULL),(5876109,'Buckles','!Maintenance, Repair and Operations!Fasteners, Clamps and Straps!Clamping Products!Buckles',2125,0,5876107,NULL,NULL),(5876110,'Hose Clamp Accessories','!Maintenance, Repair and Operations!Fasteners, Clamps and Straps!Clamping Products!Hose Clamp Accessories',2126,0,5876107,NULL,NULL),(5876111,'Hose Clamp Sets','!Maintenance, Repair and Operations!Fasteners, Clamps and Straps!Clamping Products!Hose Clamp Sets',2127,0,5876107,NULL,NULL),(5876112,'Hose Clamps','!Maintenance, Repair and Operations!Fasteners, Clamps and Straps!Clamping Products!Hose Clamps',2128,0,5876107,NULL,NULL),(5876113,'I.D. Tags','!Maintenance, Repair and Operations!Fasteners, Clamps and Straps!Clamping Products!I.D. Tags',2129,0,5876107,NULL,NULL),(5876114,'Fastening Products','!Maintenance, Repair and Operations!Fasteners, Clamps and Straps!Fastening Products',1625,0,5876087,NULL,NULL),(5876115,'Anchors','!Maintenance, Repair and Operations!Fasteners, Clamps and Straps!Fastening Products!Anchors',2130,0,5876114,NULL,NULL),(5876116,'Fittings and Accessories','!Maintenance, Repair and Operations!Fittings and Accessories',1512,0,5875888,NULL,NULL),(5876117,'Fitting Parts and Accessories','!Maintenance, Repair and Operations!Fittings and Accessories!Fitting Parts and Accessories',1626,0,5876116,NULL,NULL),(5876118,'Fittings','!Maintenance, Repair and Operations!Fittings and Accessories!Fittings',1627,0,5876116,NULL,NULL),(5876119,'Hand Tools','!Maintenance, Repair and Operations!Hand Tools',1513,0,5875888,NULL,NULL),(5876120,'Bending and Flaring Tools','!Maintenance, Repair and Operations!Hand Tools!Bending and Flaring Tools',1629,0,5876119,NULL,NULL),(5876121,'PVC Heater Bender Parts and Accessories','!Maintenance, Repair and Operations!Hand Tools!Bending and Flaring Tools!PVC Heater Bender Parts and Accessories',2132,0,5876120,NULL,NULL),(5876122,'Hammers, Sledges, Mallets and Axes','!Maintenance, Repair and Operations!Hand Tools!Hammers, Sledges, Mallets and Axes',1630,0,5876119,NULL,NULL),(5876123,'Axes and Hatchets','!Maintenance, Repair and Operations!Hand Tools!Hammers, Sledges, Mallets and Axes!Axes and Hatchets',2133,0,5876122,NULL,NULL),(5876124,'Hammers','!Maintenance, Repair and Operations!Hand Tools!Hammers, Sledges, Mallets and Axes!Hammers',2134,0,5876122,NULL,NULL),(5876125,'Automotive Finishing Hammers','!Maintenance, Repair and Operations!Hand Tools!Hammers, Sledges, Mallets and Axes!Hammers!Automotive Finishing Hammers',2879,0,5876124,NULL,NULL),(5876126,'Ball Pein Hammers','!Maintenance, Repair and Operations!Hand Tools!Hammers, Sledges, Mallets and Axes!Hammers!Ball Pein Hammers',2880,0,5876124,NULL,NULL),(5876127,'Bricklayer\'s Hammers','!Maintenance, Repair and Operations!Hand Tools!Hammers, Sledges, Mallets and Axes!Hammers!Bricklayer\'s Hammers',2881,0,5876124,NULL,NULL),(5876128,'Chipping Hammer Parts and Accessories','!Maintenance, Repair and Operations!Hand Tools!Hammers, Sledges, Mallets and Axes!Hammers!Chipping Hammer Parts and Accessories',2882,0,5876124,NULL,NULL),(5876129,'Chipping Hammers','!Maintenance, Repair and Operations!Hand Tools!Hammers, Sledges, Mallets and Axes!Hammers!Chipping Hammers',2883,0,5876124,NULL,NULL),(5876130,'Claw Hammers','!Maintenance, Repair and Operations!Hand Tools!Hammers, Sledges, Mallets and Axes!Hammers!Claw Hammers',2884,0,5876124,NULL,NULL),(5876131,'Cross Pein and Blacksmith Hammers','!Maintenance, Repair and Operations!Hand Tools!Hammers, Sledges, Mallets and Axes!Hammers!Cross Pein and Blacksmith Hammers',2885,0,5876124,NULL,NULL),(5876132,'Dead Blow Hammer Parts and Accessories','!Maintenance, Repair and Operations!Hand Tools!Hammers, Sledges, Mallets and Axes!Hammers!Dead Blow Hammer Parts and Accessories',2886,0,5876124,NULL,NULL),(5876133,'Dead Blow Hammers','!Maintenance, Repair and Operations!Hand Tools!Hammers, Sledges, Mallets and Axes!Hammers!Dead Blow Hammers',2887,0,5876124,NULL,NULL),(5876134,'Bending and Flaring Tools','!Maintenance, Repair and Operations!Hand Tools!Bending and Flaring Tools',1631,0,5876119,NULL,NULL),(5876135,'PVC Heater Benders','!Maintenance, Repair and Operations!Hand Tools!Bending and Flaring Tools!PVC Heater Benders',2135,0,5876134,NULL,NULL),(5876136,'Hammers, Sledges, Mallets and Axes','!Maintenance, Repair and Operations!Hand Tools!Hammers, Sledges, Mallets and Axes',1632,0,5876119,NULL,NULL),(5876137,'Hammers','!Maintenance, Repair and Operations!Hand Tools!Hammers, Sledges, Mallets and Axes!Hammers',2136,0,5876136,NULL,NULL),(5876138,'Drilling Hammers','!Maintenance, Repair and Operations!Hand Tools!Hammers, Sledges, Mallets and Axes!Hammers!Drilling Hammers',2889,0,5876137,NULL,NULL),(5876139,'Hammer Handles','!Maintenance, Repair and Operations!Hand Tools!Hammers, Sledges, Mallets and Axes!Hammers!Hammer Handles',2890,0,5876137,NULL,NULL),(5876140,'Mallets','!Maintenance, Repair and Operations!Hand Tools!Hammers, Sledges, Mallets and Axes!Hammers!Mallets',2891,0,5876137,NULL,NULL),(5876141,'Non-Sparking Hammers','!Maintenance, Repair and Operations!Hand Tools!Hammers, Sledges, Mallets and Axes!Hammers!Non-Sparking Hammers',2892,0,5876137,NULL,NULL),(5876142,'Sledge Hammers','!Maintenance, Repair and Operations!Hand Tools!Hammers, Sledges, Mallets and Axes!Hammers!Sledge Hammers',2893,0,5876137,NULL,NULL),(5876143,'Specialty Hammers','!Maintenance, Repair and Operations!Hand Tools!Hammers, Sledges, Mallets and Axes!Hammers!Specialty Hammers',2894,0,5876137,NULL,NULL),(5876144,'Mauls','!Maintenance, Repair and Operations!Hand Tools!Hammers, Sledges, Mallets and Axes!Mauls',2137,0,5876136,NULL,NULL),(5876145,'Hand Tool Organizers and Belts','!Maintenance, Repair and Operations!Hand Tools!Hand Tool Organizers and Belts',1633,0,5876119,NULL,NULL),(5876146,'Hand Tool Organizers and Belts Parts and Accessories','!Maintenance, Repair and Operations!Hand Tools!Hand Tool Organizers and Belts!Hand Tool Organizers and Belts Parts and Accessories',2138,0,5876145,NULL,NULL),(5876147,'Tool Aprons','!Maintenance, Repair and Operations!Hand Tools!Hand Tool Organizers and Belts!Tool Aprons',2139,0,5876145,NULL,NULL),(5876148,'Tool Bags','!Maintenance, Repair and Operations!Hand Tools!Hand Tool Organizers and Belts!Tool Bags',2140,0,5876145,NULL,NULL),(5876149,'Chisels, Punches and Pins','!Maintenance, Repair and Operations!Hand Tools!Chisels, Punches and Pins',1634,0,5876119,NULL,NULL),(5876150,'Chisels','!Maintenance, Repair and Operations!Hand Tools!Chisels, Punches and Pins!Chisels',2141,0,5876149,NULL,NULL),(5876151,'Hand Tool Organizers and Belts','!Maintenance, Repair and Operations!Hand Tools!Hand Tool Organizers and Belts',1635,0,5876119,NULL,NULL),(5876152,'Tool Belts and Suspenders','!Maintenance, Repair and Operations!Hand Tools!Hand Tool Organizers and Belts!Tool Belts and Suspenders',2142,0,5876151,NULL,NULL),(5876153,'Tool Pouch Combo Sets','!Maintenance, Repair and Operations!Hand Tools!Hand Tool Organizers and Belts!Tool Pouch Combo Sets',2143,0,5876151,NULL,NULL),(5876154,'Tool Pouches and Holders','!Maintenance, Repair and Operations!Hand Tools!Hand Tool Organizers and Belts!Tool Pouches and Holders',2144,0,5876151,NULL,NULL),(5876155,'Hand Tools Other','!Maintenance, Repair and Operations!Hand Tools!Hand Tools Other',1636,0,5876119,NULL,NULL),(5876156,'Handsaws and Sets','!Maintenance, Repair and Operations!Hand Tools!Handsaws and Sets',1637,0,5876119,NULL,NULL),(5876157,'Handsaw Blades','!Maintenance, Repair and Operations!Hand Tools!Handsaws and Sets!Handsaw Blades',2146,0,5876156,NULL,NULL),(5876158,'Handsaw Sets','!Maintenance, Repair and Operations!Hand Tools!Handsaws and Sets!Handsaw Sets',2147,0,5876156,NULL,NULL),(5876159,'Handsaws','!Maintenance, Repair and Operations!Hand Tools!Handsaws and Sets!Handsaws',2148,0,5876156,NULL,NULL),(5876160,'Hex, Torx and Spline Keys','!Maintenance, Repair and Operations!Hand Tools!Hex, Torx and Spline Keys',1638,0,5876119,NULL,NULL),(5876161,'Hex and Torx Key Accessories','!Maintenance, Repair and Operations!Hand Tools!Hex, Torx and Spline Keys!Hex and Torx Key Accessories',2149,0,5876160,NULL,NULL),(5876162,'Hex Key Sets','!Maintenance, Repair and Operations!Hand Tools!Hex, Torx and Spline Keys!Hex Key Sets',2150,0,5876160,NULL,NULL),(5876163,'Hex Keys','!Maintenance, Repair and Operations!Hand Tools!Hex, Torx and Spline Keys!Hex Keys',2151,0,5876160,NULL,NULL),(5876164,'Chisels, Punches and Pins','!Maintenance, Repair and Operations!Hand Tools!Chisels, Punches and Pins',1639,0,5876119,NULL,NULL),(5876165,'Pins','!Maintenance, Repair and Operations!Hand Tools!Chisels, Punches and Pins!Pins',2152,0,5876164,NULL,NULL),(5876166,'Hex, Torx and Spline Keys','!Maintenance, Repair and Operations!Hand Tools!Hex, Torx and Spline Keys',1640,0,5876119,NULL,NULL),(5876167,'Torx Key Sets','!Maintenance, Repair and Operations!Hand Tools!Hex, Torx and Spline Keys!Torx Key Sets',2153,0,5876166,NULL,NULL),(5876168,'Torx Keys','!Maintenance, Repair and Operations!Hand Tools!Hex, Torx and Spline Keys!Torx Keys',2154,0,5876166,NULL,NULL),(5876169,'Insert Bits and Holders','!Maintenance, Repair and Operations!Hand Tools!Insert Bits and Holders',1641,0,5876119,NULL,NULL),(5876170,'Ball Hex Bits','!Maintenance, Repair and Operations!Hand Tools!Insert Bits and Holders!Ball Hex Bits',2155,0,5876169,NULL,NULL),(5876171,'Bit Holders','!Maintenance, Repair and Operations!Hand Tools!Insert Bits and Holders!Bit Holders',2156,0,5876169,NULL,NULL),(5876172,'Double End Bits','!Maintenance, Repair and Operations!Hand Tools!Insert Bits and Holders!Double End Bits',2157,0,5876169,NULL,NULL),(5876173,'Hex Bits','!Maintenance, Repair and Operations!Hand Tools!Insert Bits and Holders!Hex Bits',2158,0,5876169,NULL,NULL),(5876174,'Insert Bit Sets','!Maintenance, Repair and Operations!Hand Tools!Insert Bits and Holders!Insert Bit Sets',2159,0,5876169,NULL,NULL),(5876175,'Phillips Bits','!Maintenance, Repair and Operations!Hand Tools!Insert Bits and Holders!Phillips Bits',2160,0,5876169,NULL,NULL),(5876176,'Slotted Bits','!Maintenance, Repair and Operations!Hand Tools!Insert Bits and Holders!Slotted Bits',2161,0,5876169,NULL,NULL),(5876177,'Specialty Bits','!Maintenance, Repair and Operations!Hand Tools!Insert Bits and Holders!Specialty Bits',2162,0,5876169,NULL,NULL),(5876178,'Chisels, Punches and Pins','!Maintenance, Repair and Operations!Hand Tools!Chisels, Punches and Pins',1642,0,5876119,NULL,NULL),(5876179,'Planer Accessories','!Maintenance, Repair and Operations!Hand Tools!Chisels, Punches and Pins!Planer Accessories',2163,0,5876178,NULL,NULL),(5876180,'Insert Bits and Holders','!Maintenance, Repair and Operations!Hand Tools!Insert Bits and Holders',1643,0,5876119,NULL,NULL),(5876181,'Square Bits','!Maintenance, Repair and Operations!Hand Tools!Insert Bits and Holders!Square Bits',2164,0,5876180,NULL,NULL),(5876182,'Torx Bits','!Maintenance, Repair and Operations!Hand Tools!Insert Bits and Holders!Torx Bits',2165,0,5876180,NULL,NULL),(5876183,'Knives and Multi-Purpose Tools','!Maintenance, Repair and Operations!Hand Tools!Knives and Multi-Purpose Tools',1644,0,5876119,NULL,NULL),(5876184,'Knife Blades','!Maintenance, Repair and Operations!Hand Tools!Knives and Multi-Purpose Tools!Knife Blades',2166,0,5876183,NULL,NULL),(5876185,'Knife Sets','!Maintenance, Repair and Operations!Hand Tools!Knives and Multi-Purpose Tools!Knife Sets',2167,0,5876183,NULL,NULL),(5876186,'Knives','!Maintenance, Repair and Operations!Hand Tools!Knives and Multi-Purpose Tools!Knives',2168,0,5876183,NULL,NULL),(5876187,'Multi-Purpose Tools','!Maintenance, Repair and Operations!Hand Tools!Knives and Multi-Purpose Tools!Multi-Purpose Tools',2169,0,5876183,NULL,NULL),(5876188,'Lawn and Garden Tools','!Maintenance, Repair and Operations!Hand Tools!Lawn and Garden Tools',1645,0,5876119,NULL,NULL),(5876189,'Fork and Hook Handles','!Maintenance, Repair and Operations!Hand Tools!Lawn and Garden Tools!Fork and Hook Handles',2170,0,5876188,NULL,NULL),(5876190,'Forks and Hooks','!Maintenance, Repair and Operations!Hand Tools!Lawn and Garden Tools!Forks and Hooks',2171,0,5876188,NULL,NULL),(5876191,'Hoe Handles','!Maintenance, Repair and Operations!Hand Tools!Lawn and Garden Tools!Hoe Handles',2172,0,5876188,NULL,NULL),(5876192,'Hoes','!Maintenance, Repair and Operations!Hand Tools!Lawn and Garden Tools!Hoes',2173,0,5876188,NULL,NULL),(5876193,'Chisels, Punches and Pins','!Maintenance, Repair and Operations!Hand Tools!Chisels, Punches and Pins',1646,0,5876119,NULL,NULL),(5876194,'Planers','!Maintenance, Repair and Operations!Hand Tools!Chisels, Punches and Pins!Planers',2174,0,5876193,NULL,NULL),(5876195,'Lawn and Garden Tools','!Maintenance, Repair and Operations!Hand Tools!Lawn and Garden Tools',1647,0,5876119,NULL,NULL),(5876196,'Pick Handles','!Maintenance, Repair and Operations!Hand Tools!Lawn and Garden Tools!Pick Handles',2175,0,5876195,NULL,NULL),(5876197,'Picks','!Maintenance, Repair and Operations!Hand Tools!Lawn and Garden Tools!Picks',2176,0,5876195,NULL,NULL),(5876198,'Post Hole Digger Handles','!Maintenance, Repair and Operations!Hand Tools!Lawn and Garden Tools!Post Hole Digger Handles',2177,0,5876195,NULL,NULL),(5876199,'Post Hole Diggers','!Maintenance, Repair and Operations!Hand Tools!Lawn and Garden Tools!Post Hole Diggers',2178,0,5876195,NULL,NULL),(5876200,'Probes','!Maintenance, Repair and Operations!Hand Tools!Lawn and Garden Tools!Probes',2179,0,5876195,NULL,NULL),(5876201,'Pruners, Loppers, and Shears','!Maintenance, Repair and Operations!Hand Tools!Lawn and Garden Tools!Pruners, Loppers, and Shears',2180,0,5876195,NULL,NULL),(5876202,'Rake Handles','!Maintenance, Repair and Operations!Hand Tools!Lawn and Garden Tools!Rake Handles',2181,0,5876195,NULL,NULL),(5876203,'Rakes','!Maintenance, Repair and Operations!Hand Tools!Lawn and Garden Tools!Rakes',2182,0,5876195,NULL,NULL),(5876204,'Shovel and Spade Handles','!Maintenance, Repair and Operations!Hand Tools!Lawn and Garden Tools!Shovel and Spade Handles',2183,0,5876195,NULL,NULL),(5876205,'Shovels and Spades','!Maintenance, Repair and Operations!Hand Tools!Lawn and Garden Tools!Shovels and Spades',2184,0,5876195,NULL,NULL),(5876206,'Chisels, Punches and Pins','!Maintenance, Repair and Operations!Hand Tools!Chisels, Punches and Pins',1648,0,5876119,NULL,NULL),(5876207,'Punch and Chisel Parts and Accessories','!Maintenance, Repair and Operations!Hand Tools!Chisels, Punches and Pins!Punch and Chisel Parts and Accessories',2185,0,5876206,NULL,NULL),(5876208,'Lawn and Garden Tools','!Maintenance, Repair and Operations!Hand Tools!Lawn and Garden Tools',1649,0,5876119,NULL,NULL),(5876209,'Sprayer Parts and Accessories','!Maintenance, Repair and Operations!Hand Tools!Lawn and Garden Tools!Sprayer Parts and Accessories',2186,0,5876208,NULL,NULL),(5876210,'Sprayers','!Maintenance, Repair and Operations!Hand Tools!Lawn and Garden Tools!Sprayers',2187,0,5876208,NULL,NULL),(5876211,'Water Hose Parts and Accessories','!Maintenance, Repair and Operations!Hand Tools!Lawn and Garden Tools!Water Hose Parts and Accessories',2188,0,5876208,NULL,NULL),(5876212,'Water Hoses','!Maintenance, Repair and Operations!Hand Tools!Lawn and Garden Tools!Water Hoses',2189,0,5876208,NULL,NULL),(5876213,'Weed Cutters','!Maintenance, Repair and Operations!Hand Tools!Lawn and Garden Tools!Weed Cutters',2190,0,5876208,NULL,NULL),(5876214,'Wheelbarrow Parts and Accessories','!Maintenance, Repair and Operations!Hand Tools!Lawn and Garden Tools!Wheelbarrow Parts and Accessories',2191,0,5876208,NULL,NULL),(5876215,'Wheelbarrows','!Maintenance, Repair and Operations!Hand Tools!Lawn and Garden Tools!Wheelbarrows',2192,0,5876208,NULL,NULL),(5876216,'Multi-Purpose Hand Tool Sets','!Maintenance, Repair and Operations!Hand Tools!Multi-Purpose Hand Tool Sets',1650,0,5876119,NULL,NULL),(5876217,'Multi-Purpose Tool Set Accessories','!Maintenance, Repair and Operations!Hand Tools!Multi-Purpose Hand Tool Sets!Multi-Purpose Tool Set Accessories',2193,0,5876216,NULL,NULL),(5876218,'Multi-Purpose Tool Sets','!Maintenance, Repair and Operations!Hand Tools!Multi-Purpose Hand Tool Sets!Multi-Purpose Tool Sets',2194,0,5876216,NULL,NULL),(5876219,'Pipe Threading Equipment','!Maintenance, Repair and Operations!Hand Tools!Pipe Threading Equipment',1651,0,5876119,NULL,NULL),(5876220,'Groovers','!Maintenance, Repair and Operations!Hand Tools!Pipe Threading Equipment!Groovers',2195,0,5876219,NULL,NULL),(5876221,'Chisels, Punches and Pins','!Maintenance, Repair and Operations!Hand Tools!Chisels, Punches and Pins',1652,0,5876119,NULL,NULL),(5876222,'Punch and Chisel Sets','!Maintenance, Repair and Operations!Hand Tools!Chisels, Punches and Pins!Punch and Chisel Sets',2196,0,5876221,NULL,NULL),(5876223,'Pipe Threading Equipment','!Maintenance, Repair and Operations!Hand Tools!Pipe Threading Equipment',1653,0,5876119,NULL,NULL),(5876224,'Tap and Die Sets','!Maintenance, Repair and Operations!Hand Tools!Pipe Threading Equipment!Tap and Die Sets',2197,0,5876223,NULL,NULL),(5876225,'Tap Tools','!Maintenance, Repair and Operations!Hand Tools!Pipe Threading Equipment!Tap Tools',2198,0,5876223,NULL,NULL),(5876226,'Tap Wrenches','!Maintenance, Repair and Operations!Hand Tools!Pipe Threading Equipment!Tap Wrenches',2199,0,5876223,NULL,NULL),(5876227,'Taps and Dies','!Maintenance, Repair and Operations!Hand Tools!Pipe Threading Equipment!Taps and Dies',2200,0,5876223,NULL,NULL),(5876228,'Threaders Manual','!Maintenance, Repair and Operations!Hand Tools!Pipe Threading Equipment!Threaders Manual',2201,0,5876223,NULL,NULL),(5876229,'Threaders Manual Accessories','!Maintenance, Repair and Operations!Hand Tools!Pipe Threading Equipment!Threaders Manual Accessories',2202,0,5876223,NULL,NULL),(5876230,'Threaders Powered','!Maintenance, Repair and Operations!Hand Tools!Pipe Threading Equipment!Threaders Powered',2203,0,5876223,NULL,NULL),(5876231,'Threaders Powered Accessories','!Maintenance, Repair and Operations!Hand Tools!Pipe Threading Equipment!Threaders Powered Accessories',2204,0,5876223,NULL,NULL),(5876232,'Pliers and Cutters','!Maintenance, Repair and Operations!Hand Tools!Pliers and Cutters',1654,0,5876119,NULL,NULL),(5876233,'Angled Cutters','!Maintenance, Repair and Operations!Hand Tools!Pliers and Cutters!Angled Cutters',2205,0,5876232,NULL,NULL),(5876234,'Bolt and Chain Cutter Parts and Accessories','!Maintenance, Repair and Operations!Hand Tools!Pliers and Cutters!Bolt and Chain Cutter Parts and Accessories',2206,0,5876232,NULL,NULL),(5876235,'Chisels, Punches and Pins','!Maintenance, Repair and Operations!Hand Tools!Chisels, Punches and Pins',1655,0,5876119,NULL,NULL),(5876236,'Punches','!Maintenance, Repair and Operations!Hand Tools!Chisels, Punches and Pins!Punches',2207,0,5876235,NULL,NULL),(5876237,'Pliers and Cutters','!Maintenance, Repair and Operations!Hand Tools!Pliers and Cutters',1656,0,5876119,NULL,NULL),(5876238,'Bolt and Chain Cutters','!Maintenance, Repair and Operations!Hand Tools!Pliers and Cutters!Bolt and Chain Cutters',2208,0,5876237,NULL,NULL),(5876239,'Cable and Wire Rope Cutter Parts and Accessories','!Maintenance, Repair and Operations!Hand Tools!Pliers and Cutters!Cable and Wire Rope Cutter Parts and Accessories',2209,0,5876237,NULL,NULL),(5876240,'Cable and Wire Rope Cutters','!Maintenance, Repair and Operations!Hand Tools!Pliers and Cutters!Cable and Wire Rope Cutters',2210,0,5876237,NULL,NULL),(5876241,'Diagonal Pliers','!Maintenance, Repair and Operations!Hand Tools!Pliers and Cutters!Diagonal Pliers',2211,0,5876237,NULL,NULL),(5876242,'Fencing Pliers','!Maintenance, Repair and Operations!Hand Tools!Pliers and Cutters!Fencing Pliers',2212,0,5876237,NULL,NULL),(5876243,'Lineman\'s Pliers','!Maintenance, Repair and Operations!Hand Tools!Pliers and Cutters!Lineman\'s Pliers',2213,0,5876237,NULL,NULL),(5876244,'Locking Plier Parts and Accessories','!Maintenance, Repair and Operations!Hand Tools!Pliers and Cutters!Locking Plier Parts and Accessories',2214,0,5876237,NULL,NULL),(5876245,'Locking Plier Sets','!Maintenance, Repair and Operations!Hand Tools!Pliers and Cutters!Locking Plier Sets',2215,0,5876237,NULL,NULL),(5876246,'Locking Pliers','!Maintenance, Repair and Operations!Hand Tools!Pliers and Cutters!Locking Pliers',2216,0,5876237,NULL,NULL),(5876247,'Multi-Purpose Plier Sets','!Maintenance, Repair and Operations!Hand Tools!Pliers and Cutters!Multi-Purpose Plier Sets',2217,0,5876237,NULL,NULL),(5876248,'Clamps and Vises','!Maintenance, Repair and Operations!Hand Tools!Clamps and Vises',1657,0,5876119,NULL,NULL),(5876249,'Clamps and Spreaders','!Maintenance, Repair and Operations!Hand Tools!Clamps and Vises!Clamps and Spreaders',2218,0,5876248,NULL,NULL),(5876250,'Angle and Corner Clamps','!Maintenance, Repair and Operations!Hand Tools!Clamps and Vises!Clamps and Spreaders!Angle and Corner Clamps',2896,0,5876249,NULL,NULL),(5876251,'Pliers and Cutters','!Maintenance, Repair and Operations!Hand Tools!Pliers and Cutters',1658,0,5876119,NULL,NULL),(5876252,'Needle Nose Plier Sets','!Maintenance, Repair and Operations!Hand Tools!Pliers and Cutters!Needle Nose Plier Sets',2219,0,5876251,NULL,NULL),(5876253,'Needle Nose Pliers','!Maintenance, Repair and Operations!Hand Tools!Pliers and Cutters!Needle Nose Pliers',2220,0,5876251,NULL,NULL),(5876254,'Nippers','!Maintenance, Repair and Operations!Hand Tools!Pliers and Cutters!Nippers',2221,0,5876251,NULL,NULL),(5876255,'Pipe Cutter Parts and Accessories','!Maintenance, Repair and Operations!Hand Tools!Pliers and Cutters!Pipe Cutter Parts and Accessories',2222,0,5876251,NULL,NULL),(5876256,'Pipe Cutters','!Maintenance, Repair and Operations!Hand Tools!Pliers and Cutters!Pipe Cutters',2223,0,5876251,NULL,NULL),(5876257,'Plier Accessories','!Maintenance, Repair and Operations!Hand Tools!Pliers and Cutters!Plier Accessories',2224,0,5876251,NULL,NULL),(5876258,'Retaining Ring Plier Parts and Accessories','!Maintenance, Repair and Operations!Hand Tools!Pliers and Cutters!Retaining Ring Plier Parts and Accessories',2225,0,5876251,NULL,NULL),(5876259,'Retaining Ring Plier Sets','!Maintenance, Repair and Operations!Hand Tools!Pliers and Cutters!Retaining Ring Plier Sets',2226,0,5876251,NULL,NULL),(5876260,'Retaining Ring Pliers','!Maintenance, Repair and Operations!Hand Tools!Pliers and Cutters!Retaining Ring Pliers',2227,0,5876251,NULL,NULL),(5876261,'Slip Joint Pliers','!Maintenance, Repair and Operations!Hand Tools!Pliers and Cutters!Slip Joint Pliers',2228,0,5876251,NULL,NULL),(5876262,'Bending and Flaring Tools','!Maintenance, Repair and Operations!Hand Tools!Bending and Flaring Tools',1659,0,5876119,NULL,NULL),(5876263,'Electric and Hydraulic Bender Parts and Accessories','!Maintenance, Repair and Operations!Hand Tools!Bending and Flaring Tools!Electric and Hydraulic Bender Parts and Accessories',2229,0,5876262,NULL,NULL),(5876264,'Clamps and Vises','!Maintenance, Repair and Operations!Hand Tools!Clamps and Vises',1660,0,5876119,NULL,NULL),(5876265,'Clamps and Spreaders','!Maintenance, Repair and Operations!Hand Tools!Clamps and Vises!Clamps and Spreaders',2230,0,5876264,NULL,NULL),(5876266,'Band Clamps','!Maintenance, Repair and Operations!Hand Tools!Clamps and Vises!Clamps and Spreaders!Band Clamps',2898,0,5876265,NULL,NULL),(5876267,'Pliers and Cutters','!Maintenance, Repair and Operations!Hand Tools!Pliers and Cutters',1661,0,5876119,NULL,NULL),(5876268,'Specialty Cutters','!Maintenance, Repair and Operations!Hand Tools!Pliers and Cutters!Specialty Cutters',2231,0,5876267,NULL,NULL),(5876269,'Specialty Pliers','!Maintenance, Repair and Operations!Hand Tools!Pliers and Cutters!Specialty Pliers',2232,0,5876267,NULL,NULL),(5876270,'Strap Cutters','!Maintenance, Repair and Operations!Hand Tools!Pliers and Cutters!Strap Cutters',2233,0,5876267,NULL,NULL),(5876271,'Tongue and Groove Plier Sets','!Maintenance, Repair and Operations!Hand Tools!Pliers and Cutters!Tongue and Groove Plier Sets',2234,0,5876267,NULL,NULL),(5876272,'Tongue and Groove Pliers','!Maintenance, Repair and Operations!Hand Tools!Pliers and Cutters!Tongue and Groove Pliers',2235,0,5876267,NULL,NULL),(5876273,'Tubing Cutter Parts and Accessories','!Maintenance, Repair and Operations!Hand Tools!Pliers and Cutters!Tubing Cutter Parts and Accessories',2236,0,5876267,NULL,NULL),(5876274,'Tubing Cutters','!Maintenance, Repair and Operations!Hand Tools!Pliers and Cutters!Tubing Cutters',2237,0,5876267,NULL,NULL),(5876275,'Prying Tools','!Maintenance, Repair and Operations!Hand Tools!Prying Tools',1662,0,5876119,NULL,NULL),(5876276,'Bar Sets','!Maintenance, Repair and Operations!Hand Tools!Prying Tools!Bar Sets',2238,0,5876275,NULL,NULL),(5876277,'Bars','!Maintenance, Repair and Operations!Hand Tools!Prying Tools!Bars',2239,0,5876275,NULL,NULL),(5876278,'Aligning Bars','!Maintenance, Repair and Operations!Hand Tools!Prying Tools!Bars!Aligning Bars',2900,0,5876277,NULL,NULL),(5876279,'Digging Bars','!Maintenance, Repair and Operations!Hand Tools!Prying Tools!Bars!Digging Bars',2901,0,5876277,NULL,NULL),(5876280,'Clamps and Vises','!Maintenance, Repair and Operations!Hand Tools!Clamps and Vises',1663,0,5876119,NULL,NULL),(5876281,'Clamps and Spreaders','!Maintenance, Repair and Operations!Hand Tools!Clamps and Vises!Clamps and Spreaders',2240,0,5876280,NULL,NULL),(5876282,'Bar Clamps and Spreaders','!Maintenance, Repair and Operations!Hand Tools!Clamps and Vises!Clamps and Spreaders!Bar Clamps and Spreaders',2902,0,5876281,NULL,NULL),(5876283,'Prying Tools','!Maintenance, Repair and Operations!Hand Tools!Prying Tools',1664,0,5876119,NULL,NULL),(5876284,'Bars','!Maintenance, Repair and Operations!Hand Tools!Prying Tools!Bars',2241,0,5876283,NULL,NULL),(5876285,'Handled Pry Bars','!Maintenance, Repair and Operations!Hand Tools!Prying Tools!Bars!Handled Pry Bars',2903,0,5876284,NULL,NULL),(5876286,'Nail Puller Bars','!Maintenance, Repair and Operations!Hand Tools!Prying Tools!Bars!Nail Puller Bars',2904,0,5876284,NULL,NULL),(5876287,'Pinch and Wedge Point Bars','!Maintenance, Repair and Operations!Hand Tools!Prying Tools!Bars!Pinch and Wedge Point Bars',2905,0,5876284,NULL,NULL),(5876288,'Rolling Head Bars','!Maintenance, Repair and Operations!Hand Tools!Prying Tools!Bars!Rolling Head Bars',2906,0,5876284,NULL,NULL),(5876289,'Specialty Bars','!Maintenance, Repair and Operations!Hand Tools!Prying Tools!Bars!Specialty Bars',2907,0,5876284,NULL,NULL),(5876290,'Tamping Bars','!Maintenance, Repair and Operations!Hand Tools!Prying Tools!Bars!Tamping Bars',2908,0,5876284,NULL,NULL),(5876291,'Wrecking Bars','!Maintenance, Repair and Operations!Hand Tools!Prying Tools!Bars!Wrecking Bars',2909,0,5876284,NULL,NULL),(5876292,'Pullers and Sets','!Maintenance, Repair and Operations!Hand Tools!Pullers and Sets',1665,0,5876119,NULL,NULL),(5876293,'Puller Parts and Accessories','!Maintenance, Repair and Operations!Hand Tools!Pullers and Sets!Puller Parts and Accessories',2242,0,5876292,NULL,NULL),(5876294,'Puller Sets','!Maintenance, Repair and Operations!Hand Tools!Pullers and Sets!Puller Sets',2243,0,5876292,NULL,NULL),(5876295,'Pullers','!Maintenance, Repair and Operations!Hand Tools!Pullers and Sets!Pullers',2244,0,5876292,NULL,NULL),(5876296,'Clamps and Vises','!Maintenance, Repair and Operations!Hand Tools!Clamps and Vises',1666,0,5876119,NULL,NULL),(5876297,'Clamps and Spreaders','!Maintenance, Repair and Operations!Hand Tools!Clamps and Vises!Clamps and Spreaders',2245,0,5876296,NULL,NULL),(5876298,'C-Clamp Parts and Accessories','!Maintenance, Repair and Operations!Hand Tools!Clamps and Vises!Clamps and Spreaders!C-Clamp Parts and Accessories',2911,0,5876297,NULL,NULL),(5876299,'Putty Knives and Scrapers','!Maintenance, Repair and Operations!Hand Tools!Putty Knives and Scrapers',1667,0,5876119,NULL,NULL),(5876300,'Scraper Parts and Accessories','!Maintenance, Repair and Operations!Hand Tools!Putty Knives and Scrapers!Scraper Parts and Accessories',2246,0,5876299,NULL,NULL),(5876301,'Scraper Sets','!Maintenance, Repair and Operations!Hand Tools!Putty Knives and Scrapers!Scraper Sets',2247,0,5876299,NULL,NULL),(5876302,'Scrapers and Putty Knives','!Maintenance, Repair and Operations!Hand Tools!Putty Knives and Scrapers!Scrapers and Putty Knives',2248,0,5876299,NULL,NULL),(5876303,'Reaming and Deburring Tools','!Maintenance, Repair and Operations!Hand Tools!Reaming and Deburring Tools',1668,0,5876119,NULL,NULL),(5876304,'Reamers and Deburring Tools','!Maintenance, Repair and Operations!Hand Tools!Reaming and Deburring Tools!Reamers and Deburring Tools',2249,0,5876303,NULL,NULL),(5876305,'Reaming and Deburring Tool Parts and Accessories','!Maintenance, Repair and Operations!Hand Tools!Reaming and Deburring Tools!Reaming and Deburring Tool Parts and Accessories',2250,0,5876303,NULL,NULL),(5876306,'Retrieving Tools','!Maintenance, Repair and Operations!Hand Tools!Retrieving Tools',1669,0,5876119,NULL,NULL),(5876307,'Magnetic Retrievers','!Maintenance, Repair and Operations!Hand Tools!Retrieving Tools!Magnetic Retrievers',2251,0,5876306,NULL,NULL),(5876308,'Non-Magnetic Retrievers','!Maintenance, Repair and Operations!Hand Tools!Retrieving Tools!Non-Magnetic Retrievers',2252,0,5876306,NULL,NULL),(5876309,'Screwdrivers and Nutdrivers','!Maintenance, Repair and Operations!Hand Tools!Screwdrivers and Nutdrivers',1670,0,5876119,NULL,NULL),(5876310,'Ball Screwdriver Sets','!Maintenance, Repair and Operations!Hand Tools!Screwdrivers and Nutdrivers!Ball Screwdriver Sets',2253,0,5876309,NULL,NULL),(5876311,'Ball Screwdrivers','!Maintenance, Repair and Operations!Hand Tools!Screwdrivers and Nutdrivers!Ball Screwdrivers',2254,0,5876309,NULL,NULL),(5876312,'Hex Screwdriver Sets','!Maintenance, Repair and Operations!Hand Tools!Screwdrivers and Nutdrivers!Hex Screwdriver Sets',2255,0,5876309,NULL,NULL),(5876313,'Clamps and Vises','!Maintenance, Repair and Operations!Hand Tools!Clamps and Vises',1671,0,5876119,NULL,NULL),(5876314,'Clamps and Spreaders','!Maintenance, Repair and Operations!Hand Tools!Clamps and Vises!Clamps and Spreaders',2256,0,5876313,NULL,NULL),(5876315,'C-Clamps','!Maintenance, Repair and Operations!Hand Tools!Clamps and Vises!Clamps and Spreaders!C-Clamps',2913,0,5876314,NULL,NULL),(5876316,'Screwdrivers and Nutdrivers','!Maintenance, Repair and Operations!Hand Tools!Screwdrivers and Nutdrivers',1672,0,5876119,NULL,NULL),(5876317,'Hex Screwdrivers','!Maintenance, Repair and Operations!Hand Tools!Screwdrivers and Nutdrivers!Hex Screwdrivers',2257,0,5876316,NULL,NULL),(5876318,'Insulated Nutdrivers','!Maintenance, Repair and Operations!Hand Tools!Screwdrivers and Nutdrivers!Insulated Nutdrivers',2258,0,5876316,NULL,NULL),(5876319,'Insulated Screwdriver and Nutdriver Sets','!Maintenance, Repair and Operations!Hand Tools!Screwdrivers and Nutdrivers!Insulated Screwdriver and Nutdriver Sets',2259,0,5876316,NULL,NULL),(5876320,'Insulated Screwdrivers','!Maintenance, Repair and Operations!Hand Tools!Screwdrivers and Nutdrivers!Insulated Screwdrivers',2260,0,5876316,NULL,NULL),(5876321,'Interchangable Drivers','!Maintenance, Repair and Operations!Hand Tools!Screwdrivers and Nutdrivers!Interchangable Drivers',2261,0,5876316,NULL,NULL),(5876322,'Nutdriver Sets','!Maintenance, Repair and Operations!Hand Tools!Screwdrivers and Nutdrivers!Nutdriver Sets',2262,0,5876316,NULL,NULL),(5876323,'Nutdrivers','!Maintenance, Repair and Operations!Hand Tools!Screwdrivers and Nutdrivers!Nutdrivers',2263,0,5876316,NULL,NULL),(5876324,'Offset  Screwdrivers','!Maintenance, Repair and Operations!Hand Tools!Screwdrivers and Nutdrivers!Offset  Screwdrivers',2264,0,5876316,NULL,NULL),(5876325,'Phillips Screwdrivers','!Maintenance, Repair and Operations!Hand Tools!Screwdrivers and Nutdrivers!Phillips Screwdrivers',2265,0,5876316,NULL,NULL),(5876326,'Screw Starters','!Maintenance, Repair and Operations!Hand Tools!Screwdrivers and Nutdrivers!Screw Starters',2266,0,5876316,NULL,NULL),(5876327,'Clamps and Vises','!Maintenance, Repair and Operations!Hand Tools!Clamps and Vises',1673,0,5876119,NULL,NULL),(5876328,'Clamps and Spreaders','!Maintenance, Repair and Operations!Hand Tools!Clamps and Vises!Clamps and Spreaders',2267,0,5876327,NULL,NULL),(5876329,'L-Clamps','!Maintenance, Repair and Operations!Hand Tools!Clamps and Vises!Clamps and Spreaders!L-Clamps',2915,0,5876328,NULL,NULL),(5876330,'Screwdrivers and Nutdrivers','!Maintenance, Repair and Operations!Hand Tools!Screwdrivers and Nutdrivers',1674,0,5876119,NULL,NULL),(5876331,'Screwdriver and Nutdriver Parts and Accessories','!Maintenance, Repair and Operations!Hand Tools!Screwdrivers and Nutdrivers!Screwdriver and Nutdriver Parts and Accessories',2268,0,5876330,NULL,NULL),(5876332,'Screwdriver and Nutdriver Sets','!Maintenance, Repair and Operations!Hand Tools!Screwdrivers and Nutdrivers!Screwdriver and Nutdriver Sets',2269,0,5876330,NULL,NULL),(5876333,'Screwdriver Sets','!Maintenance, Repair and Operations!Hand Tools!Screwdrivers and Nutdrivers!Screwdriver Sets',2270,0,5876330,NULL,NULL),(5876334,'Screw-Holding Screwdrivers','!Maintenance, Repair and Operations!Hand Tools!Screwdrivers and Nutdrivers!Screw-Holding Screwdrivers',2271,0,5876330,NULL,NULL),(5876335,'Slotted Screwdrivers','!Maintenance, Repair and Operations!Hand Tools!Screwdrivers and Nutdrivers!Slotted Screwdrivers',2272,0,5876330,NULL,NULL),(5876336,'Specialty Drivers','!Maintenance, Repair and Operations!Hand Tools!Screwdrivers and Nutdrivers!Specialty Drivers',2273,0,5876330,NULL,NULL),(5876337,'Square Screwdriver Sets','!Maintenance, Repair and Operations!Hand Tools!Screwdrivers and Nutdrivers!Square Screwdriver Sets',2274,0,5876330,NULL,NULL),(5876338,'Square Screwdrivers','!Maintenance, Repair and Operations!Hand Tools!Screwdrivers and Nutdrivers!Square Screwdrivers',2275,0,5876330,NULL,NULL),(5876339,'Torx Screwdriver Sets','!Maintenance, Repair and Operations!Hand Tools!Screwdrivers and Nutdrivers!Torx Screwdriver Sets',2276,0,5876330,NULL,NULL),(5876340,'Torx Screwdrivers','!Maintenance, Repair and Operations!Hand Tools!Screwdrivers and Nutdrivers!Torx Screwdrivers',2277,0,5876330,NULL,NULL),(5876341,'Clamps and Vises','!Maintenance, Repair and Operations!Hand Tools!Clamps and Vises',1675,0,5876119,NULL,NULL),(5876342,'Clamps and Spreaders','!Maintenance, Repair and Operations!Hand Tools!Clamps and Vises!Clamps and Spreaders',2278,0,5876341,NULL,NULL),(5876343,'Magnetic Clamps','!Maintenance, Repair and Operations!Hand Tools!Clamps and Vises!Clamps and Spreaders!Magnetic Clamps',2917,0,5876342,NULL,NULL),(5876344,'Scribers, Pin Vises and Pick Sets','!Maintenance, Repair and Operations!Hand Tools!Scribers, Pin Vises and Pick Sets',1676,0,5876119,NULL,NULL),(5876345,'Awls','!Maintenance, Repair and Operations!Hand Tools!Scribers, Pin Vises and Pick Sets!Awls',2279,0,5876344,NULL,NULL),(5876346,'Pick Sets','!Maintenance, Repair and Operations!Hand Tools!Scribers, Pin Vises and Pick Sets!Pick Sets',2280,0,5876344,NULL,NULL),(5876347,'Pin Vises','!Maintenance, Repair and Operations!Hand Tools!Scribers, Pin Vises and Pick Sets!Pin Vises',2281,0,5876344,NULL,NULL),(5876348,'Scriber Parts and Accessories','!Maintenance, Repair and Operations!Hand Tools!Scribers, Pin Vises and Pick Sets!Scriber Parts and Accessories',2282,0,5876344,NULL,NULL),(5876349,'Scriber Sets','!Maintenance, Repair and Operations!Hand Tools!Scribers, Pin Vises and Pick Sets!Scriber Sets',2283,0,5876344,NULL,NULL),(5876350,'Scribers','!Maintenance, Repair and Operations!Hand Tools!Scribers, Pin Vises and Pick Sets!Scribers',2284,0,5876344,NULL,NULL),(5876351,'Shears, Scissors and Snips','!Maintenance, Repair and Operations!Hand Tools!Shears, Scissors and Snips',1677,0,5876119,NULL,NULL),(5876352,'Shears and Scissors','!Maintenance, Repair and Operations!Hand Tools!Shears, Scissors and Snips!Shears and Scissors',2285,0,5876351,NULL,NULL),(5876353,'Snip Sets','!Maintenance, Repair and Operations!Hand Tools!Shears, Scissors and Snips!Snip Sets',2286,0,5876351,NULL,NULL),(5876354,'Snips','!Maintenance, Repair and Operations!Hand Tools!Shears, Scissors and Snips!Snips',2287,0,5876351,NULL,NULL),(5876355,'Sockets, Ratchets, Adaptors and Extensions','!Maintenance, Repair and Operations!Hand Tools!Sockets, Ratchets, Adaptors and Extensions',1678,0,5876119,NULL,NULL),(5876356,'Adapter and Extension Sets','!Maintenance, Repair and Operations!Hand Tools!Sockets, Ratchets, Adaptors and Extensions!Adapter and Extension Sets',2288,0,5876355,NULL,NULL),(5876357,'Clamps and Vises','!Maintenance, Repair and Operations!Hand Tools!Clamps and Vises',1679,0,5876119,NULL,NULL),(5876358,'Clamps and Spreaders','!Maintenance, Repair and Operations!Hand Tools!Clamps and Vises!Clamps and Spreaders',2289,0,5876357,NULL,NULL),(5876359,'Parallel Clamps','!Maintenance, Repair and Operations!Hand Tools!Clamps and Vises!Clamps and Spreaders!Parallel Clamps',2919,0,5876358,NULL,NULL),(5876360,'Sockets, Ratchets, Adaptors and Extensions','!Maintenance, Repair and Operations!Hand Tools!Sockets, Ratchets, Adaptors and Extensions',1680,0,5876119,NULL,NULL),(5876361,'Adapters and Extensions','!Maintenance, Repair and Operations!Hand Tools!Sockets, Ratchets, Adaptors and Extensions!Adapters and Extensions',2290,0,5876360,NULL,NULL),(5876362,'Drive Handle Parts','!Maintenance, Repair and Operations!Hand Tools!Sockets, Ratchets, Adaptors and Extensions!Drive Handle Parts',2291,0,5876360,NULL,NULL),(5876363,'Drive Handles','!Maintenance, Repair and Operations!Hand Tools!Sockets, Ratchets, Adaptors and Extensions!Drive Handles',2292,0,5876360,NULL,NULL),(5876364,'Ratchet Parts','!Maintenance, Repair and Operations!Hand Tools!Sockets, Ratchets, Adaptors and Extensions!Ratchet Parts',2293,0,5876360,NULL,NULL),(5876365,'Ratchets','!Maintenance, Repair and Operations!Hand Tools!Sockets, Ratchets, Adaptors and Extensions!Ratchets',2294,0,5876360,NULL,NULL),(5876366,'Socket Parts and Accessories','!Maintenance, Repair and Operations!Hand Tools!Sockets, Ratchets, Adaptors and Extensions!Socket Parts and Accessories',2295,0,5876360,NULL,NULL),(5876367,'Socket Sets','!Maintenance, Repair and Operations!Hand Tools!Sockets, Ratchets, Adaptors and Extensions!Socket Sets',2296,0,5876360,NULL,NULL),(5876368,'Sockets','!Maintenance, Repair and Operations!Hand Tools!Sockets, Ratchets, Adaptors and Extensions!Sockets',2297,0,5876360,NULL,NULL),(5876369,'Splitters','!Maintenance, Repair and Operations!Hand Tools!Splitters',1681,0,5876119,NULL,NULL),(5876370,'Bearing Splitters','!Maintenance, Repair and Operations!Hand Tools!Splitters!Bearing Splitters',2298,0,5876369,NULL,NULL),(5876371,'Nut Splitters','!Maintenance, Repair and Operations!Hand Tools!Splitters!Nut Splitters',2299,0,5876369,NULL,NULL),(5876372,'Clamps and Vises','!Maintenance, Repair and Operations!Hand Tools!Clamps and Vises',1682,0,5876119,NULL,NULL),(5876373,'Clamps and Spreaders','!Maintenance, Repair and Operations!Hand Tools!Clamps and Vises!Clamps and Spreaders',2300,0,5876372,NULL,NULL),(5876374,'Pipe Clamps','!Maintenance, Repair and Operations!Hand Tools!Clamps and Vises!Clamps and Spreaders!Pipe Clamps',2921,0,5876373,NULL,NULL),(5876375,'Staplers and Riveters','!Maintenance, Repair and Operations!Hand Tools!Staplers and Riveters',1683,0,5876119,NULL,NULL),(5876376,'C-Ring Tools','!Maintenance, Repair and Operations!Hand Tools!Staplers and Riveters!C-Ring Tools',2301,0,5876375,NULL,NULL),(5876377,'C-Rings','!Maintenance, Repair and Operations!Hand Tools!Staplers and Riveters!C-Rings',2302,0,5876375,NULL,NULL),(5876378,'Nailers','!Maintenance, Repair and Operations!Hand Tools!Staplers and Riveters!Nailers',2303,0,5876375,NULL,NULL),(5876379,'Riveter Sets','!Maintenance, Repair and Operations!Hand Tools!Staplers and Riveters!Riveter Sets',2304,0,5876375,NULL,NULL),(5876380,'Riveters','!Maintenance, Repair and Operations!Hand Tools!Staplers and Riveters!Riveters',2305,0,5876375,NULL,NULL),(5876381,'Rivets','!Maintenance, Repair and Operations!Hand Tools!Staplers and Riveters!Rivets',2306,0,5876375,NULL,NULL),(5876382,'Staplers','!Maintenance, Repair and Operations!Hand Tools!Staplers and Riveters!Staplers',2307,0,5876375,NULL,NULL),(5876383,'Staples and Nails','!Maintenance, Repair and Operations!Hand Tools!Staplers and Riveters!Staples and Nails',2308,0,5876375,NULL,NULL),(5876384,'Tool Sharpeners','!Maintenance, Repair and Operations!Hand Tools!Tool Sharpeners',1684,0,5876119,NULL,NULL),(5876385,'Tool Storage','!Maintenance, Repair and Operations!Hand Tools!Tool Storage',1685,0,5876119,NULL,NULL),(5876386,'Clamps and Vises','!Maintenance, Repair and Operations!Hand Tools!Clamps and Vises',1686,0,5876119,NULL,NULL),(5876387,'Clamps and Spreaders','!Maintenance, Repair and Operations!Hand Tools!Clamps and Vises!Clamps and Spreaders',2310,0,5876386,NULL,NULL),(5876388,'Specialty Clamps','!Maintenance, Repair and Operations!Hand Tools!Clamps and Vises!Clamps and Spreaders!Specialty Clamps',2923,0,5876387,NULL,NULL),(5876389,'Tool Storage','!Maintenance, Repair and Operations!Hand Tools!Tool Storage',1687,0,5876119,NULL,NULL),(5876390,'Tool Boxes','!Maintenance, Repair and Operations!Hand Tools!Tool Storage!Tool Boxes',2311,0,5876389,NULL,NULL),(5876391,'Tool Cabinets','!Maintenance, Repair and Operations!Hand Tools!Tool Storage!Tool Cabinets',2312,0,5876389,NULL,NULL),(5876392,'Tool Carts','!Maintenance, Repair and Operations!Hand Tools!Tool Storage!Tool Carts',2313,0,5876389,NULL,NULL),(5876393,'Tool Chests','!Maintenance, Repair and Operations!Hand Tools!Tool Storage!Tool Chests',2314,0,5876389,NULL,NULL),(5876394,'Tool Storage Parts and Accessories','!Maintenance, Repair and Operations!Hand Tools!Tool Storage!Tool Storage Parts and Accessories',2315,0,5876389,NULL,NULL),(5876395,'Tool Work Stations','!Maintenance, Repair and Operations!Hand Tools!Tool Storage!Tool Work Stations',2316,0,5876389,NULL,NULL),(5876396,'Wedges','!Maintenance, Repair and Operations!Hand Tools!Wedges',1688,0,5876119,NULL,NULL),(5876397,'Wrenches','!Maintenance, Repair and Operations!Hand Tools!Wrenches',1689,0,5876119,NULL,NULL),(5876398,'Adjustable Wrench Sets','!Maintenance, Repair and Operations!Hand Tools!Wrenches!Adjustable Wrench Sets',2318,0,5876397,NULL,NULL),(5876399,'Adjustable Wrenches','!Maintenance, Repair and Operations!Hand Tools!Wrenches!Adjustable Wrenches',2319,0,5876397,NULL,NULL),(5876400,'Basin Wrench Parts and Accessories','!Maintenance, Repair and Operations!Hand Tools!Wrenches!Basin Wrench Parts and Accessories',2320,0,5876397,NULL,NULL),(5876401,'Clamps and Vises','!Maintenance, Repair and Operations!Hand Tools!Clamps and Vises',1690,0,5876119,NULL,NULL),(5876402,'Clamps and Spreaders','!Maintenance, Repair and Operations!Hand Tools!Clamps and Vises!Clamps and Spreaders',2321,0,5876401,NULL,NULL),(5876403,'Spring Clamps','!Maintenance, Repair and Operations!Hand Tools!Clamps and Vises!Clamps and Spreaders!Spring Clamps',2925,0,5876402,NULL,NULL),(5876404,'Wrenches','!Maintenance, Repair and Operations!Hand Tools!Wrenches',1691,0,5876119,NULL,NULL),(5876405,'Basin Wrenches','!Maintenance, Repair and Operations!Hand Tools!Wrenches!Basin Wrenches',2322,0,5876404,NULL,NULL),(5876406,'Box End Wrench Sets','!Maintenance, Repair and Operations!Hand Tools!Wrenches!Box End Wrench Sets',2323,0,5876404,NULL,NULL),(5876407,'Box End Wrenches','!Maintenance, Repair and Operations!Hand Tools!Wrenches!Box End Wrenches',2324,0,5876404,NULL,NULL),(5876408,'Chain Tong and Strap Wrench Parts and Accessories','!Maintenance, Repair and Operations!Hand Tools!Wrenches!Chain Tong and Strap Wrench Parts and Accessories',2325,0,5876404,NULL,NULL),(5876409,'Chain Tong and Strap Wrenches','!Maintenance, Repair and Operations!Hand Tools!Wrenches!Chain Tong and Strap Wrenches',2326,0,5876404,NULL,NULL),(5876410,'Combination Wrench Parts and Accessories','!Maintenance, Repair and Operations!Hand Tools!Wrenches!Combination Wrench Parts and Accessories',2327,0,5876404,NULL,NULL),(5876411,'Combination Wrench Sets','!Maintenance, Repair and Operations!Hand Tools!Wrenches!Combination Wrench Sets',2328,0,5876404,NULL,NULL),(5876412,'Combination Wrenches','!Maintenance, Repair and Operations!Hand Tools!Wrenches!Combination Wrenches',2329,0,5876404,NULL,NULL),(5876413,'Crowfoot Wrench Sets','!Maintenance, Repair and Operations!Hand Tools!Wrenches!Crowfoot Wrench Sets',2330,0,5876404,NULL,NULL),(5876414,'Crowfoot Wrenches','!Maintenance, Repair and Operations!Hand Tools!Wrenches!Crowfoot Wrenches',2331,0,5876404,NULL,NULL),(5876415,'Bending and Flaring Tools','!Maintenance, Repair and Operations!Hand Tools!Bending and Flaring Tools',1692,0,5876119,NULL,NULL),(5876416,'Electric and Hydraulic Benders','!Maintenance, Repair and Operations!Hand Tools!Bending and Flaring Tools!Electric and Hydraulic Benders',2332,0,5876415,NULL,NULL),(5876417,'Clamps and Vises','!Maintenance, Repair and Operations!Hand Tools!Clamps and Vises',1693,0,5876119,NULL,NULL),(5876418,'Vises','!Maintenance, Repair and Operations!Hand Tools!Clamps and Vises!Vises',2333,0,5876417,NULL,NULL),(5876419,'Bench Vise Parts and Accessories','!Maintenance, Repair and Operations!Hand Tools!Clamps and Vises!Vises!Bench Vise Parts and Accessories',2927,0,5876418,NULL,NULL),(5876420,'Wrenches','!Maintenance, Repair and Operations!Hand Tools!Wrenches',1694,0,5876119,NULL,NULL),(5876421,'Filter Wrenches','!Maintenance, Repair and Operations!Hand Tools!Wrenches!Filter Wrenches',2334,0,5876420,NULL,NULL),(5876422,'Flare Nut Wrench Sets','!Maintenance, Repair and Operations!Hand Tools!Wrenches!Flare Nut Wrench Sets',2335,0,5876420,NULL,NULL),(5876423,'Flare Nut Wrenches','!Maintenance, Repair and Operations!Hand Tools!Wrenches!Flare Nut Wrenches',2336,0,5876420,NULL,NULL),(5876424,'Open End Wrench Sets','!Maintenance, Repair and Operations!Hand Tools!Wrenches!Open End Wrench Sets',2337,0,5876420,NULL,NULL),(5876425,'Open End Wrenches','!Maintenance, Repair and Operations!Hand Tools!Wrenches!Open End Wrenches',2338,0,5876420,NULL,NULL),(5876426,'Pipe Wrench Parts and Accessories','!Maintenance, Repair and Operations!Hand Tools!Wrenches!Pipe Wrench Parts and Accessories',2339,0,5876420,NULL,NULL),(5876427,'Pipe Wrenches','!Maintenance, Repair and Operations!Hand Tools!Wrenches!Pipe Wrenches',2340,0,5876420,NULL,NULL),(5876428,'Pump Wrench Sets','!Maintenance, Repair and Operations!Hand Tools!Wrenches!Pump Wrench Sets',2341,0,5876420,NULL,NULL),(5876429,'Pump Wrenches','!Maintenance, Repair and Operations!Hand Tools!Wrenches!Pump Wrenches',2342,0,5876420,NULL,NULL),(5876430,'Ratcheting Wrench Sets','!Maintenance, Repair and Operations!Hand Tools!Wrenches!Ratcheting Wrench Sets',2343,0,5876420,NULL,NULL),(5876431,'Clamps and Vises','!Maintenance, Repair and Operations!Hand Tools!Clamps and Vises',1695,0,5876119,NULL,NULL),(5876432,'Vises','!Maintenance, Repair and Operations!Hand Tools!Clamps and Vises!Vises',2344,0,5876431,NULL,NULL),(5876433,'Bench Vises','!Maintenance, Repair and Operations!Hand Tools!Clamps and Vises!Vises!Bench Vises',2929,0,5876432,NULL,NULL),(5876434,'Wrenches','!Maintenance, Repair and Operations!Hand Tools!Wrenches',1696,0,5876119,NULL,NULL),(5876435,'Ratcheting Wrench Sets','!Maintenance, Repair and Operations!Hand Tools!Wrenches!Ratcheting Wrench Sets',2345,0,5876434,NULL,NULL),(5876436,'Ratcheting Wrenches','!Maintenance, Repair and Operations!Hand Tools!Wrenches!Ratcheting Wrenches',2346,0,5876434,NULL,NULL),(5876437,'Socket Wrench Sets','!Maintenance, Repair and Operations!Hand Tools!Wrenches!Socket Wrench Sets',2347,0,5876434,NULL,NULL),(5876438,'Socket Wrenches','!Maintenance, Repair and Operations!Hand Tools!Wrenches!Socket Wrenches',2348,0,5876434,NULL,NULL),(5876439,'Spanner Wrench Sets','!Maintenance, Repair and Operations!Hand Tools!Wrenches!Spanner Wrench Sets',2349,0,5876434,NULL,NULL),(5876440,'Spanner Wrenches','!Maintenance, Repair and Operations!Hand Tools!Wrenches!Spanner Wrenches',2350,0,5876434,NULL,NULL),(5876441,'Specialty Wrenches','!Maintenance, Repair and Operations!Hand Tools!Wrenches!Specialty Wrenches',2351,0,5876434,NULL,NULL),(5876442,'Striking Wrenches','!Maintenance, Repair and Operations!Hand Tools!Wrenches!Striking Wrenches',2352,0,5876434,NULL,NULL),(5876443,'Structural Wrenches','!Maintenance, Repair and Operations!Hand Tools!Wrenches!Structural Wrenches',2353,0,5876434,NULL,NULL),(5876444,'Torque Wrench Parts and Accessories','!Maintenance, Repair and Operations!Hand Tools!Wrenches!Torque Wrench Parts and Accessories',2354,0,5876434,NULL,NULL),(5876445,'Clamps and Vises','!Maintenance, Repair and Operations!Hand Tools!Clamps and Vises',1697,0,5876119,NULL,NULL),(5876446,'Vises','!Maintenance, Repair and Operations!Hand Tools!Clamps and Vises!Vises',2355,0,5876445,NULL,NULL),(5876447,'Chain Vise Parts and Accessories','!Maintenance, Repair and Operations!Hand Tools!Clamps and Vises!Vises!Chain Vise Parts and Accessories',2931,0,5876446,NULL,NULL),(5876448,'Wrenches','!Maintenance, Repair and Operations!Hand Tools!Wrenches',1698,0,5876119,NULL,NULL),(5876449,'Torque Wrenches','!Maintenance, Repair and Operations!Hand Tools!Wrenches!Torque Wrenches',2356,0,5876448,NULL,NULL),(5876450,'Valve Wrenches','!Maintenance, Repair and Operations!Hand Tools!Wrenches!Valve Wrenches',2357,0,5876448,NULL,NULL),(5876451,'Clamps and Vises','!Maintenance, Repair and Operations!Hand Tools!Clamps and Vises',1699,0,5876119,NULL,NULL),(5876452,'Vises','!Maintenance, Repair and Operations!Hand Tools!Clamps and Vises!Vises',2358,0,5876451,NULL,NULL),(5876453,'Chain Vises','!Maintenance, Repair and Operations!Hand Tools!Clamps and Vises!Vises!Chain Vises',2933,0,5876452,NULL,NULL),(5876454,'Drill Press Vises','!Maintenance, Repair and Operations!Hand Tools!Clamps and Vises!Vises!Drill Press Vises',2934,0,5876452,NULL,NULL),(5876455,'Pipe Vises','!Maintenance, Repair and Operations!Hand Tools!Clamps and Vises!Vises!Pipe Vises',2935,0,5876452,NULL,NULL),(5876456,'Presses','!Maintenance, Repair and Operations!Hand Tools!Clamps and Vises!Vises!Presses',2936,0,5876452,NULL,NULL),(5876457,'Woodworker\'s Vises','!Maintenance, Repair and Operations!Hand Tools!Clamps and Vises!Vises!Woodworker\'s Vises',2937,0,5876452,NULL,NULL),(5876458,'Yoke Vise Parts and Accessories','!Maintenance, Repair and Operations!Hand Tools!Clamps and Vises!Vises!Yoke Vise Parts and Accessories',2938,0,5876452,NULL,NULL),(5876459,'Yoke Vises','!Maintenance, Repair and Operations!Hand Tools!Clamps and Vises!Vises!Yoke Vises',2939,0,5876452,NULL,NULL),(5876460,'Bending and Flaring Tools','!Maintenance, Repair and Operations!Hand Tools!Bending and Flaring Tools',1700,0,5876119,NULL,NULL),(5876461,'Flaring and Swaging Tool Parts and Accessories','!Maintenance, Repair and Operations!Hand Tools!Bending and Flaring Tools!Flaring and Swaging Tool Parts and Accessories',2359,0,5876460,NULL,NULL),(5876462,'Concrete and Masonry Tools','!Maintenance, Repair and Operations!Hand Tools!Concrete and Masonry Tools',1701,0,5876119,NULL,NULL),(5876463,'Concrete Broom and Brush Parts and Accessories','!Maintenance, Repair and Operations!Hand Tools!Concrete and Masonry Tools!Concrete Broom and Brush Parts and Accessories',2360,0,5876462,NULL,NULL),(5876464,'Concrete Brooms and Brushes','!Maintenance, Repair and Operations!Hand Tools!Concrete and Masonry Tools!Concrete Brooms and Brushes',2361,0,5876462,NULL,NULL),(5876465,'Concrete Tampers','!Maintenance, Repair and Operations!Hand Tools!Concrete and Masonry Tools!Concrete Tampers',2362,0,5876462,NULL,NULL),(5876466,'Edgers and Groovers','!Maintenance, Repair and Operations!Hand Tools!Concrete and Masonry Tools!Edgers and Groovers',2363,0,5876462,NULL,NULL),(5876467,'Grout Bags','!Maintenance, Repair and Operations!Hand Tools!Concrete and Masonry Tools!Grout Bags',2364,0,5876462,NULL,NULL),(5876468,'Jointers and Joint Rakers','!Maintenance, Repair and Operations!Hand Tools!Concrete and Masonry Tools!Jointers and Joint Rakers',2365,0,5876462,NULL,NULL),(5876469,'Knee Boards','!Maintenance, Repair and Operations!Hand Tools!Concrete and Masonry Tools!Knee Boards',2366,0,5876462,NULL,NULL),(5876470,'Masonry Brushes','!Maintenance, Repair and Operations!Hand Tools!Concrete and Masonry Tools!Masonry Brushes',2367,0,5876462,NULL,NULL),(5876471,'Masonry Lines','!Maintenance, Repair and Operations!Hand Tools!Concrete and Masonry Tools!Masonry Lines',2368,0,5876462,NULL,NULL),(5876472,'Placers','!Maintenance, Repair and Operations!Hand Tools!Concrete and Masonry Tools!Placers',2369,0,5876462,NULL,NULL),(5876473,'Bending and Flaring Tools','!Maintenance, Repair and Operations!Hand Tools!Bending and Flaring Tools',1702,0,5876119,NULL,NULL),(5876474,'Flaring and Swaging Tool Sets','!Maintenance, Repair and Operations!Hand Tools!Bending and Flaring Tools!Flaring and Swaging Tool Sets',2370,0,5876473,NULL,NULL),(5876475,'Concrete and Masonry Tools','!Maintenance, Repair and Operations!Hand Tools!Concrete and Masonry Tools',1703,0,5876119,NULL,NULL),(5876476,'Screeds','!Maintenance, Repair and Operations!Hand Tools!Concrete and Masonry Tools!Screeds',2371,0,5876475,NULL,NULL),(5876477,'Stretchers and Blocks','!Maintenance, Repair and Operations!Hand Tools!Concrete and Masonry Tools!Stretchers and Blocks',2372,0,5876475,NULL,NULL),(5876478,'Tongs','!Maintenance, Repair and Operations!Hand Tools!Concrete and Masonry Tools!Tongs',2373,0,5876475,NULL,NULL),(5876479,'Trowel and Float Accessories','!Maintenance, Repair and Operations!Hand Tools!Concrete and Masonry Tools!Trowel and Float Accessories',2374,0,5876475,NULL,NULL),(5876480,'Trowels and Floats','!Maintenance, Repair and Operations!Hand Tools!Concrete and Masonry Tools!Trowels and Floats',2375,0,5876475,NULL,NULL),(5876481,'Crimping and Stripping Tools','!Maintenance, Repair and Operations!Hand Tools!Crimping and Stripping Tools',1704,0,5876119,NULL,NULL),(5876482,'Crimper and Stripper Tool Parts and Accessories','!Maintenance, Repair and Operations!Hand Tools!Crimping and Stripping Tools!Crimper and Stripper Tool Parts and Accessories',2376,0,5876481,NULL,NULL),(5876483,'Crimpers and Strippers','!Maintenance, Repair and Operations!Hand Tools!Crimping and Stripping Tools!Crimpers and Strippers',2377,0,5876481,NULL,NULL),(5876484,'Pinch-Off Tools','!Maintenance, Repair and Operations!Hand Tools!Crimping and Stripping Tools!Pinch-Off Tools',2378,0,5876481,NULL,NULL),(5876485,'Drywall Tools','!Maintenance, Repair and Operations!Hand Tools!Drywall Tools',1705,0,5876119,NULL,NULL),(5876486,'Drywall Circle Cutters','!Maintenance, Repair and Operations!Hand Tools!Drywall Tools!Drywall Circle Cutters',2379,0,5876485,NULL,NULL),(5876487,'Drywall Lifts','!Maintenance, Repair and Operations!Hand Tools!Drywall Tools!Drywall Lifts',2380,0,5876485,NULL,NULL),(5876488,'Bending and Flaring Tools','!Maintenance, Repair and Operations!Hand Tools!Bending and Flaring Tools',1706,0,5876119,NULL,NULL),(5876489,'Flaring and Swaging Tools','!Maintenance, Repair and Operations!Hand Tools!Bending and Flaring Tools!Flaring and Swaging Tools',2381,0,5876488,NULL,NULL),(5876490,'Drywall Tools','!Maintenance, Repair and Operations!Hand Tools!Drywall Tools',1707,0,5876119,NULL,NULL),(5876491,'Drywall Tape Dispensers','!Maintenance, Repair and Operations!Hand Tools!Drywall Tools!Drywall Tape Dispensers',2382,0,5876490,NULL,NULL),(5876492,'Mud Pans','!Maintenance, Repair and Operations!Hand Tools!Drywall Tools!Mud Pans',2383,0,5876490,NULL,NULL),(5876493,'Texture Brushes','!Maintenance, Repair and Operations!Hand Tools!Drywall Tools!Texture Brushes',2384,0,5876490,NULL,NULL),(5876494,'Texture Sprayers','!Maintenance, Repair and Operations!Hand Tools!Drywall Tools!Texture Sprayers',2385,0,5876490,NULL,NULL),(5876495,'Extractors and Sets','!Maintenance, Repair and Operations!Hand Tools!Extractors and Sets',1708,0,5876119,NULL,NULL),(5876496,'Extractor Sets','!Maintenance, Repair and Operations!Hand Tools!Extractors and Sets!Extractor Sets',2386,0,5876495,NULL,NULL),(5876497,'Extractors','!Maintenance, Repair and Operations!Hand Tools!Extractors and Sets!Extractors',2387,0,5876495,NULL,NULL),(5876498,'Files','!Maintenance, Repair and Operations!Hand Tools!Files',1709,0,5876119,NULL,NULL),(5876499,'4-in-Hand Files','!Maintenance, Repair and Operations!Hand Tools!Files!4-in-Hand Files',2388,0,5876498,NULL,NULL),(5876500,'Aluminum Files','!Maintenance, Repair and Operations!Hand Tools!Files!Aluminum Files',2389,0,5876498,NULL,NULL),(5876501,'Auger Bit Files','!Maintenance, Repair and Operations!Hand Tools!Files!Auger Bit Files',2390,0,5876498,NULL,NULL),(5876502,'Barrette Files','!Maintenance, Repair and Operations!Hand Tools!Files!Barrette Files',2391,0,5876498,NULL,NULL),(5876503,'Bending and Flaring Tools','!Maintenance, Repair and Operations!Hand Tools!Bending and Flaring Tools',1710,0,5876119,NULL,NULL),(5876504,'Hand Bender Parts and Accessories','!Maintenance, Repair and Operations!Hand Tools!Bending and Flaring Tools!Hand Bender Parts and Accessories',2392,0,5876503,NULL,NULL),(5876505,'Files','!Maintenance, Repair and Operations!Hand Tools!Files',1711,0,5876119,NULL,NULL),(5876506,'Bodifiles','!Maintenance, Repair and Operations!Hand Tools!Files!Bodifiles',2393,0,5876505,NULL,NULL),(5876507,'Cabinet Files and Rasps','!Maintenance, Repair and Operations!Hand Tools!Files!Cabinet Files and Rasps',2394,0,5876505,NULL,NULL),(5876508,'Cantsaw Files','!Maintenance, Repair and Operations!Hand Tools!Files!Cantsaw Files',2395,0,5876505,NULL,NULL),(5876509,'Chain Saw Files','!Maintenance, Repair and Operations!Hand Tools!Files!Chain Saw Files',2396,0,5876505,NULL,NULL),(5876510,'Equaling Files','!Maintenance, Repair and Operations!Hand Tools!Files!Equaling Files',2397,0,5876505,NULL,NULL),(5876511,'Farrier Rasps','!Maintenance, Repair and Operations!Hand Tools!Files!Farrier Rasps',2398,0,5876505,NULL,NULL),(5876512,'File Accessories','!Maintenance, Repair and Operations!Hand Tools!Files!File Accessories',2399,0,5876505,NULL,NULL),(5876513,'File Cleaners','!Maintenance, Repair and Operations!Hand Tools!Files!File Accessories!File Cleaners',2941,0,5876512,NULL,NULL),(5876514,'File Handles','!Maintenance, Repair and Operations!Hand Tools!Files!File Accessories!File Handles',2942,0,5876512,NULL,NULL),(5876515,'File Sets','!Maintenance, Repair and Operations!Hand Tools!Files!File Sets',2400,0,5876505,NULL,NULL),(5876516,'Flat Files','!Maintenance, Repair and Operations!Hand Tools!Files!Flat Files',2401,0,5876505,NULL,NULL),(5876517,'Bending and Flaring Tools','!Maintenance, Repair and Operations!Hand Tools!Bending and Flaring Tools',1712,0,5876119,NULL,NULL),(5876518,'Hand Benders','!Maintenance, Repair and Operations!Hand Tools!Bending and Flaring Tools!Hand Benders',2402,0,5876517,NULL,NULL),(5876519,'Files','!Maintenance, Repair and Operations!Hand Tools!Files',1713,0,5876119,NULL,NULL),(5876520,'Flexible Files','!Maintenance, Repair and Operations!Hand Tools!Files!Flexible Files',2403,0,5876519,NULL,NULL),(5876521,'Half Round Files','!Maintenance, Repair and Operations!Hand Tools!Files!Half Round Files',2404,0,5876519,NULL,NULL),(5876522,'Hand Files','!Maintenance, Repair and Operations!Hand Tools!Files!Hand Files',2405,0,5876519,NULL,NULL),(5876523,'Handled Files','!Maintenance, Repair and Operations!Hand Tools!Files!Handled Files',2406,0,5876519,NULL,NULL),(5876524,'Knife Files','!Maintenance, Repair and Operations!Hand Tools!Files!Knife Files',2407,0,5876519,NULL,NULL),(5876525,'Lathe Files','!Maintenance, Repair and Operations!Hand Tools!Files!Lathe Files',2408,0,5876519,NULL,NULL),(5876526,'Mill Files','!Maintenance, Repair and Operations!Hand Tools!Files!Mill Files',2409,0,5876519,NULL,NULL),(5876527,'Needle Files','!Maintenance, Repair and Operations!Hand Tools!Files!Needle Files',2410,0,5876519,NULL,NULL),(5876528,'Pillar Files','!Maintenance, Repair and Operations!Hand Tools!Files!Pillar Files',2411,0,5876519,NULL,NULL),(5876529,'Riffler Files','!Maintenance, Repair and Operations!Hand Tools!Files!Riffler Files',2412,0,5876519,NULL,NULL),(5876530,'Bending and Flaring Tools','!Maintenance, Repair and Operations!Hand Tools!Bending and Flaring Tools',1714,0,5876119,NULL,NULL),(5876531,'Mechanical Benders','!Maintenance, Repair and Operations!Hand Tools!Bending and Flaring Tools!Mechanical Benders',2413,0,5876530,NULL,NULL),(5876532,'Files','!Maintenance, Repair and Operations!Hand Tools!Files',1715,0,5876119,NULL,NULL),(5876533,'Round Files','!Maintenance, Repair and Operations!Hand Tools!Files!Round Files',2414,0,5876532,NULL,NULL),(5876534,'Square Files','!Maintenance, Repair and Operations!Hand Tools!Files!Square Files',2415,0,5876532,NULL,NULL),(5876535,'Surform Files','!Maintenance, Repair and Operations!Hand Tools!Files!Surform Files',2416,0,5876532,NULL,NULL),(5876536,'Taper Files','!Maintenance, Repair and Operations!Hand Tools!Files!Taper Files',2417,0,5876532,NULL,NULL),(5876537,'Thread Restoring Files','!Maintenance, Repair and Operations!Hand Tools!Files!Thread Restoring Files',2418,0,5876532,NULL,NULL),(5876538,'Three Square Files','!Maintenance, Repair and Operations!Hand Tools!Files!Three Square Files',2419,0,5876532,NULL,NULL),(5876539,'Tungsten Point Files','!Maintenance, Repair and Operations!Hand Tools!Files!Tungsten Point Files',2420,0,5876532,NULL,NULL),(5876540,'Warding Files','!Maintenance, Repair and Operations!Hand Tools!Files!Warding Files',2421,0,5876532,NULL,NULL),(5876541,'Wood Rasps','!Maintenance, Repair and Operations!Hand Tools!Files!Wood Rasps',2422,0,5876532,NULL,NULL),(5876542,'Hammers, Sledges, Mallets and Axes','!Maintenance, Repair and Operations!Hand Tools!Hammers, Sledges, Mallets and Axes',1716,0,5876119,NULL,NULL),(5876543,'Axe and Hatchet Parts and Accessories','!Maintenance, Repair and Operations!Hand Tools!Hammers, Sledges, Mallets and Axes!Axe and Hatchet Parts and Accessories',2423,0,5876542,NULL,NULL),(5876544,'HVAC','!Maintenance, Repair and Operations!HVAC',1514,0,5875888,NULL,NULL),(5876545,'Heating Equipment','!Maintenance, Repair and Operations!HVAC!Heating Equipment',1718,0,5876544,NULL,NULL),(5876546,'Forced Air Heaters','!Maintenance, Repair and Operations!HVAC!Heating Equipment!Forced Air Heaters',2425,0,5876545,NULL,NULL),(5876547,'Heating Cables and Insulation','!Maintenance, Repair and Operations!HVAC!Heating Equipment!Heating Cables and Insulation',2426,0,5876545,NULL,NULL),(5876548,'Infrared and Radiant Heaters','!Maintenance, Repair and Operations!HVAC!Heating Equipment!Infrared and Radiant Heaters',2427,0,5876545,NULL,NULL),(5876549,'Ventilation Equipment','!Maintenance, Repair and Operations!HVAC!Ventilation Equipment',1719,0,5876544,NULL,NULL),(5876550,'Blower Accessories','!Maintenance, Repair and Operations!HVAC!Ventilation Equipment!Blower Accessories',2428,0,5876549,NULL,NULL),(5876551,'Blowers','!Maintenance, Repair and Operations!HVAC!Ventilation Equipment!Blowers',2429,0,5876549,NULL,NULL),(5876552,'Exhaust Fans','!Maintenance, Repair and Operations!HVAC!Ventilation Equipment!Exhaust Fans',2430,0,5876549,NULL,NULL),(5876553,'Air Cooling Equipment','!Maintenance, Repair and Operations!HVAC!Air Cooling Equipment',1720,0,5876544,NULL,NULL),(5876554,'Air Blowers','!Maintenance, Repair and Operations!HVAC!Air Cooling Equipment!Air Blowers',2431,0,5876553,NULL,NULL),(5876555,'Air Cooler Accessories','!Maintenance, Repair and Operations!HVAC!Air Cooling Equipment!Air Cooler Accessories',2432,0,5876553,NULL,NULL),(5876556,'Air Coolers','!Maintenance, Repair and Operations!HVAC!Air Cooling Equipment!Air Coolers',2433,0,5876553,NULL,NULL),(5876557,'Fan Accessories','!Maintenance, Repair and Operations!HVAC!Air Cooling Equipment!Fan Accessories',2434,0,5876553,NULL,NULL),(5876558,'Fans','!Maintenance, Repair and Operations!HVAC!Air Cooling Equipment!Fans',2435,0,5876553,NULL,NULL),(5876559,'Refrigeration Regulators and Charging Hoses','!Maintenance, Repair and Operations!HVAC!Air Cooling Equipment!Refrigeration Regulators and Charging Hoses',2436,0,5876553,NULL,NULL),(5876560,'Heating Equipment','!Maintenance, Repair and Operations!HVAC!Heating Equipment',1721,0,5876544,NULL,NULL),(5876561,'Convection Heaters','!Maintenance, Repair and Operations!HVAC!Heating Equipment!Convection Heaters',2437,0,5876560,NULL,NULL),(5876562,'Forced Air Heater Accessories','!Maintenance, Repair and Operations!HVAC!Heating Equipment!Forced Air Heater Accessories',2438,0,5876560,NULL,NULL),(5876563,'Maintenance and Repair Parts','!Maintenance, Repair and Operations!Maintenance and Repair Parts',1515,0,5875888,NULL,NULL),(5876564,'Drill Rods','!Maintenance, Repair and Operations!Maintenance and Repair Parts!Drill Rods',1722,0,5876563,NULL,NULL),(5876565,'Keystock Sets','!Maintenance, Repair and Operations!Maintenance and Repair Parts!Keystock Sets',1723,0,5876563,NULL,NULL),(5876566,'Keystocks','!Maintenance, Repair and Operations!Maintenance and Repair Parts!Keystocks',1724,0,5876563,NULL,NULL),(5876567,'Shim Sets','!Maintenance, Repair and Operations!Maintenance and Repair Parts!Shim Sets',1725,0,5876563,NULL,NULL),(5876568,'Shims','!Maintenance, Repair and Operations!Maintenance and Repair Parts!Shims',1726,0,5876563,NULL,NULL),(5876569,'Tool Wraps','!Maintenance, Repair and Operations!Maintenance and Repair Parts!Tool Wraps',1727,0,5876563,NULL,NULL),(5876570,'Marking Tools','!Maintenance, Repair and Operations!Marking Tools',1516,0,5875888,NULL,NULL),(5876571,'Stencils','!Maintenance, Repair and Operations!Marking Tools!Stencils',1729,0,5876570,NULL,NULL),(5876572,'Temperature Indicators','!Maintenance, Repair and Operations!Marking Tools!Temperature Indicators',1730,0,5876570,NULL,NULL),(5876573,'Chalk Reels','!Maintenance, Repair and Operations!Marking Tools!Chalk Reels',1731,0,5876570,NULL,NULL),(5876574,'Crayons','!Maintenance, Repair and Operations!Marking Tools!Crayons',1732,0,5876570,NULL,NULL),(5876575,'Labeling Tool Parts and Accessories','!Maintenance, Repair and Operations!Marking Tools!Labeling Tool Parts and Accessories',1733,0,5876570,NULL,NULL),(5876576,'Labeling Tools','!Maintenance, Repair and Operations!Marking Tools!Labeling Tools',1734,0,5876570,NULL,NULL),(5876577,'Marker Holders','!Maintenance, Repair and Operations!Marking Tools!Marker Holders',1735,0,5876570,NULL,NULL),(5876578,'Marker Sets','!Maintenance, Repair and Operations!Marking Tools!Marker Sets',1736,0,5876570,NULL,NULL),(5876579,'Markers','!Maintenance, Repair and Operations!Marking Tools!Markers',1737,0,5876570,NULL,NULL),(5876580,'Soapstone Holders','!Maintenance, Repair and Operations!Marking Tools!Soapstone Holders',1738,0,5876570,NULL,NULL),(5876581,'Material Handling','!Maintenance, Repair and Operations!Material Handling',1517,0,5875888,NULL,NULL),(5876582,'Carts and Trucks','!Maintenance, Repair and Operations!Material Handling!Carts and Trucks',1740,0,5876581,NULL,NULL),(5876583,'Pallet Trucks','!Maintenance, Repair and Operations!Material Handling!Carts and Trucks!Pallet Trucks',2440,0,5876582,NULL,NULL),(5876584,'Mats and Antislip Tape','!Maintenance, Repair and Operations!Mats and Antislip Tape',1518,0,5875888,NULL,NULL),(5876585,'Runners','!Maintenance, Repair and Operations!Mats and Antislip Tape!Runners',1741,0,5876584,NULL,NULL),(5876586,'Material Handling','!Maintenance, Repair and Operations!Material Handling',1519,0,5875888,NULL,NULL),(5876587,'Carts and Trucks','!Maintenance, Repair and Operations!Material Handling!Carts and Trucks',1742,0,5876586,NULL,NULL),(5876588,'Pipe Dollies','!Maintenance, Repair and Operations!Material Handling!Carts and Trucks!Pipe Dollies',2442,0,5876587,NULL,NULL),(5876589,'Platform Trucks','!Maintenance, Repair and Operations!Material Handling!Carts and Trucks!Platform Trucks',2443,0,5876587,NULL,NULL),(5876590,'Tilt Carts','!Maintenance, Repair and Operations!Material Handling!Carts and Trucks!Tilt Carts',2444,0,5876587,NULL,NULL),(5876591,'Truck Wheels','!Maintenance, Repair and Operations!Material Handling!Carts and Trucks!Truck Wheels',2445,0,5876587,NULL,NULL),(5876592,'Utility Carts','!Maintenance, Repair and Operations!Material Handling!Carts and Trucks!Utility Carts',2446,0,5876587,NULL,NULL),(5876593,'Chain, Cable, Rope and Accessories','!Maintenance, Repair and Operations!Material Handling!Chain, Cable, Rope and Accessories',1743,0,5876586,NULL,NULL),(5876594,'Block Sets','!Maintenance, Repair and Operations!Material Handling!Chain, Cable, Rope and Accessories!Block Sets',2447,0,5876593,NULL,NULL),(5876595,'Blocks','!Maintenance, Repair and Operations!Material Handling!Chain, Cable, Rope and Accessories!Blocks',2448,0,5876593,NULL,NULL),(5876596,'Chains','!Maintenance, Repair and Operations!Material Handling!Chain, Cable, Rope and Accessories!Chains',2449,0,5876593,NULL,NULL),(5876597,'Eye Bolts and Nuts','!Maintenance, Repair and Operations!Material Handling!Chain, Cable, Rope and Accessories!Eye Bolts and Nuts',2450,0,5876593,NULL,NULL),(5876598,'Carts and Trucks','!Maintenance, Repair and Operations!Material Handling!Carts and Trucks',1744,0,5876586,NULL,NULL),(5876599,'Appliance Trucks','!Maintenance, Repair and Operations!Material Handling!Carts and Trucks!Appliance Trucks',2451,0,5876598,NULL,NULL),(5876600,'Chain, Cable, Rope and Accessories','!Maintenance, Repair and Operations!Material Handling!Chain, Cable, Rope and Accessories',1745,0,5876586,NULL,NULL),(5876601,'Hooks','!Maintenance, Repair and Operations!Material Handling!Chain, Cable, Rope and Accessories!Hooks',2452,0,5876600,NULL,NULL),(5876602,'Barrel Hooks','!Maintenance, Repair and Operations!Material Handling!Chain, Cable, Rope and Accessories!Hooks!Barrel Hooks',2944,0,5876601,NULL,NULL),(5876603,'Cam-Lok Hooks','!Maintenance, Repair and Operations!Material Handling!Chain, Cable, Rope and Accessories!Hooks!Cam-Lok Hooks',2945,0,5876601,NULL,NULL),(5876604,'Foundry Hooks','!Maintenance, Repair and Operations!Material Handling!Chain, Cable, Rope and Accessories!Hooks!Foundry Hooks',2946,0,5876601,NULL,NULL),(5876605,'Grab Hooks','!Maintenance, Repair and Operations!Material Handling!Chain, Cable, Rope and Accessories!Hooks!Grab Hooks',2947,0,5876601,NULL,NULL),(5876606,'Hoist Hooks','!Maintenance, Repair and Operations!Material Handling!Chain, Cable, Rope and Accessories!Hooks!Hoist Hooks',2948,0,5876601,NULL,NULL),(5876607,'J-Hooks','!Maintenance, Repair and Operations!Material Handling!Chain, Cable, Rope and Accessories!Hooks!J-Hooks',2949,0,5876601,NULL,NULL),(5876608,'Latch Kit Assemblies','!Maintenance, Repair and Operations!Material Handling!Chain, Cable, Rope and Accessories!Hooks!Latch Kit Assemblies',2950,0,5876601,NULL,NULL),(5876609,'S-Hooks','!Maintenance, Repair and Operations!Material Handling!Chain, Cable, Rope and Accessories!Hooks!S-Hooks',2951,0,5876601,NULL,NULL),(5876610,'Sling Hooks','!Maintenance, Repair and Operations!Material Handling!Chain, Cable, Rope and Accessories!Hooks!Sling Hooks',2952,0,5876601,NULL,NULL),(5876611,'Slip Hooks','!Maintenance, Repair and Operations!Material Handling!Chain, Cable, Rope and Accessories!Hooks!Slip Hooks',2953,0,5876601,NULL,NULL),(5876612,'Carts and Trucks','!Maintenance, Repair and Operations!Material Handling!Carts and Trucks',1746,0,5876586,NULL,NULL),(5876613,'Carts','!Maintenance, Repair and Operations!Material Handling!Carts and Trucks!Carts',2453,0,5876612,NULL,NULL),(5876614,'Chain, Cable, Rope and Accessories','!Maintenance, Repair and Operations!Material Handling!Chain, Cable, Rope and Accessories',1747,0,5876586,NULL,NULL),(5876615,'Hooks','!Maintenance, Repair and Operations!Material Handling!Chain, Cable, Rope and Accessories!Hooks',2454,0,5876614,NULL,NULL),(5876616,'Snap Hooks','!Maintenance, Repair and Operations!Material Handling!Chain, Cable, Rope and Accessories!Hooks!Snap Hooks',2955,0,5876615,NULL,NULL),(5876617,'Lifting Clamp Parts and Accessories','!Maintenance, Repair and Operations!Material Handling!Chain, Cable, Rope and Accessories!Lifting Clamp Parts and Accessories',2455,0,5876614,NULL,NULL),(5876618,'Lifting Clamps','!Maintenance, Repair and Operations!Material Handling!Chain, Cable, Rope and Accessories!Lifting Clamps',2456,0,5876614,NULL,NULL),(5876619,'Links','!Maintenance, Repair and Operations!Material Handling!Chain, Cable, Rope and Accessories!Links',2457,0,5876614,NULL,NULL),(5876620,'Load Binders','!Maintenance, Repair and Operations!Material Handling!Chain, Cable, Rope and Accessories!Load Binders',2458,0,5876614,NULL,NULL),(5876621,'Pulleys and Sheaves','!Maintenance, Repair and Operations!Material Handling!Chain, Cable, Rope and Accessories!Pulleys and Sheaves',2459,0,5876614,NULL,NULL),(5876622,'Rope, Line and Twine','!Maintenance, Repair and Operations!Material Handling!Chain, Cable, Rope and Accessories!Rope, Line and Twine',2460,0,5876614,NULL,NULL),(5876623,'Shackles','!Maintenance, Repair and Operations!Material Handling!Chain, Cable, Rope and Accessories!Shackles',2461,0,5876614,NULL,NULL),(5876624,'Slings','!Maintenance, Repair and Operations!Material Handling!Chain, Cable, Rope and Accessories!Slings',2462,0,5876614,NULL,NULL),(5876625,'Straps and Bungee Cords','!Maintenance, Repair and Operations!Material Handling!Chain, Cable, Rope and Accessories!Straps and Bungee Cords',2463,0,5876614,NULL,NULL),(5876626,'Carts and Trucks','!Maintenance, Repair and Operations!Material Handling!Carts and Trucks',1748,0,5876586,NULL,NULL),(5876627,'Cryogenic Dollies','!Maintenance, Repair and Operations!Material Handling!Carts and Trucks!Cryogenic Dollies',2464,0,5876626,NULL,NULL),(5876628,'Chain, Cable, Rope and Accessories','!Maintenance, Repair and Operations!Material Handling!Chain, Cable, Rope and Accessories',1749,0,5876586,NULL,NULL),(5876629,'Swivels','!Maintenance, Repair and Operations!Material Handling!Chain, Cable, Rope and Accessories!Swivels',2465,0,5876628,NULL,NULL),(5876630,'Turnbuckles','!Maintenance, Repair and Operations!Material Handling!Chain, Cable, Rope and Accessories!Turnbuckles',2466,0,5876628,NULL,NULL),(5876631,'Wire Rope Attachments','!Maintenance, Repair and Operations!Material Handling!Chain, Cable, Rope and Accessories!Wire Rope Attachments',2467,0,5876628,NULL,NULL),(5876632,'Wire Ropes','!Maintenance, Repair and Operations!Material Handling!Chain, Cable, Rope and Accessories!Wire Ropes',2468,0,5876628,NULL,NULL),(5876633,'Hoists and Winches','!Maintenance, Repair and Operations!Material Handling!Hoists and Winches',1750,0,5876586,NULL,NULL),(5876634,'Hoist and Puller Parts and Accessories','!Maintenance, Repair and Operations!Material Handling!Hoists and Winches!Hoist and Puller Parts and Accessories',2469,0,5876633,NULL,NULL),(5876635,'Hoists and Pullers','!Maintenance, Repair and Operations!Material Handling!Hoists and Winches!Hoists and Pullers',2470,0,5876633,NULL,NULL),(5876636,'Stretchers','!Maintenance, Repair and Operations!Material Handling!Hoists and Winches!Stretchers',2471,0,5876633,NULL,NULL),(5876637,'Trolleys','!Maintenance, Repair and Operations!Material Handling!Hoists and Winches!Trolleys',2472,0,5876633,NULL,NULL),(5876638,'Winch Parts and Accessories','!Maintenance, Repair and Operations!Material Handling!Hoists and Winches!Winch Parts and Accessories',2473,0,5876633,NULL,NULL),(5876639,'Winches','!Maintenance, Repair and Operations!Material Handling!Hoists and Winches!Winches',2474,0,5876633,NULL,NULL),(5876640,'Carts and Trucks','!Maintenance, Repair and Operations!Material Handling!Carts and Trucks',1751,0,5876586,NULL,NULL),(5876641,'Cylinder Cart Parts and Accessories','!Maintenance, Repair and Operations!Material Handling!Carts and Trucks!Cylinder Cart Parts and Accessories',2475,0,5876640,NULL,NULL),(5876642,'Jacks, Lifts and Hydraulics','!Maintenance, Repair and Operations!Material Handling!Jacks, Lifts and Hydraulics',1752,0,5876586,NULL,NULL),(5876643,'Hydraulic Oils','!Maintenance, Repair and Operations!Material Handling!Jacks, Lifts and Hydraulics!Hydraulic Oils',2476,0,5876642,NULL,NULL),(5876644,'Hydraulic Tools','!Maintenance, Repair and Operations!Material Handling!Jacks, Lifts and Hydraulics!Hydraulic Tools',2477,0,5876642,NULL,NULL),(5876645,'Jacks','!Maintenance, Repair and Operations!Material Handling!Jacks, Lifts and Hydraulics!Jacks',2478,0,5876642,NULL,NULL),(5876646,'Lifts','!Maintenance, Repair and Operations!Material Handling!Jacks, Lifts and Hydraulics!Lifts',2479,0,5876642,NULL,NULL),(5876647,'Ladders, Platforms and Scaffolding','!Maintenance, Repair and Operations!Material Handling!Ladders, Platforms and Scaffolding',1753,0,5876586,NULL,NULL),(5876648,'Climber Parts and Accessories','!Maintenance, Repair and Operations!Material Handling!Ladders, Platforms and Scaffolding!Climber Parts and Accessories',2480,0,5876647,NULL,NULL),(5876649,'Climbers','!Maintenance, Repair and Operations!Material Handling!Ladders, Platforms and Scaffolding!Climbers',2481,0,5876647,NULL,NULL),(5876650,'Ladder Parts and Accessories','!Maintenance, Repair and Operations!Material Handling!Ladders, Platforms and Scaffolding!Ladder Parts and Accessories',2482,0,5876647,NULL,NULL),(5876651,'Ladder Racks','!Maintenance, Repair and Operations!Material Handling!Ladders, Platforms and Scaffolding!Ladder Racks',2483,0,5876647,NULL,NULL),(5876652,'Ladders','!Maintenance, Repair and Operations!Material Handling!Ladders, Platforms and Scaffolding!Ladders',2484,0,5876647,NULL,NULL),(5876653,'Scaffolds','!Maintenance, Repair and Operations!Material Handling!Ladders, Platforms and Scaffolding!Scaffolds',2485,0,5876647,NULL,NULL),(5876654,'Carts and Trucks','!Maintenance, Repair and Operations!Material Handling!Carts and Trucks',1754,0,5876586,NULL,NULL),(5876655,'Cylinder Carts and Trucks','!Maintenance, Repair and Operations!Material Handling!Carts and Trucks!Cylinder Carts and Trucks',2486,0,5876654,NULL,NULL),(5876656,'Ladders, Platforms and Scaffolding','!Maintenance, Repair and Operations!Material Handling!Ladders, Platforms and Scaffolding',1755,0,5876586,NULL,NULL),(5876657,'Scaffolds Parts and Accessories','!Maintenance, Repair and Operations!Material Handling!Ladders, Platforms and Scaffolding!Scaffolds Parts and Accessories',2487,0,5876656,NULL,NULL),(5876658,'Marking Tools','!Maintenance, Repair and Operations!Material Handling!Marking Tools',1756,0,5876586,NULL,NULL),(5876659,'Chalk','!Maintenance, Repair and Operations!Material Handling!Marking Tools!Chalk',2488,0,5876658,NULL,NULL),(5876660,'Chalk Reel Line and Refills','!Maintenance, Repair and Operations!Material Handling!Marking Tools!Chalk Reel Line and Refills',2489,0,5876658,NULL,NULL),(5876661,'Chalk Reel Sets','!Maintenance, Repair and Operations!Material Handling!Marking Tools!Chalk Reel Sets',2490,0,5876658,NULL,NULL),(5876662,'Chalk Reels','!Maintenance, Repair and Operations!Material Handling!Marking Tools!Chalk Reels',2491,0,5876658,NULL,NULL),(5876663,'Crayons','!Maintenance, Repair and Operations!Material Handling!Marking Tools!Crayons',2492,0,5876658,NULL,NULL),(5876664,'Labeling Tool Parts and Accessories','!Maintenance, Repair and Operations!Material Handling!Marking Tools!Labeling Tool Parts and Accessories',2493,0,5876658,NULL,NULL),(5876665,'Labeling Tools','!Maintenance, Repair and Operations!Material Handling!Marking Tools!Labeling Tools',2494,0,5876658,NULL,NULL),(5876666,'Marker Holders','!Maintenance, Repair and Operations!Material Handling!Marking Tools!Marker Holders',2495,0,5876658,NULL,NULL),(5876667,'Marker Parts and Accessories','!Maintenance, Repair and Operations!Material Handling!Marking Tools!Marker Parts and Accessories',2496,0,5876658,NULL,NULL),(5876668,'Carts and Trucks','!Maintenance, Repair and Operations!Material Handling!Carts and Trucks',1757,0,5876586,NULL,NULL),(5876669,'Drum Dollies and Racks','!Maintenance, Repair and Operations!Material Handling!Carts and Trucks!Drum Dollies and Racks',2497,0,5876668,NULL,NULL),(5876670,'Marking Tools','!Maintenance, Repair and Operations!Material Handling!Marking Tools',1758,0,5876586,NULL,NULL),(5876671,'Marker Sets','!Maintenance, Repair and Operations!Material Handling!Marking Tools!Marker Sets',2498,0,5876670,NULL,NULL),(5876672,'Markers','!Maintenance, Repair and Operations!Material Handling!Marking Tools!Markers',2499,0,5876670,NULL,NULL),(5876673,'Pencil Accessories','!Maintenance, Repair and Operations!Material Handling!Marking Tools!Pencil Accessories',2500,0,5876670,NULL,NULL),(5876674,'Pencils','!Maintenance, Repair and Operations!Material Handling!Marking Tools!Pencils',2501,0,5876670,NULL,NULL),(5876675,'Soapstone Holders','!Maintenance, Repair and Operations!Material Handling!Marking Tools!Soapstone Holders',2502,0,5876670,NULL,NULL),(5876676,'Soapstones','!Maintenance, Repair and Operations!Material Handling!Marking Tools!Soapstones',2503,0,5876670,NULL,NULL),(5876677,'Stamp Sets','!Maintenance, Repair and Operations!Material Handling!Marking Tools!Stamp Sets',2504,0,5876670,NULL,NULL),(5876678,'Stamps','!Maintenance, Repair and Operations!Material Handling!Marking Tools!Stamps',2505,0,5876670,NULL,NULL),(5876679,'Stencil Accessories','!Maintenance, Repair and Operations!Material Handling!Marking Tools!Stencil Accessories',2506,0,5876670,NULL,NULL),(5876680,'Stencil Sets','!Maintenance, Repair and Operations!Material Handling!Marking Tools!Stencil Sets',2507,0,5876670,NULL,NULL),(5876681,'Carts and Trucks','!Maintenance, Repair and Operations!Material Handling!Carts and Trucks',1759,0,5876586,NULL,NULL),(5876682,'Furniture Dollies','!Maintenance, Repair and Operations!Material Handling!Carts and Trucks!Furniture Dollies',2508,0,5876681,NULL,NULL),(5876683,'Marking Tools','!Maintenance, Repair and Operations!Material Handling!Marking Tools',1760,0,5876586,NULL,NULL),(5876684,'Stencils','!Maintenance, Repair and Operations!Material Handling!Marking Tools!Stencils',2509,0,5876683,NULL,NULL),(5876685,'Tags','!Maintenance, Repair and Operations!Material Handling!Marking Tools!Tags',2510,0,5876683,NULL,NULL),(5876686,'Temperature Indicators','!Maintenance, Repair and Operations!Material Handling!Marking Tools!Temperature Indicators',2511,0,5876683,NULL,NULL),(5876687,'Rollers','!Maintenance, Repair and Operations!Material Handling!Rollers',1761,0,5876586,NULL,NULL),(5876688,'Stands and Ramps','!Maintenance, Repair and Operations!Material Handling!Stands and Ramps',1762,0,5876586,NULL,NULL),(5876689,'Storage','!Maintenance, Repair and Operations!Material Handling!Storage',1763,0,5876586,NULL,NULL),(5876690,'Jobsite Boxes','!Maintenance, Repair and Operations!Material Handling!Storage!Jobsite Boxes',2513,0,5876689,NULL,NULL),(5876691,'Jobsite Cabinets','!Maintenance, Repair and Operations!Material Handling!Storage!Jobsite Cabinets',2514,0,5876689,NULL,NULL),(5876692,'Liquid Transfer Tanks','!Maintenance, Repair and Operations!Material Handling!Storage!Liquid Transfer Tanks',2515,0,5876689,NULL,NULL),(5876693,'Small Parts Storage','!Maintenance, Repair and Operations!Material Handling!Storage!Small Parts Storage',2516,0,5876689,NULL,NULL),(5876694,'Storage Cabinets','!Maintenance, Repair and Operations!Material Handling!Storage!Storage Cabinets',2517,0,5876689,NULL,NULL),(5876695,'Carts and Trucks','!Maintenance, Repair and Operations!Material Handling!Carts and Trucks',1764,0,5876586,NULL,NULL),(5876696,'Hand Trucks','!Maintenance, Repair and Operations!Material Handling!Carts and Trucks!Hand Trucks',2518,0,5876695,NULL,NULL),(5876697,'Storage','!Maintenance, Repair and Operations!Material Handling!Storage',1765,0,5876586,NULL,NULL),(5876698,'Storage Containers','!Maintenance, Repair and Operations!Material Handling!Storage!Storage Containers',2519,0,5876697,NULL,NULL),(5876699,'Storage Sheds','!Maintenance, Repair and Operations!Material Handling!Storage!Storage Sheds',2520,0,5876697,NULL,NULL),(5876700,'Truck Boxes','!Maintenance, Repair and Operations!Material Handling!Storage!Truck Boxes',2521,0,5876697,NULL,NULL),(5876701,'Stretch Wraps','!Maintenance, Repair and Operations!Material Handling!Stretch Wraps',1766,0,5876586,NULL,NULL),(5876702,'Trailer Hitch Accessories','!Maintenance, Repair and Operations!Material Handling!Trailer Hitch Accessories',1767,0,5876586,NULL,NULL),(5876703,'Trailer Hitch Balls, Couplers and Locks','!Maintenance, Repair and Operations!Material Handling!Trailer Hitch Accessories!Trailer Hitch Balls, Couplers and Locks',2523,0,5876702,NULL,NULL),(5876704,'Trailer Jacks','!Maintenance, Repair and Operations!Material Handling!Trailer Hitch Accessories!Trailer Jacks',2524,0,5876702,NULL,NULL),(5876705,'Wheels and Casters','!Maintenance, Repair and Operations!Material Handling!Wheels and Casters',1768,0,5876586,NULL,NULL),(5876706,'Mats and Antislip Tape','!Maintenance, Repair and Operations!Mats and Antislip Tape',1520,0,5875888,NULL,NULL),(5876707,'Antifatigue Antistatic Mats','!Maintenance, Repair and Operations!Mats and Antislip Tape!Antifatigue Antistatic Mats',1769,0,5876706,NULL,NULL),(5876708,'Disposable Renewable Mats','!Maintenance, Repair and Operations!Mats and Antislip Tape!Disposable Renewable Mats',1770,0,5876706,NULL,NULL),(5876709,'Entrance Mats','!Maintenance, Repair and Operations!Mats and Antislip Tape!Entrance Mats',1771,0,5876706,NULL,NULL),(5876710,'Oilfield','!Maintenance, Repair and Operations!Oilfield',1521,0,5875888,NULL,NULL),(5876711,'Packing Hooks','!Maintenance, Repair and Operations!Oilfield!Packing Hooks',1773,0,5876710,NULL,NULL),(5876712,'Pipe Pigs','!Maintenance, Repair and Operations!Oilfield!Pipe Pigs',1774,0,5876710,NULL,NULL),(5876713,'Rig Wash and Creme Beads','!Maintenance, Repair and Operations!Oilfield!Rig Wash and Creme Beads',1775,0,5876710,NULL,NULL),(5876714,'Roller Chain','!Maintenance, Repair and Operations!Oilfield!Roller Chain',1776,0,5876710,NULL,NULL),(5876715,'Safety Pins','!Maintenance, Repair and Operations!Oilfield!Safety Pins',1777,0,5876710,NULL,NULL),(5876716,'Sample Bags','!Maintenance, Repair and Operations!Oilfield!Sample Bags',1778,0,5876710,NULL,NULL),(5876717,'Sand Strainers','!Maintenance, Repair and Operations!Oilfield!Sand Strainers',1779,0,5876710,NULL,NULL),(5876718,'Spark Plugs','!Maintenance, Repair and Operations!Oilfield!Spark Plugs',1780,0,5876710,NULL,NULL),(5876719,'Spinning and Cathead Chains','!Maintenance, Repair and Operations!Oilfield!Spinning and Cathead Chains',1781,0,5876710,NULL,NULL),(5876720,'Other','!Maintenance, Repair and Operations!Other',1522,0,5875888,NULL,NULL),(5876721,'Oilfield','!Maintenance, Repair and Operations!Oilfield',1523,0,5875888,NULL,NULL),(5876722,'Babbit and Ladles','!Maintenance, Repair and Operations!Oilfield!Babbit and Ladles',1783,0,5876721,NULL,NULL),(5876723,'Babin Bilge Pumps','!Maintenance, Repair and Operations!Oilfield!Babin Bilge Pumps',1784,0,5876721,NULL,NULL),(5876724,'Chemical Sticks','!Maintenance, Repair and Operations!Oilfield!Chemical Sticks',1785,0,5876721,NULL,NULL),(5876725,'Coco Mats','!Maintenance, Repair and Operations!Oilfield!Coco Mats',1786,0,5876721,NULL,NULL),(5876726,'Escape Seats','!Maintenance, Repair and Operations!Oilfield!Escape Seats',1787,0,5876721,NULL,NULL),(5876727,'Exhaust Caps','!Maintenance, Repair and Operations!Oilfield!Exhaust Caps',1788,0,5876721,NULL,NULL),(5876728,'Fullers Earth','!Maintenance, Repair and Operations!Oilfield!Fullers Earth',1789,0,5876721,NULL,NULL),(5876729,'Gage Poles','!Maintenance, Repair and Operations!Oilfield!Gage Poles',1790,0,5876721,NULL,NULL),(5876730,'Plumbing','!Maintenance, Repair and Operations!Plumbing',1524,0,5875888,NULL,NULL),(5876731,'Drain Cleaning and Inspection Equipment','!Maintenance, Repair and Operations!Plumbing!Drain Cleaning and Inspection Equipment',1792,0,5876730,NULL,NULL),(5876732,'Drain Cleaners','!Maintenance, Repair and Operations!Plumbing!Drain Cleaning and Inspection Equipment!Drain Cleaners',2526,0,5876731,NULL,NULL),(5876733,'Drain Cleaning and Inspection Equipment Parts and Accessories','!Maintenance, Repair and Operations!Plumbing!Drain Cleaning and Inspection Equipment!Drain Cleaning and Inspection Equipment Parts and Accessories',2527,0,5876731,NULL,NULL),(5876734,'See Snakes, Transmitters, and Locators','!Maintenance, Repair and Operations!Plumbing!Drain Cleaning and Inspection Equipment!See Snakes, Transmitters, and Locators',2528,0,5876731,NULL,NULL),(5876735,'Plumbing Equipment','!Maintenance, Repair and Operations!Plumbing!Plumbing Equipment',1793,0,5876730,NULL,NULL),(5876736,'Drain Cleaning and Inspection Equipment','!Maintenance, Repair and Operations!Plumbing!Plumbing Equipment!Drain Cleaning and Inspection Equipment',2529,0,5876735,NULL,NULL),(5876737,'Drain Cleaners','!Maintenance, Repair and Operations!Plumbing!Plumbing Equipment!Drain Cleaning and Inspection Equipment!Drain Cleaners',2957,0,5876736,NULL,NULL),(5876738,'Drain Cleaning and Inspection Equipment Parts and Accessories','!Maintenance, Repair and Operations!Plumbing!Plumbing Equipment!Drain Cleaning and Inspection Equipment!Drain Cleaning and Inspection Equipment Parts and Accessories',2958,0,5876736,NULL,NULL),(5876739,'See Snakes, Transmitters, and Locators','!Maintenance, Repair and Operations!Plumbing!Plumbing Equipment!Drain Cleaning and Inspection Equipment!See Snakes, Transmitters, and Locators',2959,0,5876736,NULL,NULL),(5876740,'Filtration','!Maintenance, Repair and Operations!Plumbing!Plumbing Equipment!Filtration',2530,0,5876735,NULL,NULL),(5876741,'Filters and Strainers','!Maintenance, Repair and Operations!Plumbing!Plumbing Equipment!Filtration!Filters and Strainers',2960,0,5876740,NULL,NULL),(5876742,'Pipe Freezing Equipment','!Maintenance, Repair and Operations!Plumbing!Plumbing Equipment!Pipe Freezing Equipment',2531,0,5876735,NULL,NULL),(5876743,'Pipe Freezers','!Maintenance, Repair and Operations!Plumbing!Plumbing Equipment!Pipe Freezing Equipment!Pipe Freezers',2961,0,5876742,NULL,NULL),(5876744,'Pneumatics','!Maintenance, Repair and Operations!Pneumatics',1525,0,5875888,NULL,NULL),(5876745,'Drilling and Fastening Tools','!Maintenance, Repair and Operations!Pneumatics!Drilling and Fastening Tools',1795,0,5876744,NULL,NULL),(5876746,'Pneumatic Screwdrivers','!Maintenance, Repair and Operations!Pneumatics!Drilling and Fastening Tools!Pneumatic Screwdrivers',2533,0,5876745,NULL,NULL),(5876747,'Pneumatic Stapler Parts and Accessories','!Maintenance, Repair and Operations!Pneumatics!Drilling and Fastening Tools!Pneumatic Stapler Parts and Accessories',2534,0,5876745,NULL,NULL),(5876748,'Finishing Tools','!Maintenance, Repair and Operations!Pneumatics!Finishing Tools',1796,0,5876744,NULL,NULL),(5876749,'Pneumatic Angle Grinders','!Maintenance, Repair and Operations!Pneumatics!Finishing Tools!Pneumatic Angle Grinders',2535,0,5876748,NULL,NULL),(5876750,'Pneumatic Belt Sanders','!Maintenance, Repair and Operations!Pneumatics!Finishing Tools!Pneumatic Belt Sanders',2536,0,5876748,NULL,NULL),(5876751,'Pneumatic Die Grinders','!Maintenance, Repair and Operations!Pneumatics!Finishing Tools!Pneumatic Die Grinders',2537,0,5876748,NULL,NULL),(5876752,'Pneumatic Engravers','!Maintenance, Repair and Operations!Pneumatics!Finishing Tools!Pneumatic Engravers',2538,0,5876748,NULL,NULL),(5876753,'Pneumatic Files','!Maintenance, Repair and Operations!Pneumatics!Finishing Tools!Pneumatic Files',2539,0,5876748,NULL,NULL),(5876754,'Pneumatic Pad Sanders','!Maintenance, Repair and Operations!Pneumatics!Finishing Tools!Pneumatic Pad Sanders',2540,0,5876748,NULL,NULL),(5876755,'Pneumatic Straight Grinders','!Maintenance, Repair and Operations!Pneumatics!Finishing Tools!Pneumatic Straight Grinders',2541,0,5876748,NULL,NULL),(5876756,'Compressors, Blow Guns, Hoses and Fittings','!Maintenance, Repair and Operations!Pneumatics!Compressors, Blow Guns, Hoses and Fittings',1797,0,5876744,NULL,NULL),(5876757,'Blow Guns','!Maintenance, Repair and Operations!Pneumatics!Compressors, Blow Guns, Hoses and Fittings!Blow Guns',2542,0,5876756,NULL,NULL),(5876758,'Cutting Tools','!Maintenance, Repair and Operations!Pneumatics!Cutting Tools',1798,0,5876744,NULL,NULL),(5876759,'Pneumatic Reciprocating Saws','!Maintenance, Repair and Operations!Pneumatics!Cutting Tools!Pneumatic Reciprocating Saws',2543,0,5876758,NULL,NULL),(5876760,'Demolition Tools','!Maintenance, Repair and Operations!Pneumatics!Demolition Tools',1799,0,5876744,NULL,NULL),(5876761,'Pneumatic Scalers','!Maintenance, Repair and Operations!Pneumatics!Demolition Tools!Pneumatic Scalers',2544,0,5876760,NULL,NULL),(5876762,'Drilling and Fastening Tools','!Maintenance, Repair and Operations!Pneumatics!Drilling and Fastening Tools',1800,0,5876744,NULL,NULL),(5876763,'Pneumatic Drills','!Maintenance, Repair and Operations!Pneumatics!Drilling and Fastening Tools!Pneumatic Drills',2545,0,5876762,NULL,NULL),(5876764,'Pneumatic Impact Wrenches','!Maintenance, Repair and Operations!Pneumatics!Drilling and Fastening Tools!Pneumatic Impact Wrenches',2546,0,5876762,NULL,NULL),(5876765,'Pneumatic Nailer Parts and Accessories','!Maintenance, Repair and Operations!Pneumatics!Drilling and Fastening Tools!Pneumatic Nailer Parts and Accessories',2547,0,5876762,NULL,NULL),(5876766,'Pneumatic Nailers','!Maintenance, Repair and Operations!Pneumatics!Drilling and Fastening Tools!Pneumatic Nailers',2548,0,5876762,NULL,NULL),(5876767,'Pneumatic Ratchet Wrenches','!Maintenance, Repair and Operations!Pneumatics!Drilling and Fastening Tools!Pneumatic Ratchet Wrenches',2549,0,5876762,NULL,NULL),(5876768,'Power Tools','!Maintenance, Repair and Operations!Power Tools',1526,0,5875888,NULL,NULL),(5876769,'Cutting Tools','!Maintenance, Repair and Operations!Power Tools!Cutting Tools',1802,0,5876768,NULL,NULL),(5876770,'Chain Saws','!Maintenance, Repair and Operations!Power Tools!Cutting Tools!Chain Saws',2551,0,5876769,NULL,NULL),(5876771,'Chain Saws Gas','!Maintenance, Repair and Operations!Power Tools!Cutting Tools!Chain Saws!Chain Saws Gas',2963,0,5876770,NULL,NULL),(5876772,'Hammer Drilling and Demolition Tools','!Maintenance, Repair and Operations!Power Tools!Hammer Drilling and Demolition Tools',1803,0,5876768,NULL,NULL),(5876773,'Hammer Drills','!Maintenance, Repair and Operations!Power Tools!Hammer Drilling and Demolition Tools!Hammer Drills',2552,0,5876772,NULL,NULL),(5876774,'Hammer Drill Bits','!Maintenance, Repair and Operations!Power Tools!Hammer Drilling and Demolition Tools!Hammer Drills!Hammer Drill Bits',2964,0,5876773,NULL,NULL),(5876775,'Hammer Drills Corded','!Maintenance, Repair and Operations!Power Tools!Hammer Drilling and Demolition Tools!Hammer Drills!Hammer Drills Corded',2965,0,5876773,NULL,NULL),(5876776,'Rotary Hammers','!Maintenance, Repair and Operations!Power Tools!Hammer Drilling and Demolition Tools!Rotary Hammers',2553,0,5876772,NULL,NULL),(5876777,'Rotary Hammer Parts and Accessories','!Maintenance, Repair and Operations!Power Tools!Hammer Drilling and Demolition Tools!Rotary Hammers!Rotary Hammer Parts and Accessories',2966,0,5876776,NULL,NULL),(5876778,'Rotary Hammers Corded','!Maintenance, Repair and Operations!Power Tools!Hammer Drilling and Demolition Tools!Rotary Hammers!Rotary Hammers Corded',2967,0,5876776,NULL,NULL),(5876779,'Rotary Hammers Cordless','!Maintenance, Repair and Operations!Power Tools!Hammer Drilling and Demolition Tools!Rotary Hammers!Rotary Hammers Cordless',2968,0,5876776,NULL,NULL),(5876780,'SDS Bits','!Maintenance, Repair and Operations!Power Tools!Hammer Drilling and Demolition Tools!Rotary Hammers!SDS Bits',2969,0,5876776,NULL,NULL),(5876781,'Spline Bits','!Maintenance, Repair and Operations!Power Tools!Hammer Drilling and Demolition Tools!Rotary Hammers!Spline Bits',2970,0,5876776,NULL,NULL),(5876782,'Heating Tools','!Maintenance, Repair and Operations!Power Tools!Heating Tools',1804,0,5876768,NULL,NULL),(5876783,'Desoldering Tools','!Maintenance, Repair and Operations!Power Tools!Heating Tools!Desoldering Tools',2554,0,5876782,NULL,NULL),(5876784,'Desoldering Pumps','!Maintenance, Repair and Operations!Power Tools!Heating Tools!Desoldering Tools!Desoldering Pumps',2971,0,5876783,NULL,NULL),(5876785,'Desoldering Tool Parts and Accessories','!Maintenance, Repair and Operations!Power Tools!Heating Tools!Desoldering Tools!Desoldering Tool Parts and Accessories',2972,0,5876783,NULL,NULL),(5876786,'Glue Guns','!Maintenance, Repair and Operations!Power Tools!Heating Tools!Glue Guns',2555,0,5876782,NULL,NULL),(5876787,'Glue Gun Parts and Accessories','!Maintenance, Repair and Operations!Power Tools!Heating Tools!Glue Guns!Glue Gun Parts and Accessories',2973,0,5876786,NULL,NULL),(5876788,'Cutting Tools','!Maintenance, Repair and Operations!Power Tools!Cutting Tools',1805,0,5876768,NULL,NULL),(5876789,'Chop and Cut-Off Saws','!Maintenance, Repair and Operations!Power Tools!Cutting Tools!Chop and Cut-Off Saws',2556,0,5876788,NULL,NULL),(5876790,'Chop and Cut-Off Saw Parts and Accessories','!Maintenance, Repair and Operations!Power Tools!Cutting Tools!Chop and Cut-Off Saws!Chop and Cut-Off Saw Parts and Accessories',2974,0,5876789,NULL,NULL),(5876791,'Heating Tools','!Maintenance, Repair and Operations!Power Tools!Heating Tools',1806,0,5876768,NULL,NULL),(5876792,'Glue Guns','!Maintenance, Repair and Operations!Power Tools!Heating Tools!Glue Guns',2557,0,5876791,NULL,NULL),(5876793,'Glue Guns Corded','!Maintenance, Repair and Operations!Power Tools!Heating Tools!Glue Guns!Glue Guns Corded',2975,0,5876792,NULL,NULL),(5876794,'Glue Guns Cordless','!Maintenance, Repair and Operations!Power Tools!Heating Tools!Glue Guns!Glue Guns Cordless',2976,0,5876792,NULL,NULL),(5876795,'Heat Guns and Torches','!Maintenance, Repair and Operations!Power Tools!Heating Tools!Heat Guns and Torches',2558,0,5876791,NULL,NULL),(5876796,'Heat Gun Parts and Accessories','!Maintenance, Repair and Operations!Power Tools!Heating Tools!Heat Guns and Torches!Heat Gun Parts and Accessories',2977,0,5876795,NULL,NULL),(5876797,'Heat Guns Corded','!Maintenance, Repair and Operations!Power Tools!Heating Tools!Heat Guns and Torches!Heat Guns Corded',2978,0,5876795,NULL,NULL),(5876798,'Heating Torches Cordless','!Maintenance, Repair and Operations!Power Tools!Heating Tools!Heat Guns and Torches!Heating Torches Cordless',2979,0,5876795,NULL,NULL),(5876799,'Soldering Guns','!Maintenance, Repair and Operations!Power Tools!Heating Tools!Soldering Guns',2559,0,5876791,NULL,NULL),(5876800,'Soldering Gun Parts and Accessories','!Maintenance, Repair and Operations!Power Tools!Heating Tools!Soldering Guns!Soldering Gun Parts and Accessories',2980,0,5876799,NULL,NULL),(5876801,'Soldering Guns Corded','!Maintenance, Repair and Operations!Power Tools!Heating Tools!Soldering Guns!Soldering Guns Corded',2981,0,5876799,NULL,NULL),(5876802,'Soldering Irons','!Maintenance, Repair and Operations!Power Tools!Heating Tools!Soldering Irons',2560,0,5876791,NULL,NULL),(5876803,'Soldering Iron Parts and Accessories','!Maintenance, Repair and Operations!Power Tools!Heating Tools!Soldering Irons!Soldering Iron Parts and Accessories',2982,0,5876802,NULL,NULL),(5876804,'Soldering Irons Corded','!Maintenance, Repair and Operations!Power Tools!Heating Tools!Soldering Irons!Soldering Irons Corded',2983,0,5876802,NULL,NULL),(5876805,'Soldering Irons Cordless','!Maintenance, Repair and Operations!Power Tools!Heating Tools!Soldering Irons!Soldering Irons Cordless',2984,0,5876802,NULL,NULL),(5876806,'Cutting Tools','!Maintenance, Repair and Operations!Power Tools!Cutting Tools',1807,0,5876768,NULL,NULL),(5876807,'Chop and Cut-Off Saws','!Maintenance, Repair and Operations!Power Tools!Cutting Tools!Chop and Cut-Off Saws',2561,0,5876806,NULL,NULL),(5876808,'Chop and Cut-Off Saws Corded','!Maintenance, Repair and Operations!Power Tools!Cutting Tools!Chop and Cut-Off Saws!Chop and Cut-Off Saws Corded',2985,0,5876807,NULL,NULL),(5876809,'Heating Tools','!Maintenance, Repair and Operations!Power Tools!Heating Tools',1808,0,5876768,NULL,NULL),(5876810,'Soldering Stations','!Maintenance, Repair and Operations!Power Tools!Heating Tools!Soldering Stations',2562,0,5876809,NULL,NULL),(5876811,'Soldering Stations Corded','!Maintenance, Repair and Operations!Power Tools!Heating Tools!Soldering Stations!Soldering Stations Corded',2986,0,5876810,NULL,NULL),(5876812,'Multi-Purpose Power Tools','!Maintenance, Repair and Operations!Power Tools!Multi-Purpose Power Tools',1809,0,5876768,NULL,NULL),(5876813,'Combo Kits Cordless','!Maintenance, Repair and Operations!Power Tools!Multi-Purpose Power Tools!Combo Kits Cordless',2563,0,5876812,NULL,NULL),(5876814,'Multi-Purpose Tools Corded','!Maintenance, Repair and Operations!Power Tools!Multi-Purpose Power Tools!Multi-Purpose Tools Corded',2564,0,5876812,NULL,NULL),(5876815,'Multi-Purpose Tools Corded Parts and Accessories','!Maintenance, Repair and Operations!Power Tools!Multi-Purpose Power Tools!Multi-Purpose Tools Corded Parts and Accessories',2565,0,5876812,NULL,NULL),(5876816,'Multi-Purpose Tools Cordless','!Maintenance, Repair and Operations!Power Tools!Multi-Purpose Power Tools!Multi-Purpose Tools Cordless',2566,0,5876812,NULL,NULL),(5876817,'Pneumatics','!Maintenance, Repair and Operations!Power Tools!Pneumatics',1810,0,5876768,NULL,NULL),(5876818,'Air Hose Reels','!Maintenance, Repair and Operations!Power Tools!Pneumatics!Air Hose Reels',2568,0,5876817,NULL,NULL),(5876819,'Air Tool Oils','!Maintenance, Repair and Operations!Power Tools!Pneumatics!Air Tool Oils',2569,0,5876817,NULL,NULL),(5876820,'Balancers','!Maintenance, Repair and Operations!Power Tools!Pneumatics!Balancers',2570,0,5876817,NULL,NULL),(5876821,'Compressors, Blow Guns, Hoses and Fittings','!Maintenance, Repair and Operations!Power Tools!Pneumatics!Compressors, Blow Guns, Hoses and Fittings',2571,0,5876817,NULL,NULL),(5876822,'Air Compressor Parts and Accessories','!Maintenance, Repair and Operations!Power Tools!Pneumatics!Compressors, Blow Guns, Hoses and Fittings!Air Compressor Parts and Accessories',2988,0,5876821,NULL,NULL),(5876823,'Cutting Tools','!Maintenance, Repair and Operations!Power Tools!Cutting Tools',1811,0,5876768,NULL,NULL),(5876824,'Circular Saws','!Maintenance, Repair and Operations!Power Tools!Cutting Tools!Circular Saws',2572,0,5876823,NULL,NULL),(5876825,'Circular Saw Parts and Accessories','!Maintenance, Repair and Operations!Power Tools!Cutting Tools!Circular Saws!Circular Saw Parts and Accessories',2989,0,5876824,NULL,NULL),(5876826,'Pneumatics','!Maintenance, Repair and Operations!Power Tools!Pneumatics',1812,0,5876768,NULL,NULL),(5876827,'Compressors, Blow Guns, Hoses and Fittings','!Maintenance, Repair and Operations!Power Tools!Pneumatics!Compressors, Blow Guns, Hoses and Fittings',2573,0,5876826,NULL,NULL),(5876828,'Air Compressors','!Maintenance, Repair and Operations!Power Tools!Pneumatics!Compressors, Blow Guns, Hoses and Fittings!Air Compressors',2990,0,5876827,NULL,NULL),(5876829,'Air Hose Fittings','!Maintenance, Repair and Operations!Power Tools!Pneumatics!Compressors, Blow Guns, Hoses and Fittings!Air Hose Fittings',2991,0,5876827,NULL,NULL),(5876830,'Air Hoses','!Maintenance, Repair and Operations!Power Tools!Pneumatics!Compressors, Blow Guns, Hoses and Fittings!Air Hoses',2992,0,5876827,NULL,NULL),(5876831,'Blow Gun Parts and Accessories','!Maintenance, Repair and Operations!Power Tools!Pneumatics!Compressors, Blow Guns, Hoses and Fittings!Blow Gun Parts and Accessories',2993,0,5876827,NULL,NULL),(5876832,'Blow Guns','!Maintenance, Repair and Operations!Power Tools!Pneumatics!Compressors, Blow Guns, Hoses and Fittings!Blow Guns',2994,0,5876827,NULL,NULL),(5876833,'Cutting Tools','!Maintenance, Repair and Operations!Power Tools!Pneumatics!Cutting Tools',2574,0,5876826,NULL,NULL),(5876834,'Pneumatic Cutter Parts and Accessories','!Maintenance, Repair and Operations!Power Tools!Pneumatics!Cutting Tools!Pneumatic Cutter Parts and Accessories',2995,0,5876833,NULL,NULL),(5876835,'Pneumatic Cutters','!Maintenance, Repair and Operations!Power Tools!Pneumatics!Cutting Tools!Pneumatic Cutters',2996,0,5876833,NULL,NULL),(5876836,'Pneumatic Hacksaw Parts and Accessories','!Maintenance, Repair and Operations!Power Tools!Pneumatics!Cutting Tools!Pneumatic Hacksaw Parts and Accessories',2997,0,5876833,NULL,NULL),(5876837,'Pneumatic Hacksaws','!Maintenance, Repair and Operations!Power Tools!Pneumatics!Cutting Tools!Pneumatic Hacksaws',2998,0,5876833,NULL,NULL),(5876838,'Pneumatic Jig Saws','!Maintenance, Repair and Operations!Power Tools!Pneumatics!Cutting Tools!Pneumatic Jig Saws',2999,0,5876833,NULL,NULL),(5876839,'Cutting Tools','!Maintenance, Repair and Operations!Power Tools!Cutting Tools',1813,0,5876768,NULL,NULL),(5876840,'Circular Saws','!Maintenance, Repair and Operations!Power Tools!Cutting Tools!Circular Saws',2575,0,5876839,NULL,NULL),(5876841,'Circular Saws Corded','!Maintenance, Repair and Operations!Power Tools!Cutting Tools!Circular Saws!Circular Saws Corded',3000,0,5876840,NULL,NULL),(5876842,'Pneumatics','!Maintenance, Repair and Operations!Power Tools!Pneumatics',1814,0,5876768,NULL,NULL),(5876843,'Cutting Tools','!Maintenance, Repair and Operations!Power Tools!Pneumatics!Cutting Tools',2576,0,5876842,NULL,NULL),(5876844,'Pneumatic Nibbler Parts and Accessories','!Maintenance, Repair and Operations!Power Tools!Pneumatics!Cutting Tools!Pneumatic Nibbler Parts and Accessories',3001,0,5876843,NULL,NULL),(5876845,'Pneumatic Nibblers','!Maintenance, Repair and Operations!Power Tools!Pneumatics!Cutting Tools!Pneumatic Nibblers',3002,0,5876843,NULL,NULL),(5876846,'Pneumatic Reciprocating Saw Parts and Accessories','!Maintenance, Repair and Operations!Power Tools!Pneumatics!Cutting Tools!Pneumatic Reciprocating Saw Parts and Accessories',3003,0,5876843,NULL,NULL),(5876847,'Pneumatic Reciprocating Saws','!Maintenance, Repair and Operations!Power Tools!Pneumatics!Cutting Tools!Pneumatic Reciprocating Saws',3004,0,5876843,NULL,NULL),(5876848,'Pneumatic Shears','!Maintenance, Repair and Operations!Power Tools!Pneumatics!Cutting Tools!Pneumatic Shears',3005,0,5876843,NULL,NULL),(5876849,'Demolition Tools','!Maintenance, Repair and Operations!Power Tools!Pneumatics!Demolition Tools',2577,0,5876842,NULL,NULL),(5876850,'Pneumatic Chipping Hammers','!Maintenance, Repair and Operations!Power Tools!Pneumatics!Demolition Tools!Pneumatic Chipping Hammers',3006,0,5876849,NULL,NULL),(5876851,'Pneumatic Chipping Parts and Accessories','!Maintenance, Repair and Operations!Power Tools!Pneumatics!Demolition Tools!Pneumatic Chipping Parts and Accessories',3007,0,5876849,NULL,NULL),(5876852,'Pneumatic Chisels and Steel','!Maintenance, Repair and Operations!Power Tools!Pneumatics!Demolition Tools!Pneumatic Chisels and Steel',3008,0,5876849,NULL,NULL),(5876853,'Pneumatic Demolition Hammers','!Maintenance, Repair and Operations!Power Tools!Pneumatics!Demolition Tools!Pneumatic Demolition Hammers',3009,0,5876849,NULL,NULL),(5876854,'Pneumatic Rivet Hammer Parts and Accesories','!Maintenance, Repair and Operations!Power Tools!Pneumatics!Demolition Tools!Pneumatic Rivet Hammer Parts and Accesories',3010,0,5876849,NULL,NULL),(5876855,'Cutting Tools','!Maintenance, Repair and Operations!Power Tools!Cutting Tools',1815,0,5876768,NULL,NULL),(5876856,'Circular Saws','!Maintenance, Repair and Operations!Power Tools!Cutting Tools!Circular Saws',2578,0,5876855,NULL,NULL),(5876857,'Circular Saws Cordless','!Maintenance, Repair and Operations!Power Tools!Cutting Tools!Circular Saws!Circular Saws Cordless',3011,0,5876856,NULL,NULL),(5876858,'Pneumatics','!Maintenance, Repair and Operations!Power Tools!Pneumatics',1816,0,5876768,NULL,NULL),(5876859,'Demolition Tools','!Maintenance, Repair and Operations!Power Tools!Pneumatics!Demolition Tools',2579,0,5876858,NULL,NULL),(5876860,'Pneumatic Riveting Hammers','!Maintenance, Repair and Operations!Power Tools!Pneumatics!Demolition Tools!Pneumatic Riveting Hammers',3012,0,5876859,NULL,NULL),(5876861,'Pneumatic Scaler Parts and Accessories','!Maintenance, Repair and Operations!Power Tools!Pneumatics!Demolition Tools!Pneumatic Scaler Parts and Accessories',3013,0,5876859,NULL,NULL),(5876862,'Pneumatic Scalers','!Maintenance, Repair and Operations!Power Tools!Pneumatics!Demolition Tools!Pneumatic Scalers',3014,0,5876859,NULL,NULL),(5876863,'Pneumatic Tampers and Rammers','!Maintenance, Repair and Operations!Power Tools!Pneumatics!Demolition Tools!Pneumatic Tampers and Rammers',3015,0,5876859,NULL,NULL),(5876864,'Drilling and Fastening Tools','!Maintenance, Repair and Operations!Power Tools!Pneumatics!Drilling and Fastening Tools',2580,0,5876858,NULL,NULL),(5876865,'Pneumatic Drills','!Maintenance, Repair and Operations!Power Tools!Pneumatics!Drilling and Fastening Tools!Pneumatic Drills',3016,0,5876864,NULL,NULL),(5876866,'Pneumatic Impact Wrench Parts and Accessories','!Maintenance, Repair and Operations!Power Tools!Pneumatics!Drilling and Fastening Tools!Pneumatic Impact Wrench Parts and Accessories',3017,0,5876864,NULL,NULL),(5876867,'Pneumatic Impact Wrenches','!Maintenance, Repair and Operations!Power Tools!Pneumatics!Drilling and Fastening Tools!Pneumatic Impact Wrenches',3018,0,5876864,NULL,NULL),(5876868,'Pneumatic Nailer Parts and Accessories','!Maintenance, Repair and Operations!Power Tools!Pneumatics!Drilling and Fastening Tools!Pneumatic Nailer Parts and Accessories',3019,0,5876864,NULL,NULL),(5876869,'Pneumatic Nailers','!Maintenance, Repair and Operations!Power Tools!Pneumatics!Drilling and Fastening Tools!Pneumatic Nailers',3020,0,5876864,NULL,NULL),(5876870,'Pneumatic Ratchet Wrench Parts and Accessories','!Maintenance, Repair and Operations!Power Tools!Pneumatics!Drilling and Fastening Tools!Pneumatic Ratchet Wrench Parts and Accessories',3021,0,5876864,NULL,NULL),(5876871,'Cutting Tools','!Maintenance, Repair and Operations!Power Tools!Cutting Tools',1817,0,5876768,NULL,NULL),(5876872,'Cut-Out Tools','!Maintenance, Repair and Operations!Power Tools!Cutting Tools!Cut-Out Tools',2581,0,5876871,NULL,NULL),(5876873,'Cut-Out Tool Parts and Accessories','!Maintenance, Repair and Operations!Power Tools!Cutting Tools!Cut-Out Tools!Cut-Out Tool Parts and Accessories',3022,0,5876872,NULL,NULL),(5876874,'Pneumatics','!Maintenance, Repair and Operations!Power Tools!Pneumatics',1818,0,5876768,NULL,NULL),(5876875,'Drilling and Fastening Tools','!Maintenance, Repair and Operations!Power Tools!Pneumatics!Drilling and Fastening Tools',2582,0,5876874,NULL,NULL),(5876876,'Pneumatic Ratchet Wrenches','!Maintenance, Repair and Operations!Power Tools!Pneumatics!Drilling and Fastening Tools!Pneumatic Ratchet Wrenches',3023,0,5876875,NULL,NULL),(5876877,'Pneumatic Riveter Parts and Accessories','!Maintenance, Repair and Operations!Power Tools!Pneumatics!Drilling and Fastening Tools!Pneumatic Riveter Parts and Accessories',3024,0,5876875,NULL,NULL),(5876878,'Pneumatic Riveters','!Maintenance, Repair and Operations!Power Tools!Pneumatics!Drilling and Fastening Tools!Pneumatic Riveters',3025,0,5876875,NULL,NULL),(5876879,'Pneumatic Screwdrivers','!Maintenance, Repair and Operations!Power Tools!Pneumatics!Drilling and Fastening Tools!Pneumatic Screwdrivers',3026,0,5876875,NULL,NULL),(5876880,'Pneumatic Stapler Parts and Accessories','!Maintenance, Repair and Operations!Power Tools!Pneumatics!Drilling and Fastening Tools!Pneumatic Stapler Parts and Accessories',3027,0,5876875,NULL,NULL),(5876881,'Pneumatic Staplers','!Maintenance, Repair and Operations!Power Tools!Pneumatics!Drilling and Fastening Tools!Pneumatic Staplers',3028,0,5876875,NULL,NULL),(5876882,'Pneumatic Torque Multipliers','!Maintenance, Repair and Operations!Power Tools!Pneumatics!Drilling and Fastening Tools!Pneumatic Torque Multipliers',3029,0,5876875,NULL,NULL),(5876883,'Finishing Tools','!Maintenance, Repair and Operations!Power Tools!Pneumatics!Finishing Tools',2583,0,5876874,NULL,NULL),(5876884,'Pneumatic Angle Grinder Parts and Accessories','!Maintenance, Repair and Operations!Power Tools!Pneumatics!Finishing Tools!Pneumatic Angle Grinder Parts and Accessories',3030,0,5876883,NULL,NULL),(5876885,'Pneumatic Angle Grinders','!Maintenance, Repair and Operations!Power Tools!Pneumatics!Finishing Tools!Pneumatic Angle Grinders',3031,0,5876883,NULL,NULL),(5876886,'Pneumatic Belt Sander Parts and Accessories','!Maintenance, Repair and Operations!Power Tools!Pneumatics!Finishing Tools!Pneumatic Belt Sander Parts and Accessories',3032,0,5876883,NULL,NULL),(5876887,'Cutting Tools','!Maintenance, Repair and Operations!Power Tools!Cutting Tools',1819,0,5876768,NULL,NULL),(5876888,'Cut-Out Tools','!Maintenance, Repair and Operations!Power Tools!Cutting Tools!Cut-Out Tools',2584,0,5876887,NULL,NULL),(5876889,'Cut-Out Tools Corded','!Maintenance, Repair and Operations!Power Tools!Cutting Tools!Cut-Out Tools!Cut-Out Tools Corded',3033,0,5876888,NULL,NULL),(5876890,'Pneumatics','!Maintenance, Repair and Operations!Power Tools!Pneumatics',1820,0,5876768,NULL,NULL),(5876891,'Finishing Tools','!Maintenance, Repair and Operations!Power Tools!Pneumatics!Finishing Tools',2585,0,5876890,NULL,NULL),(5876892,'Pneumatic Belt Sanders','!Maintenance, Repair and Operations!Power Tools!Pneumatics!Finishing Tools!Pneumatic Belt Sanders',3034,0,5876891,NULL,NULL),(5876893,'Pneumatic Die Grinder Parts and Accessories','!Maintenance, Repair and Operations!Power Tools!Pneumatics!Finishing Tools!Pneumatic Die Grinder Parts and Accessories',3035,0,5876891,NULL,NULL),(5876894,'Pneumatic Die Grinders','!Maintenance, Repair and Operations!Power Tools!Pneumatics!Finishing Tools!Pneumatic Die Grinders',3036,0,5876891,NULL,NULL),(5876895,'Pneumatic Engraver Parts and Accessories','!Maintenance, Repair and Operations!Power Tools!Pneumatics!Finishing Tools!Pneumatic Engraver Parts and Accessories',3037,0,5876891,NULL,NULL),(5876896,'Pneumatic Engravers','!Maintenance, Repair and Operations!Power Tools!Pneumatics!Finishing Tools!Pneumatic Engravers',3038,0,5876891,NULL,NULL),(5876897,'Pneumatic Files','!Maintenance, Repair and Operations!Power Tools!Pneumatics!Finishing Tools!Pneumatic Files',3039,0,5876891,NULL,NULL),(5876898,'Pneumatic Pad Sander Parts and Accessories','!Maintenance, Repair and Operations!Power Tools!Pneumatics!Finishing Tools!Pneumatic Pad Sander Parts and Accessories',3040,0,5876891,NULL,NULL),(5876899,'Pneumatic Pad Sanders','!Maintenance, Repair and Operations!Power Tools!Pneumatics!Finishing Tools!Pneumatic Pad Sanders',3041,0,5876891,NULL,NULL),(5876900,'Pneumatic Straight Grinders','!Maintenance, Repair and Operations!Power Tools!Pneumatics!Finishing Tools!Pneumatic Straight Grinders',3042,0,5876891,NULL,NULL),(5876901,'Pneumatic Vertical Grinders','!Maintenance, Repair and Operations!Power Tools!Pneumatics!Finishing Tools!Pneumatic Vertical Grinders',3043,0,5876891,NULL,NULL),(5876902,'Cutting Tools','!Maintenance, Repair and Operations!Power Tools!Cutting Tools',1821,0,5876768,NULL,NULL),(5876903,'Cut-Out Tools','!Maintenance, Repair and Operations!Power Tools!Cutting Tools!Cut-Out Tools',2586,0,5876902,NULL,NULL),(5876904,'Cut-Out Tools Cordless','!Maintenance, Repair and Operations!Power Tools!Cutting Tools!Cut-Out Tools!Cut-Out Tools Cordless',3044,0,5876903,NULL,NULL),(5876905,'Pneumatics','!Maintenance, Repair and Operations!Power Tools!Pneumatics',1822,0,5876768,NULL,NULL),(5876906,'Routing and Trimming Tools','!Maintenance, Repair and Operations!Power Tools!Pneumatics!Routing and Trimming Tools',2587,0,5876905,NULL,NULL),(5876907,'Pneumatic Router and Trimmer Tool Parts and Accessories','!Maintenance, Repair and Operations!Power Tools!Pneumatics!Routing and Trimming Tools!Pneumatic Router and Trimmer Tool Parts and Accessories',3045,0,5876906,NULL,NULL),(5876908,'Pneumatic Routers','!Maintenance, Repair and Operations!Power Tools!Pneumatics!Routing and Trimming Tools!Pneumatic Routers',3046,0,5876906,NULL,NULL),(5876909,'Power Tools Other','!Maintenance, Repair and Operations!Power Tools!Power Tools Other',1823,0,5876768,NULL,NULL),(5876910,'Routing and Trimming Tools','!Maintenance, Repair and Operations!Power Tools!Routing and Trimming Tools',1824,0,5876768,NULL,NULL),(5876911,'Laminate Trimmers Corded','!Maintenance, Repair and Operations!Power Tools!Routing and Trimming Tools!Laminate Trimmers Corded',2589,0,5876910,NULL,NULL),(5876912,'Router and Laminate Trimmer Parts and Accessories','!Maintenance, Repair and Operations!Power Tools!Routing and Trimming Tools!Router and Laminate Trimmer Parts and Accessories',2590,0,5876910,NULL,NULL),(5876913,'Routers Corded','!Maintenance, Repair and Operations!Power Tools!Routing and Trimming Tools!Routers Corded',2591,0,5876910,NULL,NULL),(5876914,'Cutting Tools','!Maintenance, Repair and Operations!Power Tools!Cutting Tools',1825,0,5876768,NULL,NULL),(5876915,'Cutters','!Maintenance, Repair and Operations!Power Tools!Cutting Tools!Cutters',2592,0,5876914,NULL,NULL),(5876916,'Cable Cutters Cordless','!Maintenance, Repair and Operations!Power Tools!Cutting Tools!Cutters!Cable Cutters Cordless',3048,0,5876915,NULL,NULL),(5876917,'Cordless Tool Batteries and Chargers','!Maintenance, Repair and Operations!Power Tools!Cordless Tool Batteries and Chargers',1826,0,5876768,NULL,NULL),(5876918,'Batteries, Cordless Tool','!Maintenance, Repair and Operations!Power Tools!Cordless Tool Batteries and Chargers!Batteries, Cordless Tool',2593,0,5876917,NULL,NULL),(5876919,'Cutting Tools','!Maintenance, Repair and Operations!Power Tools!Cutting Tools',1827,0,5876768,NULL,NULL),(5876920,'Cutters','!Maintenance, Repair and Operations!Power Tools!Cutting Tools!Cutters',2594,0,5876919,NULL,NULL),(5876921,'Tubing Cutters Cordless','!Maintenance, Repair and Operations!Power Tools!Cutting Tools!Cutters!Tubing Cutters Cordless',3050,0,5876920,NULL,NULL),(5876922,'Dust Collectors','!Maintenance, Repair and Operations!Power Tools!Cutting Tools!Dust Collectors',2595,0,5876919,NULL,NULL),(5876923,'Handsaws and Hacksaws','!Maintenance, Repair and Operations!Power Tools!Cutting Tools!Handsaws and Hacksaws',2596,0,5876919,NULL,NULL),(5876924,'Handsaws Corded','!Maintenance, Repair and Operations!Power Tools!Cutting Tools!Handsaws and Hacksaws!Handsaws Corded',3052,0,5876923,NULL,NULL),(5876925,'Jig Saws','!Maintenance, Repair and Operations!Power Tools!Cutting Tools!Jig Saws',2597,0,5876919,NULL,NULL),(5876926,'Jig Saw Parts and Accessories','!Maintenance, Repair and Operations!Power Tools!Cutting Tools!Jig Saws!Jig Saw Parts and Accessories',3053,0,5876925,NULL,NULL),(5876927,'Jig Saws Corded','!Maintenance, Repair and Operations!Power Tools!Cutting Tools!Jig Saws!Jig Saws Corded',3054,0,5876925,NULL,NULL),(5876928,'Jig Saws Cordless','!Maintenance, Repair and Operations!Power Tools!Cutting Tools!Jig Saws!Jig Saws Cordless',3055,0,5876925,NULL,NULL),(5876929,'Lathes','!Maintenance, Repair and Operations!Power Tools!Cutting Tools!Lathes',2598,0,5876919,NULL,NULL),(5876930,'Lathe Parts and Accessories','!Maintenance, Repair and Operations!Power Tools!Cutting Tools!Lathes!Lathe Parts and Accessories',3056,0,5876929,NULL,NULL),(5876931,'Lathes','!Maintenance, Repair and Operations!Power Tools!Cutting Tools!Lathes!Lathes',3057,0,5876929,NULL,NULL),(5876932,'Miter Saws','!Maintenance, Repair and Operations!Power Tools!Cutting Tools!Miter Saws',2599,0,5876919,NULL,NULL),(5876933,'Miter Saw Parts and Accessories','!Maintenance, Repair and Operations!Power Tools!Cutting Tools!Miter Saws!Miter Saw Parts and Accessories',3058,0,5876932,NULL,NULL),(5876934,'Miter Saws','!Maintenance, Repair and Operations!Power Tools!Cutting Tools!Miter Saws!Miter Saws',3059,0,5876932,NULL,NULL),(5876935,'Cordless Tool Batteries and Chargers','!Maintenance, Repair and Operations!Power Tools!Cordless Tool Batteries and Chargers',1828,0,5876768,NULL,NULL),(5876936,'Battery Chargers','!Maintenance, Repair and Operations!Power Tools!Cordless Tool Batteries and Chargers!Battery Chargers',2600,0,5876935,NULL,NULL),(5876937,'Cutting Tools','!Maintenance, Repair and Operations!Power Tools!Cutting Tools',1829,0,5876768,NULL,NULL),(5876938,'Nibblers','!Maintenance, Repair and Operations!Power Tools!Cutting Tools!Nibblers',2601,0,5876937,NULL,NULL),(5876939,'Nibbler Parts and Accessories','!Maintenance, Repair and Operations!Power Tools!Cutting Tools!Nibblers!Nibbler Parts and Accessories',3061,0,5876938,NULL,NULL),(5876940,'Nibblers Corded','!Maintenance, Repair and Operations!Power Tools!Cutting Tools!Nibblers!Nibblers Corded',3062,0,5876938,NULL,NULL),(5876941,'Panel Saws','!Maintenance, Repair and Operations!Power Tools!Cutting Tools!Panel Saws',2602,0,5876937,NULL,NULL),(5876942,'Panel Saw Parts and Accessories','!Maintenance, Repair and Operations!Power Tools!Cutting Tools!Panel Saws!Panel Saw Parts and Accessories',3063,0,5876941,NULL,NULL),(5876943,'Panel Saws','!Maintenance, Repair and Operations!Power Tools!Cutting Tools!Panel Saws!Panel Saws',3064,0,5876941,NULL,NULL),(5876944,'Radial Arm Saws','!Maintenance, Repair and Operations!Power Tools!Cutting Tools!Radial Arm Saws',2603,0,5876937,NULL,NULL),(5876945,'Radial Arm Saws','!Maintenance, Repair and Operations!Power Tools!Cutting Tools!Radial Arm Saws!Radial Arm Saws',3065,0,5876944,NULL,NULL),(5876946,'Reciprocating Saws','!Maintenance, Repair and Operations!Power Tools!Cutting Tools!Reciprocating Saws',2604,0,5876937,NULL,NULL),(5876947,'Reciprocating Saw Parts and Accessories','!Maintenance, Repair and Operations!Power Tools!Cutting Tools!Reciprocating Saws!Reciprocating Saw Parts and Accessories',3066,0,5876946,NULL,NULL),(5876948,'Reciprocating Saws Corded','!Maintenance, Repair and Operations!Power Tools!Cutting Tools!Reciprocating Saws!Reciprocating Saws Corded',3067,0,5876946,NULL,NULL),(5876949,'Reciprocating Saws Cordless','!Maintenance, Repair and Operations!Power Tools!Cutting Tools!Reciprocating Saws!Reciprocating Saws Cordless',3068,0,5876946,NULL,NULL),(5876950,'Scroll Saws','!Maintenance, Repair and Operations!Power Tools!Cutting Tools!Scroll Saws',2605,0,5876937,NULL,NULL),(5876951,'Scroll Saw Parts and Accessories','!Maintenance, Repair and Operations!Power Tools!Cutting Tools!Scroll Saws!Scroll Saw Parts and Accessories',3069,0,5876950,NULL,NULL),(5876952,'Scroll Saws','!Maintenance, Repair and Operations!Power Tools!Cutting Tools!Scroll Saws!Scroll Saws',3070,0,5876950,NULL,NULL),(5876953,'Band Saws','!Maintenance, Repair and Operations!Power Tools!Cutting Tools!Band Saws',2606,0,5876937,NULL,NULL),(5876954,'Band Saw Parts and Accessories','!Maintenance, Repair and Operations!Power Tools!Cutting Tools!Band Saws!Band Saw Parts and Accessories',3071,0,5876953,NULL,NULL),(5876955,'Shears','!Maintenance, Repair and Operations!Power Tools!Cutting Tools!Shears',2607,0,5876937,NULL,NULL),(5876956,'Shear Parts and Accessories','!Maintenance, Repair and Operations!Power Tools!Cutting Tools!Shears!Shear Parts and Accessories',3072,0,5876955,NULL,NULL),(5876957,'Shears Corded','!Maintenance, Repair and Operations!Power Tools!Cutting Tools!Shears!Shears Corded',3073,0,5876955,NULL,NULL),(5876958,'Shears Cordless','!Maintenance, Repair and Operations!Power Tools!Cutting Tools!Shears!Shears Cordless',3074,0,5876955,NULL,NULL),(5876959,'Table Saws','!Maintenance, Repair and Operations!Power Tools!Cutting Tools!Table Saws',2608,0,5876937,NULL,NULL),(5876960,'Table Saw Parts and Accessories','!Maintenance, Repair and Operations!Power Tools!Cutting Tools!Table Saws!Table Saw Parts and Accessories',3075,0,5876959,NULL,NULL),(5876961,'Table Saws Portable','!Maintenance, Repair and Operations!Power Tools!Cutting Tools!Table Saws!Table Saws Portable',3076,0,5876959,NULL,NULL),(5876962,'Tile and Concrete Saws','!Maintenance, Repair and Operations!Power Tools!Cutting Tools!Tile and Concrete Saws',2609,0,5876937,NULL,NULL),(5876963,'Concrete Saw Parts and Accessories','!Maintenance, Repair and Operations!Power Tools!Cutting Tools!Tile and Concrete Saws!Concrete Saw Parts and Accessories',3077,0,5876962,NULL,NULL),(5876964,'Concrete Saws Gas','!Maintenance, Repair and Operations!Power Tools!Cutting Tools!Tile and Concrete Saws!Concrete Saws Gas',3078,0,5876962,NULL,NULL),(5876965,'Tile Saws Corded','!Maintenance, Repair and Operations!Power Tools!Cutting Tools!Tile and Concrete Saws!Tile Saws Corded',3079,0,5876962,NULL,NULL),(5876966,'Drilling and Fastening Tools','!Maintenance, Repair and Operations!Power Tools!Drilling and Fastening Tools',1830,0,5876768,NULL,NULL),(5876967,'Crimpers','!Maintenance, Repair and Operations!Power Tools!Drilling and Fastening Tools!Crimpers',2610,0,5876966,NULL,NULL),(5876968,'Crimper Parts and Accessories','!Maintenance, Repair and Operations!Power Tools!Drilling and Fastening Tools!Crimpers!Crimper Parts and Accessories',3080,0,5876967,NULL,NULL),(5876969,'Crimpers Corded','!Maintenance, Repair and Operations!Power Tools!Drilling and Fastening Tools!Crimpers!Crimpers Corded',3081,0,5876967,NULL,NULL),(5876970,'Cutting Tools','!Maintenance, Repair and Operations!Power Tools!Cutting Tools',1831,0,5876768,NULL,NULL),(5876971,'Band Saws','!Maintenance, Repair and Operations!Power Tools!Cutting Tools!Band Saws',2611,0,5876970,NULL,NULL),(5876972,'Band Saws Corded','!Maintenance, Repair and Operations!Power Tools!Cutting Tools!Band Saws!Band Saws Corded',3082,0,5876971,NULL,NULL),(5876973,'Drilling and Fastening Tools','!Maintenance, Repair and Operations!Power Tools!Drilling and Fastening Tools',1832,0,5876768,NULL,NULL),(5876974,'Crimpers','!Maintenance, Repair and Operations!Power Tools!Drilling and Fastening Tools!Crimpers',2612,0,5876973,NULL,NULL),(5876975,'Crimpers Cordless','!Maintenance, Repair and Operations!Power Tools!Drilling and Fastening Tools!Crimpers!Crimpers Cordless',3083,0,5876974,NULL,NULL),(5876976,'Drill Presses','!Maintenance, Repair and Operations!Power Tools!Drilling and Fastening Tools!Drill Presses',2613,0,5876973,NULL,NULL),(5876977,'Drill Press Parts and Accessories','!Maintenance, Repair and Operations!Power Tools!Drilling and Fastening Tools!Drill Presses!Drill Press Parts and Accessories',3084,0,5876976,NULL,NULL),(5876978,'Drill Presses Corded','!Maintenance, Repair and Operations!Power Tools!Drilling and Fastening Tools!Drill Presses!Drill Presses Corded',3085,0,5876976,NULL,NULL),(5876979,'Drills','!Maintenance, Repair and Operations!Power Tools!Drilling and Fastening Tools!Drills',2614,0,5876973,NULL,NULL),(5876980,'Coring Parts and Accessories','!Maintenance, Repair and Operations!Power Tools!Drilling and Fastening Tools!Drills!Coring Parts and Accessories',3086,0,5876979,NULL,NULL),(5876981,'Coring Rigs and Motors','!Maintenance, Repair and Operations!Power Tools!Drilling and Fastening Tools!Drills!Coring Rigs and Motors',3087,0,5876979,NULL,NULL),(5876982,'Drill Parts and Accessories','!Maintenance, Repair and Operations!Power Tools!Drilling and Fastening Tools!Drills!Drill Parts and Accessories',3088,0,5876979,NULL,NULL),(5876983,'Drills Corded','!Maintenance, Repair and Operations!Power Tools!Drilling and Fastening Tools!Drills!Drills Corded',3089,0,5876979,NULL,NULL),(5876984,'Drills Cordless','!Maintenance, Repair and Operations!Power Tools!Drilling and Fastening Tools!Drills!Drills Cordless',3090,0,5876979,NULL,NULL),(5876985,'Mills','!Maintenance, Repair and Operations!Power Tools!Drilling and Fastening Tools!Mills',2615,0,5876973,NULL,NULL),(5876986,'Milling Machines','!Maintenance, Repair and Operations!Power Tools!Drilling and Fastening Tools!Mills!Milling Machines',3091,0,5876985,NULL,NULL),(5876987,'Nailers','!Maintenance, Repair and Operations!Power Tools!Drilling and Fastening Tools!Nailers',2616,0,5876973,NULL,NULL),(5876988,'Nailers Corded','!Maintenance, Repair and Operations!Power Tools!Drilling and Fastening Tools!Nailers!Nailers Corded',3092,0,5876987,NULL,NULL),(5876989,'Cutting Tools','!Maintenance, Repair and Operations!Power Tools!Cutting Tools',1833,0,5876768,NULL,NULL),(5876990,'Band Saws','!Maintenance, Repair and Operations!Power Tools!Cutting Tools!Band Saws',2617,0,5876989,NULL,NULL),(5876991,'Band Saws Cordless','!Maintenance, Repair and Operations!Power Tools!Cutting Tools!Band Saws!Band Saws Cordless',3093,0,5876990,NULL,NULL),(5876992,'Drilling and Fastening Tools','!Maintenance, Repair and Operations!Power Tools!Drilling and Fastening Tools',1834,0,5876768,NULL,NULL),(5876993,'Screwdrivers','!Maintenance, Repair and Operations!Power Tools!Drilling and Fastening Tools!Screwdrivers',2618,0,5876992,NULL,NULL),(5876994,'Screwdrivers Corded','!Maintenance, Repair and Operations!Power Tools!Drilling and Fastening Tools!Screwdrivers!Screwdrivers Corded',3094,0,5876993,NULL,NULL),(5876995,'Screwdrivers Cordless','!Maintenance, Repair and Operations!Power Tools!Drilling and Fastening Tools!Screwdrivers!Screwdrivers Cordless',3095,0,5876993,NULL,NULL),(5876996,'Wrenches','!Maintenance, Repair and Operations!Power Tools!Drilling and Fastening Tools!Wrenches',2619,0,5876992,NULL,NULL),(5876997,'Impact Drivers Cordless','!Maintenance, Repair and Operations!Power Tools!Drilling and Fastening Tools!Wrenches!Impact Drivers Cordless',3096,0,5876996,NULL,NULL),(5876998,'Auger Drill Bit Sets','!Maintenance, Repair and Operations!Power Tools!Drilling and Fastening Tools!Wrenches!Impact Drivers Cordless!Auger Drill Bit Sets',3201,0,5876997,NULL,NULL),(5876999,'Impact Wrench Parts and Accessories','!Maintenance, Repair and Operations!Power Tools!Drilling and Fastening Tools!Wrenches!Impact Wrench Parts and Accessories',3097,0,5876996,NULL,NULL),(5877000,'Auger Drill Bits','!Maintenance, Repair and Operations!Power Tools!Drilling and Fastening Tools!Wrenches!Impact Wrench Parts and Accessories!Auger Drill Bits',3202,0,5876999,NULL,NULL),(5877001,'Impact Wrenches Corded','!Maintenance, Repair and Operations!Power Tools!Drilling and Fastening Tools!Wrenches!Impact Wrenches Corded',3098,0,5876996,NULL,NULL),(5877002,'Carbide Bur Sets','!Maintenance, Repair and Operations!Power Tools!Drilling and Fastening Tools!Wrenches!Impact Wrenches Corded!Carbide Bur Sets',3203,0,5877001,NULL,NULL),(5877003,'Impact Wrenches Cordless','!Maintenance, Repair and Operations!Power Tools!Drilling and Fastening Tools!Wrenches!Impact Wrenches Cordless',3099,0,5876996,NULL,NULL),(5877004,'Carbide Burs','!Maintenance, Repair and Operations!Power Tools!Drilling and Fastening Tools!Wrenches!Impact Wrenches Cordless!Carbide Burs',3204,0,5877003,NULL,NULL),(5877005,'Ratchet Wrenches Cordless','!Maintenance, Repair and Operations!Power Tools!Drilling and Fastening Tools!Wrenches!Ratchet Wrenches Cordless',3100,0,5876996,NULL,NULL),(5877006,'Chucks and Keys','!Maintenance, Repair and Operations!Power Tools!Drilling and Fastening Tools!Wrenches!Ratchet Wrenches Cordless!Chucks and Keys',3205,0,5877005,NULL,NULL),(5877007,'Finishing Tools','!Maintenance, Repair and Operations!Power Tools!Finishing Tools',1835,0,5876768,NULL,NULL),(5877008,'Engravers','!Maintenance, Repair and Operations!Power Tools!Finishing Tools!Engravers',2620,0,5877007,NULL,NULL),(5877009,'Engravers Corded','!Maintenance, Repair and Operations!Power Tools!Finishing Tools!Engravers!Engravers Corded',3101,0,5877008,NULL,NULL),(5877010,'Countersink Drill Bits','!Maintenance, Repair and Operations!Power Tools!Finishing Tools!Engravers!Engravers Corded!Countersink Drill Bits',3206,0,5877009,NULL,NULL),(5877011,'Grinders','!Maintenance, Repair and Operations!Power Tools!Finishing Tools!Grinders',2621,0,5877007,NULL,NULL),(5877012,'Angle Grinder Parts and Accessories','!Maintenance, Repair and Operations!Power Tools!Finishing Tools!Grinders!Angle Grinder Parts and Accessories',3102,0,5877011,NULL,NULL),(5877013,'Drill Bit Sharpeners','!Maintenance, Repair and Operations!Power Tools!Finishing Tools!Grinders!Angle Grinder Parts and Accessories!Drill Bit Sharpeners',3207,0,5877012,NULL,NULL),(5877014,'Angle Grinders Corded','!Maintenance, Repair and Operations!Power Tools!Finishing Tools!Grinders!Angle Grinders Corded',3103,0,5877011,NULL,NULL),(5877015,'Drill Parts and Acces.','!Maintenance, Repair and Operations!Power Tools!Finishing Tools!Grinders!Angle Grinders Corded!Drill Parts and Acces.',3208,0,5877014,NULL,NULL),(5877016,'Cutting Tools','!Maintenance, Repair and Operations!Power Tools!Cutting Tools',1836,0,5876768,NULL,NULL),(5877017,'Band Saws','!Maintenance, Repair and Operations!Power Tools!Cutting Tools!Band Saws',2622,0,5877016,NULL,NULL),(5877018,'Band Saws Horizontal/Vertical Parts and Accessories','!Maintenance, Repair and Operations!Power Tools!Cutting Tools!Band Saws!Band Saws Horizontal/Vertical Parts and Accessories',3104,0,5877017,NULL,NULL),(5877019,'Finishing Tools','!Maintenance, Repair and Operations!Power Tools!Finishing Tools',1837,0,5876768,NULL,NULL),(5877020,'Grinders','!Maintenance, Repair and Operations!Power Tools!Finishing Tools!Grinders',2623,0,5877019,NULL,NULL),(5877021,'Angle Grinders Cordless','!Maintenance, Repair and Operations!Power Tools!Finishing Tools!Grinders!Angle Grinders Cordless',3105,0,5877020,NULL,NULL),(5877022,'Hole Saw Sets','!Maintenance, Repair and Operations!Power Tools!Finishing Tools!Grinders!Angle Grinders Cordless!Hole Saw Sets',3210,0,5877021,NULL,NULL),(5877023,'Belt Grinder Parts and Accessories','!Maintenance, Repair and Operations!Power Tools!Finishing Tools!Grinders!Belt Grinder Parts and Accessories',3106,0,5877020,NULL,NULL),(5877024,'Hole Saws','!Maintenance, Repair and Operations!Power Tools!Finishing Tools!Grinders!Belt Grinder Parts and Accessories!Hole Saws',3211,0,5877023,NULL,NULL),(5877025,'Belt Grinders','!Maintenance, Repair and Operations!Power Tools!Finishing Tools!Grinders!Belt Grinders',3107,0,5877020,NULL,NULL),(5877026,'Installer Drill Bits','!Maintenance, Repair and Operations!Power Tools!Finishing Tools!Grinders!Belt Grinders!Installer Drill Bits',3212,0,5877025,NULL,NULL),(5877027,'Bench Grinder Parts and Accessories','!Maintenance, Repair and Operations!Power Tools!Finishing Tools!Grinders!Bench Grinder Parts and Accessories',3108,0,5877020,NULL,NULL),(5877028,'Nut Setters','!Maintenance, Repair and Operations!Power Tools!Finishing Tools!Grinders!Bench Grinder Parts and Accessories!Nut Setters',3213,0,5877027,NULL,NULL),(5877029,'Bench Grinders','!Maintenance, Repair and Operations!Power Tools!Finishing Tools!Grinders!Bench Grinders',3109,0,5877020,NULL,NULL),(5877030,'Power Bit Extensions and Adapters','!Maintenance, Repair and Operations!Power Tools!Finishing Tools!Grinders!Bench Grinders!Power Bit Extensions and Adapters',3214,0,5877029,NULL,NULL),(5877031,'Die Grinder Parts and Accessories','!Maintenance, Repair and Operations!Power Tools!Finishing Tools!Grinders!Die Grinder Parts and Accessories',3110,0,5877020,NULL,NULL),(5877032,'Power Bit Sets','!Maintenance, Repair and Operations!Power Tools!Finishing Tools!Grinders!Die Grinder Parts and Accessories!Power Bit Sets',3215,0,5877031,NULL,NULL),(5877033,'Die Grinders Corded','!Maintenance, Repair and Operations!Power Tools!Finishing Tools!Grinders!Die Grinders Corded',3111,0,5877020,NULL,NULL),(5877034,'Power Bits','!Maintenance, Repair and Operations!Power Tools!Finishing Tools!Grinders!Die Grinders Corded!Power Bits',3216,0,5877033,NULL,NULL),(5877035,'Die Grinders Cordless','!Maintenance, Repair and Operations!Power Tools!Finishing Tools!Grinders!Die Grinders Cordless',3112,0,5877020,NULL,NULL),(5877036,'Self Feed Bit Sets','!Maintenance, Repair and Operations!Power Tools!Finishing Tools!Grinders!Die Grinders Cordless!Self Feed Bit Sets',3217,0,5877035,NULL,NULL),(5877037,'Straight Grinders Corded','!Maintenance, Repair and Operations!Power Tools!Finishing Tools!Grinders!Straight Grinders Corded',3113,0,5877020,NULL,NULL),(5877038,'Self Feed Bits','!Maintenance, Repair and Operations!Power Tools!Finishing Tools!Grinders!Straight Grinders Corded!Self Feed Bits',3218,0,5877037,NULL,NULL),(5877039,'Planers and Jointers','!Maintenance, Repair and Operations!Power Tools!Finishing Tools!Planers and Jointers',2624,0,5877019,NULL,NULL),(5877040,'Jointer Parts and Accessories','!Maintenance, Repair and Operations!Power Tools!Finishing Tools!Planers and Jointers!Jointer Parts and Accessories',3114,0,5877039,NULL,NULL),(5877041,'Specialty Drill Bits','!Maintenance, Repair and Operations!Power Tools!Finishing Tools!Planers and Jointers!Jointer Parts and Accessories!Specialty Drill Bits',3219,0,5877040,NULL,NULL),(5877042,'Cutting Tools','!Maintenance, Repair and Operations!Power Tools!Cutting Tools',1838,0,5876768,NULL,NULL),(5877043,'Band Saws','!Maintenance, Repair and Operations!Power Tools!Cutting Tools!Band Saws',2625,0,5877042,NULL,NULL),(5877044,'Band Saws Horizontal/Vertical','!Maintenance, Repair and Operations!Power Tools!Cutting Tools!Band Saws!Band Saws Horizontal/Vertical',3115,0,5877043,NULL,NULL),(5877045,'Finishing Tools','!Maintenance, Repair and Operations!Power Tools!Finishing Tools',1839,0,5876768,NULL,NULL),(5877046,'Planers and Jointers','!Maintenance, Repair and Operations!Power Tools!Finishing Tools!Planers and Jointers',2626,0,5877045,NULL,NULL),(5877047,'Jointers Corded','!Maintenance, Repair and Operations!Power Tools!Finishing Tools!Planers and Jointers!Jointers Corded',3116,0,5877046,NULL,NULL),(5877048,'Step Drill Bit Sets','!Maintenance, Repair and Operations!Power Tools!Finishing Tools!Planers and Jointers!Jointers Corded!Step Drill Bit Sets',3221,0,5877047,NULL,NULL),(5877049,'Planer Parts and Accessories','!Maintenance, Repair and Operations!Power Tools!Finishing Tools!Planers and Jointers!Planer Parts and Accessories',3117,0,5877046,NULL,NULL),(5877050,'Step Drill Bits','!Maintenance, Repair and Operations!Power Tools!Finishing Tools!Planers and Jointers!Planer Parts and Accessories!Step Drill Bits',3222,0,5877049,NULL,NULL),(5877051,'Planers Corded','!Maintenance, Repair and Operations!Power Tools!Finishing Tools!Planers and Jointers!Planers Corded',3118,0,5877046,NULL,NULL),(5877052,'Twist Drill Bit Sets','!Maintenance, Repair and Operations!Power Tools!Finishing Tools!Planers and Jointers!Planers Corded!Twist Drill Bit Sets',3223,0,5877051,NULL,NULL),(5877053,'Polishers','!Maintenance, Repair and Operations!Power Tools!Finishing Tools!Polishers',2627,0,5877045,NULL,NULL),(5877054,'Polisher Parts and Accessories','!Maintenance, Repair and Operations!Power Tools!Finishing Tools!Polishers!Polisher Parts and Accessories',3119,0,5877053,NULL,NULL),(5877055,'Twist Drill Bits','!Maintenance, Repair and Operations!Power Tools!Finishing Tools!Polishers!Polisher Parts and Accessories!Twist Drill Bits',3224,0,5877054,NULL,NULL),(5877056,'Polishers Corded','!Maintenance, Repair and Operations!Power Tools!Finishing Tools!Polishers!Polishers Corded',3120,0,5877053,NULL,NULL),(5877057,'Wood Boring Drill Bit Sets','!Maintenance, Repair and Operations!Power Tools!Finishing Tools!Polishers!Polishers Corded!Wood Boring Drill Bit Sets',3225,0,5877056,NULL,NULL),(5877058,'Rotary Tools','!Maintenance, Repair and Operations!Power Tools!Finishing Tools!Rotary Tools',2628,0,5877045,NULL,NULL),(5877059,'Rotary Tool Parts and Accessories','!Maintenance, Repair and Operations!Power Tools!Finishing Tools!Rotary Tools!Rotary Tool Parts and Accessories',3121,0,5877058,NULL,NULL),(5877060,'Wood Boring Drill Bits','!Maintenance, Repair and Operations!Power Tools!Finishing Tools!Rotary Tools!Rotary Tool Parts and Accessories!Wood Boring Drill Bits',3226,0,5877059,NULL,NULL),(5877061,'Rotary Tools Corded','!Maintenance, Repair and Operations!Power Tools!Finishing Tools!Rotary Tools!Rotary Tools Corded',3122,0,5877058,NULL,NULL),(5877062,'Rotary Tools Cordless','!Maintenance, Repair and Operations!Power Tools!Finishing Tools!Rotary Tools!Rotary Tools Cordless',3123,0,5877058,NULL,NULL),(5877063,'Sanders','!Maintenance, Repair and Operations!Power Tools!Finishing Tools!Sanders',2629,0,5877045,NULL,NULL),(5877064,'Belt Sander Parts and Accessories','!Maintenance, Repair and Operations!Power Tools!Finishing Tools!Sanders!Belt Sander Parts and Accessories',3124,0,5877063,NULL,NULL),(5877065,'Belt Sanders Corded','!Maintenance, Repair and Operations!Power Tools!Finishing Tools!Sanders!Belt Sanders Corded',3125,0,5877063,NULL,NULL),(5877066,'Cutting Tools','!Maintenance, Repair and Operations!Power Tools!Cutting Tools',1840,0,5876768,NULL,NULL),(5877067,'Chain Saws','!Maintenance, Repair and Operations!Power Tools!Cutting Tools!Chain Saws',2630,0,5877066,NULL,NULL),(5877068,'Chain Saw Parts and Accessories','!Maintenance, Repair and Operations!Power Tools!Cutting Tools!Chain Saws!Chain Saw Parts and Accessories',3126,0,5877067,NULL,NULL),(5877069,'Finishing Tools','!Maintenance, Repair and Operations!Power Tools!Finishing Tools',1841,0,5876768,NULL,NULL),(5877070,'Sanders','!Maintenance, Repair and Operations!Power Tools!Finishing Tools!Sanders',2631,0,5877069,NULL,NULL),(5877071,'Disc Sander Parts and Accessories','!Maintenance, Repair and Operations!Power Tools!Finishing Tools!Sanders!Disc Sander Parts and Accessories',3127,0,5877070,NULL,NULL),(5877072,'Disc Sanders Corded','!Maintenance, Repair and Operations!Power Tools!Finishing Tools!Sanders!Disc Sanders Corded',3128,0,5877070,NULL,NULL),(5877073,'Sheet Sander Parts and Accessories','!Maintenance, Repair and Operations!Power Tools!Finishing Tools!Sanders!Sheet Sander Parts and Accessories',3129,0,5877070,NULL,NULL),(5877074,'Sheet Sanders Corded','!Maintenance, Repair and Operations!Power Tools!Finishing Tools!Sanders!Sheet Sanders Corded',3130,0,5877070,NULL,NULL),(5877075,'Specialty Sanders Corded','!Maintenance, Repair and Operations!Power Tools!Finishing Tools!Sanders!Specialty Sanders Corded',3131,0,5877070,NULL,NULL),(5877076,'Spindle Sanders Corded','!Maintenance, Repair and Operations!Power Tools!Finishing Tools!Sanders!Spindle Sanders Corded',3132,0,5877070,NULL,NULL),(5877077,'Hammer Drilling and Demolition Tools','!Maintenance, Repair and Operations!Power Tools!Hammer Drilling and Demolition Tools',1842,0,5876768,NULL,NULL),(5877078,'Combination Hammers','!Maintenance, Repair and Operations!Power Tools!Hammer Drilling and Demolition Tools!Combination Hammers',2632,0,5877077,NULL,NULL),(5877079,'Combination Hammers Corded','!Maintenance, Repair and Operations!Power Tools!Hammer Drilling and Demolition Tools!Combination Hammers!Combination Hammers Corded',3133,0,5877078,NULL,NULL),(5877080,'Demolition Hammers','!Maintenance, Repair and Operations!Power Tools!Hammer Drilling and Demolition Tools!Demolition Hammers',2633,0,5877077,NULL,NULL),(5877081,'Chisels and Steel','!Maintenance, Repair and Operations!Power Tools!Hammer Drilling and Demolition Tools!Demolition Hammers!Chisels and Steel',3134,0,5877080,NULL,NULL),(5877082,'Demolition Hammer Parts and Accessories','!Maintenance, Repair and Operations!Power Tools!Hammer Drilling and Demolition Tools!Demolition Hammers!Demolition Hammer Parts and Accessories',3135,0,5877080,NULL,NULL),(5877083,'Demolition Hammers Corded','!Maintenance, Repair and Operations!Power Tools!Hammer Drilling and Demolition Tools!Demolition Hammers!Demolition Hammers Corded',3136,0,5877080,NULL,NULL),(5877084,'Pumps and Power Transmission','!Maintenance, Repair and Operations!Pumps and Power Transmission',1527,0,5875888,NULL,NULL),(5877085,'Fuel Pumps','!Maintenance, Repair and Operations!Pumps and Power Transmission!Fuel Pumps',1844,0,5877084,NULL,NULL),(5877086,'Fuel Pumps','!Maintenance, Repair and Operations!Pumps and Power Transmission!Fuel Pumps!Fuel Pumps',2635,0,5877085,NULL,NULL),(5877087,'Grease and Oil Pump Parts and Accessories','!Maintenance, Repair and Operations!Pumps and Power Transmission!Grease and Oil Pump Parts and Accessories',1845,0,5877084,NULL,NULL),(5877088,'Grease and Oil Pump Parts and Accessories','!Maintenance, Repair and Operations!Pumps and Power Transmission!Grease and Oil Pump Parts and Accessories!Grease and Oil Pump Parts and Accessories',2636,0,5877087,NULL,NULL),(5877089,'Grease and Oil Pumps','!Maintenance, Repair and Operations!Pumps and Power Transmission!Grease and Oil Pumps',1846,0,5877084,NULL,NULL),(5877090,'Grease and Oil Pumps','!Maintenance, Repair and Operations!Pumps and Power Transmission!Grease and Oil Pumps!Grease and Oil Pumps',2638,0,5877089,NULL,NULL),(5877091,'Rotary Pump Parts and Accessories','!Maintenance, Repair and Operations!Pumps and Power Transmission!Rotary Pump Parts and Accessories',1847,0,5877084,NULL,NULL),(5877092,'Rotary Pump Parts and Accessories','!Maintenance, Repair and Operations!Pumps and Power Transmission!Rotary Pump Parts and Accessories!Rotary Pump Parts and Accessories',2640,0,5877091,NULL,NULL),(5877093,'Rotary Pumps','!Maintenance, Repair and Operations!Pumps and Power Transmission!Rotary Pumps',1848,0,5877084,NULL,NULL),(5877094,'Rotary Pumps','!Maintenance, Repair and Operations!Pumps and Power Transmission!Rotary Pumps!Rotary Pumps',2641,0,5877093,NULL,NULL),(5877095,'Test Pumps','!Maintenance, Repair and Operations!Pumps and Power Transmission!Test Pumps',1849,0,5877084,NULL,NULL),(5877096,'Test Pumps','!Maintenance, Repair and Operations!Pumps and Power Transmission!Test Pumps!Test Pumps',2642,0,5877095,NULL,NULL),(5877097,'Water and Solvent Pumps','!Maintenance, Repair and Operations!Pumps and Power Transmission!Water and Solvent Pumps',1850,0,5877084,NULL,NULL),(5877098,'Water and Solvent Pumps','!Maintenance, Repair and Operations!Pumps and Power Transmission!Water and Solvent Pumps!Water and Solvent Pumps',2643,0,5877097,NULL,NULL),(5877099,'Scrapers and Knives','!Maintenance, Repair and Operations!Scrapers and Knives',1528,0,5875888,NULL,NULL),(5877100,'Pumps and Power Transmission','!Maintenance, Repair and Operations!Pumps and Power Transmission',1529,0,5875888,NULL,NULL),(5877101,'Centrifugal Pumps','!Maintenance, Repair and Operations!Pumps and Power Transmission!Centrifugal Pumps',1852,0,5877100,NULL,NULL),(5877102,'Centrifugal Pumps','!Maintenance, Repair and Operations!Pumps and Power Transmission!Centrifugal Pumps!Centrifugal Pumps',2645,0,5877101,NULL,NULL),(5877103,'Trash Pumps','!Maintenance, Repair and Operations!Pumps and Power Transmission!Centrifugal Pumps!Centrifugal Pumps!Trash Pumps',3138,0,5877102,NULL,NULL),(5877104,'Water Pumps','!Maintenance, Repair and Operations!Pumps and Power Transmission!Centrifugal Pumps!Centrifugal Pumps!Water Pumps',3139,0,5877102,NULL,NULL),(5877105,'Diaphragm Pump Parts and Accessories','!Maintenance, Repair and Operations!Pumps and Power Transmission!Diaphragm Pump Parts and Accessories',1853,0,5877100,NULL,NULL),(5877106,'Diaphragm Pump Parts and Accessories','!Maintenance, Repair and Operations!Pumps and Power Transmission!Diaphragm Pump Parts and Accessories!Diaphragm Pump Parts and Accessories',2647,0,5877105,NULL,NULL),(5877107,'Diaphragm Pumps','!Maintenance, Repair and Operations!Pumps and Power Transmission!Diaphragm Pumps',1854,0,5877100,NULL,NULL),(5877108,'Diaphragm Pumps','!Maintenance, Repair and Operations!Pumps and Power Transmission!Diaphragm Pumps!Diaphragm Pumps',2649,0,5877107,NULL,NULL),(5877109,'Fuel Pump Accessories','!Maintenance, Repair and Operations!Pumps and Power Transmission!Fuel Pump Accessories',1855,0,5877100,NULL,NULL),(5877110,'Fuel Pump Accessories','!Maintenance, Repair and Operations!Pumps and Power Transmission!Fuel Pump Accessories!Fuel Pump Accessories',2650,0,5877109,NULL,NULL),(5877111,'Fuel Pumps','!Maintenance, Repair and Operations!Pumps and Power Transmission!Fuel Pumps',1856,0,5877100,NULL,NULL),(5877112,'Testing, Measuring, and Leveling Tools','!Maintenance, Repair and Operations!Testing, Measuring, and Leveling Tools',1530,0,5875888,NULL,NULL),(5877113,'Dial and Digital Indicator Parts and Accessories','!Maintenance, Repair and Operations!Testing, Measuring, and Leveling Tools!Dial and Digital Indicator Parts and Accessories',1858,0,5877112,NULL,NULL),(5877114,'Dial and Digital Indicators','!Maintenance, Repair and Operations!Testing, Measuring, and Leveling Tools!Dial and Digital Indicators',1859,0,5877112,NULL,NULL),(5877115,'Finders and Scanners','!Maintenance, Repair and Operations!Testing, Measuring, and Leveling Tools!Finders and Scanners',1860,0,5877112,NULL,NULL),(5877116,'Flange Pins','!Maintenance, Repair and Operations!Testing, Measuring, and Leveling Tools!Flange Pins',1861,0,5877112,NULL,NULL),(5877117,'Flange Pins and Aligners','!Maintenance, Repair and Operations!Testing, Measuring, and Leveling Tools!Flange Pins and Aligners',1862,0,5877112,NULL,NULL),(5877118,'Gauges/Gages','!Maintenance, Repair and Operations!Testing, Measuring, and Leveling Tools!Gauges/Gages',1863,0,5877112,NULL,NULL),(5877119,'Depth and Height Gages','!Maintenance, Repair and Operations!Testing, Measuring, and Leveling Tools!Gauges/Gages!Depth and Height Gages',2652,0,5877118,NULL,NULL),(5877120,'Drill Gages','!Maintenance, Repair and Operations!Testing, Measuring, and Leveling Tools!Gauges/Gages!Drill Gages',2653,0,5877118,NULL,NULL),(5877121,'Feeler and Pitch Gauge Sets','!Maintenance, Repair and Operations!Testing, Measuring, and Leveling Tools!Gauges/Gages!Feeler and Pitch Gauge Sets',2654,0,5877118,NULL,NULL),(5877122,'Feeler and Pitch Gauges','!Maintenance, Repair and Operations!Testing, Measuring, and Leveling Tools!Gauges/Gages!Feeler and Pitch Gauges',2655,0,5877118,NULL,NULL),(5877123,'Pressure Gauges','!Maintenance, Repair and Operations!Testing, Measuring, and Leveling Tools!Gauges/Gages!Pressure Gauges',2656,0,5877118,NULL,NULL),(5877124,'Caliper and Divider Parts and Accessories','!Maintenance, Repair and Operations!Testing, Measuring, and Leveling Tools!Caliper and Divider Parts and Accessories',1864,0,5877112,NULL,NULL),(5877125,'Gauges/Gages','!Maintenance, Repair and Operations!Testing, Measuring, and Leveling Tools!Gauges/Gages',1865,0,5877112,NULL,NULL),(5877126,'Specialty Gages','!Maintenance, Repair and Operations!Testing, Measuring, and Leveling Tools!Gauges/Gages!Specialty Gages',2658,0,5877125,NULL,NULL),(5877127,'Telescoping Gages','!Maintenance, Repair and Operations!Testing, Measuring, and Leveling Tools!Gauges/Gages!Telescoping Gages',2659,0,5877125,NULL,NULL),(5877128,'Wire Gages','!Maintenance, Repair and Operations!Testing, Measuring, and Leveling Tools!Gauges/Gages!Wire Gages',2660,0,5877125,NULL,NULL),(5877129,'Gauging Tape Refills and Parts','!Maintenance, Repair and Operations!Testing, Measuring, and Leveling Tools!Gauging Tape Refills and Parts',1866,0,5877112,NULL,NULL),(5877130,'Gauging Tape Wipers','!Maintenance, Repair and Operations!Testing, Measuring, and Leveling Tools!Gauging Tape Wipers',1867,0,5877112,NULL,NULL),(5877131,'Gauging Tapes','!Maintenance, Repair and Operations!Testing, Measuring, and Leveling Tools!Gauging Tapes',1868,0,5877112,NULL,NULL),(5877132,'Indicator Holders, Bases and Stands','!Maintenance, Repair and Operations!Testing, Measuring, and Leveling Tools!Indicator Holders, Bases and Stands',1869,0,5877112,NULL,NULL),(5877133,'Inspection Mirror Refills','!Maintenance, Repair and Operations!Testing, Measuring, and Leveling Tools!Inspection Mirror Refills',1870,0,5877112,NULL,NULL),(5877134,'Inspection Mirrors and Magnifiers','!Maintenance, Repair and Operations!Testing, Measuring, and Leveling Tools!Inspection Mirrors and Magnifiers',1871,0,5877112,NULL,NULL),(5877135,'Level Parts and Accessories','!Maintenance, Repair and Operations!Testing, Measuring, and Leveling Tools!Level Parts and Accessories',1872,0,5877112,NULL,NULL),(5877136,'Caliper and Divider Sets','!Maintenance, Repair and Operations!Testing, Measuring, and Leveling Tools!Caliper and Divider Sets',1873,0,5877112,NULL,NULL),(5877137,'Levels','!Maintenance, Repair and Operations!Testing, Measuring, and Leveling Tools!Levels',1874,0,5877112,NULL,NULL),(5877138,'Measuring Wheels','!Maintenance, Repair and Operations!Testing, Measuring, and Leveling Tools!Measuring Wheels',1875,0,5877112,NULL,NULL),(5877139,'Micrometer Parts and Accessories','!Maintenance, Repair and Operations!Testing, Measuring, and Leveling Tools!Micrometer Parts and Accessories',1876,0,5877112,NULL,NULL),(5877140,'Micrometer Sets','!Maintenance, Repair and Operations!Testing, Measuring, and Leveling Tools!Micrometer Sets',1877,0,5877112,NULL,NULL),(5877141,'Micrometers','!Maintenance, Repair and Operations!Testing, Measuring, and Leveling Tools!Micrometers',1878,0,5877112,NULL,NULL),(5877142,'Plumb Bobs','!Maintenance, Repair and Operations!Testing, Measuring, and Leveling Tools!Plumb Bobs',1879,0,5877112,NULL,NULL),(5877143,'Protractors','!Maintenance, Repair and Operations!Testing, Measuring, and Leveling Tools!Protractors',1880,0,5877112,NULL,NULL),(5877144,'Radius Markers','!Maintenance, Repair and Operations!Testing, Measuring, and Leveling Tools!Radius Markers',1881,0,5877112,NULL,NULL),(5877145,'Rulers','!Maintenance, Repair and Operations!Testing, Measuring, and Leveling Tools!Rulers',1882,0,5877112,NULL,NULL),(5877146,'Square Parts and Accessories','!Maintenance, Repair and Operations!Testing, Measuring, and Leveling Tools!Square Parts and Accessories',1883,0,5877112,NULL,NULL),(5877147,'Caliper and Micrometer Sets','!Maintenance, Repair and Operations!Testing, Measuring, and Leveling Tools!Caliper and Micrometer Sets',1884,0,5877112,NULL,NULL),(5877148,'Squares','!Maintenance, Repair and Operations!Testing, Measuring, and Leveling Tools!Squares',1885,0,5877112,NULL,NULL),(5877149,'Tape Measure Parts and Accessories','!Maintenance, Repair and Operations!Testing, Measuring, and Leveling Tools!Tape Measure Parts and Accessories',1886,0,5877112,NULL,NULL),(5877150,'Tape Measures','!Maintenance, Repair and Operations!Testing, Measuring, and Leveling Tools!Tape Measures',1887,0,5877112,NULL,NULL),(5877151,'Thermometers','!Maintenance, Repair and Operations!Testing, Measuring, and Leveling Tools!Thermometers',1888,0,5877112,NULL,NULL),(5877152,'Wiggler Sets','!Maintenance, Repair and Operations!Testing, Measuring, and Leveling Tools!Wiggler Sets',1889,0,5877112,NULL,NULL),(5877153,'Calipers and Dividers','!Maintenance, Repair and Operations!Testing, Measuring, and Leveling Tools!Calipers and Dividers',1890,0,5877112,NULL,NULL),(5877154,'Contour Marker Parts and Accessories','!Maintenance, Repair and Operations!Testing, Measuring, and Leveling Tools!Contour Marker Parts and Accessories',1891,0,5877112,NULL,NULL),(5877155,'Contour Markers','!Maintenance, Repair and Operations!Testing, Measuring, and Leveling Tools!Contour Markers',1892,0,5877112,NULL,NULL),(5877156,'Derrick Tape Refills','!Maintenance, Repair and Operations!Testing, Measuring, and Leveling Tools!Derrick Tape Refills',1893,0,5877112,NULL,NULL),(5877157,'Derrick Tapes','!Maintenance, Repair and Operations!Testing, Measuring, and Leveling Tools!Derrick Tapes',1894,0,5877112,NULL,NULL),(5877158,'Tubing','!Maintenance, Repair and Operations!Tubing',1531,0,5875888,NULL,NULL),(5877159,'Copper Tubing','!Maintenance, Repair and Operations!Tubing!Copper Tubing',1895,0,5877158,NULL,NULL),(5877160,'Welding Supplies','!Maintenance, Repair and Operations!Welding Supplies',1532,0,5875888,NULL,NULL),(5877161,'Exothermic Cutting','!Maintenance, Repair and Operations!Welding Supplies!Exothermic Cutting',1896,0,5877160,NULL,NULL),(5877162,'Exothermic Rods','!Maintenance, Repair and Operations!Welding Supplies!Exothermic Cutting!Exothermic Rods',2662,0,5877161,NULL,NULL),(5877163,'Exothermic Torches and Kits','!Maintenance, Repair and Operations!Welding Supplies!Exothermic Cutting!Exothermic Torches and Kits',2663,0,5877161,NULL,NULL),(5877164,'Filler Metals','!Maintenance, Repair and Operations!Welding Supplies!Filler Metals',1897,0,5877160,NULL,NULL),(5877165,'Mig/Tig Welding Wires','!Maintenance, Repair and Operations!Welding Supplies!Filler Metals!Mig/Tig Welding Wires',2664,0,5877164,NULL,NULL),(5877166,'Solder Alloys and Fluxes','!Maintenance, Repair and Operations!Welding Supplies!Filler Metals!Solder Alloys and Fluxes',2665,0,5877164,NULL,NULL),(5877167,'Welding/Brazing Alloys and Fluxes','!Maintenance, Repair and Operations!Welding Supplies!Filler Metals!Welding/Brazing Alloys and Fluxes',2666,0,5877164,NULL,NULL),(5877168,'Gas Cutting and Welding','!Maintenance, Repair and Operations!Welding Supplies!Gas Cutting and Welding',1898,0,5877160,NULL,NULL),(5877169,'Check Valves','!Maintenance, Repair and Operations!Welding Supplies!Gas Cutting and Welding!Check Valves',2667,0,5877168,NULL,NULL),(5877170,'Cutting Machines','!Maintenance, Repair and Operations!Welding Supplies!Gas Cutting and Welding!Cutting Machines',2668,0,5877168,NULL,NULL),(5877171,'Flash Arrestors','!Maintenance, Repair and Operations!Welding Supplies!Gas Cutting and Welding!Flash Arrestors',2669,0,5877168,NULL,NULL),(5877172,'Outfits','!Maintenance, Repair and Operations!Welding Supplies!Gas Cutting and Welding!Outfits',2670,0,5877168,NULL,NULL),(5877173,'Quick Connects','!Maintenance, Repair and Operations!Welding Supplies!Gas Cutting and Welding!Quick Connects',2671,0,5877168,NULL,NULL),(5877174,'Tubing','!Maintenance, Repair and Operations!Tubing',1533,0,5875888,NULL,NULL),(5877175,'Gage Glass','!Maintenance, Repair and Operations!Tubing!Gage Glass',1899,0,5877174,NULL,NULL),(5877176,'Welding Supplies','!Maintenance, Repair and Operations!Welding Supplies',1534,0,5875888,NULL,NULL),(5877177,'Gas Cutting and Welding','!Maintenance, Repair and Operations!Welding Supplies!Gas Cutting and Welding',1900,0,5877176,NULL,NULL),(5877178,'Regulator and Flowmeter Parts','!Maintenance, Repair and Operations!Welding Supplies!Gas Cutting and Welding!Regulator and Flowmeter Parts',2673,0,5877177,NULL,NULL),(5877179,'Regulators and Flowmeters','!Maintenance, Repair and Operations!Welding Supplies!Gas Cutting and Welding!Regulators and Flowmeters',2674,0,5877177,NULL,NULL),(5877180,'Tips and Nozzles','!Maintenance, Repair and Operations!Welding Supplies!Gas Cutting and Welding!Tips and Nozzles',2675,0,5877177,NULL,NULL),(5877181,'Torches and Handles','!Maintenance, Repair and Operations!Welding Supplies!Gas Cutting and Welding!Torches and Handles',2676,0,5877177,NULL,NULL),(5877182,'Manual Welding','!Maintenance, Repair and Operations!Welding Supplies!Manual Welding',1901,0,5877176,NULL,NULL),(5877183,'Electrode Holder Parts','!Maintenance, Repair and Operations!Welding Supplies!Manual Welding!Electrode Holder Parts',2677,0,5877182,NULL,NULL),(5877184,'Electrode Holders','!Maintenance, Repair and Operations!Welding Supplies!Manual Welding!Electrode Holders',2678,0,5877182,NULL,NULL),(5877185,'Mig Welding','!Maintenance, Repair and Operations!Welding Supplies!Mig Welding',1902,0,5877176,NULL,NULL),(5877186,'Mig Gun Parts and Accessories','!Maintenance, Repair and Operations!Welding Supplies!Mig Welding!Mig Gun Parts and Accessories',2679,0,5877185,NULL,NULL),(5877187,'Mig Contact Tips','!Maintenance, Repair and Operations!Welding Supplies!Mig Welding!Mig Gun Parts and Accessories!Mig Contact Tips',3141,0,5877186,NULL,NULL),(5877188,'Mig Goose Necks','!Maintenance, Repair and Operations!Welding Supplies!Mig Welding!Mig Gun Parts and Accessories!Mig Goose Necks',3142,0,5877186,NULL,NULL),(5877189,'Mig Insulators and Diffusers','!Maintenance, Repair and Operations!Welding Supplies!Mig Welding!Mig Gun Parts and Accessories!Mig Insulators and Diffusers',3143,0,5877186,NULL,NULL),(5877190,'Mig Liners','!Maintenance, Repair and Operations!Welding Supplies!Mig Welding!Mig Gun Parts and Accessories!Mig Liners',3144,0,5877186,NULL,NULL),(5877191,'Tubing','!Maintenance, Repair and Operations!Tubing',1535,0,5875888,NULL,NULL),(5877192,'Gage Glass Washers','!Maintenance, Repair and Operations!Tubing!Gage Glass Washers',1903,0,5877191,NULL,NULL),(5877193,'Welding Supplies','!Maintenance, Repair and Operations!Welding Supplies',1536,0,5875888,NULL,NULL),(5877194,'Mig Welding','!Maintenance, Repair and Operations!Welding Supplies!Mig Welding',1904,0,5877193,NULL,NULL),(5877195,'Mig Gun Parts and Accessories','!Maintenance, Repair and Operations!Welding Supplies!Mig Welding!Mig Gun Parts and Accessories',2681,0,5877194,NULL,NULL),(5877196,'Mig Nozzle Cleaners','!Maintenance, Repair and Operations!Welding Supplies!Mig Welding!Mig Gun Parts and Accessories!Mig Nozzle Cleaners',3146,0,5877195,NULL,NULL),(5877197,'Mig Nozzles','!Maintenance, Repair and Operations!Welding Supplies!Mig Welding!Mig Gun Parts and Accessories!Mig Nozzles',3147,0,5877195,NULL,NULL),(5877198,'Mig Welding Parts','!Maintenance, Repair and Operations!Welding Supplies!Mig Welding!Mig Gun Parts and Accessories!Mig Welding Parts',3148,0,5877195,NULL,NULL),(5877199,'Mig Guns','!Maintenance, Repair and Operations!Welding Supplies!Mig Welding!Mig Guns',2682,0,5877194,NULL,NULL),(5877200,'Other','!Maintenance, Repair and Operations!Welding Supplies!Other',1905,0,5877193,NULL,NULL),(5877201,'Plasma Cutting','!Maintenance, Repair and Operations!Welding Supplies!Plasma Cutting',1906,0,5877193,NULL,NULL),(5877202,'Plasma Parts','!Maintenance, Repair and Operations!Welding Supplies!Plasma Cutting!Plasma Parts',2684,0,5877201,NULL,NULL),(5877203,'Plasma Baffles','!Maintenance, Repair and Operations!Welding Supplies!Plasma Cutting!Plasma Parts!Plasma Baffles',3150,0,5877202,NULL,NULL),(5877204,'Plasma Caps','!Maintenance, Repair and Operations!Welding Supplies!Plasma Cutting!Plasma Parts!Plasma Caps',3151,0,5877202,NULL,NULL),(5877205,'Plasma Cups','!Maintenance, Repair and Operations!Welding Supplies!Plasma Cutting!Plasma Parts!Plasma Cups',3152,0,5877202,NULL,NULL),(5877206,'Plasma Deflectors','!Maintenance, Repair and Operations!Welding Supplies!Plasma Cutting!Plasma Parts!Plasma Deflectors',3153,0,5877202,NULL,NULL),(5877207,'Plasma Diffusers','!Maintenance, Repair and Operations!Welding Supplies!Plasma Cutting!Plasma Parts!Plasma Diffusers',3154,0,5877202,NULL,NULL),(5877208,'Valves and Regulators','!Maintenance, Repair and Operations!Valves and Regulators',1537,0,5875888,NULL,NULL),(5877209,'Regulators, Filters, and Lubricators','!Maintenance, Repair and Operations!Valves and Regulators!Regulators, Filters, and Lubricators',1907,0,5877208,NULL,NULL),(5877210,'Welding Supplies','!Maintenance, Repair and Operations!Welding Supplies',1538,0,5875888,NULL,NULL),(5877211,'Plasma Cutting','!Maintenance, Repair and Operations!Welding Supplies!Plasma Cutting',1908,0,5877210,NULL,NULL),(5877212,'Plasma Parts','!Maintenance, Repair and Operations!Welding Supplies!Plasma Cutting!Plasma Parts',2686,0,5877211,NULL,NULL),(5877213,'Plasma Electrodes','!Maintenance, Repair and Operations!Welding Supplies!Plasma Cutting!Plasma Parts!Plasma Electrodes',3156,0,5877212,NULL,NULL),(5877214,'Plasma Insulators','!Maintenance, Repair and Operations!Welding Supplies!Plasma Cutting!Plasma Parts!Plasma Insulators',3157,0,5877212,NULL,NULL),(5877215,'Plasma Miscellaneous Parts','!Maintenance, Repair and Operations!Welding Supplies!Plasma Cutting!Plasma Parts!Plasma Miscellaneous Parts',3158,0,5877212,NULL,NULL),(5877216,'Plasma Nozzles','!Maintenance, Repair and Operations!Welding Supplies!Plasma Cutting!Plasma Parts!Plasma Nozzles',3159,0,5877212,NULL,NULL),(5877217,'Plasma Rings','!Maintenance, Repair and Operations!Welding Supplies!Plasma Cutting!Plasma Parts!Plasma Rings',3160,0,5877212,NULL,NULL),(5877218,'Plasma Shields','!Maintenance, Repair and Operations!Welding Supplies!Plasma Cutting!Plasma Parts!Plasma Shields',3161,0,5877212,NULL,NULL),(5877219,'Plasma Tips','!Maintenance, Repair and Operations!Welding Supplies!Plasma Cutting!Plasma Parts!Plasma Tips',3162,0,5877212,NULL,NULL),(5877220,'Plasma Torches','!Maintenance, Repair and Operations!Welding Supplies!Plasma Cutting!Plasma Torches',2687,0,5877211,NULL,NULL),(5877221,'Soldering and Brazing','!Maintenance, Repair and Operations!Welding Supplies!Soldering and Brazing',1909,0,5877210,NULL,NULL),(5877222,'Soldering/Brazing Kits','!Maintenance, Repair and Operations!Welding Supplies!Soldering and Brazing!Soldering/Brazing Kits',2688,0,5877221,NULL,NULL),(5877223,'Soldering/Brazing Tips','!Maintenance, Repair and Operations!Welding Supplies!Soldering and Brazing!Soldering/Brazing Tips',2689,0,5877221,NULL,NULL),(5877224,'Valves and Regulators','!Maintenance, Repair and Operations!Valves and Regulators',1539,0,5875888,NULL,NULL),(5877225,'Valves','!Maintenance, Repair and Operations!Valves and Regulators!Valves',1910,0,5877224,NULL,NULL),(5877226,'Welding Supplies','!Maintenance, Repair and Operations!Welding Supplies',1540,0,5875888,NULL,NULL),(5877227,'Soldering and Brazing','!Maintenance, Repair and Operations!Welding Supplies!Soldering and Brazing',1911,0,5877226,NULL,NULL),(5877228,'Soldering/Brazing Torches','!Maintenance, Repair and Operations!Welding Supplies!Soldering and Brazing!Soldering/Brazing Torches',2691,0,5877227,NULL,NULL),(5877229,'Tig Welding','!Maintenance, Repair and Operations!Welding Supplies!Tig Welding',1912,0,5877226,NULL,NULL),(5877230,'Tig Torch Parts and Accessories','!Maintenance, Repair and Operations!Welding Supplies!Tig Welding!Tig Torch Parts and Accessories',2692,0,5877229,NULL,NULL),(5877231,'Tig Accessory Kits','!Maintenance, Repair and Operations!Welding Supplies!Tig Welding!Tig Torch Parts and Accessories!Tig Accessory Kits',3164,0,5877230,NULL,NULL),(5877232,'Tig Adaptors','!Maintenance, Repair and Operations!Welding Supplies!Tig Welding!Tig Torch Parts and Accessories!Tig Adaptors',3165,0,5877230,NULL,NULL),(5877233,'Tig Back Caps','!Maintenance, Repair and Operations!Welding Supplies!Tig Welding!Tig Torch Parts and Accessories!Tig Back Caps',3166,0,5877230,NULL,NULL),(5877234,'Tig Collets and Collet Bodies','!Maintenance, Repair and Operations!Welding Supplies!Tig Welding!Tig Torch Parts and Accessories!Tig Collets and Collet Bodies',3167,0,5877230,NULL,NULL),(5877235,'Tig Cups','!Maintenance, Repair and Operations!Welding Supplies!Tig Welding!Tig Torch Parts and Accessories!Tig Cups',3168,0,5877230,NULL,NULL),(5877236,'Tig Insulators','!Maintenance, Repair and Operations!Welding Supplies!Tig Welding!Tig Torch Parts and Accessories!Tig Insulators',3169,0,5877230,NULL,NULL),(5877237,'Tig Power Cables and Hoses','!Maintenance, Repair and Operations!Welding Supplies!Tig Welding!Tig Torch Parts and Accessories!Tig Power Cables and Hoses',3170,0,5877230,NULL,NULL),(5877238,'Tig Tungsten Electrodes','!Maintenance, Repair and Operations!Welding Supplies!Tig Welding!Tig Torch Parts and Accessories!Tig Tungsten Electrodes',3171,0,5877230,NULL,NULL),(5877239,'Tig Welding Parts and Accessories','!Maintenance, Repair and Operations!Welding Supplies!Tig Welding!Tig Torch Parts and Accessories!Tig Welding Parts and Accessories',3172,0,5877230,NULL,NULL),(5877240,'Tig Welding','!Maintenance, Repair and Operations!Welding Supplies!Tig Welding',1914,0,5877226,NULL,NULL),(5877241,'Tig Torches and Torch Bodies','!Maintenance, Repair and Operations!Welding Supplies!Tig Welding!Tig Torches and Torch Bodies',2694,0,5877240,NULL,NULL),(5877242,'Torch and Weed Burners','!Maintenance, Repair and Operations!Welding Supplies!Torch and Weed Burners',1915,0,5877226,NULL,NULL),(5877243,'Underwater Cutting and Welding','!Maintenance, Repair and Operations!Welding Supplies!Underwater Cutting and Welding',1916,0,5877226,NULL,NULL),(5877244,'Underwater Electrodes','!Maintenance, Repair and Operations!Welding Supplies!Underwater Cutting and Welding!Underwater Electrodes',2696,0,5877243,NULL,NULL),(5877245,'Welding and Cutting Accessories','!Maintenance, Repair and Operations!Welding Supplies!Welding and Cutting Accessories',1917,0,5877226,NULL,NULL),(5877246,'Anti-Spatter and Chemicals','!Maintenance, Repair and Operations!Welding Supplies!Welding and Cutting Accessories!Anti-Spatter and Chemicals',2697,0,5877245,NULL,NULL),(5877247,'Cable Connectors and Lugs','!Maintenance, Repair and Operations!Welding Supplies!Welding and Cutting Accessories!Cable Connectors and Lugs',2698,0,5877245,NULL,NULL),(5877248,'Cable Covers','!Maintenance, Repair and Operations!Welding Supplies!Welding and Cutting Accessories!Cable Covers',2699,0,5877245,NULL,NULL),(5877249,'Centering Heads','!Maintenance, Repair and Operations!Welding Supplies!Welding and Cutting Accessories!Centering Heads',2700,0,5877245,NULL,NULL),(5877250,'Cutting and Burning Guides','!Maintenance, Repair and Operations!Welding Supplies!Welding and Cutting Accessories!Cutting and Burning Guides',2701,0,5877245,NULL,NULL),(5877251,'Cylinder and Caps','!Maintenance, Repair and Operations!Welding Supplies!Welding and Cutting Accessories!Cylinder and Caps',2702,0,5877245,NULL,NULL),(5877252,'Cylinder Pliers and Wrenches','!Maintenance, Repair and Operations!Welding Supplies!Welding and Cutting Accessories!Cylinder Pliers and Wrenches',2703,0,5877245,NULL,NULL),(5877253,'Arc Gouging','!Maintenance, Repair and Operations!Welding Supplies!Arc Gouging',1918,0,5877226,NULL,NULL),(5877254,'Arc Gouging Electrodes','!Maintenance, Repair and Operations!Welding Supplies!Arc Gouging!Arc Gouging Electrodes',2704,0,5877253,NULL,NULL),(5877255,'Welding and Cutting Accessories','!Maintenance, Repair and Operations!Welding Supplies!Welding and Cutting Accessories',1919,0,5877226,NULL,NULL),(5877256,'Dry Rod Oven Parts','!Maintenance, Repair and Operations!Welding Supplies!Welding and Cutting Accessories!Dry Rod Oven Parts',2705,0,5877255,NULL,NULL),(5877257,'Dry Rod Ovens','!Maintenance, Repair and Operations!Welding Supplies!Welding and Cutting Accessories!Dry Rod Ovens',2706,0,5877255,NULL,NULL),(5877258,'Furnaces','!Maintenance, Repair and Operations!Welding Supplies!Welding and Cutting Accessories!Furnaces',2707,0,5877255,NULL,NULL),(5877259,'Ground Clamps','!Maintenance, Repair and Operations!Welding Supplies!Welding and Cutting Accessories!Ground Clamps',2708,0,5877255,NULL,NULL),(5877260,'Heat Barriers','!Maintenance, Repair and Operations!Welding Supplies!Welding and Cutting Accessories!Heat Barriers',2709,0,5877255,NULL,NULL),(5877261,'Heat Tint Removal Systems','!Maintenance, Repair and Operations!Welding Supplies!Welding and Cutting Accessories!Heat Tint Removal Systems',2710,0,5877255,NULL,NULL),(5877262,'Positioners','!Maintenance, Repair and Operations!Welding Supplies!Welding and Cutting Accessories!Positioners',2711,0,5877255,NULL,NULL),(5877263,'Rod Holders','!Maintenance, Repair and Operations!Welding Supplies!Welding and Cutting Accessories!Rod Holders',2712,0,5877255,NULL,NULL),(5877264,'Spark Lighter and Flints','!Maintenance, Repair and Operations!Welding Supplies!Welding and Cutting Accessories!Spark Lighter and Flints',2713,0,5877255,NULL,NULL),(5877265,'Tip Cleaners','!Maintenance, Repair and Operations!Welding Supplies!Welding and Cutting Accessories!Tip Cleaners',2714,0,5877255,NULL,NULL),(5877266,'Arc Gouging','!Maintenance, Repair and Operations!Welding Supplies!Arc Gouging',1920,0,5877226,NULL,NULL),(5877267,'Arc Gouging Torch Parts','!Maintenance, Repair and Operations!Welding Supplies!Arc Gouging!Arc Gouging Torch Parts',2715,0,5877266,NULL,NULL),(5877268,'Welding and Cutting Accessories','!Maintenance, Repair and Operations!Welding Supplies!Welding and Cutting Accessories',1921,0,5877226,NULL,NULL),(5877269,'Water Circulators and Coolants','!Maintenance, Repair and Operations!Welding Supplies!Welding and Cutting Accessories!Water Circulators and Coolants',2716,0,5877268,NULL,NULL),(5877270,'Welding Blankets, Curtains, and Rolls','!Maintenance, Repair and Operations!Welding Supplies!Welding and Cutting Accessories!Welding Blankets, Curtains, and Rolls',2717,0,5877268,NULL,NULL),(5877271,'Welding Cable and Hose Reels','!Maintenance, Repair and Operations!Welding Supplies!Welding and Cutting Accessories!Welding Cable and Hose Reels',2718,0,5877268,NULL,NULL),(5877272,'Welding Cables and Power Cables','!Maintenance, Repair and Operations!Welding Supplies!Welding and Cutting Accessories!Welding Cables and Power Cables',2719,0,5877268,NULL,NULL),(5877273,'Welding Clamps, Spreaders and Holders','!Maintenance, Repair and Operations!Welding Supplies!Welding and Cutting Accessories!Welding Clamps, Spreaders and Holders',2720,0,5877268,NULL,NULL),(5877274,'Welding Hose and Compressed Gas Fittings','!Maintenance, Repair and Operations!Welding Supplies!Welding and Cutting Accessories!Welding Hose and Compressed Gas Fittings',2721,0,5877268,NULL,NULL),(5877275,'Welding Hoses','!Maintenance, Repair and Operations!Welding Supplies!Welding and Cutting Accessories!Welding Hoses',2722,0,5877268,NULL,NULL),(5877276,'Welding Tables','!Maintenance, Repair and Operations!Welding Supplies!Welding and Cutting Accessories!Welding Tables',2723,0,5877268,NULL,NULL),(5877277,'Welding Umbrellas','!Maintenance, Repair and Operations!Welding Supplies!Welding and Cutting Accessories!Welding Umbrellas',2724,0,5877268,NULL,NULL),(5877278,'Welding and Cutting Machines','!Maintenance, Repair and Operations!Welding Supplies!Welding and Cutting Machines',1922,0,5877226,NULL,NULL),(5877279,'Engine Driven Welders','!Maintenance, Repair and Operations!Welding Supplies!Welding and Cutting Machines!Engine Driven Welders',2725,0,5877278,NULL,NULL),(5877280,'Arc Gouging','!Maintenance, Repair and Operations!Welding Supplies!Arc Gouging',1923,0,5877226,NULL,NULL),(5877281,'Arc Gouging Torches and Cables','!Maintenance, Repair and Operations!Welding Supplies!Arc Gouging!Arc Gouging Torches and Cables',2726,0,5877280,NULL,NULL),(5877282,'Welding and Cutting Machines','!Maintenance, Repair and Operations!Welding Supplies!Welding and Cutting Machines',1924,0,5877226,NULL,NULL),(5877283,'MIG Welders','!Maintenance, Repair and Operations!Welding Supplies!Welding and Cutting Machines!MIG Welders',2727,0,5877282,NULL,NULL),(5877284,'Plasma Cutter Parts','!Maintenance, Repair and Operations!Welding Supplies!Welding and Cutting Machines!Plasma Cutter Parts',2728,0,5877282,NULL,NULL),(5877285,'Plasma Cutters','!Maintenance, Repair and Operations!Welding Supplies!Welding and Cutting Machines!Plasma Cutters',2729,0,5877282,NULL,NULL),(5877286,'Spot Welders','!Maintenance, Repair and Operations!Welding Supplies!Welding and Cutting Machines!Spot Welders',2730,0,5877282,NULL,NULL),(5877287,'Stick Welders','!Maintenance, Repair and Operations!Welding Supplies!Welding and Cutting Machines!Stick Welders',2731,0,5877282,NULL,NULL),(5877288,'Well Being, Safety and Security','!Maintenance, Repair and Operations!Well Being, Safety and Security',1541,0,5875888,NULL,NULL),(5877289,'Clothing Protection','!Maintenance, Repair and Operations!Well Being, Safety and Security!Clothing Protection',1926,0,5877288,NULL,NULL),(5877290,'Socks','!Maintenance, Repair and Operations!Well Being, Safety and Security!Clothing Protection!Socks',2733,0,5877289,NULL,NULL),(5877291,'Heat Stress Protection','!Maintenance, Repair and Operations!Well Being, Safety and Security!Heat Stress Protection',1927,0,5877288,NULL,NULL),(5877292,'Sweat Bands','!Maintenance, Repair and Operations!Well Being, Safety and Security!Heat Stress Protection!Sweat Bands',2734,0,5877291,NULL,NULL),(5877293,'Key Control','!Maintenance, Repair and Operations!Well Being, Safety and Security!Key Control',1928,0,5877288,NULL,NULL),(5877294,'Key Cabinets','!Maintenance, Repair and Operations!Well Being, Safety and Security!Key Control!Key Cabinets',2735,0,5877293,NULL,NULL),(5877295,'Key Cases','!Maintenance, Repair and Operations!Well Being, Safety and Security!Key Control!Key Cases',2736,0,5877293,NULL,NULL),(5877296,'Key Chains','!Maintenance, Repair and Operations!Well Being, Safety and Security!Key Control!Key Chains',2737,0,5877293,NULL,NULL),(5877297,'Key Racks and Trays','!Maintenance, Repair and Operations!Well Being, Safety and Security!Key Control!Key Racks and Trays',2738,0,5877293,NULL,NULL),(5877298,'Key Tags','!Maintenance, Repair and Operations!Well Being, Safety and Security!Key Control!Key Tags',2739,0,5877293,NULL,NULL),(5877299,'Locking Devices','!Maintenance, Repair and Operations!Well Being, Safety and Security!Locking Devices',1929,0,5877288,NULL,NULL),(5877300,'Door Knobs, Levers and Deadbolts','!Maintenance, Repair and Operations!Well Being, Safety and Security!Locking Devices!Door Knobs, Levers and Deadbolts',2740,0,5877299,NULL,NULL),(5877301,'Lock Boxes','!Maintenance, Repair and Operations!Well Being, Safety and Security!Locking Devices!Lock Boxes',2741,0,5877299,NULL,NULL),(5877302,'Lockouts and Hasps','!Maintenance, Repair and Operations!Well Being, Safety and Security!Locking Devices!Lockouts and Hasps',2742,0,5877299,NULL,NULL),(5877303,'Locks','!Maintenance, Repair and Operations!Well Being, Safety and Security!Locking Devices!Locks',2743,0,5877299,NULL,NULL),(5877304,'Defibrillators and Oxygen Units','!Maintenance, Repair and Operations!Well Being, Safety and Security!Defibrillators and Oxygen Units',1930,0,5877288,NULL,NULL),(5877305,'Locking Devices','!Maintenance, Repair and Operations!Well Being, Safety and Security!Locking Devices',1931,0,5877288,NULL,NULL),(5877306,'Padlocks and Cables','!Maintenance, Repair and Operations!Well Being, Safety and Security!Locking Devices!Padlocks and Cables',2745,0,5877305,NULL,NULL),(5877307,'Security Seals','!Maintenance, Repair and Operations!Well Being, Safety and Security!Locking Devices!Security Seals',2746,0,5877305,NULL,NULL),(5877308,'Locks','!Maintenance, Repair and Operations!Well Being, Safety and Security!Locks',1932,0,5877288,NULL,NULL),(5877309,'Respiratory Protection','!Maintenance, Repair and Operations!Well Being, Safety and Security!Respiratory Protection',1933,0,5877288,NULL,NULL),(5877310,'3M Air Supplied Matrix','!Maintenance, Repair and Operations!Well Being, Safety and Security!Respiratory Protection!3M Air Supplied Matrix',2748,0,5877309,NULL,NULL),(5877311,'3M Cartridge Respirator Matrix','!Maintenance, Repair and Operations!Well Being, Safety and Security!Respiratory Protection!3M Cartridge Respirator Matrix',2749,0,5877309,NULL,NULL),(5877312,'Cartridge Respirator Parts and Accessories','!Maintenance, Repair and Operations!Well Being, Safety and Security!Respiratory Protection!Cartridge Respirator Parts and Accessories',2750,0,5877309,NULL,NULL),(5877313,'Cartridge Respirators','!Maintenance, Repair and Operations!Well Being, Safety and Security!Respiratory Protection!Cartridge Respirators',2751,0,5877309,NULL,NULL),(5877314,'Disposable Respirator Parts and Accessories','!Maintenance, Repair and Operations!Well Being, Safety and Security!Respiratory Protection!Disposable Respirator Parts and Accessories',2752,0,5877309,NULL,NULL),(5877315,'Disposable Respirators','!Maintenance, Repair and Operations!Well Being, Safety and Security!Respiratory Protection!Disposable Respirators',2753,0,5877309,NULL,NULL),(5877316,'Escape Respirators','!Maintenance, Repair and Operations!Well Being, Safety and Security!Respiratory Protection!Escape Respirators',2754,0,5877309,NULL,NULL),(5877317,'Door Chimes','!Maintenance, Repair and Operations!Well Being, Safety and Security!Door Chimes',1934,0,5877288,NULL,NULL),(5877318,'Respiratory Protection','!Maintenance, Repair and Operations!Well Being, Safety and Security!Respiratory Protection',1935,0,5877288,NULL,NULL),(5877319,'Powered Air and Supplied Air System Parts and Accessories','!Maintenance, Repair and Operations!Well Being, Safety and Security!Respiratory Protection!Powered Air and Supplied Air System Parts and Accessories',2756,0,5877318,NULL,NULL),(5877320,'Powered Air and Supplied Air Systems','!Maintenance, Repair and Operations!Well Being, Safety and Security!Respiratory Protection!Powered Air and Supplied Air Systems',2757,0,5877318,NULL,NULL),(5877321,'Respirator Fit Testing','!Maintenance, Repair and Operations!Well Being, Safety and Security!Respiratory Protection!Respirator Fit Testing',2758,0,5877318,NULL,NULL),(5877322,'Respirators Masks','!Maintenance, Repair and Operations!Well Being, Safety and Security!Respiratory Protection!Respirators Masks',2759,0,5877318,NULL,NULL),(5877323,'Safety and Security, Other','!Maintenance, Repair and Operations!Well Being, Safety and Security!Safety and Security, Other',1936,0,5877288,NULL,NULL),(5877324,'Safety Barriers and Crowd Control','!Maintenance, Repair and Operations!Well Being, Safety and Security!Safety Barriers and Crowd Control',1937,0,5877288,NULL,NULL),(5877325,'Barricades','!Maintenance, Repair and Operations!Well Being, Safety and Security!Safety Barriers and Crowd Control!Barricades',2761,0,5877324,NULL,NULL),(5877326,'Safety Cones','!Maintenance, Repair and Operations!Well Being, Safety and Security!Safety Barriers and Crowd Control!Safety Cones',2762,0,5877324,NULL,NULL),(5877327,'Safety Tapes','!Maintenance, Repair and Operations!Well Being, Safety and Security!Safety Barriers and Crowd Control!Safety Tapes',2763,0,5877324,NULL,NULL),(5877328,'Stanchion Systems','!Maintenance, Repair and Operations!Well Being, Safety and Security!Safety Barriers and Crowd Control!Stanchion Systems',2764,0,5877324,NULL,NULL),(5877329,'Safety Storage','!Maintenance, Repair and Operations!Well Being, Safety and Security!Safety Storage',1938,0,5877288,NULL,NULL),(5877330,'Safety Can Parts and Accessories','!Maintenance, Repair and Operations!Well Being, Safety and Security!Safety Storage!Safety Can Parts and Accessories',2765,0,5877329,NULL,NULL),(5877331,'Ear Protection','!Maintenance, Repair and Operations!Well Being, Safety and Security!Ear Protection',1939,0,5877288,NULL,NULL),(5877332,'Ear Plugs','!Maintenance, Repair and Operations!Well Being, Safety and Security!Ear Protection!Ear Plugs',2766,0,5877331,NULL,NULL),(5877333,'Safety Storage','!Maintenance, Repair and Operations!Well Being, Safety and Security!Safety Storage',1940,0,5877288,NULL,NULL),(5877334,'Safety Cans','!Maintenance, Repair and Operations!Well Being, Safety and Security!Safety Storage!Safety Cans',2767,0,5877333,NULL,NULL),(5877335,'Safety Storage Cabinet Parts and Accessories','!Maintenance, Repair and Operations!Well Being, Safety and Security!Safety Storage!Safety Storage Cabinet Parts and Accessories',2768,0,5877333,NULL,NULL),(5877336,'Safety Storage Cabinets','!Maintenance, Repair and Operations!Well Being, Safety and Security!Safety Storage!Safety Storage Cabinets',2769,0,5877333,NULL,NULL),(5877337,'Safety Storage Cases','!Maintenance, Repair and Operations!Well Being, Safety and Security!Safety Storage!Safety Storage Cases',2770,0,5877333,NULL,NULL),(5877338,'Safety Storage Chests','!Maintenance, Repair and Operations!Well Being, Safety and Security!Safety Storage!Safety Storage Chests',2771,0,5877333,NULL,NULL),(5877339,'Safety Storage Container Parts and Accessories','!Maintenance, Repair and Operations!Well Being, Safety and Security!Safety Storage!Safety Storage Container Parts and Accessories',2772,0,5877333,NULL,NULL),(5877340,'Safety Storage Containers','!Maintenance, Repair and Operations!Well Being, Safety and Security!Safety Storage!Safety Storage Containers',2773,0,5877333,NULL,NULL),(5877341,'Safety Storage Lockers','!Maintenance, Repair and Operations!Well Being, Safety and Security!Safety Storage!Safety Storage Lockers',2774,0,5877333,NULL,NULL),(5877342,'Safety Storage Parts and Accessories','!Maintenance, Repair and Operations!Well Being, Safety and Security!Safety Storage!Safety Storage Parts and Accessories',2775,0,5877333,NULL,NULL),(5877343,'Spill Control Safety','!Maintenance, Repair and Operations!Well Being, Safety and Security!Spill Control Safety',1941,0,5877288,NULL,NULL),(5877344,'Accumulation Centers','!Maintenance, Repair and Operations!Well Being, Safety and Security!Spill Control Safety!Accumulation Centers',2776,0,5877343,NULL,NULL),(5877345,'Ear Protection','!Maintenance, Repair and Operations!Well Being, Safety and Security!Ear Protection',1942,0,5877288,NULL,NULL),(5877346,'Earmuff Parts and Accessories','!Maintenance, Repair and Operations!Well Being, Safety and Security!Ear Protection!Earmuff Parts and Accessories',2777,0,5877345,NULL,NULL),(5877347,'Spill Control Safety','!Maintenance, Repair and Operations!Well Being, Safety and Security!Spill Control Safety',1943,0,5877288,NULL,NULL),(5877348,'Antislip Products','!Maintenance, Repair and Operations!Well Being, Safety and Security!Spill Control Safety!Antislip Products',2778,0,5877347,NULL,NULL),(5877349,'Caddies and Trays','!Maintenance, Repair and Operations!Well Being, Safety and Security!Spill Control Safety!Caddies and Trays',2779,0,5877347,NULL,NULL),(5877350,'Drain Seals','!Maintenance, Repair and Operations!Well Being, Safety and Security!Spill Control Safety!Drain Seals',2780,0,5877347,NULL,NULL),(5877351,'Modules','!Maintenance, Repair and Operations!Well Being, Safety and Security!Spill Control Safety!Modules',2781,0,5877347,NULL,NULL),(5877352,'Pallets','!Maintenance, Repair and Operations!Well Being, Safety and Security!Spill Control Safety!Pallets',2782,0,5877347,NULL,NULL),(5877353,'Ramps','!Maintenance, Repair and Operations!Well Being, Safety and Security!Spill Control Safety!Ramps',2783,0,5877347,NULL,NULL),(5877354,'Sorbent Kits','!Maintenance, Repair and Operations!Well Being, Safety and Security!Spill Control Safety!Sorbent Kits',2784,0,5877347,NULL,NULL),(5877355,'Sorbents','!Maintenance, Repair and Operations!Well Being, Safety and Security!Spill Control Safety!Sorbents',2785,0,5877347,NULL,NULL),(5877356,'Traffic Safety','!Maintenance, Repair and Operations!Well Being, Safety and Security!Traffic Safety',1944,0,5877288,NULL,NULL),(5877357,'Ear Protection','!Maintenance, Repair and Operations!Well Being, Safety and Security!Ear Protection',1945,0,5877288,NULL,NULL),(5877358,'Earmuffs','!Maintenance, Repair and Operations!Well Being, Safety and Security!Ear Protection!Earmuffs',2787,0,5877357,NULL,NULL),(5877359,'Traffic Safety','!Maintenance, Repair and Operations!Well Being, Safety and Security!Traffic Safety',1946,0,5877288,NULL,NULL),(5877360,'Anti-Fatigue Matting','!Maintenance, Repair and Operations!Well Being, Safety and Security!Traffic Safety!Anti-Fatigue Matting',2788,0,5877359,NULL,NULL),(5877361,'Anti-Skid Treads','!Maintenance, Repair and Operations!Well Being, Safety and Security!Traffic Safety!Anti-Skid Treads',2789,0,5877359,NULL,NULL),(5877362,'Detectable Safety Tapes','!Maintenance, Repair and Operations!Well Being, Safety and Security!Traffic Safety!Detectable Safety Tapes',2790,0,5877359,NULL,NULL),(5877363,'Flare Parts and Accessories','!Maintenance, Repair and Operations!Well Being, Safety and Security!Traffic Safety!Flare Parts and Accessories',2791,0,5877359,NULL,NULL),(5877364,'Flares','!Maintenance, Repair and Operations!Well Being, Safety and Security!Traffic Safety!Flares',2792,0,5877359,NULL,NULL),(5877365,'Safety Coats and Jackets','!Maintenance, Repair and Operations!Well Being, Safety and Security!Traffic Safety!Safety Coats and Jackets',2793,0,5877359,NULL,NULL),(5877366,'Safety Cones','!Maintenance, Repair and Operations!Well Being, Safety and Security!Traffic Safety!Safety Cones',2794,0,5877359,NULL,NULL),(5877367,'Safety Fences','!Maintenance, Repair and Operations!Well Being, Safety and Security!Traffic Safety!Safety Fences',2795,0,5877359,NULL,NULL),(5877368,'Safety Flags and Pennants','!Maintenance, Repair and Operations!Well Being, Safety and Security!Traffic Safety!Safety Flags and Pennants',2796,0,5877359,NULL,NULL),(5877369,'Ear Protection','!Maintenance, Repair and Operations!Well Being, Safety and Security!Ear Protection',1947,0,5877288,NULL,NULL),(5877370,'Earplug Parts and Accessories','!Maintenance, Repair and Operations!Well Being, Safety and Security!Ear Protection!Earplug Parts and Accessories',2797,0,5877369,NULL,NULL),(5877371,'Traffic Safety','!Maintenance, Repair and Operations!Well Being, Safety and Security!Traffic Safety',1948,0,5877288,NULL,NULL),(5877372,'Safety Guards and Protectors','!Maintenance, Repair and Operations!Well Being, Safety and Security!Traffic Safety!Safety Guards and Protectors',2798,0,5877371,NULL,NULL),(5877373,'Safety Hats','!Maintenance, Repair and Operations!Well Being, Safety and Security!Traffic Safety!Safety Hats',2799,0,5877371,NULL,NULL),(5877374,'Safety Shirts','!Maintenance, Repair and Operations!Well Being, Safety and Security!Traffic Safety!Safety Shirts',2800,0,5877371,NULL,NULL),(5877375,'Safety Signs','!Maintenance, Repair and Operations!Well Being, Safety and Security!Traffic Safety!Safety Signs',2801,0,5877371,NULL,NULL),(5877376,'Safety Tags','!Maintenance, Repair and Operations!Well Being, Safety and Security!Traffic Safety!Safety Tags',2802,0,5877371,NULL,NULL),(5877377,'Safety Tapes','!Maintenance, Repair and Operations!Well Being, Safety and Security!Traffic Safety!Safety Tapes',2803,0,5877371,NULL,NULL),(5877378,'Safety Vests','!Maintenance, Repair and Operations!Well Being, Safety and Security!Traffic Safety!Safety Vests',2804,0,5877371,NULL,NULL),(5877379,'Two-Way Radios','!Maintenance, Repair and Operations!Well Being, Safety and Security!Two-Way Radios',1949,0,5877288,NULL,NULL),(5877380,'Ear Protection','!Maintenance, Repair and Operations!Well Being, Safety and Security!Ear Protection',1950,0,5877288,NULL,NULL),(5877381,'Earplugs','!Maintenance, Repair and Operations!Well Being, Safety and Security!Ear Protection!Earplugs',2806,0,5877380,NULL,NULL),(5877382,'Emergency Lighting','!Maintenance, Repair and Operations!Well Being, Safety and Security!Emergency Lighting',1951,0,5877288,NULL,NULL),(5877383,'Ergonomic Protection','!Maintenance, Repair and Operations!Well Being, Safety and Security!Ergonomic Protection',1952,0,5877288,NULL,NULL),(5877384,'Back Supports','!Maintenance, Repair and Operations!Well Being, Safety and Security!Ergonomic Protection!Back Supports',2808,0,5877383,NULL,NULL),(5877385,'Clothing Protection','!Maintenance, Repair and Operations!Well Being, Safety and Security!Clothing Protection',1953,0,5877288,NULL,NULL),(5877386,'Aprons','!Maintenance, Repair and Operations!Well Being, Safety and Security!Clothing Protection!Aprons',2809,0,5877385,NULL,NULL),(5877387,'Ergonomic Protection','!Maintenance, Repair and Operations!Well Being, Safety and Security!Ergonomic Protection',1954,0,5877288,NULL,NULL),(5877388,'Back Supports','!Maintenance, Repair and Operations!Well Being, Safety and Security!Ergonomic Protection!Back Supports',2810,0,5877387,NULL,NULL),(5877389,'Elbow Pads','!Maintenance, Repair and Operations!Well Being, Safety and Security!Ergonomic Protection!Elbow Pads',2811,0,5877387,NULL,NULL),(5877390,'Knee Pads','!Maintenance, Repair and Operations!Well Being, Safety and Security!Ergonomic Protection!Knee Pads',2812,0,5877387,NULL,NULL),(5877391,'Support Gloves','!Maintenance, Repair and Operations!Well Being, Safety and Security!Ergonomic Protection!Support Gloves',2813,0,5877387,NULL,NULL),(5877392,'Wrist and Arm Supports','!Maintenance, Repair and Operations!Well Being, Safety and Security!Ergonomic Protection!Wrist and Arm Supports',2814,0,5877387,NULL,NULL),(5877393,'Eye Protection','!Maintenance, Repair and Operations!Well Being, Safety and Security!Eye Protection',1955,0,5877288,NULL,NULL),(5877394,'Eye Care','!Maintenance, Repair and Operations!Well Being, Safety and Security!Eye Protection!Eye Care',2815,0,5877393,NULL,NULL),(5877395,'Safety Glass Parts and Accessories','!Maintenance, Repair and Operations!Well Being, Safety and Security!Eye Protection!Safety Glass Parts and Accessories',2816,0,5877393,NULL,NULL),(5877396,'Safety Glasses','!Maintenance, Repair and Operations!Well Being, Safety and Security!Eye Protection!Safety Glasses',2817,0,5877393,NULL,NULL),(5877397,'Safety Goggles','!Maintenance, Repair and Operations!Well Being, Safety and Security!Eye Protection!Safety Goggles',2818,0,5877393,NULL,NULL),(5877398,'Safety Goggles Protection Parts and Accessories','!Maintenance, Repair and Operations!Well Being, Safety and Security!Eye Protection!Safety Goggles Protection Parts and Accessories',2819,0,5877393,NULL,NULL),(5877399,'Clothing Protection','!Maintenance, Repair and Operations!Well Being, Safety and Security!Clothing Protection',1956,0,5877288,NULL,NULL),(5877400,'Coats and Jackets','!Maintenance, Repair and Operations!Well Being, Safety and Security!Clothing Protection!Coats and Jackets',2820,0,5877399,NULL,NULL),(5877401,'Eye Protection','!Maintenance, Repair and Operations!Well Being, Safety and Security!Eye Protection',1957,0,5877288,NULL,NULL),(5877402,'Safety Goggles','!Maintenance, Repair and Operations!Well Being, Safety and Security!Eye Protection!Safety Goggles',2821,0,5877401,NULL,NULL),(5877403,'Fall Protection','!Maintenance, Repair and Operations!Well Being, Safety and Security!Fall Protection',1958,0,5877288,NULL,NULL),(5877404,'Body Belts','!Maintenance, Repair and Operations!Well Being, Safety and Security!Fall Protection!Body Belts',2822,0,5877403,NULL,NULL),(5877405,'Fall Protection Kits','!Maintenance, Repair and Operations!Well Being, Safety and Security!Fall Protection!Fall Protection Kits',2823,0,5877403,NULL,NULL),(5877406,'Fall Protection Parts and Accessories','!Maintenance, Repair and Operations!Well Being, Safety and Security!Fall Protection!Fall Protection Parts and Accessories',2824,0,5877403,NULL,NULL),(5877407,'Harness Parts and Accessories','!Maintenance, Repair and Operations!Well Being, Safety and Security!Fall Protection!Harness Parts and Accessories',2825,0,5877403,NULL,NULL),(5877408,'Harnesses','!Maintenance, Repair and Operations!Well Being, Safety and Security!Fall Protection!Harnesses',2826,0,5877403,NULL,NULL),(5877409,'Ladder Safety System Parts and Accessories','!Maintenance, Repair and Operations!Well Being, Safety and Security!Fall Protection!Ladder Safety System Parts and Accessories',2827,0,5877403,NULL,NULL),(5877410,'Ladder Safety Systems','!Maintenance, Repair and Operations!Well Being, Safety and Security!Fall Protection!Ladder Safety Systems',2828,0,5877403,NULL,NULL),(5877411,'Lanyards','!Maintenance, Repair and Operations!Well Being, Safety and Security!Fall Protection!Lanyards',2829,0,5877403,NULL,NULL),(5877412,'Rescue System Parts and Accessories','!Maintenance, Repair and Operations!Well Being, Safety and Security!Fall Protection!Rescue System Parts and Accessories',2830,0,5877403,NULL,NULL),(5877413,'Clothing Protection','!Maintenance, Repair and Operations!Well Being, Safety and Security!Clothing Protection',1959,0,5877288,NULL,NULL),(5877414,'Coveralls and Overalls','!Maintenance, Repair and Operations!Well Being, Safety and Security!Clothing Protection!Coveralls and Overalls',2831,0,5877413,NULL,NULL),(5877415,'Fall Protection','!Maintenance, Repair and Operations!Well Being, Safety and Security!Fall Protection',1960,0,5877288,NULL,NULL),(5877416,'Rescue Systems','!Maintenance, Repair and Operations!Well Being, Safety and Security!Fall Protection!Rescue Systems',2832,0,5877415,NULL,NULL),(5877417,'Fire Extinguishers','!Maintenance, Repair and Operations!Well Being, Safety and Security!Fire Extinguishers',1961,0,5877288,NULL,NULL),(5877418,'Fire, Gas and Water Protection','!Maintenance, Repair and Operations!Well Being, Safety and Security!Fire, Gas and Water Protection',1962,0,5877288,NULL,NULL),(5877419,'Detector Parts and Accessories','!Maintenance, Repair and Operations!Well Being, Safety and Security!Fire, Gas and Water Protection!Detector Parts and Accessories',2834,0,5877418,NULL,NULL),(5877420,'Detectors','!Maintenance, Repair and Operations!Well Being, Safety and Security!Fire, Gas and Water Protection!Detectors',2835,0,5877418,NULL,NULL),(5877421,'Fire and Gas Protection Parts and Accessories','!Maintenance, Repair and Operations!Well Being, Safety and Security!Fire, Gas and Water Protection!Fire and Gas Protection Parts and Accessories',2836,0,5877418,NULL,NULL),(5877422,'Fire Extinguishers','!Maintenance, Repair and Operations!Well Being, Safety and Security!Fire, Gas and Water Protection!Fire Extinguishers',2837,0,5877418,NULL,NULL),(5877423,'Flammable Liquid Cabinets','!Maintenance, Repair and Operations!Well Being, Safety and Security!Fire, Gas and Water Protection!Flammable Liquid Cabinets',2838,0,5877418,NULL,NULL),(5877424,'Smoke Detectors','!Maintenance, Repair and Operations!Well Being, Safety and Security!Fire, Gas and Water Protection!Smoke Detectors',2839,0,5877418,NULL,NULL),(5877425,'Testers','!Maintenance, Repair and Operations!Well Being, Safety and Security!Fire, Gas and Water Protection!Testers',2840,0,5877418,NULL,NULL),(5877426,'Clothing Protection','!Maintenance, Repair and Operations!Well Being, Safety and Security!Clothing Protection',1963,0,5877288,NULL,NULL),(5877427,'Head and Face Covers','!Maintenance, Repair and Operations!Well Being, Safety and Security!Clothing Protection!Head and Face Covers',2841,0,5877426,NULL,NULL),(5877428,'Fire, Gas and Water Protection','!Maintenance, Repair and Operations!Well Being, Safety and Security!Fire, Gas and Water Protection',1964,0,5877288,NULL,NULL),(5877429,'Vapor Monitors','!Maintenance, Repair and Operations!Well Being, Safety and Security!Fire, Gas and Water Protection!Vapor Monitors',2842,0,5877428,NULL,NULL),(5877430,'First Aid Safety','!Maintenance, Repair and Operations!Well Being, Safety and Security!First Aid Safety',1965,0,5877288,NULL,NULL),(5877431,'Antiseptics','!Maintenance, Repair and Operations!Well Being, Safety and Security!First Aid Safety!Antiseptics',2843,0,5877430,NULL,NULL),(5877432,'Bandages and Dressings','!Maintenance, Repair and Operations!Well Being, Safety and Security!First Aid Safety!Bandages and Dressings',2844,0,5877430,NULL,NULL),(5877433,'Cold and Sinus Meds','!Maintenance, Repair and Operations!Well Being, Safety and Security!First Aid Safety!Cold and Sinus Meds',2845,0,5877430,NULL,NULL),(5877434,'Cold Packs','!Maintenance, Repair and Operations!Well Being, Safety and Security!First Aid Safety!Cold Packs',2846,0,5877430,NULL,NULL),(5877435,'CPR Mask','!Maintenance, Repair and Operations!Well Being, Safety and Security!First Aid Safety!CPR Mask',2847,0,5877430,NULL,NULL),(5877436,'Eye Wash','!Maintenance, Repair and Operations!Well Being, Safety and Security!First Aid Safety!Eye Wash',2848,0,5877430,NULL,NULL),(5877437,'First Aid Kits','!Maintenance, Repair and Operations!Well Being, Safety and Security!First Aid Safety!First Aid Kits',2849,0,5877430,NULL,NULL),(5877438,'Insect Repellents and Sunscreens','!Maintenance, Repair and Operations!Well Being, Safety and Security!First Aid Safety!Insect Repellents and Sunscreens',2850,0,5877430,NULL,NULL),(5877439,'Clothing Protection','!Maintenance, Repair and Operations!Well Being, Safety and Security!Clothing Protection',1966,0,5877288,NULL,NULL),(5877440,'Pants and Shorts','!Maintenance, Repair and Operations!Well Being, Safety and Security!Clothing Protection!Pants and Shorts',2851,0,5877439,NULL,NULL),(5877441,'First Aid Safety','!Maintenance, Repair and Operations!Well Being, Safety and Security!First Aid Safety',1967,0,5877288,NULL,NULL),(5877442,'Insecticides','!Maintenance, Repair and Operations!Well Being, Safety and Security!First Aid Safety!Insecticides',2852,0,5877441,NULL,NULL),(5877443,'Ointments','!Maintenance, Repair and Operations!Well Being, Safety and Security!First Aid Safety!Ointments',2853,0,5877441,NULL,NULL),(5877444,'Pain Relievers','!Maintenance, Repair and Operations!Well Being, Safety and Security!First Aid Safety!Pain Relievers',2854,0,5877441,NULL,NULL),(5877445,'Foot Protection','!Maintenance, Repair and Operations!Well Being, Safety and Security!Foot Protection',1968,0,5877288,NULL,NULL),(5877446,'Boots and Shoes','!Maintenance, Repair and Operations!Well Being, Safety and Security!Foot Protection!Boots and Shoes',2855,0,5877445,NULL,NULL),(5877447,'Footwear Covers','!Maintenance, Repair and Operations!Well Being, Safety and Security!Foot Protection!Footwear Covers',2856,0,5877445,NULL,NULL),(5877448,'Hand Protection','!Maintenance, Repair and Operations!Well Being, Safety and Security!Hand Protection',1969,0,5877288,NULL,NULL),(5877449,'Glove Parts and Accessories','!Maintenance, Repair and Operations!Well Being, Safety and Security!Hand Protection!Glove Parts and Accessories',2857,0,5877448,NULL,NULL),(5877450,'Gloves','!Maintenance, Repair and Operations!Well Being, Safety and Security!Hand Protection!Gloves',2858,0,5877448,NULL,NULL),(5877451,'Chore Gloves','!Maintenance, Repair and Operations!Well Being, Safety and Security!Hand Protection!Gloves!Chore Gloves',3174,0,5877450,NULL,NULL),(5877452,'Coated Gloves','!Maintenance, Repair and Operations!Well Being, Safety and Security!Hand Protection!Gloves!Coated Gloves',3175,0,5877450,NULL,NULL),(5877453,'Cotton Canvas Gloves','!Maintenance, Repair and Operations!Well Being, Safety and Security!Hand Protection!Gloves!Cotton Canvas Gloves',3176,0,5877450,NULL,NULL),(5877454,'Clothing Protection','!Maintenance, Repair and Operations!Well Being, Safety and Security!Clothing Protection',1970,0,5877288,NULL,NULL),(5877455,'Rainwear','!Maintenance, Repair and Operations!Well Being, Safety and Security!Clothing Protection!Rainwear',2859,0,5877454,NULL,NULL),(5877456,'Hand Protection','!Maintenance, Repair and Operations!Well Being, Safety and Security!Hand Protection',1971,0,5877288,NULL,NULL),(5877457,'Gloves','!Maintenance, Repair and Operations!Well Being, Safety and Security!Hand Protection!Gloves',2860,0,5877456,NULL,NULL),(5877458,'Cut Resistant Gloves','!Maintenance, Repair and Operations!Well Being, Safety and Security!Hand Protection!Gloves!Cut Resistant Gloves',3178,0,5877457,NULL,NULL),(5877459,'Disposable Gloves','!Maintenance, Repair and Operations!Well Being, Safety and Security!Hand Protection!Gloves!Disposable Gloves',3179,0,5877457,NULL,NULL),(5877460,'Dotted Canvas Gloves','!Maintenance, Repair and Operations!Well Being, Safety and Security!Hand Protection!Gloves!Dotted Canvas Gloves',3180,0,5877457,NULL,NULL),(5877461,'Driver\'s Gloves','!Maintenance, Repair and Operations!Well Being, Safety and Security!Hand Protection!Gloves!Driver\'s Gloves',3181,0,5877457,NULL,NULL),(5877462,'High Dexterity Gloves','!Maintenance, Repair and Operations!Well Being, Safety and Security!Hand Protection!Gloves!High Dexterity Gloves',3182,0,5877457,NULL,NULL),(5877463,'High Dexterity Leather Palm Gloves','!Maintenance, Repair and Operations!Well Being, Safety and Security!Hand Protection!Gloves!High Dexterity Leather Palm Gloves',3183,0,5877457,NULL,NULL),(5877464,'High Heat Gloves','!Maintenance, Repair and Operations!Well Being, Safety and Security!Hand Protection!Gloves!High Heat Gloves',3184,0,5877457,NULL,NULL),(5877465,'Inspectors Gloves','!Maintenance, Repair and Operations!Well Being, Safety and Security!Hand Protection!Gloves!Inspectors Gloves',3185,0,5877457,NULL,NULL),(5877466,'Jersey Gloves','!Maintenance, Repair and Operations!Well Being, Safety and Security!Hand Protection!Gloves!Jersey Gloves',3186,0,5877457,NULL,NULL),(5877467,'Latex Gloves','!Maintenance, Repair and Operations!Well Being, Safety and Security!Hand Protection!Gloves!Latex Gloves',3187,0,5877457,NULL,NULL),(5877468,'Clothing Protection','!Maintenance, Repair and Operations!Well Being, Safety and Security!Clothing Protection',1972,0,5877288,NULL,NULL),(5877469,'Shirts','!Maintenance, Repair and Operations!Well Being, Safety and Security!Clothing Protection!Shirts',2861,0,5877468,NULL,NULL),(5877470,'Hand Protection','!Maintenance, Repair and Operations!Well Being, Safety and Security!Hand Protection',1973,0,5877288,NULL,NULL),(5877471,'Gloves','!Maintenance, Repair and Operations!Well Being, Safety and Security!Hand Protection!Gloves',2862,0,5877470,NULL,NULL),(5877472,'Leather Palm Gloves','!Maintenance, Repair and Operations!Well Being, Safety and Security!Hand Protection!Gloves!Leather Palm Gloves',3189,0,5877471,NULL,NULL),(5877473,'Neoprene Gloves','!Maintenance, Repair and Operations!Well Being, Safety and Security!Hand Protection!Gloves!Neoprene Gloves',3190,0,5877471,NULL,NULL),(5877474,'Nitrile Gloves','!Maintenance, Repair and Operations!Well Being, Safety and Security!Hand Protection!Gloves!Nitrile Gloves',3191,0,5877471,NULL,NULL),(5877475,'PVC Gloves','!Maintenance, Repair and Operations!Well Being, Safety and Security!Hand Protection!Gloves!PVC Gloves',3192,0,5877471,NULL,NULL),(5877476,'String Knit Gloves','!Maintenance, Repair and Operations!Well Being, Safety and Security!Hand Protection!Gloves!String Knit Gloves',3193,0,5877471,NULL,NULL),(5877477,'Welding Gloves','!Maintenance, Repair and Operations!Well Being, Safety and Security!Hand Protection!Gloves!Welding Gloves',3194,0,5877471,NULL,NULL),(5877478,'Hand Pads and Warmers','!Maintenance, Repair and Operations!Well Being, Safety and Security!Hand Protection!Hand Pads and Warmers',2863,0,5877470,NULL,NULL),(5877479,'Head/Face Protection','!Maintenance, Repair and Operations!Well Being, Safety and Security!Head/Face Protection',1974,0,5877288,NULL,NULL),(5877480,'Faceshields and Visors','!Maintenance, Repair and Operations!Well Being, Safety and Security!Head/Face Protection!Faceshields and Visors',2864,0,5877479,NULL,NULL),(5877481,'Hard Hat and Cap Parts and Accessories','!Maintenance, Repair and Operations!Well Being, Safety and Security!Head/Face Protection!Hard Hat and Cap Parts and Accessories',2865,0,5877479,NULL,NULL),(5877482,'Hard Hats and Caps','!Maintenance, Repair and Operations!Well Being, Safety and Security!Head/Face Protection!Hard Hats and Caps',2866,0,5877479,NULL,NULL),(5877483,'Clothing Protection','!Maintenance, Repair and Operations!Well Being, Safety and Security!Clothing Protection',1975,0,5877288,NULL,NULL),(5877484,'Sleeves and Bibs','!Maintenance, Repair and Operations!Well Being, Safety and Security!Clothing Protection!Sleeves and Bibs',2867,0,5877483,NULL,NULL),(5877485,'Head/Face Protection','!Maintenance, Repair and Operations!Well Being, Safety and Security!Head/Face Protection',1976,0,5877288,NULL,NULL),(5877486,'Welder Caps and Doo Rags','!Maintenance, Repair and Operations!Well Being, Safety and Security!Head/Face Protection!Welder Caps and Doo Rags',2868,0,5877485,NULL,NULL),(5877487,'Welding Helmet Parts and Accessories','!Maintenance, Repair and Operations!Well Being, Safety and Security!Head/Face Protection!Welding Helmet Parts and Accessories',2869,0,5877485,NULL,NULL),(5877488,'Helmet Headgear','!Maintenance, Repair and Operations!Well Being, Safety and Security!Head/Face Protection!Welding Helmet Parts and Accessories!Helmet Headgear',3196,0,5877487,NULL,NULL),(5877489,'Lenses and Plates','!Maintenance, Repair and Operations!Well Being, Safety and Security!Head/Face Protection!Welding Helmet Parts and Accessories!Lenses and Plates',3197,0,5877487,NULL,NULL),(5877490,'Welding Helmet Accessories','!Maintenance, Repair and Operations!Well Being, Safety and Security!Head/Face Protection!Welding Helmet Parts and Accessories!Welding Helmet Accessories',3198,0,5877487,NULL,NULL),(5877491,'Welding Helmets','!Maintenance, Repair and Operations!Well Being, Safety and Security!Head/Face Protection!Welding Helmets',2870,0,5877485,NULL,NULL),(5877492,'Winter Liners and Hoods','!Maintenance, Repair and Operations!Well Being, Safety and Security!Head/Face Protection!Winter Liners and Hoods',2871,0,5877485,NULL,NULL),(5877493,'Heat Stress Protection','!Maintenance, Repair and Operations!Well Being, Safety and Security!Heat Stress Protection',1977,0,5877288,NULL,NULL),(5877494,'Activity Drinks','!Maintenance, Repair and Operations!Well Being, Safety and Security!Heat Stress Protection!Activity Drinks',2872,0,5877493,NULL,NULL),(5877495,'Bandannas','!Maintenance, Repair and Operations!Well Being, Safety and Security!Heat Stress Protection!Bandannas',2873,0,5877493,NULL,NULL),(5877496,'Cooling Vests and Accessories','!Maintenance, Repair and Operations!Well Being, Safety and Security!Heat Stress Protection!Cooling Vests and Accessories',2874,0,5877493,NULL,NULL),(5877497,'Hydration Packs and Vests','!Maintenance, Repair and Operations!Well Being, Safety and Security!Heat Stress Protection!Hydration Packs and Vests',2875,0,5877493,NULL,NULL),(5877498,'Facility Maintenance','!Maintenance, Repair and Operations!Facility Maintenance',1542,0,5875888,NULL,NULL),(5877499,'Plumbing Supplies','!Maintenance, Repair and Operations!Facility Maintenance!Plumbing Supplies',1978,0,5877498,NULL,NULL),(5877500,'Tap Water Filters','!Maintenance, Repair and Operations!Facility Maintenance!Plumbing Supplies!Tap Water Filters',2876,0,5877499,NULL,NULL),(5877501,'Step Stools and Ladders','!Maintenance, Repair and Operations!Facility Maintenance!Step Stools and Ladders',1979,0,5877498,NULL,NULL),(132421389,'Bung Plugs','!Material Handling!Storage!Drums, Drum Handling & Storage!Drum Accessories!Bung Plugs',95797,0,NULL,NULL,NULL),(132421390,'Abrasives','!Abrasives',95000,0,NULL,NULL,NULL),(132421391,'Adhesives, Sealants & Tapes','!Adhesives, Sealants & Tapes',95001,0,NULL,NULL,NULL),(132421392,'Chemicals, Lubricants & Paints','!Chemicals, Lubricants & Paints',95002,0,NULL,NULL,NULL),(132421393,'Electrical','!Electrical',95003,0,NULL,NULL,NULL),(132421394,'Fasteners, Clamps & Straps','!Fasteners, Clamps & Straps',95004,0,NULL,NULL,NULL),(132421395,'Furniture','!Furniture',95005,0,NULL,NULL,NULL),(132421396,'Hand Tools','!Hand Tools',95006,0,NULL,NULL,NULL),(132421397,'Hardware','!Hardware',95007,0,NULL,NULL,NULL),(132421398,'Hospitality & Food Service Supplies','!Hospitality & Food Service Supplies',95008,0,NULL,NULL,NULL),(132421399,'HVAC','!HVAC',95009,0,NULL,NULL,NULL),(132421400,'Janitorial Equipment','!Janitorial Equipment',95010,0,NULL,NULL,NULL),(132421401,'Lighting','!Lighting',95011,0,NULL,NULL,NULL),(132421402,'Marking Tools','!Marking Tools',95012,0,NULL,NULL,NULL),(132421403,'Material Handling','!Material Handling',95013,0,NULL,NULL,NULL),(132421404,'Measuring & Leveling Tools','!Measuring & Leveling Tools',95014,0,NULL,NULL,NULL),(132421405,'MRO Supplies','!MRO Supplies',95015,0,NULL,NULL,NULL),(132421406,'Plumbing','!Plumbing',95016,0,NULL,NULL,NULL),(132421407,'Pneumatics','!Pneumatics',95017,0,NULL,NULL,NULL),(132421408,'Power Tools','!Power Tools',95018,0,NULL,NULL,NULL),(132421409,'Pumps','!Pumps',95019,0,NULL,NULL,NULL),(132421410,'Safety & Security','!Safety & Security',95020,0,NULL,NULL,NULL),(132421411,'Welding Supplies','!Welding Supplies',95021,0,NULL,NULL,NULL),(132421412,'Abrasives Other','!Abrasives!Abrasives Other',95022,0,132421390,NULL,NULL),(132421413,'Adhesive Compounds','!Adhesives, Sealants & Tapes!Adhesive Compounds',95023,0,132421391,NULL,NULL),(132421414,'Adhesive Products','!Adhesives, Sealants & Tapes!Adhesive Products',95024,0,132421391,NULL,NULL),(132421415,'Adhesives, Sealants & Tapes Other','!Adhesives, Sealants & Tapes!Adhesives, Sealants & Tapes Other',95025,0,132421391,NULL,NULL),(132421416,'Air Conditioners','!HVAC!Air Conditioners',95026,0,132421399,NULL,NULL),(132421417,'Air Cooling Equipment','!HVAC!Air Cooling Equipment',95027,0,132421399,NULL,NULL),(132421418,'Air Cooling Equipment ','!HVAC!Air Cooling Equipment ',95028,0,132421399,NULL,NULL),(132421419,'Air Filters','!HVAC!Air Filters',95029,0,132421399,NULL,NULL),(132421420,'Air Hose Reels','!Pneumatics!Air Hose Reels',95030,0,132421407,NULL,NULL),(132421421,'Air Movers','!Janitorial Equipment!Air Movers',95031,0,132421400,NULL,NULL),(132421422,'Air Tool Oils','!Pneumatics!Air Tool Oils',95032,0,132421407,NULL,NULL),(132421423,'Appliances','!Hospitality & Food Service Supplies!Appliances',95033,0,132421398,NULL,NULL),(132421424,'Arc Gouging','!Welding Supplies!Arc Gouging',95034,0,132421411,NULL,NULL),(132421425,'Auger Bits','!Power Tools!Auger Bits',95035,0,132421408,NULL,NULL),(132421426,'Automotive Maintenance Products','!MRO Supplies!Automotive Maintenance Products',95036,0,132421405,NULL,NULL),(132421427,'Automotive Tools','!Hand Tools!Automotive Tools',95037,0,132421396,NULL,NULL),(132421428,'Baking Soda','!Hospitality & Food Service Supplies!Baking Soda',95038,0,132421398,NULL,NULL),(132421429,'Balancers','!Pneumatics!Balancers',95039,0,132421407,NULL,NULL),(132421430,'Ball Valves & Accessories','!Plumbing!Ball Valves & Accessories',95040,0,132421406,NULL,NULL),(132421431,'Ballast and Accessories','!Lighting!Ballast and Accessories',95041,0,132421401,NULL,NULL),(132421432,'Band Saws','!Power Tools!Band Saws',95042,0,132421408,NULL,NULL),(132421433,'Bathroom Fixtures, Parts and Accessories','!Plumbing!Bathroom Fixtures, Parts and Accessories',95043,0,132421406,NULL,NULL),(132421434,'Batteries','!Electrical!Batteries',95044,0,132421393,NULL,NULL),(132421435,'Battery Chargers','!Electrical!Battery Chargers',95045,0,132421393,NULL,NULL),(132421436,'Beam Clamps','!Hand Tools!Beam Clamps',95046,0,132421396,NULL,NULL),(132421437,'Belt Sanders','!Power Tools!Belt Sanders',95047,0,132421408,NULL,NULL),(132421438,'Bending & Flaring Tools','!Hand Tools!Bending & Flaring Tools',95048,0,132421396,NULL,NULL),(132421439,'Bit Accessories','!Hand Tools!Bit Accessories',95049,0,132421396,NULL,NULL),(132421440,'Blocks & Pads','!HVAC!Blocks & Pads',95050,0,132421399,NULL,NULL),(132421441,'Boilers & Parts','!HVAC!Boilers & Parts',95051,0,132421399,NULL,NULL),(132421442,'Bonded Abrasives','!Abrasives!Bonded Abrasives',95052,0,132421390,NULL,NULL),(132421443,'Boxes & Covers','!Electrical!Boxes & Covers',95053,0,132421393,NULL,NULL),(132421444,'Brackets','!Fasteners, Clamps & Straps!Brackets',95054,0,132421394,NULL,NULL),(132421445,'Brushes','!Abrasives!Brushes',95055,0,132421390,NULL,NULL),(132421446,'Brushing & Sweeping Products','!Janitorial Equipment!Brushing & Sweeping Products',95056,0,132421400,NULL,NULL),(132421447,'Burners','!HVAC!Burners',95057,0,132421399,NULL,NULL),(132421448,'Cabinet Hardware','!Hardware!Cabinet Hardware',95058,0,132421397,NULL,NULL),(132421449,'Cable Protectors','!Electrical!Cable Protectors',95059,0,132421393,NULL,NULL),(132421450,'Caliper & Divider Parts & Accessories','!Measuring & Leveling Tools!Caliper & Divider Parts & Accessories',95060,0,132421404,NULL,NULL),(132421451,'Caliper & Divider Sets','!Measuring & Leveling Tools!Caliper & Divider Sets',95061,0,132421404,NULL,NULL),(132421452,'Caliper & Micrometer Sets','!Measuring & Leveling Tools!Caliper & Micrometer Sets',95062,0,132421404,NULL,NULL),(132421453,'Calipers & Dividers','!Measuring & Leveling Tools!Calipers & Dividers',95063,0,132421404,NULL,NULL),(132421454,'Capacitors','!HVAC!Capacitors',95064,0,132421399,NULL,NULL),(132421455,'Car Waxes, Polishes & Protectants','!Chemicals, Lubricants & Paints!Car Waxes, Polishes & Protectants',95065,0,132421392,NULL,NULL),(132421456,'Cartridges','!Plumbing!Cartridges',95066,0,132421406,NULL,NULL),(132421457,'Carts & Trucks','!Material Handling!Carts & Trucks',95067,0,132421403,NULL,NULL),(132421458,'Ceiling Fans & Accessories','!Lighting!Ceiling Fans & Accessories',95068,0,132421401,NULL,NULL),(132421459,'Chain, Cable, Rope & Accessories','!Material Handling!Chain, Cable, Rope & Accessories',95069,0,132421403,NULL,NULL),(132421460,'Chalk','!Marking Tools!Chalk',95070,0,132421402,NULL,NULL),(132421461,'Chalk Reel Line & Refills','!Marking Tools!Chalk Reel Line & Refills',95071,0,132421402,NULL,NULL),(132421462,'Chalk Reel Sets','!Marking Tools!Chalk Reel Sets',95072,0,132421402,NULL,NULL),(132421463,'Chalk Reels','!Marking Tools!Chalk Reels',95073,0,132421402,NULL,NULL),(132421464,'Chemicals, Lubricants & Paints Other','!Chemicals, Lubricants & Paints!Chemicals, Lubricants & Paints Other',95074,0,132421392,NULL,NULL),(132421465,'Chisel, Punches & Pins','!Hand Tools!Chisel, Punches & Pins',95075,0,132421396,NULL,NULL),(132421466,'Chisels, Punches & Pins','!Hand Tools!Chisels, Punches & Pins',95076,0,132421396,NULL,NULL),(132421467,'Chop Saws','!Power Tools!Chop Saws',95077,0,132421408,NULL,NULL),(132421468,'Circular Saw Blades','!Power Tools!Circular Saw Blades',95078,0,132421408,NULL,NULL),(132421469,'Circular Saws','!Power Tools!Circular Saws',95079,0,132421408,NULL,NULL),(132421470,'Clamp Wrenches','!Hand Tools!Clamp Wrenches',95080,0,132421396,NULL,NULL),(132421471,'Clamping Products','!Fasteners, Clamps & Straps!Clamping Products',95081,0,132421394,NULL,NULL),(132421472,'Clamps & Vises','!Hand Tools!Clamps & Vises',95082,0,132421396,NULL,NULL),(132421473,'Claw Hammers','!Hand Tools!Claw Hammers',95083,0,132421396,NULL,NULL),(132421474,'Cleaning Products','!Chemicals, Lubricants & Paints!Cleaning Products',95084,0,132421392,NULL,NULL),(132421475,'Closet Hardware','!Hardware!Closet Hardware',95085,0,132421397,NULL,NULL),(132421476,'Clothing Protection','!Safety & Security!Clothing Protection',95086,0,132421410,NULL,NULL),(132421477,'Coated Abrasives','!Abrasives!Coated Abrasives',95087,0,132421390,NULL,NULL),(132421478,'Coffee, Sugar & Creamers','!Hospitality & Food Service Supplies!Coffee, Sugar & Creamers',95088,0,132421398,NULL,NULL),(132421479,'Combo Kits','!Power Tools!Combo Kits',95089,0,132421408,NULL,NULL),(132421480,'Communicating Products','!MRO Supplies!Communicating Products',95090,0,132421405,NULL,NULL),(132421481,'Compressors, Blow Guns, Hoses & Fittings','!Pneumatics!Compressors, Blow Guns, Hoses & Fittings',95091,0,132421407,NULL,NULL),(132421482,'Concrete & Masonry Tools','!Hand Tools!Concrete & Masonry Tools',95092,0,132421396,NULL,NULL),(132421483,'Condenser Motors','!HVAC!Condenser Motors',95093,0,132421399,NULL,NULL),(132421484,'Connectors, Terminals & Clips','!Electrical!Connectors, Terminals & Clips',95094,0,132421393,NULL,NULL),(132421485,'Contactors','!Electrical!Contactors',95095,0,132421393,NULL,NULL),(132421486,'Containers','!Electrical!Containers',95096,0,132421393,NULL,NULL),(132421487,'Contour Marker Parts & Accessories','!Measuring & Leveling Tools!Contour Marker Parts & Accessories',95097,0,132421404,NULL,NULL),(132421488,'Contour Markers','!Measuring & Leveling Tools!Contour Markers',95098,0,132421404,NULL,NULL),(132421489,'Cooler & Chest Parts & Accessories','!Hospitality & Food Service Supplies!Cooler & Chest Parts & Accessories',95099,0,132421398,NULL,NULL),(132421490,'Coolers & Chests','!Hospitality & Food Service Supplies!Coolers & Chests',95100,0,132421398,NULL,NULL),(132421491,'Cord Reels','!Electrical!Cord Reels',95101,0,132421393,NULL,NULL),(132421492,'Cordless Tool Batteries & Chargers','!Power Tools!Cordless Tool Batteries & Chargers',95102,0,132421408,NULL,NULL),(132421493,'Covering Products','!MRO Supplies!Covering Products',95103,0,132421405,NULL,NULL),(132421494,'Crayons','!Marking Tools!Crayons',95104,0,132421402,NULL,NULL),(132421495,'Crimping & Stripping Tools','!Hand Tools!Crimping & Stripping Tools',95105,0,132421396,NULL,NULL),(132421496,'Cups & Lids','!Hospitality & Food Service Supplies!Cups & Lids',95106,0,132421398,NULL,NULL),(132421497,'Cutlery & Cutlery Kits','!Hospitality & Food Service Supplies!Cutlery & Cutlery Kits',95107,0,132421398,NULL,NULL),(132421498,'Cut-Off Saws','!Power Tools!Cut-Off Saws',95108,0,132421408,NULL,NULL),(132421499,'Cutting Tools','!Power Tools!Cutting Tools',95109,0,132421408,NULL,NULL),(132421500,'Defibrillators & Oxygen Units','!Safety & Security!Defibrillators & Oxygen Units',95110,0,132421410,NULL,NULL),(132421501,'Demolition Hammers','!Hand Tools!Demolition Hammers',95111,0,132421396,NULL,NULL),(132421502,'Demolition Tools','!Pneumatics!Demolition Tools',95112,0,132421407,NULL,NULL),(132421503,'Derrick Tape Refills','!Measuring & Leveling Tools!Derrick Tape Refills',95113,0,132421404,NULL,NULL),(132421504,'Derrick Tapes','!Measuring & Leveling Tools!Derrick Tapes',95114,0,132421404,NULL,NULL),(132421505,'Dial & Digital Indicator Parts & Accessories','!Measuring & Leveling Tools!Dial & Digital Indicator Parts & Accessories',95115,0,132421404,NULL,NULL),(132421506,'Dial & Digital Indicators','!Measuring & Leveling Tools!Dial & Digital Indicators',95116,0,132421404,NULL,NULL),(132421507,'Dimmers','!Lighting!Dimmers',95117,0,132421401,NULL,NULL),(132421508,'Display Accessories','!Hospitality & Food Service Supplies!Display Accessories',95118,0,132421398,NULL,NULL),(132421509,'Door Chime Kits','!Hardware!Door Chime Kits',95119,0,132421397,NULL,NULL),(132421510,'Door Hardware','!Hardware!Door Hardware',95120,0,132421397,NULL,NULL),(132421511,'Doorbell Buttons','!Hardware!Doorbell Buttons',95121,0,132421397,NULL,NULL),(132421512,'Doorbell Transformers','!Hardware!Doorbell Transformers',95122,0,132421397,NULL,NULL),(132421513,'Drain Cleaning & Inspection Equipment','!Plumbing!Drain Cleaning & Inspection Equipment',95123,0,132421406,NULL,NULL),(132421514,'Drains, Cleanouts & Accessories','!Plumbing!Drains, Cleanouts & Accessories',95124,0,132421406,NULL,NULL),(132421515,'Drilling & Fastening Tools','!Power Tools!Drilling & Fastening Tools',95125,0,132421408,NULL,NULL),(132421516,'Drinking Fountain','!Plumbing!Drinking Fountain',95126,0,132421406,NULL,NULL),(132421517,'Drive Ratchets','!Power Tools!Drive Ratchets',95127,0,132421408,NULL,NULL),(132421518,'Dryer Vents & Hoods','!Power Tools!Dryer Vents & Hoods',95128,0,132421408,NULL,NULL),(132421519,'Drywall Tools','!Hand Tools!Drywall Tools',95129,0,132421396,NULL,NULL),(132421520,'Ear Protection','!Safety & Security!Ear Protection',95130,0,132421410,NULL,NULL),(132421521,'Effluent/Sewage Pump Accessories','!Pumps!Effluent/Sewage Pump Accessories',95131,0,132421409,NULL,NULL),(132421522,'Effluent/Sewage Pumps','!Pumps!Effluent/Sewage Pumps',95132,0,132421409,NULL,NULL),(132421523,'Electrical & Lighting Other','!Electrical!Electrical & Lighting Other',95133,0,132421393,NULL,NULL),(132421524,'Electrical Tools','!Electrical!Electrical Tools',95134,0,132421393,NULL,NULL),(132421525,'Emergency Lights','!Lighting!Emergency Lights',95135,0,132421401,NULL,NULL),(132421526,'Engineer Hammers','!Hand Tools!Engineer Hammers',95136,0,132421396,NULL,NULL),(132421527,'Ergonomic Protection','!Safety & Security!Ergonomic Protection',95137,0,132421410,NULL,NULL),(132421528,'Evaporative Coolers','!Pumps!Evaporative Coolers',95138,0,132421409,NULL,NULL),(132421529,'Exothermic Cutting','!Welding Supplies!Exothermic Cutting',95139,0,132421411,NULL,NULL),(132421530,'Extension & Power Cord Accessories','!Electrical!Extension & Power Cord Accessories',95140,0,132421393,NULL,NULL),(132421531,'Extension & Power Cords','!Electrical!Extension & Power Cords',95141,0,132421393,NULL,NULL),(132421532,'Extractors & Sets','!Hand Tools!Extractors & Sets',95142,0,132421396,NULL,NULL),(132421533,'Eye Protection','!Safety & Security!Eye Protection',95143,0,132421410,NULL,NULL),(132421534,'Fall Protection','!Safety & Security!Fall Protection',95144,0,132421410,NULL,NULL),(132421535,'Fasteners, Clamps & Straps Other','!Fasteners, Clamps & Straps!Fasteners, Clamps & Straps Other',95145,0,132421394,NULL,NULL),(132421536,'Fastening Products','!Fasteners, Clamps & Straps!Fastening Products',95146,0,132421394,NULL,NULL),(132421537,'Faucet Nut Wrenches','!Hand Tools!Faucet Nut Wrenches',95147,0,132421396,NULL,NULL),(132421538,'Faucets','!Plumbing!Faucets',95148,0,132421406,NULL,NULL),(132421539,'Files','!Hand Tools!Files',95149,0,132421396,NULL,NULL),(132421540,'Filler Metals','!Welding Supplies!Filler Metals',95150,0,132421411,NULL,NULL),(132421541,'Filtration','!Plumbing!Filtration',95151,0,132421406,NULL,NULL),(132421542,'Finders & Scanners','!Measuring & Leveling Tools!Finders & Scanners',95152,0,132421404,NULL,NULL),(132421543,'Finishing Tools','!Power Tools!Finishing Tools',95153,0,132421408,NULL,NULL),(132421544,'Fire, Gas & Water Protection','!Safety & Security!Fire, Gas & Water Protection',95154,0,132421410,NULL,NULL),(132421545,'Fire, Gas & Water Protection ','!Safety & Security!Fire, Gas & Water Protection ',95155,0,132421410,NULL,NULL),(132421546,'First Aid Safety','!Safety & Security!First Aid Safety',95156,0,132421410,NULL,NULL),(132421547,'Fittings & Accessories','!MRO Supplies!Fittings & Accessories',95157,0,132421405,NULL,NULL),(132421548,'Fixtures','!Lighting!Fixtures',95158,0,132421401,NULL,NULL),(132421549,'Flange Pins & Aligners','!Measuring & Leveling Tools!Flange Pins & Aligners',95159,0,132421404,NULL,NULL),(132421550,'Flashlight & Lantern Parts & Accessories','!Lighting!Flashlight & Lantern Parts & Accessories',95160,0,132421401,NULL,NULL),(132421551,'Flashlights & Lanterns','!Lighting!Flashlights & Lanterns',95161,0,132421401,NULL,NULL),(132421552,'Foil','!Hospitality & Food Service Supplies!Foil',95162,0,132421398,NULL,NULL),(132421553,'Food Service Disposables','!Hospitality & Food Service Supplies!Food Service Disposables',95163,0,132421398,NULL,NULL),(132421554,'Food Totes','!Hospitality & Food Service Supplies!Food Totes',95164,0,132421398,NULL,NULL),(132421555,'Foodservice Supplies Other','!Hospitality & Food Service Supplies!Foodservice Supplies Other',95165,0,132421398,NULL,NULL),(132421556,'Foot Protection','!Safety & Security!Foot Protection',95166,0,132421410,NULL,NULL),(132421557,'Foyer Lighting','!Lighting!Foyer Lighting',95167,0,132421401,NULL,NULL),(132421558,'Furnaces','!HVAC!Furnaces',95168,0,132421399,NULL,NULL),(132421559,'Fuses','!Electrical!Fuses',95169,0,132421393,NULL,NULL),(132421560,'Gas Cutting & Welding','!Welding Supplies!Gas Cutting & Welding',95170,0,132421411,NULL,NULL),(132421561,'Gate Valves','!Plumbing!Gate Valves',95171,0,132421406,NULL,NULL),(132421562,'Gauges/Gages','!Measuring & Leveling Tools!Gauges/Gages',95172,0,132421404,NULL,NULL),(132421563,'Gauging Tape Refills & Parts','!Measuring & Leveling Tools!Gauging Tape Refills & Parts',95173,0,132421404,NULL,NULL),(132421564,'Gauging Tape Wipers','!Measuring & Leveling Tools!Gauging Tape Wipers',95174,0,132421404,NULL,NULL),(132421565,'Gauging Tapes','!Measuring & Leveling Tools!Gauging Tapes',95175,0,132421404,NULL,NULL),(132421566,'Generators','!Electrical!Generators',95176,0,132421393,NULL,NULL),(132421567,'Grilles, Registers & Diffusers','!HVAC!Grilles, Registers & Diffusers',95177,0,132421399,NULL,NULL),(132421568,'Grinders','!Power Tools!Grinders',95178,0,132421408,NULL,NULL),(132421569,'Ground Fault Circuit Interrupters','!Electrical!Ground Fault Circuit Interrupters',95179,0,132421393,NULL,NULL),(132421570,'Hammer Drilling & Demolition Tools','!Power Tools!Hammer Drilling & Demolition Tools',95180,0,132421408,NULL,NULL),(132421571,'Hammer Drills','!Power Tools!Hammer Drills',95181,0,132421408,NULL,NULL),(132421572,'Hammers, Sledges, Mallets & Axes','!Hand Tools!Hammers, Sledges, Mallets & Axes',95182,0,132421396,NULL,NULL),(132421573,'Hand Protection','!Safety & Security!Hand Protection',95183,0,132421410,NULL,NULL),(132421574,'Hand Tool Organizers & Belts','!Hand Tools!Hand Tool Organizers & Belts',95184,0,132421396,NULL,NULL),(132421575,'Hand Tools Other','!Hand Tools!Hand Tools Other',95185,0,132421396,NULL,NULL),(132421576,'Handsaws & Sets','!Hand Tools!Handsaws & Sets',95186,0,132421396,NULL,NULL),(132421577,'Handsaws & Sets ','!Hand Tools!Handsaws & Sets ',95187,0,132421396,NULL,NULL),(132421578,'Head/Face Protection','!Safety & Security!Head/Face Protection',95188,0,132421410,NULL,NULL),(132421579,'Heat Guns','!Power Tools!Heat Guns',95189,0,132421408,NULL,NULL),(132421580,'Heat Stress Protection','!Safety & Security!Heat Stress Protection',95190,0,132421410,NULL,NULL),(132421581,'Heating Equipment','!HVAC!Heating Equipment',95191,0,132421399,NULL,NULL),(132421582,'Heating Tools','!Power Tools!Heating Tools',95192,0,132421408,NULL,NULL),(132421583,'Hex, Torx & Spline Keys','!Hand Tools!Hex, Torx & Spline Keys',95193,0,132421396,NULL,NULL),(132421584,'Hoists & Winches','!Material Handling!Hoists & Winches',95194,0,132421403,NULL,NULL),(132421585,'Home D?cor','!Hospitality & Food Service Supplies!Home D?cor',95195,0,132421398,NULL,NULL),(132421586,'HVAC Alarms','!HVAC!HVAC Alarms',95196,0,132421399,NULL,NULL),(132421587,'HVAC Blowers & Blower Parts','!HVAC!HVAC Blowers & Blower Parts',95197,0,132421399,NULL,NULL),(132421588,'HVAC Fan Controls','!HVAC!HVAC Fan Controls',95198,0,132421399,NULL,NULL),(132421589,'HVAC Flood Detectors','!HVAC!HVAC Flood Detectors',95199,0,132421399,NULL,NULL),(132421590,'HVAC Tapes','!HVAC!HVAC Tapes',95200,0,132421399,NULL,NULL),(132421591,'HVAC Thermocouples','!HVAC!HVAC Thermocouples',95201,0,132421399,NULL,NULL),(132421592,'HVAC Transformers','!HVAC!HVAC Transformers',95202,0,132421399,NULL,NULL),(132421593,'HVAC Wire & Wire Connectors','!HVAC!HVAC Wire & Wire Connectors',95203,0,132421399,NULL,NULL),(132421594,'Impact Drivers','!Power Tools!Impact Drivers',95204,0,132421408,NULL,NULL),(132421595,'Impact Wrenches','!Power Tools!Impact Wrenches',95205,0,132421408,NULL,NULL),(132421596,'Indicator Holders, Bases & Stands','!Measuring & Leveling Tools!Indicator Holders, Bases & Stands',95206,0,132421404,NULL,NULL),(132421597,'Insect & Weed Controlling Products','!Chemicals, Lubricants & Paints!Insect & Weed Controlling Products',95207,0,132421392,NULL,NULL),(132421598,'Insert Bits & Holders','!Hand Tools!Insert Bits & Holders',95208,0,132421396,NULL,NULL),(132421599,'Insert Bits & Holders ','!Hand Tools!Insert Bits & Holders ',95209,0,132421396,NULL,NULL),(132421600,'Inspection Mirror Refills','!Measuring & Leveling Tools!Inspection Mirror Refills',95210,0,132421404,NULL,NULL),(132421601,'Inspection Mirrors & Magnifiers','!Measuring & Leveling Tools!Inspection Mirrors & Magnifiers',95211,0,132421404,NULL,NULL),(132421602,'Jacks, Lifts & Hydraulics','!Material Handling!Jacks, Lifts & Hydraulics',95212,0,132421403,NULL,NULL),(132421603,'Janitorial Carts','!Janitorial Equipment!Janitorial Carts',95213,0,132421400,NULL,NULL),(132421604,'Janitorial Equipment Other','!Janitorial Equipment!Janitorial Equipment Other',95214,0,132421400,NULL,NULL),(132421605,'Jig Saws','!Power Tools!Jig Saws',95215,0,132421408,NULL,NULL),(132421606,'Key Wrenches','!Hand Tools!Key Wrenches',95216,0,132421396,NULL,NULL),(132421607,'Knives & Multi-Purpose Tools','!Hand Tools!Knives & Multi-Purpose Tools',95217,0,132421396,NULL,NULL),(132421608,'Labeling Tool Parts & Accessories','!Marking Tools!Labeling Tool Parts & Accessories',95218,0,132421402,NULL,NULL),(132421609,'Labeling Tools','!Marking Tools!Labeling Tools',95219,0,132421402,NULL,NULL),(132421610,'Ladders, Platforms & Scaffolding','!Material Handling!Ladders, Platforms & Scaffolding',95220,0,132421403,NULL,NULL),(132421611,'Lavatory','!Plumbing!Lavatory',95221,0,132421406,NULL,NULL),(132421612,'Lawn & Garden Tools','!Hand Tools!Lawn & Garden Tools',95222,0,132421396,NULL,NULL),(132421613,'Leak Detectors & Penetrants','!Chemicals, Lubricants & Paints!Leak Detectors & Penetrants',95223,0,132421392,NULL,NULL),(132421614,'Level Parts & Accessories','!Measuring & Leveling Tools!Level Parts & Accessories',95224,0,132421404,NULL,NULL),(132421615,'Levels','!Measuring & Leveling Tools!Levels',95225,0,132421404,NULL,NULL),(132421616,'Light Bulbs','!Lighting!Light Bulbs',95226,0,132421401,NULL,NULL),(132421617,'Lighting Fixture','!Lighting!Lighting Fixture',95227,0,132421401,NULL,NULL),(132421618,'Locking Devices','!Safety & Security!Locking Devices',95228,0,132421410,NULL,NULL),(132421619,'Lubricants & Penetrants','!Chemicals, Lubricants & Paints!Lubricants & Penetrants',95229,0,132421392,NULL,NULL),(132421620,'Maintenance & Repair Parts','!MRO Supplies!Maintenance & Repair Parts',95230,0,132421405,NULL,NULL),(132421621,'Manual Welding','!Welding Supplies!Manual Welding',95231,0,132421411,NULL,NULL),(132421622,'Marker Holders','!Marking Tools!Marker Holders',95232,0,132421402,NULL,NULL),(132421623,'Marker Parts & Accessories','!Marking Tools!Marker Parts & Accessories',95233,0,132421402,NULL,NULL),(132421624,'Marker Sets','!Marking Tools!Marker Sets',95234,0,132421402,NULL,NULL),(132421625,'Markers','!Marking Tools!Markers',95235,0,132421402,NULL,NULL),(132421626,'Marking Tools Other','!Marking Tools!Marking Tools Other',95236,0,132421402,NULL,NULL),(132421627,'Material Handling Other','!Material Handling!Material Handling Other',95237,0,132421403,NULL,NULL),(132421628,'Measuring & Leveling Tools Other','!Measuring & Leveling Tools!Measuring & Leveling Tools Other',95238,0,132421404,NULL,NULL),(132421629,'Measuring Wheels','!Measuring & Leveling Tools!Measuring Wheels',95239,0,132421404,NULL,NULL),(132421630,'Micrometer Parts & Accessories','!Measuring & Leveling Tools!Micrometer Parts & Accessories',95240,0,132421404,NULL,NULL),(132421631,'Micrometer Sets','!Measuring & Leveling Tools!Micrometer Sets',95241,0,132421404,NULL,NULL),(132421632,'Micrometers','!Measuring & Leveling Tools!Micrometers',95242,0,132421404,NULL,NULL),(132421633,'Mig Welding','!Welding Supplies!Mig Welding',95243,0,132421411,NULL,NULL),(132421634,'Mini-Split Heat Pumps','!HVAC!Mini-Split Heat Pumps',95244,0,132421399,NULL,NULL),(132421635,'Miter Saws','!Power Tools!Miter Saws',95245,0,132421408,NULL,NULL),(132421636,'Mopping Products','!Janitorial Equipment!Mopping Products',95246,0,132421400,NULL,NULL),(132421637,'Motors','!HVAC!Motors',95247,0,132421399,NULL,NULL),(132421638,'MRO Supplies Other','!MRO Supplies!MRO Supplies Other',95248,0,132421405,NULL,NULL),(132421639,'Multi-Purpose Hand Tool Sets','!Hand Tools!Multi-Purpose Hand Tool Sets',95249,0,132421396,NULL,NULL),(132421640,'Multi-Purpose Power Tools','!Power Tools!Multi-Purpose Power Tools',95250,0,132421408,NULL,NULL),(132421641,'Napkins','!Hospitality & Food Service Supplies!Napkins',95251,0,132421398,NULL,NULL),(132421642,'Non Woven Abrasives','!Abrasives!Non Woven Abrasives',95252,0,132421390,NULL,NULL),(132421643,'Oilfield','!MRO Supplies!Oilfield',95253,0,132421405,NULL,NULL),(132421644,'Orbital Sanders','!Power Tools!Orbital Sanders',95254,0,132421408,NULL,NULL),(132421645,'Outdoor Lighting','!Lighting!Outdoor Lighting',95255,0,132421401,NULL,NULL),(132421646,'Pails & Tubs','!Janitorial Equipment!Pails & Tubs',95256,0,132421400,NULL,NULL),(132421647,'Paint & Paint Supplies','!Chemicals, Lubricants & Paints!Paint & Paint Supplies',95257,0,132421392,NULL,NULL),(132421648,'Paper & Plastic Bags','!Hospitality & Food Service Supplies!Paper & Plastic Bags',95258,0,132421398,NULL,NULL),(132421649,'Pencil Accessories','!Marking Tools!Pencil Accessories',95259,0,132421402,NULL,NULL),(132421650,'Pencils','!Marking Tools!Pencils',95260,0,132421402,NULL,NULL),(132421651,'Pendants/Island Lights','!Lighting!Pendants/Island Lights',95261,0,132421401,NULL,NULL),(132421652,'Personal Hygiene','!Janitorial Equipment!Personal Hygiene',95262,0,132421400,NULL,NULL),(132421653,'PEX Crimping Tools','!Power Tools!PEX Crimping Tools',95263,0,132421408,NULL,NULL),(132421654,'Pipe & Conduit Hangers','!Fasteners, Clamps & Straps!Pipe & Conduit Hangers',95264,0,132421394,NULL,NULL),(132421655,'Pipe & Tubing','!Plumbing!Pipe & Tubing',95265,0,132421406,NULL,NULL),(132421656,'Pipe & Tubing Cutters','!Power Tools!Pipe & Tubing Cutters',95266,0,132421408,NULL,NULL),(132421657,'Pipe Freezing Equipment','!Plumbing!Pipe Freezing Equipment',95267,0,132421406,NULL,NULL),(132421658,'Pipe Inspection','!Power Tools!Pipe Inspection',95268,0,132421408,NULL,NULL),(132421659,'Pipe Threader Power Drives','!Power Tools!Pipe Threader Power Drives',95269,0,132421408,NULL,NULL),(132421660,'Pipe Threaders','!Power Tools!Pipe Threaders',95270,0,132421408,NULL,NULL),(132421661,'Pipe Threading Equipment','!Hand Tools!Pipe Threading Equipment',95271,0,132421396,NULL,NULL),(132421662,'Pipe Valves & Fittings','!Plumbing!Pipe Valves & Fittings',95272,0,132421406,NULL,NULL),(132421663,'Planers','!Power Tools!Planers',95273,0,132421408,NULL,NULL),(132421664,'Plasma Cutting','!Welding Supplies!Plasma Cutting',95274,0,132421411,NULL,NULL),(132421665,'Plates','!Hospitality & Food Service Supplies!Plates',95275,0,132421398,NULL,NULL),(132421666,'Pliers & Cutters','!Hand Tools!Pliers & Cutters',95276,0,132421396,NULL,NULL),(132421667,'Plugs & Receptacles','!Electrical!Plugs & Receptacles',95277,0,132421393,NULL,NULL),(132421668,'Plumb Bobs','!Measuring & Leveling Tools!Plumb Bobs',95278,0,132421404,NULL,NULL),(132421669,'Plumbing Nuts, Washers','!Fasteners, Clamps & Straps!Plumbing Nuts, Washers',95279,0,132421394,NULL,NULL),(132421670,'Plumbing Tools','!Hand Tools!Plumbing Tools',95280,0,132421396,NULL,NULL),(132421671,'Pneumatics Other','!Pneumatics!Pneumatics Other',95281,0,132421407,NULL,NULL),(132421672,'Power Centers','!Electrical!Power Centers',95282,0,132421393,NULL,NULL),(132421673,'Power Connectors','!Power Tools!Power Connectors',95283,0,132421408,NULL,NULL),(132421674,'Power Strips','!Electrical!Power Strips',95284,0,132421393,NULL,NULL),(132421675,'Power Tools Other','!Power Tools!Power Tools Other',95285,0,132421408,NULL,NULL),(132421676,'Protractors','!Measuring & Leveling Tools!Protractors',95286,0,132421404,NULL,NULL),(132421677,'Prying Tools','!Hand Tools!Prying Tools',95287,0,132421396,NULL,NULL),(132421678,'Pullers & Sets','!Hand Tools!Pullers & Sets',95288,0,132421396,NULL,NULL),(132421679,'Pump Parts & Accessories','!Pumps!Pump Parts & Accessories',95289,0,132421409,NULL,NULL),(132421680,'Pumping Equipment','!Pumps!Pumping Equipment',95290,0,132421409,NULL,NULL),(132421681,'Push Button Stations','!Electrical!Push Button Stations',95291,0,132421393,NULL,NULL),(132421682,'Putty Knives & Scrapers','!Hand Tools!Putty Knives & Scrapers',95292,0,132421396,NULL,NULL),(132421683,'Racks & Shelving','!Material Handling!Racks & Shelving',95293,0,132421403,NULL,NULL),(132421684,'Radios, Jobsite','!Power Tools!Radios, Jobsite',95294,0,132421408,NULL,NULL),(132421685,'Radius Markers','!Measuring & Leveling Tools!Radius Markers',95295,0,132421404,NULL,NULL),(132421686,'Rags and Wipes','!Janitorial Equipment!Rags and Wipes',95296,0,132421400,NULL,NULL),(132421687,'Reaming & Deburring Tools','!Hand Tools!Reaming & Deburring Tools',95297,0,132421396,NULL,NULL),(132421688,'Recessed Lighting','!Lighting!Recessed Lighting',95298,0,132421401,NULL,NULL),(132421689,'Reciprocating Saw Blades','!Power Tools!Reciprocating Saw Blades',95299,0,132421408,NULL,NULL),(132421690,'Reciprocating Saws','!Power Tools!Reciprocating Saws',95300,0,132421408,NULL,NULL),(132421691,'Replacement Parts','!Lighting!Replacement Parts',95301,0,132421401,NULL,NULL),(132421692,'Respiratory Protection','!Safety & Security!Respiratory Protection',95302,0,132421410,NULL,NULL),(132421693,'Retrieving Tools','!Hand Tools!Retrieving Tools',95304,0,132421396,NULL,NULL),(132421694,'Rods','!Fasteners, Clamps & Straps!Rods',95305,0,132421394,NULL,NULL),(132421695,'Rollers','!Material Handling!Rollers',95306,0,132421403,NULL,NULL),(132421696,'Roof Curbs, Stands & Elbows','!Plumbing!Roof Curbs, Stands & Elbows',95307,0,132421406,NULL,NULL),(132421697,'Roof Drains','!Plumbing!Roof Drains',95308,0,132421406,NULL,NULL),(132421698,'Roof Flashing','!Plumbing!Roof Flashing',95309,0,132421406,NULL,NULL),(132421699,'Roof Jacks','!Plumbing!Roof Jacks',95310,0,132421406,NULL,NULL),(132421700,'Roof Ventilation','!HVAC!Roof Ventilation',95311,0,132421399,NULL,NULL),(132421701,'Rotary Saws','!Power Tools!Rotary Saws',95312,0,132421408,NULL,NULL),(132421702,'Rough Plumbing','!Plumbing!Rough Plumbing',95313,0,132421406,NULL,NULL),(132421703,'Routers','!Power Tools!Routers',95314,0,132421408,NULL,NULL),(132421704,'Routing & Trimming Tools','!Power Tools!Routing & Trimming Tools',95315,0,132421408,NULL,NULL),(132421705,'Rulers','!Measuring & Leveling Tools!Rulers',95316,0,132421404,NULL,NULL),(132421706,'Safety & Security Other','!Safety & Security!Safety & Security Other',95317,0,132421410,NULL,NULL),(132421707,'Safety Storage','!Safety & Security!Safety Storage',95318,0,132421410,NULL,NULL),(132421708,'Saw Combination Kits','!Power Tools!Saw Combination Kits',95319,0,132421408,NULL,NULL),(132421709,'Saw Stands','!Power Tools!Saw Stands',95320,0,132421408,NULL,NULL),(132421710,'Scoops','!Hospitality & Food Service Supplies!Scoops',95321,0,132421398,NULL,NULL),(132421711,'Screw Bits','!Power Tools!Screw Bits',95322,0,132421408,NULL,NULL),(132421712,'Screwdrivers & Nutdrivers','!Hand Tools!Screwdrivers & Nutdrivers',95323,0,132421396,NULL,NULL),(132421713,'Scribers, Pin Vises & Pick Sets','!Hand Tools!Scribers, Pin Vises & Pick Sets',95324,0,132421396,NULL,NULL),(132421714,'Sealants','!Adhesives, Sealants & Tapes!Sealants',95325,0,132421391,NULL,NULL),(132421715,'Sealing Products','!Adhesives, Sealants & Tapes!Sealing Products',95326,0,132421391,NULL,NULL),(132421716,'Shears','!Hand Tools!Shears',95327,0,132421396,NULL,NULL),(132421717,'Shears, Scissors & Snips','!Hand Tools!Shears, Scissors & Snips',95328,0,132421396,NULL,NULL),(132421718,'Shop Vacuums','!Power Tools!Shop Vacuums',95329,0,132421408,NULL,NULL),(132421719,'Shovels & Spades','!Hand Tools!Shovels & Spades',95330,0,132421396,NULL,NULL),(132421720,'Sinks, Parts & Accessories','!Plumbing!Sinks, Parts & Accessories',95331,0,132421406,NULL,NULL),(132421721,'Sledge Hammers','!Hand Tools!Sledge Hammers',95332,0,132421396,NULL,NULL),(132421722,'Soapstone Holders','!Marking Tools!Soapstone Holders',95333,0,132421402,NULL,NULL),(132421723,'Soapstones','!Marking Tools!Soapstones',95334,0,132421402,NULL,NULL),(132421724,'Socket Sets','!Hand Tools!Socket Sets',95335,0,132421396,NULL,NULL),(132421725,'Socket Wrenches','!Hand Tools!Socket Wrenches',95336,0,132421396,NULL,NULL),(132421726,'Sockets, Ratchets, Adaptors & Extensions','!Hand Tools!Sockets, Ratchets, Adaptors & Extensions',95337,0,132421396,NULL,NULL),(132421727,'Soldering & Brazing','!Welding Supplies!Soldering & Brazing',95338,0,132421411,NULL,NULL),(132421728,'Specialty Products','!Chemicals, Lubricants & Paints!Specialty Products',95339,0,132421392,NULL,NULL),(132421729,'Spill Control Safety','!Safety & Security!Spill Control Safety',95340,0,132421410,NULL,NULL),(132421730,'Splice Kits','!Electrical!Splice Kits',95341,0,132421393,NULL,NULL),(132421731,'Splitters','!Hand Tools!Splitters',95342,0,132421396,NULL,NULL),(132421732,'Spud Wrenches','!Hand Tools!Spud Wrenches',95343,0,132421396,NULL,NULL),(132421733,'Square Parts & Accessories','!Measuring & Leveling Tools!Square Parts & Accessories',95344,0,132421404,NULL,NULL),(132421734,'Squares','!Measuring & Leveling Tools!Squares',95345,0,132421404,NULL,NULL),(132421735,'Stamp Sets','!Marking Tools!Stamp Sets',95346,0,132421402,NULL,NULL),(132421736,'Stamps','!Marking Tools!Stamps',95347,0,132421402,NULL,NULL),(132421737,'Stands & Ramps','!Material Handling!Stands & Ramps',95348,0,132421403,NULL,NULL),(132421738,'Staplers & Riveters','!Hand Tools!Staplers & Riveters',95349,0,132421396,NULL,NULL),(132421739,'Stencil Accessories','!Marking Tools!Stencil Accessories',95350,0,132421402,NULL,NULL),(132421740,'Stencil Sets','!Marking Tools!Stencil Sets',95351,0,132421402,NULL,NULL),(132421741,'Stencils','!Marking Tools!Stencils',95352,0,132421402,NULL,NULL),(132421742,'Stool','!Furniture!Stool',95353,0,132421395,NULL,NULL),(132421743,'Storage','!Material Handling!Storage',95354,0,132421403,NULL,NULL),(132421744,'Strapping Products','!Fasteners, Clamps & Straps!Strapping Products',95355,0,132421394,NULL,NULL),(132421745,'Straws','!Hospitality & Food Service Supplies!Straws',95356,0,132421398,NULL,NULL),(132421746,'Stretch Wraps','!Material Handling!Stretch Wraps',95357,0,132421403,NULL,NULL),(132421747,'Switches','!Electrical!Switches',95358,0,132421393,NULL,NULL),(132421748,'Table Saws','!Power Tools!Table Saws',95359,0,132421408,NULL,NULL),(132421749,'Tags','!Marking Tools!Tags',95360,0,132421402,NULL,NULL),(132421750,'Tape Measure Parts & Accessories','!Measuring & Leveling Tools!Tape Measure Parts & Accessories',95361,0,132421404,NULL,NULL),(132421751,'Tape Measures','!Measuring & Leveling Tools!Tape Measures',95362,0,132421404,NULL,NULL),(132421752,'Tape Products','!Adhesives, Sealants & Tapes!Tape Products',95363,0,132421391,NULL,NULL),(132421753,'Tapes','!Adhesives, Sealants & Tapes!Tapes',95364,0,132421391,NULL,NULL),(132421754,'Tappers','!Power Tools!Tappers',95365,0,132421408,NULL,NULL),(132421755,'Temperature Indicators','!Marking Tools!Temperature Indicators',95366,0,132421402,NULL,NULL),(132421756,'Thermometers','!Measuring & Leveling Tools!Thermometers',95367,0,132421404,NULL,NULL),(132421757,'Ticket Holders','!Material Handling!Ticket Holders',95368,0,132421403,NULL,NULL),(132421758,'Tig Welding','!Welding Supplies!Tig Welding',95369,0,132421411,NULL,NULL),(132421759,'Timers & Switches','!Electrical!Timers & Switches',95370,0,132421393,NULL,NULL),(132421760,'Toilet Seats','!Plumbing!Toilet Seats',95371,0,132421406,NULL,NULL),(132421761,'Toilets & Urinals','!Plumbing!Toilets & Urinals',95372,0,132421406,NULL,NULL),(132421762,'Tool Belts, Pouches & Totes','!Hand Tools!Tool Belts, Pouches & Totes',95373,0,132421396,NULL,NULL),(132421763,'Tool Chests & Boxes','!Hand Tools!Tool Chests & Boxes',95374,0,132421396,NULL,NULL),(132421764,'Tool Sets','!Hand Tools!Tool Sets',95375,0,132421396,NULL,NULL),(132421765,'Tool Sharpeners','!Hand Tools!Tool Sharpeners',95376,0,132421396,NULL,NULL),(132421766,'Tool Storage','!Hand Tools!Tool Storage',95377,0,132421396,NULL,NULL),(132421767,'Torch & Weed Burners','!Welding Supplies!Torch & Weed Burners',95378,0,132421411,NULL,NULL),(132421768,'Torque Wrenches','!Hand Tools!Torque Wrenches',95379,0,132421396,NULL,NULL),(132421769,'Traffic Safety','!Safety & Security!Traffic Safety',95380,0,132421410,NULL,NULL),(132421770,'Trailer Hitch Accessories','!Material Handling!Trailer Hitch Accessories',95381,0,132421403,NULL,NULL),(132421771,'Tub Shoe Wrenches','!Hand Tools!Tub Shoe Wrenches',95382,0,132421396,NULL,NULL),(132421772,'Tube Fitting','!Plumbing!Tube Fitting',95383,0,132421406,NULL,NULL),(132421773,'Tube Nailers','!Fasteners, Clamps & Straps!Tube Nailers',95384,0,132421394,NULL,NULL),(132421774,'Tubing','!MRO Supplies!Tubing',95385,0,132421405,NULL,NULL),(132421775,'Under Cabinet Lights','!Lighting!Under Cabinet Lights',95386,0,132421401,NULL,NULL),(132421776,'Underwater Cutting & Welding','!Welding Supplies!Underwater Cutting & Welding',95387,0,132421411,NULL,NULL),(132421777,'Utility Knives','!Hand Tools!Utility Knives',95388,0,132421396,NULL,NULL),(132421778,'Vacuum Parts & Accessories','!Janitorial Equipment!Vacuum Parts & Accessories',95389,0,132421400,NULL,NULL),(132421779,'Vacuums','!Janitorial Equipment!Vacuums',95390,0,132421400,NULL,NULL),(132421780,'Valves & Regulators','!MRO Supplies!Valves & Regulators',95391,0,132421405,NULL,NULL),(132421781,'Ventilation Equipment','!HVAC!Ventilation Equipment',95392,0,132421399,NULL,NULL),(132421782,'Vinyl HVAC Tubing','!HVAC!Vinyl HVAC Tubing',95393,0,132421399,NULL,NULL),(132421783,'Wall Plates','!Electrical!Wall Plates',95394,0,132421393,NULL,NULL),(132421784,'Waste Receptacles','!Janitorial Equipment!Waste Receptacles',95395,0,132421400,NULL,NULL),(132421785,'Water Circulating Pumps','!Pumps!Water Circulating Pumps',95396,0,132421409,NULL,NULL),(132421786,'Water Filters','!Plumbing!Water Filters',95397,0,132421406,NULL,NULL),(132421787,'Water Fountains & Coolers','!Plumbing!Water Fountains & Coolers',95398,0,132421406,NULL,NULL),(132421788,'Water Heater,  Parts & Accessories','!Plumbing!Water Heater,  Parts & Accessories',95399,0,132421406,NULL,NULL),(132421789,'Wedges','!Hand Tools!Wedges',95400,0,132421396,NULL,NULL),(132421790,'Welding & Cutting Accessories','!Welding Supplies!Welding & Cutting Accessories',95401,0,132421411,NULL,NULL),(132421791,'Welding & Cutting Machines','!Welding Supplies!Welding & Cutting Machines',95402,0,132421411,NULL,NULL),(132421792,'Welding Supplies Other','!Welding Supplies!Welding Supplies Other',95403,0,132421411,NULL,NULL),(132421793,'Wheels & Casters','!Material Handling!Wheels & Casters',95404,0,132421403,NULL,NULL),(132421794,'Wiggler Sets','!Measuring & Leveling Tools!Wiggler Sets',95405,0,132421404,NULL,NULL),(132421795,'Window Hardware','!Hardware!Window Hardware',95406,0,132421397,NULL,NULL),(132421796,'Wiping Products','!Janitorial Equipment!Wiping Products',95407,0,132421400,NULL,NULL),(132421797,'Wire & Cable','!Electrical!Wire & Cable',95408,0,132421393,NULL,NULL),(132421798,'Wire Brush Abrasives','!Abrasives!Wire Brush Abrasives',95409,0,132421390,NULL,NULL),(132421799,'Wire Hooks','!Fasteners, Clamps & Straps!Wire Hooks',95410,0,132421394,NULL,NULL),(132421800,'Work Light Parts & Accessories','!Lighting!Work Light Parts & Accessories',95411,0,132421401,NULL,NULL),(132421801,'Work Lights','!Lighting!Work Lights',95412,0,132421401,NULL,NULL),(132421802,'Wrenches','!Hand Tools!Wrenches',95413,0,132421396,NULL,NULL),(132421803,'Pipe & Tubing','!Plumbing!Pipe & Tubing!ABS Pipe',95414,0,132421655,NULL,NULL),(132421804,'Conveyors','!Material Handling!Conveyors!Accessories & Replacement Parts',95415,0,NULL,NULL,NULL),(132421805,'Heat Stress Protection','!Safety & Security!Heat Stress Protection!Activity Drinks',95416,0,132421580,NULL,NULL),(132421806,'Adhesive Products','!Adhesives, Sealants & Tapes!Adhesive Products!Adhesives & Glues',95417,0,132421414,NULL,NULL),(132421807,'Wrenches','!Hand Tools!Wrenches!Adjustable Wrenches',95418,0,132421802,NULL,NULL),(132421808,'Compressors, Blow Guns, Hoses & Fittings','!Pneumatics!Compressors, Blow Guns, Hoses & Fittings!Air Compressors',95419,0,132421481,NULL,NULL),(132421809,'Compressors, Blow Guns, Hoses & Fittings','!Pneumatics!Compressors, Blow Guns, Hoses & Fittings!Air Hose Fittings',95420,0,132421808,NULL,NULL),(132421810,'Roof Ventilation','!HVAC!Roof Ventilation!All Fuel Pipe Flashing',95421,0,132421700,NULL,NULL),(132421811,'Water Heater,  Parts & Accessories','!Plumbing!Water Heater,  Parts & Accessories!Anode Rods',95422,0,132421788,NULL,NULL),(132421812,'Traffic Safety','!Safety & Security!Traffic Safety!Anti-Fatigue Matting',95423,0,132421769,NULL,NULL),(132421813,'Welding & Cutting Accessories','!Welding Supplies!Welding & Cutting Accessories!Anti-Spatter & Chemicals',95424,0,132421790,NULL,NULL),(132421814,'Carts & Trucks','!Material Handling!Carts & Trucks!Appliance Trucks',95425,0,132421457,NULL,NULL),(132421815,'Arc Gouging','!Welding Supplies!Arc Gouging!Arc Gouging Torches & Cables',95426,0,132421424,NULL,NULL),(132421816,'Audio/Visual Equipment','!Furniture!Audio/Visual Equipment!Audio/Visual Carts',95427,0,NULL,NULL,NULL),(132421817,'Audio/Visual Equipment','!Furniture!Audio/Visual Equipment!Audio/Visual Equipment',95428,0,132421816,NULL,NULL),(132421818,'Ergonomic Protection','!Safety & Security!Ergonomic Protection!Back Support',95429,0,132421527,NULL,NULL),(132421819,'Conveyors','!Material Handling!Conveyors!Ball Transfer Conveyors & Tables',95430,0,132421804,NULL,NULL),(132421820,'Ball Valves & Accessories','!Plumbing!Ball Valves & Accessories!Ball Valves',95431,0,132421430,NULL,NULL),(132421821,'Racks & Shelving','!Material Handling!Racks & Shelving!Bar & Sheet Storage Racks',95432,0,132421683,NULL,NULL),(132421822,'Carts & Trucks','!Material Handling!Carts & Trucks!Bar Trucks',95433,0,132421814,NULL,NULL),(132421823,'Prying Tools','!Hand Tools!Prying Tools!Bars',95434,0,132421677,NULL,NULL),(132421824,'Faucets','!Plumbing!Faucets!Bath Faucets',95435,0,132421538,NULL,NULL),(132421825,'Fixtures','!Lighting!Fixtures!Bathroom Fixtures',95436,0,132421548,NULL,NULL),(132421826,'Cordless Tool Batteries & Chargers','!Power Tools!Cordless Tool Batteries & Chargers!Batteries, Cordless Tool',95437,0,132421492,NULL,NULL),(132421827,'Pump Parts & Accessories','!Pumps!Pump Parts & Accessories!Bearing Assemblies',95438,0,132421679,NULL,NULL),(132421828,'Appliances','!Hospitality & Food Service Supplies!Appliances!Beverage Dispenser',95439,0,132421423,NULL,NULL),(132421829,'Outdoor Products','!Safety & Security!Outdoor Products!Bike Racks & Bike Locks',95440,0,NULL,NULL,NULL),(132421830,'Storage','!Material Handling!Storage!Bin Accessories',95441,0,132421743,NULL,NULL),(132421831,'Storage','!Material Handling!Storage!Bin Carts & Parts Organizers',95442,0,132421830,NULL,NULL),(132421832,'Storage','!Material Handling!Storage!Bin Racks',95443,0,132421831,NULL,NULL),(132421833,'Storage','!Material Handling!Storage!Bin Shelving',95444,0,132421832,NULL,NULL),(132421834,'Storage','!Material Handling!Storage!Bins',95445,0,132421833,NULL,NULL),(132421835,'Compressors, Blow Guns, Hoses & Fittings','!Pneumatics!Compressors, Blow Guns, Hoses & Fittings!Blow Guns',95446,0,132421809,NULL,NULL),(132421836,'Ventilation Equipment','!HVAC!Ventilation Equipment!Blower Accessories',95447,0,132421781,NULL,NULL),(132421837,'Outdoor Products','!Safety & Security!Outdoor Products!Bollards & Covers',95448,0,132421829,NULL,NULL),(132421838,'Pliers & Cutters','!Hand Tools!Pliers & Cutters!Bolt & Chain Cutter Parts & Accessories',95449,0,132421666,NULL,NULL),(132421839,'Office Accessories & Furnishings','!Furniture!Office Accessories & Furnishings!Book Carts',95450,0,NULL,NULL,NULL),(132421840,'Automotive Maintenance Products','!MRO Supplies!Automotive Maintenance Products!Booster Cables',95451,0,132421426,NULL,NULL),(132421841,'Foot Protection','!Safety & Security!Foot Protection!Boots & Shoes',95452,0,132421556,NULL,NULL),(132421842,'Storage','!Material Handling!Storage!Bottles, Jars & Jugs',95453,0,132421834,NULL,NULL),(132421843,'Packaging & Shipping Supplies','!Material Handling!Packaging & Shipping Supplies!Boxes & Cartons',95454,0,NULL,NULL,NULL),(132421844,'Pipe Valves & Fittings','!Plumbing!Pipe Valves & Fittings!Brass Fitting & Flanges',95455,0,132421662,NULL,NULL),(132421845,'Cranes','!Material Handling!Cranes!Bridge Cranes',95456,0,NULL,NULL,NULL),(132421846,'Brushing & Sweeping Products','!Janitorial Equipment!Brushing & Sweeping Products!Brush & Broom Handles',95457,0,132421446,NULL,NULL),(132421847,'Brushing & Sweeping Products','!Janitorial Equipment!Brushing & Sweeping Products!Brush & Brooms',95458,0,132421846,NULL,NULL),(132421848,'Brushing & Sweeping Products','!Janitorial Equipment!Brushing & Sweeping Products!Brushes & Brooms',95459,0,132421847,NULL,NULL),(132421849,'Carts & Trucks','!Material Handling!Carts & Trucks!Bulk & Basket Trucks',95460,0,132421822,NULL,NULL),(132421850,'Storage','!Material Handling!Storage!Bulk Containers',95461,0,132421842,NULL,NULL),(132421851,'Packaging & Shipping Supplies','!Material Handling!Packaging & Shipping Supplies!Bundling Materials',95462,0,132421843,NULL,NULL),(132421852,'Storage','!Material Handling!Storage!Cabinets & Lockers',95463,0,132421850,NULL,NULL),(132421853,'Dock & Warehouse','!Safety & Security!Dock & Warehouse!Cable & Hose Bridges',95464,0,NULL,NULL,NULL),(132421854,'Strapping Products','!Fasteners, Clamps & Straps!Strapping Products!Cable Ties',95465,0,132421744,NULL,NULL),(132421855,'Cafeteria Furniture','!Furniture!Cafeteria Furniture!Cafeteria Booths',95466,0,NULL,NULL,NULL),(132421856,'Cafeteria Furniture','!Furniture!Cafeteria Furniture!Cafeteria Chairs',95467,0,132421855,NULL,NULL),(132421857,'Cafeteria Furniture','!Furniture!Cafeteria Furniture!Cafeteria Cluster Seating',95468,0,132421856,NULL,NULL),(132421858,'Cafeteria Furniture','!Furniture!Cafeteria Furniture!Cafeteria Table & Chair Sets',95469,0,132421857,NULL,NULL),(132421859,'Cafeteria Furniture','!Furniture!Cafeteria Furniture!Cafeteria Tables',95470,0,132421858,NULL,NULL),(132421860,'Racks & Shelving','!Material Handling!Racks & Shelving!Cantilever Racks',95471,0,132421821,NULL,NULL),(132421861,'Dock & Warehouse','!Safety & Security!Dock & Warehouse!Cargo Bars & Straps',95472,0,132421853,NULL,NULL),(132421862,'Mats','!Janitorial Equipment!Mats!Carpet and Entrance',95473,0,NULL,NULL,NULL),(132421863,'Respiratory Protection','!Safety & Security!Respiratory Protection!Cartridge Respirator Parts & Accessories',95474,0,132421692,NULL,NULL),(132421864,'Sealing Products','!Adhesives, Sealants & Tapes!Sealing Products!Caulk Guns',95475,0,132421715,NULL,NULL),(132421865,'Chisels, Punches & Pins','!Hand Tools!Chisels, Punches & Pins!Chisels',95476,0,132421466,NULL,NULL),(132421866,'Water Circulating Pumps','!Pumps!Water Circulating Pumps!Circulators & Recirculator Pumps',95477,0,132421785,NULL,NULL),(132421867,'Blueprint Storage & Accessories','!Furniture!Blueprint Storage & Accessories!Clamps',95478,0,NULL,NULL,NULL),(132421868,'Clamps & Vises','!Hand Tools!Clamps & Vises!Clamps & Spreaders',95479,0,132421472,NULL,NULL),(132421869,'Cleaning Products','!Chemicals, Lubricants & Paints!Cleaning Products!Cleaners & Degreasers',95480,0,132421474,NULL,NULL),(132421870,'Ladders, Platforms & Scaffolding','!Material Handling!Ladders, Platforms & Scaffolding!Climber Parts & Accessories',95481,0,132421610,NULL,NULL),(132421871,'Coated Abrasives','!Abrasives!Coated Abrasives!Coated Flap Wheel Abrasives',95482,0,132421477,NULL,NULL),(132421872,'Multi-Purpose Power Tools','!Power Tools!Multi-Purpose Power Tools!Combo Kits Cordless',95483,0,132421640,NULL,NULL),(132421873,'Office Partitions & Modular System','!Furniture!Office Partitions & Modular System!Complete Workstations',95484,0,NULL,NULL,NULL),(132421874,'Computer Furniture','!Furniture!Computer Furniture!Computer Workstations',95485,0,NULL,NULL,NULL),(132421875,'Conference Room Furniture','!Furniture!Conference Room Furniture!Conference Room Chairs',95486,0,NULL,NULL,NULL),(132421876,'Conference Room Furniture','!Furniture!Conference Room Furniture!Conference Room Tables',95487,0,132421875,NULL,NULL),(132421877,'Conveyors','!Material Handling!Conveyors!Conveyor Stands',95488,0,132421819,NULL,NULL),(132421878,'Tubing','!MRO Supplies!Tubing!Copper Tubing',95489,0,132421774,NULL,NULL),(132421879,'Clothing Protection','!Safety & Security!Clothing Protection!Coveralls & Overalls',95490,0,132421476,NULL,NULL),(132421880,'Crimping & Stripping Tools','!Hand Tools!Crimping & Stripping Tools!Crimpers & Strippers',95491,0,132421495,NULL,NULL),(132421881,'Office Accessories & Furnishings','!Furniture!Office Accessories & Furnishings!Crowd Control Posts',95492,0,132421839,NULL,NULL),(132421882,'Dock & Warehouse','!Safety & Security!Dock & Warehouse!Curtain Walls',95493,0,132421861,NULL,NULL),(132421883,'Conveyors','!Material Handling!Conveyors!Curved Sections',95494,0,132421877,NULL,NULL),(132421884,'Packaging & Shipping Supplies','!Material Handling!Packaging & Shipping Supplies!Cushioning & Protective Packaging',95495,0,132421851,NULL,NULL),(132421885,'Carts & Trucks','!Material Handling!Carts & Trucks!Cylinder Carts & Trucks',95496,0,132421849,NULL,NULL),(132421886,'Safety Storage','!Safety & Security!Safety Storage!Cylinder Trucks',95497,0,132421707,NULL,NULL),(132421887,'Fire, Gas & Water Protection ','!Safety & Security!Fire, Gas & Water Protection !Detectors',95498,0,132421545,NULL,NULL),(132421888,'Dock & Warehouse','!Safety & Security!Dock & Warehouse!Dock & Warehouse Doors',95500,0,132421882,NULL,NULL),(132421889,'Dock & Warehouse','!Safety & Security!Dock & Warehouse!Dock Bumpers',95501,0,132421888,NULL,NULL),(132421890,'Dock & Warehouse','!Safety & Security!Dock & Warehouse!Dock Lights',95502,0,132421889,NULL,NULL),(132421891,'Dock & Warehouse','!Safety & Security!Dock & Warehouse!Dock Plates & Boards',95503,0,132421890,NULL,NULL),(132421892,'Dock & Warehouse','!Safety & Security!Dock & Warehouse!Dock Seals & Shelters',95504,0,132421891,NULL,NULL),(132421893,'Carts & Trucks','!Material Handling!Carts & Trucks!Dollies',95505,0,132421885,NULL,NULL),(132421894,'Carts & Trucks','!Material Handling!Carts & Trucks!Dolly Pry Bars',95506,0,132421893,NULL,NULL),(132421895,'Office Accessories & Furnishings','!Furniture!Office Accessories & Furnishings!Drafting & Magnifying Lamps',95507,0,132421881,NULL,NULL),(132421896,'Mats','!Janitorial Equipment!Mats!Drainage',95508,0,132421862,NULL,NULL),(132421897,'Drilling & Fastening Tools','!Power Tools!Drilling & Fastening Tools!Drills',95509,0,132421515,NULL,NULL),(132421898,'Carts & Trucks','!Material Handling!Carts & Trucks!Drum Dollies',95510,0,132421894,NULL,NULL),(132421899,'Storage','!Material Handling!Storage!Drums',95511,0,132421852,NULL,NULL),(132421900,'Storage','!Material Handling!Storage!Drums, Drum Handling & Storage',95512,0,132421899,NULL,NULL),(132421901,'Racks & Shelving','!Material Handling!Racks & Shelving!Dunnage Racks',95513,0,132421860,NULL,NULL),(132421902,'Brushing & Sweeping Products','!Janitorial Equipment!Brushing & Sweeping Products!Dust Pans',95514,0,132421848,NULL,NULL),(132421903,'Ear Protection','!Safety & Security!Ear Protection!Earmuffs',95515,0,132421520,NULL,NULL),(132421904,'Ear Protection ','!Safety & Security!Ear Protection !Earplugs',95516,0,NULL,NULL,NULL),(132421905,'Bending & Flaring Tools','!Hand Tools!Bending & Flaring Tools!Electric & Hydraulic Bender Parts & Accessories',95517,0,132421438,NULL,NULL),(132421906,'Manual Welding','!Welding Supplies!Manual Welding!Electrode Holders',95518,0,132421621,NULL,NULL),(132421907,'Office Accessories & Furnishings','!Furniture!Office Accessories & Furnishings!Electronic Safes',95519,0,132421895,NULL,NULL),(132421908,'Exothermic Cutting','!Welding Supplies!Exothermic Cutting!Exothermic Torches & Kits',95520,0,132421529,NULL,NULL),(132421909,'Conveyors','!Material Handling!Conveyors!Expandable/Portable Conveyors',95521,0,132421883,NULL,NULL),(132421910,'Extractors & Sets','!Hand Tools!Extractors & Sets!Extractors',95522,0,132421532,NULL,NULL),(132421911,'First Aid Safety','!Safety & Security!First Aid Safety!Eye Wash',95523,0,132421546,NULL,NULL),(132421912,'Head/Face Protection','!Safety & Security!Head/Face Protection!Faceshields & Visors',95524,0,132421578,NULL,NULL),(132421913,'Air Cooling Equipment','!HVAC!Air Cooling Equipment!Fan Accessories',95525,0,132421417,NULL,NULL),(132421914,'Gauges/Gages','!Measuring & Leveling Tools!Gauges/Gages!Feeler & Pitch Gauge Sets',95526,0,132421562,NULL,NULL),(132421915,'Files','!Hand Tools!Files!File Accessories',95527,0,132421539,NULL,NULL),(132421916,'Blueprint Storage & Accessories','!Furniture!Blueprint Storage & Accessories!File Cabinets',95528,0,132421867,NULL,NULL),(132421917,'Office Partitions & Modular System','!Furniture!Office Partitions & Modular System!Files',95529,0,132421873,NULL,NULL),(132421918,'Water Filters','!Plumbing!Water Filters!Filter Cartridges',95530,0,132421786,NULL,NULL),(132421919,'Filtration','!Plumbing!Filtration!Filters & Strainers',95531,0,132421541,NULL,NULL),(132421920,'File Cabinets','!Furniture!File Cabinets!Fireproof Files',95532,0,NULL,NULL,NULL),(132421921,'First Aid Safety','!Safety & Security!First Aid Safety!First Aid Kits',95533,0,132421911,NULL,NULL),(132421922,'Fittings & Accessories','!MRO Supplies!Fittings & Accessories!Fittings',95534,0,132421547,NULL,NULL),(132421923,'Conveyors','!Material Handling!Conveyors!Fixed Conveyors',95535,0,132421909,NULL,NULL),(132421924,'Outdoor Products','!Safety & Security!Outdoor Products!Flags',95536,0,132421837,NULL,NULL),(132421925,'Light Bulbs','!Lighting!Light Bulbs!Fluorescent Bulbs',95537,0,132421616,NULL,NULL),(132421926,'Lighting Fixture','!Lighting!Lighting Fixture!Fluorescent Lighting Accessories',95538,0,132421617,NULL,NULL),(132421927,'Office Chairs & Stools','!Furniture!Office Chairs & Stools!Folding Chairs',95539,0,NULL,NULL,NULL),(132421928,'Office Tables','!Furniture!Office Tables!Folding Tables',95540,0,NULL,NULL,NULL),(132421929,'Foot Protection','!Safety & Security!Foot Protection!Footwear Covers',95541,0,132421841,NULL,NULL),(132421930,'Office Partitions & Modular System','!Furniture!Office Partitions & Modular System!Freestanding Panels',95542,0,132421917,NULL,NULL),(132421931,'Oilfield','!MRO Supplies!Oilfield!Fullers Earth',95543,0,132421643,NULL,NULL),(132421932,'Carts & Trucks','!Material Handling!Carts & Trucks!Furniture Dollies',95544,0,132421898,NULL,NULL),(132421933,'Office Accessories & Furnishings','!Furniture!Office Accessories & Furnishings!Furniture Trucks & Caddies',95545,0,132421907,NULL,NULL),(132421934,'Cranes','!Material Handling!Cranes!Gantry Cranes',95546,0,132421845,NULL,NULL),(132421935,'Hand Protection','!Safety & Security!Hand Protection!Gloves',95547,0,132421573,NULL,NULL),(132421936,'Racks & Shelving','!Material Handling!Racks & Shelving!Gravity Flow Racks',95548,0,132421901,NULL,NULL),(132421937,'Pumping Equipment','!Pumps!Pumping Equipment!Grease & Oil Pumps',95549,0,132421680,NULL,NULL),(132421938,'Grilles, Registers & Diffusers','!HVAC!Grilles, Registers & Diffusers!Grilles',95550,0,132421567,NULL,NULL),(132421939,'Finishing Tools','!Power Tools!Finishing Tools!Grinders',95551,0,132421543,NULL,NULL),(132421940,'Hammers, Sledges, Mallets & Axes','!Hand Tools!Hammers, Sledges, Mallets & Axes!Hammers',95552,0,132421572,NULL,NULL),(132421941,'Personal Hygiene','!Janitorial Equipment!Personal Hygiene!Hand Cleaners',95553,0,132421652,NULL,NULL),(132421942,'Handsaws & Sets','!Hand Tools!Handsaws & Sets!Handsaws',95554,0,132421576,NULL,NULL),(132421943,'Carts & Trucks','!Material Handling!Carts & Trucks!HandTrucks',95555,0,132421932,NULL,NULL),(132421944,'Head/Face Protection','!Safety & Security!Head/Face Protection!Hard Hats & Caps',95556,0,132421912,NULL,NULL),(132421945,'Fall Protection','!Safety & Security!Fall Protection!Harnesses',95557,0,132421534,NULL,NULL),(132421946,'Packaging & Shipping Supplies','!Material Handling!Packaging & Shipping Supplies!Heat Sealers & Shrink Film',95558,0,132421884,NULL,NULL),(132421947,'Office Accessories & Furnishings','!Furniture!Office Accessories & Furnishings!Heated Footrest & Heating Panel',95559,0,132421933,NULL,NULL),(132421948,'Heating Equipment','!HVAC!Heating Equipment!Heating Cables & Insulation',95560,0,132421581,NULL,NULL),(132421949,'Insert Bits & Holders','!Hand Tools!Insert Bits & Holders!Hex Bits',95561,0,132421598,NULL,NULL),(132421950,'Hex, Torx & Spline Keys','!Hand Tools!Hex, Torx & Spline Keys!Hex Key Sets',95562,0,132421583,NULL,NULL),(132421951,'Hoists & Winches','!Material Handling!Hoists & Winches!Hoist & Puller Parts & Accessories',95563,0,132421584,NULL,NULL),(132421952,'Hoists & Winches','!Material Handling!Hoists & Winches!Hoists & Pullers',95564,0,132421951,NULL,NULL),(132421953,'Clamping Products','!Fasteners, Clamps & Straps!Clamping Products!Hose Clamps',95565,0,132421471,NULL,NULL),(132421954,'Dock & Warehouse','!Safety & Security!Dock & Warehouse!Industrial Fans & Heaters',95566,0,132421892,NULL,NULL),(132421955,'Dock & Warehouse','!Safety & Security!Dock & Warehouse!Industrial Ladders',95567,0,132421954,NULL,NULL),(132421956,'Outdoor Products','!Safety & Security!Outdoor Products!Industrial Sweepers',95568,0,132421924,NULL,NULL),(132421957,'Dock & Warehouse','!Safety & Security!Dock & Warehouse!Industrial Trailer Jacks',95569,0,132421955,NULL,NULL),(132421958,'Insect & Weed Controlling Products','!Chemicals, Lubricants & Paints!Insect & Weed Controlling Products!Insect Killers',95570,0,132421597,NULL,NULL),(132421959,'Traffic Safety','!Safety & Security!Traffic Safety!Inspection Mirror',95571,0,132421812,NULL,NULL),(132421960,'Rough Plumbing','!Plumbing!Rough Plumbing!Installation Supplies',95572,0,132421702,NULL,NULL),(132421961,'Carts & Trucks','!Material Handling!Carts & Trucks!Instruments Carts & Trucks',95573,0,132421943,NULL,NULL),(132421962,'Jacks, Lifts & Hydraulics','!Material Handling!Jacks, Lifts & Hydraulics!Jacks',95574,0,132421602,NULL,NULL),(132421963,'Cranes','!Material Handling!Cranes!Jib Cranes',95575,0,132421934,NULL,NULL),(132421964,'Storage','!Material Handling!Storage!Jobsite Boxes',95576,0,132421900,NULL,NULL),(132421965,'Concrete & Masonry Tools','!Hand Tools!Concrete & Masonry Tools!Jointers & Joint Rakers',95577,0,132421482,NULL,NULL),(132421966,'Office Accessories & Furnishings','!Furniture!Office Accessories & Furnishings!Key Lock Boxes',95578,0,132421947,NULL,NULL),(132421967,'Office Partitions & Modular System','!Furniture!Office Partitions & Modular System!Keyboard Tray & Center Drawer',95579,0,132421930,NULL,NULL),(132421968,'Maintenance & Repair Parts','!MRO Supplies!Maintenance & Repair Parts!Keystocks',95580,0,132421620,NULL,NULL),(132421969,'Sinks, Parts & Accessories','!Plumbing!Sinks, Parts & Accessories!Kitchen Sinks',95581,0,132421720,NULL,NULL),(132421970,'Ergonomic Protection','!Safety & Security!Ergonomic Protection!Knee Pads',95582,0,132421818,NULL,NULL),(132421971,'Electrical Tools','!Electrical!Electrical Tools!Knockout Punch Parts & Accessories',95583,0,132421524,NULL,NULL),(132421972,'Ladders, Platforms & Scaffolding','!Material Handling!Ladders, Platforms & Scaffolding!Ladders',95584,0,132421870,NULL,NULL),(132421973,'Computer Furniture','!Furniture!Computer Furniture!LAN Stations',95585,0,132421874,NULL,NULL),(132421974,'Carts & Trucks','!Material Handling!Carts & Trucks!Landscape Carts and Wagons',95586,0,132421961,NULL,NULL),(132421975,'Fall Protection','!Safety & Security!Fall Protection!Lanyards',95587,0,132421945,NULL,NULL),(132421976,'File Cabinets','!Furniture!File Cabinets!Lateral Files',95588,0,132421920,NULL,NULL),(132421977,'Outdoor Products','!Safety & Security!Outdoor Products!Lawn Care & Landscaping',95589,0,132421956,NULL,NULL),(132421978,'Specialty Products','!Chemicals, Lubricants & Paints!Specialty Products!Layout Fluids',95590,0,132421728,NULL,NULL),(132421979,'Jacks, Lifts & Hydraulics ','!Material Handling!Jacks, Lifts & Hydraulics !Lift Trucks',95591,0,NULL,NULL,NULL),(132421980,'Dock & Warehouse','!Safety & Security!Dock & Warehouse!Line Stripers & Floor Tapes',95592,0,132421957,NULL,NULL),(132421985,'Dock & Warehouse','!Safety & Security!Dock & Warehouse!Loading Ramps',95593,0,132421980,NULL,NULL),(132421986,'Locking Devices','!Safety & Security!Locking Devices!Lockouts & Hasps',95594,0,132421618,NULL,NULL),(132421987,'Workbenches & Workshop Furniture','!Furniture!Workbenches & Workshop Furniture!Machine Stands',95595,0,NULL,NULL,NULL),(132421992,'Carts & Trucks','!Material Handling!Carts & Trucks!Machinery Dollies',95596,0,132421974,NULL,NULL),(132421993,'Leak Detectors & Penetrants','!Chemicals, Lubricants & Paints!Leak Detectors & Penetrants!Magnetic Particles',95597,0,132421613,NULL,NULL),(132421994,'Retrieving Tools','!Hand Tools!Retrieving Tools!Magnetic Retrievers',95598,0,132421693,NULL,NULL),(132421995,'Carts & Trucks','!Material Handling!Carts & Trucks!Maintenance Carts',95599,0,132421992,NULL,NULL),(132421996,'Tapes','!Adhesives, Sealants & Tapes!Tapes!Mastic Tapes',95600,0,132421753,NULL,NULL),(132421997,'Workbenches & Workshop Furniture','!Furniture!Workbenches & Workshop Furniture!Mats',95601,0,132421987,NULL,NULL),(132421998,'Mig Welding','!Welding Supplies!Mig Welding!Mig Gun Parts & Accessories',95602,0,132421633,NULL,NULL),(132421999,'Dock & Warehouse','!Safety & Security!Dock & Warehouse!Modular Buildings',95603,0,132421985,NULL,NULL),(132422000,'Mopping Products','!Janitorial Equipment!Mopping Products!Mop & Squeegee Handles',95604,0,132421636,NULL,NULL),(132422001,'Mopping Products','!Janitorial Equipment!Mopping Products!Mop Buckets',95605,0,132422000,NULL,NULL),(132422002,'Mopping Products','!Janitorial Equipment!Mopping Products!Mops',95606,0,132422001,NULL,NULL),(132422003,'Conveyors','!Material Handling!Conveyors!Motorized Belt Conveyors',95607,0,132421923,NULL,NULL),(132422004,'Lubricants & Penetrants','!Chemicals, Lubricants & Paints!Lubricants & Penetrants!Multi-Purpose Lubricants',95608,0,132421619,NULL,NULL),(132422005,'Multi-Purpose Hand Tool Sets','!Hand Tools!Multi-Purpose Hand Tool Sets!Multi-Purpose Tool Sets',95609,0,132421639,NULL,NULL),(132422006,'Knives & Multi-Purpose Tools','!Hand Tools!Knives & Multi-Purpose Tools!Multi-Purpose Tools',95610,0,132421607,NULL,NULL),(132422007,'Office Tables','!Furniture!Office Tables!Non-Folding Tables',95611,0,132421928,NULL,NULL),(132422008,'Splitters','!Hand Tools!Splitters!Nut Splitters',95612,0,132421731,NULL,NULL),(132422009,'Office Chairs & Stools','!Furniture!Office Chairs & Stools!Office Chairs',95613,0,132421927,NULL,NULL),(132422010,'Office Accessories & Furnishings','!Furniture!Office Accessories & Furnishings!Office Step Stools',95614,0,132421966,NULL,NULL),(132422011,'Office Chairs & Stools','!Furniture!Office Chairs & Stools!Office Stools',95615,0,132422009,NULL,NULL),(132422013,'Spill Control Safety','!Safety & Security!Spill Control Safety!Oil Waste Receptacles',95616,0,132421729,NULL,NULL),(132422014,'Safety Storage','!Safety & Security!Safety Storage!Outdoor Drum & IBC Storage',95617,0,132421886,NULL,NULL),(132422015,'Outdoor Products','!Safety & Security!Outdoor Products!Outdoor Furniture',95618,0,132421977,NULL,NULL),(132422016,'Outdoor Products','!Safety & Security!Outdoor Products!Outdoor Security Fencing & Enclosures',95619,0,132422015,NULL,NULL),(132422017,'Outdoor Products','!Safety & Security!Outdoor Products!Outdoor Shelters & Tents',95620,0,132422016,NULL,NULL),(132422018,'Outdoor Products','!Safety & Security!Outdoor Products!Outdoor Smoking Receptacles',95621,0,132422017,NULL,NULL),(132422019,'Outdoor Products','!Safety & Security!Outdoor Products!Outdoor Trash Cans & Tops',95622,0,132422018,NULL,NULL),(132422020,'Outdoor Lighting','!Lighting!Outdoor Lighting!Outdoor Wall Sconces',95623,0,132421645,NULL,NULL),(132422021,'Packaging & Shipping Supplies','!Material Handling!Packaging & Shipping Supplies!Packing Benches & Carts',95624,0,132421946,NULL,NULL),(132422022,'Locking Devices','!Safety & Security!Locking Devices!Padlocks & Cables',95625,0,132421986,NULL,NULL),(132422023,'Storage','!Material Handling!Storage!Pails & Accessories',95626,0,132421964,NULL,NULL),(132422024,'Paint & Paint Supplies','!Chemicals, Lubricants & Paints!Paint & Paint Supplies!Paints',95627,0,132421647,NULL,NULL),(132422025,'Packaging & Shipping Supplies','!Material Handling!Packaging & Shipping Supplies!Pallet Covers',95628,0,132422021,NULL,NULL),(132422026,'Racks & Shelving','!Material Handling!Racks & Shelving!Pallet Racks',95629,0,132421936,NULL,NULL),(132422027,'Pallets','!Material Handling!Pallets!Pallet Surrounds',95630,0,5877352,NULL,NULL),(132422028,'Pallets','!Material Handling!Pallets!Pallets',95631,0,132422027,NULL,NULL),(132422029,'Carts & Trucks','!Material Handling!Carts & Trucks!Panel & Sheet Trucks',95632,0,132421995,NULL,NULL),(132422030,'Packaging & Shipping Supplies','!Material Handling!Packaging & Shipping Supplies!Paper Bags',95633,0,132422025,NULL,NULL),(132422031,'Outdoor Products','!Safety & Security!Outdoor Products!Parking Lot & Traffic Safety',95634,0,132422019,NULL,NULL),(132422032,'Workbenches & Workshop Furniture','!Furniture!Workbenches & Workshop Furniture!Part Washers & Wash Sinks',95635,0,132421997,NULL,NULL),(132422033,'Office Accessories & Furnishings','!Furniture!Office Accessories & Furnishings!Payroll Clocks',95636,0,132422010,NULL,NULL),(132422034,'Pipe Freezing Equipment','!Plumbing!Pipe Freezing Equipment!Pipe Freezers',95637,0,132421657,NULL,NULL),(132422035,'Brackets','!Fasteners, Clamps & Straps!Brackets!Pipe Support Brackets',95638,0,132421444,NULL,NULL),(132422036,'Tape Products','!Adhesives, Sealants & Tapes!Tape Products!Pipe Tapes',95639,0,132421752,NULL,NULL),(132422037,'Automotive Tools','!Hand Tools!Automotive Tools!Piston Compressors',95640,0,132421427,NULL,NULL),(132422038,'Welding & Cutting Machines','!Welding Supplies!Welding & Cutting Machines!Plasma Cutters',95641,0,132421791,NULL,NULL),(132422039,'Plasma Cutting','!Welding Supplies!Plasma Cutting!Plasma Parts',95642,0,132421664,NULL,NULL),(132422040,'Wheels & Casters','!Material Handling!Wheels & Casters!Plate Casters',95643,0,132421793,NULL,NULL),(132422041,'Carts & Trucks','!Material Handling!Carts & Trucks!Platform Trucks',95644,0,132422029,NULL,NULL),(132422042,'Demolition Tools','!Pneumatics!Demolition Tools!Pneumatic Scalers',95645,0,132421502,NULL,NULL),(132422043,'Packaging & Shipping Supplies','!Material Handling!Packaging & Shipping Supplies!Poly Bags & Bag Sealers',95646,0,132422030,NULL,NULL),(132422044,'Workbenches & Workshop Furniture','!Furniture!Workbenches & Workshop Furniture!Power Tools',95647,0,132422032,NULL,NULL),(132422045,'Computer Furniture','!Furniture!Computer Furniture!Printer & Fax Stands',95648,0,132421973,NULL,NULL),(132422046,'Audio/Visual Equipment','!Furniture!Audio/Visual Equipment!Projector Stands',95649,0,132421817,NULL,NULL),(132422047,'Pullers & Sets','!Hand Tools!Pullers & Sets!Pullers',95650,0,132421678,NULL,NULL),(132422048,'Chisel, Punches & Pins','!Hand Tools!Chisel, Punches & Pins!Punch & Chisel Sets',95651,0,132421465,NULL,NULL),(132422049,'Blueprint Storage & Accessories','!Furniture!Blueprint Storage & Accessories!Racks',95652,0,132421916,NULL,NULL),(132422050,'Reaming & Deburring Tools','!Hand Tools!Reaming & Deburring Tools!Reamers & Deburring Tools',95653,0,132421687,NULL,NULL),(132422051,'Reception Room Furniture','!Furniture!Reception Room Furniture!Reception Room Beam Seating',95654,0,NULL,NULL,NULL),(132422052,'Office Partitions & Modular System','!Furniture!Office Partitions & Modular System!Reception Stations',95655,0,132421967,NULL,NULL),(132422053,'Cutting Tools','!Power Tools!Cutting Tools!Reciprocating Saws',95656,0,132421499,NULL,NULL),(132422054,'Racks & Shelving','!Material Handling!Racks & Shelving!Reel & Spool Racks',95657,0,132422026,NULL,NULL),(132422055,'Cafeteria Furniture','!Furniture!Cafeteria Furniture!Refreshment Centers',95658,0,132421859,NULL,NULL),(132422056,'Valves & Regulators','!MRO Supplies!Valves & Regulators!Regulators, Filters, & Lubricators',95659,0,132421780,NULL,NULL),(132422057,'Bonded Abrasives','!Abrasives!Bonded Abrasives!Resin Bonded Abrasives',95660,0,132421442,NULL,NULL),(132422058,'Packaging & Shipping Supplies','!Material Handling!Packaging & Shipping Supplies!Retail Labelers and Labels',95661,0,132422043,NULL,NULL),(132422059,'File Cabinets','!Furniture!File Cabinets!Roll Files',95662,0,132421976,NULL,NULL),(132422060,'Drains, Cleanouts & Accessories','!Plumbing!Drains, Cleanouts & Accessories!Roman Tub Rough-In Valves',95663,0,132421514,NULL,NULL),(132422061,'Air Cooling Equipment ','!HVAC!Air Cooling Equipment !Room Air Conditioners',95664,0,132421418,NULL,NULL),(132422062,'Chain, Cable, Rope & Accessories','!Material Handling!Chain, Cable, Rope & Accessories!Rope, Line & Twine',95665,0,132421459,NULL,NULL),(132422063,'Hammer Drilling & Demolition Tools','!Power Tools!Hammer Drilling & Demolition Tools!Rotary Hammers',95666,0,132421570,NULL,NULL),(132422064,'File Cabinets','!Furniture!File Cabinets!Rotating Files',95667,0,132422059,NULL,NULL),(132422065,'Routing & Trimming Tools','!Power Tools!Routing & Trimming Tools!Router & Laminate Trimmer Parts & Accessories',95668,0,132421704,NULL,NULL),(132422066,'Storage','!Material Handling!Storage!Safety Cans',95669,0,132422023,NULL,NULL),(132422067,'Eye Protection','!Safety & Security!Eye Protection!Safety Glass Parts & Accessories',95670,0,132421533,NULL,NULL),(132422068,'Eye Protection','!Safety & Security!Eye Protection!Safety Glasses',95671,0,132422067,NULL,NULL),(132422069,'Traffic Safety','!Safety & Security!Traffic Safety!Safety Guards & Protectors',95672,0,132421959,NULL,NULL),(132422070,'Traffic Safety','!Safety & Security!Traffic Safety!Safety Signs',95673,0,132422069,NULL,NULL),(132422071,'Safety Storage','!Safety & Security!Safety Storage!Safety Storage Cabinets',95674,0,132422014,NULL,NULL),(132422072,'Safety Storage','!Safety & Security!Safety Storage!Safety Storage Container Parts & Accessories',95675,0,132422071,NULL,NULL),(132422073,'Traffic Safety','!Safety & Security!Traffic Safety!Safety Tapes',95676,0,132422070,NULL,NULL),(132422074,'Outdoor Products','!Safety & Security!Outdoor Products!Salt & Liquid Ice Melt',95677,0,132422031,NULL,NULL),(132422075,'Jacks, Lifts & Hydraulics ','!Material Handling!Jacks, Lifts & Hydraulics !Scissor Lifts',95678,0,132421979,NULL,NULL),(132422076,'Putty Knives & Scrapers','!Hand Tools!Putty Knives & Scrapers!Scrapers & Putty Knives',95679,0,132421682,NULL,NULL),(132422077,'Screwdrivers & Nutdrivers','!Hand Tools!Screwdrivers & Nutdrivers!Screwdriver Sets',95680,0,132421712,NULL,NULL),(132422078,'Scribers, Pin Vises & Pick Sets','!Hand Tools!Scribers, Pin Vises & Pick Sets!Scribers',95681,0,132421713,NULL,NULL),(132422079,'Dock & Warehouse','!Safety & Security!Dock & Warehouse!Security & Safety Mirrors',95682,0,132421999,NULL,NULL),(132422080,'Carts & Trucks','!Material Handling!Carts & Trucks!Security Trucks',95683,0,132422041,NULL,NULL),(132422081,'Drain Cleaning & Inspection Equipment','!Plumbing!Drain Cleaning & Inspection Equipment!See Snakes, Transmitters, & Locators',95684,0,132421513,NULL,NULL),(132422082,'Carts & Trucks','!Material Handling!Carts & Trucks!Shelf Trucks',95685,0,132422080,NULL,NULL),(132422083,'Packaging & Shipping Supplies','!Material Handling!Packaging & Shipping Supplies!Shipping Envelopes',95686,0,132422058,NULL,NULL),(132422084,'Packaging & Shipping Supplies','!Material Handling!Packaging & Shipping Supplies!Shipping Scales',95687,0,132422083,NULL,NULL),(132422085,'Packaging & Shipping Supplies','!Material Handling!Packaging & Shipping Supplies!Shipping Tags & Labels',95688,0,132422084,NULL,NULL),(132422086,'Workbenches & Workshop Furniture','!Furniture!Workbenches & Workshop Furniture!Shop Desks',95689,0,132422044,NULL,NULL),(132422087,'Bathroom Fixtures, Parts and Accessories','!Plumbing!Bathroom Fixtures, Parts and Accessories!Showers',95690,0,132421433,NULL,NULL),(132422088,'Outdoor Products','!Safety & Security!Outdoor Products!Smoking Shelters',95691,0,132422074,NULL,NULL),(132422089,'Shears, Scissors & Snips','!Hand Tools!Shears, Scissors & Snips!Snips',95692,0,132421717,NULL,NULL),(132422090,'Sockets, Ratchets, Adaptors & Extensions','!Hand Tools!Sockets, Ratchets, Adaptors & Extensions!Socket Sets',95693,0,132421726,NULL,NULL),(132422091,'Replacement Parts','!Lighting!Replacement Parts!Sockets',95694,0,132421691,NULL,NULL),(132422092,'Heating Tools','!Power Tools!Heating Tools!Soldering Guns',95695,0,132421582,NULL,NULL),(132422093,'Soldering & Brazing','!Welding Supplies!Soldering & Brazing!Soldering/Brazing Kits',95696,0,132421727,NULL,NULL),(132422094,'Spill Control Safety','!Safety & Security!Spill Control Safety!Sorbent Kits',95697,0,132422013,NULL,NULL),(132422095,'Mats','!Janitorial Equipment!Mats!Specialty',95698,0,132421896,NULL,NULL),(132422096,'Lawn & Garden Tools','!Hand Tools!Lawn & Garden Tools!Sprayers',95699,0,132421612,NULL,NULL),(132422097,'Office Chairs & Stools','!Furniture!Office Chairs & Stools!Stack Chairs',95700,0,132422011,NULL,NULL),(132422098,'Packaging & Shipping Supplies','!Material Handling!Packaging & Shipping Supplies!Staplers',95701,0,132422085,NULL,NULL),(132422099,'Staplers & Riveters','!Hand Tools!Staplers & Riveters!Staples & Nails',95702,0,132421738,NULL,NULL),(132422100,'Non Woven Abrasives','!Abrasives!Non Woven Abrasives!Steel Wool',95703,0,132421642,NULL,NULL),(132422101,'Wheels & Casters','!Material Handling!Wheels & Casters!Stem Casters',95704,0,132422040,NULL,NULL),(132422102,'Storage','!Material Handling!Storage!Storage & IBC Tanks',95705,0,132422066,NULL,NULL),(132422103,'Outdoor Products','!Safety & Security!Outdoor Products!Storage Sheds',95706,0,132422088,NULL,NULL),(132422104,'Racks & Shelving','!Material Handling!Racks & Shelving!Storage Shelving',95707,0,132422054,NULL,NULL),(132422105,'Packaging & Shipping Supplies','!Material Handling!Packaging & Shipping Supplies!Strapping & Supplies',95708,0,132422098,NULL,NULL),(132422106,'Packaging & Shipping Supplies','!Material Handling!Packaging & Shipping Supplies!Stretch Wrap & Supplies',95709,0,132422105,NULL,NULL),(132422107,'Office Accessories & Furnishings','!Furniture!Office Accessories & Furnishings!Suggestion & Cash Boxes',95710,0,132422033,NULL,NULL),(132422108,'Tube Fitting','!Plumbing!Tube Fitting!Tailpieces & Tubes',95711,0,132421772,NULL,NULL),(132422109,'Packaging & Shipping Supplies','!Material Handling!Packaging & Shipping Supplies!Tape',95712,0,132422106,NULL,NULL),(132422110,'Packaging & Shipping Supplies','!Material Handling!Packaging & Shipping Supplies!Tape Dispensers',95713,0,132422109,NULL,NULL),(132422111,'Pipe Threading Equipment','!Hand Tools!Pipe Threading Equipment!Taps & Dies',95714,0,132421661,NULL,NULL),(132422112,'Covering Products','!MRO Supplies!Covering Products!Tarps',95715,0,132421493,NULL,NULL),(132422113,'Fire, Gas & Water Protection','!Safety & Security!Fire, Gas & Water Protection!Testers',95716,0,132421544,NULL,NULL),(132422114,'Drywall Tools','!Hand Tools!Drywall Tools!Texture Brushes',95717,0,132421519,NULL,NULL),(132422115,'Tig Welding','!Welding Supplies!Tig Welding!Tig Torch Parts & Accessories',95718,0,132421758,NULL,NULL),(132422116,'Carts & Trucks','!Material Handling!Carts & Trucks!Tilt Trucks',95719,0,132422082,NULL,NULL),(132422117,'Waste Receptacles','!Janitorial Equipment!Waste Receptacles!Tilt Trucks & Trash Can Dumpers',95720,0,132421784,NULL,NULL),(132422118,'Office Accessories & Furnishings','!Furniture!Office Accessories & Furnishings!Timecard Racks',95721,0,132422107,NULL,NULL),(132422119,'Gas Cutting & Welding','!Welding Supplies!Gas Cutting & Welding!Tips & Nozzles',95722,0,132421560,NULL,NULL),(132422120,'Racks & Shelving','!Material Handling!Racks & Shelving!Tire Racks',95723,0,132422104,NULL,NULL),(132422121,'Toilets & Urinals','!Plumbing!Toilets & Urinals!Toilet & Urinal Parts',95724,0,132421761,NULL,NULL),(132422122,'Hand Tool Organizers & Belts','!Hand Tools!Hand Tool Organizers & Belts!Tool Bags',95725,0,132421574,NULL,NULL),(132422123,'Tool Storage','!Hand Tools!Tool Storage!Tool Boxes',95726,0,132421766,NULL,NULL),(132422124,'Tool Storage','!Hand Tools!Tool Storage!Tool Chests',95727,0,132422123,NULL,NULL),(132422125,'Storage','!Material Handling!Storage!Totes & Containers',95728,0,132422102,NULL,NULL),(132422126,'Wiping Products','!Janitorial Equipment!Wiping Products!Towels & Wipers',95729,0,132421796,NULL,NULL),(132422127,'Trailer Hitch Accessories','!Material Handling!Trailer Hitch Accessories!Trailer Hitch Balls, Couplers & Locks',95730,0,132421770,NULL,NULL),(132422128,'Training Room Furniture','!Furniture!Training Room Furniture!Training Room Chairs',95731,0,NULL,NULL,NULL),(132422129,'Training Room Furniture','!Furniture!Training Room Furniture!Training Room Tables',95732,0,132422128,NULL,NULL),(132422130,'Waste Receptacles','!Janitorial Equipment!Waste Receptacles!Trash Can Accessories ',95733,0,132422117,NULL,NULL),(132422131,'Waste Receptacles','!Janitorial Equipment!Waste Receptacles!Trash Can Liners',95734,0,132422130,NULL,NULL),(132422132,'Hoists & Winches','!Material Handling!Hoists & Winches!Trolleys',95735,0,132421952,NULL,NULL),(132422133,'Plumbing Tools','!Hand Tools!Plumbing Tools!Tubing Benders',95736,0,132421670,NULL,NULL),(132422134,'Communicating Products','!MRO Supplies!Communicating Products!Two-Way Radio Accessories',95737,0,132421480,NULL,NULL),(132422135,'Underwater Cutting & Welding','!Welding Supplies!Underwater Cutting & Welding!Underwater Electrodes',95738,0,132421776,NULL,NULL),(132422136,'Carts & Trucks','!Material Handling!Carts & Trucks!Utility Carts',95739,0,132422116,NULL,NULL),(132422137,'Packaging & Shipping Supplies','!Material Handling!Packaging & Shipping Supplies!Utility Knives & Box Cutters',95740,0,132422110,NULL,NULL),(132422138,'Fastening Products','!Fasteners, Clamps & Straps!Fastening Products!Velcro Fasteners',95741,0,132421536,NULL,NULL),(132422139,'File Cabinets','!Furniture!File Cabinets!Vertical Files',95742,0,132422064,NULL,NULL),(132422140,'Dock & Warehouse','!Safety & Security!Dock & Warehouse!Warehouse Mezzanines',95743,0,132422079,NULL,NULL),(132422141,'Head/Face Protection','!Safety & Security!Head/Face Protection!Welding Helmet Parts & Accessories',95744,0,132421944,NULL,NULL),(132422142,'Filler Metals','!Welding Supplies!Filler Metals!Welding/Brazing Alloys & Fluxes',95745,0,132421540,NULL,NULL),(132422143,'Dock & Warehouse','!Safety & Security!Dock & Warehouse!Wheel & Tire Chocks',95746,0,132422140,NULL,NULL),(132422144,'Wire Brush Abrasives','!Abrasives!Wire Brush Abrasives!Wheel Brushes',95747,0,132421798,NULL,NULL),(132422145,'Wheels & Casters','!Material Handling!Wheels & Casters!Wheels',95748,0,132422101,NULL,NULL),(132422146,'Home D?cor','!Hospitality & Food Service Supplies!Home D?cor!Window Covering',95749,0,132421585,NULL,NULL),(132422147,'Workbenches & Workshop Furniture','!Furniture!Workbenches & Workshop Furniture!Workbench Accessories',95750,0,132422086,NULL,NULL),(132422148,'Workbenches & Workshop Furniture','!Furniture!Workbenches & Workshop Furniture!Workbench Components',95751,0,132422147,NULL,NULL),(132422149,'Workbenches & Workshop Furniture','!Furniture!Workbenches & Workshop Furniture!Workbenches',95752,0,132422148,NULL,NULL),(132422150,'Workbenches & Workshop Furniture','!Furniture!Workbenches & Workshop Furniture!Workshop Cabinets',95753,0,132422149,NULL,NULL),(132422151,'Workbenches & Workshop Furniture','!Furniture!Workbenches & Workshop Furniture!Workshop Chairs & Stools',95754,0,132422150,NULL,NULL),(132422152,'Workbenches & Workshop Furniture','!Furniture!Workbenches & Workshop Furniture!Workshop Computer Workstations',95755,0,132422151,NULL,NULL),(132422153,'Workbenches & Workshop Furniture','!Furniture!Workbenches & Workshop Furniture!Workshop Drawer Cabinets',95756,0,132422152,NULL,NULL),(132422154,'Workbenches & Workshop Furniture','!Furniture!Workbenches & Workshop Furniture!Workshop Tool Boards & Accessories',95757,0,132422153,NULL,NULL),(132422155,'Access Panels','!Plumbing!Rough Plumbing!Installation Supplies!Access Panels',95758,0,NULL,NULL,NULL),(132422156,'Acids & Corrosives Cabinets','!Safety & Security!Safety Storage!Safety Storage Cabinets!Acids & Corrosives Cabinets',95759,0,5877336,NULL,NULL),(132422157,'Air Curtain Doors','!Safety & Security!Dock & Warehouse!Dock & Warehouse Doors!Air Curtain Doors',95760,0,NULL,NULL,NULL),(132422158,'Angle Grinders Cordless','!Power Tools!Finishing Tools!Grinders!Angle Grinders Cordless',95761,0,132421568,NULL,NULL),(132422159,'Beam & Platform Scales','!Material Handling!Packaging & Shipping Supplies!Shipping Scales!Beam & Platform Scales',95762,0,NULL,NULL,NULL),(132422160,'Bin Cabinets','!Material Handling!Storage!Cabinets & Lockers!Bin Cabinets',95763,0,NULL,NULL,NULL),(132422161,'Blower Fans','!Safety & Security!Dock & Warehouse!Industrial Fans & Heaters!Blower Fans',95764,0,NULL,NULL,NULL),(132422162,'Box & Carton Stands','!Material Handling!Packaging & Shipping Supplies!Boxes & Cartons!Box & Carton Stands',95765,0,NULL,NULL,NULL),(132422163,'Brass Nipples','!Plumbing!Pipe Valves & Fittings!Brass Fitting & Flanges!Brass Nipples',95766,0,NULL,NULL,NULL),(132422164,'Bubble Wrap','!Material Handling!Packaging & Shipping Supplies!Cushioning & Protective Packaging!Bubble Wrap',95767,0,NULL,NULL,NULL),(132422165,'Bubble/Padded Envelopes','!Material Handling!Packaging & Shipping Supplies!Shipping Envelopes!Bubble/Padded Envelopes',95768,0,NULL,NULL,NULL),(132422166,'Chipping Hammers','!Hand Tools!Hammers, Sledges, Mallets & Axes!Hammers!Chipping Hammers',95769,0,5876137,NULL,NULL),(132422167,'Clear Adhesive Tape','!Material Handling!Packaging & Shipping Supplies!Tape!Clear Adhesive Tape',95770,0,31624,NULL,NULL),(132422168,'Column & Pipe Protectors','!Safety & Security!Traffic Safety!Safety Guards & Protectors!Column & Pipe Protectors',95771,0,NULL,NULL,NULL),(132422169,'Convertible Trucks','!Material Handling!Carts & Trucks!HandTrucks!Convertible Trucks',95772,0,NULL,NULL,NULL),(132422170,'Convex Mirrors','!Safety & Security!Dock & Warehouse!Security & Safety Mirrors!Convex Mirrors',95773,0,NULL,NULL,NULL),(132422171,'Drills Corded','!Power Tools!Drilling & Fastening Tools!Drills!Drills Corded',95774,0,5876979,NULL,NULL),(132422172,'Drum Accessories','!Material Handling!Storage!Drums, Drum Handling & Storage!Drum Accessories',95775,0,NULL,NULL,NULL),(132422173,'Festooning','!Material Handling!Hoists & Winches!Hoist & Puller Parts & Accessories!Festooning',95776,0,NULL,NULL,NULL),(132422174,'File Handles','!Hand Tools!Files!File Accessories!File Handles',95777,0,5876512,NULL,NULL),(132422175,'Hand Showers & Accessories','!Plumbing!Faucets!Bath Faucets!Hand Showers & Accessories',95778,0,NULL,NULL,NULL),(132422176,'Helmet Headgear','!Safety & Security!Head/Face Protection!Welding Helmet Parts & Accessories!Helmet Headgear',95779,0,NULL,NULL,NULL),(132422177,'Mig Contact Tips','!Welding Supplies!Mig Welding!Mig Gun Parts & Accessories!Mig Contact Tips',95780,0,NULL,NULL,NULL),(132422178,'Packing Tape Dispensers','!Material Handling!Packaging & Shipping Supplies!Tape Dispensers!Packing Tape Dispensers',95781,0,31625,NULL,NULL),(132422179,'Parallel Clamps','!Hand Tools!Clamps & Vises!Clamps & Spreaders!Parallel Clamps',95782,0,NULL,NULL,NULL),(132422180,'Plasma Rings','!Welding Supplies!Plasma Cutting!Plasma Parts!Plasma Rings',95783,0,5877212,NULL,NULL),(132422181,'Reception Room Beam Seating','!Furniture!Reception Room Furniture!Reception Room Beam Seating!Reception Room Beam Seating',95784,0,NULL,NULL,NULL),(132422182,'Reciprocating Saws Cordless','!Power Tools!Cutting Tools!Reciprocating Saws!Reciprocating Saws Cordless',95785,0,132421690,NULL,NULL),(132422183,'Rolling Ladders','!Safety & Security!Dock & Warehouse!Industrial Ladders!Rolling Ladders',95786,0,NULL,NULL,NULL),(132422184,'Rotary Hammers Cordless','!Power Tools!Hammer Drilling & Demolition Tools!Rotary Hammers!Rotary Hammers Cordless',95787,0,5876776,NULL,NULL),(132422185,'Screw Jacks','!Material Handling!Jacks, Lifts & Hydraulics!Jacks!Screw Jacks',95788,0,5876645,NULL,NULL),(132422186,'Shipping Labels','!Material Handling!Packaging & Shipping Supplies!Shipping Tags & Labels!Shipping Labels',95789,0,NULL,NULL,NULL),(132422187,'Soldering Guns Corded','!Power Tools!Heating Tools!Soldering Guns!Soldering Guns Corded',95790,0,5876799,NULL,NULL),(132422188,'Strapping','!Material Handling!Packaging & Shipping Supplies!Strapping & Supplies!Strapping',95791,0,NULL,NULL,NULL),(132422189,'Stretch Wrap','!Material Handling!Packaging & Shipping Supplies!Stretch Wrap & Supplies!Stretch Wrap',95792,0,NULL,NULL,NULL),(132422190,'Tamping Bars','!Hand Tools!Prying Tools!Bars!Tamping Bars',95793,0,5876284,NULL,NULL),(132422191,'Tig Tungsten Electrodes','!Welding Supplies!Tig Welding!Tig Torch Parts & Accessories!Tig Tungsten Electrodes',95794,0,NULL,NULL,NULL),(132422192,'Welding Gloves','!Safety & Security!Hand Protection!Gloves!Welding Gloves',95795,0,5877471,NULL,NULL),(132422193,'Zoning Sensors','!Safety & Security!Fire, Gas & Water Protection !Detectors!Zoning Sensors',95796,0,5877420,NULL,NULL),(132422194,'Bung Plugs','!Material Handling!Storage!Drums, Drum Handling & Storage!Drum Accessories!Bung Plugs',95797,0,132422172,NULL,NULL);
/*!40000 ALTER TABLE `hierarchynode` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `imagetype`
--

DROP TABLE IF EXISTS `imagetype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `imagetype` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `VERSION` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UC_IMAGETYPE_1` (`NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `imagetype`
--

LOCK TABLES `imagetype` WRITE;
/*!40000 ALTER TABLE `imagetype` DISABLE KEYS */;
/*!40000 ALTER TABLE `imagetype` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `inventoryitem`
--

DROP TABLE IF EXISTS `inventoryitem`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inventoryitem` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `BEGINNINGQUANTITY` bigint(20) DEFAULT NULL,
  `CUSTOMERSKU` varchar(255) DEFAULT NULL,
  `ENDINGQUANTITY` bigint(20) DEFAULT NULL,
  `VENDORNAME` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `inventoryitem`
--

LOCK TABLES `inventoryitem` WRITE;
/*!40000 ALTER TABLE `inventoryitem` DISABLE KEYS */;
/*!40000 ALTER TABLE `inventoryitem` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `inventoryreasons`
--

DROP TABLE IF EXISTS `inventoryreasons`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inventoryreasons` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `DESCRIPTION` varchar(255) NOT NULL,
  `INVREASONTYPE` varchar(255) NOT NULL,
  `INVREASONTYPEID` bigint(20) NOT NULL UNIQUE,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `inventoryreasons`
--

LOCK TABLES `inventoryreasons` WRITE;
/*!40000 ALTER TABLE `inventoryreasons` DISABLE KEYS */;
/*!40000 ALTER TABLE `inventoryreasons` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `inventorytransaction`
--

DROP TABLE IF EXISTS `inventorytransaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inventorytransaction` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `TRANSACTIONAMOUNT` decimal(19,2) NOT NULL,
  `TRANSACTIONDATE` datetime DEFAULT NULL,
  `TRANSACTIONTYPE` varchar(255) DEFAULT NULL,
  `INVENTORYITEM_ID` bigint(20) DEFAULT NULL,
  `INVENTORYTRANSACTIONTYPES_ID` bigint(20) DEFAULT NULL,
  `INVENTORYREASONTYPES_ID` bigint(20) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK2A3D64028BE735F7` (`INVENTORYITEM_ID`),
  KEY `FK2A3D64025C751D57` (`INVENTORYTRANSACTIONTYPES_ID`),
  KEY `FK2A3D6402738FDF57` (`INVENTORYREASONTYPES_ID`),
  CONSTRAINT `FK2A3D64025C751D57` FOREIGN KEY (`INVENTORYTRANSACTIONTYPES_ID`) REFERENCES `inventorytransactiontypes` (`ID`),
  CONSTRAINT `FK2A3D64028BE735F7` FOREIGN KEY (`INVENTORYITEM_ID`) REFERENCES `inventoryitem` (`ID`),
  CONSTRAINT `FK2A3D6402738FDF57` FOREIGN KEY (`INVENTORYREASONTYPES_ID`) REFERENCES `inventoryreasons` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `inventorytransaction`
--

LOCK TABLES `inventorytransaction` WRITE;
/*!40000 ALTER TABLE `inventorytransaction` DISABLE KEYS */;
/*!40000 ALTER TABLE `inventorytransaction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `inventorytransactiontypes`
--

DROP TABLE IF EXISTS `inventorytransactiontypes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inventorytransactiontypes` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `DESCRIPTION` varchar(255) NOT NULL,
  `TRANSACTIONTYPE` varchar(255) NOT NULL,
  `TRANSCATIONTYPEID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `SYS_C0023487` (`TRANSCATIONTYPEID`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `inventorytransactiontypes`
--

LOCK TABLES `inventorytransactiontypes` WRITE;
/*!40000 ALTER TABLE `inventorytransactiontypes` DISABLE KEYS */;
INSERT INTO `inventorytransactiontypes` VALUES (1,'PO Receipt','PO Receipt',1),(2,'Pick and Pack','Pick and Pack',2),(3,'Transfer','Transfer',3),(4,'Return to Vendor','Return to Vendor',4),(5,'Return from the Customer','Return from the Customer',5);
/*!40000 ALTER TABLE `inventorytransactiontypes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `invoice`
--

DROP TABLE IF EXISTS `invoice`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `invoice` (
  `FROM_CLASS` varchar(31) NOT NULL,
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `AMOUNT` decimal(19,2) DEFAULT NULL,
  `BILLINGTYPECODE` varchar(255) DEFAULT NULL,
  `CREATEDDATE` datetime DEFAULT NULL,
  `INVOICENUMBER` varchar(255) DEFAULT NULL,
  `VERSION` bigint(20) DEFAULT NULL,
  `CUSTOMERORDER_ID` bigint(20) DEFAULT NULL,
  `SHIPMENT_ID` bigint(20) DEFAULT NULL,
  `RETURNORDER_ID` bigint(20) DEFAULT NULL,
  `RANUMBER` varchar(255) DEFAULT NULL,
  `RESTOCKINGFEE` decimal(19,2) DEFAULT NULL,
  `STATUS` varchar(255) DEFAULT NULL,
  `VALID` decimal(1,0) DEFAULT NULL,
  `APPROVALDATE` datetime DEFAULT NULL,
  `DUEDATE` datetime DEFAULT NULL,
  `INVOICEDATE` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `INDX_19` (`CUSTOMERORDER_ID`) USING BTREE,
  KEY `INDX_20` (`FROM_CLASS`) USING BTREE,
  KEY `INDX_21` (`INVOICENUMBER`) USING BTREE,
  KEY `INVOICE_CREATEDATE_IDX` (`CREATEDDATE`) USING BTREE,
  KEY `INVOICE_RETURNORDER_IDX` (`RETURNORDER_ID`) USING BTREE,
  KEY `INVOICE_SHIPMENT_IDX` (`SHIPMENT_ID`) USING BTREE,
  CONSTRAINT `FK9FA1CF0D26CAE17` FOREIGN KEY (`CUSTOMERORDER_ID`) REFERENCES `customerorder` (`ID`),
  CONSTRAINT `FK9FA1CF0D96D53A97` FOREIGN KEY (`RETURNORDER_ID`) REFERENCES `returnorder` (`ID`),
  CONSTRAINT `FK9FA1CF0DBBACDEDD` FOREIGN KEY (`SHIPMENT_ID`) REFERENCES `shipment` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `invoice`
--

LOCK TABLES `invoice` WRITE;
/*!40000 ALTER TABLE `invoice` DISABLE KEYS */;
/*!40000 ALTER TABLE `invoice` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `invoice_lineitem`
--

DROP TABLE IF EXISTS `invoice_lineitem`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `invoice_lineitem` (
  `INVOICE_ID` bigint(20) DEFAULT NULL,
  `ACTUALITEMS_ID` bigint(20) DEFAULT NULL,
  UNIQUE KEY `UK_7FC5EA39BDA9AD88` (`ACTUALITEMS_ID`) USING BTREE,
  UNIQUE KEY `INVLINEITEM_UNIQ_COLS` (`ACTUALITEMS_ID`,`INVOICE_ID`) USING BTREE,
  KEY `INVOICELINEITEM_INVOICE_IDX` (`INVOICE_ID`) USING BTREE,
  CONSTRAINT `FK7FC5EA3943253E12` FOREIGN KEY (`ACTUALITEMS_ID`) REFERENCES `lineitem` (`ID`),
  CONSTRAINT `FK7FC5EA39753382D7` FOREIGN KEY (`INVOICE_ID`) REFERENCES `invoice` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `invoice_lineitem`
--

LOCK TABLES `invoice_lineitem` WRITE;
/*!40000 ALTER TABLE `invoice_lineitem` DISABLE KEYS */;
/*!40000 ALTER TABLE `invoice_lineitem` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `invoice_special`
--

DROP TABLE IF EXISTS `invoice_special`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `invoice_special` (
  `INVOICE_ID` bigint(20) DEFAULT NULL,
  `INSTRUCTION` varchar(255) DEFAULT NULL,
  KEY `FK8B80E567753382D7` (`INVOICE_ID`),
  CONSTRAINT `FK8B80E567753382D7` FOREIGN KEY (`INVOICE_ID`) REFERENCES `invoice` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `invoice_special`
--

LOCK TABLES `invoice_special` WRITE;
/*!40000 ALTER TABLE `invoice_special` DISABLE KEYS */;
/*!40000 ALTER TABLE `invoice_special` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `invoicesendmethod`
--

DROP TABLE IF EXISTS `invoicesendmethod`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `invoicesendmethod` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `VERSION` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UC_INVOICESENDMETHOD_1` (`NAME`)
) ENGINE=InnoDB AUTO_INCREMENT=31294 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `invoicesendmethod`
--

LOCK TABLES `invoicesendmethod` WRITE;
/*!40000 ALTER TABLE `invoicesendmethod` DISABLE KEYS */;
INSERT INTO `invoicesendmethod` VALUES (31288,'Email Payment Processor',0),(31289,'Email User',0),(31290,'Mail',0),(31291,'cXML',0),(31292,'EDI',0),(31293,'Summary Bill',0);
/*!40000 ALTER TABLE `invoicesendmethod` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ipaddress`
--

DROP TABLE IF EXISTS `ipaddress`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ipaddress` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `VALUE` varchar(255) DEFAULT NULL,
  `VERSION` bigint(20) DEFAULT NULL,
  `ACCOUNT_ID` bigint(20) DEFAULT NULL,
  `CRED_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `INDX_22` (`ACCOUNT_ID`) USING BTREE,
  KEY `IPADDRESS_CRED_ID` (`CRED_ID`) USING BTREE,
  CONSTRAINT `FKD8D77CAD185A22D7` FOREIGN KEY (`ACCOUNT_ID`) REFERENCES `account` (`ID`),
  CONSTRAINT `FKD8D77CAD45A534A6` FOREIGN KEY (`CRED_ID`) REFERENCES `credential` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ipaddress`
--

LOCK TABLES `ipaddress` WRITE;
/*!40000 ALTER TABLE `ipaddress` DISABLE KEYS */;
/*!40000 ALTER TABLE `ipaddress` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ipaddress_aud`
--

DROP TABLE IF EXISTS `ipaddress_aud`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ipaddress_aud` (
  `ID` bigint(20) NOT NULL DEFAULT '0',
  `REV` bigint(20) NOT NULL AUTO_INCREMENT,
  `REVTYPE` decimal(3,0) DEFAULT NULL,
  `VALUE` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`,`REV`),
  KEY `FK726B8BFEDF74E053` (`REV`),
  CONSTRAINT `FK726B8BFEDF74E053` FOREIGN KEY (`REV`) REFERENCES `revinfo` (`REV`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ipaddress_aud`
--

LOCK TABLES `ipaddress_aud` WRITE;
/*!40000 ALTER TABLE `ipaddress_aud` DISABLE KEYS */;
/*!40000 ALTER TABLE `ipaddress_aud` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `issue_log`
--

DROP TABLE IF EXISTS `issue_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `issue_log` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `EXPECTEDRESOLUTION` datetime DEFAULT NULL,
  `FOLLOWUPCOMPLETE` decimal(1,0) DEFAULT NULL,
  `ISSUECATEGORY` varchar(255) DEFAULT NULL,
  `ISSUEDESCRIPTION` varchar(1000) DEFAULT NULL,
  `OPENEDDATE` datetime NOT NULL,
  `RESOLUTION` varchar(255) DEFAULT NULL,
  `RESOLVEDDATE` datetime DEFAULT NULL,
  `SCHEDULEDFOLLOWUP` datetime DEFAULT NULL,
  `STATUS` varchar(255) DEFAULT NULL,
  `TICKETNUMBER` varchar(255) DEFAULT NULL,
  `UPDATETIMESTAMP` datetime DEFAULT NULL,
  `VERSION` bigint(20) DEFAULT NULL,
  `CUSTOMERORDER_ID` bigint(20) DEFAULT NULL,
  `DEPARTMENT` varchar(255) DEFAULT NULL,
  `REPORTER_ID` bigint(20) DEFAULT NULL,
  `CONTACTNAME` varchar(255) DEFAULT NULL,
  `CONTACTNUMBER` varchar(255) DEFAULT NULL,
  `CONTACTEMAIL` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FKB12F029E26CAE17` (`CUSTOMERORDER_ID`),
  KEY `FKB12F029E49053676` (`REPORTER_ID`),
  CONSTRAINT `FKB12F029E26CAE17` FOREIGN KEY (`CUSTOMERORDER_ID`) REFERENCES `customerorder` (`ID`),
  CONSTRAINT `FKB12F029E49053676` FOREIGN KEY (`REPORTER_ID`) REFERENCES `systemuser` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `issue_log`
--

LOCK TABLES `issue_log` WRITE;
/*!40000 ALTER TABLE `issue_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `issue_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `item`
--

DROP TABLE IF EXISTS `item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `item` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `COMMODITYCODE` varchar(255) DEFAULT NULL,
  `DESCRIPTION` varchar(4000) NOT NULL,
  `NAME` varchar(255) NOT NULL,
  `SEARCHTERMS` varchar(4000) DEFAULT NULL,
  `VERSION` bigint(20) DEFAULT NULL,
  `ABILITYONESUBSTITUTE_ID` bigint(20) DEFAULT NULL,
  `HIERARCHYNODE_ID` bigint(20) DEFAULT NULL,
  `ITEMCATEGORY_ID` bigint(20) DEFAULT NULL,
  `MANUFACTURER_ID` bigint(20) DEFAULT NULL,
  `REPLACEMENT_ID` bigint(20) DEFAULT NULL,
  `UNITOFMEASURE_ID` bigint(20) DEFAULT NULL,
  `VENDORCATALOG_ID` bigint(20) DEFAULT NULL,
  `STATUS` varchar(255) DEFAULT NULL,
  `EXTERNALCATALOGCHANGE` decimal(1,0) DEFAULT NULL,
  `EXTERNALCATALOGRELEVANT` decimal(1,0) DEFAULT NULL,
  `LASTMODIFIED` datetime DEFAULT NULL,
  `MINIMUM` bigint(20) DEFAULT NULL,
  `MULTIPLE` bigint(20) DEFAULT NULL,
  `ITEMWEIGHT` decimal(19,5) DEFAULT NULL,
  `LASTCOSTCHANGEDATE` datetime DEFAULT NULL,
  `SMARTSEARCHKEYWORDS` varchar(255) DEFAULT NULL,
  `ISFEATUREDITEM` decimal(1,0) DEFAULT NULL,
  `BRAND_ID` bigint(20) DEFAULT NULL,
  `FORUSEWITH` varchar(255) DEFAULT NULL,
  `SMARTSEARCHDEALERDESCRIPTION` varchar(255) DEFAULT NULL,
  `CARTRIDGENUMBER` varchar(255) DEFAULT NULL,
  `PROTECTEDSEARCHTERMS` varchar(4000) DEFAULT NULL,
  `PROTECTEDCOMMODITYCODE` varchar(255) DEFAULT NULL,
  `LOTSERIAL` varchar(255) DEFAULT NULL,
  `EXPIRY` datetime DEFAULT NULL,
  `DEALERSKU` varchar(255) DEFAULT NULL,
  `VENDORSKU` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `INDEX163` (`VENDORCATALOG_ID`) USING BTREE,
  KEY `INDX_24` (`HIERARCHYNODE_ID`) USING BTREE,
  KEY `ITEM_DEALERSKU_IX` (`DEALERSKU`) USING BTREE,
  KEY `ITEM_ITEM_IX` (`REPLACEMENT_ID`) USING BTREE,
  KEY `ITEM_VENDORSKU_IX` (`VENDORSKU`) USING BTREE,
  KEY `USPS_IDX_7` (`EXTERNALCATALOGCHANGE`) USING BTREE,
  KEY `FK22EF3379FB57D7` (`BRAND_ID`),
  KEY `FK22EF334B90121D` (`ITEMCATEGORY_ID`),
  KEY `FK22EF3397CE9D17` (`UNITOFMEASURE_ID`),
  KEY `FK22EF33D72635FD` (`MANUFACTURER_ID`),
  KEY `FK22EF33E061875A` (`ABILITYONESUBSTITUTE_ID`),
  CONSTRAINT `FK22EF334B90121D` FOREIGN KEY (`ITEMCATEGORY_ID`) REFERENCES `itemcategory` (`ID`),
  CONSTRAINT `FK22EF334E03425E` FOREIGN KEY (`REPLACEMENT_ID`) REFERENCES `item` (`ID`),
  CONSTRAINT `FK22EF3379FB57D7` FOREIGN KEY (`BRAND_ID`) REFERENCES `brand` (`ID`),
  CONSTRAINT `FK22EF337ADFF69F` FOREIGN KEY (`VENDORCATALOG_ID`) REFERENCES `catalog` (`ID`),
  CONSTRAINT `FK22EF3397CE9D17` FOREIGN KEY (`UNITOFMEASURE_ID`) REFERENCES `unitofmeasure` (`ID`),
  CONSTRAINT `FK22EF33C66C90F7` FOREIGN KEY (`HIERARCHYNODE_ID`) REFERENCES `hierarchynode` (`ID`),
  CONSTRAINT `FK22EF33D72635FD` FOREIGN KEY (`MANUFACTURER_ID`) REFERENCES `manufacturer` (`ID`),
  CONSTRAINT `FK22EF33E061875A` FOREIGN KEY (`ABILITYONESUBSTITUTE_ID`) REFERENCES `item` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `item`
--

LOCK TABLES `item` WRITE;
/*!40000 ALTER TABLE `item` DISABLE KEYS */;
/*!40000 ALTER TABLE `item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `item_item`
--

DROP TABLE IF EXISTS `item_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `item_item` (
  `ITEM_ID` bigint(20) DEFAULT NULL,
  `SIMILARITEMS_ID` bigint(20) DEFAULT NULL,
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `POSITION` bigint(20) DEFAULT NULL,
  `RELATIONSHIP` varchar(255) DEFAULT NULL,
  `VERSION` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FKF7BF40BFBAC8DCDB` (`SIMILARITEMS_ID`),
  KEY `FKF7BF40BFD4B669BD` (`ITEM_ID`),
  CONSTRAINT `FKF7BF40BFBAC8DCDB` FOREIGN KEY (`SIMILARITEMS_ID`) REFERENCES `item` (`ID`),
  CONSTRAINT `FKF7BF40BFD4B669BD` FOREIGN KEY (`ITEM_ID`) REFERENCES `item` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `item_item`
--

LOCK TABLES `item_item` WRITE;
/*!40000 ALTER TABLE `item_item` DISABLE KEYS */;
/*!40000 ALTER TABLE `item_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `item_matchbook`
--

DROP TABLE IF EXISTS `item_matchbook`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `item_matchbook` (
  `ITEM_ID` bigint(20) NOT NULL DEFAULT '0',
  `MATCHBOOK_ID` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ITEM_ID`,`MATCHBOOK_ID`),
  KEY `FK2D669002AB387BF7` (`MATCHBOOK_ID`),
  CONSTRAINT `FK2D669002AB387BF7` FOREIGN KEY (`MATCHBOOK_ID`) REFERENCES `matchbook` (`ID`),
  CONSTRAINT `FK2D669002D4B669BD` FOREIGN KEY (`ITEM_ID`) REFERENCES `item` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `item_matchbook`
--

LOCK TABLES `item_matchbook` WRITE;
/*!40000 ALTER TABLE `item_matchbook` DISABLE KEYS */;
/*!40000 ALTER TABLE `item_matchbook` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `item_pricecategories`
--

DROP TABLE IF EXISTS `item_pricecategories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `item_pricecategories` (
  `ITEM_ID` bigint(20) NOT NULL DEFAULT '0',
  `PRICECATEGORIES` varchar(255) DEFAULT NULL,
  `PRICECATEGORIES_KEY` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ITEM_ID`,`PRICECATEGORIES_KEY`),
  CONSTRAINT `FKDE40B39D4B669BD` FOREIGN KEY (`ITEM_ID`) REFERENCES `item` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `item_pricecategories`
--

LOCK TABLES `item_pricecategories` WRITE;
/*!40000 ALTER TABLE `item_pricecategories` DISABLE KEYS */;
/*!40000 ALTER TABLE `item_pricecategories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `item_smartsearchcustomerskus`
--

DROP TABLE IF EXISTS `item_smartsearchcustomerskus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `item_smartsearchcustomerskus` (
  `ITEM_ID` bigint(20) NOT NULL DEFAULT '0',
  `SMARTSEARCHCUSTOMERSKUS` varchar(255) DEFAULT NULL,
  `SMARTSEARCHCUSTOMERSKUS_KEY` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ITEM_ID`,`SMARTSEARCHCUSTOMERSKUS_KEY`),
  CONSTRAINT `FK6C6C5659D4B669BD` FOREIGN KEY (`ITEM_ID`) REFERENCES `item` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `item_smartsearchcustomerskus`
--

LOCK TABLES `item_smartsearchcustomerskus` WRITE;
/*!40000 ALTER TABLE `item_smartsearchcustomerskus` DISABLE KEYS */;
/*!40000 ALTER TABLE `item_smartsearchcustomerskus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `itemcategory`
--

DROP TABLE IF EXISTS `itemcategory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `itemcategory` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `VERSION` bigint(20) DEFAULT NULL,
  `PARENT_ID` bigint(20) DEFAULT NULL,
  `COMMODITYCODE` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ITEMCAT_NAME_UNIQUE` (`NAME`),
  KEY `FK32432D51D77B2BE4` (`PARENT_ID`),
  CONSTRAINT `FK32432D51D77B2BE4` FOREIGN KEY (`PARENT_ID`) REFERENCES `itemcategory` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=135783121 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `itemcategory`
--

LOCK TABLES `itemcategory` WRITE;
/*!40000 ALTER TABLE `itemcategory` DISABLE KEYS */;
INSERT INTO `itemcategory` VALUES (31224,'Dated Goods',0,NULL,NULL),(31225,'Binding, Filing, Labeling',1,NULL,NULL),(31226,'Ink and Toner',0,NULL,NULL),(31227,'GSA',1,31226,NULL),(31228,'Ferguson',1,31226,NULL),(31229,'Paper',0,NULL,NULL),(31230,'General Supplies',0,NULL,NULL),(31231,'Breakroom/Janitorial/Maintenance',2,NULL,NULL),(31232,'Writing Instruments',0,NULL,NULL),(31233,'Office/Desk Accessories',0,NULL,NULL),(31234,'Business Machines',0,NULL,NULL),(31235,'Computer Equipment',0,NULL,NULL),(31236,'Furniture',0,NULL,NULL),(1452717,'Computer Equipment-Service Plan',0,NULL,NULL),(1452718,'Envelopes/Mailers/Shipping Supplies',0,NULL,NULL),(1452719,'Forms/Recordkeeping/Reference Materials',0,NULL,NULL),(1452720,'Money Handling Products',0,NULL,NULL),(1452721,'NIB/NISH PRODUCTS',0,NULL,NULL),(1452722,'Notebooks, Writing Pads',0,NULL,NULL),(1452723,'Toner/OEM/Ink/Ribbon',0,NULL,NULL),(1452724,'Toner/OEM/Laser/Other',0,NULL,NULL),(1452725,'Toner/Reman/Ink/Ribbon',0,NULL,NULL),(1452726,'Toner/Reman/Laser/Other',0,NULL,NULL),(1452727,'ORS',0,NULL,NULL),(1452728,'Lagasse',0,NULL,NULL),(1452729,'Copy Paper',0,NULL,NULL),(1544069,'Crafts/Class/Recreation Room',0,NULL,NULL),(1544070,'Custom Imprint/Stamp',0,NULL,NULL),(17702758,'Fee',0,NULL,NULL),(104515823,'Custom/Special',0,NULL,NULL),(135168435,'Abrasives',0,NULL,NULL),(135168436,'Adhesives, Sealants and Tape',0,NULL,NULL),(135168524,'All Other',0,NULL,NULL),(135168525,'Chemical, Lubricants and Paints',0,NULL,NULL),(135168526,'Electrical and Lighting',0,NULL,NULL),(135168527,'Fastener Products',0,NULL,NULL),(135168528,'Foodservice Supplies',0,NULL,NULL),(135168529,'General MRO Supplies',0,NULL,NULL),(135168530,'HVAC',0,NULL,NULL),(135168531,'Janitorial Equipment',0,NULL,NULL),(135168532,'Marking Tools',0,NULL,NULL),(135168534,'Material Handling',0,NULL,NULL),(135168535,'Safety',0,NULL,NULL),(135168536,'Tools',0,NULL,NULL),(135168537,'Welding Supplies',0,NULL,NULL),(135783120,'New Item',0,NULL,NULL);
/*!40000 ALTER TABLE `itemcategory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `itemclassification`
--

DROP TABLE IF EXISTS `itemclassification`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `itemclassification` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `VERSION` bigint(20) DEFAULT NULL,
  `CLASSIFICATIONCODE_ID` bigint(20) DEFAULT NULL,
  `ITEMCLASSIFICATIONIMAGE_ID` bigint(20) DEFAULT NULL,
  `ITEMCLASSTYPE_ID` bigint(20) DEFAULT NULL,
  `VALUE` varchar(255) DEFAULT NULL,
  `ITEM_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IC_ITEM_ID` (`ITEM_ID`) USING BTREE,
  KEY `INDX_25` (`CLASSIFICATIONCODE_ID`) USING BTREE,
  KEY `INDX_26` (`ITEMCLASSTYPE_ID`) USING BTREE,
  KEY `FKB69BD9F9AC87CC77` (`ITEMCLASSIFICATIONIMAGE_ID`),
  CONSTRAINT `FKB69BD9F939A07C9D` FOREIGN KEY (`CLASSIFICATIONCODE_ID`) REFERENCES `classificationcode` (`ID`),
  CONSTRAINT `FKB69BD9F950060531` FOREIGN KEY (`ITEMCLASSTYPE_ID`) REFERENCES `itemclassificationtype` (`ID`),
  CONSTRAINT `FKB69BD9F9AC87CC77` FOREIGN KEY (`ITEMCLASSIFICATIONIMAGE_ID`) REFERENCES `itemclassificationimage` (`ID`),
  CONSTRAINT `FKB69BD9F9D4B669BD` FOREIGN KEY (`ITEM_ID`) REFERENCES `item` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `itemclassification`
--

LOCK TABLES `itemclassification` WRITE;
/*!40000 ALTER TABLE `itemclassification` DISABLE KEYS */;
/*!40000 ALTER TABLE `itemclassification` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `itemclassification_item`
--

DROP TABLE IF EXISTS `itemclassification_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `itemclassification_item` (
  `ITEMS_ID` bigint(20) NOT NULL DEFAULT '0',
  `ITEMCLASSIFICATIONS_ID` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ITEMS_ID`,`ITEMCLASSIFICATIONS_ID`),
  KEY `ITEMCLASSITEM_ITEM_IDX` (`ITEMS_ID`) USING BTREE,
  KEY `FK3ECCA9D95EC40F1C` (`ITEMCLASSIFICATIONS_ID`),
  CONSTRAINT `FK3ECCA9D95EC40F1C` FOREIGN KEY (`ITEMCLASSIFICATIONS_ID`) REFERENCES `itemclassification` (`ID`),
  CONSTRAINT `FK3ECCA9D99CD121F0` FOREIGN KEY (`ITEMS_ID`) REFERENCES `item` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `itemclassification_item`
--

LOCK TABLES `itemclassification_item` WRITE;
/*!40000 ALTER TABLE `itemclassification_item` DISABLE KEYS */;
/*!40000 ALTER TABLE `itemclassification_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `itemclassificationimage`
--

DROP TABLE IF EXISTS `itemclassificationimage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `itemclassificationimage` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `IMAGEURL` varchar(255) DEFAULT NULL,
  `VERSION` bigint(20) DEFAULT NULL,
  `IMAGETYPE_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK2B0232E2EF79F2B7` (`IMAGETYPE_ID`),
  CONSTRAINT `FK2B0232E2EF79F2B7` FOREIGN KEY (`IMAGETYPE_ID`) REFERENCES `imagetype` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `itemclassificationimage`
--

LOCK TABLES `itemclassificationimage` WRITE;
/*!40000 ALTER TABLE `itemclassificationimage` DISABLE KEYS */;
/*!40000 ALTER TABLE `itemclassificationimage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `itemclassificationtype`
--

DROP TABLE IF EXISTS `itemclassificationtype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `itemclassificationtype` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `ICONURL` varchar(255) DEFAULT NULL,
  `NAME` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `TOOLTIPLABEL` varchar(255) DEFAULT NULL,
  `VERSION` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UC_ITEMCLASSIFICATIONTYPE_1` (`NAME`)
) ENGINE=InnoDB AUTO_INCREMENT=142670227 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `itemclassificationtype`
--

LOCK TABLES `itemclassificationtype` WRITE;
/*!40000 ALTER TABLE `itemclassificationtype` DISABLE KEYS */;
INSERT INTO `itemclassificationtype` VALUES (31237,'https://d5vtsjcma1jiq.cloudfront.net/phoenix-icons/assembly.png','Assembly_Code','Assembly Required',3),(31238,'https://d5vtsjcma1jiq.cloudfront.net/phoenix-icons/recycle.png','Recycle_Indicator','Recyclable Item',1),(31239,'https://i.imgur.com/DIdcWpE.png','Green_Indicator','Green Item',3),(31240,NULL,'Green_Information',NULL,0),(31241,NULL,'EPACPGCompliant_Code',NULL,0),(31242,'https://i.imgur.com/Cn4AUdI.png','MSDS_Indicator','MSDS Sheet Available',3),(31243,NULL,'UNSPSC',NULL,0),(31244,'https://d5vtsjcma1jiq.cloudfront.net/phoenix-icons/noreturns.png','Non_Returnable_Code','Non-Returnable Item',1),(31245,NULL,'OverweightOversize_Indicator',NULL,0),(31246,NULL,'WBE_Indicator',NULL,0),(5875297,'https://d5vtsjcma1jiq.cloudfront.net/phoenix-icons/dropship.png','Dropship','Drop-Ship Only: 3-5 business days',3),(7587566,'https://i.imgur.com/O2cqazx.jpg','Hazmat','Hazardous Materials Included',2),(7587571,'https://i.imgur.com/C4J84pZ.gif','HUBSupplier','Historically Underutilized Business Identifier',1),(9790064,NULL,'MSDS URL',NULL,0),(17702745,NULL,'AbilityOne_Indicator',NULL,0),(107185481,'https://i.imgur.com/bkXHtbr.png','GSA','GSA Schedule 75/51v Item',2),(139854831,'https://d5vtsjcma1jiq.cloudfront.net/phoenix-icons/dropship.png','Dropship 5-7','Drop-Ship Only: 5-7 business days',0),(142670226,'https://d5vtsjcma1jiq.cloudfront.net/images/apdimages/clearance.jpg','Clearance','Clearance Item',0);
/*!40000 ALTER TABLE `itemclassificationtype` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `itemdimension`
--

DROP TABLE IF EXISTS `itemdimension`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `itemdimension` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `VALUE` double DEFAULT NULL,
  `VERSION` bigint(20) DEFAULT NULL,
  `DIMENSIONTYPE_ID` bigint(20) DEFAULT NULL,
  `ITEM_ID` bigint(20) DEFAULT NULL,
  `UNITOFMEASURE_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `ID_ITEM_IX` (`ITEM_ID`) USING BTREE,
  KEY `FK7789077388D89C57` (`DIMENSIONTYPE_ID`),
  KEY `FK7789077397CE9D17` (`UNITOFMEASURE_ID`),
  CONSTRAINT `FK7789077388D89C57` FOREIGN KEY (`DIMENSIONTYPE_ID`) REFERENCES `dimensiontype` (`ID`),
  CONSTRAINT `FK7789077397CE9D17` FOREIGN KEY (`UNITOFMEASURE_ID`) REFERENCES `unitofmeasure` (`ID`),
  CONSTRAINT `FK77890773D4B669BD` FOREIGN KEY (`ITEM_ID`) REFERENCES `item` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `itemdimension`
--

LOCK TABLES `itemdimension` WRITE;
/*!40000 ALTER TABLE `itemdimension` DISABLE KEYS */;
/*!40000 ALTER TABLE `itemdimension` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `itemhistory`
--

DROP TABLE IF EXISTS `itemhistory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `itemhistory` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `DESCRIPTION` varchar(255) DEFAULT NULL,
  `TIME` datetime DEFAULT NULL,
  `VERSION` bigint(20) DEFAULT NULL,
  `ITEM_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IH_ITEM_IX` (`ITEM_ID`) USING BTREE,
  CONSTRAINT `FK7AD69DE1D4B669BD` FOREIGN KEY (`ITEM_ID`) REFERENCES `item` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `itemhistory`
--

LOCK TABLES `itemhistory` WRITE;
/*!40000 ALTER TABLE `itemhistory` DISABLE KEYS */;
/*!40000 ALTER TABLE `itemhistory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `itemidentifier`
--

DROP TABLE IF EXISTS `itemidentifier`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `itemidentifier` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `VALUE` varchar(255) DEFAULT NULL,
  `VERSION` bigint(20) DEFAULT NULL,
  `ITEM_ID` bigint(20) DEFAULT NULL,
  `ITEMIDENTIFIERTYPE_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `II_ITEM_IX` (`ITEM_ID`) USING BTREE,
  KEY `FK6E543C9AF89DD` (`ITEMIDENTIFIERTYPE_ID`),
  CONSTRAINT `FK6E543C9AF89DD` FOREIGN KEY (`ITEMIDENTIFIERTYPE_ID`) REFERENCES `itemidentifiertype` (`ID`),
  CONSTRAINT `FK6E543CD4B669BD` FOREIGN KEY (`ITEM_ID`) REFERENCES `item` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `itemidentifier`
--

LOCK TABLES `itemidentifier` WRITE;
/*!40000 ALTER TABLE `itemidentifier` DISABLE KEYS */;
/*!40000 ALTER TABLE `itemidentifier` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `itemidentifiertype`
--

DROP TABLE IF EXISTS `itemidentifiertype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `itemidentifiertype` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `VERSION` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UC_ITEMIDENTIFIERTYPE_1` (`NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `itemidentifiertype`
--

LOCK TABLES `itemidentifiertype` WRITE;
/*!40000 ALTER TABLE `itemidentifiertype` DISABLE KEYS */;
/*!40000 ALTER TABLE `itemidentifiertype` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `itemimage`
--

DROP TABLE IF EXISTS `itemimage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `itemimage` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `IMAGENAME` varchar(255) DEFAULT NULL,
  `IMAGEURL` varchar(255) DEFAULT NULL,
  `PRIMARY` decimal(1,0) DEFAULT NULL,
  `VERSION` bigint(20) DEFAULT NULL,
  `IMAGETYPE_ID` bigint(20) DEFAULT NULL,
  `ITEM_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `ITEMIMAGEID_ID` (`ITEM_ID`) USING BTREE,
  KEY `FKF69951E8EF79F2B7` (`IMAGETYPE_ID`),
  CONSTRAINT `FKF69951E8D4B669BD` FOREIGN KEY (`ITEM_ID`) REFERENCES `item` (`ID`),
  CONSTRAINT `FKF69951E8EF79F2B7` FOREIGN KEY (`IMAGETYPE_ID`) REFERENCES `imagetype` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `itemimage`
--

LOCK TABLES `itemimage` WRITE;
/*!40000 ALTER TABLE `itemimage` DISABLE KEYS */;
/*!40000 ALTER TABLE `itemimage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `itempackaging`
--

DROP TABLE IF EXISTS `itempackaging`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `itempackaging` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `VERSION` bigint(20) DEFAULT NULL,
  `ITEM_ID` bigint(20) DEFAULT NULL,
  `PACKAGINGDIMENSION_ID` bigint(20) DEFAULT NULL,
  `PACKAGINGTYPE_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IP_ITEM_IX` (`ITEM_ID`) USING BTREE,
  KEY `FK2436F190293DD25D` (`PACKAGINGDIMENSION_ID`),
  KEY `FK2436F190B8531B37` (`PACKAGINGTYPE_ID`),
  CONSTRAINT `FK2436F190293DD25D` FOREIGN KEY (`PACKAGINGDIMENSION_ID`) REFERENCES `packagingdimension` (`ID`),
  CONSTRAINT `FK2436F190B8531B37` FOREIGN KEY (`PACKAGINGTYPE_ID`) REFERENCES `packagingtype` (`ID`),
  CONSTRAINT `FK2436F190D4B669BD` FOREIGN KEY (`ITEM_ID`) REFERENCES `item` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `itempackaging`
--

LOCK TABLES `itempackaging` WRITE;
/*!40000 ALTER TABLE `itempackaging` DISABLE KEYS */;
/*!40000 ALTER TABLE `itempackaging` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `itempropertytype`
--

DROP TABLE IF EXISTS `itempropertytype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `itempropertytype` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `ICONURL` varchar(255) DEFAULT NULL,
  `NAME` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `SUPPLEMENTARY` decimal(1,0) NOT NULL,
  `TOOLTIPLABEL` varchar(255) DEFAULT NULL,
  `VERSION` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UC_ITEMPROPERTYTYPE_1` (`NAME`)
) ENGINE=InnoDB AUTO_INCREMENT=153880640 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `itempropertytype`
--

LOCK TABLES `itempropertytype` WRITE;
/*!40000 ALTER TABLE `itempropertytype` DISABLE KEYS */;
INSERT INTO `itempropertytype` VALUES (31188,NULL,'multiples',0,NULL,0),(31189,NULL,'Last APD Cost',0,NULL,0),(31190,NULL,'discontinued',0,NULL,0),(31191,NULL,'changed sku number',0,NULL,0),(31192,NULL,'status',0,NULL,0),(31193,NULL,'comment',0,NULL,0),(31194,NULL,'standardprice',0,NULL,0),(31195,NULL,'list price',0,NULL,0),(31196,NULL,'street',0,NULL,0),(31197,NULL,'GSAprice',0,NULL,0),(31198,NULL,'GSAindicator',0,NULL,0),(31199,NULL,'AFprice',0,NULL,0),(31200,NULL,'APDcost',0,NULL,0),(31201,NULL,'unit of measure',0,NULL,0),(31202,NULL,'quantity per unit',0,NULL,0),(31204,'https://d5vtsjcma1jiq.cloudfront.net/phoenix-icons/upsable2.png','UPSable',0,'Small Parcel Delivery',5),(31205,'http://i.imgur.com/GVk2FAe.jpg','isJWOD',0,'Ability One Item',1),(31206,NULL,'TAA',0,NULL,0),(31207,NULL,'NON-TAA',0,'Non TAA Item',1),(31208,NULL,'EPP',0,NULL,0),(31209,NULL,'hazmat',0,'Hazmat',3),(31210,NULL,'specialOrder',0,NULL,0),(31211,NULL,'quantity',0,NULL,0),(31212,NULL,'discontinuedDate',0,NULL,0),(31213,NULL,'previousSku',0,NULL,0),(31214,NULL,'customOrder',0,NULL,0),(31215,NULL,'ets_list',0,NULL,0),(31216,NULL,'availability',0,NULL,4),(31217,NULL,'icons',0,NULL,0),(31218,NULL,'force substitute',0,NULL,0),(31219,NULL,'description',0,NULL,0),(31220,NULL,'taxable',0,NULL,0),(31221,NULL,'last calculated price',0,NULL,0),(31222,NULL,'last calculated profit',0,NULL,0),(31223,NULL,'MBE',0,NULL,0),(7665351,'http://content.apdmarketplace.com/phoenix-icons/restricted2.png','restricted',0,'Restricted Item',3),(13700352,NULL,'customerReplacementSku',0,NULL,0),(13700353,NULL,'customerReplacementVendor',0,NULL,0),(13700354,NULL,'substituteSku',0,NULL,0),(13700355,NULL,'substituteVendor',0,NULL,0),(17702750,NULL,'substituteReason',0,NULL,0),(17702762,NULL,'current profit',0,NULL,0),(17702763,NULL,'last price change date',0,NULL,0),(124788201,NULL,'minimum',0,NULL,0),(133398706,NULL,'vendor catalog hierarchy',0,NULL,1),(153880639,NULL,'Item Weight',0,NULL,0);
/*!40000 ALTER TABLE `itempropertytype` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `itempropertytype_aud`
--

DROP TABLE IF EXISTS `itempropertytype_aud`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `itempropertytype_aud` (
  `ID` bigint(20) NOT NULL DEFAULT '0',
  `REV` bigint(20) NOT NULL AUTO_INCREMENT,
  `REVTYPE` decimal(3,0) DEFAULT NULL,
  `ICONURL` varchar(255) DEFAULT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `SUPPLEMENTARY` decimal(1,0) DEFAULT NULL,
  `TOOLTIPLABEL` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`,`REV`),
  KEY `FKE6209A53DF74E053` (`REV`),
  CONSTRAINT `FKE6209A53DF74E053` FOREIGN KEY (`REV`) REFERENCES `revinfo` (`REV`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `itempropertytype_aud`
--

LOCK TABLES `itempropertytype_aud` WRITE;
/*!40000 ALTER TABLE `itempropertytype_aud` DISABLE KEYS */;
/*!40000 ALTER TABLE `itempropertytype_aud` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `itemsellingpoint`
--

DROP TABLE IF EXISTS `itemsellingpoint`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `itemsellingpoint` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `POSITION` bigint(20) DEFAULT NULL,
  `VALUE` varchar(4000) NOT NULL,
  `VERSION` bigint(20) DEFAULT NULL,
  `ITEM_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `ITEM_SELLPT_ID_IX` (`ITEM_ID`) USING BTREE,
  CONSTRAINT `FK97A3D6F3D4B669BD` FOREIGN KEY (`ITEM_ID`) REFERENCES `item` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `itemsellingpoint`
--

LOCK TABLES `itemsellingpoint` WRITE;
/*!40000 ALTER TABLE `itemsellingpoint` DISABLE KEYS */;
/*!40000 ALTER TABLE `itemsellingpoint` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `itemspecification`
--

DROP TABLE IF EXISTS `itemspecification`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `itemspecification` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `ITEMSPECIFICATIONID` bigint(20) DEFAULT NULL,
  `VERSION` bigint(20) DEFAULT NULL,
  `ITEM_ID` bigint(20) DEFAULT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `PRIORITY` bigint(20) DEFAULT NULL,
  `SEQUENCE` bigint(20) DEFAULT NULL,
  `VALUE` varchar(400) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `INDEX161` (`ITEM_ID`) USING BTREE,
  CONSTRAINT `FK53120C90D4B669BD` FOREIGN KEY (`ITEM_ID`) REFERENCES `item` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `itemspecification`
--

LOCK TABLES `itemspecification` WRITE;
/*!40000 ALTER TABLE `itemspecification` DISABLE KEYS */;
/*!40000 ALTER TABLE `itemspecification` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `itemxitempropertytype`
--

DROP TABLE IF EXISTS `itemxitempropertytype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `itemxitempropertytype` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `VALUE` varchar(4000) DEFAULT NULL,
  `VERSION` bigint(20) DEFAULT NULL,
  `TYPE_ID` bigint(20) DEFAULT NULL,
  `ITEM_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `ITEM_ID_IX` (`ITEM_ID`) USING BTREE,
  KEY `TYPE_ID_IX` (`TYPE_ID`) USING BTREE,
  CONSTRAINT `FKA8404B0778D89205` FOREIGN KEY (`TYPE_ID`) REFERENCES `itempropertytype` (`ID`),
  CONSTRAINT `FKA8404B07D4B669BD` FOREIGN KEY (`ITEM_ID`) REFERENCES `item` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `itemxitempropertytype`
--

LOCK TABLES `itemxitempropertytype` WRITE;
/*!40000 ALTER TABLE `itemxitempropertytype` DISABLE KEYS */;
/*!40000 ALTER TABLE `itemxitempropertytype` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jumptrackstopconfiguration`
--

DROP TABLE IF EXISTS `jumptrackstopconfiguration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jumptrackstopconfiguration` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `ALLOWSHIPFRIDAY` decimal(1,0) NOT NULL,
  `ALLOWSHIPMONDAY` decimal(1,0) NOT NULL,
  `ALLOWSHIPSATURDAY` decimal(1,0) NOT NULL,
  `ALLOWSHIPSUNDAY` decimal(1,0) NOT NULL,
  `ALLOWSHIPTHURSDAY` decimal(1,0) NOT NULL,
  `ALLOWSHIPTUESDAY` decimal(1,0) NOT NULL,
  `ALLOWSHIPWEDNESDAY` decimal(1,0) NOT NULL,
  `VERSION` bigint(20) DEFAULT NULL,
  `ZIP` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `SYS_C0022506` (`ZIP`)
) ENGINE=InnoDB AUTO_INCREMENT=102582461 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jumptrackstopconfiguration`
--

LOCK TABLES `jumptrackstopconfiguration` WRITE;
/*!40000 ALTER TABLE `jumptrackstopconfiguration` DISABLE KEYS */;
INSERT INTO `jumptrackstopconfiguration` VALUES (102582409,1,1,0,0,1,1,1,0,'28001'),(102582410,1,1,0,0,1,1,1,0,'29621'),(102582411,1,1,0,0,1,1,1,0,'29902'),(102582412,1,1,0,0,1,1,1,0,'28422'),(102582413,1,1,0,0,1,1,1,0,'28713'),(102582414,1,1,0,0,1,1,1,0,'28467'),(102582415,1,1,0,0,1,1,1,0,'28204'),(102582416,1,1,0,0,1,1,1,0,'28212'),(102582417,1,1,0,0,1,1,1,0,'28721'),(102582418,1,1,0,0,1,1,1,0,'28722'),(102582419,1,1,0,0,1,1,1,0,'28612'),(102582420,1,1,0,0,1,1,1,0,'27703'),(102582421,1,1,0,0,1,1,1,0,'27704'),(102582422,1,1,0,0,1,1,1,0,'27713'),(102582423,1,1,0,0,1,1,1,0,'28621'),(102582424,1,1,0,0,1,1,1,0,'28734'),(102582425,1,1,0,0,1,1,1,0,'27525'),(102582426,1,1,0,0,1,1,1,0,'29340'),(102582427,1,1,0,0,1,1,1,0,'28601'),(102582428,1,1,0,0,1,1,1,0,'28637'),(102582429,1,1,0,0,1,1,1,0,'28640'),(102582430,1,1,0,0,1,1,1,0,'28352'),(102582431,1,1,0,0,1,1,1,0,'28645'),(102582432,1,1,0,0,1,1,1,0,'27549'),(102582433,1,1,0,0,1,1,1,0,'28752'),(102582434,1,1,0,0,1,1,1,0,'28107'),(102582435,1,1,0,0,1,1,1,0,'28227'),(102582436,1,1,0,0,1,1,1,0,'28655'),(102582437,1,1,0,0,1,1,1,0,'27306'),(102582438,1,1,0,0,1,1,1,0,'29576'),(102582439,1,1,0,0,1,1,1,0,'28906'),(102582440,1,1,0,0,1,1,1,0,'28761'),(102582441,1,1,0,0,1,1,1,0,'28128'),(102582442,1,1,0,0,1,1,1,0,'29585'),(102582443,1,1,0,0,1,1,1,0,'28137'),(102582445,1,1,0,0,1,1,1,0,'28671'),(102582446,1,1,0,0,1,1,1,0,'28470'),(102582447,1,1,0,0,1,1,1,0,'28150'),(102582448,1,1,0,0,1,1,1,0,'28152'),(102582449,1,1,0,0,1,1,1,0,'28461'),(102582450,1,1,0,0,1,1,1,0,'28779'),(102582451,1,1,0,0,1,1,1,0,'27371'),(102582452,1,1,0,0,1,1,1,0,'29944'),(102582453,1,1,0,0,1,1,1,0,'28170'),(102582454,1,1,0,0,1,1,1,0,'28190'),(102582455,1,1,0,0,1,1,1,0,'28396'),(102582456,1,1,0,0,1,1,1,0,'27587'),(102582457,1,1,0,0,1,1,1,0,'29488'),(102582458,1,1,0,0,1,1,1,0,'28173'),(102582459,1,1,0,0,1,1,1,0,'28427'),(102582460,1,1,0,0,1,1,1,0,'28472');
/*!40000 ALTER TABLE `jumptrackstopconfiguration` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `limitapproval`
--

DROP TABLE IF EXISTS `limitapproval`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `limitapproval` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `LIMIT` decimal(19,2) DEFAULT NULL,
  `TYPE` bigint(20) DEFAULT NULL,
  `VALUE` varchar(255) DEFAULT NULL,
  `AXCXU_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK7C45AFE90DF4B4F` (`AXCXU_ID`),
  CONSTRAINT `FK7C45AFE90DF4B4F` FOREIGN KEY (`AXCXU_ID`) REFERENCES `accountxcredentialxuser` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `limitapproval`
--

LOCK TABLES `limitapproval` WRITE;
/*!40000 ALTER TABLE `limitapproval` DISABLE KEYS */;
/*!40000 ALTER TABLE `limitapproval` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `limitapproval_aud`
--

DROP TABLE IF EXISTS `limitapproval_aud`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `limitapproval_aud` (
  `ID` bigint(20) NOT NULL DEFAULT '0',
  `REV` bigint(20) NOT NULL AUTO_INCREMENT,
  `REVTYPE` decimal(3,0) DEFAULT NULL,
  `LIMIT` decimal(19,2) DEFAULT NULL,
  `TYPE` bigint(20) DEFAULT NULL,
  `VALUE` varchar(255) DEFAULT NULL,
  `AXCXU_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`,`REV`),
  KEY `FK882ED9CFDF74E053` (`REV`),
  CONSTRAINT `FK882ED9CFDF74E053` FOREIGN KEY (`REV`) REFERENCES `revinfo` (`REV`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `limitapproval_aud`
--

LOCK TABLES `limitapproval_aud` WRITE;
/*!40000 ALTER TABLE `limitapproval_aud` DISABLE KEYS */;
/*!40000 ALTER TABLE `limitapproval_aud` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lineitem`
--

DROP TABLE IF EXISTS `lineitem`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lineitem` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `APDSKU` varchar(255) DEFAULT NULL,
  `BACKORDEREDQUANTITY` decimal(19,2) DEFAULT NULL,
  `CORE` decimal(1,0) DEFAULT NULL,
  `COST` decimal(19,2) DEFAULT NULL,
  `DESCRIPTION` varchar(4000) NOT NULL,
  `ESTIMATEDSHIPPINGAMOUNT` decimal(19,2) NOT NULL,
  `MAXIMUMTAXTOCHARGE` decimal(19,2) DEFAULT NULL,
  `ITEMTYPE` varchar(255) DEFAULT NULL,
  `LEADTIME` bigint(20) DEFAULT NULL,
  `LINENUMBER` bigint(20) DEFAULT NULL,
  `MANUFACTURERNAME` varchar(255) DEFAULT NULL,
  `MANUFACTURERPARTID` varchar(255) DEFAULT NULL,
  `ORIGINALLYADDEDNAME` varchar(255) DEFAULT NULL,
  `ORIGINALLYADDEDSKU` varchar(255) DEFAULT NULL,
  `PARENTLINENUMBER` bigint(20) DEFAULT NULL,
  `PURCHASECOMMENT` varchar(255) DEFAULT NULL,
  `QUANTITY` bigint(20) DEFAULT NULL,
  `SHORTNAME` varchar(500) DEFAULT NULL,
  `SUPPLIERPARTAUXILIARYID` varchar(255) DEFAULT NULL,
  `SUPPLIERPARTID` varchar(255) DEFAULT NULL,
  `TAXABLE` decimal(1,0) DEFAULT NULL,
  `UNITPRICE` decimal(19,2) NOT NULL,
  `UNSPSCCLASSIFICATION` varchar(255) DEFAULT NULL,
  `URL` varchar(255) DEFAULT NULL,
  `VERSION` bigint(20) DEFAULT NULL,
  `CATEGORY_ID` bigint(20) DEFAULT NULL,
  `CUSTOMERSKU_ID` bigint(20) DEFAULT NULL,
  `EXPECTED_ID` bigint(20) DEFAULT NULL,
  `ITEM_ID` bigint(20) DEFAULT NULL,
  `ORDER_ID` bigint(20) DEFAULT NULL,
  `STATUS_ID` bigint(20) DEFAULT NULL,
  `UNITOFMEASURE_ID` bigint(20) DEFAULT NULL,
  `VENDOR_ID` bigint(20) DEFAULT NULL,
  `SHIPTOEXTRASARTIFACT` varchar(4000) DEFAULT NULL,
  `CUSTOMERLINENUMBER` varchar(255) DEFAULT NULL,
  `ORIGINALQUANTITY` bigint(20) DEFAULT NULL,
  `QUANTITYINVOICEDBYVENDOR` varchar(255) DEFAULT NULL,
  `READYTOSHIPQTY` integer DEFAULT NULL,
  `EXTRANEOUSCUSTOMERSKU` varchar(255) DEFAULT NULL,
  `GLOBALTRADEITEMNUMBER` varchar(255) DEFAULT NULL,
  `UNIVERSALPRODUCTCODE` varchar(255) DEFAULT NULL,
  `CUSTOMEREXPECTEDUNITPRICE` decimal(19,2) DEFAULT NULL,
  `HASBEENREJECTED` decimal(1,0) DEFAULT NULL,
  `RETURNSHIPPING` decimal(1,0) DEFAULT NULL,
  `PAYMENTSTATUS_ID` bigint(20) DEFAULT NULL,
  `CUSTOMEREXPECTEDUOM_ID` bigint(20) DEFAULT NULL,
  `GLOBALTRADEIDENTIFICATONNUMBER` varchar(255) DEFAULT NULL,
  `DELIVERYMETHOD` varchar(255) DEFAULT NULL,
  `WEIGHT` decimal(19,5) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `INDX_28` (`CORE`) USING BTREE,
  KEY `INDX_29` (`EXPECTED_ID`) USING BTREE,
  KEY `INDX_30` (`CATEGORY_ID`) USING BTREE,
  KEY `INDX_31` (`UNITOFMEASURE_ID`) USING BTREE,
  KEY `INDX_32` (`STATUS_ID`) USING BTREE,
  KEY `INDX_33` (`CUSTOMERSKU_ID`) USING BTREE,
  KEY `INDX_34` (`VENDOR_ID`) USING BTREE,
  KEY `LINEITEM_CUSTORDER_IDX` (`ORDER_ID`) USING BTREE,
  KEY `LINEITEM_LINENUMBER_IDX` (`LINENUMBER`) USING BTREE,
  KEY `LI_ITEM_IX` (`ITEM_ID`) USING BTREE,
  KEY `FK4AAEE9471BAE365E` (`PAYMENTSTATUS_ID`),
  KEY `FK4AAEE947D0A084BD` (`CUSTOMEREXPECTEDUOM_ID`),
  CONSTRAINT `FK4AAEE9471BAE365E` FOREIGN KEY (`PAYMENTSTATUS_ID`) REFERENCES `lineitemstatus` (`ID`),
  CONSTRAINT `FK4AAEE9475B7981A4` FOREIGN KEY (`STATUS_ID`) REFERENCES `lineitemstatus` (`ID`),
  CONSTRAINT `FK4AAEE9478E1EC39` FOREIGN KEY (`ORDER_ID`) REFERENCES `customerorder` (`ID`),
  CONSTRAINT `FK4AAEE94797CE9D17` FOREIGN KEY (`UNITOFMEASURE_ID`) REFERENCES `unitofmeasure` (`ID`),
  CONSTRAINT `FK4AAEE947B7BD2710` FOREIGN KEY (`CATEGORY_ID`) REFERENCES `itemcategory` (`ID`),
  CONSTRAINT `FK4AAEE947D0A084BD` FOREIGN KEY (`CUSTOMEREXPECTEDUOM_ID`) REFERENCES `unitofmeasure` (`ID`),
  CONSTRAINT `FK4AAEE947D4B669BD` FOREIGN KEY (`ITEM_ID`) REFERENCES `item` (`ID`),
  CONSTRAINT `FK4AAEE947D50C94DD` FOREIGN KEY (`VENDOR_ID`) REFERENCES `vendor` (`ID`),
  CONSTRAINT `FK4AAEE947E0A3A08C` FOREIGN KEY (`EXPECTED_ID`) REFERENCES `lineitem` (`ID`),
  CONSTRAINT `FK4AAEE947EA944B5` FOREIGN KEY (`CUSTOMERSKU_ID`) REFERENCES `sku` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lineitem`
--

LOCK TABLES `lineitem` WRITE;
/*!40000 ALTER TABLE `lineitem` DISABLE KEYS */;
/*!40000 ALTER TABLE `lineitem` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lineitem_vendorcomments`
--

DROP TABLE IF EXISTS `lineitem_vendorcomments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lineitem_vendorcomments` (
  `LINEITEM_ID` bigint(20) DEFAULT NULL,
  `VENDORCOMMENT` varchar(255) DEFAULT NULL,
  KEY `VENDCOMM_LINEITEM` (`LINEITEM_ID`) USING BTREE,
  CONSTRAINT `FKB4713574B7777FDD` FOREIGN KEY (`LINEITEM_ID`) REFERENCES `lineitem` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lineitem_vendorcomments`
--

LOCK TABLES `lineitem_vendorcomments` WRITE;
/*!40000 ALTER TABLE `lineitem_vendorcomments` DISABLE KEYS */;
/*!40000 ALTER TABLE `lineitem_vendorcomments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lineitemhistory`
--

DROP TABLE IF EXISTS `lineitemhistory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lineitemhistory` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `DESCRIPTION` varchar(255) DEFAULT NULL,
  `UPDATETIMESTAMP` datetime DEFAULT NULL,
  `VERSION` bigint(20) DEFAULT NULL,
  `ACTIONTYPE_ID` bigint(20) DEFAULT NULL,
  `LINEITEM_ID` bigint(20) DEFAULT NULL,
  `LINEITEMSTATUS_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK5154E54D5C204E7D` (`ACTIONTYPE_ID`),
  KEY `FK5154E54DB7777FDD` (`LINEITEM_ID`),
  KEY `FK5154E54DB27BA87D` (`LINEITEMSTATUS_ID`),
  CONSTRAINT `FK5154E54D5C204E7D` FOREIGN KEY (`ACTIONTYPE_ID`) REFERENCES `actiontype` (`ID`),
  CONSTRAINT `FK5154E54DB27BA87D` FOREIGN KEY (`LINEITEMSTATUS_ID`) REFERENCES `lineitemstatus` (`ID`),
  CONSTRAINT `FK5154E54DB7777FDD` FOREIGN KEY (`LINEITEM_ID`) REFERENCES `lineitem` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lineitemhistory`
--

LOCK TABLES `lineitemhistory` WRITE;
/*!40000 ALTER TABLE `lineitemhistory` DISABLE KEYS */;
/*!40000 ALTER TABLE `lineitemhistory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lineitemstatus`
--

DROP TABLE IF EXISTS `lineitemstatus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lineitemstatus` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `DESCRIPTION` varchar(255) DEFAULT NULL,
  `VALUE` varchar(255) DEFAULT NULL,
  `VERSION` bigint(20) DEFAULT NULL,
  `PAYMENTSTATUS` decimal(1,0) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `LINEITEMSTATUS_VALUE_IDX` (`VALUE`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=194862987 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lineitemstatus`
--

LOCK TABLES `lineitemstatus` WRITE;
/*!40000 ALTER TABLE `lineitemstatus` DISABLE KEYS */;
INSERT INTO `lineitemstatus` VALUES (3965299,'The line item was created','CREATED',0,0),(3965300,'The line item was ordered','ORDERED',0,0),(3965301,'The line item was accepted','ACCEPTED',0,0),(3965302,'The line item was rejected','REJECTED',0,0),(3965303,'The line item is back ordered','BACKORDERED',0,0),(3965304,'The line item was canceled','CANCELED',0,0),(3965305,'The line item is partially shipped','PARTIAL_SHIP',0,0),(3965306,'The line item is fully shipped','FULLY_SHIPPED',0,0),(3965307,'The line item is partially billed','PARTIAL_BILL',0,1),(3965308,'The line item was fully billed','FULLY_BILLED',0,1),(3965309,'The line item was invoiced to APD','INVOICE_RECEIVED',0,0),(4494226,'The line item was partially backordered','PARTIAL_BACKORDERED',0,0),(4494227,'The line item quantity was changed','ITEM_QUANTITY_CHANGED',0,0),(17702747,'The line item was returned','RETURNED',0,0),(17702748,'The line item was partially canceled','PARTIAL_CANCEL',0,0),(17702749,'The line item was partially returned','PARTIAL_RETURN',0,0),(17709302,'The line item was resent','RESENT',0,0),(17709303,'The line item was exchanged','EXCHANGED',0,0),(111109415,'The line item has been partially charged','PARTIAL_CHARGE',0,1),(111109416,'The line item was fully charged','FULLY_CHARGED',0,1),(179035882,'The line item was marked as resolved','MANUALLY_RESOLVED',0,0),(194862986,'The line item was denied','DENIED',0,0);
/*!40000 ALTER TABLE `lineitemstatus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lineitemxreturn`
--

DROP TABLE IF EXISTS `lineitemxreturn`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lineitemxreturn` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `QUANTITY` decimal(19,2) NOT NULL,
  `VERSION` bigint(20) DEFAULT NULL,
  `LINEITEM_ID` bigint(20) DEFAULT NULL,
  `INVOICE_ID` bigint(20) DEFAULT NULL,
  `REASON` varchar(255) DEFAULT NULL,
  `RETURNORDER_ID` bigint(20) DEFAULT NULL,
  `RESTOCKINGFEE` decimal(19,2) DEFAULT NULL,
  `RETURNSHIPPING` decimal(1,0) DEFAULT NULL,
  `RETURNSHIPPINGAMOUNT` decimal(19,2) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `INDX_35` (`INVOICE_ID`) USING BTREE,
  KEY `LIXR_LINEITEM_IDX` (`LINEITEM_ID`) USING BTREE,
  KEY `RETURNORDER_LIXR_IDX` (`RETURNORDER_ID`) USING BTREE,
  CONSTRAINT `FK77B3F0815A2BC1B6` FOREIGN KEY (`INVOICE_ID`) REFERENCES `invoice` (`ID`),
  CONSTRAINT `FK77B3F08196D53A97` FOREIGN KEY (`RETURNORDER_ID`) REFERENCES `returnorder` (`ID`),
  CONSTRAINT `FK77B3F081B7777FDD` FOREIGN KEY (`LINEITEM_ID`) REFERENCES `lineitem` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lineitemxreturn`
--

LOCK TABLES `lineitemxreturn` WRITE;
/*!40000 ALTER TABLE `lineitemxreturn` DISABLE KEYS */;
/*!40000 ALTER TABLE `lineitemxreturn` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lineitemxshipment`
--

DROP TABLE IF EXISTS `lineitemxshipment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lineitemxshipment` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `PAID` decimal(1,0) DEFAULT NULL,
  `QUANTITY` decimal(19,2) NOT NULL,
  `SHIPPING` decimal(19,2) DEFAULT NULL,
  `VERSION` bigint(20) DEFAULT NULL,
  `SHIPMENTS_ID` bigint(20) DEFAULT NULL,
  `LINEITEM_ID` bigint(20) DEFAULT NULL,
  `CCTRANSACTIONID` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `LIXS_LINEITEM_IDX` (`LINEITEM_ID`) USING BTREE,
  KEY `LIXS_SHIPMENT_IDX` (`SHIPMENTS_ID`) USING BTREE,
  CONSTRAINT `FK4DFF94ABB7777FDD` FOREIGN KEY (`LINEITEM_ID`) REFERENCES `lineitem` (`ID`),
  CONSTRAINT `FK4DFF94ABFDA949FE` FOREIGN KEY (`SHIPMENTS_ID`) REFERENCES `shipment` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lineitemxshipment`
--

LOCK TABLES `lineitemxshipment` WRITE;
/*!40000 ALTER TABLE `lineitemxshipment` DISABLE KEYS */;
/*!40000 ALTER TABLE `lineitemxshipment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lineitemxshipment_taxtype`
--

DROP TABLE IF EXISTS `lineitemxshipment_taxtype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lineitemxshipment_taxtype` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `VALUE` decimal(19,2) NOT NULL,
  `VERSION` bigint(20) DEFAULT NULL,
  `LINEITEMXSHIPMENT_ID` bigint(20) DEFAULT NULL,
  `TYPE_ID` bigint(20) DEFAULT NULL,
  `LINEITEMXRETURN_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `LIXSXTT_LINEITEMXRETURN_IDX` (`LINEITEMXRETURN_ID`) USING BTREE,
  KEY `LIXSXTT_LINEITEMXSHIPMENT_IDX` (`LINEITEMXSHIPMENT_ID`) USING BTREE,
  KEY `FKD45EDD12B5202` (`TYPE_ID`),
  CONSTRAINT `FKD45EDD12B5202` FOREIGN KEY (`TYPE_ID`) REFERENCES `taxtype` (`ID`),
  CONSTRAINT `FKD45EDD1A80FC937` FOREIGN KEY (`LINEITEMXRETURN_ID`) REFERENCES `lineitemxreturn` (`ID`),
  CONSTRAINT `FKD45EDD1CB300DB7` FOREIGN KEY (`LINEITEMXSHIPMENT_ID`) REFERENCES `lineitemxshipment` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lineitemxshipment_taxtype`
--

LOCK TABLES `lineitemxshipment_taxtype` WRITE;
/*!40000 ALTER TABLE `lineitemxshipment_taxtype` DISABLE KEYS */;
/*!40000 ALTER TABLE `lineitemxshipment_taxtype` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `listhashtouid`
--

DROP TABLE IF EXISTS `listhashtouid`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `listhashtouid` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `CREATIONDATE` datetime DEFAULT NULL,
  `HASH` bigint(20) DEFAULT NULL,
  `VERSION` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `LISTHASHTOUID_HASH` (`HASH`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `listhashtouid`
--

LOCK TABLES `listhashtouid` WRITE;
/*!40000 ALTER TABLE `listhashtouid` DISABLE KEYS */;
/*!40000 ALTER TABLE `listhashtouid` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `listhashtouid_aud`
--

DROP TABLE IF EXISTS `listhashtouid_aud`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `listhashtouid_aud` (
  `ID` bigint(20) NOT NULL DEFAULT '0',
  `REV` bigint(20) NOT NULL AUTO_INCREMENT,
  `REVTYPE` decimal(3,0) DEFAULT NULL,
  `CREATIONDATE` datetime DEFAULT NULL,
  `HASH` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`,`REV`),
  KEY `FK4770819A75D1432` (`REV`),
  CONSTRAINT `FK4770819A75D1432` FOREIGN KEY (`REV`) REFERENCES `revinfo` (`REV`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `listhashtouid_aud`
--

LOCK TABLES `listhashtouid_aud` WRITE;
/*!40000 ALTER TABLE `listhashtouid_aud` DISABLE KEYS */;
/*!40000 ALTER TABLE `listhashtouid_aud` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lixshipment_vendorcomments`
--

DROP TABLE IF EXISTS `lixshipment_vendorcomments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lixshipment_vendorcomments` (
  `LIXSHIPMENT_ID` bigint(20) DEFAULT NULL,
  `VENDORCOMMENT` varchar(255) DEFAULT NULL,
  KEY `FKAC720FA6A518092D` (`LIXSHIPMENT_ID`),
  CONSTRAINT `FKAC720FA6A518092D` FOREIGN KEY (`LIXSHIPMENT_ID`) REFERENCES `lineitemxshipment` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lixshipment_vendorcomments`
--

LOCK TABLES `lixshipment_vendorcomments` WRITE;
/*!40000 ALTER TABLE `lixshipment_vendorcomments` DISABLE KEYS */;
/*!40000 ALTER TABLE `lixshipment_vendorcomments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `manufacturer`
--

DROP TABLE IF EXISTS `manufacturer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `manufacturer` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `VERSION` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UNIQUE_MANUFACTURER_NAME` (`NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `manufacturer`
--

LOCK TABLES `manufacturer` WRITE;
/*!40000 ALTER TABLE `manufacturer` DISABLE KEYS */;
/*!40000 ALTER TABLE `manufacturer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `manufacturer_aud`
--

DROP TABLE IF EXISTS `manufacturer_aud`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `manufacturer_aud` (
  `ID` bigint(20) NOT NULL DEFAULT '0',
  `REV` bigint(20) NOT NULL AUTO_INCREMENT,
  `REVTYPE` decimal(3,0) DEFAULT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`,`REV`),
  KEY `FKD2464542DF74E053` (`REV`),
  CONSTRAINT `FKD2464542DF74E053` FOREIGN KEY (`REV`) REFERENCES `revinfo` (`REV`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `manufacturer_aud`
--

LOCK TABLES `manufacturer_aud` WRITE;
/*!40000 ALTER TABLE `manufacturer_aud` DISABLE KEYS */;
/*!40000 ALTER TABLE `manufacturer_aud` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `matchbook`
--

DROP TABLE IF EXISTS `matchbook`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `matchbook` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `DEVICE` varchar(255) DEFAULT NULL,
  `FAMILY` varchar(255) DEFAULT NULL,
  `MODEL` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `VERSION` bigint(20) DEFAULT NULL,
  `MANUFACTURER_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UK_MATCHBOOK_1` (`MANUFACTURER_ID`,`MODEL`),
  CONSTRAINT `FK9C11CA0ED72635FD` FOREIGN KEY (`MANUFACTURER_ID`) REFERENCES `manufacturer` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `matchbook`
--

LOCK TABLES `matchbook` WRITE;
/*!40000 ALTER TABLE `matchbook` DISABLE KEYS */;
/*!40000 ALTER TABLE `matchbook` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `matchbook_aud`
--

DROP TABLE IF EXISTS `matchbook_aud`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `matchbook_aud` (
  `ID` bigint(20) NOT NULL DEFAULT '0',
  `REV` bigint(20) NOT NULL AUTO_INCREMENT,
  `REVTYPE` decimal(3,0) DEFAULT NULL,
  `DEVICE` varchar(255) DEFAULT NULL,
  `FAMILY` varchar(255) DEFAULT NULL,
  `MODEL` varchar(255) DEFAULT NULL,
  `MANUFACTURER_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`,`REV`),
  KEY `FK4B0E40DFDF74E053` (`REV`),
  CONSTRAINT `FK4B0E40DFDF74E053` FOREIGN KEY (`REV`) REFERENCES `revinfo` (`REV`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `matchbook_aud`
--

LOCK TABLES `matchbook_aud` WRITE;
/*!40000 ALTER TABLE `matchbook_aud` DISABLE KEYS */;
/*!40000 ALTER TABLE `matchbook_aud` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `messagemapping`
--

DROP TABLE IF EXISTS `messagemapping`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `messagemapping` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(255) DEFAULT NULL,
  `VERSION` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=117385611 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `messagemapping`
--

LOCK TABLES `messagemapping` WRITE;
/*!40000 ALTER TABLE `messagemapping` DISABLE KEYS */;
INSERT INTO `messagemapping` VALUES (4147191,'AddressID',0),(4147192,'AddressID(base 36)',0),(4147193,'Contact Name',0),(4147194,'Desktop',0),(4147195,'EXT:APD_InterchangeID',0),(4147196,'EXT:CarrierRouting',0),(4147197,'EXT:DeliveryDate',0),(4147198,'EXT:Dept',0),(4147199,'EXT:Facility',0),(4147200,'EXT:Fedex No',0),(4147201,'EXT:LocationID',0),(4147202,'EXT:Mailstop',0),(4147203,'EXT:NOWL',0),(4147204,'EXT:OrderType',0),(4147205,'EXT:POL',0),(4147206,'EXT:SolomonID',0),(4147207,'EXT:USAccount',0),(4147208,'Header Comments',0),(4147209,'OldRelease',0),(4147210,'Order Email',0),(4147211,'Street 2',0),(4147212,'Street 3',0),(8000760,'Requester Phone',0),(39304597,'PR No.',0),(39304598,'Manufacturer Part Number',0),(39304599,'Total Eaches',0),(39304600,'No Charge Item',0),(39304601,'refpo',0),(39304602,'APD_AccountingID',0),(39304603,'fulfillment',0),(39304604,'requestername',0),(39304605,'APD_InterchangeID',0),(39304606,'CarrierRouting',0),(39304607,'paymenttype',0),(39304653,'EXT:Taxable',0),(39304654,'EXT:County',0),(110066900,'EXT:blanketpo',0),(116931178,'Contact Phone',0),(117385233,'County',0),(117385538,'EXT:APD_AccountingID',0),(117385545,'EXT:fulfillment',0),(117385546,'EXT:paymenttype',0),(117385547,'EXT:refpo',0),(117385548,'EXT:requestername',0),(117385610,'Taxable',0);
/*!40000 ALTER TABLE `messagemapping` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `messagemetadata`
--

DROP TABLE IF EXISTS `messagemetadata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `messagemetadata` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `COMMUNICATIONTYPE` varchar(255) DEFAULT NULL,
  `CONTENTLENGTH` bigint(20) DEFAULT NULL,
  `DESTINATION` varchar(2000) DEFAULT NULL,
  `FILEPATH` varchar(255) DEFAULT NULL,
  `GROUPID` varchar(255) DEFAULT NULL,
  `INTERCHANGEID` varchar(255) DEFAULT NULL,
  `MESSAGEDATE` datetime DEFAULT NULL,
  `MESSAGETYPE` varchar(255) DEFAULT NULL,
  `SENDERID` varchar(255) DEFAULT NULL,
  `TRANSACTIONID` varchar(255) DEFAULT NULL,
  `VERSION` bigint(20) DEFAULT NULL,
  `CONTENTTYPE` varchar(255) DEFAULT NULL,
  `RECEIVERID` varchar(255) DEFAULT NULL,
  `BCCRECIPIENTS` varchar(2000) DEFAULT NULL,
  `CCRECIPIENTS` varchar(2000) DEFAULT NULL,
  `OBJECTSWRAPPER_ID` bigint(20) DEFAULT NULL,
  `ATTACHMENTNAME` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `MESSAGEMETA_GROUPID` (`GROUPID`) USING BTREE,
  KEY `MESSAGEMETA_INTERCHANGEID` (`INTERCHANGEID`) USING BTREE,
  KEY `MESSAGEMETA_SENDERID` (`SENDERID`) USING BTREE,
  KEY `MESSAGEMETA_TRANSACTIONID` (`TRANSACTIONID`) USING BTREE,
  KEY `FKD964A0363E63769E` (`OBJECTSWRAPPER_ID`),
  CONSTRAINT `FKD964A0363E63769E` FOREIGN KEY (`OBJECTSWRAPPER_ID`) REFERENCES `messageobjectswrapper` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `messagemetadata`
--

LOCK TABLES `messagemetadata` WRITE;
/*!40000 ALTER TABLE `messagemetadata` DISABLE KEYS */;
/*!40000 ALTER TABLE `messagemetadata` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `messageobjectswrapper`
--

DROP TABLE IF EXISTS `messageobjectswrapper`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `messageobjectswrapper` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `DATA` longblob,
  `VERSION` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `messageobjectswrapper`
--

LOCK TABLES `messageobjectswrapper` WRITE;
/*!40000 ALTER TABLE `messageobjectswrapper` DISABLE KEYS */;
/*!40000 ALTER TABLE `messageobjectswrapper` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `militaryzip`
--

DROP TABLE IF EXISTS `militaryzip`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `militaryzip` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `CITY` varchar(255) DEFAULT NULL,
  `COUNTY` varchar(255) DEFAULT NULL,
  `STATE` varchar(255) DEFAULT NULL,
  `USPSCITY` varchar(255) DEFAULT NULL,
  `VERSION` bigint(20) DEFAULT NULL,
  `ZIP` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `militaryzip`
--

LOCK TABLES `militaryzip` WRITE;
/*!40000 ALTER TABLE `militaryzip` DISABLE KEYS */;
/*!40000 ALTER TABLE `militaryzip` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `miscshipto`
--

DROP TABLE IF EXISTS `miscshipto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `miscshipto` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `AREA` varchar(255) DEFAULT NULL,
  `ADDRESSID` varchar(255) DEFAULT NULL,
  `ADDRESSID36` varchar(255) DEFAULT NULL,
  `AIRPORTCODE` varchar(255) DEFAULT NULL,
  `APDINTERCHANGEID` varchar(255) DEFAULT NULL,
  `CARRIERNAME` varchar(255) DEFAULT NULL,
  `CARRIERROUTING` varchar(255) DEFAULT NULL,
  `CONTACTNAME` varchar(255) DEFAULT NULL,
  `CONTRACTINGOFFICER` varchar(255) DEFAULT NULL,
  `CUSTOMERSHIPTOID` varchar(255) DEFAULT NULL,
  `DCNUMBER` varchar(255) DEFAULT NULL,
  `DELIVERTONAME` varchar(255) DEFAULT NULL,
  `DELIVERYDATE` varchar(255) DEFAULT NULL,
  `DEPARTMENT` varchar(255) DEFAULT NULL,
  `DEPT` varchar(255) DEFAULT NULL,
  `DESKTOP` varchar(255) DEFAULT NULL,
  `DISTRICT` varchar(255) DEFAULT NULL,
  `DIVISION` varchar(255) DEFAULT NULL,
  `EDIID` varchar(255) DEFAULT NULL,
  `FACILITY` varchar(255) DEFAULT NULL,
  `FEDEXNUMBER` varchar(255) DEFAULT NULL,
  `FINANCENUMBER` varchar(255) DEFAULT NULL,
  `GLNUMBER` varchar(255) DEFAULT NULL,
  `GLNID` varchar(255) DEFAULT NULL,
  `HEADERCOMMENTS` varchar(255) DEFAULT NULL,
  `LOB` varchar(255) DEFAULT NULL,
  `LOCATIONID` varchar(255) DEFAULT NULL,
  `LOCATIONS` varchar(255) DEFAULT NULL,
  `MAILSTOP` varchar(255) DEFAULT NULL,
  `MANAGER` varchar(255) DEFAULT NULL,
  `NAMEOFHOSPITAL` varchar(255) DEFAULT NULL,
  `NOWL` varchar(255) DEFAULT NULL,
  `OLDRELEASE` varchar(255) DEFAULT NULL,
  `ORDEREMAIL` varchar(255) DEFAULT NULL,
  `ORIGIN` varchar(255) DEFAULT NULL,
  `POL` varchar(255) DEFAULT NULL,
  `PURCHASINGGROUP` varchar(255) DEFAULT NULL,
  `REQUESTERNAME` varchar(255) DEFAULT NULL,
  `SHIPPINGMETHOD` varchar(255) DEFAULT NULL,
  `SOLOMONID` varchar(255) DEFAULT NULL,
  `STORENUMBER` varchar(255) DEFAULT NULL,
  `STREET2` varchar(255) DEFAULT NULL,
  `STREET3` varchar(255) DEFAULT NULL,
  `UNIT` varchar(255) DEFAULT NULL,
  `USACCOUNT` varchar(255) DEFAULT NULL,
  `USPSUNDER13OZ` varchar(255) DEFAULT NULL,
  `VENDOR` varchar(255) DEFAULT NULL,
  `REQUESTERPHONE` varchar(255) DEFAULT NULL,
  `CONTRACTNUMBER` varchar(255) DEFAULT NULL,
  `DIVDEPT` varchar(255) DEFAULT NULL,
  `FEDSTRIPNUMBER` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `miscshipto`
--

LOCK TABLES `miscshipto` WRITE;
/*!40000 ALTER TABLE `miscshipto` DISABLE KEYS */;
/*!40000 ALTER TABLE `miscshipto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `notetype`
--

DROP TABLE IF EXISTS `notetype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `notetype` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `VERSION` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UC_NOTETYPE_1` (`NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `notetype`
--

LOCK TABLES `notetype` WRITE;
/*!40000 ALTER TABLE `notetype` DISABLE KEYS */;
/*!40000 ALTER TABLE `notetype` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `notificationlimit`
--

DROP TABLE IF EXISTS `notificationlimit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `notificationlimit` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `EVENTTYPE` varchar(255) DEFAULT NULL,
  `LIMITTYPE` varchar(255) DEFAULT NULL,
  `VERSION` bigint(20) DEFAULT NULL,
  `CREDENTIAL_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `NL_CREDENTIAL_ID` (`CREDENTIAL_ID`) USING BTREE,
  CONSTRAINT `FK8C01ED70AFD1BCFD` FOREIGN KEY (`CREDENTIAL_ID`) REFERENCES `credential` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=119524505 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `notificationlimit`
--

LOCK TABLES `notificationlimit` WRITE;
/*!40000 ALTER TABLE `notificationlimit` DISABLE KEYS */;
INSERT INTO `notificationlimit` VALUES (75746450,'TECH_NOTIFICATION','BCC_ONLY',0,75746448),(75746451,'VENDOR_NOTIFICATION','BCC_ONLY',0,75746448),(75746452,'ORDER_REQUEST','BCC_ONLY',0,75746448),(75746453,'ADVANCED_SHIPMENT_NOTICE','ALL',0,75746448),(75746454,'ORDER_ACKNOWLEDGEMENT_ERRORS','BCC_ONLY',0,75746448),(75746455,'RETURNED','ALL',0,75746448),(75746456,'DELIVERY_DRIVER_PHOTO','NONE',0,75746448),(75746457,'ORDER_ACKNOWLEDGEMENT','ALL',0,75746448),(75746458,'JUMPTRACK_STOP_NOTIFICATION','BCC_ONLY',0,75746448),(75746459,'SHIPMENT_NOTICE','ALL',0,75746448),(75746460,'CREDIT_CARD_RECEIPT','ALL',0,75746448),(75746461,'SHIPMENT_NOTIFICATION_ERRORS','BCC_ONLY',0,75746448),(75746462,'CUSTOMER_NOTIFICATION','BCC_ONLY',0,75746448),(75746463,'VOUCHER_ERRORS','BCC_ONLY',0,75746448),(75746464,'CC_DECLINED_NOTICE','BCC_ONLY',0,75746448),(75746465,'CUSTOMER_INVOICED','ALL',0,75746448),(75746466,'RESENT_NOTFICATION','NONE',0,75746448),(75746467,'CREDIT_CARD_DECLINED','ALL',0,75746448),(75746468,'LATE_SHIPMENT_NOTICE','ALL',0,75746448),(75746469,'PUNCHOUT_SETUP_REQUEST','NONE',0,75746448),(75746470,'BACKORDERED_ITEM_NOTICE','ALL',0,75746448),(75746471,'VOUCHED','BCC_ONLY',0,75746448),(75746472,'DELIVERY_DRIVER_COMMENTS','NONE',0,75746448),(75746473,'ORDER_CANCELED','ALL',0,75746448),(75746474,'ORDER_APPROVAL_NEEDED','ALL',0,75746448),(75746475,'PURCHASE_ORDER_PARTNER_RESPONSE','NONE',0,75746448),(75746476,'ORDER_FAILED','ALL',0,75746448),(75746477,'PUNCHOUT_ORDER_MESSAGE','ALL',0,75746448),(75746478,'CREDIT_CARD_TRANSACTION','NONE',0,75746448),(75746479,'FUNCTIONAL_ACKNOWLEDGEMENT','BCC_ONLY',0,75746448),(75746480,'CREDIT_CARD_CREDIT','ALL',0,75746448),(75746481,'CUSTOMER_CREDIT_INVOICED','ALL',0,75746448),(75746482,'ORDER_DENIED','ALL',0,75746448),(75746483,'DELIVERY_SIGNATURE','NONE',0,75746448),(75746484,'PURCHASE_ORDER_SENT','BCC_ONLY',0,75746448),(75746485,'DECLINED','BCC_ONLY',0,75746448),(75746486,'JUMPTRACK_MANIFEST','BCC_ONLY',0,75746448),(119104577,'PUNCHOUT_SETUP_REQUEST','NONE',0,60094),(119104578,'CUSTOMER_NOTIFICATION','BCC_ONLY',0,60094),(119104579,'ORDER_ACKNOWLEDGEMENT_ERRORS','BCC_ONLY',0,60094),(119104580,'CUSTOMER_CREDIT_INVOICED','ALL',0,60094),(119104581,'ORDER_CANCELED','ALL',0,60094),(119104582,'CUSTOMER_INVOICED','ALL',0,60094),(119104583,'DECLINED','BCC_ONLY',0,60094),(119104584,'CC_DECLINED_NOTICE','BCC_ONLY',0,60094),(119104585,'LATE_SHIPMENT_NOTICE','ALL',0,60094),(119104586,'DELIVERY_SIGNATURE','NONE',0,60094),(119104587,'INVOICED','ALL',0,60094),(119104588,'BACKORDERED_ITEM_NOTICE','ALL',0,60094),(119104589,'VOUCHED','BCC_ONLY',0,60094),(119104590,'TECH_NOTIFICATION','BCC_ONLY',0,60094),(119104591,'RETURNED','ALL',0,60094),(119104592,'FUNCTIONAL_ACKNOWLEDGEMENT','BCC_ONLY',0,60094),(119104593,'SHIPMENT_NOTICE_PRE_ENCRYPTION','ALL',0,60094),(119104594,'PURCHASE_ORDER_PARTNER_RESPONSE','NONE',0,60094),(119104595,'RESENT_NOTFICATION','NONE',0,60094),(119104596,'INVOICE_PRE_ENCRYPTION','ALL',0,60094),(119104597,'ORDER_APPROVAL_NEEDED','ALL',0,60094),(119104598,'ORDER_DENIED','ALL',0,60094),(119104599,'PURCHASE_ORDER_SENT','BCC_ONLY',0,60094),(119104600,'JUMPTRACK_MANIFEST','BCC_ONLY',0,60094),(119104601,'VOUCHER_ERRORS','BCC_ONLY',0,60094),(119104602,'VENDOR_NOTIFICATION','BCC_ONLY',0,60094),(119104603,'ORDER_ACKNOWLEDGEMENT','ALL',0,60094),(119104604,'ORDER_ACKNOWLEDGEMENT_PRE_ENCRYPTION','ALL',0,60094),(119104605,'INVOICE_ERRORS','ALL',0,60094),(119104606,'SHIPMENT_NOTIFICATION_ERRORS','BCC_ONLY',0,60094),(119104607,'DELIVERY_DRIVER_COMMENTS','NONE',0,60094),(119104608,'PUNCHOUT_ORDER_MESSAGE','ALL',0,60094),(119104609,'CREDIT_CARD_CREDIT','ALL',0,60094),(119104610,'SHIPMENT_NOTICE','ALL',0,60094),(119104611,'ORDER_REQUEST','BCC_ONLY',0,60094),(119104612,'CREDIT_CARD_RECEIPT','ALL',0,60094),(119104613,'DELIVERY_DRIVER_PHOTO','NONE',0,60094),(119104614,'ORDER_FAILED','ALL',0,60094),(119104615,'CREDIT_CARD_TRANSACTION','NONE',0,60094),(119104616,'JUMPTRACK_STOP_NOTIFICATION','BCC_ONLY',0,60094),(119104617,'CREDIT_CARD_DECLINED','ALL',0,60094),(119104618,'ADVANCED_SHIPMENT_NOTICE','ALL',0,60094),(119496151,'VOUCHED','BCC_ONLY',0,3825168),(119496836,'TECH_NOTIFICATION','BCC_ONLY',0,3825168),(119497521,'BACKORDERED_ITEM_NOTICE','ALL',0,3825168),(119498206,'VENDOR_NOTIFICATION','BCC_ONLY',0,3825168),(119498891,'PURCHASE_ORDER_PARTNER_RESPONSE','NONE',0,3825168),(119499576,'SHIPMENT_NOTIFICATION_ERRORS','BCC_ONLY',0,3825168),(119500261,'CUSTOMER_CREDIT_INVOICED','ALL',0,3825168),(119500946,'ADVANCED_SHIPMENT_NOTICE','ALL',0,3825168),(119501631,'JUMPTRACK_STOP_NOTIFICATION','BCC_ONLY',0,3825168),(119502316,'CUSTOMER_INVOICED','ALL',0,3825168),(119503001,'ORDER_ACKNOWLEDGEMENT','ALL',0,3825168),(119503686,'RETURNED','ALL',0,3825168),(119504371,'ORDER_CANCELED','ALL',0,3825168),(119505056,'ORDER_DENIED','ALL',0,3825168),(119505741,'PURCHASE_ORDER_SENT','BCC_ONLY',0,3825168),(119506426,'CREDIT_CARD_TRANSACTION','NONE',0,3825168),(119507111,'DELIVERY_DRIVER_PHOTO','NONE',0,3825168),(119507796,'LATE_SHIPMENT_NOTICE','ALL',0,3825168),(119508481,'SHIPMENT_NOTICE','ALL',0,3825168),(119509166,'PUNCHOUT_ORDER_MESSAGE','ALL',0,3825168),(119509851,'CC_DECLINED_NOTICE','BCC_ONLY',0,3825168),(119510536,'VOUCHER_ERRORS','BCC_ONLY',0,3825168),(119511221,'CREDIT_CARD_DECLINED','ALL',0,3825168),(119511906,'FUNCTIONAL_ACKNOWLEDGEMENT','BCC_ONLY',0,3825168),(119512591,'DELIVERY_SIGNATURE','NONE',0,3825168),(119513276,'CREDIT_CARD_CREDIT','ALL',0,3825168),(119513989,'SHIPMENT_NOTICE_PRE_ENCRYPTION','ALL',0,3825168),(119513999,'SHIPMENT_NOTICE_PRE_ENCRYPTION','ALL',0,75746448),(119514713,'INVOICE_PRE_ENCRYPTION','ALL',0,3825168),(119514723,'INVOICE_PRE_ENCRYPTION','ALL',0,75746448),(119515448,'INVOICE_ERRORS','BCC_ONLY',0,3825168),(119515461,'INVOICE_ERRORS','BCC_ONLY',0,75746448),(119516170,'ORDER_ACKNOWLEDGEMENT_ERRORS','BCC_ONLY',0,3825168),(119516855,'RESENT_NOTFICATION','ALL',0,3825168),(119517540,'ORDER_APPROVAL_NEEDED','ALL',0,3825168),(119518225,'JUMPTRACK_MANIFEST','BCC_ONLY',0,3825168),(119518910,'ORDER_FAILED','ALL',0,3825168),(119519634,'INVOICED','BCC_ONLY',0,3825168),(119519647,'INVOICED','BCC_ONLY',0,75746448),(119520356,'ORDER_REQUEST','BCC_ONLY',0,3825168),(119521041,'DECLINED','BCC_ONLY',0,3825168),(119521726,'CUSTOMER_NOTIFICATION','BCC_ONLY',0,3825168),(119522411,'PUNCHOUT_SETUP_REQUEST','NONE',0,3825168),(119523096,'DELIVERY_DRIVER_COMMENTS','NONE',0,3825168),(119523781,'CREDIT_CARD_RECEIPT','ALL',0,3825168),(119524494,'ORDER_ACKNOWLEDGEMENT_PRE_ENCRYPTION','ALL',0,3825168),(119524504,'ORDER_ACKNOWLEDGEMENT_PRE_ENCRYPTION','ALL',0,75746448);
/*!40000 ALTER TABLE `notificationlimit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `notificationproperties`
--

DROP TABLE IF EXISTS `notificationproperties`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `notificationproperties` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `ADDRESSING` varchar(255) DEFAULT NULL,
  `EVENTTYPE` varchar(255) DEFAULT NULL,
  `VERSION` bigint(20) DEFAULT NULL,
  `ORDER_ID` bigint(20) DEFAULT NULL,
  `ACC_ID` bigint(20) DEFAULT NULL,
  `CRED_ID` bigint(20) DEFAULT NULL,
  `RECIPIENTS` varchar(2000) NOT NULL,
  `OVERRIDECREDENTIAL` decimal(1,0) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `INDX_36` (`ACC_ID`) USING BTREE,
  KEY `NOTPROP_ORDER_ID` (`ORDER_ID`) USING BTREE,
  KEY `NP_CRED_ID` (`CRED_ID`) USING BTREE,
  CONSTRAINT `FK2EC8B49E45A534A6` FOREIGN KEY (`CRED_ID`) REFERENCES `credential` (`ID`),
  CONSTRAINT `FK2EC8B49E8E1EC39` FOREIGN KEY (`ORDER_ID`) REFERENCES `customerorder` (`ID`),
  CONSTRAINT `FK2EC8B49EF363A403` FOREIGN KEY (`ACC_ID`) REFERENCES `account` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `notificationproperties`
--

LOCK TABLES `notificationproperties` WRITE;
/*!40000 ALTER TABLE `notificationproperties` DISABLE KEYS */;
/*!40000 ALTER TABLE `notificationproperties` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_log`
--

DROP TABLE IF EXISTS `order_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order_log` (
  `FROM_CLASS` varchar(31) NOT NULL,
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `EVENTTYPE` varchar(255) DEFAULT NULL,
  `UPDATETIMESTAMP` datetime DEFAULT NULL,
  `VERSION` bigint(20) DEFAULT NULL,
  `APDPO` varchar(255) DEFAULT NULL,
  `CARDEXPRIATION` datetime DEFAULT NULL,
  `CUSTPO` varchar(255) DEFAULT NULL,
  `FAILUREREASON` varchar(255) DEFAULT NULL,
  `FREIGHTAMOUNT` decimal(19,2) DEFAULT NULL,
  `ITEMCOUNT` bigint(20) DEFAULT NULL,
  `NAME_ON_CARD` varchar(255) DEFAULT NULL,
  `SALESTAX` decimal(19,2) DEFAULT NULL,
  `SUBTOTAL` decimal(19,2) DEFAULT NULL,
  `TIMESTAMP` datetime DEFAULT NULL,
  `TOTALAMOUNT` decimal(19,2) DEFAULT NULL,
  `TRACKINGNUM` varchar(255) DEFAULT NULL,
  `TRANSACTIONAMOUNT` decimal(19,2) DEFAULT NULL,
  `TRANSACTIONKEY` varchar(255) DEFAULT NULL,
  `TRANSACTIONSUCCESS` decimal(1,0) DEFAULT NULL,
  `TRANSACTIONTYPE` varchar(255) DEFAULT NULL,
  `CUSTOMERORDER_ID` bigint(20) DEFAULT NULL,
  `MESSAGEMETADATA_ID` bigint(20) DEFAULT NULL,
  `ORDERSTATUS_ID` bigint(20) DEFAULT NULL,
  `SHIPMENT_ID` bigint(20) DEFAULT NULL,
  `INVOICE_ID` bigint(20) DEFAULT NULL,
  `LINEITEM_ID` bigint(20) DEFAULT NULL,
  `LINEITEMSTATUS_ID` bigint(20) DEFAULT NULL,
  `CARD_IDENTIFIER` varchar(255) DEFAULT NULL,
  `RETURNORDERSTATUS` varchar(255) DEFAULT NULL,
  `RETURNORDER_ID` bigint(20) DEFAULT NULL,
  `AUTHCODE` varchar(255) DEFAULT NULL,
  `DESCRIPTION` varchar(3000) DEFAULT NULL,
  `CHANGINGUSER` varchar(255) DEFAULT NULL,
  `CREDITCARD_RETURNORDER_ID` bigint(20) DEFAULT NULL,
  `NOTE` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `CCTRANLOG_RETURN_IDX` (`CREDITCARD_RETURNORDER_ID`) USING BTREE,
  KEY `CCTRANLOG_SUCCESS_IDX` (`TRANSACTIONSUCCESS`) USING BTREE,
  KEY `INDX_37` (`ORDERSTATUS_ID`) USING BTREE,
  KEY `INDX_38` (`LINEITEM_ID`) USING BTREE,
  KEY `ORDERLOG_CUSTORDER_IDX` (`CUSTOMERORDER_ID`) USING BTREE,
  KEY `ORDERLOG_UPDATETIME_IDX` (`UPDATETIMESTAMP`) USING BTREE,
  KEY `ORDER_LOG_MMD_ID` (`MESSAGEMETADATA_ID`) USING BTREE,
  KEY `FK8ED49E93753382D7` (`INVOICE_ID`),
  KEY `FK8ED49E93B27BA87D` (`LINEITEMSTATUS_ID`),
  KEY `FK8ED49E9396D53A97` (`RETURNORDER_ID`),
  KEY `FK8ED49E93BBACDEDD` (`SHIPMENT_ID`),
  CONSTRAINT `FK8ED49E9326CAE17` FOREIGN KEY (`CUSTOMERORDER_ID`) REFERENCES `customerorder` (`ID`),
  CONSTRAINT `FK8ED49E93454B440D` FOREIGN KEY (`CREDITCARD_RETURNORDER_ID`) REFERENCES `returnorder` (`ID`),
  CONSTRAINT `FK8ED49E93753382D7` FOREIGN KEY (`INVOICE_ID`) REFERENCES `invoice` (`ID`),
  CONSTRAINT `FK8ED49E9384C13B97` FOREIGN KEY (`ORDERSTATUS_ID`) REFERENCES `orderstatus` (`ID`),
  CONSTRAINT `FK8ED49E9396D53A97` FOREIGN KEY (`RETURNORDER_ID`) REFERENCES `returnorder` (`ID`),
  CONSTRAINT `FK8ED49E93A910D157` FOREIGN KEY (`MESSAGEMETADATA_ID`) REFERENCES `messagemetadata` (`ID`),
  CONSTRAINT `FK8ED49E93B27BA87D` FOREIGN KEY (`LINEITEMSTATUS_ID`) REFERENCES `lineitemstatus` (`ID`),
  CONSTRAINT `FK8ED49E93B7777FDD` FOREIGN KEY (`LINEITEM_ID`) REFERENCES `lineitem` (`ID`),
  CONSTRAINT `FK8ED49E93BBACDEDD` FOREIGN KEY (`SHIPMENT_ID`) REFERENCES `shipment` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_log`
--

LOCK TABLES `order_log` WRITE;
/*!40000 ALTER TABLE `order_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `order_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_log_lineitemxshipment`
--

DROP TABLE IF EXISTS `order_log_lineitemxshipment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order_log_lineitemxshipment` (
  `ORDER_LOG_ID` bigint(20) DEFAULT NULL,
  `LINEITEMXSHIPMENTS_ID` bigint(20) DEFAULT NULL,
  KEY `ORDERLOGLIXS_LIXS_IDX` (`LINEITEMXSHIPMENTS_ID`) USING BTREE,
  KEY `ORDERLOGLIXS_ORDERLOG_IDX` (`ORDER_LOG_ID`) USING BTREE,
  CONSTRAINT `FK8321BDFF255C549A` FOREIGN KEY (`LINEITEMXSHIPMENTS_ID`) REFERENCES `lineitemxshipment` (`ID`),
  CONSTRAINT `FK8321BDFF3EA8F9` FOREIGN KEY (`ORDER_LOG_ID`) REFERENCES `order_log` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_log_lineitemxshipment`
--

LOCK TABLES `order_log_lineitemxshipment` WRITE;
/*!40000 ALTER TABLE `order_log_lineitemxshipment` DISABLE KEYS */;
/*!40000 ALTER TABLE `order_log_lineitemxshipment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `orderattachment`
--

DROP TABLE IF EXISTS `orderattachment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `orderattachment` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `ATTACHMENTTIMESTAMP` datetime NOT NULL,
  `FILENAME` varchar(255) NOT NULL,
  `VERSION` bigint(20) DEFAULT NULL,
  `USER_ID` bigint(20) DEFAULT NULL,
  `ORDER_ID` bigint(20) DEFAULT NULL,
  `SERVICELOG_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FKD35F2AF18E1EC39` (`ORDER_ID`),
  KEY `FKD35F2AF14E9ECF4C` (`USER_ID`),
  KEY `FKD35F2AF1797AADDD` (`SERVICELOG_ID`),
  CONSTRAINT `FKD35F2AF14E9ECF4C` FOREIGN KEY (`USER_ID`) REFERENCES `systemuser` (`ID`),
  CONSTRAINT `FKD35F2AF1797AADDD` FOREIGN KEY (`SERVICELOG_ID`) REFERENCES `service_log` (`ID`),
  CONSTRAINT `FKD35F2AF18E1EC39` FOREIGN KEY (`ORDER_ID`) REFERENCES `customerorder` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orderattachment`
--

LOCK TABLES `orderattachment` WRITE;
/*!40000 ALTER TABLE `orderattachment` DISABLE KEYS */;
/*!40000 ALTER TABLE `orderattachment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `orderbrmsrecord`
--

DROP TABLE IF EXISTS `orderbrmsrecord`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `orderbrmsrecord` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `ACTIVE` decimal(1,0) DEFAULT NULL,
  `PROCESSID` bigint(20) DEFAULT NULL,
  `SESSIONID` bigint(20) DEFAULT NULL,
  `VERSION` bigint(20) DEFAULT NULL,
  `ENTRYID` bigint(20) DEFAULT NULL,
  `SNAPSHOT` varchar(255) DEFAULT NULL,
  `REPLACESESSION` decimal(1,0) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `ORDERBRMSRECORD_ENTRYID` (`ENTRYID`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orderbrmsrecord`
--

LOCK TABLES `orderbrmsrecord` WRITE;
/*!40000 ALTER TABLE `orderbrmsrecord` DISABLE KEYS */;
/*!40000 ALTER TABLE `orderbrmsrecord` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ordereditem`
--

DROP TABLE IF EXISTS `ordereditem`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ordereditem` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `QUANTITY` bigint(20) DEFAULT NULL,
  `STATUS` varchar(255) DEFAULT NULL,
  `VERSION` bigint(20) DEFAULT NULL,
  `CUSTOMERORDER_ID` bigint(20) DEFAULT NULL,
  `ITEM_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FKE25D91006CE026BC` (`ITEM_ID`),
  KEY `FKE25D910026CAE17` (`CUSTOMERORDER_ID`),
  CONSTRAINT `FKE25D910026CAE17` FOREIGN KEY (`CUSTOMERORDER_ID`) REFERENCES `customerorder` (`ID`),
  CONSTRAINT `FKE25D91006CE026BC` FOREIGN KEY (`ITEM_ID`) REFERENCES `catalogxitem` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ordereditem`
--

LOCK TABLES `ordereditem` WRITE;
/*!40000 ALTER TABLE `ordereditem` DISABLE KEYS */;
/*!40000 ALTER TABLE `ordereditem` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ordereditem_propertytype`
--

DROP TABLE IF EXISTS `ordereditem_propertytype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ordereditem_propertytype` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `VALUE` varchar(255) DEFAULT NULL,
  `VERSION` bigint(20) DEFAULT NULL,
  `ORDEREDITEM_ID` bigint(20) DEFAULT NULL,
  `TYPE_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK94D9BBAEAF08DB97` (`ORDEREDITEM_ID`),
  KEY `FK94D9BBAE9D94BE6C` (`TYPE_ID`),
  CONSTRAINT `FK94D9BBAE9D94BE6C` FOREIGN KEY (`TYPE_ID`) REFERENCES `ordereditempropertytype` (`ID`),
  CONSTRAINT `FK94D9BBAEAF08DB97` FOREIGN KEY (`ORDEREDITEM_ID`) REFERENCES `ordereditem` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ordereditem_propertytype`
--

LOCK TABLES `ordereditem_propertytype` WRITE;
/*!40000 ALTER TABLE `ordereditem_propertytype` DISABLE KEYS */;
/*!40000 ALTER TABLE `ordereditem_propertytype` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ordereditempropertytype`
--

DROP TABLE IF EXISTS `ordereditempropertytype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ordereditempropertytype` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(255) DEFAULT NULL,
  `VERSION` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=31307 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ordereditempropertytype`
--

LOCK TABLES `ordereditempropertytype` WRITE;
/*!40000 ALTER TABLE `ordereditempropertytype` DISABLE KEYS */;
INSERT INTO `ordereditempropertytype` VALUES (31305,'City Tax',0),(31306,'State Tax',0);
/*!40000 ALTER TABLE `ordereditempropertytype` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ordereditemxexchangeditem`
--

DROP TABLE IF EXISTS `ordereditemxexchangeditem`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ordereditemxexchangeditem` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `EXCHANGEDATE` datetime DEFAULT NULL,
  `EXCHANGEQUANTITY` bigint(20) DEFAULT NULL,
  `ORDEREDQUANTITY` bigint(20) DEFAULT NULL,
  `VERSION` bigint(20) DEFAULT NULL,
  `EXCHANGEDITEM_ID` bigint(20) DEFAULT NULL,
  `ORDEREDITEM_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK69353CDC7132ABFC` (`EXCHANGEDITEM_ID`),
  KEY `FK69353CDCAF08DB97` (`ORDEREDITEM_ID`),
  CONSTRAINT `FK69353CDC7132ABFC` FOREIGN KEY (`EXCHANGEDITEM_ID`) REFERENCES `item` (`ID`),
  CONSTRAINT `FK69353CDCAF08DB97` FOREIGN KEY (`ORDEREDITEM_ID`) REFERENCES `ordereditem` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ordereditemxexchangeditem`
--

LOCK TABLES `ordereditemxexchangeditem` WRITE;
/*!40000 ALTER TABLE `ordereditemxexchangeditem` DISABLE KEYS */;
/*!40000 ALTER TABLE `ordereditemxexchangeditem` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ordereditemxshipment`
--

DROP TABLE IF EXISTS `ordereditemxshipment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ordereditemxshipment` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `QUANTITY` bigint(20) DEFAULT NULL,
  `SHIPPINGCOST` decimal(19,2) NOT NULL,
  `VERSION` bigint(20) DEFAULT NULL,
  `ORDEREDITEM_ID` bigint(20) DEFAULT NULL,
  `SHIPMENT_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FKEBEF8D12AF08DB97` (`ORDEREDITEM_ID`),
  KEY `FKEBEF8D12BBACDEDD` (`SHIPMENT_ID`),
  CONSTRAINT `FKEBEF8D12AF08DB97` FOREIGN KEY (`ORDEREDITEM_ID`) REFERENCES `ordereditem` (`ID`),
  CONSTRAINT `FKEBEF8D12BBACDEDD` FOREIGN KEY (`SHIPMENT_ID`) REFERENCES `shipment` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ordereditemxshipment`
--

LOCK TABLES `ordereditemxshipment` WRITE;
/*!40000 ALTER TABLE `ordereditemxshipment` DISABLE KEYS */;
/*!40000 ALTER TABLE `ordereditemxshipment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `orderprocesslookup`
--

DROP TABLE IF EXISTS `orderprocesslookup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `orderprocesslookup` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `VERSION` bigint(20) DEFAULT NULL,
  `CUSTOMERORDER_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `ORDERPROCESS_CUSTORDER_IDX` (`CUSTOMERORDER_ID`) USING BTREE,
  CONSTRAINT `FK2C2B183B26CAE17` FOREIGN KEY (`CUSTOMERORDER_ID`) REFERENCES `customerorder` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orderprocesslookup`
--

LOCK TABLES `orderprocesslookup` WRITE;
/*!40000 ALTER TABLE `orderprocesslookup` DISABLE KEYS */;
/*!40000 ALTER TABLE `orderprocesslookup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `orderstatus`
--

DROP TABLE IF EXISTS `orderstatus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `orderstatus` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `DESCRIPTION` varchar(255) DEFAULT NULL,
  `VALUE` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `VERSION` bigint(20) DEFAULT NULL,
  `PAYMENTSTATUS` decimal(1,0) NOT NULL,
  `OVERRIDESTATUS_ID` bigint(20) DEFAULT NULL,
  `CUSTOMERSTATUS` decimal(1,0) NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ORDERSTATUS_VALUE` (`VALUE`),
  KEY `FKC492B8C0BAA7D4D9` (`OVERRIDESTATUS_ID`),
  CONSTRAINT `FKC492B8C0BAA7D4D9` FOREIGN KEY (`OVERRIDESTATUS_ID`) REFERENCES `orderstatus` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=179035906 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orderstatus`
--

LOCK TABLES `orderstatus` WRITE;
/*!40000 ALTER TABLE `orderstatus` DISABLE KEYS */;
INSERT INTO `orderstatus` VALUES (17998,'The order was manually marked as resolved','MANUALLY_RESOLVED',0,0,NULL,0),(31300,'The order was approved','APPROVED',0,0,32001,0),(31301,'The order was denied','DENIED',0,0,NULL,0),(31302,'The order was placed','PLACED',0,0,NULL,0),(31303,'The order was canceled','CANCELED',0,0,NULL,0),(31304,'The order has shipped','SHIPPED',0,0,NULL,1),(32001,'The order is waiting to be filled','WAITING_TO_FILL',0,0,NULL,1),(32002,'The order is being acknowledged','WAITING_TO_FILL_PROCESSING',0,0,NULL,0),(313404,'The order pending a task being completed','PENDING_TASK',0,0,NULL,0),(3850157,'The order was quoted','QUOTED',0,0,NULL,1),(3850158,'The order was ordered','ORDERED',0,0,NULL,0),(3850159,'The order is pending approval','PENDING_APPROVAL',0,0,NULL,0),(3850160,'The order has been sent to the vendor','SENT_TO_VENDOR',0,0,NULL,0),(3850161,'The order was refused','REFUSED',0,0,NULL,0),(3850162,'The order has been accepted by the vendor','ACCEPTED',0,0,32001,0),(3850163,'The order is being shipped','PARTIAL_SHIP',0,0,NULL,1),(3850164,'The order was completed','COMPLETED',0,0,32001,0),(13613634,'The order has been partially billed','PARTIAL_BILL',0,1,NULL,0),(13613635,'The order has been billed','BILLED',0,1,NULL,0),(13613636,'The order has been vouched','VOUCHED',0,0,31304,0),(39304595,'The order was requested.','REQUESTED',0,0,NULL,0),(39304643,'The order is being placed','PLACED_PROCESSING',0,0,NULL,0),(39304644,'The order is being sent to the vendor','SENT_TO_VENDOR_PROCESSING',0,0,32002,0),(39304645,'The order is being vouched','VOUCHING',0,0,31304,0),(39304646,'The order is being shipped','SHIPPED_PROCESSING',0,0,NULL,0),(39304647,'The order is being cancelled','CANCELED_PROCESSING',0,0,NULL,0),(39304648,'The order is validating','VALIDATING',0,0,31302,0),(75361380,NULL,'PUNCHED',0,0,NULL,0),(102582461,'All the items in the requested order were rejected.','REJECTED',0,0,NULL,0),(111109417,'The order has been partially charged','PARTIAL_CHARGE',0,1,NULL,0),(111109418,'The order was charged','CHARGED',0,1,NULL,0),(142708380,'The order is imported from the previous system','CAPS',0,0,NULL,0),(179035879,'A user Invalidated the order','INVALIDATED',0,0,NULL,0),(179035900,'Request for quote has been created','CREATED_RFQ',0,0,NULL,0),(179035901,'Quote has been submitted','SUBMITTED_QUOTE',0,0,NULL,0),(179035902,'Quote has been accepted','ACCEPTED_QUOTE',0,0,NULL,0),(179035903,'Quote has been canceled','CANCELED_QUOTE',0,0,NULL,0),(179035904,'Quote has been expired','EXPIRED_QUOTE',0,0,NULL,0),(179035905,'Quote has been rejected','REJECTED_QUOTE',0,0,NULL,0);
/*!40000 ALTER TABLE `orderstatus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ordertype`
--

DROP TABLE IF EXISTS `ordertype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ordertype` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `DESCRIPTION` varchar(255) DEFAULT NULL,
  `VALUE` varchar(255) DEFAULT NULL,
  `VERSION` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `DEV_INDEX_18` (`VALUE`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=117472535 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ordertype`
--

LOCK TABLES `ordertype` WRITE;
/*!40000 ALTER TABLE `ordertype` DISABLE KEYS */;
INSERT INTO `ordertype` VALUES (4136185,NULL,'EDI',0),(4136186,NULL,'CXML',0),(4136187,NULL,'XCBL',0),(4136188,'eCommerce','URL',0),(4136189,'Manual Order','MANUAL',0),(4136190,'Procurement Platform','PROCUREMENT',0),(75361369,NULL,'PUNCHOUT',0),(75361374,NULL,'CXML',0),(117472534,'Adhoc','ADHOC',0);
/*!40000 ALTER TABLE `ordertype` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `outfromcredentialtable`
--

DROP TABLE IF EXISTS `outfromcredentialtable`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `outfromcredentialtable` (
  `CREDENTIAL_ID` bigint(20) NOT NULL DEFAULT '0',
  `OUTGOINGFROMCREDENTIALS_ID` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`CREDENTIAL_ID`,`OUTGOINGFROMCREDENTIALS_ID`),
  UNIQUE KEY `SYS_C0022185` (`OUTGOINGFROMCREDENTIALS_ID`),
  CONSTRAINT `FK524C227FAFD1BCFD` FOREIGN KEY (`CREDENTIAL_ID`) REFERENCES `credential` (`ID`),
  CONSTRAINT `FK524C227FE8A80542` FOREIGN KEY (`OUTGOINGFROMCREDENTIALS_ID`) REFERENCES `cxmlcredential` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `outfromcredentialtable`
--

LOCK TABLES `outfromcredentialtable` WRITE;
/*!40000 ALTER TABLE `outfromcredentialtable` DISABLE KEYS */;
/*!40000 ALTER TABLE `outfromcredentialtable` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `outfromcredentialtable_aud`
--

DROP TABLE IF EXISTS `outfromcredentialtable_aud`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `outfromcredentialtable_aud` (
  `REV` bigint(20) NOT NULL AUTO_INCREMENT,
  `CREDENTIAL_ID` bigint(20) NOT NULL DEFAULT '0',
  `OUTGOINGFROMCREDENTIALS_ID` bigint(20) NOT NULL DEFAULT '0',
  `REVTYPE` decimal(3,0) DEFAULT NULL,
  PRIMARY KEY (`REV`,`CREDENTIAL_ID`,`OUTGOINGFROMCREDENTIALS_ID`),
  CONSTRAINT `FK3294F8D0DF74E053` FOREIGN KEY (`REV`) REFERENCES `revinfo` (`REV`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `outfromcredentialtable_aud`
--

LOCK TABLES `outfromcredentialtable_aud` WRITE;
/*!40000 ALTER TABLE `outfromcredentialtable_aud` DISABLE KEYS */;
/*!40000 ALTER TABLE `outfromcredentialtable_aud` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `outsendercredentialtable`
--

DROP TABLE IF EXISTS `outsendercredentialtable`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `outsendercredentialtable` (
  `CREDENTIAL_ID` bigint(20) NOT NULL DEFAULT '0',
  `OUTGOINGSENDERCREDENTIALS_ID` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`CREDENTIAL_ID`,`OUTGOINGSENDERCREDENTIALS_ID`),
  UNIQUE KEY `SYS_C0022065` (`OUTGOINGSENDERCREDENTIALS_ID`),
  CONSTRAINT `FK85502C14AFD1BCFD` FOREIGN KEY (`CREDENTIAL_ID`) REFERENCES `credential` (`ID`),
  CONSTRAINT `FK85502C14F444CD8D` FOREIGN KEY (`OUTGOINGSENDERCREDENTIALS_ID`) REFERENCES `cxmlcredential` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `outsendercredentialtable`
--

LOCK TABLES `outsendercredentialtable` WRITE;
/*!40000 ALTER TABLE `outsendercredentialtable` DISABLE KEYS */;
/*!40000 ALTER TABLE `outsendercredentialtable` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `outsendercredentialtable_aud`
--

DROP TABLE IF EXISTS `outsendercredentialtable_aud`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `outsendercredentialtable_aud` (
  `REV` bigint(20) NOT NULL AUTO_INCREMENT,
  `CREDENTIAL_ID` bigint(20) NOT NULL DEFAULT '0',
  `OUTGOINGSENDERCREDENTIALS_ID` bigint(20) NOT NULL DEFAULT '0',
  `REVTYPE` decimal(3,0) DEFAULT NULL,
  PRIMARY KEY (`REV`,`CREDENTIAL_ID`,`OUTGOINGSENDERCREDENTIALS_ID`),
  CONSTRAINT `FKCAA02FE5DF74E053` FOREIGN KEY (`REV`) REFERENCES `revinfo` (`REV`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `outsendercredentialtable_aud`
--

LOCK TABLES `outsendercredentialtable_aud` WRITE;
/*!40000 ALTER TABLE `outsendercredentialtable_aud` DISABLE KEYS */;
/*!40000 ALTER TABLE `outsendercredentialtable_aud` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `outtocredentialtable`
--

DROP TABLE IF EXISTS `outtocredentialtable`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `outtocredentialtable` (
  `CREDENTIAL_ID` bigint(20) NOT NULL DEFAULT '0',
  `OUTGOINGTOCREDENTIALS_ID` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`CREDENTIAL_ID`,`OUTGOINGTOCREDENTIALS_ID`),
  UNIQUE KEY `SYS_C0023009` (`OUTGOINGTOCREDENTIALS_ID`),
  CONSTRAINT `FK42E3DC0EA73C6553` FOREIGN KEY (`OUTGOINGTOCREDENTIALS_ID`) REFERENCES `cxmlcredential` (`ID`),
  CONSTRAINT `FK42E3DC0EAFD1BCFD` FOREIGN KEY (`CREDENTIAL_ID`) REFERENCES `credential` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `outtocredentialtable`
--

LOCK TABLES `outtocredentialtable` WRITE;
/*!40000 ALTER TABLE `outtocredentialtable` DISABLE KEYS */;
/*!40000 ALTER TABLE `outtocredentialtable` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `outtocredentialtable_aud`
--

DROP TABLE IF EXISTS `outtocredentialtable_aud`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `outtocredentialtable_aud` (
  `REV` bigint(20) NOT NULL AUTO_INCREMENT,
  `CREDENTIAL_ID` bigint(20) NOT NULL DEFAULT '0',
  `OUTGOINGTOCREDENTIALS_ID` bigint(20) NOT NULL DEFAULT '0',
  `REVTYPE` decimal(3,0) DEFAULT NULL,
  PRIMARY KEY (`REV`,`CREDENTIAL_ID`,`OUTGOINGTOCREDENTIALS_ID`),
  CONSTRAINT `FK368752DFDF74E053` FOREIGN KEY (`REV`) REFERENCES `revinfo` (`REV`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `outtocredentialtable_aud`
--

LOCK TABLES `outtocredentialtable_aud` WRITE;
/*!40000 ALTER TABLE `outtocredentialtable_aud` DISABLE KEYS */;
/*!40000 ALTER TABLE `outtocredentialtable_aud` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `packagingdimension`
--

DROP TABLE IF EXISTS `packagingdimension`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `packagingdimension` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `VALUE` double DEFAULT NULL,
  `VERSION` bigint(20) DEFAULT NULL,
  `DIMENSIONTYPE_ID` bigint(20) DEFAULT NULL,
  `UNITOFMEASURE_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FKF5AEB54388D89C57` (`DIMENSIONTYPE_ID`),
  KEY `FKF5AEB54397CE9D17` (`UNITOFMEASURE_ID`),
  CONSTRAINT `FKF5AEB54388D89C57` FOREIGN KEY (`DIMENSIONTYPE_ID`) REFERENCES `dimensiontype` (`ID`),
  CONSTRAINT `FKF5AEB54397CE9D17` FOREIGN KEY (`UNITOFMEASURE_ID`) REFERENCES `unitofmeasure` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `packagingdimension`
--

LOCK TABLES `packagingdimension` WRITE;
/*!40000 ALTER TABLE `packagingdimension` DISABLE KEYS */;
/*!40000 ALTER TABLE `packagingdimension` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `packagingtype`
--

DROP TABLE IF EXISTS `packagingtype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `packagingtype` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `DESCRIPTION` varchar(255) DEFAULT NULL,
  `NAME` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `VERSION` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UC_PACKAGINGTYPE_1` (`NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `packagingtype`
--

LOCK TABLES `packagingtype` WRITE;
/*!40000 ALTER TABLE `packagingtype` DISABLE KEYS */;
/*!40000 ALTER TABLE `packagingtype` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `partneridxdeliverylocation`
--

DROP TABLE IF EXISTS `partneridxdeliverylocation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `partneridxdeliverylocation` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `DELIVERYLOCATION` varchar(255) NOT NULL,
  `PARTNERID` varchar(255) NOT NULL,
  `VERSION` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `PARTNERIDXDELVLOC_LOCATION` (`DELIVERYLOCATION`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `partneridxdeliverylocation`
--

LOCK TABLES `partneridxdeliverylocation` WRITE;
/*!40000 ALTER TABLE `partneridxdeliverylocation` DISABLE KEYS */;
/*!40000 ALTER TABLE `partneridxdeliverylocation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `partneridxdeliverylocation_aud`
--

DROP TABLE IF EXISTS `partneridxdeliverylocation_aud`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `partneridxdeliverylocation_aud` (
  `ID` bigint(20) DEFAULT NULL,
  `REV` bigint(20) NOT NULL AUTO_INCREMENT,
  `REVTYPE` decimal(3,0) DEFAULT NULL,
  `DELIVERYLOCATION` varchar(255) DEFAULT NULL,
  `PARTNERID` varchar(255) DEFAULT NULL,
  KEY `FKB9660D8F75D1432` (`REV`),
  CONSTRAINT `FKB9660D8F75D1432` FOREIGN KEY (`REV`) REFERENCES `revinfo` (`REV`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `partneridxdeliverylocation_aud`
--

LOCK TABLES `partneridxdeliverylocation_aud` WRITE;
/*!40000 ALTER TABLE `partneridxdeliverylocation_aud` DISABLE KEYS */;
/*!40000 ALTER TABLE `partneridxdeliverylocation_aud` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `passwordresetentry`
--

DROP TABLE IF EXISTS `passwordresetentry`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `passwordresetentry` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `EXPIRATIONDATE` datetime DEFAULT NULL,
  `GENERATEDKEY` varchar(255) DEFAULT NULL,
  `HASH` varchar(255) DEFAULT NULL,
  `SALT` varchar(255) DEFAULT NULL,
  `VERSION` bigint(20) DEFAULT NULL,
  `USER_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FKC0DA017E4E9ECF4C` (`USER_ID`),
  CONSTRAINT `FKC0DA017E4E9ECF4C` FOREIGN KEY (`USER_ID`) REFERENCES `systemuser` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `passwordresetentry`
--

LOCK TABLES `passwordresetentry` WRITE;
/*!40000 ALTER TABLE `passwordresetentry` DISABLE KEYS */;
/*!40000 ALTER TABLE `passwordresetentry` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `passwordresetentry_aud`
--

DROP TABLE IF EXISTS `passwordresetentry_aud`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `passwordresetentry_aud` (
  `ID` bigint(20) NOT NULL DEFAULT '0',
  `REV` bigint(20) NOT NULL AUTO_INCREMENT,
  `REVTYPE` decimal(3,0) DEFAULT NULL,
  `EXPIRATIONDATE` datetime DEFAULT NULL,
  `GENERATEDKEY` varchar(255) DEFAULT NULL,
  `HASH` varchar(255) DEFAULT NULL,
  `SALT` varchar(255) DEFAULT NULL,
  `USER_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`,`REV`),
  KEY `FKD90D404FDF74E053` (`REV`),
  CONSTRAINT `FKD90D404FDF74E053` FOREIGN KEY (`REV`) REFERENCES `revinfo` (`REV`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `passwordresetentry_aud`
--

LOCK TABLES `passwordresetentry_aud` WRITE;
/*!40000 ALTER TABLE `passwordresetentry_aud` DISABLE KEYS */;
/*!40000 ALTER TABLE `passwordresetentry_aud` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `paymentinformation`
--

DROP TABLE IF EXISTS `paymentinformation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `paymentinformation` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `VERSION` bigint(20) DEFAULT NULL,
  `BILLINGADDRESS_ID` bigint(20) DEFAULT NULL,
  `CARD_ID` bigint(20) DEFAULT NULL,
  `CONTACT_ID` bigint(20) DEFAULT NULL,
  `INVOICESENDMETHOD_ID` bigint(20) DEFAULT NULL,
  `PAYMENTTYPE_ID` bigint(20) DEFAULT NULL,
  `SUMMARY` decimal(1,0) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `INDX_39` (`PAYMENTTYPE_ID`) USING BTREE,
  KEY `INDX_40` (`BILLINGADDRESS_ID`) USING BTREE,
  KEY `PAYINFO_CONTACT_ID` (`CONTACT_ID`) USING BTREE,
  KEY `FKCC5E4766A6E063E3` (`CARD_ID`),
  KEY `FKCC5E4766171AC637` (`INVOICESENDMETHOD_ID`),
  CONSTRAINT `FKCC5E476613E27D12` FOREIGN KEY (`CONTACT_ID`) REFERENCES `person` (`ID`),
  CONSTRAINT `FKCC5E4766171AC637` FOREIGN KEY (`INVOICESENDMETHOD_ID`) REFERENCES `invoicesendmethod` (`ID`),
  CONSTRAINT `FKCC5E47663C027E32` FOREIGN KEY (`BILLINGADDRESS_ID`) REFERENCES `address` (`ID`),
  CONSTRAINT `FKCC5E4766A6E063E3` FOREIGN KEY (`CARD_ID`) REFERENCES `cardinformation` (`ID`),
  CONSTRAINT `FKCC5E4766C3EB5397` FOREIGN KEY (`PAYMENTTYPE_ID`) REFERENCES `paymenttype` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=153624878 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `paymentinformation`
--

LOCK TABLES `paymentinformation` WRITE;
/*!40000 ALTER TABLE `paymentinformation` DISABLE KEYS */;
INSERT INTO `paymentinformation` VALUES (32456,3,NULL,NULL,139772260,31289,31294,0),(3966429,0,NULL,3966430,NULL,NULL,31295,0),(3969675,0,NULL,3969674,NULL,NULL,31295,0),(4149030,2,NULL,NULL,21650509,31289,31294,0),(17705690,1,NULL,NULL,17705689,31289,31294,0),(17708934,0,NULL,NULL,NULL,31289,31294,0),(18808035,0,NULL,NULL,NULL,31289,31294,0),(20159351,0,NULL,NULL,NULL,31289,31294,0),(21643366,0,NULL,NULL,NULL,31289,31294,0),(23097800,0,NULL,NULL,23097798,31289,31294,0),(23540162,0,NULL,NULL,23540160,31289,31294,0),(39301458,0,NULL,NULL,NULL,NULL,31294,1),(39301884,0,NULL,NULL,NULL,NULL,31294,0),(39304899,0,NULL,NULL,NULL,31289,31294,0),(39305174,0,NULL,NULL,NULL,31289,31294,0),(39305177,0,NULL,NULL,NULL,31289,31294,0),(44249748,0,NULL,NULL,NULL,NULL,31294,1),(44249953,0,NULL,NULL,NULL,NULL,31294,0),(53927549,0,NULL,NULL,NULL,NULL,31294,0),(53927563,0,NULL,NULL,NULL,NULL,31294,0),(53927693,0,NULL,NULL,NULL,NULL,31294,0),(53927783,0,NULL,NULL,NULL,NULL,31294,0),(53927790,0,NULL,NULL,NULL,NULL,31294,0),(53927796,0,NULL,NULL,NULL,NULL,31294,0),(53927802,0,NULL,NULL,NULL,NULL,31294,0),(53927808,0,NULL,NULL,NULL,NULL,31294,0),(53927814,0,NULL,NULL,NULL,NULL,31294,0),(53927826,0,NULL,NULL,NULL,NULL,31294,0),(53927831,0,NULL,NULL,NULL,NULL,31294,0),(53927847,0,NULL,NULL,NULL,NULL,31294,0),(53927861,0,NULL,NULL,NULL,NULL,31294,0),(53927881,0,NULL,NULL,NULL,NULL,31294,0),(53927895,0,NULL,NULL,NULL,NULL,31294,0),(53927986,0,NULL,NULL,NULL,NULL,31294,0),(53928000,0,NULL,NULL,NULL,NULL,31294,0),(53928012,0,NULL,NULL,NULL,NULL,31294,0),(75745866,1,NULL,NULL,NULL,NULL,31294,0),(75745888,1,NULL,NULL,NULL,NULL,31294,0),(75745896,1,NULL,NULL,NULL,NULL,31294,0),(75745900,1,NULL,NULL,NULL,NULL,31294,0),(75745933,1,NULL,NULL,NULL,NULL,31294,0),(75745976,1,NULL,NULL,NULL,NULL,31294,0),(75786807,0,NULL,NULL,NULL,NULL,31294,0),(101638447,1,NULL,NULL,NULL,NULL,31294,0),(101638720,0,NULL,NULL,NULL,NULL,31294,0),(102461095,0,NULL,NULL,NULL,NULL,31294,1),(102529105,0,NULL,NULL,NULL,NULL,31294,0),(102554278,0,NULL,NULL,NULL,NULL,31294,0),(106404624,0,NULL,NULL,NULL,NULL,31294,1),(107141140,0,NULL,NULL,NULL,NULL,31294,0),(107181792,0,NULL,NULL,NULL,NULL,31294,0),(107216853,1,NULL,124463059,NULL,NULL,31295,0),(107272559,0,NULL,NULL,NULL,NULL,31294,0),(107272629,0,NULL,NULL,NULL,NULL,31294,0),(110062736,0,NULL,NULL,NULL,NULL,31294,0),(111406655,0,NULL,111406654,NULL,NULL,31295,0),(115385442,0,NULL,NULL,NULL,NULL,31294,0),(115497165,0,NULL,NULL,NULL,NULL,31294,0),(115498366,0,NULL,NULL,NULL,NULL,31294,0),(115628659,0,NULL,NULL,NULL,NULL,31294,1),(115630315,0,NULL,NULL,NULL,NULL,31294,1),(115630416,0,NULL,NULL,NULL,NULL,31294,1),(115630845,0,NULL,NULL,NULL,NULL,31294,1),(115631011,0,NULL,NULL,NULL,NULL,31294,1),(115631335,0,NULL,NULL,NULL,NULL,31294,1),(115631569,0,NULL,NULL,NULL,NULL,31294,1),(115631803,0,NULL,NULL,NULL,NULL,31294,1),(115632000,0,NULL,NULL,NULL,NULL,31294,1),(115633336,0,NULL,NULL,NULL,NULL,31294,1),(115633783,0,NULL,NULL,NULL,NULL,31294,1),(115634092,0,NULL,NULL,NULL,NULL,31294,1),(115634189,0,NULL,NULL,NULL,NULL,31294,1),(115639664,1,NULL,NULL,NULL,NULL,31294,0),(115639783,0,NULL,NULL,NULL,NULL,31294,0),(115650860,0,NULL,NULL,NULL,NULL,31294,0),(115655579,0,NULL,NULL,NULL,NULL,31294,0),(115657281,0,NULL,NULL,NULL,NULL,31294,0),(115672334,0,NULL,NULL,NULL,NULL,31294,0),(116661883,0,NULL,NULL,NULL,NULL,31294,0),(116663631,0,NULL,NULL,NULL,NULL,31294,0),(116664817,0,NULL,NULL,NULL,NULL,31294,0),(116665108,0,NULL,NULL,NULL,NULL,31294,0),(116668411,0,NULL,NULL,NULL,NULL,31294,0),(116668595,0,NULL,NULL,NULL,NULL,31294,0),(116681280,0,NULL,NULL,NULL,NULL,31294,0),(116696651,0,NULL,NULL,NULL,NULL,31294,0),(116697003,0,NULL,NULL,NULL,NULL,31294,0),(116931989,0,NULL,NULL,NULL,NULL,31294,0),(116941205,0,NULL,NULL,NULL,NULL,31294,0),(117456741,0,NULL,NULL,NULL,NULL,31294,0),(117496008,0,NULL,NULL,NULL,NULL,31294,0),(117496875,0,NULL,NULL,NULL,NULL,31294,0),(117497190,0,NULL,NULL,NULL,NULL,31294,0),(117499249,0,NULL,NULL,NULL,NULL,31294,0),(117499467,0,NULL,NULL,NULL,NULL,31294,0),(118799785,0,NULL,NULL,NULL,NULL,31294,0),(118802278,0,NULL,NULL,NULL,NULL,31294,0),(118802551,0,NULL,NULL,NULL,NULL,31294,0),(118805192,0,NULL,NULL,NULL,NULL,31294,0),(119085642,0,NULL,NULL,NULL,NULL,31294,0),(119088566,0,NULL,NULL,NULL,NULL,31294,0),(119089015,0,NULL,NULL,NULL,NULL,31294,0),(119089969,0,NULL,NULL,NULL,NULL,31294,0),(119091287,0,NULL,NULL,NULL,NULL,31294,0),(119111520,0,NULL,NULL,NULL,NULL,31294,0),(119156542,0,NULL,NULL,NULL,NULL,31294,0),(119360324,0,NULL,NULL,NULL,NULL,31294,0),(119396345,0,NULL,NULL,NULL,NULL,31294,0),(123724367,0,NULL,NULL,NULL,NULL,31294,0),(123896444,0,NULL,NULL,NULL,NULL,31294,0),(133003255,0,NULL,NULL,NULL,NULL,31294,0),(133474854,0,NULL,NULL,NULL,NULL,31294,0),(134487592,0,NULL,134487593,NULL,NULL,31295,0),(134845721,0,NULL,NULL,NULL,NULL,31294,0),(135384187,0,NULL,NULL,NULL,NULL,31294,1),(135390372,0,NULL,NULL,NULL,NULL,31294,0),(136006063,0,NULL,NULL,NULL,NULL,31294,1),(139648181,0,NULL,NULL,NULL,NULL,31294,1),(139648703,1,NULL,NULL,NULL,NULL,31294,0),(139648727,0,NULL,NULL,NULL,NULL,31294,0),(139648796,0,NULL,NULL,NULL,NULL,31294,0),(139769962,0,NULL,NULL,NULL,NULL,31294,1),(141079348,0,NULL,NULL,NULL,NULL,31294,0),(141549337,0,NULL,NULL,NULL,NULL,31294,0),(141552302,0,NULL,NULL,NULL,NULL,31294,1),(142686128,0,NULL,142686129,NULL,NULL,31295,0),(142686136,0,NULL,142686135,NULL,NULL,31295,0),(144373446,0,NULL,NULL,NULL,NULL,31294,1),(144478856,0,NULL,NULL,NULL,NULL,31294,0),(146644437,0,NULL,NULL,NULL,NULL,31294,0),(147081165,0,NULL,NULL,NULL,NULL,31294,0),(153624877,0,NULL,NULL,NULL,NULL,31294,0);
/*!40000 ALTER TABLE `paymentinformation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `paymentinformation_aud`
--

DROP TABLE IF EXISTS `paymentinformation_aud`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `paymentinformation_aud` (
  `ID` bigint(20) NOT NULL DEFAULT '0',
  `REV` bigint(20) NOT NULL AUTO_INCREMENT,
  `REVTYPE` decimal(3,0) DEFAULT NULL,
  `SUMMARY` decimal(1,0) DEFAULT NULL,
  `BILLINGADDRESS_ID` bigint(20) DEFAULT NULL,
  `CARD_ID` bigint(20) DEFAULT NULL,
  `CONTACT_ID` bigint(20) DEFAULT NULL,
  `PAYMENTTYPE_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`,`REV`),
  KEY `FK5BAC5237DF74E053` (`REV`),
  CONSTRAINT `FK5BAC5237DF74E053` FOREIGN KEY (`REV`) REFERENCES `revinfo` (`REV`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `paymentinformation_aud`
--

LOCK TABLES `paymentinformation_aud` WRITE;
/*!40000 ALTER TABLE `paymentinformation_aud` DISABLE KEYS */;
/*!40000 ALTER TABLE `paymentinformation_aud` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `paymenttype`
--

DROP TABLE IF EXISTS `paymenttype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `paymenttype` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(255) DEFAULT NULL,
  `VERSION` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `INDX_41` (`NAME`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=31296 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `paymenttype`
--

LOCK TABLES `paymenttype` WRITE;
/*!40000 ALTER TABLE `paymenttype` DISABLE KEYS */;
INSERT INTO `paymenttype` VALUES (31294,'Invoice',0),(31295,'Credit Card',0);
/*!40000 ALTER TABLE `paymenttype` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `paymenttype_aud`
--

DROP TABLE IF EXISTS `paymenttype_aud`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `paymenttype_aud` (
  `ID` bigint(20) NOT NULL DEFAULT '0',
  `REV` bigint(20) NOT NULL AUTO_INCREMENT,
  `REVTYPE` decimal(3,0) DEFAULT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`,`REV`),
  KEY `FK6C867811DF74E053` (`REV`),
  CONSTRAINT `FK6C867811DF74E053` FOREIGN KEY (`REV`) REFERENCES `revinfo` (`REV`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `paymenttype_aud`
--

LOCK TABLES `paymenttype_aud` WRITE;
/*!40000 ALTER TABLE `paymenttype_aud` DISABLE KEYS */;
/*!40000 ALTER TABLE `paymenttype_aud` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pendingsmartsearchdeletion`
--

DROP TABLE IF EXISTS `pendingsmartsearchdeletion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pendingsmartsearchdeletion` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `HASH` bigint(20) DEFAULT NULL,
  `VERSION` bigint(20) DEFAULT NULL,
  `CATALOG_ID` bigint(20) DEFAULT NULL,
  `ITEM_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `PENDINGDELETION_CATALOG_IDX` (`CATALOG_ID`) USING BTREE,
  KEY `PENDINGDELETION_ITEM_IDX` (`ITEM_ID`) USING BTREE,
  CONSTRAINT `FK587840C831B94B57` FOREIGN KEY (`CATALOG_ID`) REFERENCES `catalog` (`ID`),
  CONSTRAINT `FK587840C8D4B669BD` FOREIGN KEY (`ITEM_ID`) REFERENCES `item` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pendingsmartsearchdeletion`
--

LOCK TABLES `pendingsmartsearchdeletion` WRITE;
/*!40000 ALTER TABLE `pendingsmartsearchdeletion` DISABLE KEYS */;
/*!40000 ALTER TABLE `pendingsmartsearchdeletion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pendingworkflowcommand`
--

DROP TABLE IF EXISTS `pendingworkflowcommand`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pendingworkflowcommand` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `COMMANDNAME` varchar(255) DEFAULT NULL,
  `CTX` longblob,
  `EXPIRES` datetime DEFAULT NULL,
  `VERSION` bigint(20) DEFAULT NULL,
  `EXECUTED` decimal(1,0) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `PWCOMMAND_EXECUTED_IDX` (`EXECUTED`) USING BTREE,
  KEY `PWCOMMAND_EXPIRES_IDX` (`EXPIRES`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pendingworkflowcommand`
--

LOCK TABLES `pendingworkflowcommand` WRITE;
/*!40000 ALTER TABLE `pendingworkflowcommand` DISABLE KEYS */;
/*!40000 ALTER TABLE `pendingworkflowcommand` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `periodtype`
--

DROP TABLE IF EXISTS `periodtype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `periodtype` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(255) DEFAULT NULL,
  `VERSION` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=31300 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `periodtype`
--

LOCK TABLES `periodtype` WRITE;
/*!40000 ALTER TABLE `periodtype` DISABLE KEYS */;
INSERT INTO `periodtype` VALUES (31296,'Annually',0),(31297,'Quarterly',0),(31298,'Monthly',0),(31299,'Weekly',0);
/*!40000 ALTER TABLE `periodtype` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `periodtype_aud`
--

DROP TABLE IF EXISTS `periodtype_aud`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `periodtype_aud` (
  `ID` bigint(20) NOT NULL DEFAULT '0',
  `REV` bigint(20) NOT NULL AUTO_INCREMENT,
  `REVTYPE` decimal(3,0) DEFAULT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`,`REV`),
  KEY `FKE00BE50CDF74E053` (`REV`),
  CONSTRAINT `FKE00BE50CDF74E053` FOREIGN KEY (`REV`) REFERENCES `revinfo` (`REV`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `periodtype_aud`
--

LOCK TABLES `periodtype_aud` WRITE;
/*!40000 ALTER TABLE `periodtype_aud` DISABLE KEYS */;
/*!40000 ALTER TABLE `periodtype_aud` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permission`
--

DROP TABLE IF EXISTS `permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permission` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `ALLOWDELETE` decimal(1,0) NOT NULL,
  `ALLOWREAD` decimal(1,0) NOT NULL,
  `ALLOWWRITE` decimal(1,0) NOT NULL,
  `DESCRIPTION` varchar(255) DEFAULT NULL,
  `NAME` varchar(255) NOT NULL,
  `VERSION` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=140899972 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permission`
--

LOCK TABLES `permission` WRITE;
/*!40000 ALTER TABLE `permission` DISABLE KEYS */;
INSERT INTO `permission` VALUES (31027,0,0,1,'Allow the placing of orders','create order',0),(31028,0,1,1,'Allow the employee to edit order','edit order',0),(31029,0,1,0,'Allow the user to view reports','view report',0),(31030,0,1,1,'Allow the user to create reports','add report',0),(31031,0,1,0,'Allow the user to browse the catalogs assigned to them','browse catalog',0),(31032,0,1,1,'Allow the user to cashout of the shopping site','cashout',0),(31033,0,0,1,'Allow the employee to add a credential','add credential',0),(31034,0,1,1,'Allow the employee to edit a credential','edit credential',0),(31035,0,0,1,'Allow the employee to add a vendor catalog','add vendor catalog',0),(31036,0,1,1,'Allow the employee to edit a vendor catalog','edit vendor catalog',0),(31037,0,0,1,'Allow the employee to add a customer catalog','add customer catalog',0),(31038,0,1,1,'Allow the employee to edit a customer catalog','edit customer catalog',0),(31039,0,0,1,'Allow the employee to add a user','add user',0),(31040,0,1,1,'Allow the employee to edit a user','edit user',0),(31041,0,0,1,'Allow the employee to add an account','add account',0),(31042,0,1,1,'Allow the employee to edit an account','edit account',0),(31043,0,0,1,'Allow the employee to add an approval process','add approval process',0),(31044,0,1,1,'Allow the employee to edit an approval process','edit approval process',0),(31045,0,0,1,'Allow the employee to add system data','add system data',0),(31046,0,1,1,'Allow the employee to edit system data','edit system data',0),(31047,0,0,1,'Allow the employee to add cashout page','add cashout page',0),(31048,0,1,1,'Allow the employee to edit cashout page','edit cashout page',0),(31049,0,1,1,'Allow the user to approve orders','approve orders',0),(31050,0,0,1,'Allow the employee to view payment information, including CC numbers','view payment information',0),(1509657,0,1,0,'View the \"Place Special Order\" link in the shopping site.','shopping view special order form',0),(1509658,0,1,0,'View the \"Place Custom Order\" link in the shopping site.','shopping view custom order form',0),(1509659,0,1,0,'View the \"Toner Recycling Program\" link in the shopping site.','shopping view toner recycle form',0),(1509660,0,1,0,'View the \"Rebate Center\" link in the shopping site.','shopping view rebate center',0),(1509661,0,1,0,'Allow the user to backorder items if they are currently out of stock.','shopping order backordered items',0),(1509662,0,1,0,'Allow the user to place an order. A credential without this is a demo.','shopping live credential',0),(1509663,0,1,0,'Allow the user to add items. A credential without this is a \"browse\" credential.','shopping add to cart',0),(1509664,0,1,0,'Allow the user to add items with a price of zero dollars.','shopping add zero price to cart',0),(1509665,0,1,0,'Show the \"Open Market\" banner above the shopping site.','shopping show open market label',0),(1509666,0,1,0,'Allow the user to add and remove personal favorites lists.','shopping maintain user favorites',0),(1509667,0,1,0,'Allow the user to view all orders placed with their account.','shopping view account history',0),(17702751,0,1,0,'Allow the user to span root accounts.','can span root accounts',0),(17702752,0,1,0,'Allow the user to log in to the administration application.','login to admin',0),(17702753,0,1,0,'Allow the user to log in to the ecommerce application.','login to shopping',0),(17702754,0,1,0,'Allow the user to log in to the customer service application.','login to customer service',0),(17702755,0,1,0,'Allow the user to view all orders placed with their credential.','shopping view credential history',0),(17702756,0,1,0,'Permits full visibility of DB results from all Accounts.','view data for all accounts',0),(17702761,0,1,0,'Allows the user to see all claimed tasks.','customer service manager',0),(17709305,0,1,0,'Allows the user to see their issue logs in shopping.','shopping view issue logs',0),(116704860,0,1,0,'Allows the user to view all notifications.','customer service view all messages',0),(116704861,0,1,0,'Allows the user to view Purchase Order notifications.','customer service view message Purchase Order Sent',0),(132357308,0,1,0,'View the \"User Guide\" link in the shopping site.','shopping view user guide',0),(132357309,0,1,0,'View the \"Returns Guide\" link in the shopping site.','shopping view returns guide',0),(132357310,0,1,0,'Allow the user to access task management module in the shopping site.','shopping view tasks',0),(140899967,0,1,0,'Allows the user to view Purchase Order notifications.','customer service view dynamics/jumptrack',0),(140899970,0,1,1,'Allow the placing of request for quote','create RFQ',0),(140899971,0,1,1,'Allow the user to create/edit quote','create/edit quote',0);
/*!40000 ALTER TABLE `permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permission_aud`
--

DROP TABLE IF EXISTS `permission_aud`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permission_aud` (
  `ID` bigint(20) NOT NULL DEFAULT '0',
  `REV` bigint(20) NOT NULL AUTO_INCREMENT,
  `REVTYPE` decimal(3,0) DEFAULT NULL,
  `ALLOWDELETE` decimal(1,0) DEFAULT NULL,
  `ALLOWREAD` decimal(1,0) DEFAULT NULL,
  `ALLOWWRITE` decimal(1,0) DEFAULT NULL,
  `DESCRIPTION` varchar(255) DEFAULT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`,`REV`),
  KEY `FK6E934040DF74E053` (`REV`),
  CONSTRAINT `FK6E934040DF74E053` FOREIGN KEY (`REV`) REFERENCES `revinfo` (`REV`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permission_aud`
--

LOCK TABLES `permission_aud` WRITE;
/*!40000 ALTER TABLE `permission_aud` DISABLE KEYS */;
/*!40000 ALTER TABLE `permission_aud` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permissionxuser`
--

DROP TABLE IF EXISTS `permissionxuser`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permissionxuser` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `EXPIRATIONDATE` datetime DEFAULT NULL,
  `STARTDATE` datetime DEFAULT NULL,
  `VERSION` bigint(20) DEFAULT NULL,
  `PERMISSION_ID` bigint(20) DEFAULT NULL,
  `USER_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `PERMISSIONXUSER_PERMISSION_ID` (`PERMISSION_ID`) USING BTREE,
  KEY `PERMISSIONXUSER_USER_ID` (`USER_ID`) USING BTREE,
  CONSTRAINT `FK637BAFB44E9ECF4C` FOREIGN KEY (`USER_ID`) REFERENCES `systemuser` (`ID`),
  CONSTRAINT `FK637BAFB4BCDF29FD` FOREIGN KEY (`PERMISSION_ID`) REFERENCES `permission` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=148365466 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permissionxuser`
--

LOCK TABLES `permissionxuser` WRITE;
/*!40000 ALTER TABLE `permissionxuser` DISABLE KEYS */;
INSERT INTO `permissionxuser` VALUES (1544004,NULL,NULL,0,31027,33791),(1544005,NULL,NULL,0,31027,55780),(3950085,NULL,NULL,0,31040,33791),(3950086,NULL,NULL,0,31049,33791),(3950087,NULL,NULL,0,31027,33791),(3950088,NULL,NULL,0,31043,33791),(3950089,NULL,NULL,0,31028,33791),(13686127,NULL,NULL,0,31027,719670),(17702896,NULL,NULL,0,17702756,55780),(17702897,NULL,NULL,0,17702751,55780),(17703366,NULL,NULL,0,17702756,17702904),(101638727,NULL,NULL,0,31050,55780),(101638728,NULL,NULL,0,31029,55780),(102381129,NULL,NULL,0,31040,55780),(104865484,NULL,NULL,0,17702751,719670),(107137104,NULL,NULL,0,17702751,75494),(111109824,NULL,NULL,0,1509662,55780),(115499946,NULL,NULL,0,31027,33714),(115499947,NULL,NULL,0,31032,33714),(115499948,NULL,NULL,0,17702756,33714),(116249926,NULL,NULL,0,17702755,55780),(116702742,NULL,NULL,0,17702761,33791),(116709730,NULL,NULL,0,116704860,55780),(117384811,NULL,NULL,0,116704860,75494),(117384813,NULL,NULL,0,116704860,770070),(117398477,NULL,NULL,0,116704860,33791),(117542515,NULL,NULL,0,116704860,719670),(117550891,NULL,NULL,0,116704860,33714),(120138760,NULL,NULL,0,17702761,719670),(124488667,NULL,NULL,0,31028,55780),(132477203,NULL,NULL,0,31042,33714),(132477559,NULL,NULL,0,31041,33714),(132477560,NULL,NULL,0,31033,33714),(135630574,NULL,NULL,0,17702754,719670),(135702871,NULL,NULL,0,31050,719670),(142072312,NULL,NULL,0,17702761,55780),(148365465,NULL,NULL,0,31028,33714);
/*!40000 ALTER TABLE `permissionxuser` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permissionxuser_aud`
--

DROP TABLE IF EXISTS `permissionxuser_aud`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permissionxuser_aud` (
  `ID` bigint(20) NOT NULL DEFAULT '0',
  `REV` bigint(20) NOT NULL AUTO_INCREMENT,
  `REVTYPE` decimal(3,0) DEFAULT NULL,
  `EXPIRATIONDATE` datetime DEFAULT NULL,
  `STARTDATE` datetime DEFAULT NULL,
  `PERMISSION_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`,`REV`),
  KEY `FKDA20E385DF74E053` (`REV`),
  CONSTRAINT `FKDA20E385DF74E053` FOREIGN KEY (`REV`) REFERENCES `revinfo` (`REV`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permissionxuser_aud`
--

LOCK TABLES `permissionxuser_aud` WRITE;
/*!40000 ALTER TABLE `permissionxuser_aud` DISABLE KEYS */;
/*!40000 ALTER TABLE `permissionxuser_aud` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `person`
--

DROP TABLE IF EXISTS `person`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `person` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `EMAIL` varchar(255) DEFAULT NULL,
  `FIRSTNAME` varchar(255) DEFAULT NULL,
  `LASTNAME` varchar(255) DEFAULT NULL,
  `MIDDLEINITIAL` varchar(255) DEFAULT NULL,
  `PREFIX` varchar(255) DEFAULT NULL,
  `SUFFIX` varchar(255) DEFAULT NULL,
  `VERSION` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=139772261 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `person`
--

LOCK TABLES `person` WRITE;
/*!40000 ALTER TABLE `person` DISABLE KEYS */;
INSERT INTO `person` VALUES (12345,'teamphoenix@americanproduct.com','Super','User','M','Mr.','Jr.',0),(33715,'juanitarichardson@americanproduct.com','Juanita','Richardson',NULL,NULL,NULL,52),(33792,'ravenlevy@americanproduct.com','Raven','Levy',NULL,NULL,NULL,23),(55781,'donaldmangana@americanproduct.com','Don','Mangana',NULL,NULL,NULL,142),(75496,'tequiladouglas@americanproduct.com','Tequila','Douglas',NULL,'Ms. ',NULL,28),(75530,'aceroj@cintas.com','Alfredo ','Acero',NULL,NULL,NULL,1),(75545,'aaronescobar@americanproduct.com','Aaron','Escobar',NULL,'Mr.',NULL,11),(719575,'Evadinion@americanproduct.com','Eva','Dinion',NULL,NULL,NULL,7),(719664,'cykennedy@americanproduct.com','Cy','Kennedy',NULL,NULL,NULL,2),(719673,'evadinion@americanproduct.com','Eva','Dinion',NULL,NULL,NULL,89),(719748,'rassameesayavong@americanproduct.com','Raz','Sayavong',NULL,NULL,NULL,1),(770072,'darlisaking@americanproduct.com','Darlisa','King',NULL,NULL,NULL,6),(3966433,'evadinion@americanproduct.com','Fail CC User','TEST USER',NULL,NULL,NULL,1),(14178561,'kylewashington@americanproduct.com','Kyle','Washington',NULL,'Mr.',NULL,2),(14178578,'nikkibradley@americanproduct.com','Nikki','Bradley',NULL,NULL,NULL,2),(14178613,'techsupport@americanproduct.com','Technical','Department',NULL,NULL,NULL,1),(17702901,'priscillavillarasa@americanproduct.com','Priscilla','Villarasa',NULL,'Ms.',NULL,4),(17702905,'stephanievasquez@americanproduct.com','Stephanie','Vasquez',NULL,'Ms.',NULL,2),(17705689,'judygoda@americanproduct.com','Judy','Goda',NULL,NULL,NULL,0),(21650509,'ar@americanproduct.com','Accounts','Receivable',NULL,NULL,NULL,0),(23097798,'evadinion@americanproduct.com','Eva','Dinion',NULL,NULL,NULL,0),(23540160,'ar@americanproduct.com','Accounting','Department',NULL,NULL,NULL,0),(139772260,NULL,NULL,NULL,NULL,NULL,NULL,0);
/*!40000 ALTER TABLE `person` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `person_aud`
--

DROP TABLE IF EXISTS `person_aud`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `person_aud` (
  `ID` bigint(20) NOT NULL DEFAULT '0',
  `REV` bigint(20) NOT NULL AUTO_INCREMENT,
  `REVTYPE` decimal(3,0) DEFAULT NULL,
  `EMAIL` varchar(255) DEFAULT NULL,
  `FIRSTNAME` varchar(255) DEFAULT NULL,
  `LASTNAME` varchar(255) DEFAULT NULL,
  `MIDDLEINITIAL` varchar(255) DEFAULT NULL,
  `PREFIX` varchar(255) DEFAULT NULL,
  `SUFFIX` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`,`REV`),
  KEY `FK9F49F2C6DF74E053` (`REV`),
  CONSTRAINT `FK9F49F2C6DF74E053` FOREIGN KEY (`REV`) REFERENCES `revinfo` (`REV`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `person_aud`
--

LOCK TABLES `person_aud` WRITE;
/*!40000 ALTER TABLE `person_aud` DISABLE KEYS */;
/*!40000 ALTER TABLE `person_aud` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phonenumber`
--

DROP TABLE IF EXISTS `phonenumber`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phonenumber` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `AREACODE` varchar(255) DEFAULT NULL,
  `COUNTRYCODE` varchar(255) DEFAULT NULL,
  `EXCHANGE` varchar(255) DEFAULT NULL,
  `EXTENSION` varchar(255) DEFAULT NULL,
  `LINENUMBER` varchar(255) DEFAULT NULL,
  `VERSION` bigint(20) DEFAULT NULL,
  `ACCOUNT_ID` bigint(20) DEFAULT NULL,
  `PERSON_ID` bigint(20) DEFAULT NULL,
  `TYPE_ID` bigint(20) DEFAULT NULL,
  `VENDOR_ID` bigint(20) DEFAULT NULL,
  `VALUE` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `INDX_42` (`ACCOUNT_ID`) USING BTREE,
  KEY `PHONENUMBER_PERSON_ID` (`PERSON_ID`) USING BTREE,
  KEY `PHONENUMBER_TYPE_ID` (`TYPE_ID`) USING BTREE,
  KEY `PHONENUMBER_VENDOR_ID` (`VENDOR_ID`) USING BTREE,
  CONSTRAINT `FK1C4E6237185A22D7` FOREIGN KEY (`ACCOUNT_ID`) REFERENCES `account` (`ID`),
  CONSTRAINT `FK1C4E62373E66BDBD` FOREIGN KEY (`PERSON_ID`) REFERENCES `person` (`ID`),
  CONSTRAINT `FK1C4E6237D50C94DD` FOREIGN KEY (`VENDOR_ID`) REFERENCES `vendor` (`ID`),
  CONSTRAINT `FK1C4E6237D68B89AE` FOREIGN KEY (`TYPE_ID`) REFERENCES `phonenumbertype` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phonenumber`
--

LOCK TABLES `phonenumber` WRITE;
/*!40000 ALTER TABLE `phonenumber` DISABLE KEYS */;
/*!40000 ALTER TABLE `phonenumber` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phonenumber_aud`
--

DROP TABLE IF EXISTS `phonenumber_aud`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phonenumber_aud` (
  `ID` bigint(20) NOT NULL DEFAULT '0',
  `REV` bigint(20) NOT NULL AUTO_INCREMENT,
  `REVTYPE` decimal(3,0) DEFAULT NULL,
  `AREACODE` varchar(255) DEFAULT NULL,
  `COUNTRYCODE` varchar(255) DEFAULT NULL,
  `EXCHANGE` varchar(255) DEFAULT NULL,
  `EXTENSION` varchar(255) DEFAULT NULL,
  `LINENUMBER` varchar(255) DEFAULT NULL,
  `ACCOUNT_ID` bigint(20) DEFAULT NULL,
  `PERSON_ID` bigint(20) DEFAULT NULL,
  `TYPE_ID` bigint(20) DEFAULT NULL,
  `VENDOR_ID` bigint(20) DEFAULT NULL,
  `VALUE` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`,`REV`),
  KEY `FKAD809C88DF74E053` (`REV`),
  CONSTRAINT `FKAD809C88DF74E053` FOREIGN KEY (`REV`) REFERENCES `revinfo` (`REV`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phonenumber_aud`
--

LOCK TABLES `phonenumber_aud` WRITE;
/*!40000 ALTER TABLE `phonenumber_aud` DISABLE KEYS */;
/*!40000 ALTER TABLE `phonenumber_aud` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phonenumbertype`
--

DROP TABLE IF EXISTS `phonenumbertype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phonenumbertype` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `VERSION` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UC_PHONENUMBERTYPE_1` (`NAME`)
) ENGINE=InnoDB AUTO_INCREMENT=31280 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phonenumbertype`
--

LOCK TABLES `phonenumbertype` WRITE;
/*!40000 ALTER TABLE `phonenumbertype` DISABLE KEYS */;
INSERT INTO `phonenumbertype` VALUES (31276,'Home',0),(31277,'Work',0),(31278,'Cell',0),(31279,'Fax',0);
/*!40000 ALTER TABLE `phonenumbertype` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phonenumbertype_aud`
--

DROP TABLE IF EXISTS `phonenumbertype_aud`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phonenumbertype_aud` (
  `ID` bigint(20) NOT NULL DEFAULT '0',
  `REV` bigint(20) NOT NULL AUTO_INCREMENT,
  `REVTYPE` decimal(3,0) DEFAULT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`,`REV`),
  KEY `FKD70F2F62DF74E053` (`REV`),
  CONSTRAINT `FKD70F2F62DF74E053` FOREIGN KEY (`REV`) REFERENCES `revinfo` (`REV`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phonenumbertype_aud`
--

LOCK TABLES `phonenumbertype_aud` WRITE;
/*!40000 ALTER TABLE `phonenumbertype_aud` DISABLE KEYS */;
/*!40000 ALTER TABLE `phonenumbertype_aud` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pick`
--

DROP TABLE IF EXISTS `pick`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pick` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `APDORDERNUMBER` varchar(255) DEFAULT NULL,
  `CUSTOMERPONUMBER` varchar(255) DEFAULT NULL,
  `INVENTORYID` varchar(255) DEFAULT NULL,
  `ISSHIPPED` decimal(1,0) DEFAULT NULL,
  `LINEREFERENCE` varchar(255) DEFAULT NULL,
  `ORDERNUMBER` varchar(255) DEFAULT NULL,
  `QTYBO` decimal(19,2) DEFAULT NULL,
  `QTYTOPICK` decimal(19,2) DEFAULT NULL,
  `SHIPPERID` varchar(255) NOT NULL,
  `VERSION` bigint(20) DEFAULT NULL,
  `LINEITEM_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK25F441B7777FDD` (`LINEITEM_ID`),
  CONSTRAINT `FK25F441B7777FDD` FOREIGN KEY (`LINEITEM_ID`) REFERENCES `lineitem` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pick`
--

LOCK TABLES `pick` WRITE;
/*!40000 ALTER TABLE `pick` DISABLE KEYS */;
/*!40000 ALTER TABLE `pick` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ponumber`
--

DROP TABLE IF EXISTS `ponumber`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ponumber` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `EXPIRATIONDATE` datetime DEFAULT NULL,
  `STARTDATE` datetime DEFAULT NULL,
  `VALUE` varchar(255) DEFAULT NULL,
  `VERSION` bigint(20) DEFAULT NULL,
  `TYPE_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `INDX_43` (`TYPE_ID`) USING BTREE,
  KEY `PO_NUM_IDX` (`VALUE`) USING BTREE,
  CONSTRAINT `FKF1EF7E484D174BA5` FOREIGN KEY (`TYPE_ID`) REFERENCES `ponumbertype` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=150807177 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ponumber`
--

LOCK TABLES `ponumber` WRITE;
/*!40000 ALTER TABLE `ponumber` DISABLE KEYS */;
INSERT INTO `ponumber` VALUES (32565,NULL,NULL,'1234',0,31257),(1452885,NULL,NULL,'123456',0,31259),(75371004,NULL,NULL,'BlanketPO 4-21-14',0,31259),(115078668,'2014-10-31 00:00:00','2014-10-01 00:00:00','100114',2,31259),(115078694,'2014-10-31 00:00:00','2014-10-01 00:00:00','100114',3,31259),(116658103,'2014-10-31 00:00:00','2014-10-01 00:00:00','100114',1,31259),(116658146,'2014-10-31 00:00:00','2014-10-01 00:00:00','100114',1,31259),(124510295,NULL,NULL,'TOYOTA',0,31259),(135459162,NULL,NULL,'TOYOTA',0,31259),(135635233,'2014-10-31 00:00:00','2014-10-01 00:00:00','144916',5,31259),(135635235,'2014-10-31 00:00:00','2014-10-01 00:00:00','144916',1,31259),(140901758,'2014-10-31 00:00:00','2014-10-01 00:00:00','100114 ',3,31259),(141454991,'2014-10-31 00:00:00','2014-09-30 00:00:00','144916',1,31259),(141490622,'2014-10-31 00:00:00','2014-10-01 00:00:00','100114',0,31259),(141490727,'2014-10-31 00:00:00','2014-10-01 00:00:00','100114',0,31259),(141840265,'2014-10-31 00:00:00','2014-10-01 00:00:00','144916',0,31259),(143981808,NULL,NULL,'pepsi',0,31259),(146132137,'2014-11-30 00:00:00','2014-11-01 00:00:00','146014',0,31259),(146132578,'2014-11-30 00:00:00','2014-11-01 00:00:00','146014',0,31259),(146133260,'2014-11-30 00:00:00','2014-11-01 00:00:00','146014',0,31259),(146133465,'2014-11-30 00:00:00','2014-11-01 00:00:00','146014',0,31259),(150806657,'2014-12-31 00:00:00','2014-12-01 00:00:00','146762',0,31259),(150806721,'2014-12-31 00:00:00','2014-12-01 00:00:00','146762',0,31259),(150806766,'2014-12-31 00:00:00','2014-12-01 00:00:00','146762',0,31259),(150807176,'2014-12-31 00:00:00','2014-12-01 00:00:00','146762',0,31259);
/*!40000 ALTER TABLE `ponumber` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ponumber_customerorder`
--

DROP TABLE IF EXISTS `ponumber_customerorder`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ponumber_customerorder` (
  `PONUMBERS_ID` bigint(20) NOT NULL DEFAULT '0',
  `ORDERS_ID` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`PONUMBERS_ID`,`ORDERS_ID`),
  KEY `POCUSTORDER_ORDERS_IDX` (`ORDERS_ID`) USING BTREE,
  KEY `POCUSTORDER_PONUMBER_IDX` (`PONUMBERS_ID`) USING BTREE,
  CONSTRAINT `FK27F6343924BC7FA` FOREIGN KEY (`PONUMBERS_ID`) REFERENCES `ponumber` (`ID`),
  CONSTRAINT `FK27F63439A806D542` FOREIGN KEY (`ORDERS_ID`) REFERENCES `customerorder` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ponumber_customerorder`
--

LOCK TABLES `ponumber_customerorder` WRITE;
/*!40000 ALTER TABLE `ponumber_customerorder` DISABLE KEYS */;
/*!40000 ALTER TABLE `ponumber_customerorder` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ponumbertype`
--

DROP TABLE IF EXISTS `ponumbertype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ponumbertype` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `VERSION` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UC_PONUMBERTYPE_1` (`NAME`)
) ENGINE=InnoDB AUTO_INCREMENT=31260 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ponumbertype`
--

LOCK TABLES `ponumbertype` WRITE;
/*!40000 ALTER TABLE `ponumbertype` DISABLE KEYS */;
INSERT INTO `ponumbertype` VALUES (31257,'APD',0),(31258,'Customer',0),(31259,'blanket',0);
/*!40000 ALTER TABLE `ponumbertype` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ponumbertype_aud`
--

DROP TABLE IF EXISTS `ponumbertype_aud`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ponumbertype_aud` (
  `ID` bigint(20) NOT NULL DEFAULT '0',
  `REV` bigint(20) NOT NULL AUTO_INCREMENT,
  `REVTYPE` decimal(3,0) DEFAULT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`,`REV`),
  KEY `FKD05FAA73DF74E053` (`REV`),
  CONSTRAINT `FKD05FAA73DF74E053` FOREIGN KEY (`REV`) REFERENCES `revinfo` (`REV`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ponumbertype_aud`
--

LOCK TABLES `ponumbertype_aud` WRITE;
/*!40000 ALTER TABLE `ponumbertype_aud` DISABLE KEYS */;
/*!40000 ALTER TABLE `ponumbertype_aud` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pricingtype`
--

DROP TABLE IF EXISTS `pricingtype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pricingtype` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(255) DEFAULT NULL,
  `PARAMETERREGEX` varchar(255) NOT NULL,
  `VERSION` bigint(20) DEFAULT NULL,
  `USECEILING` decimal(1,0) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=141508154 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pricingtype`
--

LOCK TABLES `pricingtype` WRITE;
/*!40000 ALTER TABLE `pricingtype` DISABLE KEYS */;
INSERT INTO `pricingtype` VALUES (31247,'Cost Plus Margin %','^([0-9]+[\\.]?[0-9]*|[0-9]*\\.[0-9]+)[%]?$',0,1),(31248,'List Less Discount %, Floor at Margin %','^([0-9]+[\\.]?[0-9]*|[0-9]*\\.[0-9]+)[%]?[ ]*,[ ]*([0-9]+[\\.]?[0-9]*|[0-9]*\\.[0-9]+)[%]?$',0,1),(31249,'Fixed Price','^(\\$)?[0-9]+(\\.)?[0-9]*$',0,1),(1440005,'Cost Plus Margin %, Shipping Included','^([0-9]+[\\.]?[0-9]*|[0-9]*\\.[0-9]+)[%]?$',0,1),(1440006,'List Less Discount %, Floor at Margin %, Shipping Included','^([0-9]+[\\.]?[0-9]*|[0-9]*\\.[0-9]+)[%]?[ ]*,[ ]*([0-9]+[\\.]?[0-9]*|[0-9]*\\.[0-9]+)[%]?$',0,1),(1440007,'GSA Less','^([0-9]+[\\.]?[0-9]*|[0-9]*\\.[0-9]+)[%]?$',0,1),(1440008,'GSA Less, Shipping Included','^([0-9]+[\\.]?[0-9]*|[0-9]*\\.[0-9]+)[%]?$',0,1),(1440009,'AF Less','^([0-9]+[\\.]?[0-9]*|[0-9]*\\.[0-9]+)[%]?$',0,1),(1440010,'AF Less, Shipping Included','^([0-9]+[\\.]?[0-9]*|[0-9]*\\.[0-9]+)[%]?$',0,1),(1440011,'Street Less','^([0-9]+[\\.]?[0-9]*|[0-9]*\\.[0-9]+)[%]?$',0,1),(1440012,'Street Less, Shipping Included','^([0-9]+[\\.]?[0-9]*|[0-9]*\\.[0-9]+)[%]?$',0,1),(141508136,'Cost Plus Markup %','^([0-9]+[\\.]?[0-9]*|[0-9]*\\.[0-9]+)[%]?$',0,1),(141508137,'Cost Plus Markup %, Shipping Included','^([0-9]+[\\.]?[0-9]*|[0-9]*\\.[0-9]+)[%]?$',0,1),(141508138,'List Less Markdown %, Floor at Margin %','^([0-9]+[\\.]?[0-9]*|[0-9]*\\.[0-9]+)[%]?[ ]*,[ ]*([0-9]+[\\.]?[0-9]*|[0-9]*\\.[0-9]+)[%]?$',0,1),(141508139,'List Less Markdown %, Floor at Margin %, Shipping Included','^([0-9]+[\\.]?[0-9]*|[0-9]*\\.[0-9]+)[%]?[ ]*,[ ]*([0-9]+[\\.]?[0-9]*|[0-9]*\\.[0-9]+)[%]?$',0,1),(141508140,'Cost Plus Margin %','^([0-9]+[\\.]?[0-9]*|[0-9]*\\.[0-9]+)[%]?$',0,0),(141508141,'Cost Plus Margin %, Shipping Included','^([0-9]+[\\.]?[0-9]*|[0-9]*\\.[0-9]+)[%]?$',0,0),(141508142,'Cost Plus Markup %','^([0-9]+[\\.]?[0-9]*|[0-9]*\\.[0-9]+)[%]?$',0,0),(141508143,'Cost Plus Markup %, Shipping Included','^([0-9]+[\\.]?[0-9]*|[0-9]*\\.[0-9]+)[%]?$',0,0),(141508144,'List Less Discount %, Floor at Margin %','^([0-9]+[\\.]?[0-9]*|[0-9]*\\.[0-9]+)[%]?[ ]*,[ ]*([0-9]+[\\.]?[0-9]*|[0-9]*\\.[0-9]+)[%]?$',0,0),(141508145,'List Less Discount %, Floor at Margin %, Shipping Included','^([0-9]+[\\.]?[0-9]*|[0-9]*\\.[0-9]+)[%]?[ ]*,[ ]*([0-9]+[\\.]?[0-9]*|[0-9]*\\.[0-9]+)[%]?$',0,0),(141508146,'List Less Markdown %, Floor at Margin %','^([0-9]+[\\.]?[0-9]*|[0-9]*\\.[0-9]+)[%]?[ ]*,[ ]*([0-9]+[\\.]?[0-9]*|[0-9]*\\.[0-9]+)[%]?$',0,0),(141508147,'List Less Markdown %, Floor at Margin %, Shipping Included','^([0-9]+[\\.]?[0-9]*|[0-9]*\\.[0-9]+)[%]?[ ]*,[ ]*([0-9]+[\\.]?[0-9]*|[0-9]*\\.[0-9]+)[%]?$',0,0),(141508148,'Street Less Markdown','^([0-9]+[\\.]?[0-9]*|[0-9]*\\.[0-9]+)[%]?$',0,1),(141508149,'Street Less Markdown, Shipping Included','^([0-9]+[\\.]?[0-9]*|[0-9]*\\.[0-9]+)[%]?$',0,1),(141508150,'Street Less','^([0-9]+[\\.]?[0-9]*|[0-9]*\\.[0-9]+)[%]?$',0,0),(141508151,'Street Less, Shipping Included','^([0-9]+[\\.]?[0-9]*|[0-9]*\\.[0-9]+)[%]?$',0,0),(141508152,'Street Less Markdown','^([0-9]+[\\.]?[0-9]*|[0-9]*\\.[0-9]+)[%]?$',0,0),(141508153,'Street Less Markdown, Shipping Included','^([0-9]+[\\.]?[0-9]*|[0-9]*\\.[0-9]+)[%]?$',0,0);
/*!40000 ALTER TABLE `pricingtype` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pricingtype_aud`
--

DROP TABLE IF EXISTS `pricingtype_aud`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pricingtype_aud` (
  `ID` bigint(20) NOT NULL DEFAULT '0',
  `REV` bigint(20) NOT NULL AUTO_INCREMENT,
  `REVTYPE` decimal(3,0) DEFAULT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `PARAMETERREGEX` varchar(255) DEFAULT NULL,
  `USECEILING` decimal(1,0) DEFAULT NULL,
  PRIMARY KEY (`ID`,`REV`),
  KEY `FK33925D1DF74E053` (`REV`),
  CONSTRAINT `FK33925D1DF74E053` FOREIGN KEY (`REV`) REFERENCES `revinfo` (`REV`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pricingtype_aud`
--

LOCK TABLES `pricingtype_aud` WRITE;
/*!40000 ALTER TABLE `pricingtype_aud` DISABLE KEYS */;
/*!40000 ALTER TABLE `pricingtype_aud` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `process`
--

DROP TABLE IF EXISTS `process`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `process` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(255) DEFAULT NULL,
  `VERSION` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `process`
--

LOCK TABLES `process` WRITE;
/*!40000 ALTER TABLE `process` DISABLE KEYS */;
/*!40000 ALTER TABLE `process` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `processconfiguration`
--

DROP TABLE IF EXISTS `processconfiguration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `processconfiguration` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `VERSION` bigint(20) DEFAULT NULL,
  `ACCOUNT_ID` bigint(20) DEFAULT NULL,
  `CREDENTIAL_ID` bigint(20) DEFAULT NULL,
  `PROCESS_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UK_PROCESSCONFIGURATION_1` (`ID`,`ACCOUNT_ID`),
  UNIQUE KEY `UK_PROCESSCONFIGURATION_2` (`ID`,`CREDENTIAL_ID`),
  KEY `FKDAF5E6E7185A22D7` (`ACCOUNT_ID`),
  KEY `FKDAF5E6E7AFD1BCFD` (`CREDENTIAL_ID`),
  KEY `FKDAF5E6E7951FA397` (`PROCESS_ID`),
  CONSTRAINT `FKDAF5E6E7185A22D7` FOREIGN KEY (`ACCOUNT_ID`) REFERENCES `account` (`ID`),
  CONSTRAINT `FKDAF5E6E7951FA397` FOREIGN KEY (`PROCESS_ID`) REFERENCES `process` (`ID`),
  CONSTRAINT `FKDAF5E6E7AFD1BCFD` FOREIGN KEY (`CREDENTIAL_ID`) REFERENCES `credential` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `processconfiguration`
--

LOCK TABLES `processconfiguration` WRITE;
/*!40000 ALTER TABLE `processconfiguration` DISABLE KEYS */;
/*!40000 ALTER TABLE `processconfiguration` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `processvariable`
--

DROP TABLE IF EXISTS `processvariable`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `processvariable` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `VALUE` varchar(255) DEFAULT NULL,
  `VERSION` bigint(20) DEFAULT NULL,
  `PROCESSCONFIGURATION_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UK_PROCESSVARIABLE_1` (`PROCESSCONFIGURATION_ID`,`NAME`),
  CONSTRAINT `FKDD2D2DEB4F9C731D` FOREIGN KEY (`PROCESSCONFIGURATION_ID`) REFERENCES `processconfiguration` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `processvariable`
--

LOCK TABLES `processvariable` WRITE;
/*!40000 ALTER TABLE `processvariable` DISABLE KEYS */;
/*!40000 ALTER TABLE `processvariable` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `punchoutsession`
--

DROP TABLE IF EXISTS `punchoutsession`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `punchoutsession` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `BROWSERFORMPOSTURL` varchar(4000) DEFAULT NULL,
  `BUYERCOOKIE` varchar(255) DEFAULT NULL,
  `INITTIME` datetime DEFAULT NULL,
  `SESSIONTOKEN` varchar(255) DEFAULT NULL,
  `SYSTEMUSERLOGINNAME` varchar(255) DEFAULT NULL,
  `VERSION` bigint(20) DEFAULT NULL,
  `CREDENTIAL_ID` bigint(20) DEFAULT NULL,
  `ACCNTCREDUSER_ID` bigint(20) DEFAULT NULL,
  `CUSTOMERORDER_ID` bigint(20) DEFAULT NULL,
  `BROWSERFORMPOSTTARGETFRAME` varchar(255) DEFAULT NULL,
  `OPERATION` varchar(10) NOT NULL DEFAULT 'create',
  `ENDTIME` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `PUNCHSESS_BUYERCOOKIE` (`BUYERCOOKIE`) USING BTREE,
  KEY `PUNCHSESS_SESSIONTOKEN` (`SESSIONTOKEN`) USING BTREE,
  KEY `FKDC86EEF6A9E5B5D8` (`ACCNTCREDUSER_ID`),
  KEY `FKDC86EEF626CAE17` (`CUSTOMERORDER_ID`),
  CONSTRAINT `FKDC86EEF626CAE17` FOREIGN KEY (`CUSTOMERORDER_ID`) REFERENCES `customerorder` (`ID`),
  CONSTRAINT `FKDC86EEF6A9E5B5D8` FOREIGN KEY (`ACCNTCREDUSER_ID`) REFERENCES `accountxcredentialxuser` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `punchoutsession`
--

LOCK TABLES `punchoutsession` WRITE;
/*!40000 ALTER TABLE `punchoutsession` DISABLE KEYS */;
/*!40000 ALTER TABLE `punchoutsession` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `receiveditem`
--

DROP TABLE IF EXISTS `receiveditem`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `receiveditem` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `ARRIVALDATE` datetime DEFAULT NULL,
  `QUANTITY` bigint(20) DEFAULT NULL,
  `VERSION` bigint(20) DEFAULT NULL,
  `ORDEREDITEMXSHIPMENT_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FKDC0054B45F51433D` (`ORDEREDITEMXSHIPMENT_ID`),
  CONSTRAINT `FKDC0054B45F51433D` FOREIGN KEY (`ORDEREDITEMXSHIPMENT_ID`) REFERENCES `ordereditemxshipment` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `receiveditem`
--

LOCK TABLES `receiveditem` WRITE;
/*!40000 ALTER TABLE `receiveditem` DISABLE KEYS */;
/*!40000 ALTER TABLE `receiveditem` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `returnorder`
--

DROP TABLE IF EXISTS `returnorder`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `returnorder` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `RANUMBER` varchar(255) DEFAULT NULL,
  `RESTOCKINGFEE` decimal(19,2) DEFAULT NULL,
  `STATUS` varchar(255) DEFAULT NULL,
  `VERSION` bigint(20) DEFAULT NULL,
  `ORDER_ID` bigint(20) DEFAULT NULL,
  `CREATEDDATE` datetime DEFAULT NULL,
  `RECONCILEDDATE` datetime DEFAULT NULL,
  `CLOSEDATE` datetime DEFAULT NULL,
  `RESTOCKINGFEE_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `INDX_44` (`STATUS`) USING BTREE,
  KEY `RETURNORDER_CUSTORDER_IDX` (`ORDER_ID`) USING BTREE,
  KEY `RETURN_CLOSEDATE_IDX` (`CLOSEDATE`) USING BTREE,
  KEY `RETURN_CREATEDATE_IDX` (`CREATEDDATE`) USING BTREE,
  KEY `RETURN_RECONCILEDATE_IDX` (`RECONCILEDDATE`) USING BTREE,
  KEY `FK9F0BFDE6A6D81FD` (`RESTOCKINGFEE_ID`),
  CONSTRAINT `FK9F0BFDE6A6D81FD` FOREIGN KEY (`RESTOCKINGFEE_ID`) REFERENCES `lineitem` (`ID`),
  CONSTRAINT `FK9F0BFDE8E1EC39` FOREIGN KEY (`ORDER_ID`) REFERENCES `customerorder` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `returnorder`
--

LOCK TABLES `returnorder` WRITE;
/*!40000 ALTER TABLE `returnorder` DISABLE KEYS */;
/*!40000 ALTER TABLE `returnorder` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `revinfo`
--

DROP TABLE IF EXISTS `revinfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `revinfo` (
  `REV` bigint(20) NOT NULL AUTO_INCREMENT,
  `REVTSTMP` bigint(20) DEFAULT NULL,
  `REVISIONDATE` datetime DEFAULT NULL,
  `USERNAME` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`REV`),
  KEY `INDX_45` (`REVISIONDATE`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `revinfo`
--

LOCK TABLES `revinfo` WRITE;
/*!40000 ALTER TABLE `revinfo` DISABLE KEYS */;
INSERT INTO `revinfo` VALUES (156609526,1447339347728,'2015-11-12 09:42:28','admin'),(156609528,1447339398283,'2015-11-12 09:43:18','admin');
/*!40000 ALTER TABLE `revinfo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role`
--

DROP TABLE IF EXISTS `role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `ISACTIVE` decimal(1,0) NOT NULL,
  `NAME` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `VERSION` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ROLENAME_UC` (`NAME`)
) ENGINE=InnoDB AUTO_INCREMENT=136468119 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role`
--

LOCK TABLES `role` WRITE;
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
INSERT INTO `role` VALUES (31051,1,'Super User Level 1',2),(31054,1,'User',0),(31055,1,'Browser',0),(31056,1,'Main Administrator',0),(31057,1,'Adminstrator',0),(31058,1,'Customer Service',2),(31059,1,'Accounting',0),(31060,1,'Sales',0),(31061,1,'Merchandising',0),(31062,1,'Marketing',0),(31063,1,'Order Approver',1),(32434,1,'Admin',2),(1552771,1,'Low User',0),(17702757,1,'APD Customer Service',1),(39304650,1,'Cost Center',0),(123891398,0,'Accounts Payable',0),(123891415,0,'Tech Support',0),(136468118,1,'Marfield Customer Service',1);
/*!40000 ALTER TABLE `role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_aud`
--

DROP TABLE IF EXISTS `role_aud`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_aud` (
  `ID` bigint(20) NOT NULL DEFAULT '0',
  `REV` bigint(20) NOT NULL AUTO_INCREMENT,
  `REVTYPE` decimal(3,0) DEFAULT NULL,
  `ISACTIVE` decimal(1,0) DEFAULT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`,`REV`),
  KEY `FKF3FAE767DF74E053` (`REV`),
  CONSTRAINT `FKF3FAE767DF74E053` FOREIGN KEY (`REV`) REFERENCES `revinfo` (`REV`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_aud`
--

LOCK TABLES `role_aud` WRITE;
/*!40000 ALTER TABLE `role_aud` DISABLE KEYS */;
/*!40000 ALTER TABLE `role_aud` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_permission`
--

DROP TABLE IF EXISTS `role_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_permission` (
  `ROLES_ID` bigint(20) NOT NULL DEFAULT '0',
  `PERMISSIONS_ID` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ROLES_ID`,`PERMISSIONS_ID`),
  KEY `FKF8A56938BC8D8328` (`PERMISSIONS_ID`),
  CONSTRAINT `FKF8A5693846C1C336` FOREIGN KEY (`ROLES_ID`) REFERENCES `role` (`ID`),
  CONSTRAINT `FKF8A56938BC8D8328` FOREIGN KEY (`PERMISSIONS_ID`) REFERENCES `permission` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_permission`
--

LOCK TABLES `role_permission` WRITE;
/*!40000 ALTER TABLE `role_permission` DISABLE KEYS */;
INSERT INTO `role_permission` VALUES (31051,31027),(31054,31027),(31058,31027),(32434,31027),(1552771,31027),(31058,31028),(31059,31029),(31051,31031),(31054,31031),(31055,31031),(1552771,31031),(31051,31032),(31054,31032),(1552771,31032),(31056,31033),(31056,31034),(31056,31035),(31056,31036),(31056,31037),(31056,31038),(31056,31039),(31058,31039),(31056,31040),(31056,31041),(31056,31042),(31056,31043),(31056,31044),(31056,31045),(31056,31046),(31056,31047),(31056,31048),(31051,31049),(31063,31049),(31056,31050),(31051,1509658),(31051,1509663),(31051,1509667),(31058,17702751),(32434,17702751),(32434,17702752),(31054,17702753),(31058,17702754),(136468118,17702754),(31056,17702756),(32434,17702756),(17702757,17702756),(31054,132357308),(31054,132357309),(31063,132357310),(17702757,140899967);
/*!40000 ALTER TABLE `role_permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_permission_aud`
--

DROP TABLE IF EXISTS `role_permission_aud`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_permission_aud` (
  `REV` bigint(20) NOT NULL AUTO_INCREMENT,
  `ROLES_ID` bigint(20) NOT NULL DEFAULT '0',
  `PERMISSIONS_ID` bigint(20) NOT NULL DEFAULT '0',
  `REVTYPE` decimal(3,0) DEFAULT NULL,
  PRIMARY KEY (`REV`,`ROLES_ID`,`PERMISSIONS_ID`),
  CONSTRAINT `FKE90A3B09DF74E053` FOREIGN KEY (`REV`) REFERENCES `revinfo` (`REV`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_permission_aud`
--

LOCK TABLES `role_permission_aud` WRITE;
/*!40000 ALTER TABLE `role_permission_aud` DISABLE KEYS */;
/*!40000 ALTER TABLE `role_permission_aud` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rolexuser`
--

DROP TABLE IF EXISTS `rolexuser`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rolexuser` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `EXPIRATIONDATE` datetime DEFAULT NULL,
  `STARTDATE` datetime DEFAULT NULL,
  `VERSION` bigint(20) DEFAULT NULL,
  `ROLE_ID` bigint(20) DEFAULT NULL,
  `USER_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `ROLEXUSER_ROLE_ID` (`ROLE_ID`) USING BTREE,
  KEY `ROLEXUSER_USER_ID` (`USER_ID`) USING BTREE,
  CONSTRAINT `FK8B08ED6D4E9ECF4C` FOREIGN KEY (`USER_ID`) REFERENCES `systemuser` (`ID`),
  CONSTRAINT `FK8B08ED6DA8A94ADD` FOREIGN KEY (`ROLE_ID`) REFERENCES `role` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=156609528 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rolexuser`
--

LOCK TABLES `rolexuser` WRITE;
/*!40000 ALTER TABLE `rolexuser` DISABLE KEYS */;
INSERT INTO `rolexuser` VALUES (33716,NULL,NULL,0,31054,33714),(33793,NULL,NULL,0,32434,33791),(55785,NULL,NULL,0,32434,55780),(60091,NULL,NULL,0,31054,55780),(75495,NULL,NULL,0,32434,75494),(75529,NULL,'2012-09-28 00:00:00',0,31054,75528),(719551,NULL,NULL,0,31054,75494),(719574,NULL,NULL,1,32434,719573),(719663,NULL,NULL,0,31054,719662),(719671,NULL,NULL,0,32434,719670),(719672,NULL,NULL,0,31054,719670),(719700,NULL,NULL,0,31054,719573),(720223,NULL,NULL,0,32434,719662),(770071,NULL,NULL,0,31054,770070),(1304847,NULL,NULL,0,31054,33791),(1544006,NULL,NULL,0,31058,33791),(1544008,NULL,NULL,0,31056,55780),(3966431,NULL,NULL,0,31054,3966428),(3966432,NULL,NULL,0,31057,3966428),(7998721,NULL,NULL,1,31058,719670),(12340069,NULL,NULL,0,31051,719670),(14178445,NULL,NULL,0,31063,719670),(14178577,NULL,NULL,0,31054,14178576),(14178612,NULL,NULL,0,31054,14178611),(17702902,NULL,NULL,0,31058,17702900),(17702903,NULL,NULL,0,31054,17702900),(17702906,NULL,NULL,0,31054,17702904),(17702907,NULL,NULL,0,31058,17702904),(22216406,NULL,NULL,0,17702757,75494),(23537502,NULL,NULL,0,31058,75494),(38897870,NULL,NULL,0,31058,75544),(38897871,NULL,NULL,0,17702757,75544),(38898564,NULL,NULL,0,17702757,719670),(39300796,NULL,NULL,0,17702757,33791),(39306233,NULL,NULL,0,39304650,719670),(39306235,NULL,NULL,0,17702757,33714),(39306236,NULL,NULL,0,39304650,33714),(39573818,NULL,NULL,0,17702757,17702904),(83770439,NULL,NULL,0,17702757,14178576),(83770440,NULL,NULL,0,31058,14178576),(84114821,NULL,NULL,0,17702757,17702900),(102409280,NULL,NULL,0,31058,55780),(103526684,NULL,NULL,0,17702757,770070),(103526685,NULL,NULL,0,31058,770070),(120725006,NULL,NULL,0,32434,33714),(120725007,NULL,NULL,0,31056,33714),(124025642,NULL,NULL,0,123891398,719670),(124025691,NULL,NULL,0,123891415,33714),(124025989,NULL,NULL,0,123891415,75494),(124375336,NULL,NULL,0,31057,55780),(132958326,NULL,NULL,0,31063,55780),(134690093,NULL,NULL,0,31058,33714),(139820201,NULL,NULL,0,17702757,55780),(139820443,NULL,NULL,0,123891415,55780),(139820444,NULL,NULL,0,39304650,55780),(142076049,NULL,NULL,0,123891398,55780),(146644745,NULL,NULL,0,31054,75544),(156609527,NULL,NULL,0,31058,719573);
/*!40000 ALTER TABLE `rolexuser` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rolexuser_aud`
--

DROP TABLE IF EXISTS `rolexuser_aud`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rolexuser_aud` (
  `ID` bigint(20) NOT NULL DEFAULT '0',
  `REV` bigint(20) NOT NULL AUTO_INCREMENT,
  `REVTYPE` decimal(3,0) DEFAULT NULL,
  `EXPIRATIONDATE` datetime DEFAULT NULL,
  `STARTDATE` datetime DEFAULT NULL,
  `ROLE_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`,`REV`),
  KEY `FKD8F69CBEDF74E053` (`REV`),
  CONSTRAINT `FKD8F69CBEDF74E053` FOREIGN KEY (`REV`) REFERENCES `revinfo` (`REV`)
) ENGINE=InnoDB AUTO_INCREMENT=156609529 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rolexuser_aud`
--

LOCK TABLES `rolexuser_aud` WRITE;
/*!40000 ALTER TABLE `rolexuser_aud` DISABLE KEYS */;
INSERT INTO `rolexuser_aud` VALUES (156609527,156609528,0,NULL,NULL,31058);
/*!40000 ALTER TABLE `rolexuser_aud` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sender_credential_table`
--

DROP TABLE IF EXISTS `sender_credential_table`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sender_credential_table` (
  `CXMLCONFIGURATION_ID` bigint(20) NOT NULL DEFAULT '0',
  `SENDERCREDENTIALS_ID` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`CXMLCONFIGURATION_ID`,`SENDERCREDENTIALS_ID`),
  UNIQUE KEY `SYS_C0022042` (`SENDERCREDENTIALS_ID`),
  CONSTRAINT `FK49D502B0BFA78821` FOREIGN KEY (`SENDERCREDENTIALS_ID`) REFERENCES `cxmlcredential` (`ID`),
  CONSTRAINT `FK49D502B0D0B0DA57` FOREIGN KEY (`CXMLCONFIGURATION_ID`) REFERENCES `cxmlconfiguration` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sender_credential_table`
--

LOCK TABLES `sender_credential_table` WRITE;
/*!40000 ALTER TABLE `sender_credential_table` DISABLE KEYS */;
INSERT INTO `sender_credential_table` VALUES (17708586,17708588),(72500901,72500903),(102567915,102568001),(110038389,110038391),(123726544,123726547),(141078347,141078349),(142745206,142745209),(143975296,143975298),(143975296,143975299),(153624781,153624783);
/*!40000 ALTER TABLE `sender_credential_table` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sender_credential_table_aud`
--

DROP TABLE IF EXISTS `sender_credential_table_aud`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sender_credential_table_aud` (
  `REV` bigint(20) NOT NULL AUTO_INCREMENT,
  `CXMLCONFIGURATION_ID` bigint(20) NOT NULL DEFAULT '0',
  `SENDERCREDENTIALS_ID` bigint(20) NOT NULL DEFAULT '0',
  `REVTYPE` decimal(3,0) DEFAULT NULL,
  PRIMARY KEY (`REV`,`CXMLCONFIGURATION_ID`,`SENDERCREDENTIALS_ID`),
  CONSTRAINT `FK7D605881DF74E053` FOREIGN KEY (`REV`) REFERENCES `revinfo` (`REV`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sender_credential_table_aud`
--

LOCK TABLES `sender_credential_table_aud` WRITE;
/*!40000 ALTER TABLE `sender_credential_table_aud` DISABLE KEYS */;
/*!40000 ALTER TABLE `sender_credential_table_aud` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `service_log`
--

DROP TABLE IF EXISTS `service_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `service_log` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `FOLLOWUPCOMPLETE` decimal(1,0) DEFAULT NULL,
  `OPENEDDATE` datetime NOT NULL,
  `RESOLUTION` varchar(255) DEFAULT NULL,
  `RESOLVEDDATE` datetime DEFAULT NULL,
  `UPDATETIMESTAMP` datetime DEFAULT NULL,
  `VERSION` bigint(20) DEFAULT NULL,
  `ACCOUNT_ID` bigint(20) DEFAULT NULL,
  `APDCSRREP_ID` bigint(20) DEFAULT NULL,
  `CONTACTMETHOD_ID` bigint(20) DEFAULT NULL,
  `CUSTOMERORDER_ID` bigint(20) DEFAULT NULL,
  `SERVICEREASON_ID` bigint(20) DEFAULT NULL,
  `DEPARTMENT` varchar(255) DEFAULT NULL,
  `CONTACTNAME` varchar(255) DEFAULT NULL,
  `CONTACTNUMBER` varchar(255) DEFAULT NULL,
  `CONTACTEMAIL` varchar(255) DEFAULT NULL,
  `STATUS` varchar(255) DEFAULT NULL,
  `TICKETNUMBER` varchar(255) DEFAULT NULL,
  `SERVICEDETAILS` varchar(3000) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FKD76567A185A22D7` (`ACCOUNT_ID`),
  KEY `FKD76567AA9054CB7` (`CONTACTMETHOD_ID`),
  KEY `FKD76567A26CAE17` (`CUSTOMERORDER_ID`),
  KEY `FKD76567A4E2C1B7` (`SERVICEREASON_ID`),
  KEY `FKD76567A29E27F47` (`APDCSRREP_ID`),
  CONSTRAINT `FKD76567A185A22D7` FOREIGN KEY (`ACCOUNT_ID`) REFERENCES `account` (`ID`),
  CONSTRAINT `FKD76567A26CAE17` FOREIGN KEY (`CUSTOMERORDER_ID`) REFERENCES `customerorder` (`ID`),
  CONSTRAINT `FKD76567A29E27F47` FOREIGN KEY (`APDCSRREP_ID`) REFERENCES `systemuser` (`ID`),
  CONSTRAINT `FKD76567A4E2C1B7` FOREIGN KEY (`SERVICEREASON_ID`) REFERENCES `servicereason` (`ID`),
  CONSTRAINT `FKD76567AA9054CB7` FOREIGN KEY (`CONTACTMETHOD_ID`) REFERENCES `contactmethod` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `service_log`
--

LOCK TABLES `service_log` WRITE;
/*!40000 ALTER TABLE `service_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `service_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `servicereason`
--

DROP TABLE IF EXISTS `servicereason`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `servicereason` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(255) DEFAULT NULL,
  `VERSION` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=13613654 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `servicereason`
--

LOCK TABLES `servicereason` WRITE;
/*!40000 ALTER TABLE `servicereason` DISABLE KEYS */;
INSERT INTO `servicereason` VALUES (13613645,'Return - Damaged Item',0),(13613646,'Invoices',0),(13613647,'Credits',0),(13613648,'Cancel Order - Incorrect Item',0),(13613649,'Order Status',0),(13613650,'General Inquiry',0),(13613651,'Customer Maintenance',0),(13613652,'Technical Issue',0),(13613653,'NA',0);
/*!40000 ALTER TABLE `servicereason` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shipmanifest`
--

DROP TABLE IF EXISTS `shipmanifest`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shipmanifest` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `MANIFESTDATE` datetime DEFAULT NULL,
  `NUMBOXES` decimal(19,2) DEFAULT NULL,
  `VERSION` bigint(20) DEFAULT NULL,
  `BULKCARTONINDICATOR` varchar(255) DEFAULT NULL,
  `CARRIERCODE` varchar(255) DEFAULT NULL,
  `CARTONBARCODE` varchar(255) DEFAULT NULL,
  `CARTONID` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `CARTONWEIGHT` varchar(255) DEFAULT NULL,
  `CUSTOMERADDRESS1` varchar(255) DEFAULT NULL,
  `CUSTOMERADDRESS2` varchar(255) DEFAULT NULL,
  `CUSTOMERBARCODE` varchar(255) DEFAULT NULL,
  `CUSTOMERCITY` varchar(255) DEFAULT NULL,
  `CUSTOMERNAME` varchar(255) DEFAULT NULL,
  `CUSTOMERNUMBER` varchar(255) DEFAULT NULL,
  `CUSTOMERPOSTALCODE` varchar(255) DEFAULT NULL,
  `CUSTOMERPURCHASEORDERNUMBER` varchar(255) DEFAULT NULL,
  `CUSTOMERREFERENCEDATA` varchar(255) DEFAULT NULL,
  `CUSTOMERROUTEDATA` varchar(255) DEFAULT NULL,
  `CUSTOMERROUTEDESCRIPTION` varchar(255) DEFAULT NULL,
  `CUSTOMERSTATE` varchar(255) DEFAULT NULL,
  `DATEPROCESSED` datetime DEFAULT NULL,
  `DATETIMESENT` datetime DEFAULT NULL,
  `DEALERINFORMATION1` varchar(255) DEFAULT NULL,
  `DEALERINFORMATION2` varchar(255) DEFAULT NULL,
  `DEALERINFORMATION3` varchar(255) DEFAULT NULL,
  `DEALERINFORMATION4` varchar(255) DEFAULT NULL,
  `DEALERINFORMATION5` varchar(255) DEFAULT NULL,
  `DEALERINFORMATION6` varchar(255) DEFAULT NULL,
  `DELIVERYMETHODCODE` varchar(255) DEFAULT NULL,
  `DELIVERYTYPECODE` varchar(255) DEFAULT NULL,
  `DOCUMENTID` varchar(255) DEFAULT NULL,
  `ENDCONSUMERADDRESS1` varchar(255) DEFAULT NULL,
  `ENDCONSUMERADDRESS2` varchar(255) DEFAULT NULL,
  `ENDCONSUMERADDRESS3` varchar(255) DEFAULT NULL,
  `ENDCONSUMERCITY` varchar(255) DEFAULT NULL,
  `ENDCONSUMERNAME` varchar(255) DEFAULT NULL,
  `ENDCONSUMERPOSTALCODE` varchar(255) DEFAULT NULL,
  `ENDCONSUMERPURCHASEORDERDATA` varchar(255) DEFAULT NULL,
  `ENDCONSUMERSTATE` varchar(255) DEFAULT NULL,
  `FACILITYABBREVIATION` varchar(255) DEFAULT NULL,
  `FACILITYADDRESS1` varchar(255) DEFAULT NULL,
  `FACILITYADDRESS2` varchar(255) DEFAULT NULL,
  `FACILITYCITY` varchar(255) DEFAULT NULL,
  `FACILITYNAME` varchar(255) DEFAULT NULL,
  `FACILITYNUMBER` varchar(255) DEFAULT NULL,
  `FACILITYPOSTALCODE` varchar(255) DEFAULT NULL,
  `FACILITYSTATE` varchar(255) DEFAULT NULL,
  `FILLFACILITYNUMBER` varchar(255) DEFAULT NULL,
  `HAZARDOUSMATERIALINDICATOR` varchar(255) DEFAULT NULL,
  `ISMANIFESTED` decimal(1,0) DEFAULT NULL,
  `ITEMPREFIX` varchar(255) DEFAULT NULL,
  `ITEMSTOCK` varchar(255) DEFAULT NULL,
  `ORDERNUMBER` varchar(255) DEFAULT NULL,
  `PRIMARYORDERNUMBER` varchar(255) DEFAULT NULL,
  `SCANDATETIME` datetime DEFAULT NULL,
  `SELECTIONTYPECODE` varchar(255) DEFAULT NULL,
  `SHIPTRUCKCODE` varchar(255) DEFAULT NULL,
  `SHIPTRUCKDOCKID` varchar(255) DEFAULT NULL,
  `SHIPPINGINFORMATION1` varchar(255) DEFAULT NULL,
  `SHIPPINGINFORMATION2` varchar(255) DEFAULT NULL,
  `SHIPPINGINFORMATION3` varchar(255) DEFAULT NULL,
  `SHIPPINGINFORMATION4` varchar(255) DEFAULT NULL,
  `SHIPPINGINFORMATION5` varchar(255) DEFAULT NULL,
  `SHIPPINGINFORMATION6` varchar(255) DEFAULT NULL,
  `SPECIALINSTRUCTIONS1` varchar(255) DEFAULT NULL,
  `SPECIALINSTRUCTIONS2` varchar(255) DEFAULT NULL,
  `SUBORDERNUMBER` varchar(255) DEFAULT NULL,
  `TRADINGPARTNERID` varchar(255) DEFAULT NULL,
  `TRUCKCODEDESCRIPTION` varchar(255) DEFAULT NULL,
  `TYPE` varchar(255) DEFAULT NULL,
  `CUSTOMERORDER_ID` bigint(20) DEFAULT NULL,
  `DELIVERYDATE` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `SYS_C0022050` (`CARTONID`),
  KEY `SHIPMANIFEST_CUSTOMERORDER_ID` (`CUSTOMERORDER_ID`) USING BTREE,
  CONSTRAINT `FK1734C4AB26CAE17` FOREIGN KEY (`CUSTOMERORDER_ID`) REFERENCES `customerorder` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shipmanifest`
--

LOCK TABLES `shipmanifest` WRITE;
/*!40000 ALTER TABLE `shipmanifest` DISABLE KEYS */;
/*!40000 ALTER TABLE `shipmanifest` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shipmanifest_pick`
--

DROP TABLE IF EXISTS `shipmanifest_pick`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shipmanifest_pick` (
  `SHIPMANIFEST_ID` bigint(20) NOT NULL DEFAULT '0',
  `PICKS_ID` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`SHIPMANIFEST_ID`,`PICKS_ID`),
  UNIQUE KEY `UK_E8531B55D3FB4008` (`PICKS_ID`),
  CONSTRAINT `FKE8531B552A91F50C` FOREIGN KEY (`PICKS_ID`) REFERENCES `pick` (`ID`),
  CONSTRAINT `FKE8531B559EBD9BDD` FOREIGN KEY (`SHIPMANIFEST_ID`) REFERENCES `shipmanifest` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shipmanifest_pick`
--

LOCK TABLES `shipmanifest_pick` WRITE;
/*!40000 ALTER TABLE `shipmanifest_pick` DISABLE KEYS */;
/*!40000 ALTER TABLE `shipmanifest_pick` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shipmanifestlog`
--

DROP TABLE IF EXISTS `shipmanifestlog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shipmanifestlog` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `DATEPROCESSED` datetime DEFAULT NULL,
  `FILENAME` varchar(255) DEFAULT NULL,
  `VERSION` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shipmanifestlog`
--

LOCK TABLES `shipmanifestlog` WRITE;
/*!40000 ALTER TABLE `shipmanifestlog` DISABLE KEYS */;
/*!40000 ALTER TABLE `shipmanifestlog` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shipment`
--

DROP TABLE IF EXISTS `shipment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shipment` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `DELIVEREDTIME` datetime DEFAULT NULL,
  `SHIPTIME` datetime DEFAULT NULL,
  `SUBTOTALCOST` decimal(19,2) NOT NULL,
  `SUBTOTALPRICE` decimal(19,2) NOT NULL,
  `TOTALFREIGHTCOST` decimal(19,2) NOT NULL,
  `TOTALFREIGHTPRICE` decimal(19,2) NOT NULL,
  `TOTALSHIPPINGCOST` decimal(19,2) NOT NULL,
  `TOTALSHIPPINGPRICE` decimal(19,2) NOT NULL,
  `TOTALTAXPRICE` decimal(19,2) NOT NULL,
  `TRACKINGNUMBER` varchar(255) DEFAULT NULL,
  `VERSION` bigint(20) DEFAULT NULL,
  `ADDRESS_ID` bigint(20) DEFAULT NULL,
  `APDINVOICE_ID` bigint(20) DEFAULT NULL,
  `ORDER_ID` bigint(20) DEFAULT NULL,
  `PARENTSHIPMENT_ID` bigint(20) DEFAULT NULL,
  `SHIPPINGPARTNER_ID` bigint(20) DEFAULT NULL,
  `CUSTOMERSERVICEADDRESS_ID` bigint(20) DEFAULT NULL,
  `FROMADDRESS_ID` bigint(20) DEFAULT NULL,
  `TOCHARGE` decimal(1,0) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `INDX_46` (`ADDRESS_ID`) USING BTREE,
  KEY `SHIPMENT_CUSTORDER_IDX` (`ORDER_ID`) USING BTREE,
  KEY `SHIPMENT_SHIPTIME_IDX` (`SHIPTIME`) USING BTREE,
  KEY `FKE513D5BA8A0D04A1` (`FROMADDRESS_ID`),
  KEY `FKE513D5BAD81419CE` (`CUSTOMERSERVICEADDRESS_ID`),
  KEY `FKE513D5BA5BE726AC` (`APDINVOICE_ID`),
  KEY `FKE513D5BA495D8297` (`SHIPPINGPARTNER_ID`),
  KEY `FKE513D5BA9744A7F3` (`PARENTSHIPMENT_ID`),
  CONSTRAINT `FKE513D5BA495D8297` FOREIGN KEY (`SHIPPINGPARTNER_ID`) REFERENCES `shippingpartner` (`ID`),
  CONSTRAINT `FKE513D5BA5BE726AC` FOREIGN KEY (`APDINVOICE_ID`) REFERENCES `invoice` (`ID`),
  CONSTRAINT `FKE513D5BA8A0D04A1` FOREIGN KEY (`FROMADDRESS_ID`) REFERENCES `address` (`ID`),
  CONSTRAINT `FKE513D5BA8E1EC39` FOREIGN KEY (`ORDER_ID`) REFERENCES `customerorder` (`ID`),
  CONSTRAINT `FKE513D5BA9744A7F3` FOREIGN KEY (`PARENTSHIPMENT_ID`) REFERENCES `shipment` (`ID`),
  CONSTRAINT `FKE513D5BA99EEF977` FOREIGN KEY (`ADDRESS_ID`) REFERENCES `address` (`ID`),
  CONSTRAINT `FKE513D5BAD81419CE` FOREIGN KEY (`CUSTOMERSERVICEADDRESS_ID`) REFERENCES `address` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shipment`
--

LOCK TABLES `shipment` WRITE;
/*!40000 ALTER TABLE `shipment` DISABLE KEYS */;
/*!40000 ALTER TABLE `shipment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shippingpartner`
--

DROP TABLE IF EXISTS `shippingpartner`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shippingpartner` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `VERSION` bigint(20) DEFAULT NULL,
  `DUNS` varchar(255) NOT NULL,
  `SCACCODE` varchar(255) NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UC_SHIPPINGPARTNER_1` (`NAME`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shippingpartner`
--

LOCK TABLES `shippingpartner` WRITE;
/*!40000 ALTER TABLE `shippingpartner` DISABLE KEYS */;
/*!40000 ALTER TABLE `shippingpartner` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shippingpartner_aud`
--

DROP TABLE IF EXISTS `shippingpartner_aud`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shippingpartner_aud` (
  `ID` bigint(20) NOT NULL DEFAULT '0',
  `REV` bigint(20) NOT NULL AUTO_INCREMENT,
  `REVTYPE` decimal(3,0) DEFAULT NULL,
  `DUNS` varchar(255) DEFAULT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `SCACCODE` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`,`REV`),
  KEY `FK75423FEBDF74E053` (`REV`),
  CONSTRAINT `FK75423FEBDF74E053` FOREIGN KEY (`REV`) REFERENCES `revinfo` (`REV`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shippingpartner_aud`
--

LOCK TABLES `shippingpartner_aud` WRITE;
/*!40000 ALTER TABLE `shippingpartner_aud` DISABLE KEYS */;
/*!40000 ALTER TABLE `shippingpartner_aud` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sir_errors`
--

DROP TABLE IF EXISTS `sir_errors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sir_errors` (
  `SIR_ID` bigint(20) DEFAULT NULL,
  `ERRORS` varchar(4000) DEFAULT NULL,
  KEY `SIR_ERRORS_IDX` (`SIR_ID`) USING BTREE,
  CONSTRAINT `FK239AC02E8A0FF28C` FOREIGN KEY (`SIR_ID`) REFERENCES `syncitemresult` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sir_errors`
--

LOCK TABLES `sir_errors` WRITE;
/*!40000 ALTER TABLE `sir_errors` DISABLE KEYS */;
/*!40000 ALTER TABLE `sir_errors` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sir_warnings`
--

DROP TABLE IF EXISTS `sir_warnings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sir_warnings` (
  `SIR_ID` bigint(20) DEFAULT NULL,
  `WARNINGS` varchar(4000) DEFAULT NULL,
  KEY `SIR_WARNINGS_IDX` (`SIR_ID`) USING BTREE,
  CONSTRAINT `FK7264705A8A0FF28C` FOREIGN KEY (`SIR_ID`) REFERENCES `syncitemresult` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sir_warnings`
--

LOCK TABLES `sir_warnings` WRITE;
/*!40000 ALTER TABLE `sir_warnings` DISABLE KEYS */;
/*!40000 ALTER TABLE `sir_warnings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sku`
--

DROP TABLE IF EXISTS `sku`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sku` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `VALUE` varchar(255) DEFAULT NULL,
  `VERSION` bigint(20) DEFAULT NULL,
  `ITEM_ID` bigint(20) DEFAULT NULL,
  `TYPE_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `INDEX164` (`VALUE`) USING BTREE,
  KEY `SKU_ITEM_ID_IX` (`ITEM_ID`) USING BTREE,
  KEY `SKU_TYPE_ID_IX` (`TYPE_ID`) USING BTREE,
  CONSTRAINT `FK144FDD4B669BD` FOREIGN KEY (`ITEM_ID`) REFERENCES `item` (`ID`),
  CONSTRAINT `FK144FDDC2B4674` FOREIGN KEY (`TYPE_ID`) REFERENCES `skutype` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sku`
--

LOCK TABLES `sku` WRITE;
/*!40000 ALTER TABLE `sku` DISABLE KEYS */;
/*!40000 ALTER TABLE `sku` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `skutype`
--

DROP TABLE IF EXISTS `skutype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `skutype` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `VERSION` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UC_SKUTYPE_1` (`NAME`)
) ENGINE=InnoDB AUTO_INCREMENT=31190 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `skutype`
--

LOCK TABLES `skutype` WRITE;
/*!40000 ALTER TABLE `skutype` DISABLE KEYS */;
INSERT INTO `skutype` VALUES (31185,'dealer',0),(31186,'customer',0),(31187,'vendor',0),(31188,'manufacturer',0),(31189,'search',0);
/*!40000 ALTER TABLE `skutype` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `skutype_aud`
--

DROP TABLE IF EXISTS `skutype_aud`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `skutype_aud` (
  `ID` bigint(20) NOT NULL DEFAULT '0',
  `REV` bigint(20) NOT NULL AUTO_INCREMENT,
  `REVTYPE` decimal(3,0) DEFAULT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`,`REV`),
  KEY `FKFFADEC28DF74E053` (`REV`),
  CONSTRAINT `FKFFADEC28DF74E053` FOREIGN KEY (`REV`) REFERENCES `revinfo` (`REV`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `skutype_aud`
--

LOCK TABLES `skutype_aud` WRITE;
/*!40000 ALTER TABLE `skutype_aud` DISABLE KEYS */;
/*!40000 ALTER TABLE `skutype_aud` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `skutype_hierarchynode`
--

DROP TABLE IF EXISTS `skutype_hierarchynode`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `skutype_hierarchynode` (
  `SKUTYPES_ID` bigint(20) NOT NULL DEFAULT '0',
  `HIERARCHYNODES_ID` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`SKUTYPES_ID`,`HIERARCHYNODES_ID`),
  KEY `FKDB1B914FBA0B4EF2` (`HIERARCHYNODES_ID`),
  CONSTRAINT `FKDB1B914FA777D0F2` FOREIGN KEY (`SKUTYPES_ID`) REFERENCES `skutype` (`ID`),
  CONSTRAINT `FKDB1B914FBA0B4EF2` FOREIGN KEY (`HIERARCHYNODES_ID`) REFERENCES `hierarchynode` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `skutype_hierarchynode`
--

LOCK TABLES `skutype_hierarchynode` WRITE;
/*!40000 ALTER TABLE `skutype_hierarchynode` DISABLE KEYS */;
/*!40000 ALTER TABLE `skutype_hierarchynode` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `specificationdescription`
--

DROP TABLE IF EXISTS `specificationdescription`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `specificationdescription` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `DESCRIPTION` varchar(255) DEFAULT NULL,
  `VERSION` bigint(20) DEFAULT NULL,
  `DESCRIPTIONTYPE_ID` bigint(20) DEFAULT NULL,
  `SPECIFICATIONPROPERTY_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `SD_SPID_IX` (`SPECIFICATIONPROPERTY_ID`) USING BTREE,
  KEY `FK6A555F19352E4157` (`DESCRIPTIONTYPE_ID`),
  CONSTRAINT `FK6A555F19352E4157` FOREIGN KEY (`DESCRIPTIONTYPE_ID`) REFERENCES `descriptiontype` (`ID`),
  CONSTRAINT `FK6A555F19D189F457` FOREIGN KEY (`SPECIFICATIONPROPERTY_ID`) REFERENCES `specificationproperty` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `specificationdescription`
--

LOCK TABLES `specificationdescription` WRITE;
/*!40000 ALTER TABLE `specificationdescription` DISABLE KEYS */;
/*!40000 ALTER TABLE `specificationdescription` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `specificationproperty`
--

DROP TABLE IF EXISTS `specificationproperty`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `specificationproperty` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(255) DEFAULT NULL,
  `SEQUENCE` bigint(20) DEFAULT NULL,
  `VALUE` varchar(400) DEFAULT NULL,
  `VERSION` bigint(20) DEFAULT NULL,
  `ITEMSPECIFICATION_ID` bigint(20) DEFAULT NULL,
  `PRIORITY` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `INDEX162` (`ITEMSPECIFICATION_ID`) USING BTREE,
  KEY `INDX_48` (`NAME`) USING BTREE,
  KEY `INDX_49` (`VALUE`) USING BTREE,
  CONSTRAINT `FK16701FD87DA5C397` FOREIGN KEY (`ITEMSPECIFICATION_ID`) REFERENCES `itemspecification` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `specificationproperty`
--

LOCK TABLES `specificationproperty` WRITE;
/*!40000 ALTER TABLE `specificationproperty` DISABLE KEYS */;
/*!40000 ALTER TABLE `specificationproperty` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `syncitemresult`
--

DROP TABLE IF EXISTS `syncitemresult`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `syncitemresult` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `CORRELATIONID` varchar(255) DEFAULT NULL,
  `RESULTAPDSKU` varchar(255) DEFAULT NULL,
  `RESULTVENDOR` varchar(255) DEFAULT NULL,
  `VERSION` bigint(20) DEFAULT NULL,
  `SUMMARY_ID` bigint(20) DEFAULT NULL,
  `ERRORS` varchar(4000) DEFAULT NULL,
  `WARNINGS` varchar(4000) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `SIR_CORRID` (`CORRELATIONID`) USING BTREE,
  KEY `SIR_SUMMARY_IDX` (`SUMMARY_ID`) USING BTREE,
  CONSTRAINT `FKC520D22BBE7814EC` FOREIGN KEY (`SUMMARY_ID`) REFERENCES `syncitemresultsummary` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `syncitemresult`
--

LOCK TABLES `syncitemresult` WRITE;
/*!40000 ALTER TABLE `syncitemresult` DISABLE KEYS */;
/*!40000 ALTER TABLE `syncitemresult` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `syncitemresultsummary`
--

DROP TABLE IF EXISTS `syncitemresultsummary`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `syncitemresultsummary` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `CORRELATIONID` varchar(255) DEFAULT NULL,
  `CREATED` bigint(20) DEFAULT NULL,
  `DISCONTINUED` bigint(20) DEFAULT NULL,
  `ERRORS` bigint(20) DEFAULT NULL,
  `FAILURES` bigint(20) DEFAULT NULL,
  `UPDATED` bigint(20) DEFAULT NULL,
  `VERSION` bigint(20) DEFAULT NULL,
  `WARNINGS` bigint(20) DEFAULT NULL,
  `ZEROPRICE` bigint(20) DEFAULT NULL,
  `TOTAL` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `SIRS_CORRID` (`CORRELATIONID`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `syncitemresultsummary`
--

LOCK TABLES `syncitemresultsummary` WRITE;
/*!40000 ALTER TABLE `syncitemresultsummary` DISABLE KEYS */;
/*!40000 ALTER TABLE `syncitemresultsummary` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `systemcomment`
--

DROP TABLE IF EXISTS `systemcomment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `systemcomment` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `COMMENTDATE` datetime DEFAULT NULL,
  `CONTENT` varchar(4000) DEFAULT NULL,
  `FILEURL` varchar(255) DEFAULT NULL,
  `VERSION` bigint(20) DEFAULT NULL,
  `SHIPMENT_ID` bigint(20) DEFAULT NULL,
  `USER_ID` bigint(20) DEFAULT NULL,
  `ORDER_ID` bigint(20) DEFAULT NULL,
  `MADEBYSYSTEM` decimal(1,0) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FKE13441108E1EC39` (`ORDER_ID`),
  KEY `FKE1344110BBACDEDD` (`SHIPMENT_ID`),
  KEY `FKE13441104E9ECF4C` (`USER_ID`),
  CONSTRAINT `FKE13441104E9ECF4C` FOREIGN KEY (`USER_ID`) REFERENCES `systemuser` (`ID`),
  CONSTRAINT `FKE13441108E1EC39` FOREIGN KEY (`ORDER_ID`) REFERENCES `customerorder` (`ID`),
  CONSTRAINT `FKE1344110BBACDEDD` FOREIGN KEY (`SHIPMENT_ID`) REFERENCES `shipment` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `systemcomment`
--

LOCK TABLES `systemcomment` WRITE;
/*!40000 ALTER TABLE `systemcomment` DISABLE KEYS */;
/*!40000 ALTER TABLE `systemcomment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `systemuser`
--

DROP TABLE IF EXISTS `systemuser`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `systemuser` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `CREATIONDATE` datetime DEFAULT NULL,
  `CHANGEPASSWORD` decimal(1,0) NOT NULL,
  `CONCURRENTACCESS` decimal(1,0) NOT NULL,
  `ISACTIVE` decimal(1,0) NOT NULL,
  `LASTLOGINATTEMPTDATE` datetime DEFAULT NULL,
  `LASTLOGINDATE` datetime DEFAULT NULL,
  `LOGIN` varchar(255) NOT NULL,
  `LOGINATTEMPTS` bigint(20) DEFAULT NULL,
  `PASSWORD` varchar(255) DEFAULT NULL,
  `SALT` varchar(255) DEFAULT NULL,
  `STATUS` varchar(255) DEFAULT NULL,
  `VERSION` bigint(20) DEFAULT NULL,
  `PAYMENTINFORMATION_ID` bigint(20) DEFAULT NULL,
  `CREDITCARDATTEMPTS` bigint(20) DEFAULT NULL,
  `LASTCREDITCARDATTEMPTDATE` datetime DEFAULT NULL,
  `PREVIOUSCREDITCARDATTEMPTDATE` datetime DEFAULT NULL,
  `PERSON_ID` bigint(20) DEFAULT NULL,
  `PASSWORDCREATIONDATE` datetime DEFAULT NULL,
  `CATALOGSELECTIONTYPE` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UC_SYSTEMUSER_1` (`LOGIN`),
  UNIQUE KEY `UNIQUE_PERSONID` (`PERSON_ID`),
  KEY `SYSTEMUSER_PAYMENTINFO_ID` (`PAYMENTINFORMATION_ID`) USING BTREE,
  CONSTRAINT `FK9D23FEBA3E66BDBD` FOREIGN KEY (`PERSON_ID`) REFERENCES `person` (`ID`),
  CONSTRAINT `FK9D23FEBA7ED1037D` FOREIGN KEY (`PAYMENTINFORMATION_ID`) REFERENCES `paymentinformation` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=17702905 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `systemuser`
--

LOCK TABLES `systemuser` WRITE;
/*!40000 ALTER TABLE `systemuser` DISABLE KEYS */;
INSERT INTO `systemuser` VALUES (33714,NULL,0,0,1,NULL,'2014-12-07 00:00:00','jrichardson',0,'$2a$12$gJe0hFsAr.3ftLnSiEqAxuodSMVGQTcWjo.Wyqhd6ggUvpXjgUfo.','$2a$12$gJe0hFsAr.3ftLnSiEqAxu','active',38,17708934,1,'1969-12-31 19:00:00','1969-12-31 19:00:00',33715,NULL,NULL),(33791,NULL,1,0,1,NULL,'2014-12-04 00:00:00','rlevyapd',0,'$2a$12$nBrQvuTijRXptvudXppl7eKWfuCC3HnQwgmJlrcdnsS26atDSvt1C','$2a$12$nBrQvuTijRXptvudXppl7e',NULL,16,39304899,NULL,NULL,NULL,33792,NULL,NULL),(55780,NULL,0,0,1,NULL,'2014-12-05 00:00:00','dmangana',0,'$2a$12$9T0cPK/EqYbaDDVHeBqSc.zysWUWjV04fULfgpMYYJ2QEbeJnJ9n.','$2a$12$9T0cPK/EqYbaDDVHeBqSc.',NULL,91,133003255,1,'2014-06-18 15:26:14','2014-06-18 14:45:58',55781,NULL,NULL),(75494,NULL,1,0,1,NULL,'2014-12-05 00:00:00','tdouglas',0,'$2a$12$WZ0hcZkf3tfYafEhnH6/z.Wueb3z1oSd0eeitpa4M/Xm063xWGlUO','$2a$12$WZ0hcZkf3tfYafEhnH6/z.',NULL,22,NULL,NULL,NULL,NULL,75496,NULL,NULL),(75528,NULL,1,0,1,NULL,NULL,'aceroj',NULL,'$2a$12$0CPZ3..Ag6YQG8nmWUvOlOsuHpIRiZWFKIw/buBdUwIndxBI0Xkcq','$2a$12$0CPZ3..Ag6YQG8nmWUvOlO',NULL,0,NULL,NULL,NULL,NULL,75530,NULL,NULL),(75544,NULL,1,0,1,NULL,'2014-12-05 00:00:00','aescobar',0,'$2a$12$xqlXor0hnB3tJ3DMsC2ACuFE3momp9P/x5IgW.4lZWTmA0YA7l1n2','$2a$12$xqlXor0hnB3tJ3DMsC2ACu',NULL,9,NULL,NULL,NULL,NULL,75545,NULL,NULL),(719573,NULL,0,0,1,NULL,'2015-11-12 00:00:00','admin',0,'$2a$12$BNFFtozjVovEMVLsE2AQuexQnhKPTziqnBwRGCH6.JTcprlvuTB5i','$2a$12$BNFFtozjVovEMVLsE2AQue',NULL,2,NULL,NULL,NULL,NULL,719575,NULL,NULL),(719662,NULL,1,0,1,NULL,NULL,'cyken',0,'$2a$12$mvQBISr1ZSOibI8RZQTfaOeZw8XJoeosFSgJd2ZA3mpkf1.U.RwWG','$2a$12$mvQBISr1ZSOibI8RZQTfaO',NULL,2,NULL,NULL,NULL,NULL,719664,NULL,NULL),(719670,NULL,1,0,1,NULL,'2014-12-07 00:00:00','evdinion',0,'$2a$12$6KOcqYbUGLxQmggRKh9kS.uNiw8eCA57PJhzOA7iMUmpjTJq.H.Ae','$2a$12$6KOcqYbUGLxQmggRKh9kS.',NULL,46,18808035,NULL,NULL,NULL,719673,NULL,NULL),(719747,NULL,0,0,1,NULL,'2014-11-18 00:00:00','razsayavong',0,'$2a$12$bLvUgjVMiV5RXRxszl47N.cwuOrf.uXl4.n6W8ogcB9Si2UUb8miy','$2a$12$bLvUgjVMiV5RXRxszl47N.',NULL,1,NULL,NULL,NULL,NULL,719748,NULL,NULL),(770070,NULL,1,0,1,NULL,'2014-12-05 00:00:00','dking',0,'$2a$12$bHjZQfyv9zR/8G1tof020.0yQtlwzAn6D9Wq2H/8hRexIJvrgeE16','$2a$12$bHjZQfyv9zR/8G1tof020.',NULL,6,NULL,NULL,NULL,NULL,770072,NULL,NULL),(3966428,NULL,0,1,0,NULL,NULL,'failccuser',0,'$2a$12$XZ4Yt9sTDLIwfJC8klyeb.3lgbkCvcFdVo8OJSKXvatukLdbjVvBa','$2a$12$XZ4Yt9sTDLIwfJC8klyeb.',NULL,1,3966429,NULL,NULL,NULL,3966433,NULL,NULL),(14178557,NULL,0,0,0,NULL,NULL,'kwashington',0,'$2a$12$tv6VjhxOQSq1qYWNFG881eJD00xyO78iXJIHkug2lorI8PgSSZMM6','$2a$12$tv6VjhxOQSq1qYWNFG881e',NULL,2,NULL,NULL,NULL,NULL,14178561,NULL,NULL),(14178576,NULL,0,0,1,NULL,'2014-10-01 00:00:00','nbradley1',0,'$2a$12$3zWP0.EfmRL.UBkdMzlUhu6vRt/4YpjrNuuhODwUA/PpCH9UYvKtO','$2a$12$3zWP0.EfmRL.UBkdMzlUhu',NULL,2,NULL,NULL,NULL,NULL,14178578,NULL,NULL),(14178611,NULL,0,0,1,NULL,NULL,'techsupport',NULL,'$2a$12$.fd7.zaH55idUaiZkDy.Jei4Q4dEeLf6bh5E1oTNAb4yqyAazu1Za','$2a$12$.fd7.zaH55idUaiZkDy.Je',NULL,1,NULL,NULL,NULL,NULL,14178613,NULL,NULL),(17702900,'2014-02-03 00:00:00',1,0,1,NULL,'2014-12-05 00:00:00','pvillarasa1',0,'$2a$12$yHcFmin.UqnYTp/e0F6n3OlKUJF.4AjR2yjEI7vBDRL7VKVxGlqae','$2a$12$yHcFmin.UqnYTp/e0F6n3O',NULL,4,NULL,NULL,NULL,NULL,17702901,NULL,NULL),(17702904,'2014-02-03 00:00:00',1,0,1,'2014-05-07 00:00:00','2014-04-29 00:00:00','svasquez1',1,'$2a$12$UfNQbitKfiVkx6c8noPDkOONdKaoiltNWUvonMHD0tqQR7O4Uf2k.','$2a$12$UfNQbitKfiVkx6c8noPDkO',NULL,2,NULL,NULL,NULL,NULL,17702905,NULL,NULL);
/*!40000 ALTER TABLE `systemuser` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `systemuser_account`
--

DROP TABLE IF EXISTS `systemuser_account`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `systemuser_account` (
  `USERS_ID` bigint(20) NOT NULL DEFAULT '0',
  `ACCOUNTS_ID` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`USERS_ID`,`ACCOUNTS_ID`),
  KEY `FK139A6D887BEA6A7E` (`ACCOUNTS_ID`),
  CONSTRAINT `FK139A6D88478FA7EF` FOREIGN KEY (`USERS_ID`) REFERENCES `systemuser` (`ID`),
  CONSTRAINT `FK139A6D887BEA6A7E` FOREIGN KEY (`ACCOUNTS_ID`) REFERENCES `account` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `systemuser_account`
--

LOCK TABLES `systemuser_account` WRITE;
/*!40000 ALTER TABLE `systemuser_account` DISABLE KEYS */;
INSERT INTO `systemuser_account` VALUES (33714,55801),(33791,55801),(55780,55801),(75494,55801),(719662,55801),(770070,55801),(3966428,55801),(14178557,55801),(14178576,55801),(17702900,55801),(17702904,55801),(55780,60071),(719573,60071),(719670,60071),(14178611,720341),(75528,720354),(75544,720354),(719573,720357),(719747,6349715);
/*!40000 ALTER TABLE `systemuser_account` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `systemuser_account_aud`
--

DROP TABLE IF EXISTS `systemuser_account_aud`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `systemuser_account_aud` (
  `REV` bigint(20) NOT NULL AUTO_INCREMENT,
  `USERS_ID` bigint(20) NOT NULL DEFAULT '0',
  `ACCOUNTS_ID` bigint(20) NOT NULL DEFAULT '0',
  `REVTYPE` decimal(3,0) DEFAULT NULL,
  PRIMARY KEY (`REV`,`USERS_ID`,`ACCOUNTS_ID`),
  CONSTRAINT `FKBE449759DF74E053` FOREIGN KEY (`REV`) REFERENCES `revinfo` (`REV`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `systemuser_account_aud`
--

LOCK TABLES `systemuser_account_aud` WRITE;
/*!40000 ALTER TABLE `systemuser_account_aud` DISABLE KEYS */;
/*!40000 ALTER TABLE `systemuser_account_aud` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `systemuser_aud`
--

DROP TABLE IF EXISTS `systemuser_aud`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `systemuser_aud` (
  `ID` bigint(20) NOT NULL DEFAULT '0',
  `REV` bigint(20) NOT NULL AUTO_INCREMENT,
  `REVTYPE` decimal(3,0) DEFAULT NULL,
  `CHANGEPASSWORD` decimal(1,0) DEFAULT NULL,
  `CONCURRENTACCESS` decimal(1,0) DEFAULT NULL,
  `CREATIONDATE` datetime DEFAULT NULL,
  `CREDITCARDATTEMPTS` bigint(20) DEFAULT NULL,
  `ISACTIVE` decimal(1,0) DEFAULT NULL,
  `LASTCREDITCARDATTEMPTDATE` datetime DEFAULT NULL,
  `LASTLOGINATTEMPTDATE` datetime DEFAULT NULL,
  `LASTLOGINDATE` datetime DEFAULT NULL,
  `LOGIN` varchar(255) DEFAULT NULL,
  `LOGINATTEMPTS` bigint(20) DEFAULT NULL,
  `PASSWORD` varchar(255) DEFAULT NULL,
  `PREVIOUSCREDITCARDATTEMPTDATE` datetime DEFAULT NULL,
  `SALT` varchar(255) DEFAULT NULL,
  `STATUS` varchar(255) DEFAULT NULL,
  `PAYMENTINFORMATION_ID` bigint(20) DEFAULT NULL,
  `PERSON_ID` bigint(20) DEFAULT NULL,
  `PASSWORDCREATIONDATE` datetime DEFAULT NULL,
  `CATALOGSELECTIONTYPE` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`,`REV`),
  KEY `FK595E3F8BDF74E053` (`REV`),
  CONSTRAINT `FK595E3F8BDF74E053` FOREIGN KEY (`REV`) REFERENCES `revinfo` (`REV`)
) ENGINE=InnoDB AUTO_INCREMENT=156609529 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `systemuser_aud`
--

LOCK TABLES `systemuser_aud` WRITE;
/*!40000 ALTER TABLE `systemuser_aud` DISABLE KEYS */;
INSERT INTO `systemuser_aud` VALUES (719573,156609526,1,0,0,NULL,NULL,1,NULL,NULL,'2015-11-12 00:00:00','admin',0,'$2a$12$BNFFtozjVovEMVLsE2AQuexQnhKPTziqnBwRGCH6.JTcprlvuTB5i',NULL,'$2a$12$BNFFtozjVovEMVLsE2AQue',NULL,NULL,719575,NULL,NULL),(719573,156609528,1,0,0,NULL,NULL,1,NULL,NULL,'2015-11-12 00:00:00','admin',0,'$2a$12$BNFFtozjVovEMVLsE2AQuexQnhKPTziqnBwRGCH6.JTcprlvuTB5i',NULL,'$2a$12$BNFFtozjVovEMVLsE2AQue',NULL,NULL,719575,NULL,NULL);
/*!40000 ALTER TABLE `systemuser_aud` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `systemuser_bulletin`
--

DROP TABLE IF EXISTS `systemuser_bulletin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `systemuser_bulletin` (
  `SYSTEMUSER_ID` bigint(20) NOT NULL DEFAULT '0',
  `BULLETINS_ID` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`SYSTEMUSER_ID`,`BULLETINS_ID`),
  KEY `FK8EEEC1AC367C0B98` (`BULLETINS_ID`),
  CONSTRAINT `FK8EEEC1AC367C0B98` FOREIGN KEY (`BULLETINS_ID`) REFERENCES `bulletin` (`ID`),
  CONSTRAINT `FK8EEEC1AC64555A3D` FOREIGN KEY (`SYSTEMUSER_ID`) REFERENCES `systemuser` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `systemuser_bulletin`
--

LOCK TABLES `systemuser_bulletin` WRITE;
/*!40000 ALTER TABLE `systemuser_bulletin` DISABLE KEYS */;
/*!40000 ALTER TABLE `systemuser_bulletin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `systemuser_bulletin_aud`
--

DROP TABLE IF EXISTS `systemuser_bulletin_aud`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `systemuser_bulletin_aud` (
  `REV` bigint(20) NOT NULL AUTO_INCREMENT,
  `SYSTEMUSER_ID` bigint(20) NOT NULL DEFAULT '0',
  `BULLETINS_ID` bigint(20) NOT NULL DEFAULT '0',
  `REVTYPE` decimal(3,0) DEFAULT NULL,
  PRIMARY KEY (`REV`,`SYSTEMUSER_ID`,`BULLETINS_ID`),
  CONSTRAINT `FK114A397DDF74E053` FOREIGN KEY (`REV`) REFERENCES `revinfo` (`REV`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `systemuser_bulletin_aud`
--

LOCK TABLES `systemuser_bulletin_aud` WRITE;
/*!40000 ALTER TABLE `systemuser_bulletin_aud` DISABLE KEYS */;
/*!40000 ALTER TABLE `systemuser_bulletin_aud` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `systemuser_rolexuser_aud`
--

DROP TABLE IF EXISTS `systemuser_rolexuser_aud`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `systemuser_rolexuser_aud` (
  `REV` bigint(20) NOT NULL AUTO_INCREMENT,
  `USER_ID` bigint(20) NOT NULL DEFAULT '0',
  `ID` bigint(20) NOT NULL DEFAULT '0',
  `REVTYPE` decimal(3,0) DEFAULT NULL,
  PRIMARY KEY (`REV`,`USER_ID`,`ID`),
  CONSTRAINT `FKAFF1FF9DF74E053` FOREIGN KEY (`REV`) REFERENCES `revinfo` (`REV`)
) ENGINE=InnoDB AUTO_INCREMENT=156609529 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `systemuser_rolexuser_aud`
--

LOCK TABLES `systemuser_rolexuser_aud` WRITE;
/*!40000 ALTER TABLE `systemuser_rolexuser_aud` DISABLE KEYS */;
INSERT INTO `systemuser_rolexuser_aud` VALUES (156609528,719573,156609527,0);
/*!40000 ALTER TABLE `systemuser_rolexuser_aud` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `taxrate`
--

DROP TABLE IF EXISTS `taxrate`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `taxrate` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `CITY` varchar(255) DEFAULT NULL,
  `CITYLOCALSALES` decimal(19,6) DEFAULT NULL,
  `CITYLOCALUSE` decimal(19,6) DEFAULT NULL,
  `CITYSALES` decimal(19,6) DEFAULT NULL,
  `CITYUSE` decimal(19,6) DEFAULT NULL,
  `COMBINEDSALES` decimal(19,6) DEFAULT NULL,
  `COMBINEDUSE` decimal(19,6) DEFAULT NULL,
  `COUNTY` varchar(255) DEFAULT NULL,
  `COUNTYFIPS` varchar(255) DEFAULT NULL,
  `COUNTYLOCALSALES` decimal(19,6) DEFAULT NULL,
  `COUNTYLOCALUSE` decimal(19,6) DEFAULT NULL,
  `COUNTYSALES` decimal(19,6) DEFAULT NULL,
  `COUNTYUSE` decimal(19,6) DEFAULT NULL,
  `EFFECTIVEDATE` datetime DEFAULT NULL,
  `EXPIREDATE` datetime DEFAULT NULL,
  `GEOCODE` varchar(255) DEFAULT NULL,
  `LOCATIONINCITY` varchar(255) DEFAULT NULL,
  `STATE` varchar(255) DEFAULT NULL,
  `STATESALES` decimal(19,6) DEFAULT NULL,
  `STATEUSE` decimal(19,6) DEFAULT NULL,
  `VERSION` bigint(20) DEFAULT NULL,
  `ZIP` varchar(255) DEFAULT NULL,
  `COUNTYDEFAULT` varchar(255) DEFAULT NULL,
  `GENERALDEFAULT` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `taxrate`
--

LOCK TABLES `taxrate` WRITE;
/*!40000 ALTER TABLE `taxrate` DISABLE KEYS */;
/*!40000 ALTER TABLE `taxrate` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `taxratelog`
--

DROP TABLE IF EXISTS `taxratelog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `taxratelog` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `ACTION` varchar(255) DEFAULT NULL,
  `IDENTIFIER` varchar(255) DEFAULT NULL,
  `STATUS` varchar(255) DEFAULT NULL,
  `TIMESTAMP` datetime DEFAULT NULL,
  `TYPE` varchar(255) DEFAULT NULL,
  `VERSION` bigint(20) DEFAULT NULL,
  `ZIP` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `TAXLOG_TIME_IDX` (`TIMESTAMP`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `taxratelog`
--

LOCK TABLES `taxratelog` WRITE;
/*!40000 ALTER TABLE `taxratelog` DISABLE KEYS */;
/*!40000 ALTER TABLE `taxratelog` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `taxtype`
--

DROP TABLE IF EXISTS `taxtype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `taxtype` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(255) DEFAULT NULL,
  `VERSION` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=31266 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `taxtype`
--

LOCK TABLES `taxtype` WRITE;
/*!40000 ALTER TABLE `taxtype` DISABLE KEYS */;
INSERT INTO `taxtype` VALUES (31260,'State Tax',0),(31261,'County Tax',0),(31262,'City Tax',0),(31263,'District Tax',0),(31264,'County Local Tax',0),(31265,'City Local Tax',0);
/*!40000 ALTER TABLE `taxtype` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `to_credential_table`
--

DROP TABLE IF EXISTS `to_credential_table`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `to_credential_table` (
  `CXMLCONFIGURATION_ID` bigint(20) NOT NULL DEFAULT '0',
  `TOCREDENTIALS_ID` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`CXMLCONFIGURATION_ID`,`TOCREDENTIALS_ID`),
  UNIQUE KEY `SYS_C0022192` (`TOCREDENTIALS_ID`),
  CONSTRAINT `FKBD1F632ABEAB89E7` FOREIGN KEY (`TOCREDENTIALS_ID`) REFERENCES `cxmlcredential` (`ID`),
  CONSTRAINT `FKBD1F632AD0B0DA57` FOREIGN KEY (`CXMLCONFIGURATION_ID`) REFERENCES `cxmlconfiguration` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `to_credential_table`
--

LOCK TABLES `to_credential_table` WRITE;
/*!40000 ALTER TABLE `to_credential_table` DISABLE KEYS */;
INSERT INTO `to_credential_table` VALUES (17708586,17708593),(72500901,72500904),(72500901,72500905),(72500901,72500906),(72500901,72500907),(102567915,102567916),(102567915,102567917),(102567915,102567919),(102567915,102567920),(102567915,102567921),(102567915,102567922),(102567915,102567923),(110038389,110038392),(110038389,110038393),(110038389,110038394),(110038389,110038395),(123726544,123726548),(17708586,133458294),(17708586,133458420),(17708586,133458774),(141078347,141078350),(141078347,141078351),(141078347,141078352),(141078347,141078353),(141078347,141078354),(141078347,141078355),(141078347,141078356),(141078347,141078357),(142745206,142745210),(143975296,143975300),(153624781,153624784);
/*!40000 ALTER TABLE `to_credential_table` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `to_credential_table_aud`
--

DROP TABLE IF EXISTS `to_credential_table_aud`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `to_credential_table_aud` (
  `REV` bigint(20) NOT NULL AUTO_INCREMENT,
  `CXMLCONFIGURATION_ID` bigint(20) NOT NULL DEFAULT '0',
  `TOCREDENTIALS_ID` bigint(20) NOT NULL DEFAULT '0',
  `REVTYPE` decimal(3,0) DEFAULT NULL,
  PRIMARY KEY (`REV`,`CXMLCONFIGURATION_ID`,`TOCREDENTIALS_ID`),
  CONSTRAINT `FK8B31EBFBDF74E053` FOREIGN KEY (`REV`) REFERENCES `revinfo` (`REV`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `to_credential_table_aud`
--

LOCK TABLES `to_credential_table_aud` WRITE;
/*!40000 ALTER TABLE `to_credential_table_aud` DISABLE KEYS */;
/*!40000 ALTER TABLE `to_credential_table_aud` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `toplevelhierarchyxcatalog`
--

DROP TABLE IF EXISTS `toplevelhierarchyxcatalog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `toplevelhierarchyxcatalog` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `ORDERING` bigint(20) DEFAULT NULL,
  `VERSION` bigint(20) DEFAULT NULL,
  `CATALOG_ID` bigint(20) DEFAULT NULL,
  `DESCRIPTION` varchar(255) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `TLHXC_CAT_IX` (`CATALOG_ID`) USING BTREE,
  CONSTRAINT `FK2B17E4731B94B57` FOREIGN KEY (`CATALOG_ID`) REFERENCES `catalog` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `toplevelhierarchyxcatalog`
--

LOCK TABLES `toplevelhierarchyxcatalog` WRITE;
/*!40000 ALTER TABLE `toplevelhierarchyxcatalog` DISABLE KEYS */;
/*!40000 ALTER TABLE `toplevelhierarchyxcatalog` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `unitofmeasure`
--

DROP TABLE IF EXISTS `unitofmeasure`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `unitofmeasure` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `VERSION` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UC_UNITOFMEASURE_1` (`NAME`)
) ENGINE=InnoDB AUTO_INCREMENT=129219809 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `unitofmeasure`
--

LOCK TABLES `unitofmeasure` WRITE;
/*!40000 ALTER TABLE `unitofmeasure` DISABLE KEYS */;
INSERT INTO `unitofmeasure` VALUES (31250,'EA',0),(31251,'BX',0),(31252,'PK',0),(31253,'DZN',0),(31254,'RL',0),(31255,'CT',0),(31256,'RM',0),(720251,'ST',0),(720252,'KT',0),(720253,'DZ',0),(720255,'PR',0),(720256,'PD',0),(720257,'PL',0),(720258,'TL',0),(720259,'BD',0),(720260,'GR',0),(720261,'DS',0),(720262,'BG',0),(720263,'HU',0),(720264,'BL',0),(3842330,'CA',0),(3842331,'CS',0),(5875298,'CL',0),(5875299,'FT',0),(5875300,'LB',0),(5875301,'M',0),(5875302,'DR',0),(5875303,'PA',0),(5875304,'HD',0),(5875305,'CD',0),(5875306,'QT',0),(5875307,'GA',0),(5875308,'CN',0),(5875309,'AS',0),(5875310,'PH',0),(5875311,'LF',0),(5875312,'TE',0),(5875313,'CX',0),(5875314,'RE',0),(5875315,'BA',0),(5875316,'BO',0),(5875317,'RD',0),(5875318,'JR',0),(5875319,'TB',0),(5875320,'CQ',0),(5875321,'SP',0),(5875322,'SF',0),(5875323,'YD',0),(5875324,'CG',0),(5875325,'SH',0),(5875326,'MK',0),(5875327,'JG',0),(5875328,'BT',0),(5875329,'MC',0),(5875330,'TO',0),(5875331,'SO',0),(5875332,'OZ',0),(5875333,'SL',0),(5875334,'GL',0),(5875335,'DC',0),(5875336,'SQ',0),(5875337,'KE',0),(5875338,'GG',0),(5875339,'GS',0),(5875340,'DI',0),(5875341,'SG',0),(5875342,'BM',0),(116936259,'GRO',0),(116936263,'SET',0),(129219806,'VL',0),(129219808,'PC',0);
/*!40000 ALTER TABLE `unitofmeasure` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `unitofmeasure_aud`
--

DROP TABLE IF EXISTS `unitofmeasure_aud`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `unitofmeasure_aud` (
  `ID` bigint(20) NOT NULL DEFAULT '0',
  `REV` bigint(20) NOT NULL AUTO_INCREMENT,
  `REVTYPE` decimal(3,0) DEFAULT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`,`REV`),
  KEY `FK8B1E7E94DF74E053` (`REV`),
  CONSTRAINT `FK8B1E7E94DF74E053` FOREIGN KEY (`REV`) REFERENCES `revinfo` (`REV`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `unitofmeasure_aud`
--

LOCK TABLES `unitofmeasure_aud` WRITE;
/*!40000 ALTER TABLE `unitofmeasure_aud` DISABLE KEYS */;
/*!40000 ALTER TABLE `unitofmeasure_aud` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `userbrmsrecord`
--

DROP TABLE IF EXISTS `userbrmsrecord`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userbrmsrecord` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `ACTIVE` decimal(1,0) DEFAULT NULL,
  `PROCESSID` bigint(20) DEFAULT NULL,
  `SESSIONID` bigint(20) DEFAULT NULL,
  `VERSION` bigint(20) DEFAULT NULL,
  `USERREQUEST_ID` bigint(20) DEFAULT NULL,
  `SNAPSHOT` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FKEA537C12D8D414D7` (`USERREQUEST_ID`),
  CONSTRAINT `FKEA537C12D8D414D7` FOREIGN KEY (`USERREQUEST_ID`) REFERENCES `userrequest` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `userbrmsrecord`
--

LOCK TABLES `userbrmsrecord` WRITE;
/*!40000 ALTER TABLE `userbrmsrecord` DISABLE KEYS */;
/*!40000 ALTER TABLE `userbrmsrecord` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `userrequest`
--

DROP TABLE IF EXISTS `userrequest`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userrequest` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `TOKEN` varchar(255) DEFAULT NULL,
  `VERSION` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `UR_TOKEN_IDX` (`TOKEN`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `userrequest`
--

LOCK TABLES `userrequest` WRITE;
/*!40000 ALTER TABLE `userrequest` DISABLE KEYS */;
/*!40000 ALTER TABLE `userrequest` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usps_regions`
--

DROP TABLE IF EXISTS `usps_regions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usps_regions` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `STATE` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `STATE_ABBR` varchar(2) NOT NULL,
  `REGION` varchar(255) NOT NULL,
  PRIMARY KEY (`ID`,`STATE`),
  UNIQUE KEY `USPS_REGIONS_STATE` (`STATE`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usps_regions`
--

LOCK TABLES `usps_regions` WRITE;
/*!40000 ALTER TABLE `usps_regions` DISABLE KEYS */;
/*!40000 ALTER TABLE `usps_regions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `validationerror`
--

DROP TABLE IF EXISTS `validationerror`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `validationerror` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `DESCRIPTION` varchar(255) DEFAULT NULL,
  `NAME` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `VERSION` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UC_VALIDATIONERROR_1` (`NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `validationerror`
--

LOCK TABLES `validationerror` WRITE;
/*!40000 ALTER TABLE `validationerror` DISABLE KEYS */;
/*!40000 ALTER TABLE `validationerror` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vendor`
--

DROP TABLE IF EXISTS `vendor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vendor` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `ACTIVATIONDATE` datetime DEFAULT NULL,
  `EXPIRATIONDATE` datetime DEFAULT NULL,
  `NAME` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `VERSION` bigint(20) DEFAULT NULL,
  `CONTACT_ID` bigint(20) DEFAULT NULL,
  `COMMUNICATIONMESSAGETYPE` varchar(255) NOT NULL,
  `COMMUNICATIONDESTINATION` varchar(255) NOT NULL,
  `CONTROLACCOUNTNUMBER` varchar(255) DEFAULT NULL,
  `REMITTO` varchar(255) DEFAULT NULL,
  `SOLOMONVENDORID` varchar(255) DEFAULT NULL,
  `PARTNERID` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UC_VENDOR_1` (`NAME`),
  KEY `FK9883916813E27D12` (`CONTACT_ID`),
  CONSTRAINT `FK9883916813E27D12` FOREIGN KEY (`CONTACT_ID`) REFERENCES `person` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=150523748 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vendor`
--

LOCK TABLES `vendor` WRITE;
/*!40000 ALTER TABLE `vendor` DISABLE KEYS */;
INSERT INTO `vendor` VALUES (32570,NULL,NULL,'Empty Vendor',0,NULL,'EMAIL','orders@americanproduct.com',NULL,NULL,NULL,'Empty Vendor'),(32571,NULL,NULL,'ECS',2,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'ECS'),(32660,NULL,NULL,'Solr vendor',0,NULL,'EMAIL','orders@americanproduct.com',NULL,NULL,NULL,'Solr vendor'),(719709,NULL,NULL,'ALLIED ELECTRONICS',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'ALLIED ELECTRONICS'),(719866,NULL,NULL,'One Time Vendor',0,NULL,'EMAIL','orders@americanproduct.com',NULL,NULL,NULL,'One Time Vendor'),(719867,NULL,NULL,'1st Quality School',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'1st Quality School'),(719868,NULL,NULL,'3 SONS SUPPLY',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'3 SONS SUPPLY'),(719869,NULL,NULL,'A.C. McGunnigle Co Inc',1,NULL,'CSV','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'A.C. McGunnigle Co Inc'),(719870,NULL,NULL,'AAR CORP',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'AAR CORP'),(719871,NULL,NULL,'AD Trophy',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'AD Trophy'),(719872,NULL,NULL,'AEG Group Inc.',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'AEG Group Inc.'),(719873,NULL,NULL,'AHI Carrier',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'AHI Carrier'),(719874,NULL,NULL,'AIR SCIENCE',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'AIR SCIENCE'),(719875,NULL,NULL,'ALLMAND',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'ALLMAND'),(719876,NULL,NULL,'AMT Pump',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'AMT Pump'),(719877,NULL,NULL,'APD Store',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'APD Store'),(719878,NULL,NULL,'AS CUTE AS A BUTTON',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'AS CUTE AS A BUTTON'),(719879,NULL,NULL,'AV Now, Inc.',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'AV Now, Inc.'),(719880,NULL,NULL,'Absolute Steel',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Absolute Steel'),(719881,NULL,NULL,'Academy of Art',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Academy of Art'),(719882,NULL,NULL,'Adorama',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Adorama'),(719883,NULL,NULL,'Airport Windstock',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Airport Windstock'),(719884,NULL,NULL,'Alaska Structures',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Alaska Structures'),(719885,NULL,NULL,'All Filters',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'All Filters'),(719886,NULL,NULL,'All-State Legal',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'All-State Legal'),(719887,NULL,NULL,'Anchor- Richey EVS',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Anchor- Richey EVS'),(719888,NULL,NULL,'Applied',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Applied'),(719889,NULL,NULL,'Applied Industrial',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Applied Industrial'),(719890,NULL,NULL,'Aroundtheoffice.com',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Aroundtheoffice.com'),(719891,NULL,NULL,'Avery',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Avery'),(719892,NULL,NULL,'Azerty',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Azerty'),(719893,NULL,NULL,'BARNETT',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'BARNETT'),(719894,NULL,NULL,'Bagsunlimited',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Bagsunlimited'),(719895,NULL,NULL,'Baker & Mitchell Company',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Baker & Mitchell Company'),(719896,NULL,NULL,'Barnes & Noble',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Barnes & Noble'),(719897,NULL,NULL,'Bergco',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Bergco'),(719898,NULL,NULL,'Best Warning Light',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Best Warning Light'),(719899,NULL,NULL,'BestBuy',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'BestBuy'),(719900,NULL,NULL,'Bluestar',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Bluestar'),(719901,NULL,NULL,'Book Factory',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Book Factory'),(719902,NULL,NULL,'Box partner.com',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Box partner.com'),(719903,NULL,NULL,'Boyds',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Boyds'),(719904,NULL,NULL,'Bren Inc.',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Bren Inc.'),(719905,NULL,NULL,'Brown Tool',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Brown Tool'),(719906,NULL,NULL,'Buckingham Manufacturing',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Buckingham Manufacturing'),(719907,NULL,NULL,'Bulbs.com',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Bulbs.com'),(719908,NULL,NULL,'Buy Two Way Radios',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Buy Two Way Radios'),(719909,NULL,NULL,'Buy.com',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Buy.com'),(719910,NULL,NULL,'CAROLINA CAT',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'CAROLINA CAT'),(719911,NULL,NULL,'CASCADE TOOLS',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'CASCADE TOOLS'),(719912,NULL,NULL,'CDW',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'CDW'),(719913,NULL,NULL,'CDWG',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'CDWG'),(719914,NULL,NULL,'CHRISTMAS CENTRAL',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'CHRISTMAS CENTRAL'),(719915,NULL,NULL,'CIM USA Inc',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'CIM USA Inc'),(719916,NULL,NULL,'CMCI',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'CMCI'),(719917,NULL,NULL,'CONSUTECH',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'CONSUTECH'),(719918,NULL,NULL,'CREATIVE TENT INTERNATONAL',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'CREATIVE TENT INTERNATONAL'),(719919,NULL,NULL,'CROWN AWARDS',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'CROWN AWARDS'),(719920,NULL,NULL,'CROWN CONTROLS',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'CROWN CONTROLS'),(719921,NULL,NULL,'CSC INDUSTRIAL SALES & SERVICE LLC',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'CSC INDUSTRIAL SALES & SERVICE LLC'),(719922,NULL,NULL,'CaptiveAire',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'CaptiveAire'),(719923,NULL,NULL,'Carolina Cartridge Systems (CCS)',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Carolina Cartridge Systems (CCS)'),(719924,NULL,NULL,'Caromark',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Caromark'),(719925,NULL,NULL,'Carrier',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Carrier'),(719926,NULL,NULL,'Carrier Enterprise',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Carrier Enterprise'),(719927,NULL,NULL,'Caterpillar',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Caterpillar'),(719928,NULL,NULL,'Centerlen Services',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Centerlen Services'),(719929,NULL,NULL,'Clamshell',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Clamshell'),(719930,NULL,NULL,'Classic Office Products',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Classic Office Products'),(719931,NULL,NULL,'Clean It Supply',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Clean It Supply'),(719932,NULL,NULL,'Codemicro.com',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Codemicro.com'),(719933,NULL,NULL,'Coders Central',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Coders Central'),(719934,NULL,NULL,'Compu Toners',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Compu Toners'),(719935,NULL,NULL,'Control Specialties',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Control Specialties'),(719936,NULL,NULL,'Cregger Company',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Cregger Company'),(719937,NULL,NULL,'Culinary Depot',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Culinary Depot'),(719938,NULL,NULL,'D&L Parts Co',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'D&L Parts Co'),(719939,NULL,NULL,'DAPARAK Inc',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'DAPARAK Inc'),(719940,NULL,NULL,'DEMCO',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'DEMCO'),(719941,NULL,NULL,'DH Pump',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'DH Pump'),(719942,NULL,NULL,'DOI Direct',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'DOI Direct'),(719943,NULL,NULL,'Dakota Air Parts Intl., Inc.',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Dakota Air Parts Intl., Inc.'),(719944,NULL,NULL,'Davtech.com',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Davtech.com'),(719945,NULL,NULL,'DayTimer',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'DayTimer'),(719946,NULL,NULL,'Dell',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Dell'),(719947,NULL,NULL,'Diamond Power Int\'l Inc.',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Diamond Power Int\'l Inc.'),(719948,NULL,NULL,'Diesel Specialist',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Diesel Specialist'),(719949,NULL,NULL,'Digital Telecom',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Digital Telecom'),(719950,NULL,NULL,'Discount Roofing',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Discount Roofing'),(719951,NULL,NULL,'Discounted Wheel Warehouse',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Discounted Wheel Warehouse'),(719952,NULL,NULL,'Dougherty Equipment',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Dougherty Equipment'),(719953,NULL,NULL,'Dupli',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Dupli'),(719954,NULL,NULL,'Dynamic',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Dynamic'),(719955,NULL,NULL,'E cost',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'E cost'),(719956,NULL,NULL,'E-pest Solutions',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'E-pest Solutions'),(719957,NULL,NULL,'EMEDCO',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'EMEDCO'),(719958,NULL,NULL,'EMKA USA',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'EMKA USA'),(719959,NULL,NULL,'Eagle Press',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Eagle Press'),(719960,NULL,NULL,'Eckhart Construction Services Inc',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Eckhart Construction Services Inc'),(719961,NULL,NULL,'Eco-Safe',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Eco-Safe'),(719962,NULL,NULL,'Ecom Office Supplies',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Ecom Office Supplies'),(719963,NULL,NULL,'Edarley',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Edarley'),(719964,NULL,NULL,'Electriduct Inc',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Electriduct Inc'),(719965,NULL,NULL,'Ergostore',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Ergostore'),(719966,NULL,NULL,'Everyday Innovations',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Everyday Innovations'),(719967,NULL,NULL,'FARGO',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'FARGO'),(719968,NULL,NULL,'FERGUSON',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'FERGUSON'),(719969,NULL,NULL,'FG WILSON',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'FG WILSON'),(719970,NULL,NULL,'FLAGS UNLIMITED INC',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'FLAGS UNLIMITED INC'),(719971,NULL,NULL,'FOAMBOARDSOURCE',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'FOAMBOARDSOURCE'),(719972,NULL,NULL,'FRS Asheville',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'FRS Asheville'),(719973,NULL,NULL,'Fassride.com',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Fassride.com'),(719974,NULL,NULL,'Fastener Mart',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Fastener Mart'),(719975,NULL,NULL,'Fed Safes',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Fed Safes'),(719976,NULL,NULL,'(DO NOT USE) Ferguson ',2,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'(DO NOT USE) Ferguson '),(719977,NULL,NULL,'Fire Service Books',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Fire Service Books'),(719978,NULL,NULL,'Firefighters Bookstore',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Firefighters Bookstore'),(719979,NULL,NULL,'Franklin Planner',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Franklin Planner'),(719980,NULL,NULL,'Fresh Water Systems',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Fresh Water Systems'),(719981,NULL,NULL,'Fry Specialty',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Fry Specialty'),(719982,NULL,NULL,'Full Circle Padding',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Full Circle Padding'),(719983,NULL,NULL,'Fuller.com',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Fuller.com'),(719984,NULL,NULL,'GM Parts Giant',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'GM Parts Giant'),(719985,NULL,NULL,'GR Office Supplies',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'GR Office Supplies'),(719986,NULL,NULL,'Gardner Benoit',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Gardner Benoit'),(719987,NULL,NULL,'Garvin',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Garvin'),(719988,NULL,NULL,'Getz Mfr',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Getz Mfr'),(719989,NULL,NULL,'Global Industrial',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Global Industrial'),(719990,NULL,NULL,'Grainger',2,NULL,'EMAIL','ordermgmt@americanproduct.com,specialorders@americanproduct.com',NULL,NULL,NULL,'Grainger'),(719991,NULL,NULL,'Gray & Holt, Inc',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Gray & Holt, Inc'),(719992,NULL,NULL,'Graybar',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Graybar'),(719993,NULL,NULL,'Grizzly Industrial Inc',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Grizzly Industrial Inc'),(719994,NULL,NULL,'HACH COMPANY',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'HACH COMPANY'),(719995,NULL,NULL,'HD Supply',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'HD Supply'),(719996,NULL,NULL,'HISCO',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'HISCO'),(719997,NULL,NULL,'HUBERT',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'HUBERT'),(719998,NULL,NULL,'Hardware Store',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Hardware Store'),(719999,NULL,NULL,'Harrington Industrial Supply',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Harrington Industrial Supply'),(720000,NULL,NULL,'Headset Adapter',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Headset Adapter'),(720001,NULL,NULL,'Hewlett Packard',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Hewlett Packard'),(720002,NULL,NULL,'Hillman Group',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Hillman Group'),(720003,NULL,NULL,'ICM Corp',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'ICM Corp'),(720004,NULL,NULL,'ID Labels, Inc.',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'ID Labels, Inc.'),(720005,NULL,NULL,'ID Supply',0,NULL,'EMAIL','orders@americanproduct.com',NULL,NULL,NULL,'ID Supply'),(720006,NULL,NULL,'IMEX',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'IMEX'),(720007,NULL,NULL,'INTERNATIONAL PROCESS PLANT',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'INTERNATIONAL PROCESS PLANT'),(720008,NULL,NULL,'IT CONNECTION',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'IT CONNECTION'),(720009,NULL,NULL,'Image Star',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Image Star'),(720010,NULL,NULL,'Industrial Equipment Company',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Industrial Equipment Company'),(720011,NULL,NULL,'Industrial Safety',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Industrial Safety'),(720012,NULL,NULL,'Instrumart',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Instrumart'),(720013,NULL,NULL,'Interlight',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Interlight'),(720014,NULL,NULL,'Interline',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Interline'),(720015,NULL,NULL,'Intermec',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Intermec'),(720016,NULL,NULL,'International Configurations Inc',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'International Configurations Inc'),(720017,NULL,NULL,'Interstate Company',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Interstate Company'),(720018,NULL,NULL,'Interstate Equipment Co.',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Interstate Equipment Co.'),(720019,NULL,NULL,'IronCompany.com',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'IronCompany.com'),(720020,NULL,NULL,'JMS Online',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'JMS Online'),(720021,NULL,NULL,'James River Equipment',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'James River Equipment'),(720022,NULL,NULL,'Jelinek',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Jelinek'),(720023,NULL,NULL,'Johnstone Supply',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Johnstone Supply'),(720024,NULL,NULL,'Jones & Barlett Learning',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Jones & Barlett Learning'),(720025,NULL,NULL,'Jordan Reduction Solutions',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Jordan Reduction Solutions'),(720026,NULL,NULL,'K.L. Security',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'K.L. Security'),(720027,NULL,NULL,'Kingsway Industries, Inc.',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Kingsway Industries, Inc.'),(720028,NULL,NULL,'Kipper Tool Company',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Kipper Tool Company'),(720029,NULL,NULL,'Knapheide',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Knapheide'),(720030,NULL,NULL,'LAWSON PRODUCTS',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'LAWSON PRODUCTS'),(720031,NULL,NULL,'LC Industries',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'LC Industries'),(720032,NULL,NULL,'LEEDSWORLDREFILLS',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'LEEDSWORLDREFILLS'),(720033,NULL,NULL,'LENEAVE MACHINERY & SUPPLY',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'LENEAVE MACHINERY & SUPPLY'),(720034,NULL,NULL,'LSS',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'LSS'),(720035,NULL,NULL,'Labels, Inc',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Labels, Inc'),(720036,NULL,NULL,'Life Fitness',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Life Fitness'),(720037,NULL,NULL,'Lighting Supply',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Lighting Supply'),(720038,NULL,NULL,'Linder Industrial Machinery',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Linder Industrial Machinery'),(720039,NULL,NULL,'Lloydes of Indiana',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Lloydes of Indiana'),(720040,NULL,NULL,'Locke Well and Pump Company',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Locke Well and Pump Company'),(720041,NULL,NULL,'Lowes',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Lowes'),(720042,NULL,NULL,'MAGNUM',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'MAGNUM'),(720043,NULL,NULL,'MES Fire',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'MES Fire'),(720044,NULL,NULL,'MIDATLANTIC',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'MIDATLANTIC'),(720045,NULL,NULL,'MOTION INDUSTRIES',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'MOTION INDUSTRIES'),(720046,NULL,NULL,'MSC',2,NULL,'EMAIL','ordermgmt@americanproduct.com',NULL,NULL,NULL,'MSC'),(720047,NULL,NULL,'Mancomm',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Mancomm'),(720048,NULL,NULL,'Marfield',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Marfield'),(720049,NULL,NULL,'Marindus Co. Inc',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Marindus Co. Inc'),(720050,NULL,NULL,'Markmaster',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Markmaster'),(720051,NULL,NULL,'Matjack',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Matjack'),(720052,NULL,NULL,'McKenney Salinas- Kawasaki',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'McKenney Salinas- Kawasaki'),(720053,NULL,NULL,'McMaster-Carr',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'McMaster-Carr'),(720054,NULL,NULL,'McNaughton McKay',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'McNaughton McKay'),(720055,NULL,NULL,'Medexsupply.com',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Medexsupply.com'),(720056,NULL,NULL,'MilSpec Products, Inc',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'MilSpec Products, Inc'),(720057,NULL,NULL,'Mini Mania',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Mini Mania'),(720058,NULL,NULL,'Morrill',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Morrill'),(720059,NULL,NULL,'Musician\'s Friend',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Musician\'s Friend'),(720061,NULL,NULL,'NATIONAL GYM SUPPLY',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'NATIONAL GYM SUPPLY'),(720062,NULL,NULL,'NDT Systems',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'NDT Systems'),(720063,NULL,NULL,'Nail Gun Depot',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Nail Gun Depot'),(720064,NULL,NULL,'Nashua',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Nashua'),(720065,NULL,NULL,'National Builder Supply',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'National Builder Supply'),(720066,NULL,NULL,'National Business Furniture',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'National Business Furniture'),(720068,NULL,NULL,'Nelson',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Nelson'),(720069,NULL,NULL,'Network Element',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Network Element'),(720070,NULL,NULL,'Neve\'s Uniforms & Equipment',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Neve\'s Uniforms & Equipment'),(720071,NULL,NULL,'NewEgg',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'NewEgg'),(720072,NULL,NULL,'Newark',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Newark'),(720073,NULL,NULL,'Newark Element 14',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Newark Element 14'),(720074,NULL,NULL,'Newton\'s Fire & Safety Equipment',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Newton\'s Fire & Safety Equipment'),(720075,NULL,NULL,'Norman Camera',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Norman Camera'),(720076,NULL,NULL,'North Shore',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'North Shore'),(720077,NULL,NULL,'Northern Safety Industry',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Northern Safety Industry'),(720078,NULL,NULL,'NovaVision',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'NovaVision'),(720079,NULL,NULL,'O\'Reilly Auto Parts',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'O\'Reilly Auto Parts'),(720080,NULL,NULL,'OFFICE STAR PRODUCTS',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'OFFICE STAR PRODUCTS'),(720081,NULL,NULL,'ONLINE SPORTS',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'ONLINE SPORTS'),(720082,NULL,NULL,'ORS NASCO',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'ORS NASCO'),(720083,NULL,NULL,'One Source Industries',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'One Source Industries'),(720084,NULL,NULL,'OsteoMed',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'OsteoMed'),(720085,NULL,NULL,'PARTS TOWN',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'PARTS TOWN'),(720086,NULL,NULL,'PWS Laundry',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'PWS Laundry'),(720087,NULL,NULL,'Paddle Palace',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Paddle Palace'),(720088,NULL,NULL,'Parts Tree',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Parts Tree'),(720089,NULL,NULL,'Patriot Supply',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Patriot Supply'),(720090,NULL,NULL,'Pegasus Auto Racing',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Pegasus Auto Racing'),(720091,NULL,NULL,'Pioneer Industries, Inc.',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Pioneer Industries, Inc.'),(720092,NULL,NULL,'Pitney Bowes',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Pitney Bowes'),(720093,NULL,NULL,'Polaroid.com',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Polaroid.com'),(720094,NULL,NULL,'Pollard Water',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Pollard Water'),(720095,NULL,NULL,'Popcorn Supply Company',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Popcorn Supply Company'),(720096,NULL,NULL,'Presco',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Presco'),(720097,NULL,NULL,'Pressure Washers Direct',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Pressure Washers Direct'),(720098,NULL,NULL,'Procure Inc.',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Procure Inc.'),(720099,NULL,NULL,'Product for Industry',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Product for Industry'),(720100,NULL,NULL,'Provantage',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Provantage'),(720101,NULL,NULL,'Pumps and Tanks',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Pumps and Tanks'),(720102,NULL,NULL,'Pureland Supply',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Pureland Supply'),(720103,NULL,NULL,'R&B Wire Products',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'R&B Wire Products'),(720104,NULL,NULL,'RAPHIL USA',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'RAPHIL USA'),(720105,NULL,NULL,'RCS',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'RCS'),(720106,NULL,NULL,'RCS Houston',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'RCS Houston'),(720107,NULL,NULL,'ROCKLAND LAUNDRY',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'ROCKLAND LAUNDRY'),(720108,NULL,NULL,'Reefer Parts',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Reefer Parts'),(720109,NULL,NULL,'Rep Fitness',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Rep Fitness'),(720110,NULL,NULL,'Resturant Equipment',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Resturant Equipment'),(720111,NULL,NULL,'Rhino Linings Corporation',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Rhino Linings Corporation'),(720112,NULL,NULL,'Ring Power Crane',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Ring Power Crane'),(720113,NULL,NULL,'Riveer Environmental',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Riveer Environmental'),(720114,NULL,NULL,'River Equipment',0,NULL,'EMAIL','orders@americanproduct.com',NULL,NULL,NULL,'River Equipment'),(720115,NULL,NULL,'Rons Home and Hardware',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Rons Home and Hardware'),(720116,NULL,NULL,'Ryan Herco Flow Systems',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Ryan Herco Flow Systems'),(720117,NULL,NULL,'S & G Enterprises',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'S & G Enterprises'),(720118,NULL,NULL,'S&G Enterprises, Inc.',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'S&G Enterprises, Inc.'),(720119,NULL,NULL,'SARCOM',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'SARCOM'),(720120,NULL,NULL,'SEM Direct Online Store',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'SEM Direct Online Store'),(720121,NULL,NULL,'SIGNWAREHOUSE',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'SIGNWAREHOUSE'),(720122,NULL,NULL,'SNAP ON',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'SNAP ON'),(720123,NULL,NULL,'SP Richards',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'SP Richards'),(720124,NULL,NULL,'Safari Ltd.',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Safari Ltd.'),(720125,NULL,NULL,'Scales Galore',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Scales Galore'),(720126,NULL,NULL,'Schilling Supply',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Schilling Supply'),(720127,NULL,NULL,'School Outfitters',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'School Outfitters'),(720128,NULL,NULL,'Sea Box, Inc.',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Sea Box, Inc.'),(720129,NULL,NULL,'Securall',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Securall'),(720130,NULL,NULL,'Seneca Companies',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Seneca Companies'),(720131,NULL,NULL,'Shelby Equipment',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Shelby Equipment'),(720132,NULL,NULL,'Shop ETS Online',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Shop ETS Online'),(720133,NULL,NULL,'Southeast Industrial Equipment Inc',2,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Southeast Industrial Equipment Inc'),(720134,NULL,NULL,'Southwest Heater & Controls',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Southwest Heater & Controls'),(720135,NULL,NULL,'Specialist ID',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Specialist ID'),(720136,NULL,NULL,'Sportsmith',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Sportsmith'),(720137,NULL,NULL,'St. Regis Crystal',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'St. Regis Crystal'),(720138,NULL,NULL,'Stamp Xpress',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Stamp Xpress'),(720139,NULL,NULL,'Stanley',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Stanley'),(720140,NULL,NULL,'Staples',2,NULL,'CXML','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Staples'),(720141,NULL,NULL,'Summit Parts',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Summit Parts'),(720142,NULL,NULL,'Summit Sign Safety',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Summit Sign Safety'),(720143,NULL,NULL,'Superior Diesel',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Superior Diesel'),(720144,NULL,NULL,'TEAM CHARLOTTE',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'TEAM CHARLOTTE'),(720145,NULL,NULL,'TESSCO',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'TESSCO'),(720146,NULL,NULL,'TITAN WIRELESS',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'TITAN WIRELESS'),(720147,NULL,NULL,'TMS South',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'TMS South'),(720148,NULL,NULL,'TUFF STUFF',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'TUFF STUFF'),(720149,NULL,NULL,'Targets Online',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Targets Online'),(720150,NULL,NULL,'Tech Data',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Tech Data'),(720151,NULL,NULL,'Tech Depot',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Tech Depot'),(720152,NULL,NULL,'Technical Concept',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Technical Concept'),(720153,NULL,NULL,'Techonweb.com',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Techonweb.com'),(720154,NULL,NULL,'Texas Marking Products',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Texas Marking Products'),(720155,NULL,NULL,'The Compliance Center',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'The Compliance Center'),(720156,NULL,NULL,'Thermal Resources Sales, Inc.',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Thermal Resources Sales, Inc.'),(720157,NULL,NULL,'Thetoweldepot.com',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Thetoweldepot.com'),(720158,NULL,NULL,'Thompson & Little',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Thompson & Little'),(720159,NULL,NULL,'Tiger Foam',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Tiger Foam'),(720160,NULL,NULL,'Tonerandinksolution.com',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Tonerandinksolution.com'),(720161,NULL,NULL,'Touchboards',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Touchboards'),(720162,NULL,NULL,'Toyota Parts Zone',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Toyota Parts Zone'),(720163,NULL,NULL,'TrendShredders.com',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'TrendShredders.com'),(720164,NULL,NULL,'Tri Max',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Tri Max'),(720165,NULL,NULL,'Trophy Depot',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Trophy Depot'),(720166,NULL,NULL,'Tundra Specialties',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Tundra Specialties'),(720167,NULL,NULL,'ULINE',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'ULINE'),(720168,NULL,NULL,'USB Memory Direct',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'USB Memory Direct'),(720169,NULL,NULL,'UTTC-Mega Depot',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'UTTC-Mega Depot'),(720170,NULL,NULL,'Ultimate washer, INC.',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Ultimate washer, INC.'),(720171,NULL,NULL,'United Solutions',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'United Solutions'),(720172,NULL,NULL,'USSCO',1,NULL,'EDI','none',NULL,NULL,NULL,'USSCO'),(720173,NULL,NULL,'VLR COMMUNICATIONS',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'VLR COMMUNICATIONS'),(720174,NULL,NULL,'VQV Group',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'VQV Group'),(720175,NULL,NULL,'VacSystem',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'VacSystem'),(720176,NULL,NULL,'Venting Pipe',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Venting Pipe'),(720177,NULL,NULL,'WB Parts',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'WB Parts'),(720178,NULL,NULL,'WESCO',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'WESCO'),(720179,NULL,NULL,'WHITMAN CONTROLS',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'WHITMAN CONTROLS'),(720180,NULL,NULL,'WIREWORKS',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'WIREWORKS'),(720181,NULL,NULL,'WMPCS',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'WMPCS'),(720182,NULL,NULL,'Walmart',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Walmart'),(720183,NULL,NULL,'Wares Direct',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Wares Direct'),(720184,NULL,NULL,'Water Meters',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Water Meters'),(720185,NULL,NULL,'Water Purification Consultants, Inc.',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Water Purification Consultants, Inc.'),(720186,NULL,NULL,'Weigh South',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Weigh South'),(720187,NULL,NULL,'White Cap (HD)',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'White Cap (HD)'),(720188,NULL,NULL,'Widget Supply',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Widget Supply'),(720189,NULL,NULL,'Winzer',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Winzer'),(720190,NULL,NULL,'Workflow One',3,NULL,'EMAIL','apdnotices@americanproduct.com',NULL,NULL,NULL,'Workflow One'),(720191,NULL,NULL,'World Cord Sets Inc',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'World Cord Sets Inc'),(720192,NULL,NULL,'XEROX',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'XEROX'),(720193,NULL,NULL,'Xpedex',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Xpedex'),(720195,NULL,NULL,'Zmodo',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Zmodo'),(720196,NULL,NULL,'salestores.com',1,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'salestores.com'),(720381,NULL,NULL,'Elite Creative Solutions',1,NULL,'EMAIL','teamphoenix@americanproduct.com',NULL,NULL,NULL,'Elite Creative Solutions'),(720382,NULL,NULL,'Supplies Network',2,NULL,'EMAIL','ordermgmt@americanproduct.com,specialorders@americanproduct.com',NULL,NULL,NULL,'Supplies Network'),(720397,NULL,NULL,'Zoro Tools',0,NULL,'EMAIL','orders@americanproduct.com',NULL,NULL,NULL,'Zoro Tools'),(731724,NULL,NULL,'American Product Distributors',9,NULL,'CSV','phyllishall@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'American Product Distributors'),(1440161,'2013-10-01 00:00:00','2013-10-31 00:00:00','Products For Industry',2,NULL,'EMAIL','orders@americanproduct.com',NULL,NULL,NULL,'Products For Industry'),(1534540,NULL,NULL,'PFI',1,NULL,'EMAIL','orders@americanproduct.com',NULL,NULL,NULL,'PFI'),(7717434,NULL,NULL,'TEST',0,NULL,'EMAIL','orders@americanproduct.com',NULL,NULL,NULL,'TEST'),(7717706,NULL,NULL,'TWC DEMO',0,NULL,'EMAIL','orders@americanproduct.com',NULL,NULL,NULL,'TWC DEMO'),(7853613,NULL,NULL,'TWC DEMO FINAL',3,NULL,'EMAIL','teamphoenix@americanproduct.com',NULL,NULL,NULL,'TWC DEMO FINAL'),(14178617,NULL,NULL,'Barcodes',0,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Barcodes'),(14178618,NULL,NULL,'Builder Depot',0,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Builder Depot'),(14179144,NULL,NULL,'Charlotte Truck Center',0,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Charlotte Truck Center'),(14179145,NULL,NULL,'Chassis King',0,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Chassis King'),(14179146,NULL,NULL,'diesel',0,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'diesel'),(14179147,NULL,NULL,'Fitness Repair Parts',0,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Fitness Repair Parts'),(14179148,NULL,NULL,'Governor Control Systems',0,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Governor Control Systems'),(14179149,NULL,NULL,'HVAC PLUS',0,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'HVAC PLUS'),(14179150,NULL,NULL,'Heritage Food Service Equipment',0,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Heritage Food Service Equipment'),(14179151,NULL,NULL,'Hoist Fitness Systems',0,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Hoist Fitness Systems'),(14179152,NULL,NULL,'Hose Warehouse',0,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Hose Warehouse'),(14179153,NULL,NULL,'ITSimplify',0,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'ITSimplify'),(14179154,NULL,NULL,'Industrial Partners',0,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Industrial Partners'),(14179155,NULL,NULL,'Laminator.com',0,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Laminator.com'),(14179156,NULL,NULL,'Matrix Fitness',0,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Matrix Fitness'),(14179157,NULL,NULL,'Med Fit Systems',0,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Med Fit Systems'),(14179158,NULL,NULL,'Media Supply',0,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Media Supply'),(14179159,NULL,NULL,'NY Replacement Parts',0,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'NY Replacement Parts'),(14179160,NULL,NULL,'PRESSURE WASHER PARTS.COM',0,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'PRESSURE WASHER PARTS.COM'),(14179161,NULL,NULL,'Reflect-a-life',0,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Reflect-a-life'),(14179162,NULL,NULL,'SnappyPopcorn.Com',0,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'SnappyPopcorn.Com'),(14179163,NULL,NULL,'United Stationers',1,NULL,'EMAIL','ordermgmt@americanproduct.com',NULL,NULL,NULL,'United Stationers'),(14179164,NULL,NULL,'White\'s International Truck',0,NULL,'EMAIL','specialorders@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'White\'s International Truck'),(17703796,NULL,NULL,'4 Imprint',0,NULL,'EMAIL','ordermgmt@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'4 Imprint'),(17704623,NULL,NULL,'Hanna Instruments',0,NULL,'EMAIL','ordermgmt@americanproduct.com,teamphoenix@americanproduct.com',NULL,NULL,NULL,'Hanna Instruments'),(21230198,NULL,NULL,'Dell Inc.',0,NULL,'EMAIL','ordermgmt@americanproduct.com',NULL,NULL,NULL,'Dell Inc.'),(39032725,NULL,NULL,'DM TEST',0,NULL,'EMAIL','donaldmangana@americanproduct.com',NULL,NULL,NULL,'DM TEST'),(39303594,NULL,NULL,'United Refrigeration Inc.',0,NULL,'EMAIL','teamphoenix@americanproduct.com',NULL,NULL,NULL,'United Refrigeration Inc.'),(44254478,NULL,NULL,'Staples Custom Orders',0,NULL,'EMAIL','StephanieVasquez@americanproduct.com',NULL,NULL,NULL,'Staples Custom Orders'),(70239533,NULL,NULL,'APD Manual MRO',0,NULL,'EMAIL','ordermgmt@americanproduct.com',NULL,NULL,NULL,'APD Manual MRO'),(75744891,NULL,NULL,'Southwest Mobile Storage, Inc.',0,NULL,'EMAIL','ordermgmt@americanproduct.com',NULL,NULL,NULL,'Southwest Mobile Storage, Inc.'),(83769560,NULL,NULL,'Broadway Lights',0,NULL,'EMAIL','ordermgmt@americanproduct.com',NULL,NULL,NULL,'Broadway Lights'),(84115825,NULL,NULL,'Industrial Air Power',0,NULL,'EMAIL','ordermgmt@americanproduct.com',NULL,NULL,NULL,'Industrial Air Power'),(106340683,NULL,NULL,'Vindum Engineering, Inc',0,NULL,'EMAIL','ordermgmt@americanproduct.com',NULL,NULL,NULL,'Vindum Engineering, Inc'),(106409715,NULL,NULL,'CKE Parts',0,NULL,'EMAIL','ordermgmt@americanproduct.com',NULL,NULL,NULL,'CKE Parts'),(106409717,NULL,NULL,'DC Coffee products',0,NULL,'EMAIL','ordermgmt@americanproduct.com',NULL,NULL,NULL,'DC Coffee products'),(117469195,NULL,NULL,'Snappy Popcorn',0,NULL,'EMAIL','ordermgmt@americanproduct.com',NULL,NULL,NULL,'Snappy Popcorn'),(117503682,NULL,NULL,'Super Media Store',0,NULL,'EMAIL','ordermgmt@americanproduct.com',NULL,NULL,NULL,'Super Media Store'),(119360564,NULL,NULL,'Quality Logo Products',0,NULL,'EMAIL','ordermgmt@americanproduct.com',NULL,NULL,NULL,'Quality Logo Products'),(123920573,NULL,NULL,'National Tool Warehouse ',0,NULL,'EMAIL','ordermgmt@americanproduct.com',NULL,NULL,NULL,'National Tool Warehouse '),(124917614,NULL,NULL,'ORS',0,NULL,'EMAIL','ordermgmt@americanproduct.com',NULL,NULL,NULL,'ORS'),(139794112,NULL,NULL,'Shoplet',0,NULL,'EMAIL','ordermgmt@americanproduct.com',NULL,NULL,NULL,'Shoplet'),(139868297,NULL,NULL,'CHR2 United Refrigeration Inc.',0,NULL,'EMAIL','ordermgmt@americanproduct.com',NULL,NULL,NULL,'CHR2 United Refrigeration Inc.'),(140486737,NULL,NULL,'Home Security Store',0,NULL,'EMAIL','ordermgmt@americanproduct.com',NULL,NULL,NULL,'Home Security Store'),(140496128,NULL,NULL,'AEC Carolina',0,NULL,'EMAIL','ordermgmt@americanproduct.com',NULL,NULL,NULL,'AEC Carolina'),(143929836,NULL,NULL,'Any Promo',0,NULL,'EMAIL','ordermgmt@americanproduct.com',NULL,NULL,NULL,'Any Promo'),(143929838,NULL,NULL,'Displays 2 Go',0,NULL,'EMAIL','ordermgmt@americanproduct.com',NULL,NULL,NULL,'Displays 2 Go'),(144047306,NULL,NULL,'American Clean State',0,NULL,'EMAIL','ordermgmt@americanproduct.com',NULL,NULL,NULL,'American Clean State'),(144164472,NULL,NULL,'The Business Center',0,NULL,'EMAIL','ordermgmt@americanproduct.com',NULL,NULL,NULL,'The Business Center'),(145918740,NULL,NULL,'Latta Equipment',0,NULL,'EMAIL','ordermgmt@americanproduct.com',NULL,NULL,NULL,'Latta Equipment'),(148121032,NULL,NULL,'Manual Order- Marfield',0,NULL,'EMAIL','apdnotices@americanproduct.com',NULL,NULL,NULL,'Manual Order- Marfield'),(150523747,NULL,NULL,'Office Link',0,NULL,'EMAIL','ordermgmt@americanproduct.com',NULL,NULL,NULL,'Office Link');
/*!40000 ALTER TABLE `vendor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vendor_address_aud`
--

DROP TABLE IF EXISTS `vendor_address_aud`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vendor_address_aud` (
  `REV` bigint(20) NOT NULL AUTO_INCREMENT,
  `VENDOR_ID` bigint(20) NOT NULL DEFAULT '0',
  `ID` bigint(20) NOT NULL DEFAULT '0',
  `REVTYPE` decimal(3,0) DEFAULT NULL,
  PRIMARY KEY (`REV`,`VENDOR_ID`,`ID`),
  CONSTRAINT `FKF42F6B4EDF74E053` FOREIGN KEY (`REV`) REFERENCES `revinfo` (`REV`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vendor_address_aud`
--

LOCK TABLES `vendor_address_aud` WRITE;
/*!40000 ALTER TABLE `vendor_address_aud` DISABLE KEYS */;
/*!40000 ALTER TABLE `vendor_address_aud` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vendor_aud`
--

DROP TABLE IF EXISTS `vendor_aud`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vendor_aud` (
  `ID` bigint(20) NOT NULL DEFAULT '0',
  `REV` bigint(20) NOT NULL AUTO_INCREMENT,
  `REVTYPE` decimal(3,0) DEFAULT NULL,
  `ACTIVATIONDATE` datetime DEFAULT NULL,
  `COMMUNICATIONDESTINATION` varchar(255) DEFAULT NULL,
  `COMMUNICATIONMESSAGETYPE` varchar(255) DEFAULT NULL,
  `EXPIRATIONDATE` datetime DEFAULT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `CONTACT_ID` bigint(20) DEFAULT NULL,
  `CONTROLACCOUNTNUMBER` varchar(255) DEFAULT NULL,
  `REMITTO` varchar(255) DEFAULT NULL,
  `SOLOMONVENDORID` varchar(255) DEFAULT NULL,
  `PARTNERID` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`,`REV`),
  KEY `FKA038CB39DF74E053` (`REV`),
  CONSTRAINT `FKA038CB39DF74E053` FOREIGN KEY (`REV`) REFERENCES `revinfo` (`REV`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vendor_aud`
--

LOCK TABLES `vendor_aud` WRITE;
/*!40000 ALTER TABLE `vendor_aud` DISABLE KEYS */;
/*!40000 ALTER TABLE `vendor_aud` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vendor_communicationmethod`
--

DROP TABLE IF EXISTS `vendor_communicationmethod`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vendor_communicationmethod` (
  `VENDORS_ID` bigint(20) NOT NULL DEFAULT '0',
  `COMMUNICATIONMETHODS_ID` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`VENDORS_ID`,`COMMUNICATIONMETHODS_ID`),
  KEY `FK884AF6E0F3928A32` (`COMMUNICATIONMETHODS_ID`),
  CONSTRAINT `FK884AF6E03F37A71A` FOREIGN KEY (`VENDORS_ID`) REFERENCES `vendor` (`ID`),
  CONSTRAINT `FK884AF6E0F3928A32` FOREIGN KEY (`COMMUNICATIONMETHODS_ID`) REFERENCES `communicationmethod` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vendor_communicationmethod`
--

LOCK TABLES `vendor_communicationmethod` WRITE;
/*!40000 ALTER TABLE `vendor_communicationmethod` DISABLE KEYS */;
INSERT INTO `vendor_communicationmethod` VALUES (1440161,1510046);
/*!40000 ALTER TABLE `vendor_communicationmethod` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vendor_communicationmethod_aud`
--

DROP TABLE IF EXISTS `vendor_communicationmethod_aud`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vendor_communicationmethod_aud` (
  `REV` bigint(20) NOT NULL AUTO_INCREMENT,
  `VENDORS_ID` bigint(20) NOT NULL DEFAULT '0',
  `COMMUNICATIONMETHODS_ID` bigint(20) NOT NULL DEFAULT '0',
  `REVTYPE` decimal(3,0) DEFAULT NULL,
  PRIMARY KEY (`REV`,`VENDORS_ID`,`COMMUNICATIONMETHODS_ID`),
  CONSTRAINT `FKEA60B4B1DF74E053` FOREIGN KEY (`REV`) REFERENCES `revinfo` (`REV`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vendor_communicationmethod_aud`
--

LOCK TABLES `vendor_communicationmethod_aud` WRITE;
/*!40000 ALTER TABLE `vendor_communicationmethod_aud` DISABLE KEYS */;
/*!40000 ALTER TABLE `vendor_communicationmethod_aud` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vendor_phonenumber_aud`
--

DROP TABLE IF EXISTS `vendor_phonenumber_aud`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vendor_phonenumber_aud` (
  `REV` bigint(20) NOT NULL AUTO_INCREMENT,
  `VENDOR_ID` bigint(20) NOT NULL DEFAULT '0',
  `ID` bigint(20) NOT NULL DEFAULT '0',
  `REVTYPE` decimal(3,0) DEFAULT NULL,
  PRIMARY KEY (`REV`,`VENDOR_ID`,`ID`),
  CONSTRAINT `FK90E5B3B1DF74E053` FOREIGN KEY (`REV`) REFERENCES `revinfo` (`REV`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vendor_phonenumber_aud`
--

LOCK TABLES `vendor_phonenumber_aud` WRITE;
/*!40000 ALTER TABLE `vendor_phonenumber_aud` DISABLE KEYS */;
/*!40000 ALTER TABLE `vendor_phonenumber_aud` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vendorpo`
--

DROP TABLE IF EXISTS `vendorpo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vendorpo` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `MANUFACTURERSKU` varchar(255) DEFAULT NULL,
  `VENDORSKU` varchar(255) DEFAULT NULL,
  `QUANTITY` bigint(20) DEFAULT NULL,
  `INVENTORYITEM_ID` bigint(20) DEFAULT NULL,
  `LINEITEM_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK85E4E1878BE735F7` (`INVENTORYITEM_ID`),
  KEY `FK85E4E187B7777FDD` (`LINEITEM_ID`),
  CONSTRAINT `FK85E4E1878BE735F7` FOREIGN KEY (`INVENTORYITEM_ID`) REFERENCES `inventoryitem` (`ID`),
  CONSTRAINT `FK85E4E187B7777FDD` FOREIGN KEY (`LINEITEM_ID`) REFERENCES `lineitem` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vendorpo`
--

LOCK TABLES `vendorpo` WRITE;
/*!40000 ALTER TABLE `vendorpo` DISABLE KEYS */;
/*!40000 ALTER TABLE `vendorpo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vendorpropertytype`
--

DROP TABLE IF EXISTS `vendorpropertytype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vendorpropertytype` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `VERSION` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UC_VENDORPROPERTYTYPE_1` (`NAME`)
) ENGINE=InnoDB AUTO_INCREMENT=83016 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vendorpropertytype`
--

LOCK TABLES `vendorpropertytype` WRITE;
/*!40000 ALTER TABLE `vendorpropertytype` DISABLE KEYS */;
INSERT INTO `vendorpropertytype` VALUES (75671,'Contacts',0),(83015,'\'contacts\'',0);
/*!40000 ALTER TABLE `vendorpropertytype` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vendorpropertytype_aud`
--

DROP TABLE IF EXISTS `vendorpropertytype_aud`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vendorpropertytype_aud` (
  `ID` bigint(20) NOT NULL DEFAULT '0',
  `REV` bigint(20) NOT NULL AUTO_INCREMENT,
  `REVTYPE` decimal(3,0) DEFAULT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`,`REV`),
  KEY `FKF0283288DF74E053` (`REV`),
  CONSTRAINT `FKF0283288DF74E053` FOREIGN KEY (`REV`) REFERENCES `revinfo` (`REV`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vendorpropertytype_aud`
--

LOCK TABLES `vendorpropertytype_aud` WRITE;
/*!40000 ALTER TABLE `vendorpropertytype_aud` DISABLE KEYS */;
/*!40000 ALTER TABLE `vendorpropertytype_aud` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vendorxvendorpropertytype`
--

DROP TABLE IF EXISTS `vendorxvendorpropertytype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vendorxvendorpropertytype` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `VALUE` varchar(255) DEFAULT NULL,
  `VERSION` bigint(20) DEFAULT NULL,
  `TYPE_ID` bigint(20) DEFAULT NULL,
  `VENDOR_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK6F8A7827D50C94DD` (`VENDOR_ID`),
  KEY `FK6F8A7827FF9A87FA` (`TYPE_ID`),
  CONSTRAINT `FK6F8A7827D50C94DD` FOREIGN KEY (`VENDOR_ID`) REFERENCES `vendor` (`ID`),
  CONSTRAINT `FK6F8A7827FF9A87FA` FOREIGN KEY (`TYPE_ID`) REFERENCES `vendorpropertytype` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vendorxvendorpropertytype`
--

LOCK TABLES `vendorxvendorpropertytype` WRITE;
/*!40000 ALTER TABLE `vendorxvendorpropertytype` DISABLE KEYS */;
/*!40000 ALTER TABLE `vendorxvendorpropertytype` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vendorxvendorpropertytype_aud`
--

DROP TABLE IF EXISTS `vendorxvendorpropertytype_aud`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vendorxvendorpropertytype_aud` (
  `ID` bigint(20) NOT NULL DEFAULT '0',
  `REV` bigint(20) NOT NULL AUTO_INCREMENT,
  `REVTYPE` decimal(3,0) DEFAULT NULL,
  `VALUE` varchar(255) DEFAULT NULL,
  `TYPE_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`,`REV`),
  KEY `FK37E03A78DF74E053` (`REV`),
  CONSTRAINT `FK37E03A78DF74E053` FOREIGN KEY (`REV`) REFERENCES `revinfo` (`REV`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vendorxvendorpropertytype_aud`
--

LOCK TABLES `vendorxvendorpropertytype_aud` WRITE;
/*!40000 ALTER TABLE `vendorxvendorpropertytype_aud` DISABLE KEYS */;
/*!40000 ALTER TABLE `vendorxvendorpropertytype_aud` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vxvxvpt_aud`
--

DROP TABLE IF EXISTS `vxvxvpt_aud`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vxvxvpt_aud` (
  `REV` bigint(20) NOT NULL AUTO_INCREMENT,
  `VENDOR_ID` bigint(20) NOT NULL DEFAULT '0',
  `ID` bigint(20) NOT NULL DEFAULT '0',
  `REVTYPE` decimal(3,0) DEFAULT NULL,
  PRIMARY KEY (`REV`,`VENDOR_ID`,`ID`),
  CONSTRAINT `FK54BC8847DF74E053` FOREIGN KEY (`REV`) REFERENCES `revinfo` (`REV`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vxvxvpt_aud`
--

LOCK TABLES `vxvxvpt_aud` WRITE;
/*!40000 ALTER TABLE `vxvxvpt_aud` DISABLE KEYS */;
/*!40000 ALTER TABLE `vxvxvpt_aud` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `workflowlog`
--

DROP TABLE IF EXISTS `workflowlog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `workflowlog` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `CHANGEDATE` datetime NOT NULL,
  `CSRUSER` varchar(255) NOT NULL,
  `TARGET` varchar(255) DEFAULT NULL,
  `TASKEVENT` varchar(255) NOT NULL,
  `TASKID` bigint(20) DEFAULT NULL,
  `VERSION` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `workflowlog`
--

LOCK TABLES `workflowlog` WRITE;
/*!40000 ALTER TABLE `workflowlog` DISABLE KEYS */;
/*!40000 ALTER TABLE `workflowlog` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `zipplusfour`
--

DROP TABLE IF EXISTS `zipplusfour`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `zipplusfour` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `COUNTYFIPS` varchar(255) DEFAULT NULL,
  `HI` varchar(255) DEFAULT NULL,
  `LOW` varchar(255) DEFAULT NULL,
  `VERSION` bigint(20) DEFAULT NULL,
  `ZIP` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `COUNTY_FIPS_IDX` (`COUNTYFIPS`) USING BTREE,
  KEY `ZIP_IDX` (`ZIP`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `zipplusfour`
--

LOCK TABLES `zipplusfour` WRITE;
/*!40000 ALTER TABLE `zipplusfour` DISABLE KEYS */;
/*!40000 ALTER TABLE `zipplusfour` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-07-01 10:00:26
