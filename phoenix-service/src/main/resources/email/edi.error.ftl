<html>
<body>
<h1>
An edi transaction encountered errors.
</h1>
<h2>
The transaction can be identified by the following number(s):
<#list dto.relevantNumberDto as relevantNumber>
    The ${relevantNumber.type} is ${relevantNumber.value},
</#list>
</h2>
<#list dto.ediErrorDtos as error>
    <h2>
    The document type is ${error.ediType},
    <#list dto.relevantNumberDto as relevantNumber>
        the ${relevantNumber.type} is ${relevantNumber.value}, 
    </#list></h2>
    <p>
    There were the following issues:</p>
    
    <#list error.errorDataDtos as data>
        <p>
        The technical description of the error is: ${data.techErrorDescription}. 
        </p>
        <p> The error has the following specifications: </p>
        <#list data.errorSpecifications as specification>
            <p>${specification}</p>
        </#list>
        <p>USPS suggests you take the followig actions:</p>
        <#list data.recommendedActions as recommendation>
            <p>${recommendation}</p>
        </#list>        
    </#list>
</#list>
<p>The raw edi message used to create this email is as follows:</p>
<p>${dto.rawEdi}</p>
</body>
</html>