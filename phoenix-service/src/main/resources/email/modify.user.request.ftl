<html>
    <body>
        <h1>User Modification Request</h1>
        <br />
        The following user changes have been requested<#if userId??> by ${userId}</#if>. Please review and REPLY TO ALL with your decision.
        <p>
       <b> USER INFORMATION </b>
        </p>
   
   		     <p>
        <#if userType?has_content>User Type: ${userType}<br /></#if>
        <#if requestType?has_content>Request Type: ${requestType}<br /></#if>
        <#if reason?has_content>Reason: ${reason}<br /></#if>
        <#if userId?has_content>ID: ${userId}<br /></#if>
        <#if endUser?has_content>End User: ${endUser}<br /></#if>
        <#if firstName?has_content>First Name: ${firstName}<br /></#if>
        <#if lastName?has_content>Last Name: ${lastName}<br /></#if>
        <#if email?has_content>E-mail: ${email}<br /></#if>
        <#if phone?has_content>Phone: ${phone} <#if ext??>EXT: ${ext}</#if><br /></#if>
        <#if sAddress1?has_content>Address: ${sAddress1}<br /></#if>
        <#if sAddress2?has_content>${sAddress2}<br /></#if>
        <#if sCity?has_content>City: ${sCity}<br /></#if>
        <#if sState?has_content>State: ${sState}<br /></#if>
        <#if sZip?has_content>Zip: ${sZip}<br /></#if>
        <#if mailstop?has_content>Mail Stop: ${mailstop}<br /></#if>
        <#if pole?has_content>Pole: ${pole}<br /></#if>
        <#if endUser?has_content>End User: ${endUser}<br /></#if>
        <#if affiliation?has_content>Affiliation: ${affiliation}<br /></#if>
        <#if psg?has_content>PSG: ${psg}<br />a</#if>
        <#if corpBuNum?has_content>Corp or B.U. Number: ${corpBuNum}<br />b</#if>
        <#if sDesktop?has_content>Desktop: ${sDesktop}<br /></#if>
        <#if sDepartment?has_content>Department: ${sDepartment}<br /></#if>
        <#if sCostCenter?has_content>Cost Center: ${sCostCenter}<br /></#if>
        </p>
   
        <#list departments as dept>
        <p>
        <#if dept.divDept?has_content>Div-Department ${dept_index + 1}: ${dept.divDept}<br /></#if>
        <#if dept.managerName?has_content>Manager Name: ${dept.managerName}<br /></#if>
        <#if dept.managerEmail?has_content>Manager Email: ${dept.managerEmail}<br /></#if>
        <#if dept.name??>Department Name: ${dept.name}<br /></#if>
        <#if dept.desktop??>Department Desktop: ${dept.desktop}<br /></#if>
        <#if dept.costCenter??>Department Cost Center: ${dept.costCenter}<br /></#if>
        <#if dept.shipAddress??>Department Ship Address: ${dept.shipAddress}<br /></#if>
        <#if dept.shipCity??>Department Ship City: ${dept.shipCity}<br /></#if>
        <#if dept.shipState??>Department Ship State: ${dept.shipState}<br /></#if>
        <#if dept.shipZip??>Department Ship Zip: ${dept.shipZip}<br /></#if>
        
        </p>
        </#list>
        <p>
        <b>REQUESTOR INFORMATION</b>
        <br />
        <#if rName?has_content>Requestor Name: ${rName}<br /></#if>
        <#if rEmail?has_content>Requestor Email: ${rEmail}<br /></#if>
        <#if rPhone?has_content>Requestor Phone: ${rPhone}<br /></#if>
        <#if rMgrEmails?has_content>Requestor Manager Emails: ${rMgrEmails}<br /></#if>
        <#if aName?has_content>Approver Name: ${aName}<br /></#if>
        <#if aEmail?has_content>Approver Email: ${aEmail}<br /></#if>
        <#if aPhone?has_content>Approver Phone: ${aPhone}<br /></#if>
        </p>
    </body>
</html>