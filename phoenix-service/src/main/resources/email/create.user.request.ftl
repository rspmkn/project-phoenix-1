<html>
    <body>
        <h1>User Creation Request</h1>
        <br />
        A new user has requested access to the ${tenantName} online catalog.
		Please review and REPLY TO ${csrEmail} with your decision.
        
        <p>
       <b> USER INFORMATION </b>
        </p>
   
   		     <p>
        <#if userId??>ID: ${userId}</#if><br />
        <#if companyName?has_content>Company: ${companyName}<br /></#if>
        <#if firstName??>First Name: ${firstName}</#if><br />
        <#if lastName??>Last Name: ${lastName}</#if><br />
        <#if email??>E-mail: ${email}</#if><br />
        <#if phone??>Phone: ${phone} <#if ext??>EXT: ${ext}</#if></#if><br />
        <#if sAddress1??>Address: ${sAddress1}<br /></#if>
        <#if sAddress2?has_content>${sAddress2}<br /></#if>
        <#if sCity??>${sCity}</#if>, <#if sState??>${sState}</#if> <#if sZip??>${sZip}</#if><br />
        <#if mailstop??>Mail Stop: ${mailstop}<br /></#if>
        <#if pole??>Pole: ${pole}<br /></#if>
        <#if endUser??>End User: ${endUser}<br /></#if>
        <#if affiliation??>Affiliation: ${affiliation}<br /></#if>
        <#if psg??>PSG: ${psg}<br /></#if>
        <#if corpBuNum??>Corp or B.U. Number:${corpBuNum}<br /></#if>
        <#if sDesktop?has_content>Desktop: ${sDesktop}<br /></#if>
        <#if sDepartment?has_content>Department: ${sDepartment}<br /></#if>
        <#if deptName?has_content>Department: ${deptName}<br /></#if>
        <#if sCostCenter?has_content>Cost Center: ${sCostCenter}<br /></#if>
        <#if costCenter?has_content>Cost Center: ${costCenter}<br /></#if>
        <#if location?has_content>Location: ${location}<br /></#if>
    	<#if aName?has_content>Approver Name: ${aName}<br /></#if>
        <#if aEmail?has_content>Approver Email: ${aEmail}<br /></#if>
        <#if aPhone?has_content>Approver Phone: ${aPhone}<br /></#if>
        </p>
   
        <#list departments as dept>
        <p>
        <#if dept.divDept??>Div-Department ${dept_index + 1}: ${dept.divDept}<br /></#if>
        <#if dept.managerName??>Manager Name: ${dept.managerName}<br /></#if>
        <#if dept.managerEmail??>Manager Email: ${dept.managerEmail}<br /></#if>
        <#if dept.name??>Department Name: ${dept.name}<br /></#if>
        <#if dept.desktop??>Department Desktop: ${dept.desktop}<br /></#if>
        <#if dept.costCenter??>Department Cost Center: ${dept.costCenter}<br /></#if>
        <#if dept.shipAddress??>Department Ship Address: ${dept.shipAddress}<br /></#if>
        <#if dept.shipCity??>Department Ship City: ${dept.shipCity}<br /></#if>
        <#if dept.shipState??>Department Ship State: ${dept.shipState}<br /></#if>
        <#if dept.shipZip??>Department Ship Zip: ${dept.shipZip}<br /></#if>
        </p>
        </#list>
    </body>
</html>