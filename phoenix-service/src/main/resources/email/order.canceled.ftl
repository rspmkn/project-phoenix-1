<html>
<body>
<#if uspsIrEmail>
<p>Dear ${requesterName}, 
<p>Thank you for ordering your supplies from ${tenantLongName} on eBuy2.  We appreciate your business. 
<p>In reference to your PO <#if customerPo??># ${customerPo} </#if>the following items are currently not available.

<#else>
<p>The item/s listed below are no longer available for purchase and have been cancelled from your order.

<p>Placed on ${orderDate}

<p>Placed by ${requesterName}
</#if>
<!--&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;adding spacing-->
<table>
  <tr>
    <th>Quantity</th>
    <th>SKU</th>
    <th>Description</th>
  </tr>
<#list cancelledItems as cancelledItem> 
  <tr>
    <td>${cancelledItem.quantity}&nbsp;&nbsp;&nbsp;&nbsp;</td>
    <td>${cancelledItem.item.apdSku}&nbsp;&nbsp;&nbsp;&nbsp;</td>
    <td><#if cancelledItem.item.shortName??>${cancelledItem.item.shortName}<#elseif cancelledItem.item.description??>${cancelledItem.item.description}</#if></td>
  </tr>
</#list>
</table>

<#if uspsIrEmail>
<p>Since we are unsure when these items will become available, we recommend that you find an alternative source for ordering these items.  Please be aware that this does not occur often and we apologize for any inconvenience this may cause you.
<p>Again, we appreciate your business and we look forward to your future orders.
<#else>
<p>We apologize for the inconvenience this may have caused you. Please email or call us if you need assistance in selecting comparable replacement items.Thank you for shopping ${tenantName}.

<p>${csrEmail} , ${csrPhone}
</#if>

</body>
</html>
