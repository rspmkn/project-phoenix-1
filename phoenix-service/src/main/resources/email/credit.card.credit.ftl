<p>A credit has been applied for the following amount:
${result.totalAmount?string.currency}
 </p>

To the following card: <br />
<#if result.nameOnCard??>${result.nameOnCard} <br /></#if>
<#if result.cardIdentifier??>${result.cardIdentifier} <br /></#if>
<#if result.cardExpriation??>${result.cardExpriation} <br /></#if>

 
