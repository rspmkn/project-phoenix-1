<html>
<body style="font-family:Calibiri">
<div style="border-bottom-style:double">

	<div style="float:left; margin-right:30%">
	</div>	 
		<div style="padding-left:60%; margin-top:15px">
		<table>
			<tr>
				<td><b style="font-size:20px">Pro-Forma Invoice: </b> ${quoteNumber}</td>
			</tr>
			<tr>
				<td><b style="font-size:20px">Date: </b>${date}</td>
			</tr>
			<tr>
				<td><b style="font-size:20px">Client Ref: </b><#if clientRef??>${clientRef}</#if></td>
			</tr>
		</table>
	</div>
</div>

<div style="padding-bottom:100px; padding-top:10px">
	<b style="float:left; width:50%"><u>Bill To:</u> 
	<#if billToAddress??> <br>${billToAddress}</#if>
	</b>
	<b>
	<u>Ship To:</u>
	<#if shippingAddress??><br>${shippingAddress}</#if>
	</b>
</div>

<div>Contact Person: <#if userName??>${userName}</#if></div>


<table style="tableLayout:auto; width: 100%; margin-top: 15px; padding-bottom:10px; border-spacing:0;">
	<tr>
		<th align=left style="border-bottom-style:solid; border-width:2px">Currency: USD</th>
		<th align=left style="border-bottom-style:solid; border-width:2px">Phone Number: <#if phoneNumber??>${phoneNumber}</#if></th>
		<th align=left style="border-bottom-style:solid; border-width:2px">Fax Number:</th>
	</tr>
	<tr>
		<th align=left>Sales Manager <#if salesManager??>${salesManager}</#if></th>
		<th align=left>Terms of Sale <#if terms??>${terms}</#if></th>
		<th></th>
	</tr>
</table>
<div>Payment Terms: <#if paymentTerms??>${termsOfPayment}</#if></div>
<div>Exp Date: <#if expirationDate??>${expirationDate}</#if></div>

<table style="tableLayout:auto; width: 100%; margin-top: 15px; position: relative; border-spacing:0; padding-bottom:25px">
	<tr>
		<th style="border:1px solid black; background-color:#C0C0C0"><b>Item Description</b></th>
		<th style="border:1px solid black; background-color:#C0C0C0"><b>NDC-No</b></th>
		<th style="border:1px solid black; background-color:#C0C0C0"><b>Manufacturer</b></th>
		<th style="border:1px solid black; background-color:#C0C0C0"><b>Quantity</b></th> 
		<th style="border:1px solid black; background-color:#C0C0C0"><b>Unit Price</b></th>
		<th style="border:1px solid black; background-color:#C0C0C0"><b>Line Total</b></th>
	</tr>
	<#list items as wrapper>
	  <tr>
		<td style="border:1px solid black;"><#if wrapper.item.shortName??>${wrapper.item.shortName}<#elseif wrapper.item.description??>${wrapper.item.description}</#if></td>
		<td style="border:1px solid black;"><#if wrapper.item.supplierPartId??>${wrapper.item.supplierPartId}</#if></td>
		<td style="border:1px solid black;"><#if wrapper.item.manufacturerName??>${wrapper.item.manufacturerName}</#if></td>
		<td style="border:1px solid black;"><#if wrapper.item.quantity??>${wrapper.item.quantity}</#if></td>
		<td style="border:1px solid black;"><#if wrapper.item.unitPrice??>${wrapper.item.unitPrice}</#if></td>
		<td style="border:1px solid black;"><#if wrapper.item.unitPrice??><#if wrapper.item.quantity??>${wrapper.item.unitPrice * wrapper.item.quantity}</#if></#if></td>
	  </tr>
	  </#list>
	<tr>
		<td></td>
		<td></td>
		<td></td>
		<td style="border:1px solid black;">FREIGHT:</td>
		<td style="border:1px solid black;"><#if freight??>${freight}</#if></td>
	</tr>
	<tr>
		<td></td>
		<td></td>
		<td></td>
		<td style="border:1px solid black;">ORDER TOTAL:</td>
		<td style="border:1px solid black;"><#if orderTotal??>${orderTotal}</#if></td>
	</tr>
</table>

<div>Comments: <#if comments??><br>${comments}</#if></div>
<div style="padding-top:15px; position: relative">
	<p style=" float:left; width:75%">Thank you for your business! <br>
	*** CREDIT TERMS WILL BE ISSUED TO QUALIFIED CUSTOMERS BASED ON A COMPLETED AND APPROVED CREDIT APPLICATION <br>
	PRICES AND AVAILABILITY ARE SUBJECT TO CHANGE WITHOUT NOTICE <br>
	SIGNED PURCHASE ORDERS ARE REQUIRED FOR ALL PURCHASES <br>
	SHIPPING QUOTES AVAILABLE UPON REQUEST<br>
	<b>NATIONAL DRUG SOURCE, INC. 4450 Raceway Drive, Concord, NC, 28027</b><br>
	www.nationaldrugsource.com | P704-723-6505x302 | F704-723-8399 | jtorres@nationaldrugsource.com</p>
	<div><br>__________________________________________ <br>
	   <div>Authorized Signature</div>
	</div>
</div>


</html>
