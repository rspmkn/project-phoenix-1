<p>A charge has been declined for the following amount:
${result.totalAmount?string.currency}, for order number: ${result.apdPo}. This is a result of shipment ${result.trackingNum}
</p>
<p>
On the following card:<br />
<#if result.nameOnCard??>${result.nameOnCard} <br /></#if>
<#if result.cardIdentifier??>${result.cardIdentifier} <br /></#if>
<#if result.cardExpriation??>${result.cardExpriation} <br /></#if>
