<html>
<body>
<h1>
A/P Credit Invoice <#if invoiceNumber??># ${invoiceNumber}<#elseif raNumber??># ${raNumber}</#if> for order <#if apdPo??># ${apdPo}</#if>
</h1>
<h2>
The above Credit Invoice # was received for <#if amount??>${amount?string.currency}<#else>$0.00</#if>.
</h2>
</body>
</html>