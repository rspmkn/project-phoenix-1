<html>
<body style="font-family:Calibiri">
<div style="padding-bottom:50px">
	<div style="float:left; margin-right:30%">
		<h3>National Drug Source</h3>
		4450 Raceway Drive </br>
		 Concord, NC 28027 </br>
		 Phone:704-723-6505x304 </br>
		 Fax:704-723-6399
	</div>	 
	<div>
		<h1> Sales Quotation</h1><br /> 
	</div>
	<div style="padding-left:60%; margin-top:15px">
		<table>
			<tr>
				<td><b>Quote No: </b><#if quoteNumber??>${quoteNumber}</#if></td>
			</tr>
			<tr>
				<td><b>Date: </b><#if date??>${date}</#if></td>
			</tr>
			<tr>
				<td><b>Terms of Payment: </b><#if termsOfPayment??>${termsOfPayment}</#if></td>
			</tr>
			<tr>
				<td><b>Client Ref: </b><#if clientRef??>${clientRef}</#if></td>
			</tr>
		</table>
	</div>
</div>

<div style="padding-bottom:50px">
	<p style="float:left; width:60%"><b>Client:</b> 
	${client}<br />
	</p>
	<table>
		<tr>
			<td><b>Contact Person: </b><#if contactPerson??>${contactPerson}</#if></td>
		</tr>
		<tr>
			<td><b>Phone Number:</b><#if phoneNumber??>${phoneNumber}</#if><br />
			</td>
		</tr>
		<tr>
			<td><b>Fax Number: </b><#if faxNumber??>${faxNumber}</#if></td>
		</tr>
		<tr>
			<td><b>Email: </b><#if email??>${email}</#if></td>
		</tr>	
	</table>
</div>


<table style="tableLayout:auto; width: 100%; margin-top: 15px; position: relative; border:1px solid black;">
  <tr>
    <th style="border:1px solid black; background-color:#C0C0C0"><b>Item Description</b></th>
    <th style="border:1px solid black; background-color:#C0C0C0"><b>NDC-No</b></th>
    <th style="border:1px solid black; background-color:#C0C0C0"><b>Manufacturer</b></th>
    <th style="border:1px solid black; background-color:#C0C0C0"><b>Unit Price</b></th>
  </tr>
 <#list items as wrapper>
  <tr>
    <td style="border:1px solid black;"><#if wrapper.item.shortName??>${wrapper.item.shortName}<#elseif wrapper.item.description??>${wrapper.item.description}</#if>
    <#if wrapper.originallyAddedSku?? && wrapper.originallyAddedName??><br/>This item has been substituted for item ${wrapper.originallyAddedSku} ${wrapper.originallyAddedName}, per your contract or personal selection.</#if></td>
    <td style="border:1px solid black;"><#if wrapper.item.supplierPartId??>${wrapper.item.supplierPartId}</#if></td>
    <td style="border:1px solid black;"><#if wrapper.item.manufacturerName??>${wrapper.item.manufacturerName}</#if></td>
	<td style="border:1px solid black;">${wrapper.item.unitPrice?string.currency}</td>
  </tr>
</#list>
</table>
<div>Comments: <#if comments??>${comments}</#if></div>
<div style="padding-top:15px; position: relative;">
	<p>Thank you for your business! <br>
	*** CREDIT TERMS WILL BE ISSUED TO QUALIFIED CUSTOMERS BASED ON A COMPLETED AND APPROVED CREDIT APPLICATION <br>
	PRICES AND AVAILABILITY ARE SUBJECT TO CHANGE WITHOUT NOTICE <br>
	SIGNED PURCHASE ORDERS ARE REQUIRED FOR ALL PURCHASES <br>
	SHIPPING QUOTES AVAILABLE UPON REQUEST</p>
</div>


</html>
