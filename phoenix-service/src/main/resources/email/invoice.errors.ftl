<html>
<body>
<p>Invoice <#if invoiceNumber??>${invoiceNumber}</#if> has the following errors:

<table>
  <tr>
    <th>SKU</th>
    <th>Error</th>
  </tr>
<#list items as item>
  <#list item.validationErrorDtos as error>
  <tr>
    <td><#if item.apdSku??>${item.apdSku}<#elseif item.supplierPartId??>item.supplierPartId</#if></td>
    <td><#if error.error??>${error.error}</#if></td>
  </tr>
  </#list>
</#list>
</table>

</body>
</html>
