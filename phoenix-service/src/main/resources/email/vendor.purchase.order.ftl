<html>


<body>
<p>Hello,

<p>Please process the attached Purchase Order and send an Order Acknowledgement once it has been processed. Please make sure to include the estimated delivery time and tracking number(s).
If there is anything wrong with the purchase order or the item is not in stock then please contact us immediately. All replies should be sent to ${replyTo}.


<p>Thank You

<p>	
</body>
</html>