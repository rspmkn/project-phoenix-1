<html>
    <body>
        <h1>User Request</h1>
        <br />
        A new user has requested access to the ${tenantName} online catalog.
        <table>
            <#list fieldList as field>
                <tr>
                    <td>
                        ${field}
                    </td>
                </tr>
            </#list>
        </table>
        
    </body>
</html>