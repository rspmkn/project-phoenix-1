<table style="width:100%">
	<col width="50%">
	<tr>
		<td><b>${invoiceNumber}</b></td>
	</tr>
</table>

<div style="width:100%; text-align:center; padding-top:15px; position: relative;">
    <h2>INVOICE</h2><br />
</div>
<#if summary??>
    <div style="width:100%; text-align:center; padding-top:15px; position: relative;">
  		<h3 style="color:red;">Paid on Summary Bill -- Do not submit to Accounts Payable for disbursement. Please review for accuracy and report all discrepancies to <#if csEmail??>${csEmail}<#else>customerservice@americanproduct.com</#if>  or <b><#if csPhone??>${csPhone}<#else>(877) 769-0752</#if></b> </h3><<br/>
    </div>
</#if>

<table style="width:100%; padding-top: 5px">
	<col width="100%">	
	<tr>
		<td style="padding-top: 15px; text-align: center;">
		<p>Thank you for ordering from ${remitName}. If you have any questions, call your dedicated Customer Service team @ <b><#if csPhone??>${csPhone}<#else>1-800-849-5842</#if></b></p>
		</td>
	</tr>
</table>

<table style="width:100%; padding-top: 15px">
	<col width="25%">	
	<col width="50%">
	<col width="25%">
	<tr>
		<td><p><b>Billing Information</b><br />
				<#if billingAddress.companyName??>${billingAddress.companyName}<br /></#if>
				<#if billingAddress.name??>${billingAddress.name}<br /></#if>
				<#if billingAddress.line1??>${billingAddress.line1}<br /></#if>
				<#if billingAddress.city??>${billingAddress.city},</#if> <#if billingAddress.state??>${billingAddress.state}</#if> <#if billingAddress.zip??>${billingAddress.zip}</#if><br />
		</td>
		<td><p><b>Shipping Information</b><br />
				<#if shippingAddress.companyName??>${shippingAddress.companyName}<br /></#if>
				<#if shippingAddress.name??>${shipName}<br /></#if>
				<#if desktopLabel??>${desktopLabel}: </#if><#if desktop??>${desktop}<br /></#if>
                <#if departmentLabel??>${departmentLabel}: </#if><#if department??>${department}<br /></#if>
                <#if costCenterLabel??>${costCenterLabel}: </#if><#if costcenter??>${costcenter}<br /></#if>
                <#if mailStopLabel??>${mailStopLabel}: </#if><#if mailStop??>${mailStop}<br /></#if>
                <#if poleLabel??>${poleLabel}: </#if><#if pole??>${pole}<br /></#if>
                <#if phoneLabel??>${phoneLabel}: </#if><#if phone??>${phone}<br /></#if>
				<#if shippingAddress.line1??>${shippingAddress.line1}</#if><br />
				<#if shippingAddress.city??>${shippingAddress.city},</#if> <#if shippingAddress.state??>${shippingAddress.state}</#if> <#if shippingAddress.zip??>${shippingAddress.zip}</#if><br >
		</td>
	</tr>
</table>

<table style="width:100%; padding-top: 5px">
	<col width="25%">	
	<col width="5%">	
	<#if blanketPo??><tr><td><b>Blanket PO#: </b> ${blanketPo}</td></tr></#if>
    <#if customerPo??><tr><td><b>Customer PO#: </b> ${customerPo}</td></tr></#if>
	<tr>
		<td><b>${tenantCode} Order #: </b> ${apdPo}</td>
	</tr>
	<tr>
		<td><b>Order Date: </b>${orderDate}</td>
	</tr>
	<tr>
		<td><b>Invoice Date: </b>${invoiceDate}</td>
	</tr>	
	<tr>
		<td><b>Shipment #: </b><#if trackingNumber??>${trackingNumber}</#if></td>
	</tr>
	<tr>
		<td><b>Invoice #: </b>${invoiceNumber}</td>
	</tr>	
	<tr>
		<td><b>PaymentTerms: </b>${paymentTerms}</td>
	</tr>
</table>

<table style="tableLayout:auto; width:100%; margin-top: 20px; position: relative; border:1px solid black;">
  <tr>
  	<th style="border:1px solid black; background-color: #C0C0C0">Total Qty Ordered</th>
  	<th style="border:1px solid black; background-color: #C0C0C0">Total Qty Shipped</th>
    <th style="border:1px solid black; background-color: #C0C0C0">Qty Billed This Invoice</th>
    <th style="border:1px solid black; background-color: #C0C0C0">SKU #</th>
    <th style="border:1px solid black; background-color: #C0C0C0">Description</th>
    <th style="border:1px solid black; background-color: #C0C0C0">Status</th>
    <th style="border:1px solid black; background-color: #C0C0C0">Unit Price</th>
    <th style="border:1px solid black; background-color: #C0C0C0">Unit of Measure</th>
    <th style="border:1px solid black; background-color: #C0C0C0">Total</th>
  </tr>
<#list items as item>
  <tr>
  	<td style="border:1px solid black;">${item.lineItemDto.quantity}</td>
  	<td style="border:1px solid black;">${item.lineItemDto.quantityShipped}</td>
  	<td style="border:1px solid black;">${item.quantity}</td>
    <td style="border:1px solid black;">
    	<#if skuOnEmailFlag?? && skuOnEmailFlag = 'B' && item.lineItemDto.supplierPartIdAndSupplierPartAuxId??>
    		${item.lineItemDto.supplierPartIdAndSupplierPartAuxId}
    	<#elseif skuOnEmailFlag?? && skuOnEmailFlag = 'A' && item.lineItemDto.supplierPartAuxId??>
    		${item.lineItemDto.supplierPartAuxId}
    	<#elseif item.lineItemDto.supplierPartId??>
    		${item.lineItemDto.supplierPartId}
    	</#if>
    </td>
    <td style="border:1px solid black;"><#if item.lineItemDto.shortName??>${item.lineItemDto.shortName}<#elseif item.lineItemDto.description??>${item.lineItemDto.description}</#if></td>
    <td style="border:1px solid black;">${item.lineItemDto.status.value}</td>
    <td style="border:1px solid black;">${item.lineItemDto.unitPrice?string.currency}</td>
    <td style="border:1px solid black;">${item.lineItemDto.unitOfMeasure.name}</td>
    <td style="border:1px solid black;">${item.calculateSubTotal()?string.currency}</td>
  </tr>
</#list>
</table>

<table style="padding-top: 5px; float: left;">
	<tr>
		<td></td>
		<td><b>Total Merchandise</b></td>
		<td>${merchandiseTotal?string.currency}</td>
	</tr>
	<#if minimumOrderFee != 0>
        <tr>
                <td></td>
                <td><b>Minimum Order Fee</b></td>
                <td>${minimumOrderFee?string.currency}</td>
        </tr>
    </#if>
	<tr>
		<td></td>
		<td><b>Est. Shipping/Handling</b></td>
		<td>${shippingTotal?string.currency}</td>
	</tr>
	<tr>
		<td></td>
		<td><b>Tax</b></td>
		<td>${taxTotal?string.currency}</td>
	</tr>
	<tr>
		<td></td>
		<td><b>Total Order</b></td>
		<td>${orderTotal?string.currency}</td>
	</tr>	
</table>

<table style="width:100%; padding-top: 15px">
	<col width="100%">
	<tr>
		<td>
		<p><b>PLEASE REMIT PAYMENT TO: <#if remitName == "Marquette Commercial Finance" ></b>This amount due has been assigned to ${remitName}. All checks must be payable and forwarded to:</#if><br />
			${remitName}<br />
			${remitStreet}<br />
			${remitCityState}<br />
		</td>
	</tr>	
</table>
     