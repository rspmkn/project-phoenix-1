<html>
    <body>
       	<p>A marquette invoice was not successfully placed on the server.</p>
       	
       	<#if invoiceNumber??>
			<p>The invoice number is is: ${invoiceNumber}</p>
		<#else>
			<p>The invoice number is unknown.</p>
		</#if>
		<#if exception??>
			<p>The following error occured: ${exception}</p>
		</#if>
    </body>
</html>