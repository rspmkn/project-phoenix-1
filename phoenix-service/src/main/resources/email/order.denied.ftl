<html>
<body>
<p>Dear Valued Customer:

<p> Your order, detailed below, has been denied by your manager.

<p>Comments: ${approvalComment}
<p>Placed on ${orderDate}


<table>
  <tr>
    <th>QTY</th>
    <th>SKU</th>
    <th>DESCRIPTION</th>
    <th>UNIT PRICE</th>
    <th>UOM</th>
    <th>EXT PRICE</th>
  </tr>
<#list items as item>
  <tr>
    <td>${item.quantity}</td>
    <td>${item.apdSku}</td>
    <td><#if item.shortName??>${item.shortName}<#elseif item.description??>${item.description}</#if></td>
    <td>${item.unitPrice?string.currency}</td>
    <td>${item.unitOfMeasure.name}</td>
    <td>${item.subTotal?string.currency}</td>
  </tr>
</#list>
</table>

<p>Total Merchandise:........${merchandiseTotal?string.currency}
<p>Est. Shipping/Handling:...${shippingTotal?string.currency}
<p>Tax:......................${taxTotal?string.currency}
<p>Total:....................${orderTotal?string.currency}

</body>
</html>
