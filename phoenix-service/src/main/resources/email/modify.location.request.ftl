<html>
    <body>
        <h1>Modify Location Request</h1>
        <br />
        The following location changes have been requested by ${userId}. Please review and REPLY TO ALL with your decision.
           <p>
       <b> USER INFORMATION </b>
        </p>
   
   		     <p>
        <#if requestType?has_content>Request Type: ${requestType}<br /></#if>
        <#if userId?has_content>ID: ${userId}<br /></#if>
        <#if firstName?has_content>First Name: ${firstName}<br /></#if>
        <#if lastName?has_content>Last Name: ${lastName}<br /></#if>
        <#if email?has_content>E-mail: ${email}<br /></#if>
        <#if phone?has_content>Phone: ${phone} <#if ext??>EXT: ${ext}</#if><br /></#if>
        </p>
          <p>
       <b> LOCATION INFORMATION OLD</b>
        </p>
   
        <p>
        <#if sAddress1?has_content>Address: ${sAddress1}<br /></#if>
        <#if sAddress2?has_content>${sAddress2}<br /></#if>
        <#if sCity?has_content>City: ${sCity}<br /></#if>
        <#if sState?has_content>State: ${sState}<br /></#if>
        <#if sZip?has_content>Zip: ${sZip}<br /></#if>
        <#if sComments?has_content>Comments: ${sComments}<br /></#if>
        </p>
          <p>
       <b> LOCATION INFORMATION NEW</b>
        </p>
   
        <p>
        <#if sAddress1?has_content>Address: ${sAddress1New}<br /></#if>
        <#if sAddress2?has_content>${sAddress2New}<br /></#if>
        <#if sCity?has_content>City: ${sCityNew}<br /></#if>
        <#if sState?has_content>State: ${sStateNew}<br /></#if>
        <#if sZip?has_content>Zip: ${sZipNew}<br /></#if>
        <#if sComments?has_content>Comments: ${sCommentsNew}<br /></#if>
        </p>
         <#if rName?has_content>
        <p>
        <b>REQUESTOR INFORMATION</b>
        <br />
        </#if>
        <#if rName?has_content>Requestor Name: ${rName}<br /></#if>
        <#if rEmail?has_content>Requestor Email: ${rEmail}<br /></#if>
        <#if rPhone?has_content>Requestor Phone: ${rPhone}<br /></#if>
        <#if rMgrEmails?has_content>Requestor Manager Emails: ${rMgrEmails}<br /></#if>
        </p>
    </body>
</html>