<html>
<body style="font-family:Calibiri">
<#if usps?? && usps>Thank you for ordering.<#else>

<table style="width:100%">
	<col width="50%">
	<tr>
		<td style="height:20px"><b>#${apdPo}</b></td>
	</tr>
</table>


<div style="width:100%; text-align:center; padding-top:15px; position: relative;">
<h2>ORDER ACKNOWLEDGEMENT</h2><br />
</div>


<table style="width:100%; padding-top: 5px">
	<col width="100%">	
	<tr>
		<td style="padding-top: 10px; text-align: left;">
		<p>Your order, approved and submitted on ${orderDate} , is being acknowledged to contain the products listed below. A credit card receipt or an invoice will be emailed to you upon shipment of the products ordered. Thank you for ordering.</p>
		</td>
	</tr>
</table>

<table style="width:100%; padding-top: 15px">
	<col width="25%">	
	<col width="50%">
	<col width="25%">
	<tr>
		<td><p><b>Billing Information</b><br />
				<#if billingAddress.companyName??>${billingAddress.companyName}<br /></#if>
				<#if billingAddress.name??>${billingAddress.name}<br /></#if>
				<#if billingAddress.line1??>${billingAddress.line1}<br /></#if>
				<#if billingAddress.city??>${billingAddress.city},</#if> <#if billingAddress.state??>${billingAddress.state}</#if> <#if billingAddress.zip??>${billingAddress.zip}</#if><br />
		</td>		
		<td></td>
		<td><p><b>Shipping Information</b><br />
				<#if shippingAddress.companyName??>${shippingAddress.companyName}<br /></#if>
				<#if shippingAddress.name??>${shippingAddress.name}<br /></#if>
				<#if desktopLabel??>${desktopLabel}: </#if><#if desktop??>${desktop}<br /></#if>
                <#if departmentLabel??>${departmentLabel}: </#if><#if department??>${department}<br /></#if>
                <#if costCenterLabel??>${costCenterLabel}: </#if><#if costcenter??>${costcenter}<br /></#if>
                <#if divDeptLabel??>${divDeptLabel}: </#if><#if divDept??>${divDept}<br /></#if>
                <#if mailStopLabel??>${mailStopLabel}: </#if><#if mailStop??>${mailStop}<br /></#if>
                <#if poleLabel??>${poleLabel}: </#if><#if pole??>${pole}<br /></#if>
                <#if phoneLabel??>${phoneLabel}: </#if><#if phone??>${phone}<br /></#if>
				<#if shippingAddress.line1??>${shippingAddress.line1}</#if><br />
				<#if shippingAddress.city??>${shippingAddress.city},</#if> <#if shippingAddress.state??>${shippingAddress.state}</#if> <#if shippingAddress.zip??>${shippingAddress.zip}</#if><br >
		</td>
	</tr>
</table>

<table style="width:100%; padding-top: 5px">
	<col width="25%">	
	<col width="5%">	
    <#if blanketPo??><tr><td><b>Blanket PO#: </b> ${blanketPo}</td></tr></#if>
    <#if customerPo??><tr><td><b>Customer PO#: </b> ${customerPo}</td></tr></#if>
	<tr>
		<td><b>${tenantCode} Order #: </b> ${apdPo}</td>
	</tr>
	<tr>
		<td><b>Order Date: </b>${orderDate}</td>
	</tr>	
</table>

<table style="tableLayout:auto; width: 100%; margin-top: 15px; position: relative; border:1px solid black;">
  <tr>
    <th style="border:1px solid black; background-color:#C0C0C0"><b>Quantity</b></th>
    <th style="border:1px solid black; background-color:#C0C0C0"><b>SKU</b></th>
    <th style="border:1px solid black; background-color:#C0C0C0"><b>Description</b></th>
    <th style="border:1px solid black; background-color:#C0C0C0"><b>Status</b></th>
    <th style="border:1px solid black; background-color:#C0C0C0"><b>Unit Price</b></th>
    <th style="border:1px solid black; background-color:#C0C0C0"><b>Unit of Measure</b></th>
    <th style="border:1px solid black; background-color:#C0C0C0"><b>Total</b></th>
  </tr>
<#list items as wrapper>
  <tr>
    <td style="border:1px solid black;">${wrapper.item.quantity}</td>
    <td style="border:1px solid black;">
    	<#if skuOnEmailFlag?? && skuOnEmailFlag = 'B'>
    		${wrapper.item.supplierPartIdAndSupplierPartAuxId}
    	<#elseif skuOnEmailFlag?? && skuOnEmailFlag = 'A'>
    		${wrapper.item.supplierPartAuxId}
    	<#else>
    		${wrapper.item.supplierPartId}
    	</#if>
    </td>
    <td style="border:1px solid black;"><#if wrapper.item.shortName??>${wrapper.item.shortName}<#elseif wrapper.item.description??>${wrapper.item.description}</#if>
    <#if wrapper.originallyAddedSku?? && wrapper.originallyAddedName??><br/>This item has been substituted for item ${wrapper.originallyAddedSku} ${wrapper.originallyAddedName}, per your contract or personal selection.</#if></td>
    <td style="border:1px solid black;">${wrapper.item.status.value}</td>
    <td style="border:1px solid black;">${wrapper.item.unitPrice?string.currency}</td>
    <td style="border:1px solid black;"><#if wrapper.item.unitOfMeasure??>${wrapper.item.unitOfMeasure.name}</#if></td>
    <td style="border:1px solid black;">${wrapper.item.subTotal?string.currency}</td>
  </tr>
</#list>
</table>

<div style="padding-top:5px;float:right;">
	<table padding-top: 5px">
		<tr>
			<td></td>
			<td><b>Total Merchandise</b></td>
			<td>${merchandiseTotal?string.currency}</td>
		</tr>
		<#if minimumOrderFee??>
            <tr>
                <td></td>
                <td><b>Minimum Order Fee</b></td>
                <td>${minimumOrderFee?string.currency}</td>
            </tr>
        </#if>
		<tr>
			<td></td>
			<td><b>Est. Shipping/Handling</b></td>
			<td>${shippingTotal?string.currency}</td>
		</tr>
		<tr>
			<td></td>
			<td><b>Tax</b></td>
			<td>${taxTotal?string.currency}</td>
		</tr>
		<tr>
			<td></td>
			<td><b>Total Order</b></td>
			<td>${orderTotal?string.currency}</td>
		</tr>	
	</table>
</div>


<div style="padding-top:15px; position: relative;">
	<p>If you have any questions, call your dedicated Customer Service team @ <b><#if csPhone??>${csPhone}<#else>1-800-849-5842</#if></b></p>
</div>

</#if>

</html>
