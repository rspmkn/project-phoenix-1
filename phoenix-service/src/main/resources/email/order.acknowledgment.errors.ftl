<html>
<body>
<p>Order ${apdPo} has the following errors:

<table>
  <tr>
    <th>SKU</th>
    <th>Error</th>
  </tr>
<#list items as item>
  <#list item.validationErrorDtos as error>
  <tr>
    <td>${item.buyerPartNumber}</td>
    <td>${error.error}</td>
  </tr>
  </#list>
</#list>
</table>

</body>
</html>
