<html>
<body>
<h2>
An inbound message could not be linked to an existing order.
</h2>
<p>
<#if dto.apdPo??>The document expected an order with ${tenantCode} PO of ${dto.apdPo}, but no such order was found in the system.
<#else>No ${tenantCode} PO was given for the inbound document.</#if> This issue will have to be remediated manually.
</p>
<p>The raw message for the inbound document is attached.</p>
</body>
</html>