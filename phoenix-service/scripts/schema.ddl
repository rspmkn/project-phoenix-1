
    create table ACCXCREDXUSER_CCENTER (
        id bigint not null auto_increment,
        active boolean not null,
        currentCharge decimal(19,2),
        pendingCharge decimal(19,2),
        totalLimit decimal(19,2),
        version integer not null,
        ACCOUNTCREDENTIALUSERS_ID bigint not null,
        ALLOWEDCOSTCENTERS_ID bigint not null,
        primary key (id)
    ) type=InnoDB;

    create table ACCXCREDXUSER_CCENTER_AUD (
        id bigint not null,
        REV integer not null,
        REVTYPE tinyint,
        active boolean,
        currentCharge decimal(19,2),
        pendingCharge decimal(19,2),
        totalLimit decimal(19,2),
        ACCOUNTCREDENTIALUSERS_ID bigint,
        ALLOWEDCOSTCENTERS_ID bigint,
        primary key (id, REV)
    ) type=InnoDB;

    create table AccXCredXUser_Dept (
        id bigint not null auto_increment,
        active boolean not null,
        recipientsEmails varchar(255),
        recipientsNames varchar(255),
        version integer not null,
        AccountXCredentialXUser_id bigint not null,
        departments_id bigint not null,
        primary key (id)
    ) type=InnoDB;

    create table AccXCredXUser_Dept_AUD (
        id bigint not null,
        REV integer not null,
        REVTYPE tinyint,
        active boolean,
        recipientsEmails varchar(255),
        recipientsNames varchar(255),
        AccountXCredentialXUser_id bigint,
        departments_id bigint,
        primary key (id, REV)
    ) type=InnoDB;

    create table AccXCredXUser_Desktops (
        AccountXCredentialXUser_id bigint not null,
        desktops_id bigint not null,
        primary key (AccountXCredentialXUser_id, desktops_id)
    ) type=InnoDB;

    create table AccXCredXUser_Desktops_AUD (
        REV integer not null,
        AccountXCredentialXUser_id bigint not null,
        desktops_id bigint not null,
        REVTYPE tinyint,
        primary key (REV, AccountXCredentialXUser_id, desktops_id)
    ) type=InnoDB;

    create table Account (
        id bigint not null auto_increment,
        apdAssignedAccountId varchar(255),
        creationDate date,
        csrEmail varchar(255),
        csrPhone varchar(255),
        isActive boolean not null,
        locationCode varchar(255),
        name varchar(255) not null unique,
        showGlobalBulletin boolean,
        solomonCustomerId varchar(255) not null,
        useLocationCode boolean not null,
        version integer not null,
        cxmlConfiguration_id bigint,
        parentAccount_id bigint,
        paymentInformation_id bigint,
        primaryContact_id bigint,
        rootAccount_id bigint,
        primary key (id)
    ) type=InnoDB;

    create table AccountBRMSRecord (
        id bigint not null auto_increment,
        active boolean,
        processId bigint,
        sessionId integer,
        snapshot varchar(255),
        version integer not null,
        account_id bigint,
        primary key (id)
    ) type=InnoDB;

    create table AccountGroup (
        id bigint not null auto_increment,
        name varchar(255),
        version integer not null,
        primary key (id)
    ) type=InnoDB;

    create table AccountGroup_AUD (
        id bigint not null,
        REV integer not null,
        REVTYPE tinyint,
        name varchar(255),
        primary key (id, REV)
    ) type=InnoDB;

    create table AccountLineage (
        id bigint not null auto_increment,
        priority integer,
        version integer not null,
        account_id bigint,
        ancestorAccount_id bigint,
        primary key (id)
    ) type=InnoDB;

    create table AccountLineage_AUD (
        id bigint not null,
        REV integer not null,
        REVTYPE tinyint,
        priority integer,
        account_id bigint,
        ancestorAccount_id bigint,
        primary key (id, REV)
    ) type=InnoDB;

    create table AccountPropertyType (
        id bigint not null auto_increment,
        name varchar(255) not null unique,
        version integer not null,
        primary key (id)
    ) type=InnoDB;

    create table AccountPropertyType_AUD (
        id bigint not null,
        REV integer not null,
        REVTYPE tinyint,
        name varchar(255),
        primary key (id, REV)
    ) type=InnoDB;

    create table AccountXAccountPropertyType (
        id bigint not null auto_increment,
        value varchar(255),
        version integer not null,
        type_id bigint not null,
        account_id bigint,
        primary key (id)
    ) type=InnoDB;

    create table AccountXCredentialXUser (
        id bigint not null auto_increment,
        active boolean not null,
        credentialSelectionImage varchar(255),
        name varchar(255),
        punchLevel varchar(255),
        usAccountNumber varchar(255),
        version integer not null,
        account_id bigint,
        address_id bigint,
        approverUser_id bigint,
        cred_id bigint,
        paymentInformation_id bigint,
        user_id bigint,
        primary key (id)
    ) type=InnoDB;

    create table Account_AUD (
        id bigint not null,
        REV integer not null,
        REVTYPE tinyint,
        apdAssignedAccountId varchar(255),
        creationDate date,
        csrEmail varchar(255),
        csrPhone varchar(255),
        isActive boolean,
        locationCode varchar(255),
        name varchar(255),
        showGlobalBulletin boolean,
        solomonCustomerId varchar(255),
        useLocationCode boolean,
        cxmlConfiguration_id bigint,
        parentAccount_id bigint,
        paymentInformation_id bigint,
        primaryContact_id bigint,
        rootAccount_id bigint,
        primary key (id, REV)
    ) type=InnoDB;

    create table Account_AccountGroup (
        accounts_id bigint not null,
        groups_id bigint not null,
        primary key (accounts_id, groups_id)
    ) type=InnoDB;

    create table Account_AccountGroup_AUD (
        REV integer not null,
        accounts_id bigint not null,
        groups_id bigint not null,
        REVTYPE tinyint,
        primary key (REV, accounts_id, groups_id)
    ) type=InnoDB;

    create table Account_Bulletin (
        Account_id bigint not null,
        bulletins_id bigint not null,
        primary key (Account_id, bulletins_id)
    ) type=InnoDB;

    create table Account_Bulletin_AUD (
        REV integer not null,
        Account_id bigint not null,
        bulletins_id bigint not null,
        REVTYPE tinyint,
        primary key (REV, Account_id, bulletins_id)
    ) type=InnoDB;

    create table Account_IpAddress_AUD (
        REV integer not null,
        account_id bigint not null,
        id bigint not null,
        REVTYPE tinyint,
        primary key (REV, account_id, id)
    ) type=InnoDB;

    create table Account_PoNumber (
        Account_id bigint not null,
        blanketPos_id bigint not null,
        primary key (Account_id, blanketPos_id)
    ) type=InnoDB;

    create table ActionType (
        id bigint not null auto_increment,
        description varchar(255),
        name varchar(255) not null unique,
        version integer not null,
        primary key (id)
    ) type=InnoDB;

    create table ActionType_AUD (
        id bigint not null,
        REV integer not null,
        REVTYPE tinyint,
        description varchar(255),
        name varchar(255),
        primary key (id, REV)
    ) type=InnoDB;

    create table Address (
        id bigint not null auto_increment,
        city varchar(255),
        company varchar(255),
        country varchar(255),
        county varchar(255),
        geoCode varchar(255),
        line1 varchar(255),
        line2 varchar(255),
        name varchar(255),
        pendingShipToAuthorization boolean,
        isPrimary boolean,
        shipToId varchar(255),
        state varchar(255),
        version integer not null,
        zip varchar(255),
        account_id bigint,
        miscShipTo_id bigint,
        person_id bigint,
        type_id bigint,
        vendor_id bigint,
        primary key (id)
    ) type=InnoDB;

    create table AddressPropertyType (
        id bigint not null auto_increment,
        name varchar(255),
        version integer not null,
        primary key (id)
    ) type=InnoDB;

    create table AddressPropertyType_AUD (
        id bigint not null,
        REV integer not null,
        REVTYPE tinyint,
        name varchar(255),
        primary key (id, REV)
    ) type=InnoDB;

    create table AddressType (
        id bigint not null auto_increment,
        name varchar(255) not null unique,
        version integer not null,
        primary key (id)
    ) type=InnoDB;

    create table AddressType_AUD (
        id bigint not null,
        REV integer not null,
        REVTYPE tinyint,
        name varchar(255),
        primary key (id, REV)
    ) type=InnoDB;

    create table AddressXAddressPropertyType (
        id bigint not null auto_increment,
        value varchar(255),
        version integer not null,
        address_id bigint,
        type_id bigint,
        primary key (id)
    ) type=InnoDB;

    create table Address_AUD (
        id bigint not null,
        REV integer not null,
        REVTYPE tinyint,
        city varchar(255),
        company varchar(255),
        country varchar(255),
        county varchar(255),
        geoCode varchar(255),
        line1 varchar(255),
        line2 varchar(255),
        name varchar(255),
        pendingShipToAuthorization boolean,
        isPrimary boolean,
        shipToId varchar(255),
        state varchar(255),
        zip varchar(255),
        account_id bigint,
        person_id bigint,
        type_id bigint,
        vendor_id bigint,
        primary key (id, REV)
    ) type=InnoDB;

    create table ApdPoLog (
        id bigint not null auto_increment,
        creationDate datetime,
        version integer not null,
        apdPo_id bigint,
        primary key (id)
    ) type=InnoDB;

    create table AttachmentType (
        id bigint not null auto_increment,
        name varchar(255),
        version integer not null,
        primary key (id)
    ) type=InnoDB;

    create table AttachmentType_AUD (
        id bigint not null,
        REV integer not null,
        REVTYPE tinyint,
        name varchar(255),
        primary key (id, REV)
    ) type=InnoDB;

    create table Brand (
        id bigint not null auto_increment,
        logo varchar(255),
        name varchar(255),
        usscoId varchar(255),
        primary key (id)
    ) type=InnoDB;

    create table Bulletin (
        id bigint not null auto_increment,
        csr boolean not null,
        expirationDate date,
        global boolean not null,
        MESSAGE longtext,
        name varchar(255),
        version integer not null,
        primary key (id)
    ) type=InnoDB;

    create table Bulletin_AUD (
        id bigint not null,
        REV integer not null,
        REVTYPE tinyint,
        csr boolean,
        expirationDate date,
        global boolean,
        MESSAGE longtext,
        name varchar(255),
        primary key (id, REV)
    ) type=InnoDB;

    create table CAROUSEL_DISPLAY (
        FROM_CLASS varchar(31) not null,
        id bigint not null auto_increment,
        backgroundImageUrl varchar(255),
        body longtext,
        header varchar(255),
        linkText varchar(255),
        linkUrl varchar(255),
        version integer not null,
        subdomain varchar(255),
        catalog_id bigint,
        primary key (id)
    ) type=InnoDB;

    create table CAROUSEL_DISPLAY_AUD (
        FROM_CLASS varchar(31) not null,
        id bigint not null,
        REV integer not null,
        REVTYPE tinyint,
        backgroundImageUrl varchar(255),
        body longtext,
        header varchar(255),
        linkText varchar(255),
        linkUrl varchar(255),
        catalog_id bigint,
        subdomain varchar(255),
        primary key (id, REV)
    ) type=InnoDB;

    create table CanceledQuantity (
        id bigint not null auto_increment,
        cancelDate datetime,
        quantity decimal(19,2) not null,
        reason varchar(255),
        version integer,
        lineItem_id bigint,
        primary key (id)
    ) type=InnoDB;

    create table CardInformation (
        id bigint not null auto_increment,
        cardType varchar(255),
        cardVaultToken varchar(255),
        cvvIndicator varchar(255),
        debit boolean not null,
        expiration date,
        ghost boolean not null,
        identifier varchar(255),
        name_on_card varchar(255),
        version integer not null,
        primary key (id)
    ) type=InnoDB;

    create table CardInformation_AUD (
        id bigint not null,
        REV integer not null,
        REVTYPE tinyint,
        cardType varchar(255),
        cardVaultToken varchar(255),
        cvvIndicator varchar(255),
        debit boolean,
        expiration date,
        ghost boolean,
        identifier varchar(255),
        name_on_card varchar(255),
        primary key (id, REV)
    ) type=InnoDB;

    create table Cart (
        id bigint not null auto_increment,
        estimatedShippingAmount decimal(19,2),
        expirationDate datetime,
        maximumTaxToCharge decimal(19,2),
        version integer not null,
        userCredential_id bigint,
        primary key (id)
    ) type=InnoDB;

    create table CartItem (
        id bigint not null auto_increment,
        quantity integer not null,
        version integer not null,
        catalogXItem_id bigint not null,
        cart_id bigint,
        primary key (id)
    ) type=InnoDB;

    create table CashoutPage (
        id bigint not null auto_increment,
        billtoAddressDisplayFormat varchar(255),
        cancelDate boolean,
        deliveryDate boolean,
        expectedDate boolean,
        hideCCNumber boolean,
        higherPriceNotice boolean,
        multipleShipTo boolean,
        name varchar(255),
        noCardAction varchar(255),
        serviceDate boolean,
        shiptoAddressDisplayFormat varchar(255),
        updateUserInformation boolean,
        version integer not null,
        primary key (id)
    ) type=InnoDB;

    create table CashoutPageXField (
        id bigint not null auto_increment,
        defaultValue varchar(255),
        inboundMapping varchar(255),
        includeLabelInMapping boolean,
        label varchar(255),
        required boolean not null,
        updatable boolean not null,
        version integer not null,
        field_id bigint not null,
        mapping_id bigint,
        page_id bigint,
        primary key (id)
    ) type=InnoDB;

    create table CashoutPageXField_AUD (
        id bigint not null,
        REV integer not null,
        REVTYPE tinyint,
        defaultValue varchar(255),
        inboundMapping varchar(255),
        includeLabelInMapping boolean,
        label varchar(255),
        required boolean,
        updatable boolean,
        primary key (id, REV)
    ) type=InnoDB;

    create table Catalog (
        id bigint not null auto_increment,
        changeDate date,
        changedItemRelevantFields boolean,
        coreItemHash bigint,
        diffCsvChecked boolean,
        expirationDate date,
        itemHash bigint,
        lastProcessEnd datetime,
        lastProcessStart datetime,
        marketingImageLinkUrlTop varchar(255),
        marketingImageUrlBottom varchar(255),
        marketingImageUrlTop varchar(255),
        marketingLinkUrlBottom varchar(255),
        name varchar(255) not null unique,
        nonUpsableShippingMaximum decimal(19,2),
        nonUpsableShippingMinimum decimal(19,2),
        nonUpsableShippingRate decimal(19,2),
        nonUpsableShippingRatePercent boolean,
        processedQueueCount integer,
        processingCompleteEmails varchar(255),
        regenerateCsvDate date,
        regenerateCsvNightly boolean,
        regenerateSmartOciDate date,
        reindexDate date,
        reindexNightly boolean,
        searchType varchar(255),
        startDate date,
        unprocessedQueueCount integer,
        upsableShippingMaximum decimal(19,2),
        upsableShippingMinimum decimal(19,2),
        upsableShippingRate decimal(19,2),
        upsableShippingRatePercent boolean,
        version integer not null,
        coreListUid_id bigint,
        customer_id bigint,
        listUid_id bigint,
        parent_id bigint,
        replacement_id bigint,
        vendor_id bigint,
        primary key (id)
    ) type=InnoDB;

    create table CatalogCsv (
        id bigint not null auto_increment,
        catalogId bigint not null,
        correlationId varchar(255),
        processedItems bigint,
        totalItems bigint,
        version integer not null,
        primary key (id)
    ) type=InnoDB;

    create table CatalogUpload (
        id bigint not null auto_increment,
        action integer,
        catalogId bigint,
        completed boolean not null,
        correlationId varchar(255),
        oldCatalogId bigint,
        persistChanges boolean,
        processedItems bigint,
        totalItems bigint,
        version integer not null,
        primary key (id)
    ) type=InnoDB;

    create table CatalogXCategoryXPricingType (
        id bigint not null auto_increment,
        parameter varchar(255),
        version integer not null,
        catalog_id bigint,
        category_id bigint,
        type_id bigint,
        primary key (id)
    ) type=InnoDB;

    create table CatalogXItem (
        id bigint not null auto_increment,
        added boolean,
        coreItemExpirationDate date,
        coreItemStartDate date,
        customerSkuString varchar(255),
        deleted boolean,
        isFeaturedItem boolean,
        lastModified datetime not null,
        modified boolean,
        mostPopular boolean,
        overrideCatalogs varchar(255),
        price decimal(19,2) not null,
        pricingParameter varchar(255),
        solrOverrideDescription varchar(255),
        version integer not null,
        catalog_id bigint not null,
        customerUnitOfmeasure_id bigint,
        item_id bigint not null,
        pricingType_id bigint,
        substituteItem_id bigint,
        primary key (id),
        unique (catalog_id, item_id)
    ) type=InnoDB;

    create table CatalogXItemXItemPropertyType (
        id bigint not null auto_increment,
        value longtext,
        version integer not null,
        type_id bigint not null,
        catalogxitem_id bigint,
        primary key (id)
    ) type=InnoDB;

    create table Catalog_AUD (
        id bigint not null,
        REV integer not null,
        REVTYPE tinyint,
        lastProcessEnd datetime,
        lastProcessStart datetime,
        nonUpsableShippingMaximum decimal(19,2),
        nonUpsableShippingMinimum decimal(19,2),
        nonUpsableShippingRate decimal(19,2),
        nonUpsableShippingRatePercent boolean,
        upsableShippingRate decimal(19,2),
        upsableShippingRatePercent boolean,
        primary key (id, REV)
    ) type=InnoDB;

    create table CategoryMarkup (
        id bigint not null auto_increment,
        markupPercentage decimal(19,2),
        version integer not null,
        catalog_id bigint,
        category_id bigint,
        primary key (id)
    ) type=InnoDB;

    create table CategoryMarkup_AUD (
        id bigint not null,
        REV integer not null,
        REVTYPE tinyint,
        markupPercentage decimal(19,2),
        catalog_id bigint,
        primary key (id, REV)
    ) type=InnoDB;

    create table CommunicationMethod (
        id bigint not null auto_increment,
        name varchar(255) not null unique,
        version integer not null,
        primary key (id)
    ) type=InnoDB;

    create table CommunicationMethod_AUD (
        id bigint not null,
        REV integer not null,
        REVTYPE tinyint,
        name varchar(255),
        primary key (id, REV)
    ) type=InnoDB;

    create table ContactLogComment (
        id bigint not null auto_increment,
        commentTimestamp datetime,
        content longtext not null,
        version integer not null,
        apdCsrRep_id bigint not null,
        servicelog_id bigint,
        issuelog_id bigint,
        primary key (id)
    ) type=InnoDB;

    create table ContactMethod (
        id bigint not null auto_increment,
        name varchar(255),
        version integer not null,
        primary key (id)
    ) type=InnoDB;

    create table ContactMethod_AUD (
        id bigint not null,
        REV integer not null,
        REVTYPE tinyint,
        name varchar(255),
        primary key (id, REV)
    ) type=InnoDB;

    create table CostCenter (
        id bigint not null auto_increment,
        description varchar(255),
        name varchar(255),
        ccNumber varchar(255),
        spendingLimit decimal(19,2),
        totalSpent decimal(19,2),
        version integer not null,
        period_id bigint,
        primary key (id)
    ) type=InnoDB;

    create table CostCenter_AUD (
        id bigint not null,
        REV integer not null,
        REVTYPE tinyint,
        description varchar(255),
        name varchar(255),
        ccNumber varchar(255),
        spendingLimit decimal(19,2),
        totalSpent decimal(19,2),
        period_id bigint,
        primary key (id, REV)
    ) type=InnoDB;

    create table Credential (
        id bigint not null auto_increment,
        activationDate date,
        adot boolean not null,
        authorizeAddress boolean not null,
        credentialSelectionImage varchar(255),
        cidest varchar(255),
        cimtype varchar(255),
        csrEmail varchar(255),
        csrPhone varchar(255),
        customFleName varchar(255),
        customerOutgoingSharedSecret varchar(255),
        defaultPunchoutZipCode varchar(255),
        deploymentmode varchar(255),
        ediRecCode varchar(255),
        ediVersion varchar(255),
        expirationDate date,
        idest varchar(255),
        imtype varchar(255),
        name varchar(255) not null unique,
        outgoingSenderSharedSecret varchar(255),
        partnerId varchar(255),
        poackdest varchar(255),
        poackmtype varchar(255),
        punchopallow varchar(255),
        punchouttype varchar(255),
        requireZipCodeInput boolean not null,
        RETURNCARTDOM varchar(255),
        RETURNCARTID varchar(255),
        sndest varchar(255),
        snmtype varchar(255),
        specialFleName varchar(255),
        storeCredit decimal(19,2),
        terms varchar(255),
        usrGuideFleName varchar(255),
        vendorName varchar(255),
        vendorOrderUrl varchar(255),
        vendorPunchoutUrl varchar(255),
        version integer not null,
        willCallEdiOverride boolean not null,
        wrapAndLabel boolean not null,
        cashoutPage_id bigint,
        catalog_id bigint,
        communicationMethod_id bigint,
        defaultShipFromAddress_id bigint,
        paymentInformation_id bigint,
        rootAccount_id bigint not null,
        skuType_id bigint,
        user_id bigint,
        primary key (id)
    ) type=InnoDB;

    create table CredentialPropertyType (
        id bigint not null auto_increment,
        defaultValue varchar(255),
        name varchar(255) not null unique,
        version integer not null,
        primary key (id)
    ) type=InnoDB;

    create table CredentialPropertyType_AUD (
        id bigint not null,
        REV integer not null,
        REVTYPE tinyint,
        defaultValue varchar(255),
        name varchar(255),
        primary key (id, REV)
    ) type=InnoDB;

    create table CredentialXVendor (
        id bigint not null auto_increment,
        accountId varchar(255) not null,
        version integer not null,
        credential_id bigint,
        vendor_id bigint,
        primary key (id)
    ) type=InnoDB;

    create table CredentialXVendor_AUD (
        id bigint not null,
        REV integer not null,
        REVTYPE tinyint,
        accountId varchar(255),
        credential_id bigint,
        vendor_id bigint,
        primary key (id, REV)
    ) type=InnoDB;

    create table Credential_AUD (
        id bigint not null,
        REV integer not null,
        REVTYPE tinyint,
        activationDate date,
        adot boolean,
        authorizeAddress boolean,
        credentialSelectionImage varchar(255),
        cidest varchar(255),
        cimtype varchar(255),
        csrEmail varchar(255),
        csrPhone varchar(255),
        customFleName varchar(255),
        customerOutgoingSharedSecret varchar(255),
        defaultPunchoutZipCode varchar(255),
        deploymentmode varchar(255),
        ediRecCode varchar(255),
        ediVersion varchar(255),
        expirationDate date,
        idest varchar(255),
        imtype varchar(255),
        name varchar(255),
        outgoingSenderSharedSecret varchar(255),
        partnerId varchar(255),
        poackdest varchar(255),
        poackmtype varchar(255),
        punchopallow varchar(255),
        punchouttype varchar(255),
        requireZipCodeInput boolean,
        RETURNCARTDOM varchar(255),
        RETURNCARTID varchar(255),
        sndest varchar(255),
        snmtype varchar(255),
        specialFleName varchar(255),
        storeCredit decimal(19,2),
        terms varchar(255),
        usrGuideFleName varchar(255),
        vendorName varchar(255),
        vendorOrderUrl varchar(255),
        vendorPunchoutUrl varchar(255),
        willCallEdiOverride boolean,
        wrapAndLabel boolean,
        catalog_id bigint,
        communicationMethod_id bigint,
        defaultShipFromAddress_id bigint,
        paymentInformation_id bigint,
        rootAccount_id bigint,
        skuType_id bigint,
        user_id bigint,
        primary key (id, REV)
    ) type=InnoDB;

    create table Credential_Bulletin (
        Credential_id bigint not null,
        bulletins_id bigint not null,
        primary key (Credential_id, bulletins_id)
    ) type=InnoDB;

    create table Credential_Bulletin_AUD (
        REV integer not null,
        Credential_id bigint not null,
        bulletins_id bigint not null,
        REVTYPE tinyint,
        primary key (REV, Credential_id, bulletins_id)
    ) type=InnoDB;

    create table Credential_CostCenter (
        Credential_id bigint not null,
        costCenters_id bigint not null,
        primary key (Credential_id, costCenters_id)
    ) type=InnoDB;

    create table Credential_CostCenter_AUD (
        REV integer not null,
        Credential_id bigint not null,
        costCenters_id bigint not null,
        REVTYPE tinyint,
        primary key (REV, Credential_id, costCenters_id)
    ) type=InnoDB;

    create table Credential_Department (
        Credential_id bigint not null,
        departments_id bigint not null,
        primary key (Credential_id, departments_id)
    ) type=InnoDB;

    create table Credential_Department_AUD (
        REV integer not null,
        Credential_id bigint not null,
        departments_id bigint not null,
        REVTYPE tinyint,
        primary key (REV, Credential_id, departments_id)
    ) type=InnoDB;

    create table Credential_LimitApproval (
        Credential_id bigint not null,
        limitApproval_id bigint not null,
        primary key (Credential_id, limitApproval_id)
    ) type=InnoDB;

    create table Credential_LimitApproval_AUD (
        REV integer not null,
        Credential_id bigint not null,
        limitApproval_id bigint not null,
        REVTYPE tinyint,
        primary key (REV, Credential_id, limitApproval_id)
    ) type=InnoDB;

    create table Credential_Permission (
        Credential_id bigint not null,
        permissions_id bigint not null,
        primary key (Credential_id, permissions_id)
    ) type=InnoDB;

    create table Credential_Permission_AUD (
        REV integer not null,
        Credential_id bigint not null,
        permissions_id bigint not null,
        REVTYPE tinyint,
        primary key (REV, Credential_id, permissions_id)
    ) type=InnoDB;

    create table Credential_PoNumber (
        Credential_id bigint not null,
        blanketPos_id bigint not null,
        primary key (Credential_id, blanketPos_id)
    ) type=InnoDB;

    create table Credential_PropertyType (
        id bigint not null auto_increment,
        value varchar(255),
        version integer not null,
        type_id bigint not null,
        cred_id bigint,
        primary key (id)
    ) type=InnoDB;

    create table Credential_PropertyType_AUD (
        id bigint not null,
        REV integer not null,
        REVTYPE tinyint,
        value varchar(255),
        type_id bigint,
        primary key (id, REV)
    ) type=InnoDB;

    create table Credential_Role (
        Credential_id bigint not null,
        roles_id bigint not null,
        primary key (Credential_id, roles_id)
    ) type=InnoDB;

    create table Credential_Role_AUD (
        REV integer not null,
        Credential_id bigint not null,
        roles_id bigint not null,
        REVTYPE tinyint,
        primary key (REV, Credential_id, roles_id)
    ) type=InnoDB;

    create table CsvUploadedItem (
        id bigint not null auto_increment,
        version integer not null,
        primary key (id)
    ) type=InnoDB;

    create table CsvUploadedItemField (
        id bigint not null auto_increment,
        FIELDKEY varchar(255) not null,
        value longtext not null,
        version integer not null,
        uploadeditem_id bigint,
        primary key (id)
    ) type=InnoDB;

    create table CustomerCost (
        id bigint not null auto_increment,
        value decimal(19,2),
        version integer not null,
        customer_id bigint,
        item_id bigint,
        primary key (id)
    ) type=InnoDB;

    create table CustomerOrder (
        id bigint not null auto_increment,
        adot boolean not null,
        apdSalesPerson varchar(255),
        approvalComment longtext,
        assignedCostCenterLabel varchar(255),
        buyerCookie varchar(255),
        cancelDate date,
        currencyCode varchar(255),
        customerRevisionDate datetime,
        customerRevisionNumber integer,
        deliveryDate date,
        estimatedShipping varchar(255),
        estimatedShippingAmount decimal(19,2),
        expiryDate date,
        maximumTaxToCharge decimal(19,2),
        merchandiseTypeCode varchar(255),
        operationAllowed varchar(255),
        orderDate datetime,
        orderExtrasArtifact longtext,
        orderOrigin varchar(255),
        orderPayloadId varchar(255),
        orderTotal decimal(19,2) not null,
        passedInitialValidation boolean not null,
        paymentTransactionKey varchar(255),
        requestedDeliveryDate date,
        requisitionName varchar(255),
        resendAttempts integer,
        shipDate date,
        shipNoLaterDate date,
        shipNotBeforeDate date,
        shipToExtrasArtifact longtext,
        taxAssigned boolean,
        terms varchar(255),
        version integer not null,
        willCallEdiOverride boolean not null,
        wrapAndLabel boolean not null,
        account_id bigint,
        address_id bigint,
        assignedCostCenter_id bigint,
        cardInformation_id bigint,
        credential_id bigint,
        customerStatus_id bigint,
        parentOrder_id bigint,
        paymentInformation_id bigint,
        paymentStatus_id bigint,
        status_id bigint,
        type_id bigint,
        user_id bigint,
        primary key (id)
    ) type=InnoDB;

    create table CustomerOrderRequest (
        id bigint not null auto_increment,
        token varchar(255) not null unique,
        version integer not null,
        customerOrder_id bigint,
        primary key (id)
    ) type=InnoDB;

    create table CustomerOrder_TaxType (
        id bigint not null auto_increment,
        value decimal(19,2) not null,
        version integer not null,
        type_id bigint not null,
        order_id bigint,
        primary key (id)
    ) type=InnoDB;

    create table CustomerSpecificIconUrl (
        id bigint not null auto_increment,
        overrideIconUrl varchar(255),
        version integer not null,
        classificationType_id bigint,
        propertyType_id bigint,
        account_id bigint,
        primary key (id),
        check ((PROPERTYTYPE_ID IS NULL AND CLASSIFICATIONTYPE_ID IS NOT NULL) OR (PROPERTYTYPE_ID IS NOT NULL AND CLASSIFICATIONTYPE_ID IS NULL))
    ) type=InnoDB;

    create table CxmlConfiguration (
        id bigint not null auto_increment,
        credentialNamePrefix varchar(255),
        credentialNameXpathExpression varchar(255),
        deploymentMode varchar(255),
        name varchar(255) unique,
        senderSharedSecret varchar(255),
        systemUserPrefix varchar(255),
        systemUserXpathExpression varchar(255),
        SYSTEMUSERXPATHEXPDEFRES varchar(255),
        version integer not null,
        primary key (id)
    ) type=InnoDB;

    create table CxmlConfiguration_AUD (
        id bigint not null,
        REV integer not null,
        REVTYPE tinyint,
        credentialNamePrefix varchar(255),
        credentialNameXpathExpression varchar(255),
        deploymentMode varchar(255),
        name varchar(255),
        senderSharedSecret varchar(255),
        systemUserPrefix varchar(255),
        systemUserXpathExpression varchar(255),
        SYSTEMUSERXPATHEXPDEFRES varchar(255),
        primary key (id, REV)
    ) type=InnoDB;

    create table CxmlCredential (
        id bigint not null auto_increment,
        domain varchar(255),
        identifier varchar(255),
        version integer not null,
        primary key (id)
    ) type=InnoDB;

    create table CxmlCredential_AUD (
        id bigint not null,
        REV integer not null,
        REVTYPE tinyint,
        domain varchar(255),
        identifier varchar(255),
        primary key (id, REV)
    ) type=InnoDB;

    create table Department (
        id bigint not null auto_increment,
        name varchar(255),
        version integer not null,
        primary key (id)
    ) type=InnoDB;

    create table Department_AUD (
        id bigint not null,
        REV integer not null,
        REVTYPE tinyint,
        name varchar(255),
        primary key (id, REV)
    ) type=InnoDB;

    create table DescriptionType (
        id bigint not null auto_increment,
        name varchar(255),
        version integer not null,
        primary key (id)
    ) type=InnoDB;

    create table Desktop (
        id bigint not null auto_increment,
        name varchar(255),
        version integer not null,
        primary key (id)
    ) type=InnoDB;

    create table Desktop_AUD (
        id bigint not null,
        REV integer not null,
        REVTYPE tinyint,
        name varchar(255),
        primary key (id, REV)
    ) type=InnoDB;

    create table DimensionType (
        id bigint not null auto_increment,
        dimensionTypeName varchar(255),
        version integer not null,
        primary key (id)
    ) type=InnoDB;

    create table FLATITEMCLASSIFICATION (
        id bigint not null auto_increment,
        value varchar(255),
        version integer not null,
        item_id bigint,
        ItemClassType_id bigint,
        primary key (id)
    ) type=InnoDB;

    create table FLATITEMSPECIFICATION (
        id bigint not null auto_increment,
        name varchar(255),
        priority bigint,
        sequence integer,
        value longtext,
        version integer not null,
        item_id bigint,
        primary key (id)
    ) type=InnoDB;

    create table FavoritesList (
        id bigint not null auto_increment,
        name varchar(255),
        version integer not null,
        catalog_id bigint,
        axcxu_id bigint,
        primary key (id),
        unique (axcxu_id, name, catalog_id)
    ) type=InnoDB;

    create table Field (
        id bigint not null auto_increment,
        name varchar(255) unique,
        version integer not null,
        fieldType_id bigint,
        primary key (id)
    ) type=InnoDB;

    create table FieldOptions (
        id bigint not null auto_increment,
        value varchar(255),
        version integer not null,
        field_id bigint,
        primary key (id)
    ) type=InnoDB;

    create table FieldType (
        id bigint not null auto_increment,
        name varchar(255) not null unique,
        version integer not null,
        primary key (id)
    ) type=InnoDB;

    create table GlobalSetting (
        id bigint not null auto_increment,
        name varchar(255),
        value varchar(255),
        version integer not null,
        primary key (id)
    ) type=InnoDB;

    create table HierarchyNode (
        id bigint not null auto_increment,
        description varchar(255),
        indexedPaths longtext,
        publicid bigint not null,
        type varchar(255),
        version integer not null,
        parent_id bigint,
        primary key (id)
    ) type=InnoDB;

    create table INVOICE (
        FROM_CLASS varchar(31) not null,
        id bigint not null auto_increment,
        amount decimal(19,2),
        approvalDate datetime,
        billingTypeCode varchar(255),
        createdDate datetime,
        dueDate date,
        invoiceDate date,
        invoiceNumber varchar(255),
        version integer not null,
        raNumber varchar(255),
        restockingFee decimal(19,2),
        status varchar(255),
        valid boolean,
        returnOrder_id bigint,
        shipment_id bigint,
        customerOrder_id bigint,
        primary key (id)
    ) type=InnoDB;

    create table INVOICE_LineItem (
        INVOICE_id bigint not null,
        actualItems_id bigint not null
    ) type=InnoDB;

    create table INVOICE_SPECIAL (
        INVOICE_ID bigint not null,
        INSTRUCTION varchar(255)
    ) type=InnoDB;

    create table ISSUE_LOG (
        id bigint not null auto_increment,
        contactEmail varchar(255),
        contactName varchar(255),
        contactNumber varchar(255),
        department varchar(255),
        followUpComplete boolean,
        openedDate datetime not null,
        resolution varchar(255),
        resolvedDate datetime,
        status varchar(255),
        ticketNumber varchar(255),
        updateTimestamp datetime,
        version integer not null,
        expectedResolution datetime,
        issueCategory varchar(255),
        issueDescription longtext,
        scheduledFollowUp datetime,
        customerOrder_id bigint,
        reporter_id bigint,
        primary key (id)
    ) type=InnoDB;

    create table ITEM_ITEM (
        id bigint not null auto_increment,
        position integer not null,
        relationship varchar(255) not null,
        version integer not null,
        ITEM_ID bigint not null,
        SIMILARITEMS_ID bigint not null,
        primary key (id)
    ) type=InnoDB;

    create table ImageType (
        id bigint not null auto_increment,
        name varchar(255) not null unique,
        version integer not null,
        primary key (id)
    ) type=InnoDB;

    create table InventoryBin (
        id bigint not null auto_increment,
        onHandQuantity bigint,
        inventoryBinType_id bigint not null,
        inventoryLocation_id bigint not null,
        inventoryitem_id bigint not null,
        primary key (id)
    ) type=InnoDB;

    create table InventoryBinType (
        id bigint not null auto_increment,
        binName varchar(255),
        defaultBin boolean,
        description varchar(255),
        inventoryBinTypeID bigint not null,
        primary key (id)
    ) type=InnoDB;

    create table InventoryItem (
        id bigint not null auto_increment,
        UPC bigint,
        expiry datetime,
        lot bigint,
        onHandQty bigint,
        vendorName varchar(255),
        vendorSku varchar(255),
        item_id bigint not null,
        primary key (id)
    ) type=InnoDB;

    create table InventoryLocation (
        id bigint not null auto_increment,
        defaultLocation varchar(255),
        locationName varchar(255),
        inventorySite_id bigint not null,
        primary key (id)
    ) type=InnoDB;

    create table InventoryReasons (
        id bigint not null auto_increment,
        description varchar(255) not null,
        invReasonType varchar(255) not null,
        invReasonTypeId bigint not null unique,
        primary key (id)
    ) type=InnoDB;

    create table InventorySite (
        id bigint not null auto_increment,
        attention varchar(255),
        city varchar(255),
        country varchar(255),
        fax bigint,
        line1 varchar(255),
        line2 varchar(255),
        phone bigint,
        receivingHours varchar(255),
        siteName varchar(255),
        state varchar(255),
        zip varchar(255),
        primary key (id)
    ) type=InnoDB;

    create table InventoryTransaction (
        id bigint not null auto_increment,
        comments varchar(255),
        fromBinOnHandQty bigint,
        quantity bigint,
        returnAuthorization varchar(255),
        shipmentNumber bigint,
        transactionAmount decimal(19,2) not null,
        transactionDate date,
        transactionType varchar(255),
        user varchar(255),
        fromBinType_id bigint not null,
        inventoryItem_id bigint,
        inventoryReasonTypes_id bigint not null,
        inventoryTransactionTypes_id bigint not null,
        toBinType_id bigint not null,
        primary key (id)
    ) type=InnoDB;

    create table InventoryTransactionTypes (
        id bigint not null auto_increment,
        description varchar(255) not null,
        transactiontype varchar(255) not null,
        transcationtypeid bigint not null unique,
        primary key (id)
    ) type=InnoDB;

    create table InvoiceSendMethod (
        id bigint not null auto_increment,
        name varchar(255) not null unique,
        version integer not null,
        primary key (id)
    ) type=InnoDB;

    create table IpAddress (
        id bigint not null auto_increment,
        value varchar(255),
        version integer not null,
        cred_id bigint,
        account_id bigint,
        primary key (id)
    ) type=InnoDB;

    create table IpAddress_AUD (
        id bigint not null,
        REV integer not null,
        REVTYPE tinyint,
        value varchar(255),
        primary key (id, REV)
    ) type=InnoDB;

    create table Item (
        id bigint not null auto_increment,
        cartridgeNumber varchar(255),
        commodityCode varchar(255),
        dealerSku varchar(255),
        description longtext not null,
        expiry datetime,
        externalCatalogChange boolean,
        externalCatalogRelevant boolean,
        forUseWith varchar(255),
        isFeaturedItem boolean,
        itemWeight decimal(19,2),
        lastCostChangeDate datetime,
        lastModified datetime not null,
        lotSerial varchar(255),
        minimum integer,
        multiple integer,
        name varchar(255) not null,
        protectedCommodityCode varchar(255),
        protectedSearchTerms longtext,
        searchTerms longtext,
        smartSearchDealerDescription varchar(255),
        smartSearchKeywords longtext,
        status varchar(255),
        vendorSku varchar(255),
        version integer not null,
        abilityOneSubstitute_id bigint,
        brand_id bigint,
        hierarchyNode_id bigint,
        itemCategory_id bigint,
        manufacturer_id bigint,
        replacement_id bigint,
        unitOfMeasure_id bigint not null,
        vendorCatalog_id bigint not null,
        primary key (id)
    ) type=InnoDB;

    create table ItemCategory (
        id bigint not null auto_increment,
        commodityCode varchar(255),
        name varchar(255) unique,
        version integer not null,
        parent_id bigint,
        primary key (id)
    ) type=InnoDB;

    create table ItemClassificationType (
        id bigint not null auto_increment,
        iconUrl varchar(255),
        name varchar(255) not null unique,
        toolTipLabel varchar(255),
        version integer not null,
        primary key (id)
    ) type=InnoDB;

    create table ItemDimension (
        id bigint not null auto_increment,
        value double precision,
        version integer not null,
        dimensionType_id bigint,
        item_id bigint,
        unitOfMeasure_id bigint,
        primary key (id)
    ) type=InnoDB;

    create table ItemIdentifier (
        id bigint not null auto_increment,
        value varchar(255),
        version integer not null,
        item_id bigint,
        itemIdentifierType_id bigint,
        primary key (id)
    ) type=InnoDB;

    create table ItemIdentifierType (
        id bigint not null auto_increment,
        name varchar(255) not null unique,
        version integer not null,
        primary key (id)
    ) type=InnoDB;

    create table ItemImage (
        id bigint not null auto_increment,
        imageName varchar(255),
        imageUrl varchar(255),
        isPrimary boolean,
        version integer not null,
        imageType_id bigint,
        item_id bigint,
        primary key (id)
    ) type=InnoDB;

    create table ItemPackaging (
        id bigint not null auto_increment,
        version integer not null,
        item_id bigint,
        packagingDimension_id bigint,
        packagingType_id bigint,
        primary key (id)
    ) type=InnoDB;

    create table ItemPropertyType (
        id bigint not null auto_increment,
        iconUrl varchar(255),
        name varchar(255) not null unique,
        supplementary boolean not null,
        toolTipLabel varchar(255),
        version integer not null,
        primary key (id)
    ) type=InnoDB;

    create table ItemPropertyType_AUD (
        id bigint not null,
        REV integer not null,
        REVTYPE tinyint,
        iconUrl varchar(255),
        name varchar(255),
        supplementary boolean,
        toolTipLabel varchar(255),
        primary key (id, REV)
    ) type=InnoDB;

    create table ItemSellingPoint (
        id bigint not null auto_increment,
        position integer not null,
        value longtext not null,
        version integer not null,
        item_id bigint,
        primary key (id)
    ) type=InnoDB;

    create table ItemXItemPropertyType (
        id bigint not null auto_increment,
        value longtext,
        version integer not null,
        type_id bigint not null,
        item_id bigint,
        primary key (id)
    ) type=InnoDB;

    create table Item_Matchbook (
        Item_id bigint not null,
        matchbook_id bigint not null,
        primary key (Item_id, matchbook_id)
    ) type=InnoDB;

    create table Item_priceCategories (
        Item_id bigint not null,
        priceCategories varchar(255),
        priceCategories_KEY bigint,
        primary key (Item_id, priceCategories_KEY)
    ) type=InnoDB;

    create table Item_smartSearchCustomerSkus (
        Item_id bigint not null,
        smartSearchCustomerSkus varchar(255),
        smartSearchCustomerSkus_KEY bigint,
        primary key (Item_id, smartSearchCustomerSkus_KEY)
    ) type=InnoDB;

    create table JumptrackStopConfiguration (
        id bigint not null auto_increment,
        allowShipFriday boolean not null,
        allowShipMonday boolean not null,
        allowShipSaturday boolean not null,
        allowShipSunday boolean not null,
        allowShipThursday boolean not null,
        allowShipTuesday boolean not null,
        allowShipWednesday boolean not null,
        version integer not null,
        zip varchar(255) not null unique,
        primary key (id)
    ) type=InnoDB;

    create table LimitApproval (
        id bigint not null auto_increment,
        APPROVALLIMIT decimal(19,2),
        type integer not null,
        VALUE varchar(255),
        AXCXU_ID bigint,
        primary key (id)
    ) type=InnoDB;

    create table LimitApproval_AUD (
        id bigint not null,
        REV integer not null,
        REVTYPE tinyint,
        APPROVALLIMIT decimal(19,2),
        type integer,
        VALUE varchar(255),
        AXCXU_ID bigint,
        primary key (id, REV)
    ) type=InnoDB;

    create table LineItem (
        id bigint not null auto_increment,
        apdSku varchar(255),
        backorderedQuantity decimal(19,2),
        core boolean,
        cost decimal(19,2),
        customerExpectedUnitPrice decimal(19,2),
        customerLineNumber varchar(255),
        deliveryMethod varchar(255),
        description longtext not null,
        estimatedShippingAmount decimal(19,2) not null,
        extraneousCustomerSku varchar(255),
        globalTradeIdentificatonNumber varchar(255),
        globalTradeItemNumber varchar(255),
        hasBeenRejected boolean,
        itemType varchar(255),
        leadTime integer,
        lineNumber integer,
        manufacturerName varchar(255),
        manufacturerPartId varchar(255),
        maximumTaxToCharge decimal(19,2),
        originalQuantity integer,
        originallyAddedName varchar(255),
        originallyAddedSku varchar(255),
        parentLineNumber integer,
        purchaseComment varchar(255),
        quantity integer not null,
        quantityInvoicedByVendor decimal(19,2),
        readyToShipQTY integer,
        returnShipping boolean,
        shipToExtrasArtifact longtext,
        shortName varchar(255),
        supplierPartAuxiliaryId varchar(255),
        supplierPartId varchar(255),
        taxable boolean,
        unitPrice decimal(19,2) not null,
        universalProductCode varchar(255),
        unspscClassification varchar(255),
        url varchar(255),
        version integer not null,
        weight decimal(19,2),
        category_id bigint,
        customerExpectedUoM_id bigint,
        customerSku_id bigint,
        expected_id bigint,
        item_id bigint,
        order_id bigint,
        paymentStatus_id bigint,
        status_id bigint,
        unitOfMeasure_id bigint not null,
        vendor_id bigint,
        primary key (id)
    ) type=InnoDB;

    create table LineItemStatus (
        id bigint not null auto_increment,
        description varchar(255),
        paymentStatus boolean not null,
        value varchar(255),
        version integer not null,
        primary key (id)
    ) type=InnoDB;

    create table LineItemXReturn (
        id bigint not null auto_increment,
        quantity decimal(19,2) not null,
        reason varchar(255),
        restockingFee decimal(19,2),
        returnShipping boolean,
        returnShippingAmount decimal(19,2),
        version integer not null,
        lineItem_id bigint,
        INVOICE_ID bigint,
        RETURNORDER_ID bigint,
        primary key (id)
    ) type=InnoDB;

    create table LineItemXShipment (
        id bigint not null auto_increment,
        ccTransactionId varchar(255),
        paid boolean,
        quantity decimal(19,2) not null,
        shipping decimal(19,2),
        version integer not null,
        lineItem_id bigint,
        shipments_id bigint,
        primary key (id)
    ) type=InnoDB;

    create table LineItemXShipment_TaxType (
        id bigint not null auto_increment,
        value decimal(19,2) not null,
        version integer not null,
        lineItemXReturn_id bigint,
        lineItemXShipment_id bigint,
        type_id bigint,
        primary key (id),
        check (LINEITEMXSHIPMENT_ID IS NOT NULL OR LINEITEMXRETURN_ID IS NOT NULL AND NOT (LINEITEMXSHIPMENT_ID IS NOT NULL AND LINEITEMXRETURN_ID IS NOT NULL))
    ) type=InnoDB;

    create table ListHashToUid (
        id bigint not null auto_increment,
        creationDate datetime,
        hash bigint not null,
        version integer not null,
        primary key (id)
    ) type=InnoDB;

    create table ListHashToUid_AUD (
        id bigint not null,
        REV integer not null,
        REVTYPE tinyint,
        creationDate datetime,
        hash bigint,
        primary key (id, REV)
    ) type=InnoDB;

    create table Manufacturer (
        id bigint not null auto_increment,
        name varchar(255) unique,
        version integer not null,
        primary key (id)
    ) type=InnoDB;

    create table Manufacturer_AUD (
        id bigint not null,
        REV integer not null,
        REVTYPE tinyint,
        name varchar(255),
        primary key (id, REV)
    ) type=InnoDB;

    create table Matchbook (
        id bigint not null auto_increment,
        device varchar(255),
        family varchar(255),
        model varchar(255) not null,
        version integer not null,
        manufacturer_id bigint not null,
        primary key (id),
        unique (manufacturer_id, model)
    ) type=InnoDB;

    create table Matchbook_AUD (
        id bigint not null,
        REV integer not null,
        REVTYPE tinyint,
        device varchar(255),
        family varchar(255),
        model varchar(255),
        manufacturer_id bigint,
        primary key (id, REV)
    ) type=InnoDB;

    create table MessageMapping (
        id bigint not null auto_increment,
        name varchar(255),
        version integer not null,
        primary key (id)
    ) type=InnoDB;

    create table MessageMetadata (
        id bigint not null auto_increment,
        attachmentName varchar(255),
        bccRecipients longtext,
        ccRecipients longtext,
        communicationType varchar(255),
        contentLength bigint not null,
        contentType varchar(255),
        destination longtext,
        filePath varchar(255),
        groupId varchar(255),
        interchangeId varchar(255),
        messageDate date,
        messageType varchar(255),
        receiverId varchar(255),
        senderId varchar(255),
        transactionId varchar(255),
        version integer not null,
        objectsWrapper_id bigint,
        primary key (id)
    ) type=InnoDB;

    create table MessageObjectsWrapper (
        id bigint not null auto_increment,
        data longblob,
        version integer not null,
        primary key (id)
    ) type=InnoDB;

    create table MilitaryZip (
        id bigint not null auto_increment,
        city varchar(255),
        county varchar(255),
        state varchar(255),
        uspsCity varchar(255),
        version integer not null,
        zip varchar(255),
        primary key (id)
    ) type=InnoDB;

    create table MiscShipTo (
        id bigint not null auto_increment,
        Area varchar(255),
        addressId varchar(255),
        addressId36 varchar(255),
        airportCode varchar(255),
        apdInterchangeId varchar(255),
        carrierName varchar(255),
        carrierRouting varchar(255),
        contactName varchar(255),
        contractNumber varchar(255),
        contractingOfficer varchar(255),
        customerShipToID varchar(255),
        dcNumber varchar(255),
        deliverToName varchar(255),
        deliveryDate varchar(255),
        department varchar(255),
        dept varchar(255),
        desktop varchar(255),
        district varchar(255),
        division varchar(255),
        ediID varchar(255),
        facility varchar(255),
        fedexNumber varchar(255),
        fedstripNumber varchar(255),
        financeNumber varchar(255),
        glNumber varchar(255),
        glnID varchar(255),
        headerComments varchar(255),
        lob varchar(255),
        locationId varchar(255),
        locations varchar(255),
        mailStop varchar(255),
        manager varchar(255),
        nameOfHospital varchar(255),
        nowl varchar(255),
        oldRelease varchar(255),
        orderEmail varchar(255),
        origin varchar(255),
        pol varchar(255),
        purchasingGroup varchar(255),
        requesterName varchar(255),
        requesterPhone varchar(255),
        shippingMethod varchar(255),
        solomonId varchar(255),
        storeNumber varchar(255),
        street2 varchar(255),
        street3 varchar(255),
        unit varchar(255),
        usAccount varchar(255),
        uspsUnder13Oz varchar(255),
        vendor varchar(255),
        primary key (id)
    ) type=InnoDB;

    create table NoteType (
        id bigint not null auto_increment,
        name varchar(255) not null unique,
        version integer not null,
        primary key (id)
    ) type=InnoDB;

    create table NotificationLimit (
        id bigint not null auto_increment,
        eventType varchar(255),
        limitType varchar(255),
        version integer not null,
        credential_id bigint,
        primary key (id)
    ) type=InnoDB;

    create table NotificationProperties (
        id bigint not null auto_increment,
        addressing varchar(255),
        eventType varchar(255),
        overrideCredential boolean,
        recipients longtext not null,
        version integer not null,
        order_id bigint,
        cred_id bigint,
        acc_id bigint,
        primary key (id)
    ) type=InnoDB;

    create table ORDER_LOG (
        FROM_CLASS varchar(31) not null,
        id bigint not null auto_increment,
        eventType varchar(255),
        updateTimestamp datetime,
        version integer not null,
        changingUser varchar(255),
        description longtext,
        apdPo varchar(255),
        authCode varchar(255),
        cardExpriation date,
        card_identifier varchar(255),
        custPo varchar(255),
        failureReason varchar(255),
        freightAmount decimal(19,2),
        itemCount integer,
        name_on_card varchar(255),
        salesTax decimal(19,2),
        subtotal decimal(19,2),
        timestamp datetime,
        totalAmount decimal(19,2),
        trackingNum varchar(255),
        transactionAmount decimal(19,2),
        transactionKey varchar(255),
        transactionSuccess boolean,
        transactionType varchar(255),
        note varchar(255),
        returnOrderStatus varchar(255),
        customerOrder_id bigint,
        messageMetadata_id bigint,
        orderStatus_id bigint,
        creditcard_returnorder_id bigint,
        invoice_id bigint,
        lineItemStatus_id bigint,
        returnOrder_id bigint,
        shipment_id bigint,
        lineItem_id bigint,
        primary key (id),
        check ((FROM_CLASS!='CREDITCARDTRANSACTION_LOG') OR ((ITEMCOUNT IS NOT NULL) AND (TRANSACTIONSUCCESS IS NOT NULL)))
    ) type=InnoDB;

    create table ORDER_LOG_LineItemXShipment (
        ORDER_LOG_id bigint not null,
        lineItemXShipments_id bigint not null
    ) type=InnoDB;

    create table OrderAttachment (
        id bigint not null auto_increment,
        attachmentTimestamp datetime not null,
        fileName varchar(255) not null,
        version integer not null,
        user_id bigint not null,
        servicelog_id bigint,
        order_id bigint,
        primary key (id)
    ) type=InnoDB;

    create table OrderBRMSRecord (
        id bigint not null auto_increment,
        active boolean,
        entryId bigint not null,
        processId bigint,
        replaceSession boolean,
        sessionId integer,
        snapshot varchar(255),
        version integer not null,
        primary key (id)
    ) type=InnoDB;

    create table OrderProcessLookup (
        id bigint not null auto_increment,
        version integer not null,
        customerOrder_id bigint,
        primary key (id)
    ) type=InnoDB;

    create table OrderStatus (
        id bigint not null auto_increment,
        customerStatus boolean not null,
        description varchar(255),
        paymentStatus boolean not null,
        value varchar(255) unique,
        version integer not null,
        overrideStatus_id bigint,
        primary key (id)
    ) type=InnoDB;

    create table OrderType (
        id bigint not null auto_increment,
        description varchar(255),
        value varchar(255),
        version integer not null,
        primary key (id)
    ) type=InnoDB;

    create table PackagingDimension (
        id bigint not null auto_increment,
        value double precision,
        version integer not null,
        dimensionType_id bigint,
        unitOfMeasure_id bigint,
        primary key (id)
    ) type=InnoDB;

    create table PackagingType (
        id bigint not null auto_increment,
        description varchar(255),
        name varchar(255) not null unique,
        version integer not null,
        primary key (id)
    ) type=InnoDB;

    create table PartnerIdXDeliveryLocation (
        id bigint not null auto_increment,
        deliveryLocation varchar(255) not null,
        partnerId varchar(255) not null unique,
        version integer not null,
        primary key (id)
    ) type=InnoDB;

    create table PartnerIdXDeliveryLocation_AUD (
        id bigint not null,
        REV integer not null,
        REVTYPE tinyint,
        deliveryLocation varchar(255),
        partnerId varchar(255),
        primary key (id, REV)
    ) type=InnoDB;

    create table PasswordResetEntry (
        id bigint not null auto_increment,
        expirationDate datetime,
        generatedKey varchar(255),
        hash varchar(255),
        salt varchar(255),
        version integer not null,
        user_id bigint,
        primary key (id)
    ) type=InnoDB;

    create table PasswordResetEntry_AUD (
        id bigint not null,
        REV integer not null,
        REVTYPE tinyint,
        expirationDate datetime,
        generatedKey varchar(255),
        hash varchar(255),
        salt varchar(255),
        user_id bigint,
        primary key (id, REV)
    ) type=InnoDB;

    create table PaymentInformation (
        id bigint not null auto_increment,
        summary boolean not null,
        version integer not null,
        billingAddress_id bigint,
        card_id bigint,
        contact_id bigint,
        invoiceSendMethod_id bigint,
        paymentType_id bigint not null,
        primary key (id)
    ) type=InnoDB;

    create table PaymentInformation_AUD (
        id bigint not null,
        REV integer not null,
        REVTYPE tinyint,
        summary boolean,
        billingAddress_id bigint,
        card_id bigint,
        contact_id bigint,
        paymentType_id bigint,
        primary key (id, REV)
    ) type=InnoDB;

    create table PaymentType (
        id bigint not null auto_increment,
        name varchar(255),
        version integer not null,
        primary key (id)
    ) type=InnoDB;

    create table PaymentType_AUD (
        id bigint not null,
        REV integer not null,
        REVTYPE tinyint,
        name varchar(255),
        primary key (id, REV)
    ) type=InnoDB;

    create table PendingSmartSearchDeletion (
        id bigint not null auto_increment,
        hash bigint,
        version integer not null,
        catalog_id bigint,
        item_id bigint,
        primary key (id)
    ) type=InnoDB;

    create table PendingWorkflowCommand (
        id bigint not null auto_increment,
        commandName varchar(255),
        ctx blob,
        executed boolean not null,
        expires datetime,
        version integer not null,
        primary key (id)
    ) type=InnoDB;

    create table PeriodType (
        id bigint not null auto_increment,
        name varchar(255),
        version integer not null,
        primary key (id)
    ) type=InnoDB;

    create table PeriodType_AUD (
        id bigint not null,
        REV integer not null,
        REVTYPE tinyint,
        name varchar(255),
        primary key (id, REV)
    ) type=InnoDB;

    create table Permission (
        id bigint not null auto_increment,
        allowDelete boolean not null,
        allowRead boolean not null,
        allowWrite boolean not null,
        description varchar(255),
        name varchar(255) not null,
        version integer not null,
        primary key (id)
    ) type=InnoDB;

    create table PermissionXUser (
        id bigint not null auto_increment,
        expirationDate date,
        startDate date,
        version integer not null,
        permission_id bigint,
        user_id bigint,
        primary key (id)
    ) type=InnoDB;

    create table PermissionXUser_AUD (
        id bigint not null,
        REV integer not null,
        REVTYPE tinyint,
        expirationDate date,
        startDate date,
        permission_id bigint,
        primary key (id, REV)
    ) type=InnoDB;

    create table Permission_AUD (
        id bigint not null,
        REV integer not null,
        REVTYPE tinyint,
        allowDelete boolean,
        allowRead boolean,
        allowWrite boolean,
        description varchar(255),
        name varchar(255),
        primary key (id, REV)
    ) type=InnoDB;

    create table Person (
        id bigint not null auto_increment,
        email varchar(255),
        firstName varchar(255),
        lastName varchar(255),
        middleInitial varchar(255),
        prefix varchar(255),
        suffix varchar(255),
        version integer not null,
        primary key (id)
    ) type=InnoDB;

    create table Person_AUD (
        id bigint not null,
        REV integer not null,
        REVTYPE tinyint,
        email varchar(255),
        firstName varchar(255),
        lastName varchar(255),
        middleInitial varchar(255),
        prefix varchar(255),
        suffix varchar(255),
        primary key (id, REV)
    ) type=InnoDB;

    create table PhoneNumber (
        id bigint not null auto_increment,
        areaCode varchar(255),
        countryCode varchar(255),
        exchange varchar(255),
        extension varchar(255),
        lineNumber varchar(255),
        value varchar(255),
        version integer not null,
        account_id bigint,
        person_id bigint,
        type_id bigint,
        vendor_id bigint,
        primary key (id)
    ) type=InnoDB;

    create table PhoneNumberType (
        id bigint not null auto_increment,
        name varchar(255) not null unique,
        version integer not null,
        primary key (id)
    ) type=InnoDB;

    create table PhoneNumberType_AUD (
        id bigint not null,
        REV integer not null,
        REVTYPE tinyint,
        name varchar(255),
        primary key (id, REV)
    ) type=InnoDB;

    create table PhoneNumber_AUD (
        id bigint not null,
        REV integer not null,
        REVTYPE tinyint,
        areaCode varchar(255),
        countryCode varchar(255),
        exchange varchar(255),
        extension varchar(255),
        lineNumber varchar(255),
        value varchar(255),
        account_id bigint,
        person_id bigint,
        type_id bigint,
        vendor_id bigint,
        primary key (id, REV)
    ) type=InnoDB;

    create table Pick (
        id bigint not null auto_increment,
        apdOrderNumber varchar(255),
        customerPoNumber varchar(255),
        inventoryId varchar(255),
        isShipped boolean,
        lineReference varchar(255),
        orderNumber varchar(255),
        qtyBo decimal(19,2),
        qtyToPick decimal(19,2),
        shipperId varchar(255) not null,
        version integer not null,
        lineItem_id bigint,
        primary key (id)
    ) type=InnoDB;

    create table PoNumber (
        id bigint not null auto_increment,
        expirationDate date,
        startDate date,
        value varchar(255),
        version integer not null,
        type_id bigint not null,
        primary key (id)
    ) type=InnoDB;

    create table PoNumberType (
        id bigint not null auto_increment,
        name varchar(255) not null unique,
        version integer not null,
        primary key (id)
    ) type=InnoDB;

    create table PoNumberType_AUD (
        id bigint not null,
        REV integer not null,
        REVTYPE tinyint,
        name varchar(255),
        primary key (id, REV)
    ) type=InnoDB;

    create table PoNumber_CustomerOrder (
        poNumbers_id bigint not null,
        orders_id bigint not null,
        primary key (poNumbers_id, orders_id)
    ) type=InnoDB;

    create table PricingType (
        id bigint not null auto_increment,
        name varchar(255),
        parameterRegEx varchar(255) not null,
        useCeiling boolean not null,
        version integer not null,
        primary key (id)
    ) type=InnoDB;

    create table PricingType_AUD (
        id bigint not null,
        REV integer not null,
        REVTYPE tinyint,
        name varchar(255),
        parameterRegEx varchar(255),
        useCeiling boolean,
        primary key (id, REV)
    ) type=InnoDB;

    create table Process (
        id bigint not null auto_increment,
        name varchar(255),
        version integer not null,
        primary key (id)
    ) type=InnoDB;

    create table ProcessConfiguration (
        id bigint not null auto_increment,
        version integer not null,
        account_id bigint,
        credential_id bigint,
        process_id bigint not null,
        primary key (id),
        unique (id, credential_id),
        unique (id, account_id)
    ) type=InnoDB;

    create table ProcessVariable (
        id bigint not null auto_increment,
        name varchar(255) not null,
        value varchar(255),
        version integer not null,
        processConfiguration_id bigint,
        primary key (id),
        unique (processConfiguration_id, name)
    ) type=InnoDB;

    create table PunchoutSession (
        id bigint not null auto_increment,
        browserFormPostTargetFrame varchar(255),
        browserFormPostUrl varchar(255),
        buyerCookie varchar(255),
        endTime datetime,
        initTime datetime,
        operation varchar2(10) default 'create' not null,
        sessionToken varchar(255),
        systemUserLoginName varchar(255),
        version integer not null,
        accntCredUser_id bigint,
        customerOrder_id bigint,
        primary key (id)
    ) type=InnoDB;

    create table REVINFO (
        REV integer not null auto_increment,
        revisionDate datetime,
        REVTSTMP bigint,
        username varchar(255),
        primary key (REV)
    ) type=InnoDB;

    create table ReturnOrder (
        id bigint not null auto_increment,
        closeDate datetime,
        createdDate datetime,
        raNumber varchar(255),
        reconciledDate datetime,
        status varchar(255),
        version integer not null,
        order_id bigint,
        restockingFee_id bigint,
        primary key (id)
    ) type=InnoDB;

    create table Role (
        id bigint not null auto_increment,
        isActive boolean not null,
        name varchar(255) not null unique,
        version integer not null,
        primary key (id)
    ) type=InnoDB;

    create table RoleXUser (
        id bigint not null auto_increment,
        expirationDate date,
        startDate date,
        version integer not null,
        role_id bigint,
        user_id bigint,
        primary key (id)
    ) type=InnoDB;

    create table RoleXUser_AUD (
        id bigint not null,
        REV integer not null,
        REVTYPE tinyint,
        expirationDate date,
        startDate date,
        role_id bigint,
        primary key (id, REV)
    ) type=InnoDB;

    create table Role_AUD (
        id bigint not null,
        REV integer not null,
        REVTYPE tinyint,
        isActive boolean,
        name varchar(255),
        primary key (id, REV)
    ) type=InnoDB;

    create table Role_Permission (
        roles_id bigint not null,
        permissions_id bigint not null,
        primary key (roles_id, permissions_id)
    ) type=InnoDB;

    create table Role_Permission_AUD (
        REV integer not null,
        roles_id bigint not null,
        permissions_id bigint not null,
        REVTYPE tinyint,
        primary key (REV, roles_id, permissions_id)
    ) type=InnoDB;

    create table SERVICE_LOG (
        id bigint not null auto_increment,
        contactEmail varchar(255),
        contactName varchar(255),
        contactNumber varchar(255),
        department varchar(255),
        followUpComplete boolean,
        openedDate datetime not null,
        resolution varchar(255),
        resolvedDate datetime,
        status varchar(255),
        ticketNumber varchar(255),
        updateTimestamp datetime,
        version integer not null,
        serviceDetails longtext,
        customerOrder_id bigint,
        account_id bigint,
        apdCsrRep_id bigint,
        contactMethod_id bigint,
        serviceReason_id bigint,
        primary key (id)
    ) type=InnoDB;

    create table SerialNumber (
        id bigint not null auto_increment,
        serialNumber varchar(255),
        inventoryItem_id bigint not null,
        primary key (id)
    ) type=InnoDB;

    create table ServiceReason (
        id bigint not null auto_increment,
        name varchar(255),
        version integer not null,
        primary key (id)
    ) type=InnoDB;

    create table ShipManifest (
        id bigint not null auto_increment,
        bulkCartonIndicator varchar(255),
        carrierCode varchar(255),
        cartonBarcode varchar(255),
        cartonID varchar(255) unique,
        cartonWeight varchar(255),
        customerAddress1 varchar(255),
        customerAddress2 varchar(255),
        customerBarCode varchar(255),
        customerCity varchar(255),
        customerName varchar(255),
        customerNumber varchar(255),
        customerPostalCode varchar(255),
        customerPurchaseOrderNumber varchar(255),
        customerReferenceData varchar(255),
        customerRouteData varchar(255),
        customerRouteDescription varchar(255),
        customerState varchar(255),
        dateProcessed date,
        dateTimeSent date,
        dealerInformation1 varchar(255),
        dealerInformation2 varchar(255),
        dealerInformation3 varchar(255),
        dealerInformation4 varchar(255),
        dealerInformation5 varchar(255),
        dealerInformation6 varchar(255),
        deliveryDate date,
        deliveryMethodCode varchar(255),
        deliveryTypeCode varchar(255),
        documentID varchar(255),
        endConsumerAddress1 varchar(255),
        endConsumerAddress2 varchar(255),
        endConsumerAddress3 varchar(255),
        endConsumerCity varchar(255),
        endConsumerName varchar(255),
        endConsumerPostalCode varchar(255),
        endConsumerPurchaseOrderData varchar(255),
        endConsumerState varchar(255),
        facilityAbbreviation varchar(255),
        facilityAddress1 varchar(255),
        facilityAddress2 varchar(255),
        facilityCity varchar(255),
        facilityName varchar(255),
        facilityNumber varchar(255),
        facilityPostalCode varchar(255),
        facilityState varchar(255),
        fillFacilityNumber varchar(255),
        hazardousMaterialIndicator varchar(255),
        isManifested boolean,
        itemPrefix varchar(255),
        itemStock varchar(255),
        manifestDate date,
        numBoxes decimal(19,2),
        orderNumber varchar(255),
        primaryOrderNumber varchar(255),
        scanDateTime date,
        selectionTypeCode varchar(255),
        shipTruckCode varchar(255),
        shipTruckDockId varchar(255),
        shippingInformation1 varchar(255),
        shippingInformation2 varchar(255),
        shippingInformation3 varchar(255),
        shippingInformation4 varchar(255),
        shippingInformation5 varchar(255),
        shippingInformation6 varchar(255),
        specialInstructions1 varchar(255),
        specialInstructions2 varchar(255),
        subOrderNumber varchar(255),
        tradingPartnerID varchar(255),
        truckCodeDescription varchar(255),
        type varchar(255),
        version integer not null,
        customerOrder_id bigint,
        primary key (id)
    ) type=InnoDB;

    create table ShipManifest_Pick (
        ShipManifest_id bigint not null,
        picks_id bigint not null,
        primary key (ShipManifest_id, picks_id),
        unique (picks_id)
    ) type=InnoDB;

    create table Shipment (
        id bigint not null auto_increment,
        deliveredTime datetime,
        shipTime datetime,
        subTotalCost decimal(19,2) not null,
        subTotalPrice decimal(19,2) not null,
        toCharge boolean,
        totalFreightCost decimal(19,2) not null,
        totalFreightPrice decimal(19,2) not null,
        totalShippingCost decimal(19,2) not null,
        totalShippingPrice decimal(19,2) not null,
        totalTaxPrice decimal(19,2) not null,
        trackingNumber varchar(255),
        version integer not null,
        apdInvoice_id bigint,
        customerServiceAddress_id bigint,
        order_id bigint,
        parentShipment_id bigint,
        address_id bigint,
        shippingPartner_id bigint,
        primary key (id)
    ) type=InnoDB;

    create table ShippingPartner (
        id bigint not null auto_increment,
        duns varchar(255) not null unique,
        name varchar(255) not null unique,
        scacCode varchar(255) not null unique,
        version integer not null,
        primary key (id)
    ) type=InnoDB;

    create table ShippingPartner_AUD (
        id bigint not null,
        REV integer not null,
        REVTYPE tinyint,
        duns varchar(255),
        name varchar(255),
        scacCode varchar(255),
        primary key (id, REV)
    ) type=InnoDB;

    create table Sku (
        id bigint not null auto_increment,
        value varchar(255),
        version integer not null,
        item_id bigint,
        type_id bigint,
        primary key (id)
    ) type=InnoDB;

    create table SkuType (
        id bigint not null auto_increment,
        name varchar(255) not null unique,
        version integer not null,
        primary key (id)
    ) type=InnoDB;

    create table SkuType_AUD (
        id bigint not null,
        REV integer not null,
        REVTYPE tinyint,
        name varchar(255),
        primary key (id, REV)
    ) type=InnoDB;

    create table SkuType_HierarchyNode (
        skuTypes_id bigint not null,
        hierarchyNodes_id bigint not null,
        primary key (skuTypes_id, hierarchyNodes_id)
    ) type=InnoDB;

    create table SyncItemResult (
        id bigint not null auto_increment,
        correlationId varchar(255),
        errors longtext,
        resultApdSku varchar(255),
        resultVendor varchar(255),
        version integer not null,
        warnings longtext,
        summary_id bigint,
        primary key (id)
    ) type=InnoDB;

    create table SyncItemResultSummary (
        id bigint not null auto_increment,
        correlationId varchar(255),
        created integer not null,
        discontinued integer not null,
        errors integer not null,
        failures integer not null,
        total integer,
        updated integer not null,
        version integer not null,
        warnings integer not null,
        zeroPrice integer not null,
        primary key (id)
    ) type=InnoDB;

    create table SystemComment (
        id bigint not null auto_increment,
        commentDate datetime,
        content longtext,
        fileUrl varchar(255),
        madeBySystem boolean,
        version integer not null,
        shipment_id bigint,
        user_id bigint,
        order_id bigint,
        primary key (id),
        check ((MADEBYSYSTEM = 1) OR (USER_ID IS NOT NULL))
    ) type=InnoDB;

    create table SystemUser (
        id bigint not null auto_increment,
        catalogSelectionType varchar(255),
        changePassword boolean not null,
        concurrentAccess boolean not null,
        creationDate date,
        creditCardAttempts integer,
        isActive boolean not null,
        lastCreditCardAttemptDate datetime,
        lastLoginAttemptDate date,
        lastLoginDate date,
        login varchar(255) not null unique,
        loginAttempts integer,
        password varchar(255),
        passwordCreationDate date,
        previousCreditCardAttemptDate datetime,
        salt varchar(255),
        status varchar(255),
        version integer not null,
        paymentInformation_id bigint,
        person_id bigint not null,
        primary key (id),
        unique (person_id)
    ) type=InnoDB;

    create table SystemUser_AUD (
        id bigint not null,
        REV integer not null,
        REVTYPE tinyint,
        catalogSelectionType varchar(255),
        changePassword boolean,
        concurrentAccess boolean,
        creationDate date,
        creditCardAttempts integer,
        isActive boolean,
        lastCreditCardAttemptDate datetime,
        lastLoginAttemptDate date,
        lastLoginDate date,
        login varchar(255),
        loginAttempts integer,
        password varchar(255),
        passwordCreationDate date,
        previousCreditCardAttemptDate datetime,
        salt varchar(255),
        status varchar(255),
        paymentInformation_id bigint,
        person_id bigint,
        primary key (id, REV)
    ) type=InnoDB;

    create table SystemUser_Account (
        users_id bigint not null,
        accounts_id bigint not null,
        primary key (users_id, accounts_id)
    ) type=InnoDB;

    create table SystemUser_Account_AUD (
        REV integer not null,
        users_id bigint not null,
        accounts_id bigint not null,
        REVTYPE tinyint,
        primary key (REV, users_id, accounts_id)
    ) type=InnoDB;

    create table SystemUser_Bulletin (
        SystemUser_id bigint not null,
        bulletins_id bigint not null,
        primary key (SystemUser_id, bulletins_id)
    ) type=InnoDB;

    create table SystemUser_Bulletin_AUD (
        REV integer not null,
        SystemUser_id bigint not null,
        bulletins_id bigint not null,
        REVTYPE tinyint,
        primary key (REV, SystemUser_id, bulletins_id)
    ) type=InnoDB;

    create table SystemUser_RoleXUser_AUD (
        REV integer not null,
        user_id bigint not null,
        id bigint not null,
        REVTYPE tinyint,
        primary key (REV, user_id, id)
    ) type=InnoDB;

    create table TaxRate (
        id bigint not null auto_increment,
        city varchar(255),
        cityLocalSales decimal(19,2),
        cityLocalUse decimal(19,2),
        citySales decimal(19,2),
        cityUse decimal(19,2),
        combinedSales decimal(19,2),
        combinedUse decimal(19,2),
        county varchar(255),
        countyDefault varchar(255),
        countyFips varchar(255),
        countyLocalSales decimal(19,2),
        countyLocalUse decimal(19,2),
        countySales decimal(19,2),
        countyUse decimal(19,2),
        effectiveDate date,
        expireDate date,
        generalDefault varchar(255),
        geocode varchar(255),
        locationInCity varchar(255),
        state varchar(255),
        stateSales decimal(19,2),
        stateUse decimal(19,2),
        version integer not null,
        zip varchar(255),
        primary key (id)
    ) type=InnoDB;

    create table TaxRateLog (
        id bigint not null auto_increment,
        action varchar(255),
        identifier varchar(255),
        status varchar(255),
        timestamp datetime,
        type varchar(255),
        version integer not null,
        zip varchar(255),
        primary key (id)
    ) type=InnoDB;

    create table TaxType (
        id bigint not null auto_increment,
        name varchar(255),
        version integer not null,
        primary key (id)
    ) type=InnoDB;

    create table TopLevelHierarchyXCatalog (
        id bigint not null auto_increment,
        description varchar(255) not null,
        ordering integer not null,
        version integer not null,
        catalog_id bigint,
        primary key (id)
    ) type=InnoDB;

    create table UnitOfMeasure (
        id bigint not null auto_increment,
        name varchar(255) not null unique,
        version integer not null,
        primary key (id)
    ) type=InnoDB;

    create table UnitOfMeasure_AUD (
        id bigint not null,
        REV integer not null,
        REVTYPE tinyint,
        name varchar(255),
        primary key (id, REV)
    ) type=InnoDB;

    create table UserBRMSRecord (
        id bigint not null auto_increment,
        active boolean,
        processId bigint,
        sessionId integer,
        snapshot varchar(255),
        version integer not null,
        userRequest_id bigint,
        primary key (id)
    ) type=InnoDB;

    create table UserRequest (
        id bigint not null auto_increment,
        token varchar(255),
        version integer not null,
        primary key (id)
    ) type=InnoDB;

    create table ValidationError (
        id bigint not null auto_increment,
        description varchar(255),
        name varchar(255) not null unique,
        version integer not null,
        primary key (id)
    ) type=InnoDB;

    create table Vendor (
        id bigint not null auto_increment,
        activationDate date,
        communicationDestination varchar(255) not null,
        communicationMessageType varchar(255) not null,
        controlAccountNumber varchar(255),
        expirationDate date,
        name varchar(255) not null unique,
        partnerId varchar(255),
        remitTo varchar(255),
        solomonVendorId varchar(255),
        version integer not null,
        contact_id bigint,
        primary key (id)
    ) type=InnoDB;

    create table VendorPo (
        id bigint not null auto_increment,
        manufactuerSku varchar(255),
        quantity integer not null,
        vendorSku varchar(255),
        inventoryItem_id bigint,
        lineitem_id bigint,
        primary key (id)
    ) type=InnoDB;

    create table VendorPropertyType (
        id bigint not null auto_increment,
        name varchar(255) not null unique,
        version integer not null,
        primary key (id)
    ) type=InnoDB;

    create table VendorPropertyType_AUD (
        id bigint not null,
        REV integer not null,
        REVTYPE tinyint,
        name varchar(255),
        primary key (id, REV)
    ) type=InnoDB;

    create table VendorXVendorPropertyType (
        id bigint not null auto_increment,
        value varchar(255),
        version integer not null,
        type_id bigint not null,
        vendor_id bigint,
        primary key (id)
    ) type=InnoDB;

    create table VendorXVendorPropertyType_AUD (
        id bigint not null,
        REV integer not null,
        REVTYPE tinyint,
        value varchar(255),
        type_id bigint,
        primary key (id, REV)
    ) type=InnoDB;

    create table Vendor_AUD (
        id bigint not null,
        REV integer not null,
        REVTYPE tinyint,
        activationDate date,
        communicationDestination varchar(255),
        communicationMessageType varchar(255),
        controlAccountNumber varchar(255),
        expirationDate date,
        name varchar(255),
        partnerId varchar(255),
        remitTo varchar(255),
        solomonVendorId varchar(255),
        contact_id bigint,
        primary key (id, REV)
    ) type=InnoDB;

    create table Vendor_Address_AUD (
        REV integer not null,
        vendor_id bigint not null,
        id bigint not null,
        REVTYPE tinyint,
        primary key (REV, vendor_id, id)
    ) type=InnoDB;

    create table Vendor_CommunicationMethod (
        vendors_id bigint not null,
        communicationMethods_id bigint not null,
        primary key (vendors_id, communicationMethods_id)
    ) type=InnoDB;

    create table Vendor_CommunicationMethod_AUD (
        REV integer not null,
        vendors_id bigint not null,
        communicationMethods_id bigint not null,
        REVTYPE tinyint,
        primary key (REV, vendors_id, communicationMethods_id)
    ) type=InnoDB;

    create table Vendor_PhoneNumber_AUD (
        REV integer not null,
        vendor_id bigint not null,
        id bigint not null,
        REVTYPE tinyint,
        primary key (REV, vendor_id, id)
    ) type=InnoDB;

    create table WorkflowLog (
        id bigint not null auto_increment,
        changeDate datetime not null,
        csrUser varchar(255) not null,
        target varchar(255),
        taskEvent varchar(255) not null,
        taskId bigint not null,
        version integer not null,
        primary key (id)
    ) type=InnoDB;

    create table ZipPlusFour (
        id bigint not null auto_increment,
        countyFips varchar(255),
        hi varchar(255),
        low varchar(255),
        version integer not null,
        zip varchar(255),
        primary key (id)
    ) type=InnoDB;

    create table acxacpt_AUD (
        id bigint not null,
        REV integer not null,
        REVTYPE tinyint,
        value varchar(255),
        type_id bigint,
        primary key (id, REV)
    ) type=InnoDB;

    create table axapt_AUD (
        id bigint not null,
        REV integer not null,
        REVTYPE tinyint,
        value varchar(255),
        address_id bigint,
        type_id bigint,
        primary key (id, REV)
    ) type=InnoDB;

    create table axcxu_aud (
        id bigint not null,
        REV integer not null,
        REVTYPE tinyint,
        active boolean,
        credentialSelectionImage varchar(255),
        name varchar(255),
        punchLevel varchar(255),
        usAccountNumber varchar(255),
        account_id bigint,
        address_id bigint,
        approverUser_id bigint,
        cred_id bigint,
        paymentInformation_id bigint,
        user_id bigint,
        primary key (id, REV)
    ) type=InnoDB;

    create table cctranlog_faildetails (
        cctranlog_id bigint not null,
        failuredetails varchar(255)
    ) type=InnoDB;

    create table cctranlog_vendorList (
        cctranlog_id bigint not null,
        vendorList varchar(255)
    ) type=InnoDB;

    create table comp_favlist_catxitem (
        items_id bigint not null,
        favoriteslist_id bigint not null,
        primary key (items_id, favoriteslist_id)
    ) type=InnoDB;

    create table customerOutFromCred (
        Credential_id bigint not null,
        customerOutoingFromCred_id bigint not null,
        primary key (Credential_id, customerOutoingFromCred_id),
        unique (customerOutoingFromCred_id)
    ) type=InnoDB;

    create table customerOutFromCred_AUD (
        REV integer not null,
        Credential_id bigint not null,
        customerOutoingFromCred_id bigint not null,
        REVTYPE tinyint,
        primary key (REV, Credential_id, customerOutoingFromCred_id)
    ) type=InnoDB;

    create table customerOutSenderCred (
        Credential_id bigint not null,
        customerOutoingSenderCred_id bigint not null,
        primary key (Credential_id, customerOutoingSenderCred_id),
        unique (customerOutoingSenderCred_id)
    ) type=InnoDB;

    create table customerOutSenderCred_AUD (
        REV integer not null,
        Credential_id bigint not null,
        customerOutoingSenderCred_id bigint not null,
        REVTYPE tinyint,
        primary key (REV, Credential_id, customerOutoingSenderCred_id)
    ) type=InnoDB;

    create table customerOutToCred (
        Credential_id bigint not null,
        customerOutoingToCred_id bigint not null,
        primary key (Credential_id, customerOutoingToCred_id),
        unique (customerOutoingToCred_id)
    ) type=InnoDB;

    create table customerOutToCred_AUD (
        REV integer not null,
        Credential_id bigint not null,
        customerOutoingToCred_id bigint not null,
        REVTYPE tinyint,
        primary key (REV, Credential_id, customerOutoingToCred_id)
    ) type=InnoDB;

    create table cxcpt_AUD (
        REV integer not null,
        cred_id bigint not null,
        id bigint not null,
        REVTYPE tinyint,
        primary key (REV, cred_id, id)
    ) type=InnoDB;

    create table cxcxpt_AUD (
        id bigint not null,
        REV integer not null,
        REVTYPE tinyint,
        parameter varchar(255),
        catalog_id bigint,
        type_id bigint,
        primary key (id, REV)
    ) type=InnoDB;

    create table favoriteslist_catalogxitem (
        favoriteslist_id bigint not null,
        items_id bigint not null,
        primary key (favoriteslist_id, items_id)
    ) type=InnoDB;

    create table from_credential_table (
        CxmlConfiguration_id bigint not null,
        fromCredentials_id bigint not null,
        primary key (CxmlConfiguration_id, fromCredentials_id),
        unique (fromCredentials_id)
    ) type=InnoDB;

    create table from_credential_table_AUD (
        REV integer not null,
        CxmlConfiguration_id bigint not null,
        fromCredentials_id bigint not null,
        REVTYPE tinyint,
        primary key (REV, CxmlConfiguration_id, fromCredentials_id)
    ) type=InnoDB;

    create table liXShipment_vendorComments (
        liXShipment_id bigint not null,
        vendorComment varchar(255)
    ) type=InnoDB;

    create table lineItem_vendorComments (
        lineItem_id bigint not null,
        vendorComment varchar(255)
    ) type=InnoDB;

    create table outFromCredentialTable (
        Credential_id bigint not null,
        outgoingFromCredentials_id bigint not null,
        primary key (Credential_id, outgoingFromCredentials_id),
        unique (outgoingFromCredentials_id)
    ) type=InnoDB;

    create table outFromCredentialTable_AUD (
        REV integer not null,
        Credential_id bigint not null,
        outgoingFromCredentials_id bigint not null,
        REVTYPE tinyint,
        primary key (REV, Credential_id, outgoingFromCredentials_id)
    ) type=InnoDB;

    create table outSenderCredentialTable (
        Credential_id bigint not null,
        outgoingSenderCredentials_id bigint not null,
        primary key (Credential_id, outgoingSenderCredentials_id),
        unique (outgoingSenderCredentials_id)
    ) type=InnoDB;

    create table outSenderCredentialTable_AUD (
        REV integer not null,
        Credential_id bigint not null,
        outgoingSenderCredentials_id bigint not null,
        REVTYPE tinyint,
        primary key (REV, Credential_id, outgoingSenderCredentials_id)
    ) type=InnoDB;

    create table outToCredentialTable (
        Credential_id bigint not null,
        outgoingToCredentials_id bigint not null,
        primary key (Credential_id, outgoingToCredentials_id),
        unique (outgoingToCredentials_id)
    ) type=InnoDB;

    create table outToCredentialTable_AUD (
        REV integer not null,
        Credential_id bigint not null,
        outgoingToCredentials_id bigint not null,
        REVTYPE tinyint,
        primary key (REV, Credential_id, outgoingToCredentials_id)
    ) type=InnoDB;

    create table sender_credential_table (
        CxmlConfiguration_id bigint not null,
        senderCredentials_id bigint not null,
        primary key (CxmlConfiguration_id, senderCredentials_id),
        unique (senderCredentials_id)
    ) type=InnoDB;

    create table sender_credential_table_AUD (
        REV integer not null,
        CxmlConfiguration_id bigint not null,
        senderCredentials_id bigint not null,
        REVTYPE tinyint,
        primary key (REV, CxmlConfiguration_id, senderCredentials_id)
    ) type=InnoDB;

    create table to_credential_table (
        CxmlConfiguration_id bigint not null,
        toCredentials_id bigint not null,
        primary key (CxmlConfiguration_id, toCredentials_id),
        unique (toCredentials_id)
    ) type=InnoDB;

    create table to_credential_table_AUD (
        REV integer not null,
        CxmlConfiguration_id bigint not null,
        toCredentials_id bigint not null,
        REVTYPE tinyint,
        primary key (REV, CxmlConfiguration_id, toCredentials_id)
    ) type=InnoDB;

    create table vxvxvpt_AUD (
        REV integer not null,
        vendor_id bigint not null,
        id bigint not null,
        REVTYPE tinyint,
        primary key (REV, vendor_id, id)
    ) type=InnoDB;

    alter table ACCXCREDXUSER_CCENTER 
        add index FKCE242C77FFE1BA34 (ACCOUNTCREDENTIALUSERS_ID), 
        add constraint FKCE242C77FFE1BA34 
        foreign key (ACCOUNTCREDENTIALUSERS_ID) 
        references AccountXCredentialXUser (id);

    alter table ACCXCREDXUSER_CCENTER 
        add index FKCE242C77179FBA96 (ALLOWEDCOSTCENTERS_ID), 
        add constraint FKCE242C77179FBA96 
        foreign key (ALLOWEDCOSTCENTERS_ID) 
        references CostCenter (id);

    alter table ACCXCREDXUSER_CCENTER_AUD 
        add index FK8EE746C875D1432 (REV), 
        add constraint FK8EE746C875D1432 
        foreign key (REV) 
        references REVINFO (REV);

    alter table AccXCredXUser_Dept 
        add index FK44393CA6540E04AE (departments_id), 
        add constraint FK44393CA6540E04AE 
        foreign key (departments_id) 
        references Department (id);

    alter table AccXCredXUser_Dept 
        add index FK44393CA661F26857 (AccountXCredentialXUser_id), 
        add constraint FK44393CA661F26857 
        foreign key (AccountXCredentialXUser_id) 
        references AccountXCredentialXUser (id);

    alter table AccXCredXUser_Dept_AUD 
        add index FKD68AA77775D1432 (REV), 
        add constraint FKD68AA77775D1432 
        foreign key (REV) 
        references REVINFO (REV);

    alter table AccXCredXUser_Desktops 
        add index FKDB3499B861F26857 (AccountXCredentialXUser_id), 
        add constraint FKDB3499B861F26857 
        foreign key (AccountXCredentialXUser_id) 
        references AccountXCredentialXUser (id);

    alter table AccXCredXUser_Desktops 
        add index FKDB3499B8377FE6DC (desktops_id), 
        add constraint FKDB3499B8377FE6DC 
        foreign key (desktops_id) 
        references Desktop (id);

    alter table AccXCredXUser_Desktops_AUD 
        add index FK978D2B8975D1432 (REV), 
        add constraint FK978D2B8975D1432 
        foreign key (REV) 
        references REVINFO (REV);

    create index ACCOUNT_NAME_IDX on Account (name);

    alter table Account 
        add index FK1D0C220D48B9DF01 (parentAccount_id), 
        add constraint FK1D0C220D48B9DF01 
        foreign key (parentAccount_id) 
        references Account (id);

    alter table Account 
        add index FK1D0C220D7ED1037D (paymentInformation_id), 
        add constraint FK1D0C220D7ED1037D 
        foreign key (paymentInformation_id) 
        references PaymentInformation (id);

    alter table Account 
        add index FK1D0C220D46F7094 (primaryContact_id), 
        add constraint FK1D0C220D46F7094 
        foreign key (primaryContact_id) 
        references Person (id);

    alter table Account 
        add index FK1D0C220DD0B0DA57 (cxmlConfiguration_id), 
        add constraint FK1D0C220DD0B0DA57 
        foreign key (cxmlConfiguration_id) 
        references CxmlConfiguration (id);

    alter table Account 
        add index FK1D0C220DE5957139 (rootAccount_id), 
        add constraint FK1D0C220DE5957139 
        foreign key (rootAccount_id) 
        references Account (id);

    alter table AccountBRMSRecord 
        add index FK9D4C7BB4185A22D7 (account_id), 
        add constraint FK9D4C7BB4185A22D7 
        foreign key (account_id) 
        references Account (id);

    alter table AccountGroup_AUD 
        add index FK1846C94375D1432 (REV), 
        add constraint FK1846C94375D1432 
        foreign key (REV) 
        references REVINFO (REV);

    create index ACCOUNT_LINEAGE_IDX on AccountLineage (priority);

    alter table AccountLineage 
        add index FKF99CD1BE1976C0EA (ancestorAccount_id), 
        add constraint FKF99CD1BE1976C0EA 
        foreign key (ancestorAccount_id) 
        references Account (id);

    alter table AccountLineage 
        add index FKF99CD1BE185A22D7 (account_id), 
        add constraint FKF99CD1BE185A22D7 
        foreign key (account_id) 
        references Account (id);

    alter table AccountLineage_AUD 
        add index FK576DF08F75D1432 (REV), 
        add constraint FK576DF08F75D1432 
        foreign key (REV) 
        references REVINFO (REV);

    alter table AccountPropertyType_AUD 
        add index FKEF0CD92D75D1432 (REV), 
        add constraint FKEF0CD92D75D1432 
        foreign key (REV) 
        references REVINFO (REV);

    alter table AccountXAccountPropertyType 
        add index FKF7E20391185A22D7 (account_id), 
        add constraint FKF7E20391185A22D7 
        foreign key (account_id) 
        references Account (id);

    alter table AccountXAccountPropertyType 
        add index FKF7E20391460EDA79 (type_id), 
        add constraint FKF7E20391460EDA79 
        foreign key (type_id) 
        references AccountPropertyType (id);

    alter table AccountXCredentialXUser 
        add index FK34428EE1DC8230E7 (approverUser_id), 
        add constraint FK34428EE1DC8230E7 
        foreign key (approverUser_id) 
        references SystemUser (id);

    alter table AccountXCredentialXUser 
        add index FK34428EE17ED1037D (paymentInformation_id), 
        add constraint FK34428EE17ED1037D 
        foreign key (paymentInformation_id) 
        references PaymentInformation (id);

    alter table AccountXCredentialXUser 
        add index FK34428EE199EEF977 (address_id), 
        add constraint FK34428EE199EEF977 
        foreign key (address_id) 
        references Address (id);

    alter table AccountXCredentialXUser 
        add index FK34428EE14E9ECF4C (user_id), 
        add constraint FK34428EE14E9ECF4C 
        foreign key (user_id) 
        references SystemUser (id);

    alter table AccountXCredentialXUser 
        add index FK34428EE145A534A6 (cred_id), 
        add constraint FK34428EE145A534A6 
        foreign key (cred_id) 
        references Credential (id);

    alter table AccountXCredentialXUser 
        add index FK34428EE1185A22D7 (account_id), 
        add constraint FK34428EE1185A22D7 
        foreign key (account_id) 
        references Account (id);

    alter table Account_AUD 
        add index FK970E815E75D1432 (REV), 
        add constraint FK970E815E75D1432 
        foreign key (REV) 
        references REVINFO (REV);

    alter table Account_AccountGroup 
        add index FK2FA3E964236EB562 (groups_id), 
        add constraint FK2FA3E964236EB562 
        foreign key (groups_id) 
        references AccountGroup (id);

    alter table Account_AccountGroup 
        add index FK2FA3E9647BEA6A7E (accounts_id), 
        add constraint FK2FA3E9647BEA6A7E 
        foreign key (accounts_id) 
        references Account (id);

    alter table Account_AccountGroup_AUD 
        add index FK7F34C53575D1432 (REV), 
        add constraint FK7F34C53575D1432 
        foreign key (REV) 
        references REVINFO (REV);

    alter table Account_Bulletin 
        add index FKB24DABB9367C0B98 (bulletins_id), 
        add constraint FKB24DABB9367C0B98 
        foreign key (bulletins_id) 
        references Bulletin (id);

    alter table Account_Bulletin 
        add index FKB24DABB9185A22D7 (Account_id), 
        add constraint FKB24DABB9185A22D7 
        foreign key (Account_id) 
        references Account (id);

    alter table Account_Bulletin_AUD 
        add index FK37DB550A75D1432 (REV), 
        add constraint FK37DB550A75D1432 
        foreign key (REV) 
        references REVINFO (REV);

    alter table Account_IpAddress_AUD 
        add index FK5006654C75D1432 (REV), 
        add constraint FK5006654C75D1432 
        foreign key (REV) 
        references REVINFO (REV);

    alter table Account_PoNumber 
        add index FKF0898C3A185A22D7 (Account_id), 
        add constraint FKF0898C3A185A22D7 
        foreign key (Account_id) 
        references Account (id);

    alter table Account_PoNumber 
        add index FKF0898C3ACE415F34 (blanketPos_id), 
        add constraint FKF0898C3ACE415F34 
        foreign key (blanketPos_id) 
        references PoNumber (id);

    alter table ActionType_AUD 
        add index FKA116A88175D1432 (REV), 
        add constraint FKA116A88175D1432 
        foreign key (REV) 
        references REVINFO (REV);

    alter table Address 
        add index FK1ED033D4D50C94DD (vendor_id), 
        add constraint FK1ED033D4D50C94DD 
        foreign key (vendor_id) 
        references Vendor (id);

    alter table Address 
        add index FK1ED033D4E4B11BD (miscShipTo_id), 
        add constraint FK1ED033D4E4B11BD 
        foreign key (miscShipTo_id) 
        references MiscShipTo (id);

    alter table Address 
        add index FK1ED033D43E66BDBD (person_id), 
        add constraint FK1ED033D43E66BDBD 
        foreign key (person_id) 
        references Person (id);

    alter table Address 
        add index FK1ED033D4185A22D7 (account_id), 
        add constraint FK1ED033D4185A22D7 
        foreign key (account_id) 
        references Account (id);

    alter table Address 
        add index FK1ED033D46C357B4B (type_id), 
        add constraint FK1ED033D46C357B4B 
        foreign key (type_id) 
        references AddressType (id);

    alter table AddressPropertyType_AUD 
        add index FKE49A7CF475D1432 (REV), 
        add constraint FKE49A7CF475D1432 
        foreign key (REV) 
        references REVINFO (REV);

    create index ADDRESS_TYPE_IDX on AddressType (name);

    alter table AddressType_AUD 
        add index FK952813FF75D1432 (REV), 
        add constraint FK952813FF75D1432 
        foreign key (REV) 
        references REVINFO (REV);

    alter table AddressXAddressPropertyType 
        add index FKF9E70B1F99EEF977 (address_id), 
        add constraint FKF9E70B1F99EEF977 
        foreign key (address_id) 
        references Address (id);

    alter table AddressXAddressPropertyType 
        add index FKF9E70B1FDD1B79C0 (type_id), 
        add constraint FKF9E70B1FDD1B79C0 
        foreign key (type_id) 
        references AddressPropertyType (id);

    alter table Address_AUD 
        add index FK115657A575D1432 (REV), 
        add constraint FK115657A575D1432 
        foreign key (REV) 
        references REVINFO (REV);

    create index po_apdpolog_idx on ApdPoLog (apdPo_id);

    create index createdate_apdpolog_idx on ApdPoLog (creationDate);

    alter table ApdPoLog 
        add index FK3425E7D05ECADFF1 (apdPo_id), 
        add constraint FK3425E7D05ECADFF1 
        foreign key (apdPo_id) 
        references PoNumber (id);

    alter table AttachmentType_AUD 
        add index FKFD3C366E75D1432 (REV), 
        add constraint FKFD3C366E75D1432 
        foreign key (REV) 
        references REVINFO (REV);

    create index BRAND_NAME_IX on Brand (name);

    create index BRAND_ID_IX on Brand (usscoId);

    alter table Bulletin_AUD 
        add index FK51BD901875D1432 (REV), 
        add constraint FK51BD901875D1432 
        foreign key (REV) 
        references REVINFO (REV);

    create index CAROUSEL_SUBDOMAIN_IDX on CAROUSEL_DISPLAY (subdomain);

    alter table CAROUSEL_DISPLAY 
        add index FK989B3CC331B94B57 (catalog_id), 
        add constraint FK989B3CC331B94B57 
        foreign key (catalog_id) 
        references Catalog (id);

    alter table CAROUSEL_DISPLAY_AUD 
        add index FK2B85511475D1432 (REV), 
        add constraint FK2B85511475D1432 
        foreign key (REV) 
        references REVINFO (REV);

    create index CANCEL_CANCELDATE_IDX on CanceledQuantity (cancelDate);

    create index CANCELQUANTITY_LINEITEM_IDX on CanceledQuantity (lineItem_id);

    alter table CanceledQuantity 
        add index FK3BD1E864B7777FDD (lineItem_id), 
        add constraint FK3BD1E864B7777FDD 
        foreign key (lineItem_id) 
        references LineItem (id);

    alter table CardInformation_AUD 
        add index FK69BCAFAD75D1432 (REV), 
        add constraint FK69BCAFAD75D1432 
        foreign key (REV) 
        references REVINFO (REV);

    alter table Cart 
        add index FK1FEF409ADA0556 (userCredential_id), 
        add constraint FK1FEF409ADA0556 
        foreign key (userCredential_id) 
        references AccountXCredentialXUser (id);

    alter table CartItem 
        add index FK4393E73779C529D (cart_id), 
        add constraint FK4393E73779C529D 
        foreign key (cart_id) 
        references Cart (id);

    alter table CartItem 
        add index FK4393E73D4DB665D (catalogXItem_id), 
        add constraint FK4393E73D4DB665D 
        foreign key (catalogXItem_id) 
        references CatalogXItem (id);

    alter table CashoutPageXField 
        add index FKE48C344CA5D99E16 (mapping_id), 
        add constraint FKE48C344CA5D99E16 
        foreign key (mapping_id) 
        references MessageMapping (id);

    alter table CashoutPageXField 
        add index FKE48C344C71C12AF2 (page_id), 
        add constraint FKE48C344C71C12AF2 
        foreign key (page_id) 
        references CashoutPage (id);

    alter table CashoutPageXField 
        add index FKE48C344C44537EF7 (field_id), 
        add constraint FKE48C344C44537EF7 
        foreign key (field_id) 
        references Field (id);

    alter table CashoutPageXField_AUD 
        add index FK9FAD5C1D75D1432 (REV), 
        add constraint FK9FAD5C1D75D1432 
        foreign key (REV) 
        references REVINFO (REV);

    create index CATALOG_NAME_IDX on Catalog (name);

    alter table Catalog 
        add index FK8457F7F9A6F76EBE (replacement_id), 
        add constraint FK8457F7F9A6F76EBE 
        foreign key (replacement_id) 
        references Catalog (id);

    alter table Catalog 
        add index FK8457F7F9738CD86E (listUid_id), 
        add constraint FK8457F7F9738CD86E 
        foreign key (listUid_id) 
        references ListHashToUid (id);

    alter table Catalog 
        add index FK8457F7F96277CD4D (coreListUid_id), 
        add constraint FK8457F7F96277CD4D 
        foreign key (coreListUid_id) 
        references ListHashToUid (id);

    alter table Catalog 
        add index FK8457F7F9D50C94DD (vendor_id), 
        add constraint FK8457F7F9D50C94DD 
        foreign key (vendor_id) 
        references Vendor (id);

    alter table Catalog 
        add index FK8457F7F9DE9C8386 (customer_id), 
        add constraint FK8457F7F9DE9C8386 
        foreign key (customer_id) 
        references Account (id);

    alter table Catalog 
        add index FK8457F7F92AEE8D26 (parent_id), 
        add constraint FK8457F7F92AEE8D26 
        foreign key (parent_id) 
        references Catalog (id);

    create index cs_corrId on CatalogCsv (correlationId);

    create index cu_corrId on CatalogUpload (correlationId);

    alter table CatalogXCategoryXPricingType 
        add index FK7BA786C5B7BD2710 (category_id), 
        add constraint FK7BA786C5B7BD2710 
        foreign key (category_id) 
        references ItemCategory (id);

    alter table CatalogXCategoryXPricingType 
        add index FK7BA786C51B2C461D (type_id), 
        add constraint FK7BA786C51B2C461D 
        foreign key (type_id) 
        references PricingType (id);

    alter table CatalogXCategoryXPricingType 
        add index FK7BA786C531B94B57 (catalog_id), 
        add constraint FK7BA786C531B94B57 
        foreign key (catalog_id) 
        references Catalog (id);

    create index CXI_ITEM_IDX on CatalogXItem (item_id);

    alter table CatalogXItem 
        add index FKC4EC1F3291FB7B97 (pricingType_id), 
        add constraint FKC4EC1F3291FB7B97 
        foreign key (pricingType_id) 
        references PricingType (id);

    alter table CatalogXItem 
        add index FKC4EC1F32D4B669BD (item_id), 
        add constraint FKC4EC1F32D4B669BD 
        foreign key (item_id) 
        references Item (id);

    alter table CatalogXItem 
        add index FKC4EC1F329260A0F5 (customerUnitOfmeasure_id), 
        add constraint FKC4EC1F329260A0F5 
        foreign key (customerUnitOfmeasure_id) 
        references UnitOfMeasure (id);

    alter table CatalogXItem 
        add index FKC4EC1F3220C000E2 (substituteItem_id), 
        add constraint FKC4EC1F3220C000E2 
        foreign key (substituteItem_id) 
        references CatalogXItem (id);

    alter table CatalogXItem 
        add index FKC4EC1F3231B94B57 (catalog_id), 
        add constraint FKC4EC1F3231B94B57 
        foreign key (catalog_id) 
        references Catalog (id);

    create index CATALOGXITEM_ID_IX on CatalogXItemXItemPropertyType (catalogxitem_id);

    alter table CatalogXItemXItemPropertyType 
        add index FK846B38E8D4DB665D (catalogxitem_id), 
        add constraint FK846B38E8D4DB665D 
        foreign key (catalogxitem_id) 
        references CatalogXItem (id);

    alter table CatalogXItemXItemPropertyType 
        add index FK846B38E878D89205 (type_id), 
        add constraint FK846B38E878D89205 
        foreign key (type_id) 
        references ItemPropertyType (id);

    alter table Catalog_AUD 
        add index FK2765814A75D1432 (REV), 
        add constraint FK2765814A75D1432 
        foreign key (REV) 
        references REVINFO (REV);

    alter table CategoryMarkup 
        add index FK16ACD4E6B7BD2710 (category_id), 
        add constraint FK16ACD4E6B7BD2710 
        foreign key (category_id) 
        references ItemCategory (id);

    alter table CategoryMarkup 
        add index FK16ACD4E631B94B57 (catalog_id), 
        add constraint FK16ACD4E631B94B57 
        foreign key (catalog_id) 
        references Catalog (id);

    alter table CategoryMarkup_AUD 
        add index FK98F81FB775D1432 (REV), 
        add constraint FK98F81FB775D1432 
        foreign key (REV) 
        references REVINFO (REV);

    alter table CommunicationMethod_AUD 
        add index FK570CD68875D1432 (REV), 
        add constraint FK570CD68875D1432 
        foreign key (REV) 
        references REVINFO (REV);

    alter table ContactLogComment 
        add index FKE766069B29E27F47 (apdCsrRep_id), 
        add constraint FKE766069B29E27F47 
        foreign key (apdCsrRep_id) 
        references SystemUser (id);

    alter table ContactLogComment 
        add index FKE766069B797AADDD (servicelog_id), 
        add constraint FKE766069B797AADDD 
        foreign key (servicelog_id) 
        references SERVICE_LOG (id);

    alter table ContactLogComment 
        add index FKE766069BE92BED1D (issuelog_id), 
        add constraint FKE766069BE92BED1D 
        foreign key (issuelog_id) 
        references ISSUE_LOG (id);

    alter table ContactMethod_AUD 
        add index FKFC333E5275D1432 (REV), 
        add constraint FKFC333E5275D1432 
        foreign key (REV) 
        references REVINFO (REV);

    alter table CostCenter 
        add index FK537676A269C00797 (period_id), 
        add constraint FK537676A269C00797 
        foreign key (period_id) 
        references PeriodType (id);

    alter table CostCenter_AUD 
        add index FK3062837375D1432 (REV), 
        add constraint FK3062837375D1432 
        foreign key (REV) 
        references REVINFO (REV);

    alter table Credential 
        add index FK4E15C477B4B76DDE (defaultShipFromAddress_id), 
        add constraint FK4E15C477B4B76DDE 
        foreign key (defaultShipFromAddress_id) 
        references Address (id);

    alter table Credential 
        add index FK4E15C477BACFEB7 (skuType_id), 
        add constraint FK4E15C477BACFEB7 
        foreign key (skuType_id) 
        references SkuType (id);

    alter table Credential 
        add index FK4E15C4777ED1037D (paymentInformation_id), 
        add constraint FK4E15C4777ED1037D 
        foreign key (paymentInformation_id) 
        references PaymentInformation (id);

    alter table Credential 
        add index FK4E15C4774E9ECF4C (user_id), 
        add constraint FK4E15C4774E9ECF4C 
        foreign key (user_id) 
        references SystemUser (id);

    alter table Credential 
        add index FK4E15C477E5957139 (rootAccount_id), 
        add constraint FK4E15C477E5957139 
        foreign key (rootAccount_id) 
        references Account (id);

    alter table Credential 
        add index FK4E15C47762FD6737 (communicationMethod_id), 
        add constraint FK4E15C47762FD6737 
        foreign key (communicationMethod_id) 
        references CommunicationMethod (id);

    alter table Credential 
        add index FK4E15C47731B94B57 (catalog_id), 
        add constraint FK4E15C47731B94B57 
        foreign key (catalog_id) 
        references Catalog (id);

    alter table Credential 
        add index FK4E15C477DD089F57 (cashoutPage_id), 
        add constraint FK4E15C477DD089F57 
        foreign key (cashoutPage_id) 
        references CashoutPage (id);

    alter table CredentialPropertyType_AUD 
        add index FKD2E8679775D1432 (REV), 
        add constraint FKD2E8679775D1432 
        foreign key (REV) 
        references REVINFO (REV);

    alter table CredentialXVendor 
        add index FK17F98769D50C94DD (vendor_id), 
        add constraint FK17F98769D50C94DD 
        foreign key (vendor_id) 
        references Vendor (id);

    alter table CredentialXVendor 
        add index FK17F98769AFD1BCFD (credential_id), 
        add constraint FK17F98769AFD1BCFD 
        foreign key (credential_id) 
        references Credential (id);

    alter table CredentialXVendor_AUD 
        add index FKE7D1D8BA75D1432 (REV), 
        add constraint FKE7D1D8BA75D1432 
        foreign key (REV) 
        references REVINFO (REV);

    alter table Credential_AUD 
        add index FKC4CDEC875D1432 (REV), 
        add constraint FKC4CDEC875D1432 
        foreign key (REV) 
        references REVINFO (REV);

    alter table Credential_Bulletin 
        add index FK3B63208F367C0B98 (bulletins_id), 
        add constraint FK3B63208F367C0B98 
        foreign key (bulletins_id) 
        references Bulletin (id);

    alter table Credential_Bulletin 
        add index FK3B63208FAFD1BCFD (Credential_id), 
        add constraint FK3B63208FAFD1BCFD 
        foreign key (Credential_id) 
        references Credential (id);

    alter table Credential_Bulletin_AUD 
        add index FK9CDE6EE075D1432 (REV), 
        add constraint FK9CDE6EE075D1432 
        foreign key (REV) 
        references REVINFO (REV);

    alter table Credential_CostCenter 
        add index FKAD50676AAFD1BCFD (Credential_id), 
        add constraint FKAD50676AAFD1BCFD 
        foreign key (Credential_id) 
        references Credential (id);

    alter table Credential_CostCenter 
        add index FKAD50676A309BEEE (costCenters_id), 
        add constraint FKAD50676A309BEEE 
        foreign key (costCenters_id) 
        references CostCenter (id);

    alter table Credential_CostCenter_AUD 
        add index FK36C6D03B75D1432 (REV), 
        add constraint FK36C6D03B75D1432 
        foreign key (REV) 
        references REVINFO (REV);

    alter table Credential_Department 
        add index FK33A103A540E04AE (departments_id), 
        add constraint FK33A103A540E04AE 
        foreign key (departments_id) 
        references Department (id);

    alter table Credential_Department 
        add index FK33A103AAFD1BCFD (Credential_id), 
        add constraint FK33A103AAFD1BCFD 
        foreign key (Credential_id) 
        references Credential (id);

    alter table Credential_Department_AUD 
        add index FKBB0F910B75D1432 (REV), 
        add constraint FKBB0F910B75D1432 
        foreign key (REV) 
        references REVINFO (REV);

    alter table Credential_LimitApproval 
        add index FK20BF5536AFD1BCFD (Credential_id), 
        add constraint FK20BF5536AFD1BCFD 
        foreign key (Credential_id) 
        references Credential (id);

    alter table Credential_LimitApproval 
        add index FK20BF5536C2092797 (limitApproval_id), 
        add constraint FK20BF5536C2092797 
        foreign key (limitApproval_id) 
        references LimitApproval (id);

    alter table Credential_LimitApproval_AUD 
        add index FK5A31F80775D1432 (REV), 
        add constraint FK5A31F80775D1432 
        foreign key (REV) 
        references REVINFO (REV);

    alter table Credential_Permission 
        add index FKB1D192B7AFD1BCFD (Credential_id), 
        add constraint FKB1D192B7AFD1BCFD 
        foreign key (Credential_id) 
        references Credential (id);

    alter table Credential_Permission 
        add index FKB1D192B7BC8D8328 (permissions_id), 
        add constraint FKB1D192B7BC8D8328 
        foreign key (permissions_id) 
        references Permission (id);

    alter table Credential_Permission_AUD 
        add index FK74F78D0875D1432 (REV), 
        add constraint FK74F78D0875D1432 
        foreign key (REV) 
        references REVINFO (REV);

    alter table Credential_PoNumber 
        add index FK799F0110AFD1BCFD (Credential_id), 
        add constraint FK799F0110AFD1BCFD 
        foreign key (Credential_id) 
        references Credential (id);

    alter table Credential_PoNumber 
        add index FK799F0110CE415F34 (blanketPos_id), 
        add constraint FK799F0110CE415F34 
        foreign key (blanketPos_id) 
        references PoNumber (id);

    alter table Credential_PropertyType 
        add index FK2165219745A534A6 (cred_id), 
        add constraint FK2165219745A534A6 
        foreign key (cred_id) 
        references Credential (id);

    alter table Credential_PropertyType 
        add index FK21652197B66C2309 (type_id), 
        add constraint FK21652197B66C2309 
        foreign key (type_id) 
        references CredentialPropertyType (id);

    alter table Credential_PropertyType_AUD 
        add index FKC068ABE875D1432 (REV), 
        add constraint FKC068ABE875D1432 
        foreign key (REV) 
        references REVINFO (REV);

    alter table Credential_Role 
        add index FK7D571B5E46C1C336 (roles_id), 
        add constraint FK7D571B5E46C1C336 
        foreign key (roles_id) 
        references Role (id);

    alter table Credential_Role 
        add index FK7D571B5EAFD1BCFD (Credential_id), 
        add constraint FK7D571B5EAFD1BCFD 
        foreign key (Credential_id) 
        references Credential (id);

    alter table Credential_Role_AUD 
        add index FK7BAA6A2F75D1432 (REV), 
        add constraint FK7BAA6A2F75D1432 
        foreign key (REV) 
        references REVINFO (REV);

    create index uploadeditem_field_idx on CsvUploadedItemField (uploadeditem_id);

    alter table CsvUploadedItemField 
        add index FK707855E14A59795D (uploadeditem_id), 
        add constraint FK707855E14A59795D 
        foreign key (uploadeditem_id) 
        references CsvUploadedItem (id);

    create index CC_ITEM_IX on CustomerCost (item_id);

    alter table CustomerCost 
        add index FK3F6DD8EBD4B669BD (item_id), 
        add constraint FK3F6DD8EBD4B669BD 
        foreign key (item_id) 
        references Item (id);

    alter table CustomerCost 
        add index FK3F6DD8EBDE9C8386 (customer_id), 
        add constraint FK3F6DD8EBDE9C8386 
        foreign key (customer_id) 
        references Account (id);

    create index CUSTORDER_ORDERDATE_IDX on CustomerOrder (orderDate);

    alter table CustomerOrder 
        add index FKAEF781F05F7CC357 (cardInformation_id), 
        add constraint FKAEF781F05F7CC357 
        foreign key (cardInformation_id) 
        references CardInformation (id);

    alter table CustomerOrder 
        add index FKAEF781F08B76AAE3 (parentOrder_id), 
        add constraint FKAEF781F08B76AAE3 
        foreign key (parentOrder_id) 
        references CustomerOrder (id);

    alter table CustomerOrder 
        add index FKAEF781F07ED1037D (paymentInformation_id), 
        add constraint FKAEF781F07ED1037D 
        foreign key (paymentInformation_id) 
        references PaymentInformation (id);

    alter table CustomerOrder 
        add index FKAEF781F07175679D (assignedCostCenter_id), 
        add constraint FKAEF781F07175679D 
        foreign key (assignedCostCenter_id) 
        references ACCXCREDXUSER_CCENTER (id);

    alter table CustomerOrder 
        add index FKAEF781F099EEF977 (address_id), 
        add constraint FKAEF781F099EEF977 
        foreign key (address_id) 
        references Address (id);

    alter table CustomerOrder 
        add index FKAEF781F04E9ECF4C (user_id), 
        add constraint FKAEF781F04E9ECF4C 
        foreign key (user_id) 
        references SystemUser (id);

    alter table CustomerOrder 
        add index FKAEF781F0D6EA6B25 (status_id), 
        add constraint FKAEF781F0D6EA6B25 
        foreign key (status_id) 
        references OrderStatus (id);

    alter table CustomerOrder 
        add index FKAEF781F0AFD1BCFD (credential_id), 
        add constraint FKAEF781F0AFD1BCFD 
        foreign key (credential_id) 
        references Credential (id);

    alter table CustomerOrder 
        add index FKAEF781F0185A22D7 (account_id), 
        add constraint FKAEF781F0185A22D7 
        foreign key (account_id) 
        references Account (id);

    alter table CustomerOrder 
        add index FKAEF781F04C0A74E5 (type_id), 
        add constraint FKAEF781F04C0A74E5 
        foreign key (type_id) 
        references OrderType (id);

    alter table CustomerOrder 
        add index FKAEF781F0EB7E507 (customerStatus_id), 
        add constraint FKAEF781F0EB7E507 
        foreign key (customerStatus_id) 
        references OrderStatus (id);

    alter table CustomerOrder 
        add index FKAEF781F0971F1FDF (paymentStatus_id), 
        add constraint FKAEF781F0971F1FDF 
        foreign key (paymentStatus_id) 
        references OrderStatus (id);

    alter table CustomerOrderRequest 
        add index FK5C66FA7F26CAE17 (customerOrder_id), 
        add constraint FK5C66FA7F26CAE17 
        foreign key (customerOrder_id) 
        references CustomerOrder (id);

    alter table CustomerOrder_TaxType 
        add index FKD872C6168E1EC39 (order_id), 
        add constraint FKD872C6168E1EC39 
        foreign key (order_id) 
        references CustomerOrder (id);

    alter table CustomerOrder_TaxType 
        add index FKD872C6162B5202 (type_id), 
        add constraint FKD872C6162B5202 
        foreign key (type_id) 
        references TaxType (id);

    alter table CustomerSpecificIconUrl 
        add index FKD7C87CC6F5826A90 (propertyType_id), 
        add constraint FKD7C87CC6F5826A90 
        foreign key (propertyType_id) 
        references ItemPropertyType (id);

    alter table CustomerSpecificIconUrl 
        add index FKD7C87CC68CC90C70 (classificationType_id), 
        add constraint FKD7C87CC68CC90C70 
        foreign key (classificationType_id) 
        references ItemClassificationType (id);

    alter table CustomerSpecificIconUrl 
        add index FKD7C87CC6185A22D7 (account_id), 
        add constraint FKD7C87CC6185A22D7 
        foreign key (account_id) 
        references Account (id);

    alter table CxmlConfiguration_AUD 
        add index FK5BD0393375D1432 (REV), 
        add constraint FK5BD0393375D1432 
        foreign key (REV) 
        references REVINFO (REV);

    alter table CxmlCredential_AUD 
        add index FK41C768BC75D1432 (REV), 
        add constraint FK41C768BC75D1432 
        foreign key (REV) 
        references REVINFO (REV);

    alter table Department_AUD 
        add index FKB4AB444375D1432 (REV), 
        add constraint FKB4AB444375D1432 
        foreign key (REV) 
        references REVINFO (REV);

    alter table Desktop_AUD 
        add index FKAFA7F7CD75D1432 (REV), 
        add constraint FKAFA7F7CD75D1432 
        foreign key (REV) 
        references REVINFO (REV);

    create index fic_item_id on FLATITEMCLASSIFICATION (item_id);

    alter table FLATITEMCLASSIFICATION 
        add index FKBF6E98F250060531 (ItemClassType_id), 
        add constraint FKBF6E98F250060531 
        foreign key (ItemClassType_id) 
        references ItemClassificationType (id);

    alter table FLATITEMCLASSIFICATION 
        add index FKBF6E98F2D4B669BD (item_id), 
        add constraint FKBF6E98F2D4B669BD 
        foreign key (item_id) 
        references Item (id);

    create index fis_item_id on FLATITEMSPECIFICATION (item_id);

    alter table FLATITEMSPECIFICATION 
        add index FK21CE8657D4B669BD (item_id), 
        add constraint FK21CE8657D4B669BD 
        foreign key (item_id) 
        references Item (id);

    alter table FavoritesList 
        add index FKFC5FBB7590DF4B4F (axcxu_id), 
        add constraint FKFC5FBB7590DF4B4F 
        foreign key (axcxu_id) 
        references AccountXCredentialXUser (id);

    alter table FavoritesList 
        add index FKFC5FBB7531B94B57 (catalog_id), 
        add constraint FKFC5FBB7531B94B57 
        foreign key (catalog_id) 
        references Catalog (id);

    alter table Field 
        add index FK40BB0DA532E0A57 (fieldType_id), 
        add constraint FK40BB0DA532E0A57 
        foreign key (fieldType_id) 
        references FieldType (id);

    alter table FieldOptions 
        add index FKC6EDA46444537EF7 (field_id), 
        add constraint FKC6EDA46444537EF7 
        foreign key (field_id) 
        references Field (id);

    alter table HierarchyNode 
        add index FK6128C1B7ECE7DD24 (parent_id), 
        add constraint FK6128C1B7ECE7DD24 
        foreign key (parent_id) 
        references HierarchyNode (id);

    create index INVOICE_RETURNORDER_IDX on INVOICE (returnOrder_id);

    create index INVOICE_CREATEDATE_IDX on INVOICE (createdDate);

    create index INVOICE_SHIPMENT_IDX on INVOICE (shipment_id);

    alter table INVOICE 
        add index FK9FA1CF0DBBACDEDD (shipment_id), 
        add constraint FK9FA1CF0DBBACDEDD 
        foreign key (shipment_id) 
        references Shipment (id);

    alter table INVOICE 
        add index FK9FA1CF0D96D53A97 (returnOrder_id), 
        add constraint FK9FA1CF0D96D53A97 
        foreign key (returnOrder_id) 
        references ReturnOrder (id);

    alter table INVOICE 
        add index FK9FA1CF0D26CAE17 (customerOrder_id), 
        add constraint FK9FA1CF0D26CAE17 
        foreign key (customerOrder_id) 
        references CustomerOrder (id);

    alter table INVOICE_LineItem 
        add index FK7FC5EA39753382D7 (INVOICE_id), 
        add constraint FK7FC5EA39753382D7 
        foreign key (INVOICE_id) 
        references INVOICE (id);

    alter table INVOICE_LineItem 
        add index FK7FC5EA3943253E12 (actualItems_id), 
        add constraint FK7FC5EA3943253E12 
        foreign key (actualItems_id) 
        references LineItem (id);

    alter table INVOICE_SPECIAL 
        add index FK8B80E567753382D7 (INVOICE_ID), 
        add constraint FK8B80E567753382D7 
        foreign key (INVOICE_ID) 
        references INVOICE (id);

    alter table ISSUE_LOG 
        add index FKB12F029E49053676 (reporter_id), 
        add constraint FKB12F029E49053676 
        foreign key (reporter_id) 
        references SystemUser (id);

    alter table ISSUE_LOG 
        add index FKB12F029E26CAE17 (customerOrder_id), 
        add constraint FKB12F029E26CAE17 
        foreign key (customerOrder_id) 
        references CustomerOrder (id);

    alter table ITEM_ITEM 
        add index FK2836ACBFD4B669BD (ITEM_ID), 
        add constraint FK2836ACBFD4B669BD 
        foreign key (ITEM_ID) 
        references Item (id);

    alter table ITEM_ITEM 
        add index FK2836ACBFBAC8DCDB (SIMILARITEMS_ID), 
        add constraint FK2836ACBFBAC8DCDB 
        foreign key (SIMILARITEMS_ID) 
        references Item (id);

    alter table InventoryBin 
        add index FKA3C3722B8BE735F7 (inventoryitem_id), 
        add constraint FKA3C3722B8BE735F7 
        foreign key (inventoryitem_id) 
        references InventoryItem (id);

    alter table InventoryBin 
        add index FKA3C3722BADFE8437 (inventoryLocation_id), 
        add constraint FKA3C3722BADFE8437 
        foreign key (inventoryLocation_id) 
        references InventoryLocation (id);

    alter table InventoryBin 
        add index FKA3C3722B63AB2FFD (inventoryBinType_id), 
        add constraint FKA3C3722B63AB2FFD 
        foreign key (inventoryBinType_id) 
        references InventoryBinType (id);

    alter table InventoryItem 
        add index FKD4AE2A6FD4B669BD (item_id), 
        add constraint FKD4AE2A6FD4B669BD 
        foreign key (item_id) 
        references Item (id);

    alter table InventoryLocation 
        add index FK4EED41118AF45977 (inventorySite_id), 
        add constraint FK4EED41118AF45977 
        foreign key (inventorySite_id) 
        references InventorySite (id);

    alter table InventoryTransaction 
        add index FK2A3D64028BE735F7 (inventoryItem_id), 
        add constraint FK2A3D64028BE735F7 
        foreign key (inventoryItem_id) 
        references InventoryItem (id);

    alter table InventoryTransaction 
        add index FK2A3D6402738FDF57 (inventoryReasonTypes_id), 
        add constraint FK2A3D6402738FDF57 
        foreign key (inventoryReasonTypes_id) 
        references InventoryReasons (id);

    alter table InventoryTransaction 
        add index FK2A3D6402441E8A5C (toBinType_id), 
        add constraint FK2A3D6402441E8A5C 
        foreign key (toBinType_id) 
        references InventoryBinType (id);

    alter table InventoryTransaction 
        add index FK2A3D6402289424CB (fromBinType_id), 
        add constraint FK2A3D6402289424CB 
        foreign key (fromBinType_id) 
        references InventoryBinType (id);

    alter table InventoryTransaction 
        add index FK2A3D64025C751D57 (inventoryTransactionTypes_id), 
        add constraint FK2A3D64025C751D57 
        foreign key (inventoryTransactionTypes_id) 
        references InventoryTransactionTypes (id);

    alter table IpAddress 
        add index FKD8D77CAD185A22D7 (account_id), 
        add constraint FKD8D77CAD185A22D7 
        foreign key (account_id) 
        references Account (id);

    alter table IpAddress 
        add index FKD8D77CAD45A534A6 (cred_id), 
        add constraint FKD8D77CAD45A534A6 
        foreign key (cred_id) 
        references Credential (id);

    alter table IpAddress_AUD 
        add index FK726B8BFE75D1432 (REV), 
        add constraint FK726B8BFE75D1432 
        foreign key (REV) 
        references REVINFO (REV);

    create index ITEM_ITEM_IX on Item (replacement_id);

    create index ITEM_DEALERSKU_IX on Item (dealerSku);

    create index ITEM_VENDORSKU_IX on Item (vendorSku);

    alter table Item 
        add index FK22EF3397CE9D17 (unitOfMeasure_id), 
        add constraint FK22EF3397CE9D17 
        foreign key (unitOfMeasure_id) 
        references UnitOfMeasure (id);

    alter table Item 
        add index FK22EF334E03425E (replacement_id), 
        add constraint FK22EF334E03425E 
        foreign key (replacement_id) 
        references Item (id);

    alter table Item 
        add index FK22EF33C66C90F7 (hierarchyNode_id), 
        add constraint FK22EF33C66C90F7 
        foreign key (hierarchyNode_id) 
        references HierarchyNode (id);

    alter table Item 
        add index FK22EF3379FB57D7 (brand_id), 
        add constraint FK22EF3379FB57D7 
        foreign key (brand_id) 
        references Brand (id);

    alter table Item 
        add index FK22EF33D72635FD (manufacturer_id), 
        add constraint FK22EF33D72635FD 
        foreign key (manufacturer_id) 
        references Manufacturer (id);

    alter table Item 
        add index FK22EF334B90121D (itemCategory_id), 
        add constraint FK22EF334B90121D 
        foreign key (itemCategory_id) 
        references ItemCategory (id);

    alter table Item 
        add index FK22EF337ADFF69F (vendorCatalog_id), 
        add constraint FK22EF337ADFF69F 
        foreign key (vendorCatalog_id) 
        references Catalog (id);

    alter table Item 
        add index FK22EF33E061875A (abilityOneSubstitute_id), 
        add constraint FK22EF33E061875A 
        foreign key (abilityOneSubstitute_id) 
        references Item (id);

    alter table ItemCategory 
        add index FK32432D51D77B2BE4 (parent_id), 
        add constraint FK32432D51D77B2BE4 
        foreign key (parent_id) 
        references ItemCategory (id);

    create index ID_ITEM_IX on ItemDimension (item_id);

    alter table ItemDimension 
        add index FK7789077397CE9D17 (unitOfMeasure_id), 
        add constraint FK7789077397CE9D17 
        foreign key (unitOfMeasure_id) 
        references UnitOfMeasure (id);

    alter table ItemDimension 
        add index FK7789077388D89C57 (dimensionType_id), 
        add constraint FK7789077388D89C57 
        foreign key (dimensionType_id) 
        references DimensionType (id);

    alter table ItemDimension 
        add index FK77890773D4B669BD (item_id), 
        add constraint FK77890773D4B669BD 
        foreign key (item_id) 
        references Item (id);

    create index II_ITEM_IX on ItemIdentifier (item_id);

    alter table ItemIdentifier 
        add index FK6E543CD4B669BD (item_id), 
        add constraint FK6E543CD4B669BD 
        foreign key (item_id) 
        references Item (id);

    alter table ItemIdentifier 
        add index FK6E543C9AF89DD (itemIdentifierType_id), 
        add constraint FK6E543C9AF89DD 
        foreign key (itemIdentifierType_id) 
        references ItemIdentifierType (id);

    create index ITEMIMAGEID_IX on ItemImage (item_id);

    alter table ItemImage 
        add index FKF69951E8D4B669BD (item_id), 
        add constraint FKF69951E8D4B669BD 
        foreign key (item_id) 
        references Item (id);

    alter table ItemImage 
        add index FKF69951E8EF79F2B7 (imageType_id), 
        add constraint FKF69951E8EF79F2B7 
        foreign key (imageType_id) 
        references ImageType (id);

    create index IP_ITEM_IX on ItemPackaging (item_id);

    alter table ItemPackaging 
        add index FK2436F190B8531B37 (packagingType_id), 
        add constraint FK2436F190B8531B37 
        foreign key (packagingType_id) 
        references PackagingType (id);

    alter table ItemPackaging 
        add index FK2436F190293DD25D (packagingDimension_id), 
        add constraint FK2436F190293DD25D 
        foreign key (packagingDimension_id) 
        references PackagingDimension (id);

    alter table ItemPackaging 
        add index FK2436F190D4B669BD (item_id), 
        add constraint FK2436F190D4B669BD 
        foreign key (item_id) 
        references Item (id);

    alter table ItemPropertyType_AUD 
        add index FKE6209A5375D1432 (REV), 
        add constraint FKE6209A5375D1432 
        foreign key (REV) 
        references REVINFO (REV);

    create index ITEM_SELLPT_ID_IX on ItemSellingPoint (item_id);

    alter table ItemSellingPoint 
        add index FK97A3D6F3D4B669BD (item_id), 
        add constraint FK97A3D6F3D4B669BD 
        foreign key (item_id) 
        references Item (id);

    create index ITEM_ID_IX on ItemXItemPropertyType (item_id);

    create index TYPE_ID_IX on ItemXItemPropertyType (type_id);

    alter table ItemXItemPropertyType 
        add index FKA8404B07D4B669BD (item_id), 
        add constraint FKA8404B07D4B669BD 
        foreign key (item_id) 
        references Item (id);

    alter table ItemXItemPropertyType 
        add index FKA8404B0778D89205 (type_id), 
        add constraint FKA8404B0778D89205 
        foreign key (type_id) 
        references ItemPropertyType (id);

    alter table Item_Matchbook 
        add index FK2D669002D4B669BD (Item_id), 
        add constraint FK2D669002D4B669BD 
        foreign key (Item_id) 
        references Item (id);

    alter table Item_Matchbook 
        add index FK2D669002AB387BF7 (matchbook_id), 
        add constraint FK2D669002AB387BF7 
        foreign key (matchbook_id) 
        references Matchbook (id);

    alter table Item_priceCategories 
        add index FKDE40B39D4B669BD (Item_id), 
        add constraint FKDE40B39D4B669BD 
        foreign key (Item_id) 
        references Item (id);

    alter table Item_smartSearchCustomerSkus 
        add index FK6C6C5659D4B669BD (Item_id), 
        add constraint FK6C6C5659D4B669BD 
        foreign key (Item_id) 
        references Item (id);

    alter table LimitApproval 
        add index FK7C45AFE90DF4B4F (AXCXU_ID), 
        add constraint FK7C45AFE90DF4B4F 
        foreign key (AXCXU_ID) 
        references AccountXCredentialXUser (id);

    alter table LimitApproval_AUD 
        add index FK882ED9CF75D1432 (REV), 
        add constraint FK882ED9CF75D1432 
        foreign key (REV) 
        references REVINFO (REV);

    create index LINEITEM_LINENUMBER_IDX on LineItem (lineNumber);

    create index LI_ITEM_IX on LineItem (item_id);

    create index LINEITEM_CUSTORDER_IDX on LineItem (order_id);

    alter table LineItem 
        add index FK4AAEE94797CE9D17 (unitOfMeasure_id), 
        add constraint FK4AAEE94797CE9D17 
        foreign key (unitOfMeasure_id) 
        references UnitOfMeasure (id);

    alter table LineItem 
        add index FK4AAEE947D50C94DD (vendor_id), 
        add constraint FK4AAEE947D50C94DD 
        foreign key (vendor_id) 
        references Vendor (id);

    alter table LineItem 
        add index FK4AAEE947D0A084BD (customerExpectedUoM_id), 
        add constraint FK4AAEE947D0A084BD 
        foreign key (customerExpectedUoM_id) 
        references UnitOfMeasure (id);

    alter table LineItem 
        add index FK4AAEE947D4B669BD (item_id), 
        add constraint FK4AAEE947D4B669BD 
        foreign key (item_id) 
        references Item (id);

    alter table LineItem 
        add index FK4AAEE947EA944B5 (customerSku_id), 
        add constraint FK4AAEE947EA944B5 
        foreign key (customerSku_id) 
        references Sku (id);

    alter table LineItem 
        add index FK4AAEE947B7BD2710 (category_id), 
        add constraint FK4AAEE947B7BD2710 
        foreign key (category_id) 
        references ItemCategory (id);

    alter table LineItem 
        add index FK4AAEE9475B7981A4 (status_id), 
        add constraint FK4AAEE9475B7981A4 
        foreign key (status_id) 
        references LineItemStatus (id);

    alter table LineItem 
        add index FK4AAEE9478E1EC39 (order_id), 
        add constraint FK4AAEE9478E1EC39 
        foreign key (order_id) 
        references CustomerOrder (id);

    alter table LineItem 
        add index FK4AAEE947E0A3A08C (expected_id), 
        add constraint FK4AAEE947E0A3A08C 
        foreign key (expected_id) 
        references LineItem (id);

    alter table LineItem 
        add index FK4AAEE9471BAE365E (paymentStatus_id), 
        add constraint FK4AAEE9471BAE365E 
        foreign key (paymentStatus_id) 
        references LineItemStatus (id);

    create index RETURNORDER_LIXR_IDX on LineItemXReturn (RETURNORDER_ID);

    create index LIXR_LINEITEM_IDX on LineItemXReturn (lineItem_id);

    alter table LineItemXReturn 
        add index FK77B3F0815A2BC1B6 (INVOICE_ID), 
        add constraint FK77B3F0815A2BC1B6 
        foreign key (INVOICE_ID) 
        references INVOICE (id);

    alter table LineItemXReturn 
        add index FK77B3F08196D53A97 (RETURNORDER_ID), 
        add constraint FK77B3F08196D53A97 
        foreign key (RETURNORDER_ID) 
        references ReturnOrder (id);

    alter table LineItemXReturn 
        add index FK77B3F081B7777FDD (lineItem_id), 
        add constraint FK77B3F081B7777FDD 
        foreign key (lineItem_id) 
        references LineItem (id);

    create index LIXS_LINEITEM_IDX on LineItemXShipment (lineItem_id);

    create index LIXS_SHIPMENT_IDX on LineItemXShipment (shipments_id);

    alter table LineItemXShipment 
        add index FK4DFF94ABB7777FDD (lineItem_id), 
        add constraint FK4DFF94ABB7777FDD 
        foreign key (lineItem_id) 
        references LineItem (id);

    alter table LineItemXShipment 
        add index FK4DFF94ABFDA949FE (shipments_id), 
        add constraint FK4DFF94ABFDA949FE 
        foreign key (shipments_id) 
        references Shipment (id);

    create index LIXSXTT_LINEITEMXSHIPMENT_IDX on LineItemXShipment_TaxType (lineItemXShipment_id);

    create index LIXSXTT_LINEITEMXRETURN_IDX on LineItemXShipment_TaxType (lineItemXReturn_id);

    alter table LineItemXShipment_TaxType 
        add index FKD45EDD1A80FC937 (lineItemXReturn_id), 
        add constraint FKD45EDD1A80FC937 
        foreign key (lineItemXReturn_id) 
        references LineItemXReturn (id);

    alter table LineItemXShipment_TaxType 
        add index FKD45EDD1CB300DB7 (lineItemXShipment_id), 
        add constraint FKD45EDD1CB300DB7 
        foreign key (lineItemXShipment_id) 
        references LineItemXShipment (id);

    alter table LineItemXShipment_TaxType 
        add index FKD45EDD12B5202 (type_id), 
        add constraint FKD45EDD12B5202 
        foreign key (type_id) 
        references TaxType (id);

    create index LISTHASHTOUID_HASH on ListHashToUid (hash);

    alter table ListHashToUid_AUD 
        add index FK4770819A75D1432 (REV), 
        add constraint FK4770819A75D1432 
        foreign key (REV) 
        references REVINFO (REV);

    alter table Manufacturer_AUD 
        add index FKD246454275D1432 (REV), 
        add constraint FKD246454275D1432 
        foreign key (REV) 
        references REVINFO (REV);

    alter table Matchbook 
        add index FK9C11CA0ED72635FD (manufacturer_id), 
        add constraint FK9C11CA0ED72635FD 
        foreign key (manufacturer_id) 
        references Manufacturer (id);

    alter table Matchbook_AUD 
        add index FK4B0E40DF75D1432 (REV), 
        add constraint FK4B0E40DF75D1432 
        foreign key (REV) 
        references REVINFO (REV);

    alter table MessageMetadata 
        add index FKD964A0363E63769E (objectsWrapper_id), 
        add constraint FKD964A0363E63769E 
        foreign key (objectsWrapper_id) 
        references MessageObjectsWrapper (id);

    alter table NotificationLimit 
        add index FK8C01ED70AFD1BCFD (credential_id), 
        add constraint FK8C01ED70AFD1BCFD 
        foreign key (credential_id) 
        references Credential (id);

    alter table NotificationProperties 
        add index FK2EC8B49E8E1EC39 (order_id), 
        add constraint FK2EC8B49E8E1EC39 
        foreign key (order_id) 
        references CustomerOrder (id);

    alter table NotificationProperties 
        add index FK2EC8B49E45A534A6 (cred_id), 
        add constraint FK2EC8B49E45A534A6 
        foreign key (cred_id) 
        references Credential (id);

    alter table NotificationProperties 
        add index FK2EC8B49EF363A403 (acc_id), 
        add constraint FK2EC8B49EF363A403 
        foreign key (acc_id) 
        references Account (id);

    create index CCTRANLOG_RETURN_IDX on ORDER_LOG (creditcard_returnorder_id);

    create index CCTRANLOG_SUCCESS_IDX on ORDER_LOG (transactionSuccess);

    create index ORDERLOG_UPDATETIME_IDX on ORDER_LOG (updateTimestamp);

    create index ORDERLOG_CUSTORDER_IDX on ORDER_LOG (customerOrder_id);

    alter table ORDER_LOG 
        add index FK8ED49E93BBACDEDD (shipment_id), 
        add constraint FK8ED49E93BBACDEDD 
        foreign key (shipment_id) 
        references Shipment (id);

    alter table ORDER_LOG 
        add index FK8ED49E93753382D7 (invoice_id), 
        add constraint FK8ED49E93753382D7 
        foreign key (invoice_id) 
        references INVOICE (id);

    alter table ORDER_LOG 
        add index FK8ED49E93454B440D (creditcard_returnorder_id), 
        add constraint FK8ED49E93454B440D 
        foreign key (creditcard_returnorder_id) 
        references ReturnOrder (id);

    alter table ORDER_LOG 
        add index FK8ED49E9396D53A97 (returnOrder_id), 
        add constraint FK8ED49E9396D53A97 
        foreign key (returnOrder_id) 
        references ReturnOrder (id);

    alter table ORDER_LOG 
        add index FK8ED49E93B7777FDD (lineItem_id), 
        add constraint FK8ED49E93B7777FDD 
        foreign key (lineItem_id) 
        references LineItem (id);

    alter table ORDER_LOG 
        add index FK8ED49E9326CAE17 (customerOrder_id), 
        add constraint FK8ED49E9326CAE17 
        foreign key (customerOrder_id) 
        references CustomerOrder (id);

    alter table ORDER_LOG 
        add index FK8ED49E93A910D157 (messageMetadata_id), 
        add constraint FK8ED49E93A910D157 
        foreign key (messageMetadata_id) 
        references MessageMetadata (id);

    alter table ORDER_LOG 
        add index FK8ED49E93B27BA87D (lineItemStatus_id), 
        add constraint FK8ED49E93B27BA87D 
        foreign key (lineItemStatus_id) 
        references LineItemStatus (id);

    alter table ORDER_LOG 
        add index FK8ED49E9384C13B97 (orderStatus_id), 
        add constraint FK8ED49E9384C13B97 
        foreign key (orderStatus_id) 
        references OrderStatus (id);

    alter table ORDER_LOG_LineItemXShipment 
        add index FK8321BDFF255C549A (lineItemXShipments_id), 
        add constraint FK8321BDFF255C549A 
        foreign key (lineItemXShipments_id) 
        references LineItemXShipment (id);

    alter table ORDER_LOG_LineItemXShipment 
        add index FK8321BDFF3EA8F9 (ORDER_LOG_id), 
        add constraint FK8321BDFF3EA8F9 
        foreign key (ORDER_LOG_id) 
        references ORDER_LOG (id);

    alter table OrderAttachment 
        add index FKD35F2AF1797AADDD (servicelog_id), 
        add constraint FKD35F2AF1797AADDD 
        foreign key (servicelog_id) 
        references SERVICE_LOG (id);

    alter table OrderAttachment 
        add index FKD35F2AF14E9ECF4C (user_id), 
        add constraint FKD35F2AF14E9ECF4C 
        foreign key (user_id) 
        references SystemUser (id);

    alter table OrderAttachment 
        add index FKD35F2AF18E1EC39 (order_id), 
        add constraint FKD35F2AF18E1EC39 
        foreign key (order_id) 
        references CustomerOrder (id);

    create index orderbrmsrecord_entryid on OrderBRMSRecord (entryId);

    create index ORDERLOG_CUSTORDER_IDX on OrderProcessLookup (customerOrder_id);

    alter table OrderProcessLookup 
        add index FK2C2B183B26CAE17 (customerOrder_id), 
        add constraint FK2C2B183B26CAE17 
        foreign key (customerOrder_id) 
        references CustomerOrder (id);

    alter table OrderStatus 
        add index FKC492B8C0BAA7D4D9 (overrideStatus_id), 
        add constraint FKC492B8C0BAA7D4D9 
        foreign key (overrideStatus_id) 
        references OrderStatus (id);

    alter table PackagingDimension 
        add index FKF5AEB54397CE9D17 (unitOfMeasure_id), 
        add constraint FKF5AEB54397CE9D17 
        foreign key (unitOfMeasure_id) 
        references UnitOfMeasure (id);

    alter table PackagingDimension 
        add index FKF5AEB54388D89C57 (dimensionType_id), 
        add constraint FKF5AEB54388D89C57 
        foreign key (dimensionType_id) 
        references DimensionType (id);

    create index PARTNERIDXDELVLOC_LOCATION on PartnerIdXDeliveryLocation (deliveryLocation);

    create index PARTNERIDXDELVLOC_PARTNERID on PartnerIdXDeliveryLocation (partnerId);

    alter table PartnerIdXDeliveryLocation_AUD 
        add index FKB9660D8F75D1432 (REV), 
        add constraint FKB9660D8F75D1432 
        foreign key (REV) 
        references REVINFO (REV);

    alter table PasswordResetEntry 
        add index FKC0DA017E4E9ECF4C (user_id), 
        add constraint FKC0DA017E4E9ECF4C 
        foreign key (user_id) 
        references SystemUser (id);

    alter table PasswordResetEntry_AUD 
        add index FKD90D404F75D1432 (REV), 
        add constraint FKD90D404F75D1432 
        foreign key (REV) 
        references REVINFO (REV);

    alter table PaymentInformation 
        add index FKCC5E4766C3EB5397 (paymentType_id), 
        add constraint FKCC5E4766C3EB5397 
        foreign key (paymentType_id) 
        references PaymentType (id);

    alter table PaymentInformation 
        add index FKCC5E476613E27D12 (contact_id), 
        add constraint FKCC5E476613E27D12 
        foreign key (contact_id) 
        references Person (id);

    alter table PaymentInformation 
        add index FKCC5E47663C027E32 (billingAddress_id), 
        add constraint FKCC5E47663C027E32 
        foreign key (billingAddress_id) 
        references Address (id);

    alter table PaymentInformation 
        add index FKCC5E4766171AC637 (invoiceSendMethod_id), 
        add constraint FKCC5E4766171AC637 
        foreign key (invoiceSendMethod_id) 
        references InvoiceSendMethod (id);

    alter table PaymentInformation 
        add index FKCC5E4766A6E063E3 (card_id), 
        add constraint FKCC5E4766A6E063E3 
        foreign key (card_id) 
        references CardInformation (id);

    alter table PaymentInformation_AUD 
        add index FK5BAC523775D1432 (REV), 
        add constraint FK5BAC523775D1432 
        foreign key (REV) 
        references REVINFO (REV);

    alter table PaymentType_AUD 
        add index FK6C86781175D1432 (REV), 
        add constraint FK6C86781175D1432 
        foreign key (REV) 
        references REVINFO (REV);

    create index PENDINGDELETION_ITEM_IDX on PendingSmartSearchDeletion (item_id);

    create index PENDINGDELETION_CATALOG_IDX on PendingSmartSearchDeletion (catalog_id);

    alter table PendingSmartSearchDeletion 
        add index FK587840C8D4B669BD (item_id), 
        add constraint FK587840C8D4B669BD 
        foreign key (item_id) 
        references Item (id);

    alter table PendingSmartSearchDeletion 
        add index FK587840C831B94B57 (catalog_id), 
        add constraint FK587840C831B94B57 
        foreign key (catalog_id) 
        references Catalog (id);

    create index PWCOMMAND_EXECUTED_IDX on PendingWorkflowCommand (executed);

    create index PWCOMMAND_EXPIRES_IDX on PendingWorkflowCommand (expires);

    alter table PeriodType_AUD 
        add index FKE00BE50C75D1432 (REV), 
        add constraint FKE00BE50C75D1432 
        foreign key (REV) 
        references REVINFO (REV);

    alter table PermissionXUser 
        add index FK637BAFB4BCDF29FD (permission_id), 
        add constraint FK637BAFB4BCDF29FD 
        foreign key (permission_id) 
        references Permission (id);

    alter table PermissionXUser 
        add index FK637BAFB44E9ECF4C (user_id), 
        add constraint FK637BAFB44E9ECF4C 
        foreign key (user_id) 
        references SystemUser (id);

    alter table PermissionXUser_AUD 
        add index FKDA20E38575D1432 (REV), 
        add constraint FKDA20E38575D1432 
        foreign key (REV) 
        references REVINFO (REV);

    alter table Permission_AUD 
        add index FK6E93404075D1432 (REV), 
        add constraint FK6E93404075D1432 
        foreign key (REV) 
        references REVINFO (REV);

    alter table Person_AUD 
        add index FK9F49F2C675D1432 (REV), 
        add constraint FK9F49F2C675D1432 
        foreign key (REV) 
        references REVINFO (REV);

    alter table PhoneNumber 
        add index FK1C4E6237D50C94DD (vendor_id), 
        add constraint FK1C4E6237D50C94DD 
        foreign key (vendor_id) 
        references Vendor (id);

    alter table PhoneNumber 
        add index FK1C4E62373E66BDBD (person_id), 
        add constraint FK1C4E62373E66BDBD 
        foreign key (person_id) 
        references Person (id);

    alter table PhoneNumber 
        add index FK1C4E6237185A22D7 (account_id), 
        add constraint FK1C4E6237185A22D7 
        foreign key (account_id) 
        references Account (id);

    alter table PhoneNumber 
        add index FK1C4E6237D68B89AE (type_id), 
        add constraint FK1C4E6237D68B89AE 
        foreign key (type_id) 
        references PhoneNumberType (id);

    alter table PhoneNumberType_AUD 
        add index FKD70F2F6275D1432 (REV), 
        add constraint FKD70F2F6275D1432 
        foreign key (REV) 
        references REVINFO (REV);

    alter table PhoneNumber_AUD 
        add index FKAD809C8875D1432 (REV), 
        add constraint FKAD809C8875D1432 
        foreign key (REV) 
        references REVINFO (REV);

    alter table Pick 
        add index FK25F441B7777FDD (lineItem_id), 
        add constraint FK25F441B7777FDD 
        foreign key (lineItem_id) 
        references LineItem (id);

    create index PO_NUM_IDX on PoNumber (value);

    alter table PoNumber 
        add index FKF1EF7E484D174BA5 (type_id), 
        add constraint FKF1EF7E484D174BA5 
        foreign key (type_id) 
        references PoNumberType (id);

    alter table PoNumberType_AUD 
        add index FKD05FAA7375D1432 (REV), 
        add constraint FKD05FAA7375D1432 
        foreign key (REV) 
        references REVINFO (REV);

    alter table PoNumber_CustomerOrder 
        add index FK27F6343924BC7FA (poNumbers_id), 
        add constraint FK27F6343924BC7FA 
        foreign key (poNumbers_id) 
        references PoNumber (id);

    alter table PoNumber_CustomerOrder 
        add index FK27F63439A806D542 (orders_id), 
        add constraint FK27F63439A806D542 
        foreign key (orders_id) 
        references CustomerOrder (id);

    alter table PricingType_AUD 
        add index FK33925D175D1432 (REV), 
        add constraint FK33925D175D1432 
        foreign key (REV) 
        references REVINFO (REV);

    alter table ProcessConfiguration 
        add index FKDAF5E6E7AFD1BCFD (credential_id), 
        add constraint FKDAF5E6E7AFD1BCFD 
        foreign key (credential_id) 
        references Credential (id);

    alter table ProcessConfiguration 
        add index FKDAF5E6E7185A22D7 (account_id), 
        add constraint FKDAF5E6E7185A22D7 
        foreign key (account_id) 
        references Account (id);

    alter table ProcessConfiguration 
        add index FKDAF5E6E7951FA397 (process_id), 
        add constraint FKDAF5E6E7951FA397 
        foreign key (process_id) 
        references Process (id);

    alter table ProcessVariable 
        add index FKDD2D2DEB4F9C731D (processConfiguration_id), 
        add constraint FKDD2D2DEB4F9C731D 
        foreign key (processConfiguration_id) 
        references ProcessConfiguration (id);

    alter table PunchoutSession 
        add index FKDC86EEF626CAE17 (customerOrder_id), 
        add constraint FKDC86EEF626CAE17 
        foreign key (customerOrder_id) 
        references CustomerOrder (id);

    alter table PunchoutSession 
        add index FKDC86EEF6A9E5B5D8 (accntCredUser_id), 
        add constraint FKDC86EEF6A9E5B5D8 
        foreign key (accntCredUser_id) 
        references AccountXCredentialXUser (id);

    create index RETURN_CREATEDATE_IDX on ReturnOrder (createdDate);

    create index RETURNORDER_CUSTORDER_IDX on ReturnOrder (order_id);

    create index RETURN_RECONCILEDATE_IDX on ReturnOrder (reconciledDate);

    create index RETURN_CLOSEDATE_IDX on ReturnOrder (closeDate);

    alter table ReturnOrder 
        add index FK9F0BFDE8E1EC39 (order_id), 
        add constraint FK9F0BFDE8E1EC39 
        foreign key (order_id) 
        references CustomerOrder (id);

    alter table ReturnOrder 
        add index FK9F0BFDE6A6D81FD (restockingFee_id), 
        add constraint FK9F0BFDE6A6D81FD 
        foreign key (restockingFee_id) 
        references LineItem (id);

    alter table RoleXUser 
        add index FK8B08ED6DA8A94ADD (role_id), 
        add constraint FK8B08ED6DA8A94ADD 
        foreign key (role_id) 
        references Role (id);

    alter table RoleXUser 
        add index FK8B08ED6D4E9ECF4C (user_id), 
        add constraint FK8B08ED6D4E9ECF4C 
        foreign key (user_id) 
        references SystemUser (id);

    alter table RoleXUser_AUD 
        add index FKD8F69CBE75D1432 (REV), 
        add constraint FKD8F69CBE75D1432 
        foreign key (REV) 
        references REVINFO (REV);

    alter table Role_AUD 
        add index FKF3FAE76775D1432 (REV), 
        add constraint FKF3FAE76775D1432 
        foreign key (REV) 
        references REVINFO (REV);

    alter table Role_Permission 
        add index FKF8A5693846C1C336 (roles_id), 
        add constraint FKF8A5693846C1C336 
        foreign key (roles_id) 
        references Role (id);

    alter table Role_Permission 
        add index FKF8A56938BC8D8328 (permissions_id), 
        add constraint FKF8A56938BC8D8328 
        foreign key (permissions_id) 
        references Permission (id);

    alter table Role_Permission_AUD 
        add index FKE90A3B0975D1432 (REV), 
        add constraint FKE90A3B0975D1432 
        foreign key (REV) 
        references REVINFO (REV);

    alter table SERVICE_LOG 
        add index FKD76567A29E27F47 (apdCsrRep_id), 
        add constraint FKD76567A29E27F47 
        foreign key (apdCsrRep_id) 
        references SystemUser (id);

    alter table SERVICE_LOG 
        add index FKD76567AA9054CB7 (contactMethod_id), 
        add constraint FKD76567AA9054CB7 
        foreign key (contactMethod_id) 
        references ContactMethod (id);

    alter table SERVICE_LOG 
        add index FKD76567A26CAE17 (customerOrder_id), 
        add constraint FKD76567A26CAE17 
        foreign key (customerOrder_id) 
        references CustomerOrder (id);

    alter table SERVICE_LOG 
        add index FKD76567A185A22D7 (account_id), 
        add constraint FKD76567A185A22D7 
        foreign key (account_id) 
        references Account (id);

    alter table SERVICE_LOG 
        add index FKD76567A4E2C1B7 (serviceReason_id), 
        add constraint FKD76567A4E2C1B7 
        foreign key (serviceReason_id) 
        references ServiceReason (id);

    alter table SerialNumber 
        add index FKEC9D27D8BE735F7 (inventoryItem_id), 
        add constraint FKEC9D27D8BE735F7 
        foreign key (inventoryItem_id) 
        references InventoryItem (id);

    alter table ShipManifest 
        add index FK1734C4AB26CAE17 (customerOrder_id), 
        add constraint FK1734C4AB26CAE17 
        foreign key (customerOrder_id) 
        references CustomerOrder (id);

    alter table ShipManifest_Pick 
        add index FKE8531B559EBD9BDD (ShipManifest_id), 
        add constraint FKE8531B559EBD9BDD 
        foreign key (ShipManifest_id) 
        references ShipManifest (id);

    alter table ShipManifest_Pick 
        add index FKE8531B552A91F50C (picks_id), 
        add constraint FKE8531B552A91F50C 
        foreign key (picks_id) 
        references Pick (id);

    create index SHIPMENT_CUSTORDER_IDX on Shipment (order_id);

    create index SHIPMENT_SHIPTIME_IDX on Shipment (shipTime);

    alter table Shipment 
        add index FKE513D5BA9744A7F3 (parentShipment_id), 
        add constraint FKE513D5BA9744A7F3 
        foreign key (parentShipment_id) 
        references Shipment (id);

    alter table Shipment 
        add index FKE513D5BA99EEF977 (address_id), 
        add constraint FKE513D5BA99EEF977 
        foreign key (address_id) 
        references Address (id);

    alter table Shipment 
        add index FKE513D5BA5BE726AC (apdInvoice_id), 
        add constraint FKE513D5BA5BE726AC 
        foreign key (apdInvoice_id) 
        references INVOICE (id);

    alter table Shipment 
        add index FKE513D5BA8E1EC39 (order_id), 
        add constraint FKE513D5BA8E1EC39 
        foreign key (order_id) 
        references CustomerOrder (id);

    alter table Shipment 
        add index FKE513D5BA495D8297 (shippingPartner_id), 
        add constraint FKE513D5BA495D8297 
        foreign key (shippingPartner_id) 
        references ShippingPartner (id);

    alter table Shipment 
        add index FKE513D5BAD81419CE (customerServiceAddress_id), 
        add constraint FKE513D5BAD81419CE 
        foreign key (customerServiceAddress_id) 
        references Address (id);

    alter table ShippingPartner_AUD 
        add index FK75423FEB75D1432 (REV), 
        add constraint FK75423FEB75D1432 
        foreign key (REV) 
        references REVINFO (REV);

    create index SKU_ITEM_ID_IX on Sku (item_id);

    create index SKU_TYPE_ID_IX on Sku (type_id);

    alter table Sku 
        add index FK144FDD4B669BD (item_id), 
        add constraint FK144FDD4B669BD 
        foreign key (item_id) 
        references Item (id);

    alter table Sku 
        add index FK144FDDC2B4674 (type_id), 
        add constraint FK144FDDC2B4674 
        foreign key (type_id) 
        references SkuType (id);

    alter table SkuType_AUD 
        add index FKFFADEC2875D1432 (REV), 
        add constraint FKFFADEC2875D1432 
        foreign key (REV) 
        references REVINFO (REV);

    alter table SkuType_HierarchyNode 
        add index FKDB1B914FBA0B4EF2 (hierarchyNodes_id), 
        add constraint FKDB1B914FBA0B4EF2 
        foreign key (hierarchyNodes_id) 
        references HierarchyNode (id);

    alter table SkuType_HierarchyNode 
        add index FKDB1B914FA777D0F2 (skuTypes_id), 
        add constraint FKDB1B914FA777D0F2 
        foreign key (skuTypes_id) 
        references SkuType (id);

    create index sir_summary_idx on SyncItemResult (summary_id);

    create index sir_corrId on SyncItemResult (correlationId);

    alter table SyncItemResult 
        add index FKC520D22BBE7814EC (summary_id), 
        add constraint FKC520D22BBE7814EC 
        foreign key (summary_id) 
        references SyncItemResultSummary (id);

    create index sirs_corrId on SyncItemResultSummary (correlationId);

    alter table SystemComment 
        add index FKE1344110BBACDEDD (shipment_id), 
        add constraint FKE1344110BBACDEDD 
        foreign key (shipment_id) 
        references Shipment (id);

    alter table SystemComment 
        add index FKE13441104E9ECF4C (user_id), 
        add constraint FKE13441104E9ECF4C 
        foreign key (user_id) 
        references SystemUser (id);

    alter table SystemComment 
        add index FKE13441108E1EC39 (order_id), 
        add constraint FKE13441108E1EC39 
        foreign key (order_id) 
        references CustomerOrder (id);

    create index LOGIN_INDEX on SystemUser (login);

    alter table SystemUser 
        add index FK9D23FEBA3E66BDBD (person_id), 
        add constraint FK9D23FEBA3E66BDBD 
        foreign key (person_id) 
        references Person (id);

    alter table SystemUser 
        add index FK9D23FEBA7ED1037D (paymentInformation_id), 
        add constraint FK9D23FEBA7ED1037D 
        foreign key (paymentInformation_id) 
        references PaymentInformation (id);

    alter table SystemUser_AUD 
        add index FK595E3F8B75D1432 (REV), 
        add constraint FK595E3F8B75D1432 
        foreign key (REV) 
        references REVINFO (REV);

    alter table SystemUser_Account 
        add index FK139A6D88478FA7EF (users_id), 
        add constraint FK139A6D88478FA7EF 
        foreign key (users_id) 
        references SystemUser (id);

    alter table SystemUser_Account 
        add index FK139A6D887BEA6A7E (accounts_id), 
        add constraint FK139A6D887BEA6A7E 
        foreign key (accounts_id) 
        references Account (id);

    alter table SystemUser_Account_AUD 
        add index FKBE44975975D1432 (REV), 
        add constraint FKBE44975975D1432 
        foreign key (REV) 
        references REVINFO (REV);

    alter table SystemUser_Bulletin 
        add index FK8EEEC1AC367C0B98 (bulletins_id), 
        add constraint FK8EEEC1AC367C0B98 
        foreign key (bulletins_id) 
        references Bulletin (id);

    alter table SystemUser_Bulletin 
        add index FK8EEEC1AC64555A3D (SystemUser_id), 
        add constraint FK8EEEC1AC64555A3D 
        foreign key (SystemUser_id) 
        references SystemUser (id);

    alter table SystemUser_Bulletin_AUD 
        add index FK114A397D75D1432 (REV), 
        add constraint FK114A397D75D1432 
        foreign key (REV) 
        references REVINFO (REV);

    alter table SystemUser_RoleXUser_AUD 
        add index FKAFF1FF975D1432 (REV), 
        add constraint FKAFF1FF975D1432 
        foreign key (REV) 
        references REVINFO (REV);

    create index TAXLOG_TIME_IDX on TaxRateLog (timestamp);

    create index TLHXC_CAT_IX on TopLevelHierarchyXCatalog (catalog_id);

    alter table TopLevelHierarchyXCatalog 
        add index FK2B17E4731B94B57 (catalog_id), 
        add constraint FK2B17E4731B94B57 
        foreign key (catalog_id) 
        references Catalog (id);

    create index UOM_NAME_IDX on UnitOfMeasure (name);

    alter table UnitOfMeasure_AUD 
        add index FK8B1E7E9475D1432 (REV), 
        add constraint FK8B1E7E9475D1432 
        foreign key (REV) 
        references REVINFO (REV);

    alter table UserBRMSRecord 
        add index FKEA537C12D8D414D7 (userRequest_id), 
        add constraint FKEA537C12D8D414D7 
        foreign key (userRequest_id) 
        references UserRequest (id);

    create index UR_TOKEN_IDX on UserRequest (token);

    alter table Vendor 
        add index FK9883916813E27D12 (contact_id), 
        add constraint FK9883916813E27D12 
        foreign key (contact_id) 
        references Person (id);

    alter table VendorPo 
        add index FK85E4E1878BE735F7 (inventoryItem_id), 
        add constraint FK85E4E1878BE735F7 
        foreign key (inventoryItem_id) 
        references InventoryItem (id);

    alter table VendorPo 
        add index FK85E4E187B7777FDD (lineitem_id), 
        add constraint FK85E4E187B7777FDD 
        foreign key (lineitem_id) 
        references LineItem (id);

    alter table VendorPropertyType_AUD 
        add index FKF028328875D1432 (REV), 
        add constraint FKF028328875D1432 
        foreign key (REV) 
        references REVINFO (REV);

    alter table VendorXVendorPropertyType 
        add index FK6F8A7827D50C94DD (vendor_id), 
        add constraint FK6F8A7827D50C94DD 
        foreign key (vendor_id) 
        references Vendor (id);

    alter table VendorXVendorPropertyType 
        add index FK6F8A7827FF9A87FA (type_id), 
        add constraint FK6F8A7827FF9A87FA 
        foreign key (type_id) 
        references VendorPropertyType (id);

    alter table VendorXVendorPropertyType_AUD 
        add index FK37E03A7875D1432 (REV), 
        add constraint FK37E03A7875D1432 
        foreign key (REV) 
        references REVINFO (REV);

    alter table Vendor_AUD 
        add index FKA038CB3975D1432 (REV), 
        add constraint FKA038CB3975D1432 
        foreign key (REV) 
        references REVINFO (REV);

    alter table Vendor_Address_AUD 
        add index FKF42F6B4E75D1432 (REV), 
        add constraint FKF42F6B4E75D1432 
        foreign key (REV) 
        references REVINFO (REV);

    alter table Vendor_CommunicationMethod 
        add index FK884AF6E0F3928A32 (communicationMethods_id), 
        add constraint FK884AF6E0F3928A32 
        foreign key (communicationMethods_id) 
        references CommunicationMethod (id);

    alter table Vendor_CommunicationMethod 
        add index FK884AF6E03F37A71A (vendors_id), 
        add constraint FK884AF6E03F37A71A 
        foreign key (vendors_id) 
        references Vendor (id);

    alter table Vendor_CommunicationMethod_AUD 
        add index FKEA60B4B175D1432 (REV), 
        add constraint FKEA60B4B175D1432 
        foreign key (REV) 
        references REVINFO (REV);

    alter table Vendor_PhoneNumber_AUD 
        add index FK90E5B3B175D1432 (REV), 
        add constraint FK90E5B3B175D1432 
        foreign key (REV) 
        references REVINFO (REV);

    create index COUNTY_FIPS_IDX on ZipPlusFour (countyFips);

    create index ZIP_IDX on ZipPlusFour (zip);

    alter table acxacpt_AUD 
        add index FKFA2B070D75D1432 (REV), 
        add constraint FKFA2B070D75D1432 
        foreign key (REV) 
        references REVINFO (REV);

    alter table axapt_AUD 
        add index FKB065255F75D1432 (REV), 
        add constraint FKB065255F75D1432 
        foreign key (REV) 
        references REVINFO (REV);

    alter table axcxu_aud 
        add index FK27E6F47A75D1432 (REV), 
        add constraint FK27E6F47A75D1432 
        foreign key (REV) 
        references REVINFO (REV);

    alter table cctranlog_faildetails 
        add index FK362E01FEEA1EA233 (cctranlog_id), 
        add constraint FK362E01FEEA1EA233 
        foreign key (cctranlog_id) 
        references ORDER_LOG (id);

    alter table cctranlog_vendorList 
        add index FK65DD662CEA1EA233 (cctranlog_id), 
        add constraint FK65DD662CEA1EA233 
        foreign key (cctranlog_id) 
        references ORDER_LOG (id);

    alter table comp_favlist_catxitem 
        add index FK3BD333EBE4345037 (favoriteslist_id), 
        add constraint FK3BD333EBE4345037 
        foreign key (favoriteslist_id) 
        references FavoritesList (id);

    alter table comp_favlist_catxitem 
        add index FK3BD333EB34FADEEF (items_id), 
        add constraint FK3BD333EB34FADEEF 
        foreign key (items_id) 
        references CatalogXItem (id);

    alter table customerOutFromCred 
        add index FKFB1F76C8AFD1BCFD (Credential_id), 
        add constraint FKFB1F76C8AFD1BCFD 
        foreign key (Credential_id) 
        references Credential (id);

    alter table customerOutFromCred 
        add index FKFB1F76C8440D5C8D (customerOutoingFromCred_id), 
        add constraint FKFB1F76C8440D5C8D 
        foreign key (customerOutoingFromCred_id) 
        references CxmlCredential (id);

    alter table customerOutFromCred_AUD 
        add index FKDDA3009975D1432 (REV), 
        add constraint FKDDA3009975D1432 
        foreign key (REV) 
        references REVINFO (REV);

    alter table customerOutSenderCred 
        add index FK875A4FD3F111D462 (customerOutoingSenderCred_id), 
        add constraint FK875A4FD3F111D462 
        foreign key (customerOutoingSenderCred_id) 
        references CxmlCredential (id);

    alter table customerOutSenderCred 
        add index FK875A4FD3AFD1BCFD (Credential_id), 
        add constraint FK875A4FD3AFD1BCFD 
        foreign key (Credential_id) 
        references Credential (id);

    alter table customerOutSenderCred_AUD 
        add index FKAF645C2475D1432 (REV), 
        add constraint FKAF645C2475D1432 
        foreign key (REV) 
        references REVINFO (REV);

    alter table customerOutToCred 
        add index FKBB8919AFD1BCFD (Credential_id), 
        add constraint FKBB8919AFD1BCFD 
        foreign key (Credential_id) 
        references Credential (id);

    alter table customerOutToCred 
        add index FKBB8919CA6ABBDC (customerOutoingToCred_id), 
        add constraint FKBB8919CA6ABBDC 
        foreign key (customerOutoingToCred_id) 
        references CxmlCredential (id);

    alter table customerOutToCred_AUD 
        add index FKB75B826A75D1432 (REV), 
        add constraint FKB75B826A75D1432 
        foreign key (REV) 
        references REVINFO (REV);

    alter table cxcpt_AUD 
        add index FK42BA79E375D1432 (REV), 
        add constraint FK42BA79E375D1432 
        foreign key (REV) 
        references REVINFO (REV);

    alter table cxcxpt_AUD 
        add index FKB62881FF75D1432 (REV), 
        add constraint FKB62881FF75D1432 
        foreign key (REV) 
        references REVINFO (REV);

    alter table favoriteslist_catalogxitem 
        add index FK229D1F9CE4345037 (favoriteslist_id), 
        add constraint FK229D1F9CE4345037 
        foreign key (favoriteslist_id) 
        references FavoritesList (id);

    alter table favoriteslist_catalogxitem 
        add index FK229D1F9C34FADEEF (items_id), 
        add constraint FK229D1F9C34FADEEF 
        foreign key (items_id) 
        references CatalogXItem (id);

    alter table from_credential_table 
        add index FK5AAAFDDBD0B0DA57 (CxmlConfiguration_id), 
        add constraint FK5AAAFDDBD0B0DA57 
        foreign key (CxmlConfiguration_id) 
        references CxmlConfiguration (id);

    alter table from_credential_table 
        add index FK5AAAFDDBE0E054D6 (fromCredentials_id), 
        add constraint FK5AAAFDDBE0E054D6 
        foreign key (fromCredentials_id) 
        references CxmlCredential (id);

    alter table from_credential_table_AUD 
        add index FKEF1EC62C75D1432 (REV), 
        add constraint FKEF1EC62C75D1432 
        foreign key (REV) 
        references REVINFO (REV);

    alter table liXShipment_vendorComments 
        add index FKAC720FA6A518092D (liXShipment_id), 
        add constraint FKAC720FA6A518092D 
        foreign key (liXShipment_id) 
        references LineItemXShipment (id);

    alter table lineItem_vendorComments 
        add index FKB4713574B7777FDD (lineItem_id), 
        add constraint FKB4713574B7777FDD 
        foreign key (lineItem_id) 
        references LineItem (id);

    alter table outFromCredentialTable 
        add index FK524C227FE8A80542 (outgoingFromCredentials_id), 
        add constraint FK524C227FE8A80542 
        foreign key (outgoingFromCredentials_id) 
        references CxmlCredential (id);

    alter table outFromCredentialTable 
        add index FK524C227FAFD1BCFD (Credential_id), 
        add constraint FK524C227FAFD1BCFD 
        foreign key (Credential_id) 
        references Credential (id);

    alter table outFromCredentialTable_AUD 
        add index FK3294F8D075D1432 (REV), 
        add constraint FK3294F8D075D1432 
        foreign key (REV) 
        references REVINFO (REV);

    alter table outSenderCredentialTable 
        add index FK85502C14F444CD8D (outgoingSenderCredentials_id), 
        add constraint FK85502C14F444CD8D 
        foreign key (outgoingSenderCredentials_id) 
        references CxmlCredential (id);

    alter table outSenderCredentialTable 
        add index FK85502C14AFD1BCFD (Credential_id), 
        add constraint FK85502C14AFD1BCFD 
        foreign key (Credential_id) 
        references Credential (id);

    alter table outSenderCredentialTable_AUD 
        add index FKCAA02FE575D1432 (REV), 
        add constraint FKCAA02FE575D1432 
        foreign key (REV) 
        references REVINFO (REV);

    alter table outToCredentialTable 
        add index FK42E3DC0EA73C6553 (outgoingToCredentials_id), 
        add constraint FK42E3DC0EA73C6553 
        foreign key (outgoingToCredentials_id) 
        references CxmlCredential (id);

    alter table outToCredentialTable 
        add index FK42E3DC0EAFD1BCFD (Credential_id), 
        add constraint FK42E3DC0EAFD1BCFD 
        foreign key (Credential_id) 
        references Credential (id);

    alter table outToCredentialTable_AUD 
        add index FK368752DF75D1432 (REV), 
        add constraint FK368752DF75D1432 
        foreign key (REV) 
        references REVINFO (REV);

    alter table sender_credential_table 
        add index FK49D502B0BFA78821 (senderCredentials_id), 
        add constraint FK49D502B0BFA78821 
        foreign key (senderCredentials_id) 
        references CxmlCredential (id);

    alter table sender_credential_table 
        add index FK49D502B0D0B0DA57 (CxmlConfiguration_id), 
        add constraint FK49D502B0D0B0DA57 
        foreign key (CxmlConfiguration_id) 
        references CxmlConfiguration (id);

    alter table sender_credential_table_AUD 
        add index FK7D60588175D1432 (REV), 
        add constraint FK7D60588175D1432 
        foreign key (REV) 
        references REVINFO (REV);

    alter table to_credential_table 
        add index FKBD1F632AD0B0DA57 (CxmlConfiguration_id), 
        add constraint FKBD1F632AD0B0DA57 
        foreign key (CxmlConfiguration_id) 
        references CxmlConfiguration (id);

    alter table to_credential_table 
        add index FKBD1F632ABEAB89E7 (toCredentials_id), 
        add constraint FKBD1F632ABEAB89E7 
        foreign key (toCredentials_id) 
        references CxmlCredential (id);

    alter table to_credential_table_AUD 
        add index FK8B31EBFB75D1432 (REV), 
        add constraint FK8B31EBFB75D1432 
        foreign key (REV) 
        references REVINFO (REV);

    alter table vxvxvpt_AUD 
        add index FK54BC884775D1432 (REV), 
        add constraint FK54BC884775D1432 
        foreign key (REV) 
        references REVINFO (REV);
