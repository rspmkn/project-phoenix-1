package com.apd.phoenix.core;

import java.security.SecureRandom;
import org.apache.commons.lang.RandomStringUtils;

/**
 * This class provides utility methods to use for encryption.
 * 
 * @author RHC
 *
 */
public final class EncryptionUtils {

    //This constant is the difficulty used for salts. 
    //The value of this constant can be changed as computers get faster over time.
    private static final int SALT_WORK_CONSTANT = 12;

    //This constant is used for the default string length for a secure, random string.
    //The value of this constant can be changed as computers get faster over time.
    private static final int RANDOM_STRING_LENGTH = 21;

    //This random number generator is used as a seed for the salt generation and 
    //for the randomAlphanumeric generation.
    private static final SecureRandom secureRandom = new SecureRandom();

    /**
     * This method returns a secure, randomly-generated salt.
     * 
     * @return A randomly-generated salt for a hashed password.
     */
    public static final String salt() {
        return BCrypt.gensalt(SALT_WORK_CONSTANT, secureRandom);
    }

    /**
     * This method takes a plaintext password and a salt, and returns a hash of the password.
     * 
     * @param plaintext - the plaintext password
     * @param salt - the salt for the hash
     * @return The hashed password.
     */
    public static final String hash(String plaintext, String salt) {
        return BCrypt.hashpw(plaintext, salt);
    }

    /**
     * This method returns a cryptographically-secure random string. This method can be used to generate 
     * pseudorandom temporary passwords, e.g. for first-time login. The size of the string is determined by the 
     * RANDOM_STRING_LENGTH private static final integer on EncryptionUtils.
     * 
     * @return The random alphanumeric string.
     */
    public static final String randomAlphanumeric() {
        return randomAlphanumeric(RANDOM_STRING_LENGTH);
    }

    /**
     * This method returns a cryptographically-secure random string. This method can be used to generate 
     * pseudorandom temporary passwords, e.g. for first-time login. The size of the string is determined by the 
     * length integer parameter.
     * 
     * @param length - the length of the string to return
     * @return A random alphanumeric string.
     */
    public static final String randomAlphanumeric(int length) {
        return RandomStringUtils.random(length, 0, 0, true, true, null, secureRandom);
    }
}
