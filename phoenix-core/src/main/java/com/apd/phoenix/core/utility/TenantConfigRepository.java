package com.apd.phoenix.core.utility;

import java.io.File;
import java.io.FileInputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class is used to lookup tenant specific configurations, such as a tenants domain name, or database schema.  
 * These configs are stored in a {@link TenantConfiguration} object. TenantConfigRepository loads configurations for
 * all tenants  when it's instantiated and stores them in a cache for more efficient access at runtime.  It also
 * takes advantage of the singleton pattern to ensure the amount of trips to the filesystem during an invocation of the
 * constructor to read the tenant configs are somewhat minimized as this is an expensive operation.
 * 
 * @author rmallred
 *
 */
public class TenantConfigRepository {

    //This tenant will be used to generate the generic tenant ID, which is used to set
    //up routes that don't use tenant-specific properties
    private static final String GENERIC_TENANT = "APD";
    private static final Logger LOG = LoggerFactory.getLogger(TenantConfigRepository.class);
    private static final String CONFIG_LOCATION_SYSTEM_PROPERTY_NAME = "phoenix.config.home";
    private static final Map<String, TenantConfiguration> configCache = Collections
            .synchronizedMap(new HashMap<String, TenantConfiguration>());
    private static final Map<String, String> domainToNameCache = Collections
            .synchronizedMap(new HashMap<String, String>());
    private static final Map<String, Map<String, Properties>> tenantProperties = Collections
            .synchronizedMap(new HashMap<String, Map<String, Properties>>());
    private static final Map<String, Properties> genericProperties = Collections
            .synchronizedMap(new HashMap<String, Properties>());

    private static final Map<Long, String> tenantIDToNameCache = Collections
            .synchronizedMap(new HashMap<Long, String>());

    private static final TenantConfigRepository singleton = new TenantConfigRepository();

    private static final String DEFAULT_JBPM_SCHEMA_SUFFIX = "_JBPM";

    public static TenantConfigRepository getInstance() {
        return singleton;
    }

    public TenantConfigRepository() {
        String propertyHome = System.getProperty(CONFIG_LOCATION_SYSTEM_PROPERTY_NAME);
        File file = new File(propertyHome);
        String[] directories = file.list(new FilenameFilter() {

            @Override
            public boolean accept(File current, String name) {
                //the "keys" directory was a legacy directory for SSL keys, before multitenancy
                return new File(current, name).isDirectory() && !name.equalsIgnoreCase("keys");
            }
        });
        List<String> tenantConfigDirectories = (Arrays.asList(directories));
        LOG.info("Found tenant configs in the following directories " + tenantConfigDirectories.toString());
        try {
            for (String tenantDir : tenantConfigDirectories) {
                String tenantPropsFilePath = propertyHome + "/" + tenantDir + "/tenant.properties";
                InputStream is = new FileInputStream(tenantPropsFilePath);
                Properties tenantProps = new Properties();
                tenantProps.load(is);
                String tenantName = tenantProps.getProperty("name");
                String tenantSchema = tenantProps.getProperty("schema.name");
                String tenantJbpmSchema = tenantProps.getProperty("jbpm.schema.name");
                if (StringUtils.isBlank(tenantJbpmSchema)) {
                    //if no jbpm schema name is specified, defaults to default schema plus "_JBPM"
                    tenantJbpmSchema = tenantSchema + DEFAULT_JBPM_SCHEMA_SUFFIX;
                }
                String tenantDomains = tenantProps.getProperty("domain");
                Long tenantId = Long.valueOf(tenantProps.getProperty("id"));
                String tenantCode = tenantProps.getProperty("code");
                String tenantCustomerServicePhoneNumber = tenantProps.getProperty("customerServicePhoneNumber");
                TenantConfiguration tenantConfig = new TenantConfiguration(tenantId, tenantName, tenantSchema,
                        tenantJbpmSchema, tenantDomains, tenantCode, tenantCustomerServicePhoneNumber, tenantDir);
                configCache.put(tenantName, tenantConfig);
                for (String domain : tenantDomains.split(",")) {
                    domainToNameCache.put(domain, tenantName);
                }
                tenantIDToNameCache.put(tenantId, tenantName);
                loadTenantProperties(tenantDir, tenantName);
            }
            loadGenericPropertyFiles();
        }
        catch (IOException e) {
            LOG.error("Error loading tenant properties", e);
        }

    }

    public Set<String> getTenantNames() {
        return configCache.keySet();
    }

    public TenantConfiguration getTenantPropertiesByName(String name) {
        if (!StringUtils.isBlank(name)) {

            TenantConfiguration config = get(name);
            if (config != null) {
                return config;
            }
        }
        return null;
    }

    public Long getTenantIdByName(String tenantName) {
        return getTenantPropertiesByName(tenantName).getId();
    }

    /**
     * Returns a tenant ID that can be used when accessing tenant-specific properties files to access values
     * that are not tenant specific. For example, getting DLQs from ussco.integration.properties - the file is
     * tenant-specific, but the same DLQs are used across all tenants.
     * 
     * If this tenant ID is used instead of an appropriate tenant ID for a tenant-specific property, unpredictable
     * behavior can result. Do not use this method unless you know exactly why it's necessary.
     * 
     * In short: only use this method when there isn't a tenant ID available, and you need to get non-tenant-aware
     * properties from files that are tenant-specific.
     * 
     * @return
     */
    public Long getGenericTenantId() {
        LOG.debug("Using getGenericTenantId. Unpredictable behavior may result if this method is misused!");
        return this.getTenantIdByName(GENERIC_TENANT);

    }

    public Long getTenantIdByDomain(String tenantDomain) {
        return getTenantPropertiesByDomain(tenantDomain).getId();
    }

    public TenantConfiguration getTenantPropertiesById(Long tenantId) {
        return getTenantPropertiesByName(tenantIDToNameCache.get(tenantId));
    }

    public TenantConfiguration getTenantPropertiesByDomain(String domain) {
        return getTenantPropertiesByName(domainToNameCache.get(domain));
    }

    public TenantConfiguration get(String name) {
        return configCache.get(name);
    }

    /**
     * Used when direct access to the Repo isn't possible
     */
    public String getTenantSchemaByDomain(String domain) {
        LOG.debug("Looking for schema for tenant domain: {}", domain);
        return getTenantPropertiesByDomain(domain).getSchemaName();
    }

    /**
     * Used when direct access to the Repo isn't possible
     */
    public String getTenantJbpmSchemaByDomain(String domain) {
        LOG.debug("Looking for jbpm schema for tenant domain: {}", domain);
        return getTenantPropertiesByDomain(domain).getJbpmSchemaName();
    }

    public String getProperty(Long tenantId, String propFile, String property, String defaultValue) {
        return getProperty(tenantIDToNameCache.get(tenantId), propFile, property, defaultValue);
    }

    public String getProperty(Long tenantId, String propFile, String property) {
        return getProperty(tenantId, propFile, property, null);
    }

    /**
     * This method is used to pull properties values from configuration files.  It first checks for tenant specific properties files, then defaults to a generic
     * property file
     * 
     * @param tenantName
     * @param propFile
     * @param property
     * @return
     */
    public String getProperty(String tenantName, String propFile, String property) {
        return getProperty(tenantName, propFile, property, null);
    }

    /**
     * This method is used to pull properties values from configuration files.  It first checks for tenant specific properties files, then defaults to a generic
     * property file for any tenant, if still nothing is found, the String passed into the defaultValue parameter is used.
     * 
     * @param tenant
     * @param propFile
     * @param property
     * @return
     */
    public String getProperty(String tenant, String propFile, String property, String defaultValue) {
        Properties tenantProps = null;
        propFile = propFile.replace(".properties", "");
        if (tenant != null && !tenant.equalsIgnoreCase("")) {
            //Attempt to pull tenant specific properties first
            tenantProps = tenantProperties.get(tenant).get(propFile);
        }
        //If none are found, check for generic properties next
        if (tenantProps == null) {
            tenantProps = genericProperties.get(propFile);
        }
        return tenantProps.getProperty(property, defaultValue);
    }

    /**
     * Used to pull {@link Properites} objects from the TenantConfigRepo by tenant and propFile
     * @param tenant Name of the tenant you want to pull properties for
     * @param propFile Name of the properties file you'd like
     * @return Properties object for the file and tenant specified, or a generic one if no tenant specific version is found, 
     * finally null if neither a tenant-specific or generic version exists
     */
    public Properties getPropertiesByTenantName(String tenant, String propFile) {
        Properties tenantProps = null;
        propFile = propFile.replace(".properties", "");
        if (tenant != null) {
            //Attempt to pull tenant specific properties first
            tenantProps = tenantProperties.get(tenant).get(propFile);
        }
        //If none are found, check for generic properties next
        if (tenantProps == null) {
            tenantProps = genericProperties.get(propFile);
        }
        return tenantProps;
    }

    public Properties getPropertiesByTenantId(Long tenantId, String propFile) {
        return getPropertiesByTenantName(tenantIDToNameCache.get(tenantId), propFile);
    }

    public Properties getProperties(String propFile) {
        return getPropertiesByTenantName(null, propFile);
    }

    public String getProperty(String propFile, String property) {
        return getProperty("", propFile, property);
    }

    public String getTenantNameById(Long tenantId) {
        return tenantIDToNameCache.get(tenantId);
    }

    public String getTenantNameByDomain(String domain) {
        return domainToNameCache.get(domain);
    }

    public List<String> getTenantDomainsById(Long tenantId) {
        return get(getTenantNameById(tenantId)).getDomains();
    }

    public String getTenantDefaultDomainById(Long tenantId) {
        return getTenantDomainsById(tenantId).get(0);
    }

    public String getTenantSpecificEncryptionKeyPath(Long tenantId, String keyFileName) {
        String propertyHome = System.getProperty(CONFIG_LOCATION_SYSTEM_PROPERTY_NAME);

        String tenantName = tenantIDToNameCache.get(tenantId);

        TenantConfiguration config = configCache.get(tenantName);

        return "file:" + propertyHome + "/" + config.getConfDirectoryName() + "/keys/" + keyFileName;

    }

    private void loadTenantProperties(String tenantDir, String tenantName) throws IOException {
    	String propertyHome = System.getProperty(CONFIG_LOCATION_SYSTEM_PROPERTY_NAME);    	
    	File[] files = new File(propertyHome + "/" + tenantDir).listFiles();
    	Map<String, Properties> propertiesMap = new HashMap<>();
    	for (File file : files) {
    	    if (file.isFile()) {
    	    	InputStream is = new FileInputStream(file);
                Properties tenantProps = new Properties();
                tenantProps.load(is);
                propertiesMap.put(file.getName().replace(".properties", ""), tenantProps);
    	    }
    	}
    	tenantProperties.put(tenantName, propertiesMap);
    }

    private void loadGenericPropertyFiles() throws IOException {
        String propertyHome = System.getProperty(CONFIG_LOCATION_SYSTEM_PROPERTY_NAME);
        File[] files = new File(propertyHome + "/").listFiles();
        for (File file : files) {
            if (file.isFile()) {
                InputStream is = new FileInputStream(file);
                Properties genericPropFile = new Properties();
                genericPropFile.load(is);
                genericProperties.put(file.getName().replace(".properties", ""), genericPropFile);
            }
        }

    }

}
