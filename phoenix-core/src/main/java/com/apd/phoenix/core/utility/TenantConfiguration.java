package com.apd.phoenix.core.utility;

import java.util.Arrays;
import java.util.List;

/**
 * Simple POJO to hold values related to each tenant.  These values include a
 * unique numeric ID, a display name, a schema name, and a list of valid domain names
 * 
 * @author rmallred
 *
 */
public class TenantConfiguration {

    private static final String COLLECTION_SUFFIX = "_collection";
    private Long id;
    private String tenantDisplayName;
    private String schemaName;
    private String jbpmSchemaName;
    private List<String> domains;
    private String code;
    private String customerServiceNumber;
    private String confDirectoryName;

    public TenantConfiguration(Long id, String tenantDisplayName, String schemaName, String jbpmSchemaName,
            String domains, String code, String customerServiceNumber, String confDirectoryName) {
        this.setId(id);
        this.setTenantDisplayName(tenantDisplayName);
        this.setSchemaName(schemaName);
        this.setJbpmSchemaName(jbpmSchemaName);
        this.setDomains(Arrays.asList((domains.split(","))));
        this.setCode(code);
        this.setCustomerServiceNumber(customerServiceNumber);
        this.setConfDirectoryName(confDirectoryName);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTenantDisplayName() {
        return tenantDisplayName;
    }

    public void setTenantDisplayName(String tenantDisplayName) {
        this.tenantDisplayName = tenantDisplayName;
    }

    public String getSchemaName() {
        return schemaName;
    }

    public void setSchemaName(String schemaName) {
        this.schemaName = schemaName;
    }

    public String getJbpmSchemaName() {
        return jbpmSchemaName;
    }

    public void setJbpmSchemaName(String jbpmSchemaName) {
        this.jbpmSchemaName = jbpmSchemaName;
    }

    public List<String> getDomains() {
        return domains;
    }

    public void setDomains(List<String> domains) {
        this.domains = domains;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCustomerServiceNumber() {
        return customerServiceNumber;
    }

    public void setCustomerServiceNumber(String customerServiceNumber) {
        this.customerServiceNumber = customerServiceNumber;
    }

    @Override
    public String toString() {
        return "TenantConfiguration [id=" + id + ", tenantDisplayName=" + tenantDisplayName + ", schemaName="
                + schemaName + ", domains=" + domains + ", code=" + code + ", customerServiceNumber="
                + customerServiceNumber + "]";
    }

    public String getSolrCollection() {
        return schemaName + COLLECTION_SUFFIX;
    }

    public String getConfDirectoryName() {
        return confDirectoryName;
    }

    public void setConfDirectoryName(String confDirectoryName) {
        this.confDirectoryName = confDirectoryName;
    }

}
