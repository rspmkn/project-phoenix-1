package com.apd.phoenix.core.utility;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Deprecated
public class EcommercePropertiesLoader {

    private static Logger logger = LoggerFactory.getLogger(EcommercePropertiesLoader.class);
    private PropertiesConfiguration ecommerceProperties;
    private static final EcommercePropertiesLoader singleton = new EcommercePropertiesLoader();

    public static EcommercePropertiesLoader getInstance() {
        return singleton;
    }

    private EcommercePropertiesLoader() {
        String propertyHome = System.getProperty("phoenix.config.home");

        try (InputStream is = new FileInputStream(propertyHome + "/ecommerce.properties")) {

            ecommerceProperties = new PropertiesConfiguration();
            ecommerceProperties.load(is);
        } catch (ConfigurationException | IOException ex) {
            logger.error("Could not load \"ecommerceProperties.properties\": ", ex);
        }
    }

    public PropertiesConfiguration getEcommerceProperties() {
        return ecommerceProperties;
    }
}
