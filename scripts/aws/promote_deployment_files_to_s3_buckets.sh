#!/bin/sh
export PHOENIX_VERSION=$1
export TO=$2
export FROM=$3

aws copy apd-phoenix-$TO-configs/eap-deployment-artifacts/phoenix-administration-$PHOENIX_VERSION.war /apd-phoenix-$FROM-configs/eap-deployment-artifacts/phoenix-administration-$PHOENIX_VERSION.war

aws copy apd-phoenix-$TO-configs/eap-deployment-artifacts/phoenix-customerservice-$PHOENIX_VERSION.war /apd-phoenix-$FROM-configs/eap-deployment-artifacts/phoenix-customerservice-$PHOENIX_VERSION.war

aws copy apd-phoenix-$TO-configs/eap-deployment-artifacts/phoenix-shopping-$PHOENIX_VERSION.war /apd-phoenix-$FROM-configs/eap-deployment-artifacts/phoenix-shopping-$PHOENIX_VERSION.war

aws copy apd-phoenix-$TO-configs/eap-deployment-artifacts/phoenix-integration-ws-$PHOENIX_VERSION.war /apd-phoenix-$FROM-configs/eap-deployment-artifacts/phoenix-integration-ws-$PHOENIX_VERSION.war

aws copy apd-phoenix-$TO-configs/eap-deployment-artifacts/phoenix-rest-service-$PHOENIX_VERSION.war /apd-phoenix-$FROM-configs/eap-deployment-artifacts/phoenix-rest-service-$PHOENIX_VERSION.war

aws copy apd-phoenix-$TO-configs/eap-deployment-artifacts/phoenix-batch-service-$PHOENIX_VERSION.ear /apd-phoenix-$FROM-configs/eap-deployment-artifacts/phoenix-batch-service-$PHOENIX_VERSION.ear

aws copy apd-phoenix-$TO-configs/ansible-deploy.tar.gz /apd-phoenix-dev-configs/ansible-deploy.tar.gz
