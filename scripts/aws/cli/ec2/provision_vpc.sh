#!/bin/bash

echo "CAUTION: CloudFormation templates used to create environment stacks for APD Marketplace software supersede any provisioning tasks included in this script" 

## Create the VPC (confirm if we want a different vpc for production)
# Create VPC with Public and Private Subnets

## Create the security groups
## WebServerSG - For the web servers in the public subnet
# Create WebServerSG w/description and id of vpc

## NATSG - For the NAT instance in the public subnet
# Create NATSG w/description and id of vpc

## Create the security groups
## AppServerSG - For the web servers in the public subnet
# Create WebServerSG w/description and id of vpc

## DBServerSG - For the app servers in the private subnet
# Create AppServerSG w/description and id of vpc

## VPNSG - For the VPN Server in the public subnet (confirm)
# Create VPNSG w/description and id of vpc

## Create WebServerSG Inbound/Outbound traffic rules [TCP Port (Service)][Source/Destination]
# Create Inbound Rule = HTTP 0.0.0.0/0
# Create Inbound Rule = HTTPS 0.0.0.0/0
# Create Inbound Rule = SSH [public IP address range of your network]
# Delete Outbound Rule = All 0.0.0.0/0
# Create Outbound Rule = 1521 (Oracle) [ID of the DBServerSG security group] 

## Create NATSG Inbound/Outbound traffic rules [TCP Port (Service)][Source/Destination]
# Create Inbound Rule = HTTP [IP address range of private subnet]
# Create Inbound Rule = HTTPS [IP address range of private subnet]
# Create Inbound Rule = SSH [public IP address range of your network]
# Delete Outbound Rule = All 0.0.0.0/0
# Create Outbound Rule = HTTP 0.0.0.0/0
# Create Outbound Rule = HTTPS 0.0.0.0/0

## Create AppServerSG Inbound/Outbound traffic rules [TCP Port (Service)][Source/Destination]
# Create Inbound Rule = HTTP [ID of WebServerSG security group]
# Create Inbound Rule = HTTPS [ID of WebServerSG security group]
# Delete Outbound Rule = All 0.0.0.0/0
# Create Outbound Rule = HTTP 0.0.0.0/0
# Create Outbound Rule = HTTPS 0.0.0.0/0

## Create DBServerSG Inbound/Outbound traffic rules [TCP Port (Service)][Source/Destination]
# Create Inbound Rule = 1521 (Oracle) [ID of AppServerSG security group]
# Delete Outbound Rule = All 0.0.0.0/0
# Create Outbound Rule = HTTP 0.0.0.0/0
# Create Outbound Rule = HTTPS 0.0.0.0/0

## Create VPNSG Inbound/Outbound traffic rules [TCP Port (Service)][Source/Destination]

## Change Security Group for the network interface for the NAT instance to NATSG

## Launching an instance in the public subnet
# select the ID of the VPC, the public subnet, and the WebServerSG security group
## Allocate new EIP address to ID of VPC 
## ec2-describe-addresses
## ec2-allocate-address
## ec2-release-address
## ec2-associate-address
## ec2-disassociate-address
# Associate the EIP address with the Private IP Address of the instance being launched in the public subnet

## Launching an instance in the private subnet
# Select the ID of the VPC, the private subnet, and the AppServerSG or DBServerSG security group




