#!/bin/bash

# README
# This script applies the patch updates associated with version 5.3.1.BRMS-P02 of JBoss BRMS to an existing JBoss BRMS
# standalone installation.  In the APD environments, this script is to be run on any EC2 server hosting an earlier version of JBoss BRMS 5.3.1.

JBOSS_HOME=/usr/share/jbossas
JBOSS_CONFIG=standalone/deployments 
JBOSS_BACK=/root/back
PATCH_DIR=/root/patch
GUVNOR_BACK=/root/gback
cp -r $JBOSS_HOME/$JBOSS_CONFIG/* $GUVNOR_BACK

mv $JBOSS_HOME/$JBOSS_CONFIG/designer.war/js/Plugins/propertywindow.js $JBOSS_BACK
mv $JBOSS_HOME/$JBOSS_CONFIG/designer.war/WEB-INF/classes/org/jbpm/designer/bpmn2/impl/Bpmn2JsonMarsher.class $JBOSS_BACK
mv $JBOSS_HOME/$JBOSS_CONFIG/designer.war/WEB-INF/classes/org/jbpm/designer/bpmn2/impl/Bpmn2JsonUnmarsher.class $JBOSS_BACK
mv $JBOSS_HOME/$JBOSS_CONFIG/designer.war/WEB-INF/classes/org/jbpm/designer/bpmn2/validation/BPMN2SyntaxChecker.class $JBOSS_BACK
mv $JBOSS_HOME/$JBOSS_CONFIG/designer.war/WEB-INF/classes/org/jbpm/designer/web/preprocessing/impl/JbpmPreprocessingUnit.class $JBOSS_BACK
mv $JBOSS_HOME/$JBOSS_CONFIG/designer.war/WEB-INF/classes/org/jbpm/designer/web/preprocessing/impl/JbpmPreprocessingUnit\$ThemeInfo.class $JBOSS_BACK
mv $JBOSS_HOME/$JBOSS_CONFIG/designer.war/WEB-INF/classes/org/jbpm/designer/web/batikprotocolhandler/GuvnorParsedURLData.class $JBOSS_BACK
mv $JBOSS_HOME/$JBOSS_CONFIG/designer.war/WEB-INF/classes/org/jbpm/designer/web/server/ServletUtil.class $JBOSS_BACK
mv $JBOSS_HOME/$JBOSS_CONFIG/designer.war/WEB-INF/classes/org/jbpm/designer/web/profile/IDiagramProfile.class $JBOSS_BACK
mv $JBOSS_HOME/$JBOSS_CONFIG/designer.war/WEB-INF/classes/org/jbpm/designer/web/profile/impl/DefaultProfileImpl.class $JBOSS_BACK
mv $JBOSS_HOME/$JBOSS_CONFIG/designer.war/WEB-INF/classes/org/jbpm/designer/web/profile/impl/DefaultProfileImpl\$1.class $JBOSS_BACK
mv $JBOSS_HOME/$JBOSS_CONFIG/designer.war/WEB-INF/classes/org/jbpm/designer/web/profile/impl/DefaultProfileImpl\$2.class $JBOSS_BACK
mv $JBOSS_HOME/$JBOSS_CONFIG/designer.war/WEB-INF/classes/org/jbpm/designer/web/profile/impl/EpnProfileImpl.class $JBOSS_BACK
mv $JBOSS_HOME/$JBOSS_CONFIG/designer.war/WEB-INF/classes/org/jbpm/designer/web/profile/impl/EpnProfileImpl\$1.class $JBOSS_BACK
mv $JBOSS_HOME/$JBOSS_CONFIG/designer.war/WEB-INF/classes/org/jbpm/designer/web/profile/impl/EpnProfileImpl\$2.class $JBOSS_BACK
mv $JBOSS_HOME/$JBOSS_CONFIG/designer.war/WEB-INF/classes/org/jbpm/designer/web/profile/impl/JbpmProfileImpl.class $JBOSS_BACK
mv $JBOSS_HOME/$JBOSS_CONFIG/designer.war/WEB-INF/classes/org/jbpm/designer/web/profile/impl/JbpmProfileImpl\$1.class $JBOSS_BACK
mv $JBOSS_HOME/$JBOSS_CONFIG/designer.war/WEB-INF/classes/org/jbpm/designer/web/profile/impl/JbpmProfileImpl$\2.class $JBOSS_BACK
mv $JBOSS_HOME/$JBOSS_CONFIG/designer.war/WEB-INF/classes/org/jbpm/designer/web/repository/impl/UUIDBasedJbpmRepository.class $JBOSS_BACK
mv $JBOSS_HOME/$JBOSS_CONFIG/designer.war/WEB-INF/lib/drools-compiler-5.3.1.BRMS* $JBOSS_BACK
mv $JBOSS_HOME/$JBOSS_CONFIG/designer.war/WEB-INF/lib/drools-core-5.3.1.BRMS* $JBOSS_BACK
mv $JBOSS_HOME/$JBOSS_CONFIG/designer.war/WEB-INF/lib/jbpm-bpmn2-5.3.1.BRMS* $JBOSS_BACK
mv $JBOSS_HOME/$JBOSS_CONFIG/designer.war/WEB-INF/lib/jbpm-bpmn2-emfextmodel-5.3.1.BRMS.jar $JBOSS_BACK
mv $JBOSS_HOME/$JBOSS_CONFIG/designer.war/WEB-INF/lib/jbpm-flow-5.3.1.BRMS* $JBOSS_BACK
mv $JBOSS_HOME/$JBOSS_CONFIG/designer.war/WEB-INF/lib/jbpm-flow-builder-5.3.1.BRMS.jar $JBOSS_BACK
mv $JBOSS_HOME/$JBOSS_CONFIG/designer.war/WEB-INF/lib/jbpm-workitems-5.3.1.BRMS.jar $JBOSS_BACK
mv $JBOSS_HOME/$JBOSS_CONFIG/designer.war/WEB-INF/lib/knowledge-api-5.3.1.BRMS.jar $JBOSS_BACK
mv $JBOSS_HOME/$JBOSS_CONFIG/designer.war/WEB-INF/lib/mvel2-2.1.3.Final.jar $JBOSS_BACK
mv $JBOSS_HOME/$JBOSS_CONFIG/designer.war/profiles/jbpm.xml $JBOSS_BACK
mv $JBOSS_HOME/$JBOSS_CONFIG/designer.war/stencilsets/bpmn2.0jbpm/stencildata/bpmn2.0jbpm.orig $JBOSS_BACK
mv $JBOSS_HOME/$JBOSS_CONFIG/designer.war/WEB-INF/lib/protobuf-java-2.4.1.jar $JBOSS_BACK
mv $JBOSS_HOME/$JBOSS_CONFIG/jboss-brms.war/org.drools.guvnor.Guvnor/* $JBOSS_BACK
mv $JBOSS_HOME/$JBOSS_CONFIG/jboss-brms.war/WEB-INF/classes/preferences.properties $JBOSS_BACK
mv $JBOSS_HOME/$JBOSS_CONFIG/jboss-brms.war/WEB-INF/classes/org/drools/guvnor/client/configurations/ApplicationPreferences.class $JBOSS_BACK
mv $JBOSS_HOME/$JBOSS_CONFIG/jboss-brms.war/WEB-INF/classes/org/drools/guvnor/client/decisiontable/widget/VerticalDecisionTableHeaderWidget\$HeaderWidget\$HeaderSplitter\$1.class $JBOSS_BACK
mv $JBOSS_HOME/$JBOSS_CONFIG/jboss-brms.war/WEB-INF/classes/org/drools/guvnor/client/decisiontable/widget/VerticalDecisionTableHeaderWidget\$HeaderWidget\$HeaderSplitter.class $JBOSS_BACK
mv $JBOSS_HOME/$JBOSS_CONFIG/jboss-brms.war/WEB-INF/classes/org/drools/guvnor/client/decisiontable/widget/VerticalDecisionTableHeaderWidget\$HeaderWidget\$HeaderSorter\$1.class $JBOSS_BACK
mv $JBOSS_HOME/$JBOSS_CONFIG/jboss-brms.war/WEB-INF/classes/org/drools/guvnor/client/decisiontable/widget/VerticalDecisionTableHeaderWidget\$HeaderWidget\$HeaderSorter.class $JBOSS_BACK
mv $JBOSS_HOME/$JBOSS_CONFIG/jboss-brms.war/WEB-INF/classes/org/drools/guvnor/client/decisiontable/widget/VerticalDecisionTableHeaderWidget\$HeaderWidget\$HeaderSplitter\$HeaderRowAnimation.class $JBOSS_BACK
mv $JBOSS_HOME/$JBOSS_CONFIG/jboss-brms.war/WEB-INF/classes/org/drools/guvnor/client/decisiontable/widget/VerticalDecisionTableHeaderWidget\$HeaderWidget\$1.class $JBOSS_BACK
mv $JBOSS_HOME/$JBOSS_CONFIG/jboss-brms.war/WEB-INF/classes/org/drools/guvnor/client/decisiontable/widget/VerticalDecisionTableHeaderWidget\$HeaderWidget.class $JBOSS_BACK
mv $JBOSS_HOME/$JBOSS_CONFIG/jboss-brms.war/WEB-INF/classes/org/drools/guvnor/client/decisiontable/widget/VerticalDecisionTableHeaderWidget\$1.class $JBOSS_BACK
mv $JBOSS_HOME/$JBOSS_CONFIG/jboss-brms.war/WEB-INF/classes/org/drools/guvnor/client/decisiontable/widget/VerticalDecisionTableHeaderWidget\$2.class $JBOSS_BACK
mv $JBOSS_HOME/$JBOSS_CONFIG/jboss-brms.war/WEB-INF/classes/org/drools/guvnor/client/decisiontable/widget/VerticalDecisionTableHeaderWidget.class $JBOSS_BACK
mv $JBOSS_HOME/$JBOSS_CONFIG/jboss-brms.war/WEB-INF/classes/org/drools/guvnor/client/messages/Constants.class $JBOSS_BACK
mv $JBOSS_HOME/$JBOSS_CONFIG/jboss-brms.war/WEB-INF/classes/org/drools/guvnor/client/modeldriven/HumanReadable.class $JBOSS_BACK
mv $JBOSS_HOME/$JBOSS_CONFIG/jboss-brms.war/WEB-INF/classes/org/drools/guvnor/client/modeldriven/ui/RuleModellerActionSelectorPopup.class $JBOSS_BACK
mv $JBOSS_HOME/$JBOSS_CONFIG/jboss-brms.war/WEB-INF/classes/org/drools/guvnor/client/modeldriven/ui/RuleModellerActionSelectorPopup\$1.class $JBOSS_BACK
mv $JBOSS_HOME/$JBOSS_CONFIG/jboss-brms.war/WEB-INF/classes/org/drools/guvnor/client/modeldriven/ui/RuleModellerActionSelectorPopup\$2.class $JBOSS_BACK
mv $JBOSS_HOME/$JBOSS_CONFIG/jboss-brms.war/WEB-INF/classes/org/drools/guvnor/client/modeldriven/ui/RuleModellerActionSelectorPopup\$3.class $JBOSS_BACK
mv $JBOSS_HOME/$JBOSS_CONFIG/jboss-brms.war/WEB-INF/classes/org/drools/guvnor/client/modeldriven/ui/RuleModellerActionSelectorPopup\$4.class $JBOSS_BACK
mv $JBOSS_HOME/$JBOSS_CONFIG/jboss-brms.war/WEB-INF/classes/org/drools/guvnor/client/modeldriven/ui/RuleModellerActionSelectorPopup\$5.class $JBOSS_BACK
mv $JBOSS_HOME/$JBOSS_CONFIG/jboss-brms.war/WEB-INF/classes/org/drools/guvnor/client/modeldriven/ui/RuleModellerActionSelectorPopup\$6.class $JBOSS_BACK
mv $JBOSS_HOME/$JBOSS_CONFIG/jboss-brms.war/WEB-INF/classes/org/drools/guvnor/client/modeldriven/ui/RuleModellerActionSelectorPopup\$7.class $JBOSS_BACK
mv $JBOSS_HOME/$JBOSS_CONFIG/jboss-brms.war/WEB-INF/classes/org/drools/guvnor/client/modeldriven/ui/RuleModellerActionSelectorPopup\$8.class $JBOSS_BACK
mv $JBOSS_HOME/$JBOSS_CONFIG/jboss-brms.war/WEB-INF/classes/org/drools/guvnor/client/modeldriven/ui/RuleModellerActionSelectorPopup\$9.class $JBOSS_BACK
mv $JBOSS_HOME/$JBOSS_CONFIG/jboss-brms.war/WEB-INF/classes/org/drools/guvnor/client/modeldriven/ui/RuleModellerActionSelectorPopup\$10.class $JBOSS_BACK
mv $JBOSS_HOME/$JBOSS_CONFIG/jboss-brms.war/WEB-INF/classes/org/drools/guvnor/client/modeldriven/ui/RuleModellerActionSelectorPopup\$11.class $JBOSS_BACK
mv $JBOSS_HOME/$JBOSS_CONFIG/jboss-brms.war/WEB-INF/classes/org/drools/guvnor/client/modeldriven/ui/RuleModellerActionSelectorPopup\$12.class $JBOSS_BACK
mv $JBOSS_HOME/$JBOSS_CONFIG/jboss-brms.war/WEB-INF/classes/org/drools/guvnor/client/modeldriven/ui/RuleModellerActionSelectorPopup\$13.class $JBOSS_BACK
mv $JBOSS_HOME/$JBOSS_CONFIG/jboss-brms.war/WEB-INF/classes/org/drools/guvnor/client/modeldriven/ui/RuleModellerActionSelectorPopup\$14.class $JBOSS_BACK
mv $JBOSS_HOME/$JBOSS_CONFIG/jboss-brms.war/WEB-INF/classes/org/drools/guvnor/client/modeldriven/ui/RuleModellerActionSelectorPopup\$15.class $JBOSS_BACK
mv $JBOSS_HOME/$JBOSS_CONFIG/jboss-brms.war/WEB-INF/classes/org/drools/guvnor/client/modeldriven/ui/RuleModellerActionSelectorPopup\$16.class $JBOSS_BACK
mv $JBOSS_HOME/$JBOSS_CONFIG/jboss-brms.war/WEB-INF/classes/org/drools/guvnor/client/modeldriven/ui/RuleModellerActionSelectorPopup\$17.class $JBOSS_BACK
mv $JBOSS_HOME/$JBOSS_CONFIG/jboss-brms.war/WEB-INF/classes/org/drools/guvnor/client/modeldriven/ui/TemplateDataHeaderWidget\$HeaderWidget\$HeaderSorter\$1.class $JBOSS_BACK
mv $JBOSS_HOME/$JBOSS_CONFIG/jboss-brms.war/WEB-INF/classes/org/drools/guvnor/client/modeldriven/ui/TemplateDataHeaderWidget\$HeaderWidget\$HeaderSorter.class $JBOSS_BACK
mv $JBOSS_HOME/$JBOSS_CONFIG/jboss-brms.war/WEB-INF/classes/org/drools/guvnor/client/modeldriven/ui/TemplateDataHeaderWidget\$HeaderWidget\$1.class $JBOSS_BACK
mv $JBOSS_HOME/$JBOSS_CONFIG/jboss-brms.war/WEB-INF/classes/org/drools/guvnor/client/modeldriven/ui/TemplateDataHeaderWidget\$HeaderWidget\$2.class $JBOSS_BACK
mv $JBOSS_HOME/$JBOSS_CONFIG/jboss-brms.war/WEB-INF/classes/org/drools/guvnor/client/modeldriven/ui/TemplateDataHeaderWidget\$HeaderWidget.class $JBOSS_BACK
mv $JBOSS_HOME/$JBOSS_CONFIG/jboss-brms.war/WEB-INF/classes/org/drools/guvnor/client/modeldriven/ui/TemplateDataHeaderWidget\$1.class $JBOSS_BACK
mv $JBOSS_HOME/$JBOSS_CONFIG/jboss-brms.war/WEB-INF/classes/org/drools/guvnor/client/modeldriven/ui/TemplateDataHeaderWidget.class $JBOSS_BACK
mv $JBOSS_HOME/$JBOSS_CONFIG/jboss-brms.war/WEB-INF/classes/org/drools/guvnor/client/rpc/AssetPageRequest.class $JBOSS_BACK
mv $JBOSS_HOME/$JBOSS_CONFIG/jboss-brms.war/WEB-INF/classes/org/drools/guvnor/client/widgets/decoratedgrid/AbstractCellFactory.class $JBOSS_BACK
mv $JBOSS_HOME/$JBOSS_CONFIG/jboss-brms.war/WEB-INF/classes/org/drools/guvnor/client/widgets/decoratedgrid/CellHeightCalculatorImpl.class $JBOSS_BACK
mv $JBOSS_HOME/$JBOSS_CONFIG/jboss-brms.war/WEB-INF/classes/org/drools/guvnor/client/widgets/decoratedgrid/CellHeightCalculatorImplIE.class $JBOSS_BACK
mv $JBOSS_HOME/$JBOSS_CONFIG/jboss-brms.war/WEB-INF/classes/org/drools/guvnor/client/widgets/decoratedgrid/CheckboxCellImpl.class $JBOSS_BACK
mv $JBOSS_HOME/$JBOSS_CONFIG/jboss-brms.war/WEB-INF/classes/org/drools/guvnor/client/widgets/decoratedgrid/CheckboxCellImplIE.class $JBOSS_BACK
mv $JBOSS_HOME/$JBOSS_CONFIG/jboss-brms.war/WEB-INF/classes/org/drools/guvnor/client/widgets/decoratedgrid/DynamicColumn.class $JBOSS_BACK
mv $JBOSS_HOME/$JBOSS_CONFIG/jboss-brms.war/WEB-INF/classes/org/drools/guvnor/client/widgets/decoratedgrid/SortConfiguration.class $JBOSS_BACK
mv $JBOSS_HOME/$JBOSS_CONFIG/jboss-brms.war/WEB-INF/classes/org/drools/guvnor/client/widgets/tables/AbstractAssetPagedTable.class $JBOSS_BACK
mv $JBOSS_HOME/$JBOSS_CONFIG/jboss-brms.war/WEB-INF/classes/org/drools/guvnor/client/widgets/tables/AbstractPagedTable.class $JBOSS_BACK
mv $JBOSS_HOME/$JBOSS_CONFIG/jboss-brms.war/WEB-INF/classes/org/drools/guvnor/client/widgets/tables/AbstractSimpleTable.class $JBOSS_BACK
mv $JBOSS_HOME/$JBOSS_CONFIG/jboss-brms.war/WEB-INF/classes/org/drools/guvnor/client/widgets/tables/AdminArchivedPagedTable.class $JBOSS_BACK
mv $JBOSS_HOME/$JBOSS_CONFIG/jboss-brms.war/WEB-INF/classes/org/drools/guvnor/client/widgets/tables/AssetPagedTable.class $JBOSS_BACK
mv $JBOSS_HOME/$JBOSS_CONFIG/jboss-brms.war/WEB-INF/classes/org/drools/guvnor/client/widgets/tables/BuildPackageErrorsSimpleTable.class $JBOSS_BACK
mv $JBOSS_HOME/$JBOSS_CONFIG/jboss-brms.war/WEB-INF/classes/org/drools/guvnor/client/widgets/tables/CategoryPagedTable.class $JBOSS_BACK
mv $JBOSS_HOME/$JBOSS_CONFIG/jboss-brms.war/WEB-INF/classes/org/drools/guvnor/client/widgets/tables/DependenciesPagedTable.class $JBOSS_BACK
mv $JBOSS_HOME/$JBOSS_CONFIG/jboss-brms.war/WEB-INF/classes/org/drools/guvnor/client/widgets/tables/DependenciesPagedTableReadOnly.class $JBOSS_BACK
mv $JBOSS_HOME/$JBOSS_CONFIG/jboss-brms.war/WEB-INF/classes/org/drools/guvnor/client/widgets/tables/InboxIncomingPagedTable.class $JBOSS_BACK
mv $JBOSS_HOME/$JBOSS_CONFIG/jboss-brms.war/WEB-INF/classes/org/drools/guvnor/client/widgets/tables/InboxPagedTable.class $JBOSS_BACK
mv $JBOSS_HOME/$JBOSS_CONFIG/jboss-brms.war/WEB-INF/classes/org/drools/guvnor/client/widgets/tables/LogPagedTable.class $JBOSS_BACK
mv $JBOSS_HOME/$JBOSS_CONFIG/jboss-brms.war/WEB-INF/classes/org/drools/guvnor/client/widgets/tables/PermissionsPagedTablePresenter.class $JBOSS_BACK
mv $JBOSS_HOME/$JBOSS_CONFIG/jboss-brms.war/WEB-INF/classes/org/drools/guvnor/client/widgets/tables/PermissionsPagedTableView.class $JBOSS_BACK
mv $JBOSS_HOME/$JBOSS_CONFIG/jboss-brms.war/WEB-INF/classes/org/drools/guvnor/client/widgets/tables/PropertiesEditorSimpleTable.class $JBOSS_BACK
mv $JBOSS_HOME/$JBOSS_CONFIG/jboss-brms.war/WEB-INF/classes/org/drools/guvnor/client/widgets/tables/QueryPagedTable.class $JBOSS_BACK
mv $JBOSS_HOME/$JBOSS_CONFIG/jboss-brms.war/WEB-INF/classes/org/drools/guvnor/client/widgets/tables/SnapshotComparisonPagedTable.class $JBOSS_BACK
mv $JBOSS_HOME/$JBOSS_CONFIG/jboss-brms.war/WEB-INF/classes/org/drools/guvnor/client/widgets/tables/SortDirection*.class $JBOSS_BACK
mv $JBOSS_HOME/$JBOSS_CONFIG/jboss-brms.war/WEB-INF/classes/org/drools/guvnor/client/widgets/tables/SortableHeader*.class $JBOSS_BACK
# mv $JBOSS_HOME/$JBOSS_CONFIG/jboss-brms.war/WEB-INF/classes/org/drools/guvnor/client/widgets/tables/SortableHeaderGroup*.class $JBOSS_BACK
mv $JBOSS_HOME/$JBOSS_CONFIG/jboss-brms.war/WEB-INF/classes/org/drools/guvnor/client/widgets/tables/StatePagedTable.class $JBOSS_BACK
mv $JBOSS_HOME/$JBOSS_CONFIG/jboss-brms.war/WEB-INF/classes/org/drools/guvnor/server/RepositoryAssetOperations.class $JBOSS_BACK
mv $JBOSS_HOME/$JBOSS_CONFIG/jboss-brms.war/WEB-INF/classes/org/drools/guvnor/server/RepositoryPackageOperations*.class $JBOSS_BACK
mv $JBOSS_HOME/$JBOSS_CONFIG/jboss-brms.war/WEB-INF/classes/org/drools/guvnor/server/builder/pagerow/AssetPageRowBuilder.class $JBOSS_BACK
mv $JBOSS_HOME/$JBOSS_CONFIG/jboss-brms.war/WEB-INF/classes/org/drools/guvnor/server/configurations/ApplicationPreferencesInitializer.class $JBOSS_BACK
mv $JBOSS_HOME/$JBOSS_CONFIG/jboss-brms.war/WEB-INF/classes/org/drools/guvnor/server/files/OryxEditorServlet.class $JBOSS_BACK
mv $JBOSS_HOME/$JBOSS_CONFIG/jboss-brms.war/WEB-INF/classes/org/drools/guvnor/server/files/PackageDeploymentServlet.class $JBOSS_BACK
mv $JBOSS_HOME/$JBOSS_CONFIG/jboss-brms.war/WEB-INF/classes/org/drools/guvnor/server/files/RepositoryServlet.class $JBOSS_BACK
mv $JBOSS_HOME/$JBOSS_CONFIG/jboss-brms.war/WEB-INF/classes/org/drools/guvnor/server/files/RepositoryServlet\$Command.class $JBOSS_BACK
mv $JBOSS_HOME/$JBOSS_CONFIG/jboss-brms.war/WEB-INF/classes/org/drools/guvnor/server/jaxrs/PackageResource.class $JBOSS_BACK
mv $JBOSS_HOME/$JBOSS_CONFIG/jboss-brms.war/WEB-INF/lib/cxf-*.jar $JBOSS_BACK
mv $JBOSS_HOME/$JBOSS_CONFIG/jboss-brms.war/WEB-INF/lib/drools-compiler-5.3.1.BRMS* $JBOSS_BACK
mv $JBOSS_HOME/$JBOSS_CONFIG/jboss-brms.war/WEB-INF/lib/drools-core-5.3.1.BRMS* $JBOSS_BACK
mv $JBOSS_HOME/$JBOSS_CONFIG/jboss-brms.war/WEB-INF/lib/drools-decisiontables-5.3.1.BRMS.jar $JBOSS_BACK
mv $JBOSS_HOME/$JBOSS_CONFIG/jboss-brms.war/WEB-INF/lib/drools-templates-5.3.1.BRMS.jar $JBOSS_BACK
mv $JBOSS_HOME/$JBOSS_CONFIG/jboss-brms.war/WEB-INF/lib/drools-verifier-5.3.1.BRMS.jar $JBOSS_BACK
mv $JBOSS_HOME/$JBOSS_CONFIG/jboss-brms.war/WEB-INF/lib/droolsjbpm-ide-common-5.3.1.BRMS* $JBOSS_BACK
mv $JBOSS_HOME/$JBOSS_CONFIG/jboss-brms.war/WEB-INF/lib/guvnor-repository-5.3.1.BRMS* $JBOSS_BACK
mv $JBOSS_HOME/$JBOSS_CONFIG/jboss-brms.war/WEB-INF/lib/jbpm-bpmn2-5.3.1.BRMS* $JBOSS_BACK
mv $JBOSS_HOME/$JBOSS_CONFIG/jboss-brms.war/WEB-INF/lib/jbpm-flow-5.3.1.BRMS* $JBOSS_BACK
mv $JBOSS_HOME/$JBOSS_CONFIG/jboss-brms.war/WEB-INF/lib/jbpm-flow-builder-5.3.1.BRMS.jar $JBOSS_BACK
mv $JBOSS_HOME/$JBOSS_CONFIG/jboss-brms.war/WEB-INF/lib/knowledge-api-5.3.1.BRMS.jar $JBOSS_BACK
mv $JBOSS_HOME/$JBOSS_CONFIG/jboss-brms.war/WEB-INF/lib/mvel2-2.1.3.Final.jar $JBOSS_BACK
mv $JBOSS_HOME/$JBOSS_CONFIG/jboss-brms.war/WEB-INF/lib/webdav-servlet-2.0.jar $JBOSS_BACK
mv $JBOSS_HOME/$JBOSS_CONFIG/jboss-brms.war/WEB-INF/lib/xmlschema-core-2.0.2.jar $JBOSS_BACK
mv $JBOSS_HOME/$JBOSS_CONFIG/jboss-brms.war/WEB-INF/web.xml $JBOSS_BACK
mv $JBOSS_HOME/$JBOSS_CONFIG/jboss-brms.war/WEB-INF/lib/protobuf-java-2.4.1.jar $JBOSS_BACK

cp $PATCH_DIR/jbpm-bpmn2-emfextmodel-5.3.1.BRMS-P02.jar $JBOSS_HOME/$JBOSS_CONFIG/designer.war/WEB-INF/lib/
cp $PATCH_DIR/jbpm-workitems-5.3.1.BRMS-P02.jar $JBOSS_HOME/$JBOSS_CONFIG/designer.war/WEB-INF/lib/
cp $PATCH_DIR/knowledge-api-5.3.1.BRMS-P02.jar $JBOSS_HOME/$JBOSS_CONFIG/designer.war/WEB-INF/lib/


cp $PATCH_DIR/drools-decisiontables-5.3.1.BRMS-P02.jar $JBOSS_HOME/$JBOSS_CONFIG/jboss-brms.war/WEB-INF/lib/
cp $PATCH_DIR/drools-templates-5.3.1.BRMS-P02.jar $JBOSS_HOME/$JBOSS_CONFIG/jboss-brms.war/WEB-INF/lib/
cp $PATCH_DIR/drools-verifier-5.3.1.BRMS-P02.jar  $JBOSS_HOME/$JBOSS_CONFIG/jboss-brms.war/WEB-INF/lib/
cp $PATCH_DIR/knowledge-api-5.3.1.BRMS-P02.jar $JBOSS_HOME/$JBOSS_CONFIG/jboss-brms.war/WEB-INF/lib/

cp -r $PATCH_DIR/designer.war/* $JBOSS_HOME/$JBOSS_CONFIG/designer.war/
cp -r $PATCH_DIR/jboss-brms.war/* $JBOSS_HOME/$JBOSS_CONFIG/jboss-brms.war/

chown -R jboss:jboss $JBOSS_HOME

#You will need to edit the $JBOSS_HOME/$JBOSS_CONFIG/designer.war/profiles/jbpm.xml
#  The credentials to log into guvnor have been overwritten to admin:admin
#  You need to change them to valid credentials to log into guvnor with
