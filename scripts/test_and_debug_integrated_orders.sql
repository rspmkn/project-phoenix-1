---- CREDENTIAL PROPERTIES ----

-- list all credential property types
select * from credentialpropertytype cpt order by upper(cpt.name) 

-- list properties for credential (by credential name)
select cxpt.id, cpt.name, cxpt.VALUE, cpt.DEFAULTVALUE 
from credential_propertytype cxpt
	join credentialpropertytype cpt on cpt.id = cxpt.TYPE_ID
	join credential c on c.id = cxpt.cred_id
where upper(c.name) like '%NGC_STAPLES_PUNCHOUT%'
order by upper(cpt.name)

-- add credential property
-- insert into credential_propertytype cxpt (id, version, cred_id, type_id, value) values (hibernate_sequence.nextval, 0, 1000209235, 1019821490, 'ngc@apdmarketplace.com');

-- update credential property for existing credential (by credential name and name of property)
update credential_propertytype cxpt 
set cxpt.value = 'Y' 
where cxpt.type_id = (select cpt.id from credentialpropertytype cpt where upper(cpt.name) = 'CHARGE SALES TAX') 
	and cxpt.cred_id = (select c.id from credential c where upper(c.name) like '%NGC_STAPLES_PUNCHOUT%'); 

	
---- CASHOUT PAGE FIELD MAPPINGS ----

-- list cashout page field mappings for credential (by credential name)
select c.id "cred id", p.id as "page id", 
	c.name as "credential", 
	cpxf.inboundmapping as "inbound", 
	f.name as "field", 
	mm.name as "outbound", 
	cpxf.defaultvalue as "default value"
from cashoutpagexfield cpxf 
	join field f on f.ID = cpxf.FIELD_ID
	join fieldtype ft on ft.id = f.FIELDTYPE_ID
	join cashoutpage p on p.ID = cpxf.PAGE_ID
	join credential c on c.CASHOUTPAGE_ID = p.ID
	left join messagemapping mm on mm.ID = cpxf.MAPPING_ID
where upper(c.name) like '%NGC_STAPLES_PUNCHOUT%'
	
-- update cashout page field mapping for credential cashout page (by credential name)
update cashoutpagexfield cpxf 
set cpxf.field_id = (select f.id from field f where f.name = 'same field or some other field'),
	cpxf.mapping_id = (select m.id from messagemapping m where m.name = 'same mapping or some other mapping') 
where cpxf.field_id = (select f.id from field f where f.name = 'requestername')
	and cpxf.mapping_id = (select m.id from messagemapping m where m.name = 'requestername') 
	and cpxf.page_id = (select c.cashoutpage_id from credential c where upper(c.name) like '%NGC_STAPLES_PUNCHOUT%');
	
-- add cashout page field mapping to existing credential cashout page
insert into cashoutpagexfield (id, required, updatable, version, page_id, field_id, mapping_id, defaultvalue) 
	select hibernate_sequence.nextval, 1, 1, 0, 1019762827, 1000352013, 1000351978, 'speed' from dual;
	--(select c.cashoutpage_id from credential c where upper(c.name) like '%NGC_STAPLES_PUNCHOUT%'),
	--(select f.id from field f where f.name = 'fulfillment'), 
	--(select m.id from messagemapping m where m.name = 'fulfillment')) from dual;

	
---- CUSTOMER ORDER SHIPTO/BILLTO AND PAYMENT INFO ----

-- show payment info for account (by APD customer account ID)
select * from paymentinformation p
	join paymenttype pt on pt.ID = p.paymenttype_id 
	left join cardinformation ci on ci.id = p.card_id
where p.id in 
	(select a.paymentinformation_id 
	from account a 
	where a.apdassignedaccountid like '%NGCO%')
	
-- show order details for po number
select * from customerorder co
	join ponumber_customerorder poco on poco.ORDERS_ID = co.id
	join ponumber po on po.ID = poco.PONUMBERS_ID
	where po.VALUE = 'APD901002300'
		
-- show payment info for order (by order id)
select * from paymentinformation pi 
	join paymenttype pt on pt.ID = pi.paymenttype_id 
	left join cardinformation ci on ci.id = pi.card_id
where pi.id in 
	(select co.paymentinformation_id 
	from customerorder co 
	where co.id = 1030303030304044)

-- show billto address for order (by order id)
select * from address a
	left join miscshipto m on m.id = a.miscshipto_id
	left join addressType at on at.id = a.type_id
where a.id in
	(select pi.BILLINGADDRESS_ID 
	from paymentinformation pi
	where pi.ID in 
		(select co.PAYMENTINFORMATION_ID 
		from customerorder co 
		where co.id = 1030303030304044))
			
-- show shipto for order (by order id)
select * from address a
	left join miscshipto m on m.id = a.miscshipto_id
	left join addressType at on at.id = a.type_id
where a.id in
	(select co.address_id 
	from customerorder co 
	where co.id = 1030303030303857)