INSERT INTO CREDENTIALPROPERTYTYPE (ID, NAME, VERSION) 
	VALUES (hibernate_sequence.nextval, 'information', 0);
INSERT INTO CREDENTIALPROPERTYTYPE (ID, NAME, VERSION) 
	VALUES (hibernate_sequence.nextval, 'type', 0);
INSERT INTO CREDENTIALPROPERTYTYPE (ID, NAME, VERSION) 
	VALUES (hibernate_sequence.nextval, 'session timeout', 0);
INSERT INTO CREDENTIALPROPERTYTYPE (ID, NAME, VERSION) 
	VALUES (hibernate_sequence.nextval, 'return button text', 0);
INSERT INTO CREDENTIALPROPERTYTYPE (ID, NAME, VERSION) 
	VALUES (hibernate_sequence.nextval, 'deployment', 0);
INSERT INTO CREDENTIALPROPERTYTYPE (ID, NAME, VERSION) 
	VALUES (hibernate_sequence.nextval, 'vendor path prefix', 0);
INSERT INTO CREDENTIALPROPERTYTYPE (ID, NAME, VERSION) 
	VALUES (hibernate_sequence.nextval, 'button label', 0);
INSERT INTO CREDENTIALPROPERTYTYPE (ID, NAME, VERSION) 
	VALUES (hibernate_sequence.nextval, 'AR GL account', 0);
INSERT INTO CREDENTIALPROPERTYTYPE (ID, NAME, VERSION) 
	VALUES (hibernate_sequence.nextval, 'AR subaccount', 0);
INSERT INTO CREDENTIALPROPERTYTYPE (ID, NAME, VERSION) 
	VALUES (hibernate_sequence.nextval, 'contract number', 0);
INSERT INTO CREDENTIALPROPERTYTYPE (ID, NAME, VERSION) 
	VALUES (hibernate_sequence.nextval, 'xCBL buyer MPID', 0);
INSERT INTO CREDENTIALPROPERTYTYPE (ID, NAME, VERSION) 
	VALUES (hibernate_sequence.nextval, 'xCBL seller MPID', 0);
INSERT INTO CREDENTIALPROPERTYTYPE (ID, NAME, VERSION) 
	VALUES (hibernate_sequence.nextval, 'shop again URL base', 0);
INSERT INTO CREDENTIALPROPERTYTYPE (ID, NAME, VERSION) 
	VALUES (hibernate_sequence.nextval, 'HTTP root', 0);
INSERT INTO CREDENTIALPROPERTYTYPE (ID, NAME, VERSION) 
	VALUES (hibernate_sequence.nextval, 'HTTPS root', 0);
INSERT INTO CREDENTIALPROPERTYTYPE (ID, NAME, VERSION) 
	VALUES (hibernate_sequence.nextval, 'HTTP path', 0);
INSERT INTO CREDENTIALPROPERTYTYPE (ID, NAME, VERSION) 
	VALUES (hibernate_sequence.nextval, 'HTTPS path', 0);
INSERT INTO CREDENTIALPROPERTYTYPE (ID, NAME, VERSION) 
	VALUES (hibernate_sequence.nextval, 'image: header', 0);
INSERT INTO CREDENTIALPROPERTYTYPE (ID, NAME, VERSION) 
	VALUES (hibernate_sequence.nextval, 'login frame template', 0);
INSERT INTO CREDENTIALPROPERTYTYPE (ID, NAME, VERSION) 
	VALUES (hibernate_sequence.nextval, 'credit card processor', 0);
INSERT INTO CREDENTIALPROPERTYTYPE (ID, NAME, VERSION) 
	VALUES (hibernate_sequence.nextval, 'batch credit card transactions', 0);
INSERT INTO CREDENTIALPROPERTYTYPE (ID, NAME, VERSION) 
	VALUES (hibernate_sequence.nextval, 'credit card merchant number', 0);
INSERT INTO CREDENTIALPROPERTYTYPE (ID, NAME, VERSION) 
	VALUES (hibernate_sequence.nextval, 'vendor connection login', 0);
INSERT INTO CREDENTIALPROPERTYTYPE (ID, NAME, VERSION) 
	VALUES (hibernate_sequence.nextval, 'vendor connection password', 0);
INSERT INTO CREDENTIALPROPERTYTYPE (ID, NAME, VERSION) 
	VALUES (hibernate_sequence.nextval, 'vendor connection email', 0);
INSERT INTO CREDENTIALPROPERTYTYPE (ID, NAME, VERSION) 
	VALUES (hibernate_sequence.nextval, 'email quotes as saved list', 0);
INSERT INTO CREDENTIALPROPERTYTYPE (ID, NAME, VERSION) 
	VALUES (hibernate_sequence.nextval, 'merchandise discount', 0);
INSERT INTO CREDENTIALPROPERTYTYPE (ID, NAME, VERSION) 
	VALUES (hibernate_sequence.nextval, 'discount error allowance', 0);
INSERT INTO CREDENTIALPROPERTYTYPE (ID, NAME, VERSION) 
	VALUES (hibernate_sequence.nextval, 'cost of goods', 0);
INSERT INTO CREDENTIALPROPERTYTYPE (ID, NAME, VERSION) 
	VALUES (hibernate_sequence.nextval, 'cc proc perc of order', 0);
INSERT INTO CREDENTIALPROPERTYTYPE (ID, NAME, VERSION) 
	VALUES (hibernate_sequence.nextval, 'cc proc flat per order', 0);
INSERT INTO CREDENTIALPROPERTYTYPE (ID, NAME, VERSION) 
	VALUES (hibernate_sequence.nextval, 'rev.net proc perc of order', 0);
INSERT INTO CREDENTIALPROPERTYTYPE (ID, NAME, VERSION) 
	VALUES (hibernate_sequence.nextval, 'send PDF attachment', 0);
INSERT INTO CREDENTIALPROPERTYTYPE (ID, NAME, VERSION) 
	VALUES (hibernate_sequence.nextval, 'send TXT attachment', 0);
INSERT INTO CREDENTIALPROPERTYTYPE (ID, NAME, VERSION) 
	VALUES (hibernate_sequence.nextval, 'send buyer email to vendor', 0);
INSERT INTO CREDENTIALPROPERTYTYPE (ID, NAME, VERSION) 
	VALUES (hibernate_sequence.nextval, 'default shipto email contact', 0);
INSERT INTO CREDENTIALPROPERTYTYPE (ID, NAME, VERSION) 
	VALUES (hibernate_sequence.nextval, 'send declines to', 0);
INSERT INTO CREDENTIALPROPERTYTYPE (ID, NAME, VERSION) 
	VALUES (hibernate_sequence.nextval, 'email template', 0);
INSERT INTO CREDENTIALPROPERTYTYPE (ID, NAME, VERSION) 
	VALUES (hibernate_sequence.nextval, 'SKU on emails', 0);
INSERT INTO CREDENTIALPROPERTYTYPE (ID, NAME, VERSION) 
	VALUES (hibernate_sequence.nextval, 'allow additional emails', 0);
INSERT INTO CREDENTIALPROPERTYTYPE (ID, NAME, VERSION) 
	VALUES (hibernate_sequence.nextval, 'cc payments allowed types', 0);
INSERT INTO CREDENTIALPROPERTYTYPE (ID, NAME, VERSION) 
	VALUES (hibernate_sequence.nextval, 'invoice payments', 0);
INSERT INTO CREDENTIALPROPERTYTYPE (ID, NAME, VERSION) 
	VALUES (hibernate_sequence.nextval, 'invoice number required', 0);
INSERT INTO CREDENTIALPROPERTYTYPE (ID, NAME, VERSION) 
	VALUES (hibernate_sequence.nextval, 'use billing PO for invoices', 0);
INSERT INTO CREDENTIALPROPERTYTYPE (ID, NAME, VERSION) 
	VALUES (hibernate_sequence.nextval, 'use default credit card number', 0);
INSERT INTO CREDENTIALPROPERTYTYPE (ID, NAME, VERSION) 
	VALUES (hibernate_sequence.nextval, 'charge sales tax', 0);
INSERT INTO CREDENTIALPROPERTYTYPE (ID, NAME, VERSION) 
	VALUES (hibernate_sequence.nextval, 'state exemptions for sales tax', 0);
INSERT INTO CREDENTIALPROPERTYTYPE (ID, NAME, VERSION) 
	VALUES (hibernate_sequence.nextval, 'zip code exceptions for sales tax', 0);
INSERT INTO CREDENTIALPROPERTYTYPE (ID, NAME, VERSION) 
	VALUES (hibernate_sequence.nextval, 'use alternate shiptos', 0);
INSERT INTO CREDENTIALPROPERTYTYPE (ID, NAME, VERSION) 
	VALUES (hibernate_sequence.nextval, 'use alternate billtos', 0);
INSERT INTO CREDENTIALPROPERTYTYPE (ID, NAME, VERSION) 
	VALUES (hibernate_sequence.nextval, 'require authorized shiptos', 0);
INSERT INTO CREDENTIALPROPERTYTYPE (ID, NAME, VERSION) 
	VALUES (hibernate_sequence.nextval, 'blank address 2/3 when authorized', 0);
INSERT INTO CREDENTIALPROPERTYTYPE (ID, NAME, VERSION) 
	VALUES (hibernate_sequence.nextval, 'allow rejected items to ship', 0);
INSERT INTO CREDENTIALPROPERTYTYPE (ID, NAME, VERSION) 
	VALUES (hibernate_sequence.nextval, 'use last shipto', 0);
INSERT INTO CREDENTIALPROPERTYTYPE (ID, NAME, VERSION) 
	VALUES (hibernate_sequence.nextval, 'use common units of measure', 0);
INSERT INTO CREDENTIALPROPERTYTYPE (ID, NAME, VERSION) 
	VALUES (hibernate_sequence.nextval, 'use ISO units of measure', 0);
INSERT INTO CREDENTIALPROPERTYTYPE (ID, NAME, VERSION) 
	VALUES (hibernate_sequence.nextval, 'admin orders: pick supplier', 0);
INSERT INTO CREDENTIALPROPERTYTYPE (ID, NAME, VERSION) 
	VALUES (hibernate_sequence.nextval, 'protect shiptos', 0);
INSERT INTO CREDENTIALPROPERTYTYPE (ID, NAME, VERSION) 
	VALUES (hibernate_sequence.nextval, 'concatenate item number to description', 0);
INSERT INTO CREDENTIALPROPERTYTYPE (ID, NAME, VERSION) 
	VALUES (hibernate_sequence.nextval, 'allow attachments to orders', 0);
INSERT INTO CREDENTIALPROPERTYTYPE (ID, NAME, VERSION) 
	VALUES (hibernate_sequence.nextval, 'unscheduled service fee', 0);
INSERT INTO CREDENTIALPROPERTYTYPE (ID, NAME, VERSION) 
	VALUES (hibernate_sequence.nextval, 'days to keep order on hold', 0);
INSERT INTO CREDENTIALPROPERTYTYPE (ID, NAME, VERSION) 
	VALUES (hibernate_sequence.nextval, 'default order history timeframe', 0);
INSERT INTO CREDENTIALPROPERTYTYPE (ID, NAME, VERSION) 
	VALUES (hibernate_sequence.nextval, 'default address when adding user', 0);
INSERT INTO CREDENTIALPROPERTYTYPE (ID, NAME, VERSION) 
	VALUES (hibernate_sequence.nextval, 'allow PO editing', 0);
INSERT INTO CREDENTIALPROPERTYTYPE (ID, NAME, VERSION) 
	VALUES (hibernate_sequence.nextval, 'use order comments', 0);
INSERT INTO CREDENTIALPROPERTYTYPE (ID, NAME, VERSION) 
	VALUES (hibernate_sequence.nextval, 'retry refused order', 0);
INSERT INTO CREDENTIALPROPERTYTYPE (ID, NAME, VERSION) 
	VALUES (hibernate_sequence.nextval, 'bulletin button text', 0);
INSERT INTO CREDENTIALPROPERTYTYPE (ID, NAME, VERSION) 
	VALUES (hibernate_sequence.nextval, 'spending limit message', 0);
INSERT INTO CREDENTIALPROPERTYTYPE (ID, NAME, VERSION) 
	VALUES (hibernate_sequence.nextval, 'EDI: ISA sender qualifier', 0);
INSERT INTO CREDENTIALPROPERTYTYPE (ID, NAME, VERSION) 
	VALUES (hibernate_sequence.nextval, 'EDI: ISA sender ID', 0);
INSERT INTO CREDENTIALPROPERTYTYPE (ID, NAME, VERSION) 
	VALUES (hibernate_sequence.nextval, 'EDI: ISA receiver qualifier', 0);
INSERT INTO CREDENTIALPROPERTYTYPE (ID, NAME, VERSION) 
	VALUES (hibernate_sequence.nextval, 'EDI: ISA receiver ID', 0);
INSERT INTO CREDENTIALPROPERTYTYPE (ID, NAME, VERSION) 
	VALUES (hibernate_sequence.nextval, 'EDI: GS sender ID', 0);
INSERT INTO CREDENTIALPROPERTYTYPE (ID, NAME, VERSION) 
	VALUES (hibernate_sequence.nextval, 'EDI: GS receiver ID', 0);
INSERT INTO CREDENTIALPROPERTYTYPE (ID, NAME, VERSION) 
	VALUES (hibernate_sequence.nextval, 'EDI: field separator', 0);
INSERT INTO CREDENTIALPROPERTYTYPE (ID, NAME, VERSION) 
	VALUES (hibernate_sequence.nextval, 'EDI: record separator', 0);
INSERT INTO CREDENTIALPROPERTYTYPE (ID, NAME, VERSION) 
	VALUES (hibernate_sequence.nextval, 'auto PO evaluation', 0);
INSERT INTO CREDENTIALPROPERTYTYPE (ID, NAME, VERSION) 
	VALUES (hibernate_sequence.nextval, 'solomon vendor ID', 0);
INSERT INTO CREDENTIALPROPERTYTYPE (ID, NAME, VERSION) 
	VALUES (hibernate_sequence.nextval, 'default invoice terms', 0);
INSERT INTO CREDENTIALPROPERTYTYPE (ID, NAME, VERSION) 
	VALUES (hibernate_sequence.nextval, 'allowed invoice terms', 0);
INSERT INTO CREDENTIALPROPERTYTYPE (ID, NAME, VERSION) 
	VALUES (hibernate_sequence.nextval, 'bank ABA number', 0);
INSERT INTO CREDENTIALPROPERTYTYPE (ID, NAME, VERSION) 
	VALUES (hibernate_sequence.nextval, 'bank account number', 0);
INSERT INTO CREDENTIALPROPERTYTYPE (ID, NAME, VERSION) 
	VALUES (hibernate_sequence.nextval, 'billto heading title', 0);
INSERT INTO CREDENTIALPROPERTYTYPE (ID, NAME, VERSION) 
	VALUES (hibernate_sequence.nextval, 'shipto heading title', 0);
INSERT INTO CREDENTIALPROPERTYTYPE (ID, NAME, VERSION) 
	VALUES (hibernate_sequence.nextval, 'multiple billto pull down heading', 0);
INSERT INTO CREDENTIALPROPERTYTYPE (ID, NAME, VERSION) 
	VALUES (hibernate_sequence.nextval, 'multiple shipto pull down heading', 0);
INSERT INTO CREDENTIALPROPERTYTYPE (ID, NAME, VERSION) 
	VALUES (hibernate_sequence.nextval, 'notes/comments', 0);
INSERT INTO CREDENTIALPROPERTYTYPE (ID, NAME, VERSION) 
	VALUES (hibernate_sequence.nextval, 'shipping rate', 0);
INSERT INTO CREDENTIALPROPERTYTYPE (ID, NAME, VERSION) 
	VALUES (hibernate_sequence.nextval, 'zip code for stock check', 0);
INSERT INTO CREDENTIALPROPERTYTYPE (ID, NAME, VERSION) 
	VALUES (hibernate_sequence.nextval, 'cc preauth strategy (full, one, none)', 0);
INSERT INTO CREDENTIALPROPERTYTYPE (ID, NAME, VERSION) 
	VALUES (hibernate_sequence.nextval, 'US Account #', 0);
INSERT INTO CREDENTIALPROPERTYTYPE (ID, NAME, VERSION) 
	VALUES (hibernate_sequence.nextval, 'APD sales person', 0);
INSERT INTO CREDENTIALPROPERTYTYPE (ID, NAME, VERSION) 
	VALUES (hibernate_sequence.nextval, 'check all facilities (ADOT)', 0);
INSERT INTO CREDENTIALPROPERTYTYPE (ID, NAME, VERSION) 
	VALUES (hibernate_sequence.nextval, 'cost-price discrepancy threshold', 0);
INSERT INTO CREDENTIALPROPERTYTYPE(ID,NAME,VERSION)
	VALUES(hibernate_sequence.nextval,'needs usps/ussco workflow',0);
INSERT INTO CREDENTIALPROPERTYTYPE(ID,NAME,VERSION)
	VALUES(hibernate_sequence.nextval,'allowed country codes',0);
INSERT INTO CREDENTIALPROPERTYTYPE(ID,NAME,VERSION)
	VALUES(hibernate_sequence.nextval,'allow military zip codes',0);
INSERT INTO CREDENTIALPROPERTYTYPE(ID,NAME,VERSION)
	VALUES(hibernate_sequence.nextval,'DUNS number (for ariba)',0);
INSERT INTO CREDENTIALPROPERTYTYPE(ID,NAME,VERSION)
	VALUES(hibernate_sequence.nextval,'punchout URL',0);
INSERT INTO CREDENTIALPROPERTYTYPE(ID,NAME,VERSION)
	VALUES(hibernate_sequence.nextval,'punchoutexit URL',0);
INSERT INTO CREDENTIALPROPERTYTYPE(ID,NAME,VERSION)
	VALUES(hibernate_sequence.nextval,'orderrequest URL',0);
INSERT INTO CREDENTIALPROPERTYTYPE(ID,NAME,VERSION)
	VALUES(hibernate_sequence.nextval,'punch frame template',0);
INSERT INTO CREDENTIALPROPERTYTYPE(ID,NAME,VERSION)
	VALUES(hibernate_sequence.nextval,'from credential',0);
INSERT INTO CREDENTIALPROPERTYTYPE(ID,NAME,VERSION)
	VALUES(hibernate_sequence.nextval,'from punch identity',0);
INSERT INTO CREDENTIALPROPERTYTYPE(ID,NAME,VERSION)
	VALUES(hibernate_sequence.nextval,'from order identity',0);
INSERT INTO CREDENTIALPROPERTYTYPE(ID,NAME,VERSION)
	VALUES(hibernate_sequence.nextval,'to credential',0);
INSERT INTO CREDENTIALPROPERTYTYPE(ID,NAME,VERSION)
	VALUES(hibernate_sequence.nextval,'to punch identity',0);
INSERT INTO CREDENTIALPROPERTYTYPE(ID,NAME,VERSION)
	VALUES(hibernate_sequence.nextval,'to order identity',0);
INSERT INTO CREDENTIALPROPERTYTYPE(ID,NAME,VERSION)
	VALUES(hibernate_sequence.nextval,'sender credential',0);
INSERT INTO CREDENTIALPROPERTYTYPE(ID,NAME,VERSION)
	VALUES(hibernate_sequence.nextval,'sender credential punch user',0);
INSERT INTO CREDENTIALPROPERTYTYPE(ID,NAME,VERSION)
	VALUES(hibernate_sequence.nextval,'sender credential punch password',0);
INSERT INTO CREDENTIALPROPERTYTYPE(ID,NAME,VERSION)
	VALUES(hibernate_sequence.nextval,'sender credential order user',0);
INSERT INTO CREDENTIALPROPERTYTYPE(ID,NAME,VERSION)
	VALUES(hibernate_sequence.nextval,'sender credential order password',0);
INSERT INTO CREDENTIALPROPERTYTYPE(ID,NAME,VERSION)
	VALUES(hibernate_sequence.nextval,'cc payments',0);
INSERT INTO CREDENTIALPROPERTYTYPE(ID,NAME,VERSION)
	VALUES(hibernate_sequence.nextval,'minimum order amount required',0);
--note: The following property only affects the target data binding for 
-- outbound order requests.  Currently the shipto data for each line item on the same 
-- purchase order must be identical (data model supports only one shipto per order at the moment)
INSERT INTO CREDENTIALPROPERTYTYPE(ID,NAME,VERSION)
	VALUES(hibernate_sequence.nextval,'set line item shipto on outbound order',0);
INSERT INTO CREDENTIALPROPERTYTYPE(ID,NAME,VERSION)
	VALUES(hibernate_sequence.nextval,'prepend order item description with line #',0); 
INSERT INTO CREDENTIALPROPERTYTYPE(ID,NAME,VERSION)
	VALUES(hibernate_sequence.nextval,'require authorized shiptos',0);
INSERT INTO CREDENTIALPROPERTYTYPE(ID,NAME,VERSION)
	VALUES(hibernate_sequence.nextval,'sku on shopping cart',0);
INSERT INTO CREDENTIALPROPERTYTYPE(ID,NAME,VERSION)
	VALUES(hibernate_sequence.nextval,'show search results summary',0);
INSERT INTO CREDENTIALPROPERTYTYPE(ID,NAME,VERSION,DEFAULTVALUE) 
	VALUES(hibernate_sequence.nextval,'banner file name',0,'');
