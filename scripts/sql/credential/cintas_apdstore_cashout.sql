INSERT INTO CASHOUTPAGE (ID, CANCELDATE, DELIVERYDATE, EXPECTEDDATE, HIDECCNUMBER, HIGHERPRICENOTICE, MULTIPLESHIPTO, NAME, SERVICEDATE, UPDATEUSERINFORMATION, VERSION)
	VALUES (hibernate_sequence.nextval, 0, 0, 0, 1, 0, 0, 'Cintas/APDStore page', 0, 1, 0);
UPDATE CREDENTIAL c SET c.CASHOUTPAGE_ID=(SELECT page.id FROM CASHOUTPAGE page WHERE page.name LIKE 
	'Cintas/APDStore page') WHERE c.ID=(SELECT cred.id FROM CREDENTIAL cred WHERE cred.name LIKE 'Cintas/APDStore');
	
INSERT INTO CASHOUTPAGEXFIELD (ID, LABEL, DEFAULTVALUE, REQUIRED, VERSION, FIELD_ID, PAGE_ID, UPDATABLE) 
	VALUES (hibernate_sequence.nextval, 'Enter invoice number', '', 0, 0, (SELECT f.id FROM FIELD f WHERE f.name LIKE 
	'custom po number'), (SELECT p.id FROM CASHOUTPAGE p WHERE p.name LIKE 'Cintas/APDStore page'), 0); 
	
INSERT INTO CASHOUTPAGEXFIELD (ID, LABEL, DEFAULTVALUE, REQUIRED, VERSION, FIELD_ID, PAGE_ID, UPDATABLE) 
	VALUES (hibernate_sequence.nextval, 'Est Shipping/Handling **', '', 0, 0, (SELECT f.id FROM FIELD f WHERE f.name LIKE 
	'shipping cost'), (SELECT p.id FROM CASHOUTPAGE p WHERE p.name LIKE 'Cintas/APDStore page'), 1); 

INSERT INTO CASHOUTPAGEXFIELD (ID, LABEL, DEFAULTVALUE, REQUIRED, VERSION, FIELD_ID, PAGE_ID, UPDATABLE) 
	VALUES (hibernate_sequence.nextval, 'select address', '', 0, 0, (SELECT f.id FROM FIELD f WHERE f.name LIKE 
	'existing shipto address selection'), (SELECT p.id FROM CASHOUTPAGE p WHERE p.name LIKE 'Cintas/APDStore page'), 1); 	

INSERT INTO CASHOUTPAGEXFIELD (ID, LABEL, DEFAULTVALUE, REQUIRED, VERSION, FIELD_ID, PAGE_ID, UPDATABLE) 
	VALUES (hibernate_sequence.nextval, 'Name', '', 1, 0, (SELECT f.id FROM FIELD f WHERE f.name LIKE 
	'requester name'), (SELECT p.id FROM CASHOUTPAGE p WHERE p.name LIKE 'Cintas/APDStore page'), 1); 
