UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_item';

INSERT INTO ITEM (ID, LASTMODIFIED, DESCRIPTION, NAME, SEARCHTERMS, VERSION, ABILITYONESUBSTITUTE_ID, HIERARCHYNODE_ID, ITEMCATEGORY_ID, MANUFACTURER_ID, REPLACEMENT_ID, VENDORCATALOG_ID, UNITOFMEASURE_ID)
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_item'), '1-Feb-2014', 'OEM ink tank for Canon® PIXMA iP3300, iP3500, iP4200, iP4300, iP4500, iP5200, iP5200R, iP6600D. iP6700D, MP500, MP510, MP520, MP530, MP610, MP530, MP600, MP800, MP800R, MP810, MP830, MP850, MP950, MP960, MP970, MX700, Pro9000, Pro9000MKII.',
	'Canon® 0620B015 (CLI-8) Eight-Color Multipack Ink Tank', '"Printer Supplies" Ink Inks Toner Toners Black Cyan Green Magenta "Photo Cyan" "Photo Magenta" MP600 MP800 MP800R MP810 MP830 MP850 MP950 MP960 MP970',
	0, null, (SELECT h.ID FROM HIERARCHYNODE h WHERE h.DESCRIPTION LIKE 'Inkjet Printer Supplies'), null, 
	(SELECT m.ID FROM MANUFACTURER m where m.name='AT-A-GLANCE'), null, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_vendor_catalog'),
	(SELECT u.ID FROM UNITOFMEASURE u WHERE u.NAME = 'EA'));
	
INSERT INTO ITEMXITEMPROPERTYTYPE (ID, VALUE, VERSION, TYPE_ID, ITEM_ID)
	VALUES (hibernate_sequence.nextval, 'available', 0, (SELECT t.ID FROM ITEMPROPERTYTYPE t WHERE t.NAME LIKE 'status'), (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_item'));
	
INSERT INTO ITEMXITEMPROPERTYTYPE (ID, VALUE, VERSION, TYPE_ID, ITEM_ID)
	VALUES (hibernate_sequence.nextval, '37.39', 0, (SELECT t.ID FROM ITEMPROPERTYTYPE t WHERE t.NAME LIKE 'list price'), (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_item'));
	
INSERT INTO SKU (ID, VALUE, VERSION, ITEM_ID, TYPE_ID)
	VALUES (hibernate_sequence.nextval, 'CNM0620B015', 0, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_item'),
	(SELECT sType.ID FROM SKUTYPE sType WHERE sType.NAME LIKE 'APD'));

--/* Assembly_Code classification */
	
UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_codetype';
UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_classcode';
UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_itemclass';

INSERT INTO CODETYPE (ID, NAME, VERSION) VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_codetype'), 'N', 0);
	
INSERT INTO CLASSIFICATIONCODE (ID, VERSION, CODETYPE_ID) VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_classcode'), 
	0, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_codetype'));

INSERT INTO ITEMCLASSIFICATION (ID, VERSION, CLASSIFICATIONCODE_ID, ITEMCLASSIFICATIONIMAGE_ID, ITEMCLASSTYPE_ID)
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemclass'), 0, 
	(SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_classcode'), null,
	(SELECT t.ID FROM ITEMCLASSIFICATIONTYPE t WHERE t.NAME LIKE 'Assembly_Code'));

INSERT INTO ITEMCLASSIFICATION_ITEM (ITEMCLASSIFICATIONS_ID, ITEMS_ID)
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemclass'), (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_item'));

--/* Recycle_Indicator classification */
	
UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_codetype';
UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_classcode';
UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_itemclass';

INSERT INTO CODETYPE (ID, NAME, VERSION) VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_codetype'), 'N', 0);

INSERT INTO CLASSIFICATIONCODE (ID, VERSION, CODETYPE_ID) VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_classcode'), 
	0, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_codetype'));

INSERT INTO ITEMCLASSIFICATION (ID, VERSION, CLASSIFICATIONCODE_ID, ITEMCLASSIFICATIONIMAGE_ID, ITEMCLASSTYPE_ID)
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemclass'), 0, 
	(SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_classcode'), null,
	(SELECT t.ID FROM ITEMCLASSIFICATIONTYPE t WHERE t.NAME LIKE 'Recycle_Indicator'));
	
INSERT INTO ITEMCLASSIFICATION_ITEM (ITEMCLASSIFICATIONS_ID, ITEMS_ID)
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemclass'), (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_item'));

--/* EPACPGCompliant_Code classification */
	
UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_codetype';
UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_classcode';
UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_itemclass';

INSERT INTO CODETYPE (ID, NAME, VERSION) VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_codetype'), 'Information Not Provided By Manufacturer', 0);

INSERT INTO CLASSIFICATIONCODE (ID, VERSION, CODETYPE_ID) VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_classcode'), 
	0, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_codetype'));

INSERT INTO ITEMCLASSIFICATION (ID, VERSION, CLASSIFICATIONCODE_ID, ITEMCLASSIFICATIONIMAGE_ID, ITEMCLASSTYPE_ID)
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemclass'), 0, 
	(SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_classcode'), null,
	(SELECT t.ID FROM ITEMCLASSIFICATIONTYPE t WHERE t.NAME LIKE 'EPACPGCompliant_Code'));

INSERT INTO ITEMCLASSIFICATION_ITEM (ITEMCLASSIFICATIONS_ID, ITEMS_ID)
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemclass'), (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_item'));

--/* MSDS_Indicator classification */
	
UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_codetype';
UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_classcode';
UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_itemclass';

INSERT INTO CODETYPE (ID, NAME, VERSION) VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_codetype'), 'N', 0);

INSERT INTO CLASSIFICATIONCODE (ID, VERSION, CODETYPE_ID) VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_classcode'), 
	0, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_codetype'));

INSERT INTO ITEMCLASSIFICATION (ID, VERSION, CLASSIFICATIONCODE_ID, ITEMCLASSIFICATIONIMAGE_ID, ITEMCLASSTYPE_ID)
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemclass'), 0, 
	(SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_classcode'), null,
	(SELECT t.ID FROM ITEMCLASSIFICATIONTYPE t WHERE t.NAME LIKE 'MSDS_Indicator'));
	
INSERT INTO ITEMCLASSIFICATION_ITEM (ITEMCLASSIFICATIONS_ID, ITEMS_ID)
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemclass'), (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_item'));

--/* UNSPSC classification */
	
UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_codetype';
UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_classcode';
UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_itemclass';

INSERT INTO CODETYPE (ID, NAME, VERSION) VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_codetype'), '44103105', 0);

INSERT INTO CLASSIFICATIONCODE (ID, VERSION, CODETYPE_ID) VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_classcode'), 
	0, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_codetype'));

INSERT INTO ITEMCLASSIFICATION (ID, VERSION, CLASSIFICATIONCODE_ID, ITEMCLASSIFICATIONIMAGE_ID, ITEMCLASSTYPE_ID)
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemclass'), 0, 
	(SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_classcode'), null,
	(SELECT t.ID FROM ITEMCLASSIFICATIONTYPE t WHERE t.NAME LIKE 'UNSPSC'));

INSERT INTO ITEMCLASSIFICATION_ITEM (ITEMCLASSIFICATIONS_ID, ITEMS_ID)
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemclass'), (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_item'));

--/* Global Product Type specification */

UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_itemspec';
UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_specproperty';

INSERT INTO ITEMSPECIFICATION (ID, ITEMSPECIFICATIONID, VERSION, ITEM_ID) 
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemspec'), 0, 0, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_item'));

INSERT INTO SPECIFICATIONPROPERTY (ID, VERSION, ITEMSPECIFICATION_ID, SEQUENCE, NAME, VALUE)
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_specproperty'), 0, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemspec'),
	1, 'Global Product Type', 'Print and Imaging Supplies-Ink/Ink Cartridges');
	
--/* Device Types specification */

UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_itemspec';
UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_specproperty';

INSERT INTO ITEMSPECIFICATION (ID, ITEMSPECIFICATIONID, VERSION, ITEM_ID) 
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemspec'), 0, 0, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_item'));

INSERT INTO SPECIFICATIONPROPERTY (ID, VERSION, ITEMSPECIFICATION_ID, SEQUENCE, NAME, VALUE)
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_specproperty'), 0, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemspec'),
	2, 'Device Types', 'Inkjet Printe');
	
--/* Color(s) specification */

UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_itemspec';
UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_specproperty';

INSERT INTO ITEMSPECIFICATION (ID, ITEMSPECIFICATIONID, VERSION, ITEM_ID) 
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemspec'), 0, 0, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_item'));

INSERT INTO SPECIFICATIONPROPERTY (ID, VERSION, ITEMSPECIFICATION_ID, SEQUENCE, NAME, VALUE)
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_specproperty'), 0, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemspec'),
	3, 'Color(s)', 'Black; Cyan; Green; Magenta; Photo Cyan; Photo Magenta; Red; Yellow');
	
--/* Supply Type specification */

UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_itemspec';
UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_specproperty';

INSERT INTO ITEMSPECIFICATION (ID, ITEMSPECIFICATIONID, VERSION, ITEM_ID) 
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemspec'), 0, 0, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_item'));

INSERT INTO SPECIFICATIONPROPERTY (ID, VERSION, ITEMSPECIFICATION_ID, SEQUENCE, NAME, VALUE)
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_specproperty'), 0, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemspec'),
	5, 'Supply Type', 'Ink Tank');
	
--/* OEM/Compatible specification */

UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_itemspec';
UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_specproperty';

INSERT INTO ITEMSPECIFICATION (ID, ITEMSPECIFICATIONID, VERSION, ITEM_ID) 
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemspec'), 0, 0, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_item'));

INSERT INTO SPECIFICATIONPROPERTY (ID, VERSION, ITEMSPECIFICATION_ID, SEQUENCE, NAME, VALUE)
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_specproperty'), 0, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemspec'),
	6, 'OEM/Compatible', 'OEM');
	
--/* Remanufactured specification */

UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_itemspec';
UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_specproperty';

INSERT INTO ITEMSPECIFICATION (ID, ITEMSPECIFICATIONID, VERSION, ITEM_ID) 
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemspec'), 0, 0, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_item'));

INSERT INTO SPECIFICATIONPROPERTY (ID, VERSION, ITEMSPECIFICATION_ID, SEQUENCE, NAME, VALUE)
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_specproperty'), 0, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemspec'),
	10, 'Remanufactured', 'No');
	
--/* Market Indicator (Cartridge Number) specification */

UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_itemspec';
UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_specproperty';

INSERT INTO ITEMSPECIFICATION (ID, ITEMSPECIFICATIONID, VERSION, ITEM_ID) 
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemspec'), 0, 0, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_item'));

INSERT INTO SPECIFICATIONPROPERTY (ID, VERSION, ITEMSPECIFICATION_ID, SEQUENCE, NAME, VALUE)
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_specproperty'), 0, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemspec'),
	18, 'Market Indicator (Cartridge Number)', 'CLI-8');
	
--/* Pre-Consumer Recycled Content Percent specification */

UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_itemspec';
UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_specproperty';

INSERT INTO ITEMSPECIFICATION (ID, ITEMSPECIFICATIONID, VERSION, ITEM_ID) 
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemspec'), 0, 0, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_item'));

INSERT INTO SPECIFICATIONPROPERTY (ID, VERSION, ITEMSPECIFICATION_ID, SEQUENCE, NAME, VALUE)
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_specproperty'), 0, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemspec'),
	85, 'Pre-Consumer Recycled Content Percent', '0%');
	
--/* Post-Consumer Recycled Content Percent specification */

UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_itemspec';
UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_specproperty';

INSERT INTO ITEMSPECIFICATION (ID, ITEMSPECIFICATIONID, VERSION, ITEM_ID) 
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemspec'), 0, 0, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_item'));

INSERT INTO SPECIFICATIONPROPERTY (ID, VERSION, ITEMSPECIFICATION_ID, SEQUENCE, NAME, VALUE)
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_specproperty'), 0, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemspec'),
	86, 'Post-Consumer Recycled Content Percent', '0%');
	
--/* Total Recycled Content Percent specification */

UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_itemspec';
UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_specproperty';

INSERT INTO ITEMSPECIFICATION (ID, ITEMSPECIFICATIONID, VERSION, ITEM_ID) 
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemspec'), 0, 0, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_item'));

INSERT INTO SPECIFICATIONPROPERTY (ID, VERSION, ITEMSPECIFICATION_ID, SEQUENCE, NAME, VALUE)
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_specproperty'), 0, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemspec'),
	87, 'Total Recycled Content Percent', '0%');
	
--/* Quantity specification */

UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_itemspec';
UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_specproperty';

INSERT INTO ITEMSPECIFICATION (ID, ITEMSPECIFICATIONID, VERSION, ITEM_ID) 
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemspec'), 0, 0, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_item'));

INSERT INTO SPECIFICATIONPROPERTY (ID, VERSION, ITEMSPECIFICATION_ID, SEQUENCE, NAME, VALUE)
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_specproperty'), 0, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemspec'),
	90, 'Quantity', '8 per pack');
	
--/* Special Features specification */

UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_itemspec';
UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_specproperty';

INSERT INTO ITEMSPECIFICATION (ID, ITEMSPECIFICATIONID, VERSION, ITEM_ID) 
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemspec'), 0, 0, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_item'));

INSERT INTO SPECIFICATIONPROPERTY (ID, VERSION, ITEMSPECIFICATION_ID, SEQUENCE, NAME, VALUE)
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_specproperty'), 0, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemspec'),
	91, 'Special Features', 'Value Pack');

INSERT INTO CATALOGXITEM (ID, LASTMODIFIED,  PRICE, VERSION, ITEM_ID, CATALOG_ID, SUBSTITUTEITEM_ID) 
	VALUES (hibernate_sequence.nextval, '1-Feb-2014', 10.99, 0, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_item'), (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_catalog'),
		(SELECT cxi.ID FROM CATALOGXITEM cxi, ITEM item WHERE cxi.ITEM_ID=item.ID AND item.NAME LIKE '%The Action Planner® Weekly Appointment Book%'));
