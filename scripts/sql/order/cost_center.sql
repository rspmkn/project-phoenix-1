INSERT INTO COSTCENTER (ID, DESCRIPTION, NAME, CCNUMBER, SPENDINGLIMIT, TOTALSPENT, VERSION, PERIOD_ID) 
	VALUES (hibernate_sequence.nextval, 'CSS Cost Center Description', 'CSS Cost Center', 'CSS1', 1000, 500, 0, 
	(select p.id from PERIODTYPE p where p.NAME like 'Monthly'));
INSERT INTO COSTCENTER (ID, DESCRIPTION, NAME, CCNUMBER, SPENDINGLIMIT, TOTALSPENT, VERSION, PERIOD_ID) 
	VALUES (hibernate_sequence.nextval, 'Cintas Cost Center Description', 'Cintas Cost Center', 'CINT1', 1820, 10000, 0, 
	(select p.id from PERIODTYPE p where p.NAME like 'Quarterly'));

INSERT INTO ACCXCREDXUSER_CCENTER (ID, ACCOUNTCREDENTIALUSERS_ID, ALLOWEDCOSTCENTERS_ID, VERSION) 
	VALUES (hibernate_sequence.nextval, (select acu.id from ACCOUNTXCREDENTIALXUSER acu where acu.id = (SELECT var.ID FROM IDS_USED var WHERE var.NAME LIKE 'ACCOUNTXCREDENTIALXUSER_id')), 
	(select cc.id from COSTCENTER cc where cc.NAME like 'Cintas Cost Center'), 0);

	