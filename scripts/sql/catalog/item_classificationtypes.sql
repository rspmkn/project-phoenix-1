INSERT INTO ITEMCLASSIFICATIONTYPE (ID, NAME, VERSION, ICONURL, TOOLTIPLABEL) VALUES (hibernate_sequence.nextval, 'Assembly_Code', 0, 
'https://d2eodrsfnjvy5e.cloudfront.net/phoenix-icons/assembly.png', 'Assembly Required');

INSERT INTO ITEMCLASSIFICATIONTYPE (ID, NAME, VERSION, ICONURL, TOOLTIPLABEL) VALUES (hibernate_sequence.nextval, 'Recycle_Indicator', 0,
'https://d2eodrsfnjvy5e.cloudfront.net/phoenix-icons/recycle.png', 'Recyclable Item');

INSERT INTO ITEMCLASSIFICATIONTYPE (ID, NAME, VERSION, ICONURL, TOOLTIPLABEL) VALUES (hibernate_sequence.nextval, 'Green_Indicator', 0,
'https://d2eodrsfnjvy5e.cloudfront.net/phoenix-icons/green.png', 'Green Item');

INSERT INTO ITEMCLASSIFICATIONTYPE (ID, NAME, VERSION, ICONURL, TOOLTIPLABEL) VALUES (hibernate_sequence.nextval, 'Green_Information', 0,
null, null);

INSERT INTO ITEMCLASSIFICATIONTYPE (ID, NAME, VERSION, ICONURL, TOOLTIPLABEL) VALUES (hibernate_sequence.nextval, 'EPACPGCompliant_Code', 0,
null, null);

INSERT INTO ITEMCLASSIFICATIONTYPE (ID, NAME, VERSION, ICONURL, TOOLTIPLABEL) VALUES (hibernate_sequence.nextval, 'MSDS_Indicator', 0,
'https://d2eodrsfnjvy5e.cloudfront.net/phoenix-icons/msds.png', 'See Item Specification on Product Details page for data sheet url');

INSERT INTO ITEMCLASSIFICATIONTYPE (ID, NAME, VERSION, ICONURL, TOOLTIPLABEL) VALUES (hibernate_sequence.nextval, 'UNSPSC', 0,
null, null);

INSERT INTO ITEMCLASSIFICATIONTYPE (ID, NAME, VERSION, ICONURL, TOOLTIPLABEL) VALUES (hibernate_sequence.nextval, 'Non_Returnable_Code', 0,
'https://d2eodrsfnjvy5e.cloudfront.net/phoenix-icons/noreturns.png', 'Non-Returnable Item');

INSERT INTO ITEMCLASSIFICATIONTYPE (ID, NAME, VERSION, ICONURL, TOOLTIPLABEL) VALUES (hibernate_sequence.nextval, 'OverweightOversize_Indicator', 0,
null, null);

INSERT INTO ITEMCLASSIFICATIONTYPE (ID, NAME, VERSION, ICONURL, TOOLTIPLABEL) VALUES (hibernate_sequence.nextval, 'Hazmat', 0,
null, null);

INSERT INTO ITEMCLASSIFICATIONTYPE (ID, NAME, VERSION, ICONURL, TOOLTIPLABEL) VALUES (hibernate_sequence.nextval, 'AbilityOne_Indicator', 0,
null, null);

INSERT INTO ITEMCLASSIFICATIONTYPE (ID, NAME, VERSION, ICONURL, TOOLTIPLABEL) VALUES (hibernate_sequence.nextval, 'WBE_Indicator', 0,
null, null);
