INSERT INTO PHONENUMBER (ID, AREACODE, COUNTRYCODE, EXCHANGE, EXTENSION, LINENUMBER, VERSION, ACCOUNT_ID, PERSON_ID, TYPE_ID, VENDOR_ID) 
VALUES (hibernate_sequence.nextval, 
'999', '1', '555', '32', '1234', 0, 
null, 
null, 
(select t.id from PHONENUMBERTYPE t where t.NAME like 'Work'), 
(select v.id from Vendor v where v.NAME = 'USSCO'));

INSERT INTO PHONENUMBER (ID, AREACODE, COUNTRYCODE, EXCHANGE, EXTENSION, LINENUMBER, VERSION, ACCOUNT_ID, PERSON_ID, TYPE_ID, VENDOR_ID) 
VALUES (hibernate_sequence.nextval, 
'999', '1', '555', null, '2234', 0, 
null, 
null, 
(select t.id from PHONENUMBERTYPE t where t.NAME like 'Fax'), 
(select v.id from Vendor v where v.NAME = 'USSCO'));

INSERT INTO PHONENUMBER (ID, AREACODE, COUNTRYCODE, EXCHANGE, EXTENSION, LINENUMBER, VERSION, ACCOUNT_ID, PERSON_ID, TYPE_ID, VENDOR_ID) 
VALUES (hibernate_sequence.nextval, 
'999', '1', '555', '999', '0001', 0, 
null, 
null, 
(select t.id from PHONENUMBERTYPE t where t.NAME like 'Work'), 
(select v.id from Vendor v where v.NAME = 'ECS'));

INSERT INTO PHONENUMBER (ID, AREACODE, COUNTRYCODE, EXCHANGE, EXTENSION, LINENUMBER, VERSION, ACCOUNT_ID, PERSON_ID, TYPE_ID, VENDOR_ID) 
VALUES (hibernate_sequence.nextval, 
'999', '1', '555', null, '0002', 0, 
null, 
null, 
(select t.id from PHONENUMBERTYPE t where t.NAME like 'Fax'), 
(select v.id from Vendor v where v.NAME = 'ECS'));


INSERT INTO ADDRESS (ID, CITY, LINE1, LINE2, STATE, ZIP, PRIMARY, VERSION, ACCOUNT_ID, PERSON_ID, TYPE_ID, VENDOR_ID)
        VALUES (hibernate_sequence.nextval, 'Charlotte', 
        '1000 USSCO Lane', 'Suite 99', 'NC', '28201', 1, 0, 
		null, 
		null,
        (SELECT t.ID FROM ADDRESSTYPE t WHERE t.NAME LIKE 'Work'), 
		(select v.id from Vendor v where v.NAME = 'USSCO'));
		
INSERT INTO ADDRESS (ID, CITY, LINE1, LINE2, STATE, ZIP, PRIMARY, VERSION, ACCOUNT_ID, PERSON_ID, TYPE_ID, VENDOR_ID)
        VALUES (hibernate_sequence.nextval, 'Charlotte', 
        '1000 ECS DR', null, 'NC', '28214', 1, 0, 
		null, 
		null,
        (SELECT t.ID FROM ADDRESSTYPE t WHERE t.NAME LIKE 'Work'), 
		(select v.id from Vendor v where v.NAME = 'ECS'));