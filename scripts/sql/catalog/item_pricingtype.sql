/* fixed price. parameter is an integer or a decimal with two decimal digits. Optional dollar sign in front. */
INSERT INTO PRICINGTYPE (ID, NAME, PARAMETERREGEX, VERSION, USECEILING) 
	VALUES (hibernate_sequence.nextval, 'Fixed Price', '^(\$)?[0-9]+(\.[0-9][0-9])?$', 0, 1);

/* cost plus margin. parameter is a fraction or percentage. */
INSERT INTO PRICINGTYPE (ID, NAME, PARAMETERREGEX, VERSION, USECEILING) 
	VALUES (hibernate_sequence.nextval, 'Cost Plus Margin %', '^([0-9]+[\.]?[0-9]*|[0-9]*\.[0-9]+)[%]?$', 0, 1);

/* cost plus margin, shipping included. parameter is a fraction or percentage. */
INSERT INTO PRICINGTYPE (ID, NAME, PARAMETERREGEX, VERSION, USECEILING) 
	VALUES (hibernate_sequence.nextval, 'Cost Plus Margin %, Shipping Included', '^([0-9]+[\.]?[0-9]*|[0-9]*\.[0-9]+)[%]?$', 0, 1);

/* cost plus margin. parameter is a fraction or percentage. */
INSERT INTO PRICINGTYPE (ID, NAME, PARAMETERREGEX, VERSION, USECEILING) 
	VALUES (hibernate_sequence.nextval, 'Cost Plus Markup %', '^([0-9]+[\.]?[0-9]*|[0-9]*\.[0-9]+)[%]?$', 0, 1);

/* cost plus margin, shipping included. parameter is a fraction or percentage. */
INSERT INTO PRICINGTYPE (ID, NAME, PARAMETERREGEX, VERSION, USECEILING) 
	VALUES (hibernate_sequence.nextval, 'Cost Plus Markup %, Shipping Included', '^([0-9]+[\.]?[0-9]*|[0-9]*\.[0-9]+)[%]?$', 0, 1);

/* list less, with a floor. parameter is a fraction or percentage, followed by zero or more spaces, followed by a comma, followed by spaces, followed by a fraction or percentage. */
INSERT INTO PRICINGTYPE (ID, NAME, PARAMETERREGEX, VERSION, USECEILING) 
	VALUES (hibernate_sequence.nextval, 'List Less Discount %, Floor at Margin %', '^([0-9]+[\.]?[0-9]*|[0-9]*\.[0-9]+)[%]?[ ]*,[ ]*([0-9]+[\.]?[0-9]*|[0-9]*\.[0-9]+)[%]?$', 0, 1);

/* list less, with a floor, shipping included. parameter is a fraction or percentage, followed by zero or more spaces, followed by a comma, followed by spaces, followed by a fraction or percentage. */
INSERT INTO PRICINGTYPE (ID, NAME, PARAMETERREGEX, VERSION, USECEILING) 
	VALUES (hibernate_sequence.nextval, 'List Less Discount %, Floor at Margin %, Shipping Included', '^([0-9]+[\.]?[0-9]*|[0-9]*\.[0-9]+)[%]?[ ]*,[ ]*([0-9]+[\.]?[0-9]*|[0-9]*\.[0-9]+)[%]?$', 0, 1);

/* list less markdown, with a floor. parameter is a fraction or percentage, followed by zero or more spaces, followed by a comma, followed by spaces, followed by a fraction or percentage. */
INSERT INTO PRICINGTYPE (ID, NAME, PARAMETERREGEX, VERSION, USECEILING) 
	VALUES (hibernate_sequence.nextval, 'List Less Markdown %, Floor at Margin %', '^([0-9]+[\.]?[0-9]*|[0-9]*\.[0-9]+)[%]?[ ]*,[ ]*([0-9]+[\.]?[0-9]*|[0-9]*\.[0-9]+)[%]?$', 0, 1);

/* list less markdown, with a floor, shipping included. parameter is a fraction or percentage, followed by zero or more spaces, followed by a comma, followed by spaces, followed by a fraction or percentage. */
INSERT INTO PRICINGTYPE (ID, NAME, PARAMETERREGEX, VERSION, USECEILING) 
	VALUES (hibernate_sequence.nextval, 'List Less Markdown %, Floor at Margin %, Shipping Included', '^([0-9]+[\.]?[0-9]*|[0-9]*\.[0-9]+)[%]?[ ]*,[ ]*([0-9]+[\.]?[0-9]*|[0-9]*\.[0-9]+)[%]?$', 0, 1);

/* cost plus margin. parameter is a fraction or percentage. */
INSERT INTO PRICINGTYPE (ID, NAME, PARAMETERREGEX, VERSION, USECEILING) 
	VALUES (hibernate_sequence.nextval, 'Cost Plus Margin %', '^([0-9]+[\.]?[0-9]*|[0-9]*\.[0-9]+)[%]?$', 0, 0);

/* cost plus margin, shipping included. parameter is a fraction or percentage. */
INSERT INTO PRICINGTYPE (ID, NAME, PARAMETERREGEX, VERSION, USECEILING) 
	VALUES (hibernate_sequence.nextval, 'Cost Plus Margin %, Shipping Included', '^([0-9]+[\.]?[0-9]*|[0-9]*\.[0-9]+)[%]?$', 0, 0);

/* cost plus margin. parameter is a fraction or percentage. */
INSERT INTO PRICINGTYPE (ID, NAME, PARAMETERREGEX, VERSION, USECEILING) 
	VALUES (hibernate_sequence.nextval, 'Cost Plus Markup %', '^([0-9]+[\.]?[0-9]*|[0-9]*\.[0-9]+)[%]?$', 0, 0);

/* cost plus margin, shipping included. parameter is a fraction or percentage. */
INSERT INTO PRICINGTYPE (ID, NAME, PARAMETERREGEX, VERSION, USECEILING) 
	VALUES (hibernate_sequence.nextval, 'Cost Plus Markup %, Shipping Included', '^([0-9]+[\.]?[0-9]*|[0-9]*\.[0-9]+)[%]?$', 0, 0);

/* list less, with a floor. parameter is a fraction or percentage, followed by zero or more spaces, followed by a comma, followed by spaces, followed by a fraction or percentage. */
INSERT INTO PRICINGTYPE (ID, NAME, PARAMETERREGEX, VERSION, USECEILING) 
	VALUES (hibernate_sequence.nextval, 'List Less Discount %, Floor at Margin %', '^([0-9]+[\.]?[0-9]*|[0-9]*\.[0-9]+)[%]?[ ]*,[ ]*([0-9]+[\.]?[0-9]*|[0-9]*\.[0-9]+)[%]?$', 0, 0);

/* list less, with a floor, shipping included. parameter is a fraction or percentage, followed by zero or more spaces, followed by a comma, followed by spaces, followed by a fraction or percentage. */
INSERT INTO PRICINGTYPE (ID, NAME, PARAMETERREGEX, VERSION, USECEILING) 
	VALUES (hibernate_sequence.nextval, 'List Less Discount %, Floor at Margin %, Shipping Included', '^([0-9]+[\.]?[0-9]*|[0-9]*\.[0-9]+)[%]?[ ]*,[ ]*([0-9]+[\.]?[0-9]*|[0-9]*\.[0-9]+)[%]?$', 0, 0);

/* list less markdown, with a floor. parameter is a fraction or percentage, followed by zero or more spaces, followed by a comma, followed by spaces, followed by a fraction or percentage. */
INSERT INTO PRICINGTYPE (ID, NAME, PARAMETERREGEX, VERSION, USECEILING) 
	VALUES (hibernate_sequence.nextval, 'List Less Markdown %, Floor at Margin %', '^([0-9]+[\.]?[0-9]*|[0-9]*\.[0-9]+)[%]?[ ]*,[ ]*([0-9]+[\.]?[0-9]*|[0-9]*\.[0-9]+)[%]?$', 0, 0);

/* list less markdown, with a floor, shipping included. parameter is a fraction or percentage, followed by zero or more spaces, followed by a comma, followed by spaces, followed by a fraction or percentage. */
INSERT INTO PRICINGTYPE (ID, NAME, PARAMETERREGEX, VERSION, USECEILING) 
	VALUES (hibernate_sequence.nextval, 'List Less Markdown %, Floor at Margin %, Shipping Included', '^([0-9]+[\.]?[0-9]*|[0-9]*\.[0-9]+)[%]?[ ]*,[ ]*([0-9]+[\.]?[0-9]*|[0-9]*\.[0-9]+)[%]?$', 0, 0);

/* GSA less. parameter is a fraction or percentage. */
INSERT INTO PRICINGTYPE (ID, NAME, PARAMETERREGEX, VERSION, USECEILING) 
	VALUES (hibernate_sequence.nextval, 'GSA Less', '^([0-9]+[\.]?[0-9]*|[0-9]*\.[0-9]+)[%]?$', 0, 1);

/* GSA less, shipping included. parameter is a fraction or percentage. */
INSERT INTO PRICINGTYPE (ID, NAME, PARAMETERREGEX, VERSION, USECEILING) 
	VALUES (hibernate_sequence.nextval, 'GSA Less, Shipping Included', '^([0-9]+[\.]?[0-9]*|[0-9]*\.[0-9]+)[%]?$', 0, 1);

/* AF less. parameter is a fraction or percentage. */
INSERT INTO PRICINGTYPE (ID, NAME, PARAMETERREGEX, VERSION, USECEILING) 
	VALUES (hibernate_sequence.nextval, 'AF Less', '^([0-9]+[\.]?[0-9]*|[0-9]*\.[0-9]+)[%]?$', 0, 1);

/* AF less, shipping included. parameter is a fraction or percentage. */
INSERT INTO PRICINGTYPE (ID, NAME, PARAMETERREGEX, VERSION, USECEILING) 
	VALUES (hibernate_sequence.nextval, 'AF Less, Shipping Included', '^([0-9]+[\.]?[0-9]*|[0-9]*\.[0-9]+)[%]?$', 0, 1);

/* Street less. parameter is a fraction or percentage. */
INSERT INTO PRICINGTYPE (ID, NAME, PARAMETERREGEX, VERSION, USECEILING) 
	VALUES (hibernate_sequence.nextval, 'Street Less', '^([0-9]+[\.]?[0-9]*|[0-9]*\.[0-9]+)[%]?$', 0, 1);

/* Street less, shipping included. parameter is a fraction or percentage. */
INSERT INTO PRICINGTYPE (ID, NAME, PARAMETERREGEX, VERSION, USECEILING) 
	VALUES (hibernate_sequence.nextval, 'Street Less, Shipping Included', '^([0-9]+[\.]?[0-9]*|[0-9]*\.[0-9]+)[%]?$', 0, 1);

/* Street less. parameter is a fraction or percentage. */
INSERT INTO PRICINGTYPE (ID, NAME, PARAMETERREGEX, VERSION, USECEILING) 
	VALUES (hibernate_sequence.nextval, 'Street Less Markdown', '^([0-9]+[\.]?[0-9]*|[0-9]*\.[0-9]+)[%]?$', 0, 1);

/* Street less, shipping included. parameter is a fraction or percentage. */
INSERT INTO PRICINGTYPE (ID, NAME, PARAMETERREGEX, VERSION, USECEILING) 
	VALUES (hibernate_sequence.nextval, 'Street Less Markdown, Shipping Included', '^([0-9]+[\.]?[0-9]*|[0-9]*\.[0-9]+)[%]?$', 0, 1);

/* Street less. parameter is a fraction or percentage. */
INSERT INTO PRICINGTYPE (ID, NAME, PARAMETERREGEX, VERSION, USECEILING) 
	VALUES (hibernate_sequence.nextval, 'Street Less', '^([0-9]+[\.]?[0-9]*|[0-9]*\.[0-9]+)[%]?$', 0, 0);

/* Street less, shipping included. parameter is a fraction or percentage. */
INSERT INTO PRICINGTYPE (ID, NAME, PARAMETERREGEX, VERSION, USECEILING) 
	VALUES (hibernate_sequence.nextval, 'Street Less, Shipping Included', '^([0-9]+[\.]?[0-9]*|[0-9]*\.[0-9]+)[%]?$', 0, 0);

/* Street less. parameter is a fraction or percentage. */
INSERT INTO PRICINGTYPE (ID, NAME, PARAMETERREGEX, VERSION, USECEILING) 
	VALUES (hibernate_sequence.nextval, 'Street Less Markdown', '^([0-9]+[\.]?[0-9]*|[0-9]*\.[0-9]+)[%]?$', 0, 0);

/* Street less, shipping included. parameter is a fraction or percentage. */
INSERT INTO PRICINGTYPE (ID, NAME, PARAMETERREGEX, VERSION, USECEILING) 
	VALUES (hibernate_sequence.nextval, 'Street Less Markdown, Shipping Included', '^([0-9]+[\.]?[0-9]*|[0-9]*\.[0-9]+)[%]?$', 0, 0);


