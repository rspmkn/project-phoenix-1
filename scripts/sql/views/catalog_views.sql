-- Creates single Column of comma seperated Catalog.NAME(s) based on Account relationship
Create View AccountCatalog_List As
SELECT
     ACCOUNT.ID AS ACCOUNT_ID,
     ACCOUNT.NAME AS ACCOUNT_NAME,
     listagg (Catalog.NAME, ',') WITHIN GROUP (ORDER BY Catalog.NAME) CatalogNames    
FROM
      ACCOUNT
      Inner Join Catalog On ACCOUNT.ID = Catalog.CUSTOMER_ID
      Group By Account.Name, Account.ID 
      Order By Account.ID;
      
-- Creates single Column of comma seperated Catalog.NAME(s) based on Vendor relationship
Create View VendorCatalog_List As
SELECT
     Vendor.ID AS Vendor_ID,
     Vendor.NAME AS Vendor_NAME,
     listagg (Catalog.NAME, ',') WITHIN GROUP (ORDER BY Catalog.NAME) CatalogNames   
FROM
      Vendor
      Inner Join Catalog On Vendor.ID = Catalog.VENDOR_ID
      Group By VENDOR.Name, VENDOR.ID 
      Order By VENDOR.ID;