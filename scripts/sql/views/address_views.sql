-- Creates View of all Address Ids and related AddressProperties
Create View Address_AddressPropType_Pivot as
WITH Address_AddressPropType_Pivot AS (
            SELECT ADDRESSPROPERTYTYPE.Name, ADDRESSXADDRESSPROPERTYTYPE.ADDRESS_ID,
            ADDRESSXADDRESSPROPERTYTYPE.Value
            From ADDRESSXADDRESSPROPERTYTYPE Inner Join ADDRESSPROPERTYTYPE on ADDRESSXADDRESSPROPERTYTYPE.Type_id = ADDRESSPROPERTYTYPE.id
            )
    SELECT *
    FROM   Address_AddressPropType_Pivot
    PIVOT (
           Max(Value)       
           FOR Name          
           IN  ('APD EDI ID' as APD_EDI_ID, 'Airport Code' as Airport_Code, 'Desktop' as Desktop, 
           'Building/Department'as Building_Department, 'US Account Number' as US_Account_Number, 'Cost Center' as Cost_Center)  
         );