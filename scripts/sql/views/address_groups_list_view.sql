-- Creates single Column of comma seperated AccountGroup.Names 

Create View AccountGroupNames_List As
SELECT
     Account.ID AS ACCOUNT_ID,
     ACCOUNT."NAME" AS ACCOUNT_NAME,
     listagg (AccountGroup.Name, ',') WITHIN GROUP (ORDER BY AccountGroup.Name) GroupNames
FROM
      "ACCOUNT" ACCOUNT
      INNER JOIN ACCOUNT_ACCOUNTGROUP ON ACCOUNT.ID = ACCOUNT_ACCOUNTGROUP.ACCOUNTS_ID
      INNER JOIN ACCOUNTGROUP ON ACCOUNT_ACCOUNTGROUP.GROUPS_ID = ACCOUNTGROUP.ID
      Group By Account.Name, Account.ID
      Order By Account.ID;