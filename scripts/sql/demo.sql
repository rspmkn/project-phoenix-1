--/* Updating the next sequence value for hibernate to a number greater than the primary keys used in this script. */
alter sequence hibernate_sequence increment by 50;
select hibernate_sequence.nextval from dual;
alter sequence hibernate_sequence increment by 1;

insert into skutype (id,name,version) values(1,'APD',0);
insert into skutype (id,name,version) values(2,'WALMART',0);
insert into skutype (id,name,version) values(3,'customer',0);

INSERT INTO PAYMENTTYPE (ID, NAME, VERSION) VALUES (1, 'Invoice', 0);
INSERT INTO PAYMENTTYPE (ID, NAME, VERSION) VALUES (2, 'Credit Card', 0);

INSERT INTO ACCOUNT (ID, APDASSIGNEDACCOUNTID, CREATIONDATE, ISACTIVE, NAME, VERSION, PARENTACCOUNT_ID, PAYMENTINFORMATION_ID, ALLOWEDPAYMENTTYPE_ID, ROOTACCOUNT_ID) VALUES (1, null, null, 1, 'account1', 0, null, null, 1, null);
INSERT INTO ACCOUNT (ID, APDASSIGNEDACCOUNTID, CREATIONDATE, ISACTIVE, NAME, VERSION, PARENTACCOUNT_ID, PAYMENTINFORMATION_ID, ALLOWEDPAYMENTTYPE_ID, ROOTACCOUNT_ID) VALUES (2, null, null, 1, 'account2', 0, 1, null, 2, 1);

INSERT INTO VENDOR (ID, EXPIRATIONDATE, NAME, VERSION)
	VALUES (hibernate_sequence.nextval, null, 'USSCO', 0);
INSERT INTO VENDOR (ID, EXPIRATIONDATE, NAME, VERSION)
	VALUES (hibernate_sequence.nextval, null, 'Marfield', 0);
INSERT INTO VENDOR (ID, EXPIRATIONDATE, NAME, VERSION)
	VALUES (hibernate_sequence.nextval, null, 'APD', 0);

--/* Manufacturers */
insert into manufacturer (ID, NAME, VERSION) values(1, 'Manufacturer 1',0);
insert into manufacturer (ID, NAME, VERSION) values(2, 'Manufacturer 2',0);

--/* Vendor Catalogs */
insert into catalog (ID, EXPIRATIONDATE, NAME, STARTDATE, VERSION, CUSTOMER_ID, PARENT_ID, REPLACEMENT_ID, VENDOR_ID) 
	values(1,null,'Demo Vendor Catalog 1',null,0,null,null,null,1);
insert into catalog (ID, EXPIRATIONDATE, NAME, STARTDATE, VERSION, CUSTOMER_ID, PARENT_ID, REPLACEMENT_ID, VENDOR_ID)
	values(2,null,'Demo Vendor Catalog 2',null,0,null,null,null,2);

--/* Customer Catalogs */
insert into catalog (ID, EXPIRATIONDATE, NAME, STARTDATE, VERSION, CUSTOMER_ID, PARENT_ID, REPLACEMENT_ID, VENDOR_ID)
	values(3,null,'Demo Customer Catalog 1',null,0,1,null,null,null);
insert into catalog (ID, EXPIRATIONDATE, NAME, STARTDATE, VERSION, CUSTOMER_ID, PARENT_ID, REPLACEMENT_ID, VENDOR_ID)
	values(4,null,'Demo Customer Catalog 2',null,0,2,null,null,null);

--/* Unit of Measure */
insert into UnitofMeasure values(1,'EA',0);
insert into UnitofMeasure values(2,'BX',0);  

-- /* insert into item values(ID,DESCRIPTION,NAME,VERSION,ITEMCATEGORY_ID,MANUFACTURER_ID,REPLACEMENT_ID,VENDORCATALOG_ID) */
insert into item (id,name,description,version,UNITOFMEASURE_ID,vendorcatalog_id) values(1,'window cleaner','windex',0,1,1);
insert into item (id,name,description,version,UNITOFMEASURE_ID,vendorcatalog_id) values(2,'12 Rolls of Paper Towels','Bounty Paper Towels',0,1,1);
insert into item (id,name,description,version,UNITOFMEASURE_ID,vendorcatalog_id) values (3,'Floor Cleaner','Pinesol original scent floor cleaner',0,1,1);
insert into Item (id,name,description,version,UNITOFMEASURE_ID,vendorcatalog_id) values (4,'Bleach','Clorox bleach unscented',0,1,1);
insert into Item (id,name,description,version,UNITOFMEASURE_ID,vendorcatalog_id) values (5,'Detergent','Lavendar scented',0,1,1);


-- /* 
-- credential (ID,ACTIVATIONDATE,AUTHORIZEADDRESS,EXPIRATIONDATE,NAME,PUNCHLEVEL,PUNCHOUTIDENTITY,STORECREDIT,VERSION,CASHOUTPAGE_ID,
--             CATALOG_ID,COMMUNICATIONMETHOD_ID,PROCESS_ID,ROOTACCOUNT_ID,SKUTYPE_ID)
-- */
insert into credential values(1,null,1,null,'credential1',null,null,null,1,null,1,null,null,null,2);
insert into credential values(2,null,1,null,'credential2',null,null,null,1,null,2,null,null,null,2);

insert into process values (1, 'NovantOrderApproval', 0);
insert into process values (2, 'CintasApproval', 0);
insert into process values (3, 'CHSOrderApproval', 0);
insert into process values (4, 'CintasOrderApproval', 0);

insert into processconfiguration values(1, 0, 1, null, 1);
insert into processconfiguration values(2, 0, null, 1, 3);

insert into processVariable values(1,'NewOrderApprover','apd', 0, 1);
insert into processVariable values(2,'NewOrderApprover','apd', 0, 2);

insert into orderstatus values (1, 'APPROVED', 'APPROVED', 0);
insert into orderstatus values (2, 'DENIED', 'DENIED', 0);
insert into orderstatus values (3, 'CREATED', 'CREATED', 0);

--/* CATALOGXITEM (ID, COREITEMEXPIRATIONDATE, COREITEMSTARTDATE, PRICE, PRICINGPARAMETER, VERSION, CATALOG_ID, CUSTOMERSKU_ID, ITEM_ID, PRICINGTYPE_ID, SUBSTITUTEITEM_ID) VALUES (0, '', '', 0, 0, '', 0, 0, 0, 0, 0, 0);
insert into catalogxitem values(1,null,null,1.99,null,0,3,null,1,null,null);
insert into catalogxitem values(2,null,null,2.99,null,0,3,null,2,null,null);
insert into CatalogXItem (id,catalog_id,item_id,price,version) values (3,1,3,3.00,0);
insert into CatalogXItem (id,catalog_id,item_id,price,version) values (4,1,4,3.00,0);
insert into CatalogXItem (id,catalog_id,item_id,price,version) values (5,1,5,3.00,0);


--/* Customer Costs */
insert into customercost values(1,9.99,0,1,1);

--/* Item Classifications */
insert into itemclassificationtype values(1,'ItemClassification A',0);
insert into itemclassificationtype values(2,'ItemClassification B',0);
insert into itemclassificationtype values(3,'ItemClassification C',0);

insert into codetype values(1,'CodeType A',0);
insert into codetype values(2,'CodeType B',0);
insert into codetype values(3,'CodeType C',0);

insert into classificationcode values(1,0,1);
insert into classificationcode values(2,0,2);
insert into classificationcode values(3,0,3);

insert into imagetype values(1,'jpg',0);
insert into imagetype values(2,'tif',0);
insert into imagetype values(3,'bmp',0);

insert into itemclassificationimage values(1,'http://urlOne.com',0,1);
insert into itemclassificationimage values(2,'http://urlTwo.com',0,2);
insert into itemclassificationimage values(3,'http://urlThree.com',0,3);

insert into notetype values(1,'NoteType A',0);
insert into notetype values(2,'NoteType B',0);
insert into notetype values(3,'NoteType C',0);

INSERT INTO ITEMCLASSIFICATION (ID, VERSION, CLASSIFICATIONCODE_ID, ITEMCLASSIFICATIONIMAGE_ID, ITEMCLASSTYPE_ID) VALUES (1,0,1,1,1);
INSERT INTO ITEMCLASSIFICATION (ID, VERSION, CLASSIFICATIONCODE_ID, ITEMCLASSIFICATIONIMAGE_ID, ITEMCLASSTYPE_ID) VALUES (2,0,2,2,2);
INSERT INTO ITEMCLASSIFICATION (ID, VERSION, CLASSIFICATIONCODE_ID, ITEMCLASSIFICATIONIMAGE_ID, ITEMCLASSTYPE_ID) VALUES (3,0,3,3,3);

-- insert into itemclass_itemclasstype values(1,1);
-- insert into itemclass_itemclasstype values(2,2);
-- insert into itemclass_itemclasstype values(3,3);

--insert into classificationnote values(1,0,1,1);
--insert into classificationnote values(2,0,2,2);
--insert into classificationnote values(3,0,3,3);

--insert into itemclassification_item values(1,1);
--insert into itemclassification_item values(2,1);
--insert into itemclassification_item values(3,1);

--/* Setup user accounts..move to diff file */
insert into systemuser (ID, CREATIONDATE, CHANGEPASSWORD, CONCURRENTACCESS, ISACTIVE, LASTLOGINDATE, LOGIN, PASSWORD, SALT, STATUS, VERSION, PAYMENTINFORMATION_ID) values(1,null,0,0,1,null,'apd','$2a$12$sBg5273drc7KN0ZfQvEkJerHbULw4N470MyUuC8QFFVOf8szMjDjO','$2a$12$sBg5273drc7KN0ZfQvEkJe','active',0,null);
insert into systemuser (ID, CREATIONDATE, CHANGEPASSWORD, CONCURRENTACCESS, ISACTIVE, LASTLOGINDATE, LOGIN, PASSWORD, SALT, STATUS, VERSION, PAYMENTINFORMATION_ID) values(2,null,0,0,1,null,'admin','$2a$12$g1thHAXgKOymC5085ynTCOK7jwI5eMMUC5PueTXuTjQGqlWkFqa1K','$2a$12$g1thHAXgKOymC5085ynTCO','active',0,null);
insert into systemuser (ID, CREATIONDATE, CHANGEPASSWORD, CONCURRENTACCESS, ISACTIVE, LASTLOGINDATE, LOGIN, PASSWORD, SALT, STATUS, VERSION, PAYMENTINFORMATION_ID) values(3,null,0,0,1,null,'user','$2a$12$36KFVJcoqjMMSFnfDYQdQO64/kKi59F3FahShbGfULRjhR3jxrBoa','$2a$12$36KFVJcoqjMMSFnfDYQdQO','active',0,null);

--/*Setup persons */
insert into person values(1,null,'Adam','Smith','W',null,null,0,1,null);
insert into person values(2,'DaBoss@gmail.net','Da','Boss','A',null,null,0,2,null);
insert into person values(3,'SomeUser@gmail.net','Some','User','A',null,null,0,3,null);


--/* Setup Sample Roles */
INSERT INTO ROLE (ID, ISACTIVE, NAME, VERSION) VALUES (1, 1, 'Admin', 0);
INSERT INTO ROLE (ID, ISACTIVE, NAME, VERSION) VALUES (2, 1, 'User', 0);
INSERT INTO ROLE (ID, ISACTIVE, NAME, VERSION) VALUES (3, 1, 'Customer Service', 0);

--/* Setup the sample Users with Roles */
INSERT INTO ROLEXUSER (ID, EXPIRATIONDATE, STARTDATE, VERSION, ROLE_ID, USER_ID) VALUES (1, null, null, 0, 1, 1);
INSERT INTO ROLEXUSER (ID, EXPIRATIONDATE, STARTDATE, VERSION, ROLE_ID, USER_ID) VALUES (2, null, null, 0, 1, 2);
INSERT INTO ROLEXUSER (ID, EXPIRATIONDATE, STARTDATE, VERSION, ROLE_ID, USER_ID) VALUES (3, null, null, 0, 2, 2);
INSERT INTO ROLEXUSER (ID, EXPIRATIONDATE, STARTDATE, VERSION, ROLE_ID, USER_ID) VALUES (4, null, null, 0, 2, 3);
INSERT INTO ROLEXUSER (ID, EXPIRATIONDATE, STARTDATE, VERSION, ROLE_ID, USER_ID) VALUES (5, null, null, 0, 3, 2);

-- /* insert into accountxcredentialxuser values(id,punchlevel,version,account_id,address_id,approveruser_id,cred_id,paymentinformation_id,user_id) */

insert into accountxcredentialxuser values(1,null,0,1,null,null,1,null,1);
insert into accountxcredentialxuser values(2,null,0,1,null,null,2,null,1);
insert into accountxcredentialxuser values(3,null,0,1,null,null,1,null,2);
insert into accountxcredentialxuser values(4,null,0,1,null,null,2,null,2);

--/* Manufacturers */
insert into itemcategory values(1, 'Computer Equipment',0, null);
insert into itemcategory values(2, 'Accessories',0, 1);

--/* Item History */
insert into itemhistory values(1, 'Item Imported',TIMESTAMP'2007-12-12 09:15:47',0,2);
insert into itemhistory values(2, 'Changed APD SKU',TIMESTAMP'2007-12-15 08:23:23',0,2);
insert into itemhistory values(3, 'Changed Item Category',TIMESTAMP'2007-12-20 12:56:30',0,2);

--/* insert into hierarchynode values(id,description,version,parent_id) */

--/* INSERT INTO HIERARCHYNODE (ID, DESCRIPTION, IDPATH, PUBLICID, VERSION, PARENT_ID) VALUES (0, '', '', 0, 0, 0);

insert into hierarchynode (ID, DESCRIPTION, PUBLICID, VERSION, PARENT_ID) values(1,'Office Supplies',1,0,null);
insert into hierarchynode (ID, DESCRIPTION, PUBLICID, VERSION, PARENT_ID) values(2,'MRO Supplies',2,0,null);
insert into hierarchynode (ID, DESCRIPTION, PUBLICID, VERSION, PARENT_ID) values(3,'Corporate Identity',3,0,null);
insert into hierarchynode (ID, DESCRIPTION, PUBLICID, VERSION, PARENT_ID) values(4,'Electronics',4,0,null);

insert into hierarchynode (ID, DESCRIPTION, PUBLICID, VERSION, PARENT_ID) values(5,'Binders',5,0,1);
insert into hierarchynode (ID, DESCRIPTION, PUBLICID, VERSION, PARENT_ID) values(6,'Paper',6,0,1);
insert into hierarchynode (ID, DESCRIPTION, PUBLICID, VERSION, PARENT_ID) values(7,'Toner Cartridges',7,0,1);
insert into hierarchynode (ID, DESCRIPTION, PUBLICID, VERSION, PARENT_ID) values(8,'Desk Accessories',8,0,1);

 
--/* insert into sku values(id,version,hierarchynode_id,item_id,type_id) */
-- ID
-- VERSION
-- HIERARCHYNODE_ID
-- ITEM_ID
-- TYPE_ID
insert into sku values(1,'AAG275048',0,1,1);
insert into sku values(2,'AAG123465',0,1,2);
insert into sku values(3,'WMRT108549',0,2,2);
-- insert into sku values(3,0,3,3,1);
-- insert into sku values(4,0,4,4,1);
-- insert into sku values(5,0,5,5,1);
-- insert into sku values(6,0,6,6,1);
-- insert into sku values(7,0,7,7,1);
-- insert into sku values(8,0,8,8,1);


--/* insert into skutype_hierarchynode values(skutype_id,hierarchynodes_id); */
insert into skutype_hierarchynode values(1,1);
insert into skutype_hierarchynode values(1,2);
insert into skutype_hierarchynode values(1,3);
insert into skutype_hierarchynode values(1,4);

insert into skutype_hierarchynode values(2,3);
insert into skutype_hierarchynode values(2,4);

insert into accountpropertytype values(1,'Description',0);
insert into accountpropertytype values(2,'CXML identity',0);

insert into addresstype values (1,'Home',0);
insert into addresstype values (2,'Work',0);

insert into credentialpropertytype values (1,'Punchout URL',0);
insert into credentialpropertytype values (2,'Welcome Message',0);
insert into credentialpropertytype values (3,'Customer URL',0);
insert into credentialpropertytype values (4,'OfferMessage',0);

insert into eventtype values (1,'Placed',0);
insert into eventtype values (2,'Canceled',0);

insert into fieldtype values (1,'Text Field',0);
insert into fieldtype values (2,'Dropdown',0);

insert into field values (1,'Comment',0,1);
insert into field values (2,'States',0,2);

insert into fieldoptions values (1,'NC',0,2);
insert into fieldoptions values (2,'SC',0,2);
insert into fieldoptions values (3,'GA',0,2);

insert into permission values (1,0,1,1,'View and Edit Users','User Admin',0);
insert into permission values (2,0,1,0,'View Users','User Maintenance',0);

insert into AccountGroup values(1,'Clinics',0);
insert into AccountGroup values(2,'Outpatient',0);
 
insert into ItemIdentifierType values(1,'ItemIdentifierType1',0);
insert into ItemIdentifierType values(2,'ItemIdentifierType2',0);              
 
insert into ItemPropertyType values(1,'ItemPropertyType1', 0, 0);
insert into ItemPropertyType values(2,'ItemPropertyType2', 1, 0);
insert into ItemPropertyType values(3,'discontinued', 0, 0);
insert into ItemPropertyType values(4,'changed sku number', 1, 0);
insert into itempropertytype values(5, 'Made By Elves',0,0);
insert into itempropertytype values(6, 'Magical Hazard',0,0);
insert into itempropertytype values(7, 'Poltergeist Affinity',1,0);

--/* Item Properties id,value,version,type_id,item_id */
insert into itemxitempropertytype values(1,'true',0,5,2);
insert into itemxitempropertytype values(2,'true',0,6,2);
insert into itemxitempropertytype values(3,'true',0,7,2);
insert into itemxitempropertytype values(4,'$5.00',0,1,2);
insert into itemxitempropertytype values(5,'Package Deal',0,2,2);

INSERT INTO PACKAGINGTYPE VALUES (1, 'Packing Type Description1', 'PackagingType1', 0);
INSERT INTO PACKAGINGTYPE VALUES (2, 'Packing Type Description2', 'PackagingType2', 0);
 
insert into PhoneNumberType values(1,'Home',0);
insert into PhoneNumberType values(2,'Cell',0);
 
insert into PONumberType values(1,'POType1',0);
insert into PONumberType values(2,'POType2',0);          
 
insert into ShippingPartner values(1,'UPS',0);
insert into ShippingPartner values(2,'FedEx',0);        
 
insert into VendorPropertyType values(1,'Site URL',0);
insert into VendorPropertyType values(2,'Image URL',0);

insert into COMMUNICATIONMETHOD values(1,'XML',0);
insert into COMMUNICATIONMETHOD values(2,'EDI',0);

-- Favorites list
insert into FavoritesList (id,name,version,catalog_id,axcxu_id) values (1,'User FavoritesList 1',0,null,1);
insert into FavoritesList (id,name,version,catalog_id,axcxu_id) values (2,'User FavoritesList 2',0,null,1);
insert into FavoritesList (id,name,version,catalog_id,axcxu_id) values (3,'User FavoritesList 3',0,null,1);
insert into FavoritesList (id,name,version,catalog_id,axcxu_id) values (4,'Walmart FavoritesList 1',0,3,null);
insert into FavoritesList (id,name,version,catalog_id,axcxu_id) values (5,'Staple FavoritesList 1',0,3,null);

insert into FAVORITESLIST_CATALOGXITEM values (1,1);
insert into FAVORITESLIST_CATALOGXITEM values (1,2);
insert into FAVORITESLIST_CATALOGXITEM values (1,3);
insert into FAVORITESLIST_CATALOGXITEM values (1,4);
insert into FAVORITESLIST_CATALOGXITEM values (1,5);
insert into FAVORITESLIST_CATALOGXITEM values (2,2);
insert into FAVORITESLIST_CATALOGXITEM values (2,4);
insert into FAVORITESLIST_CATALOGXITEM values (3,1);
insert into FAVORITESLIST_CATALOGXITEM values (3,3);
insert into FAVORITESLIST_CATALOGXITEM values (3,5);

insert into FAVORITESLIST_CATALOGXITEM values (4,1);
insert into FAVORITESLIST_CATALOGXITEM values (4,2);
insert into FAVORITESLIST_CATALOGXITEM values (4,3);
insert into FAVORITESLIST_CATALOGXITEM values (5,4);
insert into FAVORITESLIST_CATALOGXITEM values (5,5);


