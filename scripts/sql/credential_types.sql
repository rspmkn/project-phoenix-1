INSERT INTO CREDENTIALPROPERTYTYPE(ID,NAME,VERSION)
VALUES(hibernate_sequence.nextval,'minimum order amount','0');
INSERT INTO CREDENTIALPROPERTYTYPE(ID,NAME,VERSION)
VALUES(hibernate_sequence.nextval,'minimum order fee','0');
INSERT INTO CREDENTIALPROPERTYTYPE(ID,NAME,VERSION)
VALUES(hibernate_sequence.nextval,'handling fee','0');
INSERT INTO CREDENTIALPROPERTYTYPE(ID,NAME,VERSION)
-- A credential property used to specify the route code sent to USSCo in edi 850
VALUES(hibernate_sequence.nextval,'route code','0');
INSERT INTO ITEMCATEGORY(ID,NAME,VERSION)
VALUES(hibernate_sequence.nextval,'fee','0');

