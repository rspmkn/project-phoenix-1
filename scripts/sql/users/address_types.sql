INSERT INTO ADDRESSTYPE (ID, NAME, VERSION) 
	VALUES (hibernate_sequence.nextval, 'SHIPTO', 0);
	
INSERT INTO ADDRESSTYPE (ID, NAME, VERSION) 
	VALUES (hibernate_sequence.nextval, 'BILLTO', 0);
	
INSERT INTO ADDRESSTYPE (ID, NAME, VERSION) 
	VALUES (hibernate_sequence.nextval, 'Home', 0);
	
INSERT INTO ADDRESSTYPE (ID, NAME, VERSION) 
	VALUES (hibernate_sequence.nextval, 'Work', 0);

INSERT INTO ADDRESSPROPERTYTYPE (ID, NAME, VERSION)
	VALUES (hibernate_sequence.nextval, 'APD EDI ID', 0);

INSERT INTO ADDRESSPROPERTYTYPE (ID, NAME, VERSION)
	VALUES (hibernate_sequence.nextval, 'Airport Code', 0);

INSERT INTO ADDRESSPROPERTYTYPE (ID, NAME, VERSION)
	VALUES (hibernate_sequence.nextval, 'Desktop', 0);

INSERT INTO ADDRESSPROPERTYTYPE (ID, NAME, VERSION)
	VALUES (hibernate_sequence.nextval, 'Building/Department', 0);

INSERT INTO ADDRESSPROPERTYTYPE (ID, NAME, VERSION)
	VALUES (hibernate_sequence.nextval, 'US Account Number', 0);