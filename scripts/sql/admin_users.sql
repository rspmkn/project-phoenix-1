INSERT INTO IDS_USED (NAME, ID) VALUES ('admin user person', hibernate_sequence.nextval);
insert into person values((SELECT var.ID FROM IDS_USED var WHERE var.NAME LIKE 'admin user person'),null,'Adam','Smith','W',null,null,0);
insert into systemuser (ID, CREATIONDATE, CHANGEPASSWORD, CONCURRENTACCESS, ISACTIVE, LASTLOGINDATE, LOGIN, PASSWORD, SALT, STATUS, VERSION, PAYMENTINFORMATION_ID, PERSON_ID) 
	values(hibernate_sequence.nextval,null,0,0,1,null,'apd','$2a$12$sBg5273drc7KN0ZfQvEkJerHbULw4N470MyUuC8QFFVOf8szMjDjO','$2a$12$sBg5273drc7KN0ZfQvEkJe','active',0,null, (SELECT var.ID FROM IDS_USED var WHERE var.NAME LIKE 'admin user person'));

UPDATE IDS_USED SET ID = hibernate_sequence.nextval WHERE NAME = 'admin user person';
insert into person values((SELECT var.ID FROM IDS_USED var WHERE var.NAME LIKE 'admin user person'),'teamphoenix@americanproduct.com','Da','Boss','A',null,null,0);
insert into systemuser (ID, CREATIONDATE, CHANGEPASSWORD, CONCURRENTACCESS, ISACTIVE, LASTLOGINDATE, LOGIN, PASSWORD, SALT, STATUS, VERSION, PAYMENTINFORMATION_ID, PERSON_ID) 
	values(hibernate_sequence.nextval,null,0,0,1,null,'admin','$2a$12$g1thHAXgKOymC5085ynTCOK7jwI5eMMUC5PueTXuTjQGqlWkFqa1K','$2a$12$g1thHAXgKOymC5085ynTCO','active',0,null, (SELECT var.ID FROM IDS_USED var WHERE var.NAME LIKE 'admin user person'));

UPDATE IDS_USED SET ID = hibernate_sequence.nextval WHERE NAME = 'admin user person';
insert into person values((SELECT var.ID FROM IDS_USED var WHERE var.NAME LIKE 'admin user person'),'teamphoenix@americanproduct.com','Some','User','A',null,null,0);
insert into systemuser (ID, CREATIONDATE, CHANGEPASSWORD, CONCURRENTACCESS, ISACTIVE, LASTLOGINDATE, LOGIN, PASSWORD, SALT, STATUS, VERSION, PAYMENTINFORMATION_ID, PERSON_ID) 
	values(hibernate_sequence.nextval,null,0,0,1,null,'user','$2a$12$36KFVJcoqjMMSFnfDYQdQO64/kKi59F3FahShbGfULRjhR3jxrBoa','$2a$12$36KFVJcoqjMMSFnfDYQdQO','active',0,null,(SELECT var.ID FROM IDS_USED var WHERE var.NAME LIKE 'admin user person'));

INSERT INTO ROLEXUSER (ID, EXPIRATIONDATE, STARTDATE, VERSION, ROLE_ID, USER_ID) VALUES (hibernate_sequence.nextval, null, null, 0, (select r.id from role r where r.name like 'Admin'), (select s.id from systemuser s where s.login like 'apd'));
INSERT INTO ROLEXUSER (ID, EXPIRATIONDATE, STARTDATE, VERSION, ROLE_ID, USER_ID) VALUES (hibernate_sequence.nextval, null, null, 0, (select r.id from role r where r.name like 'Admin'), (select s.id from systemuser s where s.login like 'admin'));
INSERT INTO ROLEXUSER (ID, EXPIRATIONDATE, STARTDATE, VERSION, ROLE_ID, USER_ID) VALUES (hibernate_sequence.nextval, null, null, 0, (select r.id from role r where r.name like 'User'), (select s.id from systemuser s where s.login like 'admin'));
INSERT INTO ROLEXUSER (ID, EXPIRATIONDATE, STARTDATE, VERSION, ROLE_ID, USER_ID) VALUES (hibernate_sequence.nextval, null, null, 0, (select r.id from role r where r.name like 'Customer Service'), (select s.id from systemuser s where s.login like 'admin'));
INSERT INTO ROLEXUSER (ID, EXPIRATIONDATE, STARTDATE, VERSION, ROLE_ID, USER_ID) VALUES (hibernate_sequence.nextval, null, null, 0, (select r.id from role r where r.name like 'User'), (select s.id from systemuser s where s.login like 'user'));
