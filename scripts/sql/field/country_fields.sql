INSERT INTO FIELDOPTIONS (ID, VALUE, VERSION, FIELD_ID) VALUES (hibernate_sequence.nextval, 'US', 0, (SELECT f.id 
	FROM FIELD f WHERE f.name LIKE 'ship us and canada'));
INSERT INTO FIELDOPTIONS (ID, VALUE, VERSION, FIELD_ID) VALUES (hibernate_sequence.nextval, 'CA', 0, (SELECT f.id 
	FROM FIELD f WHERE f.name LIKE 'ship us and canada'));

INSERT INTO FIELDOPTIONS (ID, VALUE, VERSION, FIELD_ID) VALUES (hibernate_sequence.nextval, 'US', 0, (SELECT f.id 
	FROM FIELD f WHERE f.name LIKE 'ship us only'));
	
INSERT INTO FIELDOPTIONS (ID, VALUE, VERSION, FIELD_ID) VALUES (hibernate_sequence.nextval, 'US', 0, (SELECT f.id 
	FROM FIELD f WHERE f.name LIKE 'ship north america'));
INSERT INTO FIELDOPTIONS (ID, VALUE, VERSION, FIELD_ID) VALUES (hibernate_sequence.nextval, 'CA', 0, (SELECT f.id 
	FROM FIELD f WHERE f.name LIKE 'ship north america'));
INSERT INTO FIELDOPTIONS (ID, VALUE, VERSION, FIELD_ID) VALUES (hibernate_sequence.nextval, 'MX', 0, (SELECT f.id 
	FROM FIELD f WHERE f.name LIKE 'ship north america'));


INSERT INTO FIELDOPTIONS (ID, VALUE, VERSION, FIELD_ID) VALUES (hibernate_sequence.nextval, 'US', 0, (SELECT f.id 
	FROM FIELD f WHERE f.name LIKE 'bill us and canada'));
INSERT INTO FIELDOPTIONS (ID, VALUE, VERSION, FIELD_ID) VALUES (hibernate_sequence.nextval, 'CA', 0, (SELECT f.id 
	FROM FIELD f WHERE f.name LIKE 'bill us and canada'));

INSERT INTO FIELDOPTIONS (ID, VALUE, VERSION, FIELD_ID) VALUES (hibernate_sequence.nextval, 'US', 0, (SELECT f.id 
	FROM FIELD f WHERE f.name LIKE 'bill us only'));
	
INSERT INTO FIELDOPTIONS (ID, VALUE, VERSION, FIELD_ID) VALUES (hibernate_sequence.nextval, 'US', 0, (SELECT f.id 
	FROM FIELD f WHERE f.name LIKE 'bill north america'));
INSERT INTO FIELDOPTIONS (ID, VALUE, VERSION, FIELD_ID) VALUES (hibernate_sequence.nextval, 'CA', 0, (SELECT f.id 
	FROM FIELD f WHERE f.name LIKE 'bill north america'));
INSERT INTO FIELDOPTIONS (ID, VALUE, VERSION, FIELD_ID) VALUES (hibernate_sequence.nextval, 'MX', 0, (SELECT f.id 
	FROM FIELD f WHERE f.name LIKE 'bill north america'));