INSERT INTO FIELDOPTIONS (ID, VALUE, VERSION, FIELD_ID) VALUES (hibernate_sequence.nextval, 'AL', 0, (SELECT f.id 
	FROM FIELD f WHERE f.name LIKE 'ship states'));
INSERT INTO FIELDOPTIONS (ID, VALUE, VERSION, FIELD_ID) VALUES (hibernate_sequence.nextval, 'AK', 0, (SELECT f.id 
	FROM FIELD f WHERE f.name LIKE 'ship states'));
INSERT INTO FIELDOPTIONS (ID, VALUE, VERSION, FIELD_ID) VALUES (hibernate_sequence.nextval, 'AZ', 0, (SELECT f.id 
	FROM FIELD f WHERE f.name LIKE 'ship states'));
INSERT INTO FIELDOPTIONS (ID, VALUE, VERSION, FIELD_ID) VALUES (hibernate_sequence.nextval, 'AR', 0, (SELECT f.id 
	FROM FIELD f WHERE f.name LIKE 'ship states'));
INSERT INTO FIELDOPTIONS (ID, VALUE, VERSION, FIELD_ID) VALUES (hibernate_sequence.nextval, 'CA', 0, (SELECT f.id 
	FROM FIELD f WHERE f.name LIKE 'ship states'));
INSERT INTO FIELDOPTIONS (ID, VALUE, VERSION, FIELD_ID) VALUES (hibernate_sequence.nextval, 'CO', 0, (SELECT f.id 
	FROM FIELD f WHERE f.name LIKE 'ship states'));
INSERT INTO FIELDOPTIONS (ID, VALUE, VERSION, FIELD_ID) VALUES (hibernate_sequence.nextval, 'CT', 0, (SELECT f.id 
	FROM FIELD f WHERE f.name LIKE 'ship states'));
INSERT INTO FIELDOPTIONS (ID, VALUE, VERSION, FIELD_ID) VALUES (hibernate_sequence.nextval, 'DE', 0, (SELECT f.id 
	FROM FIELD f WHERE f.name LIKE 'ship states'));
INSERT INTO FIELDOPTIONS (ID, VALUE, VERSION, FIELD_ID) VALUES (hibernate_sequence.nextval, 'DC', 0, (SELECT f.id 
	FROM FIELD f WHERE f.name LIKE 'ship states'));
INSERT INTO FIELDOPTIONS (ID, VALUE, VERSION, FIELD_ID) VALUES (hibernate_sequence.nextval, 'FL', 0, (SELECT f.id 
	FROM FIELD f WHERE f.name LIKE 'ship states'));
INSERT INTO FIELDOPTIONS (ID, VALUE, VERSION, FIELD_ID) VALUES (hibernate_sequence.nextval, 'GA', 0, (SELECT f.id 
	FROM FIELD f WHERE f.name LIKE 'ship states'));
INSERT INTO FIELDOPTIONS (ID, VALUE, VERSION, FIELD_ID) VALUES (hibernate_sequence.nextval, 'HI', 0, (SELECT f.id 
	FROM FIELD f WHERE f.name LIKE 'ship states'));
INSERT INTO FIELDOPTIONS (ID, VALUE, VERSION, FIELD_ID) VALUES (hibernate_sequence.nextval, 'ID', 0, (SELECT f.id 
	FROM FIELD f WHERE f.name LIKE 'ship states'));
INSERT INTO FIELDOPTIONS (ID, VALUE, VERSION, FIELD_ID) VALUES (hibernate_sequence.nextval, 'IL', 0, (SELECT f.id 
	FROM FIELD f WHERE f.name LIKE 'ship states'));
INSERT INTO FIELDOPTIONS (ID, VALUE, VERSION, FIELD_ID) VALUES (hibernate_sequence.nextval, 'IN', 0, (SELECT f.id 
	FROM FIELD f WHERE f.name LIKE 'ship states'));
INSERT INTO FIELDOPTIONS (ID, VALUE, VERSION, FIELD_ID) VALUES (hibernate_sequence.nextval, 'IA', 0, (SELECT f.id 
	FROM FIELD f WHERE f.name LIKE 'ship states'));
INSERT INTO FIELDOPTIONS (ID, VALUE, VERSION, FIELD_ID) VALUES (hibernate_sequence.nextval, 'KS', 0, (SELECT f.id 
	FROM FIELD f WHERE f.name LIKE 'ship states'));
INSERT INTO FIELDOPTIONS (ID, VALUE, VERSION, FIELD_ID) VALUES (hibernate_sequence.nextval, 'KY', 0, (SELECT f.id 
	FROM FIELD f WHERE f.name LIKE 'ship states'));
INSERT INTO FIELDOPTIONS (ID, VALUE, VERSION, FIELD_ID) VALUES (hibernate_sequence.nextval, 'LA', 0, (SELECT f.id 
	FROM FIELD f WHERE f.name LIKE 'ship states'));
INSERT INTO FIELDOPTIONS (ID, VALUE, VERSION, FIELD_ID) VALUES (hibernate_sequence.nextval, 'ME', 0, (SELECT f.id 
	FROM FIELD f WHERE f.name LIKE 'ship states'));
INSERT INTO FIELDOPTIONS (ID, VALUE, VERSION, FIELD_ID) VALUES (hibernate_sequence.nextval, 'MD', 0, (SELECT f.id 
	FROM FIELD f WHERE f.name LIKE 'ship states'));
INSERT INTO FIELDOPTIONS (ID, VALUE, VERSION, FIELD_ID) VALUES (hibernate_sequence.nextval, 'MA', 0, (SELECT f.id 
	FROM FIELD f WHERE f.name LIKE 'ship states'));
INSERT INTO FIELDOPTIONS (ID, VALUE, VERSION, FIELD_ID) VALUES (hibernate_sequence.nextval, 'MI', 0, (SELECT f.id 
	FROM FIELD f WHERE f.name LIKE 'ship states'));
INSERT INTO FIELDOPTIONS (ID, VALUE, VERSION, FIELD_ID) VALUES (hibernate_sequence.nextval, 'MN', 0, (SELECT f.id 
	FROM FIELD f WHERE f.name LIKE 'ship states'));
INSERT INTO FIELDOPTIONS (ID, VALUE, VERSION, FIELD_ID) VALUES (hibernate_sequence.nextval, 'MS', 0, (SELECT f.id 
	FROM FIELD f WHERE f.name LIKE 'ship states'));
INSERT INTO FIELDOPTIONS (ID, VALUE, VERSION, FIELD_ID) VALUES (hibernate_sequence.nextval, 'MO', 0, (SELECT f.id 
	FROM FIELD f WHERE f.name LIKE 'ship states'));
INSERT INTO FIELDOPTIONS (ID, VALUE, VERSION, FIELD_ID) VALUES (hibernate_sequence.nextval, 'MT', 0, (SELECT f.id 
	FROM FIELD f WHERE f.name LIKE 'ship states'));
INSERT INTO FIELDOPTIONS (ID, VALUE, VERSION, FIELD_ID) VALUES (hibernate_sequence.nextval, 'NE', 0, (SELECT f.id 
	FROM FIELD f WHERE f.name LIKE 'ship states'));
INSERT INTO FIELDOPTIONS (ID, VALUE, VERSION, FIELD_ID) VALUES (hibernate_sequence.nextval, 'NV', 0, (SELECT f.id 
	FROM FIELD f WHERE f.name LIKE 'ship states'));
INSERT INTO FIELDOPTIONS (ID, VALUE, VERSION, FIELD_ID) VALUES (hibernate_sequence.nextval, 'NH', 0, (SELECT f.id 
	FROM FIELD f WHERE f.name LIKE 'ship states'));
INSERT INTO FIELDOPTIONS (ID, VALUE, VERSION, FIELD_ID) VALUES (hibernate_sequence.nextval, 'NJ', 0, (SELECT f.id 
	FROM FIELD f WHERE f.name LIKE 'ship states'));
INSERT INTO FIELDOPTIONS (ID, VALUE, VERSION, FIELD_ID) VALUES (hibernate_sequence.nextval, 'NM', 0, (SELECT f.id 
	FROM FIELD f WHERE f.name LIKE 'ship states'));
INSERT INTO FIELDOPTIONS (ID, VALUE, VERSION, FIELD_ID) VALUES (hibernate_sequence.nextval, 'NY', 0, (SELECT f.id 
	FROM FIELD f WHERE f.name LIKE 'ship states'));
INSERT INTO FIELDOPTIONS (ID, VALUE, VERSION, FIELD_ID) VALUES (hibernate_sequence.nextval, 'NC', 0, (SELECT f.id 
	FROM FIELD f WHERE f.name LIKE 'ship states'));
INSERT INTO FIELDOPTIONS (ID, VALUE, VERSION, FIELD_ID) VALUES (hibernate_sequence.nextval, 'ND', 0, (SELECT f.id 
	FROM FIELD f WHERE f.name LIKE 'ship states'));
INSERT INTO FIELDOPTIONS (ID, VALUE, VERSION, FIELD_ID) VALUES (hibernate_sequence.nextval, 'OH', 0, (SELECT f.id 
	FROM FIELD f WHERE f.name LIKE 'ship states'));
INSERT INTO FIELDOPTIONS (ID, VALUE, VERSION, FIELD_ID) VALUES (hibernate_sequence.nextval, 'OK', 0, (SELECT f.id 
	FROM FIELD f WHERE f.name LIKE 'ship states'));
INSERT INTO FIELDOPTIONS (ID, VALUE, VERSION, FIELD_ID) VALUES (hibernate_sequence.nextval, 'OR', 0, (SELECT f.id 
	FROM FIELD f WHERE f.name LIKE 'ship states'));
INSERT INTO FIELDOPTIONS (ID, VALUE, VERSION, FIELD_ID) VALUES (hibernate_sequence.nextval, 'PA', 0, (SELECT f.id 
	FROM FIELD f WHERE f.name LIKE 'ship states'));
INSERT INTO FIELDOPTIONS (ID, VALUE, VERSION, FIELD_ID) VALUES (hibernate_sequence.nextval, 'RI', 0, (SELECT f.id 
	FROM FIELD f WHERE f.name LIKE 'ship states'));
INSERT INTO FIELDOPTIONS (ID, VALUE, VERSION, FIELD_ID) VALUES (hibernate_sequence.nextval, 'SC', 0, (SELECT f.id 
	FROM FIELD f WHERE f.name LIKE 'ship states'));
INSERT INTO FIELDOPTIONS (ID, VALUE, VERSION, FIELD_ID) VALUES (hibernate_sequence.nextval, 'SD', 0, (SELECT f.id 
	FROM FIELD f WHERE f.name LIKE 'ship states'));
INSERT INTO FIELDOPTIONS (ID, VALUE, VERSION, FIELD_ID) VALUES (hibernate_sequence.nextval, 'TN', 0, (SELECT f.id 
	FROM FIELD f WHERE f.name LIKE 'ship states'));
INSERT INTO FIELDOPTIONS (ID, VALUE, VERSION, FIELD_ID) VALUES (hibernate_sequence.nextval, 'TX', 0, (SELECT f.id 
	FROM FIELD f WHERE f.name LIKE 'ship states'));
INSERT INTO FIELDOPTIONS (ID, VALUE, VERSION, FIELD_ID) VALUES (hibernate_sequence.nextval, 'UT', 0, (SELECT f.id 
	FROM FIELD f WHERE f.name LIKE 'ship states'));
INSERT INTO FIELDOPTIONS (ID, VALUE, VERSION, FIELD_ID) VALUES (hibernate_sequence.nextval, 'VT', 0, (SELECT f.id 
	FROM FIELD f WHERE f.name LIKE 'ship states'));
INSERT INTO FIELDOPTIONS (ID, VALUE, VERSION, FIELD_ID) VALUES (hibernate_sequence.nextval, 'VA', 0, (SELECT f.id 
	FROM FIELD f WHERE f.name LIKE 'ship states'));
INSERT INTO FIELDOPTIONS (ID, VALUE, VERSION, FIELD_ID) VALUES (hibernate_sequence.nextval, 'WA', 0, (SELECT f.id 
	FROM FIELD f WHERE f.name LIKE 'ship states'));
INSERT INTO FIELDOPTIONS (ID, VALUE, VERSION, FIELD_ID) VALUES (hibernate_sequence.nextval, 'WV', 0, (SELECT f.id 
	FROM FIELD f WHERE f.name LIKE 'ship states'));
INSERT INTO FIELDOPTIONS (ID, VALUE, VERSION, FIELD_ID) VALUES (hibernate_sequence.nextval, 'WI', 0, (SELECT f.id 
	FROM FIELD f WHERE f.name LIKE 'ship states'));
INSERT INTO FIELDOPTIONS (ID, VALUE, VERSION, FIELD_ID) VALUES (hibernate_sequence.nextval, 'WY', 0, (SELECT f.id 
	FROM FIELD f WHERE f.name LIKE 'ship states'));
	
	

	
INSERT INTO FIELDOPTIONS (ID, VALUE, VERSION, FIELD_ID) VALUES (hibernate_sequence.nextval, 'AL', 0, (SELECT f.id 
	FROM FIELD f WHERE f.name LIKE 'ship contiguous states'));
INSERT INTO FIELDOPTIONS (ID, VALUE, VERSION, FIELD_ID) VALUES (hibernate_sequence.nextval, 'AZ', 0, (SELECT f.id 
	FROM FIELD f WHERE f.name LIKE 'ship contiguous states'));
INSERT INTO FIELDOPTIONS (ID, VALUE, VERSION, FIELD_ID) VALUES (hibernate_sequence.nextval, 'AR', 0, (SELECT f.id 
	FROM FIELD f WHERE f.name LIKE 'ship contiguous states'));
INSERT INTO FIELDOPTIONS (ID, VALUE, VERSION, FIELD_ID) VALUES (hibernate_sequence.nextval, 'CA', 0, (SELECT f.id 
	FROM FIELD f WHERE f.name LIKE 'ship contiguous states'));
INSERT INTO FIELDOPTIONS (ID, VALUE, VERSION, FIELD_ID) VALUES (hibernate_sequence.nextval, 'CO', 0, (SELECT f.id 
	FROM FIELD f WHERE f.name LIKE 'ship contiguous states'));
INSERT INTO FIELDOPTIONS (ID, VALUE, VERSION, FIELD_ID) VALUES (hibernate_sequence.nextval, 'CT', 0, (SELECT f.id 
	FROM FIELD f WHERE f.name LIKE 'ship contiguous states'));
INSERT INTO FIELDOPTIONS (ID, VALUE, VERSION, FIELD_ID) VALUES (hibernate_sequence.nextval, 'DE', 0, (SELECT f.id 
	FROM FIELD f WHERE f.name LIKE 'ship contiguous states'));
INSERT INTO FIELDOPTIONS (ID, VALUE, VERSION, FIELD_ID) VALUES (hibernate_sequence.nextval, 'DC', 0, (SELECT f.id 
	FROM FIELD f WHERE f.name LIKE 'ship contiguous states'));
INSERT INTO FIELDOPTIONS (ID, VALUE, VERSION, FIELD_ID) VALUES (hibernate_sequence.nextval, 'FL', 0, (SELECT f.id 
	FROM FIELD f WHERE f.name LIKE 'ship contiguous states'));
INSERT INTO FIELDOPTIONS (ID, VALUE, VERSION, FIELD_ID) VALUES (hibernate_sequence.nextval, 'GA', 0, (SELECT f.id 
	FROM FIELD f WHERE f.name LIKE 'ship contiguous states'));
INSERT INTO FIELDOPTIONS (ID, VALUE, VERSION, FIELD_ID) VALUES (hibernate_sequence.nextval, 'ID', 0, (SELECT f.id 
	FROM FIELD f WHERE f.name LIKE 'ship contiguous states'));
INSERT INTO FIELDOPTIONS (ID, VALUE, VERSION, FIELD_ID) VALUES (hibernate_sequence.nextval, 'IL', 0, (SELECT f.id 
	FROM FIELD f WHERE f.name LIKE 'ship contiguous states'));
INSERT INTO FIELDOPTIONS (ID, VALUE, VERSION, FIELD_ID) VALUES (hibernate_sequence.nextval, 'IN', 0, (SELECT f.id 
	FROM FIELD f WHERE f.name LIKE 'ship contiguous states'));
INSERT INTO FIELDOPTIONS (ID, VALUE, VERSION, FIELD_ID) VALUES (hibernate_sequence.nextval, 'IA', 0, (SELECT f.id 
	FROM FIELD f WHERE f.name LIKE 'ship contiguous states'));
INSERT INTO FIELDOPTIONS (ID, VALUE, VERSION, FIELD_ID) VALUES (hibernate_sequence.nextval, 'KS', 0, (SELECT f.id 
	FROM FIELD f WHERE f.name LIKE 'ship contiguous states'));
INSERT INTO FIELDOPTIONS (ID, VALUE, VERSION, FIELD_ID) VALUES (hibernate_sequence.nextval, 'KY', 0, (SELECT f.id 
	FROM FIELD f WHERE f.name LIKE 'ship contiguous states'));
INSERT INTO FIELDOPTIONS (ID, VALUE, VERSION, FIELD_ID) VALUES (hibernate_sequence.nextval, 'LA', 0, (SELECT f.id 
	FROM FIELD f WHERE f.name LIKE 'ship contiguous states'));
INSERT INTO FIELDOPTIONS (ID, VALUE, VERSION, FIELD_ID) VALUES (hibernate_sequence.nextval, 'ME', 0, (SELECT f.id 
	FROM FIELD f WHERE f.name LIKE 'ship contiguous states'));
INSERT INTO FIELDOPTIONS (ID, VALUE, VERSION, FIELD_ID) VALUES (hibernate_sequence.nextval, 'MD', 0, (SELECT f.id 
	FROM FIELD f WHERE f.name LIKE 'ship contiguous states'));
INSERT INTO FIELDOPTIONS (ID, VALUE, VERSION, FIELD_ID) VALUES (hibernate_sequence.nextval, 'MA', 0, (SELECT f.id 
	FROM FIELD f WHERE f.name LIKE 'ship contiguous states'));
INSERT INTO FIELDOPTIONS (ID, VALUE, VERSION, FIELD_ID) VALUES (hibernate_sequence.nextval, 'MI', 0, (SELECT f.id 
	FROM FIELD f WHERE f.name LIKE 'ship contiguous states'));
INSERT INTO FIELDOPTIONS (ID, VALUE, VERSION, FIELD_ID) VALUES (hibernate_sequence.nextval, 'MN', 0, (SELECT f.id 
	FROM FIELD f WHERE f.name LIKE 'ship contiguous states'));
INSERT INTO FIELDOPTIONS (ID, VALUE, VERSION, FIELD_ID) VALUES (hibernate_sequence.nextval, 'MS', 0, (SELECT f.id 
	FROM FIELD f WHERE f.name LIKE 'ship contiguous states'));
INSERT INTO FIELDOPTIONS (ID, VALUE, VERSION, FIELD_ID) VALUES (hibernate_sequence.nextval, 'MO', 0, (SELECT f.id 
	FROM FIELD f WHERE f.name LIKE 'ship contiguous states'));
INSERT INTO FIELDOPTIONS (ID, VALUE, VERSION, FIELD_ID) VALUES (hibernate_sequence.nextval, 'MT', 0, (SELECT f.id 
	FROM FIELD f WHERE f.name LIKE 'ship contiguous states'));
INSERT INTO FIELDOPTIONS (ID, VALUE, VERSION, FIELD_ID) VALUES (hibernate_sequence.nextval, 'NE', 0, (SELECT f.id 
	FROM FIELD f WHERE f.name LIKE 'ship contiguous states'));
INSERT INTO FIELDOPTIONS (ID, VALUE, VERSION, FIELD_ID) VALUES (hibernate_sequence.nextval, 'NV', 0, (SELECT f.id 
	FROM FIELD f WHERE f.name LIKE 'ship contiguous states'));
INSERT INTO FIELDOPTIONS (ID, VALUE, VERSION, FIELD_ID) VALUES (hibernate_sequence.nextval, 'NH', 0, (SELECT f.id 
	FROM FIELD f WHERE f.name LIKE 'ship contiguous states'));
INSERT INTO FIELDOPTIONS (ID, VALUE, VERSION, FIELD_ID) VALUES (hibernate_sequence.nextval, 'NJ', 0, (SELECT f.id 
	FROM FIELD f WHERE f.name LIKE 'ship contiguous states'));
INSERT INTO FIELDOPTIONS (ID, VALUE, VERSION, FIELD_ID) VALUES (hibernate_sequence.nextval, 'NM', 0, (SELECT f.id 
	FROM FIELD f WHERE f.name LIKE 'ship contiguous states'));
INSERT INTO FIELDOPTIONS (ID, VALUE, VERSION, FIELD_ID) VALUES (hibernate_sequence.nextval, 'NY', 0, (SELECT f.id 
	FROM FIELD f WHERE f.name LIKE 'ship contiguous states'));
INSERT INTO FIELDOPTIONS (ID, VALUE, VERSION, FIELD_ID) VALUES (hibernate_sequence.nextval, 'NC', 0, (SELECT f.id 
	FROM FIELD f WHERE f.name LIKE 'ship contiguous states'));
INSERT INTO FIELDOPTIONS (ID, VALUE, VERSION, FIELD_ID) VALUES (hibernate_sequence.nextval, 'ND', 0, (SELECT f.id 
	FROM FIELD f WHERE f.name LIKE 'ship contiguous states'));
INSERT INTO FIELDOPTIONS (ID, VALUE, VERSION, FIELD_ID) VALUES (hibernate_sequence.nextval, 'OH', 0, (SELECT f.id 
	FROM FIELD f WHERE f.name LIKE 'ship contiguous states'));
INSERT INTO FIELDOPTIONS (ID, VALUE, VERSION, FIELD_ID) VALUES (hibernate_sequence.nextval, 'OK', 0, (SELECT f.id 
	FROM FIELD f WHERE f.name LIKE 'ship contiguous states'));
INSERT INTO FIELDOPTIONS (ID, VALUE, VERSION, FIELD_ID) VALUES (hibernate_sequence.nextval, 'OR', 0, (SELECT f.id 
	FROM FIELD f WHERE f.name LIKE 'ship contiguous states'));
INSERT INTO FIELDOPTIONS (ID, VALUE, VERSION, FIELD_ID) VALUES (hibernate_sequence.nextval, 'PA', 0, (SELECT f.id 
	FROM FIELD f WHERE f.name LIKE 'ship contiguous states'));
INSERT INTO FIELDOPTIONS (ID, VALUE, VERSION, FIELD_ID) VALUES (hibernate_sequence.nextval, 'RI', 0, (SELECT f.id 
	FROM FIELD f WHERE f.name LIKE 'ship contiguous states'));
INSERT INTO FIELDOPTIONS (ID, VALUE, VERSION, FIELD_ID) VALUES (hibernate_sequence.nextval, 'SC', 0, (SELECT f.id 
	FROM FIELD f WHERE f.name LIKE 'ship contiguous states'));
INSERT INTO FIELDOPTIONS (ID, VALUE, VERSION, FIELD_ID) VALUES (hibernate_sequence.nextval, 'SD', 0, (SELECT f.id 
	FROM FIELD f WHERE f.name LIKE 'ship contiguous states'));
INSERT INTO FIELDOPTIONS (ID, VALUE, VERSION, FIELD_ID) VALUES (hibernate_sequence.nextval, 'TN', 0, (SELECT f.id 
	FROM FIELD f WHERE f.name LIKE 'ship contiguous states'));
INSERT INTO FIELDOPTIONS (ID, VALUE, VERSION, FIELD_ID) VALUES (hibernate_sequence.nextval, 'TX', 0, (SELECT f.id 
	FROM FIELD f WHERE f.name LIKE 'ship contiguous states'));
INSERT INTO FIELDOPTIONS (ID, VALUE, VERSION, FIELD_ID) VALUES (hibernate_sequence.nextval, 'UT', 0, (SELECT f.id 
	FROM FIELD f WHERE f.name LIKE 'ship contiguous states'));
INSERT INTO FIELDOPTIONS (ID, VALUE, VERSION, FIELD_ID) VALUES (hibernate_sequence.nextval, 'VT', 0, (SELECT f.id 
	FROM FIELD f WHERE f.name LIKE 'ship contiguous states'));
INSERT INTO FIELDOPTIONS (ID, VALUE, VERSION, FIELD_ID) VALUES (hibernate_sequence.nextval, 'VA', 0, (SELECT f.id 
	FROM FIELD f WHERE f.name LIKE 'ship contiguous states'));
INSERT INTO FIELDOPTIONS (ID, VALUE, VERSION, FIELD_ID) VALUES (hibernate_sequence.nextval, 'WA', 0, (SELECT f.id 
	FROM FIELD f WHERE f.name LIKE 'ship contiguous states'));
INSERT INTO FIELDOPTIONS (ID, VALUE, VERSION, FIELD_ID) VALUES (hibernate_sequence.nextval, 'WV', 0, (SELECT f.id 
	FROM FIELD f WHERE f.name LIKE 'ship contiguous states'));
INSERT INTO FIELDOPTIONS (ID, VALUE, VERSION, FIELD_ID) VALUES (hibernate_sequence.nextval, 'WI', 0, (SELECT f.id 
	FROM FIELD f WHERE f.name LIKE 'ship contiguous states'));
INSERT INTO FIELDOPTIONS (ID, VALUE, VERSION, FIELD_ID) VALUES (hibernate_sequence.nextval, 'WY', 0, (SELECT f.id 
	FROM FIELD f WHERE f.name LIKE 'ship contiguous states'));