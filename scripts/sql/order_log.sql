INSERT INTO LINEITEMSTATUS (ID,VALUE,VERSION)
        VALUES(hibernate_sequence.nextval,'rejected',0);

INSERT INTO ORDERSTATUS(id,description,value,version)
VALUES(hibernate_sequence.nextval,'order was requested', 'REQUESTED', 0);

INSERT INTO ORDER_LOG (ID,VERSION,FROM_CLASS,LINEITEM_ID,LINEITEMSTATUS_ID,EVENTTYPE)
	VALUES(hibernate_sequence.nextval,0,
		'LINEITEMLOG', 
		(SELECT lineItem.id FROM LINEITEM 
			WHERE LINEITEM.description LIKE 'Large%'),
		(SELECT lineItemStatus.id FROM LINEITEMSTATUS 
			WHERE LINEITEMSTATUS.value='rejected'),
		'DECLINED'
	);

INSERT INTO ORDER_LOG (ID,VERSION,FROM_CLASS,LINEITEM_ID,LINEITEMSTATUS_ID,EVENTTYPE)
        VALUES(hibernate_sequence.nextval,0,
                'LINEITEMLOG',
                (SELECT lineItem.id FROM LINEITEM WHERE LINEITEM.description LIKE 'Large%'),
                (SELECT lineItemStatus.id FROM LINEITEMSTATUS 
			WHERE LINEITEMSTATUS.value='rejected'),
                'DECLINED'
        );

INSERT INTO ORDER_LOG (ID,VERSION,FROM_CLASS,LINEITEM_ID,LINEITEMSTATUS_ID,EVENTTYPE)
        VALUES(hibernate_sequence.nextval,0,
                'LINEITEMLOG',
                (SELECT lineItem.id FROM LINEITEM 
			WHERE LINEITEM.description LIKE 'Poster%' AND rownum = 1),
                (SELECT lineItemStatus.id FROM LINEITEMSTATUS 
			WHERE LINEITEMSTATUS.value='rejected'),
                'DECLINED'
        );

INSERT INTO CUSTOMERORDER(ID, ESTIMATEDSHIPPINGAMOUNT, ORDERTOTAL, ACCOUNT_ID ,VERSION, ADDRESS_ID,
 	CREDENTIAL_ID,STATUS_ID, USER_ID,ORDERDATE, TAXASSIGNED, WILLCALLEDIOVERRIDE, WRAPANDLABEL, ADOT)
	VALUES(hibernate_sequence.nextval,
		1337,
		1337,
		(SELECT account.id FROM ACCOUNT
			WHERE ACCOUNT.name='CSS Hospitals And Clinics' AND rownum = 1),
		'2',
		(SELECT address.id FROM ADDRESS WHERE rownum = 1),
		(SELECT credential.id FROM CREDENTIAL WHERE CREDENTIAL.name='Solr Credential'),
		(SELECT orderStatus.id FROM ORDERSTATUS WHERE ORDERSTATUS.value='QUOTED'),
		(SELECT systemUser.id FROM SYSTEMUSER WHERE systemUser.login='admin'),
		date '2013-03-07','1', 0, 0, 0
	);

INSERT INTO CUSTOMERORDER(ID, ESTIMATEDSHIPPINGAMOUNT, ORDERTOTAL, ACCOUNT_ID ,VERSION, ADDRESS_ID,
 	CREDENTIAL_ID,STATUS_ID, USER_ID,ORDERDATE, TAXASSIGNED, WILLCALLEDIOVERRIDE, WRAPANDLABEL, ADOT)
	VALUES(hibernate_sequence.nextval,
		13381,
		13381,
		(SELECT account.id FROM ACCOUNT
			WHERE ACCOUNT.name='CSS Hospitals And Clinics' AND rownum = 1),
		'2',
		(SELECT address.id FROM ADDRESS WHERE rownum = 1),
		(SELECT credential.id FROM CREDENTIAL WHERE CREDENTIAL.name='Solr Credential'),
		(SELECT orderStatus.id FROM ORDERSTATUS WHERE ORDERSTATUS.value='PLACED'),
		(SELECT systemUser.id FROM SYSTEMUSER WHERE systemUser.login='admin'),
		date '2012-03-07','1', 0, 0, 0
	);

INSERT INTO CUSTOMERORDER(ID, ESTIMATEDSHIPPINGAMOUNT, ORDERTOTAL, ACCOUNT_ID ,VERSION, ADDRESS_ID,
 	CREDENTIAL_ID,STATUS_ID, USER_ID,ORDERDATE, TAXASSIGNED, WILLCALLEDIOVERRIDE, WRAPANDLABEL, ADOT)
	VALUES(hibernate_sequence.nextval,
		1339,
		1339,
		(SELECT account.id FROM ACCOUNT
			WHERE ACCOUNT.name='CSS Hospitals And Clinics' AND rownum = 1),
		'2',
		(SELECT address.id FROM ADDRESS WHERE rownum = 1),
		(SELECT credential.id FROM CREDENTIAL WHERE CREDENTIAL.name='Solr Credential'),
		(SELECT orderStatus.id FROM ORDERSTATUS WHERE ORDERSTATUS.value='PLACED'),
		(SELECT systemUser.id FROM SYSTEMUSER WHERE systemUser.login='admin'),
		date '2012-04-07','1', 0, 0, 0
	);

INSERT INTO CUSTOMERORDER(ID, ESTIMATEDSHIPPINGAMOUNT, ORDERTOTAL, ACCOUNT_ID ,VERSION, ADDRESS_ID,
 	CREDENTIAL_ID,STATUS_ID, USER_ID,ORDERDATE, TAXASSIGNED, WILLCALLEDIOVERRIDE, WRAPANDLABEL, ADOT)
	VALUES(hibernate_sequence.nextval,
		40,
		40,
		(SELECT account.id FROM ACCOUNT
			WHERE ACCOUNT.name='CSS Hospitals And Clinics' AND rownum = 1),
		'2',
		(SELECT address.id FROM ADDRESS WHERE rownum = 1),
		(SELECT credential.id FROM CREDENTIAL WHERE CREDENTIAL.name='Solr Credential'),
		(SELECT orderStatus.id FROM ORDERSTATUS WHERE ORDERSTATUS.value='PLACED'),
		(SELECT systemUser.id FROM SYSTEMUSER WHERE systemUser.login='admin'),
		date '2013-03-07','1', 0, 0, 0
	);

INSERT INTO CUSTOMERORDER(ID, ESTIMATEDSHIPPINGAMOUNT, ORDERTOTAL, ACCOUNT_ID ,VERSION, ADDRESS_ID,
 	CREDENTIAL_ID,STATUS_ID, USER_ID,ORDERDATE, TAXASSIGNED, WILLCALLEDIOVERRIDE, WRAPANDLABEL, ADOT)
	VALUES(hibernate_sequence.nextval,
		50,
		50,
		(SELECT account.id FROM ACCOUNT
			WHERE ACCOUNT.name='CSS Hospitals And Clinics' AND rownum = 1),
		'2',
		(SELECT address.id FROM ADDRESS WHERE rownum = 1),
		(SELECT credential.id FROM CREDENTIAL WHERE CREDENTIAL.name='Solr Credential'),
		(SELECT orderStatus.id FROM ORDERSTATUS WHERE ORDERSTATUS.value='PLACED'),
		(SELECT systemUser.id FROM SYSTEMUSER WHERE systemUser.login='admin'),
		date '2012-03-07','1', 0, 0, 0
	);

INSERT INTO CUSTOMERORDER(ID, ESTIMATEDSHIPPINGAMOUNT, ORDERTOTAL, ACCOUNT_ID ,VERSION, ADDRESS_ID,
 	CREDENTIAL_ID,STATUS_ID, USER_ID,ORDERDATE, TAXASSIGNED, WILLCALLEDIOVERRIDE, WRAPANDLABEL, ADOT)
	VALUES(hibernate_sequence.nextval,
		60,
		60,
		(SELECT account.id FROM ACCOUNT
			WHERE ACCOUNT.name='CSS Hospitals And Clinics' AND rownum = 1),
		'2',
		(SELECT address.id FROM ADDRESS WHERE rownum = 1),
		(SELECT credential.id FROM CREDENTIAL WHERE CREDENTIAL.name='Solr Credential'),
		(SELECT orderStatus.id FROM ORDERSTATUS WHERE ORDERSTATUS.value='PLACED'),
		(SELECT systemUser.id FROM SYSTEMUSER WHERE systemUser.login='admin'),
		date '2012-04-07','1', 0, 0, 0
	);

INSERT INTO CUSTOMERORDER(ID, ESTIMATEDSHIPPINGAMOUNT, ORDERTOTAL, ACCOUNT_ID ,VERSION, ADDRESS_ID,
 	CREDENTIAL_ID,STATUS_ID, USER_ID,ORDERDATE, TAXASSIGNED, WILLCALLEDIOVERRIDE, WRAPANDLABEL, ADOT)
	VALUES(hibernate_sequence.nextval,
		70,
		70,
		(SELECT account.id FROM ACCOUNT
			WHERE ACCOUNT.name='CSS Hospitals And Clinics' AND rownum = 1),
		'2',
		(SELECT address.id FROM ADDRESS WHERE rownum = 1),
		(SELECT credential.id FROM CREDENTIAL WHERE CREDENTIAL.name='Solr Credential'),
		(SELECT orderStatus.id FROM ORDERSTATUS WHERE ORDERSTATUS.value='PLACED'),
		(SELECT systemUser.id FROM SYSTEMUSER WHERE systemUser.login='admin'),
		date '2013-03-07','1', 0, 0, 0
	);

INSERT INTO CUSTOMERORDER(ID, ESTIMATEDSHIPPINGAMOUNT, ORDERTOTAL, ACCOUNT_ID ,VERSION, ADDRESS_ID,
 	CREDENTIAL_ID,STATUS_ID, USER_ID,ORDERDATE, TAXASSIGNED, WILLCALLEDIOVERRIDE, WRAPANDLABEL, ADOT)
	VALUES(hibernate_sequence.nextval,
		80,
		80,
		(SELECT account.id FROM ACCOUNT
			WHERE ACCOUNT.name='CSS Hospitals And Clinics' AND rownum = 1),
		'2',
		(SELECT address.id FROM ADDRESS WHERE rownum = 1),
		(SELECT credential.id FROM CREDENTIAL WHERE CREDENTIAL.name='Solr Credential'),
		(SELECT orderStatus.id FROM ORDERSTATUS WHERE ORDERSTATUS.value='PLACED'),
		(SELECT systemUser.id FROM SYSTEMUSER WHERE systemUser.login='admin'),
		date '2012-03-07','1', 0, 0, 0
	);

INSERT INTO CUSTOMERORDER(ID, ESTIMATEDSHIPPINGAMOUNT, ORDERTOTAL, ACCOUNT_ID ,VERSION, ADDRESS_ID,
 	CREDENTIAL_ID,STATUS_ID, USER_ID,ORDERDATE, TAXASSIGNED, WILLCALLEDIOVERRIDE, WRAPANDLABEL, ADOT)
	VALUES(hibernate_sequence.nextval,
		90,
		90,
		(SELECT account.id FROM ACCOUNT
			WHERE ACCOUNT.name='CSS Hospitals And Clinics' AND rownum = 1),
		'2',
		(SELECT address.id FROM ADDRESS WHERE rownum = 1),
		(SELECT credential.id FROM CREDENTIAL WHERE CREDENTIAL.name='Solr Credential'),
		(SELECT orderStatus.id FROM ORDERSTATUS WHERE ORDERSTATUS.value='PLACED'),
		(SELECT systemUser.id FROM SYSTEMUSER WHERE systemUser.login='admin'),
		date '2012-04-07','1', 0, 0, 0
	);

INSERT INTO CUSTOMERORDER(ID, ESTIMATEDSHIPPINGAMOUNT, ORDERTOTAL, ACCOUNT_ID ,VERSION, ADDRESS_ID,
 	CREDENTIAL_ID,STATUS_ID, USER_ID,ORDERDATE, TAXASSIGNED, WILLCALLEDIOVERRIDE, WRAPANDLABEL, ADOT)
	VALUES(hibernate_sequence.nextval,
		101,
		101,
		(SELECT account.id FROM ACCOUNT
			WHERE ACCOUNT.name='CSS Hospitals And Clinics' AND rownum = 1),
		'2',
		(SELECT address.id FROM ADDRESS WHERE rownum = 1),
		(SELECT credential.id FROM CREDENTIAL WHERE CREDENTIAL.name='Solr Credential'),
		(SELECT orderStatus.id FROM ORDERSTATUS WHERE ORDERSTATUS.value='QUOTED'),
		(SELECT systemUser.id FROM SYSTEMUSER WHERE systemUser.login='admin'),
		date '2000-02-03','1', 0, 0, 0
	);

INSERT INTO ORDER_LOG (ID,FROM_CLASS,CUSTOMERORDER_ID,ORDERSTATUS_ID,UPDATETIMESTAMP,VERSION)
        VALUES(hibernate_sequence.nextval,
                'OrderLog',
                (SELECT CUSTOMERORDER.id FROM CUSTOMERORDER 
			WHERE customerOrder.ESTIMATEDSHIPPINGAMOUNT=1337 AND rownum = 1),
                (SELECT ORDERSTATUS.id FROM ORDERSTATUS 
			WHERE ORDERSTATUS.value='PLACED'),
		date '2013-01-07',
		'1'
        );

INSERT INTO ORDER_LOG (ID,FROM_CLASS,CUSTOMERORDER_ID,ORDERSTATUS_ID,UPDATETIMESTAMP,VERSION)
        VALUES(hibernate_sequence.nextval,
                'OrderLog',
                (SELECT CUSTOMERORDER.id FROM CUSTOMERORDER 
			WHERE customerOrder.ESTIMATEDSHIPPINGAMOUNT=1338 AND rownum = 1),
                (SELECT ORDERSTATUS.id FROM ORDERSTATUS 
			WHERE ORDERSTATUS.value='PLACED'),
		date '2012-01-07',
		'1'
        );


INSERT INTO ORDER_LOG (ID,FROM_CLASS,CUSTOMERORDER_ID,ORDERSTATUS_ID,UPDATETIMESTAMP,VERSION)
        VALUES(hibernate_sequence.nextval,
                'OrderLog',
                (SELECT CUSTOMERORDER.id FROM CUSTOMERORDER 
			WHERE customerOrder.ESTIMATEDSHIPPINGAMOUNT=1339 AND rownum = 1),
                (SELECT ORDERSTATUS.id FROM ORDERSTATUS 
			WHERE ORDERSTATUS.value='PLACED'),
		date '2012-02-07',
		'1'
        );

INSERT INTO ORDER_LOG (ID,FROM_CLASS,CUSTOMERORDER_ID,ORDERSTATUS_ID,UPDATETIMESTAMP,VERSION)
        VALUES(hibernate_sequence.nextval,
                'OrderLog',
                (SELECT CUSTOMERORDER.id FROM CUSTOMERORDER 
			WHERE customerOrder.ESTIMATEDSHIPPINGAMOUNT=40 AND rownum = 1),
                (SELECT ORDERSTATUS.id FROM ORDERSTATUS 
			WHERE ORDERSTATUS.value='SHIPPED'),
		date '2013-03-07',
		'1'
        );
INSERT INTO ORDER_LOG (ID,FROM_CLASS,CUSTOMERORDER_ID,ORDERSTATUS_ID,UPDATETIMESTAMP,VERSION)
        VALUES(hibernate_sequence.nextval,
                'OrderLog',
                (SELECT CUSTOMERORDER.id FROM CUSTOMERORDER 
			WHERE customerOrder.ESTIMATEDSHIPPINGAMOUNT=50 AND rownum = 1),
                (SELECT ORDERSTATUS.id FROM ORDERSTATUS 
			WHERE ORDERSTATUS.value='SHIPPED'),
		date '2013-03-07',
		'1'
        );
INSERT INTO ORDER_LOG (ID,FROM_CLASS,CUSTOMERORDER_ID,ORDERSTATUS_ID,UPDATETIMESTAMP,VERSION)
        VALUES(hibernate_sequence.nextval,
                'OrderLog',
                (SELECT CUSTOMERORDER.id FROM CUSTOMERORDER 
			WHERE customerOrder.ESTIMATEDSHIPPINGAMOUNT=60 AND rownum = 1),
                (SELECT ORDERSTATUS.id FROM ORDERSTATUS 
			WHERE ORDERSTATUS.value='SHIPPED'),
		date '2013-03-07',
		'1'
        );
INSERT INTO ORDER_LOG (ID,FROM_CLASS,CUSTOMERORDER_ID,ORDERSTATUS_ID,UPDATETIMESTAMP,VERSION)
        VALUES(hibernate_sequence.nextval,
                'OrderLog',
                (SELECT CUSTOMERORDER.id FROM CUSTOMERORDER 
			WHERE customerOrder.ESTIMATEDSHIPPINGAMOUNT=70 AND rownum = 1),
                (SELECT ORDERSTATUS.id FROM ORDERSTATUS 
			WHERE ORDERSTATUS.value='SHIPPED'),
		date '2013-03-07',
		'1'
        );
INSERT INTO ORDER_LOG (ID,FROM_CLASS,CUSTOMERORDER_ID,ORDERSTATUS_ID,UPDATETIMESTAMP,VERSION)
        VALUES(hibernate_sequence.nextval,
                'OrderLog',
                (SELECT CUSTOMERORDER.id FROM CUSTOMERORDER 
			WHERE customerOrder.ESTIMATEDSHIPPINGAMOUNT=80 AND rownum = 1),
                (SELECT ORDERSTATUS.id FROM ORDERSTATUS 
			WHERE ORDERSTATUS.value='SHIPPED'),
		date '2013-03-07',
		'1'
        );
INSERT INTO ORDER_LOG (ID,FROM_CLASS,CUSTOMERORDER_ID,ORDERSTATUS_ID,UPDATETIMESTAMP,VERSION)
        VALUES(hibernate_sequence.nextval,
                'OrderLog',
                (SELECT CUSTOMERORDER.id FROM CUSTOMERORDER 
			WHERE customerOrder.ESTIMATEDSHIPPINGAMOUNT=90 AND rownum = 1),
                (SELECT ORDERSTATUS.id FROM ORDERSTATUS 
			WHERE ORDERSTATUS.value='SHIPPED'),
		date '2013-03-07',
		'1'
        );



