INSERT INTO TAXTYPE (ID, NAME, VERSION)
	VALUES (hibernate_sequence.nextval, 'State Tax', 0);

INSERT INTO TAXTYPE (ID, NAME, VERSION)
	VALUES (hibernate_sequence.nextval, 'County Tax', 0);

INSERT INTO TAXTYPE (ID, NAME, VERSION)
	VALUES (hibernate_sequence.nextval, 'City Tax', 0);

INSERT INTO TAXTYPE (ID, NAME, VERSION)
	VALUES (hibernate_sequence.nextval, 'District Tax', 0);

INSERT INTO TAXTYPE (ID, NAME, VERSION)
	VALUES (hibernate_sequence.nextval, 'County Local Tax', 0);

INSERT INTO TAXTYPE (ID, NAME, VERSION)
	VALUES (hibernate_sequence.nextval, 'City Local Tax', 0);