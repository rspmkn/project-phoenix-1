--/* Use temp table to store PaymentInformation Ids */
INSERT INTO IDS_USED (NAME, ID) VALUES ('cmc_phone', hibernate_sequence.nextval);
INSERT INTO IDS_USED (NAME, ID) VALUES ('cmc_address', hibernate_sequence.nextval);
INSERT INTO IDS_USED (NAME, ID) VALUES ('cmc_ped_phone', hibernate_sequence.nextval);
INSERT INTO IDS_USED (NAME, ID) VALUES ('cmc_ped_phone2', hibernate_sequence.nextval);
INSERT INTO IDS_USED (NAME, ID) VALUES ('cmc_ped_address', hibernate_sequence.nextval);

INSERT INTO IDS_USED (NAME, ID) VALUES ('cintas_address', hibernate_sequence.nextval);
INSERT INTO IDS_USED (NAME, ID) VALUES ('cintas_address_young', hibernate_sequence.nextval);
INSERT INTO IDS_USED (NAME, ID) VALUES ('cintas_phone', hibernate_sequence.nextval);
INSERT INTO IDS_USED (NAME, ID) VALUES ('cintas_young_phone', hibernate_sequence.nextval);
INSERT INTO IDS_USED (NAME, ID) VALUES ('cintas_young_phone2', hibernate_sequence.nextval);
INSERT INTO IDS_USED (NAME, ID) VALUES ('cintas_young_phone3', hibernate_sequence.nextval);

-- Phone Numbers are assigned to only one of 3 entities at the time (Account, Person, or Vendor)
-- The same number may exist for multiple entities, but they would be multiple entries in the database 
INSERT INTO PHONENUMBER (ID, AREACODE, COUNTRYCODE, EXCHANGE, EXTENSION, LINENUMBER, VERSION, ACCOUNT_ID, PERSON_ID, TYPE_ID, VENDOR_ID) 
VALUES ((SELECT var.ID FROM IDS_USED var WHERE var.NAME LIKE 'cmc_ped_phone'), 
'704', '1', '381', '', '9920', 0, 
(select a.id from ACCOUNT a where a.NAME = 'CMC Main- Pediatric Hemo/Oncology'), 
null, 
(select t.id from PHONENUMBERTYPE t where t.NAME like 'Work'), 
null);

INSERT INTO PHONENUMBER (ID, AREACODE, COUNTRYCODE, EXCHANGE, EXTENSION, LINENUMBER, VERSION, ACCOUNT_ID, PERSON_ID, TYPE_ID, VENDOR_ID) 
VALUES (hibernate_sequence.nextval, 
'704', '1', '381', '', '9920', 0, 
null, 
(select p.id from PERSON p where p.EMAIL like 'ebuzz@cmc.com'), 
(select t.id from PHONENUMBERTYPE t where t.NAME like 'Work'), 
null);

INSERT INTO PHONENUMBER (ID, AREACODE, COUNTRYCODE, EXCHANGE, EXTENSION, LINENUMBER, VERSION, ACCOUNT_ID, PERSON_ID, TYPE_ID, VENDOR_ID) 
VALUES ((SELECT var.ID FROM IDS_USED var WHERE var.NAME LIKE 'cmc_ped_phone2'), 
'704', '1', '555', '', '9921', 0, 
(select a.id from ACCOUNT a where a.NAME = 'CMC Main- Pediatric Hemo/Oncology'), 
null, 
(select t.id from PHONENUMBERTYPE t where t.NAME like 'Fax'), 
null);

INSERT INTO PHONENUMBER (ID, AREACODE, COUNTRYCODE, EXCHANGE, EXTENSION, LINENUMBER, VERSION, ACCOUNT_ID, PERSON_ID, TYPE_ID, VENDOR_ID) 
VALUES (hibernate_sequence.nextval, 
'704', '1', '555', '', '9921', 0, 
null, 
(select p.id from PERSON p where p.EMAIL like 'ebuzz@cmc.com'), 
(select t.id from PHONENUMBERTYPE t where t.NAME like 'Fax'), 
null);


INSERT INTO PHONENUMBER (ID, AREACODE, COUNTRYCODE, EXCHANGE, EXTENSION, LINENUMBER, VERSION, ACCOUNT_ID, PERSON_ID, TYPE_ID, VENDOR_ID) 
VALUES ((SELECT var.ID FROM IDS_USED var WHERE var.NAME LIKE 'cintas_young_phone'), 
'704', '1', '522', '', '9411', 0, 
null, 
(SELECT var.ID FROM IDS_USED var WHERE var.NAME LIKE 'person_id'), 
(select t.id from PHONENUMBERTYPE t where t.NAME like 'Cell'), 
null);

INSERT INTO PHONENUMBER (ID, AREACODE, COUNTRYCODE, EXCHANGE, EXTENSION, LINENUMBER, VERSION, ACCOUNT_ID, PERSON_ID, TYPE_ID, VENDOR_ID) 
VALUES ((SELECT var.ID FROM IDS_USED var WHERE var.NAME LIKE 'cintas_young_phone2'), 
'704', '1', '555', '', '3467', 0, 
null, 
(SELECT var.ID FROM IDS_USED var WHERE var.NAME LIKE 'person_id'), 
(select t.id from PHONENUMBERTYPE t where t.NAME like 'Home'), 
null);

INSERT INTO PHONENUMBER (ID, AREACODE, COUNTRYCODE, EXCHANGE, EXTENSION, LINENUMBER, VERSION, ACCOUNT_ID, PERSON_ID, TYPE_ID, VENDOR_ID) 
VALUES ((SELECT var.ID FROM IDS_USED var WHERE var.NAME LIKE 'cintas_young_phone3'), 
'704', '1', '555', '', '9811', 0, 
(select a.id from ACCOUNT a where a.NAME = 'Cintas Youngstown'), 
null, 
(select t.id from PHONENUMBERTYPE t where t.NAME like 'Work'), 
null);

INSERT INTO PHONENUMBER (ID, AREACODE, COUNTRYCODE, EXCHANGE, EXTENSION, LINENUMBER, VERSION, ACCOUNT_ID, PERSON_ID, TYPE_ID, VENDOR_ID) 
VALUES (hibernate_sequence.nextval, 
'704', '1', '555', '', '9811', 0, 
null, 
(SELECT var.ID FROM IDS_USED var WHERE var.NAME LIKE 'person_id'), 
(select t.id from PHONENUMBERTYPE t where t.NAME like 'Work'), 
null);

--CMC address example
INSERT INTO ADDRESS (ID, CITY, LINE1, LINE2, STATE, ZIP, PRIMARY, VERSION, ACCOUNT_ID, PERSON_ID, TYPE_ID, VENDOR_ID)
        VALUES (hibernate_sequence.nextval, 'Charlotte', 
        '1000 Blythe Blvd', 'MCP 6th Floor', 'NC', '28203-5812', 1, 0, 
		(select a.id from ACCOUNT a where a.NAME = 'CMC Main- Pediatric Hemo/Oncology'), 
		null,
        (SELECT t.ID FROM ADDRESSTYPE t WHERE t.NAME LIKE 'Work'), 
		null);
		
INSERT INTO ADDRESS (ID, CITY, LINE1, LINE2, STATE, ZIP, PRIMARY, VERSION, ACCOUNT_ID, PERSON_ID, TYPE_ID, VENDOR_ID)
        VALUES ((SELECT var.ID FROM IDS_USED var WHERE var.NAME LIKE 'cmc_ped_address'), 'Charlotte', 
        '1000 Blythe Blvd', 'MCP 6th Floor', 'NC', '28203-5812', 1, 0, 
		null, 
		(select p.id from PERSON p where p.EMAIL like 'ebuzz@cmc.com'),
        (SELECT t.ID FROM ADDRESSTYPE t WHERE t.NAME LIKE 'Work'), 
		null);

INSERT INTO ADDRESSXADDRESSPROPERTYTYPE (ID, VALUE, VERSION, ADDRESS_ID, TYPE_ID) 
	VALUES (hibernate_sequence.nextval, '0001622800', 0, 
	(SELECT var.ID FROM IDS_USED var WHERE var.NAME LIKE 'cmc_ped_address'), 
	(SELECT t.ID FROM ADDRESSPROPERTYTYPE t WHERE t.NAME = 'Cost Center'));
	
INSERT INTO ADDRESSXADDRESSPROPERTYTYPE (ID, VALUE, VERSION, ADDRESS_ID, TYPE_ID) 
	VALUES (hibernate_sequence.nextval, '201666', 0, 
	(SELECT var.ID FROM IDS_USED var WHERE var.NAME LIKE 'cmc_ped_address'),
	(SELECT t.ID FROM ADDRESSPROPERTYTYPE t WHERE t.NAME = 'US Account Number'));
	
INSERT INTO ADDRESSXADDRESSPROPERTYTYPE (ID, VALUE, VERSION, ADDRESS_ID, TYPE_ID) 
	VALUES (hibernate_sequence.nextval, 'CLT', 0, 
	(SELECT var.ID FROM IDS_USED var WHERE var.NAME LIKE 'cmc_ped_address'),
	(SELECT t.ID FROM ADDRESSPROPERTYTYPE t WHERE t.NAME = 'Airport Code'));
	
INSERT INTO ADDRESSXADDRESSPROPERTYTYPE (ID, VALUE, VERSION, ADDRESS_ID, TYPE_ID) 
	VALUES (hibernate_sequence.nextval, 'ZZ:CSS', 0, 
	(SELECT var.ID FROM IDS_USED var WHERE var.NAME LIKE 'cmc_ped_address'),
	(SELECT t.ID FROM ADDRESSPROPERTYTYPE t WHERE t.NAME = 'APD EDI ID'));
	
INSERT INTO ADDRESSXADDRESSPROPERTYTYPE (ID, VALUE, VERSION, ADDRESS_ID, TYPE_ID) 
	VALUES (hibernate_sequence.nextval, 'MCP 6th Floor', 0, 
	(SELECT var.ID FROM IDS_USED var WHERE var.NAME LIKE 'cmc_ped_address'),
	(SELECT t.ID FROM ADDRESSPROPERTYTYPE t WHERE t.NAME = 'Desktop'));
	
INSERT INTO ADDRESSXADDRESSPROPERTYTYPE (ID, VALUE, VERSION, ADDRESS_ID, TYPE_ID) 
	VALUES (hibernate_sequence.nextval, 'Pediatric Hemo/Oncology', 0, 
	(SELECT var.ID FROM IDS_USED var WHERE var.NAME LIKE 'cmc_ped_address'), 
	(SELECT t.ID FROM ADDRESSPROPERTYTYPE t WHERE t.NAME = 'Building/Department'));


--cintas young address
INSERT INTO ADDRESS (ID, CITY, LINE1, LINE2, STATE, ZIP, PRIMARY, VERSION, ACCOUNT_ID, PERSON_ID, TYPE_ID, VENDOR_ID)
        VALUES ((SELECT var.ID FROM IDS_USED var WHERE var.NAME LIKE 'cintas_address_young'), 'NORTH JACKSON', 
        '12601 Debartolo Dr.', null, 'OH', '44451', 1, 0, 
		(select a.id from ACCOUNT a where a.NAME = 'Cintas Youngstown'),
		null, 
        (SELECT t.ID FROM ADDRESSTYPE t WHERE t.NAME LIKE 'SHIPTO'), 
		null);
		
INSERT INTO ADDRESS (ID, CITY, LINE1, LINE2, STATE, ZIP, PRIMARY, VERSION, ACCOUNT_ID, PERSON_ID, TYPE_ID, VENDOR_ID)
        VALUES (hibernate_sequence.nextval, 'NORTH JACKSON', 
        '12601 Debartolo Dr.', null, 'OH', '44451', 1, 0, 
		null,
		(SELECT var.ID FROM IDS_USED var WHERE var.NAME LIKE 'person_id'), 
        (SELECT t.ID FROM ADDRESSTYPE t WHERE t.NAME LIKE 'SHIPTO'), 
		null);
        