INSERT INTO INVOICESENDMETHOD (ID, NAME, VERSION) VALUES (hibernate_sequence.nextval, 'Email Payment Processor', 0);
INSERT INTO INVOICESENDMETHOD (ID, NAME, VERSION) VALUES (hibernate_sequence.nextval, 'Email User', 0);
INSERT INTO INVOICESENDMETHOD (ID, NAME, VERSION) VALUES (hibernate_sequence.nextval, 'Mail', 0);
INSERT INTO INVOICESENDMETHOD (ID, NAME, VERSION) VALUES (hibernate_sequence.nextval, 'cXML', 0);
INSERT INTO INVOICESENDMETHOD (ID, NAME, VERSION) VALUES (hibernate_sequence.nextval, 'EDI', 0);
INSERT INTO INVOICESENDMETHOD (ID, NAME, VERSION) VALUES (hibernate_sequence.nextval, 'Summary Bill', 0);

INSERT INTO PAYMENTTYPE (ID, NAME, VERSION) VALUES (hibernate_sequence.nextval, 'Invoice', 0);
INSERT INTO PAYMENTTYPE (ID, NAME, VERSION) VALUES (hibernate_sequence.nextval, 'Credit Card', 0);