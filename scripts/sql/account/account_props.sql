INSERT INTO ACCOUNTGROUP (ID, NAME, VERSION) VALUES (hibernate_sequence.nextval, 'Clinic', 0);
INSERT INTO ACCOUNTGROUP (ID, NAME, VERSION) VALUES (hibernate_sequence.nextval, 'Hospital', 0);
INSERT INTO ACCOUNTGROUP (ID, NAME, VERSION) VALUES (hibernate_sequence.nextval, 'Commercial', 0);
INSERT INTO ACCOUNTGROUP (ID, NAME, VERSION) VALUES (hibernate_sequence.nextval, 'Government', 0);
INSERT INTO ACCOUNTGROUP (ID, NAME, VERSION) VALUES (hibernate_sequence.nextval, 'Novant', 0);
INSERT INTO ACCOUNTGROUP (ID, NAME, VERSION) VALUES (hibernate_sequence.nextval, 'CSS', 0);

INSERT INTO ACCOUNTPROPERTYTYPE (ID, NAME, VERSION) VALUES (hibernate_sequence.nextval, 'BillTo Name', 0);
--INSERT INTO ACCOUNTPROPERTYTYPE (ID, NAME, VERSION) VALUES (hibernate_sequence.nextval, 'APD Vendor Id', 0); 
INSERT INTO ACCOUNTPROPERTYTYPE (ID, NAME, VERSION) VALUES (hibernate_sequence.nextval, 'Invoice Batch', 0);
INSERT INTO ACCOUNTPROPERTYTYPE (ID, NAME, VERSION) VALUES (hibernate_sequence.nextval, 'Subdomain', 0);
INSERT INTO ACCOUNTPROPERTYTYPE (ID, NAME, VERSION) VALUES (hibernate_sequence.nextval, 'Can Edit Customer PO', 0);

INSERT INTO ACCOUNTPROPERTYTYPE (ID, NAME, VERSION) VALUES (hibernate_sequence.nextval, 'minimum order amount', 0);
INSERT INTO ACCOUNTPROPERTYTYPE (ID, NAME, VERSION) VALUES (hibernate_sequence.nextval, 'minimum order fee', 0);

INSERT INTO ACCOUNTPROPERTYTYPE (ID, NAME, VERSION) VALUES (hibernate_sequence.nextval, 'user registration domain', 0);
INSERT INTO ACCOUNTPROPERTYTYPE (ID, NAME, VERSION) VALUES (hibernate_sequence.nextval, 'hide user registration link', 0);
INSERT INTO ACCOUNTPROPERTYTYPE (ID, NAME, VERSION) VALUES (hibernate_sequence.nextval, 'authorized shipto prefix', 0);
