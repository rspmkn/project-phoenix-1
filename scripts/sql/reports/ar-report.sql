
SELECT 
thisAccount.name AS "Account Name", 
rootAccount.name AS "Customer Name", 
systemuser.login AS "User", 
costCenter.name AS "Cost Center Name", 
apdPo.value AS "APD PO", 
customerPo.value AS "Customer PO", 
shipment.trackingNumber AS "Tracking Number",
invoice.invoiceNumber AS "Invoice Number", 
thisAccount.id AS "Accounting ID", 
glAccount.VALUE AS "GL Account Number", 
subAccount.VALUE AS "AR Subaccount Number", 
COALESCE(shipment.subTotalPrice, SUM(LINEITEMXRETURN.QUANTITY * COALESCE(LINEITEM.UNITPRICE, 0))) AS "Merchandise Price", 
COALESCE(shipment.totalTaxPrice, 0) AS "Tax", 
COALESCE(shipment.totalShippingPrice, RETURNORDER.RESTOCKINGFEE) AS "Shipping, Restocking Fee", 
COALESCE(shipment.subTotalCost, 0) AS "APD Cost", 
billAddress.company AS "Bill Company Name", 
shipAddress.state AS "Shipto State", 
shipAddress.zip AS "Shipto ZIP", 
customerorder.orderDate AS "Order Date", 
shipment.shipTime AS "Ship Date", 
invoice.createdDate AS "Invoice Date" 
FROM INVOICE 
LEFT JOIN SHIPMENT ON (INVOICE.SHIPMENT_ID = SHIPMENT.ID)
LEFT JOIN RETURNORDER ON (INVOICE.RETURNORDER_ID = RETURNORDER.ID)
LEFT JOIN CUSTOMERORDER ON (RETURNORDER.ORDER_ID = CUSTOMERORDER.ID OR SHIPMENT.ORDER_ID = CUSTOMERORDER.ID)
LEFT JOIN ADDRESS shipAddress ON (CUSTOMERORDER.ADDRESS_ID = shipAddress.ID)
LEFT JOIN PAYMENTINFORMATION ON (CUSTOMERORDER.PAYMENTINFORMATION_ID = PAYMENTINFORMATION.ID)
LEFT JOIN ADDRESS billAddress ON (PAYMENTINFORMATION.BILLINGADDRESS_ID = billAddress.ID)
LEFT JOIN PONUMBER_CUSTOMERORDER apdPoJoin 
	JOIN PONUMBERTYPE apdPoType ON (apdPoType.NAME = 'APD')
	JOIN PONUMBER apdPo 
	ON (apdPo.ID = apdPoJoin.PONUMBERS_ID AND apdPo.TYPE_ID = apdPoType.ID)
ON (CUSTOMERORDER.ID = apdPoJoin.ORDERS_ID)
LEFT JOIN PONUMBER_CUSTOMERORDER customerPoJoin 
	JOIN PONUMBERTYPE customerPoType ON (customerPoType.NAME = 'Customer')
	JOIN PONUMBER customerPo 
	ON (customerPo.ID = customerPoJoin.PONUMBERS_ID AND customerPo.TYPE_ID = customerPoType.ID)
ON (CUSTOMERORDER.ID = customerPoJoin.ORDERS_ID)
LEFT JOIN CREDENTIAL ON (CUSTOMERORDER.CREDENTIAL_ID = CREDENTIAL.ID)
LEFT JOIN Credential_PropertyType glAccount 
	JOIN CREDENTIALPROPERTYTYPE glCredentialPropertyType 
	ON (glAccount.TYPE_ID = glCredentialPropertyType.ID AND glCredentialPropertyType.NAME = 'AR GL account')
ON (glAccount.CRED_ID = CREDENTIAL.ID)
LEFT JOIN Credential_PropertyType subAccount 
	JOIN CREDENTIALPROPERTYTYPE subCredentialPropertyType 
	ON (subAccount.TYPE_ID = subCredentialPropertyType.ID AND subCredentialPropertyType.NAME = 'AR subaccount')
ON (subAccount.CRED_ID = CREDENTIAL.ID)
LEFT JOIN ACCOUNT thisAccount ON (thisAccount.ID = CUSTOMERORDER.ACCOUNT_ID)
LEFT JOIN ACCOUNT rootAccount ON (thisAccount.ROOTACCOUNT_ID = rootAccount.ID OR (thisAccount.ID = rootAccount.ID AND thisAccount.ROOTACCOUNT_ID IS NULL))
LEFT JOIN SYSTEMUSER ON (SYSTEMUSER.ID = CUSTOMERORDER.USER_ID)
LEFT JOIN ACCXCREDXUSER_CCENTER ON (ACCXCREDXUSER_CCENTER.ID = CUSTOMERORDER.ASSIGNEDCOSTCENTER_ID)
LEFT JOIN COSTCENTER ON (ACCXCREDXUSER_CCENTER.ALLOWEDCOSTCENTERS_ID = COSTCENTER.ID)
LEFT JOIN LINEITEMXRETURN ON (RETURNORDER.ID = LINEITEMXRETURN.RETURNORDER_ID)
LEFT JOIN LINEITEM ON (LINEITEMXRETURN.LINEITEMS_ID = LINEITEM.ID)
WHERE (invoice.FROM_CLASS = 'CUSTOMER_CREDIT_INVOICE' OR invoice.FROM_CLASS = 'CUSTOMER_INVOICE')
GROUP BY INVOICE.ID, thisAccount.name, rootAccount.name, systemuser.login, costCenter.name, apdPo.value, customerPo.value, shipment.trackingNumber,
invoice.invoiceNumber, thisAccount.id, glAccount.VALUE, subAccount.VALUE, shipment.subTotalPrice, shipment.totalTaxPrice, shipment.totalShippingPrice, 
RETURNORDER.RESTOCKINGFEE, shipment.subTotalCost, billAddress.company, shipAddress.state, shipAddress.zip, customerorder.orderDate, shipment.shipTime, 
invoice.createdDate;

