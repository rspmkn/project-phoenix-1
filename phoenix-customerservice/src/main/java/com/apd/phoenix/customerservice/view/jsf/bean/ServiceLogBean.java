/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.customerservice.view.jsf.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.SessionContext;
import javax.ejb.Stateful;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.ConversationScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.apd.phoenix.customerservice.view.jsf.bean.login.LoginBean;
import com.apd.phoenix.service.business.AccountBp;
import com.apd.phoenix.service.business.ContactLogBp;
import com.apd.phoenix.service.business.ContactMethodBp;
import com.apd.phoenix.service.business.ServiceLogBp;
import com.apd.phoenix.service.business.ServiceLogSearchCriteria;
import com.apd.phoenix.service.business.ServiceReasonBp;
import com.apd.phoenix.service.model.Account;
import com.apd.phoenix.service.model.ContactMethod;
import com.apd.phoenix.service.model.CustomerOrder;
import com.apd.phoenix.service.model.ServiceLog;
import com.apd.phoenix.service.model.ServiceReason;
import com.apd.phoenix.service.model.ContactLog.TicketStatus;
import com.apd.phoenix.web.FileUploadBean;

@Named
@Stateful
@ConversationScoped
@Lock(LockType.READ)
public class ServiceLogBean implements Serializable {

	private static final int MAX_AGE_OF_ACCOUNT_LOGS = 30;

	private static final long serialVersionUID = -2976447088563969031L;

	private static final Logger LOG = LoggerFactory.getLogger(ServiceLogBean.class);

    @Inject
    private ServiceLogBp serviceLogBp;
    @Inject
    private ContactMethodBp contactMethodBp;
    @Inject
    private ServiceReasonBp serviceReasonBp;
    @Inject
    private AccountBp accountBp;
    @Inject
    private LoginBean loginBean;
    @Inject
    private ContactLogBean contactLogBean;
    @Inject
    private ContactLogBp contactLogBp;    
	@Inject
	private FileUploadBean fileUploadBean;

    private ServiceLog currentServiceLog;

    private String ticketNumber;
    private String accountName;
    private String exactAccountName;
    private String department;
    private String contactName;
    private String contactNumber;
    private String contactEmail;
    private String apdRep;
    private CustomerOrder customerOrder;
    private Date startDate;
    private Date endDate;
    private boolean includeDetails = true;
    private boolean csvDownload = false;
    private String poNumber;
    private ServiceLogStatus serviceLogStatus = ServiceLogStatus.ALL;
    private boolean showingAccountLogs = false;
    
    private List<ServiceLog> serviceLogs = new ArrayList<>();

    @Inject
    private Conversation conversation;

    public enum ServiceLogStatus {
        ALL, OPEN, CLOSED;
    }
    
    @PostConstruct
    public void init() {
        //nothing yet
    }

    private void beginConversation() {
        if (conversation.isTransient()) {
            conversation.begin();
        }
    }

    @Lock(LockType.WRITE)
	public void save() {
    	this.currentServiceLog.setUpdateTimestamp(new Date());
    	if(this.currentServiceLog.getId() == null) {
    		this.serviceLogs.add(this.currentServiceLog);
    		this.currentServiceLog.setAccount(accountBp.findByName(this.accountName));
    		this.currentServiceLog.setCustomerOrder(this.customerOrder);
    		this.currentServiceLog.setTicketNumber(contactLogBp.newTicketNumber());
    		this.serviceLogBp.create(this.currentServiceLog);
		}
    	else {
    		this.serviceLogBp.update(this.currentServiceLog);
    	}
    	contactLogBean.eagerLoad(this.currentServiceLog);
    	if(!StringUtils.isEmpty(getFileUploadBean().getFileName())){
    		getFileUploadBean().saveUploadedFile(this.currentServiceLog, loginBean.getSystemUser());
    	}
	}

    @Lock(LockType.WRITE)
    public void create() {
        this.currentServiceLog = new ServiceLog();
        this.currentServiceLog.setAccount(accountBp.findByName(this.accountName));
        this.currentServiceLog.setDepartment(this.department);
        this.currentServiceLog.setCustomerOrder(this.customerOrder);
        this.currentServiceLog.setOpenedDate(new Date());
        this.currentServiceLog.setUpdateTimestamp(new Date());
        this.currentServiceLog.setFollowUpComplete(false);
        this.currentServiceLog.setStatus(TicketStatus.OPEN);
        this.currentServiceLog.setApdCsrRep(loginBean.getSystemUser());
        this.getFileUploadBean().preUploadFile();
    }

    @Lock(LockType.WRITE)
    public void resolve() {
        this.currentServiceLog.setResolvedDate(new Date());
        this.currentServiceLog.setStatus(TicketStatus.CLOSED);
        save();
    }

    @Lock(LockType.WRITE)
    public void retrieve() {
    	
        if (FacesContext.getCurrentInstance().isPostback()) {
            return;
        }
        
        beginConversation();
    }

    @Lock(LockType.WRITE)
    public String search() {
    	TicketStatus statusToSearch = null;
        if(this.serviceLogStatus == ServiceLogStatus.CLOSED) {
        	statusToSearch = TicketStatus.CLOSED;
        }
        if(this.serviceLogStatus == ServiceLogStatus.OPEN) {
        	statusToSearch = TicketStatus.OPEN;
        }
        if (this.showingAccountLogs) {
            Calendar cal = Calendar.getInstance();
            cal.add(Calendar.DATE, -1 * MAX_AGE_OF_ACCOUNT_LOGS);
        	this.startDate = cal.getTime();
        }
        
    	this.setServiceLogs(serviceLogBp.getServiceLogs(new ServiceLogSearchCriteria(ticketNumber, accountName, exactAccountName, department, contactName, contactNumber, contactEmail, apdRep, startDate, endDate, statusToSearch, poNumber, null, null, null)));
    	
		for(ServiceLog log:this.getServiceLogs()){
    		serviceLogBp.eagerLoad(log);
    	}
        
        return "serviceLogSearchResult?faces-redirect=true";
    }

    @Lock(LockType.WRITE)
    public void cancel() {
        this.currentServiceLog = new ServiceLog();
    }

    public List<ContactMethod> getContactMethodList() {
        return contactMethodBp.findAll(ContactMethod.class, 0, 0);
    }

    public List<ServiceReason> getServiceReasonList() {
        return serviceReasonBp.findAll(ServiceReason.class, 0, 0);
    }
    
    public ServiceLogStatus[] getServiceLogStatusList() {
        return ServiceLogStatus.values();
    }

    @Resource
    private SessionContext sessionContext;

    public Converter getContactMethodConverter() {

        return new Converter() {

            @Override
            public Object getAsObject(FacesContext context, UIComponent component, String value) {

                if (value.equals("")) {
                    return null;
                }

                return contactMethodBp.findById(Long.valueOf(value), ContactMethod.class);
            }

            @Override
            public String getAsString(FacesContext context, UIComponent component, Object value) {
                if (value == null) {
                    return "";
                }
                return String.valueOf(((ContactMethod) value).getId());
            }
        };
    }

    public Converter getServiceReasonConverter() {

        return new Converter() {

            @Override
            public Object getAsObject(FacesContext context, UIComponent component, String value) {

                if (value.equals("")) {
                    return null;
                }

                return serviceReasonBp.findById(Long.valueOf(value), ServiceReason.class);
            }

            @Override
            public String getAsString(FacesContext context, UIComponent component, Object value) {
                if (value == null) {
                    return "";
                }
                return String.valueOf(((ServiceReason) value).getId());
            }
        };
    }

    public ServiceLog getCurrentServiceLog() {
        return currentServiceLog;
    }

    @Lock(LockType.WRITE)
    public void setCurrentServiceLog(ServiceLog currentServiceLog) {
        this.currentServiceLog = currentServiceLog;
        if (this.currentServiceLog != null && this.currentServiceLog.getId() != null) {
        	this.currentServiceLog = this.serviceLogBp.eagerLoad(this.currentServiceLog);
        }
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

	public ServiceLogStatus getServiceLogStatus() {
		return serviceLogStatus;
	}

	public void setServiceLogStatus(ServiceLogStatus serviceLogStatus) {
		this.serviceLogStatus = serviceLogStatus;
	}

	public boolean isIncludeDetails() {
		return includeDetails;
	}

	public void setIncludeDetails(boolean includeDetails) {
		this.includeDetails = includeDetails;
	}

	public boolean isCsvDownload() {
		return csvDownload;
	}

	public void setCsvDownload(boolean csvDownload) {
		this.csvDownload = csvDownload;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public String getPoNumber() {
		return poNumber;
	}

	public void setPoNumber(String poNumber) {
		this.poNumber = poNumber;
	}

	public List<ServiceLog> getServiceLogs() {
		return this.serviceLogs;
	}

	public void setServiceLogs(List<ServiceLog> serviceLogs) {
		this.serviceLogs = serviceLogs;
	}

    public String getTicketNumber() {
		return ticketNumber;
	}

	public void setTicketNumber(String ticketNumber) {
		this.ticketNumber = ticketNumber;
	}

	public String getAccountName() {
		return accountName;
	}

	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	public void clearAccountName() {
		this.accountName = null;
	}

	public String getContactName() {
		return contactName;
	}

	public void setContactName(String contactName) {
		this.contactName = contactName;
	}

	public String getContactNumber() {
		return contactNumber;
	}

	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}

	public String getContactEmail() {
		return contactEmail;
	}

	public void setContactEmail(String contactEmail) {
		this.contactEmail = contactEmail;
	}

	public String getApdRep() {
		return apdRep;
	}

	public void setApdRep(String apdRep) {
		this.apdRep = apdRep;
	}
	
	public void setAccount(Account account) {
		if (account != null && account.getId() != null) {
			this.accountName = account.getName();
		}
	}

    @Lock(LockType.WRITE)
	public void setCustomerOrder(CustomerOrder customerOrder) {
		if (customerOrder != null) {
			this.customerOrder = customerOrder;
			if (customerOrder.getApdPo() != null) {
				this.setPoNumber(customerOrder.getApdPo().getValue());
			}
			if (customerOrder.getAccount() != null) {
				this.setAccount(customerOrder.getAccount());
			}
			this.search();
		}
	}

    @Lock(LockType.WRITE)
	public void showAccountLogs(String accountName) {
		if (StringUtils.isNotBlank(accountName)) {
			this.showingAccountLogs = true;
			this.exactAccountName = accountName;
			this.search();
		}
	}

	public FileUploadBean getFileUploadBean() {
		return fileUploadBean;
	}

	public void setFileUploadBean(FileUploadBean fileUploadBean) {
		this.fileUploadBean = fileUploadBean;
	}

}

