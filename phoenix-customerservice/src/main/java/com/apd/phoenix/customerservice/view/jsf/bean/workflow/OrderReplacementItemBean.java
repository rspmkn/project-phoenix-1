package com.apd.phoenix.customerservice.view.jsf.bean.workflow;

import java.math.BigDecimal;
import javax.ejb.Stateful;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.business.LineItemBp;
import com.apd.phoenix.service.model.Account;
import com.apd.phoenix.service.model.LineItem;

@Named
@Stateful
@RequestScoped
public class OrderReplacementItemBean extends OrderHumanTaskFormBean {

    private static final Logger logger = LoggerFactory.getLogger(OrderReplacementItemBean.class);

    @Inject
    LineItemBp lineItemBp;

    private LineItem lineItem;

    private Account account;

    @Override
    public void init() {
        long lineItemId = (Long) taskContent.get("replacementItemId");
        this.lineItem = lineItemBp.findById(lineItemId, LineItem.class);
        this.account = lineItem.getOrder().getAccount();
    }

    public String complete() {
        return this.completeOrderTask(null);
    }

    public LineItem getLineItem() {
        return lineItem;
    }

    public String getDescription() {
        return lineItem.getDescription();
    }

    public Integer getQuantity() {
        return lineItem.getQuantity();
    }

    public BigDecimal getPrice() {
        return lineItem.getCost();
    }

    public String getApdSku() {
        return lineItem.getApdSku();
    }

    public String getCustomerSku() {
        return lineItem.getCustomerSku().getValue();
    }

    public String getManufacturerPartId() {
        return lineItem.getManufacturerPartId();
    }

    public String accountName() {
        return account.getName();
    }

    public String apdVendor() {
        return account.getApdAssignedAccountId();
    }
}
