package com.apd.phoenix.customerservice.view.jsf.bean;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.business.EntityComparator;

/**
 * Utilities for working with Java Server Faces views.
 */

public final class ViewUtils {

    private static final Logger LOGGER = LoggerFactory.getLogger(ViewUtils.class);

    public static <T> List<T> asList(Collection<T> collection) {

        if (collection == null) {
            return null;
        }

        List<T> toReturn = new ArrayList<T>(collection);

        Collections.sort(toReturn, new EntityComparator());

        return toReturn;
    }

    private ViewUtils() {

        // Can never be called
    }

    //fill in OR conditions
    public static boolean isCurrencyType(String rowType) {
        if (/**/rowType.equals("") || /**/rowType.equals("")) {
            return true;
        }
        return false;
    }

    public static String toCurrency(String rawValue) {

        if (StringUtils.isBlank(rawValue)) {
            return "";
        }

        double doubleValue = 0.0;

        try {
            doubleValue = Double.parseDouble(rawValue);
        }
        catch (Exception e) {
            LOGGER.debug("Could not parse double.", e);
            return "[NaN: " + rawValue + " ]";
        }

        DecimalFormat formatter = new DecimalFormat("$ #,##0.00");
        formatter.setRoundingMode(RoundingMode.HALF_UP);

        String currencyFormatValue = formatter.format(doubleValue);
        return currencyFormatValue;
    }
}
