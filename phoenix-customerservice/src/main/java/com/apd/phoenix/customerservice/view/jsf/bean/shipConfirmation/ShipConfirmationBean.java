package com.apd.phoenix.customerservice.view.jsf.bean.shipConfirmation;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;

import javax.ejb.Stateful;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.ConversationScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.apd.phoenix.core.utility.TenantConfigRepository;
import com.apd.phoenix.service.business.CustomerOrderBp;
import com.apd.phoenix.service.business.LineItemBp;
import com.apd.phoenix.service.message.api.MessageService;
import com.apd.phoenix.service.model.Account;
import com.apd.phoenix.service.model.Credential.Terms;
import com.apd.phoenix.service.model.CustomerOrder;
import com.apd.phoenix.service.model.Item;
import com.apd.phoenix.service.model.LineItem;
import com.apd.phoenix.service.model.MessageMetadata.MessageType;
import com.apd.phoenix.service.model.Pick;
import com.apd.phoenix.service.model.ShipManifest;
import com.apd.phoenix.service.model.ShipManifest.ManifestType;
import com.apd.phoenix.service.model.dto.AddressDto;
import com.apd.phoenix.service.model.dto.AdvanceShipmentNoticeDto;
import com.apd.phoenix.service.model.dto.LineItemXShipmentDto;
import com.apd.phoenix.service.model.dto.PickDto;
import com.apd.phoenix.service.model.dto.ShipmentDto;
import com.apd.phoenix.service.model.dto.ShippingPartnerDto;
import com.apd.phoenix.service.model.factory.DtoFactory;
import com.apd.phoenix.service.persistence.jpa.PickDao;
import com.apd.phoenix.service.persistence.jpa.ShipManifestDao;
import com.apd.phoenix.service.persistence.multitenancy.impl.CurrentTenantIdentifierResolverImpl;
import com.apd.phoenix.service.workflow.WorkflowService;

@Named
@Stateful
@ConversationScoped
public class ShipConfirmationBean {
	
	private static final String ITEM_WEIGHT = "Item Weight";

	private static final Logger LOG = LoggerFactory
			.getLogger(ShipConfirmationBean.class);

    @Inject
    PickDao pickDao;

    @Inject
    ShipManifestDao shipManifestDao;

    @Inject
    CustomerOrderBp customerOrderBp;

    @Inject
    WorkflowService workflowService;

    private String shipperId;

    private String apdPo;

    private List<Pick> searchResults;

    private CustomerOrder order;

    private int boxes = 0;
    
    private List<String> errors = new ArrayList<>();

    @Inject
    private Conversation conversation;

    @Inject
    private MessageService messageService;
    
    @Inject
    private LineItemBp lineItemBp;

    private static final String APD_SHIPPING_PARTNER = "csr.address.name";

    private static final String APD_STREET = "csr.address.street";

    private static final String APD_CITY = "csr.address.city";

    private static final String APD_STATE = "csr.address.state";

    private static final String APD_ZIP = "csr.address.zip";

    private void endConversation() {
        if (!conversation.isTransient()) {
            conversation.end();
        }
    }

    private void beginConversation() {
        if (conversation.isTransient()) {
            conversation.begin();
        }
    }

    public void search() {
        searchResults = pickDao.findPicksByShipperIdOrApdPo(shipperId, apdPo);

        if (searchResults != null && searchResults.size() > 0) {
            order = searchResults.get(0).getLineItem().getOrder();
            order = customerOrderBp.hydrateForOrderDetails(order);

        }
        beginConversation();
    }

    public String confirm() {
    	if(!validate()) {
    		return "";
    	}
    	List<PickDto> pickList = new ArrayList<>();
    	
    	//Only create ship manifest for certain customers
    	Properties tenantProperties = TenantConfigRepository.getInstance().getPropertiesByTenantId(CurrentTenantIdentifierResolverImpl.getCurrentTenant(), "tenant");
    	String manifestCustomers = tenantProperties.getProperty("shipManifestCustomers");
    	String[] manifestCustomersList = manifestCustomers.split(",");
    	Account account = order.getAccount().getRootAccount() != null ? order.getAccount().getRootAccount() : order.getAccount();
    	if(StringUtils.isNotEmpty(account.getName())
    		&& isInList(account.getName(), manifestCustomersList)) {
    		LOG.info("Manifesting customer {}", account.getName());
    	    	ShipManifest shipManifest = new ShipManifest();
    	    	shipManifest.setNumBoxes(BigInteger.valueOf(getBoxes()));
    	    	shipManifest.setPicks(new HashSet<Pick>(searchResults));
    	    	shipManifest.setManifestDate(new Date());
    	    	shipManifest.setType(ManifestType.APD);
    	    	shipManifest.setCustomerOrder(order);
    	    	shipManifestDao.create(shipManifest);
    	} else {
    		LOG.info("Customer "+account.getName()+" will not be manifested.");
    	}
    	

        for (Pick pick : searchResults) {
            pick.setShipped(true);
            pick = pickDao.update(pick);
            
            pickList.add(DtoFactory.createPickDto(pick));
        }
        messageService.sendShipConfirmation(pickList, MessageType.CSV);
        //Workflow
        AdvanceShipmentNoticeDto advanceShipmentNoticeDto = createAdvanceShipmentNoticeDto(pickList);
        workflowService.processAdvancedShipmentNotice(advanceShipmentNoticeDto);
        
        return "packingSlip.xhtml?faces-redirect=true";
    }
    
    private boolean isInList(String name, String[] customerList) {
    	for(String customerName : customerList) {
    		if(name.equals(customerName)) {
    			return true;
    		}	
    	}
    	return false;
    }

    private boolean validate() {
    	errors = new ArrayList<>();
		if(order == null) {
			errors.add("No order was found for pick confirmation.");
		}
		if(boxes <= 0) {
			errors.add("The number of boxes must be greater than 0.");
		}
		for(Pick pick : searchResults) {
			if(BigInteger.valueOf(pick.getLineItem().getQuantity()).compareTo(pick.getQtyToPick()) == -1 ) {
				errors.add("Cannot ship confirm more than the ordered quantity.");
			}
		}
		if(!errors.isEmpty()) {
			return false;
		}
		return true;
	}

	public String resetSearch() {
        endConversation();
        return "pickList.xhtml?faces-redirect=true";
    }

    public String printPackingSlip() {
        return "printPackingSlip.xhtml?faces-redirect=true";
    }
    
    public String reprintPackingSlip() {
        searchResults = pickDao.findShippedPicksByShipperIdOrApdPo(shipperId, apdPo);

        if (searchResults != null && searchResults.size() > 0) {
            order = searchResults.get(0).getLineItem().getOrder();
            order = customerOrderBp.hydrateForOrderDetails(order);

        }
        beginConversation();
    	if (searchResults != null && searchResults.size() > 0) {
    		return this.printPackingSlip();
    	}
    	else {
    		return null;
    	}
    }

    public BigDecimal computeExtPrice(BigDecimal unitPrice, Integer qty) {
        if (unitPrice != null && qty != null) {
            return unitPrice.multiply(new BigDecimal(qty));
        }
        return BigDecimal.ZERO;
    }

    public String getShipperId() {
        return shipperId;
    }

    public void setShipperId(String shipperId) {
        this.shipperId = shipperId;
    }

    public String getApdPo() {
        return apdPo;
    }

    public void setApdPo(String apdPo) {
        this.apdPo = apdPo;
    }

    public List<Pick> getSearchResults() {
        return searchResults;
    }

    public void setSearchResults(List<Pick> searchResults) {
        this.searchResults = searchResults;
    }

    public int getBoxes() {
        return boxes;
    }

    public void setBoxes(int boxes) {
        this.boxes = boxes;
    }

    public CustomerOrder getOrder() {
        return order;
    }

    public void setOrder(CustomerOrder order) {
        this.order = order;
    }

    private AdvanceShipmentNoticeDto createAdvanceShipmentNoticeDto(List<PickDto> pickList) {
        AdvanceShipmentNoticeDto advanceShipmentNoticeDto = new AdvanceShipmentNoticeDto();
        advanceShipmentNoticeDto.setShipmentDtos(new ArrayList<ShipmentDto>());
        ShipmentDto shipmentDto = new ShipmentDto();
        ShippingPartnerDto shippingPartnerDto = new ShippingPartnerDto();
        shippingPartnerDto.setName(this.getTenantPropertyByName(APD_SHIPPING_PARTNER));
        shipmentDto.setShippingPartnerDto(shippingPartnerDto);

        AddressDto addressDto = new AddressDto();
        addressDto.setName(this.getTenantPropertyByName(APD_SHIPPING_PARTNER));
        addressDto.setLine1(this.getTenantPropertyByName(APD_STREET));
        addressDto.setCity(this.getTenantPropertyByName(APD_CITY));
        addressDto.setState(this.getTenantPropertyByName(APD_STATE));
        addressDto.setZip(this.getTenantPropertyByName(APD_ZIP));
        shipmentDto.setFromAddress(addressDto);
        shipmentDto.setLineItemXShipments(new ArrayList<LineItemXShipmentDto>());

        for (PickDto pickDto : pickList) {
        	if (shipmentDto.getTrackingNumber() == null || shipmentDto.getTrackingNumber().isEmpty()) {
        		shipmentDto.setTrackingNumber(pickDto.getShipperId());
        	}
            if (advanceShipmentNoticeDto.getApdPo() == null) {
                advanceShipmentNoticeDto.setApdPo(pickDto.getApdOrderNumber());
            }
            LineItemXShipmentDto lineItemXShipmentDto = new LineItemXShipmentDto();
            lineItemXShipmentDto.setLineItemDto(pickDto.getLineItem());
            lineItemXShipmentDto.setQuantity(pickDto.getQtyToPick());
            shipmentDto.getLineItemXShipments().add(lineItemXShipmentDto);
        }
        advanceShipmentNoticeDto.getShipmentDtos().add(shipmentDto);
        return advanceShipmentNoticeDto;
    }

	public List<String> getErrors() {
		return errors;
	}

	public void setErrors(List<String> errors) {
		this.errors = errors;
	}
    
	public String getSolomonOrderNumber() {
		if (searchResults == null || searchResults.isEmpty() || searchResults.get(0) == null) {
			return "";
		}
		return searchResults.get(0).getOrderNumber();
	}
	
	public String getSolomonCustomerId() {
		if (order != null && order.getAccount() != null && StringUtils.isNotBlank(order.getAccount().getSolomonCustomerId())) {
			return order.getAccount().getSolomonCustomerId();
		}
		return "";
	}
	
	public String getTerms() {
		if (order != null && order.getTerms() != null) {
			return order.getTerms().getLabel();
		}
		if (order != null && order.getCredential() != null && order.getCredential().getTerms() != null) {
			return order.getCredential().getTerms().getLabel();
		}
		return Terms.TEN.getLabel();
	}
	
	public String getTotalWeight() {
		BigDecimal sum = BigDecimal.ZERO;
		for (Pick pick : this.searchResults) {
			if (pick.getLineItem() != null && pick.getLineItem().getItem() != null) {
				Item item = lineItemBp.findById(pick.getLineItem().getId(), LineItem.class).getItem();
				BigDecimal weightValue = null;
				if(item.getItemWeight() != null) {
					weightValue = item.getItemWeight();
				} else if(item.getSpecificationsMap().get(ITEM_WEIGHT) != null) {
					try {
					    //used for legacy orders, moving forward will use the ItemWeight field.
						weightValue = new BigDecimal(item.getSpecificationsMap().get(ITEM_WEIGHT));
					} catch (NumberFormatException exp) {
						LOG.debug("Error parsing weight of an item from specification", exp);
					}
				}
				
				if (weightValue != null) {
					try {
						sum = sum.add(new BigDecimal(pick.getQtyToPick().toString()).multiply(weightValue));
					}
					catch (Exception e) {
						LOG.debug("Error parsing weight of an item", e);
					}
				}
			}
		}
		return sum.toPlainString();
	}
	private String getTenantPropertyByName(String propertyName) {
        return TenantConfigRepository.getInstance().getProperty(getTenantName(), "tenant", propertyName);
    }

    private String getTenantName() {
        return TenantConfigRepository.getInstance().getTenantNameById(
                CurrentTenantIdentifierResolverImpl.getCurrentTenant());
    }
}
