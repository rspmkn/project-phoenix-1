/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.customerservice.view.jsf.bean;

import java.io.Serializable;
import java.util.Date;
import javax.ejb.Stateful;
import javax.enterprise.context.ConversationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.customerservice.view.jsf.bean.login.LoginBean;
import com.apd.phoenix.service.business.IssueLogBp;
import com.apd.phoenix.service.business.ServiceLogBp;
import com.apd.phoenix.service.model.ContactLog;
import com.apd.phoenix.service.model.ContactLogComment;
import com.apd.phoenix.service.model.IssueLog;
import com.apd.phoenix.service.model.ServiceLog;

@Named
@Stateful
@ConversationScoped
public class ContactLogBean implements Serializable {

    private static final long serialVersionUID = -392601552924722711L;

    private static final Logger LOG = LoggerFactory.getLogger(ContactLogBean.class);

    @Inject
    private ServiceLogBp serviceLogBp;

    @Inject
    private IssueLogBp issueLogBp;

    @Inject
    private LoginBean loginBean;

    private ContactLog currentContactLog;
    private ContactLogComment currentComment;

    public ContactLog getCurrentContactLog() {
        return currentContactLog;
    }

    public void setCurrentContactLog(ContactLog currentContactLog) {
        this.currentContactLog = this.eagerLoad(currentContactLog);
    }

    public ContactLogComment getCurrentComment() {
        if (this.currentComment == null) {
            this.newComment();
        }
        return currentComment;
    }

    public void discardCurrentComment() {
        this.currentComment = null;
    }

    public void addComment() {
        this.currentContactLog.getComments().add(currentComment);
        this.currentComment.setCommentTimestamp(new Date());
        if (currentContactLog instanceof IssueLog) {
            this.currentContactLog = issueLogBp.update((IssueLog) currentContactLog);
        }
        if (currentContactLog instanceof ServiceLog) {
            this.currentContactLog = serviceLogBp.update((ServiceLog) currentContactLog);
        }
        this.currentContactLog = this.eagerLoad(this.currentContactLog);
        this.newComment();
    }

    private void newComment() {
        this.currentComment = new ContactLogComment();
        currentComment.setApdCsrRep(loginBean.getSystemUser());
    }

    public ContactLog eagerLoad(ContactLog toLoad) {
        if (toLoad instanceof IssueLog) {
            toLoad = issueLogBp.eagerLoad((IssueLog) toLoad);
        }
        if (toLoad instanceof ServiceLog) {
            toLoad = serviceLogBp.eagerLoad((ServiceLog) toLoad);
        }
        if (toLoad.getCustomerOrder() != null) {
            toLoad.getCustomerOrder().toString();
        }
        toLoad.getComments().size();
        for (ContactLogComment comment : toLoad.getComments()) {
            if (comment.getApdCsrRep() != null) {
                comment.getApdCsrRep().toString();
            }
        }
        return toLoad;
    }
}
