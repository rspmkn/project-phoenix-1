package com.apd.phoenix.customerservice.view.jsf.bean.workflow;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.Stateful;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.ConversationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.event.ValueChangeEvent;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.commons.lang.StringUtils;

import com.apd.phoenix.service.business.AccountXCredentialXUserBp;
import com.apd.phoenix.service.business.CustomerOrderBp;
import com.apd.phoenix.service.business.SecurityContext;
import com.apd.phoenix.service.business.SystemUserBp;
import com.apd.phoenix.service.model.AccountXCredentialXUser;
import com.apd.phoenix.service.model.Credential;
import com.apd.phoenix.service.model.CredentialXCredentialPropertyType;
import com.apd.phoenix.service.model.CustomerOrder;
import com.apd.phoenix.service.model.SystemUser;
import com.apd.phoenix.service.model.OrderStatus.OrderStatusEnum;

/**
 * This class provides backing functionality for the manual-order.xhtml page. It contains several Converters, a 
 * CustomerOrder object, and methods to add items to the order.
 * 
 * @author RHC
 *
 */
@Named
@Stateful
@ConversationScoped
public class ManualOrderSelectionBean implements Serializable {

	private static final long serialVersionUID = -2388341212538843974L;

    private String login;
    private String orderNumber;
    private boolean orderFound;
    private CustomerOrder manualOrder;
    
    private AccountXCredentialXUser axcxu = new AccountXCredentialXUser();

    private List<AccountXCredentialXUser> userAxcxus = new ArrayList<>();

    private Map<String, String> propertyMap;

	@Inject
    private Conversation conversation;

    @Inject
    private SystemUserBp systemUserBp;
    
    @Inject
    private AccountXCredentialXUserBp axcxuBp;
    
    @Inject
    private SecurityContext securityContext;
    
    @Inject
    private CustomerOrderBp customerOrderBp;
    
    @PostConstruct
    public void create() {
        if (conversation.isTransient()) {
            conversation.begin();
        }
    }

    public boolean isValidUser(String login) {
    	List<SystemUser> userList = systemUserBp.getFilteredSystemUserByLogin(login);
        return userList != null && !userList.isEmpty();
    }

    public boolean inputUserIsValidUser() {
        return isValidUser(this.login);
    }

    private UIComponent userInput;

    public UIComponent getUserInput() {
        return userInput;
    }

    public void setUserInput(UIComponent userInput) {
        this.userInput = userInput;
    }

    private UIComponent orderNumberInput;
    
    public List<AccountXCredentialXUser> getUserAxcxus() {
        if (inputUserIsValidUser()) {
            userAxcxus = axcxuBp.getAllAXCXU(systemUserBp.getFilteredSystemUserByLogin(this.login).get(0));
        }
        else {
        	userAxcxus = new ArrayList<>();
        }
        return userAxcxus;
    }

	public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
        if (inputUserIsValidUser()) {
            login = systemUserBp.getFilteredSystemUserByLogin(this.login).get(0).getLogin();
        }
        else if (!StringUtils.isBlank(login)) {
            FacesContext.getCurrentInstance().addMessage(userInput.getClientId(),
                    new FacesMessage("No user found with that username"));
            login = null;
        }
    }

    public AccountXCredentialXUser getAxcxu() {
		return axcxu;
	}

	public void setAxcxu(AccountXCredentialXUser axcxu) {
        this.axcxu = axcxuBp.getAccountXCredentialXUserForBrowse(axcxu.getId());
        propertyMap = new HashMap<>();
        for (CredentialXCredentialPropertyType property : this.axcxu.getCredential().getProperties()) {
        	propertyMap.put(property.getType().getName(), property.getValue());
        }
        securityContext.setCredential(this.axcxu.getCredential());
	}

    public Map<String, String> getPropertyMap() {
        return propertyMap;
    }

	public Converter getAxcxuConverter() {

        return new Converter() {

            @Override
            public Object getAsObject(FacesContext context, UIComponent component, String value) {
                return axcxuBp.getAXCXUForConverter(Long.valueOf(value));
            }

            @Override
            public String getAsString(FacesContext context, UIComponent component, Object value) {

                if (value == null) {
                    return "";
                }

                return String.valueOf(((AccountXCredentialXUser) value).getId());
            }
        };
    }

	public String getOrderNumber() {
		return orderNumber;
	}

	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}
	
	public void searchForManualOrder(ValueChangeEvent event) {
		if(StringUtils.isBlank((String) event.getNewValue())) {
			return;
		}
		this.orderNumber = (String) event.getNewValue();
		CustomerOrder manualOrder = this.customerOrderBp.searchByApdPo(orderNumber, true);
		if(manualOrder != null && manualOrder.getType().getValue().equals("MANUAL") 
				&& manualOrder.getStatus().getValue().equals("QUOTED")) {
			this.setAxcxu(customerOrderBp.getAXCXU(manualOrder));
			this.setManualOrder(manualOrder);
			this.setOrderFound(true);
		} else {
			this.setOrderFound(false);
			 FacesContext.getCurrentInstance().addMessage(orderNumberInput.getClientId(),
	                    new FacesMessage("Manual order not found"));
		}
	}
	
	public void searchRequestForQuote(ValueChangeEvent event) {
		if(StringUtils.isBlank((String) event.getNewValue())) {
			return;
		}
		this.orderNumber = (String) event.getNewValue();
		CustomerOrder quote = this.customerOrderBp.searchByApdPo(orderNumber, true);
		if(quote != null && (quote.getStatus().getValue().equals(OrderStatusEnum.REQUEST_FOR_QUOTE.getValue()) 
				|| quote.getStatus().getValue().equals(OrderStatusEnum.SUBMITTED_QUOTE.getValue()))) {
			this.setAxcxu(customerOrderBp.getAXCXU(quote));
			this.setManualOrder(quote);
			this.setOrderFound(true);			
		} else {
			this.setOrderFound(false);			
			 FacesContext.getCurrentInstance().addMessage(orderNumberInput.getClientId(),
	                    new FacesMessage("Request for quote not found"));
		}
	}

	public boolean isOrderFound() {
		return this.orderFound;
	}

	public void setOrderFound(boolean orderFound) {
		this.orderFound = orderFound;
	}

	public CustomerOrder getManualOrder() {
		return this.manualOrder;
	}

	public void setManualOrder(CustomerOrder manualOrder) {
		this.manualOrder = manualOrder;
	}

	public UIComponent getOrderNumberInput() {
		return orderNumberInput;
	}

	public void setOrderNumberInput(UIComponent orderNumberInput) {
		this.orderNumberInput = orderNumberInput;
	}	
}
