/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.customerservice.view.jsf.bean.returns;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.Stateful;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.ConversationScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.apd.phoenix.core.PageNumberDisplay;
import com.apd.phoenix.service.business.ReturnOrderBp;
import com.apd.phoenix.service.model.ReturnOrder;


@Named
@Stateful
@ConversationScoped
public class ReturnSearchBean implements Serializable {

	private static final long serialVersionUID = -4511800693686081490L;

    private String raNumber;
    private String poNumber;
    private String username;

    private Date startDate;
    private Date endDate;

    private List<ReturnOrder> returnList = new ArrayList<>();

    private PageNumberDisplay pageDisplay = new PageNumberDisplay(0);
    
    private boolean closed = Boolean.TRUE;
    private boolean reconciled = Boolean.TRUE;
    private boolean unreconciled = Boolean.TRUE;

    @Inject
    private Conversation conversation;
    
    @Inject
    private ReturnOrderBp returnBp;

    @PostConstruct
    public void init() {
        //nothing yet
    }

    private void endConversation() {
        if (!conversation.isTransient()) {
            conversation.end();
        }
    }

    private void beginConversation() {
        if (conversation.isTransient()) {
            conversation.begin();
        }
    }

    public void retrieveReturnHistory() {
        endConversation();
        beginConversation();
        //If the user didn't check any boxes to sort by return order status show all returns rather than no returns
        if(!reconciled && !unreconciled && !closed){
        	reconciled = true;
        	unreconciled = true;
        	closed = true;
        }
        this.returnList = returnBp.returnSearch(raNumber, poNumber, username, startDate, endDate, isClosed(), isReconciled(), isUnreconciled());
        pageDisplay = new PageNumberDisplay(this.returnList.size());
    }

    public String getRaNumber() {
        return raNumber;
    }

    public void setRaNumber(String raNumber) {
        this.raNumber = raNumber;
    }
    
    public String getPoNumber() {
        return poNumber;
    }

    public void setPoNumber(String poNumber) {
        this.poNumber = poNumber;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }
    
    public List<ReturnOrder> getReturnList() {
        return returnList.subList(pageDisplay.getSize() * (pageDisplay.getPage() - 1), 
        		Math.min(pageDisplay.getSize() * pageDisplay.getPage(), returnList.size()));
    }

    public void setReturnList(List<ReturnOrder> returnList) {
        this.returnList = returnList;
    }

    public PageNumberDisplay getPageDisplay() {
        return pageDisplay;
    }

    public void setPageDisplay(PageNumberDisplay pageDisplay) {
        this.pageDisplay = pageDisplay;
    }

	public boolean isClosed() {
		return closed;
	}

	public void setClosed(boolean closed) {
		this.closed = closed;
	}

	public boolean isReconciled() {
		return reconciled;
	}

	public void setReconciled(boolean reconciled) {
		this.reconciled = reconciled;
	}

	public boolean isUnreconciled() {
		return unreconciled;
	}

	public void setUnreconciled(boolean unreconciled) {
		this.unreconciled = unreconciled;
	}
}
