/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.customerservice.view.jsf.bean.workflow;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.ejb.Stateful;
import javax.enterprise.context.RequestScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.inject.Inject;
import javax.inject.Named;
import com.apd.phoenix.customerservice.view.jsf.bean.workflow.OrderHumanTaskFormBean;
import com.apd.phoenix.service.business.ShipmentBp;
import com.apd.phoenix.service.model.LineItemXShipment;
import com.apd.phoenix.service.model.Shipment;
import com.apd.phoenix.service.model.dto.ShipmentDto;

@Named
@Stateful
@RequestScoped
public class CreditCardChargeFailedBean extends OrderHumanTaskFormBean {

    private String splitCharge;
    private String newCardNumber;
    private String newCardDate;
    private Shipment shipmentToCharge;

    @Inject
    private ShipmentBp shipmentBp;

    @Override
    public void init() {
        ShipmentDto shipmentDto = (ShipmentDto) this.taskContent.get("inputShipmentDto");
        if (shipmentDto != null) {
            shipmentToCharge = shipmentBp.retrieveShipment(shipmentDto);
        }
    }

    public String retry() {
        return this.complete(true);
    }

    public String cancel() {
        return this.complete(false);
    }

    private String complete(boolean retryCharge){
        Map<String, Object> params = new HashMap<>();
        params.put("retryCharge", retryCharge);
        
        if (splitCharge != null && !splitCharge.isEmpty()) {
        	params.put("splitCharge", BigDecimal.valueOf(Double.parseDouble(splitCharge)));
        }
        params.put("newCardNumber", newCardNumber);
        params.put("newCardDate", newCardDate);
        //flags the shipment as the shipment to be charged, for legacy orders
        shipmentToCharge.setToCharge(true);
        shipmentToCharge = this.shipmentBp.update(shipmentToCharge);
		params.put("outputShipmentDto", (ShipmentDto) this.taskContent.get("inputShipmentDto"));
        return this.completeOrderTask(params);
    }

    public String getSplitCharge() {
        return splitCharge;
    }

    public void setSplitCharge(String splitCharge) {
        this.splitCharge = splitCharge;
    }

    public String getNewCardNumber() {
        return newCardNumber;
    }

    public void setNewCardNumber(String newCardNumber) {
        this.newCardNumber = newCardNumber;
    }

    public String getNewCardDate() {
        return newCardDate;
    }

    public void setNewCardDate(String newCardDate) {
        this.newCardDate = newCardDate;
    }

    public Shipment getShipmentToCharge() {
        return this.shipmentToCharge;
    }

    public void setShipmentToCharge(Shipment toCharge) {
        this.shipmentToCharge = toCharge;
    }

    public List<Shipment> getShipmentList() {
    	List<Shipment> toReturn = new ArrayList<>();
    	for (Shipment shipment : customerOrder.getShipments()) {
    		boolean paid = true;
    		for (LineItemXShipment item : shipment.getItemXShipments()) {
    			if (item.getPaid() == null || !item.getPaid()) {
    				paid = false;
    			}
    		}
    		if (!paid) {
    			toReturn.add(shipment);
    		}
    	}
    	return toReturn;
    }

    public Converter getConverter() {

        return new Converter() {

            @Override
            public Object getAsObject(FacesContext context, UIComponent component, String value) {
                return shipmentBp.findById(Long.valueOf(value), Shipment.class);
            }

            @Override
            public String getAsString(FacesContext context, UIComponent component, Object value) {

                if (value == null) {
                    return "";
                }

                return String.valueOf(((Shipment) value).getId());
            }
        };
    }

}
