/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.apd.phoenix.customerservice.view.jsf.bean.workflow;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.ejb.Stateful;
import javax.enterprise.context.ConversationScoped;
import javax.inject.Named;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.model.LineItem;

@Named
@Stateful
@ConversationScoped
public class ContactCustomerForCartBean extends OrderHumanTaskFormBean {

    private static final Logger LOGGER = LoggerFactory.getLogger(ContactCustomerForCartBean.class);

    private List<LineItem> lineItems;

    @Override
    public void init() {
        lineItems = new ArrayList<LineItem>(customerOrder.getItems());
    }

    public void retrieve() {
    }

    public String complete(){
    	Map<String, Object> results = new HashMap<>();

    	return this.completeOrderTask(results);
    }

    public List<LineItem> getLineItems() {
        return lineItems;
    }

    public void setLineItems(List<LineItem> lineItems) {
        this.lineItems = lineItems;
    }
}
