package com.apd.phoenix.customerservice.view.jsf.bean;

import java.io.Serializable;
import java.util.List;
import javax.ejb.Stateful;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import com.apd.phoenix.service.business.BulletinBp;
import com.apd.phoenix.service.model.Bulletin;

/**
 * This session-scoped bean stores whether the bulletin modal should be displayed. It displays once per shopping 
 * session. If it should be displayed, it uses the BulletinDisplayAccessBean to load the bulletins.
 * 
 * @author RHC
 *
 */
@Named
@Stateful
@SessionScoped
public class BulletinDisplayBean implements Serializable {

    private static final long serialVersionUID = 6505751838383055954L;

    private boolean hasDisplayedBulletins = false;

    @Inject
    private BulletinBp bulletinBp;

    @Inject
    private BulletinDisplayAccessBean bulletinDisplayAccessBean;

    /**
     * Returns whether the bulletins should be displayed. 
     * 
     * @return
     */
    public boolean getShouldDisplayBulletins() {

        boolean toReturn = (!hasDisplayedBulletins || bulletinDisplayAccessBean.hasLoadedBulletins())
                && !getBulletinList().isEmpty();
        hasDisplayedBulletins = true;
        return toReturn;
    }

    /**
     * Returns the list of bulletins to display.
     * 
     * @return
     */
    public List<Bulletin> getBulletinList() {
        return bulletinDisplayAccessBean.getBulletinList();
    }

    public String sanitizedMessage(Bulletin bulletin) {
        return Jsoup.clean(bulletin.getMessage(), Whitelist.basicWithImages());
    }
}
