package com.apd.phoenix.customerservice.view.jsf.bean;

import java.io.Serializable;
import java.util.Collection;
import javax.ejb.Stateful;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.ConversationScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.business.AccountBp;
import com.apd.phoenix.service.model.Account;
import com.apd.phoenix.web.AccountAddressPagenationControllerBean;
import com.apd.phoenix.web.searchbeans.AccountSearchBean;

/**
 * Backing bean for SystemUser entities.
 * <p>
 * This class provides CRUD functionality for all SystemUser entities. It focuses
 * purely on Java EE 6 standards (e.g. <tt>&#64;ConversationScoped</tt> for
 * state management, <tt>PersistenceContext</tt> for persistence,
 * <tt>CriteriaBuilder</tt> for searches) rather than introducing a CRUD framework or
 * custom base class.
 */

@Named
@Stateful
@ConversationScoped
public class AccountViewBean extends AccountAddressPagenationControllerBean implements Serializable {

    private static final long serialVersionUID = 1L;

    private static final Logger LOG = LoggerFactory.getLogger(AccountViewBean.class);

    @Inject
    private AccountBp bp;

    @Inject
    private AccountSearchBean accountSearchBean;

    @Inject
    private Conversation conversation;

    public void retrieve() {

        if (this.accountSearchBean.getSelection() != null) {
            this.setAccount(this.bp.findById(accountSearchBean.getSelection().getId(), Account.class));
            this.setAccount(this.bp.eagerLoad(this.getAccount()));
            this.accountSearchBean.setSelection(null);
            populatePageNumberDisplay();
        }

        if (FacesContext.getCurrentInstance().isPostback()) {
            return;
        }

        if (this.conversation.isTransient()) {
            this.conversation.begin();
        }
        populatePageNumberDisplay();
    }

    public Collection<Account> getChildren() {
        return this.bp.getChildren(this.getAccount());
    }
}
