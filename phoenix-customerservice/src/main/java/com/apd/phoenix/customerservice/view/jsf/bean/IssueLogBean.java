/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.customerservice.view.jsf.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateful;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.ConversationScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.inject.Inject;
import javax.inject.Named;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.apd.phoenix.core.PageNumberDisplay;
import com.apd.phoenix.service.business.IssueLogBp;
import com.apd.phoenix.service.business.IssueLogSearchCriteria;
import com.apd.phoenix.service.business.ItemCategoryBp;
import com.apd.phoenix.service.model.ContactLog.TicketStatus;
import com.apd.phoenix.service.model.CustomerOrder;
import com.apd.phoenix.service.model.IssueLog;
import com.apd.phoenix.service.model.IssueLog.IssueCategory;
import com.apd.phoenix.service.model.ItemCategory;

@Named
@Stateful
@ConversationScoped
public class IssueLogBean implements Serializable {

	private static final long serialVersionUID = 7161358959708681190L;

	private static final Logger LOG = LoggerFactory.getLogger(IssueLogBean.class);

    @Inject
    private ItemCategoryBp itemCategoryBp;
    @Inject
    private IssueLogBp issueLogBp;

    private IssueLog currentIssueLog;

    private String ticketNumber;
    private String department;
    private String contactName;
    private String contactNumber;
    private String contactEmail;
    private CustomerOrder customerOrder;
    private Date startDate;
    private Date endDate;
    private Date followupDate;
    private Date expectedResolutionDate;
    private boolean csvDownload = false;
    private TicketStatus ticketStatus;
    private IssueCategory issueCategory;
    
    private List<IssueLog> issueLogs = new ArrayList<>();
    private PageNumberDisplay issueLogsPagination = new PageNumberDisplay(0);

    @Inject
    private Conversation conversation;
    
    @PostConstruct
    public void init() {
        //nothing yet
    }

    private void beginConversation() {
        if (conversation.isTransient()) {
            conversation.begin();
        }
    }
    
    public void retrieveIssueLog(IssueLog issueLog) {
    	this.currentIssueLog = issueLog;
    }
    
    public void save() {
    	this.currentIssueLog.setUpdateTimestamp(new Date());
    	this.issueLogBp.update(this.currentIssueLog);
		search();
	}
    
    public void resolve() {
        this.currentIssueLog.setResolvedDate(new Date());
        this.currentIssueLog.setStatus(TicketStatus.CLOSED);
        save();
    }

    public void retrieve() {
    	
        if (FacesContext.getCurrentInstance().isPostback()) {
            return;
        }
        
    	beginConversation();
    }

    public String search() {
        this.issueLogs = issueLogBp.getIssueLogs(new IssueLogSearchCriteria(ticketNumber, department, contactName, contactNumber, contactEmail, customerOrder, startDate, endDate, issueCategory, ticketStatus, null, null, null));
        issueLogsPagination = new PageNumberDisplay(issueLogs.size());
        
        return "issueLogSearchResult?faces-redirect=true";
    }
    
    public void cancel() {
        this.currentIssueLog = new IssueLog();
    }
    
    public TicketStatus[] getTicketStatusList() {
        return TicketStatus.values();
    }
    
    public IssueCategory[] getIssueCategoryList() {
        return IssueCategory.values();
    }
    
    public List<ItemCategory> getItemCategoryList() {
    	return itemCategoryBp.findAll(ItemCategory.class, 0, 0);
    }

    @Resource
    private SessionContext sessionContext;

    public Converter getItemCategoryConverter() {

        return new Converter() {

            @Override
            public Object getAsObject(FacesContext context, UIComponent component, String value) {

                if (value.equals("")) {
                    return null;
                }

                return itemCategoryBp.findById(Long.valueOf(value), ItemCategory.class);
            }

            @Override
            public String getAsString(FacesContext context, UIComponent component, Object value) {
                if (value == null) {
                    return "";
                }
                return String.valueOf(((ItemCategory) value).getId());
            }
        };
    }

    public IssueLog getCurrentIssueLog() {
        return currentIssueLog;
    }

    public void setCurrentIssueLog(IssueLog currentIssueLog) {
        this.currentIssueLog = currentIssueLog;
        if (this.currentIssueLog != null && this.currentIssueLog.getId() != null) {
        	this.currentIssueLog = this.issueLogBp.eagerLoad(this.currentIssueLog);
        }
    }

    public String getTicketNumber() {
		return ticketNumber;
	}

	public void setTicketNumber(String ticketNumber) {
		this.ticketNumber = ticketNumber;
	}

	public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

	public String getContactName() {
		return contactName;
	}

	public void setContactName(String contactName) {
		this.contactName = contactName;
	}

	public String getContactNumber() {
		return contactNumber;
	}

	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}

	public String getContactEmail() {
		return contactEmail;
	}

	public void setContactEmail(String contactEmail) {
		this.contactEmail = contactEmail;
	}

	public boolean isCsvDownload() {
		return csvDownload;
	}

	public void setCsvDownload(boolean csvDownload) {
		this.csvDownload = csvDownload;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	@SuppressWarnings("unchecked")
	public List<IssueLog> getIssueLogs() {
		ArrayList<IssueLog> toReturn = new ArrayList<>();
		toReturn.addAll((List<IssueLog>) issueLogsPagination.currentPageElements(issueLogs));
		return toReturn;
	}

	public void setIssueLogs(List<IssueLog> issueLogs) {
		this.issueLogs = issueLogs;
	}

	public PageNumberDisplay getIssueLogsPagination() {
		return issueLogsPagination;
	}

	public void setIssueLogsPagination(PageNumberDisplay issueLogsPagination) {
		this.issueLogsPagination = issueLogsPagination;
	}

	public CustomerOrder getCustomerOrder() {
		return customerOrder;
	}

	public void setCustomerOrder(CustomerOrder customerOrder) {
		this.customerOrder = customerOrder;
	}

	public TicketStatus getTicketStatus() {
		return ticketStatus;
	}

	public void setTicketStatus(TicketStatus ticketStatus) {
		this.ticketStatus = ticketStatus;
	}

	public IssueCategory getIssueCategory() {
		return issueCategory;
	}

	public void setIssueCategory(IssueCategory issueCategory) {
		this.issueCategory = issueCategory;
	}

	public Date getFollowupDate() {
		return followupDate;
	}

	public void setFollowupDate(Date followupDate) {
		this.followupDate = followupDate;
	}

	public Date getExpectedResolutionDate() {
		return expectedResolutionDate;
	}

	public void setExpectedResolutionDate(Date expectedResolutionDate) {
		this.expectedResolutionDate = expectedResolutionDate;
	}

}
