/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.customerservice.view.jsf.bean.report;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateful;
import javax.enterprise.context.ConversationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.report.ReportService;
import com.apd.phoenix.service.utility.FileDownloader;

@Named
@Stateful
@ConversationScoped
public class ArReportBean extends AbstractJasperReportBean implements Serializable {

    private static final long serialVersionUID = 5556845450144240260L;

    private static final Logger LOG = LoggerFactory.getLogger(ArReportBean.class);

    @Inject
    private ReportService reportService;

    private Date invoiceStartDate;

    private Date invoiceEndDate;

    private List<String> accounts;

    public String generateReport() {
        LOG.info("downloading AR report");
        return FileDownloader.downloadFile(reportService.generateCustomerServiceArReport(accounts, invoiceStartDate,
                invoiceEndDate));
    }

    public Date getInvoiceStartDate() {
        return invoiceStartDate;
    }

    public void setInvoiceStartDate(Date invoiceStartDate) {
        this.invoiceStartDate = invoiceStartDate;
    }

    public Date getInvoiceEndDate() {
        return invoiceEndDate;
    }

    public void setInvoiceEndDate(Date invoiceEndDate) {
        this.invoiceEndDate = invoiceEndDate;
    }

    public List<String> getAccounts() {
        return accounts;
    }

    public void setAccounts(List<String> accounts) {
        this.accounts = accounts;
    }
}
