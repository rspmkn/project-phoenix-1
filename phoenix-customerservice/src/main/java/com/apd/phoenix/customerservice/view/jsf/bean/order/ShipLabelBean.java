/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.customerservice.view.jsf.bean.order;

import java.io.Serializable;
import javax.ejb.Stateful;
import javax.enterprise.context.ConversationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.customerservice.view.jsf.utility.ShippingLabelPDFCreator;
import com.apd.phoenix.service.business.CustomerOrderBp;
import com.apd.phoenix.service.model.CustomerOrder;

@Named
@Stateful
@ConversationScoped
public class ShipLabelBean implements Serializable {

    private static final Logger LOG = LoggerFactory.getLogger(ShipLabelBean.class);

    @Inject
    private CustomerOrderBp customerOrderBp;

    @Inject
    private ShippingLabelPDFCreator shippingLabelPDFCreator;

    private String apdPo;

    public void printShippingLabel() {
        CustomerOrder co = customerOrderBp.searchByApdPo(apdPo);
        if (co == null) {
            LOG.debug("No customer order found for APD PO: " + apdPo);
            return;
        }
        customerOrderBp.hydrateForOrderDetails(co);
        shippingLabelPDFCreator.downloadShippingLabel(co);
    }

    public String getApdPo() {
        return apdPo;
    }

    public void setApdPo(String apdPo) {
        this.apdPo = apdPo;
    }
}
