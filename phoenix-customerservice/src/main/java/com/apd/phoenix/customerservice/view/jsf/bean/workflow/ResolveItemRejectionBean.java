package com.apd.phoenix.customerservice.view.jsf.bean.workflow;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.ejb.Stateful;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.model.dto.LineItemDto;
import com.apd.phoenix.service.business.LineItemBp;
import com.apd.phoenix.service.model.dto.POAcknowledgementDto;
import com.apd.phoenix.service.model.LineItem;
import com.apd.phoenix.service.model.LineItemStatus.LineItemStatusEnum;

@Named
@Stateful
@RequestScoped
public class ResolveItemRejectionBean extends OrderHumanTaskFormBean {

    private static final Logger logger = LoggerFactory.getLogger(ResolveItemRejectionBean.class);

    @Inject
    LineItemBp lineItemBp;

    List<LineItem> lineItems;

    private Map<Integer, Integer> acceptedQuantityMap = new HashMap<Integer, Integer>();

    @Override
    public void init() {

        POAcknowledgementDto poAckDto = (POAcknowledgementDto) this.taskContent.get("inputPoAckDto");

        lineItems = new ArrayList<LineItem>();
        for (LineItem lineItem : customerOrder.getItems()) {
            if (LineItemStatusEnum.REJECTED.getValue().equals(lineItem.getStatus().getValue()) && poAckDto != null) {
                for (LineItemDto lineItemDto : poAckDto.getLineItems()) {
                    if (lineItem.getLineNumber() != null
                            && lineItemDto.getQuantity().compareTo(
                                    BigInteger.valueOf(lineItem.getQuantity().intValue())) < 0) {
                        acceptedQuantityMap.put(lineItem.getLineNumber(), lineItem.getQuantity()
                                - lineItemDto.getQuantity().intValue());
                    }
                }
            }
            lineItems.add(lineItem);
        }
    }

    public String resolved() {
        if (this.isPreMigrationTask()) {
            this.userTaskService.exitTask(this.getTaskId());
            return TO_TASK_SCREEN;
        }
        else {
            return completeTask(false);
        }
    }

    public String cancelItems() {
        if (this.isPreMigrationTask()) {
            this.userTaskService.exitTask(this.getTaskId());
            return toChangeOrderForm();
        }
        else {
            return completeTask(true);
        }
    }

    public boolean canEditOrder() {
        return customerOrderBp.canEditOrder(this.customerOrder);
    }

    private String completeTask(boolean cancelRejectedItems){
        Map<String, Object> params = new HashMap<>();
        params.put("cancelRejectedItemsOut", cancelRejectedItems);
        return this.completeOrderTask(params);
    }

    public List<LineItem> getLineItems() {
        return lineItems;
    }

    public void setLineItems(List<LineItem> lineItems) {
        this.lineItems = lineItems;
    }

    public Map<Integer, Integer> getAcceptedQuantityMap() {
        return this.acceptedQuantityMap;
    }
}
