/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.apd.phoenix.customerservice.view.jsf.bean.workflow;

import com.apd.phoenix.service.executor.command.api.Command;
import com.apd.phoenix.service.itemrequest.ItemRequest;
import javax.ejb.Stateful;
import javax.inject.Named;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.enterprise.context.RequestScoped;

@Named
@Stateful
@RequestScoped
public class RequestItemAdditionBean extends HumanTaskFormBean {

    private static final Logger logger = LoggerFactory.getLogger(RequestItemAdditionBean.class);

    private ItemRequest request;

    public void retrieve() {

    }

    public String resolved() {
        long requestId = (Long) taskContent.get("requestId");
        logger.info("Completing task with requestId=" + requestId);
        return this.completeTask(null, requestId, Command.Type.USER_REQUEST);
    }

    @Override
    public void initDomainInfo() {
        request = (ItemRequest) taskContent.get("itemRequest");
    }

    public ItemRequest getRequest() {
        return this.request;
    }
}
