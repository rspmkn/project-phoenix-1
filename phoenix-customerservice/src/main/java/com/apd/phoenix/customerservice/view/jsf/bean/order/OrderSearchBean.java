/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.customerservice.view.jsf.bean.order;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.AccessTimeout;
import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.SessionContext;
import javax.ejb.Stateful;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.ConversationScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.kie.api.task.model.Status;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.apd.phoenix.core.PageNumberDisplay;
import com.apd.phoenix.customerservice.view.jsf.bean.workflow.OrderDetailsBean;
import com.apd.phoenix.service.brms.core.order.OrderSearchReport;
import com.apd.phoenix.service.brms.core.workflow.PhoenixTaskSummary;
import com.apd.phoenix.service.brms.core.workflow.UserTaskService;
import com.apd.phoenix.service.business.AccountBp;
import com.apd.phoenix.service.business.CustomerOrderBp;
import com.apd.phoenix.service.business.CustomerOrderSearchCriteria;
import com.apd.phoenix.service.business.LineItemStatusBp;
import com.apd.phoenix.service.business.OrderStatusBp;
import com.apd.phoenix.service.business.PaymentTypeBp;
import com.apd.phoenix.service.model.Account;
import com.apd.phoenix.service.model.CustomerOrder;
import com.apd.phoenix.service.model.LineItemStatus;
import com.apd.phoenix.service.model.OrderStatus;
import com.apd.phoenix.service.model.PaymentType;
import com.apd.phoenix.service.persistence.jpa.LineItemStatusDao;
import com.apd.phoenix.service.persistence.jpa.OrderStatusDao;


@Named
@Stateful
@ConversationScoped
@Lock(LockType.READ)
@AccessTimeout(unit = TimeUnit.SECONDS, value = 40)
public class OrderSearchBean implements Serializable {

    private static final Logger LOG = LoggerFactory.getLogger(OrderSearchBean.class);
    
    private static final String DOWNLOAD_CONTENT_TYPE = "application/x-force-download";
    private static final String ATTACHMENT_HEADER = "Content-disposition";
    private static final String ATTACHMENT_VALUE = "attachment; filename=download.csv";

    @Inject
    private OrderStatusDao orderStatusDao;
    
    @Inject
    private OrderSearchReport orderSearchReport;
    
    @Inject
    private LineItemStatusDao lineItemStatusDao;
    
    @Inject
    private CustomerOrderBp customerOrderBp;

    @Inject
    private OrderStatusBp orderStatusBp;
    
    @Inject
    private LineItemStatusBp lineItemStatusBp;
    
    @Inject
    private PaymentTypeBp paymentTypeBp;
    
    @Inject
    UserTaskService userTaskService;
    
    @Inject
    private AccountBp accountBp;
    
    @Inject
    private OrderDetailsBean orderDetailsBean;

    private String apdNumber;
    private String quickSearchPo;
    private String customerNumber;

    private String email;
    private String username;

    private String sku;

    private BigDecimal matchPrice;

    private OrderStatus currentStatus;
    private OrderStatus changeStatus;
    private Account account;
    
    private LineItemStatus currentLineItemStatus;
    
    private PaymentType paymentType;

    private Date startDate;
    private Date endDate;

    private List<CustomerOrder> orderList = new ArrayList<>();

    private PageNumberDisplay pageDisplay = new PageNumberDisplay(0);
    private CustomerOrderSearchCriteria customerOrderSearchCriteria;
    private int lastPage = -1;
    private boolean displayNoResultsMessage = false;
    
    private boolean errorRedirect = false;

    @Inject
    private Conversation conversation;

    @PostConstruct
    public void init() {
        //nothing yet
    }

    private void endConversation() {
        if (!conversation.isTransient()) {
            conversation.end();
        }
    }

    private void beginConversation() {
        if (conversation.isTransient()) {
            conversation.begin();
        }
    }

    public void retrieveOrderHistory() {
        endConversation();
        beginConversation();
        if (this.account != null) {
            this.account = this.accountBp.eagerLoad(this.account);
        }
        this.orderList = new ArrayList<CustomerOrder>();
        this.lastPage = -1;
        this.pageDisplay = new PageNumberDisplay(0);
        displayNoResultsMessage = true;
        String selectedPo = (!StringUtils.isEmpty(apdNumber) ? apdNumber : customerNumber).trim();
        //splits the inserted SKU string with comma delimeters
        String[] skuArray = this.sku.split(",");
        List<String> skuList = new ArrayList<>();
        for (String s : skuArray) {
        	String toAdd = s.trim().toUpperCase();
        	if (!StringUtils.isEmpty(toAdd)) {
        		skuList.add(toAdd);
        	}
        }
       customerOrderSearchCriteria =new CustomerOrderSearchCriteria(changeStatus, startDate, 
        		endDate, currentStatus, selectedPo, this.account, this.username, this.email, skuList, this.matchPrice, this.currentLineItemStatus, this.paymentType, null, null);
       int resultsize = customerOrderBp.getCustomerOrdersCount(customerOrderSearchCriteria);
       pageDisplay = new PageNumberDisplay(resultsize);
    }
    
    private void loadOrders() {
    	if (pageDisplay != null && customerOrderSearchCriteria != null && lastPage != pageDisplay.getPage()) {
    		customerOrderSearchCriteria.setStart((pageDisplay.getPage()-1) * pageDisplay.getSize());
    		customerOrderSearchCriteria.setPageSize(pageDisplay.getSize());
    		lastPage = pageDisplay.getPage();
    		this.orderList = customerOrderBp.getCustomerOrdersPaginated(customerOrderSearchCriteria);
    	}
    }
    
    public String getApdNumber() {
        return apdNumber;
    }

    public void setApdNumber(String apdNumber) {
        this.apdNumber = apdNumber;
    }

    public String getCustomerNumber() {
        return customerNumber;
    }

    public void setCustomerNumber(String customerNumber) {
        this.customerNumber = customerNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public BigDecimal getMatchPrice() {
        return matchPrice;
    }

    public void setMatchPrice(BigDecimal matchPrice) {
        this.matchPrice = matchPrice;
    }

    public OrderStatus getCurrentStatus() {
        return currentStatus;
    }

    public void setCurrentStatus(OrderStatus orderStatus) {
        this.currentStatus = orderStatus;
    }

    public OrderStatus getChangeStatus() {
        return changeStatus;
    }

    public void setChangeStatus(OrderStatus orderStatus) {
        this.changeStatus = orderStatus;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public List<OrderStatus> getStatusList() {
        return orderStatusBp.getStatusList();
    }
    
    public List<LineItemStatus> getLineItemStatusList() {
        return lineItemStatusBp.getStatusList();
    }

    public List<PaymentType> getPaymentTypeList() {
        return paymentTypeBp.getPaymentTypeList();
    }
    
    public List<CustomerOrder> getOrderList() {
    	loadOrders();
        return orderList;
    }

    public void setOrderList(List<CustomerOrder> orderList) {
        this.orderList = orderList;
    }

    public PageNumberDisplay getPageDisplay() {
        return pageDisplay;
    }

    public void setPageDisplay(PageNumberDisplay pageDisplay) {
        this.pageDisplay = pageDisplay;
    }

    @Resource
    private SessionContext sessionContext;

    public Converter getStatusConverter() {

        return new Converter() {

            @Override
            public Object getAsObject(FacesContext context, UIComponent component, String value) {

                if (value.equals("")) {
                    return null;
                }

                return orderStatusDao.getOrderStatus(Long.valueOf(value));
            }

            @Override
            public String getAsString(FacesContext context, UIComponent component, Object value) {
                if (value == null) {
                    return "";
                }
                return String.valueOf(((OrderStatus) value).getId());
            }
        };
    }
    
    public Converter getLineItemStatusConverter() {

        return new Converter() {

			@Override
            public Object getAsObject(FacesContext context, UIComponent component, String value) {

                if (value.equals("")) {
                    return null;
                }
                return lineItemStatusDao.findById(Long.valueOf(value), LineItemStatus.class);
            }

            @Override
            public String getAsString(FacesContext context, UIComponent component, Object value) {
                if (value == null) {
                    return "";
                }
                return String.valueOf(((LineItemStatus) value).getId());
            }
        };
    }
    
    public Converter getPaymentTypeConverter() {

        return new Converter() {

            @Override
            public Object getAsObject(FacesContext context, UIComponent component, String value) {

                if (value.equals("")) {
                    return null;
                }

                return paymentTypeBp.findById(Long.valueOf(value), PaymentType.class);
            }

            @Override
            public String getAsString(FacesContext context, UIComponent component, Object value) {
                if (value == null) {
                    return "";
                }
                return String.valueOf(((PaymentType) value).getId());
            }
        };
    }

	public LineItemStatus getCurrentLineItemStatus() {
		return currentLineItemStatus;
	}

	public void setCurrentLineItemStatus(LineItemStatus currentLineItemStatus) {
		this.currentLineItemStatus = currentLineItemStatus;
	}

	public PaymentType getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(PaymentType paymentType) {
		this.paymentType = paymentType;
	}
	
	public String hasOpenTasks(CustomerOrder customerOrder){
		List<PhoenixTaskSummary> results = userTaskService.getOrderTasks(customerOrder);
		if (results != null && !results.isEmpty()) {
			for (PhoenixTaskSummary task : results) {
				if (!task.getStatus().equals(Status.Completed) &&
					!task.getStatus().equals(Status.Error) &&
					!task.getStatus().equals(Status.Exited) &&
					!task.getStatus().equals(Status.Failed) &&
					!task.getStatus().equals(Status.Obsolete)) {
					return "Y";
				}
			}
		} 
		return "N";
	}

	public boolean isDisplayNoResultsMessage() {
		return displayNoResultsMessage;
	}

	public void setDisplayNoResultsMessage(boolean displayNoResultsMessage) {
		this.displayNoResultsMessage = displayNoResultsMessage;
	}

	public Account getAccount() {
		return account;
	}

	public void setAccount(Account account) {
		this.account = account;
	}
	
	public void clearAccount() {
		this.setAccount(null);
	}
	
	public void clearSearchFields() {
		this.changeStatus = null;
		this.startDate = null; 
		this.endDate = null; 
		this.currentStatus = null;
		this.apdNumber = null;
		this.customerNumber = null;
		this.account = null;
		this.username = null;
		this.email = null;
		this.matchPrice = null;
		this.currentLineItemStatus = null;
		this.paymentType = null;
		this.sku = null;
	}

	public String getQuickSearchPo() {
		return quickSearchPo;
	}

	public void setQuickSearchPo(String quickSearchPo) {
		CustomerOrder quickSearchOrder = this.customerOrderBp.searchByApdPo(quickSearchPo, true);
		if (quickSearchOrder != null) {
			this.quickSearchPo = quickSearchPo;
			this.orderDetailsBean.setOrder(quickSearchOrder);
		}
		this.apdNumber = this.quickSearchPo;
	}
	
	public void downloadCustomerOrderList() {
        
		String selectedPo = (!StringUtils.isEmpty(apdNumber) ? apdNumber : customerNumber).trim();
        //splits the inserted SKU string with comma delimeters
        String[] skuArray = this.sku.split(",");
        List<String> skuList = new ArrayList<>();
        for (String s : skuArray) {
        	String toAdd = s.trim().toUpperCase();
        	if (!StringUtils.isEmpty(toAdd)) {
        		skuList.add(toAdd);
        	}
        }
               
        CustomerOrderSearchCriteria criterion = new CustomerOrderSearchCriteria(changeStatus, startDate, 
        		endDate, currentStatus, selectedPo, this.account, this.username, this.email, skuList, this.matchPrice, this.currentLineItemStatus, this.paymentType, null, null);
        
        List<String> customerOrderList = orderSearchReport.getOrderSearchReport(criterion);
        FacesContext facesContext = FacesContext.getCurrentInstance();
        HttpServletResponse response = (HttpServletResponse) facesContext.getExternalContext().getResponse();
        response.setContentType(DOWNLOAD_CONTENT_TYPE);
        response.setHeader(ATTACHMENT_HEADER, ATTACHMENT_VALUE);
        ServletOutputStream out = null;
        try {
            out = response.getOutputStream();
            for (String line : customerOrderList) {
                out.write((line + "\n").getBytes());
            }
            //out.write();
            out.close();
        } catch (Exception ex) {

        }
        facesContext.responseComplete();
    }	
	
	public String getDisplayStatus(CustomerOrder order) {
		if (order == null || order.getStatus() == null) {
			return "";
		}
		else if (order.getPaymentStatus() == null) {
			return order.getStatus().getValue();
		}
		else {
			return order.getStatus().getValue() + " / " + order.getPaymentStatus().getValue();
		}
	}

	public boolean getErrorRedirect() {
		return errorRedirect;
	}

	public void setErrorRedirect(boolean errorRedirect) {
		this.errorRedirect = errorRedirect;
	}
}
