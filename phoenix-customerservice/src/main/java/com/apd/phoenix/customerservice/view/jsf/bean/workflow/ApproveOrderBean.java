/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.customerservice.view.jsf.bean.workflow;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.ejb.Stateful;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import com.apd.phoenix.service.business.AccountBp;
import com.apd.phoenix.service.business.CashoutPageBp;
import com.apd.phoenix.service.business.PoNumberBp;
import com.apd.phoenix.service.model.LineItem;
import com.apd.phoenix.service.model.PoNumber;
import com.apd.phoenix.web.cashout.CashoutPageContainer;

@Named
@Stateful
@RequestScoped
public class ApproveOrderBean extends OrderHumanTaskFormBean {

    @Inject
    AccountBp accountBp;

    @Inject
    private CashoutPageContainer pageValues;

    @Inject
    CashoutPageBp cashoutPageBp;

    @Inject
    PoNumberBp poNumberBp;

    private String customerPo;
    private List<LineItem> lineItems;

    @Override
    public void init() {
        PoNumber customerPo = customerOrder.getCustomerPo();
        if (customerPo != null) {
            this.customerPo = customerPo.getValue();
        }
        else {
            this.customerPo = "";
        }

        this.pageValues.init(cashoutPageBp.getFromCredentialForCashout(this.customerOrder.getCredential()));
        this.pageValues.orderToPage(this.customerOrder);
        lineItems = new ArrayList<LineItem>(customerOrder.getItems());
    }

    public void retrieve() {

    }

    public String approve() {
        return this.complete(true, false);
    }

    public String deny() {
        return this.complete(false, false);
    }

    public String setToOrder() {
        return this.complete(false, true);
    }

    /*
     * If setToOrder is true, the approved field is ignored and the order will have it's status changed to 'SENT_TO_VENDOR'
     * If setToOrder is false, then the 'approved' argument will dictate the path the order takes.
     */
    private String complete(boolean approved, boolean setToOrder){
    	if (canEditCustomerPo()){
    		PoNumber newCustomerPo = customerOrder.getCustomerPo();
    		newCustomerPo.setValue(customerPo);
    		newCustomerPo = poNumberBp.update(newCustomerPo);
    	}
        Map<String, Object> params = new HashMap<>();
        customerOrderBp.update(customerOrder);
        params.put("approved", approved);
        params.put("setToOrder", setToOrder);
        return this.completeOrderTask(params);
    }

    public boolean canEditCustomerPo() {
        return accountBp.canEditCustomerPo(customerOrder.getAccount());
    }

    public String getCustomerPo() {
        return customerPo;
    }

    public void setCustomerPo(String customerPo) {
        this.customerPo = customerPo;
    }

    public CashoutPageContainer getPageValues() {
        return this.pageValues;
    }

    public void setPageValues(CashoutPageContainer pageValues) {
        this.pageValues = pageValues;
    }

    public List<LineItem> getLineItems() {
        return lineItems;
    }

    public void setLineItems(List<LineItem> lineItems) {
        this.lineItems = lineItems;
    }

}
