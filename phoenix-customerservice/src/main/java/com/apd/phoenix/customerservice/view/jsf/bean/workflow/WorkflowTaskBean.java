/**
 * 
 */
package com.apd.phoenix.customerservice.view.jsf.bean.workflow;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.annotation.PostConstruct;
import javax.ejb.AccessTimeout;
import javax.ejb.Stateful;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.commons.lang.StringUtils;
import org.kie.api.task.model.Status;
import org.kie.api.task.model.Task;
import org.kie.api.task.model.User;
import org.kie.api.task.model.TaskSummary;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.apd.phoenix.core.PageNumberDisplay;
import com.apd.phoenix.core.PageNumberDisplay.PageSize;
import com.apd.phoenix.customerservice.view.jsf.utility.CustomerServicePropertiesLoader;
import com.apd.phoenix.service.brms.core.workflow.PhoenixTaskSummary;
import com.apd.phoenix.service.brms.core.workflow.UserTaskService;
import com.apd.phoenix.service.business.CustomerOrderBp;
import com.apd.phoenix.service.business.OrderLogBp;
import com.apd.phoenix.service.business.SecurityContext;
import com.apd.phoenix.service.business.SystemUserBp;
import com.apd.phoenix.service.business.WorkflowLogBp;
import com.apd.phoenix.service.model.WorkflowLog;
import com.apd.phoenix.service.model.WorkflowLog.TaskEvent;

@Named
@Stateful
@SessionScoped
@AccessTimeout(unit = TimeUnit.SECONDS, value = 30)
public class WorkflowTaskBean implements Serializable {

    private static final int TASK_PROCESSING_COMPLETE_MINUTES = 5;

	private static final String MANAGER_PERMISSION = "customer service manager";

	private static final Logger logger = LoggerFactory.getLogger(WorkflowTaskBean.class);

    private static final String TO_HUMAN_TASK_FORM = "/admin/customerService/humanTask?faces-redirect=true";

    private static final long serialVersionUID = -7878893937965889019L;

    private static final String TECH_SUPPORT = "Tech Support";

    private Long id;

    private long historyTaskId = 0;

    private String user;

    private Long taskToWork;

    private String taskToWorkName;

    private List<PhoenixTaskSummary> availableTasks;
    
    private Map<PhoenixTaskSummary, Boolean> taskSelected = new HashMap<>();

    private List<PhoenixTaskSummary> assignedTasks;

    private boolean showingCompletedTasks;

    private String taskToClaim;

    private String taskToRelease;

    private String delegateUser;

    @Inject
    private UserTaskService userTaskService;

    @Inject
    private SystemUserBp systemUserBp;
    
    @Inject
    private OrderLogBp orderLogBp;

    private PageNumberDisplay pageNumberDisplay = new PageNumberDisplay(0);

    private PageNumberDisplay assignedTasksPaginator = new PageNumberDisplay(0);

    private String taskType;
    
    private Map<Long, Date> tasksStarted = new HashMap<>();

    public PageNumberDisplay getPageNumberDisplay() {
        return pageNumberDisplay;
    }

    public void setPageNumberDisplay(PageNumberDisplay pageNumberDisplay) {
        this.pageNumberDisplay = pageNumberDisplay;
    }

    public PageNumberDisplay getAssignedTasksPaginator() {
        return assignedTasksPaginator;
    }

    public void setAssignedTasksPaginator(PageNumberDisplay assignedTasksPaginator) {
        this.assignedTasksPaginator = assignedTasksPaginator;
    }

    @PostConstruct
    public void init() {
        ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
        user = context.getUserPrincipal().getName();
        retrieve();
    }

    public String getTaskName(TaskSummary task) {
        return CustomerServicePropertiesLoader.getInstance().getCustomerServiceProperties().getString(task.getName());
    }

    public String getApdPo(TaskSummary task) {
        String description = task.getDescription();
        if (StringUtils.isBlank(description)) {
            return "N/A";
        }
        else {
            return description;
        }
    }

    public void refreshList() {
        pageNumberDisplay.setResultQuantity((int) userTaskService.numberOfAvailableTasks(user, taskType, this.showingCompletedTasks));
        assignedTasksPaginator.setResultQuantity((int) userTaskService.numberOfAssignedTasks(user, taskType, this.showingCompletedTasks));
        pageNumberDisplay.setPageSize(PageSize.TWENTY_FIVE);
        assignedTasksPaginator.setPageSize(PageSize.TEN);
        assignedTasks = userTaskService.getAssignedTasksPaginated(user, taskType, assignedTasksPaginator, this.showingCompletedTasks,false);
        availableTasks = userTaskService.getAvailableTasksPaginated(user, taskType, pageNumberDisplay, this.showingCompletedTasks);
        this.taskSelected = new HashMap<>();
    }
    
    public String claimAndStartTask(TaskSummary task) {
    	claimTask(task);
    	refreshList();
    	return startTask(task);
    }

    public void claimTask(TaskSummary task) {
        boolean claimSuccessful = false;
        try {
            if (isOwner(task)) {
                logger.info("Claiming task=" + task.getId() + " as user=" + user);
                claimSuccessful = userTaskService.claimTask(task.getId(), user);
            }
        }
        catch (Exception e) {
            logger.error("Exception when claiming task", e);
        }
        if (!claimSuccessful) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("You are not able to claim task " + task.getId()));
        }
    }

    public boolean canClaim(TaskSummary task) {
        return task.getStatus().equals(Status.Ready) && !this.isTaskWaitingForCompletion(task.getId());
    }

    public void releaseTask(TaskSummary task) {
        userTaskService.releaseTask(task.getId(), user);
    	refreshList();
    }

    public void delegateTask(Task task, String toUser) {
        if (task.getTaskData().getStatus().equals(Status.Suspended)) {
            userTaskService.resumeTask(task.getId(), "Administrator");
        }
        userTaskService.delegateTask(task.getId(), "Administrator", toUser);
    	refreshList();
    }

    public void forceClaim() {
        long taskId = Long.parseLong(taskToClaim);
        logger.info("Delegating taskid=" + taskId + ", to user=" + delegateUser);
        this.delegateTask(userTaskService.getTask(taskId), delegateUser);
    	refreshList();
    }

    public void resume() {
        long taskId = Long.parseLong(taskToRelease);
        logger.info("Resuming taskid=" + taskId);
        userTaskService.resumeTask(taskId, "Administrator");
        taskToRelease = "";
    	refreshList();
    }

    public String startTask(TaskSummary task) {
        if (isOwner(task)) {
            logger.info("Starting taskid=" + task.getId() + " taskname=" + task.getName());
            userTaskService.startTask(task.getId(), user);
            this.taskToWork = task.getId();
            this.taskToWorkName = task.getName();
            return TO_HUMAN_TASK_FORM;
        }
        else {
            return "";
        }
    }

    public String continueTask(TaskSummary task) {
        if (isOwner(task)) {
            logger.info("Continuing taskid=" + task.getId() + " taskname=" + task.getName());
            this.taskToWork = task.getId();
            this.taskToWorkName = task.getName();
            return TO_HUMAN_TASK_FORM;
        }
        else {
            return "";
        }
    }

    public boolean canContinue(TaskSummary task) {
        if (task == null) {
            logger.error("Can't continue a null task!!!!");
            return false;
        }
        return task.getStatus().equals(Status.InProgress) && !this.isTaskWaitingForCompletion(task.getId());
    }

    public boolean canStart(TaskSummary task) {
        if (task == null) {
            logger.error("Can't start a null task!!!!!");
            return false;
        }
        return task.getStatus().equals(Status.Reserved) && !this.isTaskWaitingForCompletion(task.getId());
    }

    public boolean canRelease(TaskSummary task) {
        if (task == null) {
            logger.error("Can't start a null task!!!!!");
            return false;
        }
        return (task.getStatus().equals(Status.Reserved) || task.getStatus().equals(Status.InProgress)) && !this.isTaskWaitingForCompletion(task.getId());
    }

    public void retrieve() {
        this.refreshList();
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the taskToWork
     */
    public Long getTaskToWork() {
        return taskToWork;
    }

    /**
     * @param taskToWork
     *            the taskToWork to set
     */
    public void setTaskToWork(Long taskToWork) {
        this.taskToWork = taskToWork;
    }

    /**
     * @return the availableTasks
     */
    public List<PhoenixTaskSummary> getAvailableTasks() {
        return this.availableTasks;
    }

    /**
     * @return the assignedTasks
     */
    public List<PhoenixTaskSummary> getAssignedTasks() {
        return this.assignedTasks;
    }

    public boolean isShowingCompletedTasks() {
        return showingCompletedTasks;
    }

    public void setShowingCompletedTasks(boolean showingCompletedTasks) {
        this.showingCompletedTasks = showingCompletedTasks;
    }

    public long getNumberOfAssignedTasks() {
        return userTaskService.numberOfAssignedTasks(this.user,false);
    }

    public String getTaskToWorkName() {
        return taskToWorkName;
    }

    public void setTaskToWorkName(String taskToWorkName) {
        this.taskToWorkName = taskToWorkName;
    }

    public String getTaskToClaim() {
        return taskToClaim;
    }

    public void setTaskToClaim(String taskToClaim) {
        this.taskToClaim = taskToClaim;
    }

    public boolean canForceClaim() {
        return this.isTechSupport();
    }

    public boolean isTechSupport() {
        return systemUserBp.getRolesForUser(user).contains(TECH_SUPPORT);
    }

    public boolean isManager() {
        return securityContext.hasPermission(MANAGER_PERMISSION);
    }

    public String getDelegateUser() {
        return delegateUser;
    }

    public void setDelegateUser(String delegateUser) {
        this.delegateUser = delegateUser;
    }

    public String getTaskToRelease() {
        return taskToRelease;
    }

    public void setTaskToRelease(String taskToRelease) {
        this.taskToRelease = taskToRelease;
    }

    public boolean isOwner(TaskSummary taskSummary) {
        Task task = userTaskService.getTask(taskSummary.getId());
        User owner = task.getTaskData().getActualOwner();
        if (owner != null && !owner.getId().equals(user)) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage("You are not the owner of this task, '" + owner.getId() + "' is!"));
            return false;
        }
        return true;
    }

    @Inject
    private SecurityContext securityContext;

    public boolean hasPermission(String permission) {
        return securityContext.hasPermission(permission);
    }

    public String getStatus(TaskSummary task) {

        if (task == null) {
        	return "";
        } else if (task.getStatus() == Status.Suspended || this.isTaskWaitingForCompletion(task.getId())) {
        	//if the task has been flagged as Suspended (for pre-3.0 tasks) or isTaskWaitingForCompleting
        	//returns true, displays the following text instead of the task's actual status.
            return "Completing...";
        }
        else {
            return task.getStatus().toString();
        }
    }

    /**
     * Takes a task ID, and determines if the task is probably waiting in workflow to be completed. It
     * does so by checking how long ago the task was completed - if it's more than a few minutes ago,
     * the task likely failed or didn't process, and returns false. If the task was completed more 
     * recently, returns true.
     * 
     * @param taskId
     * @return
     */
    private boolean isTaskWaitingForCompletion(Long taskId) {
    	if (taskId == null) {
    		return false;
    	}

        Calendar tasksStillProcessingCalendar = Calendar.getInstance();
        tasksStillProcessingCalendar.add(Calendar.MINUTE, -1 * TASK_PROCESSING_COMPLETE_MINUTES);
        Date taskStartDate = this.tasksStarted.get(taskId);
    	
    	return taskStartDate != null && taskStartDate.after(tasksStillProcessingCalendar.getTime());
    }

    @Inject
    CustomerOrderBp customerOrderBp;

    public String getAccountName(PhoenixTaskSummary task) {
        if (StringUtils.isNotBlank(task.getAccount())) {
            return task.getAccount();
        }
        String apdPo = task.getDescription();
        if (StringUtils.isBlank(apdPo)) {
            return "N/A";
        }
        else {
            String toReturn = customerOrderBp.getAccountName(apdPo);
            if (StringUtils.isBlank(toReturn)) {
                return "N/A";
            }
            return toReturn;
        }
    }

    @Inject
    private WorkflowLogBp workflowLogBp;

    public void historyOfTask(TaskSummary task) {
        this.historyTaskId = task.getId();
    }

    public List<WorkflowLog> getWorkflowLogList() {
        return workflowLogBp.getLogsForTask(historyTaskId);
    }

    public List<String> getAllTaskTypes() {
        return this.userTaskService.getAllOrderTypes();
    }

    public String getTaskType() {
        return taskType;
    }

    public void setTaskType(String taskType) {
        this.taskType = taskType;
    }

	public Map<PhoenixTaskSummary, Boolean> getTaskSelected() {
		return this.taskSelected;
	}

	public void setTaskSelected(Map<PhoenixTaskSummary, Boolean> taskSelected) {
		this.taskSelected = taskSelected;
	}
	
	public Map<Long, Date> getTasksStarted() {
		return tasksStarted;
	}

	public void setTasksStarted(Map<Long, Date> tasksStarted) {
		this.tasksStarted = tasksStarted;
	}

	public void claimSelectedTasks() {
		for (PhoenixTaskSummary task : taskSelected.keySet()) {
			if (taskSelected.get(task) != null && taskSelected.get(task)) {
				this.claimTask(task);
			}
		}
    	refreshList();
	}

    public void duplicate() {
        long taskId = Long.parseLong(taskToRelease);
		this.removeTask(taskId, TaskEvent.DUPLICATE);
		PhoenixTaskSummary taskSummary = userTaskService.taskToSummary(userTaskService.getTask(taskId));
		orderLogBp.logChangeOrder(customerOrderBp.searchByApdPo(getApdPo(taskSummary)), "Removed the task " + getTaskName(taskSummary) + " id:" + taskId +"  (because it was a duplicate)", user);
    }

    public void alreadyFixed() {
    	long taskId = Long.parseLong(taskToRelease);
        this.removeTask(taskId, TaskEvent.ALREADY_FIXED);
    	PhoenixTaskSummary taskSummary = userTaskService.taskToSummary(userTaskService.getTask(taskId));
		orderLogBp.logChangeOrder(customerOrderBp.searchByApdPo(getApdPo(taskSummary)), "Removed the task " + getTaskName(taskSummary) + " id:" + taskId +"  (because it had already been fixed)", user);

    }
    
    private void removeTask(long taskId, TaskEvent event) {
        logger.warn("Removing taskid=" + taskId + " because of " + event);
        userTaskService.removeTask(taskId, "Administrator", event);
        taskToRelease = "";
    	refreshList();
    }
}
