package com.apd.phoenix.customerservice.view.jsf.bean.report;

import java.io.File;
import java.util.List;
import javax.enterprise.context.Conversation;
import javax.inject.Inject;
import com.apd.phoenix.service.utility.CsvExportService;
import com.apd.phoenix.service.utility.FileDownloader;

public abstract class AbstractReportBean<T> {

    @Inject
    private CsvExportService csvService;

    @Inject
    private ViewReportResultsBean viewBean;

    @Inject
    private Conversation conversation;

    public void retrieve() {
        if (conversation.isTransient()) {
            conversation.begin();
        }
    }

    public String downloadAsCsv() {
        File toDownload = csvService.getFile(this.getResultHeader(), this.getResults());
        FileDownloader.downloadFile(toDownload);
        toDownload.delete();
        return null;
    }

    public String viewResultsInPage() {
        viewBean.setReportBean(this);
        return "reportResult.xhtml?faces-redirect=true";
    }

    protected abstract List<Object[]> getResults();

    protected abstract String[] getResultHeader();
}
