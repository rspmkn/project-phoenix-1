package com.apd.phoenix.customerservice.view.jsf.bean.workflow;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Stateful;
import javax.enterprise.context.ConversationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.component.html.HtmlInputText;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.DataModel;
import javax.faces.model.SelectItem;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.lang.StringUtils;
import org.richfaces.component.UIDataTable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.apd.phoenix.core.PageNumberDisplay;
import com.apd.phoenix.customerservice.view.jsf.bean.ViewUtils;
import com.apd.phoenix.customerservice.view.jsf.bean.login.LoginBean;
import com.apd.phoenix.customerservice.view.jsf.utility.CustomerServicePropertiesLoader;
import com.apd.phoenix.service.business.CashoutPageBp;
import com.apd.phoenix.service.business.CommentBp;
import com.apd.phoenix.service.business.CustomerOrderBp;
import com.apd.phoenix.service.business.ItemCategoryBp;
import com.apd.phoenix.service.business.LineItemBp;
import com.apd.phoenix.service.business.OrderLogBp;
import com.apd.phoenix.service.business.OrderStatusBp;
import com.apd.phoenix.service.business.PoNumberBp;
import com.apd.phoenix.service.business.VendorBp;
import com.apd.phoenix.service.message.api.MessageService;
import com.apd.phoenix.service.model.AccountXCredentialXUser;
import com.apd.phoenix.service.model.Comment;
import com.apd.phoenix.service.model.Credential;
import com.apd.phoenix.service.model.CustomerOrder;
import com.apd.phoenix.service.model.FieldOptions;
import com.apd.phoenix.service.model.ItemCategory;
import com.apd.phoenix.service.model.LineItem;
import com.apd.phoenix.service.model.OrderAttachment;
import com.apd.phoenix.service.model.PoNumber;
import com.apd.phoenix.service.model.MessageMetadata.MessageType;
import com.apd.phoenix.service.model.OrderStatus.OrderStatusEnum;
import com.apd.phoenix.service.model.UnitOfMeasure;
import com.apd.phoenix.service.model.Vendor;
import com.apd.phoenix.service.model.dto.POAcknowledgementDto;
import com.apd.phoenix.service.persistence.jpa.UnitOfMeasureDao;
import com.apd.phoenix.web.FileUploadBean;
import com.apd.phoenix.web.cashout.AbstractCashoutBean;

/**
 * This class provides backing functionality for the manual-order.xhtml page. It contains several Converters, a 
 * CustomerOrder object, and methods to add items to the order.
 * 
 * @author RHC
 *
 */
@Named
@Stateful
@ConversationScoped
public class ManualOrderBean extends AbstractCashoutBean implements Serializable {

    private static final Logger logger = LoggerFactory.getLogger(ManualOrderBean.class);

    public static final String DEFAULT_UNSPSC = "44120000";
    private static final long serialVersionUID = -2388341212538843974L;

    private static final String STATE_EXEMPT_FOR_SALES_TAX = "state exemptions for sales tax";
    private static final String SHIP_STATES = "ship states";

    private List<LineItem> lineItems;

    private UIDataTable table;
    private DataModel<LineItem> selectedRow;
    private String deliveryMethod;
    private boolean showAddComment = false;
    private List<Comment> orderComments = new ArrayList<>();
    private PageNumberDisplay commentsPagination = new PageNumberDisplay(0);
    private Comment currentComment = new Comment();
    private boolean createRfq;
    private BigDecimal calculateMarginPersentage = BigDecimal.ZERO;
    private BigDecimal estimatedShippingAmount  = BigDecimal.ZERO;
    private Date expirationDate;
    
    public List<LineItem> getLineItems() {
        return this.lineItems;
    }

    public void setLineItems(List<LineItem> lineItems) {
        this.lineItems = lineItems;
    }

    public void newItem() {
        if (this.lineItems != null) {
            return;
        }
        createDefaultLineItems();
    }

    @Inject
    private VendorBp vendorBp;

    @Inject
    private OrderLogBp orderLogBp;

    private List<Vendor> allVendors;

    @Inject
    private CustomerOrderBp customerOrderBp;

    @Inject
    private CashoutPageBp cashoutBp;
    
    @Inject
    private CommentBp commentBp;
    
    @Inject
	private LoginBean loginBean;
    
    @Inject
	private FileUploadBean fileUploadBean;
    
    @Inject
	private Vendor vendor = new Vendor();
    
    @Inject
    LineItemBp lineItemBp;
    
    @Inject
    private OrderStatusBp statusBp;
    
    @Inject
    MessageService messageService;
    
    @Inject
    private PoNumberBp poBp;

    public Converter getVendorConverter() {

        return new Converter() {

            @Override
            public Object getAsObject(FacesContext context, UIComponent component, String value) {
                return vendorBp.findById(Long.valueOf(value), Vendor.class);
            }

            @Override
            public String getAsString(FacesContext context, UIComponent component, Object value) {

                if (value == null) {
                    return "";
                }

                return String.valueOf(((Vendor) value).getId());
            }
        };
    }

    public List<Vendor> getAllVendors() {
        if (allVendors == null) {
            List<Vendor> results = vendorBp.findAll(Vendor.class, 0, 0);
            allVendors = new ArrayList<Vendor>();
            for (Vendor vendor : results) {
                if (createRfq) {
                	allVendors.add(vendor);
                } else if (vendor.getCommunicationMessageType().equals(MessageType.EMAIL)) {
                	allVendors.add(vendor);
                }    
            }
        }
        return allVendors;
    }

    @Inject
    private UnitOfMeasureDao uomDao;

    private List<UnitOfMeasure> allUnitsOfMeasure;

    public Converter getUnitOfMeasureConverter() {

        return new Converter() {

            @Override
            public Object getAsObject(FacesContext context, UIComponent component, String value) {
                return uomDao.findById(Long.valueOf(value), UnitOfMeasure.class);
            }

            @Override
            public String getAsString(FacesContext context, UIComponent component, Object value) {

                if (value == null) {
                    return "";
                }

                return String.valueOf(((UnitOfMeasure) value).getId());
            }
        };
    }

    public List<UnitOfMeasure> getAllUnitsOfMeasure() {

        if (this.allUnitsOfMeasure == null) {
            this.allUnitsOfMeasure = uomDao.findAll(UnitOfMeasure.class, 0, 0);
        }
        return this.allUnitsOfMeasure;
    }

    @Inject
    private ItemCategoryBp categoryBp;

    private List<ItemCategory> allCategories;

    public Converter getCategoryConverter() {

        return new Converter() {

            @Override
            public Object getAsObject(FacesContext context, UIComponent component, String value) {
                return categoryBp.findById(Long.valueOf(value), ItemCategory.class);
            }

            @Override
            public String getAsString(FacesContext context, UIComponent component, Object value) {

                if (value == null) {
                    return "";
                }

                return String.valueOf(((ItemCategory) value).getId());
            }
        };
    }

    public List<ItemCategory> getAllCategories() {

        if (this.allCategories == null) {
            allCategories = categoryBp.findAll(ItemCategory.class, 0, 0);
        }
        return allCategories;
    }

    @Inject
    private ManualOrderSelectionBean selectionBean;

    @Override
    protected AccountXCredentialXUser getCurrentAXCXU() {
        return selectionBean.getAxcxu();
    }

    @Override
    protected Map<String, String> credentialPropertiesMap() {
        return selectionBean.getPropertyMap();
    }

    @Override
    protected PropertiesConfiguration getProperties() {
        return CustomerServicePropertiesLoader.getInstance().getCustomerServiceProperties();
    }

    @Override
    protected void prePlaceOrder() {
        // do nothing, no pre-order setup required
    }

    @Override
    protected String postPlaceOrder() {
        ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
        if (context != null && context.getUserPrincipal() != null
                && !StringUtils.isEmpty(context.getUserPrincipal().getName())) {
            orderLogBp.logChangeOrder(this.currentOrder, "create manual order", context.getUserPrincipal().getName());
        }
        else {
            logger.error("Could not determine user who placed order.");
        }
        return "manualOrderConfirm?faces-redirect=true";
    }

    @Override
    public String placeOrder() {
        
        if (validate()) {
            return null;
        }
        return super.placeOrder();
    }

    @Override
    protected String orderTypeValue() {
    	if(this.isCreateRfq()) {
    		return "URL";
    	}
        return "MANUAL";
    }

    @Override
    protected boolean canPersistBeforePlace() {
        return false;
    }

    public enum TaxableType {
        Y("Y", true), N("N", false);

        private final String label;
        private final boolean value;

        private TaxableType(String label, boolean value) {
            this.label = label;
            this.value = value;
        }

        public String getLabel() {
            return this.label;
        }

        public boolean getValue() {
            return this.value;
        }
    }

    public SelectItem[] getTaxableTypes() {
        SelectItem[] items = new SelectItem[TaxableType.values().length];
        int i = 0;
        for (TaxableType g : TaxableType.values()) {
            items[i++] = new SelectItem(g.getValue(), g.getLabel());
        }
        return items;
    }

    public boolean isDefaultTaxable() {
        boolean defaultTaxable = true;
        //marks the order as taxable, unless "charge sales tax" is set to "no" on the Credential
        String chargeSales = credentialPropertiesMap().get("charge sales tax");
        if (chargeSales != null && (chargeSales.equalsIgnoreCase("no") || chargeSales.equalsIgnoreCase("false"))) {
            defaultTaxable = false;
        }

        if (credentialPropertiesMap().get(STATE_EXEMPT_FOR_SALES_TAX) != null) {

            String exmptState = credentialPropertiesMap().get(STATE_EXEMPT_FOR_SALES_TAX);

            if (this.getPageValues().getValue().get(this.getPageValues().getField().get(SHIP_STATES)) instanceof FieldOptions) {
                FieldOptions state = (FieldOptions) this.getPageValues().getValue().get(
                        this.getPageValues().getField().get(SHIP_STATES));

                if (state != null && state.getValue().equals(exmptState)) {
                    defaultTaxable = false;
                }
            }
        }
        return defaultTaxable;
    }

    public String addItemsToOrder() {
        //get all existing value but set "editable" to false
    	boolean lineItemsUpdated = false;
    	int lineNumber = 0;
        for (LineItem item : this.lineItems) {
        	lineNumber++;
            if (item.getVendor() != null && StringUtils.isNotEmpty(item.getSupplierPartId()) 
            		&& item.getUnitOfMeasure() != null 
            		&& item.getQuantity() > 0) {
            	item.setLineNumber(lineNumber);
                if (StringUtils.isBlank(item.getApdSku())) {
                    item.setApdSku(item.getSupplierPartId());
                }
                if(item.getId() != null) {
                	lineItemBp.update(item);
                	lineItemsUpdated = true;
                } else {
                	this.addLineItem(item, item.getQuantity(), null);
                }                
            }

        }
        
        BigDecimal estimatedShippingAmount = BigDecimal.ZERO;
        if(!createRfq) {
	        for (LineItem item : this.lineItems) {
	        	if (item.getEstimatedShippingAmount() != null && item.getEstimatedShippingAmount().compareTo(BigDecimal.ZERO) == 1) {
	        		this.estimatedShippingAmount = estimatedShippingAmount.add(item.getEstimatedShippingAmount());
	        	}
	        }
        }
        currentOrder.setEstimatedShippingAmount(this.estimatedShippingAmount);
        if(lineItemsUpdated) {
        	this.setCurrentOrder(this.customerOrderBp.update(this.getCurrentOrder()));
        	this.setCurrentOrder(this.customerOrderBp.hydrateForOrderDetails(this.getCurrentOrder()));
        }
        this.lineItems = ViewUtils.asList(this.getCurrentOrder().getItems());
        createDefaultLineItems();
        return null;
    }

    public void createDefaultLineItems() {
    	if(this.lineItems == null) {
    		this.lineItems = new ArrayList<>();
    	}
    	boolean isDefaultTaxable = this.isDefaultTaxable();
        LineItem item = null;
        for (int i = 0; i <= 4; i++) {
        	item = new LineItem();
        	item.setOrder(this.currentOrder);
        	item.setQuantity(1);
        	item.setTaxable(isDefaultTaxable);
        	item.setLineNumber(-1); //Used by the forge view
        	item.setUnspscClassification(DEFAULT_UNSPSC);
        	item.setCost(BigDecimal.ZERO);
        	if(StringUtils.isNotEmpty(this.getDeliveryMethod())) {
        		item.setDeliveryMethod(this.getDeliveryMethod());
        	} else {
        		item.setDeliveryMethod(DeliveryMethod.DROP_SHIP.getLabel());
        	}
        	lineItems.add(item);
        }
    }

    public void removeLineItem(LineItem lineItem) {
        super.removeLineItem(lineItem);
        lineItems.remove(lineItem);
        if(lineItem.getId() != null) {
        	lineItemBp.delete(lineItem.getId(), LineItem.class);
        	this.setCurrentOrder(this.customerOrderBp.update(this.getCurrentOrder()));
        }
        int lineNumber = 0;
        for (LineItem item : ViewUtils.asList(this.getCurrentOrder().getItems())) {
        	lineNumber++;
        	item.setLineNumber(lineNumber);
        	  if(lineItem.getId() != null) {
        		  lineItemBp.update(item);
        	  }
        }
        
    }

    public BigDecimal getTotalMerch() {
        BigDecimal totalMerch = BigDecimal.ZERO;
        for (LineItem item : lineItems) {
            totalMerch = totalMerch.add(item.getTotalPrice());
        }
        return totalMerch;
    }

    public BigDecimal getTotalShipping() {
        BigDecimal totalShipping = BigDecimal.ZERO;
        for (LineItem item : lineItems) {
            totalShipping = totalShipping.add(item.getEstimatedShippingAmount());
        }
        return totalShipping;
    }
    
    public BigDecimal getTotalExtendedCost() {
        BigDecimal totalExtCost = BigDecimal.ZERO;
        for (LineItem item : lineItems) {
        	totalExtCost = totalExtCost.add(item.getExtendedCost());
        }
        return totalExtCost;
    }
    
    public BigDecimal getTotalExtendedWeight() {
    	 BigDecimal totalExtWeight = BigDecimal.ZERO;
         for (LineItem item : lineItems) {
        	 totalExtWeight = totalExtWeight.add(this.getExtendedWeight(item));
         }
         return totalExtWeight;
    }    
    
    public SelectItem[] getDeliveryMethods() {
        SelectItem[] items = new SelectItem[DeliveryMethod.values().length];
        int i = 0;
        for (DeliveryMethod g : DeliveryMethod.values()) {
            items[i++] = new SelectItem(g.getLabel(), g.getLabel());
        }
        return items;
    }

    public static enum DeliveryMethod {
        CUSTOMER_PICK("CUSTOMER PICK"), DROP_SHIP("DROP SHIP"), WILL_CALL("WILL CALL");

        private final String label;

        private DeliveryMethod(String label) {
            this.label = label;
        }

        public String getLabel() {
            return this.label;
        }
    }

    public String fetchUnspscClassification(ValueChangeEvent event) {
        ItemCategory category = (ItemCategory) event.getNewValue();
        Integer currIndex = getTable().getRowIndex();
        LineItem item = this.lineItems.get(currIndex.intValue());
        if(item.getItem() != null && StringUtils.isNotEmpty(item.getItem().getProtectedCommodityCode())) {
        	item.setUnspscClassification(item.getItem().getProtectedCommodityCode());
        }
        else if (category != null && StringUtils.isNotEmpty(category.getCommodityCode())) {
            item.setUnspscClassification(category.getCommodityCode());
        }
        else {
            item.setUnspscClassification(DEFAULT_UNSPSC);
        }
        return null;

    }

    public boolean isRemovable(LineItem item) {
        return (this.getCurrentOrder().getItems().contains(item));
    }   

    public void setDeliveryMethod(String deliveryMethod) {
        this.deliveryMethod = deliveryMethod;
    }

    public UIDataTable getTable() {
        return table;
    }

    public void setTable(UIDataTable table) {
        this.table = table;
    }

    public DataModel<LineItem> getSelectedRow() {
        return selectedRow;
    }

    public void setSelectedRow(DataModel<LineItem> selectedRow) {
        this.selectedRow = selectedRow;
    }

    public String deliveryMethodToLineItems(ValueChangeEvent event) {
        String deliveryMethod = (String) event.getNewValue();
        for (DeliveryMethod g : DeliveryMethod.values()) {
            if (g.getLabel().equals(deliveryMethod)) {
                for (LineItem item : lineItems) {
                    item.setDeliveryMethod(g.getLabel());
                }
                break;
            }
        }
        return null;

    }

    public String getDeliveryMethod() {
        return deliveryMethod;
    }

    @Override
    protected void createOrder() {
        if (selectionBean.getManualOrder() != null) {
            this.setCurrentOrder(this.customerOrderBp.hydrateForOrderDetails(this.selectionBean.getManualOrder()));
            this.lineItems = ViewUtils.asList(this.getCurrentOrder().getItems());
            createDefaultLineItems();
        }
        else {
            super.createOrder();
            if(this.isCreateRfq()) {
	            for (PoNumber ponum : currentOrder.getPoNumbers()) {
	            	if(CustomerOrder.APD_PO_VALUE.equals(ponum.getType().getName())) {
	            		ponum.setValue(ponum.getValue()+"-Q");
	            	}
	            }
	            customerOrderBp.setOrderStatus(currentOrder, statusBp.getOrderStatus(OrderStatusEnum.REQUEST_FOR_QUOTE));
            }
        }
    }

    @PostConstruct
    public void create() {

        if (getConversation().isTransient()) {
            getConversation().begin();
        }
        if (this.getCurrentOrder() != null)
            getContainer().init(cashoutBp.getFromCredentialForCashout(this.getCurrentAXCXU().getCredential()));

        for (String fieldName : getContainer().getField().keySet()) {
            setOptionsForUser(fieldName);
        }
        if (selectionBean.getManualOrder() != null) {
            getContainer().manualOrderToPage(
                    this.customerOrderBp.hydrateForOrderDetailsDifferentTransaction(selectionBean.getManualOrder()));
            orderComments = new ArrayList<>();
    		orderComments.addAll(selectionBean.getManualOrder().getComments());
    		commentsPagination = new PageNumberDisplay(orderComments.size());
    		Collections.sort(orderComments, new CommentComparator());
        }
        
    }

	public boolean isShowAddComment() {
		return showAddComment;
	}

	public void setShowAddComment(boolean showAddComment) {
		this.showAddComment = showAddComment;
	}
	
	public List<Comment> getOrderComments() {
		return this.orderComments.subList(
				commentsPagination.getSize() * (commentsPagination.getPage() - 1), 
				Math.min(orderComments.size(), commentsPagination.getSize() * commentsPagination.getPage()));
	}

	public PageNumberDisplay getCommentsPagination() {
		return commentsPagination;
	}

	public void setCommentsPagination(PageNumberDisplay commentsPagination) {
		this.commentsPagination = commentsPagination;
	}

	public void setOrderComments(List<Comment> orderComments) {
		this.orderComments = orderComments;
	}

	public Comment getCurrentComment() {
		return currentComment;
	}

	@Lock(LockType.WRITE)
	public void setCurrentComment(Comment currentComment) {
		this.currentComment = currentComment;
	}

	@Lock(LockType.WRITE)
	public void addCurrentComment() {
		if (this.currentComment.getId() == null) {
			this.currentComment.setCommentDate(new Date());
			this.currentComment.setUser(loginBean.getSystemUser());
			this.getCurrentOrder().getComments().add(this.currentComment);
			this.setCurrentOrder(this.commentBp.addComment(this.getCurrentOrder(), currentComment));
			orderComments = new ArrayList<>();
			orderComments.addAll(this.getCurrentOrder().getComments());
			commentsPagination = new PageNumberDisplay(orderComments.size());
			Collections.sort(orderComments, new CommentComparator());
		}
		this.setCurrentComment(new Comment());
		this.showAddComment = false;
	}

	@Lock(LockType.WRITE)
	public void discardCurrentComment() {
		this.showAddComment = false;
		this.setCurrentComment(new Comment());
	}
	
	private class CommentComparator implements Comparator<Object> {

	    @Override
	    public int compare(Object arg0, Object arg1) {
	    	if (!(arg1 instanceof Comment)) {
	    		return -1;
	    	}
	    	if (!(arg0 instanceof Comment)) {
	    		return 1;
	    	}
	    	Comment comment0 = (Comment) arg0;
	    	Comment comment1 = (Comment) arg1;
	    	if (comment0.getCommentDate().before(comment1.getCommentDate())) {
	    		return -1;
	    	}
	    	if (comment0.getCommentDate().after(comment1.getCommentDate())) {
	    		return 1;
	    	}
	    	return 0;
	    }
	}
	
	@Lock(LockType.WRITE)
    public void saveUploadedFile(){
    	OrderAttachment file = getFileUploadBean().saveUploadedFile(null, loginBean.getSystemUser());	    	
    	this.getCurrentOrder().getAttachments().add(file);
    	this.setCurrentOrder(this.customerOrderBp.update(this.getCurrentOrder()));
    	
    }
	
	public SelectItem[] getCommunicationMessageType() {
        SelectItem[] items = new SelectItem[MessageType.values().length];
        int i = 0;
        for (MessageType g : MessageType.values()) {
            items[i++] = new SelectItem(g, g.getLabel());
        }
        return items;
    }
	
	public String createVendor() {
		if(StringUtils.isNotEmpty(this.vendor.getName())) {
			if(vendorBp.findByNameCaseInsensitive(this.vendor.getName()) == null) {
				vendorBp.create(this.vendor);
				this.allVendors = null;
				getAllVendors();
				this.vendor = new Vendor();
			} else {				 
				FacesContext.getCurrentInstance().addMessage(
                        null,
                        new FacesMessage(FacesMessage.SEVERITY_ERROR, "Vendor already exists ", "You need to select a unique vendor name."));
				return null;
			}
		}
		return null;
	}
	
	public void clearSelectedVendor() {
		this.vendor = new Vendor();	
	}

	public FileUploadBean getFileUploadBean() {
		return this.fileUploadBean;
	}

	public void setFileUploadBean(FileUploadBean fileUploadBean) {
		this.fileUploadBean = fileUploadBean;
	}

	public Vendor getVendor() {
		return vendor;
	}

	public void setVendor(Vendor vendor) {
		this.vendor = vendor;
	}	
	
	public String holdOrder() {
		
		if(this.getCurrentOrder().getId() != null) {
			this.setCurrentOrder(this.customerOrderBp.update(this.getCurrentOrder()));
		}
		return super.holdOrder();
	}
	
	/**
     * This method takes a LineItem, the quantity of the item, and the amount to charge for shipping, and adds the 
     * item to the currentOrder.
     * 
     * If the specified shipRate is not null, the shiprate is used to set the estimated shipping amount on the item.
     * 
     * @param lineItem
     * @param quantity
     * @param shipRate
     */
	@Override
    protected void addLineItem(LineItem lineItem, int quantity, BigDecimal shipRate) {

        lineItem.setQuantity(quantity);
        lineItem.setOriginalQuantity(quantity);
        lineItem.setOrder(currentOrder);
        if (shipRate != null && shipRate.compareTo(BigDecimal.ZERO) != 0) {
            lineItem.setEstimatedShippingAmount(lineItem.getTotalPrice().multiply(shipRate));
        }               
        currentOrder.getItems().add(lineItem);

    }	
	
	public boolean isCreateRfq() {
		return createRfq;
	}

	public void setCreateRfq(boolean createRfq) {
		this.createRfq = createRfq;
	}	
	
	public BigDecimal calculateMarginPersentage(LineItem lineItem) {
		 if (lineItem.getCost() != null && lineItem.getCost().compareTo(BigDecimal.ZERO) > 0 && lineItem.getUnitPrice() != null
	                && lineItem.getUnitPrice().compareTo(BigDecimal.ZERO) > 0) {
	            return (((lineItem.getUnitPrice().subtract(lineItem.getCost())).divide(lineItem.getCost(), 2, RoundingMode.HALF_UP))
	                    .multiply(BigDecimal.valueOf(100)));
	      }
		 return BigDecimal.ZERO;
	}
	
	public void setCalculateMarginPersentage(BigDecimal bigDecimal) {
		
	}
	
	public BigDecimal getCalculateMarginPersentage() {
		return BigDecimal.ZERO;
	}
	
	/**
     * This is a utility method, to return the total weight of this line item on this order.
     * 
     * @return
     */
    public BigDecimal getExtendedWeight(LineItem lineItem) {
    	if(lineItem.getWeight() != null) {
    		return lineItem.getWeight().multiply(BigDecimal.valueOf(lineItem.getQuantity()));
    	} else if(lineItem.getItem() != null && lineItem.getItem().getItemWeight() != null) {
        	return lineItem.getItem().getItemWeight().multiply(BigDecimal.valueOf(lineItem.getQuantity()));
        } 
        return BigDecimal.ZERO;
    }	
	
	 public String  cancelQuote() {
		 customerOrderBp.setOrderStatus(this.getCurrentOrder(), statusBp.getOrderStatus(OrderStatusEnum.CANCELED_QUOTE));
		 this.getContainer().orderToPage(this.getCurrentOrder());
		 ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
	        if (context != null && context.getUserPrincipal() != null
	                && !StringUtils.isEmpty(context.getUserPrincipal().getName())) {
	            orderLogBp.logChangeOrder(this.getCurrentOrder(), "request for quote has been cancelled", context.getUserPrincipal().getName());
	        }
	        else {
	            logger.error("Could not determine user who cancelled quote.");
	        }
	        return "quoteOrderConfirm?faces-redirect=true";
	 }
	 
	 public String  rejectQuote() {
		 customerOrderBp.setOrderStatus(this.getCurrentOrder(), statusBp.getOrderStatus(OrderStatusEnum.REJECTED_QUOTE));
		 this.getContainer().orderToPage(this.getCurrentOrder());
		 ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
	        if (context != null && context.getUserPrincipal() != null
	                && !StringUtils.isEmpty(context.getUserPrincipal().getName())) {
	            orderLogBp.logChangeOrder(this.getCurrentOrder(), "submitted quote has been rejected", context.getUserPrincipal().getName());
	        }
	        else {
	            logger.error("Could not determine user who cancelled quote.");
	        }
	        return "quoteOrderConfirm?faces-redirect=true";
	 }
	 
	 public String  submitQuote() {
		 if(validate()) {
			 return null;
		 }		 
		 super.holdOrder();
		 
		 this.currentOrder.setExpiryDate(expirationDate);		 
		 if(this.getCurrentOrder().getId() != null) {
			 this.currentOrder = customerOrderBp.update(this.currentOrder);
		 }
		 
		 customerOrderBp.setOrderStatus(this.currentOrder, statusBp.getOrderStatus(OrderStatusEnum.SUBMITTED_QUOTE));
		 POAcknowledgementDto poAcknowledgementDto = customerOrderBp.createFullAcknowledgement(this.currentOrder);
		 poAcknowledgementDto = customerOrderBp.populateOrderAcknowledgement(this.currentOrder, poAcknowledgementDto);
		 
	     messageService.submitRequestForQuote(poAcknowledgementDto, MessageType.EMAIL);
		 this.getContainer().orderToPage(currentOrder);
		 ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
	        if (context != null && context.getUserPrincipal() != null
	                && !StringUtils.isEmpty(context.getUserPrincipal().getName())) {
	            orderLogBp.logChangeOrder(this.currentOrder, "request for quote has been submited", context.getUserPrincipal().getName());
	        }
	        else {
	            logger.error("Could not determine user who cancelled quote.");
	        }
	        return "quoteOrderConfirm?faces-redirect=true";
	 }
	 
	 public String  acceptQuote() {
		 if(validate()) {
			 return null;
		 }		 
		 
		
		this.getContainer().orderToPage(getCurrentOrder());		
		ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
		    if (context != null && context.getUserPrincipal() != null
		            && !StringUtils.isEmpty(context.getUserPrincipal().getName())) {
		        orderLogBp.logChangeOrder(this.getCurrentOrder(), "The RFQ has been accepted", context.getUserPrincipal().getName());
		}
		else {
		    logger.error("Could not determine user who accepted quote.");
		}
		
		super.placeOrder();
		
		for (PoNumber ponum : getCurrentOrder().getPoNumbers()) {
         	if(CustomerOrder.APD_PO_VALUE.equals(ponum.getType().getName())) {
         		if(ponum.getValue().indexOf("-Q") > 0) {       			 
         			ponum.setValue(ponum.getValue().substring(0, ponum.getValue().indexOf("-")));
         			poBp.update(ponum);
         		}
         	}
	    }
		setCurrentOrder(customerOrderBp.update(getCurrentOrder()));
		
		POAcknowledgementDto poAcknowledgementDto = customerOrderBp.createFullAcknowledgement(this.currentOrder);
		poAcknowledgementDto = customerOrderBp.populateOrderAcknowledgement(this.currentOrder, poAcknowledgementDto);
		messageService.sendProformaInvoice(poAcknowledgementDto, MessageType.EMAIL);
		
		return "quoteOrderConfirm?faces-redirect=true";
		
	 }

	public BigDecimal getEstimatedShippingAmount() {
		return estimatedShippingAmount;
	}

	public void setEstimatedShippingAmount(BigDecimal estimatedShippingAmount) {
		this.estimatedShippingAmount = estimatedShippingAmount;
	}
	
	public boolean showSubmitQuote() {
		return OrderStatusEnum.REQUEST_FOR_QUOTE.getValue().equals(this.currentOrder.getStatus().getValue()) || 
				OrderStatusEnum.SUBMITTED_QUOTE.getValue().equals(this.currentOrder.getStatus().getValue());
	}
	
	public boolean showAcceptQuote() {
		return OrderStatusEnum.SUBMITTED_QUOTE.getValue().equals(this.currentOrder.getStatus().getValue());
	}
	
	public boolean showCancelQuote() {
		return this.currentOrder.getId() != null && OrderStatusEnum.REQUEST_FOR_QUOTE.getValue().equals(this.currentOrder.getStatus().getValue());
	}
	
	public boolean showRejectQuote() {
		return OrderStatusEnum.SUBMITTED_QUOTE.getValue().equals(this.currentOrder.getStatus().getValue());
	}

	public Date getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(Date expirationDate) {
		this.expirationDate = expirationDate;
	}
	
	public boolean validate() {
		boolean hasFailed = false;
        if (this.currentOrder.getItems().isEmpty()) {
            FacesContext.getCurrentInstance().addMessage(
                    null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, "There must be at least one item on the order",
                            "There must be at least one item on the order"));
            hasFailed = true;
        }
        for (LineItem item : this.currentOrder.getItems()) {
            if (item.getVendor() == null) {
                FacesContext.getCurrentInstance().addMessage(
                        null,
                        new FacesMessage(FacesMessage.SEVERITY_ERROR, "Item ".concat(item.getLineNumber() + "").concat(
                                " is missing a Vendor"), "Item ".concat(item.getLineNumber() + "").concat(
                                " is missing a Vendor")));
                hasFailed = true;
            }
            if (StringUtils.isBlank(item.getApdSku())) {
                FacesContext.getCurrentInstance().addMessage(
                        null,
                        new FacesMessage(FacesMessage.SEVERITY_ERROR, "Item ".concat(item.getLineNumber() + "").concat(
                                " is missing a SKU"), "Item ".concat(item.getLineNumber() + "").concat(
                                " is missing a SKU")));
                hasFailed = true;
            }
            if (item.getCategory() == null) {
                FacesContext.getCurrentInstance().addMessage(
                        null,
                        new FacesMessage(FacesMessage.SEVERITY_ERROR, "Item ".concat(item.getLineNumber() + "").concat(
                                " is missing a Category"), "Item ".concat(item.getLineNumber() + "").concat(
                                " is missing a Category")));
                hasFailed = true;
            }
            if (StringUtils.isBlank(item.getDescription())) {
                FacesContext.getCurrentInstance().addMessage(
                        null,
                        new FacesMessage(FacesMessage.SEVERITY_ERROR, "Item ".concat(item.getLineNumber() + "").concat(
                                " is missing a Description"), "Item ".concat(item.getLineNumber() + "").concat(
                                " is missing a Description")));
                hasFailed = true;
            }
            if (item.getUnitOfMeasure() == null) {
                FacesContext.getCurrentInstance().addMessage(
                        null,
                        new FacesMessage(FacesMessage.SEVERITY_ERROR, "Item ".concat(item.getLineNumber() + "").concat(
                                " is missing a Unit of Measure"), "Item ".concat(item.getLineNumber() + "").concat(
                                " is missing a Unit of Measure")));
                hasFailed = true;
            }
            if (StringUtils.isBlank(item.getUnspscClassification())) {
                FacesContext.getCurrentInstance().addMessage(
                        null,
                        new FacesMessage(FacesMessage.SEVERITY_ERROR, "Item ".concat(item.getLineNumber() + "").concat(
                                " is missing a UNSPSC"), "Item ".concat(item.getLineNumber() + "").concat(
                                " is missing a UNSPSC")));
                hasFailed = true;
            }
            if (item.getUnitPrice().compareTo(BigDecimal.ZERO) != 1) {
                FacesContext.getCurrentInstance().addMessage(
                        null,
                        new FacesMessage(FacesMessage.SEVERITY_ERROR, "Item ".concat(item.getLineNumber() + "").concat(
                                " Unit Price must be greater then zero"), "Item ".concat(item.getLineNumber() + "").concat(
                                " Unit Price must be greater then zero")));
                hasFailed = true;
            }
            if (item.getCost().compareTo(BigDecimal.ZERO) != 1) {
                FacesContext.getCurrentInstance().addMessage(
                        null,
                        new FacesMessage(FacesMessage.SEVERITY_ERROR, "Item ".concat(item.getLineNumber() + "").concat(
                                " Unit Cost must be greater then zero"), "Item ".concat(item.getLineNumber() + "").concat(
                                " Unit Cost must be greater then zero")));
                hasFailed = true;
            } 
            
        }
        return hasFailed;
	}
}
