package com.apd.phoenix.customerservice.view.jsf.bean.workflow;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateful;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.apd.phoenix.service.model.dto.LineItemDto;
import com.apd.phoenix.service.model.dto.PurchaseOrderDto;

@Named
@Stateful
@RequestScoped
public class ReviewOrderRequestBean extends OrderHumanTaskFormBean {

    private static final Logger LOGGER = LoggerFactory.getLogger(ReviewOrderRequestBean.class);

    PurchaseOrderDto orderRequestDto;
    
    List<LineItemDto> invalidItems = new ArrayList<>();

    @Override
    public void init() {
    	orderRequestDto = (PurchaseOrderDto) this.taskContent.get("orderRequestDto");
    	for (LineItemDto lineItemDto : orderRequestDto.getItems()) {
    		if (lineItemDto.getValid() == null || !lineItemDto.getValid()) {
    			invalidItems.add(lineItemDto);
    		}
    	}
    }

    public String completeTask(){
        Map<String, Object> params = new HashMap<>();
        return this.completeOrderTask(params);
    }

}
