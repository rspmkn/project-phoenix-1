package com.apd.phoenix.customerservice.exceptions;

import java.io.IOException;
import java.util.Iterator;
import javax.enterprise.context.NonexistentConversationException;
import javax.faces.FacesException;
import javax.faces.context.ExceptionHandler;
import javax.faces.context.ExceptionHandlerWrapper;
import javax.faces.context.FacesContext;
import javax.faces.event.ExceptionQueuedEvent;
import javax.faces.event.ExceptionQueuedEventContext;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.LoggerFactory;

/**
 *
 * @author dnorris
 */
public class CustomExceptionHandler extends ExceptionHandlerWrapper {

    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(CustomExceptionHandler.class);
    private ExceptionHandler wrapped;

    CustomExceptionHandler(ExceptionHandler exception) {
        this.wrapped = exception;
    }

    @Override
    public ExceptionHandler getWrapped() {
        return wrapped;
    }

    @Override
    public void handle() throws FacesException {

        boolean hasNonexistentConversation = false;
        boolean hasInvalidatedSession = false;
        final Iterator<ExceptionQueuedEvent> i = getUnhandledExceptionQueuedEvents().iterator();
        while (i.hasNext()) {
            ExceptionQueuedEvent event = i.next();
            ExceptionQueuedEventContext context = (ExceptionQueuedEventContext) event.getSource();

            // get the exception from context
            Throwable t = context.getException();

            if (!(t instanceof NonexistentConversationException)) {
                logger.error("Critical Exception!", t);
                redirect("/customerservice/login/error.xhtml", true);
                hasInvalidatedSession = true;
            }
            else {
                hasNonexistentConversation = true;
            }

            //remove it from queue
            i.remove();
        }
        //if only a NonexistenctConversationException occurred, redirects to main page
        if (hasNonexistentConversation && !hasInvalidatedSession) {
            logger.warn("NonexistentConversationException occurred");
            redirect("/customerservice/admin/orderSearch/order-search.xhtml?errorRedirect=true", false);
        }
        //parent hanle
        getWrapped().handle();
    }

    private void redirect(String destination, boolean invalidate) {
        try {
            final FacesContext fc = FacesContext.getCurrentInstance();
            fc.getExternalContext().redirect(destination);
            if (invalidate) {
                fc.getExternalContext().invalidateSession();
            }
        }
        catch (IOException ex) {
            logger.error("IOException", ex);
        }
    }
}