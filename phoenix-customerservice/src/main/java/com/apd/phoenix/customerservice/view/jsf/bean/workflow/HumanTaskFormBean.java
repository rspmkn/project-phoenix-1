/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.customerservice.view.jsf.bean.workflow;

import java.text.ParseException;
import java.util.Date;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import org.apache.commons.lang.time.DateUtils;
import org.kie.api.task.model.Task;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.brms.core.workflow.UserTaskService;
import com.apd.phoenix.service.business.SystemUserBp;
import com.apd.phoenix.service.executor.command.api.CompleteTaskCommand;
import com.apd.phoenix.service.workflow.WorkflowService;

public abstract class HumanTaskFormBean {

    private static final Logger LOGGER = LoggerFactory.getLogger(HumanTaskFormBean.class);

    protected static final String TO_TASK_SCREEN = "/admin/customerService/customer-service?faces-redirect=true";

    //stores the date of the JBPM 6 migration, since some tasks created before the migration can't be completed - 
    //instead they are just exited. See CreateManualOrderBean, ResolveBackorderedItemsBean, and ResolveItemRejectionBean
    private static final String[] MIGRATION_DATE_FORMAT = new String[] { "yyyy-MM-dd" };
    private static final String MIGRATION_DATE = "2016-04-02";

    @Inject
    protected UserTaskService userTaskService;

    @Inject
    WorkflowTaskBean workflowTaskBean;

    @Inject
    SystemUserBp systemUserBp;

    @Inject
    private WorkflowService workflowService;

    protected long taskId;

    protected Map<String, Object> taskContent;

    private boolean preMigrationTask = false;

    @PostConstruct
    public void postConstruct() {
        taskId = workflowTaskBean.getTaskToWork();
        taskContent = userTaskService.getTaskContent(taskId);
        Task task = userTaskService.getTask(taskId);
        if (task != null && task.getTaskData() != null && task.getTaskData().getCreatedOn() != null) {
            try {
                Date migrationDate = DateUtils.parseDate(MIGRATION_DATE, MIGRATION_DATE_FORMAT);
                preMigrationTask = task.getTaskData().getCreatedOn().before(migrationDate);
                if (LOGGER.isDebugEnabled()) {
                    LOGGER.debug("Setting preMigrationTask = " + this.preMigrationTask);
                }
            }
            catch (ParseException e) {
                LOGGER.error("Exception when parsing JBPM migration date, assuming post-migration");
            }
        }
        this.initDomainInfo();
    }

    public abstract void initDomainInfo();

    protected String completeTask(Map<String, Object> params, long domainId, CompleteTaskCommand.Type domainIdType) {
        String user = this.getUser();
        Task task = userTaskService.getTask(taskId);
        if (!task.getTaskData().getActualOwner().getId().equals(user)) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("You are not the owner of this task!"));
            return "";
        }
        workflowTaskBean.getTasksStarted().put(taskId, new Date());
        workflowService.completeTask(taskId, domainId, domainIdType, user, params);
        return TO_TASK_SCREEN;
    }

    public String getUser() {
        ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
        return context.getUserPrincipal().getName();
    }

    public long getTaskId() {
        return this.taskId;
    }

    protected boolean isPreMigrationTask() {
        return preMigrationTask;
    }
}
