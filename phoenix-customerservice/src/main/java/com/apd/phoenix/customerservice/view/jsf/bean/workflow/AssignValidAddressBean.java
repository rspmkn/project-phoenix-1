/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.apd.phoenix.customerservice.view.jsf.bean.workflow;

import com.apd.phoenix.customerservice.view.jsf.bean.workflow.AssignValidAddressBean;
import com.apd.phoenix.customerservice.view.jsf.bean.workflow.OrderHumanTaskFormBean;
import com.apd.phoenix.service.business.AccountBp;
import com.apd.phoenix.service.business.AddressBp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.ejb.Stateful;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.inject.Inject;
import javax.inject.Named;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.model.Address;
import com.apd.phoenix.service.model.MiscShipTo;
import javax.enterprise.context.RequestScoped;

@Named
@Stateful
@RequestScoped
public class AssignValidAddressBean extends OrderHumanTaskFormBean {

    private static final Logger logger = LoggerFactory.getLogger(AssignValidAddressBean.class);

    @Inject
    private AccountBp accountBp;

    @Inject
    private AddressBp addressBp;

    private Address selectedAddress;

    private Address proposedAddress;

    private String line1;

    private String city;

    private String zip;

    private String state;

    private String desktop;

    private List<Address> addressList;

    private String usscoAccountNumber;

    private Boolean useDefaultUsscoAccountNumber = Boolean.FALSE;

    private String defaultUsscoAccountNumber;

    @Override
    public void init() {

        defaultUsscoAccountNumber = null;

        this.proposedAddress = customerOrder.getAddress();
        if (line1 == null) {
            line1 = this.proposedAddress.getLine1();
        }
        if (city == null) {
            city = this.proposedAddress.getCity();
        }
        if (zip == null) {
            zip = this.proposedAddress.getZip();
        }
        if (state == null) {
            state = this.proposedAddress.getState();
        }
        if (desktop == null && this.proposedAddress.getMiscShipTo() != null) {
            desktop = this.proposedAddress.getMiscShipTo().getDesktop();
        }
        else if (desktop == null) {
            desktop = "";
        }
        this.addressList = accountBp.getAddressesByZip(customerOrder.getAccount(), proposedAddress.getZip());
    }

    public void retrieve() {

    }

    public String requestNewShipTo() {
        boolean usePending = false;
        boolean useExisting = false;
        this.proposedAddress = addressBp.findById(proposedAddress.getId(), Address.class);
        updateAddressFields();

        if (neitherUsNumberSelected()) {
            FacesContext.getCurrentInstance().addMessage(
                    null,
                    new FacesMessage("Please designate a ussco Account Id, "
                            + "OR check the box inicating that the default will be used"));
            return "";
        }
        if (bothUsNumbersSelected()) {
            FacesContext.getCurrentInstance().addMessage(
                    null,
                    new FacesMessage("Please designate a ussco Account Id, "
                            + "OR check the box inicating that the default will be used, but not BOTH"));
            return "";
        }

        return this.completeTask(usePending, useExisting);
    }

    private boolean bothUsNumbersSelected() {
        return !StringUtils.isEmpty(usscoAccountNumber) && useDefaultUsscoAccountNumber;
    }

    private boolean neitherUsNumberSelected() {
        return StringUtils.isEmpty(usscoAccountNumber)
                && (useDefaultUsscoAccountNumber == null || !useDefaultUsscoAccountNumber);
    }

    public String useSelectedAddress() {
        if (bothUsNumbersSelected()) {
            FacesContext.getCurrentInstance().addMessage(
                    null,
                    new FacesMessage("Please uncheck use default ussco Account Id to use the entered Ussco Id, "
                            + "OR remove the Ussco Account Id and keep use default checked."));
            return "";
        }
        if (this.selectedAddress.getMiscShipTo() == null) {
            this.selectedAddress.setMiscShipTo(new MiscShipTo());
        }
        String usAccount = "";
        if (useDefaultUsscoAccountNumber) {
            usAccount = null;
        }
        if (!StringUtils.isEmpty(usscoAccountNumber)) {
            usAccount = usscoAccountNumber;
        }
        if (usAccount != "") {
            selectedAddress.getMiscShipTo().setUsAccount(usAccount);
            selectedAddress = addressBp.update(selectedAddress);
        }

        String shipToId = this.selectedAddress.getShipToId();

        this.proposedAddress.setShipToId(shipToId);
        if (this.proposedAddress.getMiscShipTo() == null) {
            this.proposedAddress.setMiscShipTo(new MiscShipTo());
        }
        this.proposedAddress.getMiscShipTo().setAddressId(shipToId);
        this.proposedAddress.setPendingShipToAuthorization(this.selectedAddress.getPendingShipToAuthorization());
        this.proposedAddress.setLine1(this.selectedAddress.getLine1());
        this.proposedAddress.setCity(this.selectedAddress.getCity());
        this.proposedAddress.setState(this.selectedAddress.getState());
        this.proposedAddress.setZip(this.selectedAddress.getZip());
        this.proposedAddress.setCompany(this.selectedAddress.getCompany());
        this.proposedAddress.setCountry(this.selectedAddress.getCountry());
        this.proposedAddress.getMiscShipTo().setUsAccount(this.selectedAddress.getMiscShipTo().getUsAccount());

        //Note : only desktop input text will be updated
        this.proposedAddress.getMiscShipTo().setDesktop(desktop);
        customerOrder.setAddress(this.proposedAddress);
        customerOrder = customerOrderBp.update(customerOrder);

        if (this.proposedAddress.getPendingShipToAuthorization() == null) {
            return requestNewShipTo();
        }
        boolean usePending = this.proposedAddress.getPendingShipToAuthorization();
        boolean useExisting = true;
        return this.completeTask(usePending, useExisting);
    }

    private String completeTask(boolean usePending, boolean useExisting) {
        Map<String, Object> params = new HashMap<>();
        params.put("usePending", usePending);
        params.put("useExisting", useExisting);
        params.put("usscoAccountNumber", this.proposedAddress.getMiscShipTo().getUsAccount());
        return this.completeOrderTask(params);
    }

    public String getLabel(Address temp) {
        String toReturn = "";
        String delim = ",";
        if (temp.getPendingShipToAuthorization() != null && temp.getPendingShipToAuthorization()) {
            toReturn = "(Pending) ";
        }
        toReturn += temp.getLine1() + delim + temp.getCity() + delim + temp.getState() + delim + temp.getZip();
        String usAccount = "";
        if (temp.getMiscShipTo() != null && !StringUtils.isEmpty(temp.getMiscShipTo().getUsAccount())) {
            usAccount = temp.getMiscShipTo().getUsAccount();
        }
        else {
            if (defaultUsscoAccountNumber == null) {
                defaultUsscoAccountNumber = customerOrderBp.getDefaultUsscoAccountNumberForOrder(customerOrder);
            }
            usAccount = defaultUsscoAccountNumber;
        }
        toReturn = toReturn + delim + "Ussco Acccount Number: " + usAccount;
        return toReturn;
    }

    private Address updateAddressFields() {
        this.proposedAddress.setLine1(line1);
        this.proposedAddress.setCity(city);
        this.proposedAddress.setState(state);
        this.proposedAddress.setZip(zip);
        ;
        if (this.proposedAddress.getMiscShipTo() == null) {
            this.proposedAddress.setMiscShipTo(new MiscShipTo());
        }
        this.proposedAddress.getMiscShipTo().setDesktop(desktop);
        if (useDefaultUsscoAccountNumber) {
            this.proposedAddress.getMiscShipTo().setUsAccount(customerOrderBp.getUsAccountNumber(customerOrder));
        }
        else {
            this.proposedAddress.getMiscShipTo().setUsAccount(this.usscoAccountNumber);
        }
        return addressBp.update(proposedAddress);
    }

    public Address getSelectedAddress() {
        return selectedAddress;
    }

    public void setSelectedAddress(Address selectedAddress) {
        this.selectedAddress = selectedAddress;
    }

    //    public Address getProposedAddress() {
    //        return proposedAddress;
    //    }

    //    public void setProposedAddress(Address proposedAddress) {
    //        this.proposedAddress = proposedAddress;
    //    }

    public List<Address> getAddressList() {
        return addressList;
    }

    public void setAddressList(List<Address> addressList) {
        this.addressList = addressList;
    }

    public Converter getAddressConverter() {

        return new Converter() {

            @Override
            public Object getAsObject(FacesContext context, UIComponent component, String value) {

                if (StringUtils.isBlank(value)) {
                    return null;
                }
                return addressBp.findById(Long.valueOf(value), Address.class);
            }

            @Override
            public String getAsString(FacesContext context, UIComponent component, Object value) {

                if (value == null) {
                    return "";
                }

                return String.valueOf(((Address) value).getId());
            }
        };
    }

    public String getLine1() {
        return line1;
    }

    public void setLine1(String line1) {
        this.line1 = line1;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getDesktop() {
        return desktop;
    }

    public void setDesktop(String desktop) {
        this.desktop = desktop;
    }

    public String getUsscoAccountNumber() {
        return usscoAccountNumber;
    }

    public void setUsscoAccountNumber(String usscoAccountNumber) {
        this.usscoAccountNumber = usscoAccountNumber;
    }

    public Boolean getUseDefaultUsscoAccountNumber() {
        return useDefaultUsscoAccountNumber;
    }

    public void setUseDefaultUsscoAccountNumber(Boolean useDefaultUsscoAccountNumber) {
        this.useDefaultUsscoAccountNumber = useDefaultUsscoAccountNumber;
    }

}
