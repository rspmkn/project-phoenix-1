package com.apd.phoenix.customerservice.view.jsf.bean.workflow;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.ejb.Stateful;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import com.apd.phoenix.service.business.LineItemBp;
import com.apd.phoenix.service.model.LineItem;
import com.apd.phoenix.service.model.LineItemStatus.LineItemStatusEnum;

@Named
@Stateful
@RequestScoped
public class ResolveBackorderedItemsBean extends OrderHumanTaskFormBean {

    @Inject
    LineItemBp lineItemBp;

    List<LineItem> lineItems;

    @Override
    public void init() {
        lineItems = new ArrayList<LineItem>();
        for (LineItem lineItem : customerOrder.getItems()) {
            if (lineItem.getStatus().getValue().equals(LineItemStatusEnum.BACKORDERED.getValue())
                    || lineItem.getStatus().getValue().equals(LineItemStatusEnum.PARTIAL_BACKORDERED.getValue())) {
                lineItems.add(lineItem);
            }
        }
    }

    public String resolved() {
        if (this.isPreMigrationTask()) {
            this.userTaskService.exitTask(this.getTaskId());
            return TO_TASK_SCREEN;
        }
        else {
            return completeTask();
        }
    }

    public boolean canEditOrder() {
        return customerOrderBp.canEditOrder(this.customerOrder);
    }

    private String completeTask(){
        Map<String, Object> params = new HashMap<>();
        return this.completeOrderTask(params);
    }

    public List<LineItem> getLineItems() {
        return lineItems;
    }

    public void setLineItems(List<LineItem> lineItems) {
        this.lineItems = lineItems;
    }

}
