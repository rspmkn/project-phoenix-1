/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.customerservice.view.jsf.bean.report;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateful;
import javax.enterprise.context.ConversationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import com.apd.phoenix.service.model.dto.DailyChargeReportDto;
import com.apd.phoenix.service.payment.ws.client.TransactionType;
import com.apd.phoenix.service.report.ReportService;
import com.apd.phoenix.service.utility.FileDownloader;

@Named
@Stateful
@ConversationScoped
public class DailyChargeReportBean extends AbstractJasperReportBean implements Serializable {

    private static final long serialVersionUID = 1L;

    @Inject
    private ReportService reportService;

    private List<String> accounts;
    private Date startDate;
    private Date endDate;
    private TransactionGroup currentTransGroup;
    private String poNum;
    private boolean csvDownload;

    List<DailyChargeReportDto> reportRows = new ArrayList<DailyChargeReportDto>();

    public String generateReport() {

        return FileDownloader.downloadFile(reportService.generateCustomerServiceDailyChargeReport(accounts,
                currentTransGroup.getTransactionTypes(), poNum, startDate, endDate));
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public TransactionGroup getCurrentTransGroup() {
        return currentTransGroup;
    }

    public void setCurrentTransGroup(TransactionGroup currentTransGroup) {
        this.currentTransGroup = currentTransGroup;
    }

    public String getPoNum() {
        return poNum;
    }

    public void setPoNum(String poNum) {
        this.poNum = poNum;
    }

    public boolean isCsvDownload() {
        return csvDownload;
    }

    public void setCsvDownload(boolean csvDownload) {
        this.csvDownload = csvDownload;
    }

    public List<DailyChargeReportDto> getReportRows() {
        return reportRows;
    }

    public void setReportRows(List<DailyChargeReportDto> reportRows) {
        this.reportRows = reportRows;
    }

    public List<String> getAccounts() {
        return accounts;
    }

    public void setAccounts(List<String> accounts) {
        this.accounts = accounts;
    }

    public TransactionGroup[] getTransactionGroups() {
        return TransactionGroup.values();
    }

    private static final TransactionType[] ALL_TRANS = { TransactionType.AUTHORIZE_CAPTURE, TransactionType.CAPTURE,
            TransactionType.CREDIT };
    private static final TransactionType[] CHARGE_TRANS = { TransactionType.AUTHORIZE_CAPTURE, TransactionType.CAPTURE };
    private static final TransactionType[] CREDIT_TRANS = { TransactionType.CREDIT };

    public enum TransactionGroup {
        ALL(ALL_TRANS), CHARGE(CHARGE_TRANS), CREDIT(CREDIT_TRANS);

        private List<String> transactionTypes;

        private TransactionGroup(TransactionType[] types) {
			this.transactionTypes = new ArrayList<>();
			for (TransactionType type : types) {
				this.transactionTypes.add(type.getTransactionValue());
			}
		}

        public List<String> getTransactionTypes() {
            return this.transactionTypes;
        }
    }
}
