package com.apd.phoenix.jreport;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Map;

import net.sf.jasperreports.engine.JRParameter;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.jaspersoft.jasperserver.api.common.domain.ExecutionContext;
import com.jaspersoft.jasperserver.api.common.service.BeanForInterfaceImplementationFactory;
import com.jaspersoft.jasperserver.api.engine.jasperreports.service.impl.JdbcDataSourceService;
import com.jaspersoft.jasperserver.api.engine.jasperreports.service.impl.JdbcReportDataSourceServiceFactory;
import com.jaspersoft.jasperserver.api.metadata.common.service.RepositoryService;
import com.jaspersoft.jasperserver.api.metadata.jasperreports.domain.ReportDataSource;
import com.jaspersoft.jasperserver.api.metadata.jasperreports.service.ReportDataSourceService;
import com.jaspersoft.jasperserver.war.common.JasperServerUtil;

public class CustomDataSourceService implements ReportDataSourceService {
	private static final Log log = LogFactory.getLog(CustomDataSourceService.class);
	
	private JdbcDataSourceService connection;
	private Connection con;
	private RepositoryService repositoryService;
	private BeanForInterfaceImplementationFactory dataSourceServiceFactory;
	
	public CustomDataSourceService(RepositoryService repositoryService, BeanForInterfaceImplementationFactory dsServiceFactory) {
	  super();
	  this.repositoryService = repositoryService;
	  this.dataSourceServiceFactory = dsServiceFactory;
	}
	 
	 
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void setReportParameterValues(Map parametersValue) {
		log.info("Inside setReportParameterValues method");
		String tenantName = (String) parametersValue.get("tenantName");
		Statement stmt = null;
		String query = null;
		if( (tenantName == null) || (tenantName.isEmpty()) ) {
			log.info("tenantName is empty");
			tenantName = "PHOENIX";
		} else {
			log.info("tenantName is not empty="+tenantName);
		}
	 
		String ds_uri = "/properties/MultiTenantDataSource";
	 
		connection = getRepositoryDatasource(ds_uri);
	 
	  try {
		  
		  if(connection != null) { 
				
				con =  connection.getDataSource().getConnection();
				con.setCatalog(tenantName);
				log.info("Inside setSchema= "+query);
				parametersValue.put(JRParameter.REPORT_CONNECTION, con);
		  }
		  
		  
	  } catch (SQLException e) {
	    log.error("Error while " + query , e);
	  } finally { 
			try {
				if (stmt != null) { stmt.close(); }
			} catch(SQLException exp) {
				log.error("Error in closing statement", exp);
			}
		}
	}
	 
	 
	public JdbcDataSourceService getRepositoryDatasource(String repositoryURI) {
		log.info("Inside getRepositoryDatasource method");
		
		log.info("Before Getting ExecutionContext");
		ExecutionContext context = JasperServerUtil.getExecutionContext();
		
		ReportDataSource datasource = (ReportDataSource) repositoryService
				.getResource(context, repositoryURI);
	 
		
		JdbcReportDataSourceServiceFactory factory = (JdbcReportDataSourceServiceFactory) dataSourceServiceFactory
				.getBean(datasource.getClass());
		
		JdbcDataSourceService DSservice = (JdbcDataSourceService) factory
				.createService(datasource);
		
		log.info("JdbcDataSourceService Created");
	 
		return DSservice;
	}
	 
	@Override
	public void closeConnection() {
	  if(connection != null) {
	   connection.closeConnection();
	  }
	  try {
		  if(con != null && !con.isClosed()) {
			  con.close();
		  }
	  } catch(SQLException exp) {
		log.error("Error in closing connection", exp);
	  }
	}
	
}