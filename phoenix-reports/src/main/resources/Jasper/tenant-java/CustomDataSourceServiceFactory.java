package com.apd.phoenix.jreport;

import com.jaspersoft.jasperserver.api.metadata.common.service.RepositoryService;
import com.jaspersoft.jasperserver.api.metadata.jasperreports.domain.ReportDataSource;
import com.jaspersoft.jasperserver.api.metadata.jasperreports.service.ReportDataSourceService;
import com.jaspersoft.jasperserver.api.metadata.jasperreports.service.ReportDataSourceServiceFactory;
import com.jaspersoft.jasperserver.api.common.service.BeanForInterfaceImplementationFactory;

public class CustomDataSourceServiceFactory implements
		ReportDataSourceServiceFactory {
	private RepositoryService repositoryService;
	private BeanForInterfaceImplementationFactory dataSourceServiceFactory;

	public CustomDataSourceServiceFactory() {
	}

	/* Factory methods */
	public CustomDataSourceService createDataSourceService() {
		return new CustomDataSourceService(repositoryService,
				dataSourceServiceFactory);
	}

	@Override
	public ReportDataSourceService createService(ReportDataSource dataSource) {
		return new CustomDataSourceService(repositoryService,
				dataSourceServiceFactory);
	}

	/* getters */
	public RepositoryService getRepositoryService() {
		return repositoryService;
	}

	public BeanForInterfaceImplementationFactory getDataSourceServiceFactory() {
		return dataSourceServiceFactory;
	}

	/* Setters */
	public void setRepositoryService(RepositoryService repositoryService) {
		this.repositoryService = repositoryService;
	}

	public void setDataSourceServiceFactory(
			BeanForInterfaceImplementationFactory dataSourceServiceFactory) {
		this.dataSourceServiceFactory = dataSourceServiceFactory;
	}
}