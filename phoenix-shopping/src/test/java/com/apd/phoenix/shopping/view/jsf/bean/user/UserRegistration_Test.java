package com.apd.phoenix.shopping.view.jsf.bean.user;

import java.util.ArrayList;
import java.util.List;
import org.junit.Test;
import com.apd.phoenix.service.business.SystemUserBp;
import com.apd.phoenix.service.model.SystemUser;

public class UserRegistration_Test {

    List<String> registrationGroup = new ArrayList<String>();
    List<String> notRegistrationGroup = new ArrayList<String>();

    UserRegistrationBean userRegistrationBean;
    SystemUserBp systemUserBp = new SystemUserBp();

    @Test
    public void userRegistrationBeanCall() {
        userRegistrationBean = new UserRegistrationBean();
        userRegistrationBean.beginUserRegistration();
    }

    @Test
    public void userRegistrationBpCall() {
        systemUserBp = new SystemUserBp();
        SystemUser passedUser = new SystemUser();
        passedUser.setLogin("New User");
        systemUserBp.beginUserRegistration(passedUser);
    }

}