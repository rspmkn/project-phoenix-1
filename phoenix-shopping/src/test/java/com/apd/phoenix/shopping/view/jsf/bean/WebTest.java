package com.apd.phoenix.shopping.view.jsf.bean;

import java.net.URL;
import org.jboss.arquillian.drone.api.annotation.Drone;
import org.jboss.arquillian.test.api.ArquillianResource;
import org.jboss.shrinkwrap.api.Filters;
import org.jboss.shrinkwrap.api.GenericArchive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.asset.StringAsset;
import org.jboss.shrinkwrap.api.importer.ExplodedImporter;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.DependencyResolvers;
import org.jboss.shrinkwrap.resolver.api.maven.MavenDependencyResolver;
import org.openqa.selenium.WebDriver;
import com.apd.phoenix.service.business.AbstractBp;
import com.apd.phoenix.service.business.Bp;
import com.apd.phoenix.service.persistence.jpa.AbstractDao;
import com.apd.phoenix.service.persistence.jpa.Dao;
import com.apd.phoenix.service.security.PhoenixLoginModule;
import com.apd.phoenix.shopping.view.jsf.bean.utils.ViewUtils;
import com.apd.phoenix.core.BCrypt;
import com.apd.phoenix.core.EncryptionUtils;

public abstract class WebTest {

    protected static final String WEBAPP_SRC = "src/main/webapp";

    @Drone
    protected WebDriver driver;

    @ArquillianResource
    protected URL deploymentUrl;

    public static WebArchive createDeployment() {
        MavenDependencyResolver resolver = DependencyResolvers.use(MavenDependencyResolver.class).loadMetadataFromPom(
                "pom.xml");

        WebArchive wa = ShrinkWrap.create(WebArchive.class, "test.war").addClasses(AbstractBp.class, Bp.class,
                AbstractDao.class, Dao.class, PhoenixLoginModule.class, EncryptionUtils.class, BCrypt.class).addClass(
                ViewUtils.class).addPackage("com.apd.phoenix.service.model").addAsLibraries(
                resolver.artifacts("commons-lang:commons-lang", "org.richfaces.ui:richfaces-components-ui",
                        "org.richfaces.core:richfaces-core-impl").resolveAsFiles()).merge(
                ShrinkWrap.create(GenericArchive.class).as(ExplodedImporter.class).importDirectory(WEBAPP_SRC).as(
                        GenericArchive.class), "/", Filters.includeAll()).addAsResource("test-persistence.xml",
                "META-INF/persistence.xml").addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml").addAsWebInfResource(
                new StringAsset("<faces-config version=\"2.0\"/>"), "faces-config.xml");

        return wa;
    }
}
