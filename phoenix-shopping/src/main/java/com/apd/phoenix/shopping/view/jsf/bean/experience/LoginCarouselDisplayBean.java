package com.apd.phoenix.shopping.view.jsf.bean.experience;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Stateful;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.apd.phoenix.service.business.IpSubdomainRestriction;
import com.apd.phoenix.service.model.LoginCarouselDisplay;
import com.apd.phoenix.service.persistence.jpa.LoginCarouselDisplayDao;

/**
 * This bean fetches the login carousel information. Note that the methods on this bean are called on
 * the login page, so they should be performant
 * 
 * @author RHC
 *
 */
@Named
@Stateful
@RequestScoped
@Lock(LockType.READ)
public class LoginCarouselDisplayBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private LoginCarouselDisplayDao dao;

	private List<LoginCarouselDisplay> carousels = new ArrayList<>();
	
	@PostConstruct
	public void init() {
		//TODO: cache carousel values, per PHOEN-5527
        HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext()
                .getRequest();
		LoginCarouselDisplay searchDisplay = new LoginCarouselDisplay();
		searchDisplay.setSubdomain(IpSubdomainRestriction.ipAndDomainInfo(request)[1]);
		List<LoginCarouselDisplay> fetchedCarousels = dao.searchByExactExample(searchDisplay, 0, 0);
		this.carousels = new ArrayList<LoginCarouselDisplay>();
		for (LoginCarouselDisplay carousel : fetchedCarousels) {
			if (StringUtils.isNotBlank(carousel.getBackgroundImageUrl())) {
				this.carousels.add(carousel);
			}
		}
	}

	public List<LoginCarouselDisplay> getCarousels() {
		return carousels;
	}

	public void setCarousels(List<LoginCarouselDisplay> carousels) {
		this.carousels = carousels;
	}
	
	public int getCarouselQuantity() {
		return carousels != null ? carousels.size() : 0;
	}
}
