package com.apd.phoenix.shopping.cxml;

import com.apd.phoenix.service.integration.cxml.model.cxml.CXML;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.StringWriter;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.InputStreamRequestEntity;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.RequestEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import static com.apd.phoenix.service.cxml.CxmlConstants.*;

/**
 *
 * @author dnorris
 */
public class CxmlHttpClientUtils {

    private static final Logger logger = LoggerFactory.getLogger(CxmlHttpClientUtils.class);
    private static final String CXML_HEADER = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
            + "<!DOCTYPE cXML SYSTEM \"http://xml.cxml.org/schemas/cXML/1.2.014/cXML.dtd\">";

    public static InputStream postCxmlContent(String destinationUrl, CXML cxml) throws CxmlHttpClientUtilsException {
        PostMethod post = null;
        InputStream responseContent = null;
        try (StringWriter sw = new StringWriter()) {
            JAXBContext jaxbContext = JAXBContext.newInstance(CXML.class);
            Marshaller marshaller = jaxbContext.createMarshaller();
            marshaller.setProperty("com.sun.xml.bind.xmlDeclaration", Boolean.FALSE);
            marshaller.setProperty("com.sun.xml.bind.xmlHeaders", CXML_HEADER);
            marshaller.marshal(cxml, sw);
            try (InputStream in = new ByteArrayInputStream(sw.toString().getBytes("UTF-8"))) {
                RequestEntity entity = new InputStreamRequestEntity(in, "text/xml; UTF-8");
                post = new PostMethod(destinationUrl);
                post.setRequestEntity(entity);
            }
            HttpClient httpclient = new HttpClient();
            int result = httpclient.executeMethod(post);
            if (result != CXML_STATUS_OK) {
                throw new Exception("Response from external site returned status code: " + result);
            }
            responseContent = post.getResponseBodyAsStream();
        } catch (Exception ex) {
           throw new CxmlHttpClientUtilsException(ex.getMessage(), ex);

        }
        return responseContent;
    }

    public static final class CxmlHttpClientUtilsException extends Exception {

        private static final long serialVersionUID = -3711705910840303497L;

        public CxmlHttpClientUtilsException() {
            super();
        }

        public CxmlHttpClientUtilsException(String message) {
            super(message);
        }

        public CxmlHttpClientUtilsException(String message, Throwable t) {
            super(message, t);
        }
    }
}
