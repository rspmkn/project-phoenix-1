package com.apd.phoenix.shopping.view.jsf.bean.utils;

import java.util.Properties;
import javax.inject.Inject;
import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;
import org.jboss.weld.context.ConversationContext;
import org.jboss.weld.context.http.Http;
import com.apd.phoenix.core.utility.PropertiesLoader;

/**
 *
 * @author anicholson
 */
@WebListener
public class SessionListener implements HttpSessionListener {

    @Inject
    @Http
    ConversationContext conversationContext;

    @Override
    public void sessionCreated(HttpSessionEvent hse) {
        Properties commonProperties = PropertiesLoader.getAsProperties("ecommerce.properties");
        long sessionTimeoutSeconds = hse.getSession().getMaxInactiveInterval();
        long conversationTimeoutMs = sessionTimeoutSeconds * 1000;
        conversationContext.setDefaultTimeout(conversationTimeoutMs);
        conversationContext.setConcurrentAccessTimeout(Long.parseLong(commonProperties.getProperty(
                "conversation.concurrentAccess.timeout", "5000")));
    }

    @Override
    public void sessionDestroyed(HttpSessionEvent hse) {
    }

}
