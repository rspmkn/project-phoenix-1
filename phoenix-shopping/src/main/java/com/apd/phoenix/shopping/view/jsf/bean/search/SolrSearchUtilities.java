/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.shopping.view.jsf.bean.search;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import org.apache.commons.lang.StringUtils;
import org.apache.solr.client.solrj.response.FacetField;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.model.Matchbook;
import com.apd.phoenix.service.product.Product;
import com.apd.phoenix.shopping.view.jsf.bean.MarketingImagesBean;
import com.apd.phoenix.shopping.view.jsf.bean.utils.AttributeFilterType;
import com.apd.phoenix.shopping.view.jsf.bean.utils.FilterKey;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;

/**
 * Utilities for supporting the Search UI
 */
@Stateless
@LocalBean
public class SolrSearchUtilities implements SolrSearcher {

    private static final String MODEL_NUMBER = "modelNumber";
    private static final Logger logger = LoggerFactory.getLogger(SolrSearchUtilities.class);

    /**
     * Parses Solr Query Response Object to build a map the available filters
     * The key in this map is the filter category and the value is the filter
     * name
     */
    public static Multimap<AttributeFilterType, FilterKey> parseFilters(QueryResponse response) {
        Multimap<AttributeFilterType, FilterKey> facetMap = HashMultimap.create();
        FacetField filterFields = response.getFacetField("filters");
        for (FacetField.Count c : filterFields.getValues()) {
            String[] data = c.getName().split("\\|");
            String filterCategoryName = data[0].trim();
            FacetField catFilterFields = response.getFacetField(getKeyName(filterCategoryName)); //get field corresponding to current category
            String filterName = null;
            if (catFilterFields != null) {
                for (FacetField.Count c2 : catFilterFields.getValues()) { //iterate through each filter field looking for filters in same category
                    String[] data2 = c2.getName().split("\\|");
                    if (filterCategoryName.equals(data2[0].trim())) { //prefix checking for filters in same category
                        filterName = data2[1].trim();
                        if (!"".equals(filterCategoryName) && !"null".equals(filterCategoryName) && filterName != null
                                && !"".equals(filterName) && !"null".equals(filterName)) {
                            String combinedNameCount = filterName + " (" + c2.getCount() + ")";
                            FilterKey filterKey = new FilterKey(combinedNameCount, combinedNameCount);
                            facetMap.put(new AttributeFilterType(filterCategoryName.trim(), null), filterKey);
                        }
                    }
                }
            }
            else {
                filterName = data[1].trim();
            }
            AttributeFilterType attributeFilterType = new AttributeFilterType(filterCategoryName, null);
            if (filterCategoryName != null && !"".equals(filterCategoryName) && !"null".equals(filterCategoryName)
                    && filterName != null && !"".equals(filterName) && !"null".equals(filterName)) {
                String combinedNameCount = filterName + " (" + c.getCount() + ")";
                boolean exists = false;
                if (facetMap.get(attributeFilterType) != null) {
                    for (FilterKey filterKey : facetMap.get(attributeFilterType)) {
                        if (combinedNameCount.contains(filterKey.getFilter())) {
                            exists = true;
                        }
                    }
                }
                if (!exists) { //above loop takes precedence in adding filters to facetMap
                    FilterKey filterKey = new FilterKey(combinedNameCount, combinedNameCount);
                    facetMap.put(attributeFilterType, filterKey);
                }
            }
        }
        FacetField filterPriorityFields = response.getFacetField("filtersPriority");
        Map<String, WeightedValue> priorityCounts = new HashMap<String, WeightedValue>();
        for (FacetField.Count c : filterPriorityFields.getValues()) {
            String[] data = c.getName().split("\\|");
            String filterCategoryName = data[0].trim();
            String filterPriorityValue = data[1].trim();
            try {
                BigDecimal priority = new BigDecimal(filterPriorityValue);
                WeightedValue priorityWeightedValue = priorityCounts.get(filterCategoryName);
                if (priorityWeightedValue == null) {
                    priorityWeightedValue = new WeightedValue();
                }
                BigDecimal additionalCount = new BigDecimal(c.getCount());
                priorityWeightedValue.value = priorityWeightedValue.value.add(priority.multiply(additionalCount));
                priorityWeightedValue.number = priorityWeightedValue.number.add(additionalCount);
                priorityCounts.put(filterCategoryName, priorityWeightedValue);
            }
            catch (NumberFormatException e) {
                logger.debug("Could not parse priority '" + filterPriorityValue + "' as number.");
            }
        }

        for (AttributeFilterType key : facetMap.keys()) {
            WeightedValue weightedValue = priorityCounts.get(key.getName());
            if (weightedValue != null) {
                key.setPriority(weightedValue.getAverage());
            }
        }
        return facetMap;
    }

    /**
     * Method for parsing the manufacturer facet data from the QueryResponse
     * response The key for the returned HashMap is the manufacturer name, and
     * the value is the count of results tied to that manufacturer
     */
    public static Map<FilterKey, Integer> parseManufacturerFacets(QueryResponse response) {
        HashMap<FilterKey, Integer> manufacturerWidgetData = new HashMap<>();
        FacetField manufacturerField = response.getFacetField("brandName");
        FilterKey filterKey = null;
        for (FacetField.Count c : manufacturerField.getValues()) {
        	filterKey = new FilterKey(c.getName(), c.getName());
            manufacturerWidgetData.put(filterKey,(int) (long) c.getCount());
        }
        return manufacturerWidgetData;
    }

    /**
     * Converts a list of SolrDocuments into a collection of Product objects for
     * use in the UI
     */
    public static List<Product> convertToProductList(SolrDocumentList solrQueryResults) {
        List<Product> products = new ArrayList<>();
        if(solrQueryResults == null){
        	return products;
        }
        for (SolrDocument doc : solrQueryResults) {
            Product product = new Product();
            product.setId(Long.parseLong((String) doc.getFieldValue(ID_FIELD)));
            product.setName((String) doc.getFieldValue(SOLR_PRODUCT_NAME_FIELD));
            product.setStatus((String) doc.getFieldValue(ITEM_STATUS));
            product.setCustomerSku((String) doc.getFieldValue(CUSTOMER_SKU_FIELD));
            if (StringUtils.isEmpty(product.getCustomerSku())) {
                product.setCustomerSku((String) doc.getFieldValue(APD_SKU_FIELD));
            }
            product.setManufacturerSku((String) doc.getFieldValue(MANUFACTURER_SKU_FIELD));
            product.setVendorSku((String) doc.getFieldValue(VENDOR_SKU_FIELD));
            product.setApdSku((String) doc.getFieldValue(APD_SKU_FIELD));
            product.setDescription((String) doc.getFieldValue(SOLR_PRODUCT_DESCTIPTION_FIELD));
            product.setManufacturerName((String) doc.getFieldValue(SOLR_PRODUCT_BRAND_NAME_FIELD));
            product.setBrandImage((String) doc.getFieldValue(SOLR_PRODUCT_BRAND_LOGO_FIELD));
            float priceField = (Float) doc.getFieldValue(SOLR_PRODUCT_PRICE_FIELD);
            product.setPrice(new BigDecimal("" + priceField));
            product.setVendorName((String) doc.getFieldValue(VENDOR_NAME_FIELD));
            Collection<Object> iconNameValues = doc.getFieldValues(ICON_MAP_FIELD);
            Map<String, String> iconMap = new HashMap<>();
            if (iconNameValues != null) {
                for (Object mapValue : iconNameValues) {
                    if (mapValue instanceof String) {
                        String urlMapValue = (String) mapValue;
                        String[] data = urlMapValue.split("\\|");
                        String url = data[0].trim();
                        String toolTipValue = data[1].trim();
                        if (!"null".equals(url)) {
                            iconMap.put(url, toolTipValue);
                        }
                        else {
                            logger.debug("item classification " + toolTipValue + " missing icon URL");
                        }
                    }
                }
            }
            product.setCoreItemStartDate((Date) doc.getFieldValue(CORE_ITEM_START_DATE_FIELD));
            product.setCoreItemExpirationDate((Date) doc.getFieldValue(CORE_ITEM_EXPIRATION_DATE_FIELD));
            if (product.isCoreItem()) {
                iconMap.put(SearchUtilities.coreItemIconUrl, "Core Item");
            }
            
            product.setIconMap(iconMap);
            Collection<Object> imageUrlValues = doc.getFieldValues(IMAGE_FIELD);
            
            List<String> imageUrls = new ArrayList<>();
            if (imageUrlValues != null) {
                for (Object imageUrl : imageUrlValues) {
                    if (imageUrl instanceof String) {
                        imageUrls.add((String) imageUrl);
                    }
                }
            }
            //If these are no images add a placeholder image
            if (imageUrls.isEmpty()) {
                imageUrls.add(MarketingImagesBean.getImageUrl("searchUtilities1"));
            }
            //For now set first image as main image
            //TODO choose this by the size of the image once that ETL process is determined
            for (String url : imageUrls) {
                product.setMainImageUrl(url);
                break;
            }
            product.setImageUrls(imageUrls);
            product.setUnits((String) doc.getFieldValue(UNIT_OF_MEASURE_NAME));

            try {
	            product.setMultiple(doc.getFieldValue(ITEM_MULTIPLE) != null ? 
	            		Integer.parseInt(doc.getFieldValue(ITEM_MULTIPLE).toString()) : null);
	            product.setMinimum(doc.getFieldValue(ITEM_MINIMUM) != null ?
	            		Integer.parseInt(doc.getFieldValue(ITEM_MINIMUM).toString()) : null);
            } catch (NumberFormatException e) {
            	logger.warn("Unable to parse minimum or multiple from Solr document");
            }
            
            Collection<Object> propertyNameValues = doc.getFieldValues(ITEM_PROPERTY_VALUES_FIELD);
            Map<String, String> properties = new HashMap<>();
            if (propertyNameValues != null) {
                for (Object mapValue : propertyNameValues) {
                    if (mapValue instanceof String) {
                        String urlMapValue = (String) mapValue;
                        String[] data = urlMapValue.split("\\|");
                        String name = data[0].trim();
                        String value = data[1].trim();
                        properties.put(name, value);
                    }
                }
            }
            product.setProperties(properties);
            
            product.setCustomerReplacementSku(doc.getFieldValue(CUSTOMER_REPLACEMENT_SKU) != null ? (String) doc.getFieldValue(CUSTOMER_REPLACEMENT_SKU) : null);
            product.setCustomerReplacementVendor(doc.getFieldValue(CUSTOMER_REPLACEMENT_VENDOR) != null ? (String) doc.getFieldValue(CUSTOMER_REPLACEMENT_VENDOR) : null);
            product.setSpecialOrder(doc.getFieldValue(SPECIAL_ORDER) != null ? (String)doc.getFieldValue(SPECIAL_ORDER) : null);
            product.setCustomOrder(doc.getFieldValue(CUSTOM_ORDER) != null ? (String) doc.getFieldValue(CUSTOM_ORDER) : null);

            Collection<Object> sellingPointValues = doc.getFieldValues(SELLING_POINTS);
            List<String> sellingPoints = new ArrayList<>();
            if(sellingPointValues != null) {
            	for(Object sellingpoint : sellingPointValues) {
            		  if (sellingpoint instanceof String) {
            			  sellingPoints.add((String)sellingpoint);
            		  }
            	}
            	
            }
            product.setSellingPoints(sellingPoints);
            
            products.add(product);
        }
        return products;
    }

    /**
     * Replaces characters which will impact regex search with .
     *
     * @return regex safe search string
     */
    public static String removeRegexCharacters(String input) {
        if (StringUtils.isNotBlank(input)) {
            String output = input.replace("&", ".");
            output = output.replace('/', '.');
            return output;
        }
        return input;
    }

    public static String getTagName(String filterCategory) {
        if (filterCategory != null) {
            return SolrSearchUtilities.makeSolrSafe(filterCategory.replace(" ", "")) + "Filter";
        }
        else {
            return "";
        }
    }

    public static String getKeyName(String filterCategory) {
        if (filterCategory != null) {
            return SolrSearchUtilities.makeSolrSafe(filterCategory.replace(" ", "")) + "Field";
        }
        else {
            return "";
        }
    }

    /**
     * Makes a string safe for inputting to solr for a search. Used on the literal strings passed into the main search box
     * @param input
     * @return solr safe search string
     */
    public static String makeSolrSafeAlphaNumeric(String input) {
        //calls makeSolrSafe, then strips out any (space-delimited) words that consist only of
        //special characters
        return (" " + makeSolrSafe(input) + " ").replaceAll(" [^A-Za-z0-9]* ", " ").trim();
    }

    /**
     * Makes a string safe for inputting to solr for a search. Used on the literal strings passed as search input, which could have legal special characters
     * @param input
     * @return solr safe search string
     */
    public static String makeSolrSafe(String input) {
        return escapeSolrSearchInput(replaceSolrSyntaxWords(input));
    }

    /**
     * Escapes solr syntax characters that throw errors when present in a search string
     * @param input
     * @return escaped search string
     */
    private static String escapeSolrSearchInput(String input) {
        int numberOfQuotes = StringUtils.countMatches(input, "\"");
        if ((numberOfQuotes % 2) == 1) {
            /**If there are an even number of qutes, then the quotes are being used to indicate something eg 
             * '"copy paper" "standard ruled"' otherwise escape the quotation mark
             */
            input = input.replaceAll("\"", "\\\"");
        }
        return input.replace("||", "\\||").replace("&&", "\\&&").replace("+", "\\+").replace("-", "\\-"); //all solr syntax words that throw errors when present in search at time of writing
    }

    /**
     * Makes boolean connective solr syntax terms lowercase so that they are ignored by solr
     * @param input
     * @return string with AND, OR, and NOT replaced with and, or, and not
     */
    private static String replaceSolrSyntaxWords(String input) {
        return input.replace("AND", "and").replace("OR", "or").replace("NOT", "not"); //AND, OR, and NOT are solr syntax, lowercase are not
    }

    public static Map<FilterKey, BigInteger> parseGreenFilter(
			QueryResponse response) {
		HashMap<FilterKey, BigInteger> greenWidgetData = new HashMap<>();
        FacetField greenFacetField = response.getFacetField("green");
        FilterKey filterKey = null;
        for (FacetField.Count c : greenFacetField.getValues()) {
        	filterKey = new FilterKey(c.getName(), c.getName());
            greenWidgetData.put(filterKey, BigInteger.valueOf((long) c.getCount()));
        }
        return greenWidgetData;
	}
}
