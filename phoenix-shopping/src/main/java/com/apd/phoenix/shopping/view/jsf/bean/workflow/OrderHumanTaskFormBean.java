/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.shopping.view.jsf.bean.workflow;

import com.apd.phoenix.service.business.CustomerOrderBp;
import com.apd.phoenix.service.business.OrderProcessLookupBp;
import com.apd.phoenix.service.executor.command.api.CompleteTaskCommand;
import com.apd.phoenix.service.model.CustomerOrder;
import com.apd.phoenix.service.model.PoNumber;
import java.util.Map;
import javax.inject.Inject;

public abstract class OrderHumanTaskFormBean extends HumanTaskFormBean {

    protected CustomerOrder customerOrder;

    private static final String DEFAULT_PO_VALUE = "####";

    private Long id;

    @Inject
    protected CustomerOrderBp customerOrderBp;

    @Inject
    private OrderProcessLookupBp processLookupBp;

    @Override
    public void initDomainInfo() {
        id = ((Double) taskContent.get("orderId")).longValue();
        //the variable name "orderId" is a misnomer, a legacy from when the process ID was set to the order ID
        CustomerOrder lazy = processLookupBp.getOrderFromProcessId(id);

        //TODO: Leverage the DTO to populate the customer order object as needed for order task forms
        customerOrder = customerOrderBp.hydrateForOrderDetails(lazy);

        this.init();
    }

    public abstract void init();

    protected String completeOrderTask(Map<String, Object> params) {
        return this.completeTask(params, id, CompleteTaskCommand.Type.ORDER);
    }

    public CustomerOrder getCustomerOrder() {
        return customerOrder;
    }

    public String getApdPo() {
        PoNumber apdPo = customerOrder.getApdPo();
        if (apdPo != null) {
            return apdPo.getValue();
        }
        else {
            return DEFAULT_PO_VALUE;
        }
    }
}
