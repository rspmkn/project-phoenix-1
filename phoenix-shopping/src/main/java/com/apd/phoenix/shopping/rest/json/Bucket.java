package com.apd.phoenix.shopping.rest.json;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder( { "header", "i", "sugs" })
public class Bucket {

    @JsonProperty("header")
    private String header;
    @JsonProperty("i")
    private Integer i;
    @JsonProperty("sugs")
    private List<Sug> sugs = new ArrayList<Sug>();
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * 
     * @return
     *     The header
     */
    @JsonProperty("header")
    public String getHeader() {
        return header;
    }

    /**
     * 
     * @param header
     *     The header
     */
    @JsonProperty("header")
    public void setHeader(String header) {
        this.header = header;
    }

    /**
     * 
     * @return
     *     The i
     */
    @JsonProperty("i")
    public Integer getI() {
        return i;
    }

    /**
     * 
     * @param i
     *     The i
     */
    @JsonProperty("i")
    public void setI(Integer i) {
        this.i = i;
    }

    /**
     * 
     * @return
     *     The sugs
     */
    @JsonProperty("sugs")
    public List<Sug> getSugs() {
        return sugs;
    }

    /**
     * 
     * @param sugs
     *     The sugs
     */
    @JsonProperty("sugs")
    public void setSugs(List<Sug> sugs) {
        this.sugs = sugs;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
