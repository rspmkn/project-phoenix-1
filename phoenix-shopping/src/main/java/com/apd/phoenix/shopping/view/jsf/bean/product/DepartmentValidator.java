/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.shopping.view.jsf.bean.product;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import org.apache.commons.lang.StringUtils;
import com.apd.phoenix.shopping.view.jsf.bean.user.UserMaintenanceBean;

/**
 *
 * @author zgutterm
 */
@FacesValidator("departmentValidator")
public class DepartmentValidator implements Validator {

    public static final String THIS_FIELD_IS_REQUIRED = "This field is required";

    @Override
    public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {
        //Cannot inject a bean in a JSF validator
        UserMaintenanceBean userMaintenanceBean = context.getApplication().evaluateExpressionGet(context,
                "#{userMaintenanceBean}", UserMaintenanceBean.class);

        if (userMaintenanceBean.isDepartmentListEmpty()) {
            if (!(value instanceof String) || (String) value == null || StringUtils.isEmpty((String) value)) {
                throw new ValidatorException(new FacesMessage(THIS_FIELD_IS_REQUIRED));
            }
        }
    }
}
