/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.shopping.view.jsf.bean.order;

import java.io.Serializable;
import javax.ejb.LocalBean;
import javax.ejb.Stateful;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.model.CatalogXItem;
import com.apd.phoenix.service.model.Sku;
import com.apd.phoenix.service.product.Product;
import com.apd.phoenix.shopping.view.jsf.bean.cashout.AddToCartBean;
import com.apd.phoenix.shopping.view.jsf.bean.search.SearchEngine;
import com.apd.phoenix.shopping.view.jsf.bean.search.SearchUtilities;
import com.apd.phoenix.shopping.view.jsf.bean.utils.ViewUtils;

/**
 *
 * @author dnorris
 */
@Named
@Stateful
@LocalBean
@RequestScoped
public class ExpressOrderBean implements Serializable {

    private static final long serialVersionUID = 1L;

    private static final Logger logger = LoggerFactory.getLogger(ExpressOrderBean.class);
    private static final String EXPRESS_ORDER_PAGE_LINK = "/shopping/ecommerce/order/expressOrder.xhtml";
    private String sku;
    private String price;
    private int qty;
    private String units;
    private String name;
    private String errorMessage;
    private Product product;
    @Inject
    private AddToCartBean addToCartBean;
    @Inject
    private SearchEngine searchEngine;
    @Inject
    private SearchUtilities searchUtilities;

    public void populateProductData() {
        if (logger.isDebugEnabled()) {
            logger.debug("Express order search");
        }
        if (StringUtils.isBlank(sku)) {
            resetPage();
            errorMessage = " NO SKU Entered";
            return;
        }
        product = searchEngine.expressOrderSearch(sku);
        CatalogXItem cxi = searchUtilities.getFromProduct(product);
        if (product == null || cxi == null) {
            resetPage();
            errorMessage = " EXACT MATCH NOT FOUND.";
            return;
        }
        boolean foundSku = (cxi.getCustomerSku() != null && sku.equalsIgnoreCase(cxi.getCustomerSku().getValue()));
        for (Sku itemSku : cxi.getItem().getSkus()) {
            if (sku.equalsIgnoreCase(itemSku.getValue())) {
                foundSku = true;
            }
        }
        if (!foundSku) {
            //if an item is found, but none of its SKUs match the value entered
            resetPage();
            errorMessage = " EXACT MATCH NOT FOUND.";
            return;
        }
        handleResults();
    }

    private void handleResults() {
        addToCartBean.getDisplayQuantity().put(product, "");
        price = product.getPrice().toPlainString();
        units = product.getUnits();
        name = product.getName();
        errorMessage = "";
    }

    public String getOrderPageLink() {
        return EXPRESS_ORDER_PAGE_LINK;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public String getPrice() {
        return ViewUtils.toCurrency(price);
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public String getUnits() {
        return units;
    }

    public void setUnits(String units) {
        this.units = units;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    private void resetPage() {
        sku = "";
        price = "";
        qty = 0;
        units = "";
        errorMessage = "";
        name = "";
        product = null;
    }

    public Product getProduct() {
        return product;
    }

    public void setQuantity() {
        populateProductData();
        if (qty < 1) {
            qty = 1;
        }
        if (product != null) {
            addToCartBean.setDisplayQuantity(product, qty);
        }
    }

    public void addToCart() {
        populateProductData();
        if (qty < 1) {
            qty = 1;
        }
        if (product != null && product.getId() != null && !addToCartBean.isAddingPrevented(product)) {
            addToCartBean.addSingleItem(product, qty);
        }
        resetPage();
    }
}
