/*
 * To change this template; choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.shopping.view.jsf.bean.search;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.apd.phoenix.service.product.Product;
import com.apd.phoenix.shopping.view.jsf.bean.search.ProductSort.SortOption;
import com.apd.phoenix.shopping.view.jsf.bean.utils.AttributeFilterType;
import com.apd.phoenix.shopping.view.jsf.bean.utils.FilterKey;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;

/**
 *
 * @author RHC
 */
public class ResponseData {

    private List<Product> products = new ArrayList<>();
    private int numFound = 0;
    private List<SearchHierarchy> matchingHierarchies = new ArrayList<>();
    private List<SearchHierarchy> breadcrumbs = new ArrayList<>();
    private Multimap<AttributeFilterType, FilterKey> filters = HashMultimap.create();
    private Map<FilterKey, Integer> manufacturers = new HashMap<>();
	private Multimap<String, FilterKey> appliedFilters = HashMultimap.create();
	private Map<FilterKey,Integer> avaliableCategoryFilters = new HashMap<>();
	private List<Product> featuredProducts;
	private boolean showCoreItemFilter = true;
	private boolean showPriceFilter = true;
	private boolean showAnyFilter = true;
	private String autoCorrectMessage;
	private String autoCorrectValue;
	private List<SortOption> sortOptions = new ArrayList<SortOption>();
	private String didYouMeanMessage;
	private String crossReference;
	private Map<FilterKey,BigInteger> greenFilter;
	private boolean clearSearchString = false;
	private String contractFilterId;
	private List<String> synonyms;

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }

    public int getNumFound() {
        return numFound;
    }

    public void setNumFound(int numFound) {
        this.numFound = numFound;
    }

    public List<SearchHierarchy> getMatchingHierarchies() {
        return matchingHierarchies;
    }

    public void setMatchingHierarchies(List<SearchHierarchy> matchingHierarchies) {
        this.matchingHierarchies = matchingHierarchies;
    }

    public List<SearchHierarchy> getBreadcrumbs() {
        return breadcrumbs;
    }

    public void setBreadcrumbs(List<SearchHierarchy> breadcrumbs) {
        this.breadcrumbs = breadcrumbs;
    }

    public Map<FilterKey, Integer> getManufacturers() {
        return manufacturers;
    }

    public void setManufacturers(Map<FilterKey, Integer> manufacturers) {
        this.manufacturers = manufacturers;
    }

	public List<Product> getFeaturedProducts() {
		return featuredProducts;
	}

	public void setFeaturedProducts(List<Product> featuredProducts) {
		this.featuredProducts = featuredProducts;
	}

	public Multimap<String, FilterKey> getAppliedFilters() {
		return appliedFilters;
	}

	public void setAppliedFilters(Multimap<String, FilterKey> appliedFilters) {
		this.appliedFilters = appliedFilters;
	}

	public boolean isShowCoreItemFilter() {
		return showCoreItemFilter;
	}

	public void setShowCoreItemFilter(boolean showCoreItemFilter) {
		this.showCoreItemFilter = showCoreItemFilter;
	}

	public boolean isShowPriceFilter() {
		return showPriceFilter;
	}

	public void setShowPriceFilter(boolean showPriceFilter) {
		this.showPriceFilter = showPriceFilter;
	}

	public boolean isShowAnyFilter() {
		return showAnyFilter;
	}

	public void setShowAnyFilter(boolean showAnyFilter) {
		this.showAnyFilter = showAnyFilter;
	}

	public String getAutoCorrectMessage() {
		return autoCorrectMessage;
	}

	public void setAutoCorrectMessage(String autoCorrectMessage) {
		this.autoCorrectMessage = autoCorrectMessage;
	}

	public String getAutoCorrectValue() {
		return autoCorrectValue;
	}

	public void setAutoCorrectValue(String autoCorrectValue) {
		this.autoCorrectValue = autoCorrectValue;
	}

	public List<SortOption> getSortOptions() {
		return sortOptions;
	}

	public void setSortOptions(List<SortOption> sortOptions) {
		this.sortOptions = sortOptions;
	}

	public String getDidYouMeanMessage() {
		return didYouMeanMessage;
	}

	public void setDidYouMeanMessage(String didYouMeanMessage) {
		this.didYouMeanMessage = didYouMeanMessage;
	}

	public String getCrossReference() {
		return crossReference;
	}

	public void setCrossReference(String crossReference) {
		this.crossReference = crossReference;
	}

	public Map<FilterKey,Integer> getAvaliableCategoryFilters() {
		return avaliableCategoryFilters;
	}

	public void setAvaliableCategoryFilters(Map<FilterKey,Integer> avaliableCategoryFilters) {
		this.avaliableCategoryFilters = avaliableCategoryFilters;
	}

	public Map<FilterKey,BigInteger> getGreenFilter() {
		return greenFilter;
	}

	public void setGreenFilter(Map<FilterKey,BigInteger> greenFilter) {
		this.greenFilter = greenFilter;
	}

	public Multimap<AttributeFilterType, FilterKey> getFilters() {
		return filters;
	}

	public void setFilters(Multimap<AttributeFilterType, FilterKey> filters) {
		this.filters = filters;
	}

	public boolean isClearSearchString() {
		return clearSearchString;
	}

	public void setClearSearchString(boolean clearSearchString) {
		this.clearSearchString = clearSearchString;
	}

	public String getContractFilterId() {
		return contractFilterId;
	}

	public void setContractFilterId(String contractFilterId) {
		this.contractFilterId = contractFilterId;
	}

	public List<String> getSynonyms() {
		if(synonyms == null){
			synonyms = new ArrayList<String>();
		}
		return synonyms;
	}

	public void setSynonyms(List<String> synonyms) {
		this.synonyms = synonyms;
	}
}
