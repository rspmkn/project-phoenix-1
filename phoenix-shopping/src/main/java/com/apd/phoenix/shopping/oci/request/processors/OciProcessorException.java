package com.apd.phoenix.shopping.oci.request.processors;

public class OciProcessorException extends Exception {

    private static final long serialVersionUID = -3711702310840303497L;

    public OciProcessorException(String message) {
        super(message);
    }

    public OciProcessorException(Exception e) {
        super(e);
    }
}
