/*
 * To change this template; choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.shopping.view.jsf.bean.search;

import java.io.Serializable;
import java.util.Comparator;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author RHC
 */
public class SearchHierarchy implements Serializable {

    private static final long serialVersionUID = 1L;

    private static final Logger logger = LoggerFactory.getLogger(SearchHierarchy.class);

    private String displayName;

    private String searchEngineKey;

    public SearchHierarchy() {
    }

    public SearchHierarchy(String displayName, String searchEngineKey) {
        this.displayName = displayName;
        this.searchEngineKey = searchEngineKey;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getSearchEngineKey() {
        return searchEngineKey;
    }

    public void setSearchEngineKey(String searchEngineKey) {
        this.searchEngineKey = searchEngineKey;
    }

    public static class SearchHierarchyComparator implements Comparator<SearchHierarchy> {

        @Override
        public int compare(SearchHierarchy o1, SearchHierarchy o2) {
            return o1.getDisplayName().compareTo(o2.getDisplayName());
        }

    }

    @Override
    public String toString() {
        logger
                .warn("The toString() method on SearchHierarchy shouldn't be called, use displayName or searchEngineKey instead");
        return this.getDisplayName();
    }

    @Override
    public boolean equals(Object obj) {
        try {
            SearchHierarchy that = (SearchHierarchy) obj;
            return StringUtils.equals(that.getDisplayName(), this.getDisplayName())
                    && StringUtils.equals(that.getSearchEngineKey(), this.getSearchEngineKey());
        }
        catch (Exception e) {
            return false;
        }
    }

    @Override
    public int hashCode() {
        return StringUtils.isNotBlank(searchEngineKey) ? searchEngineKey.hashCode() : 0;
    }
}
