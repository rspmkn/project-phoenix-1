package com.apd.phoenix.shopping.cxml.request.processors;

import static com.apd.phoenix.service.cxml.CxmlConstants.CXML_STATUS_BAD_REQUEST;
import static com.apd.phoenix.service.cxml.CxmlConstants.CXML_STATUS_INTERNAL_ERROR;
import static com.apd.phoenix.service.cxml.CxmlConstants.CXML_STATUS_NOT_ACCEPTABLE;
import static com.apd.phoenix.service.cxml.CxmlConstants.CXML_STATUS_NOT_IMPLEMENTED;
import static com.apd.phoenix.service.cxml.CxmlConstants.CXML_STATUS_OK;
import static com.apd.phoenix.service.cxml.CxmlConstants.CXML_STATUS_UNAUTHORIZED;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import javax.annotation.PostConstruct;
import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Singleton;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;
import org.xml.sax.InputSource;
import com.apd.phoenix.core.StringEscape;
import com.apd.phoenix.core.utility.TenantConfigRepository;
import com.apd.phoenix.service.business.CustomerOrderBp;
import com.apd.phoenix.service.business.PunchoutSessionBp;
import com.apd.phoenix.service.business.SystemUserBp;
import com.apd.phoenix.service.integration.cxml.model.cxml.CXML;
import com.apd.phoenix.service.integration.cxml.model.cxml.Credential;
import com.apd.phoenix.service.integration.cxml.model.cxml.Header;
import com.apd.phoenix.service.integration.cxml.model.cxml.Message;
import com.apd.phoenix.service.integration.cxml.model.cxml.OrderRequest;
import com.apd.phoenix.service.integration.cxml.model.cxml.PunchOutOrderMessage;
import com.apd.phoenix.service.integration.cxml.model.cxml.PunchOutSetupRequest;
import com.apd.phoenix.service.integration.cxml.model.cxml.PunchOutSetupResponse;
import com.apd.phoenix.service.integration.cxml.model.cxml.Request;
import com.apd.phoenix.service.integration.cxml.model.cxml.Response;
import com.apd.phoenix.service.integration.cxml.model.cxml.SharedSecret;
import com.apd.phoenix.service.integration.cxml.model.cxml.StartPage;
import com.apd.phoenix.service.integration.cxml.model.cxml.Status;
import com.apd.phoenix.service.integration.cxml.model.cxml.URL;
import com.apd.phoenix.service.model.Account;
import com.apd.phoenix.service.model.AccountXCredentialXUser;
import com.apd.phoenix.service.model.CustomerOrder;
import com.apd.phoenix.service.model.CustomerOrder.PunchoutOrderOperation;
import com.apd.phoenix.service.model.CxmlConfiguration;
import com.apd.phoenix.service.model.MessageMetadata.PunchoutType;
import com.apd.phoenix.service.model.OrderStatus.OrderStatusEnum;
import com.apd.phoenix.service.model.PunchoutSession;
import com.apd.phoenix.service.model.SystemUser;
import com.apd.phoenix.service.persistence.multitenancy.impl.CurrentTenantIdentifierResolverImpl;
import com.apd.phoenix.shopping.camel.process.CxmlCxmlTranslator;
import com.apd.phoenix.shopping.cxml.response.CxmlSchemaResponseProcessor;
import com.apd.phoenix.shopping.view.jsf.bean.login.LoginBean;
import com.apd.phoenix.web.utils.SessionUtils;

/**
 *
 * @author dnorris
 */
@Singleton
@Lock(LockType.READ)
public class CxmlRequestProcessor extends BaseRequestProcessor {

    private static final Logger logger = LoggerFactory.getLogger(CxmlRequestProcessor.class);

    @Inject
    private CxmlSchemaResponseProcessor responseProcessor;

    @Inject
    private PunchoutSessionBp punchoutSessionBp;

    @Inject
    private CustomerOrderBp customerOrderBp;

    @Inject
    private SystemUserBp systemUserBp;

    private JAXBContext cxmlJaxbContext;

    private JAXBContext punchoutSetupJaxbContext;

    @PostConstruct
    private void init() {
        try {
            cxmlJaxbContext = JAXBContext.newInstance(CXML.class);
            punchoutSetupJaxbContext = JAXBContext.newInstance(PunchOutSetupRequest.class);
        }
        catch (JAXBException ex) {
            logger.error("Error initializing CxmlRequestProcessor bean", ex);
        }
    }

    @Override
    public void processRequest(HttpServletRequest httpRequest, HttpServletResponse httpResponse, String cXML) {
        try (ByteArrayInputStream inputStream = new ByteArrayInputStream(cXML.getBytes())) {
            XMLInputFactory inputFactory = XMLInputFactory.newInstance();
            XMLEventReader reader = inputFactory.createXMLEventReader(inputStream);
            Unmarshaller jaxbUnmarshaller = cxmlJaxbContext.createUnmarshaller();
            CXML cxmlDocument = (CXML) jaxbUnmarshaller.unmarshal(reader);
            
            Header cxmlHeader = null;
            Message cxmlMessage = null;
            Request cxmlRequest = null;
            Response cxmlResponse = null;
            String payloadId = cxmlDocument.getPayloadID();
            for (Object o : cxmlDocument.getHeaderOrMessageOrRequestOrResponse()) {
                if (o instanceof Header) {
                    cxmlHeader = (Header) o;
                }
                else if (o instanceof Message) {
                	cxmlMessage = (Message) o;
                }
                else if (o instanceof Request) {
                    cxmlRequest = (Request) o;
                }
                else if (o instanceof Response) {
                	cxmlResponse = (Response) o;
                }
            }
            final boolean hasInvalidResponse = cxmlResponse != null && (cxmlMessage!=null || cxmlRequest!=null||cxmlHeader!=null);
            // A valid CXML document must include both a Header and one of message or request.  If the inbound CXML 
            // document is a response in reply to a prior asynchronous CXML request document, it should include only a response)
            if (cxmlHeader ==  null
            		|| hasInvalidResponse
            		|| ((cxmlMessage == null && cxmlRequest == null) 
            				|| (cxmlMessage != null && cxmlRequest != null))) {
            	String errorMessage = "Invalid cxml request recieved";
                if (logger.isDebugEnabled()) {
                    logger.debug("Error thrown in processRequest method, " + errorMessage);
                }
                cxmlResponse = responseProcessor.createInvalidResponse();
            }
            if (cxmlRequest != null) {
            	Account account = getAssociatedAccount(cxmlHeader);
            	cxmlResponse = processRequest(cxmlRequest, account, cXML, payloadId);
            	responseProcessor.initializeResponse(httpResponse, cxmlResponse);
            } else if (cxmlMessage != null) {
        		validateAndProcessMessage(cxmlMessage, cXML, payloadId, httpRequest, httpResponse);
            }
        } catch (JAXBException | XMLStreamException | IOException ex) {
            if (logger.isDebugEnabled()) {
                logger.debug("Error parsing cxml", ex);
            }
            logger.error("Error parsing cxml", ex);
            Response cxmlResponse = responseProcessor.createInvalidResponse();
            responseProcessor.initializeResponse(httpResponse, cxmlResponse);
        } catch (CxmlProcessorException ex) {
            logger.error("CxmlProcessorException caught, enable debug logging to see more details. -- " + ex.getMessage());
            if (logger.isDebugEnabled()) {
                logger.debug(ex.getMessage(), ex);
            }
	    Response cxmlResponse;
            switch (ex.getStatusCode()) {
                case CXML_STATUS_INTERNAL_ERROR:
                    cxmlResponse = responseProcessor.createInternalErrorResponse();
                    responseProcessor.initializeResponse(httpResponse, cxmlResponse);
                    break;
                case CXML_STATUS_BAD_REQUEST:
                    cxmlResponse = responseProcessor.createInvalidResponse();
                    responseProcessor.initializeResponse(httpResponse, cxmlResponse);
                    break;
                case CXML_STATUS_NOT_ACCEPTABLE:
                    cxmlResponse = responseProcessor.createInvalidResponse();
                    responseProcessor.initializeResponse(httpResponse, cxmlResponse);
                    break;
                case CXML_STATUS_NOT_IMPLEMENTED:                    
                    cxmlResponse = responseProcessor.createNotImplementedResponse();
                    responseProcessor.initializeResponse(httpResponse, cxmlResponse);
                    break;
                default:
                    cxmlResponse = responseProcessor.createInternalErrorResponse();
                    responseProcessor.initializeResponse(httpResponse, cxmlResponse);
                    break;
            }
        }
        
    }

    private Account getAssociatedAccount(Header header) throws CxmlProcessorException {
        Account toReturn;
        try {
            List<Credential> fromCredentials = header.getFrom().getCredential();
            List<Credential> toCredentials = header.getTo().getCredential();
            List<Credential> senderCredentials = header.getSender().getCredential();
            Map<String, String> fromCredentialMap = getCredentialMapFromList(fromCredentials);
            Map<String, String> toCredentialMap = getCredentialMapFromList(toCredentials);
            Map<String, String> senderCredentialMap = getCredentialMapFromList(senderCredentials);
            String senderSharedSecret = getSharedSecret(senderCredentials);
            toReturn = cxmlConfigurationBp.getConfigurationFromCxmlHeaderData(fromCredentialMap, toCredentialMap,
                    senderCredentialMap, senderSharedSecret);
        }
        catch (Exception ex) {
            String errorMessage = "Could not locate account for the cxml credential set.";
            if (logger.isDebugEnabled()) {
                logger.debug(errorMessage, ex);
            }
            throw new CxmlProcessorException.Builder().message(errorMessage).statusCode(CXML_STATUS_NOT_ACCEPTABLE)
                    .build();
        }
        if (toReturn == null) {
            String errorMessage = "Could not locate account for the cxml credential set.";
            if (logger.isDebugEnabled()) {
                logger.debug(errorMessage);
            }
            throw new CxmlProcessorException.Builder().message(errorMessage).statusCode(CXML_STATUS_NOT_ACCEPTABLE)
                    .build();
        }
        return toReturn;
    }

    private static String getSharedSecret(List<Credential> senderCredentials) {
        String sharedSecret = "";
        List<Object> nodeList = senderCredentials.get(0).getSharedSecretOrDigitalSignatureOrCredentialMac();
        for (Object o : nodeList) {
            if (o instanceof SharedSecret) {
                SharedSecret ss = (SharedSecret) o;
                sharedSecret = (String) ss.getContent().get(0);
            }
        }
        return sharedSecret;
    }

    private static Map<String, String> getCredentialMapFromList(List<Credential> credentials) throws CxmlProcessorException {
        Map<String, String> credentialMap = new HashMap<>();
        for (Credential credential : credentials) {
            String identity = (String) credential.getIdentity().getContent().get(0);
            //Security check for duplicate domains in request
            if (!credentialMap.containsKey(credential.getDomain())) {
                credentialMap.put(credential.getDomain(), identity);
            } else {
                //reject request per the cxml user guide
                //("The receiver should reject the document if there are multiple credentials in a To, From,
                //  or Sender section that use different values but use the same domain.")
                throw new CxmlProcessorException.Builder().message("Message contains duplicate domain names").statusCode(CXML_STATUS_NOT_ACCEPTABLE).build();
            }
        }
        return credentialMap;
    }

    private Response processRequest(Request request, Account account, String rawMessage, String payloadID)
            throws CxmlProcessorException {
        for (Object o : request
                .getProfileRequestOrOrderRequestOrMasterAgreementRequestOrPurchaseRequisitionRequestOrPunchOutSetupRequestOrProviderSetupRequestOrStatusUpdateRequestOrGetPendingRequestOrSubscriptionListRequestOrSubscriptionContentRequestOrSupplierListRequestOrSupplierDataRequestOrSubscriptionStatusUpdateRequestOrCopyRequestOrCatalogUploadRequestOrAuthRequestOrDataRequestOrOrganizationDataRequest()) {
            if (o instanceof OrderRequest) {
                Response cxmlResponse = cxmlService.processOrderRequest((OrderRequest) o, rawMessage, payloadID,
                        account);
                return cxmlResponse;
            }
            else if (o instanceof PunchOutSetupRequest) {
                Response cxmlResponse = generatePunchoutSetupResponse((PunchOutSetupRequest) o, account);
                return cxmlResponse;
            }
        }
        logger.error("Unsupported cxml request received");
        return responseProcessor.createNotImplementedResponse();
    }

    private void validateAndProcessMessage(Message message, String rawMessage, String payloadID,
            HttpServletRequest httpRequest, HttpServletResponse httpResponse) throws CxmlProcessorException {
        for (Object o : message
                .getPunchOutOrderMessageOrProviderDoneMessageOrSubscriptionChangeMessageOrDataAvailableMessageOrSupplierChangeMessageOrOrganizationChangeMessage()) {
            if (o instanceof PunchOutOrderMessage) {

                try {
                    //Authenticate the punchout session token
                    HttpSession httpSession = httpRequest.getSession();
                    String currentSessionToken = LoginBean.getPunchoutSessionAttribute(httpSession);
                    String requestedSessionToken = httpRequest.getParameter("sessionToken");
                    String sessionToAuthenticate = null;

                    if (requestedSessionToken != null && !requestedSessionToken.equals(currentSessionToken)) {
                        sessionToAuthenticate = requestedSessionToken;
                    }
                    else if (httpRequest.getUserPrincipal() == null) {
                        sessionToAuthenticate = currentSessionToken;
                    }

                    if (sessionToAuthenticate != null) {
                        //Log out of current session
                        if (httpRequest.getUserPrincipal() != null) {
                            httpRequest.logout();
                        }
                        //Invalidate the current session if it's not already invalid
                        if (currentSessionToken != null) {
                            SessionUtils.invalidateSessionIfValid(httpRequest, httpSession);
                        }
                        httpRequest.login(sessionToAuthenticate, sessionToAuthenticate);
                        httpSession = httpRequest.getSession();
                        LoginBean.setPunchoutSessionAttribute(httpSession, sessionToAuthenticate);
                    }
                    currentSessionToken = LoginBean.getPunchoutSessionAttribute(httpSession);

                    PunchoutSession punchoutSessionSearch = new PunchoutSession();
                    punchoutSessionSearch.setBuyerCookie(((PunchOutOrderMessage) o).getBuyerCookie().getContent()
                            .get(0).toString());
                    punchoutSessionSearch.setSessionToken(currentSessionToken);
                    List<PunchoutSession> matchingSessions = punchoutSessionBp.searchByExactExample(
                            punchoutSessionSearch, 0, 0);

                    PunchoutSession currentPunchoutSession = null;

                    if (matchingSessions == null || matchingSessions.size() != 1) {
                        throw new CxmlProcessorException.Builder()
                                .message(
                                        "Either the buyer cookie does not match the requested punchout session or the punchout session has ended.")
                                .statusCode(CXML_STATUS_UNAUTHORIZED).build();
                    }
                    else {
                        currentPunchoutSession = matchingSessions.get(0);
                    }

                    String quoteId = cxmlService.processPunchoutOrderMessage((PunchOutOrderMessage) o, rawMessage,
                            payloadID);
                    String quoteURL = "/shopping/ecommerce/cashout/checkout.xhtml?quoteId="
                            + StringEscape.escapeForUrl(quoteId);

                    CustomerOrder customerOrder = customerOrderBp.searchByApdPo(quoteId);
                    currentPunchoutSession.setCustomerOrder(customerOrder);
                    punchoutSessionBp.update(currentPunchoutSession);

                    httpResponse.sendRedirect(httpResponse.encodeRedirectURL(quoteURL));
                }
                catch (IOException e) {
                    throw new CxmlProcessorException.Builder().message(e.getMessage()).statusCode(
                            CXML_STATUS_INTERNAL_ERROR).build();
                }
                catch (ServletException e) {
                    throw new CxmlProcessorException.Builder().message(
                            "The request punchout session has already ended.  Please punch in again.").statusCode(
                            CXML_STATUS_UNAUTHORIZED).build();
                }
            }
        }
    }

    private Response generatePunchoutSetupResponse(PunchOutSetupRequest request, Account account)
            throws CxmlProcessorException {
        Properties properties = TenantConfigRepository.getInstance().getPropertiesByTenantId(
                CurrentTenantIdentifierResolverImpl.getCurrentTenant(), "tenant");
        PunchoutSession session = createPunchoutSessionEntity(request, account);
        Response response = createBaseOkResponse();
        PunchOutSetupResponse punchoutResponse = new PunchOutSetupResponse();
        StartPage startPage = new StartPage();
        URL url = new URL();
        url.setvalue(properties.getProperty("punchoutBaseLoginUrl")
                + StringEscape.escapeForUrl(session.getSessionToken()));
        startPage.setURL(url);
        punchoutResponse.setStartPage(startPage);
        response
                .getProfileResponseOrPunchOutSetupResponseOrProviderSetupResponseOrGetPendingResponseOrSubscriptionListResponseOrSubscriptionContentResponseOrSupplierListResponseOrSupplierDataResponseOrAuthResponseOrDataResponseOrOrganizationDataResponse()
                .add(punchoutResponse);
        return response;
    }

    private static Response createBaseOkResponse() {
        Response cxmlResponse = new Response();
        Status status = new Status();
        status.setCode(Integer.toString(CXML_STATUS_OK));
        status.setText("Ok");
        cxmlResponse.setStatus(status);
        return cxmlResponse;
    }

    private PunchoutSession createPunchoutSessionEntity(PunchOutSetupRequest request, Account account)
            throws CxmlProcessorException {

        PunchoutSession punchoutSession = new PunchoutSession();

        try {
            //        PunchoutSession session = new PunchoutSession();
            String buyerCookie = (String) request.getBuyerCookie().getContent().get(0);
            //        session.setBuyerCookie(buyerCookie);
            String userLogin = extractSystemUserInfo(account, request);
            //        session.setSystemUserLoginName(extractSystemUserInfo(account, request));
            String browserFormPostUrl = request.getBrowserFormPost().getURL().getvalue();
            //        session.setBrowserFormPostUrl(browserFormPostUrl);
            PunchoutOrderOperation operation = PunchoutOrderOperation.valueOf(request.getOperation());

            punchoutSession.setBuyerCookie(buyerCookie);
            punchoutSession.setSystemUserLoginName(userLogin);
            punchoutSession.setBrowserFormPostUrl(browserFormPostUrl);
            punchoutSession.setOperation(operation);

            if (operation.equals(PunchoutOrderOperation.edit) || operation.equals(PunchoutOrderOperation.inspect)) {
                String quotedOrderNumber = CxmlCxmlTranslator.getQuotedOrderNumber(request.getItemOut());
                if (StringUtils.isBlank(quotedOrderNumber)) {
                    //TODO verify this logic
                    throw new CxmlProcessorException.Builder()
                            .message(
                                    "Order number not found in "
                                            + "SupplierPartAuxiliaryId nor SupplierPartId in any items in the Punchout edit request."
                                            + "Previous Punchout order could not be loaded for editing.").statusCode(
                                    CXML_STATUS_INTERNAL_ERROR).build();
                }
                CustomerOrder existingPunchoutOrder = customerOrderBp.searchByApdPo(quotedOrderNumber);
                if (existingPunchoutOrder == null) {
                    throw new CxmlProcessorException.Builder().message(
                            "Punchout order with order number \"" + quotedOrderNumber + "\"could not be found.")
                            .statusCode(CXML_STATUS_INTERNAL_ERROR).build();
                }
                if (!existingPunchoutOrder.getStatus().getValue().equals(OrderStatusEnum.PUNCHED.getValue())) {
                    throw new CxmlProcessorException.Builder().message(
                            "Punchout order with order number \"" + quotedOrderNumber + "\" is already processed.")
                            .statusCode(CXML_STATUS_INTERNAL_ERROR).build();
                }

                punchoutSession.setCustomerOrder(existingPunchoutOrder);

                PunchoutSession punchoutSessionSearch = new PunchoutSession();
                punchoutSessionSearch.setBuyerCookie(buyerCookie);
                //        	punchoutSessionSearch.setBrowserFormPostUrl(browserFormPostUrl);
                //        	punchoutSessionSearch.setSystemUserLoginName(userLogin);
                punchoutSessionSearch.setOperation(PunchoutOrderOperation.valueOf(request.getOperation()));
                punchoutSessionSearch.setCustomerOrder(existingPunchoutOrder);

                if (operation.equals(PunchoutOrderOperation.edit)) {
                    if (!existingPunchoutOrder.getOperationAllowed().equals(PunchoutOrderOperation.edit)) {
                        throw new CxmlProcessorException.Builder().message(
                                "Editing is not permitted for punchout order with order number \"" + quotedOrderNumber
                                        + "\".").statusCode(CXML_STATUS_INTERNAL_ERROR).build();
                    }

                    List<PunchoutSession> openEditSessions = punchoutSessionBp
                            .getOpenPunchoutSessions(punchoutSessionSearch);
                    if (!CollectionUtils.isEmpty(openEditSessions)) {
                        if (openEditSessions.size() > 1) {
                            String errorMessage = "Concurrent edit operations for the same punchout order not allowed.";
                            if (logger.isDebugEnabled()) {
                                logger.debug(errorMessage);
                            }
                            throw new CxmlProcessorException.Builder().message(errorMessage).statusCode(
                                    CXML_STATUS_INTERNAL_ERROR).build();
                        }
                        else {
                            punchoutSession = openEditSessions.get(0);
                            // punchoutSession.setEndTime(null);
                            punchoutSessionBp.update(punchoutSession);
                        }
                    }
                }
                else if (operation.equals(PunchoutOrderOperation.inspect)) {
                    if (existingPunchoutOrder.getOperationAllowed().equals(PunchoutOrderOperation.create)) {
                        throw new CxmlProcessorException.Builder().message(
                                "Viewing details for punchout order with order number \"" + quotedOrderNumber
                                        + "\" is not permitted.").statusCode(CXML_STATUS_INTERNAL_ERROR).build();
                    }

                    List<PunchoutSession> openInspectSessions = punchoutSessionBp
                            .getOpenPunchoutSessions(punchoutSessionSearch);
                    if (!CollectionUtils.isEmpty(openInspectSessions)) {
                        // Reuse one of the existing open inspect sessions that originated from the same buyer cookie
                        punchoutSession = openInspectSessions.remove(0);
                        punchoutSessionBp.update(punchoutSession);

                        // Clean up any remaining open inspect sessions for that buyer cookie.
                        for (PunchoutSession openInspectSession : openInspectSessions) {
                            //    openInspectSession.setEndTime(new Date());
                            punchoutSessionBp.update(openInspectSession);
                        }
                    }
                }
            }

            if (punchoutSession.getAccntCredUser() == null) {
                com.apd.phoenix.service.model.Credential credential = extractCredentialInfo(account, userLogin, request);
                AccountXCredentialXUser accountXCredentialXUser = getAccountCredUser(account, credential, userLogin);
                punchoutSession.setAccntCredUser(accountXCredentialXUser);
            }

            if (punchoutSession.getSessionToken() == null) {
                punchoutSessionBp.createPunchoutSession(punchoutSession);
            }
            else {
                punchoutSessionBp.update(punchoutSession);
            }
        }
        catch (Exception e) {
            String errorMessage = "Error persisting PunchoutSession entity";
            if (logger.isDebugEnabled()) {
                logger.debug(errorMessage, e);
            }
            throw new CxmlProcessorException.Builder().message(errorMessage).statusCode(CXML_STATUS_INTERNAL_ERROR)
                    .build();
        }
        return punchoutSession;
    }

    private AccountXCredentialXUser getAccountCredUser(Account account,
            com.apd.phoenix.service.model.Credential credential, String loginName) throws CxmlProcessorException {
        AccountXCredentialXUser accountCredUser = null;
        try {
            List<AccountXCredentialXUser> credentialList = getAccountXCredentialsForUser(loginName);
            if (credentialList.size() == 1) {
                accountCredUser = credentialList.get(0);
                validateAccountMatch(accountCredUser, account);
            }
            else {
                for (AccountXCredentialXUser accountXCred : credentialList) {
                    if (accountXCred.getCredential().getId().toString().equals(credential.getId().toString())) {
                        accountCredUser = accountXCred;
                        break;
                    }
                }
            }
            if (accountCredUser == null) {
                throw new Exception();
            }
        }
        catch (Exception ex) {
            if (ex instanceof CxmlProcessorException) {
                throw (CxmlProcessorException) ex;
            }
            else {
                throw new CxmlProcessorException.Builder().message(
                        "Could not find AccountXCredentialXUser for this shopping session.").statusCode(
                        CXML_STATUS_INTERNAL_ERROR).build();
            }
        }
        return accountCredUser;
    }

    private static void validateAccountMatch(AccountXCredentialXUser accountCredUser, Account account)
            throws CxmlProcessorException {
        if (accountCredUser.getAccount().getId().compareTo(account.getId()) != 0) {
            final boolean parentAccountMatches = accountCredUser.getAccount().getParentAccount() != null
                    && accountCredUser.getAccount().getParentAccount().getId().compareTo(account.getId()) == 0;
            final boolean rootAccountMatches = accountCredUser.getAccount().getRootAccount() != null
                    && accountCredUser.getAccount().getRootAccount().getId().compareTo(account.getId()) == 0;
            if (parentAccountMatches || rootAccountMatches) {
                return;
            }
            String errorMessage = "accountCredUser id does not match account id associated with this punchout attempt.";
            if (logger.isDebugEnabled()) {
                logger.debug(errorMessage);
            }
            throw new CxmlProcessorException.Builder().message(errorMessage).statusCode(CXML_STATUS_INTERNAL_ERROR)
                    .build();
        }
    }

    private List<AccountXCredentialXUser> getAccountXCredentialsForUser(String loginName) throws CxmlProcessorException {
        List<AccountXCredentialXUser> resultList = null;
        SystemUser systemUser = null;
        try {
            systemUser = systemUserBp.getSystemUserByLogin(loginName).get(0);
            if (systemUser == null) {
                throw new Exception();
            }
            resultList = axcxuBp.getAllAXCXU(systemUser);
            if (resultList == null || resultList.isEmpty()) {
                throw new Exception();
            }
        }
        catch (Exception ex) {
            throw new CxmlProcessorException.Builder().message(
                    "Could not locate system user from login name: " + loginName)
                    .statusCode(CXML_STATUS_INTERNAL_ERROR).build();
        }
        return resultList;
    }

    private String extractSystemUserInfo(Account account, PunchOutSetupRequest request) throws CxmlProcessorException {
        try {
            String userLogin = "";

            //Get default 
            CxmlConfiguration cxmlConfiguration = account.getCxmlConfiguration();
            String defaultUserLogin = null;
            if (cxmlConfiguration != null) {
                defaultUserLogin = cxmlConfiguration.getSystemUserXpathExpressionDefaultResult();
            }

            String xpathPrefix = "/cXML/Request";
            //Remove xpath prefix before evaluation
            String requestTypeAgnosticXpath = StringUtils.substringAfter(getSystemUserXpathFromAccount(account),
                    xpathPrefix);
            if (StringUtils.isNotBlank(requestTypeAgnosticXpath)
                    && !requestTypeAgnosticXpath.equalsIgnoreCase("NOT APPLICABLE")) {
                XPath xPath = XPathFactory.newInstance().newXPath();
                String xml = marshallToString(punchoutSetupJaxbContext, request);

                //Allow for invalid xpath, can use the default login.
                try {
                    userLogin = xPath.evaluate(requestTypeAgnosticXpath, new InputSource(new StringReader(xml)));
                }
                catch (XPathExpressionException e) {
                    if (logger.isDebugEnabled()) {
                        logger.warn("invalidXPathExpression: " + requestTypeAgnosticXpath + " using default value.");
                    }
                }
            }
            if (StringUtils.isNotBlank(cxmlConfiguration.getSystemUserPrefix()) && StringUtils.isNotBlank(userLogin)) {
                userLogin = cxmlConfiguration.getSystemUserPrefix().trim() + userLogin.trim();
            }
            boolean userExists = !systemUserBp.isLoginAvailable(userLogin);
            if ((StringUtils.isEmpty(userLogin) && StringUtils.isNotEmpty(defaultUserLogin)) || !userExists) {
                userLogin = defaultUserLogin;
            }
            return userLogin;
        }
        catch (JAXBException ex) {
            String errorMessage = "Unable to get system user information using xpath expression";
            if (logger.isDebugEnabled()) {
                logger.debug(errorMessage, ex);
            }
            throw new CxmlProcessorException.Builder().message(errorMessage).statusCode(CXML_STATUS_INTERNAL_ERROR)
                    .build();
        }
    }

    private com.apd.phoenix.service.model.Credential extractCredentialInfo(Account account, String userLogin,
            PunchOutSetupRequest request) throws CxmlProcessorException {
        try {
            CxmlConfiguration cxmlConfiguration = account.getCxmlConfiguration();

            String xpathPrefix = "/cXML/Request";
            String requestTypeAgnosticXpath = StringUtils.substringAfter(getCredentialNameXpathFromAccount(account),
                    xpathPrefix);
            String credentialToSelect = "";
            if (StringUtils.isNotBlank(requestTypeAgnosticXpath)) {
                XPath xPath = XPathFactory.newInstance().newXPath();
                String xml = marshallToString(punchoutSetupJaxbContext, request);
                credentialToSelect = xPath.evaluate(requestTypeAgnosticXpath, new InputSource(new StringReader(xml)));
            }
            if (StringUtils.isNotBlank(credentialToSelect)
                    && StringUtils.isNotBlank(cxmlConfiguration.getCredentialNamePrefix())) {
                credentialToSelect = cxmlConfiguration.getCredentialNamePrefix().trim() + credentialToSelect.trim();
            }

            List<com.apd.phoenix.service.model.Credential> matchingCredentials = new ArrayList<com.apd.phoenix.service.model.Credential>();
            for (com.apd.phoenix.service.model.Credential credential : account.getCredentials()) {
                if (credential.getPunchoutType() != null && credential.getPunchoutType().equals(PunchoutType.CXML)) {
                    matchingCredentials.add(credential);
                }
            }
            if (matchingCredentials.size() > 1) {
                for (Iterator<com.apd.phoenix.service.model.Credential> iter = matchingCredentials.iterator(); iter
                        .hasNext();) {
                    com.apd.phoenix.service.model.Credential accountCredential = (com.apd.phoenix.service.model.Credential) iter
                            .next();
                    boolean userHasCredential = false;
                    for (AccountXCredentialXUser userCredential : getAccountXCredentialsForUser(userLogin)) {
                        if (userCredential.getCredential().equals(accountCredential)
                                && (userCredential.getAccount().equals(account) || (userCredential.getAccount() != null &&
                                // allows for the account to be used if the parent account is configured
                                // to use that cxml configuration
                                userCredential.getAccount().getRootAccount().equals(account)))) {
                            userHasCredential = true;
                            break;
                        }
                    }
                    if (!userHasCredential
                            || (StringUtils.isNotBlank(credentialToSelect) && !StringUtils.equals(credentialToSelect,
                                    accountCredential.getName()))) {
                        iter.remove();
                    }
                }

                if (matchingCredentials.size() > 1) {
                    String errorMessage = "Ambiguous result, multiple matching credentials from search";
                    if (logger.isDebugEnabled()) {
                        logger.debug(errorMessage);
                    }
                    throw new CxmlProcessorException.Builder().message(errorMessage).statusCode(
                            CXML_STATUS_INTERNAL_ERROR).build();
                }
                else {
                    return matchingCredentials.get(0);
                }

            }
            else {
                if (StringUtils.isNotBlank(credentialToSelect)
                        && !StringUtils.equals(credentialToSelect, matchingCredentials.get(0).getName())) {
                    String errorMessage = "The requested punchout credential not available for the account and/or user.";
                    if (StringUtils.isNotBlank(matchingCredentials.get(0).getName())) {
                        errorMessage = errorMessage + " Expected " + matchingCredentials.get(0).getName()
                                + ", but the only avaliable credential was " + credentialToSelect + ".";
                    }
                    if (logger.isDebugEnabled()) {
                        logger.debug(errorMessage);
                    }
                    throw new CxmlProcessorException.Builder().message(errorMessage).statusCode(
                            CXML_STATUS_INTERNAL_ERROR).build();
                }
                return matchingCredentials.get(0);
            }

        }
        catch (Exception ex) {
            String errorMessage = "Unable to get credential user information using xpath expression";
            if (logger.isDebugEnabled()) {
                logger.debug(errorMessage, ex);
            }
            throw new CxmlProcessorException.Builder().message(errorMessage).statusCode(CXML_STATUS_INTERNAL_ERROR)
                    .build();
        }
    }

}
