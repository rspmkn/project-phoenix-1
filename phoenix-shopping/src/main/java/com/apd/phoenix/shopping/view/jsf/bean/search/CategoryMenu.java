package com.apd.phoenix.shopping.view.jsf.bean.search;

import com.google.common.collect.HashMultimap;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author RHC
 */
public class CategoryMenu implements Searcher, Serializable {

    private static final long serialVersionUID = 1L;

    private static final Logger logger = LoggerFactory.getLogger(CategoryMenu.class);

    private Map<CategoryMenuNode, HashMultimap<SearchHierarchy, SearchHierarchy>> categoryData;

    private Set<SearchHierarchy> topLevelCategories;

    private List<SearchHierarchy> popularTopLevelCategories;

    private String orderingString;

    public CategoryMenu() {

    }

    /**
     * Builds the internal data structure for storing the category hierarchy
     * data
     */
    public void buildCategoryMenu(Set<SearchHierarchy[]> facetFieldValues) {
        categoryData = new HashMap<>();
        for (SearchHierarchy[] levels : facetFieldValues) {
            if (levels.length == 0) {
                break;
            }
            for (int i = 0; i < levels.length; i++) {
                HashMultimap<SearchHierarchy, SearchHierarchy> levelData = HashMultimap.create();
                CategoryMenuNode node = new CategoryMenuNode(i, i == 0 ? null : levels[i == 1 ? i-1 : i-2]);
                if (categoryData.keySet().contains(node)) {
                    levelData = categoryData.get(node);
                }
                if (i == 0) {
                    levelData.put(levels[i], levels[i]);
                } else {
                    levelData.put(levels[i - 1], levels[i]);
                }
                categoryData.put(node, levelData);
            }
        }
        initTopLevelCategories();
    }

    /**
     * Return a url for first level category links
     *
     * @return url
     */
    public String getUrl(String searchEngineKey) {
        StringBuilder url = new StringBuilder(SEARCH_PAGE_BASE_URL);
        url.append("?").append(HIERARCHY_KEY_PARAMETER);
        url.append("=");
        url.append(encode(searchEngineKey));
        url.append("&");
        url.append(SearchUtilities.NEW_SEARCH_PARAMETER);
        url.append("=");
        url.append(Boolean.TRUE);
        return url.toString();
    }

    /**
     * Method to reconstuct the full hierarchy path to support the url
     * construction process
     *
     * @return url encoded string representation of full hierarchy path
     */
    private String encode(String value) {
        try {
            value = URLEncoder.encode(value, "UTF-8");
        }
        catch (UnsupportedEncodingException ex) {
            logger.error("Error url decoding value " + value, ex);
        }
        return value;

    }

    /**
     * @return level 0 category names
     */
    public Set<SearchHierarchy> getTopLevelCategories() {
        return topLevelCategories;
    }

    public List<SearchHierarchy> getTopLevelCategoriesList() {
    	if (this.topLevelCategories == null) {
    		return null;
    	}
    	List<SearchHierarchy> toReturn = new ArrayList<>();
    	toReturn.addAll(topLevelCategories);
    	Collections.sort(toReturn, new SearchHierarchy.SearchHierarchyComparator());
        return toReturn;
    }

    public List<SearchHierarchy> getPopularTopLevelCategoriesList() {
        return this.popularTopLevelCategories;
    }

    private void initTopLevelCategories() {
        topLevelCategories = new HashSet<>();
        for (CategoryMenuNode node : categoryData.keySet()) {
            if (node.getLevel() == 0) {
                topLevelCategories.addAll(categoryData.get(node).keySet());
            }
        }
    }

    /**
     * @return Menu items for given category
     */
    public Collection<SearchHierarchy> getMenuItems(int level, SearchHierarchy parentMenu, SearchHierarchy menu) {
        List<SearchHierarchy> toReturn = new ArrayList<>();
        for (CategoryMenuNode node : categoryData.keySet()) {
            if (node.getLevel() == level && node.getParent() != null && node.getParent().equals(parentMenu)) {
                toReturn.addAll(categoryData.get(node).get(menu));
            }
        }
        Collections.sort(toReturn, new SearchHierarchy.SearchHierarchyComparator());
        return toReturn;
    }

    /**
     * Calls getMenuItems passing the menuName as the parentMenuName, since menus on this level reference themselves as parents, i.e.
     * the label of the CategoryMenuNode for items on this level is equal to the key of the corresponding levelData HashMultiMap<String, String>
     * @param menuName
     * @return Menu items for a level-one category
     */
    public Collection<SearchHierarchy> getLevelOneMenuItems(SearchHierarchy menu) {
        return getMenuItems(1, menu, menu);
    }

    /**
     * Method for exposing the menu size for calculating the width of the popup
     * in the ui
     */
    public int getMenuWidth(SearchHierarchy category) {
        return getLevelOneMenuItems(category).size();
    }

    /**
     * @return data structure containing the hierarchy data
     */
    public Map<CategoryMenuNode, HashMultimap<SearchHierarchy, SearchHierarchy>> getCategoryData() {
        return categoryData;
    }

    /**
     * Takes a list of the top-level categories in the database, sorted by popularity
     * 
     * @param popularCategories
     */
	public void setPopularTopLevelCategories(Map<String, Integer> catalogPriorities) {
        this.popularTopLevelCategories = new ArrayList<SearchHierarchy>();
        List<SearchHierarchy> allTopLevelCategories = new ArrayList<SearchHierarchy>();
        //adds all current top level categories
        allTopLevelCategories.addAll(this.topLevelCategories);
        //sorts by popularity
        Collections.sort(allTopLevelCategories, new HierarchyPopularityComparator(catalogPriorities));
        //adds all items that are specified in the catalog priority
        for (SearchHierarchy hierarchy : allTopLevelCategories) {
        	if (hierarchy != null && catalogPriorities.get(hierarchy.getDisplayName()) != null) {
        		this.popularTopLevelCategories.add(hierarchy);
        	}
        }
    }

    public String getOrderingString() {
        return orderingString;
    }

    public void setOrderingString(String orderingString) {
        this.orderingString = orderingString;
    }

    private class HierarchyPopularityComparator implements Comparator<SearchHierarchy> {

        private Map<String, Integer> catalogPriorities = new HashMap<>();

        private HierarchyPopularityComparator(Map<String, Integer> catalogPriorities) {
        	if (catalogPriorities != null) {
        		this.catalogPriorities = catalogPriorities;
        	}
        }

        @Override
        public int compare(SearchHierarchy o1, SearchHierarchy o2) {
            Integer position1 = catalogPriorities.get(o1.getDisplayName());
            Integer position2 = catalogPriorities.get(o2.getDisplayName());
            if ((position1 != null || position2 != null) && position1 != position2) {
            	return compareUnequalValues(position1, position2);
            }
            return o1.getDisplayName().compareTo(o2.getDisplayName());
        }
        
        private int compareUnequalValues(Integer val1, Integer val2) {
        	if (val1 == null && val2 != null) {
        		return 1;
        	}
        	if (val1 != null && val2 == null) {
        		return -1;
        	}
        	if (val1 == null && val2 == null) {
        		//shouldn't happen, blocked by nullcheck in compare() method
        		return 0;
        	}
        	return val1 - val2;
        }
    }

}