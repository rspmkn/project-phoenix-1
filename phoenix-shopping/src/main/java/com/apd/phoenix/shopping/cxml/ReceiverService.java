/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.shopping.cxml;

import com.apd.phoenix.shopping.cxml.request.processors.CatalogRequestProcessor;
import com.apd.phoenix.shopping.cxml.request.processors.CxmlRequestProcessor;
import com.apd.phoenix.shopping.cxml.request.processors.FulFillRequestProcessor;
import com.apd.phoenix.shopping.cxml.request.processors.InvoiceRequestProcessor;
import com.ctc.wstx.evt.WDTD;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.XMLEvent;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author dnorris
 */
public class ReceiverService extends HttpServlet {

    private static final long serialVersionUID = 645693990360581571L;
    private static final String URL_ENCODED_CXML_FIELD_NAME = "cxml-urlencoded";
    private static final String BASE_64_ENCODED_FIELD_NAME = "cxml-base64";
    private static final Logger logger = LoggerFactory.getLogger(ReceiverService.class);

    @Inject
    private FulFillRequestProcessor fulfillRequestProcessor;

    @Inject
    private InvoiceRequestProcessor invoiceRequestProcessor;

    @Inject
    private CatalogRequestProcessor catalogRequestProcessor;

    @Inject
    private CxmlRequestProcessor cxmlRequestProcessor;

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "Get is not supported");
    }

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException {
        try {
            String cXML = parseRequestForCxml(request);
            if (logger.isDebugEnabled()) {
                logger.debug("Raw Incoming CXML: " + cXML);
            }
            processRequest(request, response, cXML);
        }
        catch (IllegalStateException ex) {
            logger.error(ex.getMessage(), ex);
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
        }
    }

    private String parseRequestForCxml(HttpServletRequest request) throws IllegalStateException {
        String cXML = null;
        try {
            for (String param : request.getParameterMap().keySet()) {
                if (param.equalsIgnoreCase(URL_ENCODED_CXML_FIELD_NAME)) {
                    cXML = request.getParameter(param);
                    break;
                }
                else if (param.equalsIgnoreCase(BASE_64_ENCODED_FIELD_NAME)) {
                    cXML = request.getParameter(param);
                    cXML = base64Decode(cXML);
                    break;
                }
            }
            if (StringUtils.isBlank(cXML)) {
                cXML = IOUtils.toString(request.getInputStream());
                if (StringUtils.isBlank(cXML)) {
                    throw new Exception();
                }
            }
        }
        catch (Exception ex) {
            if (logger.isDebugEnabled()) {
                logger.debug(ex.getMessage(), ex);
            }
            throw new IllegalStateException("Could not find cxml content in request");
        }
        return cXML;
    }

    private String urlDecode(String cXML) throws UnsupportedEncodingException {
        cXML = URLDecoder.decode(cXML, "UTF-8");
        return cXML;
    }

    private String base64Decode(String cXML) throws UnsupportedEncodingException {
        byte[] decoded = Base64.decodeBase64(cXML.getBytes("UTF-8"));
        cXML = new String(decoded, "UTF-8");
        return cXML;
    }

    private void processRequest(HttpServletRequest request, HttpServletResponse response, String cXML) {
        try {           
            CxmlDtdType cxmlDtdType = getDtdType(cXML);
            switch (cxmlDtdType) {
                case FULFILL_DTD:
                    fulfillRequestProcessor.processRequest(request, response, cXML);
                    break;
                case CATALOG_DTD:
                    catalogRequestProcessor.processRequest(request, response, cXML);
                    break;
                case INVOICE_DETAIL_DTD:
                    invoiceRequestProcessor.processRequest(request, response, cXML);
                    break;
                case CXML_DTD:
                    cxmlRequestProcessor.processRequest(request, response, cXML);
                    break;
            }
        } catch (XMLStreamException | IOException ex) {
            logger.error("Error while attempting to parse cxml request.", ex);
            //Set response code
            //No cxml body attached in this case
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
        }
    }

    private CxmlDtdType getDtdType(String cXML) throws XMLStreamException, IOException {
        String dtdType = "";
        try (ByteArrayInputStream inputStream = new ByteArrayInputStream(cXML.getBytes())) {
            XMLInputFactory inputFactory = XMLInputFactory.newInstance();
            XMLEventReader reader = inputFactory.createXMLEventReader(inputStream);
            while (reader.hasNext()) {
                XMLEvent event = reader.nextEvent();
                if (event.getEventType() == XMLStreamConstants.DTD) {
                    WDTD dtd = (WDTD) event;
                    String systemId = dtd.getSystemId();
                    dtdType = StringUtils.substringAfterLast(systemId, "/");
                    break;
                }
            }
            CxmlDtdType cxmlDtdType = CxmlDtdType.fromString(dtdType);
            return cxmlDtdType;
        }
    }
}
