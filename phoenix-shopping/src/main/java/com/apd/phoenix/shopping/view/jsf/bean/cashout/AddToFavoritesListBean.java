package com.apd.phoenix.shopping.view.jsf.bean.cashout;

import javax.ejb.Stateful;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.business.AccountXCredentialXUserBp;
import com.apd.phoenix.service.business.CatalogXItemBp;
import com.apd.phoenix.service.business.FavoritesListBp;
import com.apd.phoenix.service.model.CatalogXItem;
import com.apd.phoenix.service.model.FavoritesList;
import com.apd.phoenix.service.product.Product;
import com.apd.phoenix.shopping.view.jsf.bean.favorites.FavoritesBean;
import com.apd.phoenix.shopping.view.jsf.bean.login.CredentialSelectionBean;

@Named
@Stateful
@SessionScoped
public class AddToFavoritesListBean {

    private static final Logger logger = LoggerFactory.getLogger(AddToFavoritesListBean.class);

    @Inject
    private AccountXCredentialXUserBp accountXCredentialXUserBp;
    @Inject
    private CredentialSelectionBean credentialSelectionBean;
    @Inject
    private FavoritesBean favoritesBean;
    @Inject
    private FavoritesListBp favoritesListBp;
    @Inject
    private CatalogXItemBp catalogXItemBp;
    private String selectedFavoritesListName;
    private CatalogXItem catalogXItem;
    private boolean productAddedToList = false;
    private Product product;

    private long getMatchingListID() {
        FavoritesList list = accountXCredentialXUserBp.getFavoritesListByNameAndCredential(selectedFavoritesListName,
                this.credentialSelectionBean.getCurrentAXCXU());
        if (list != null) {
            return list.getId();
        }
        return -1;
    }

    public void setupAddToList(Long productId) {
        if (this.product == null) {
            this.product = new Product();
        }
        this.product.setId(productId);
    }

    public void addProductToFavorites() {
        if (product.getId() != null && product.getId() != 0) {
            catalogXItem = catalogXItemBp.findById(product.getId(), CatalogXItem.class);
        }
        else {
            logger.error("Could not determine catxitem to add to favorites list");
            return;
        }
        FavoritesList selectedList = new FavoritesList();
        selectedList.setName(selectedFavoritesListName);
        selectedList.setId(getMatchingListID());
        if (selectedList.getId() == -1) {
            //Add new list
            favoritesBean.addNewUserList(selectedFavoritesListName);
            selectedList.setId(getMatchingListID());
            if (selectedList.getId() == -1) {
                logger.error("Could not add new favorites list.");
                FacesContext.getCurrentInstance().addMessage(null,
                        new FacesMessage("Error creating new favorites list"));
            }
            selectedList = favoritesListBp.searchByExactExample(selectedList, 0, 0).get(0);
            selectedList = favoritesListBp.findById(selectedList.getId(), FavoritesList.class);
            selectedList.getUserItems().add(catalogXItem);
            selectedList = favoritesListBp.update(selectedList);
            favoritesBean.refreshPage();
        }
        else {
            selectedList = favoritesListBp.searchByExactExample(selectedList, 0, 0).get(0);
            selectedList = favoritesListBp.findById(selectedList.getId(), FavoritesList.class);
            selectedList.getUserItems().add(catalogXItem);
            selectedList = favoritesListBp.update(selectedList);
            favoritesBean.refreshPage();
        }
        favoritesBean.updateUserFavoritesList();
        //this.initializePageData();
        this.productAddedToList = true;
    }

    public String getSelectedFavoritesListName() {
        return selectedFavoritesListName;
    }

    public void setSelectedFavoritesListName(String selectedFavoritesListName) {
        this.selectedFavoritesListName = selectedFavoritesListName;
    }

    public CatalogXItem getCatalogXItem() {
        return catalogXItem;
    }

    public void setCatalogXItem(CatalogXItem catalogXItem) {
        this.catalogXItem = catalogXItem;
    }

    public boolean getProductAddedToList() {
        return productAddedToList;
    }

    public void setProductAddedToList(boolean productAddedToList) {
        this.productAddedToList = productAddedToList;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }
}
