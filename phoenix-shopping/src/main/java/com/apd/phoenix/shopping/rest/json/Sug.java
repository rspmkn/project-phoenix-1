package com.apd.phoenix.shopping.rest.json;

import java.util.HashMap;
import java.util.Map;
import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder( { "i", "val" })
public class Sug {

    @JsonProperty("i")
    private Integer i;
    @JsonProperty("val")
    private String val;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * 
     * @return
     *     The i
     */
    @JsonProperty("i")
    public Integer getI() {
        return i;
    }

    /**
     * 
     * @param i
     *     The i
     */
    @JsonProperty("i")
    public void setI(Integer i) {
        this.i = i;
    }

    /**
     * 
     * @return
     *     The val
     */
    @JsonProperty("val")
    public String getVal() {
        return val;
    }

    /**
     * 
     * @param val
     *     The val
     */
    @JsonProperty("val")
    public void setVal(String val) {
        this.val = val;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
