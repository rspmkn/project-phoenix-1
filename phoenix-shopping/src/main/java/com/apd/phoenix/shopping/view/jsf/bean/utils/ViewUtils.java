package com.apd.phoenix.shopping.view.jsf.bean.utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.business.EntityComparator;
import com.apd.phoenix.service.model.CatalogXItem;
import com.apd.phoenix.service.product.Product;
import com.apd.phoenix.service.product.ProductNotFoundException;
import com.apd.phoenix.shopping.view.jsf.bean.search.SearchEngine;

/**
 * Utilities for presentation layer.
 */
@Named
@LocalBean
@Stateless
public class ViewUtils {

    private static final Logger LOGGER = LoggerFactory.getLogger(ViewUtils.class);

    @Inject
    private SearchEngine searchEngine;

    public static <T> List<T> asList(Collection<T> collection) {

        if (collection == null) {
            return null;
        }

        List<T> toReturn = new ArrayList<>(collection);

        Collections.sort(toReturn, new EntityComparator());

        return toReturn;
    }

    public <T, S> List<Map.Entry<T, S>> mapToList(Map<T, S> map) {
        if (map == null) {
            return null;
        }
        List<Map.Entry<T, S>> list = new ArrayList<>();
        list.addAll(map.entrySet());
        return list;
    }

    public List<Product> convertCatalogXItemsToProducts(List<CatalogXItem> items) {
        if (items == null) {
            return null;
        }
        List<Product> products = new ArrayList<>();
        for (CatalogXItem catXItem : items) {
            Product p = searchEngine.getProductFromCatalogXItem(catXItem);
            if (p != null && !"no".equals(p.getProperties().get(CatalogXItem.AVAILABILITY_PROPERTY))) {
                products.add(p);
            }
        }
        return products;
    }

    public static String toCurrency(String rawValue) {
        double doubleValue = 0.0;

        try {
            doubleValue = Double.parseDouble(rawValue);
        }
        catch (Exception e) {
            LOGGER.debug("Could not parse double ", e);
        }

        String currencyFormatValue = new java.text.DecimalFormat("$#,##0.00").format(doubleValue);
        return currencyFormatValue;
    }

    /**
     * The method on SearchEngine should be used instead
     */
    @Deprecated
    public Product getCatalogProductFromVendorNameAndApdSku(String vendorName, String apdSku)
            throws ProductNotFoundException {
        return searchEngine.getCatalogProductFromVendorNameAndApdSku(vendorName, apdSku);
    }

    /**
     * The method on SearchEngine should be used instead
     */
    @Deprecated
    public Product getProductFromVendorNameAndApdSku(String vendorName, String apdSku) {
        return searchEngine.getProductFromVendorNameAndApdSku(vendorName, apdSku);
    }

    /**
     * The method on SearchEngine should be used instead
     */
    @Deprecated
    public Product getProductFromCatalogXItem(CatalogXItem item) {
        return searchEngine.getProductFromCatalogXItem(item);
    }
}
