/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.shopping.view.jsf.bean;

import com.apd.phoenix.service.product.Product;
import com.apd.phoenix.shopping.view.jsf.bean.search.SearchEngine;
import com.apd.phoenix.shopping.view.jsf.bean.utils.LinksBean;
import com.apd.phoenix.core.PageNumberDisplay;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.annotation.PostConstruct;
import javax.ejb.LocalBean;
import javax.ejb.Stateful;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author dcnorris
 */
@Named
@Stateful
@LocalBean
//TODO consider converting this to conversation scope
@SessionScoped
public class HomeBean implements Serializable {

    private static final long serialVersionUID = 1L;
    @Inject
    private LinksBean linksBean;
    private static final Logger logger = LoggerFactory.getLogger(HomeBean.class);
    private List<Product> featuredProductList = new ArrayList<>();
    private int previousPage;
    private int numFound;
    private int randomSort;
    private PageNumberDisplay pageDisplay;
    public static final PageNumberDisplay.PageSize PAGE_SIZE = PageNumberDisplay.PageSize.SIX;
    public static final int ENTRIES_PER_ROW = 3;
    @Inject
    private SearchEngine searchEngine;

    @PostConstruct
    public void init() {
        numFound = searchEngine.featuredProductsFound();
        pageDisplay = new PageNumberDisplay(numFound);
        pageDisplay.setPageSize(PAGE_SIZE);
        Random rand = new Random();
        randomSort = rand.nextInt(99999);
    }
    
    public List<Product> getFeaturedProductList() {
        if (pageDisplay.getPage() != previousPage) {
        	if (logger.isDebugEnabled()) {
        		logger.debug("Sending request for featured products");
        	}
            previousPage = pageDisplay.getPage();
            featuredProductList = searchEngine.featuredProductList((pageDisplay.getPage() - 1) * pageDisplay.getSize(), randomSort);
        }
        return featuredProductList;
    }

    public int getPreviousPage() {
        return previousPage;
    }

    public void setPreviousPage(int previousPage) {
        this.previousPage = previousPage;
    }

    public PageNumberDisplay getPageDisplay() {
        return pageDisplay;
    }

    public void setPageDisplay(PageNumberDisplay pageDisplay) {
        this.pageDisplay = pageDisplay;
    }
    
    public int getENTRIES_PER_ROW() {
    	return ENTRIES_PER_ROW;
    }

}
