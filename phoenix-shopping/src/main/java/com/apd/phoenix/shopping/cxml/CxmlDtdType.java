/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.apd.phoenix.shopping.cxml;

/**
 *
 * @author dnorris
 */
public enum CxmlDtdType {
    FULFILL_DTD("fulfill.dtd"), CATALOG_DTD("catalog.dtd"), INVOICE_DETAIL_DTD("invoicedetail.dtd"), CXML_DTD(
            "cxml.dtd");

    private String typeValue;

    private CxmlDtdType(String typeValue) {
        this.typeValue = typeValue;
    }

    public String getTypeValue() {
        return typeValue;
    }

    public static CxmlDtdType fromString(String text) {
        if (text != null) {
            for (CxmlDtdType cXmlDtdType : CxmlDtdType.values()) {
                if (text.equalsIgnoreCase(cXmlDtdType.getTypeValue())) {
                    return cXmlDtdType;
                }
            }
        }
        return null;
    }

}
