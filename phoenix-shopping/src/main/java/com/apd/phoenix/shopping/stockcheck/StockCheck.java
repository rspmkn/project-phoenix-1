package com.apd.phoenix.shopping.stockcheck;

/**
 * Interface for StockCheck functionality.
 * 
 * @author RHC
 *
 */
public interface StockCheck {

    /**
     * Returns the quantity of the item specified with the vendor SKU that is available
     * 
     * @param vendorSku
     * @return
     */
    public int available(String vendorSku);

    /**
     * Returns true if the stock check implementation should be used with the specified vendor
     * 
     * @param vendorName
     * @return
     */
    public boolean vendorNameMatches(String vendorName);
}
