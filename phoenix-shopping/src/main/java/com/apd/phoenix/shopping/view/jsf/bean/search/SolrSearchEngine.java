package com.apd.phoenix.shopping.view.jsf.bean.search;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import org.apache.commons.lang.StringUtils;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrQuery.ORDER;
import org.apache.solr.client.solrj.SolrServer;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.response.FacetField;
import org.apache.solr.client.solrj.response.FacetField.Count;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.client.solrj.util.ClientUtils;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;
import org.apache.solr.common.params.ModifiableSolrParams;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.core.StringEscape;
import com.apd.phoenix.core.utility.EcommercePropertiesLoader;
import com.apd.phoenix.core.utility.PropertiesLoader;
import com.apd.phoenix.service.business.CatalogXItemBp;
import com.apd.phoenix.service.business.MatchbookBp;
import com.apd.phoenix.service.model.Catalog.SearchType;
import com.apd.phoenix.service.model.CatalogXItem;
import com.apd.phoenix.service.model.Manufacturer;
import com.apd.phoenix.service.model.Matchbook;
import com.apd.phoenix.service.product.Product;
import com.apd.phoenix.service.product.ProductNotFoundException;
import com.apd.phoenix.service.solr.SolrServiceBean;
import com.apd.phoenix.shopping.view.jsf.bean.HomeBean;
import com.apd.phoenix.shopping.view.jsf.bean.favorites.FavoritesViewBean;
import com.apd.phoenix.shopping.view.jsf.bean.login.CredentialSelectionBean;
import com.apd.phoenix.shopping.view.jsf.bean.search.ProductSort.SortOption;
import com.apd.phoenix.shopping.view.jsf.bean.search.productlookup.ProductLookupService;
import com.apd.phoenix.shopping.view.jsf.bean.utils.FilterKey;
import com.apd.phoenix.shopping.view.jsf.bean.utils.LinksBean;

@LocalBean
@Stateless
@SolrSearch
public class SolrSearchEngine implements SearchEngine, SolrSearcher {

    private static final String EXPANDED_SYNONYMS_SOLR_FIELD_NAME = "expandedSynonyms";

    private static final String SOLR_FIELD_MOST_POPULAR = "mostPopular";

    private static final String GREEN_SOLR_FIELD = "green";

    private static final String YES = "Y";

    private static final String CROSSREF_KEY = "CROSSREFERENCE_FACET_QUERY:";

    private static final Logger logger = LoggerFactory.getLogger(SolrSearchEngine.class);

    private static final int MAX_NUMBER_OF_SUGGESTIONS_NEEDED = 10;

    private static final SortOption[] SOLR_SORT_OPTIONS = { SortOption.BestMatch, SortOption.SortByProductNameAsc,
            SortOption.SortByProductNameDesc, SortOption.SortBySkuAsc, SortOption.SortBySkuDesc,
            SortOption.SortByBrandAsc, SortOption.SortByBrandDesc, SortOption.SortByPriceAsc,
            SortOption.SortByPriceDesc, SortOption.MostPopular };

    private static final String SOLR_BRAND_NAME = "brandName";

    @Inject
    private SolrServiceBean solrServiceBean;

    @Inject
    private SolrQueryUtilities solrQueryUtilities;

    @Inject
    private CredentialSelectionBean credentialSelectionBean;

    @Inject
    private CatalogXItemBp catalogXItemBp;

    @Inject
    private LinksBean linksBean;

    @Inject
    private ProductLookupService productLookup;

    @Inject
    private MatchbookBp matchbookBp;

    @Inject
    private FavoritesViewBean favoritesViewBean;

    private SolrServer getSolrServer() {
        return solrServiceBean.getSolrServer();
    }

    @Override
    public CategoryMenu buildCategoryMenu() {
    	SolrQuery solrQuery = buildCategoryMenuSolrQuery();
		QueryResponse categoryMenuResponse;
		CategoryMenu toReturn = new CategoryMenu();
		try {
			categoryMenuResponse = getSolrServer().query(solrQuery);
	        FacetField field = categoryMenuResponse.getFacetField(HIERARCHY_PATH_FACET_VALUE);
	        Set<SearchHierarchy[]> facetFieldValues = new HashSet<>();
	        for (FacetField.Count c : field.getValues()) {
	        	String[] hierarchies = c.getName().substring(1, c.getName().length()).split(HIERARCHY_DELIMITER);
	        	String existingPath = "";
	        	List<SearchHierarchy> toAdd = new ArrayList<SearchHierarchy>(hierarchies.length);
	        	for (String hierarchy : hierarchies) {
	        		existingPath = existingPath + HIERARCHY_DELIMITER + hierarchy;
	        		toAdd.add(new SearchHierarchy(hierarchy, existingPath));
	        	}
	            facetFieldValues.add(toAdd.toArray(new SearchHierarchy[0]));
	        }
	        toReturn.buildCategoryMenu(facetFieldValues);
		} catch (SolrServerException e) {
			logger.error("Solr Exception while building category menu", e);
		}
		return toReturn;
    }

    private SolrQuery buildCategoryMenuSolrQuery() {
        SolrQuery solrQuery = new SolrQuery();
        solrQuery.addField(HIERARCHY_PATH_FACET_VALUE);
        solrQuery = solrQueryUtilities.addFilterByCatalogID(solrQuery);
        solrQuery.add("q", "*:*");
        solrQuery.addFacetField(HIERARCHY_PATH_FACET_VALUE);
        ModifiableSolrParams param = new ModifiableSolrParams();
        param.set("facet.mincount", 1);
        param.set("facet.limit", -1);
        solrQuery.add(param);
        return solrQuery;
    }

    @Override
    public int featuredProductsFound() {
        QueryResponse response = this.getFeaturedProductsResponse(0, 0);
        if (response != null && response.getResults() != null) {
            return (int) response.getResults().getNumFound();
        }
        return 0;
    }

    @Override
    public List<Product> featuredProductList(Integer start, Integer randomSort) {
    	QueryResponse response = this.getFeaturedProductsResponse(start, randomSort);
    	if (response != null && response.getResults() != null) {
    		return SolrSearchUtilities.convertToProductList(response.getResults());
    	}
    	return new ArrayList<>();
    }

    private QueryResponse getFeaturedProductsResponse(Integer start, Integer randomSort) {
        SolrQuery solrQuery = new SolrQuery();
        solrQuery = solrQueryUtilities.addFilterByCatalogID(solrQuery);
        solrQuery = solrQueryUtilities.addAvailableOnlyFilter(solrQuery);
        solrQuery.setStart(start);
        solrQuery.setRows(HomeBean.PAGE_SIZE.toInt());
        solrQuery.add("q", "*:*");
        this.addCoreFilterQuery(solrQuery);
        solrQuery.addSort("random_" + randomSort, SolrQuery.ORDER.asc);
        try {
            return getSolrServer().query(solrQuery);
        }
        catch (SolrServerException e) {
            logger.error("Error attempting to Query Solr Index Server", e);
        }
        return null;
    }

    @Override
    public Product expressOrderSearch(String sku) {
        if (StringUtils.isBlank(sku)) {
            return null;
        }
        sku = solrQueryUtilities.escapeSpecialChars(sku.toUpperCase());
        String alternate = "";
        String primary = "";
        String customerQuery = CUSTOMER_SKU_FIELD_S + ":" + sku;
        String apdQuery = APD_SKU_FIELD_S + ":" + sku;
        if (isCustomerSkuTypeAvailable()) {
            primary = customerQuery;
            alternate = apdQuery;
        }
        else {
            //Search by apdSKu
            primary = apdQuery;
            alternate = customerQuery;
        }
        QueryResponse response = queryForSku(primary);
        int numFound = (int) response.getResults().getNumFound();
        if (numFound == 1) {
            return getSingleResult(response);
        }
        else if (numFound > 1) {
            logger.warn("Multiple items found with " + (isCustomerSkuTypeAvailable() ? "customer" : "apd") + " sku "
                    + sku);
            return getSingleResult(response);
        }
        else {
            response = queryForSku(alternate);
            numFound = (int) response.getResults().getNumFound();
            if (numFound == 1) {
                return getSingleResult(response);
            }
            else if (numFound > 1) {
                logger.warn("Multiple items found with " + (isCustomerSkuTypeAvailable() ? "apd" : "customer")
                        + " sku " + sku);
                return getSingleResult(response);
            }
        }
        return null;
    }

    private boolean isCustomerSkuTypeAvailable() {
        return credentialSelectionBean.getCurrentCredential().getSkuType().getName().equals("customer");
    }

    private QueryResponse queryForSku(String queryString) {
        SolrQuery solrQuery = new SolrQuery();
        solrQuery = solrQueryUtilities.addFilterByCatalogID(solrQuery);
        //Search by Customer Sku
        solrQuery.add("q", queryString);
        try {
            return getSolrServer().query(solrQuery);
        }
        catch (SolrServerException e) {
            logger.error("Error contacting index server", e);
            return null;
        }
    }

    private Product getSingleResult(QueryResponse response) {
        SolrDocumentList searchResultDocuments = response.getResults();
        if (response != null && response.getResults() != null && !response.getResults().isEmpty()) {
            return SolrSearchUtilities.convertToProductList(searchResultDocuments).get(0);
        }
        return null;
    }

    @Override
    public Product getCatalogProductFromVendorNameAndApdSku(String vendorName, String apdSku)
            throws ProductNotFoundException {
        Product product = new Product();
        try {
            SolrQuery solrQuery = new SolrQuery();
            solrQuery.set("q", APD_SKU_FIELD_S + ":\"" + apdSku + "\"");
            solrQuery.addFilterQuery(VENDOR_NAME_FIELD + ":\"" + vendorName + "\"");
            solrQueryUtilities.addFilterByCatalogID(solrQuery);
            QueryResponse response = getSolrServer().query(solrQuery);
            SolrDocumentList searchResultDocuments = response.getResults();
            List<Product> products = SolrSearchUtilities.convertToProductList(searchResultDocuments);
            if (!products.isEmpty()) {
                if (products.get(0) != null
                        && !"no".equals(products.get(0).getProperties().get(CatalogXItem.AVAILABILITY_PROPERTY))) {
                    product = products.get(0);
                }
            }
        }
        catch (SolrServerException e) {
            logger.error("Could not reach solr", e);
        }
        if (product == null || product.getId() == null) {
            throw new ProductNotFoundException();
        }
        return product;
    }

    @Override
    public Product getProductFromVendorNameAndApdSku(String vendorName, String apdSku) {
        Product product = new Product();
        try {
            SolrQuery solrQuery = new SolrQuery();
            solrQuery.set("q", APD_SKU_FIELD_S + ":\"" + apdSku + "\"");
            solrQuery.addFilterQuery(VENDOR_NAME_FIELD + ":\"" + vendorName + "\"");
            QueryResponse response = getSolrServer().query(solrQuery);
            SolrDocumentList searchResultDocuments = response.getResults();
            List<Product> products = SolrSearchUtilities.convertToProductList(searchResultDocuments);
            if (!products.isEmpty()) {
                product = products.get(0);
            }
        }
        catch (SolrServerException e) {
            logger.error("Could not reach solr", e);
        }
        return product;
    }

    @Override
    public Product getProductFromCatalogXItem(CatalogXItem catXItem) {
        if (catXItem == null) {
            return null;
        }
        Product product = null;
        SolrQuery solrQuery = new SolrQuery();
        solrQuery = solrQueryUtilities.addFilterByCatalogID(solrQuery);
        try {
            solrQuery.set("q", ID_FIELD + ":" + catXItem.getId());
            QueryResponse response = getSolrServer().query(solrQuery);
            SolrDocumentList searchResultDocuments = response.getResults();
            List<Product> products = SolrSearchUtilities.convertToProductList(searchResultDocuments);
            if (!products.isEmpty()) {
                product = products.get(0);
            }
            if (EcommercePropertiesLoader.getInstance().getEcommerceProperties().getBoolean("isDevelopmentMode")) {
                //Will be reached when solr index is not in sync with local development db instance
                logger
                        .error("This code should not execute in a production environment.  It is for development testing only.");
                product = productLookup.getProduct(catXItem);
            }
        }
        catch (SolrServerException ex) {
            logger.error("Could not reach solr", ex);
        }
        if (product == null) {
            try {
                product = productLookup.getProduct(catXItem);
            }
            catch (Exception e) {
                logger.debug("Error converting catalogXItem to product");
                logger.trace("Error: ", e);
            }
        }
        return product;
    }

    @Override
    public ResponseData query(QueryData query) {
        try {
            if (StringUtils.isNotBlank(query.getFavoritesName()) && StringUtils.isNotBlank(query.getFavoritesType())
                    && query.getFavoritesType().equals(FavoritesViewBean.PERSONAL_FAVORITES_TYPE)) {
                //calling favorites bean to perform query for user favorites
                //company favorites are indexed, so that's handled with the solr query
                return favoritesViewBean.query(query);
            }
            SolrQuery solrQuery = queryDataToSolrQuery(query);
            if (logger.isDebugEnabled()) {
                logger.debug(solrQuery.toString());
            }
            QueryResponse response = getSolrServer().query(solrQuery);
            if (response.getResults() == null) {
                return new ResponseData();
            }
            ResponseData responseData = solrQueryResponseToResponseData(response, query.getSearchEngineHierarchyKey());
            QueryResponse fearuredProducts = getFeaturedProducts(query, response);
            responseData.setFeaturedProducts(SolrSearchUtilities.convertToProductList(fearuredProducts.getResults()));
            return responseData;
        }
        catch (SolrServerException e) {
            logger.error("Could not reach solr", e);
            return new ResponseData();
        }
    }

    private QueryResponse getFeaturedProducts(QueryData queryData, QueryResponse responseData) {
        SolrQuery solrQuery = new SolrQuery();
        String hierarchy = null;
        solrQuery = solrQueryUtilities.addFilterByCatalogID(solrQuery);
        solrQuery = solrQueryUtilities.addAvailableOnlyFilter(solrQuery);
        solrQuery.setStart(0);
        solrQuery.setRows(HomeBean.PAGE_SIZE.toInt());
        if (StringUtils.isNotBlank(queryData.getSearchEngineHierarchyKey())) {
            hierarchy = queryData.getSearchEngineHierarchyKey();
        }
        else if (StringUtils.isNotBlank(queryData.getSearchString())) {
            for (SolrDocument doc : responseData.getResults()) {
                hierarchy = (String) doc.getFieldValue(HIERARCHY_PATH_FACET_VALUE);
                if (StringUtils.isNotBlank(hierarchy)) {
                    if (StringUtils.countMatches(hierarchy, "!") > 2) {
                        hierarchy = hierarchy.substring(0, hierarchy.lastIndexOf("!"));
                    }
                    break;
                }
            }
        }
        else {
            hierarchy = "!" + SearchBean.DEFAULT_CATEGORY;
        }
        solrQuery.set("q", "isFeaturedItem:1");
        solrQuery.addFilterQuery(HIERARCHY_PATH_FACET_VALUE + ":" + "/"
                + SolrSearchUtilities.removeRegexCharacters(hierarchy) + HIERARCHY_REGEX_ARG + "/");
        solrQuery.addSort("random_" + 0, SolrQuery.ORDER.asc);

        try {
            return getSolrServer().query(solrQuery);
        }
        catch (SolrServerException e) {
            logger.error("Error attempting to Query Solr Index Server", e);
        }
        return null;
    }

    private SolrQuery queryDataToSolrQuery(QueryData queryData) {
        SolrQuery solrQuery = new SolrQuery();

        ModifiableSolrParams params = new ModifiableSolrParams();
        params.set("facet", true);
        params.set("facet.mincount", 1);

        if (queryData.getStart() > 0) {
            params.set("start", queryData.getStart());
        }

        //the following conditional block sets the "q" parameter and the search handler
        String matchbookIdField = queryData.getMatchbookId();
        if (queryData.isMatchbookSearch() && StringUtils.isNotBlank(matchbookIdField)) {
            //matchbook search
            solrQuery.setRequestHandler(INK_TONER_SEARCH_REQUEST_HANDLER);
            //"q" is set to the search string for matchbook searching
            if (matchbookIdField.contains(",")) {
                List<String> matchbookIds = new ArrayList<String>(Arrays.asList(matchbookIdField.split(",")));
                String queryString = MB_ID_FIELD + ":" + matchbookIds.get(0);
                matchbookIds.remove(0);
                while (!matchbookIds.isEmpty()) {
                    queryString = queryString + " OR " + MB_ID_FIELD + ":" + matchbookIds.get(0);
                    matchbookIds.remove(0);
                }
                params.set("q", queryString);
            }
            else {
                params.set("q", MB_ID_FIELD + ":" + matchbookIdField);
            }
        }
        else {
            //otherwise, normal search
            solrQuery.setRequestHandler(SEARCH_REQUEST_HANDLER);
            if (StringUtils.isNotBlank(queryData.getSearchString())) {
                params.set("q", SolrSearchUtilities.makeSolrSafeAlphaNumeric(queryData.getSearchString()));
            }
            //if the search string is blank, the "q" field is not set, and the user is navigating by filtering
        }

        solrQuery.add(params);
        solrQuery.setRows(queryData.getRows());
        addSortOption(queryData, solrQuery);
        if (StringUtils.isNotBlank(queryData.getFavoritesId())) {
            solrQuery.addFilterQuery(COMPANY_FAVORITES_FIELD + ":" + queryData.getFavoritesId());
        }

        if (StringUtils.isNotBlank(queryData.getSearchEngineHierarchyKey())
                && !queryData.getSearchEngineHierarchyKey().equals(SolrSearcher.ALL_HIERARCHY_PATH)) {
            solrQuery.addFilterQuery(HIERARCHY_PATH_FACET_VALUE + ":" + "/"
                    + SolrSearchUtilities.removeRegexCharacters(queryData.getSearchEngineHierarchyKey())
                    + HIERARCHY_REGEX_ARG + "/");
        }
        addFacetFieldsToQuery(queryData, solrQuery);
        addFilterFieldsToQuery(queryData, solrQuery);
        if (StringUtils.isNotBlank(queryData.getCartridgeNumber())) {
            solrQuery
                    .addFilterQuery(CARTRIDGE_NUMBER_CI + ": \"" + queryData.getCartridgeNumber().toLowerCase() + "\"");
        }
        addManufacturerFilter(queryData, solrQuery);
        addPriceRangeFilter(queryData, solrQuery);
        addRefineSearchFilter(queryData, solrQuery);
        if (!queryData.isShowNonCore()) {
            this.addCoreFilterQuery(solrQuery);
        }
        addCoreItemBoost(queryData, solrQuery);
        addPhraseSlopBoost(queryData, solrQuery);
        addUnavailableItemsFilter(queryData, solrQuery);
        //Used to see which synonyms were found for category matching
        solrQuery.add("debugQuery", "on");
        addGreenFilter(queryData, solrQuery);
        solrQuery = solrQueryUtilities.addFilterByCatalogID(solrQuery);

        return solrQuery;
    }

    private void addSortOption(QueryData queryData, SolrQuery solrQuery) {
        switch (queryData.getSortOption()) {
            case SortByProductNameAsc:
                solrQuery.addSort(NAME_SORT_FIELD, ORDER.asc);
                break;
            case SortByProductNameDesc:
                solrQuery.addSort(NAME_SORT_FIELD, ORDER.desc);
                break;
            case SortBySkuAsc:
                switch (credentialSelectionBean.getCredentialSkuType()) {
                    case "customer":
                        solrQuery.addSort(CUSTOMER_SKU_FIELD, ORDER.asc);
                        break;
                    case "manufacturer":
                        solrQuery.addSort(MANUFACTURER_SKU_FIELD, ORDER.asc);
                        break;
                    case "vendor":
                        solrQuery.addSort(MANUFACTURER_SKU_FIELD, ORDER.asc);
                        break;
                    default:
                        solrQuery.addSort(APD_SKU_FIELD, ORDER.asc);
                        break;
                }
                break;
            case SortBySkuDesc:
                switch (credentialSelectionBean.getCredentialSkuType()) {
                    case "customer":
                        solrQuery.addSort(CUSTOMER_SKU_FIELD, ORDER.desc);
                        break;
                    case "manufacturer":
                        solrQuery.addSort(MANUFACTURER_SKU_FIELD, ORDER.desc);
                        break;
                    case "vendor":
                        solrQuery.addSort(MANUFACTURER_SKU_FIELD, ORDER.desc);
                        break;
                    default:
                        solrQuery.addSort(APD_SKU_FIELD, ORDER.desc);
                        break;
                }
                break;
            case SortByPriceAsc:
                solrQuery.addSort(SOLR_PRODUCT_PRICE_FIELD, ORDER.asc);
                break;
            case SortByPriceDesc:
                solrQuery.addSort(SOLR_PRODUCT_PRICE_FIELD, ORDER.desc);
                break;
            case SortByBrandAsc:
                solrQuery.addSort(SOLR_PRODUCT_BRAND_NAME_FIELD, ORDER.asc);
                break;
            case SortByBrandDesc:
                solrQuery.addSort(SOLR_PRODUCT_BRAND_NAME_FIELD, ORDER.desc);
                break;
            case MostPopular:
                solrQuery.addSort(SOLR_FIELD_MOST_POPULAR, ORDER.desc);
                break;
            case BestMatch:
            default:
                //do nothing
                break;
        }
    }

    private void addFacetFieldsToQuery(QueryData queryData, SolrQuery solrQuery) {
        solrQuery.addFacetField("filters");
        solrQuery.addFacetField("filtersPriority");
        solrQuery.addFacetField(SOLR_PRODUCT_BRAND_NAME_FIELD);
        solrQuery.addFacetField(HEIRARCHY_PATH_SOLR_COLUMN);
        //setting the limit to a high value, so that the category is likely to be in the 
        //list of available filters, so it can be auto-selected in checkIfCategoryMatches()
        //in SearchBean
        solrQuery.setParam("f." + HEIRARCHY_PATH_SOLR_COLUMN + ".facet.limit", "500");
        solrQuery.addFacetField(GREEN_SOLR_FIELD);
        if (StringUtils.isNotBlank(queryData.getSearchString())) {
            String searchString = SolrSearchUtilities.makeSolrSafe(queryData.getSearchString().toUpperCase());
            //the comment is a hack so that we can look up this facet query easily
            solrQuery.addFacetQuery("/* " + CROSSREF_KEY + searchString + "*/ crossReferenceSkus:\"" + searchString
                    + "\" -CUSTOMER_SKU_typeahead:\"" + searchString + "\"");
        }
    }

    private void addFilterFieldsToQuery(QueryData queryData, SolrQuery solrQuery) {
        if (StringUtils.isNotBlank(queryData.getActiveFiltersString())) {
            separateFilterFacets(queryData.getActiveFiltersString(), queryData);
            Map<String, Set<String>> filterMap = parseFilters(queryData.getActiveFilters());
            for (String filterCategory : filterMap.keySet()) {
                //tag the query constructed for this category for reference in exclusion below
                String filterQueryString = "{!tag=" + SolrSearchUtilities.getTagName(filterCategory) + "}(";
                for (String filterCategoryChild : filterMap.get(filterCategory)) {
                    filterQueryString += "filters:\""
                            + SolrSearchUtilities.makeSolrSafe(QueryData.getFilterString(filterCategory,
                                    filterCategoryChild)) + "\"" + " OR ";
                }
                filterQueryString = filterQueryString.substring(0, filterQueryString.length() - 4) + ")"; //remove the last " OR " and close parentheses
                solrQuery.addFilterQuery(filterQueryString);
                //add facet field excluding the filters from this category for later reference to get filter counts for filters in the same category
                solrQuery.addFacetField("{!key=" + SolrSearchUtilities.getKeyName(filterCategory) + " ex="
                        + SolrSearchUtilities.getTagName(filterCategory) + "}filters");
            }
        }
    }

    private void separateFilterFacets(String paramString, QueryData queryData) {
        String[] filters = paramString.split(DELIMITER_SPLITTER);
        queryData.getActiveFilters().addAll(Arrays.asList(filters));
    }

    /**
     * Parses filters from a set of strings containing filter category information into a map of filter categories mapped onto filters in that category
     * @param filters
     * @return 
     */
    private Map<String, Set<String>> parseFilters(Set<String> filters) {
        Map<String, Set<String>> toReturn = new HashMap<>();
        for (String f : filters) {
            String filterCategory = getFilterCategory(f);
            String filterCategoryChild = getFilterCategoryChild(f);
            if (toReturn.get(filterCategory) == null) {
                Set<String> newSet = new HashSet<>();
                newSet.add(filterCategoryChild);
                toReturn.put(filterCategory, newSet);
            }
            else {
                toReturn.get(filterCategory).add(filterCategoryChild);
            }
        }
        return toReturn;
    }

    public String getFilterCategory(String filterString) {
        return filterString.split(INNER_FILTER_DELIMITER_SPLITTER)[0];
    }

    public String getFilterCategoryChild(String filterString) {
        return filterString.split(INNER_FILTER_DELIMITER_SPLITTER)[1];
    }

    private void addManufacturerFilter(QueryData queryData, SolrQuery solrQuery) {
        if (StringUtils.isNotBlank(queryData.getSelectedManufacturer())) {
            solrQuery.addFilterQuery("{!raw f=brandName}" + queryData.getSelectedManufacturer());
        }
    }

    private void addPriceRangeFilter(QueryData queryData, SolrQuery solrQuery) {
        if (queryData.getLowPrice() != -1 && queryData.getHighPrice() != -1) {
            solrQuery.addFilterQuery("price:[" + queryData.getLowPrice() + " TO " + queryData.getHighPrice() + "]");
        }
    }

    private void addRefineSearchFilter(QueryData queryData, SolrQuery solrQuery) {
        if (queryData.getRefineSearchStrings() != null) {
            for (String refineSearchString : queryData.getRefineSearchStrings()) {
                String refineSearchFilter = "(" + APD_SKU_FIELD + ":"
                        + SolrSearchUtilities.makeSolrSafe(refineSearchString).trim();
                refineSearchFilter += SOLR_OR_DELIMETER + CUSTOMER_SKU_FIELD + ":"
                        + SolrSearchUtilities.makeSolrSafe(refineSearchString).trim();

                refineSearchFilter += SOLR_OR_DELIMETER + VENDOR_SKU_FIELD + ":"
                        + SolrSearchUtilities.makeSolrSafe(refineSearchString).trim();
                refineSearchFilter += SOLR_OR_DELIMETER + SOLR_PRODUCT_NAME_FIELD + ":"
                        + SolrSearchUtilities.makeSolrSafe(refineSearchString).trim();

                refineSearchFilter += SOLR_OR_DELIMETER + MANUFACTURER_SKU_FIELD + ":"
                        + SolrSearchUtilities.makeSolrSafe(refineSearchString).trim();

                refineSearchFilter += SOLR_OR_DELIMETER + "searchterms:"
                        + SolrSearchUtilities.makeSolrSafe(refineSearchString).trim();

                refineSearchFilter += SOLR_OR_DELIMETER + "protectedsearchterms:"
                        + SolrSearchUtilities.makeSolrSafe(refineSearchString).trim();

                refineSearchFilter += ")";
                solrQuery.addFilterQuery(refineSearchFilter);
            }
        }
    }

    private void addCoreItemBoost(QueryData queryData, SolrQuery solrQuery) {
        solrQuery.add("bf", this.getCoreItemBoostSolrFunction());
    }

    private String getCoreItemBoostSolrFunction() {
        String boostFactor = PropertiesLoader.getAsProperties("solr.properties").getProperty("core.item.boost");
        //note: by this query, only core item start date is checked when determining whether to boost. This follows 
        //business requirements, where items that have been expired from a core list should still be boosted.
        String toReturn = "map(if(" + CORE_ITEM_START_DATE_FIELD + ",div(abs(ms(NOW," + CORE_ITEM_START_DATE_FIELD
                + ")),ms(NOW,coreitemstartdate)),-1),0,1," + boostFactor + ",1)";
        return toReturn;
    }

    private void addPhraseSlopBoost(QueryData queryData, SolrQuery solrQuery) {
        String phraseFields = PropertiesLoader.getAsProperties("solr.properties").getProperty("phrase.fields");
        for (String field : StringUtils.split(phraseFields, ",")) {
            solrQuery.add("pf", field);
        }
        String phraseSlop = PropertiesLoader.getAsProperties("solr.properties").getProperty("phrase.slop");
        solrQuery.add("ps", phraseSlop);
    }

    private void addUnavailableItemsFilter(QueryData queryData, SolrQuery solrQuery) {
        solrQuery.addFilterQuery(UNAVAILABLE_ITEMS_FILTER);
    }

    private ResponseData solrQueryResponseToResponseData(QueryResponse response, String hierarchyKey) {
        ResponseData toReturn = new ResponseData();
        //sets products on response data
        toReturn.setProducts(SolrSearchUtilities.convertToProductList(response.getResults()));
        //sets numFound
        toReturn.setNumFound((int) response.getResults().getNumFound());
        //sets hierarchy path
        toReturn.setMatchingHierarchies(new ArrayList<SearchHierarchy>());
        FacetField hierarchyField = response.getFacetField(HEIRARCHY_PATH_SOLR_COLUMN);
        if (hierarchyField != null && hierarchyField.getValues() != null) {
            HierarchyTree hierarchyTrees = new HierarchyTree();
            FilterKey selectedHierarchy = null;
            for (FacetField.Count c : hierarchyField.getValues()) {
                if (c != null && StringUtils.isNotBlank(c.getName())) {
                    toReturn.getMatchingHierarchies().add(
                            new SearchHierarchy(
                                    c.getName().substring(c.getName().lastIndexOf(HIERARCHY_DELIMITER) + 1), c
                                            .getName()));
                    //if there is no category specified in search parameters, include all hierarchies to filter
                    boolean addHierarchy = false;
                    if (hierarchyKey != null) {
                        //otherwise, include hierarchies if they are direct children of the search hierarchy
                        if (!addHierarchy && c.getName().startsWith(hierarchyKey)
                                && c.getName().length() >= hierarchyKey.length() + HIERARCHY_DELIMITER.length()) {
                            //if the name of this hierarchy after the search hierarchy contains the delimeter, then 
                            //this hierarchy is not a direct child, but a grandchild
                            addHierarchy = !c.getName().substring(hierarchyKey.length() + HIERARCHY_DELIMITER.length())
                                    .contains(HIERARCHY_DELIMITER);
                        }
                    }
                    else {
                        //For cases where there is no selected hierarchy, we will need to construct a hierarchy map and determine the lowest common root hierarchy and only include that level
                        hierarchyTrees = insertIntoHierarchyTrees(c, hierarchyTrees);
                    }
                    if (addHierarchy) {

                        FilterKey filterKey = new FilterKey(c.getName(), c.getName().substring(
                                c.getName().lastIndexOf(HIERARCHY_DELIMITER) + 1));
                        toReturn.getAvaliableCategoryFilters().put(filterKey, (int) c.getCount());
                        if (filterKey.getId().equals(hierarchyKey)) {
                            selectedHierarchy = filterKey;
                        }
                    }
                }
            }
            //traverse down the tree of hierarchyNodes until there is a branch
            while (hierarchyTrees.children.keySet().size() == 1) {
                hierarchyTrees = hierarchyTrees.children.get(hierarchyTrees.children.keySet().iterator().next());
            }
            for (String key : hierarchyTrees.children.keySet()) {
                HierarchyTree hierarchyTreeChild = hierarchyTrees.children.get(key);
                FilterKey filterKey = new FilterKey(hierarchyTreeChild.fullName, hierarchyTreeChild.value);
                toReturn.getAvaliableCategoryFilters().put(filterKey, (int) hierarchyTreeChild.count);
                if (filterKey.getId() != null && filterKey.getId().equals(hierarchyKey)) {
                    selectedHierarchy = filterKey;
                }
            }
            //Only include selected filter if it's the only one
            if (selectedHierarchy != null && toReturn.getAvaliableCategoryFilters().size() > 1) {
                toReturn.getAvaliableCategoryFilters().remove(selectedHierarchy);
            }
        }
        if (StringUtils.isNotBlank(hierarchyKey)) {
            toReturn.setBreadcrumbs(new ArrayList<SearchHierarchy>());
            String[] hierarchyArray = hierarchyKey.substring(1).split(HIERARCHY_DELIMITER);
            String pathSoFar = "";
            for (String hierarchy : hierarchyArray) {
                pathSoFar += HIERARCHY_DELIMITER + hierarchy;
                toReturn.getBreadcrumbs().add(new SearchHierarchy(hierarchy, pathSoFar));
            }
        }
        //sets filter facets
        toReturn.setFilters(SolrSearchUtilities.parseFilters(response));
        //sets manufacturer facets
        toReturn.setManufacturers(SolrSearchUtilities.parseManufacturerFacets(response));

        toReturn.setGreenFilter(SolrSearchUtilities.parseGreenFilter(response));
        //sets the available sort options
        for (SortOption option : SOLR_SORT_OPTIONS) {
            toReturn.getSortOptions().add(option);
        }
        //sets the cross reference text
        if (response != null && response.getFacetQuery() != null && !response.getFacetQuery().isEmpty()) {
            for (String s : response.getFacetQuery().keySet()) {
                if (s.contains(CROSSREF_KEY) && response.getFacetQuery().get(s) != null
                        && response.getFacetQuery().get(s) > 0) {
                    String searchString = s.substring(s.indexOf(CROSSREF_KEY) + CROSSREF_KEY.length(), s.length());
                    searchString = searchString.substring(0, searchString.indexOf("*/"));
                    toReturn.setCrossReference("Item " + searchString
                            + " was cross-referenced to our Catalog. Recommended products are displayed below.");
                }
            }
        }
        List<String> synonyms = (List<String>) response.getDebugMap().get(EXPANDED_SYNONYMS_SOLR_FIELD_NAME);
        if (synonyms != null && !synonyms.isEmpty()) {
            toReturn.setSynonyms(synonyms);
        }
        return toReturn;
    }

    private HierarchyTree insertIntoHierarchyTrees(Count hierarchyCount, HierarchyTree hierarchyTree) {
        HierarchyTree root = hierarchyTree;
        String name = hierarchyCount.getName();
        while (!StringUtils.isEmpty(name)) {
            int indexOfDelimeter = name.indexOf(HIERARCHY_DELIMITER);
            String hierarchyName;
            if (indexOfDelimeter >= 0) {
                hierarchyName = name.substring(0, indexOfDelimeter);
                name = name.substring(indexOfDelimeter + 1);
            }
            else {
                hierarchyName = name;
                name = "";
            }
            if (root.children.get(hierarchyName) != null) {
                root = root.children.get(hierarchyName);
            }
            else {
                HierarchyTree newTree = new HierarchyTree();
                newTree.value = hierarchyName;
                root.children.put(hierarchyName, newTree);
                root = newTree;
            }

        }
        root.count = hierarchyCount.getCount();
        root.fullName = hierarchyCount.getName();
        return hierarchyTree;
    }

    @Override
    public String getSearchTermsJson() {
        return "'" + linksBean.getLookahead()
                + this.credentialSelectionBean.getCurrentCredential().getCatalog().getId() + "/'";
    }

    /*
     * These methods are not implemented for SmartSearch as well, because they are called from the typeahead rest endpoint which 
     * cannot use the standard producer to pick between search engines as it will not have a credential when it is instantiated.
     */

    public List<String> getSearchSuggestions(String query, String selectedPath) {
        if (logger.isDebugEnabled()) {
            logger.debug("Typeahead request heard.");
        }
        List<String> returnList = new ArrayList<String>();
        returnList = getFacetNames(returnList, SolrSearcher.HIERARCHY_PATH_FACET_VALUE, query, selectedPath, true,
                false);
        if (returnList.size() < 10) {
            returnList = getFacetNames(returnList, "globalProductType", query);
        }
        if (returnList.size() < 10) {
            returnList = getFacetNames(returnList, SOLR_BRAND_NAME, query);
        }
        return returnList;
    }

    public List<String> getSuggestionsByCartridgeNumber(String query) {
        List<String> returnList = new ArrayList<String>();
        returnList = getFacetNames(returnList, SolrSearcher.CARTRIDGE_NUMBER_FACET_VALUE, query);
        return returnList;
    }

    public List<String> getSuggestionsByModelNumber(String query) {
        List<String> returnList = new ArrayList<String>();
        returnList = getFacetNames(returnList, SolrSearcher.MODEL_NUMBER_FACET_VALUE, query, null, false, true);
        return returnList;
    }

    private List<String> getFacetNames(List<String> returnList, String fieldName, String searchString) {
        return getFacetNames(returnList, fieldName, searchString, null, false, false);
    }

    private List<String> getFacetNames(List<String> returnList, String fieldName, String searchString,
            String selectedPath, boolean hierarchy, boolean multivalued) {
        String dynamicFieldExtention = "_typeahead";
        if (multivalued) {
            dynamicFieldExtention = "_typeaheadMulti";
        }
        SolrQuery solrQuery = new SolrQuery();
        //solrQuery = solrQuery.setRequestHandler("TermsSearchHandler");
        String facetField = fieldName;
        if (fieldName.equals(SolrSearcher.HIERARCHY_PATH_FACET_VALUE)) {
            facetField = HIERARCHY_PATH_FACET_VALUE_S;
        }
        solrQuery.addFacetField(facetField);
        //Don't actually need any item data, we're just looking to see if any items exist in the catalog
        solrQuery.setRows(1);
        solrQuery = solrQuery.addFilterQuery(SolrSearcher.UNAVAILABLE_ITEMS_FILTER);
        solrQuery = solrQueryUtilities.addFilterByCatalogID(solrQuery);
        ModifiableSolrParams params = new ModifiableSolrParams();
        params.set("facet", true);
        params.set("facet.mincount", 1);
        //Use _typeahead field, because need to be case insensitive 
        searchString = searchString.toLowerCase();
        params.set("q", fieldName + dynamicFieldExtention + ":"
                + (hierarchy ? ClientUtils.escapeQueryChars(SolrSearcher.HIERARCHY_DELIMITER) + "*" : "")
                + ClientUtils.escapeQueryChars(searchString) + "*");
        if (hierarchy) {
            //build query by matching on a regular expression 
            StringBuffer sb = new StringBuffer();
            //first, add the filter query parameter
            sb.append(fieldName + "_typeahead:");
            //add the leading character for Solr regex
            sb.append("/");
            //add the currently selected category path
            sb.append(ClientUtils.escapeQueryChars(selectedPath));
            //add a hierarchy delimeter
            sb.append(ClientUtils.escapeQueryChars(SolrSearcher.HIERARCHY_DELIMITER));
            //add a wildcard
            sb.append(".*");
            //add the search string
            //split around non-alphanumeric characters
            String[] searchStringAlphaNumeric = searchString.split("[^a-zA-Z0-9]+");
            for (String searchStringPiece : searchStringAlphaNumeric) {
                //then, add the alphanumeric piece of the search string
                sb.append(ClientUtils.escapeQueryChars(searchStringPiece));
                //add a wildcard that does not include the hierarchy delimeter, 
                //so that only child categories are matched
                sb.append("[^" + ClientUtils.escapeQueryChars(SolrSearcher.HIERARCHY_DELIMITER) + "]*");
            }
            //finally, end with the ending character for Solr regex
            sb.append("/");
            //then, add the parameter
            solrQuery.addFilterQuery(sb.toString());
        }
        solrQuery.add(params);
        if (logger.isDebugEnabled()) {
            logger.debug(solrQuery.toString());
        }
        QueryResponse response;
        try {
            response = getSolrServer().query(solrQuery);
            FacetField facet = response.getFacetField(facetField);
            if (facet != null) {
                for (Count value : facet.getValues()) {
                    if (returnList.size() > MAX_NUMBER_OF_SUGGESTIONS_NEEDED) {
                        break;
                    }
                    String name = value.getName();
                    if (fieldName.equals(SolrSearcher.HIERARCHY_PATH_FACET_VALUE)) {
                        /* The String will be a hierarchy eg: !eggs!chicken!gradeALarge the typeahead 
                         * only needs the last element. */
                        name = name.substring(name.lastIndexOf(SolrSearcher.HIERARCHY_DELIMITER) + 1);
                        returnList.add(name);
                    }
                    else {
                        returnList.add(name.trim());
                    }
                }
            }
            else {
                if (logger.isDebugEnabled()) {
                    logger.debug("No facet found  for " + fieldName + ".");
                }
            }
        }
        catch (SolrServerException e) {
            logger.error("Could not connect to solr server for typeahead.");
        }
        return returnList;
    }

    @Override
	public Map<String, List<Product>> getRelatedItems(Product product) {
		CatalogXItem item = this.catalogXItemBp.findById(product.getId(), CatalogXItem.class);
		
		Map<CatalogXItem, String> relativeList = catalogXItemBp.getCatalogXItemsSimilarItems(item);
		Map<String, List<Product>> toReturn = new HashMap<>();		
		for (Map.Entry<CatalogXItem, String> entry : relativeList.entrySet()) {
			Product productRelative = this.getProductFromCatalogXItem(entry.getKey());
			if(productRelative == null) {
				continue;
			}
			
			String relationship = "";
			switch (entry.getValue()) {
			
			case "Companion For/Companion"   : 	relationship = "COMPANION ITEMS";
					                		 	break;
			case "Accessory For/Accessories" : 	relationship = "ACCESSORIES";
                                             	break;                                             	
			case "Downsell/Upsell" 			 : 	relationship = "UPSELL";
         										break;
			default 			  			 :  relationship = StringUtils.upperCase(entry.getValue());
												break;
					                
			}
					                
			if (!toReturn.containsKey(relationship)) {
				toReturn.put(relationship, new ArrayList<Product>());
			}
			toReturn.get(relationship).add(productRelative);
			
		}		
		return toReturn;
	}

    @Override
    public Set<Matchbook> getMatchbooksByCatalogAndManufacturer(List<Long> catalogIds, Manufacturer manufacturer) {
        return matchbookBp.searchByCatalogAndManufacturer(catalogIds, manufacturer);
    }

    @Override
    public Collection<Manufacturer> matchbookManufacturersFromCatalog(List<Long> catalogIds) {
        return matchbookBp.matchbookManufacturersFromCatalog(catalogIds);
    }

    @Override
    public String inkAndTonerSearch(Matchbook selectedModel, Manufacturer selectedManufacturer) {
        Long matchbookId = matchbookBp.searchByModelAndManufacturer(selectedModel.getModel(), selectedManufacturer)
                .get(0).getId();
        return SearchUtilities.SEARCH_PAGE_BASE_URL + "?" + SearchUtilities.MB_ID_PARAM + "=" + matchbookId + "&"
                + SearchUtilities.INK_TONER_MODEL + "=" + selectedModel.getModel() + "&"
                + SearchUtilities.INK_TONER_MANUFACTURER + "=" + selectedManufacturer;
    }

    private void addCoreFilterQuery(SolrQuery solrQuery) {
        solrQuery.addFilterQuery("(-coreitemstartdate:[* TO *] AND *:*) OR coreitemstartdate:[* TO NOW]");
        solrQuery.addFilterQuery("(-coreitemexpirationdate:[* TO *] AND *:*) OR coreitemexpirationdate:[NOW TO *]");
        solrQuery.addFilterQuery("coreitemstartdate:[* TO *] OR coreitemexpirationdate:[* TO *]");

    }

    @Override
    public String searchByCartridgeNumber(String cartridgeNumber) {
        return SearchUtilities.SEARCH_PAGE_BASE_URL + "?" + SearchUtilities.INK_TONER_CARTRIDGE_NUMBER + "="
                + cartridgeNumber;
    }

    @Override
    public boolean isSearchByCartridgeSupported() {
        return true;
    }

    @Override
    public boolean isSearchEngineAvailable() {
        return true;
    }

    @Override
    public boolean usesActiveFilters() {
        return false;
    }

    @Override
    public SearchType getSearchType() {
        return SearchType.SOLR_SEARCH;
    }

    @Override
    public String searchByModelNumber(String modelNumber) throws InvalidModelNumberException {
        List<Matchbook> matchbooks = matchbookBp.searchByModel(modelNumber, credentialSelectionBean
                .getCurrentCredential().getCatalog().getId());
        if (matchbooks.isEmpty()) {
            throw new InvalidModelNumberException();
        }
        String matchbookIdParam = matchbooks.get(0).getId().toString();
        matchbooks.remove(0);
        while (!matchbooks.isEmpty()) {
            matchbookIdParam = matchbookIdParam + "," + matchbooks.get(0).getId();
            matchbooks.remove(0);
        }
        return SearchUtilities.SEARCH_PAGE_BASE_URL + "?" + SearchUtilities.MB_ID_PARAM + "="
                + StringEscape.escapeForUrl(matchbookIdParam);

    }

    @Override
    public boolean isSearchByModelSupported() {
        return true;
    }

    @Override
    public String getCartridgeLookahead() {
        return linksBean.getCartridgeLookahead()
                + this.credentialSelectionBean.getCurrentCredential().getCatalog().getId() + "/";
    }

    @Override
    public String getModelLookahead() {
        return linksBean.getSolrModelLookahead()
                + this.credentialSelectionBean.getCurrentCredential().getCatalog().getId() + "/";
    }

    public class HierarchyTree {

        String value;
        String fullName;
        long count = 0;
        Map<String, HierarchyTree> children = new HashMap<String, HierarchyTree>();
    }

    private void addGreenFilter(QueryData queryData, SolrQuery solrQuery) {
        if (StringUtils.isNotBlank(queryData.getGreenFilterId()) && YES.equals(queryData.getGreenFilterId())) {
            solrQuery.addFilterQuery("{!raw f=green}" + YES);
        }
    }
}
