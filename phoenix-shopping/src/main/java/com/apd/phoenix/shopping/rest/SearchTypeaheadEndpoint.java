package com.apd.phoenix.shopping.rest;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.apache.cxf.jaxrs.client.JAXRSClientFactory;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.shopping.rest.client.SmartSearchTypeaheadInterface;
import com.apd.phoenix.shopping.rest.json.Bucket;
import com.apd.phoenix.shopping.rest.json.Sug;
import com.apd.phoenix.shopping.rest.json.Sugs;
import com.apd.phoenix.shopping.rest.json.TypeaheadResponse;
import com.apd.phoenix.shopping.view.jsf.bean.search.SolrSearch;
import com.apd.phoenix.shopping.view.jsf.bean.search.SolrSearchEngine;
import com.apd.phoenix.shopping.view.jsf.bean.search.SolrSearcher;
import com.apd.smartsearch.model.utilities.SmartSearchPropertiesLoader;
import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;
import com.google.gson.Gson;

@Stateless
@Path("/typeahead")
public class SearchTypeaheadEndpoint {

    private static final Logger LOG = LoggerFactory.getLogger(SearchTypeaheadEndpoint.class);

    private static final String ACCEPT = "*/*";

    //uses the specific solr search engine since not in shopping
    @Inject
    @SolrSearch
    SolrSearchEngine solrSearchEngine;

    @GET
    @Path("/mainSearch/{catalogId}/{query}/{selectedPath}/{sessionId}")
    @Produces("application/json")
    public Response mainSearch(@PathParam("query") String query, @PathParam("selectedPath") String selectedPath, @PathParam("catalogId") Long catalogId) {
    	if (selectedPath == null || selectedPath.equals(SolrSearcher.ALL_HIERARCHY_PATH)) {
    		selectedPath = "";
    	}
		List<String> jsonList = new ArrayList<>();
		try {
			query = URLDecoder.decode(query,"UTF-8");
			selectedPath = URLDecoder.decode(selectedPath,"UTF-8");
		} catch (UnsupportedEncodingException e) {
			LOG.error(e.getLocalizedMessage());
		}
        for (String listName : solrSearchEngine.getSearchSuggestions(query, selectedPath)) {
        	jsonList.add(listName);
        }
        String jsonResponse =  (new Gson()).toJson(jsonList);
        return Response.ok(jsonResponse, MediaType.APPLICATION_JSON).build();
    }

    @GET
    @Path("/smartSearch/{query}/{selectedPath}/{sessionId}")
    @Produces("application/json")
    public Response smartSearch(@PathParam("query") String query, @PathParam("selectedPath") String selectedPath,
            @PathParam("sessionId") String sessionId) {
        SmartSearchTypeaheadInterface smartSearchInterface = setupSmartSearchTypaheadInterface();
        TypeaheadResponse response;
        try {
            response = smartSearchInterface.getSmartSearchSuggestions(sessionId, URLEncoder.encode(query, "UTF-8"),
                    "Standard", ACCEPT);
            String jsonResponse = convertSmartSearchResponseToSimpleJSONList(response);
            return Response.ok(jsonResponse, MediaType.APPLICATION_JSON).build();
        }
        catch (UnsupportedEncodingException e) {
            LOG.error("Exception building typeahead request", e);
            return Response.ok().build();
        }
    }

    private String convertSmartSearchResponseToSimpleJSONList(
			TypeaheadResponse response) {
		Sugs sugs = response.getSugs();
    	List<String> suggestions = new ArrayList<>();
    	List<String> categorySuggestions = new ArrayList<>();
    	for(Bucket bucket: sugs.getBuckets()){
    		//Empty Header means primary suggestions
    		if(StringUtils.isEmpty(bucket.getHeader())){
    			for(Sug sug:bucket.getSugs()){
    				suggestions.add(sug.getVal());
    			}
    		}
    		if("Related Categories".equals(bucket.getHeader())){
    			for(Sug sug:bucket.getSugs()){
    				categorySuggestions.add(sug.getVal());
    			}
    		}
    	}
    	List<String> jsonList = new ArrayList<>();
    	jsonList.addAll(suggestions);
    	if(!categorySuggestions.isEmpty()){
    		jsonList.addAll(categorySuggestions);
    	}
    	String jsonResponse =  (new Gson()).toJson(jsonList);
		return jsonResponse;
	}

    private SmartSearchTypeaheadInterface setupSmartSearchTypaheadInterface() {
		ArrayList<JacksonJsonProvider> providerList = new ArrayList<>();
    	
    	JacksonJsonProvider provider = new JacksonJsonProvider();
    	provider.addUntouchable(Response.class);
		providerList.add(provider);         
    	         
    	SmartSearchTypeaheadInterface smartSearchInterface = JAXRSClientFactory.create(SmartSearchPropertiesLoader.getInstance().getProperties().getString("typeaheadUrl"), SmartSearchTypeaheadInterface.class, providerList);
		return smartSearchInterface;
	}

    @GET
    @Path("/smartSearch/cartridge/{query}/{sessionId}")
    @Produces("application/json")
    public Response smartSearchCartridge(@PathParam("query") String query, @PathParam("sessionId") String sessionId) {
        LOG.info("Hit Cartridge Typeahead endpoint.");
        SmartSearchTypeaheadInterface smartSearchInterface = setupSmartSearchTypaheadInterface();
        TypeaheadResponse response;
        try {
            response = smartSearchInterface.getSmartSearchSuggestions(sessionId, URLEncoder.encode(query, "UTF-8"),
                    "ImageSupplies", ACCEPT);
            String jsonResponse = convertSmartSearchResponseToSimpleJSONList(response);
            return Response.ok(jsonResponse, MediaType.APPLICATION_JSON).build();
        }
        catch (UnsupportedEncodingException e) {
            LOG.error("Exception building typeahead request", e);
            return Response.ok().build();
        }
    }

    @GET
    @Path("/cartridgeSearch/{catalogId}/{query}/{sessionId}")
    @Produces("application/json")
    public Response cartridgeSearch(@PathParam("query") String query, @PathParam("catalogId") Long catalogId) {
		List<String> jsonList = new ArrayList<>();
		try {
			query = URLDecoder.decode(query,"UTF-8");
		} catch (UnsupportedEncodingException e) {
			LOG.error(e.getLocalizedMessage());
		}
        for (String listName : solrSearchEngine.getSuggestionsByCartridgeNumber(query)) {
        	jsonList.add(listName);
        }
        String jsonResponse =  (new Gson()).toJson(jsonList);
        return Response.ok(jsonResponse, MediaType.APPLICATION_JSON).build();
    }

    @GET
    @Path("/smartSearch/model/{query}/{sessionId}")
    @Produces("application/json")
    public Response smartSearchModel(@PathParam("query") String query, @PathParam("sessionId") String sessionId) {
        LOG.info("Hit Cartridge Typeahead endpoint.");
        SmartSearchTypeaheadInterface smartSearchInterface = setupSmartSearchTypaheadInterface();
        TypeaheadResponse response;
        try {
            response = smartSearchInterface.getSmartSearchSuggestions(sessionId, URLEncoder.encode(query, "UTF-8"),
                    "SuppliesFinderModel", ACCEPT);
            String jsonResponse = convertSmartSearchResponseToSimpleJSONList(response);
            return Response.ok(jsonResponse, MediaType.APPLICATION_JSON).build();
        }
        catch (UnsupportedEncodingException e) {
            LOG.error("Exception building typeahead request", e);
            return Response.ok().build();
        }
    }

    @GET
    @Path("/modelSearch/{catalogId}/{query}/{sessionId}")
    @Produces("application/json")
    public Response solrModelSearch(@PathParam("query") String query, @PathParam("catalogId") Long catalogId) {
		List<String> jsonList = new ArrayList<>();
		try {
			query = URLDecoder.decode(query,"UTF-8");
		} catch (UnsupportedEncodingException e) {
			LOG.error(e.getLocalizedMessage());
		}
        for (String listName : solrSearchEngine.getSuggestionsByModelNumber(query)) {
        	jsonList.add(listName);
        }
        String jsonResponse =  (new Gson()).toJson(jsonList);
        return Response.ok(jsonResponse, MediaType.APPLICATION_JSON).build();
    }
}