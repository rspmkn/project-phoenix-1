package com.apd.phoenix.shopping.view.jsf.bean.utils;

import java.math.BigDecimal;
import java.math.BigInteger;

public class AttributeFilterType {

    private String name;
    private BigDecimal priority;
    private BigInteger sequence;

    public AttributeFilterType(String filterDescription, BigInteger sequenceInput) {
        this.name = filterDescription;
        this.sequence = sequenceInput;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigInteger getSequence() {
        return sequence;
    }

    public void setSequence(BigInteger sequence) {
        this.sequence = sequence;
    }

    //Since this is just a wrapper to include the sequence with the name, only the name is used for equality and hashing
    @Override
    public boolean equals(Object o) {
        if (o == null) {
            return false;
        }
        if (o instanceof AttributeFilterType) {
            AttributeFilterType casted = (AttributeFilterType) o;
            if (this.getName() == null && casted.getName() != null) {
                return false;
            }
            return this.getName().equals(casted.getName());
        }
        return false;
    }

    @Override
    public int hashCode() {
        if (this.name != null) {
            return name.hashCode();
        }
        return super.hashCode();
    }

    public BigDecimal getPriority() {
        return priority;
    }

    public void setPriority(BigDecimal priority) {
        this.priority = priority;
    }

}
