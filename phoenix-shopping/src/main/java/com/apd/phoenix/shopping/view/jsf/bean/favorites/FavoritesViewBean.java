/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.shopping.view.jsf.bean.favorites;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.business.AccountXCredentialXUserBp;
import com.apd.phoenix.service.business.CatalogBp;
import com.apd.phoenix.service.business.FavoritesListBp;
import com.apd.phoenix.service.model.CatalogXItem;
import com.apd.phoenix.service.model.FavoritesList;
import com.apd.phoenix.shopping.view.jsf.bean.login.CredentialSelectionBean;
import com.apd.phoenix.shopping.view.jsf.bean.search.QueryData;
import com.apd.phoenix.shopping.view.jsf.bean.search.ResponseData;
import com.apd.phoenix.shopping.view.jsf.bean.utils.LinksBean;
import com.apd.phoenix.shopping.view.jsf.bean.utils.ViewUtils;

/**
 *
 * @author dnorris
 */
@Named
@Stateless
public class FavoritesViewBean implements Serializable {

    private static final Logger logger = LoggerFactory.getLogger(FavoritesViewBean.class);
    public static final String COMPANY_FAVORITES_TYPE = "COMPANY FAVORITES";
    public static final String PERSONAL_FAVORITES_TYPE = "MY FAVORITES";

    @Inject
    private FavoritesListBp favoritesListBp;
    @Inject
    private AccountXCredentialXUserBp accountXCredUserBp;
    @Inject
    private CredentialSelectionBean credentialSelectionBean;
    @Inject
    private CatalogBp catalogBp;
    @Inject
    private ViewUtils viewUtils;
    @Inject
    private LinksBean linksBean;

    public boolean isPersonalFavoritesType(String favoritesType) {
        return PERSONAL_FAVORITES_TYPE.equals(favoritesType);
    }

    public boolean isCompanyFavoritesType(String favoritesType) {
        return COMPANY_FAVORITES_TYPE.equals(favoritesType);
    }

    public String getCOMPANY_FAVORITES_TYPE() {
        return COMPANY_FAVORITES_TYPE;
    }

    public String getPERSONAL_FAVORITES_TYPE() {
        return PERSONAL_FAVORITES_TYPE;
    }

    public ResponseData query(QueryData searchQuery) {
        if (StringUtils.isBlank(searchQuery.getFavoritesName()) || StringUtils.isBlank(searchQuery.getFavoritesType())) {
            return new ResponseData();
        }
        if (credentialSelectionBean.getCurrentAXCXU() != null) {
        	ResponseData toReturn = new ResponseData();
        	
        	//since this method queries the database directly, no filters can be applied
        	toReturn.setShowAnyFilter(false);
        	String listName = searchQuery.getFavoritesName();
        	switch (searchQuery.getFavoritesType()) {
        	case PERSONAL_FAVORITES_TYPE:
                long userID = credentialSelectionBean.getCurrentAXCXU().getId();
                List<FavoritesList> userFavorites = accountXCredUserBp.getUnInitializedFavoritesLists(userID);
                for (FavoritesList list : userFavorites) {
                    if (listName.equals(list.getName())) {
                        Set<CatalogXItem> temp = favoritesListBp.getInitializedSet(
                                list.getId(), searchQuery.getStart(), searchQuery.getRows());
                        toReturn.setNumFound(favoritesListBp.getListItemCount(list.getId()));
                        toReturn.setProducts(viewUtils.convertCatalogXItemsToProducts(new ArrayList<>(temp)));
                        return toReturn;
                    }
                }
                break;
        	case COMPANY_FAVORITES_TYPE:
        		long credentialId = credentialSelectionBean.getCurrentCredential().getId();
                List<FavoritesList> companyFavorites = catalogBp.getFavoritesLists(credentialId);
                for (FavoritesList list : companyFavorites) {
                    if (listName.equals(list.getName())) {
                        Set<CatalogXItem> temp = favoritesListBp.getInitializedSet(
                                list.getId(), searchQuery.getStart(), searchQuery.getRows());
                        toReturn.setNumFound(favoritesListBp.getListItemCount(list.getId()));
                        toReturn.setProducts(viewUtils.convertCatalogXItemsToProducts(new ArrayList<>(temp)));
                        return toReturn;
                    }
                }
                break;
        	}
        }
        return new ResponseData();
    }

    public String getListTypeUrl(String favoritesType, List<?> favoritesLists) {
        if (favoritesLists == null || favoritesLists.isEmpty()) {
            return "#";
        }
        if (this.isPersonalFavoritesType(favoritesType)) {
            if (credentialSelectionBean.hasPermission("shopping maintain user favorites")) {
                return linksBean.getUserFavorites();
            }
            else {
                return "#";
            }
        }
        return linksBean.getCompanyFavorites();
    }
}
