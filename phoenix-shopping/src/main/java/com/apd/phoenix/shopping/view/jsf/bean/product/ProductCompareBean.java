/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.shopping.view.jsf.bean.product;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateful;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.apd.phoenix.service.model.CatalogXItem;
import com.apd.phoenix.service.model.Item;
import com.apd.phoenix.service.product.Product;
import com.apd.phoenix.service.product.ProductNotFoundException;
import com.apd.phoenix.shopping.view.jsf.bean.search.SearchEngine;
import com.apd.phoenix.shopping.view.jsf.bean.search.SearchUtilities;
import com.apd.phoenix.shopping.view.jsf.bean.utils.LinksBean;
import com.apd.phoenix.shopping.view.jsf.bean.utils.ShoppingPropertiesLoader;

/**
 *
 * @author dcnorris
 */
@Named
@Stateful
@SessionScoped
public class ProductCompareBean {
	
	@Inject
	private LinksBean linksBean;

	@Inject
	private SearchUtilities searchUtils;
	
	@Inject
	private SearchEngine searchEngine;

    private static final int MAX_NUM_ITEMS_TO_COMPARE = 4;
    private static final String MANY_PREFIX = ShoppingPropertiesLoader.getInstance().getValidationProperties()
			.getString("browse.compare.tooManyPrefix");
    private static final String MANY_SUFFIX = ShoppingPropertiesLoader.getInstance().getValidationProperties()
			.getString("browse.compare.tooManySuffix");
    private static final String FEW = ShoppingPropertiesLoader.getInstance().getValidationProperties()
			.getString("browse.compare.tooFew");
    private List<ComparisonItem> compareList = new ArrayList<>();

    private List<Product> productsToCompare = new ArrayList<>();
    private Map<Product, Boolean> compareMap = new CompareListMap();

    private boolean selectQuantityModalDisplay = false;

    private static final Logger logger = LoggerFactory.getLogger(ProductCompareBean.class);

    public void init(Integer pageNumber) {
    	if (productsToCompare.size() > MAX_NUM_ITEMS_TO_COMPARE) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, 
                    		MANY_PREFIX + " " + MAX_NUM_ITEMS_TO_COMPARE + " " + MANY_SUFFIX, 
                    		MANY_PREFIX + " " + MAX_NUM_ITEMS_TO_COMPARE + " " + MANY_SUFFIX));
    		return;
    	}
    	if (productsToCompare.size() < 2) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, 
                    		FEW, 
                    		FEW));
    		return;
    	}
    	
        compareList = new ArrayList<>();

        for (Product product : productsToCompare) {
        	ComparisonItem comp;
			try {
				comp = new ComparisonItem(searchEngine.getCatalogProductFromVendorNameAndApdSku(product.getVendorName(), product.getApdSku()));
	        	compareList.add(comp);
			} catch (ProductNotFoundException e) {
				logger.warn("Unable to find product with APD SKU {} and vendor name {}", product.getApdSku(), product.getVendorName());
			}
        }
        
        ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
        
        try {
        	String urlParams = "";
        	if (pageNumber != null) {
        		urlParams = "?prevPageNumber=" + pageNumber;
        	}
			context.redirect(linksBean.getCompare() + urlParams);
		} catch (IOException e) {
			//do nothing
		}
    }

    public List<ComparisonItem> getCompareList() {
        return this.compareList;
    }

    public void remove(int index) {
    	ComparisonItem item = compareList.remove(index);
    	if (item != null) {
    		compareMap.put(item.getProduct(), false);
    	}
    }

    public List<String> compileUniqueSpecLabels() {
        List<String> list = new ArrayList<>();
        for (ComparisonItem item : compareList) {
        	List<String> itemList = new ArrayList<>();
            for (String key : item.getProductDetails().keySet()) {
               itemList.add(key);
            }
            Iterator<String> iterator = itemList.iterator();
            ProductDetailsBean.removeNullValues(iterator, item.getProductDetails());
            for(String itemListElement:itemList){
            	 if (!list.contains(itemListElement) && StringUtils.isNotBlank(itemListElement)) {
                 	list.add(itemListElement);
                 }
            }
        }
        Collections.sort(list);
        return list;
    }

    public List<Product> getProductsToCompare() {
		return productsToCompare;
	}

	public void setProductsToCompare(List<Product> productsToCompare) {
		this.productsToCompare = productsToCompare;
	}

    public Map<Product, Boolean> getCompareMap() {
        return compareMap;
    }

    public void setCompareMap(Map<Product, Boolean> compareMap) {
        this.compareMap = compareMap;
    }

	public boolean isSelectQuantityModalDisplay() {
		return selectQuantityModalDisplay;
	}

	public void setSelectQuantityModalDisplay(boolean selectQuantityModalDisplay) {
		this.selectQuantityModalDisplay = selectQuantityModalDisplay;
	}
	
	public int getMaxCompareQuantity() {
		return MAX_NUM_ITEMS_TO_COMPARE;
	}

	public class ComparisonItem {

        private Product product;
        private Map<String, String> productDetails = new HashMap<>();

        public ComparisonItem(Product product) {
        	this.setProduct(product);
        	CatalogXItem catalogItem = searchUtils.getFromProduct(product);
        	if (catalogItem != null) {
        		buildDetails(catalogItem.getItem());
        	}
        }

        public void buildDetails(Item item) {
            if (product.getSpecifications() != null && !product.getSpecifications().isEmpty()) {
                productDetails = product.getSpecifications();
            }
            else {
            	productDetails = item.getSpecificationsMap();
            }
        }

        public String retrieveValue(String key) {
            if (productDetails.containsKey(key)) {
                return productDetails.get(key);
            }
            else {
                return "";
            }
        }

		public Product getProduct() {
			return product;
		}

		public void setProduct(Product product) {
			this.product = product;
		}

        public Map<String, String> getProductDetails() {
            return productDetails;
        }

        public void setProductDetails(Map<String, String> productDetails) {
            this.productDetails = productDetails;
        }
    }

    private class CompareListMap extends HashMap<Product, Boolean> {

        private static final long serialVersionUID = 1L;

        @Override
        public Boolean put(Product key, Boolean value) {
    		setSelectQuantityModalDisplay(false);
            boolean currentValue = getProductsToCompare().contains(key);
            if (value && !currentValue) {
                getProductsToCompare().add(key);
            }
            else if (!value && currentValue) {
                getProductsToCompare().remove(key);
            }
            Boolean toReturn = super.put(key, value);
            if (!value) {
            	super.remove(key);
            } else if (value && getProductsToCompare().size() > MAX_NUM_ITEMS_TO_COMPARE) {
        		this.put(key, false);
        		setSelectQuantityModalDisplay(true);
        	}
            return toReturn;
        }

        @Override
        public Boolean get(Object o) {
            if (o instanceof Product) {
                super.put((Product) o, getProductsToCompare().contains(o));
            }
            return super.get(o);
        }
    }
}
