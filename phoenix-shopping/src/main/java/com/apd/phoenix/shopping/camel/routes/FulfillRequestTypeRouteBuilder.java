package com.apd.phoenix.shopping.camel.routes;

import org.apache.camel.LoggingLevel;
import com.apd.phoenix.service.integration.cxml.model.fulfill.Response;
import com.apd.phoenix.service.integration.cxml.model.fulfill.Status;
import com.apd.phoenix.service.integration.multitenancy.AbstractTenantAwareRouteBuilder;
import com.apd.phoenix.service.model.dto.OrderLogDto.EventTypeDto;
import com.apd.phoenix.shopping.camel.process.CxmlFulfillTranslator;
import com.apd.phoenix.shopping.camel.process.CxmlRequestProcessor;
import com.apd.phoenix.shopping.camel.process.CxmlTransactionLogger;

public class FulfillRequestTypeRouteBuilder extends AbstractTenantAwareRouteBuilder {

    private String fulfillShipNoticeRequestSource;

    private static final String fulfillLogSource = "direct:fulfillLogSource";

    @Override
    public void configureRouteImplementation() throws Exception {

        onException(Exception.class).maximumRedeliveries(0).handled(true).log(LoggingLevel.INFO,
                "Error in cxml processing: ${exception.stacktrace}").setBody(constant(generateFailResponse()));

        //Ship Notice Request
        from(fulfillShipNoticeRequestSource).log(LoggingLevel.INFO, "processing ship notice")
        //Set Headers
                .setHeader("fulfillRequestType", constant("ShipNoticeRequest")).setHeader("groupId").jxpath(
                        "/in/body/shipNoticeHeader/shipmentID")
                //Translate to Advanced Ship Notice
                .bean(CxmlFulfillTranslator.class, "fromShipNoticeToAdvShipNotice")
                //Split on shipAcks
                .split().body()
                //Set headers
                .setHeader("apdPo").jxpath("/in/body/apdPo")
                //Log
                .wireTap(fulfillLogSource).end()
                //send to async destination
                .bean(CxmlRequestProcessor.class, "processInboundShipNoticeRequest").end()
                //Generate response
                .setBody(constant(generateOKResponse()));

        from(fulfillLogSource).setHeader("messageEventType", constant(EventTypeDto.ADVANCED_SHIPMENT_NOTICE))
        //Set the body to the original cxml message
                .setBody(simple("${header.rawMessage}")).bean(CxmlTransactionLogger.class, "logCxmlTransaction");

    }

    private Response generateOKResponse() {
        Response response = new Response();
        Status status = new Status();
        status.setCode("200");
        status.setText("OK");
        response.setStatus(status);
        return response;
    }

    private Response generateFailResponse() {
        Response response = new Response();
        Status status = new Status();
        status.setCode("400");
        status.setText("Failed");
        response.setStatus(status);
        return response;
    }

    public String getFulfillShipNoticeRequestSource() {
        return fulfillShipNoticeRequestSource;
    }

    public void setFulfillShipNoticeRequestSource(String fulfillShipNoticeRequestSource) {
        this.fulfillShipNoticeRequestSource = fulfillShipNoticeRequestSource;
    }

}
