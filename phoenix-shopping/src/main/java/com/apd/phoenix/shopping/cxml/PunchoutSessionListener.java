/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.shopping.cxml;

import com.apd.phoenix.service.business.PunchoutSessionBp;
import com.apd.phoenix.service.model.PunchoutSession;
import com.apd.phoenix.service.persistence.multitenancy.impl.CurrentTenantIdentifierResolverImpl;
import com.apd.phoenix.shopping.view.jsf.bean.login.LoginBean;
import javax.ejb.EJB;
import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;
import org.apache.commons.lang.StringUtils;

/**
 *
 * @author dnorris
 */
@WebListener
public class PunchoutSessionListener implements HttpSessionListener {

    @EJB
    private PunchoutSessionBp punchoutSessionBp;

    @Override
    public void sessionCreated(HttpSessionEvent se) {
        //do nothing
    }

    @Override
    public void sessionDestroyed(HttpSessionEvent se) {
        cleanUpPunchoutSession(se);
    }

    private void cleanUpPunchoutSession(HttpSessionEvent se) {
        String punchoutSessionToken = LoginBean.getPunchoutSessionAttribute(se.getSession());
        //the tenantId should always be set on the session if the punchout token is set
        Long tenantId = LoginBean.getTenantSessionAttribute(se.getSession());
        if (StringUtils.isNotBlank(punchoutSessionToken)) {
            CurrentTenantIdentifierResolverImpl.setCurrentTenant(tenantId);
            PunchoutSession session = new PunchoutSession();
            session.setSessionToken(punchoutSessionToken);
            if (checkSessionExist(session)) {
                session = punchoutSessionBp.searchByExactExample(session, 0, 0).get(0);
                punchoutSessionBp.delete(session.getId(), PunchoutSession.class);
            }
        }
    }

    private boolean checkSessionExist(PunchoutSession punchoutSession) {
        try {
            punchoutSessionBp.searchByExactExample(punchoutSession, 0, 0).get(0);
        }
        catch (IndexOutOfBoundsException ex) {
            return false;
        }
        return true;
    }

}
