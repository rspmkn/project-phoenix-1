package com.apd.phoenix.shopping.view.jsf.bean.login;

import java.io.IOException;
import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import javax.inject.Inject;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.apd.phoenix.core.utility.EcommercePropertiesLoader;
import com.apd.phoenix.service.model.SystemUser;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@WebFilter
public class ChangePasswordFilter implements Serializable, Filter {

    private static final long serialVersionUID = 1L;

    private static final Logger LOG = LoggerFactory.getLogger(ChangePasswordFilter.class);

    @Inject
    private LoginBean loginBean;

    @Inject
    private CredentialSelectionBean credBean;

    private int passwordExpirationMonths = EcommercePropertiesLoader.getInstance().getEcommerceProperties().getInt(
            "passwordExpirationMonths", 3);

    @Override
    public void destroy() {
        //do nothing
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException,
            ServletException {
        if (StringUtils.isBlank(loginBean.getUsername())) {
            loginBean.setUsername(((HttpServletRequest) request).getUserPrincipal().getName());
        }
        SystemUser user = loginBean.getSystemUser();
        Calendar c = Calendar.getInstance();
        c.setTime(new Date());
        c.add(Calendar.MONTH, -1 * passwordExpirationMonths);
        Date earliestPasswordDate = c.getTime();

        //if the user needs to reset their password, redirects to change password page
        if (user != null
                && user.getId() != null
                && !user.getConcurrentAccess()
                && ((user.getPasswordCreationDate() != null && user.getPasswordCreationDate().before(
                        earliestPasswordDate)) || (user.getChangePassword()))) {
            LOG.info("Redirecting user {} to change password page", user.getLogin());
            credBean.redirectToChangePassword((HttpServletRequest) request, (HttpServletResponse) response);
            return;
        }

        //otherwise, the response is sent normally
        chain.doFilter(request, response);
    }

    @Override
    public void init(FilterConfig arg0) throws ServletException {
        //do nothing
    }

}
