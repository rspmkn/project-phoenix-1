package com.apd.phoenix.shopping.view.jsf.bean.login;

import java.io.IOException;
import java.io.Serializable;
import javax.inject.Inject;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@WebFilter
public class CredentialFilter implements Serializable, Filter {

    private static final long serialVersionUID = 1757260435884209285L;
    private static final Logger LOG = LoggerFactory.getLogger(CredentialFilter.class);

    @Inject
    private CredentialSelectionBean credentialSelectionBean;

    @Inject
    private LoginBean loginBean;

    @Override
    public void destroy() {
        //do nothing
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException,
            ServletException {

        //if the currently selected credential is null...
        if (credentialSelectionBean.getCurrentAXCXU() == null) {

            //...first tries to get a single credential from the currently logged-in user...
            //sets the username on the LoginBean to the user principal that sent the request
            loginBean.setUsername(((HttpServletRequest) request).getUserPrincipal().getName());

            credentialSelectionBean.setArrivalRequestUrl((HttpServletRequest) request);
            credentialSelectionBean.redirectToCredentialSelection((HttpServletRequest) request,
                    (HttpServletResponse) response);
            return;
        }
        else if (credentialSelectionBean.getPunchoutBuyerCookie() == null) {
            credentialSelectionBean.setupExternalPunchoutSession();
        }

        //otherwise, the response is sent normally
        chain.doFilter(request, response);
    }

    @Override
    public void init(FilterConfig arg0) throws ServletException {
        //do nothing
    }

}
