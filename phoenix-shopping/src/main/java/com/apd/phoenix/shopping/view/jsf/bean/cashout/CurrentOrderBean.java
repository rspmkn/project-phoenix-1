package com.apd.phoenix.shopping.view.jsf.bean.cashout;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import javax.enterprise.context.SessionScoped;

import com.apd.phoenix.service.model.CatalogXItem;
import com.apd.phoenix.service.model.CustomerOrder;

@SessionScoped
public class CurrentOrderBean implements Serializable {

    private static final long serialVersionUID = 1L;
    private CustomerOrder currentOrder;
    private boolean overrideWithCart = true;
    private Map<CatalogXItem, BigDecimal> promisedPrices = new HashMap<>();

    public CustomerOrder getCurrentOrder() {
        return currentOrder;
    }

    public void setCurrentOrder(CustomerOrder currentOrder) {
        this.currentOrder = currentOrder;
        this.overrideWithCart = true;
    }

    public void setCurrentOrder(CustomerOrder currentOrder, boolean overrideWithCart) {
        this.currentOrder = currentOrder;
        this.overrideWithCart = overrideWithCart;
    }

    public boolean getShouldOverrideWithCart() {
        return this.overrideWithCart;
    }

	public Map<CatalogXItem, BigDecimal> getPromisedPrices() {
		return promisedPrices;
	}

	public void setPromisedPrices(Map<CatalogXItem, BigDecimal> promisedPrices) {
		this.promisedPrices = promisedPrices;
	}

}
