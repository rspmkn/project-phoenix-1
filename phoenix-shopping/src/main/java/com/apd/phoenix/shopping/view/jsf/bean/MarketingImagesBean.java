package com.apd.phoenix.shopping.view.jsf.bean;

import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;
import javax.inject.Named;
import org.apache.commons.lang.StringUtils;
import com.apd.phoenix.core.StringEscape;
import com.apd.phoenix.core.utility.TenantConfigRepository;
import com.apd.phoenix.service.business.CatalogBp;
import com.apd.phoenix.service.model.CarouselDisplay;
import com.apd.phoenix.service.model.Catalog;
import com.apd.phoenix.service.model.Credential;
import com.apd.phoenix.service.model.CredentialPropertyType.CredentialPropertyTypeEnum;
import com.apd.phoenix.service.model.CredentialXCredentialPropertyType;
import com.apd.phoenix.service.persistence.multitenancy.impl.CurrentTenantIdentifierResolverImpl;
import com.apd.phoenix.shopping.view.jsf.bean.login.CredentialSelectionBean;

/**
 *
 * @author anicholson
 */
@Named
public class MarketingImagesBean {

    private static final String IMAGE_LINK_DESTINATION_SUFFIX = "ImageLinkDestination";
    private static final String IMAGE_DIRECTORY = "/images/marketing/";

    private static final String PROPERTIES_PREFIX = "image.name.";

    @Inject
    CredentialSelectionBean credentialSelectionBean;

    @Inject
    CatalogBp catalogBp;

    public String getBannerUrl() {
        if (credentialSelectionBean.getCurrentCredential() != null
                && credentialSelectionBean.getCurrentCredential().getProperties() != null) {
            String bannerName = findBannerName(credentialSelectionBean.getCurrentCredential());
            if (StringUtils.isNotEmpty(bannerName)) {
                return getImageDirectoryURL() + StringEscape.escapeForUrl(bannerName);
            }
        }
        return getImageUrl("banner");
    }

    private String findBannerName(Credential currentCredential) {
        for (CredentialXCredentialPropertyType credentialPropety : currentCredential.getProperties()) {
            if (credentialPropety.getType() != null
                    && CredentialPropertyTypeEnum.BANNER_NAME.getValue().equals(credentialPropety.getType().getName())) {
                return credentialPropety.getValue();
            }
        }
        return null;
    }

    public static String getImageUrl(String location) {
        String imageName = TenantConfigRepository.getInstance().getProperties("themeMap").getProperty(
                PROPERTIES_PREFIX + location);
        String url = getImageDirectoryURL() + StringEscape.escapeForUrl(imageName);
        return url;
    }

    public String getImageLinkDestination(String location) {

        String url = "";
        Credential cred = credentialSelectionBean.getCurrentCredential();

        if (cred != null) {
            url = cred.getPropertyValueByType(location + IMAGE_LINK_DESTINATION_SUFFIX);
        }

        if (StringUtils.isEmpty(url)) {
            url = TenantConfigRepository.getInstance().getProperties("themeMap").getProperty(
                    "image.link.destination." + location);
        }
        return url;
    }

    private static String getImageDirectoryURL() {
        Long tenantId = CurrentTenantIdentifierResolverImpl.getCurrentTenant();
        return TenantConfigRepository.getInstance().getPropertiesByTenantId(tenantId, "tenant").getProperty(
                "content.service.secure.tenant.domain")
                + IMAGE_DIRECTORY;

    }

    public String getTopHomeImageUrl() {
        if (isCatalogNotNull()
                && StringUtils.isNotBlank(credentialSelectionBean.getCurrentCredential().getCatalog()
                        .getMarketingImageUrlTop())) {
            return credentialSelectionBean.getCurrentCredential().getCatalog().getMarketingImageUrlTop();
        }
        else {
            return getImageUrl("home1");
        }
    }

    public String getTopHomeImageLinkDestination() {
        if (isCatalogNotNull()
                && StringUtils.isNotBlank(credentialSelectionBean.getCurrentCredential().getCatalog()
                        .getMarketingImageLinkUrlTop())) {
            return credentialSelectionBean.getCurrentCredential().getCatalog().getMarketingImageLinkUrlTop();
        }
        else {
            return getImageLinkDestination("home1");
        }
    }

    public String getBottomHomeImageUrl() {
        if (isCatalogNotNull()
                && StringUtils.isNotBlank(credentialSelectionBean.getCurrentCredential().getCatalog()
                        .getMarketingImageUrlBottom())) {
            return credentialSelectionBean.getCurrentCredential().getCatalog().getMarketingImageUrlBottom();
        }
        else {
            return getImageUrl("home2");
        }
    }

    public String getBottomHomeImageLinkDestination() {
        if (isCatalogNotNull()
                && StringUtils.isNotBlank(credentialSelectionBean.getCurrentCredential().getCatalog()
                        .getMarketingLinkUrlBottom())) {
            return credentialSelectionBean.getCurrentCredential().getCatalog().getMarketingLinkUrlBottom();
        }
        else {
            return getImageLinkDestination("home2");
        }
    }

    private boolean isCatalogNotNull() {
        return credentialSelectionBean.getCurrentCredential() != null
                && credentialSelectionBean.getCurrentCredential().getCatalog() != null;
    }

    public List<CarouselDisplay> getHomePageCarouselDisplays() {
        List<CarouselDisplay> display = new ArrayList<CarouselDisplay>();
        if (isCatalogNotNull()) {
            Catalog catalog = catalogBp.hydrateCarousel(credentialSelectionBean.getCurrentCredential().getCatalog());
            if (!catalog.getCarouselDisplayItems().isEmpty()) {
                display.addAll(catalog.getCarouselDisplayItems());
            }
        }
        if (display.isEmpty()) {
            CarouselDisplay defaultImage = new CarouselDisplay();
            defaultImage.setBackgroundImageUrl(getImageUrl("ip-banner"));
            defaultImage.setLinkUrl(getImageLinkDestination("ip-banner"));
            display.add(defaultImage);
        }
        return display;
    }

    public int getCarouselQuantity() {
        return getHomePageCarouselDisplays() != null ? getHomePageCarouselDisplays().size() : 0;
    }

}
