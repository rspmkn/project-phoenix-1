/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.shopping.view.jsf.bean.order;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import javax.ejb.Stateful;
import javax.enterprise.context.ConversationScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.commons.lang.StringUtils;

import com.apd.phoenix.core.utility.TenantConfigRepository;
import com.apd.phoenix.service.business.ItemBp;
import com.apd.phoenix.service.business.LineItemBp;
import com.apd.phoenix.service.model.Item;
import com.apd.phoenix.service.model.ItemClassificationType;
import com.apd.phoenix.service.model.ItemImage;
import com.apd.phoenix.service.model.ItemXItemPropertyType;
import com.apd.phoenix.service.model.LineItem;
import com.apd.phoenix.shopping.view.jsf.bean.MarketingImagesBean;
import com.apd.phoenix.shopping.view.jsf.bean.login.CredentialSelectionBean;

/**
 *
 * @author RHC
 */
@Named
@Stateful
@ConversationScoped
public class LineItemToProduct implements Serializable {

	private static final long serialVersionUID = 1L;

	//map from Item id to main image
	Map<Long, String> imageUrls = new HashMap<>();

	//map from Item id icon URLs and tooltips
	Map<Long, Map<String, String>> icons = new HashMap<>();
	
//	public static final String DEFAULT_IMAGE = MarketingImagesBean.getImageUrl("lineItemToProduct1");

	@Inject
	private LineItemBp lineItemBp;
	
	@Inject
	private ItemBp itemBp;
	
	@Inject
	private CredentialSelectionBean credentialSelectionBean;
	
	public String getDefaultImage(){
		return MarketingImagesBean.getImageUrl("lineItemToProduct1");
	}

	public String getImageUrl(LineItem lineItem) {
		//first, obtains the key
		Long key = this.getKey(lineItem);
		if (key == null) {
			return getDefaultImage();
		}
		//if the URL has already been fetched, returns it
		if (imageUrls.containsKey(key)) {
			return imageUrls.get(key);
		}
		//otherwise, looks up the item
		Item item = this.getItem(key, lineItem);
		if (item == null) {
			String defaultImage = getDefaultImage();
			imageUrls.put(key, defaultImage);
			return defaultImage;
		}
		//iterates through the images, if any are marked as primary, 
		//return it
		for (ItemImage image : item.getItemImages()) {
			if (image.getPrimary() != null && image.getPrimary()) {
				imageUrls.put(key, image.getImageUrl());
				return image.getImageUrl();
			}
		}
		//otherwise, return the first image
		for (ItemImage image : item.getItemImages()) {
			imageUrls.put(key, image.getImageUrl());
			return image.getImageUrl();
		}
		//if there are no images
		String defaultImage = getDefaultImage();
		imageUrls.put(key, defaultImage);
		return defaultImage;
	}

	public Map<String, String> getIconMap(LineItem lineItem) {
		//first, obtains the key
		Long key = this.getKey(lineItem);
		if (key == null) {
			return new HashMap<>();
		}
		//if the URL has already been fetched, returns it
		if (icons.containsKey(key)) {
			return icons.get(key);
		}
		//otherwise, looks up the item
		Item item = this.getItem(key, lineItem);
		if (item == null) {
			icons.put(key, new HashMap<String, String>());
			return new HashMap<>();
		}
		//iterates through the classifications, adding them to the icon set
		//note that this is independent of Solr, getting the latest information from the database
		Map<String, String> toReturn = new HashMap<String, String>();
		for (ItemClassificationType classType : item.getClassifications().keySet()) {
			if (classType != null && StringUtils.isNotBlank(classType.getIconUrl()) && StringUtils.isNotBlank(classType.getToolTipLabel())) {
				toReturn.put(this.getIconUrl(classType.getIconUrl()), classType.getToolTipLabel());
			}
		}
		for (ItemXItemPropertyType property : item.getPropertiesReadOnly()) {
			if (!"N".equals(property.getValue()) && !"NO".equals(property.getValue()) 
					&& StringUtils.isNotBlank(property.getType().getIconUrl()) && StringUtils.isNotBlank(property.getType().getToolTipLabel())) {
				toReturn.put(this.getIconUrl(property.getType().getIconUrl()), property.getType().getToolTipLabel());
			}
		}
		if (lineItem.getCore() != null && lineItem.getCore()) {
			toReturn.put(this.getIconUrl((String)TenantConfigRepository.getInstance().getProperties("ecommerce")
					.get("coreItemIconUrl")), "Core Item");
		}
		//if there are no icons
		icons.put(key, toReturn);
		return toReturn;
	}
	
	private String getIconUrl(String originalUrl) {
		return credentialSelectionBean.getIconUrl(originalUrl);
	}
	
	/**
	 * Takes a lineItem, and returns the appropriate key
	 * 
	 * @param lineItem
	 * @return
	 */
	private Long getKey(LineItem lineItem) {
		Long key = null;
		//if the line item's id is not null, returns it
		if (lineItem != null && lineItem.getId() != null) {
			key = lineItem.getId();
		//if the line item's id is null, then it is transient, and returns the id of the vendor item
		} else if (lineItem != null && lineItem.getItem() != null && lineItem.getItem().getId() != null) {
			key = lineItem.getItem().getId();
		}
		return key;
	}
	
	/**
	 * Takes a key, and returns a managed Item 
	 * 
	 * @param key
	 * @return
	 */
	private Item getItem(Long key, LineItem item) {
		if (item != null && item.getId() != null && item.getId().equals(key)) {
			item = lineItemBp.findById(key, LineItem.class);
			return item != null ? item.getItem() : null;
		} else if (item != null && item.getItem() != null) {
			return itemBp.findById(key, Item.class);
		}
		return null;
	}
}
