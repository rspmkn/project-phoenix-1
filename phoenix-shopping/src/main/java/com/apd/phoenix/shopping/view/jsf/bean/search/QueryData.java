/*
 * To change this template; choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.shopping.view.jsf.bean.search;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;

import com.apd.phoenix.shopping.view.jsf.bean.search.ProductSort.SortOption;
import com.apd.phoenix.shopping.view.jsf.bean.utils.FilterKey;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;

/**
 *
 * @author dcnorris
 */
public class QueryData {

    private static final String INNER_FILTER_DELIMITER = " | ";
    private Boolean newSearch;
    private String searchString;
    private List<String> refineSearchStrings;
    private String activeFiltersString;
    private String searchEngineHierarchyKey;
    private String greenFilterId;
    private String filterToRemove;
    //TODO: refactor these, per PHOEN-5374
    private float lowPrice = -1;
    private float highPrice = -1;
    private int start = 0;
    private String selectedFilterName = "";
    private String selectedManufacturer;
    private Set<String> activeFilters = new HashSet<>();
    private String matchbookId = "";
    private String inkTonerManufacturerCode;
    private String inkTonerModel;
    private String inkTonerModelCode;
    private String cartridgeNumber;
    private boolean matchbookSearch = false;
    private int rows = 0;
    private SortOption sortOption;
    private Multimap<String, FilterKey> filters = HashMultimap.create();
    private Multimap<String, FilterKey> appliedFilters = HashMultimap.create();
    private boolean showNonCore = true;
    private String contractFilterId;
    private String favoritesId;
    private String favoritesName;
    private String favoritesType;
    
    public String getSearchString() {
        return searchString;
    }
    
    public void setSearchString(String searchString) {
        this.searchString = searchString;
    }

    public List<String> getRefineSearchStrings() {
        return refineSearchStrings;
    }

    public void setRefineSearchStrings(List<String> refineSearchStrings) {
        this.refineSearchStrings = refineSearchStrings;
    }

    public String getSearchEngineHierarchyKey() {
        return searchEngineHierarchyKey;
    }

    public void setSearchEngineHierarchyKey(String searchEngineHierarchyKey) {
        this.searchEngineHierarchyKey = searchEngineHierarchyKey;
    }

	public float getLowPrice() {
        return lowPrice;
    }

    public void setLowPrice(float lowPrice) {
        this.lowPrice = lowPrice;
    }

    public float getHighPrice() {
        return highPrice;
    }

    public void setHighPrice(float highPrice) {
        this.highPrice = highPrice;
    }

    public String getFiltersString() {
        return activeFiltersString;
    }

    public void setFiltersString(String activeFiltersString) {
        this.activeFiltersString = activeFiltersString;
    }

    public String getActiveFiltersString() {
        return activeFiltersString;
    }

    public void setActiveFiltersString(String activeFiltersString) {
        this.activeFiltersString = activeFiltersString;
    }

    public int getStart() {
        return start;
    }

    public void setStart(int start) {
        this.start = start;
    }

    public String getSelectedFilterName() {
        return selectedFilterName;
    }

    public void setSelectedFilterName(String selectedFilterName) {
        this.selectedFilterName = selectedFilterName;
    }

    public String getSelectedManufacturer() {
        return selectedManufacturer;
    }

    public void setSelectedManufacturer(String selectedManufacturer) {
        this.selectedManufacturer = selectedManufacturer;
    }

    public Set<String> getActiveFilters() {
        return activeFilters;
    }

    public void setActiveFilters(Set<String> activeFilters) {
        this.activeFilters = activeFilters;
    }

    public static QueryData clone(QueryData object) {
        QueryData data = new QueryData();
        data.setActiveFiltersString(object.getActiveFiltersString());
        data.setFiltersString(object.getActiveFiltersString());
        data.setSearchEngineHierarchyKey(object.getSearchEngineHierarchyKey());
        data.setHighPrice(object.getHighPrice());
        data.setLowPrice(object.getLowPrice());
        data.setSearchString(object.getSearchString());
        data.setSelectedFilterName(object.getSelectedFilterName());
        data.setSelectedManufacturer(object.getSelectedManufacturer());
        data.setStart(object.getStart());
        data.setActiveFilters(new HashSet<String>(object.getActiveFilters()));
        if (object.getRefineSearchStrings() != null) {
        	data.setRefineSearchStrings(new ArrayList<String>());
        	data.getRefineSearchStrings().addAll(object.getRefineSearchStrings());
        }
        data.setMatchbookId(object.getMatchbookId());
        data.setMatchbookSearch(object.isMatchbookSearch());
        data.setRows(object.getRows());
        data.setSortOption(object.getSortOption());
        data.setFilters(object.getFilters());
        data.setShowNonCore(object.isShowNonCore());
        data.setCartridgeNumber(object.getCartridgeNumber());
        data.setInkTonerManufacturerCode(object.getInkTonerManufacturerCode());
        data.setInkTonerModel(object.getInkTonerModel());
        data.setInkTonerModelCode(object.getInkTonerModelCode());
        data.setGreenFilterId(object.getGreenFilterId());
    	Multimap<String, FilterKey> copyAppliedFilters = HashMultimap.create();
		for(String key:object.getAppliedFilters().keys()){
        	object.getAppliedFilters().putAll(key,new ArrayList<>(object.getAppliedFilters().get(key)));
        }
		data.setAppliedFilters(copyAppliedFilters);
		data.setContractFilterId(object.getContractFilterId());
		data.setNewSearch(object.getNewSearch());
		data.setNewRefineSearchString(object.getNewRefineSearchString());
		data.setFavoritesId(object.getFavoritesId());
		data.setFavoritesName(object.getFavoritesName());
		data.setFavoritesType(object.getFavoritesType());
        return data;
    }

    public String getMatchbookId() {
        return matchbookId;
    }

    public void setMatchbookId(String matchbookId) {
            this.matchbookId = matchbookId;
    }

    public boolean isMatchbookSearch() {
		return matchbookSearch;
	}

	public void setMatchbookSearch(boolean matchbookSearch) {
		this.matchbookSearch = matchbookSearch;
	}

	public int getRows() {
		return rows;
	}

	public void setRows(int rows) {
		this.rows = rows;
	}

	public SortOption getSortOption() {
		return sortOption;
	}

	public void setSortOption(SortOption sortOption) {
		this.sortOption = sortOption;
	}

	public Multimap<String, FilterKey> getFilters() {
		return filters;
	}

	public void setFilters(Multimap<String, FilterKey> filters) {
		this.filters = filters;
	}

	public UTF8EncodedString getSearchStringEncoded() {
            return new UTF8EncodedString(searchString);
    }

    public UTF8EncodedString getActiveFiltersStringEncoded() {
            return new UTF8EncodedString(activeFiltersString);
    }

    public UTF8EncodedString getHierarchyKeyEncoded() {
            return new UTF8EncodedString(searchEngineHierarchyKey);
    }

    public UTF8EncodedString getSelectedFilterNameEncoded() {
            return new UTF8EncodedString(selectedFilterName);
    }

    public UTF8EncodedString getSelectedManufacturerEncoded() {
            return new UTF8EncodedString(selectedManufacturer);
    }

    public UTF8EncodedString getRefineSearchStringsEncoded() {
        return new UTF8EncodedString(this.getRefineSearchStringParameter());
    }

    public static String getFilterString(String filterCategory, String filterCategoryChild) {
        return filterCategory + INNER_FILTER_DELIMITER + filterCategoryChild;
    }

	public String getInkTonerManufacturerCode() {
		return inkTonerManufacturerCode;
	}

	public void setInkTonerManufacturerCode(String inkTonerManufacturerCode) {
		this.inkTonerManufacturerCode = inkTonerManufacturerCode;
	}

	public String getInkTonerModelCode() {
		return inkTonerModelCode;
	}

	public void setInkTonerModelCode(String inkTonerModelCode) {
		this.inkTonerModelCode = inkTonerModelCode;
	}

	public String getInkTonerModel() {
		return inkTonerModel;
	}

	public void setInkTonerModel(String inkTonerModel) {
		this.inkTonerModel = inkTonerModel;
	}

	public Multimap<String, FilterKey> getAppliedFilters() {
		return appliedFilters;
	}

	public void setAppliedFilters(Multimap<String, FilterKey> appliedFilters) {
		this.appliedFilters = appliedFilters;
	}	

	public boolean isShowNonCore() {
		return showNonCore;
	}

	public void setShowNonCore(boolean showNonCore) {
		this.showNonCore = showNonCore;
	}
	
	public String getShowNonCoreString() {
		return "" + this.showNonCore;
	}
	
	public void setShowNonCoreString(String showNonCoreString) {
		this.showNonCore = !(showNonCoreString.equals(Boolean.FALSE.toString()));
	}

	public String getCartridgeNumber() {
		return cartridgeNumber;
	}

	public void setCartridgeNumber(String cartridgeNumber) {
		this.cartridgeNumber = cartridgeNumber;
	}

	public Boolean getNewSearch() {
		return newSearch;
	}

	public void setNewSearch(Boolean newSearch) {
		this.newSearch = newSearch;
	}

	public String getGreenFilterId() {
		return greenFilterId;
	}

	public void setGreenFilterId(String greenFilterId) {
		this.greenFilterId = greenFilterId;
	}
	public String getFilterToRemove() {
		return filterToRemove;
	}

	public void setFilterToRemove(String filterToRemove) {
		this.filterToRemove = filterToRemove;
	}
	
	public String getNewRefineSearchString() {
		return "";
	}
	
	public void setNewRefineSearchString(String newString) {
		if (this.refineSearchStrings != null && this.refineSearchStrings.contains(newString)) {
			return;
		}
		if (this.refineSearchStrings == null) {
			this.refineSearchStrings = new ArrayList<>();
		}
		if(StringUtils.isNotEmpty(newString)) {
			this.refineSearchStrings.add(newString);
		}
	}
	
	public void setRefineSearchStringParameter(String parameter) {
		if (StringUtils.isBlank(parameter)) {
			return;
		}
		this.refineSearchStrings = new ArrayList<>();
		for (String s : parameter.split(",(?=([^\"]*\"[^\"]*\")*[^\"]*$)")) {
			this.refineSearchStrings.add(s);
		}
	}
	
	public String getRefineSearchStringParameter() {
		if (refineSearchStrings == null || refineSearchStrings.isEmpty()) {
			return "";
		}
    	StringBuffer toReturn = new StringBuffer();
    	for (String s : refineSearchStrings) {
    		if (toReturn.length() > 0) {
    			toReturn.append(",");
    		}
    		toReturn.append(StringEscapeUtils.escapeCsv(s));
    	}
    	return toReturn.toString();
	}
	
	public String getLastSearchString() {
		if (this.refineSearchStrings != null && !this.refineSearchStrings.isEmpty()) {
			return refineSearchStrings.get(refineSearchStrings.size() - 1);
		}
		return this.searchString;
	}

	public String getContractFilterId() {
		return contractFilterId;
	}

	public void setContractFilterId(String contractFilterId) {
		this.contractFilterId = contractFilterId;
	}

	public String getFavoritesId() {
		return favoritesId;
	}

	public void setFavoritesId(String favoritesId) {
		this.favoritesId = favoritesId;
	}

	public String getFavoritesName() {
		return favoritesName;
	}

	public void setFavoritesName(String favoritesName) {
		this.favoritesName = favoritesName;
	}

	public String getFavoritesType() {
		return favoritesType;
	}

	public void setFavoritesType(String favoritesType) {
		this.favoritesType = favoritesType;
	}
}
