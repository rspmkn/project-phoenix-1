package com.apd.phoenix.shopping.view.jsf.bean.cashout;

import java.io.IOException;
import java.io.Serializable;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Workaround for lock exceptions occurring when calling the "retrieve" method on Cashout.
 * 
 * If an exception occurs on the retrieve method, the page is refreshed several times, which
 * would get around the lock exception. After the last retry, if the error persists, the
 * user is shown an error page as expected.
 * 
 * @author RHC
 *
 */
@Named
@SessionScoped
public class CashoutBeanRetrieveWrapper implements Serializable {

    private static final long serialVersionUID = 1L;

    private static final Logger LOGGER = LoggerFactory.getLogger(CashoutBeanRetrieveWrapper.class);

    private static final int TOTAL_TRIES = 5;

    @Inject
    private CashoutBean cashoutBean;

    @Inject
    private Conversation conversation;

    private int tries = 0;

    public void retrieve() throws IOException {
        if (tries < TOTAL_TRIES) {
            try {
                cashoutBean.retrieve();
                tries = 0;
            }
            catch (Exception e) {
                tries++;
                LOGGER.warn("Reattempting load of cashout page, iteration " + tries);
                if (!conversation.isTransient()) {
                    conversation.end();
                }
                ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
                ec.redirect(((HttpServletRequest) ec.getRequest()).getRequestURI());
            }
            return;
        }
        LOGGER.error("Could not hit cashout page");
        tries = 0;
        throw new RuntimeException();
    }
}
