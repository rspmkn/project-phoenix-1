package com.apd.phoenix.shopping.view.jsf.bean.search;

import com.apd.phoenix.shopping.view.jsf.bean.product.Breadcrumb;
import com.apd.phoenix.shopping.view.jsf.bean.search.SearchHierarchy.SearchHierarchyComparator;
import com.apd.phoenix.shopping.view.jsf.bean.utils.FilterKey;
import com.google.common.collect.HashMultimap;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author dcnorris
 */
public class SubCategoryMenu implements Searcher {

    private static final Logger logger = LoggerFactory.getLogger(SubCategoryMenu.class);
    private List<SearchHierarchy> nextLevelMenuEntries;
    private List<SearchHierarchy> subList;
    private List<SearchHierarchy> extendedList;

    public SubCategoryMenu() {
    };

    public SubCategoryMenu(ResponseData response, Map<CategoryMenuNode, HashMultimap<SearchHierarchy, SearchHierarchy>> categoryData, Breadcrumb breadcrumb) {
        nextLevelMenuEntries = new ArrayList<>();
        if (response.getAvaliableCategoryFilters() != null && !response.getAvaliableCategoryFilters().isEmpty()) {
        	//tries to pull next level menu entries from response
        	for(FilterKey category:response.getAvaliableCategoryFilters().keySet()){
        		nextLevelMenuEntries.add(new SearchHierarchy(category.getFilter(),category.getId()));
        	}
        } else {
        	//if not specified on response, uses existing category and breadcrumbs to determine next level
	        int level = breadcrumb.getCrumbs().size() + 1;
	        CategoryMenuNode node = new CategoryMenuNode(level, breadcrumb.getTail());
	        if (categoryData.get(node) != null) {
	        	nextLevelMenuEntries.addAll(categoryData.get(node).get(breadcrumb.getTail()));
	        }
        }
        buildSubMenuLists();
    }

    /**
     * Builds two list for storing the next level category links the sublist
     * will hold the first 3 entries, and the extendedList will hold any
     * overflow for display in the dropdown widget
     */
    private void buildSubMenuLists() {
        subList = new ArrayList<>();
        extendedList = new ArrayList<>();
        List<SearchHierarchy> sortedNextLevelEntries = new ArrayList<>();
        sortedNextLevelEntries.addAll(nextLevelMenuEntries);
        Collections.sort(sortedNextLevelEntries, new SearchHierarchyComparator());
        for (int i = 0; i < sortedNextLevelEntries.size() && i < 3; i++) {
            subList.add(sortedNextLevelEntries.get(i));
        }
        if (sortedNextLevelEntries.size() > 3) {
            for (int i = 3; i < sortedNextLevelEntries.size(); i++) {
                extendedList.add(sortedNextLevelEntries.get(i));
            }
        }
    }

    /**
     * @return url for the subCategoryMenu entries
     */
    public String getUrl(String categoryKey) {
        try {
            categoryKey = URLEncoder.encode(categoryKey, "UTF-8");
        }
        catch (UnsupportedEncodingException ex) {
            logger.error("Error url decoding hierarchy path string", ex);
        }
        StringBuilder url = new StringBuilder(SEARCH_PAGE_BASE_URL);
        url.append("?").append(HIERARCHY_KEY_PARAMETER);
        url.append("=");
        url.append(categoryKey);
        url.append("&");
        url.append(SearchUtilities.NEW_SEARCH_PARAMETER);
        url.append("=");
        url.append(Boolean.TRUE);
        return url.toString();
    }

    /**
     * Return first three next level links
     */
    public List<SearchHierarchy> getSubList() {
        return subList;
    }

    /**
     * Return remaining next level links for the dropdown widget
     */
    public List<SearchHierarchy> getExtendedList() {
        return extendedList;
    }
}
