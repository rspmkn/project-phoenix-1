package com.apd.phoenix.shopping.view.jsf.bean.product;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.math.BigDecimal;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.ejb.LocalBean;
import javax.ejb.Stateful;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang.StringUtils;
import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.core.StringEscape;
import com.apd.phoenix.service.business.AccountXCredentialXUserBp;
import com.apd.phoenix.service.business.CatalogXItemBp;
import com.apd.phoenix.service.business.EntityComparator;
import com.apd.phoenix.service.business.FavoritesListBp;
import com.apd.phoenix.service.model.CatalogXItem;
import com.apd.phoenix.service.model.FavoritesList;
import com.apd.phoenix.service.model.Item;
import com.apd.phoenix.service.model.ItemImage;
import com.apd.phoenix.service.model.ItemSellingPoint;
import com.apd.phoenix.service.product.Product;
import com.apd.phoenix.service.product.ProductNotFoundException;
import com.apd.phoenix.shopping.view.jsf.bean.cashout.AddToCartBean;
import com.apd.phoenix.shopping.view.jsf.bean.favorites.FavoritesBean;
import com.apd.phoenix.shopping.view.jsf.bean.login.CredentialSelectionBean;
import com.apd.phoenix.shopping.view.jsf.bean.search.SearchEngine;
import com.apd.phoenix.shopping.view.jsf.bean.search.Searcher;
import com.apd.phoenix.shopping.view.jsf.bean.utils.LinksBean;
import javax.faces.application.FacesMessage;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;

@Named
@Stateful
@LocalBean
@RequestScoped
public class ProductDetailsBean implements Serializable, Searcher {

    private static final String NULL = "null";

    private static final long serialVersionUID = 4952590880241861324L;

    private static final Logger logger = LoggerFactory.getLogger(ProductDetailsBean.class);
    private CatalogXItem catalogXItem;
    private ItemImage selectedItemImage;
    private String selectedFavoritesListName;
    private String apdSku;
    private String vendorName;
    private Product product;
    private List<String> itemImages;
    private Map<String, String> specifications;
    private Map<String, String> classifications;
    private List<ItemSellingPoint> sellingPoints;
    @Inject
    private CatalogXItemBp catalogXItemBp;
    @Inject
    private FavoritesListBp favoritesListBp;
    @Inject
    private AccountXCredentialXUserBp accountXCredentialXUserBp;
    @Inject
    private CredentialSelectionBean credentialSelectionBean;
    @Inject
    private FavoritesBean favoritesBean;
    @Inject
    private LinksBean linksBean;
    @Inject
    private AddToCartBean addToCartBean;
    @Inject
    private SearchEngine searchEngine;
    @Inject
    private ProductRelationshipBean relationshipBean;
    private Integer qty;
    private boolean productAddedToList = false;
    private String displayFavoritesListName;
    private Item item;

    public void retrieve() {
        if (FacesContext.getCurrentInstance().isValidationFailed()) {
            redirectInvalidRequest();
        }
        if (!FacesContext.getCurrentInstance().isPostback()) {
            initializePageData();
        }
    }

    private void redirectInvalidRequest() {
        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect(SEARCH_PAGE_BASE_URL);
        }
        catch (Exception ex) {
            logger.error("Error redirecting to product browse page", ex);
        }
    }

    private void initializePageData() {
        catalogXItem = retrieveCatalogXItem();

        if (catalogXItem == null) {
            try {
                FacesContext.getCurrentInstance().getExternalContext().redirect(linksBean.getItemNotFound());
            }
            catch (IOException ex) {
                logger.error("IOException during redirect to product not found page.", ex);
            }
        }
        else {
            retrieveItemSpecifications();
            retrieveItemClassifications();
            retrieveItemSellingPoints();
            retrieveItemImages();
            retrieveForUseWIth();
            retrieveSubstitutionInfo();
            relationshipBean.retrieveSimilarItems(product);
        }
    }

    protected void retrieveForUseWIth() {
        if (StringUtils.isEmpty(product.getForModelNumbers())) {
            product.setForUseWithFromItem(this.getItem());
        }

    }

    private CatalogXItem retrieveCatalogXItem() {
        try {
            product = searchEngine.getCatalogProductFromVendorNameAndApdSku(vendorName, apdSku);
        }
        catch (ProductNotFoundException e) {
            logger.error("No product found");
        }
        if (product != null && product.getId() != null && product.getId() != 0) {
            this.catalogXItem = catalogXItemBp.findById(product.getId(), CatalogXItem.class);
            this.item = catalogXItem.getItem();
        }
        else if (product != null) {
            catalogXItem = new CatalogXItem();
        }
        return catalogXItem;
    }

    protected void retrieveItemClassifications() {
        if (this.getItem() != null) {
            classifications = this.getItem().getClassificationStringMap();
        }
    }

    protected void retrieveItemSellingPoints() {
        this.sellingPoints = new ArrayList<ItemSellingPoint>();
        if (product.getSellingPoints() != null && product.getSellingPoints().size() > 0) {
            ItemSellingPoint itemSellingPoint = null;
            int sequence = 1;
            for (String sellingPoint : product.getSellingPoints()) {
                itemSellingPoint = new ItemSellingPoint();
                itemSellingPoint.setValue(sellingPoint);
                itemSellingPoint.setPosition(sequence++);
                this.sellingPoints.add(itemSellingPoint);
            }
        }
        else if (this.getItem().getSellingPoints() != null) {
            for (ItemSellingPoint sellingPoint : catalogXItem.getItem().getSellingPoints()) {
                this.sellingPoints.add(sellingPoint);
            }
        }
        Collections.sort(this.sellingPoints, new EntityComparator());
    }

    protected void retrieveItemSpecifications() {
        if (product.getSpecifications() != null && !product.getSpecifications().isEmpty()) {
            specifications = product.getSpecifications();
        }
        else {
            specifications = this.getItem().getSpecificationsMap();
        }
        if (StringUtils.isNotEmpty(product.getWarranty()) && specifications != null) {
            specifications.put("Warranty", product.getWarranty());
        }
    }

    protected void retrieveItemImages() {
        itemImages = new ArrayList<String>();
        if (product.getImageUrls() != null && !product.getImageUrls().isEmpty()) {
            itemImages.addAll(product.getImageUrls());
        }
        else {
            for (ItemImage itemImage : catalogXItem.getItem().getItemImages()) {
                itemImages.add(itemImage.getImageUrl());
            }
        }
    }

    public Product getProduct() {
        reinit();
        return product;
    }

    public String getProductName() {
        return product.getName();
    }

    public String getProductDescription() {
        return product.getDescription();
    }

    public String getSafeProductDescription() {
        if (this.product != null) {
            String description = this.product.getDescription() == null ? "" : Jsoup.clean(
                    this.product.getDescription(), Whitelist.none());
            description = description.replaceAll("\\&quot;", "\"").replaceAll("\\&amp;", "&");
            return description;
        }
        return "";
    }

    public String getProductManufacturer() {
        return product.getManufacturerSku();
    }

    public String getProductPrice() {
        BigDecimal price = product.getPrice();
        return price.toString();
    }

    public BigDecimal getProductPriceDecimal() {
        return product.getPrice();
    }

    public String getProductSku() {
        //Note customer sku will be the same as the apdSku if a true customer sku did not exist
        if (StringUtils.isNotBlank(product.getCustomerSku())) {
            return product.getCustomerSku();
        }
        return product.getApdSku();
    }

    public String getVendorSku() {
        return product.getVendorSku();
    }

    public List<String> getItemSpecificationKeys() {
        if (specifications == null) {
            return new ArrayList<>();
        }
        return new ArrayList<>(specifications.keySet());
    }

    public List<String> getNotEmptyItemSpecificationKeys() {
        List<String> specificationKeys = getItemSpecificationKeys();
        Iterator<String> iterator = specificationKeys.iterator();
        removeNullValues(iterator, specifications);
        return specificationKeys;
    }

    public static void removeNullValues(Iterator<String> iterator, Map<String, String> map) {
        while (iterator.hasNext()) {
            String next = iterator.next();
            String nextValue = map.get(next);
            if (StringUtils.isEmpty(nextValue) || NULL.equalsIgnoreCase(nextValue)) {
                iterator.remove();
            }
        }
    }

    public String getItemSpecificationValue(String key) {
        return specifications.get(key);
    }

    public List<String> getItemClassificationKeys() {
        if (classifications == null) {
            return new ArrayList<>();
        }
        return new ArrayList<>(classifications.keySet());
    }

    public List<String> getNotEmptyItemClassificationKeys() {
        List<String> classificationKeys = getItemClassificationKeys();
        Iterator<String> iterator = classificationKeys.iterator();
        removeNullValues(iterator, classifications);
        return classificationKeys;
    }

    public String getItemClassificationValue(String key) {
        return classifications.get(key);
    }

    public void retrieveSubstitutionInfo() {
        CatalogXItem substitute = catalogXItem.getSubstituteItem();
        relationshipBean.setSubstituteProductList(new ArrayList<Product>());
        if (substitute != null) {
            relationshipBean.getSubstituteProductList().add(searchEngine.getProductFromCatalogXItem(substitute));
        }
    }

    public void addProductToFavorites() {
        this.reinit();
        FavoritesList selectedList = new FavoritesList();
        selectedList.setName(selectedFavoritesListName);
        selectedList.setId(getMatchingListID());
        if (selectedList.getId() == -1) {
            //Add new list
            favoritesBean.addNewUserList(selectedFavoritesListName);
            selectedList.setId(getMatchingListID());
            if (selectedList.getId() == -1) {
                logger.error("Could not add new favorites list.");
                FacesContext.getCurrentInstance().addMessage(null,
                        new FacesMessage("Error creating new favorites list"));
            }
            selectedList = favoritesListBp.searchByExactExample(selectedList, 0, 0).get(0);
            selectedList = favoritesListBp.findById(selectedList.getId(), FavoritesList.class);
            selectedList.getUserItems().add(catalogXItem);
            selectedList = favoritesListBp.update(selectedList);
            favoritesBean.refreshPage();
        }
        else {
            selectedList = favoritesListBp.searchByExactExample(selectedList, 0, 0).get(0);
            selectedList = favoritesListBp.findById(selectedList.getId(), FavoritesList.class);
            selectedList.getUserItems().add(catalogXItem);
            selectedList = favoritesListBp.update(selectedList);
            favoritesBean.refreshPage();
        }
        favoritesBean.updateUserFavoritesList();
        this.initializePageData();
        this.productAddedToList = true;
        try {
            String str = this.product.getDetailsLink();
            str = str + "&productAddedToList=true&displayFavoritesListName="
                    + StringEscape.escapeForUrl(selectedFavoritesListName);
            FacesContext.getCurrentInstance().getExternalContext().redirect(str);
        }
        catch (Exception e) {
            //if an error occurs, there won't be any indication that the item was added to the list 
            logger.warn("Error when redirecting after favorites list add", e);
        }
    }

    private long getMatchingListID() {
        FavoritesList list = accountXCredentialXUserBp.getFavoritesListByNameAndCredential(selectedFavoritesListName,
                this.credentialSelectionBean.getCurrentAXCXU());
        if (list != null) {
            return list.getId();
        }
        return -1;
    }

    public boolean isFilePresent(String path) {
        final File file = new File(path);
        return file.exists();
    }

    public CatalogXItem getCatalogXItem() {
        return this.catalogXItem;
    }

    public void setCatalogXItem(CatalogXItem instance) {
        this.catalogXItem = instance;
    }

    public List<String> getItemImages() {
        return this.itemImages;
    }

    public void setItemImages(List<String> itemImages) {
        this.itemImages = itemImages;
    }

    public ItemImage getSelectedItemImage() {
        return this.selectedItemImage;
    }

    public void setSelectedItemImage(ItemImage image) {
        this.selectedItemImage = image;
    }

    public String getURLString(List<String> stringSet) {
        if (stringSet != null && !stringSet.isEmpty()) {
            return (String) stringSet.toArray()[0];
        }
        return "";
    }

    public String getApdSku() {
        return apdSku;
    }

    public void setApdSku(String apdSku) {
        this.apdSku = apdSku;
    }

    public String getVendorName() {
        return vendorName;
    }

    public void setVendorName(String vendorName) {
        this.vendorName = vendorName;
    }

    public String getSelectedFavoritesListName() {
        return selectedFavoritesListName;
    }

    public void setSelectedFavoritesListName(String selectedFavoritesListName) {
        this.selectedFavoritesListName = selectedFavoritesListName;
    }

    private void reinit() {
        if (product != null) {
            return;
        }
        try {
            String url = FacesContext.getCurrentInstance().getExternalContext().getRequestHeaderMap().get("referer");
            if (StringUtils.isBlank(url)) {
                url = ((HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest())
                        .getRequestURL().toString();
            }
            List<NameValuePair> parameters = URLEncodedUtils.parse(new URI(url), "UTF-8");
            for (NameValuePair nvp : parameters) {
                if (nvp.getName().equals("apdSku") && StringUtils.isBlank(apdSku)) {
                    apdSku = nvp.getValue();
                }
                if (nvp.getName().equals("vendorName") && StringUtils.isBlank(vendorName)) {
                    vendorName = nvp.getValue();
                }
            }
            if (apdSku != null && vendorName != null) {
                retrieveCatalogXItem();
            }
        }
        catch (URISyntaxException ex) {
            //do nothing
        }
    }

    public void addToCart() {
        reinit();
        if (product != null) {
            addToCartBean.getDisplayQuantity().put(product, qty.toString());
            addToCartBean.addSingleItem(product);
        }
    }

    /**
     * @return the qty
     */
    public Integer getQty() {
        return qty;
    }

    /**
     * @param qty the qty to set
     */
    public void setQty(Integer qty) {
        this.qty = qty;
    }

    public List<ItemSellingPoint> getSellingPoints() {
        return sellingPoints;
    }

    public void setSellingPoints(List<ItemSellingPoint> sellingPoints) {
        this.sellingPoints = sellingPoints;
    }

    public boolean getProductAddedToList() {
        return this.productAddedToList;
    }

    public void setProductAddedToList(boolean productAddedToList) {
        this.productAddedToList = productAddedToList;
    }

    public String getDisplayFavoritesListName() {
        return displayFavoritesListName;
    }

    public void setDisplayFavoritesListName(String displayFavoritesListName) {
        this.displayFavoritesListName = displayFavoritesListName;
    }

    public String getProductBrandImage() {
        return product.getBrandImage();
    }

    public String getLargerImageUrl(String imageUrl) {
        if (product.getLargerImagesMap() != null && product.getLargerImagesMap().containsKey(imageUrl)) {
            return product.getLargerImagesMap().get(imageUrl);
        }
        return imageUrl;
    }

    /**
     * This method takes the ID of a tab and the value of the "productTab" URL parameter, and returns
     * whether that tab should be rendered.
     * 
     * PHOEN-5372 This method can be made accessible for PeekProductDetails. Additionally, PeekProductDetails
     * should include the ProductRelationshipBean logic.
     * 
     * @param tabId
     * @param selectedTabParameter
     * @return
     */
    public boolean shouldRenderTab(String tabId, String selectedTabParameter) {
        //if the selected tab parameter is specified, returns true if the tab ID matches, or false if it doesn't
        if (StringUtils.isNotBlank(selectedTabParameter)) {
            return tabId.equals(selectedTabParameter);
        }
        //if one of the tabs is the first relationship tab, that tab is selected
        if ("relationshipTab0".equals(tabId)) {
            return true;
        }
        //if there are no relationships, and the current tab is "tab1" (the product details tab), returns that tab;
        //returns false for all other cases
        return (relationshipBean.getOrderedRelationshipTabList() == null || relationshipBean
                .getOrderedRelationshipTabList().isEmpty())
                && "tab1".equals(tabId);
    }

    protected void retrieveSimilarItems() {
        relationshipBean.retrieveSimilarItems(product);
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }

}
