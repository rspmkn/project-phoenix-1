package com.apd.phoenix.shopping.view.jsf.bean.user;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateful;
import javax.enterprise.context.ConversationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.apd.phoenix.service.model.UserRequest;
import com.apd.phoenix.service.model.dto.UserModificationRequestDto;
import com.apd.phoenix.service.business.AccountBp;
import com.apd.phoenix.service.business.EmailFactoryBp;
import com.apd.phoenix.service.business.UserRequestBp;
import com.apd.phoenix.service.workflow.WorkflowService;
import com.apd.phoenix.shopping.view.jsf.bean.PageCssBean;
import com.apd.phoenix.shopping.view.jsf.bean.login.LoginBean;

@Named
@Stateful
@ConversationScoped
public class CintasUserCreationRequestBean {

    private static final Logger LOG = LoggerFactory.getLogger(CintasUserCreationRequestBean.class);

    private static final String REDIRECT_THANKYOU_PAGE = "/ecommerce/user/thankYouUserRegistration?faces-redirect=true";
    
    @Inject
    private WorkflowService workflowService;

    @Inject
    private UserRequestBp userRequestBp;
    
    @Inject
    private PageCssBean pageCssBean;
    
    @Inject
    private LoginBean loginBean;
    
    @Inject
    private AccountBp accountBp;
    
    private String firstName;
    private String lastName;
    private String email;
    private String phone;

    private String companyName;
    private String billLine1;
    private String billLine2;
    private String billCity;
    private String billState;
    private String billZip;
    private String billContactName;
    private String billContactEmail;
    private String billContactPhone;

    private String locationLocationName;
    private String locationLocationId;
    private String locationLine1;
    private String locationLine2;
    private String locationCity;
    private String locationState;
    private String locationZip;

    private String shipLocationName;
    private String shipLocationId;
    private String shipLine1;
    private String shipLine2;
    private String shipCity;
    private String shipState;
    private String shipZip;

    private String managerName;
    private String managerEmail;
    private String managerPhone;
    
    private List<String> emailList = new ArrayList<>();

    public String submit() {
        LOG.info("Submitting user modification request");
        UserRequest ur = userRequestBp.createUserRequest();
        Long id = ur.getId();
        String token = ur.getToken();
        LOG.info("Request id {}, token {}", id, token);
        if (id != null) {
        	
            UserModificationRequestDto umr = new UserModificationRequestDto();
            umr.setRequestType(EmailFactoryBp.USER_REQUEST_CREATE);
            
            boolean ipAddressValid = false;
            for (String address : loginBean.getValidIpAddresses()) {
            	ipAddressValid = this.accountBp.validIpAddress(address, "cnts");
            }
            
            if (!ipAddressValid) {
                FacesContext.getCurrentInstance().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_ERROR, "Your IP address is invalid", "Your IP address is invalid"));
                return null;
            }
            
            umr.setInternalEmail(this.getCsrEmail());
            
            umr.setFirstName(this.firstName);
            umr.setLastName(this.lastName);
            umr.setEmail(this.email);
            umr.setPhone(this.phone);
            umr.setCompanyName(this.companyName);
            umr.setBillLine1(this.billLine1);
            umr.setBillLine2(this.billLine2);
            umr.setBillCity(this.billCity);
            umr.setBillState(this.billState);
            umr.setBillZip(this.billZip);
            umr.setBillContactName(this.billContactName);
            umr.setBillContactEmail(this.billContactEmail);
            umr.setBillContactPhone(this.billContactPhone);
            umr.setLocationLocationName(this.locationLocationName);
            umr.setLocationLocationId(this.locationLocationId);
            umr.setLocationLine1(this.locationLine1);
            umr.setLocationLine2(this.locationLine2);
            umr.setLocationCity(this.locationCity);
            umr.setLocationState(this.locationState);
            umr.setLocationZip(this.locationZip);
            umr.setShipLocationName(this.shipLocationName);
            umr.setShipLocationId(this.shipLocationId);
            umr.setShipLine1(this.shipLine1);
            umr.setShipLine2(this.shipLine2);
            umr.setShipCity(this.shipCity);
            umr.setShipState(this.shipState);
            umr.setShipZip(this.shipZip);
            umr.setManagerName(this.managerName);
            umr.setManagerEmail(this.managerEmail);
            umr.setManagerPhone(this.managerPhone);
            
            if (StringUtils.isNotBlank(this.getCsrEmail())) {
	            for(String email : this.getCsrEmail().split(",")) {
	            	emailList.add(email.trim());
	            }
            }
            
            umr.setEmailList(this.emailList);
            
            LOG.info("Sending emails to: {}",emailList);
            Map<String,Object> params = new HashMap<>();
            params.put("request", umr);
            params.put("emailList", emailList);

            workflowService.startUserRequest(id,params);
        }

        return REDIRECT_THANKYOU_PAGE;
    }
    
    /**
     * Sets the shipping information to the same as the location information
     */
    public void shippingSameAsLocation() {
    	this.shipLocationName = this.locationLocationName;
    	this.shipLocationId = this.locationLocationId;
    	this.shipLine1 = this.locationLine1;
    	this.shipLine2 = this.locationLine2;
    	this.shipCity = this.locationCity;
    	this.shipState = this.locationState;
    	this.shipZip = this.locationZip;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getBillLine1() {
		return billLine1;
	}

	public void setBillLine1(String billLine1) {
		this.billLine1 = billLine1;
	}

	public String getBillLine2() {
		return billLine2;
	}

	public void setBillLine2(String billLine2) {
		this.billLine2 = billLine2;
	}

	public String getBillCity() {
		return billCity;
	}

	public void setBillCity(String billCity) {
		this.billCity = billCity;
	}

	public String getBillState() {
		return billState;
	}

	public void setBillState(String billState) {
		this.billState = billState;
	}

	public String getBillZip() {
		return billZip;
	}

	public void setBillZip(String billZip) {
		this.billZip = billZip;
	}

	public String getBillContactName() {
		return billContactName;
	}

	public void setBillContactName(String billContactName) {
		this.billContactName = billContactName;
	}

	public String getBillContactEmail() {
		return billContactEmail;
	}

	public void setBillContactEmail(String billContactEmail) {
		this.billContactEmail = billContactEmail;
	}

	public String getBillContactPhone() {
		return billContactPhone;
	}

	public void setBillContactPhone(String billContactPhone) {
		this.billContactPhone = billContactPhone;
	}

	public String getLocationLocationName() {
		return locationLocationName;
	}

	public void setLocationLocationName(String locationLocationName) {
		this.locationLocationName = locationLocationName;
	}

	public String getLocationLocationId() {
		return locationLocationId;
	}

	public void setLocationLocationId(String locationLocationId) {
		this.locationLocationId = locationLocationId;
	}

	public String getLocationLine1() {
		return locationLine1;
	}

	public void setLocationLine1(String locationLine1) {
		this.locationLine1 = locationLine1;
	}

	public String getLocationLine2() {
		return locationLine2;
	}

	public void setLocationLine2(String locationLine2) {
		this.locationLine2 = locationLine2;
	}

	public String getLocationCity() {
		return locationCity;
	}

	public void setLocationCity(String locationCity) {
		this.locationCity = locationCity;
	}

	public String getLocationState() {
		return locationState;
	}

	public void setLocationState(String locationState) {
		this.locationState = locationState;
	}

	public String getLocationZip() {
		return locationZip;
	}

	public void setLocationZip(String locationZip) {
		this.locationZip = locationZip;
	}

	public String getShipLocationName() {
		return shipLocationName;
	}

	public void setShipLocationName(String shipLocationName) {
		this.shipLocationName = shipLocationName;
	}

	public String getShipLocationId() {
		return shipLocationId;
	}

	public void setShipLocationId(String shipLocationId) {
		this.shipLocationId = shipLocationId;
	}

	public String getShipLine1() {
		return shipLine1;
	}

	public void setShipLine1(String shipLine1) {
		this.shipLine1 = shipLine1;
	}

	public String getShipLine2() {
		return shipLine2;
	}

	public void setShipLine2(String shipLine2) {
		this.shipLine2 = shipLine2;
	}

	public String getShipCity() {
		return shipCity;
	}

	public void setShipCity(String shipCity) {
		this.shipCity = shipCity;
	}

	public String getShipState() {
		return shipState;
	}

	public void setShipState(String shipState) {
		this.shipState = shipState;
	}

	public String getShipZip() {
		return shipZip;
	}

	public void setShipZip(String shipZip) {
		this.shipZip = shipZip;
	}

	public String getManagerName() {
		return managerName;
	}

	public void setManagerName(String managerName) {
		this.managerName = managerName;
	}

	public String getManagerEmail() {
		return managerEmail;
	}

	public void setManagerEmail(String managerEmail) {
		this.managerEmail = managerEmail;
	}

	public String getManagerPhone() {
		return managerPhone;
	}

	public void setManagerPhone(String managerPhone) {
		this.managerPhone = managerPhone;
	}
	
	public String getCsrPhone() {
		return pageCssBean.getCallUsPhone();
	}
	
	public String getCsrEmail() {
		return pageCssBean.getEmailUsAddress();
	}

}
