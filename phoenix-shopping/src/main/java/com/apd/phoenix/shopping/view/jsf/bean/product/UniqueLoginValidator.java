/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.shopping.view.jsf.bean.product;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import com.apd.phoenix.shopping.view.jsf.bean.PageCssBean;
import com.apd.phoenix.shopping.view.jsf.bean.user.UserRegistrationBean;

/**
 *
 * @author zgutterm
 */
//TODO Refactor organization email validators into one class
@FacesValidator("uniqueLoginValidator")
public class UniqueLoginValidator implements Validator {

    public static final String PLEASE_ENTER_A_VALID_USER_NAME = "Please enter a valid username";
    public static final String PLEASE_ENTER_AN_UNIQUE_USER_NAME = "Username is already taken. Please choose another";

    @Override
    public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {
        //Cannot inject a bean in a JSF validator
        UserRegistrationBean userRegBean = context.getApplication().evaluateExpressionGet(context,
                "#{userRegistrationBean}", UserRegistrationBean.class);

        if (!(value instanceof String)) {
            throw new ValidatorException(new FacesMessage(PLEASE_ENTER_A_VALID_USER_NAME));
        }

        if (!userRegBean.isUsernameAvailable((String) value)) {
            throw new ValidatorException(new FacesMessage(PLEASE_ENTER_AN_UNIQUE_USER_NAME));
        }
    }
}
