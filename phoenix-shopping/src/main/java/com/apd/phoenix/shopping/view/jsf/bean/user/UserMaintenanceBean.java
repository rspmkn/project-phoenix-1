package com.apd.phoenix.shopping.view.jsf.bean.user;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.ejb.Stateful;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.core.utility.PropertiesLoader;
import com.apd.phoenix.service.business.EmailFactoryBp;
import com.apd.phoenix.service.business.SystemUserBp;
import com.apd.phoenix.service.business.UserRequestBp;
import com.apd.phoenix.service.model.UserRequest;
import com.apd.phoenix.service.model.dto.DepartmentRequestDto;
import com.apd.phoenix.service.model.dto.UserModificationRequestDto;
import com.apd.phoenix.service.workflow.WorkflowService;
import com.apd.phoenix.shopping.view.jsf.bean.PageCssBean;
import com.apd.phoenix.shopping.view.jsf.bean.login.LoginBean;

@Named
@Stateful
@SessionScoped
public class UserMaintenanceBean {

    private static final Logger LOG = LoggerFactory.getLogger(UserMaintenanceBean.class);

    private static final String REDIRECT_THANKYOU_PAGE = "/ecommerce/user/thankYouUserRegistration?faces-redirect=true";
    private static final String REDIRECT_TO_CREATE_USER_PAGE = "/ecommerce/user/cCreateUserRequest?faces-redirect=true";
    private static final String REDIRECT_TO_CHANGE_USER_PAGE = "/ecommerce/user/changeUser?faces-redirect=true";

    @Inject
    PageCssBean pageCssBean;

    @Inject
    WorkflowService workflowService;

    @Inject
    UserRequestBp userRequestBp;

    @Inject
    SystemUserBp systemUserBp;

    @Inject
    private LoginBean loginBean;

    /*New User*/
    private String requestType;
    private String reason;
    private String endUser;
    private String userId;
    private String affiliation;
    private String isPSG;
    private String corpBuNum;
    private String firstName;
    private String lastName;
    private String email;
    private String companyEmail;
    private String phone;
    private String ext;

    /*Ship-to <Old> Address*/
    private String sAddress1;
    private String sAddress2;
    private String sCity;
    private String sState;
    private String sZip;
    private String sDesktop;
    private String sDepartment;
    private String sCostCenter;
    private String sComments;
    private String mailstop;
    private String pole;

    /*Ship-to New Location*/
    private String newAddress1;
    private String newAddress2;
    private String newCity;
    private String newState;
    private String newZip;
    private String newDesktop;
    private String newDepartment;
    private String newCostCenter;
    private String newComments;

    /*Departments*/
    private String dMultipleDepts;
    private String dName;
    private String dDesktop;
    private String dCostCenter;
    private String dSameAsDefault;
    private String dAddress;
    private String dCity;
    private String dState;
    private String dZip;
    private String dDivDept;
    private String dManagerEmail;
    private String dManagerName;
    private List<DepartmentRequestDto> departmentsList;

    /*Approver*/
    private String aName;
    private String aEmail;
    private String aPhone;

    /*Requestor*/
    private String rName;
    private String rEmail;
    private String rPhone;
    private String rMgrEmails;

    private List<String> emailList;
    private Boolean setDeptSameAddressFlag = false;
    private Boolean departmentManageFlag = false;
    private boolean departmentListEmpty;

    @PostConstruct
    public void init() {        
    	departmentsList = new ArrayList<>();
    	emailList = new ArrayList<>();    	
    }

    public void addDepartment() {

        DepartmentRequestDto department = new DepartmentRequestDto();
        department.setName(dName);
        department.setDesktop(dDesktop);
        department.setCostCenter(dCostCenter);
        department.setIsSameAddress(dSameAsDefault);
        department.setShipAddress(dAddress);
        department.setShipCity(dCity);
        department.setShipState(dState);
        department.setShipZip(dZip);

        departmentsList.add(department);

        dName = "";
        dDesktop = "";
        dCostCenter = "";
        dAddress = (setDeptSameAddressFlag ? sAddress1 + ";" + sAddress2 : "");
        dCity = (setDeptSameAddressFlag ? sCity : "");
        dState = (setDeptSameAddressFlag ? sState : "");
        dZip = (setDeptSameAddressFlag ? sZip : "");

        departmentManageFlag = true;
    }

    public void addDepartmentWalmart() {

        if (StringUtils.isEmpty(dDivDept) || StringUtils.isEmpty(dManagerName) || StringUtils.isEmpty(dManagerEmail)) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage("You must fill out all fields when adding a department"));
        }
        else {

            DepartmentRequestDto department = new DepartmentRequestDto();
            department.setDivDept(dDivDept);
            department.setManagerName(dManagerName);
            department.setManagerEmail(dManagerEmail);

            departmentsList.add(department);

            dDivDept = "";
            dManagerName = "";
            dManagerEmail = "";
        }

    }

    public void removeDepartmentFromTable(DepartmentRequestDto departmentToRemove) {

        LOG.info("Remove Department From Table called...");
        departmentsList.remove(departmentToRemove);
        departmentManageFlag = true;
    }

    public String submitCreateUser() {
        LOG.info("Submitting create user request");
        UserRequest ur = userRequestBp.createUserRequest();
        Long id = ur.getId();
        String token = ur.getToken();
        LOG.info("Request id {}, token {}", id, token);
        if (id != null) {
            //Add last remaining department           
            UserModificationRequestDto umr = new UserModificationRequestDto();
            
            /*New User*/
            umr.setUserType(requestType);
            umr.setRequestType(EmailFactoryBp.USER_REQUEST_CREATE);
            umr.setReason(reason);
            umr.setAffiliation(affiliation);
            umr.setIsPSG(isPSG);
            umr.setCorpBuNum(corpBuNum);
            umr.setFirstName(firstName);
            umr.setLastName(lastName);
            umr.setEmail(email);
            umr.setPhone(phone);
            umr.setExt(ext);

            /*Ship To Address*/
            umr.setSAddress1(sAddress1);
            umr.setSAddress2(sAddress2);
            umr.setSCity(sCity);
            umr.setSState(sState);
            umr.setSZip(sZip);
            umr.setSDesktop(sDesktop);
            umr.setSDepartment(sDepartment);
            umr.setSCostCenter(sCostCenter);
            
            /*Departments*/
            umr.setDepartmentsList(departmentsList);
            
            /*Approver*/
            umr.setAName(aName);
            umr.setAEmail(aEmail);
            umr.setAPhone(aPhone);
            
            umr.setInternalEmail(getCSREmail());
            updateEmailList();
            umr.setEmailList(emailList);
            
            if (loginBean.getIsMarfieldSubdomain()) {
            	umr.setNoManagerApproval(true);
            }

            LOG.info("Sending emails to: {}",emailList);
            Map<String,Object> params = new HashMap<>();
            params.put("request", umr);
            params.put("emailList", emailList);

            workflowService.startUserRequest(id,params);
            clearAll();
        }

        return  REDIRECT_THANKYOU_PAGE;
    }

    public String submitModifyUser() {
        LOG.info("Submitting modify user request");
        UserRequest ur = userRequestBp.createUserRequest();
        Long id = ur.getId();
        String token = ur.getToken();
        LOG.info("Request id {}, token {}", id, token);
        if (id != null) {
            //Add last remaining department
            UserModificationRequestDto umr = new UserModificationRequestDto();

            /*New User*/
            umr.setUserType(requestType);
            umr.setRequestType(EmailFactoryBp.USER_REQUEST_MODIFY);
            umr.setReason(reason);
            umr.setAffiliation(affiliation);
            umr.setIsPSG(isPSG);
            umr.setCorpBuNum(corpBuNum);
            umr.setFirstName(firstName);
            umr.setLastName(lastName);
            umr.setEmail(email);
            umr.setPhone(phone);
            umr.setExt(ext);

            /*Ship To Address*/
            umr.setSAddress1(sAddress1);
            umr.setSAddress2(sAddress2);
            umr.setSCity(sCity);
            umr.setSState(sState);
            umr.setSZip(sZip);
            umr.setSDesktop(sDesktop);
            umr.setSDepartment(sDepartment);
            umr.setSCostCenter(sCostCenter);
            

            /*Departments*/
            umr.setDepartmentsList(departmentsList);

            /*Approver*/
            umr.setAName(aName);
            umr.setAEmail(aEmail);
            umr.setAPhone(aPhone);

            umr.setInternalEmail(getCSREmail());
            
            if (loginBean.getIsMarfieldSubdomain()) {
            	umr.setNoManagerApproval(true);
            }

            //Ensure that there is an email address before it is added to email list
            updateEmailList();
            umr.setEmailList(emailList);

            LOG.info("Sending emails to: {}",emailList);
            Map<String,Object> params = new HashMap<>();
            params.put("request", umr);
            params.put("emailList", emailList);

            workflowService.startUserRequest(id,params);
            clearAll();
        }

        return  REDIRECT_THANKYOU_PAGE;
    }

    public String submitCreateUserWalmart() {
    	if (isDepartmentListEmpty()){
    		FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage("You must add at least one department"));
    		return null;
    	}else{
        LOG.info("Submitting create user request");
        UserRequest ur = userRequestBp.createUserRequest();
        Long id = ur.getId();
        String token = ur.getToken();
        LOG.info("Request id {}, token {}", id, token);
        if (id != null) {
            //Add last remaining department           
            UserModificationRequestDto umr = new UserModificationRequestDto();
            
            /*New User*/
            umr.setUserType(requestType);
            umr.setRequestType(EmailFactoryBp.USER_REQUEST_CREATE);
            umr.setReason(reason);
            umr.setEndUser(endUser);
            umr.setUserId(userId);
            umr.setFirstName(firstName);
            umr.setLastName(lastName);
            umr.setEmail(email);
            umr.setPhone(phone);
            umr.setExt(ext);

            /*Ship To Address*/
            umr.setSAddress1(sAddress1);
            umr.setSAddress2(sAddress2);
            umr.setSCity(sCity);
            umr.setSState(sState);
            umr.setSZip(sZip);
            umr.setSDesktop(sDesktop);
            umr.setSDepartment(sDepartment);
            umr.setSCostCenter(sCostCenter);
            umr.setMailStop(mailstop);
            umr.setPole(pole);
            
            /*Departments*/
            umr.setDepartmentsList(departmentsList);
            
            umr.setInternalEmail(getCSREmail());

            //Ensure that there is an email address before it is added to email list
            addToEmailList(getCSREmail());
            
            //Add all manager emails
            for (DepartmentRequestDto d: departmentsList){
            	addToEmailList(d.getManagerEmail());
            }
            
            String hqEmail = PropertiesLoader.getAsProperties("themeMap.properties").getProperty(
                    "walmart_hqemail");
        	addToEmailList(hqEmail);
        	umr.setEmailList(emailList);
            LOG.info("Sending emails to: {}",emailList);
            Map<String,Object> params = new HashMap<>();
            params.put("request", umr);
            params.put("emailList", emailList);

            workflowService.startUserRequest(id,params);
            clearAll();
        }

        return  REDIRECT_THANKYOU_PAGE;
    	}
    	
    }

    public String submitModifyUserWalmart() {
        LOG.info("Submitting modify user request");
        UserRequest ur = userRequestBp.createUserRequest();
        Long id = ur.getId();
        String token = ur.getToken();
        LOG.info("Request id {}, token {}", id, token);
        if (id != null) {
            //Add last remaining department
            UserModificationRequestDto umr = new UserModificationRequestDto();

            /*New User*/
            umr.setUserType(requestType);
            umr.setRequestType(EmailFactoryBp.USER_REQUEST_MODIFY);
            umr.setReason(reason);
            umr.setEndUser(endUser);
            umr.setUserId(userId);
            umr.setFirstName(firstName);
            umr.setLastName(lastName);
            umr.setEmail(email);
            umr.setPhone(phone);
            umr.setExt(ext);

            /*Ship To Address*/
            umr.setSAddress1(sAddress1);
            umr.setSAddress2(sAddress2);
            umr.setSCity(sCity);
            umr.setSState(sState);
            umr.setSZip(sZip);
            umr.setSDesktop(sDesktop);
            umr.setSDepartment(sDepartment);
            umr.setSCostCenter(sCostCenter);
            umr.setMailStop(mailstop);
            umr.setPole(pole);

            /*Requestor*/
            umr.setRequesterName(rName);
            umr.setRequesterEmail(rEmail);
            umr.setRequesterPhone(rPhone);
            umr.setRequesterManagerEmail(rMgrEmails);

            /*Departments*/
            umr.setDepartmentsList(departmentsList);

            umr.setInternalEmail(getCSREmail());

            //Ensure that there is an email address before it is added to email list
            updateEmailList();
            umr.setEmailList(emailList);
          //Add all manager emails
            for (DepartmentRequestDto d: departmentsList){
            	addToEmailList(d.getManagerEmail());
            }
            
            String hqEmail = PropertiesLoader.getAsProperties("themeMap.properties").getProperty(
                    "walmart_hqemail");
        	addToEmailList(hqEmail);
           

            LOG.info("Sending emails to: {}",emailList);
            Map<String,Object> params = new HashMap<>();
            params.put("request", umr);
            params.put("emailList", emailList);

            workflowService.startUserRequest(id,params);
            clearAll();
        }

        return  REDIRECT_THANKYOU_PAGE;
    }

    public String submitAddLocation() {
        LOG.info("Submitting add location modification request");
        UserRequest ur = userRequestBp.createUserRequest();
        Long id = ur.getId();
        String token = ur.getToken();
        LOG.info("Request id {}, token {}", id, token);
        if (id != null) {
            //Add last remaining department           
            UserModificationRequestDto umr = new UserModificationRequestDto();
            
            /*New User*/
            umr.setRequestType(EmailFactoryBp.LOCATION_REQUEST_CREATE);
            umr.setUserId(userId);
            umr.setFirstName(firstName);
            umr.setLastName(lastName);
            umr.setEmail(email);
            umr.setPhone(phone);
            umr.setExt(ext);

            /*Ship To Address*/
            umr.setSAddress1(sAddress1);
            umr.setSAddress2(sAddress2);
            umr.setSCity(sCity);
            umr.setSState(sState);
            umr.setSZip(sZip);
            umr.setSDesktop(sDesktop);
            umr.setSDepartment(sDepartment);
            umr.setSCostCenter(sCostCenter);
            umr.setSComments(sComments);
            
            umr.setInternalEmail(getCSREmail());

            //Ensure that there is an email address before it is added to email list
            addToEmailList(email);
            addToEmailList(aEmail);
            addToEmailList(getCSREmail());
            umr.setEmailList(emailList);
            
            if (loginBean.getIsMarfieldSubdomain()) {
            	umr.setNoManagerApproval(true);
            }
      
            
            LOG.info("Sending emails to: {}",emailList);
            Map<String,Object> params = new HashMap<>();
            params.put("request", umr);
            params.put("emailList", emailList);

            workflowService.startUserRequest(id,params);
            clearAll();
        }

        return REDIRECT_THANKYOU_PAGE;
    }

    public String submitChangeLocation() {
        LOG.info("Submitting change location modification request");
        UserRequest ur = userRequestBp.createUserRequest();
        Long id = ur.getId();
        String token = ur.getToken();
        LOG.info("Request id {}, token {}", id, token);
        if (id != null) {
            //Add last remaining department           
            UserModificationRequestDto umr = new UserModificationRequestDto();
            
            /*New User*/
            umr.setRequestType(EmailFactoryBp.LOCATION_REQUEST_MODIFY);
            umr.setUserId(userId);
            umr.setFirstName(firstName);
            umr.setLastName(lastName);
            umr.setEmail(email);
            umr.setPhone(phone);
            umr.setExt(ext);

            /*Ship To Address*/
            umr.setAddress1Old(sAddress1);
            umr.setAddress2Old(sAddress2);
            umr.setCityOld(sCity);
            umr.setStateOld(sState);
            umr.setZipOld(sZip);            
            umr.setCommentsOld(sComments);
            
            /*Ship To New Address*/
            umr.setAddress1New(newAddress1);
            umr.setAddress2New(newAddress2);
            umr.setCityNew(newCity);
            umr.setStateNew(newState);
            umr.setZipNew(newZip);            
            umr.setCommentsNew(newComments);
            
            /*Requestor*/
            umr.setRequesterName(rName);
            umr.setRequesterEmail(rEmail);
            umr.setRequesterPhone(rPhone);
            umr.setRequesterManagerEmail(rMgrEmails);
            
            umr.setInternalEmail(getCSREmail());
            
            if (loginBean.getIsMarfieldSubdomain()) {
            	umr.setNoManagerApproval(true);
            }

            //Ensure that there is an email address before it is added to email list
            updateEmailList();
            umr.setEmailList(emailList);

            LOG.info("Sending emails to: {}",emailList);
            Map<String,Object> params = new HashMap<>();
            params.put("request", umr);
            params.put("emailList", emailList);

            workflowService.startUserRequest(id,params);
            clearAll();
        }

        return  REDIRECT_THANKYOU_PAGE;
    }

    public String submitCreateNovantUser() {
        LOG.info("Submitting Novant Create User Request");
        UserRequest ur = userRequestBp.createUserRequest();
        Long id = ur.getId();
        String token = ur.getToken();
        LOG.info("Request id {}, token {}", id, token);
        if (id != null) {
            //Add last remaining department           
            UserModificationRequestDto umr = new UserModificationRequestDto();

            /*New User*/
            umr.setRequestType(EmailFactoryBp.USER_REQUEST_CREATE);
            umr.setFirstName(firstName);
            umr.setLastName(lastName);
            umr.setCompanyEmail(companyEmail);
            umr.setEmail(email);
            umr.setPhone(phone);
            umr.setExt(ext);

            /*Ship To Address*/
            umr.setSAddress1(sAddress1);
            umr.setSAddress2(sAddress2);
            umr.setSCity(sCity);
            umr.setSState(sState);
            umr.setSZip(sZip);
            
            /*Novant does not require manager approval*/
            umr.setNoManagerApproval(true);
            
            umr.setInternalEmail(getCSREmail());
            updateEmailList();
            umr.setEmailList(emailList);
            LOG.info("Sending emails to: {}",emailList);
            Map<String,Object> params = new HashMap<>();
            params.put("request", umr);
            params.put("emailList", emailList);

            workflowService.startUserRequest(id,params);
            
            clearAll();
        }

        return  REDIRECT_THANKYOU_PAGE;
    }

    public void clearAll() {

        /*The sole purpose of the 'departmentManageFlag' is because when an action is perform on the departments section is performed
         * the clearAll method is called. The semmantics of jsf do not prevent this from happening, therefore, we needed to handle this within code.*/
        if (departmentManageFlag) {
            departmentManageFlag = false;
            return;
        }
        /*New User*/
        reason = "";
        userId = "";
        affiliation = "";
        isPSG = "";
        corpBuNum = "";
        firstName = "";
        lastName = "";
        companyEmail = "";
        email = "";
        phone = "";
        ext = "";
        emailList.clear();

        /*Ship-to Address*/
        sAddress1 = "";
        sAddress2 = "";
        sCity = "";
        sState = "";
        sZip = "";
        sDesktop = "";
        sDepartment = "";
        sCostCenter = "";
        sComments = "";
        mailstop = "";
        pole = "";

        /*Ship-to Location*/
        newAddress1 = "";
        newAddress2 = "";
        newCity = "";
        newState = "";
        newZip = "";
        newDesktop = "";
        newDepartment = "";
        newCostCenter = "";
        newComments = "";

        /*Departments*/
        dName = "";
        dDesktop = "";
        dCostCenter = "";
        dSameAsDefault = "";
        dAddress = "";
        dCity = "";
        dState = "";
        dZip = "";
        dDivDept = "";
        dManagerName = "";
        dManagerEmail = "";
        departmentsList.clear();

        /*Approver*/
        aName = "";
        aEmail = "";
        aPhone = "";

        /*Requestor*/
        rName = "";
        rEmail = "";
        rPhone = "";
        rMgrEmails = "";

    }

    private void updateEmailList() {
        addToEmailList(aEmail);
        addToEmailList(rMgrEmails);
        addToEmailList(getCSREmail());
    }

    private void addToEmailList(String emailAddress) {
        if (StringUtils.isNotBlank(emailAddress)) {
            emailList.add(emailAddress);
        }
    }

    private String getCSREmail() {

        return pageCssBean.getEmailUsAddress();
    }

    /*New User*/
    public String getRequestType() {
        return requestType;
    }

    public void setRequestType(String requestType) {
        this.requestType = requestType;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getEndUser() {
        return endUser;
    }

    public void setEndUser(String endUser) {
        this.endUser = endUser;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getAffiliation() {
        return affiliation;
    }

    public void setAffiliation(String affiliation) {
        this.affiliation = affiliation;
    }

    public String getIsPSG() {
        return isPSG;
    }

    public void setIsPSG(String isPSG) {
        this.isPSG = isPSG;
    }

    public String getCorpBuNum() {
        return corpBuNum;
    }

    public void setCorpBuNum(String corpBuNum) {
        this.corpBuNum = corpBuNum;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCompanyEmail() {
        return companyEmail;
    }

    public void setCompanyEmail(String email) {
        this.companyEmail = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getExt() {
        return ext;
    }

    public void setExt(String ext) {
        this.ext = ext;
    }

    /*Ship-to Address*/
    public String getSAddress1() {
        return sAddress1;
    }

    public void setSAddress1(String sAddress1) {
        this.sAddress1 = sAddress1;
    }

    public String getSAddress2() {
        return sAddress2;
    }

    public void setSAddress2(String sAddress2) {
        this.sAddress2 = sAddress2;
    }

    public String getSCity() {
        return sCity;
    }

    public void setSCity(String sCity) {
        this.sCity = sCity;
    }

    public String getSState() {
        return sState;
    }

    public void setSState(String sState) {
        this.sState = sState;
    }

    public String getSZip() {
        return sZip;
    }

    public void setSZip(String sZip) {
        this.sZip = sZip;
    }

    public String getSDesktop() {
        return sDesktop;
    }

    public void setSDesktop(String sDesktop) {
        this.sDesktop = sDesktop;
    }

    public String getSDepartment() {
        return sDepartment;
    }

    public void setSDepartment(String sDepartment) {
        this.sDepartment = sDepartment;
    }

    public String getSCostCenter() {
        return sCostCenter;
    }

    public void setSCostCenter(String sCostCenter) {
        this.sCostCenter = sCostCenter;
    }

    public String getSComments() {
        return sComments;
    }

    public void setSComments(String sComments) {
        this.sComments = sComments;
    }

    public String getMailstop() {
        return mailstop;
    }

    public void setMailstop(String mailstop) {
        this.mailstop = mailstop;
    }

    public String getPole() {
        return pole;
    }

    public void setPole(String pole) {
        this.pole = pole;
    }

    /*Ship-to New Address*/
    public String getNewAddress1() {
        return newAddress1;
    }

    public void setNewAddress1(String newAddress1) {
        this.newAddress1 = newAddress1;
    }

    public String getNewAddress2() {
        return newAddress2;
    }

    public void setNewAddress2(String newAddress2) {
        this.newAddress2 = newAddress2;
    }

    public String getNewCity() {
        return newCity;
    }

    public void setNewCity(String newCity) {
        this.newCity = newCity;
    }

    public String getNewState() {
        return newState;
    }

    public void setNewState(String newState) {
        this.newState = newState;
    }

    public String getNewZip() {
        return newZip;
    }

    public void setNewZip(String newZip) {
        this.newZip = newZip;
    }

    public String getNewComments() {
        return newComments;
    }

    public void setNewComments(String newComments) {
        this.newComments = newComments;
    }

    /*Departments*/
    public String getDMultipleDepts() {
        return dMultipleDepts;
    }

    public void setDMultipleDepts(String dMultipleDepts) {
        this.dMultipleDepts = dMultipleDepts;
    }

    public String getDName() {
        return dName;
    }

    public void setDName(String dName) {
        this.dName = dName;
    }

    public String getDDesktop() {
        return dDesktop;
    }

    public void setDDesktop(String dDesktop) {
        this.dDesktop = dDesktop;
    }

    public String getDCostCenter() {
        return dCostCenter;
    }

    public void setDCostCenter(String dCostCenter) {
        this.dCostCenter = dCostCenter;
    }

    public String getDSameAsDefault() {
        return dSameAsDefault;
    }

    public void setDSameAsDefault(String dSameAsDefault) {

        this.dSameAsDefault = dSameAsDefault;
    }

    public void valueChangeMethod() {
        if (dSameAsDefault.equals("Yes")) {
            this.dAddress = sAddress1 + (StringUtils.isEmpty(sAddress2) ? "" : "; " + sAddress2);
            this.dCity = sCity;
            this.dState = sState;
            this.dZip = sZip;
        }
        else {
            this.dAddress = "";
            this.dCity = "";
            this.dState = "";
            this.dZip = "";
        }
    }

    public String getDAddress() {
        return dAddress;
    }

    public void setDAddress(String dAddress) {
        this.dAddress = dAddress;
    }

    public String getDCity() {
        return dCity;
    }

    public void setDCity(String dCity) {
        this.dCity = dCity;
    }

    public String getDState() {
        return dState;
    }

    public void setDState(String dState) {
        this.dState = dState;
    }

    public String getDZip() {
        return dZip;
    }

    public void setDZip(String dZip) {
        this.dZip = dZip;
    }

    public String getDDivDept() {
        return dDivDept;
    }

    public void setDDivDept(String dDivDept) {
        this.dDivDept = dDivDept;
    }

    public String getDManagerName() {
        return dManagerName;
    }

    public void setDManagerName(String dManagerName) {
        this.dManagerName = dManagerName;
    }

    public String getDManagerEmail() {
        return dManagerEmail;
    }

    public void setDManagerEmail(String dManagerEmail) {
        this.dManagerEmail = dManagerEmail;
    }

    public List<DepartmentRequestDto> getDepartmentsList() {
        return this.departmentsList;
    }

    public String getRedirectToCreateUserPage() {
        return REDIRECT_TO_CREATE_USER_PAGE;
    }

    public String getRedirectToChangeUserPage() {
        return REDIRECT_TO_CHANGE_USER_PAGE;
    }

    /*Approver*/
    public String getAName() {
        return aName;
    }

    public void setAName(String aName) {
        this.aName = aName;
    }

    public String getAEmail() {
        return aEmail;
    }

    public void setAEmail(String aEmail) {
        this.aEmail = aEmail;
    }

    public String getAPhone() {
        return aPhone;
    }

    public void setAPhone(String aPhone) {
        this.aPhone = aPhone;
    }

    /*Requestor*/
    public String getRName() {
        return rName;
    }

    public void setRName(String rName) {
        this.rName = rName;
    }

    public String getREmail() {
        return rEmail;
    }

    public void setREmail(String rEmail) {
        this.rEmail = rEmail;
    }

    public String getRPhone() {
        return rPhone;
    }

    public void setRPhone(String rPhone) {
        this.rPhone = rPhone;
    }

    public String getRMgrEmails() {
        return rMgrEmails;
    }

    public void setRMgrEmails(String rMgrEmail) {
        this.rMgrEmails = rMgrEmail;
    }

    public boolean isDepartmentListEmpty() {
        return departmentsList == null || departmentsList.isEmpty();
    }

    public void setDepartmentListEmpty(boolean departmentListEmpty) {
        this.departmentListEmpty = departmentListEmpty;
    }
}
