/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.shopping.view.jsf.bean.data;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.model.CatalogXItem;
import com.apd.phoenix.service.model.DescriptionType;
import com.apd.phoenix.service.model.FavoritesList;
import com.apd.phoenix.service.model.HierarchyNode;
import com.apd.phoenix.service.model.Item;
import com.apd.phoenix.service.model.ItemPropertyType;
import com.apd.phoenix.service.model.ItemXItemPropertyType;
import com.apd.phoenix.service.model.Manufacturer;
import com.apd.phoenix.service.model.OrderStatus;
import com.apd.phoenix.service.model.Sku;

//import com.apd.phoenix.shopping.view.jsf.bean.favorites.FavoriteList;
//import com.apd.phoenix.shopping.view.jsf.bean.favorites.FavoriteList;

/**
 *
 * @author dcnorris
 */
public class MockDataFactory {

    private static final Logger logger = LoggerFactory.getLogger(MockDataFactory.class);

    public static class ItemFactory {

        public static Item stub(String itemName) {
            Item stub = new Item();
            stub.setName(itemName);
            stub.setDescription("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque semper, "
                    + "est et tincidunt hendrerit, purus felis ornare ante, et commodo sapien ipsum sit amet elit.");
            Manufacturer manufacturer = ManufacturerFactory.stub("N/A");
            stub.setManufacturer(manufacturer);
            return stub;
        }

        public static Item stub(String itemName, String description) {
            Item stub = new Item();
            stub.setName(itemName);
            stub.setDescription(description);
            Manufacturer manufacturer = ManufacturerFactory.stub("N/A");
            stub.setManufacturer(manufacturer);
            return stub;
        }

        public static Item stub(String itemName, String description, String manufacturerName) {
            Item stub = new Item();
            stub.setName(itemName);
            stub.setDescription(description);
            Manufacturer manufacturer = ManufacturerFactory.stub(manufacturerName);
            stub.setManufacturer(manufacturer);
            return stub;
        }

        public static ArrayList<Item> getItemList(int size) {
            ArrayList<Item> stub = new ArrayList<>();
            for (int i = 0; i < size; i++) {
                stub.add(ItemFactory.stub("Ladder "+ i));
            }
            return stub;
        }

        public static ArrayList<CatalogXItem> getCatalogItemList(int size) {
            ArrayList<CatalogXItem> stub = new ArrayList<>();
            for (int i = 0; i < size; i++) {
                Item item = ItemFactory.stub(
                        "3750 Commercial Grade Packaging Tape "+i,
                        "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque semper, est et tincidunt hendrerit, purus felis ornare ante, et commodo sapien ipsum sit amet elit.", "Scotch");
                ArrayList<ItemXItemPropertyType> properties = ItemXItemPropertyTypeFactory
                        .stub();
                item.setProperties(new HashSet<ItemXItemPropertyType>(properties));
                Sku customerSku = SkuFactory.stub("CUSKU" + i);
            	CatalogXItem catalogItem = new CatalogXItem();
                catalogItem.setItem(item);
                catalogItem.setCustomerSku(customerSku);
                catalogItem.setCustomerSku(customerSku);
                catalogItem.setPrice(new BigDecimal("100.00").add(new BigDecimal(i)));
                stub.add(catalogItem);
            }
            return stub;
        }

        public static ArrayList<CatalogXItem> retrieveSimilarItems(int size) {
            ArrayList<CatalogXItem> catalogItems = new ArrayList<CatalogXItem>();
            Item item = ItemFactory.stub("3750 Commercial Grade Packaging Tape",
                    "3750 Commercial Grade Packaging Tape", "Scotch");
            ArrayList<ItemXItemPropertyType> properties = ItemXItemPropertyTypeFactory.stub();
            item.setProperties(new HashSet<ItemXItemPropertyType>(properties));
            Sku customerSku = SkuFactory.stub("MMMM12345");
            for (int i = 0; i < size; i++) {
                CatalogXItem catalogItem = new CatalogXItem();
                catalogItem.setItem(item);
                catalogItem.setPrice(new BigDecimal("180.96"));
                catalogItem.setCustomerSku(customerSku);
                catalogItems.add(catalogItem);
            }
            return catalogItems;
        }
    }

    public static class ManufacturerFactory {

        public static Manufacturer stub(String name) {
            Manufacturer stub = new Manufacturer();
            stub.setName(name);
            return stub;
        }

        public static List<Manufacturer> manufacturerList() {
            List<Manufacturer> toReturn = new ArrayList<Manufacturer>();
            toReturn.add(new Manufacturer());
            toReturn.add(new Manufacturer());
            toReturn.get(0).setName("Manufacturer 1");
            toReturn.get(0).setId((long) 1);
            toReturn.get(1).setName("Manufacturer 2");
            toReturn.get(1).setId((long) 2);
            return toReturn;
        }

        public static Map<Manufacturer, Integer> manufacturerItemQuantities() {
            Map<Manufacturer, Integer> toReturn = new HashMap<Manufacturer, Integer>();
            toReturn.put(manufacturerList().get(0), 10);
            toReturn.put(manufacturerList().get(1), 20);
            return toReturn;
        }
    }

    public static class OrderStatusFactory {

        public static OrderStatus stub(String status) {
            OrderStatus stub = new OrderStatus();
            stub.setValue(status);
            return stub;
        }

        public static ArrayList<OrderStatus> getStatusList(int size) {
            ArrayList<OrderStatus> stub = new ArrayList<>();
            for (int i = 0; i < size; i++) {
                stub.add(OrderStatusFactory.stub("On Hold"));
            }
            return stub;
        }
    }

    public static class FavoritesListsFactory {

        public static FavoritesList createdFavoritesList = null;

        public static ArrayList<FavoritesList> getCatalogFavoritesLists(int size) {
            ArrayList<FavoritesList> stub = new ArrayList<FavoritesList>();

            FavoritesList tempList = new FavoritesList();
            //tempList.setItems(new HashSet<CatalogXItem>(ItemFactory.getCatalogItemList(5)));
            tempList.setName("Catalog List List 1");
            stub.add(tempList);

            return stub;
        }

        public static ArrayList<FavoritesList> getUserFavoritesLists(int size) {
            ArrayList<FavoritesList> stub = new ArrayList<FavoritesList>();

            FavoritesList tempList_1 = new FavoritesList();
            //tempList_1.setItems(new HashSet<CatalogXItem>(ItemFactory.getCatalogItemList(6)));
            tempList_1.setName("User Favorite List 1");
            stub.add(tempList_1);
            FavoritesList tempList_2 = new FavoritesList();
            //tempList_2.setItems(new HashSet<CatalogXItem>(ItemFactory.getCatalogItemList(4)));
            tempList_2.setName("User Favorite List 2");
            stub.add(tempList_2);
            if (createdFavoritesList != null) {
                //createdFavoritesList.setItems(new HashSet<CatalogXItem>(ItemFactory.getCatalogItemList(1)));
                stub.add(createdFavoritesList);
            }

            return stub;
        }

        public static void populateUserFavoritesList(FavoritesList list, int size) {
            //list.setItems(new HashSet<CatalogXItem>(ItemFactory.getCatalogItemList(size)));
        }

        public static FavoritesList getFavoritesList() {
            FavoritesList tempList = new FavoritesList();
            //tempList.setItems(new HashSet<CatalogXItem>(ItemFactory.getCatalogItemList(6)));
            return tempList;
        }

        public static void setCreatedFavoritesList(FavoritesList list) {
            createdFavoritesList = list;
        }

    }

    public static class SkuFactory {

        public static Sku stub(String value) {
            Sku stub = new Sku();
            stub.setValue(value);
            return stub;
        }
    }

    public static class ItemXItemPropertyTypeFactory {

        public static ItemXItemPropertyType stub(String propertyName, String propertyValue) {
            ItemPropertyType itemPropertyType = new ItemPropertyType();
            itemPropertyType.setName(propertyName);
            ItemXItemPropertyType property = new ItemXItemPropertyType();
            property.setType(itemPropertyType);
            property.setValue(propertyValue);
            return property;
        }

        public static ArrayList<ItemXItemPropertyType> stub() {
            ArrayList<ItemXItemPropertyType> properties = new ArrayList<ItemXItemPropertyType>();
            ItemXItemPropertyType property = ItemXItemPropertyTypeFactory.stub("Unit", "PK");
            properties.add(property);
            return properties;
        }
    }

    public static class DescriptionTypeFactory {

        public static DescriptionType stub(String name) {
            DescriptionType descType = new DescriptionType();
            descType.setName(name);
            return descType;
        }

    }

    public static class HierarchyFactory {

        public static List<HierarchyNode> breadcrumbs() {
            List<HierarchyNode> toReturn = new ArrayList<HierarchyNode>();
            toReturn.add(new HierarchyNode());
            toReturn.add(new HierarchyNode());
            toReturn.add(new HierarchyNode());
            toReturn.get(0).setDescription("Breakroom & Janitorial");
            toReturn.get(1).setDescription("Facilities Maintenance");
            toReturn.get(2).setDescription("Step Stools & Ladders");
            return toReturn;
        }

        public static List<HierarchyNode> nextLevelNodes() {
            List<HierarchyNode> toReturn = new ArrayList<HierarchyNode>();
            toReturn.add(new HierarchyNode());
            toReturn.add(new HierarchyNode());
            toReturn.add(new HierarchyNode());
            toReturn.add(new HierarchyNode());
            toReturn.get(0).setDescription("Small Ladders");
            toReturn.get(1).setDescription("Extension Ladders");
            toReturn.get(2).setDescription("A Frames");
            toReturn.get(3).setDescription("Step Stools");
            return toReturn;
        }

        public static List<HierarchyNode> highLevelNodes() {
            List<HierarchyNode> toReturn = new ArrayList<HierarchyNode>();
            toReturn.add(new HierarchyNode());
            toReturn.add(new HierarchyNode());
            toReturn.add(new HierarchyNode());
            toReturn.add(new HierarchyNode());
            toReturn.add(new HierarchyNode());
            toReturn.add(new HierarchyNode());
            toReturn.get(0).setDescription("Breakroom & Janitorial");
            toReturn.get(1).setDescription("Furniture & Accessories");

            HierarchyNode h21 = new HierarchyNode();
            h21.setDescription("Bookcases");
            //Furn. & Acc -> Bookcases -> (children)
            HierarchyNode h211 = new HierarchyNode();
            h211.setDescription("Wood");
            h21.getChildren().add(h211);
            HierarchyNode h212 = new HierarchyNode();
            h212.setDescription("Aluminum");
            h21.getChildren().add(h212);
            HierarchyNode h213 = new HierarchyNode();
            h213.setDescription("Steel");
            h21.getChildren().add(h213);

            HierarchyNode h22 = new HierarchyNode();
            h22.setDescription("Filing Cabinets");
            //Furn. & Acc -> Filing Cabinets -> (children)
            HierarchyNode h221 = new HierarchyNode();
            h221.setDescription("Wood");
            h22.getChildren().add(h221);
            HierarchyNode h222 = new HierarchyNode();
            h222.setDescription("Aluminum");
            h22.getChildren().add(h222);
            HierarchyNode h223 = new HierarchyNode();
            h223.setDescription("Steel");

            HierarchyNode h23 = new HierarchyNode();
            h23.setDescription("Storage Cabinets & Lockers");
            //Furn. & Acc -> Stor. Cab. & Loc. -> (children)
            HierarchyNode h231 = new HierarchyNode();
            h231.setDescription("Wood");
            h23.getChildren().add(h231);
            HierarchyNode h232 = new HierarchyNode();
            h232.setDescription("Aluminum");
            h23.getChildren().add(h232);
            HierarchyNode h233 = new HierarchyNode();
            h233.setDescription("Steel");
            h23.getChildren().add(h233);
            HierarchyNode h234 = new HierarchyNode();
            h234.setDescription("Refurbished");
            h23.getChildren().add(h234);
            HierarchyNode h235 = new HierarchyNode();
            h235.setDescription("Personal Employee");
            h23.getChildren().add(h235);

            toReturn.get(1).getChildren().add(h23);
            toReturn.get(1).getChildren().add(h21);
            toReturn.get(1).getChildren().add(h22);

            toReturn.get(2).setDescription("General Supplies");
            toReturn.get(3).setDescription("Office Supplies");
            toReturn.get(4).setDescription("Office / Desk Accessories");
            toReturn.get(5).setDescription("Technology");
            return toReturn;
        }
    }

    public static class FilterFactory {

        public static List<String> getFilterList() {
            ArrayList<String> currentFilterList = new ArrayList<String>();
            currentFilterList.add("Filter One");
            currentFilterList.add("Filter Two");
            return currentFilterList;
        }

        public static Map<String, List<String>> getFilterAttributes() {
            HashMap<String, List<String>> currentFilterAttributes = new HashMap<String, List<String>>();
            currentFilterAttributes.put("Filter One", new ArrayList<String>());
            currentFilterAttributes.put("Filter Two", new ArrayList<String>());
            currentFilterAttributes.get("Filter One").add("Attribute One");
            currentFilterAttributes.get("Filter One").add("Attribute Two");
            currentFilterAttributes.get("Filter One").add("Attribute Three");
            currentFilterAttributes.get("Filter Two").add("Attribute One");
            currentFilterAttributes.get("Filter Two").add("Attribute Two");
            currentFilterAttributes.get("Filter Two").add("Attribute Three");
            return currentFilterAttributes;
        }

        public static Map<String, Map<String, Boolean>> getFilterAttributesSelected() {
            HashMap<String, Map<String, Boolean>> currentFilterAttributesSelected = new HashMap<String, Map<String, Boolean>>();
            currentFilterAttributesSelected.put("Filter One", new HashMap<String, Boolean>());
            currentFilterAttributesSelected.put("Filter Two", new HashMap<String, Boolean>());
            currentFilterAttributesSelected.get("Filter One").put("Attribute One", true);
            currentFilterAttributesSelected.get("Filter One").put("Attribute Two", false);
            currentFilterAttributesSelected.get("Filter One").put("Attribute Three", true);
            currentFilterAttributesSelected.get("Filter Two").put("Attribute One", false);
            currentFilterAttributesSelected.get("Filter Two").put("Attribute Two", true);
            currentFilterAttributesSelected.get("Filter Two").put("Attribute Three", false);
            return currentFilterAttributesSelected;
        }

        public static Map<String, Boolean> getFilterAttributesShown() {
            HashMap<String, Boolean> currentFilterAttributesShown = new HashMap<String, Boolean>();
            currentFilterAttributesShown.put("Filter One", false);
            currentFilterAttributesShown.put("Filter Two", false);
            return currentFilterAttributesShown;
        }
    }

}
