package com.apd.phoenix.shopping.camel.routes;

import org.apache.camel.LoggingLevel;
import com.apd.phoenix.service.business.CustomerOrderBp;
import com.apd.phoenix.service.integration.cxml.model.cxml.Response;
import com.apd.phoenix.service.integration.cxml.model.cxml.Status;
import com.apd.phoenix.service.integration.multitenancy.AbstractTenantAwareRouteBuilder;
import com.apd.phoenix.service.model.dto.OrderLogDto.EventTypeDto;
import com.apd.phoenix.service.workflow.WorkflowService;
import com.apd.phoenix.shopping.camel.process.CxmlCxmlTranslator;
import com.apd.phoenix.shopping.camel.process.CxmlTransactionLogger;
import com.apd.phoenix.shopping.cxml.request.processors.CxmlProcessorException;

public class CxmlRequestTypeRouteBuilder extends AbstractTenantAwareRouteBuilder {

    private String cxmlOrderRequestSource;

    private String cxmlOrderRequestOutput;

    private static final String cxmlOrderRequestLogSource = "direct:cxmlOrderRequestLogSource";

    private String cxmlPunchOutSetupRequestSource;

    private String cxmlPunchOutSetupRequestOutput;

    private String cxmlPunchOutOrderMessageSource;

    private static final String cxmlPunchOutOrderMessageLogSource = "direct:cxmlPunchOutOrderMessageLogSource";

    @Override
    public void configureRouteImplementation() throws Exception {

        onException(Exception.class).maximumRedeliveries(0).handled(true).log(LoggingLevel.INFO,
                "Error in handling the cxml request or message.").setBody(constant("Error Occurred!"));

        onException(CxmlProcessorException.class).maximumRedeliveries(0).handled(true).log(LoggingLevel.INFO,
                "Error in cxml processing").setBody(constant(generateFailResponse()));

        //Punch out order message
        from(cxmlPunchOutOrderMessageSource).log(LoggingLevel.INFO, "processing punch out order message")
        //Log
                .wireTap(cxmlPunchOutOrderMessageLogSource)
                //Translate to Advanced Ship Notice
                .bean(CxmlCxmlTranslator.class, "fromPunchOutOrderMessageToPurchaseOrder").bean(CustomerOrderBp.class,
                        "createOrUpdatePunchoutOrder");

        //Log PunchOut Order Message
        from(cxmlPunchOutOrderMessageLogSource).setHeader("messageEventType",
                constant(EventTypeDto.PUNCHOUT_ORDER_MESSAGE)).bean(CxmlCxmlTranslator.class,
                "getPunchOutOrderMessageApdPo")
        //Set the body to the original cxml message
                .setBody(simple("${header.rawMessage}")).bean(CxmlTransactionLogger.class, "logCxmlTransaction");

        //Order Request
        from(cxmlOrderRequestSource).log(LoggingLevel.INFO, "processing order request")
        //Verify order
                .bean(CxmlCxmlTranslator.class, "verifyAndSetupOrderRequest")
                //Log
                .wireTap(cxmlOrderRequestLogSource)
                //Set Headers
                .setHeader("cxmlRequestType", constant("OrderRequestRequest"))
                //Translate to purchase order
                .bean(CxmlCxmlTranslator.class, "fromOrderToPurchaseOrder").bean(WorkflowService.class, "processOrder")
                //Generate response
                .setBody(constant(generateOKResponse()));

        //Log Order Request
        from(cxmlOrderRequestLogSource).setHeader("messageEventType", constant(EventTypeDto.ORDER_REQUEST))
        //Set the body to the original cxml message
                .setBody(simple("${header.rawMessage}")).bean(CxmlTransactionLogger.class, "logCxmlTransaction");

        //        //Log Punch Out Request
        //        from(cxmlPunchOutSetupRequestLogSource).setHeader("messageEventType",
        //                constant(EventTypeDto.PUNCHOUT_SETUP_REQUEST))
        //        //TODO: Set the correct APO PO
        //                .setHeader("apdPo", constant("APD23911991"))
        //                .bean(CxmlCxmlTranslator.class, "getPunchOutSetupRequestApdPo")
        //                //Set the body to the original cxml message
        //                .setBody(simple("${header.rawMessage}")).bean(CxmlTransactionLogger.class, "logCxmlTransaction");

    }

    private Response generateOKResponse() {
        Response response = new Response();
        Status status = new Status();
        status.setCode("200");
        status.setText("OK");
        response.setStatus(status);
        return response;
    }

    private Response generateFailResponse() {
        Response response = new Response();
        Status status = new Status();
        status.setCode("400");
        status.setText("Failed");
        response.setStatus(status);
        return response;
    }

    public String getCxmlOrderRequestSource() {
        return cxmlOrderRequestSource;
    }

    public void setCxmlOrderRequestSource(String cxmlOrderRequestSource) {
        this.cxmlOrderRequestSource = cxmlOrderRequestSource;
    }

    public String getCxmlOrderRequestOutput() {
        return cxmlOrderRequestOutput;
    }

    public void setCxmlOrderRequestOutput(String cxmlOrderRequestOutput) {
        this.cxmlOrderRequestOutput = cxmlOrderRequestOutput;
    }

    public String getCxmlPunchOutSetupRequestSource() {
        return cxmlPunchOutSetupRequestSource;
    }

    public void setCxmlPunchOutSetupRequestSource(String cxmlPunchOutSetupRequestSource) {
        this.cxmlPunchOutSetupRequestSource = cxmlPunchOutSetupRequestSource;
    }

    public String getCxmlPunchOutSetupRequestOutput() {
        return cxmlPunchOutSetupRequestOutput;
    }

    public void setCxmlPunchOutSetupRequestOutput(String cxmlPunchOutSetupRequestOutput) {
        this.cxmlPunchOutSetupRequestOutput = cxmlPunchOutSetupRequestOutput;
    }

    public String getCxmlPunchOutOrderMessageSource() {
        return cxmlPunchOutOrderMessageSource;
    }

    public void setCxmlPunchOutOrderMessageSource(String cxmlPunchOutOrderMessageSource) {
        this.cxmlPunchOutOrderMessageSource = cxmlPunchOutOrderMessageSource;
    }
}
