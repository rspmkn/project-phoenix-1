/**
 * 
 */
package com.apd.phoenix.shopping.view.jsf.bean.workflow;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.Stateful;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.workflow.RemoteTaskService;
import com.apd.phoenix.service.workflow.RemoteTaskService.RemoteTaskSummary;

@Named
@Stateful
@SessionScoped
public class WorkflowTaskBean implements Serializable {

    private static final String HUMAN_TASK_FORM = "/ecommerce/task/humanTaskForm.xhtml?faces-redirect=true";

    private static final Logger logger = LoggerFactory.getLogger(WorkflowTaskBean.class);

    private static final long serialVersionUID = -7878893937965889019L;

    private Long id;

    private String user;

    private Long taskToWork;

    private String taskToWorkName;

    private List<RemoteTaskSummary> assignedTasks;

    private boolean showingCompletedTasks;

    @Inject
    private RemoteTaskService remoteTaskService;

    @PostConstruct
    public void init() {
        logger.info("Initializing wtb");
        ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
        user = context.getUserPrincipal().getName();
        logger.info("...for user " + user);
    }

    public void refreshList() {
    	assignedTasks = remoteTaskService.getAssignedTasks(user);
    	if (assignedTasks == null) {
    		assignedTasks = new ArrayList<>();
    	}
    }

    public void retrieve() {
        this.refreshList();
        if (FacesContext.getCurrentInstance().isPostback()) {
            return;
        }
    }

    public String startTask(RemoteTaskSummary task) {
        logger.info("Starting taskid=" + task.getId() + " taskname=" + task.getName());
        this.taskToWork = task.getId();
        this.taskToWorkName = task.getName();
        return HUMAN_TASK_FORM;
    }

    public String getDisplayStatus(RemoteTaskSummary task) {
        return (task != null && task.getCanBeWorked() != null && task.getCanBeWorked()) ? "Open" : "Closed";
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the taskToWork
     */
    public Long getTaskToWork() {
        return taskToWork;
    }

    /**
     * @param taskToWork
     *            the taskToWork to set
     */
    public void setTaskToWork(Long taskToWork) {
        this.taskToWork = taskToWork;
    }

    public boolean isShowingCompletedTasks() {
        return showingCompletedTasks;
    }

    public void setShowingCompletedTasks(boolean showingCompletedTasks) {
        this.showingCompletedTasks = showingCompletedTasks;
    }

    public String getTaskToWorkName() {
        return taskToWorkName;
    }

    public void setTaskToWorkName(String taskToWorkName) {
        this.taskToWorkName = taskToWorkName;
    }

    public List<RemoteTaskSummary> getAssignedTasks() {
        return this.assignedTasks;
    }

}
