/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.shopping.view.jsf.bean.search;

/**
 *
 * @author dcnorris
 */
public interface Searcher {

    static final String SEARCH_PAGE_BASE_URL = "/shopping/ecommerce/product/browse.xhtml";
    static final String HIERARCHY_KEY_PARAMETER = "hierarchyKey";
    static final String HIERARCHY_DELIMITER = "!";
    static final String ALL_HIERARCHY_PATH = "!All";
    static final String DELIMITER = "|||";
    static final String MB_ID_PARAM = "mb";
    public static final String INK_TONER_MANUFACTURER = "inkTonerManufacturer";
    public static final String INK_TONER_MODEL = "inkTonerModel";
}
