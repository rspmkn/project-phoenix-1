/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.shopping.cxml.request.processors;

import com.apd.phoenix.service.business.AccountBp;
import com.apd.phoenix.service.business.AccountXCredentialXUserBp;
import com.apd.phoenix.service.business.CredentialBp;
import com.apd.phoenix.service.business.CxmlConfigurationBp;
import com.apd.phoenix.service.business.SystemUserBp;
import com.apd.phoenix.service.model.Account;
import com.apd.phoenix.service.model.CxmlConfiguration;
import com.apd.phoenix.shopping.camel.service.api.CxmlService;
import com.apd.phoenix.shopping.cxml.BaseCxmlProcessor;
import java.io.StringWriter;
import javax.inject.Inject;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

/**
 *
 * @author dnorris
 */
public abstract class BaseRequestProcessor extends BaseCxmlProcessor implements RequestProcessor {

    @Inject
    protected CxmlService cxmlService;

    @Inject
    protected CxmlConfigurationBp cxmlConfigurationBp;

    @Inject
    protected AccountBp accountBp;

    @Inject
    protected AccountXCredentialXUserBp axcxuBp;

    @Inject
    protected SystemUserBp systemUserBp;

    @Inject
    protected CredentialBp credentialBp;

    protected String marshallToString(JAXBContext pContext, Object pObject) throws JAXBException {
        StringWriter sw = new StringWriter();
        Marshaller marshaller = pContext.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
        marshaller.marshal(pObject, sw);
        return sw.toString();
    }

    protected String getSystemUserXpathFromAccount(Account account) {
        CxmlConfiguration cXmlConfiguration = account.getCxmlConfiguration();
        return cXmlConfiguration.getSystemUserXpathExpression();
    }

    protected String getCredentialNameXpathFromAccount(Account account) {
        CxmlConfiguration cXmlConfiguration = account.getCxmlConfiguration();
        return cXmlConfiguration.getCredentialNameXpathExpression();
    }
}
