package com.apd.phoenix.shopping.view.jsf.bean.product;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.shopping.view.jsf.bean.search.SearchHierarchy;
import com.apd.phoenix.shopping.view.jsf.bean.search.SearchUtilities;
import com.apd.phoenix.shopping.view.jsf.bean.search.Searcher;

/**
 *
 * @author dcnorris
 */
public class Breadcrumb implements Searcher {

    private static final Logger logger = LoggerFactory.getLogger(Breadcrumb.class);
    private List<SearchHierarchy> crumbs;
    private SearchHierarchy tail;

    public Breadcrumb() {
    }

    public Breadcrumb(List<SearchHierarchy> hierarchyPath) {
        if (hierarchyPath != null) {
            parseHierarchyPath(hierarchyPath);
        }
    }

    private void parseHierarchyPath(List<SearchHierarchy> hierarchyPath) {
        crumbs = new ArrayList<>();
        for (SearchHierarchy hierarchy : hierarchyPath) {
            logger.debug("Added to breadcrumb list:" + hierarchy.getDisplayName());
            crumbs.add(hierarchy);
        }
        //Store Tail
        tail = crumbs.get(crumbs.size() - 1);
        //Remove tail
        crumbs.remove(crumbs.size() - 1);
    }

    public List<SearchHierarchy> getCrumbs() {
        if (crumbs == null) {
            return new ArrayList<>();
        }
        return crumbs;
    }

    public String getCrumbURL(SearchHierarchy crumb) {
        StringBuilder url = new StringBuilder(SEARCH_PAGE_BASE_URL);
        try {
            url.append("?").append(HIERARCHY_KEY_PARAMETER);
            url.append("=");
            url.append(URLEncoder.encode(crumb.getSearchEngineKey(), "UTF-8"));
            url.append("&");
            url.append(SearchUtilities.NEW_SEARCH_PARAMETER);
            url.append("=");
            url.append(Boolean.TRUE);
        }
        catch (UnsupportedEncodingException ex) {
            logger.error("Error url decoding hierarchy path string", ex);
        }
        return url.toString();
    }

    public void setCrumbs(List<SearchHierarchy> crumbs) {
        this.crumbs = crumbs;
    }

    public SearchHierarchy getTail() {
        return tail;
    }
}
