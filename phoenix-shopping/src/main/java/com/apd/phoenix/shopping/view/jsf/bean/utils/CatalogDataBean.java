package com.apd.phoenix.shopping.view.jsf.bean.utils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.business.CatalogBp;
import com.apd.phoenix.service.business.FavoritesListBp;
import com.apd.phoenix.service.model.Catalog;
import com.apd.phoenix.service.model.FavoritesList;
import com.apd.phoenix.service.model.TopLevelHierarchyXCatalog;
import com.apd.phoenix.service.product.CatalogData;
import com.apd.phoenix.service.product.CatalogData.FavoritesListInfo;
import com.apd.phoenix.service.product.CatalogDataCacheManager;
import com.apd.phoenix.shopping.view.jsf.bean.login.CredentialSelectionBean;
import com.apd.phoenix.shopping.view.jsf.bean.search.CategoryMenu;
import com.apd.phoenix.shopping.view.jsf.bean.search.SearchEngine;

/**
 * This is a utility bean, used to fetch cached catalog data
 * 
 * @author RHC
 *
 */
@Named
@Stateless
public class CatalogDataBean implements Serializable {

    private static final long serialVersionUID = 8697562319435603407L;

    private static final Logger logger = LoggerFactory.getLogger(CatalogDataBean.class);

    @Inject
    private CatalogDataCacheManager catalogCacheManager;

    @Inject
    private SearchEngine searchEngine;

    @Inject
    private CredentialSelectionBean credentialSelectionBean;

    @Inject
    private FavoritesListBp favoritesListBp;

    @Inject
    private CatalogBp catalogBp;

    public CatalogData getCatalogData() {

		Catalog catalog = credentialSelectionBean.getCurrentCredential().getCatalog();
        //note: when testing, if you want to clear this cached data, just pull up the customer catalog in the
        //admin app and hit save. 
        CatalogData data = this.catalogCacheManager.getCachedCatalogData(catalog.getId(), searchEngine.getSearchType());
        if (data == null) {
            logger.debug("Regenerating catalog data");
            data = new CatalogData();
            data.setCategoryData(searchEngine.buildCategoryMenu());
            data.setMatchbookManufacturers(searchEngine.matchbookManufacturersFromCatalog(credentialSelectionBean
                    .getCatalogIdList()));
            Map<String, Integer> catalogPriorities = new HashMap<>();
            for (TopLevelHierarchyXCatalog node : credentialSelectionBean.getCurrentCredential().getCatalog().getTopLevelHierarchies()) {
            	Integer previousPopularity = catalogPriorities.get(node.getDescription());
            	if (node.getOrdering() != null && (previousPopularity == null || previousPopularity > node.getOrdering())) {
            		catalogPriorities.put(node.getDescription(), node.getOrdering());
            	}
            }
            ((CategoryMenu)data.getCategoryData()).setPopularTopLevelCategories(catalogPriorities);
            //also ensure that all empty company favorites lists are removed, in case any were cleared 
            //since the data was last fetched
            favoritesListBp.removeEmptyCompanyLists(credentialSelectionBean.getCatalogIdList());
            //then gets the company favorites lists that should be displayed
            ArrayList<FavoritesListInfo> favoritesInfoList = new ArrayList<>();
            for (FavoritesList list : catalogBp.getFavoritesLists(credentialSelectionBean.getCurrentCredential().getCatalog())) {
            	FavoritesListInfo newInfo = new FavoritesListInfo();
            	newInfo.setId(list.getId());
            	newInfo.setName(list.getName());
            	favoritesInfoList.add(newInfo);
            	data.setCompanyFavoritesLists(favoritesInfoList);
            }
            catalogCacheManager.setCachedCatalogData(catalog.getId(), searchEngine.getSearchType(), data);
        }
        return data;
	}
}
