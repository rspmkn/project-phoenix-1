package com.apd.phoenix.shopping.view.jsf.bean.login;

import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Stateful;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.model.Catalog.SearchType;
import com.apd.phoenix.shopping.view.jsf.bean.search.SearchEngine;

/**
 *
 * @author RHC
 */
@Named
@Stateful
@SessionScoped
@Lock(LockType.READ)
public class SearchEngineTypeBean {

    private static final Logger logger = LoggerFactory.getLogger(SearchEngineTypeBean.class);

    private SearchType searchType;

    @Inject
    private SearchEngine searchEngine;

    public SearchType getSearchType() {
        return searchType;
    }

    public void setSearchType(SearchType searchType) {
        this.searchType = searchType;
        if (this.searchEngine == null || !this.searchEngine.isSearchEngineAvailable()) {
            logger.warn("Search engine " + searchType + " is not available, using Solr");
            this.searchType = SearchType.SOLR_SEARCH;
        }
    }
}
