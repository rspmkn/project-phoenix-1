package com.apd.phoenix.shopping.view.jsf.bean.utils;

import java.io.IOException;
import java.io.InputStream;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ShoppingPropertiesLoader {

    private static Logger logger = LoggerFactory.getLogger(ShoppingPropertiesLoader.class);
    private PropertiesConfiguration validationProperties;
    private PropertiesConfiguration displayProperties;
    private static final ShoppingPropertiesLoader singleton = new ShoppingPropertiesLoader();

    public static ShoppingPropertiesLoader getInstance() {
        return singleton;
    }

    private ShoppingPropertiesLoader() {

        validationProperties = this.loadProperties("com/apd/phoenix/messages/validation.properties");
        displayProperties = this.loadProperties("com/apd/phoenix/strings/display.properties");
    }

    public PropertiesConfiguration getValidationProperties() {
        return validationProperties;
    }

    public PropertiesConfiguration getDisplayProperties() {
        return displayProperties;
    }

    private PropertiesConfiguration loadProperties(String uri) {
        PropertiesConfiguration toReturn = new PropertiesConfiguration();
    	
        try(InputStream is = ShoppingPropertiesLoader.class.getClassLoader().getResourceAsStream(uri)) {
            toReturn.load(is);
        }
        catch (ConfigurationException | IOException ex) {
            logger.error("Could not load \"ecommerceProperties.properties\": ", ex);
        }
        
        return toReturn;
    }
}
