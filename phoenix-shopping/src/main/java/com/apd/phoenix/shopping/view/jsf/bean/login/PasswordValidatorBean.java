package com.apd.phoenix.shopping.view.jsf.bean.login;

import java.io.Serializable;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.validation.constraints.Size;
import com.apd.phoenix.service.business.DuplicatePasswordException;
import com.apd.phoenix.service.business.InsecurePasswordException;
import com.apd.phoenix.service.business.SystemUserBp;
import com.apd.phoenix.shopping.view.jsf.bean.utils.ShoppingPropertiesLoader;

@ManagedBean
@RequestScoped
public class PasswordValidatorBean implements Serializable {

    private static final long serialVersionUID = 1L;

    private String currentPassword = "";

    @Size(min = 5, max = 15, message = "Password length must be between {min} and {max} characters.")
    private String password = "";

    private String confirm = "";

    @Inject
    private SystemUserBp systemUserBp;

    @Inject
    private LoginBean loginBean;

    public boolean passwordsMatch() {
        return password.equals(confirm);
    }

    public void storeNewPassword() {

        if (!passwordsMatch()) {
            String message = ShoppingPropertiesLoader.getInstance().getValidationProperties().getString(
                    "user.password.error.notmatch");
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, message, message));
            return;
        }

        try {
            systemUserBp.encryptAndSetStrongPassword(loginBean.getSystemUser(), password);
            loginBean.setSystemUser(systemUserBp.update(loginBean.getSystemUser()));
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO, "Successfully changed!", "Successfully changed!"));
        }
        catch (DuplicatePasswordException e) {
            String message = ShoppingPropertiesLoader.getInstance().getValidationProperties().getString(
                    "user.password.error.notdifferent");
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, message, message));
        }
        catch (InsecurePasswordException e) {
            String message = ShoppingPropertiesLoader.getInstance().getValidationProperties().getString(
                    "user.password.error.insecure");
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, message, message));
            return;
        }
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setConfirm(String confirm) {
        this.confirm = confirm;
    }

    public String getPassword() {
        return password;
    }

    public String getConfirm() {
        return confirm;
    }

    public String getCurrentPassword() {
        return currentPassword;
    }

    public void setCurrentPassword(String currentPassword) {
        this.currentPassword = currentPassword;
    }
}