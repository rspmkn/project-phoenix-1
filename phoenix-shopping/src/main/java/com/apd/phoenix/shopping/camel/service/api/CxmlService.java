package com.apd.phoenix.shopping.camel.service.api;

import com.apd.phoenix.service.model.Account;

public interface CxmlService {

    public com.apd.phoenix.service.integration.cxml.model.fulfill.Response processShipNoticeRequest(
            com.apd.phoenix.service.integration.cxml.model.fulfill.ShipNoticeRequest shipNoticeRequest,
            String rawMessage, String payloadId);

    public com.apd.phoenix.service.integration.cxml.model.cxml.Response processOrderRequest(
            com.apd.phoenix.service.integration.cxml.model.cxml.OrderRequest orderRequest, String rawMessage,
            String payloadId, Account account);

    public com.apd.phoenix.service.integration.cxml.model.cxml.Response processPunchoutSetupRequest(
            com.apd.phoenix.service.integration.cxml.model.cxml.PunchOutSetupRequest punchoutSetupRequest,
            String rawMessage, String payloadId);

    // Returns quote ID (e.g. requisition ID)
    public String processPunchoutOrderMessage(
            com.apd.phoenix.service.integration.cxml.model.cxml.PunchOutOrderMessage punchoutOrderMessage,
            String rawMessage, String payloadId);
}
