package com.apd.phoenix.shopping.stockcheck;

import javax.inject.Inject;
import com.apd.phoenix.shopping.view.jsf.bean.login.CredentialSelectionBean;

public abstract class AbstractStockCheck implements StockCheck {

    public static final String YES_VALUE = "Y";
    private static final String STOCK_CHECK_ALL_FACILITIES_PROPERTY_TYPE = "check all facilities (ADOT)";
    private static final String STOCK_CHECK_ZIP_PROPERTY_TYPE = "zip code for stock check";

    @Inject
    protected CredentialSelectionBean credBean;

    protected String getZipCode() {
        return credBean.getPropertyMap().get(STOCK_CHECK_ZIP_PROPERTY_TYPE);
    }

    protected boolean isAllFacilitiesChecked() {
        return YES_VALUE.equals(credBean.getPropertyMap().get(STOCK_CHECK_ALL_FACILITIES_PROPERTY_TYPE));
    }
}
