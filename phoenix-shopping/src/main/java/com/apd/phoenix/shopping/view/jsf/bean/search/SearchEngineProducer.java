package com.apd.phoenix.shopping.view.jsf.bean.search;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.enterprise.context.ConversationScoped;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import javax.inject.Named;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.model.Catalog.SearchType;
import com.apd.phoenix.shopping.view.jsf.bean.login.SearchEngineTypeBean;

@Named
@Stateless
@LocalBean
public class SearchEngineProducer {

    private static final Logger logger = LoggerFactory.getLogger(SearchEngineProducer.class);

    @Inject
    private SearchEngineTypeBean searchEngineType;

    @Inject
    @SolrSearch
    private SearchEngine solrSearch;

    @Inject
    @SmartSearch
    private SearchEngine smartSearch;

    @Produces
    @ConversationScoped
    public SearchEngine getEngine() {
        SearchType searchType = this.searchEngineType.getSearchType();
        if (searchType == null) {
            logger.warn("No search type specified, using Solr");
            return solrSearch;
        }
        switch (this.searchEngineType.getSearchType()) {
            case SMART_SEARCH:
                if (logger.isDebugEnabled()) {
                    logger.debug("Getting smart search engine");
                }
                return smartSearch;
            case SOLR_SEARCH:
                if (logger.isDebugEnabled()) {
                    logger.debug("Getting solr search engine");
                }
                return solrSearch;
            default:
                if (logger.isDebugEnabled()) {
                    logger.debug("Unimplemented search engine specified, using Solr");
                }
                return solrSearch;
        }
    }
}
