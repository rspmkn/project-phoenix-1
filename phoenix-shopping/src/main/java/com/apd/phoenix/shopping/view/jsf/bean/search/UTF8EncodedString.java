package com.apd.phoenix.shopping.view.jsf.bean.search;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

public class UTF8EncodedString {

    private String unencodedString;
    private String encodedString;
    private static final String urlEncoding = StandardCharsets.UTF_8.name();

    public UTF8EncodedString(String string) {
        this.unencodedString = string;
        try {
            this.encodedString = URLEncoder.encode(string, urlEncoding);
        }
        catch (UnsupportedEncodingException e) {
            encodedString = "";
        }
    }

    @Override
    public String toString() {
        return getEncodedString();
    }

    public String getEncodedString() {
        return encodedString;
    }

    public String getUnencodedString() {
        return unencodedString;
    }

    public void setString(String string) {
        this.unencodedString = string;
        try {
            this.encodedString = URLEncoder.encode(string, urlEncoding);
        }
        catch (UnsupportedEncodingException e) {
            encodedString = "";
        }
    }

    public void setUnencodedString(String unencodedString) {
        this.unencodedString = unencodedString;
    }

}
