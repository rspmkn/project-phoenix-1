package com.apd.phoenix.shopping.view.jsf.bean.order;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.AccessTimeout;
import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.SessionContext;
import javax.ejb.Stateful;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.ConversationScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.model.SelectItem;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.apd.phoenix.service.business.CustomerOrderBp;
import com.apd.phoenix.service.business.CustomerOrderSearchCriteria;
import com.apd.phoenix.service.business.OrderStatusBp;
import com.apd.phoenix.service.model.Account;
import com.apd.phoenix.service.model.Address;
import com.apd.phoenix.service.model.CustomerOrder;
import com.apd.phoenix.service.model.MiscShipTo;
import com.apd.phoenix.service.model.OrderStatus;
import com.apd.phoenix.service.model.PoNumber;
import com.apd.phoenix.service.persistence.jpa.OrderStatusDao;
import com.apd.phoenix.service.persistence.jpa.PoNumberDao;
import com.apd.phoenix.shopping.view.jsf.bean.login.CredentialSelectionBean;
import com.apd.phoenix.shopping.view.jsf.bean.login.LoginBean;
import com.apd.phoenix.core.PageNumberDisplay;
import com.apd.phoenix.core.PageNumberDisplay.PageSize;
import com.apd.phoenix.service.model.Credential;
import com.apd.phoenix.service.model.OrderStatus.OrderStatusEnum;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang.StringEscapeUtils;

@Named
@Stateful
@ConversationScoped
@Lock(LockType.READ)
@AccessTimeout(unit = TimeUnit.SECONDS, value = 30)
public class OrderHistoryBean implements Serializable {

    private Long id;
    private static final String DOWNLOAD_CONTENT_TYPE = "application/x-force-download";
    private static final String ATTACHMENT_HEADER = "Content-disposition";
    private static final String ATTACHMENT_VALUE = "attachment; filename=download.csv";

    private PageNumberDisplay listPaginationWidget;
    private static final PageSize pageSize = PageSize.FIVE;
    private int numFound;
    private int previousFavoritesListPage;
    private String addOrderId;
    private RequestLimitation requestLimitation = RequestLimitation.USER_ONLY;
    private boolean displayNoResultsMessage = false;
    private DateRanges dateRange = DateRanges.LAST_10_DAYS;

    @Inject
    Conversation conversation;

    @Inject
    CustomerOrderBp customerOrderBp;

    @Inject
    OrderStatusBp orderStatusBp;

    @Inject
    PoNumberDao poNumberDao;

    @Inject
    LoginBean loginBean;

    @Inject
    CredentialSelectionBean credentialSelectionBean;

    @Inject
    OrderStatusDao orderStatusDao;

    @PostConstruct
    public void create() {
        this.retrieveOrderStatusList();
    }

    private String selectedPO;
    private BigDecimal selectedShipmentAmount;
    private Date selectedStartDate;
    private Date selectedEndDate;
    private List<OrderStatus> statusList;
    private OrderStatus selectedStatus;
    private List<CustomerOrder> orderList = new ArrayList<>();

    private static final Logger logger = LoggerFactory
            .getLogger(OrderHistoryBean.class);

    public void retrieve() {
        if (FacesContext.getCurrentInstance().isPostback()) {
            return;
        }
    }

    private void endConversation() {
        if (!conversation.isTransient()) {
            conversation.end();
        }
    }

    private void beginConversation() {
        if (conversation.isTransient()) {
            conversation.begin();
        }
    }

    public void retrieveOrderHistory() {
        endConversation();
        beginConversation();
        Account account = credentialSelectionBean.getCurrentAXCXU().getAccount();
        String userId = null;
        if (this.requestLimitation.equals(RequestLimitation.USER_ONLY)) {
            userId = credentialSelectionBean.getCurrentAXCXU().getUser().getLogin() + "";
        }
        getStartAndEndDate();        
        CustomerOrderSearchCriteria criterion = new CustomerOrderSearchCriteria(selectedStartDate,
                selectedEndDate,
                null,
                selectedPO,
                account,
                userId,
                null,
                null,
                null,
                0,
                pageSize.toInt(),
                null);
        numFound = customerOrderBp.getCustomerOrdersCount(criterion);
        setListPaginationWidget(new PageNumberDisplay(this.numFound));
        getListPaginationWidget().setPageSize(pageSize);
        int start = (getListPaginationWidget().getPage() - 1) * getListPaginationWidget().getSize();
        criterion.setStart(start);
        this.orderList = customerOrderBp.getCustomerOrdersPaginated(criterion);
        previousFavoritesListPage = 0;
        setDisplayNoResultsMessage(true);
    }

    public void retrieveOrderStatusList() {
        this.statusList = orderStatusBp.getStatusList();
    }

    public Date getSelectedStartDate() {
        return this.selectedStartDate;
    }

    public void setSelectedStartDate(Date date) {
        this.selectedStartDate = date;
    }

    public Date getSelectedEndDate() {
        return this.selectedEndDate;
    }

    public void setSelectedEndDate(Date date) {
        this.selectedEndDate = date;
    }

    public List<OrderStatus> getStatusList() {
        return this.statusList;
    }

    public void setStatusList(List<OrderStatus> list) {
        this.statusList = list;
    }

    public OrderStatus getSelectedStatus() {
        return this.selectedStatus;
    }

    public void setSelectedStatus(OrderStatus status) {
        this.selectedStatus = status;
    }

    public List<CustomerOrder> getOrderList() {
        if (listPaginationWidget != null) {
            if (listPaginationWidget.getPage() != previousFavoritesListPage) {
                previousFavoritesListPage = listPaginationWidget.getPage();
                int start = (listPaginationWidget.getPage() - 1) * listPaginationWidget.getSize();
                Account account = credentialSelectionBean.getCurrentAXCXU().getAccount();
                String userId = null;
                if (this.requestLimitation.equals(RequestLimitation.USER_ONLY)) {
                    userId = credentialSelectionBean.getCurrentAXCXU().getUser().getLogin() + "";
                }
                Credential credential = null;
                if (this.requestLimitation.equals(RequestLimitation.CREDENTIAL)) {
                    credential = credentialSelectionBean.getCurrentCredential();
                }
                CustomerOrderSearchCriteria criterion = new CustomerOrderSearchCriteria(selectedStartDate,
                        selectedEndDate,
                        selectedStatus,
                        selectedPO,
                        account,
                        credential,
                        userId,
                        null,
                        null,
                        null,
                        start,
                        pageSize.toInt(),
                        null);
                this.orderList = customerOrderBp.getCustomerOrdersPaginated(criterion);
            }
        }
        return this.orderList;
    }

    public void setOrderList(List<CustomerOrder> list) {
        this.orderList = list;
    }

    public void setSelectedPO(String po) {
        this.selectedPO = po;
    }

    public String getSelectedPO() {
        return this.selectedPO;
    }

    public void setSelectedShipmentAmount(BigDecimal amt) {
        this.selectedShipmentAmount = amt;
    }

    public BigDecimal getSelectedShipmentAmount() {
        return this.selectedShipmentAmount;
    }

    public OrderStatus getOrderStatusById(String id) {
        return this.orderStatusDao.getOrderStatus(Long.valueOf(id));
    }

    public PoNumber getPoNumber(String value) {
        PoNumber toReturn = this.poNumberDao.getPoNumber(value);
        if (toReturn == null) {
            toReturn = new PoNumber();
            toReturn.setValue(value);
            toReturn.setId(-1l);
        }
        return toReturn;
    }

    @Resource
    private SessionContext sessionContext;

    public Converter getPoConverter() {

        final OrderHistoryBean ejbProxy = this.sessionContext
                .getBusinessObject(OrderHistoryBean.class);

        return new Converter() {

            @Override
            public Object getAsObject(FacesContext context,
                    UIComponent component, String value) {

                if (value.equals("")) {
                    return null;
                }

                return ejbProxy.getPoNumber(value);
            }

            @Override
            public String getAsString(FacesContext context,
                    UIComponent component, Object value) {
                if (value == null) {
                    return "";
                }
                return ((PoNumber) value).getValue();
            }
        };
    }

    public Converter getStatusConverter() {

        final OrderHistoryBean ejbProxy = this.sessionContext
                .getBusinessObject(OrderHistoryBean.class);

        return new Converter() {

            @Override
            public Object getAsObject(FacesContext context,
                    UIComponent component, String value) {

                if (value.equals("")) {
                    return null;
                }

                return ejbProxy.getOrderStatusById(value);
            }

            @Override
            public String getAsString(FacesContext context,
                    UIComponent component, Object value) {
                if (value == null) {
                    return "";
                }
                return String.valueOf(((OrderStatus) value).getId());
            }
        };
    }

    public List<String> getCustomerOrderCSV(List<CustomerOrder> customerOrders) {
        Map<String, String[]> csv = new HashMap<>();
        //used to define the ordering of the columns
        List<String> columns = new ArrayList<>(Arrays.asList("Order PO", "Customer PO", "Contact", "Account", "Credential", "User", "Order Date", "Order Type", "Status", "Total", "Last Ship Date"));
        for (String s : columns) {
            csv.put(s, new String[customerOrders.size()]);
        }
        int i = 0;
        for (CustomerOrder co : customerOrders) {
            csv.get("Order PO")[i] = co.getApdPo() == null ? "" : co.getApdPo().getValue();
            csv.get("Customer PO")[i] = co.getCustomerPo() == null ? "" : co.getCustomerPo().getValue();
            csv.get("Contact")[i] = co.getAccount() == null ? "" : co.getAccount().getPrimaryContact() == null ? "" : co.getAccount().getPrimaryContact().toString().replace(",", "");
            csv.get("Account")[i] = co.getAccount() == null ? "" : co.getAccount().getName();
            csv.get("Credential")[i] = co.getCredential() == null ? "" : co.getCredential().getName();
            csv.get("User")[i] = co.getUser() == null ? "" : co.getUser().getLogin();
            csv.get("Order Date")[i] = co.getOrderPlacedDate() == null ? "" : new SimpleDateFormat("MM/dd/yyyy").format(co.getOrderPlacedDate());
            csv.get("Order Type")[i] = co.getType() == null ? "" : co.getType().getValue().replace(",", "");
            csv.get("Status")[i] = co.getStatus() == null ? "" : co.getStatus().getValue().replace(",", "");
            csv.get("Total")[i] = co.getOrderTotal() == null ? "" : (co.getOrderTotal().compareTo(new BigDecimal(".001")) < 0 ? "" : co.getOrderTotal() + "");
            csv.get("Last Ship Date")[i] = co.getLastShipDate() == null ? "" : co.getLastShipDate().toString();
            Address address = customerOrderBp.getAddress(co);
            MiscShipTo mst = address.getMiscShipTo();
            if (mst != null) {
                Method[] methods = mst.getClass().getDeclaredMethods();
                //must use reflection to determine which fields of the MiscShipTo have values set and add those fields to the columns list/csv map
                for (Method method : methods) {
                    String name = method.getName();
                    if (name.startsWith("get")) { //method is a getter
                        String fieldName = name.replace("get", "");
                        String formattedField = fieldName;
                        int offset = 0;
                        for (int j = 1; j < fieldName.length(); j++) {
                            char c = fieldName.charAt(j);
                            if (Character.isUpperCase(c)) {
                                formattedField = formattedField.substring(0, j + offset) + " " + formattedField.substring(j + offset);
                                offset++;
                            }
                        }
                        try {
                            Object methodReturn = method.invoke(mst, new Object[0]);
                            if (methodReturn != null && methodReturn instanceof String) {
                                if (csv.get(formattedField) == null) {
                                    csv.put(formattedField, new String[customerOrders.size()]);
                                    columns.add(formattedField);
                                }
                                csv.get(formattedField)[i] = (String) methodReturn;
                            }
                        } catch (IllegalAccessException | InvocationTargetException | IllegalArgumentException e) {
                            logger.error(e.getMessage(), e);
                        }
                    }
                }
                i++;
            }
        }
        List<String> toReturn = new ArrayList<>();
        StringBuilder line = new StringBuilder();
        //iterate through columns to use the defined ordering of column names
        for (String s : columns) {
            line.append(StringEscapeUtils.escapeCsv(s)).append(",");
        }
        toReturn.add(line.toString().substring(0, line.length() - 1));
        for (i = 0; i < customerOrders.size(); i++) {
            line = new StringBuilder();
            //iterate through columns instead of csv map to preserve the ordering of column names
            for (String s : columns) {
                String val = csv.get(s)[i];
                if (val == null || "null".equals(val)) {
                    val = "";
                }
                line.append(StringEscapeUtils.escapeCsv(val)).append(",");
            }
            toReturn.add(line.toString().substring(0, line.length() - 1));
        }
        return toReturn;
    }

    public void downloadCustomerOrderList() {
        Account account = credentialSelectionBean.getCurrentAXCXU().getAccount();
        String userId = null;
        if (this.requestLimitation.equals(RequestLimitation.USER_ONLY)) {
            userId = credentialSelectionBean.getCurrentAXCXU().getUser().getLogin() + "";
        }
        Credential credential = null;
        if (this.requestLimitation.equals(RequestLimitation.CREDENTIAL)) {
            credential = credentialSelectionBean.getCurrentCredential();
        }
        CustomerOrderSearchCriteria criterion = new CustomerOrderSearchCriteria(selectedStartDate,
                selectedEndDate,
                selectedStatus,
                selectedPO,
                account,
                credential,
                userId,
                null,
                null,
                null,
                0,
                0,
                null);
        List<CustomerOrder> allOrders = customerOrderBp.getCustomerOrdersPaginated(criterion);
        List<String> customerOrderList = this.getCustomerOrderCSV(allOrders);
        FacesContext facesContext = FacesContext.getCurrentInstance();
        HttpServletResponse response = (HttpServletResponse) facesContext.getExternalContext().getResponse();
        response.setContentType(DOWNLOAD_CONTENT_TYPE);
        response.setHeader(ATTACHMENT_HEADER, ATTACHMENT_VALUE);
        ServletOutputStream out = null;
        try {
            out = response.getOutputStream();
            for (String line : customerOrderList) {
                out.write((line + "\n").getBytes());
            }
            //out.write();
            out.close();
        } catch (Exception ex) {

        }
        facesContext.responseComplete();
    }

    /**
     * @return the listPaginationWidget
     */
    public PageNumberDisplay getListPaginationWidget() {
        return listPaginationWidget;
    }

    /**
     * @param listPaginationWidget the listPaginationWidget to set
     */
    public void setListPaginationWidget(PageNumberDisplay listPaginationWidget) {
        this.listPaginationWidget = listPaginationWidget;
    }

    public CustomerOrder getOrderById(String id) {
        for (CustomerOrder order : orderList) {
            String orderId = order.getId().toString();
            if (orderId.equals(id)) {
                return order;
            }
        }
        return null;
    }

    /**
     * @return the addOrderId
     */
    public String getAddOrderId() {
        return addOrderId;
    }

    /**
     * @param addOrderId the addOrderId to set
     */
    public void setAddOrderId(String addOrderId) {
        this.addOrderId = addOrderId;
    }

    public RequestLimitation getRequestLimitation() {
        return this.requestLimitation;
    }

    public void setRequestLimitation(RequestLimitation limitation) {
        this.requestLimitation = limitation;
    }

    private enum RequestLimitation {

        USER_ONLY("Only my orders"), ACCOUNT("All orders for my account"), CREDENTIAL("All orders for my credential");
        private final String label;

        private RequestLimitation(String label) {
            this.label = label;
        }

        private String getLabel() {
            return this.label;
        }
    }

    public boolean isRequestingUserOnlyHistory() {
        return this.requestLimitation == RequestLimitation.USER_ONLY;
    }

    public SelectItem[] getRequestLimitations() {
        SelectItem[] items = new SelectItem[RequestLimitation.values().length];
        int i = 0;
        for (RequestLimitation g : RequestLimitation.values()) {
            items[i++] = new SelectItem(g, g.getLabel());
        }
        return items;
    }

    public SelectItem[] getRequestLimitationsForUser() {
        List<SelectItem> items = new ArrayList<>();
        for (RequestLimitation g : RequestLimitation.values()) {
            if (g.equals(RequestLimitation.ACCOUNT) && !credentialSelectionBean.hasPermission("shopping view account history")) {
                break;
            }
            if (g.equals(RequestLimitation.CREDENTIAL) && !credentialSelectionBean.hasPermission("shopping view credential history")) {
                break;
            }
            items.add(new SelectItem(g, g.getLabel()));
        }
        SelectItem[] toReturn = new SelectItem[items.size()];
        toReturn = items.toArray(toReturn);
        return toReturn;
    }

    public boolean isDisplayNoResultsMessage() {
        return displayNoResultsMessage;
    }

    public void setDisplayNoResultsMessage(boolean displayNoResultsMessage) {
        this.displayNoResultsMessage = displayNoResultsMessage;
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    
    /**
     * This method takes a CustomerOrder, and returns the value to display in the "status" column of the search 
     * results page
     * 
     * @param order
     * @return
     */
    public String statusToDisplay(CustomerOrder order) {
    	//uses QUOTED status as the default, if there is no customer status
    	String status = OrderStatusEnum.QUOTED.getValue();
    	if (order != null && order.getCustomerStatus() != null) {
    		status = order.getCustomerStatus().getValue();
    	}
		String toReturn = "";
		for (String statusPart : status.split("_")) {
			toReturn = toReturn.concat(statusPart).concat(" ");
		}
		return toReturn;
    }
    
    private enum DateRanges {

        LAST_10_DAYS("10 Days"), LAST_30_DAYS("30 Days"), LAST_60_DAYS("60 Days"), LAST_90_DAYS("90 Days");
        private final String label;

        private DateRanges(String label) {
            this.label = label;
        }

        private String getLabel() {
            return this.label;
        }
    }
    
    public SelectItem[] getDateRanges() {
        List<SelectItem> dateRanges = new ArrayList<>();
        for (DateRanges g : DateRanges.values()) {            
        	dateRanges.add(new SelectItem(g, g.getLabel()));
        }
        SelectItem[] toReturn = new SelectItem[dateRanges.size()];
        toReturn = dateRanges.toArray(toReturn);
        return toReturn;
    }

	public DateRanges getDateRange() {
		return dateRange;
	}

	public void setDateRange(DateRanges dateRange) {
		this.dateRange = dateRange;
	}
	
	public void getStartAndEndDate(){
		Calendar cal = GregorianCalendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("MM-dd-yyyy");
		Date selectedStartDate = null;
		this.selectedEndDate = new Date();
		try {
			if(this.dateRange.equals(DateRanges.LAST_10_DAYS)) {			
				cal.add( Calendar.DAY_OF_YEAR, -10);
			} else if(this.dateRange.equals(DateRanges.LAST_30_DAYS)) {			
				cal.add( Calendar.DAY_OF_YEAR, -30);
			} else if(this.dateRange.equals(DateRanges.LAST_60_DAYS)) {			
				cal.add( Calendar.DAY_OF_YEAR, -60);
			} else if (this.dateRange.equals(DateRanges.LAST_90_DAYS)) {
				cal.add( Calendar.DAY_OF_YEAR, -90);
			}
			this.selectedStartDate = sdf.parse(sdf.format(cal.getTime()));
		} catch(Exception exp) {
			
		}
	}
}
