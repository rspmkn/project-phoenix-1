package com.apd.phoenix.shopping.view.jsf.bean.search.productlookup;

import java.util.List;
import com.apd.phoenix.service.model.CatalogXItem;
import com.apd.phoenix.service.product.Product;
import com.apd.phoenix.service.product.ProductNotFoundException;

/**
 * This interface specifies the API for looking up Products based on identifying information. This 
 * interface and its implementation was written for the implementation of USSCO SmartSearch, so
 * the default vendor is USSCO.
 * 
 * @author RHC
 *
 */
public interface ProductLookupService {

    /**
     * Deprecated. Use the version with List<Long> instead of single customerCatalogId, with credentialSelectionBean.getCatalogIdList()
     */
    @Deprecated
    public Product getProduct(String apdSku, Long customerCatalogId) throws ProductNotFoundException;

    /**
     * Takes information about a USSCO Product, and returns that Product
     * 
     * @param apdSku - the APD SKU of the item
     * @param customerCatalogId - the ID of the current customer catalog
     * @return
     * @throws ProductNotFoundException If there is no product matching the given parameters
     */
    public Product getProduct(String apdSku, List<Long> customerCatalogId) throws ProductNotFoundException;

    /**
     * Deprecated. Use the version with List<Long> instead of single customerCatalogId, with credentialSelectionBean.getCatalogIdList()
     */
    @Deprecated
    public Product getProduct(String apdSku, String vendorName, Long customerCatalogId) throws ProductNotFoundException;

    /**
     * Takes information about a Product, and returns that Product
     * 
     * @param apdSku - the APD SKU of the item
     * @param vendorName - the vendor name of the item
     * @param customerCatalogId - the ID of the current customer catalog
     * @return
     * @throws ProductNotFoundException If there is no product matching the given parameters
     */
    public Product getProduct(String apdSku, String vendorName, List<Long> customerCatalogId)
            throws ProductNotFoundException;

    /**
     * Takes a CatalogXItem, and generates a Product. 
     * 
     * @param item - the CatalogXItem
     * @return the Product to be displayed
     */
    public Product getProduct(CatalogXItem item);
}
