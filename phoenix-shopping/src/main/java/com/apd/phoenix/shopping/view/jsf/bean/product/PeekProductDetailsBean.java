package com.apd.phoenix.shopping.view.jsf.bean.product;

import com.apd.phoenix.service.business.EntityComparator;
import com.apd.phoenix.service.business.ItemBp;
import com.apd.phoenix.service.model.Item;
import com.apd.phoenix.service.model.ItemImage;
import com.apd.phoenix.service.model.ItemSellingPoint;
import com.apd.phoenix.service.product.Product;
import com.apd.phoenix.shopping.view.jsf.bean.search.SearchEngine;
import com.apd.phoenix.shopping.view.jsf.bean.search.SmartSearch;
import com.apd.phoenix.shopping.view.jsf.bean.search.SmartSearchEngine;
import com.apd.phoenix.shopping.view.jsf.bean.utils.LinksBean;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import org.apache.commons.lang.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author anicholson
 */
@RequestScoped
@Named
public class PeekProductDetailsBean extends ProductDetailsBean {

    private static final Logger logger = LoggerFactory.getLogger(PeekProductDetailsBean.class);
    private String apdSku;
    private String vendorName;

    @Inject
    private ItemBp itemBp;
    @Inject
    private LinksBean linksBean;
    @Inject
    private SearchEngine searchEngine;
    @Inject
    @SmartSearch
    private SmartSearchEngine alternateSearchEngine;

    public void retrieve() {

        try {
            super.setProduct(retrieveProductInfo());
            super.setItem(retrieveItem());
            super.retrieveItemSpecifications();
            super.retrieveItemClassifications();
            super.retrieveItemSellingPoints();
            super.retrieveForUseWIth();
            super.retrieveSimilarItems();
        }
        catch (Exception e) {
            try {
                FacesContext.getCurrentInstance().getExternalContext().redirect(linksBean.getItemNotFound());
            }
            catch (IOException ex) {
                logger.error("IOException during redirect to product not found page.", ex);
            }
        }
    }

    public List<ItemImage> fetchItemImages() {
        return new ArrayList<ItemImage>(super.getItem().getItemImages());
    }

    private Item retrieveItem() {
        Item item = itemBp.searchByExactExample(itemBp.searchItem(apdSku, vendorName), 0, 0).get(0);
        item = itemBp.hydrateItem(item);
        return item;
    }

    private Product retrieveProductInfo() {
        Product product = searchEngine.getProductFromVendorNameAndApdSku(vendorName, apdSku);
        if (StringUtils.isEmpty(product.getApdSku())) {
            //If the first product could not be found in the search engine
            product = alternateSearchEngine.getProductFromVendorNameAndApdSku(vendorName, apdSku);
        }
        return product;
    }

    public String getApdSku() {
        return apdSku;
    }

    public void setApdSku(String apdSku) {
        this.apdSku = apdSku;
    }

    public String getVendorName() {
        return vendorName;
    }

    public void setVendorName(String vendorName) {
        this.vendorName = vendorName;
    }
}
