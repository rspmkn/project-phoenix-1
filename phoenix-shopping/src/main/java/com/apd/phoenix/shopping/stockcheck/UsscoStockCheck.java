package com.apd.phoenix.shopping.stockcheck;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import org.apache.commons.lang.StringUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.CoreConnectionPNames;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.core.utility.EcommercePropertiesLoader;
import com.apd.phoenix.core.utility.TenantConfigRepository;
import com.apd.phoenix.service.persistence.jpa.SequenceDao;
import com.apd.phoenix.service.persistence.multitenancy.impl.CurrentTenantIdentifierResolverImpl;

/**
 * This class provides functionality to communicate with USSCO via Interlink to check the quantity for an item.
 *
 * @author RHC
 *
 */
@Stateless
@LocalBean
public class UsscoStockCheck extends AbstractStockCheck {

    private static final String US_ACCOUNT_PROPERTY_TYPE = "US Account #";

    private static final String STOCK_CHECK_VENDOR_PARTNERID = "USSCO";

    private static final String ADOT_ALL_ZIP = "     ";

    private static final int RESPONSE_PER_FACILITY_LENGTH = 21;

    private static final int RESPONSE_TOTAL_LENGTH = 2247;

    private static final int RESPONSE_PREFIX_LENGTH = 147;

    private static final int FACILITY_QUANTITY = 100;

    private static final String REQUEST_TRANSMISSION_NAME = "RequestTransmission";

    private static final String REQUEST_TYPE_VALUE = "3";

    private static final String REQUEST_TYPE_NAME = "requesttype";

    private static final String RESPONSE_DELIMETER = "&||&";

    private static final String VALID_RETURN_CODE = "000";

    private static final String DEFAULT_ZIP = "csr.address.zip";

    private static final String ADOT_LOCAL = "Y";

    private static final String ADOT_ALL = " ";

    private static final String RECORD_TYPE = "MF";

    private static final String CONFIRMATION_FORMAT = "A";

    private static final String REQUESTOR_ID = "APDPHOEN";

    private static final String PASSWORD = "92011079";

    private static final String SIGNON_ACCOUNT_NUMBER = "201107";

    private static final String DEFAULT_MF_ACCOUNT_NUMBER = "201813";

    private static final String DATA_STRING_LENGTH = "00363";

    private static final String SEQUENCE_NUMBER = "0001";

    private static final String POSITION_INDICATOR = "L";

    private static final String TRANSMISSION_TYPE = "D";

    private static final String APD_SYSTEM_ID = "AMPD";

    private static final String TRANSACTION_ID = "OE01";

    private static final String REQUEST_URL = "https://elink2.unitedstationers.com/interlink/interlinkdirect.asp";

    private static final int STOCK_CHECK_TIMEOUT = EcommercePropertiesLoader.getInstance().getEcommerceProperties()
            .getInt("ussco.stockcheck.timeout");

    @Inject
    private SequenceDao sequenceDao;

    private static final Logger logger = LoggerFactory.getLogger(UsscoStockCheck.class);

    /**
     * This method takes the vendor SKU of an item and a zip code, and returns the quantity available.
     * <br /><br />
     * This method uses Apache HttpComponents.
     *
     * @param vendorSku
     * @param zip
     * @return
     */
    public int available(String vendorSku) {
    	String zip = super.getZipCode();
    	boolean checkAllFacilities = super.isAllFacilitiesChecked();
    	//TODO: refactor, use the "getUsAccountNumber" method on CustomerOrderBp
		String usAccountNumber = credBean.getPropertyMap().get(US_ACCOUNT_PROPERTY_TYPE);
		if (!StringUtils.isEmpty(credBean.getCurrentAXCXU().getUsAccountNumber())) {
			usAccountNumber = credBean.getCurrentAXCXU().getUsAccountNumber();
		}
		//generates the data string
    	String dataString = getDataString(vendorSku, zip, usAccountNumber, checkAllFacilities);
		HttpClient httpClient = new DefaultHttpClient();
    	try {
    		//sets up the request
	    	HttpPost httpPost = new HttpPost(REQUEST_URL);
	    	List<NameValuePair> nvps = new ArrayList<>();
	    	//sets the parameters for the request
	    	nvps.add(new BasicNameValuePair(REQUEST_TYPE_NAME, REQUEST_TYPE_VALUE));
	    	nvps.add(new BasicNameValuePair(REQUEST_TRANSMISSION_NAME, dataString));
	    	httpPost.setEntity(new UrlEncodedFormEntity(nvps));
	    	HttpParams params = new BasicHttpParams();
	    	params.setParameter(CoreConnectionPNames.CONNECTION_TIMEOUT, STOCK_CHECK_TIMEOUT);
	    	params.setParameter(CoreConnectionPNames.SO_TIMEOUT, STOCK_CHECK_TIMEOUT);
	    	httpPost.setParams(params);
	    	try{
	    		HttpResponse response = httpClient.execute(httpPost);
	    		//gets the response InputStream
	    	    HttpEntity entity = response.getEntity();
	    	    int toReturn = parseReply(entity.getContent());
	    	    EntityUtils.consume(entity);
	    	    return toReturn;
	    	} catch (Exception e) {
	    		logger.warn("Error getting USSCO stock check, defaulting to allowed", e);
	    		return Integer.MAX_VALUE;
	    	}

    	}
    	catch (Exception e) {
    		//catches any error that occurred, from incorrectly formatted ZIP code to server errors
    		//if an error occurs, allow adding to cart. From Juanita: "If the website is down allow the user 
    		//to add the item to the cart and per our internal discussion we decided not to display any warnings."
    		logger.warn(e.getMessage());
	        return Integer.MAX_VALUE;
    	}
    }

    /**
     * This method returns the datastring sent to USSCO for a stock check. The string consists of two parts: the
     * request header, containing general information, and the Multi-Facility Stock Check request. 
     * <br /> <br />
     * Documentation about the construction and content of the string is attached to JIRA issue PHOEN-1826.
     *
     * @param vendorSku
     * @param zip
     * @return
     */
    private String getDataString(String vendorSku, String zip, String usAccountNumber, boolean checkAllFacilities) {
        StringBuffer toReturn = new StringBuffer();
        /* ================ Creating Transmission Header Request ================ */
        /* Note: the comment labels match the field names in the "TCPIP Interlink Record Layouts.xls" document */
        //United Transaction ID
        toReturn.append(TRANSACTION_ID);
        //System ID
        toReturn.append(APD_SYSTEM_ID);
        //Reserved
        //12 characters
        toReturn.append("            ");
        //Transmission Type
        toReturn.append(TRANSMISSION_TYPE);
        //Position Indicator*
        toReturn.append(POSITION_INDICATOR);
        //Send date and time information
        Calendar c = Calendar.getInstance();
        //Send Date
        toReturn.append(c.get(Calendar.YEAR) + "-" + twoDigitNumber(c.get(Calendar.MONTH) + 1) + "-"
                + twoDigitNumber(c.get(Calendar.DATE)));
        //Send Time
        toReturn.append(twoDigitNumber(c.get(Calendar.HOUR_OF_DAY)) + ":" + twoDigitNumber(c.get(Calendar.MINUTE))
                + ":" + twoDigitNumber(c.get(Calendar.SECOND)));
        //Sequence Number*
        toReturn.append(SEQUENCE_NUMBER);
        //Data Length
        //equal to the length of the header plus the length of the body, minus 49, 
        //the length of the header up to this point.
        toReturn.append(DATA_STRING_LENGTH);
        //Sign on Account No.
        toReturn.append(SIGNON_ACCOUNT_NUMBER);
        //Reserved
        //3 characters
        toReturn.append("   ");
        //Transmission Number*
        //a five-digit, unique number
        //first gets the next value from a sequence, and converts to string
        String transmissionNumber = "" + sequenceDao.nextVal(SequenceDao.Sequence.USSCO_STOCKCHECK);
        //then, pads with zeros
        for (int i = 0; transmissionNumber.length() + i < 5; i++) {
            toReturn.append("0");
        }
        //then adds the number to the transmission
        toReturn.append(transmissionNumber);
        //Sign on Password
        toReturn.append(PASSWORD);
        //Requestor ID
        toReturn.append(REQUESTOR_ID);
        //Confirmation Record Format
        toReturn.append(CONFIRMATION_FORMAT);
        /* ================ Creating Multi-Facility Stock Check (MF) Request ================ */
        /* Note: the comment labels match the field names in the "TCPIP Interlink Record Layouts.xls" document */
        //Record Type
        toReturn.append(RECORD_TYPE);
        //Account Number
        String mfNumberToUse = DEFAULT_MF_ACCOUNT_NUMBER;
        if (!StringUtils.isEmpty(usAccountNumber)) {
            mfNumberToUse = usAccountNumber;
        }
        toReturn.append(mfNumberToUse);
        //Reserved
        //3 characters
        toReturn.append("   ");
        //Item:
        // Prefix
        // Stock Number
        toReturn.append(vendorSku);
        //adds padding after the prefix and stock number, if it's less than 15 characters
        for (int i = 0; i + vendorSku.length() < 15; i++) {
            toReturn.append(" ");
        }
        //ADOT Type
        //"Y", because we want to check all facilities for stock, not just the nearest ones
        if (checkAllFacilities) {
            toReturn.append(ADOT_ALL);
        }
        else {
            toReturn.append(ADOT_LOCAL);
        }
        //Zip Code
        String zipToUse = this.getTenantPropertyByName(DEFAULT_ZIP);
        if (!StringUtils.isEmpty(zip)) {
            zipToUse = zip;
        }
        if (checkAllFacilities) {
            toReturn.append(ADOT_ALL_ZIP);
        }
        else {
            toReturn.append(zipToUse);
        }
        //Facility Action Codes omitted
        for (int i = 0; i < FACILITY_QUANTITY; i++) {
            toReturn.append("   ");
        }

        return toReturn.toString();
    }

    /**
     * Utility method. Turns integers into strings with length at least two.
     * 
     * @param number
     * @return
     */
    private String twoDigitNumber(int number) {
        if (number >= 10) {
            return "" + number;
        }
        return "0" + number;
    }

    /**
     * This method takes the response as an InputStream, and pulls out the available quantity as an integer
     * 
     * @param content
     * @return
     */
    private int parseReply(InputStream content) throws IOException, UsscoServerException {
        //arrays for the different parts of the response
        byte[] returnCodeArray = new byte[3];
        byte[] replyArray = new byte[2500];
        int toReturn = 0;
        //reads the return code, either "000" or "008"
        content.read(returnCodeArray);
        if (!(new String(returnCodeArray)).equals(VALID_RETURN_CODE)) {
            //if "008", no response available
            content.read(replyArray);
            throw new UsscoServerException("From USSCO: " + new String(replyArray));
        }
        //reads off the rest of the reply
        content.read(replyArray);
        String replyWithDelimeter = new String(replyArray);
        String reply = replyWithDelimeter.substring(replyWithDelimeter.indexOf(RESPONSE_DELIMETER)
                + RESPONSE_DELIMETER.length());
        if (!reply.startsWith(TRANSACTION_ID)) {
            //if an error code
            content.read(replyArray);
            throw (new UsscoServerException("From USSCO: " + new String(replyArray)));
        }
        String replyBody = reply.substring(71);
        //iterates over the facility stock information in the reply
        for (int i = 0; i * RESPONSE_PER_FACILITY_LENGTH + RESPONSE_PREFIX_LENGTH < RESPONSE_TOTAL_LENGTH
                && i * RESPONSE_PER_FACILITY_LENGTH + RESPONSE_PREFIX_LENGTH < replyBody.length(); i++) {
            String stockInformation = replyBody.substring(i * RESPONSE_PER_FACILITY_LENGTH + RESPONSE_PREFIX_LENGTH,
                    (i + 1) * RESPONSE_PER_FACILITY_LENGTH + RESPONSE_PREFIX_LENGTH);
            try {
                toReturn += Integer.parseInt(stockInformation.substring(5, 14));
            }
            catch (NumberFormatException e) {
                //do nothing
            }
        }
        return toReturn;
    }

    private class UsscoServerException extends Exception {

        private String message;

        private UsscoServerException(String message) {
            this.message = message;
        }

        @Override
        public String getMessage() {
            return this.message;
        }

        private static final long serialVersionUID = 1L;
    }

    @Override
    public boolean vendorNameMatches(String vendorName) {
        return STOCK_CHECK_VENDOR_PARTNERID.equalsIgnoreCase(vendorName);
    }

    private String getTenantPropertyByName(String propertyName) {
        return TenantConfigRepository.getInstance().getProperty(getTenantName(), "tenant", propertyName);
    }

    private String getTenantName() {
        return TenantConfigRepository.getInstance().getTenantNameById(
                CurrentTenantIdentifierResolverImpl.getCurrentTenant());
    }
}
