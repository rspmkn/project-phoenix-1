package com.apd.phoenix.shopping.view.jsf.bean.search;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import com.apd.phoenix.service.model.Catalog.SearchType;
import com.apd.phoenix.service.model.CatalogXItem;
import com.apd.phoenix.service.model.Manufacturer;
import com.apd.phoenix.service.model.Matchbook;
import com.apd.phoenix.service.product.Product;
import com.apd.phoenix.service.product.ProductNotFoundException;

public interface SearchEngine {

    public CategoryMenu buildCategoryMenu();

    public int featuredProductsFound();

    public List<Product> featuredProductList(Integer start, Integer randomSort);

    public Product expressOrderSearch(String sku);

    public Product getProductFromCatalogXItem(CatalogXItem item);

    public Product getProductFromVendorNameAndApdSku(String vendorName, String apdSku);

    public Product getCatalogProductFromVendorNameAndApdSku(String vendorName, String apdSku)
            throws ProductNotFoundException;

    public ResponseData query(QueryData query);

    public String getSearchTermsJson();

    public Map<String, List<Product>> getRelatedItems(Product product);

    public Set<Matchbook> getMatchbooksByCatalogAndManufacturer(List<Long> catalogIds, Manufacturer manufacturer);

    public Collection<Manufacturer> matchbookManufacturersFromCatalog(List<Long> catalogIds);

    public String inkAndTonerSearch(Matchbook selectedModel, Manufacturer selectedManufacturer);

    public String searchByCartridgeNumber(String cartridgeNumber);

    public boolean isSearchByCartridgeSupported();

    public boolean isSearchEngineAvailable();

    public boolean usesActiveFilters();

    public SearchType getSearchType();

    public boolean isSearchByModelSupported();

    public String searchByModelNumber(String ModelNumber) throws InvalidModelNumberException;

    public String getCartridgeLookahead();

    public String getModelLookahead();

}
