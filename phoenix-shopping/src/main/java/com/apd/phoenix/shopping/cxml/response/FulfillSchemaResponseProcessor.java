/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.shopping.cxml.response;

import com.apd.phoenix.service.integration.cxml.model.fulfill.CXML;
import com.apd.phoenix.service.integration.cxml.model.fulfill.Response;
import com.apd.phoenix.service.integration.cxml.model.fulfill.Status;
import com.apd.phoenix.service.persistence.multitenancy.api.CurrentTenantDomain;
import java.io.IOException;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import org.slf4j.LoggerFactory;
import static com.apd.phoenix.service.cxml.CxmlConstants.*;

/**
 *
 * @author dnorris
 */
@Stateless
public class FulfillSchemaResponseProcessor extends BaseResponseProcessor<CXML, Response> {

    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(FulfillSchemaResponseProcessor.class);
    private static final String CXML_HEADER = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
            + "<!DOCTYPE cXML SYSTEM \"http://xml.cxml.org/schemas/cXML/1.2.014/fulfill.dtd\">";

    @Inject
    private CurrentTenantDomain currentTenantDomain;

    @Override
    public CXML createBaseResponse() {
        CXML cxmlResponseMessage = new CXML();
        cxmlResponseMessage.setPayloadID(generatePayloadId(currentTenantDomain.getDomain()));
        cxmlResponseMessage.setXmlLang(XML_LANG);
        cxmlResponseMessage.setTimestamp(generateTimeStamp());
        return cxmlResponseMessage;
    }

    @Override
    public void initializeResponse(HttpServletResponse response, Response cxmlResponse) {
        try {
            if (cxmlResponse.getStatus() != null && cxmlResponse.getStatus().getCode() != null) {
                response.setStatus(Integer.parseInt(cxmlResponse.getStatus().getCode()));
            } else {
                //Assume 200 status
                response.setStatus(CXML_STATUS_OK);
                Status status = new Status();
                status.setCode(Integer.toString(CXML_STATUS_OK));
                status.setText("success");
                cxmlResponse.setStatus(status);
            }
            CXML cxmlResponseMessage = createBaseResponse();
            cxmlResponseMessage.getHeaderOrMessageOrRequestOrResponse().add(cxmlResponse);
            JAXBContext jaxbContext = JAXBContext.newInstance(CXML.class);
            Marshaller marshaller = jaxbContext.createMarshaller();
            marshaller.setProperty("com.sun.xml.bind.xmlDeclaration", Boolean.FALSE);
            marshaller.setProperty("com.sun.xml.bind.xmlHeaders", CXML_HEADER);
            marshaller.marshal(cxmlResponseMessage, response.getOutputStream());
        } catch (JAXBException | IOException ex) {
            logger.error("Error Generating cxml response.", ex);
        }
    }

    @Override
    public Response createInvalidResponse() {
        Response cxmlResponse = new Response();
        Status status = new Status();
        status.setCode(Integer.toString(CXML_STATUS_NOT_ACCEPTABLE));
        status.setText("Invalid cxml request.");
        cxmlResponse.setStatus(status);
        return cxmlResponse;
    }

    @Override
    public Response createOkResponse() {
        Response cxmlResponse = new Response();
        Status status = new Status();
        status.setCode(Integer.toString(CXML_STATUS_OK));
        status.setText("success");
        cxmlResponse.setStatus(status);
        return cxmlResponse;
    }

    @Override
    public Response createInternalErrorResponse() {
        Response cxmlResponse = new Response();
        Status status = new Status();
        status.setCode(Integer.toString(CXML_STATUS_NOT_ACCEPTABLE));
        status.setText("Invalid cxml request.");
        cxmlResponse.setStatus(status);
        return cxmlResponse;
    }

    @Override
    public Response createNotImplementedResponse() {
        Response cxmlResponse = new Response();
        Status status = new Status();
        status.setCode(Integer.toString(CXML_STATUS_NOT_IMPLEMENTED));
        status.setText("Request Type Not Supported");
        cxmlResponse.setStatus(status);
        return cxmlResponse;
    }
}
