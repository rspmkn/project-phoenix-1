package com.apd.phoenix.shopping.view.jsf.bean.experience;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.ejb.Stateful;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.apd.phoenix.service.business.EntityComparator;
import com.apd.phoenix.service.business.IssueLogBp;
import com.apd.phoenix.service.business.SecurityContext;
import com.apd.phoenix.service.model.ContactLogComment;
import com.apd.phoenix.service.model.IssueLog;
import com.apd.phoenix.service.model.SystemUser;
import com.apd.phoenix.shopping.view.jsf.bean.login.LoginBean;

/**
 *
 * @author RHC
 */
@Named
@Stateful
@RequestScoped
public class IssueLogViewBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private IssueLogBp issueLogBp;
	
	@Inject
	private LoginBean loginBean;
	
	@Inject
	private SecurityContext securityContext;
	
	private String commentText;
	
	private IssueLog issueLog;
	
	private List<IssueLog> issueLogList = new ArrayList<>();
	
	private List<ContactLogComment> logCommentList = new ArrayList<>();
	
	public void loadExistingLogs() {
		if (securityContext.hasPermission("shopping view issue logs")) {
			IssueLog searchLog = new IssueLog();
			searchLog.setReporter(new SystemUser());
			searchLog.getReporter().setId(this.loginBean.getSystemUser().getId());
			this.issueLogList = issueLogBp.searchByExactExample(searchLog, 0, 0);
			Collections.sort(this.issueLogList, new EntityComparator());
		}
	}
	
	public List<IssueLog> getLogs() {
		return issueLogList;
	}
	
	public List<ContactLogComment> getComments() {
		return this.logCommentList;
	}
	
	public void addComment() {
		this.setIssueId(this.issueLog.getId());
		ContactLogComment newComment = new ContactLogComment();
		newComment.setApdCsrRep(this.loginBean.getSystemUser());
		newComment.setCommentTimestamp(new Date());
		newComment.setContent(this.commentText);
		this.issueLog.getComments().add(newComment);
		this.issueLogBp.update(this.issueLog);
		this.loadIssueComments();
	}
	
	public void loadIssueComments() {
		this.setIssueId(this.issueLog.getId());
		this.logCommentList = new ArrayList<>();
		for (ContactLogComment comment : this.issueLog.getComments()) {
			if (comment.getApdCsrRep().getId().equals(this.loginBean.getSystemUser().getId())) {
				this.logCommentList.add(comment);
			}
		}
		Collections.sort(this.logCommentList, new EntityComparator());
	}

	public String getCommentText() {
		return commentText;
	}

	public void setCommentText(String commentText) {
		this.commentText = commentText;
	}
	
	public Long getIssueId() {
		if (this.issueLog == null) {
			return null;
		}
		return this.issueLog.getId();
	}
	
	public void setIssueId(Long issueId) {
		this.issueLog = issueLogBp.findById(issueId, IssueLog.class);
	}
}
