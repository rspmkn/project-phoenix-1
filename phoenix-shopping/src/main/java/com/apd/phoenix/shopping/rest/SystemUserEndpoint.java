/*
 * 
 */
package com.apd.phoenix.shopping.rest;

import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriBuilder;
import com.apd.phoenix.service.business.SystemUserBp;
import com.apd.phoenix.service.model.SystemUser;

// TODO: Auto-generated Javadoc
/**
 * The Class SystemUserEndpoint.
 */
@Stateless
@Path("/systemusers")
public class SystemUserEndpoint {

    @Inject
    private SystemUserBp systemUserBp;

    /**
     * Creates the.
     *
     * @param entity the entity
     * @return the response
     */
    @POST
    @Consumes("application/xml")
    @Produces("application/xml")
    public Response create(SystemUser entity) {
        this.systemUserBp.create(entity);
        return Response.created(
                UriBuilder.fromResource(SystemUserEndpoint.class).path(String.valueOf(entity.getId())).build()).build();
    }

    /**
     * Delete by id.
     *
     * @param id the id
     * @return the response
     */
    @DELETE
    @Path("/{id:[0-9][0-9]*}")
    public Response deleteById(@PathParam("id") Long id) {
        this.systemUserBp.delete(id, SystemUser.class);
        return Response.noContent().build();
    }

    /**
     * Find by id.
     *
     * @param id the id
     * @return the response
     */
    @GET
    @Path("/{id:[0-9][0-9]*}")
    @Produces("application/xml")
    public Response findById(@PathParam("id") Long id) {

        SystemUser systemUser = this.systemUserBp.findById(id, SystemUser.class);
        if (systemUser == null) {
            return Response.status(Status.NOT_FOUND).build();
        }
        else {
            systemUser.getAccounts().size();
            systemUser.getPerson().getPhoneNumbers().size();
            systemUser.getPerson().getAddresses().size();
        }
        return Response.ok(systemUser).build();
    }

    /**
     * List all.
     *
     * @return the list
     */
    @GET
    @Produces("application/xml")
    public List<SystemUser> listAll() {
        final List<SystemUser> results = this.systemUserBp.findAll(SystemUser.class, 0, 0);
        if (null != results && results.size() > 0) {
            for (SystemUser user : results) {
                user.getAccounts().size();
                user.getPerson().getPhoneNumbers().size();
                user.getPerson().getAddresses().size();
            }
        }
        return results;
    }

    /**
     * Update.
     *
     * @param id the id
     * @param entity the entity
     * @return the response
     */
    @PUT
    @Path("/{id:[0-9][0-9]*}")
    @Consumes("application/xml")
    public Response update(@PathParam("id") Long id, SystemUser entity) {
        this.systemUserBp.update(entity);
        return Response.noContent().build();
    }
}