/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.shopping.cxml;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

/**
 *
 * @author dnorris
 */
public abstract class BaseCxmlProcessor {

    protected static String generateTimeStamp() {
        TimeZone tz = TimeZone.getTimeZone("UTC");
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssXXX");
        df.setTimeZone(tz);
        String nowAsISO = df.format(new Date());
        return nowAsISO;
    }

    //Format should be datetime.process id.random number@hostname
    protected static String generatePayloadId(String tenantDomain) {
        String hostname = removeTLD(tenantDomain);
        return new Date().getTime() + "." + Math.round(Math.random() * 10000) + "@" + hostname;

    }

    private static String removeTLD(String domain) {
        int dotIndex = domain.lastIndexOf(".");

        if (dotIndex > 0) {
            return domain.substring(0, dotIndex);
        }
        else {
            return domain;
        }
    }

}
