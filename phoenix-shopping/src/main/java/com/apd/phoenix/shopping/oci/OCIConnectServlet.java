package com.apd.phoenix.shopping.oci;

import java.io.IOException;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.shopping.oci.request.CallUpCatalogRequest;
import com.apd.phoenix.shopping.oci.request.processors.CallUpCatalogRequestProcessor;
import com.apd.phoenix.shopping.oci.request.processors.OciProcessorException;

@WebServlet("/oci")
public class OCIConnectServlet extends HttpServlet {

    private static final long serialVersionUID = 1L;

    private static Logger LOGGER = LoggerFactory.getLogger(OCIConnectServlet.class);

    @Inject
    CallUpCatalogRequestProcessor callUpRequestProcessor;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            callUpRequestProcessor.processRequest(new CallUpCatalogRequest(request, response));
        }
        catch (OciProcessorException oe) {
            LOGGER.error(oe.toString());
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException,
            IOException {
        try {
            callUpRequestProcessor.processRequest(new CallUpCatalogRequest(request, response));
        }
        catch (OciProcessorException oe) {
            LOGGER.error(oe.toString());
        }
    }
}
