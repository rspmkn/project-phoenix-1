package com.apd.phoenix.shopping.oci;

import java.util.Arrays;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class OCIRequestWrapper extends HttpServletRequestWrapper {

    protected static final Logger LOG = LoggerFactory.getLogger(OCIRequestWrapper.class);

    public static final String REQUEST_PARAM_USERNAME = "USERNAME";
    public static final String REQUEST_PARAM_PASSWORD = "PASSWORD";
    public static final String REQUEST_PARAM_RETURN_URL = "HOOK_URL";
    public static final String REQUEST_PARAM_TARGET = "~TARGET";
    public static final String REQUEST_PARAM_OKCODE = "-OkCode";
    public static final String REQUEST_PARAM_CALLER = "~CALLER";
    public static final String REQUEST_PARAM_OCI_VERSION = "OCI_VERSION";
    public static final String REQUEST_PARAM_OPI_VERSION = "OPI_VERSION";
    public static final String REQUEST_PARAM_HTTP_CONTENT_CHARSET = "http_content_charset";
    // Request parameters that trigger additional OCI functions
    public static final String REQUEST_PARAM_FUNCTION = "FUNCTION";
    public static final String REQUEST_PARAM_PRODUCTID = "PRODUCTID";
    public static final String REQUEST_PARAM_QUANTITY = "QUANTITY";
    public static final String REQUEST_PARAM_SEARCH_STRING = "SEARCHSTRING";

    public static final String REQUEST_PARAM_TARGET_VALUE = "_top";
    public static final String REQUEST_PARAM_CALLER_VALUE = "CTLG";
    public static final String REQUEST_PARAM_OKCODE_VALUE = "ADDI";

    public static final List<String> SUPPORTED_OCI_VERSIONS = Arrays.asList(new String[] { "3.0", "4.0" });

    private HttpServletResponse _response;

    public OCIRequestWrapper(HttpServletRequest httpRequest, HttpServletResponse httpResponse) {
        super(httpRequest);
        _response = httpResponse;
    }

    public boolean isValid() {
        // Validate fixed request parameter values common to all OCI requests
        return (getParameter(REQUEST_PARAM_OKCODE) == null || getParameter(REQUEST_PARAM_OKCODE).equals(
                REQUEST_PARAM_OKCODE_VALUE))
                && (getParameter(REQUEST_PARAM_CALLER) == null || getParameter(REQUEST_PARAM_CALLER).equals(
                        REQUEST_PARAM_CALLER_VALUE))
                && (getParameter(REQUEST_PARAM_TARGET) == null || getParameter(REQUEST_PARAM_TARGET).equals(
                        REQUEST_PARAM_TARGET_VALUE))
                && (getParameter(REQUEST_PARAM_OCI_VERSION) == null || SUPPORTED_OCI_VERSIONS
                        .contains(getParameter(REQUEST_PARAM_OCI_VERSION)));
    }

    public void authenticate() throws ServletException {
        this.login(getParameter(REQUEST_PARAM_USERNAME), getParameter(REQUEST_PARAM_PASSWORD));
    }

    public HttpServletResponse getResponse() {
        return _response;
    }
}
