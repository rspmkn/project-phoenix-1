/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.shopping.view.jsf.bean.search;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import javax.ejb.Stateful;
import javax.inject.Named;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.model.Manufacturer;
import com.apd.phoenix.service.model.Matchbook;
import com.apd.phoenix.shopping.view.jsf.bean.login.CredentialSelectionBean;
import java.util.Collections;
import javax.annotation.PostConstruct;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.ConversationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.inject.Inject;

/**
 *
 * @author dcnorris
 */
@Named
@Stateful
@ConversationScoped
public class InkTonerSearchBean {

    private static final Logger logger = LoggerFactory.getLogger(InkTonerSearchBean.class);
    private Manufacturer selectedManufacturer;
    private List<Matchbook> models;
    private String selectedModel;
    private static final String SELECT_BRAND_DEFAULT = "Select a brand";
    private String cartridgeNumber;
    private String modelNumber;
    private static final String MISSING_FIELDS_MESSAGE = "You must select a manufacturer and a brand";
    @Inject
    private Conversation conversation;
    @Inject
    private CredentialSelectionBean credentialSelectionBean;
    @Inject
    private SearchEngine searchEngine;

    @PostConstruct
    public void init() {
        if (conversation.isTransient()) {
            conversation.begin();
        }
    }

    public void search() {
        if (StringUtils.isBlank(selectedModel) || selectedManufacturer == null
                || StringUtils.isBlank(selectedManufacturer.getName())) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, MISSING_FIELDS_MESSAGE, MISSING_FIELDS_MESSAGE));
            return;
        }
        String browseLink = searchEngine.inkAndTonerSearch(findModelByModelName(selectedModel), selectedManufacturer);
        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect(browseLink);
            conversation.end();
        }
        catch (Exception ex) {
            logger.error("Exception in InkTonerSearchBean#search()", ex);
        }
    }

    public void searchByCartridgeNumber() {
        String browseLink = searchEngine.searchByCartridgeNumber(cartridgeNumber);
        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect(browseLink);
            conversation.end();
        }
        catch (Exception ex) {
            logger.error("Exception in InkTonerSearchBean#search()", ex);
        }
    }

    public void searchByModelNumber() {
        String browseLink;
        try {
            browseLink = searchEngine.searchByModelNumber(modelNumber);
        }
        catch (InvalidModelNumberException e) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage("No items found with model number " + modelNumber));
            return;
        }
        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect(browseLink);
            conversation.end();
        }
        catch (Exception ex) {
            logger.error("Exception in InkTonerSearchBean#search()", ex);
        }
    }

    private Matchbook findModelByModelName(String selectedModel) {
        if (StringUtils.isEmpty(selectedModel)) {
            return null;
        }
        for (Matchbook model : models) {
            if (selectedModel.equals(model.getModel())) {
                return model;
            }
        }
        return null;
    }

    public String getSELECT_BRAND_DEFAULT() {
        return SELECT_BRAND_DEFAULT;
    }

    public void manufacturerValueChanged(ValueChangeEvent e) {
        selectedManufacturer = (Manufacturer) e.getNewValue();
        setModels(new ArrayList<Matchbook>());
        for (Matchbook m : searchEngine.getMatchbooksByCatalogAndManufacturer(credentialSelectionBean
                .getCatalogIdList(), selectedManufacturer)) {
            if (StringUtils.isNotBlank(m.getModel())) {
                if (m.getModel() != null) {
                    getModels().add(m);
                }
            }
        }
        Collections.sort(getModels(), new Comparator<Matchbook>() {

            @Override
            public int compare(Matchbook arg0, Matchbook arg1) {
                return arg0.getModel().compareTo(arg1.getModel());
            }

        });
    }

    public boolean isSearchByCartridgeSupported() {
        return searchEngine.isSearchByCartridgeSupported();
    }

    public Manufacturer getSelectedManufacturer() {
        return selectedManufacturer;
    }

    public void setSelectedManufacturer(Manufacturer selectedManufacturer) {
        this.selectedManufacturer = selectedManufacturer;
    }

    public List<Matchbook> getModels() {
        return models;
    }

    public void setModels(List<Matchbook> models) {
        this.models = models;
    }

    public String getSelectedModel() {
        return selectedModel;
    }

    public void setSelectedModel(String selectedModel) {
        this.selectedModel = selectedModel;
    }

    public String getCartridgeNumber() {
        return cartridgeNumber;
    }

    public void setCartridgeNumber(String cartridgeNumber) {
        this.cartridgeNumber = cartridgeNumber;
    }

    public boolean isSearchByModelSupported() {
        return searchEngine.isSearchByModelSupported();
    }

    public String getModelNumber() {
        return modelNumber;
    }

    public void setModelNumber(String modelNumber) {
        this.modelNumber = modelNumber;
    }
}
