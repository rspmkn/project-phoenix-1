package com.apd.phoenix.shopping.view.jsf.bean.product;

import com.apd.phoenix.core.StringEscape;
import javax.ejb.Stateful;
import javax.inject.Named;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.product.Product;
import com.apd.phoenix.shopping.view.jsf.bean.search.SearchBean;
import com.apd.phoenix.core.utility.EcommercePropertiesLoader;
import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

@Named
@Stateful
@RequestScoped
public class ProductBrowseBean {

    private static final Logger logger = LoggerFactory.getLogger(ProductBrowseBean.class);
    private int FILTERS_TO_SHOW_MULTIPLIER;
    private int ATTRIBUTES_TO_SHOW;
    private boolean shouldShowAllFilters;
    @Inject
    private SearchBean searchBean;

    @PostConstruct
    public void init() {
        FILTERS_TO_SHOW_MULTIPLIER = EcommercePropertiesLoader.getInstance().getEcommerceProperties().getInt(
                "FILTERS_TO_SHOW");
        ATTRIBUTES_TO_SHOW = EcommercePropertiesLoader.getInstance().getEcommerceProperties().getInt(
                "ATTRIBUTES_TO_SHOW");
    }

    // TODO: more elegant solution leveraging heights of product divs / heights of filter divs
    /**
     * Returns the approximate number of filters per item displayed in search results, to be multiplied
     * but the number of items being displayed so the filter list does not display longer than the product list
     *
     * @return
     */
    public int getDefaultFiltersToShowMultiplier() {
        return FILTERS_TO_SHOW_MULTIPLIER;
    }

    public int getDefaultFiltersToShow() {
        if (searchBean.getPageSize() != null) {
            return FILTERS_TO_SHOW_MULTIPLIER * searchBean.getPageSize().toInt();
        }
        else {
            return 0;
        }
    }

    /**
     * Returns the default number of attributes to show per filter
     *
     * @return
     */
    public int getDefaultAttributesToShow() {
        return ATTRIBUTES_TO_SHOW;
    }

    /**
     * Returns whether all filters should be shown.
     *
     * @return
     */
    public boolean isShowAllFilters() {
        return this.shouldShowAllFilters;
    }

    /**
     * Sets whether all filters should be shown.
     *
     * @param b
     */
    public void setShowAllFilters() {
        this.shouldShowAllFilters = true;
    }

    public String getProductDetailsLink(String apdSku, String vendor) {
        StringBuilder url = new StringBuilder(Product.getDETAILS_PAGE_BASE_URL());
        url.append("?");
        url.append("apdSku=");
        url.append(StringEscape.escapeForUrl(apdSku));
        url.append("&");
        url.append("vendorName=");
        url.append(StringEscape.escapeForUrl(vendor));
        return url.toString();
    }

    public String getSubstituteProductDetailsLink(Product product) {
        return this.getProductDetailsLink(product.getProperties().get("substituteSku"), product.getProperties().get(
                "substituteVendor"));
    }

    public String getReplacementProductDetailsLink(Product product) {
        return this.getProductDetailsLink(product.getReplacementSku(), product.getReplacementVendor());
    }
}
