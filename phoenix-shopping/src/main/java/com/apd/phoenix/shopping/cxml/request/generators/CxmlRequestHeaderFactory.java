/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.shopping.cxml.request.generators;

import static com.apd.phoenix.service.cxml.CxmlConstants.USER_AGENT;
import java.util.HashSet;
import java.util.Set;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import com.apd.phoenix.service.business.CredentialBp;
import com.apd.phoenix.service.integration.cxml.model.cxml.Credential;
import com.apd.phoenix.service.integration.cxml.model.cxml.From;
import com.apd.phoenix.service.integration.cxml.model.cxml.Header;
import com.apd.phoenix.service.integration.cxml.model.cxml.Identity;
import com.apd.phoenix.service.integration.cxml.model.cxml.ObjectFactory;
import com.apd.phoenix.service.integration.cxml.model.cxml.Sender;
import com.apd.phoenix.service.integration.cxml.model.cxml.SharedSecret;
import com.apd.phoenix.service.integration.cxml.model.cxml.To;
import com.apd.phoenix.service.model.CxmlCredential;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author nreidelb
 */
@RequestScoped
public class CxmlRequestHeaderFactory {

    private static final Logger logger = LoggerFactory.getLogger(CxmlRequestHeaderFactory.class);

    private static final ObjectFactory factory = new ObjectFactory();

    @Inject
    private CredentialBp credentialBp;
    private com.apd.phoenix.service.model.Credential shoppingCredential;

    public Header generateOutgoingToCustomerHeader(Long credentialId) {
        Header header = factory.createHeader();
        shoppingCredential = credentialBp.findById(credentialId, com.apd.phoenix.service.model.Credential.class);
        From from = factory.createFrom();
        from.getCredential().addAll(generateHeaderCredentials(shoppingCredential.getCustomerOutoingFromCred()));
        To to = factory.createTo();
        to.getCredential().addAll(generateHeaderCredentials(shoppingCredential.getCustomerOutoingToCred()));
        Sender sender = factory.createSender();
        sender.setUserAgent(USER_AGENT);
        sender.getCredential().addAll(generateHeaderCredentials(shoppingCredential.getCustomerOutoingSenderCred()));
        if (!StringUtils.isEmpty(shoppingCredential.getCustomerOutgoingSharedSecret())) {
            SharedSecret secret = factory.createSharedSecret();
            secret.getContent().add(shoppingCredential.getCustomerOutgoingSharedSecret());
            if (!sender.getCredential().isEmpty()) {
                sender.getCredential().get(0).getSharedSecretOrDigitalSignatureOrCredentialMac().add(secret);
            }
            else {
                logger.warn("Outgoing Customer CXML created without sender credential.");
            }
        }
        header.setFrom(from);
        header.setTo(to);
        header.setSender(sender);
        return header;
    }

    public Header generateReturnCartOutgoingToCustomerHeader(Long credentialId) {
        Header header = factory.createHeader();
        shoppingCredential = credentialBp.findById(credentialId, com.apd.phoenix.service.model.Credential.class);
        From from = factory.createFrom();
        from.getCredential().addAll(generateHeaderCredentials(shoppingCredential.getCustomerOutoingFromCred()));
        To to = factory.createTo();
        to.getCredential().addAll(generateHeaderCredentials(shoppingCredential.getCustomerOutoingToCred()));
        Sender sender = factory.createSender();
        sender.setUserAgent(USER_AGENT);
        sender.getCredential().addAll(generateAPDReturnCartCredential(shoppingCredential));
        header.setFrom(from);
        header.setTo(to);
        header.setSender(sender);
        return header;
    }

    public Header generateOutgoingToSupplierHeader(Long credentialId) {
        Header header = factory.createHeader();
        shoppingCredential = credentialBp.findById(credentialId, com.apd.phoenix.service.model.Credential.class);

        //        From from = factory.createFrom();
        //        from.getCredential().addAll(generateHeaderCredentials(shoppingCredential.getOutgoingFromCredentials()));
        //        To to = factory.createTo();
        //        to.getCredential().addAll(generateHeaderCredentials(shoppingCredential.getOutgoingToCredentials()));
        //        Sender sender = factory.createSender();
        //        from.getCredential().addAll(generateHeaderCredentials(shoppingCredential.getOutgoingFromCredentials()));
        //        From from = generateFrom(shoppingCredential.getOutgoingFromCredentials());
        //        To to = generateTo(shoppingCredential.getOutgoingToCredentials());
        //        Sender sender = generateSender(shoppingCredential.getOutgoingSenderCredentials());
        //        addSharedSecret(sender);
        header.setFrom(generateOutgoingFrom(shoppingCredential));
        header.setTo(generateOutgoingTo(shoppingCredential));
        header.setSender(generateOutgoingSender(shoppingCredential));
        return header;
    }

    private void addSharedSecret(Sender sender) {
        SharedSecret sharedSecret = factory.createSharedSecret();
        sharedSecret.getContent().add(shoppingCredential.getOutgoingSenderSharedSecret());
        sender.getCredential().get(0).getSharedSecretOrDigitalSignatureOrCredentialMac().add(sharedSecret);
    }

    private static Set<Credential> generateHeaderCredentials(Set<CxmlCredential> configuredCredentialSet) {
        Set<Credential> headerCredentialSet = new HashSet<Credential>();
        for (CxmlCredential cxmlCredential : configuredCredentialSet) {
            Credential headerCredential = factory.createCredential();
            headerCredential.setDomain(cxmlCredential.getDomain());
            Identity identity = factory.createIdentity();
            identity.getContent().add(cxmlCredential.getIdentifier());
            headerCredential.setIdentity(identity);
            headerCredentialSet.add(headerCredential);
        }
        return headerCredentialSet;
    }

    private static Set<Credential> generateAPDReturnCartCredential(com.apd.phoenix.service.model.Credential credential) {
        Set<Credential> headerCredentialSet = new HashSet<Credential>();
        Credential headerCredential = factory.createCredential();
        headerCredential.setDomain(credential.getReturnCartCredentialDomain());
        Identity identity = factory.createIdentity();
        identity.getContent().add(credential.getReturnCartCredentialIdentity());
        headerCredential.setIdentity(identity);
        headerCredentialSet.add(headerCredential);
        return headerCredentialSet;
    }

    private From generateOutgoingFrom(com.apd.phoenix.service.model.Credential shoppingCredential) {
        From from = factory.createFrom();
        from.getCredential().addAll(generateHeaderCredentials(shoppingCredential.getOutgoingFromCredentials()));
        return from;
    }

    private To generateOutgoingTo(com.apd.phoenix.service.model.Credential shoppingCredential) {
        To to = factory.createTo();
        to.getCredential().addAll(generateHeaderCredentials(shoppingCredential.getOutgoingToCredentials()));
        return to;
    }

    private Sender generateOutgoingSender(com.apd.phoenix.service.model.Credential shoppingCredential) {
        Sender sender = factory.createSender();
        sender.getCredential().addAll(generateHeaderCredentials(shoppingCredential.getOutgoingSenderCredentials()));
        sender.setUserAgent(USER_AGENT);
        SharedSecret sharedSecret = factory.createSharedSecret();
        sharedSecret.getContent().add(shoppingCredential.getOutgoingSenderSharedSecret());
        for (Credential credential : sender.getCredential()) {
            credential.getSharedSecretOrDigitalSignatureOrCredentialMac().add(sharedSecret);
        }
        return sender;
    }

    @Deprecated
    private From generateFrom(Set<CxmlCredential> fromCredentialSet) {
        From from = factory.createFrom();
        for (CxmlCredential fromCred : fromCredentialSet) {
            Credential fromCredential = factory.createCredential();
            fromCredential.setDomain(fromCred.getDomain());
            Identity fromIdentity = factory.createIdentity();
            fromIdentity.getContent().add(fromCred.getIdentifier());
            fromCredential.setIdentity(fromIdentity);
            from.getCredential().add(fromCredential);
        }
        return from;
    }

    @Deprecated
    private To generateTo(Set<CxmlCredential> toCredentialSet) {
        To to = factory.createTo();
        for (CxmlCredential toCred : toCredentialSet) {
            Credential toCredential = factory.createCredential();
            toCredential.setDomain(toCred.getDomain());
            Identity toIdentity = factory.createIdentity();
            toIdentity.getContent().add(toCred.getIdentifier());
            toCredential.setIdentity(toIdentity);
            to.getCredential().add(toCredential);
        }
        return to;
    }

    @Deprecated
    private Sender generateSender(Set<CxmlCredential> senderCredentialSet) {
        Sender sender = factory.createSender();
        for (CxmlCredential senderCred : senderCredentialSet) {
            Credential senderCredential = factory.createCredential();
            senderCredential.setDomain(senderCred.getDomain());
            Identity senderIdentity = factory.createIdentity();
            senderIdentity.getContent().add(senderCred.getIdentifier());
            senderCredential.setIdentity(senderIdentity);
            sender.getCredential().add(senderCredential);
        }
        sender.setUserAgent(USER_AGENT);
        return sender;
    }
}
