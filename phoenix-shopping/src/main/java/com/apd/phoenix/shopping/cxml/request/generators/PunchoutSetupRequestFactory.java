package com.apd.phoenix.shopping.cxml.request.generators;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.core.EncryptionUtils;
import com.apd.phoenix.core.utility.TenantConfigRepository;
import com.apd.phoenix.service.business.CredentialBp;
import com.apd.phoenix.service.integration.cxml.model.cxml.Address;
import com.apd.phoenix.service.integration.cxml.model.cxml.BrowserFormPost;
import com.apd.phoenix.service.integration.cxml.model.cxml.BuyerCookie;
import com.apd.phoenix.service.integration.cxml.model.cxml.CXML;
import com.apd.phoenix.service.integration.cxml.model.cxml.Extrinsic;
import com.apd.phoenix.service.integration.cxml.model.cxml.Header;
import com.apd.phoenix.service.integration.cxml.model.cxml.ObjectFactory;
import com.apd.phoenix.service.integration.cxml.model.cxml.PostalAddress;
import com.apd.phoenix.service.integration.cxml.model.cxml.PunchOutSetupRequest;
import com.apd.phoenix.service.integration.cxml.model.cxml.Request;
import com.apd.phoenix.service.integration.cxml.model.cxml.ShipTo;
import com.apd.phoenix.service.integration.cxml.model.cxml.URL;
import com.apd.phoenix.service.model.Credential;
import com.apd.phoenix.service.model.CxmlConfiguration;
import com.apd.phoenix.service.model.CxmlConfiguration.DeploymentMode;
import com.apd.phoenix.service.persistence.multitenancy.api.CurrentTenantDomain;
import com.apd.phoenix.service.persistence.multitenancy.impl.CurrentTenantIdentifierResolverImpl;
import com.apd.phoenix.shopping.view.jsf.bean.login.CredentialSelectionBean;

/**
 *
 * @author dnorris
 */
@RequestScoped
public class PunchoutSetupRequestFactory extends BaseCxmlMessageGenerator {

    private static final Logger LOGGER = LoggerFactory.getLogger(PunchoutSetupRequestFactory.class);
    private static final ObjectFactory CXML_FACTORY = new ObjectFactory();
    private static final String CXML_ENDPOINT_REQUEST_PATH = properties.getString("CXML_ENDPOINT_REQUEST_PATH");
    private static final String DEFAULT_HTTP_PORT = "80";
    private static final String DEFAULT_HTTPS_PORT = "443";

    @Inject
    private CredentialSelectionBean credentialSelectionBean;

    @Inject
    private CredentialBp credentialBp;

    @Inject
    private CxmlRequestHeaderFactory cxmlRequestHeaderFactory;

    @Inject
    private CurrentTenantDomain currentTenantDomain;

    private Credential credential;
    private String punchoutZipCode;
    private String punchoutReturnAuthority;

    @PostConstruct
    private void init() {

        // Reload punchoutSession and credential from the current persistence context
        credential = credentialBp.findById(credentialSelectionBean.getCurrentAXCXU().getCredential().getId(),
                Credential.class);

        if (credentialSelectionBean.getPunchoutBuyerCookie() == null
                || credentialSelectionBean.getPunchoutOperation() == null) {
            credentialSelectionBean.setupExternalPunchoutSession();
        }

        try {
            String cxmlEndpointScheme = FacesContext.getCurrentInstance().getExternalContext().getRequestScheme();
            String cxmlEndpointServerName = FacesContext.getCurrentInstance().getExternalContext()
                    .getRequestServerName();
            String cxmlEndpointServerPort = Integer.toString(FacesContext.getCurrentInstance().getExternalContext()
                    .getRequestServerPort());
            punchoutReturnAuthority = cxmlEndpointScheme + "://" + cxmlEndpointServerName;
            if (!DEFAULT_HTTP_PORT.equals(cxmlEndpointServerPort) && !DEFAULT_HTTPS_PORT.equals(cxmlEndpointServerPort)) {
                punchoutReturnAuthority += ":" + cxmlEndpointServerPort;
            }
        }
        catch (Exception e) {
            throw new RuntimeException("No punchout return authority could be determined.");
        }
    }

    public CXML generatePunchoutSetupRequest(String punchoutZipCode) {
        this.punchoutZipCode = punchoutZipCode;
        return generatePunchoutSetupRequest();
    }

    public CXML generatePunchoutSetupRequest() {
        CXML cxml = CXML_FACTORY.createCXML();
        Header header = cxmlRequestHeaderFactory.generateOutgoingToSupplierHeader(credential.getId());
        Request request = generateCxmlRequestBody();
        cxml.getHeaderOrMessageOrRequestOrResponse().add(header);
        cxml.getHeaderOrMessageOrRequestOrResponse().add(request);
        cxml.setPayloadID(generatePayloadId(currentTenantDomain.getDomain()));
        cxml.setTimestamp(generateTimeStamp());
        cxml.setXmlLang(ENGLISH);
        return cxml;
    }

    private Request generateCxmlRequestBody() {
        Request request = CXML_FACTORY.createRequest();
        PunchOutSetupRequest punchoutSetupRequest = generatePunchoutSetupRequestBody();
        request
                .getProfileRequestOrOrderRequestOrMasterAgreementRequestOrPurchaseRequisitionRequestOrPunchOutSetupRequestOrProviderSetupRequestOrStatusUpdateRequestOrGetPendingRequestOrSubscriptionListRequestOrSubscriptionContentRequestOrSupplierListRequestOrSupplierDataRequestOrSubscriptionStatusUpdateRequestOrCopyRequestOrCatalogUploadRequestOrAuthRequestOrDataRequestOrOrganizationDataRequest()
                .add(punchoutSetupRequest);
        request.setDeploymentMode(getDeploymentMode());
        return request;
    }

    private String getDeploymentMode() {
        DeploymentMode deploymentMode = this.credential.getDeploymentMode();
        return deploymentMode != null ? deploymentMode.getLabel() : CxmlConfiguration.DeploymentMode.TEST.getLabel();
    }

    private PunchOutSetupRequest generatePunchoutSetupRequestBody() {
        PunchOutSetupRequest punchoutSetupRequest = CXML_FACTORY.createPunchOutSetupRequest();

        BrowserFormPost browserFormPost = generateBrowserFormPost();
        punchoutSetupRequest.setBrowserFormPost(browserFormPost);

        BuyerCookie buyerCookie = generateBuyerCookie();
        punchoutSetupRequest.setBuyerCookie(buyerCookie);

        ShipTo shipTo = generateShipTo();
        punchoutSetupRequest.setShipTo(shipTo);

        punchoutSetupRequest.getExtrinsic().addAll(generateExtrinsicFields());
        if (credentialSelectionBean.getPunchoutOperation() != null) {
            punchoutSetupRequest.setOperation(credentialSelectionBean.getPunchoutOperation().name());
        }
        else {
            LOGGER.error("Null punchout operation on CredentialSelectionBean");
        }

        return punchoutSetupRequest;
    }

    //TODO stop hardcoding this and dynamically determine which extrinsic fields to include
    private List<Extrinsic> generateExtrinsicFields() {
        List<Extrinsic> extrinsicFields = new ArrayList<>();
        String userName = credentialSelectionBean.getCurrentAXCXU().getUser().getLogin();
        Extrinsic apdUserName = new Extrinsic();
        apdUserName.setName("APDUserName");
        apdUserName.getContent().add(userName);
        extrinsicFields.add(apdUserName);
        Extrinsic uniqueName = new Extrinsic();
        uniqueName.setName("UniqueName");
        uniqueName.getContent().add(userName + "." + EncryptionUtils.randomAlphanumeric(7));
        extrinsicFields.add(uniqueName);
        Extrinsic userEmail = new Extrinsic();
        userEmail.setName("UserEmail");
        userEmail.getContent().add(userName + "@" + TenantConfigRepository.getInstance().getTenantDefaultDomainById(CurrentTenantIdentifierResolverImpl.getCurrentTenant()));
        extrinsicFields.add(userEmail);
        return extrinsicFields;
    }

    private ShipTo generateShipTo() {
        ShipTo shipTo = CXML_FACTORY.createShipTo();
        Address address = CXML_FACTORY.createAddress();
        PostalAddress postalAddress = CXML_FACTORY.createPostalAddress();
        //TODO dynamically determine this value
        if (StringUtils.isNotBlank(punchoutZipCode)) {
            postalAddress.setPostalCode(punchoutZipCode);
        }
        else if (StringUtils.isNotBlank(credential.getDefaultPunchoutZipCode())) {
            postalAddress.setPostalCode(credential.getDefaultPunchoutZipCode());
        }
        else {
            postalAddress.setPostalCode("00000");
        }
        address.setPostalAddress(postalAddress);
        //TODO possibly dynamically determine this value
        address.setIsoCountryCode("US");
        shipTo.setAddress(address);
        return shipTo;
    }

    private BuyerCookie generateBuyerCookie() {
        BuyerCookie buyerCookie = CXML_FACTORY.createBuyerCookie();
        buyerCookie.getContent().add(credentialSelectionBean.getPunchoutBuyerCookie());
        return buyerCookie;
    }

    private BrowserFormPost generateBrowserFormPost() {
        BrowserFormPost browserFormPost = CXML_FACTORY.createBrowserFormPost();
        if (punchoutReturnAuthority == null) {
            throw new RuntimeException("No punchout return authority could be determined.");
        }
        URL url = CXML_FACTORY.createURL();
        url.setvalue(punchoutReturnAuthority + CXML_ENDPOINT_REQUEST_PATH);
        browserFormPost.setURL(url);
        return browserFormPost;
    }
}
