package com.apd.phoenix.shopping.stockcheck;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.core.utility.EcommercePropertiesLoader;
import com.sprich.sprws.sprws.StockCheck_php.StockCheckInputs;
import com.sprich.sprws.sprws.StockCheck_php.StockCheckLocator;
import com.sprich.sprws.sprws.StockCheck_php.StockCheckResults;
import com.sprich.sprws.sprws.StockCheck_php.StockCheckRow;

/**
 * This class provides functionality to communicate with SP Richards to check the quantity for an item.
 *
 * @author RHC
 *
 */
@Stateless
@LocalBean
public class SpRichardsStockCheck extends AbstractStockCheck {

    private static final Logger logger = LoggerFactory.getLogger(SpRichardsStockCheck.class);

    private static final String STOCK_CHECK_VENDOR_PARTNERID = "sprichards";

    private static final PropertiesConfiguration properties = EcommercePropertiesLoader.getInstance()
            .getEcommerceProperties();

    private static final String GROUPCODE = properties.getString("sprichards.stockcheck.groupcode");

    private static final String USER_ID = properties.getString("sprichards.stockcheck.userid");

    private static final String PASSWORD = properties.getString("sprichards.stockcheck.password");

    private static final String ACTION = "F";

    private static final String CUST_NUMBER = properties.getString("sprichards.stockcheck.customernumber");

    private static final String DC_NUMBER = "";

    private static final String SORT_BY = "A";

    private static final String MIN_IN_FULL_PACKS = "N";

    private static final String AVAILABLE_ONLY = "Y";

    /**
     * This method takes the vendor SKU of an item, and returns the quantity available.
     *
     * @param vendorSku
     * @return
     */
    public int available(String vendorSku) {
        if (logger.isDebugEnabled()) {
            logger.debug("Stock check for SP Richards item {}", vendorSku);
        }
        if (StringUtils.isBlank(PASSWORD)) {
            if (logger.isDebugEnabled()) {
                logger.debug("No SP Richards Stock Check password defined, assuming product is available.");
            }
            return Integer.MAX_VALUE;
        }
        StockCheckResults response = null;
        StockCheckInputs request = new StockCheckInputs();
        request.setGroupCode(GROUPCODE);
        request.setUserID(USER_ID);
        request.setPassword(PASSWORD);
        request.setAction(ACTION);
        request.setCustNumber(CUST_NUMBER);
        request.setDcNumber(DC_NUMBER);
        request.setItemNumber(vendorSku);
        request.setSortBy(SORT_BY);
        request.setMinInFullPacks(MIN_IN_FULL_PACKS);
        request.setAvailableOnly(AVAILABLE_ONLY);
        try {
            response = (new StockCheckLocator()).getStockCheckPort().stockCheck(request);
        }
        catch (Exception e) {
            logger.warn("Exception sending SP Richard stock check, assuming item is available", e);
            return Integer.MAX_VALUE;
        }
        if (response == null) {
            logger.warn("Null message, assuming item is available");
        }
        else if (response.getResultsRows() == null) {
            logger.warn("Null contents, with message {}, assuming item is available", response.getErrorMessage());
        }
        else {
            int available = 0;
            for (StockCheckRow row : response.getResultsRows()) {
                if (row != null && StringUtils.isNotBlank(row.getAvailable())) {
                    try {
                        available += Integer.parseInt(row.getAvailable());
                    }
                    catch (NumberFormatException e) {
                        logger.warn("Could not parse availability {}", row.getAvailable());
                    }
                }
            }
            if (logger.isDebugEnabled()) {
                logger.debug("Amount available from SP Richards: {}", available);
            }
            return available;
        }
        return Integer.MAX_VALUE;
    }

    @Override
    public boolean vendorNameMatches(String vendorName) {
        return STOCK_CHECK_VENDOR_PARTNERID.equalsIgnoreCase(vendorName);
    }
}
