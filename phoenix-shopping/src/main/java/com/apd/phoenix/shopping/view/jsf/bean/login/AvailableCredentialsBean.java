package com.apd.phoenix.shopping.view.jsf.bean.login;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Stateful;
import javax.enterprise.context.ConversationScoped;
import javax.faces.bean.ManagedBean;
import javax.inject.Inject;
import javax.inject.Named;
import javax.security.jacc.PolicyContext;
import javax.security.jacc.PolicyContextException;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.business.AccountXCredentialXUserBp;
import com.apd.phoenix.service.business.IpSubdomainRestriction;
import com.apd.phoenix.service.model.AccountXCredentialXUser;
import com.apd.phoenix.service.model.SystemUser;

@Named
@ManagedBean
@ConversationScoped
@Stateful
public class AvailableCredentialsBean implements Serializable {

    private static final long serialVersionUID = 1L;
    private static final Logger logger = LoggerFactory.getLogger(AvailableCredentialsBean.class);

    @Inject
    private LoginBean loginBean;

    @Inject
    private CredentialSelectionBean credBean;

    @Inject
    private AccountXCredentialXUserBp axcxuBp;

    @Inject
    private IpSubdomainRestriction restriction;

    private List<AccountXCredentialXUser> userCredentials;

    /**
     * Returns only AccountXCredentialXUsers for the current user that are valid/authorized
     * based on the requested url and the nature of the user web session 
     * (e.g. punchin from procurement platform or direct login)
     *
     * @return
     */
    public List<AccountXCredentialXUser> getUserCredentials() {
    	if (userCredentials != null) {
    		return userCredentials;
    	}
        SystemUser user = loginBean.getSystemUser();
        List<AccountXCredentialXUser> userCredentialsList = new ArrayList<>();
        if (credBean.isPunchoutSession() && credBean.getPunchoutSession().getAccntCredUser() != null) {
        	userCredentialsList.add(axcxuBp.findById(credBean.getPunchoutSession().getAccntCredUser().getId(), AccountXCredentialXUser.class));
        }
        else  {
        	userCredentialsList = axcxuBp.getAllAXCXU(user);
        }
        userCredentials = new ArrayList<>();
        try {
            HttpServletRequest request = (HttpServletRequest) PolicyContext
                    .getContext("javax.servlet.http.HttpServletRequest");
            for (AccountXCredentialXUser credential : userCredentialsList) {
                if (restriction.isAccXCredXUserValid(credential, request, credBean.isPunchoutSession())) {
                	userCredentials.add(credential);
                }
            }
        } catch (PolicyContextException ex) {
            logger.error(null, ex);
        }
        return userCredentials;
    }

    /**
     * Attempts to direct the user away from the credential selection page 
     */
    @Lock(LockType.WRITE)
    public void trySkipSelection() {
        //if a credential is currently selected, no selection is required
        if (credBean.getCurrentCredential() != null) {
            credBean.redirectToRequestedPageOrStartPage();
            return;
        }

        List<AccountXCredentialXUser> validShoppingCredentials = this.getUserCredentials();

        if (validShoppingCredentials == null || validShoppingCredentials.isEmpty()) {
            //if there are no credentials, user is kicked out
            logger.error("Could not select credential, invalidating session.");
            loginBean.logout();
        }
        else if (validShoppingCredentials.size() == 1) {
            //if there is exactly one available credential, it is used, and no selection is required
            credBean.setCurrentAXCXU(validShoppingCredentials.get(0));
            credBean.redirectToRequestedPageOrStartPage();
            return;
        }
    }
}