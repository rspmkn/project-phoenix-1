/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.shopping.view.jsf.bean;

import java.io.Serializable;
import java.util.Properties;
import javax.annotation.PostConstruct;
import javax.ejb.Stateful;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang.StringUtils;
import com.apd.phoenix.core.StringEscape;
import com.apd.phoenix.core.utility.TenantConfigRepository;
import com.apd.phoenix.service.business.AccountBp;
import com.apd.phoenix.service.business.IpSubdomainRestriction;
import com.apd.phoenix.service.persistence.multitenancy.api.CurrentTenantDomain;
import com.apd.phoenix.service.persistence.multitenancy.impl.CurrentTenantIdentifierResolverImpl;

/**
 * This class is used by the ecommerceMainTemplate.xthml template page to get
 * the appropriate css file for a specific customer.
 * <br /><br />
 * To set a CSS file for a particular account, save the file in
 * phoenix-shopping/src/main/webapp/resources/css/main, then add a property to
 * cssMap.properties and cssMapResponsive.properties, from the root account name
 * to the path of the CSS file.
 *
 * @author RHC
 *
 */
@Named
@Stateful
@RequestScoped
public class PageCssBean implements Serializable {

    private static final long serialVersionUID = 1L;
    private static final String CUSTOM_CSS_URL_SUFFIX = "/phoenix-css/";
    private static final String SUBDOMAIN_IMAGES_SUFFIX = "/images/";
    private static final String SUBDOMAIN_WELCOME_IMAGES_SUFFIX = "welcome-images/";
    private static final String SUBDOMAIN_LOGOS_SUFFIX = "logos/";
    private static final String SUBDOMAIN_CSS_LOCATION = "css/subDomain/";
    private static final String CSS_FILE_EXTENSION = ".css";

    private Properties themeMapProperties;
    private Properties tenantProperties;

    private String staticContentURL;
    private String tenantName;

    @Inject
    private AccountBp accountBp;

    @Inject
    private CurrentTenantDomain currentTenantDomain;

    @PostConstruct
    public void init() {
        tenantName = TenantConfigRepository.getInstance().getTenantNameByDomain(currentTenantDomain.getDomain());
        if (tenantName == null) {
            tenantName = TenantConfigRepository.getInstance().getTenantNameById(
                    CurrentTenantIdentifierResolverImpl.getCurrentTenant());
        }

        staticContentURL = TenantConfigRepository.getInstance().getProperty(tenantName, "tenant",
                "content.service.secure.tenant.domain");
        themeMapProperties = TenantConfigRepository.getInstance().getPropertiesByTenantName(tenantName, "themeMap");
        tenantProperties = TenantConfigRepository.getInstance().getPropertiesByTenantName(tenantName, "tenant");
    }

    public String getSubDomainBasedCssFile() {
        String cssKey = getSubdomainValue();
        if (cssKey.isEmpty() || cssKey.equals(IpSubdomainRestriction.GENERIC_SUBDOMAIN)) {
            return "";
        }
        return staticContentURL + CUSTOM_CSS_URL_SUFFIX + SUBDOMAIN_CSS_LOCATION + StringEscape.escapeForUrl(cssKey)
                + CSS_FILE_EXTENSION;
    }

    public String getSubdomainValue() {
        HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext()
                .getRequest();
        return IpSubdomainRestriction.ipAndDomainInfo(request)[1];
    }

    public String getSubDomainWelcomeMessage() {
        return this.getSubdomainSpecificProperty("_WelcomMessage");
    }

    public String getSubDomainWelcomeMessageHeader() {
        return this.getSubdomainSpecificProperty("_WelcomMessageHeader");
    }

    public String getSubDomainWelcomeImage() {
        return staticContentURL + SUBDOMAIN_IMAGES_SUFFIX + SUBDOMAIN_WELCOME_IMAGES_SUFFIX
                + this.getSubdomainSpecificProperty("_WelcomeImage");
    }

    public String getSubDomainLogo() {
        String imageName = this.getSubdomainSpecificProperty("_Logo");
        if (StringUtils.isNotEmpty(imageName)) {
            return staticContentURL + SUBDOMAIN_IMAGES_SUFFIX + SUBDOMAIN_LOGOS_SUFFIX + imageName;
        }
        else {
            return "";
        }
    }

    public String getCustomerServiceInfo() {
        return this.getSubdomainSpecificProperty("_customerServiceInfo");
    }

    public String getCallUsPhone() {
        return this.getSubdomainSpecificProperty("_callUsPhone");
    }

    public String getCallUsDays() {
        return this.getSubdomainSpecificProperty("_callUsDays");
    }

    public String getCallUsHrs() {
        return this.getSubdomainSpecificProperty("_callUsHrs");
    }

    public String getEmailUsAddress() {
        return this.getSubdomainSpecificProperty("_emailUsAddress");
    }

    public String getVisitUsSite() {
        return this.getSubdomainSpecificProperty("_visitUsSite");
    }

    public String getVisitUsSiteToDisplay() {
        return this.getSubdomainSpecificProperty("_visitUsSiteToDsiplay");
    }

    public String getFaceBook() {
        return this.getSubdomainSpecificProperty("_facebook");
    }

    public String getLinkedIn() {
        return this.getSubdomainSpecificProperty("_linkedin");
    }

    public String getYouTube() {
        return this.getSubdomainSpecificProperty("_youtube");
    }

    public String getWordPress() {
        return this.getSubdomainSpecificProperty("_wordpress");
    }

    public String getCopyRight() {
        return this.getSubdomainSpecificProperty("_copyright");
    }

    public String getAboutUsUrl() {
        return this.getSubdomainSpecificProperty("_aboutUsUrl");
    }

    public String getContactUsUrl() {
        return this.getSubdomainSpecificProperty("_contactUsUrl");
    }

    public String getReturnPolicyUrl() {
        return this.getSubdomainSpecificProperty("_returnPolicyUrl");
    }

    public String getTermsOfUseUrl() {
        return this.getSubdomainSpecificProperty("_termsOfUseUrl");
    }

    public String getSecurityUrl() {
        return this.getSubdomainSpecificProperty("_securityUrl");
    }

    public String getPrivacyUrl() {
        return this.getSubdomainSpecificProperty("_privacyUrl");
    }

    public String getPrivacyEmail() {
        return tenantProperties.getProperty("privacy_page_email_address");
    }

    public String getCreateUserRequestInstructions() {
        return this.getSubdomainSpecificProperty("_createUserRequest_instructions");
    }

    public String getShowTrustKeeperPopup() {
        return this.getSubdomainSpecificProperty("_showTrustKeeperPopup");
    }

    /**
     * Takes a property name, and returns the value of that property for the 
     * subdomain and tenant. Follows this logic:
     * 
     * If the tenant is APD, checks <subdomain>_<property_value>, then 
     * <generic_subdomain>_<property_value>. If the tenant isn't APD, checks
     * <tenantName>_<subdomain>_<property_value>, then 
     * <tenantName>_<generic_subdomain>_<property_value>, then
     * <generic_subdomain>_<property_value>.
     * 
     * @param propertyName
     * @return
     */
    private String getSubdomainSpecificProperty(String propertyName) {
        //sets the tenant name prefix for the values to fetch
        String tenantPrefix = tenantName + "_";
        //if the tenant is APD, sets the prefix to the empty string
        if (TenantConfigRepository.getInstance().getGenericTenantId().equals(
                TenantConfigRepository.getInstance().getTenantIdByName(tenantName))) {
            tenantPrefix = "";
        }
        //used to look up the tenant's subdomain-specific styling
        String subdomainKey = tenantPrefix + this.getSubdomainValue() + propertyName;
        //used to look up the tenant's generic styling
        String tenantKey = tenantPrefix + IpSubdomainRestriction.GENERIC_SUBDOMAIN + propertyName;
        //used to look up the site's generic styling 
        String genericKey = IpSubdomainRestriction.GENERIC_SUBDOMAIN + propertyName;
        if (themeMapProperties.containsKey(subdomainKey)) {
            return themeMapProperties.getProperty(subdomainKey, "");
        }
        else if (themeMapProperties.containsKey(tenantKey)) {
            return themeMapProperties.getProperty(tenantKey, "");
        }
        else {
            return themeMapProperties.getProperty(genericKey, "");
        }
    }

    public void validateEmailDomain(FacesContext context, UIComponent component, Object value) {
        if (value != null && !accountBp.validEmailAddress(value.toString(), getSubdomainValue())) {
            ((UIInput) component).setValid(false);
            FacesMessage message = new FacesMessage();
            message.setSeverity(FacesMessage.SEVERITY_ERROR);
            message.setSummary(TenantConfigRepository.getInstance().getProperty(tenantName, "ecommerce",
                    "registration.email.invalid.summary"));
            message.setDetail(TenantConfigRepository.getInstance().getProperty(tenantName, "ecommerce",
                    "registration.email.invalid.detail"));
            context.addMessage(null, message);
        }
    }
}
