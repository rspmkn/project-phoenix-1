/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.shopping.view.jsf.bean.search.productlookup;

import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import com.apd.phoenix.service.business.CatalogBp;
import com.apd.phoenix.service.model.CatalogXItem;
import com.apd.phoenix.service.product.Product;
import com.apd.phoenix.service.product.ProductCacheManager;
import com.apd.phoenix.service.product.ProductNotFoundException;
import com.apd.phoenix.service.workflow.WorkflowService;
import com.apd.phoenix.shopping.view.jsf.bean.MarketingImagesBean;

/**
 * Takes item information and returns the customer information. Ultimately, this information will be cached, 
 * but currently the system hits the database each time.
 * 
 * @author RHC
 */
@Named
@Stateless
public class ProductLookupServiceImpl implements ProductLookupService {

    @Inject
    private ProductCacheManager cacheManager;

    @Inject
    private CatalogBp catalogBp;

    @Deprecated
    @Override
    public Product getProduct(String apdSku, Long customerCatalogId) throws ProductNotFoundException {
        return this.getProduct(apdSku, WorkflowService.USSCO_VENDOR_NAME, catalogBp
                .getParentCatalogIdList(customerCatalogId));
    }

    @Override
    public Product getProduct(String apdSku, List<Long> customerCatalogId) throws ProductNotFoundException {
        return getProduct(apdSku, WorkflowService.USSCO_VENDOR_NAME, customerCatalogId);
    }

    @Deprecated
    @Override
    public Product getProduct(String apdSku, String vendorName, Long customerCatalogId) throws ProductNotFoundException {
        return this.getProduct(apdSku, vendorName, catalogBp.getParentCatalogIdList(customerCatalogId));
    }

    @Override
    public Product getProduct(String apdSku, String vendorName, List<Long> customerCatalogId)
            throws ProductNotFoundException {
        return populateInformation(cacheManager.getProduct(apdSku, vendorName, customerCatalogId));
    }

    @Override
    public Product getProduct(CatalogXItem item) {
        return populateInformation(new Product(item));
    }

    private Product populateInformation(Product product) {
        if (product == null) {
            return null;
        }
        if (product.getImageUrls().isEmpty()) {
            product.getImageUrls().add(MarketingImagesBean.getImageUrl("searchUtilities1"));
        }
        //set first image as main image
        for (String url : product.getImageUrls()) {
            product.setMainImageUrl(url);
            break;
        }
        return product;
    }
}
