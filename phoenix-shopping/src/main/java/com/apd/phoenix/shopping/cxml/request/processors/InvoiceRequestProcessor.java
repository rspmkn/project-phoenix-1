/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.shopping.cxml.request.processors;

import com.apd.phoenix.service.integration.cxml.model.invoice.CXML;
import com.apd.phoenix.service.integration.cxml.model.invoice.Credential;
import com.apd.phoenix.service.integration.cxml.model.invoice.Header;
import com.apd.phoenix.service.integration.cxml.model.invoice.Request;
import com.apd.phoenix.service.integration.cxml.model.invoice.Response;
import com.apd.phoenix.service.integration.cxml.model.invoice.SharedSecret;
import com.apd.phoenix.service.model.Account;
import com.apd.phoenix.shopping.cxml.response.InvoiceSchemaResponseProcessor;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import org.slf4j.LoggerFactory;

/**
 *
 * @author dnorris
 */
@Stateless
@LocalBean
public class InvoiceRequestProcessor extends BaseRequestProcessor {

    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(InvoiceRequestProcessor.class);

    @Inject
    private InvoiceSchemaResponseProcessor invoiceSchemaResponseProcessor;

    @Override
    public void processRequest(HttpServletRequest request, HttpServletResponse response, String cXML) {
        try(ByteArrayInputStream inputStream = new ByteArrayInputStream(cXML.getBytes())) {
            XMLInputFactory inputFactory = XMLInputFactory.newInstance();
            XMLEventReader reader = inputFactory.createXMLEventReader(inputStream);
            JAXBContext jaxbContext = JAXBContext.newInstance(CXML.class);
            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            CXML fulFillCxml = (CXML) jaxbUnmarshaller.unmarshal(reader);
            Response fulFillResponse = processRequest(fulFillCxml, cXML);
            invoiceSchemaResponseProcessor.initializeResponse(response, fulFillResponse);
        } catch (JAXBException | XMLStreamException | IOException ex) {
            logger.error("Error parsing cxml", ex);
            Response fulFillResponse = invoiceSchemaResponseProcessor.createInvalidResponse();
            invoiceSchemaResponseProcessor.initializeResponse(response, fulFillResponse);
        }
    }

    private Response processRequest(CXML cxml, String rawMessage) {
        //Send to queue
        Header header = null;
        Request request = null;
        for (Object o : cxml.getHeaderOrMessageOrRequestOrResponse()) {
            if (o instanceof Header) {
                header = (Header) o;
            }
            if (o instanceof Request) {
                request = (Request) o;
            }
        }
        if (header == null || request == null) {
            logger.error("Invalid cxml request recieved");
            return invoiceSchemaResponseProcessor.createInvalidResponse();
        }
        Account account = getAssociatedAccount(header);
        //TODO pass to processor
        return processRequest(request, rawMessage);
    }

    private Account getAssociatedAccount(Header header) throws IllegalStateException {
        Account toReturn;
        List<Credential> fromCredentials = header.getFrom().getCredential();
        List<Credential> toCredentials = header.getTo().getCredential();
        List<Credential> senderCredentials = header.getSender().getCredential();
        Map<String, String> fromCredentialMap = getCredentialMapFromList(fromCredentials);
        Map<String, String> toCredentialMap = getCredentialMapFromList(toCredentials);
        Map<String, String> senderCredentialMap = getCredentialMapFromList(senderCredentials);
        String senderSharedSecret = getSharedSecret(senderCredentials);
        try {
            toReturn = cxmlConfigurationBp.getConfigurationFromCxmlHeaderData(fromCredentialMap, toCredentialMap,
                    senderCredentialMap, senderSharedSecret);
        }
        catch (Exception ex) {
            logger.error("Exception validating cxml credentials", ex);
            throw new IllegalStateException("Could not locate account for the cxml credential set.");
        }
        if (toReturn == null) {
            logger.error("Account is null");
            throw new IllegalStateException("Could not locate account for the cxml credential set.");
        }
        return toReturn;
    }

    private String getSharedSecret(List<Credential> senderCredentials) {
        String sharedSecret = "";
        List<Object> nodeList = senderCredentials.get(0).getSharedSecretOrDigitalSignatureOrCredentialMac();
        for (Object o : nodeList) {
            if (o instanceof SharedSecret) {
                SharedSecret ss = (SharedSecret) o;
                sharedSecret = (String) ss.getContent().get(0);
            }
        }
        return sharedSecret;
    }

    private Map<String, String> getCredentialMapFromList(List<Credential> credentials) throws IllegalStateException {
        Map<String, String> credentialMap = new HashMap<>();
        for (Credential credential : credentials) {
            String identity = (String) credential.getIdentity().getContent().get(0);
            //Security check for duplicate domains in request
            if (!credentialMap.containsKey(credential.getDomain())) {
                credentialMap.put(credential.getDomain(), identity);
            } else {
                //reject request per the cxml user guide
                //("The receiver should reject the document if there are multiple credentials in a To, From,
                //  or Sender section that use different values but use the same domain.")
                throw new IllegalStateException("Message contains duplicate domain names");
            }
        }
        return credentialMap;
    }

    private Response processRequest(Request request, String rawMessage) {
        logger.error("Unsupported cxml request received");
        return invoiceSchemaResponseProcessor.createNotImplementedResponse();
    }
}
