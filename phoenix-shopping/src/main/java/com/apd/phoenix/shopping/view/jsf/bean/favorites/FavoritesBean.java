package com.apd.phoenix.shopping.view.jsf.bean.favorites;

import java.io.IOException;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.ejb.Stateful;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.core.PageNumberDisplay;
import com.apd.phoenix.core.PageNumberDisplay.PageSize;
import com.apd.phoenix.service.business.AccountXCredentialXUserBp;
import com.apd.phoenix.service.business.CatalogXItemBp;
import com.apd.phoenix.service.business.FavoritesListBp;
import com.apd.phoenix.service.model.AccountXCredentialXUser;
import com.apd.phoenix.service.model.CatalogXItem;
import com.apd.phoenix.service.model.FavoritesList;
import com.apd.phoenix.service.product.CatalogData.FavoritesListInfo;
import com.apd.phoenix.service.product.Product;
import com.apd.phoenix.shopping.view.jsf.bean.login.CredentialSelectionBean;
import com.apd.phoenix.shopping.view.jsf.bean.utils.CatalogDataBean;
import com.apd.phoenix.shopping.view.jsf.bean.utils.LinksBean;
import com.apd.phoenix.shopping.view.jsf.bean.utils.ViewUtils;
import com.google.gson.Gson;

/**
 *
 * @author Red Hat Consulting
 */
@Named
@Stateful
@SessionScoped
public class FavoritesBean implements Serializable {

    private static final String URL_ENCODING = "UTF-8";
    private static final Logger logger = LoggerFactory.getLogger(FavoritesBean.class);
    private static final long serialVersionUID = 1L;
    private static final String FAVORITES_LIST_PARAM = "favoritesName";
    private static final String FAVORITES_TYPE_PARAM = "favoritesType";
    private static final String FAVORITES_ID_PARAM = "favoritesId";
    private String favoritesListName;
    private FavoritesList selectedList;
    private List<CatalogXItem> selectedListItems;
    private String selectedFavoritesListName;
    private AccountXCredentialXUser user;
    @Inject
    private FavoritesListBp favoritesListBp;
    @Inject
    private AccountXCredentialXUserBp accountXCredentialXUserBp;
    @Inject
    private CredentialSelectionBean credentialSelectionBean;
    @Inject
    private CatalogXItemBp catalogXItemBp;
    @Inject
    private CatalogDataBean catalogDataBean;
    private String newListName;
    private String errorMessage;
    private List<FavoritesList> userFavoritesLists;
    private List<FavoritesListInfo> companyFavoritesLists;
    private Map<Long, Integer> listSizes;
    private PageNumberDisplay listPaginationWidget;
    private int previousFavoritesListPage;
    private PageSize pageSize = PageSize.FIVE;
    private PageNumberDisplay itemPaginationWidget;
    private int previousListItemPage;
    private PageSize itemPageSize = PageSize.TEN;
    private long accountId;
    private List<String> favoriteListNames;
    private Long selectedListId;
    @Inject
    private ViewUtils viewUtils;
    @Inject
    private LinksBean linksBean;
    private boolean showAll = false;

    @PostConstruct
    public void init() {
        accountId = credentialSelectionBean.getCurrentAXCXU().getId();
        user = accountXCredentialXUserBp.getUserForFavoritesManagement(accountId);
        initializePagination();
        favoriteListNames = accountXCredentialXUserBp.getFavoritesListNames(accountId);
        companyFavoritesLists = catalogDataBean.getCatalogData().getCompanyFavoritesLists();
        updateUserFavoritesList();
    }

    public void updateUserFavoritesList() {
        userFavoritesLists = accountXCredentialXUserBp.getInitializedFavoritesLists(accountId);
        listSizes = new HashMap<>();
    }

    public void refreshPage() {
        if (!allRequestParametersEmpty()) {

        }
        else if (!FacesContext.getCurrentInstance().isPostback()) {
            selectedList = null;
            errorMessage = "";
            newListName = "";
            initializePagination();
            showAll = false;
        }
        int start = (listPaginationWidget.getPage() - 1) * listPaginationWidget.getSize();
        userFavoritesLists = accountXCredentialXUserBp.getInitializedFavoritesLists(accountId, start, pageSize.toInt());
    }

    public void checkPermission() {
        if (!credentialSelectionBean.hasPermission("shopping maintain user favorites")) {
            try {
                HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance()
                        .getExternalContext().getResponse();
                response.sendError(HttpServletResponse.SC_FORBIDDEN);
                return;
            }
            catch (IOException e) {
                //do nothing
            }
        }
    }

    public void initializePagination() {
        int numFound = accountXCredentialXUserBp.getFavoritesListCount(accountId);
        listPaginationWidget = new PageNumberDisplay(numFound);
        listPaginationWidget.setPageSize(pageSize);
        int start = (listPaginationWidget.getPage() - 1) * listPaginationWidget.getSize();
        userFavoritesLists = accountXCredentialXUserBp.getInitializedFavoritesLists(accountId, start, pageSize.toInt());
        previousFavoritesListPage = 0;
    }

    public void updatePagination() {
        int currentPage = listPaginationWidget.getPage();
        int numFound = accountXCredentialXUserBp.getFavoritesListCount(accountId);
        listPaginationWidget = new PageNumberDisplay(numFound);
        listPaginationWidget.setPageSize(pageSize);
        listPaginationWidget.setPage(currentPage);
        int start = (listPaginationWidget.getPage() - 1) * listPaginationWidget.getSize();
        userFavoritesLists = accountXCredentialXUserBp.getInitializedFavoritesLists(accountId, start, pageSize.toInt());
        previousFavoritesListPage = 0;
        showAll = false;
    }

    public String getViewLink(String favoritesListName, long id, String favoritesListType) {
        String browseLink = linksBean.getBrowse();
        try {
            browseLink = linksBean.getBrowse() + "?" + FAVORITES_ID_PARAM + "="
                    + URLEncoder.encode((id == 0 ? "" : id + ""), URL_ENCODING) + "&" + FAVORITES_LIST_PARAM + "="
                    + URLEncoder.encode(favoritesListName, URL_ENCODING) + "&" + FAVORITES_TYPE_PARAM + "="
                    + URLEncoder.encode(favoritesListType, URL_ENCODING);
        }
        catch (UnsupportedEncodingException e) {
            logger.error("Unable to encode favorites list name in URL: " + favoritesListName);
            logger.debug("Error", e);
        }
        return browseLink;
    }

    public void saveSelectedList() {
    	logger.info("Updating the selected list");
        selectedList = favoritesListBp.update(selectedList);
        logger.info("Loading the selected list");
        selectedList = favoritesListBp.getHydratedFavoritesList(selectedList);
        selectedListItems = new ArrayList<>(favoritesListBp.getInitializedSet(selectedList.getId()));
        updateUserFavoritesList();
        if (!this.showAll) {
            updatePagination();
        }        
    }

    /**
     * Returns a subset of userFavoritesLists corresponding to the page of the list being viewed as determined by listPaginationWidget.
     * Use this method in contexts with pages.
     * 
     * @return paginated userFavoritesLists
     */
    public List<FavoritesList> getUserFavoritesListsPaginated() {
        if (listPaginationWidget.getPage() != previousFavoritesListPage) {
            previousFavoritesListPage = listPaginationWidget.getPage();
            int start = (listPaginationWidget.getPage() - 1) * listPaginationWidget.getSize();
            userFavoritesLists = accountXCredentialXUserBp.getInitializedFavoritesLists(accountId, start, pageSize
                    .toInt());
            selectedList = null;
        }
        return userFavoritesLists;
    }

    /**
     * Returns the entire userFavoritesLists
     * 
     * @return userFavoritesLists
     */
    public List<FavoritesList> getAllUserFavoritesLists() {
        userFavoritesLists = accountXCredentialXUserBp.getInitializedFavoritesLists(accountId);
        return userFavoritesLists;
    }

    public List<FavoritesList> getAllUserFavorites() {
        showAll = true;
        userFavoritesLists = accountXCredentialXUserBp.getInitializedFavoritesLists(accountId);
        return userFavoritesLists;
    }

    public List<FavoritesListInfo> getCompanyFavoritesLists() {
        return companyFavoritesLists;
    }

    public FavoritesList getSelectedList() {
        return this.selectedList;
    }

    public void setSelectedList(FavoritesList selectedList) {
    	if (selectedList == null) {
    		return;
    	}
        this.selectedList = favoritesListBp.getHydratedFavoritesList(selectedList);
        int numFound = favoritesListBp.getListItemCount(selectedList.getId());
        itemPaginationWidget = new PageNumberDisplay(numFound);
        previousListItemPage = 0;
        itemPaginationWidget.setPageSize(itemPageSize);
        int start = (itemPaginationWidget.getPage() - 1) * itemPaginationWidget.getSize();
        this.selectedListItems = new ArrayList<>(favoritesListBp.getInitializedSet(selectedList.getId(),start, itemPageSize.toInt()));
    }

    public void setSelectedListById() {
        if (this.selectedListId == null) {
            return;
        }
        FavoritesList list = this.favoritesListBp.findById(selectedListId, FavoritesList.class);
        this.setSelectedList(list);
    }

    public String getSelectedFavoritesListName() {
        return this.selectedFavoritesListName;
    }

    public void setSelectedFavoritesListName(String name) {
        this.selectedFavoritesListName = name;
    }

    public List<Product> getSelectedListItems() {
        if (selectedListItems == null) {
            return new ArrayList<>();
        } else {
            if (itemPaginationWidget.getPage() != previousListItemPage) {
            	previousListItemPage = itemPaginationWidget.getPage();
            	int start = (itemPaginationWidget.getPage() - 1) * itemPaginationWidget.getSize();
            	selectedListItems = new ArrayList<>(favoritesListBp.getInitializedSet(selectedList.getId(),start, itemPageSize.toInt()));
            	return viewUtils.convertCatalogXItemsToProducts(this.selectedListItems); 
            } else if(selectedList != null){
            	selectedListItems = new ArrayList<>(favoritesListBp.getInitializedSet(selectedList.getId()));
            	return viewUtils.convertCatalogXItemsToProducts(this.selectedListItems); 
            }
            return new ArrayList<>();
        }
    }

    public void addNewUserList() {
        if (StringUtils.isEmpty(newListName)) {
            errorMessage = "Enter a name for the new list.";
            return;
        }
        if (isUniqueListName()) {
            FavoritesList favoritesList = new FavoritesList();
            favoritesList.setName(newListName);
            user = accountXCredentialXUserBp.getUserForFavoritesManagement(user.getId());
            user.getFavorites().add(favoritesList);
            user = accountXCredentialXUserBp.update(user);
            favoriteListNames = accountXCredentialXUserBp.getFavoritesListNames(user.getId());
            errorMessage = "";
            newListName = "";
            updatePagination();
            updateUserFavoritesList();
        }
        else {
            errorMessage = "Enter a unique name for the new list.";
        }
    }

    public void addNewUserList(String newListName) {
        this.newListName = newListName;
        addNewUserList();
    }

    private boolean isUniqueListName() {
        for (String listName : favoriteListNames) {
            if (listName.equalsIgnoreCase(newListName)) {
                return false;
            }
        }
        return true;
    }

    public void removeSelectedList() {
        if (selectedList != null) {
            user.getFavorites().remove(selectedList);
            user = accountXCredentialXUserBp.update(user);
            selectedList = null;
            favoriteListNames = accountXCredentialXUserBp.getFavoritesListNames(user.getId());
            updateUserFavoritesList();
            if (!this.showAll) {
                updatePagination();
            }
            this.selectedListItems = null;
        }
    }

    public void removeFromSelectedList(Product product) {
        CatalogXItem toRemove = new CatalogXItem();
        logger.info("Removing product id[" + product.getId());
        toRemove.setId(product.getId());
        toRemove = catalogXItemBp.findById(toRemove.getId(), CatalogXItem.class);
        logger.info("Found candidate for removal=" + toRemove);
        selectedList.getUserItems().remove(toRemove);
        saveSelectedList();
        this.selectedListItems = null;
    }

    public String getNewListName() {
        return newListName;
    }

    public void setNewListName(String newListName) {
        this.newListName = newListName;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public String getProductDetailURL(Product product) {
        if (product != null) {
            return product.getDetailsLink();
        }
        return "";
    }

    public List<String> getFavoritesListNames() {
        List<FavoritesList> favoritesLists = accountXCredentialXUserBp
                .getUnInitializedFavoritesLists(credentialSelectionBean.getCurrentAXCXU().getId());
        List<String> listNames = new ArrayList<>();
        for (FavoritesList list : favoritesLists) {
            listNames.add(list.getName());
        }
        return listNames;
    }

    public String getFavoritesListJson() {
    	List<String> jsonList = new ArrayList<>();
    	if (this.userFavoritesLists != null) {
	        for (FavoritesList list : this.userFavoritesLists) {
	        	if (list != null) {
	        		jsonList.add(list.getName());
	        	}
	        }
    	}
        return "var favoritesDropdownData = " + (new Gson()).toJson(jsonList) + ";";
    }

    public int getPreviousFavoritesListPage() {
        return previousFavoritesListPage;
    }

    public void setPreviousFavoritesListPage(int previousFavoritesListPage) {
        this.previousFavoritesListPage = previousFavoritesListPage;
    }

    public int getPreviousListItemPage() {
        return previousListItemPage;
    }

    public void setPreviousListItemPage(int previousListItemPage) {
        this.previousListItemPage = previousListItemPage;
    }

    public PageSize getPageSize() {
        return pageSize;
    }

    public void setPageSize(PageSize pageSize) {
        this.pageSize = pageSize;
    }

    public PageSize getItemPageSize() {
        return itemPageSize;
    }

    public void setItemPageSize(PageSize itemPageSize) {
        this.itemPageSize = itemPageSize;
    }

    public PageNumberDisplay getListPaginationWidget() {
        return listPaginationWidget;
    }

    public PageNumberDisplay getItemPaginationWidget() {
        return itemPaginationWidget;
    }

    /**
     * @return the selectedListName
     */
    public Long getSelectedListId() {
        return selectedListId;
    }

    /**
     * @param selectedListName the selectedListName to set
     */
    public void setSelectedListId(Long selectedListId) {
        this.selectedListId = selectedListId;
    }

    private boolean allRequestParametersEmpty() {
        Iterator<String> requestParameters = FacesContext.getCurrentInstance().getExternalContext()
                .getRequestParameterNames();
        if (requestParameters.hasNext()) {
            return false;
        }
        return true;
    }

    public int getItemCount(Long id) {
        if (id == null) {
            return -1;
        }
        else if (!listSizes.containsKey(id)) {
            listSizes.put(id, favoritesListBp.getListItemCount(id));
        }
        return listSizes.get(id);
    }

    public boolean isUserListEmpty() {
        return listIsEmpty(userFavoritesLists);
    }

    private boolean listIsEmpty(List<FavoritesList> list) {
        if (list == null) {
            return true;
        }
        else if (list.isEmpty()) {
            return true;
        }
        else {
            return listHasOnlyNullElements(list);
        }
    }

    private boolean listHasOnlyNullElements(List<FavoritesList> list) {
        for (FavoritesList l : list) {
            if (l != null) {
                return false;
            }
        }
        return true;
    }

    public static String getFAVORITES_LIST_PARAM() {
        return FAVORITES_LIST_PARAM;
    }

    public static String getFAVORITES_TYPE_PARAM() {
        return FAVORITES_TYPE_PARAM;
    }

    public static String getFAVORITES_ID_PARAM() {
        return FAVORITES_ID_PARAM;
    }

    public String getFavoritesListName() {
        return favoritesListName;
    }

    public void setFavoritesListName(String favoritesListName) {
        this.favoritesListName = favoritesListName;
    }

}
