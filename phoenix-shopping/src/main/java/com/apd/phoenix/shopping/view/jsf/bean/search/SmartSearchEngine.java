package com.apd.phoenix.shopping.view.jsf.bean.search;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Set;
import javax.ejb.LocalBean;
import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.business.CatalogXItemBp;
import com.apd.phoenix.service.model.Catalog;
import com.apd.phoenix.service.model.Catalog.SearchType;
import com.apd.phoenix.service.model.CatalogXItem;
import com.apd.phoenix.service.model.CredentialPropertyType.CredentialPropertyTypeEnum;
import com.apd.phoenix.service.model.ItemClassificationType;
import com.apd.phoenix.service.model.Manufacturer;
import com.apd.phoenix.service.model.Matchbook;
import com.apd.phoenix.service.model.Sku;
import com.apd.phoenix.service.persistence.multitenancy.impl.CurrentTenantIdentifierResolverImpl;
import com.apd.phoenix.service.product.Product;
import com.apd.phoenix.service.product.ProductNotFoundException;
import com.apd.phoenix.service.search.integration.client.SmartSearchConstants;
import com.apd.phoenix.service.search.integration.client.backend.SmartSearchCatalogMaintenanceService;
import com.apd.phoenix.service.search.integration.client.frontend.SmartSearchFrontendService;
import com.apd.phoenix.service.utility.SmartSearchUtils;
import com.apd.phoenix.shopping.view.jsf.bean.MarketingImagesBean;
import com.apd.phoenix.shopping.view.jsf.bean.favorites.FavoritesViewBean;
import com.apd.phoenix.shopping.view.jsf.bean.login.CredentialSelectionBean;
import com.apd.phoenix.shopping.view.jsf.bean.search.ProductSort.SortOption;
import com.apd.phoenix.shopping.view.jsf.bean.search.productlookup.ProductLookupService;
import com.apd.phoenix.shopping.view.jsf.bean.utils.AttributeFilterType;
import com.apd.phoenix.shopping.view.jsf.bean.utils.FilterKey;
import com.apd.phoenix.shopping.view.jsf.bean.utils.LinksBean;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.LinkedHashMultimap;
import com.google.common.collect.Multimap;
import com.ussco.ws.ecatalog.catalog._1.AttributeType;
import com.ussco.ws.ecatalog.catalog._1.CategoryType;
import com.ussco.ws.ecatalog.catalog._1.FilterType;
import com.ussco.ws.ecatalog.catalog._1.FilterValueType;
import com.ussco.ws.ecatalog.catalog._1.GetItemsResponse;
import com.ussco.ws.ecatalog.catalog._1.GetRelatedItemResponse;
import com.ussco.ws.ecatalog.catalog._1.GetSearchResponse;
import com.ussco.ws.ecatalog.catalog._1.GetSuppliesFinderResponse;
import com.ussco.ws.ecatalog.catalog._1.ItemIndicatorType;
import com.ussco.ws.ecatalog.catalog._1.ItemType;
import com.ussco.ws.ecatalog.catalog._1.ItemType.ItemImage;
import com.ussco.ws.ecatalog.catalog._1.ItemType.SellingCopy;
import com.ussco.ws.ecatalog.catalog._1.ItemsEntryType;
import com.ussco.ws.ecatalog.catalog._1.MatchType;
import com.ussco.ws.ecatalog.catalog._1.MerchandisingZoneType;
import com.ussco.ws.ecatalog.catalog._1.RelatedItemResponseType;
import com.ussco.ws.ecatalog.catalog._1.RelatedItemResponseType.RelatedItem;
import com.ussco.ws.ecatalog.catalog._1.SortListType.Sort;
import com.ussco.ws.ecatalog.catalog._1.SortType;

@LocalBean
@Stateless
@Lock(LockType.READ)
@SmartSearch
public class SmartSearchEngine implements SearchEngine {

    private static final int MAXIMUM_NUMBER_OF_TOP_LEVEL_HIERARCHIES_IN_ECDB3 = 5;

    private static final String IMAGE_COMING_SOON_NAME = "NOA.JPG";

    private static final String IMAGE_HTTP_PREFIX = "https://";

    private static final int NUMBER_OF_FEATURED_PRODUCTS = 6;

    private static final Logger logger = LoggerFactory.getLogger(SmartSearchEngine.class);

    public static final String SORT_TYPE_BM = "BM";

    private static final String APD_SKU_NAME = "dealer";
    static final String DELIMITER_SPLITTER = "\\|\\|\\|";
    static final String INNER_FILTER_DELIMITER_SPLITTER = "\\| ";
    private static final String YES = "Y";
    private static final String NO = "N";
    private static final String SMART_SEARCH_CATEGORY = "Category";
    private static final String SMART_SEARCH_KEYWORD = "Keyword";

    @Inject
    private SmartSearchFrontendService smartSearchFrontendService;

    @Inject
    private LinksBean linksBean;

    @Inject
    private CredentialSelectionBean credentialSelectionBean;

    @Inject
    private ProductLookupService productLookupService;

    @Inject
    private CatalogXItemBp catalogXItemBp;

    @Inject
    private FavoritesViewBean favoritesViewBean;

    @Inject
    private ItemClassificationTypeBean classificationTypeBean;

    @Override
    public CategoryMenu buildCategoryMenu() {

    	logger.info("Building SmartSearch Category Menu");
    	CategoryMenu toReturn = new CategoryMenu();
    	
        GetSearchResponse smartSearchResponse = buildCategoryMenuSmartSearchQuery();
        Set<SearchHierarchy[]> facetFieldValues = new HashSet<>();
		for(CategoryType firstLevelCategory: smartSearchResponse.getResponseHeader().getCatNavBanner().getCategory()){
			List<SearchHierarchy> category = new ArrayList<>();
        	category.add(0, new SearchHierarchy(firstLevelCategory.getCategoryDescription(), firstLevelCategory.getCategoryId()));
        	for (CategoryType secondLevelCategory : firstLevelCategory.getCategory()) {
        		category.add(1, new SearchHierarchy(secondLevelCategory.getCategoryDescription(), secondLevelCategory.getCategoryId()));
        		facetFieldValues.add(category.toArray(new SearchHierarchy[0]));
        	}
		}
		String orderingString;
		if(facetFieldValues.size() < MAXIMUM_NUMBER_OF_TOP_LEVEL_HIERARCHIES_IN_ECDB3){
			orderingString  = ResourceBundle.getBundle("com.apd.phoenix.search.topLevelHierarcy").getString("smartSearch.ecdb3");
		} else {
			orderingString = ResourceBundle.getBundle("com.apd.phoenix.search.topLevelHierarcy").getString("smartSearch.ecdb2015");
		}
		toReturn.setOrderingString(orderingString);
		toReturn.buildCategoryMenu(facetFieldValues);
        return toReturn;
    }

    private GetSearchResponse buildCategoryMenuSmartSearchQuery() {
    	List<FilterType> filters = new ArrayList<>();
    	filters.add(SmartSearchUtils.getFilterType("", "Keyword", BigInteger.valueOf(1)));
        return smartSearchFrontendService.searchListItemsByKeyword(getSessionId(), getUserAgent(),
                credentialSelectionBean.getCatalogUidIdList(), credentialSelectionBean.getCatalogCoreUidIdList(),
                filters, BigInteger.ONE, BigInteger.ONE);
    }

    private GetSearchResponse buildHeartbeatSmartSearchQuery() {
    	List<FilterType> filters = new ArrayList<>();
    	filters.add(SmartSearchUtils.getFilterType("", "Keyword", BigInteger.valueOf(1)));
        return smartSearchFrontendService.searchListItemsByKeyword(getSessionId(), getUserAgent(),
        		new ArrayList<Long>(), new ArrayList<Long>(),
                filters, BigInteger.ONE, BigInteger.ONE);
    }

    @Override
    public int featuredProductsFound() {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public List<Product> featuredProductList(Integer start, Integer randomSort) {
        Catalog catalog = credentialSelectionBean.getCurrentCredential().getCatalog();
        List<CatalogXItem> randomCoreItems = catalogXItemBp.getRandomCoreItems(catalog, randomSort,
                NUMBER_OF_FEATURED_PRODUCTS);
        return this.getProductListFromCatalogXItemList(randomCoreItems);
    }

    @Override
    public Product expressOrderSearch(String sku) {
        if (StringUtils.isBlank(sku)) {
            return null;
        }
        QueryData query = new QueryData();
        query.setStart(0);
        query.setRows(1);
        query.setSearchString(sku);
        query.setSortOption(SortOption.BestMatch);
        query.setNewSearch(true);
        ResponseData response = this.query(query);
        if (response != null && response.getProducts() != null && !response.getProducts().isEmpty()) {
            if (response.getProducts().size() > 1) {
                logger.warn("Multiple items found with sku " + sku);
            }
            return response.getProducts().get(0);
        }
        return null;
    }

    @Override
    public Product getCatalogProductFromVendorNameAndApdSku(String vendorName, String apdSku)
            throws ProductNotFoundException {
        Product toReturn = getProductFromSmartSearch(apdSku, vendorName);
        if (toReturn == null || toReturn.getId() == null) {
            throw new ProductNotFoundException();
        }
        return toReturn;
    }

    @Override
    public Product getProductFromVendorNameAndApdSku(String vendorName, String apdSku) {
        return getProductFromSmartSearch(apdSku, vendorName);
    }

    @Override
    public Product getProductFromCatalogXItem(CatalogXItem item) {
        List<CatalogXItem> items = new ArrayList<CatalogXItem>();
        items.add(item);
        List<Product> resultList = this.getProductListFromCatalogXItemList(items);
        if (resultList != null && !resultList.isEmpty()) {
            return resultList.get(0);
        }
        return null;
    }

    private List<Product> getProductListFromCatalogXItemList(List<CatalogXItem> items) {
        List<String> skuList = new ArrayList<String>();
        List<String> vendorList = new ArrayList<String>();
        for (CatalogXItem item : items) {
            String apdSku = null;
            for (Sku sku : item.getItem().getSkus()) {
                if (sku.getType().getName().equals(APD_SKU_NAME)) {
                    apdSku = sku.getValue();
                    break;
                }
            }
            String vendorName = null;
            if (item.getItem().getVendorCatalog() != null && item.getItem().getVendorCatalog().getVendor() != null) {
                vendorName = item.getItem().getVendorCatalog().getVendor().getName();
            }
            if (apdSku != null) {
                skuList.add(apdSku);
                vendorList.add(vendorName);
            }
        }
        return getProductsFromSmartSearch(skuList, vendorList);
    }

    private Product getProductFromSmartSearch(String sku, String vendor) {
        List<String> skuList = new ArrayList<String>();
        List<String> vendorList = new ArrayList<String>();
        skuList.add(sku);
        vendorList.add(vendor);
        List<Product> productList = this.getProductsFromSmartSearch(skuList, vendorList);
        if (productList != null && !productList.isEmpty()) {
            return productList.get(0);
        }
        return null;
    }

    private List<Product> getProductsFromSmartSearch(List<String> skuList, List<String> vendorList) {
        List<Product> toReturn = new ArrayList<Product>();
        List<String> dealerPartNumberList = new ArrayList<String>();
        //generates the dealer item number for each sku, vendor pair
        //note: the dealer item number is the APD SKU, followed by a delimeter, followed by vendor name
        //this is to ensure that dealer item numbers are unique 
        for (int i = 0; i < skuList.size() && i < vendorList.size(); i++) {
            dealerPartNumberList.add(SmartSearchCatalogMaintenanceService.getDealerPartNumber(skuList.get(i),
                    vendorList.get(i)));
        }
        GetItemsResponse response = this.smartSearchFrontendService.getItem(getSessionId(), getUserAgent(),
                dealerPartNumberList, credentialSelectionBean.getCatalogUidIdList(), credentialSelectionBean
                        .getCatalogCoreUidIdList());

        //iterates through the skus and vendors, to find the item in the response that matches it
        for (int i = 0; i < skuList.size() && i < vendorList.size(); i++) {
            String dealerPartNumber = dealerPartNumberList.get(i);
            String sku = skuList.get(i);
            String vendor = vendorList.get(i);
            boolean inResponse = false;
            //iterates through the items in the response, with nullchecks, to see if one matches
            if (response != null && response.getItemsResponse() != null
                    && response.getItemsResponse().getItemEntry() != null) {
                List<ItemsEntryType> itemsEntryList = response.getItemsResponse().getItemEntry();
                for (ItemsEntryType type : itemsEntryList) {
                    if (type != null && type.getItem() != null) {
                        List<ItemType> itemTypeList = type.getItem();
                        for (ItemType itemType : itemTypeList) {
                            if (itemType != null && dealerPartNumber.equals(itemType.getDealerItemNumber())) {
                                toReturn.add(createProductFromItem(response.getItemsResponse().getItemEntry().get(0)
                                        .getItem().get(0), vendor));
                                inResponse = true;
                            }
                        }
                    }
                }
            }
            //if none matches, calls product lookup
            if (!inResponse) {
                try {
                    toReturn.add(productLookupService.getProduct(sku, vendor, credentialSelectionBean
                            .getCatalogIdList()));
                }
                catch (ProductNotFoundException e) {
                    logger.warn("Unable to find item with SKU {}", sku);
                    if (StringUtils.isNotBlank(vendor)) {
                        logger.warn("No vendor specified, could not fetch from DB");
                    }
                    toReturn.add(null);
                }
            }
        }
        return toReturn;
    }

    @Override
    public ResponseData query(QueryData query) {
    	
    	if (StringUtils.isNotBlank(query.getFavoritesName()) && StringUtils.isNotBlank(query.getFavoritesType())) {
    		return favoritesViewBean.query(query);
    	}

    	List<FilterType> filters = new ArrayList<>();
        
        BigInteger start = BigInteger.valueOf(query.getStart() + 1);
        BigInteger rows = BigInteger.valueOf(query.getRows());

        String keyWordString = query.getSearchString();
        if(query.getLowPrice() != -1){
        	String category = SmartSearchUtils.getCategoryCodeFromPrice(BigDecimal.valueOf(query.getLowPrice()));
        	category = credentialSelectionBean.getCurrentCredential().getCatalog().getId().toString() + "-" + category;
        	if(keyWordString == null){
        		keyWordString = category;
        	} else {
        		keyWordString = keyWordString + " " + category;
        	}
        }
        
        if (StringUtils.isNotBlank(query.getSearchEngineHierarchyKey()) 
        		&& !query.getSearchEngineHierarchyKey().equals(SolrSearcher.ALL_HIERARCHY_PATH)) {
        	Iterator<FilterType> filterIterator = filters.iterator();
        	//Remove previous filter value
        	while(filterIterator.hasNext()){
        		if(SMART_SEARCH_CATEGORY.equals(filterIterator.next().getFilterStyle())){
        			filterIterator.remove();
        		}
        	}
        	filters.add(SmartSearchUtils.getFilterType(query.getSearchEngineHierarchyKey(), SMART_SEARCH_CATEGORY, BigInteger.valueOf(2)));
        }        
        
        
        if (!StringUtils.isEmpty(query.getInkTonerManufacturerCode()) || !StringUtils.isEmpty(query.getInkTonerModelCode())) {
        	if(!StringUtils.isEmpty(query.getInkTonerManufacturerCode())){
        		filters.add(SmartSearchUtils.getFilterType(query.getInkTonerManufacturerCode(), "SuppliesFinderBrand", query.getSelectedManufacturer(), BigInteger.valueOf(3)));
        	}
        	if(!StringUtils.isEmpty(query.getInkTonerModelCode())){
        		filters.add(SmartSearchUtils.getFilterType(query.getInkTonerModelCode(), "SuppliesFinderModel", query.getInkTonerModel(), BigInteger.valueOf(3)));
        	}
        }      
        if(StringUtils.isNotBlank(query.getSelectedManufacturer())) {
        	filters.add(SmartSearchUtils.getFilterType(query.getSelectedManufacturer(), "Brand", BigInteger.valueOf(3)));
        } 
        if(StringUtils.isNotBlank(query.getCartridgeNumber())){
        	filters.add(SmartSearchUtils.getFilterType(query.getCartridgeNumber(),"Keyword", "Keyword Filter", BigInteger.valueOf(filters.size() +1),"ImageSupplies"));
        }
        if (StringUtils.isNotBlank(keyWordString)) {
        	filters.add(SmartSearchUtils.getFilterType(keyWordString, SMART_SEARCH_KEYWORD, BigInteger.valueOf(1)));
        }
        if (query.getRefineSearchStrings() != null) {
        	for (String refineSearchString : query.getRefineSearchStrings()) {
    			filters.add(SmartSearchUtils.getFilterType(refineSearchString, SMART_SEARCH_KEYWORD, BigInteger.valueOf(1)));
        	}
    	}
        if(!query.isShowNonCore() && StringUtils.isNotBlank(query.getContractFilterId())) {
        	filters.add(SmartSearchUtils.getFilterType(query.getContractFilterId(), "Contract", BigInteger.valueOf(4)));
        }
        if(StringUtils.isNotBlank(query.getGreenFilterId())){
        	FilterKey key = new FilterKey(query.getGreenFilterId(), "");
        	List<FilterKey> greenKeys = new ArrayList<FilterKey>();
        	greenKeys.add(key);
        	filters.add(createAppliedFilters("ItemIndicator", greenKeys, filters.size()));
        }
        
        if(StringUtils.isNotBlank(query.getActiveFiltersString())) {        	
        	query.getActiveFilters().addAll(Arrays.asList(query.getActiveFiltersString().split(DELIMITER_SPLITTER)));
        	int  counter = 4;
        	 for (String f : query.getActiveFilters()) {        		 
    			 if(f.indexOf("|") > 0) {
    				 String filterId = f.split(INNER_FILTER_DELIMITER_SPLITTER)[1].trim();
					filters.add(SmartSearchUtils.getFilterType(filterId, "Attribute", BigInteger.valueOf(counter++)));
    			 }
        		 
        	 }
        }
        
        GetSearchResponse searchResponse = smartSearchFrontendService.searchListItemsByKeyword(getSessionId(),
                getUserAgent(), 
                //Test catalog with all data
                //"_ECATALOG",
                credentialSelectionBean.getCatalogUidIdList(), credentialSelectionBean.getCatalogCoreUidIdList(),
                filters, this.getSortCodeString(query.getSortOption()), start, rows, query.isShowNonCore(), credentialSelectionBean.getCatalogIdList());
        if (searchResponse != null && searchResponse.getSearchResponse() != null && searchResponse.getSearchResponse().getItemPage() != null) {            
             return smartSearchResponseToResponseData(searchResponse, query.getSearchEngineHierarchyKey());            
        }
        return this.getEmptyResponseData();
    }

    private ResponseData getEmptyResponseData() {
        ResponseData toReturn = new ResponseData();
        toReturn.setShowCoreItemFilter(false);
        return toReturn;
    }

    private String getSortCodeString(SortOption sortOption) {
        switch (sortOption) {
            case BestMatch:
                return "BM";
            case SortByBrandAsc:
                return "BA";
            case SortByBrandDesc:
                return "BD";
            case MostPopular:
                return "MP";
            case SortBySkuAsc:
                return "IA";
            case SortBySkuDesc:
                return "ID";
            case SortByPriceAsc:
                return "PA";
            case SortByPriceDesc:
                return "PD";
            default:
                logger.warn("Invalid sort option specified " + sortOption.name());
                return "BM";
        }
    }

    private SortOption getSortOptionFromTypeString(String sortCode) {
        switch (sortCode) {
            case "BM":
                return SortOption.BestMatch;
            case "BA":
                return SortOption.SortByBrandAsc;
            case "BD":
                return SortOption.SortByBrandDesc;
            case "MP":
                return SortOption.MostPopular;
            case "IA":
                return SortOption.SortBySkuAsc;
            case "ID":
                return SortOption.SortBySkuDesc;
            default:
                logger.warn("Invalid sort code specified " + sortCode);
                return SortOption.BestMatch;
        }
    }

    @Override
    public String getSearchTermsJson() {
        return "'" + linksBean.getSmartSearchLookahead() + "'";
    }

    private String getSessionId() {
        ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
        return context.getSession(true).toString();
    }

    private String getUserAgent() {
        ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
        if (context.getUserPrincipal() != null && !StringUtils.isEmpty(context.getUserPrincipal().getName())) {
            return context.getUserPrincipal().getName();
        }
        else {
            return "apdSite";
        }
    }

    private ResponseData smartSearchResponseToResponseData(GetSearchResponse searchResponse, String heirarchyPath) {
        ResponseData toReturn = new ResponseData();

        if (searchResponse.getSearchResponse().getItemPage().getItems().getItem() != null) {

            if (searchResponse.getSearchResponse().getAppliedFilters() != null) {
                for (FilterType filter : searchResponse.getSearchResponse().getAppliedFilters().getFilter()) {
                    if (filter.getFilterValue() != null) {
                        for (FilterValueType subFilter : filter.getFilterValue()) {
                            if (StringUtils.isNotEmpty(subFilter.getAutoSpellCorrect())) {
                                toReturn
                                        .setAutoCorrectMessage("Your search for '" + subFilter.getValue()
                                                + "' was automatically corrected to '"
                                                + subFilter.getAutoSpellCorrect() + "'.");
                                toReturn.setAutoCorrectValue(subFilter.getAutoSpellCorrect());
                            }
                            if (StringUtils.isNotEmpty(subFilter.getDidYouMean())) {
                                toReturn.setDidYouMeanMessage(subFilter.getDidYouMean());
                            }
                        }
                    }
                    //sets hierarchy path
                    if (SMART_SEARCH_CATEGORY.equals(filter.getFilterDescription())) {
                        toReturn.setBreadcrumbs(new ArrayList<SearchHierarchy>());
                        for (FilterValueType filterValue : filter.getFilterValue()) {
                            toReturn.getBreadcrumbs().add(
                                    new SearchHierarchy(filterValue.getDescription(), filterValue.getValue()));
                        }
                    }
                }
            }
            if (StringUtils.isEmpty(heirarchyPath) && !toReturn.getBreadcrumbs().isEmpty()) {
                //Matched on hierarchy path without one being selected
                toReturn.setClearSearchString(true);
            }

            toReturn.setProducts(convertToProductList(searchResponse.getSearchResponse().getItemPage().getItems()
                    .getItem()));

            toReturn.setNumFound(searchResponse.getSearchResponse().getItemPage().getTotalResults().intValue());

            toReturn.setMatchingHierarchies(new ArrayList<SearchHierarchy>());
            if (toReturn.getBreadcrumbs() != null && !toReturn.getBreadcrumbs().isEmpty()) {
                toReturn.getMatchingHierarchies().add(
                        toReturn.getBreadcrumbs().get(toReturn.getBreadcrumbs().size() - 1));
            }

            //sets filter facets
            toReturn.setFilters(getFilters(searchResponse));
            toReturn.setGreenFilter(getGreenFilter(searchResponse));

            toReturn.setAvaliableCategoryFilters(getAvaliableCategoryFilters(searchResponse));

            //sets filter facets
            getShowCoreItemFilters(searchResponse, toReturn);

            if (searchResponse.getSearchResponse().getItemPage() != null
                    && searchResponse.getSearchResponse().getItemPage().getMerchandisingZone() != null) {
                List<MerchandisingZoneType> merchandisingZone = searchResponse.getSearchResponse().getItemPage()
                        .getMerchandisingZone();
                for (MerchandisingZoneType zone : merchandisingZone) {
                    if (zone.getHeadingText().equals("Featured Products")) {
                        toReturn.setFeaturedProducts(convertToProductList(zone.getItem()));
                    }
                }
            }

            //sets manufacturer facets
            toReturn.setManufacturers(getBrands(searchResponse));

            for (SortOption option : SortOption.values()) {
                boolean addOption = false;
                //if the sort option is the current applied sort, adds it to the available sort options
                SortType appliedSort = searchResponse.getSearchResponse().getAppliedSort();
                if (appliedSort != null && StringUtils.isNotBlank(appliedSort.getSortCode())
                        && option.equals(this.getSortOptionFromTypeString(appliedSort.getSortCode()))) {
                    addOption = true;
                }

                //if the sort option is in the list of available sorts, adds it to the available sort options
                if (searchResponse.getSearchResponse().getAvailableSorts() != null
                        && searchResponse.getSearchResponse().getAvailableSorts().getSort() != null) {
                    for (Sort sort : searchResponse.getSearchResponse().getAvailableSorts().getSort()) {
                        if (sort != null && StringUtils.isNotBlank(sort.getSortCode())
                                && option.equals(this.getSortOptionFromTypeString(sort.getSortCode()))) {
                            addOption = true;
                        }
                    }
                }

                //if there are less than SORT_BY_PRICE_MAX_QUANTITY results and the the sort option is sort by
                //price, adds it to the available sort options
                if (toReturn.getNumFound() <= SmartSearchFrontendService.SORT_BY_PRICE_MAX_QUANTITY) {
                    if (option.equals(SortOption.SortByPriceAsc) || option.equals(SortOption.SortByPriceDesc)) {
                        addOption = true;
                    }
                }

                if (addOption) {
                    toReturn.getSortOptions().add(option);
                }
            }

            //get appliedfilters
            toReturn.setAppliedFilters(getAppliedFiltersFromResponse(searchResponse));

            //check for cross reference
            if (searchResponse.getSearchResponse().getItemPage().getItems().getCrossReference() != null) {
                toReturn.setCrossReference(searchResponse.getSearchResponse().getItemPage().getItems()
                        .getCrossReference().getMatchDescription());
            }
        }
        return toReturn;
    }

    private Map<FilterKey, BigInteger> getGreenFilter(GetSearchResponse searchResponse) {
        Map<FilterKey, BigInteger> greenFilterOptions = new HashMap<FilterKey, BigInteger>();
        if (searchResponse.getSearchResponse().getAvailableFilters() != null) {
            for (FilterType filter : searchResponse.getSearchResponse().getAvailableFilters().getFilter())
                if ("ItemIndicator".equals(filter.getFilterStyle()) && "Green".equals(filter.getFilterDescription())) {
                    for (FilterValueType filterValue : filter.getFilterValue()) {
                        greenFilterOptions.put(new FilterKey(filterValue.getValue(), filterValue.getDescription()),
                                filterValue.getAvailableResults());
                    }
                }
        }
        return greenFilterOptions;
    }

    private List<Product> convertToProductList(List<ItemType> items) {
        List<Product> products = new ArrayList<>();
        for (ItemType item : items) {
        	if (item != null) {
	            products.add(this.createProductFromItem(item, null));
        	}
        }
        return products;
    }

    public static Multimap<AttributeFilterType, FilterKey> getFilters(GetSearchResponse searchResponse) {
        Multimap<AttributeFilterType, FilterKey> facetMap = HashMultimap.create();
        if (searchResponse.getSearchResponse().getAvailableFilters() != null
                && searchResponse.getSearchResponse().getAvailableFilters().getFilter() != null) {
            List<FilterType> filters = searchResponse.getSearchResponse().getAvailableFilters().getFilter();
            FilterKey filterKey = null;
            for (FilterType filter : filters) {
                if ("Attribute".equals(filter.getFilterStyle())) {
                    for (FilterValueType filterValue : filter.getFilterValue()) {
                        String value = filterValue.getDescription() + " ("
                                + filterValue.getAvailableResults().toString() + ")";
                        filterKey = new FilterKey(filterValue.getValue(), value);
                        AttributeFilterType attributeFilterType = new AttributeFilterType(
                                filter.getFilterDescription(), filter.getSequence());
                        facetMap.put(attributeFilterType, filterKey);
                    }
                }
            }
        }
        return facetMap;

    }

    public static void getShowCoreItemFilters(GetSearchResponse searchResponse, ResponseData responseData) {

        if (searchResponse.getSearchResponse().getAvailableFilters() != null
                && searchResponse.getSearchResponse().getAvailableFilters().getFilter() != null) {
            List<FilterType> filters = searchResponse.getSearchResponse().getAvailableFilters().getFilter();
            for (FilterType filter : filters) {
                if ("Contract".equals(filter.getFilterStyle())) {
                    for (FilterValueType filterValue : filter.getFilterValue()) {
                        if ("Y".equals(filterValue.getDescription())) {
                            responseData.setShowCoreItemFilter(true);
                            responseData.setContractFilterId(filterValue.getValue());
                            return;
                        }
                    }
                }
            }
        }
        responseData.setShowCoreItemFilter(false);

    }

    public static Map<FilterKey, Integer> getBrands(GetSearchResponse searchResponse) {
    	Map<FilterKey, Integer> manfacturerCount = new HashMap<>();
    	if(manfacturerCount.size() < 1 && searchResponse.getSearchResponse().getAvailableFilters() != null && searchResponse.getSearchResponse().getAvailableFilters().getFilter() != null){
	    	List<FilterType> filters = searchResponse.getSearchResponse().getAvailableFilters().getFilter();
	    	for(FilterType filter : filters) {
	    		
	    		if("Brand".equals(filter.getFilterStyle())) {
	    			FilterKey filterKey = null;
	    			for (FilterValueType filterValue : filter.getFilterValue()) {
	    				filterKey = new FilterKey(filterValue.getValue(), filterValue.getDescription());	    				
	    				manfacturerCount.put(filterKey, filterValue.getAvailableResults().intValue());
	    			}
	    		}
	    	}
    	}
    	return manfacturerCount;
    }

    private Map<FilterKey,Integer> getAvaliableCategoryFilters(GetSearchResponse searchResponse) {
    	Map<FilterKey,Integer> keys = new HashMap<>();
        if (searchResponse.getSearchResponse().getAvailableFilters() != null
                && searchResponse.getSearchResponse().getAvailableFilters().getFilter() != null) {
            List<FilterType> filters = searchResponse.getSearchResponse().getAvailableFilters().getFilter();
            FilterKey filterKey = null;
            for (FilterType filter : filters) {
                if ("Category".equals(filter.getFilterStyle())) {
                    for (FilterValueType filterValue : filter.getFilterValue()) {
                        String value = filterValue.getDescription();
                        filterKey = new FilterKey(filterValue.getValue(), value);
                        keys.put(filterKey, filterValue.getAvailableResults().intValue());
                    }
                }
            }
        }
        return keys;

    }

    public Map<String, String> getItemIndicator(Product product, List<ItemIndicatorType> indicatorList) {

    	List<ItemClassificationType> itemClassificationsType = classificationTypeBean.getItemClassificationTypes();
    	Map<String, String> iconMap = new HashMap<>();

    	if(itemClassificationsType.size() <= 0 || indicatorList.size() <= 0) {
    		return iconMap;
    	}
    	for (ItemIndicatorType indicator : indicatorList) {
    		for(ItemClassificationType ItemClassificationType : itemClassificationsType) {    			
    			if(indicator.getIndicatorType().equals(ItemClassificationType.getName()) 
    					&& StringUtils.isNotEmpty(indicator.getIndicatorValue())) {
    				switch (indicator.getIndicatorType()) {    				
    					
    					case "RelatedItemsAvailable" ://don't display related item icon
    					case "Stocked" 				 :
    					case "Warranty" 			 :
    					case "ValuePack" 			 :
    									 			 	break;
    						             
    					default 	   				 :	if(!NO.equals(indicator.getIndicatorValue())) {
															iconMap.put(ItemClassificationType.getIconUrl(), ItemClassificationType.getToolTipLabel());
						 				 			   	}            
    				}
    				    
    			}
    		}    		              
    	}
    	return iconMap;

    }

    @Override
    public Map<String, List<Product>> getRelatedItems(Product product) {
    	Map<String, List<Product>> toReturn = new HashMap<>();
    	if (product == null || StringUtils.isBlank(product.getVendorSku())) {
    		return toReturn;
    	}
    	GetRelatedItemResponse response = this.smartSearchFrontendService.getRelatedItems(getSessionId(), getUserAgent(), product.getVendorSku(), credentialSelectionBean.getCatalogUidIdList(), credentialSelectionBean.getCatalogCoreUidIdList());
        if (response == null || response.getRelatedItemResponse() == null || response.getRelatedItemResponse().getItemRelation() == null) {
        	return toReturn;
        }
        for (RelatedItemResponseType responseItem : response.getRelatedItemResponse().getItemRelation()) {
        	if (responseItem != null && responseItem.getRelatedItem() != null) {
        		for (RelatedItem responseItemInfo : responseItem.getRelatedItem()) {
        			if (responseItemInfo != null && StringUtils.isNotBlank(responseItemInfo.getRelationshipType()) && StringUtils.isNotBlank(responseItemInfo.getDealerItemNumber())) {
        				String apdSku = SmartSearchFrontendService.skuFromDealerPart(responseItemInfo.getDealerItemNumber());
        				String relationship = "";
        				switch (responseItemInfo.getRelationshipType()) {
        				
        				case "CompanionItems" : relationship = "COMPANION ITEMS";
        						                break;
        				case "Family"         : relationship = "PRODUCT FAMILY";
						  			            break;
        				case "Accessories"    : relationship = "ACCESSORIES";
  			            						break;
        				case "CrossSell"      : relationship = "CROSSSELL";
  			            						break;
        				case "Equivalent"     : relationship = "EQUIVALENT";
  			            						break;
        				case "Supplies"       : relationship = "SUPPLIES";
  			            						break;
        				case "Upsell"         : relationship = "UPSELL";
  												break;
        				case "ValueSell"      : relationship = "VALUESELL";
  												break;
  						default 			  : break;
  						
        				}
        				try {
							Product relatedProduct = productLookupService.getProduct(apdSku, credentialSelectionBean.getCatalogIdList());							
							if(responseItemInfo.getSellingPoint() != null && responseItemInfo.getSellingPoint().size() > 0) {
					        	List <String> sellingPoints = new ArrayList<String>();
						        for (ItemType.SellingPoint sellingpoint : responseItemInfo.getSellingPoint()) {
						        	sellingPoints.add(sellingpoint.getValue());
						        }
						        relatedProduct.setSellingPoints(sellingPoints);
					    	}
							
							if(responseItemInfo.getBrand() != null) {
								relatedProduct.setManufacturerName(responseItemInfo.getBrand().getBrandDescription());
					        }
							
							if(responseItemInfo.getItemIndicator() != null) {
								relatedProduct.setIconMap(getItemIndicator(relatedProduct, responseItemInfo.getItemIndicator()));
						    }
	        				if (!toReturn.containsKey(relationship)) {
	        					toReturn.put(relationship, new ArrayList<Product>());
	        				}
	        				toReturn.get(relationship).add(relatedProduct);
						} catch (ProductNotFoundException e) {
							//do nothing, item will not be added
							if (logger.isDebugEnabled()) {
								logger.debug("Not adding item with APD sku " + apdSku + "to related items");
							}
						}
        				
        			}
        		}
        	}
        }
        return toReturn;
    }

    @Override
    public Set<Matchbook> getMatchbooksByCatalogAndManufacturer(List<Long> catalogIds, Manufacturer manufacturer) {
        Set<Matchbook> matchbooks = new HashSet<Matchbook>();
        GetSuppliesFinderResponse response = smartSearchFrontendService.getMatchbooks(getSessionId(), getUserAgent(),
                credentialSelectionBean.getCatalogUidIdList(), credentialSelectionBean.getCatalogCoreUidIdList(),
                manufacturer);
        if (response.getSuppliesFinderResponse().getAvailableFilters().getFilter().size() != 1
                || response.getSuppliesFinderResponse().getAvailableFilters().getFilter().get(0).getFilterValue()
                        .size() != 1) {
            logger.error("Unexpected number of avalialbe filter types.");
        }
        for (FilterType topLevelFilter : response.getSuppliesFinderResponse().getAvailableFilters().getFilter()) {
            for (FilterValueType midLevelFilter : topLevelFilter.getFilterValue()) {
                for (FilterValueType filter : midLevelFilter.getSubFilter().getFilterValue()) {
                    Matchbook matchbook = new Matchbook();
                    matchbook.setModel(filter.getDescription());
                    matchbook.setSmartSearchId(filter.getValue());
                    matchbooks.add(matchbook);
                }
            }
        }
        return matchbooks;
    }

    @Override
    public Collection<Manufacturer> matchbookManufacturersFromCatalog(List<Long> catalogIds) {
        GetSuppliesFinderResponse response = smartSearchFrontendService.matchbookManufactuerersFromCatalog(
                credentialSelectionBean.getCatalogUidIdList(), credentialSelectionBean.getCatalogCoreUidIdList(),
                getSessionId(), getUserAgent());
        Collection<Manufacturer> manufacturers = new ArrayList<Manufacturer>();
        if (response != null && response.getSuppliesFinderResponse() != null
                && response.getSuppliesFinderResponse().getAvailableFilters() != null) {
            for (FilterType filter : response.getSuppliesFinderResponse().getAvailableFilters().getFilter()) {
                if ("SuppliesFinderBrand".equals(filter.getFilterStyle())) {
                    for (FilterValueType brand : filter.getFilterValue()) {
                        Manufacturer manufacturer = new Manufacturer();
                        manufacturer.setName(brand.getDescription());
                        manufacturer.setSmartSearchCode(brand.getValue());
                        manufacturers.add(manufacturer);
                    }
                }
            }
        }
        return manufacturers;
    }

    @Override
    public String inkAndTonerSearch(Matchbook selectedModel, Manufacturer selectedManufacturer) {
        return SearchUtilities.SEARCH_PAGE_BASE_URL + "?" + SearchUtilities.INK_TONER_MODEL_CODE + "="
                + selectedModel.getSmartSearchId() + "&" + SearchUtilities.INK_TONER_MODEL + "="
                + selectedModel.getModel() + "&" + SearchUtilities.INK_TONER_MANUFACTURER_CODE + "="
                + selectedManufacturer.getSmartSearchCode() + "&" + SearchUtilities.INK_TONER_MANUFACTURER + "="
                + selectedManufacturer.getName();
    }

    @Override
    public String searchByCartridgeNumber(String cartridgeNumber) {
        return SearchUtilities.SEARCH_PAGE_BASE_URL + "?" + SearchUtilities.INK_TONER_CARTRIDGE_NUMBER + "="
                + cartridgeNumber;
    }

    public static Multimap<String, FilterKey> getAppliedFiltersFromResponse(GetSearchResponse searchResponse) {
        Multimap<String, FilterKey> appliedFilters = LinkedHashMultimap.create();
        if (searchResponse.getSearchResponse().getAppliedFilters() != null
                && searchResponse.getSearchResponse().getAppliedFilters().getFilter() != null) {
            List<FilterType> filters = searchResponse.getSearchResponse().getAppliedFilters().getFilter();
            FilterKey filterKey = null;
            for (FilterType filter : filters) {
                for (FilterValueType filterValue : filter.getFilterValue()) {
                    String value = StringUtils
                            .defaultIfBlank(filterValue.getAutoSpellCorrect(), filterValue.getValue());
                    String description = StringUtils.defaultIfBlank(filterValue.getAutoSpellCorrect(), filterValue
                            .getDescription());
                    filterKey = new FilterKey(value, description);
                    appliedFilters.put(filter.getFilterStyle(), filterKey);
                }
            }
        }
        return appliedFilters;
    }

    public static FilterType createAppliedFilters(String type, Collection<FilterKey> filters, int sequence) {

        FilterType filterType = new FilterType();
        filterType.setFilterStyle(type);
        filterType.setFilterDescription(type);
        filterType.setCrossReference(MatchType.NONE);
        filterType.setSequence(BigInteger.valueOf(sequence));
        filterType.setKeywordInterface("Standard");
        int counter = 1;
        for (FilterKey filterKey : filters) {

            FilterValueType filterValueType = new FilterValueType();
            filterValueType.setSequence(BigInteger.valueOf(counter++));
            filterValueType.setValue(filterKey.getId());
            filterValueType.setDescription(filterKey.getFilter());
            filterType.getFilterValue().add(filterValueType);
        }
        return filterType;
    }

    private Product createProductFromItem(ItemType item, String vendor) {
    	Product product = null;
    	if (item == null) {
    		return product;
    	}

    	String apdSku = StringUtils.defaultIfBlank(SmartSearchFrontendService.skuFromDealerPart(item.getDealerItemNumber()), item.getItemNumber());
    	if (StringUtils.isBlank(apdSku)) {
    		return product;
    	}
    	
    	vendor = StringUtils.defaultIfBlank(vendor, "USSCO");
    	for (ItemIndicatorType indicator : item.getItemIndicator()) {
    		if (indicator.getIndicatorType().equals(SmartSearchConstants.VENDOR_NAME_SMARTSEARCH_INDICATOR) && StringUtils.isNotBlank(indicator.getIndicatorValue())) {
    			vendor = indicator.getIndicatorValue();
    		}
    	}

		try {
			//tries to pull the product from the cache/database
			product = productLookupService.getProduct(apdSku, vendor, credentialSelectionBean.getCatalogIdList());
		} catch (ProductNotFoundException e) {
			if (logger.isDebugEnabled()) {
				logger.debug("Unable to find product with APD SKU " + apdSku);
			}
			//if not in the cache, builds an empty, invalid product
            product = new Product();
            product.setId(0l);
            //sets the following values for products returned by SmartSearch that aren't in the DB
            product.setPrice(BigDecimal.ZERO);
            product.setVendorName(vendor);
            product.setApdSku(apdSku);
            product.setStatus("CONTACT_REQUIRED");
            product.setCustomerSku(apdSku);
            if (StringUtils.isEmpty(product.getCustomerSku())) {
                product.setCustomerSku("");
            }
            product.setCustomerReplacementSku(null);
            product.setCustomerReplacementVendor(null);
            product.setSpecialOrder(null);
            product.setCustomOrder(null);
		}

		//The logic for the product name is as follows:
		//1. If the description is not null, and the "smart search use dealer description" flag is not set to true, use the smart search description
		//2. Otherwise, if the dealer description is not null, use the dealer description
		//3. Otherwise, use the description on the cached Product
		String dealerDescriptionProperty = "false";
		if(credentialSelectionBean.getPropertyMap() != null){
			dealerDescriptionProperty = StringUtils.defaultIfBlank(credentialSelectionBean.getPropertyMap().get(CredentialPropertyTypeEnum.SMART_SEARCH_USE_DEALER_DESCRIPTION.getValue()), "false");
		}
		boolean dealerDescriptionOverride = dealerDescriptionProperty.equalsIgnoreCase("t") || dealerDescriptionProperty.equalsIgnoreCase("true");
		if (StringUtils.isNotBlank(item.getDescription()) && !dealerDescriptionOverride) {
			product.setName(item.getDescription());
		} else if (StringUtils.isNotBlank(item.getDealerDescription())) {
			product.setName(item.getDealerDescription());
		}
		if (StringUtils.isNotBlank(item.getManufacturerPartNumber())) {
			product.setManufacturerSku(item.getManufacturerPartNumber());
		}
		if (StringUtils.isNotBlank(item.getItemNumber())) {
			product.setVendorSku(item.getItemNumber());
		}
        if (item.getSellingCopy() != null) {
	        for (SellingCopy copy : item.getSellingCopy()) {
	        	if (copy.getStyle().equals("Long") && StringUtils.isNotBlank(copy.getValue())) {
	        		product.setDescription(copy.getValue());
	        	}
	        }
        }
        if (StringUtils.isBlank(product.getDescription())) {
	        if (StringUtils.isNotBlank(item.getDescription())) {
	        	product.setDescription(item.getDescription());
	        } else if (StringUtils.isNotBlank(item.getDealerDescription())) {
	        	product.setDescription(item.getDealerDescription());
	        }
        }
        //set Brand 
        if(item.getBrand() != null && StringUtils.isNotBlank(item.getBrand().getBrandDescription())) {
        	product.setManufacturerName(item.getBrand().getBrandDescription());
        }
        
        List<ItemImage> images = item.getItemImage();

        if (images != null && !images.isEmpty()) {
            product.setMainImageUrl(null); //will be set to a value below
            
            List<String> imageUrls = new ArrayList<>();
            //a map from the small image urls to the image title
            Map<String, String> smallImageToTitle = new HashMap<>();
            //a map from the image titles to the large images
            Map<String, String> titleTolargeImage = new HashMap<>();
            
        	//for each image, checks if it's a small or large image, then puts it in its map
            for (ItemImage image : images) {
            	String imageUrl = image.getImageURL();
            	if (imageUrl.toUpperCase().contains(IMAGE_COMING_SOON_NAME)) {
            		//the "NOA.JPG" image is an "image coming soon" placeholder that is included
            		//with some products with images. If there are no non-NOA images, then
            		//the "searchUtilities1" image will be used (see below)
            		continue;
            	}
            	if (!imageUrl.startsWith("http")) {
            		imageUrl = IMAGE_HTTP_PREFIX + imageUrl;
            	}
            	//if it's ItemStandardA, AltItem1StandardA, AltItem2StandardA, etc.
            	if (image.getImageType().endsWith("StandardA") || image.getImageType().startsWith("Dealer")) {
            		imageUrls.add(imageUrl);
            		smallImageToTitle.put(imageUrl, image.getImageTitle());
            		//the "ItemStandardA" image type is the primary 240x240 image for the item
            		if (image.getImageType().equals("ItemStandardA")) {
            			product.setMainImageUrl(imageUrl);
            		}
            	} 
            	//if it's ItemLarge, AltItem1Large, AltItem2Large, etc.
            	else if (image.getImageType().endsWith("Large")) {
            		titleTolargeImage.put(image.getImageTitle(), imageUrl);
            	}
            }
            product.setLargerImagesMap(new HashMap<String, String>());
            //finally, assembles the Product's largerImageMap, using the product titles to determine 
            //which small image has which larger image.
            for (String smallImageUrl : smallImageToTitle.keySet()) {
            	if (titleTolargeImage.containsKey(smallImageToTitle.get(smallImageUrl))) {
            		product.getLargerImagesMap().put(smallImageUrl, titleTolargeImage.get(smallImageToTitle.get(smallImageUrl)));
            	}
            }
            //If these are no images add a placeholder image
            if (imageUrls.isEmpty()) {
                imageUrls.add(MarketingImagesBean.getImageUrl("searchUtilities1"));
            }
            //For now set first image as main image
            if (StringUtils.isBlank(product.getMainImageUrl())) {
    	        for (String url : imageUrls) {
    	            product.setMainImageUrl(url);
    	            break;
    	        }
            }

            product.setImageUrls(imageUrls);
        }
        
        if (StringUtils.isNotBlank(item.getUnitOfMeasure())) {
        	product.setUnits(item.getUnitOfMeasure());
        } else if (StringUtils.isNotBlank(item.getDealerUnitOfMeasure())) {
        	product.setUnits(item.getDealerUnitOfMeasure());
        }

        if(item.getItemIndicator() != null) {
        	product.setIconMap(getItemIndicator(product, item.getItemIndicator()));
        }

        if(item.getSellingPoint() != null && item.getSellingPoint().size() > 0) {
        	List <String> sellingPoints = new ArrayList<String>();
	        for (ItemType.SellingPoint sellingpoint : item.getSellingPoint()) {
	        	sellingPoints.add(sellingpoint.getValue());
	        }
	        product.setSellingPoints(sellingPoints);
    	}
        
        if(StringUtils.isNotEmpty(item.getWarrantyText())) {
        	product.setWarranty(item.getWarrantyText());
        }
        
        if(StringUtils.isNotEmpty(item.getForModelNumbers())) {
        	product.setForModelNumbers(item.getForModelNumbers());
        }
        if(item.getBrand() != null && item.getBrand().getBrandImage() != null && !item.getBrand().getBrandImage().isEmpty() && StringUtils.isNotBlank(item.getBrand().getBrandImage().get(1).getImageURL())) {        	
        	product.setBrandImage(IMAGE_HTTP_PREFIX+item.getBrand().getBrandImage().get(1).getImageURL());
        }
        
        if(item.getAttribute() != null && !item.getAttribute().isEmpty() ) {
        	Map<String, String> specifications = new LinkedHashMap<>();
        	for(AttributeType attributeType : item.getAttribute()) {
        		specifications.put(attributeType.getAttributeName(), attributeType.getAttributeValue());
        	}
        	
        	if(item.getCartonWeight() != null) {
        		specifications.put("Carton Weight", item.getCartonWeight().getValue() +" "+ item.getCartonWeight().getUnitCode());
        	}
        	
        	if(item.getWeight() != null) {
        		specifications.put("Item Weight", item.getWeight().getValue() +" "+ item.getWeight().getUnitCode());
        	}        	
        	if(item.getCartonPackQuantity()  != null) {
        		specifications.put("Carton Quantity", String.valueOf(item.getCartonPackQuantity()));
        	}        	
        	if(StringUtils.isNotEmpty(item.getCountryOfOrigin())) {
        		specifications.put("Country of Origin", item.getCountryOfOrigin());
        	}
        	product.setSpecifications(specifications);
        }

        return product;
    }

    @Override
    public boolean isSearchByCartridgeSupported() {
        return true;
    }

    @Override
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public boolean isSearchEngineAvailable() {
        try {
            GetSearchResponse response = this.buildHeartbeatSmartSearchQuery();
            return response.getResponseHeader().getResultStatus().getStatusCode().equals("200");
        }
        catch (Exception e) {
            return false;
        }
    }

    @Override
    public boolean usesActiveFilters() {
        return true;
    }

    @Override
    public SearchType getSearchType() {
        return SearchType.SMART_SEARCH;
    }

    @Override
    public boolean isSearchByModelSupported() {
        return true;
    }

    @Override
    public String searchByModelNumber(String modelNumber) {
        return SearchUtilities.SEARCH_PAGE_BASE_URL + "?searchString=" + modelNumber;
    }

    @Override
    public String getCartridgeLookahead() {
        return linksBean.getSmartSearchCartridgeLookahead();
    }

    @Override
    public String getModelLookahead() {
        return linksBean.getSmartSearchModelLookahead();
    }
}
