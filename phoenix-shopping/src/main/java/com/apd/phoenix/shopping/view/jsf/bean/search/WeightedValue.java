package com.apd.phoenix.shopping.view.jsf.bean.search;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class WeightedValue {

    public WeightedValue() {
        number = BigDecimal.ZERO;
        value = BigDecimal.ZERO;
    }

    public BigDecimal number;
    public BigDecimal value;

    BigDecimal getAverage() {
        if (BigDecimal.ZERO.equals(number)) {
            return null;
        }
        return value.divide(number, RoundingMode.HALF_UP);
    }

}
