/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.shopping.view.jsf.bean.search;

public class ProductView {

    private ViewOption selectedViewBy = ViewOption.ListView;

    public ProductView(ViewOption selectedViewBy) {
        this.selectedViewBy = selectedViewBy;
    }

    public enum ViewOption {

        ListView("List"), TableView("Table");

        private final String label;

        private ViewOption(String label) {
            this.label = label;
        }

        public String getLabel() {
            return label;
        }
    }

    public ViewOption getSelectedViewBy() {
        return selectedViewBy;
    }

    public void setSelectedViewBy(ViewOption selectedViewBy) {
        this.selectedViewBy = selectedViewBy;
    }

    public String getName() {
        return selectedViewBy.name();
    }

    public String getSelectedViewByString() {
        return selectedViewBy.name();
    }

    public void setSelectedViewByString(String selectedViewByString) {
        for (ViewOption option : ViewOption.values()) {
            if (option.name().equals(selectedViewByString)) {
                setSelectedViewBy(option);
                break;
            }
        }
    }
}
