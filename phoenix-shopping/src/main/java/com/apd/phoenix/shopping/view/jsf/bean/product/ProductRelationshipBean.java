package com.apd.phoenix.shopping.view.jsf.bean.product;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.ejb.LocalBean;
import javax.ejb.Stateful;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.apache.commons.lang.StringUtils;
import com.apd.phoenix.core.PageNumberDisplay;
import com.apd.phoenix.core.PageNumberDisplay.PageSize;
import com.apd.phoenix.service.product.Product;
import com.apd.phoenix.shopping.view.jsf.bean.search.SearchEngine;
import com.apd.phoenix.shopping.view.jsf.bean.search.Searcher;

@Named
@Stateful
@LocalBean
@SessionScoped
public class ProductRelationshipBean implements Serializable, Searcher {

    private static final String UPSELL_RELATIONSHIP_TYPE = "UPSELL";

    private static final PageSize RELATED_ITEM_QUANTITY = PageSize.FIVE;

    private static final long serialVersionUID = 1L;

    private Map<String, List<Product>> similarItemMap;

    private Map<String, PageNumberDisplay> paginationMap;

    private List<Product> substituteProductList;

    private static final String[] ORDERED_RELATIONSHIP_LIST = { "SUPPLIES", "ACCESSORIES", "COMPANION ITEMS", "FAMILY" };

    @Inject
    private SearchEngine searchEngine;

    public void retrieveSimilarItems(Product product) {
        this.similarItemMap = this.searchEngine.getRelatedItems(product);
        if (this.similarItemMap != null) {
            for (String key : this.similarItemMap.keySet()) {
                if (similarItemMap.get(key) == null || similarItemMap.get(key).isEmpty()) {
                    similarItemMap.remove(key);
                }
            }
        }
        paginationMap = new HashMap<>();
        for (String key : similarItemMap.keySet()) {
        	PageNumberDisplay pageNumber = new PageNumberDisplay(similarItemMap.get(key).size());
        	pageNumber.setPageSize(RELATED_ITEM_QUANTITY);
        	paginationMap.put(key, pageNumber);
        }
    }

    public Map<String, List<Product>> getSimilarItemMap() {
        return similarItemMap;
    }

    public void setSimilarItemMap(Map<String, List<Product>> similarItemList) {
        this.similarItemMap = similarItemList;
    }

    public Map<String, PageNumberDisplay> getPaginationMap() {
        return paginationMap;
    }

    public void setPaginationMap(Map<String, PageNumberDisplay> paginationMap) {
        this.paginationMap = paginationMap;
    }

    public List<Product> getSubstituteProductList() {
        return substituteProductList;
    }

    public void setSubstituteProductList(List<Product> substituteProductList) {
        this.substituteProductList = substituteProductList;
    }

    public List<String> getOrderedRelationshipTabList() {
    	List<String> toReturn = new ArrayList<>();
    	if (similarItemMap != null) {
    		for (String relationshipType : similarItemMap.keySet()) {
    			//the "UPSELL" relationship is not displayed in the tabs, 
    			//since it's displayed in the right column
    			if (StringUtils.isNotBlank(relationshipType) && !UPSELL_RELATIONSHIP_TYPE.equals(relationshipType)) {
    				toReturn.add(relationshipType);
    			}
    		}
	    	Collections.sort(toReturn, new RelationshipNameComparator());
    	}
    	return toReturn;
    }

    private class RelationshipNameComparator implements Comparator<String> {

        @Override
        public int compare(String o1, String o2) {
            int position1 = ORDERED_RELATIONSHIP_LIST.length;
            int position2 = ORDERED_RELATIONSHIP_LIST.length;
            for (int i = 0; i < ORDERED_RELATIONSHIP_LIST.length; i++) {
                if (ORDERED_RELATIONSHIP_LIST[i].equalsIgnoreCase(o1)) {
                    position1 = i;
                }
                if (ORDERED_RELATIONSHIP_LIST[i].equalsIgnoreCase(o2)) {
                    position2 = i;
                }
            }
            return position1 - position2;
        }
    }

}
