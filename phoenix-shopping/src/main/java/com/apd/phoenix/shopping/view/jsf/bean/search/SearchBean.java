/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.shopping.view.jsf.bean.search;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.annotation.PostConstruct;
import javax.ejb.LocalBean;
import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Stateful;
import javax.enterprise.context.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.model.SelectItem;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.core.PageNumberDisplay;
import com.apd.phoenix.core.PageNumberDisplay.PageSize;
import com.apd.phoenix.service.business.CatalogXItemBp;
import com.apd.phoenix.service.business.EntityComparator;
import com.apd.phoenix.service.model.Catalog.SearchType;
import com.apd.phoenix.service.model.CatalogXItem;
import com.apd.phoenix.service.model.Manufacturer;
import com.apd.phoenix.service.product.CatalogData;
import com.apd.phoenix.service.product.Product;
import com.apd.phoenix.shopping.view.jsf.bean.login.CredentialSelectionBean;
import com.apd.phoenix.shopping.view.jsf.bean.product.Breadcrumb;
import com.apd.phoenix.shopping.view.jsf.bean.search.ProductSort.SortOption;
import com.apd.phoenix.shopping.view.jsf.bean.search.ProductView.ViewOption;
import com.apd.phoenix.shopping.view.jsf.bean.utils.AttributeFilterType;
import com.apd.phoenix.shopping.view.jsf.bean.utils.CatalogDataBean;
import com.apd.phoenix.shopping.view.jsf.bean.utils.FilterKey;
import com.apd.phoenix.shopping.view.jsf.bean.utils.LinksBean;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;

/**
 *
 * @author dcnorris
 */
@Named
@Stateful
@LocalBean
@SessionScoped
@Lock(LockType.READ)
public class SearchBean implements Searcher {

    private static final String GLOBAL_PRODUCT_TYPE_FILTER_NAME = "Global Product Type";
    private static final int MAX_EXPANDED_CATEGORIES_TO_DISPLAY = 50;
    public static final String DEFAULT_CATEGORY = "Office Supplies";
    private static final int INITIAL_NUMBER_OF_AVALIABLE_FILTERS_TO_DISPLAY = 5;
    private static final Logger logger = LoggerFactory.getLogger(SearchBean.class);
    private List<Product> products;
    private ResponseData response;
    private int numFound;
    private List<String> categories;
    private Map<FilterKey, Integer> manufacturerWidgetData;
    private Multimap<AttributeFilterType, FilterKey> facetMap;
    private String requestParameterString;
    private CategoryMenu categoryMenu;
    private Breadcrumb breadcrumb;
    private SubCategoryMenu subCategoryMenu;
    private QueryData queryData;
    private QueryData previousQueryData;
    private PageNumberDisplay pageDisplay;
    private int displayCount = 0;
    private PageSize pageSize = PageSize.TEN;
    private int pageSizeIndex;
    private int previousPage = 0;
    private boolean resetSort;

    private String inkTonerManufacturer;
    private ProductSort productSort;
    private ProductView productView;
    private boolean showtableview = false;

    private List<Manufacturer> manufacturers;
    @Inject
    private SearchEngine searchEngine;
    @Inject
    private LinksBean linksBean;
    @Inject
    private CatalogXItemBp catalogXItemBp;
    @Inject
    private CatalogDataBean catalogDataBean;
    @Inject
    private CredentialSelectionBean credentialSelectionBean;

    private List<Product> featuredProducts;
    private Multimap<String, FilterKey> appliedFilters = HashMultimap.create();
    private List<FilterKey> manuallyAppliedFilters;
    private String autoCorrectMessage;
    private String didYouMeanMessage;
    private String crossReference;
    private Map<FilterKey, Integer> categoryWidgetData;
    private String categoryId;
    private List<String> lastResultsUrl;
    private Integer lastResultsPage;
    private AvaliableFilterCounter attributeFilterCounter = new AvaliableFilterCounter() {

        @Override
        public Integer getCount(FilterKey key) {
            try {
                return Integer.valueOf(key.getFilter().substring(StringUtils.lastIndexOf(key.getFilter(), "(") + 1,
                        StringUtils.lastIndexOf(key.getFilter(), ")")));
            }
            catch (NumberFormatException e) {
                logger.error("could not find number of elements in filter" + key.getFilter());
                return -1;
            }
        }
    };

    protected static String REFINE_YOUR_SEARCH_STRING = "Enter Search Term";
    private static String SMART_SEARCH_KEYWORD = "Keyword";
    private static final String HOMEPAGE = "/shopping/ecommerce/home.xhtml";

    public enum SmartSearchTopLevelCategory {
        OFFICE_SUPPLY("Office Supplies"), TECHNOLOGY("Technology"), MAINTENANCE_BREAKROOM("Maintenance & Breakroom"), FURNITURE(
                "Furniture"), ;

        private String topLevelcategory;

        private SmartSearchTopLevelCategory(String strCateogry) {
            topLevelcategory = strCateogry;
        }

        public String getTopLevelCategory() {
            return topLevelcategory;
        }
    }

    @PostConstruct
    @Lock(LockType.WRITE)
    public void init() {
        breadcrumb = new Breadcrumb();
        categories = new ArrayList<>();
        queryData = new QueryData();
        productSort = new ProductSort();
        categories.add("All");
        initCatalogData();
        resetSort = true;
     
    }

    @Lock(LockType.WRITE)
    private void initCatalogData() {
        initProductView();
        CatalogData data = this.catalogDataBean.getCatalogData();
        manufacturers = new ArrayList<Manufacturer>();
        manufacturers.addAll(data.getMatchbookManufacturers());
        Collections.sort(manufacturers, new EntityComparator());

        categoryMenu = (CategoryMenu) data.getCategoryData();

        initSearchCombobox();
    }

    @Lock(LockType.WRITE)
    private void initSearchCombobox() {
        for (SearchHierarchy categoryName : categoryMenu.getTopLevelCategoriesList()) {
            categories.add(categoryName.getDisplayName());
        }
    }

    @Lock(LockType.WRITE)
    public void newProductSearch() {
        queryData.setNewSearch(Boolean.TRUE);
        productSearchInit();
    }

    @Lock(LockType.WRITE)
    public void productSearchInit() {
        try {
            if (StringUtils.isEmpty(queryData.getActiveFiltersString())) {
                //sync activeFilters set with activeFilersString
                queryData.setActiveFilters(new HashSet<String>());
            }
            requestParameterString = SearchUtilities.generateRequestParameterString(queryData);
            FacesContext.getCurrentInstance().getExternalContext().redirect(requestParameterString);
        }
        catch (Exception ex) {
            logger.warn("IOException in productSearchInit()");
        }
    }

    @Lock(LockType.WRITE)
    public void productRefineSearchInit() {
        resetSort = false;
        List<String> refineSearchString = queryData.getRefineSearchStrings();
        queryData = QueryData.clone(previousQueryData);
        if (refineSearchString != null) {
            if (queryData.getRefineSearchStrings() == null) {
                queryData.setRefineSearchStrings(new ArrayList<String>());
            }
            for (String s : refineSearchString) {
                queryData.setNewRefineSearchString(s);
            }
        }
        productSearchInit();
    }

    //TODO add user specific filtering
    @Lock(LockType.WRITE)
    public void productSearch() {
        FacesContext context = FacesContext.getCurrentInstance();
        ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
        if (context.isPostback()) {
            return; // Skip postback requests.
        }
        if (allRequestParametersEmpty()) {
            try {
                externalContext.redirect(getFirstLevelHierarchyBrowseUrl());
                return;
            }
            catch (Exception ex) {
                logger.warn("Error Redirecting to first level hierarchy on Product Browse Page.");
            }
        }
        queryData.setMatchbookSearch(this.isInkTonerSearch());
        //Only carry over filters if it's not a new search
        if (!Boolean.TRUE.equals(queryData.getNewSearch())) {
            queryData.setAppliedFilters(this.appliedFilters);
        }
        else {
            queryData.setNewSearch(Boolean.FALSE);
        }
        if (!queryData.isShowNonCore()) {
            queryData.setContractFilterId(previousQueryData.getContractFilterId());
        }
        breadcrumb = new Breadcrumb();
        subCategoryMenu = new SubCategoryMenu();
        productSearchCore();
        HttpServletRequest request = (HttpServletRequest) externalContext.getRequest();
        if (this.products != null && this.products.size() == 1 && this.pageDisplay != null
                && this.pageDisplay.getPage() == 1 && !"true".equals(request.getParameter("redirect"))) {
            //if there is only one product, on the first page of results,
            //and that product is valid, redirects to that product's details page
            Product onlyProduct = this.products.get(0);
            if (this.products.get(0).getId() != null
                    && catalogXItemBp.findById(onlyProduct.getId(), CatalogXItem.class) != null
                    && StringUtils.isNotBlank(onlyProduct.getDetailsLink())) {
                try {
                    FacesContext.getCurrentInstance().getExternalContext().redirect(onlyProduct.getDetailsLink());
                }
                catch (Exception e) {
                    logger.warn("Unable to redirect to single product");
                }
            }
        }
    }

    private void productSearchCore() {
        addAllParams();
        querySearchEngine();
        handlResults();
    }

    @Lock(LockType.WRITE)
    private void addAllParams() {
        initializeSearch();
        queryData.setRows(pageSize.toInt());
        addSortOptions();

        addFilterFieldsToQuery();
        if (!StringUtils.isEmpty(queryData.getFilterToRemove())) {
            Iterator<FilterKey> iterator = appliedFilters.get(SearchUtilities.ATTRIBUTE_MAP_KEY).iterator();
            while (iterator.hasNext()) {
                if (iterator.next().getId().trim().equals(queryData.getFilterToRemove().trim())) {
                    iterator.remove();
                    break;
                }
            }
            Iterator<String> activeFilterIterator = queryData.getActiveFilters().iterator();
            while (activeFilterIterator.hasNext()) {
                if (activeFilterIterator.next().contains(queryData.getFilterToRemove())) {
                    activeFilterIterator.remove();
                    break;
                }
            }
            queryData.setFilterToRemove(null);
        }
        if (!FacesContext.getCurrentInstance().isPostback()) {
            Integer pageSizeParameter = this.getPageUrlParameter();
            if (pageSizeParameter != null) {
                queryData.setStart((pageSizeParameter - 1) * pageSize.toInt());
            }
        }
        return;
    }

    private boolean allRequestParametersEmpty() {
        //"newSearch" and "showNonCore" are not very restrictive parameters, so if they are the only parameters
        //that are applied, then the parameters are considered empty
        for (String key : FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().keySet()) {
            if (!SearchUtilities.NEW_SEARCH_PARAMETER.equals(key)
                    && !SearchUtilities.SHOW_NON_CORE_PARAMETER.equals(key)) {
                return false;
            }
        }
        return true;
    }

    @Lock(LockType.WRITE)
    private void querySearchEngine() {
        logger.info(queryData.toString());
        response = searchEngine.query(queryData);
        previousQueryData = queryData;
    }

    @Lock(LockType.WRITE)
    private void handlResults() {
        try {
            //If autocorrected, update query data with correct message
            if (StringUtils.isNotEmpty(response.getAutoCorrectMessage())) {
                autoCorrectMessage = response.getAutoCorrectMessage();
                previousQueryData.setSearchString(response.getAutoCorrectValue());
                queryData.setSearchString(response.getAutoCorrectValue());
            }
            else {
                autoCorrectMessage = null;
            }

            if (response.getAppliedFilters() != null && response.getAppliedFilters().size() == 1
                    && response.getAppliedFilters().containsKey(SMART_SEARCH_KEYWORD)) {
                for (FilterKey key : response.getAppliedFilters().get(SMART_SEARCH_KEYWORD)) {
                    queryData.setSearchString(key.getId());
                }
            }
            if (response.getBreadcrumbs() != null && !response.getBreadcrumbs().isEmpty()
                    && !response.getBreadcrumbs().get(0).getSearchEngineKey().equals(ALL_HIERARCHY_PATH)) {
                //intialize hierarchy view only if the user browsed from the left menu, not when searching
                initSubMenuWidgets(response);
            }

            if (queryData != null && queryData.getSearchString() != null) {
                String heirarchyMatch = checkForHierarchyMatches(response.getSynonyms());
                if (!(queryData.getSearchEngineHierarchyKey() != null && queryData.getSearchEngineHierarchyKey()
                        .equals(queryData.getSearchString()))
                        && null != heirarchyMatch) {
                    searchByHierarchyOnly(heirarchyMatch);
                    return;
                }
                FilterKey globalProductTypeMatch = checkForGlobalProductTypeMatch(response.getSynonyms());
                if (globalProductTypeMatch != null) {
                    searchByGlobalProductType(globalProductTypeMatch);
                    return;
                }
            }
            if (response.isClearSearchString()) {
                queryData.setSearchString(null);
                previousQueryData.setSearchString(null);
                queryData.setSearchEngineHierarchyKey(breadcrumb.getTail().getSearchEngineKey());
                previousQueryData.setSearchEngineHierarchyKey(breadcrumb.getTail().getSearchEngineKey());
            }

            if (StringUtils.isNotEmpty(response.getDidYouMeanMessage())) {
                didYouMeanMessage = response.getDidYouMeanMessage();
            }
            else {
                didYouMeanMessage = null;
            }

            if (StringUtils.isNotEmpty(response.getCrossReference())) {
                crossReference = response.getCrossReference();
            }
            else {
                crossReference = null;
            }
            products = response.getProducts();
            featuredProducts = response.getFeaturedProducts();
            appliedFilters = response.getAppliedFilters();
            numFound = response.getNumFound();
            displayCount = products.size();
            Integer page = getPageUrlParameter();
            pageDisplay = new PageNumberDisplay(numFound);
            pageDisplay.setPageSize(pageSize);
            if (page != null) {
                pageDisplay.setPage(page);
            }
            previousPage = pageDisplay.getPage();
            logger.info("Results Found: " + numFound);
            parseFacets();
            if (!response.getSortOptions().contains(productSort.getSelectedSortBy())) {
                productSort.setSelectedSortBy(SortOption.BestMatch);
            }

            queryData.setContractFilterId(response.getContractFilterId());
        }
        catch (Exception ex) {
            logger.error("Error handling search results", ex);
        }
        clearParameters();
    }

    private void searchByGlobalProductType(FilterKey globalProductTypeMatch) {
        queryData.setSearchString("");
        queryData.setActiveFilters(new HashSet<String>());
        String globalProductTypeFilter = GLOBAL_PRODUCT_TYPE_FILTER_NAME + "| "
                + extractFilterNameFromLabel(globalProductTypeMatch.getFilter()).trim() + "| "
                + globalProductTypeMatch.getId();
        queryData.getActiveFilters().add(globalProductTypeFilter.trim());
        setActiveFiltersString(this.queryData);
        productSearch();
    }

    private FilterKey checkForGlobalProductTypeMatch(List<String> synonyms) {
        FilterKey filterKey = null;
        filterKey = checkForGlobalProductTypeMatch(queryData.getSearchString());
        for (String synonym : synonyms) {
            if (filterKey != null) {
                break;
            }
            filterKey = checkForGlobalProductTypeMatch(synonym);
        }
        return filterKey;
    }

    private FilterKey checkForGlobalProductTypeMatch(String searchString) {
        if (StringUtils.isBlank(searchString)) {
            return null;
        }
        String compareString = searchString.replaceAll("[^A-Za-z0-9]", "");
        if (StringUtils.isBlank(compareString)) {
            return null;
        }
        Collection<FilterKey> avaliableGlobalProdcutTypes = response.getFilters().get(
                new AttributeFilterType(GLOBAL_PRODUCT_TYPE_FILTER_NAME, null));
        for (FilterKey key : avaliableGlobalProdcutTypes) {
            if (key != null && StringUtils.isNotBlank(key.getFilter())) {
                String compareGlobalProductType = key.getFilter();
                compareGlobalProductType = extractFilterNameFromLabel(compareGlobalProductType);
                compareGlobalProductType = compareGlobalProductType.replaceAll("[^A-Za-z0-9]", "");
                if (compareGlobalProductType.equalsIgnoreCase(compareString)) {
                    return key;
                }
            }
        }
        return null;
    }

    private String extractFilterNameFromLabel(String compareGlobalProductType) {
        //Only get the part before the parentheses indicating the number of objects found
        int lastIndexOfOpenParen = compareGlobalProductType.lastIndexOf("(");
        if (lastIndexOfOpenParen != -1) {
            compareGlobalProductType = compareGlobalProductType.substring(0, lastIndexOfOpenParen);
        }
        return compareGlobalProductType;
    }

    private String checkForHierarchyMatches(List<String> synonymList) {
        String returnString = null;
        returnString = checkForHierarchyMatches(queryData.getSearchString());
        for (String synonym : synonymList) {
            if (!StringUtils.isEmpty(returnString)) {
                break;
            }
            returnString = checkForHierarchyMatches(synonym);
        }
        return returnString;
    }

    public String removeSearchStringFilter() {
        QueryData changedData = QueryData.clone(previousQueryData);
        changedData.setSearchString(null);
        return SearchUtilities.generateRequestParameterString(changedData);
    }

    public String removeCategoryFilter() {
        QueryData changedData = QueryData.clone(previousQueryData);
        //If there were not 2 levels to the hierarchy, set the hierarchy key to null
        if (breadcrumb.getCrumbs().size() < 1) {
            changedData.setSearchEngineHierarchyKey(null);
        }
        else {
            //Otherwise go up one hierarchy level
            changedData.setSearchEngineHierarchyKey(breadcrumb.getCrumbs().get(breadcrumb.getCrumbs().size() - 1)
                    .getSearchEngineKey());
        }
        return SearchUtilities.generateRequestParameterString(changedData);
    }

    public String findCategory() {
        for (FilterKey key : categoryWidgetData.keySet()) {
            if (key.getId().equals(previousQueryData.getSearchEngineHierarchyKey())) {
                return key.getFilter();
            }
        }
        logger.error("Could not find category name for id " + previousQueryData.getSearchEngineHierarchyKey());
        return null;
    }

    public String removeRefineSearchStringFilter(String toRemove) {
        QueryData data = QueryData.clone(previousQueryData);
        if (this.appliedFilters.containsKey(SMART_SEARCH_KEYWORD)) {
            FilterKey filterToRemove = null;
            for (FilterKey key : this.appliedFilters.get(SMART_SEARCH_KEYWORD)) {
                if (toRemove.equals(key.getFilter()) || toRemove.equals(key.getId())) {
                    filterToRemove = key;
                }
            }
            if (filterToRemove != null) {
                this.appliedFilters.get(SMART_SEARCH_KEYWORD).remove(filterToRemove);
            }
        }
        if (data.getRefineSearchStrings() != null && data.getRefineSearchStrings().contains(toRemove)) {
            data.getRefineSearchStrings().remove(toRemove);
        }
        setActiveFiltersString(data);
        return SearchUtilities.generateRequestParameterString(data);
    }

    public String removeSelectedManufacturer() {
        QueryData changedData = QueryData.clone(previousQueryData);
        changedData.setSelectedManufacturer(null);
        return SearchUtilities.generateRequestParameterString(changedData);
    }

    public String removePriceFilter() {
        QueryData data = QueryData.clone(previousQueryData);
        data.setHighPrice(-1);
        data.setLowPrice(-1);
        return SearchUtilities.generateRequestParameterString(data);
    }

    public String getPriceFilterLabel() {
        //TODO: refactor this, per PHOEN-5374
        if (previousQueryData.getLowPrice() == 0.00f) {
            return "Under $25";
        }
        else if (previousQueryData.getLowPrice() == 25.00f) {
            return "$25 to $50";
        }
        else if (previousQueryData.getLowPrice() == 50.00f) {
            return "$50 to $100";
        }
        else if (previousQueryData.getLowPrice() == 100.00f) {
            return "$100 to $200";
        }
        else if (previousQueryData.getLowPrice() == 200.00f) {
            return "Over $200";
        }
        return "Price Filter";
    }

    public List<String> getFilterStrings(String key) {
        List<String> returnList = new ArrayList<String>();
        Multimap<String, FilterKey> filterMap = appliedFilters;
        Collection<FilterKey> filterKeys = filterMap.get(key);
        if (filterKeys != null) {
            for (FilterKey filterKey : filterKeys) {
                returnList.add(filterKey.getFilter());
            }
        }
        return returnList;
    }

    private boolean filterKeyValid(String key) {
        if (key == null) {
            return false;
        }
        //Don't include a filter for suppliesFinderBrand, since we only search with both brand and model
        switch (key) {
            case "Keyword":
            case "SuppliesFinderBrand":
                return false;
        }
        return true;
    }

    public String returnToInkToner() {
        return linksBean.getInkTonerSearch();
    }

    public Collection<String> getActiveFilterMapKeys() {
        ArrayList<String> returnList = new ArrayList<String>();
        Set<String> keySet = this.appliedFilters.asMap().keySet();
        for (String key : keySet) {
            if (filterKeyValid(key)) {
                returnList.add(key);
            }
        }
        return returnList;
    }

    @Lock(LockType.WRITE)
    private void searchByHierarchyOnly(String heirarchyKey) {
        queryData.setSearchEngineHierarchyKey(heirarchyKey);
        queryData.setSearchString("");
        productSearch();

    }

    @Lock(LockType.WRITE)
    private void initializeSearch() {

        //using search bar, and resetting sort
        if (StringUtils.isNotBlank(queryData.getSearchString()) && resetSort) {
            //Set to best match by default when using search bar
            productSort.setSelectedSortBy(SortOption.BestMatch);

        }
        resetSort = true;

    }

    @Lock(LockType.WRITE)
    private void addSortOptions() {
        queryData.setSortOption(productSort.getSelectedSortBy());
    }

    @Lock(LockType.WRITE)
    public void clearAllFilters() {
        queryData.setActiveFilters(new HashSet<String>());
        queryData.setActiveFiltersString("");
        queryData.setFiltersString("");
        previousQueryData.setActiveFilters(new HashSet<String>());
        previousQueryData.setActiveFiltersString("");
        previousQueryData.setFiltersString("");
        productSearchRefresh();
    }

    @Lock(LockType.WRITE)
    private void addFilterFieldsToQuery() {
        Multimap<String, FilterKey> filters = HashMultimap.create();
        if (facetMap != null) {
            for (AttributeFilterType filter : facetMap.keySet()) {
                filters.putAll(filter.getName(), facetMap.get(filter));
            }
        }
        queryData.setFilters(filters);
    }

    @Lock(LockType.WRITE)
    public void productSearchRefresh() {
        queryData = previousQueryData;
        if (allRequestParametersEmpty()) {
            try {
                FacesContext.getCurrentInstance().getExternalContext().redirect(getFirstLevelHierarchyBrowseUrl());
                return;
            }
            catch (Exception ex) {
                logger.warn("Error Redirecting to first level hierarchy on Product Browse Page.");
            }
        }
        productSearchCore();
    }

    @Lock(LockType.WRITE)
    private void clearParameters() {
        previousQueryData = queryData;
        queryData = new QueryData();
    }

    public List<String> getCategories() {
        if (categories.size() == 1) {
            initSearchCombobox();
        }
        return categories;
    }

    @Lock(LockType.WRITE)
    private void initSubMenuWidgets(ResponseData response) {
        breadcrumb = new Breadcrumb(response.getBreadcrumbs());
        subCategoryMenu = new SubCategoryMenu(response, categoryMenu.getCategoryData(), breadcrumb);
    }

    @Lock(LockType.WRITE)
    private void parseFacets() {
        resetFacetData();
        facetMap = response.getFilters();
        manufacturerWidgetData = response.getManufacturers();
        categoryWidgetData = response.getAvaliableCategoryFilters();
    }

    public boolean isCategoryNarrowEnoughForAdditionalFilters() {
        if (getCategoryWidgetKeySet() == null) {
            return true;
        }
        else {
            return getCategoryWidgetKeySet().size() <= 1;
        }

    }

    private String checkForHierarchyMatches(String searchString) {
        if (StringUtils.isBlank(searchString)) {
            return null;
        }
        String compareString = searchString.replaceAll("[^A-Za-z0-9]", "");
        if (StringUtils.isBlank(compareString)) {
            return null;
        }
        for (SearchHierarchy hierarchy : response.getMatchingHierarchies()) {
            if (hierarchy != null && StringUtils.isNotBlank(hierarchy.getDisplayName())) {
                String compareHierarchyDisplay = hierarchy.getDisplayName().replaceAll("[^A-Za-z0-9]", "");
                if (compareHierarchyDisplay.equalsIgnoreCase(compareString)) {
                    return hierarchy.getSearchEngineKey();
                }
            }
        }
        return null;

    }

    @Lock(LockType.WRITE)
    private void resetFacetData() {
        manufacturerWidgetData = new HashMap<>();
        facetMap = HashMultimap.create();
    }

    public List<AttributeFilterType> getFilterCategoryNames() {
        if (facetMap == null) {
            return null;
        }
        List<AttributeFilterType> categoryNameList = new ArrayList<>();
        for(AttributeFilterType name:facetMap.keySet()){
        	if(!isHandledSeparately(name) && !isAlreadyApplied(name.getName())){
        		categoryNameList.add(name);
        	}
        }
        Collections.sort(categoryNameList, new Comparator<AttributeFilterType>(){

        	//Sort by priority (highest to lowest) or by sequence lowest to highest
			@Override
			public int compare(AttributeFilterType arg0, AttributeFilterType arg1) {
				if(arg0.getPriority() == null || arg1.getPriority() == null){
					if(arg0.getPriority() == null && arg1.getPriority() != null){
						return 1;
        			}
					if(arg1.getPriority() == null && arg0.getPriority() != null){
						return -1;
					}
					if(arg0.getSequence() != null){
						return arg0.getSequence().compareTo(arg1.getSequence());
					}
					if(arg1.getSequence() != null){
						return -arg1.getSequence().compareTo(arg0.getSequence());
					}
					return 0;
				}
				else{
					return arg1.getPriority().compareTo(arg0.getPriority());
				}
			}
        });
        return categoryNameList;

    }

    private boolean isAlreadyApplied(String name) {
        for (String filter : findActiveFilters()) {
            if (parseFilterAttributeTypeName(filter).equals(name)) {
                return true;
            }
        }
        return false;
    }

    private boolean isHandledSeparately(AttributeFilterType name) {
        if (GLOBAL_PRODUCT_TYPE_FILTER_NAME.equals(name.getName()) || "Green".equals(name.getName())) {
            return true;
        }
        return false;
    }

    public List<FilterKey> getGlobalTypeList() {
        List<FilterKey> returnList = getFilterAttributeList(GLOBAL_PRODUCT_TYPE_FILTER_NAME);
        sortListOfAvaliableFilters(returnList, attributeFilterCounter);
        return returnList;
    }

    public boolean shouldRenderGlobalTypeList() {
        return isCategoryNarrowEnoughForAdditionalFilters()
                && getFilterAttributeList(GLOBAL_PRODUCT_TYPE_FILTER_NAME) != null
                && !getFilterAttributeList(GLOBAL_PRODUCT_TYPE_FILTER_NAME).isEmpty()
                && !isAlreadyApplied(GLOBAL_PRODUCT_TYPE_FILTER_NAME);
    }

    public List<FilterKey> getFilterAttributeList(String categoryName) {
        List<FilterKey> filterCategoryChildren = new ArrayList<>();       
        filterCategoryChildren.addAll(facetMap.get(new AttributeFilterType(categoryName, null)));
        return sortListOfAvaliableFilters(filterCategoryChildren, attributeFilterCounter);
    }

    public boolean isFilterActive(String categoryName, String filterCategoryChild) {
        int n = filterCategoryChild.lastIndexOf(" (");
        filterCategoryChild = filterCategoryChild.substring(0, n);
        String filterString = QueryData.getFilterString(categoryName, filterCategoryChild);
        queryData.setActiveFilters(getLastActiveFilter());
        if (queryData.getActiveFilters() != null && queryData.getActiveFilters().contains(filterString)) {
            return true;
        }
        return false;
    }

    private Set<String> getLastActiveFilter() {
        Set<String> filters = new HashSet<>();
        if (previousQueryData.getActiveFilters() != null) {
            filters = previousQueryData.getActiveFilters();
        }
        return filters;
    }

    public Map<FilterKey, Integer> getManufacturerWidgetData() {
        if (manufacturerWidgetData == null) {
            return null;
        }
        return manufacturerWidgetData;
    }

    public String findSelectedManufacturerName() {
        String manufacturer = this.getPreviousQueryData().getSelectedManufacturer();
        Collection<FilterKey> manufacturerWidgetKeySet = appliedFilters.get("Brand");
        if (manufacturerWidgetKeySet != null) {
            for (FilterKey filterKey : manufacturerWidgetKeySet) {
                if (manufacturer.equals(filterKey.getId())) {
                    return filterKey.getFilter();
                }
            }
        }
        return manufacturer;
    }

    public List<FilterKey> getManufacturerWidgetKeySet() {
        if (manufacturerWidgetData == null) {
            return null;
        }
        List<FilterKey> keyList = new ArrayList<>(manufacturerWidgetData.keySet());
        return sortListOfAvaliableFilters(keyList, new AvaliableFilterCounter(){
			@Override
			public Integer getCount(FilterKey key) {
				return getManufacturerCountByName(key);
			}
        	
        });
    }

    public List<FilterKey> getCategoryWidgetKeySet() {
        if (categoryWidgetData == null) {
            return null;
        }
        List<FilterKey> keyList = new ArrayList<>(categoryWidgetData.keySet());
        Iterator<FilterKey> keyListIterator = keyList.iterator();
        while(keyListIterator.hasNext()){
        	FilterKey key = keyListIterator.next();
        	if(key.getFilter() != null && key.getId().equals(previousQueryData.getSearchEngineHierarchyKey())){
        		keyListIterator.remove();
        	}
        }
        List<FilterKey> toReturn = sortListOfAvaliableFilters(keyList, new AvaliableFilterCounter(){

			@Override
			public Integer getCount(FilterKey key) {
				return getCategoryCountByName(key);
			}
        	
        });
        
        if (toReturn != null && toReturn.size() > MAX_EXPANDED_CATEGORIES_TO_DISPLAY) {
        	return toReturn.subList(0, MAX_EXPANDED_CATEGORIES_TO_DISPLAY);
        } else {
        	return toReturn;
        }
    }

    private interface AvaliableFilterCounter {

        public Integer getCount(FilterKey key);
    }

    private List<FilterKey> sortListOfAvaliableFilters(List<FilterKey> keyList, final AvaliableFilterCounter counter) {
        Comparator<FilterKey> highestToLowestCountComparator = new Comparator<FilterKey>() {

            @Override
            public int compare(FilterKey o1, FilterKey o2) {
                return counter.getCount(o2).compareTo(counter.getCount(o1));
            }

        };
        FilterKey[] keyArray = new FilterKey[keyList.size()];
        keyArray = keyList.toArray(keyArray);
        Arrays.sort(keyArray, highestToLowestCountComparator);
        Comparator<FilterKey> alphabeticalComparitor = new Comparator<FilterKey>() {

            @Override
            public int compare(FilterKey o1, FilterKey o2) {
                return o1.getFilter().compareTo(o2.getFilter());
            }

        };
        if (keyArray.length <= INITIAL_NUMBER_OF_AVALIABLE_FILTERS_TO_DISPLAY) {
            Arrays.sort(keyArray, alphabeticalComparitor);
        }
        else {
            Arrays.sort(keyArray, 0, INITIAL_NUMBER_OF_AVALIABLE_FILTERS_TO_DISPLAY, alphabeticalComparitor);
            Arrays.sort(keyArray, INITIAL_NUMBER_OF_AVALIABLE_FILTERS_TO_DISPLAY, keyArray.length,
                    alphabeticalComparitor);
        }
        return Arrays.asList(keyArray);
    }

    public Integer getManufacturerCountByName(FilterKey brand) {
        if (manufacturerWidgetData == null) {
            return 0;
        }
        return manufacturerWidgetData.get(brand);
    }

    public Integer getCategoryCountByName(FilterKey category) {
        if (categoryWidgetData == null) {
            return 0;
        }
        return categoryWidgetData.get(category);
    }

    public boolean renderManufacturerWidget() {
        if (manufacturerWidgetData != null && getManufacturerWidgetKeySet() != null
                && getManufacturerWidgetKeySet().size() > 0
                && (previousQueryData == null || StringUtils.isBlank(previousQueryData.getSelectedManufacturer()))) {
            return true;
        }
        return false;
    }

    @Lock(LockType.WRITE)
    public void applyFilter() {
        resetSort = false;
        queryData.setActiveFilters(getLastActiveFilter());
        if (queryData.getActiveFilters().contains(queryData.getSelectedFilterName())) {
            //deactivate
            deactivateFilter();
        }
        else {
            //activate
            queryData.getActiveFilters().add(queryData.getSelectedFilterName());
            setActiveFiltersString(this.queryData);
        }
        String activeFilterString = queryData.getActiveFiltersString();
        Set<String> activeFilters = queryData.getActiveFilters();
        queryData = previousQueryData;
        queryData.setActiveFilters(activeFilters);
        queryData.setActiveFiltersString(activeFilterString);
        productSearchInit();
    }

    @Lock(LockType.WRITE)
    public void deactivateFilter() {
        queryData.getActiveFilters().remove(queryData.getSelectedFilterName());
        setActiveFiltersString(this.queryData);
    }

    private void setActiveFiltersString(QueryData queryData) {
        queryData.setActiveFiltersString(StringUtils.join(queryData.getActiveFilters(), DELIMITER));
    }

    public String removeSelectedFilter(String filter) {
        QueryData data = QueryData.clone(previousQueryData);
        data.getActiveFilters().remove(filter);
        setActiveFiltersString(data);
        data.setFilterToRemove(parseFilterId(filter));
        return SearchUtilities.generateRequestParameterString(data);
    }

    public List<String> findActiveFilters() {
        List<String> filters = new ArrayList<String>();
        filters.addAll(previousQueryData.getActiveFilters());
        return filters;
    }

    // the filter is in the form attributeTypeName|id|selectedAttributeName
    // Sometimes there will be no id and selectedAttributeName will be in the id slot
    public String parseFilterName(String filterName) {
        String filterNameParsed;
        if (filterName.contains("|")) {
            String[] filterParts = filterName.split("\\|");
            if (filterParts.length == 2) {
                filterNameParsed = filterParts[1].trim();
            }
            else {
                filterNameParsed = filterParts[2].trim();
            }
        }
        else {
            filterNameParsed = filterName;
        }
        // filter names will include the number of results in parens at the end of them
        if (filterNameParsed.contains("(")) {
            return filterNameParsed.substring(0, filterNameParsed.indexOf("("));
        }
        else {
            return filterNameParsed;
        }
    }

    public String parseFilterAttributeTypeName(String filterName) {
        if (filterName.contains("|")) {
            String[] filterParts = filterName.split("\\|");
            return filterParts[0];
        }
        else {
            return filterName;
        }
    }

    public String parseFilterId(String filterString) {
        if (filterString.contains("|")) {
            String[] filterParts = filterString.split("\\|");
            return filterParts[1];
        }
        else {
            return filterString;
        }
    }

    @SuppressWarnings("static-method")
    public boolean isCategoryActive() {
        return true;
    }

    public boolean renderCategoryFilters() {
        if (categoryWidgetData != null && categoryWidgetData.keySet() != null && getCategoryWidgetKeySet().size() > 0) {
            return true;
        }
        return false;
    }

    public String filterOnManufacturer(String mname) {
        QueryData data = QueryData.clone(previousQueryData);
        data.setSelectedManufacturer(mname);
        return SearchUtilities.generateRequestParameterString(data);
    }

    public String filterOnGreen(FilterKey key) {
        QueryData data = QueryData.clone(previousQueryData);
        data.setGreenFilterId(key.getId());
        return SearchUtilities.generateRequestParameterString(data);
    }

    public String removeGreenFilter() {
        QueryData data = QueryData.clone(previousQueryData);
        data.setGreenFilterId(null);
        return SearchUtilities.generateRequestParameterString(data);
    }

    public String findGreenFilterLabel() {
        Collection<FilterKey> appliedIndicatorFilters = this.appliedFilters.get("ItemIndicator");
        for (FilterKey key : appliedIndicatorFilters) {
            if (key.getId().equals(previousQueryData.getGreenFilterId())) {
                return greenFilterDisplay(key, false);
            }
        }
        if ("Y".equals(previousQueryData.getGreenFilterId())) {
            return "Green";
        }
        return null;
    }

    public String greenFilterDisplay(FilterKey key, boolean showNumber) {
        BigInteger count = response.getGreenFilter().get(key);
        String label;
        if ("Y".equals(key.getFilter())) {
            label = "Green";
        }
        else {
            label = "Not Green";
        }
        if (count == null || !showNumber) {
            return label;
        }
        else {
            return label + " (" + count + ")";
        }
    }

    public String filterOnCategory(String categoryName, String categoryId) {
        QueryData data = QueryData.clone(previousQueryData);
        data.setSearchEngineHierarchyKey(categoryId);
        return SearchUtilities.generateRequestParameterString(data);
    }

    public int getNumFound() {
        return numFound;
    }

    public void setRequestParameterString(String requestParameterString) {
        this.requestParameterString = requestParameterString;
    }

    public String getRequestParameterString() {
        return requestParameterString;
    }

    @Lock(LockType.WRITE)
    public List<Product> getProducts() {
        if (pageDisplay != null && pageDisplay.getPage() != previousPage) {
            previousPage = pageDisplay.getPage();
            previousQueryData.setStart((pageDisplay.getPage() - 1) * pageDisplay.getSize());
            ResponseData updatedResponse = searchEngine.query(previousQueryData);
            products = updatedResponse.getProducts();
            featuredProducts = updatedResponse.getFeaturedProducts();
            numFound = updatedResponse.getNumFound();
            displayCount = products.size();
        }
        return products;
    }

    public CategoryMenu getCategoryMenu() {
        return categoryMenu;
    }

    public Breadcrumb getBreadcrumb() {
        return breadcrumb;
    }

    public SubCategoryMenu getSubCategoryMenu() {
        return subCategoryMenu;
    }

    public String getPriceUrl(float lowPrice, float highPrice) {
        QueryData data = QueryData.clone(previousQueryData);
        data.setLowPrice(lowPrice);
        if (highPrice == 0) {
            highPrice = Float.MAX_VALUE;
        }
        data.setHighPrice(highPrice);
        return SearchUtilities.generateRequestParameterString(data);
    }

    public boolean priceFilterIsApplied(float min, float max) {
        if (Float.compare(previousQueryData.getLowPrice(), min) == 0
                && Float.compare(previousQueryData.getHighPrice(), max) == 0) {
            return true;
        }
        else
            return false;
    }

    public QueryData getQueryData() {
        return queryData;
    }

    public void setQueryData(QueryData queryData) {
        this.queryData = queryData;
    }

    /**
     * Returns the object used to store the page display information for the
     * page navigation.
     *
     * @return The page display information.
     */
    public PageNumberDisplay getPageDisplay() {
        if (this.pageDisplay == null) {
            //this should never be called, pageDisplay is always populated
            this.pageDisplay = new PageNumberDisplay(1);
        }
        return pageDisplay;
    }

    public SelectItem[] getProductPageSizes() {
    	List<SelectItem> itemList = new ArrayList<>();
    	for (PageSize p : PageSize.values()) {
    		//HUNDRED and SEVENTY_FIVE are removed, per PHOEN-5355. 
    		//Could be resolved by using List instead of Detail result styling, but
    		//that results in no featured products being listed sometimes.
    		if (!p.equals(PageSize.HUNDRED) && !p.equals(PageSize.SEVENTY_FIVE)) {
    			itemList.add(new SelectItem(p, p.getLabel()));
    		}
    	}
        return itemList.toArray(new SelectItem[itemList.size()]);
    }

    public int getDisplayCount() {
        return displayCount;
    }

    public boolean isIsSearchByString() {
        return StringUtils.isNotEmpty(previousQueryData.getSearchString());
    }

    public QueryData getPreviousQueryData() {
        return previousQueryData;
    }

    public PageSize getPageSize() {
        return pageSize;
    }

    public void setPageSize(PageSize pageSize) {
        this.pageSize = pageSize;
    }

    public void refreshPage() {
        resetSort = false;
        queryData = QueryData.clone(previousQueryData);
        queryData.setStart(0);
        productSearchInit();
    }

    public int getPageSizeIndex() {
        return pageSizeIndex;
    }

    public void setPageSizeIndex(int pageSizeIndex) {
        this.pageSizeIndex = pageSizeIndex;
        int i = 0;
        for (PageSize p : PageSize.values()) {
            if (i == pageSizeIndex) {
                setPageSize(p);
                break;
            }
            i++;
        }
    }

    public ProductSort getProductSort() {
        return productSort;
    }

    public ProductView getProductView() {
        return productView;
    }

    public List<Manufacturer> getManufacturers() {
        return manufacturers;
    }

    public void setManufacturers(List<Manufacturer> manufacturers) {
        this.manufacturers = manufacturers;
    }

    private String getFirstLevelHierarchyBrowseUrl() {
        return HOMEPAGE;
    }

    public boolean isSelectedManufacturer(String manufacturer) {
        if (previousQueryData.getSelectedManufacturer() != null && manufacturer != null) {
            return previousQueryData.getSelectedManufacturer().equals(manufacturer);
        }
        else {
            return false;
        }
    }

    public boolean isSelectedCategory(String category) {
        return false;
    }

    public boolean shouldRenderPriceFilters() {
        return previousQueryData != null && previousQueryData.getLowPrice() == -1
                && previousQueryData.getHighPrice() == -1 && response != null && response.isShowPriceFilter();
    }

    public boolean shouldRenderCoreFilter() {
        return response != null && response.isShowCoreItemFilter() && previousQueryData.isShowNonCore();
    }

    public boolean shouldRenderAnyFilters() {
        return response != null && response.isShowAnyFilter();
    }

    public String getDefaultRefineSearchString() {
        return REFINE_YOUR_SEARCH_STRING;
    }

    public boolean isInkTonerSearch() {
        return (queryData != null && StringUtils.isNotBlank(queryData.getMatchbookId()))
                || (previousQueryData != null && StringUtils.isNotBlank(previousQueryData.getMatchbookId()));
    }

    public boolean isInkTonerRendered() {
        return !(manufacturers == null || manufacturers.isEmpty());
    }

    public boolean isResetSort() {
        return resetSort;
    }

    public void setResetSort(boolean resetSort) {
        this.resetSort = resetSort;
    }

    public String getPageNumber() {
        return Integer.toString(this.pageDisplay.getPage());
    }

    public Converter getInkTonerManufacturerConverter() {
        return new Converter() {

            @Override
            public Object getAsObject(FacesContext context, UIComponent component, String value) {
                if (manufacturers == null) {
                    return null;
                }
                for (Manufacturer m : manufacturers) {
                    if (m.getName().equals(value)) {
                        return m;
                    }
                }
                return null;
            }

            @Override
            public String getAsString(FacesContext context, UIComponent component, Object value) {
                if (value == null) {
                    return null;
                }
                return ((Manufacturer) value).getName();
            }
        };
    }

    public String getInkTonerManufacturer() {
        return inkTonerManufacturer;
    }

    public void setInkTonerManufacturer(String inkTonerManufacturer) {
        this.inkTonerManufacturer = inkTonerManufacturer;
    }

    public String processCategoryNameForLabel(String name) {
        if (StringUtils.isNotBlank(name)) {
            name = name.replaceAll("[^A-Za-z0-9]", "");
        }
        return name;
    }

    public String getSearchTermsJson() {
        return searchEngine.getSearchTermsJson();
    }

    public String getCartridgeSearchURL() {
        return "'" + searchEngine.getCartridgeLookahead() + "'";
    }

    public List<Product> getFeaturedProducts() {
        return featuredProducts;
    }

    public void setFeaturedProducts(List<Product> featuredProducts) {
        this.featuredProducts = featuredProducts;
    }

    public boolean isShowingNonCore() {
        if (previousQueryData != null) {
            return previousQueryData.isShowNonCore();
        }
        else {
            return true;
        }
    }

    public String showNonCore() {
        return this.getCoreItemsUrl(true);
    }

    public String showOnlyCore() {
        return this.getCoreItemsUrl(false);
    }

    private String getCoreItemsUrl(boolean showNonCore) {
        QueryData data = QueryData.clone(previousQueryData);
        data.setShowNonCore(showNonCore);
        return SearchUtilities.generateRequestParameterString(data);
    }

    public String getAutoCorrectMessage() {
        return autoCorrectMessage;
    }

    public void setAutoCorrectMessage(String autoCorrectMessage) {
        this.autoCorrectMessage = autoCorrectMessage;
    }

    class ValueComparator<K, V> implements Comparator<K> {

        Map<K, V> map;

        public ValueComparator(Map<K, V> map) {
            this.map = map;
        }

        @SuppressWarnings("unchecked")
        public int compare(K keyA, K keyB) {
            V valueA = map.get(keyA);
            V valueB = map.get(keyB);
            try {
                return ((Comparable<V>) valueB).compareTo(valueA);
            }
            catch (ClassCastException e) {
                return valueB.hashCode() - valueA.hashCode();
            }
        }
    }

    public SelectItem[] getSortByOptions() {
        SelectItem[] items = new SelectItem[response.getSortOptions().size()];
        int i = 0;
        for (SortOption option : response.getSortOptions()) {
            items[i++] = new SelectItem(option, option.getLabel());
        }
        return items;
    }

    public void initProductView() {
        //default view type if property not found
        productView = new ProductView(ViewOption.ListView);
        this.showtableview = false;

        String showTableViewProp = credentialSelectionBean.getPropertyMap().get("show table view");

        if (StringUtils.isNotEmpty(showTableViewProp)
                && (showTableViewProp.equalsIgnoreCase("YES") || showTableViewProp.equalsIgnoreCase("TRUE"))) {
            this.showtableview = true;
            String defaultviewtype = credentialSelectionBean.getPropertyMap().get("defaultresultsview");
            if (StringUtils.isNotEmpty(defaultviewtype)
                    && defaultviewtype.equalsIgnoreCase(ViewOption.TableView.getLabel())) {
                productView = new ProductView(ViewOption.TableView);
            }
        }
    }

    public String[] getViewedByOptions() {
        ViewOption[] options = ViewOption.values();
        String[] items = new String[options.length];

        for (int i = 0; i < options.length; i++) {
            items[i] = options[i].name();
        }
        return items;
    }

    public String findSessionId() {
        ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
        Object session = context.getSession(true);
        if (session instanceof HttpSession) {
            HttpSession httpSession = (HttpSession) session;
            return httpSession.getId();
        }
        return session.toString();
    }

    public String getDidYouMeanMessage() {
        return didYouMeanMessage;
    }

    public void setDidYouMeanMessage(String didYouMeanMessage) {
        this.didYouMeanMessage = didYouMeanMessage;
    }

    public String getDidYouMeanURL() {
        QueryData data = QueryData.clone(previousQueryData);
        if (previousQueryData.getRefineSearchStrings() == null || previousQueryData.getRefineSearchStrings().isEmpty()) {
            data.setSearchString(this.getDidYouMeanMessage());
        }
        else {
            data.getRefineSearchStrings().remove(data.getLastSearchString());
            data.setNewRefineSearchString(didYouMeanMessage);
            data.setNewSearch(true);
        }
        return SearchUtilities.generateRequestParameterString(data);
    }

    public String getCrossReference() {
        return crossReference;
    }

    public void setCrossReference(String crossReference) {
        this.crossReference = crossReference;
    }

    public boolean usesActiveFilters() {
        return searchEngine.usesActiveFilters();
    }

    public Map<FilterKey, Integer> getCategoryWidgetData() {
        return categoryWidgetData;
    }

    public void setCategoryWidgetData(Map<FilterKey, Integer> categoryWidgetData) {
        this.categoryWidgetData = categoryWidgetData;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getReturnUrl() {
        if (lastResultsUrl != null && lastResultsUrl.size() > 1) {
            return lastResultsUrl.get(lastResultsUrl.size() - 2);
        }
        return "";
    }

    public Integer getLastResultsPage() {
        return lastResultsPage;
    }

    public void setLastResultsPage(Integer lastResultsPage) {
        this.lastResultsPage = lastResultsPage;
    }

    public void regenerateLastResultsUrl() {
        addLastResultsUrl(false);
    }

    public void addNewLastResultsUrl() {
        addLastResultsUrl(true);
    }

    public void addNewLastResultsUrlIf(boolean shouldAddUrl) {
        if (shouldAddUrl) {
            addLastResultsUrl(true);
        }
    }

    private void addLastResultsUrl(boolean additionalUrl) {
        if (!FacesContext.getCurrentInstance().isPostback()) {
        	if (!additionalUrl || this.lastResultsUrl == null) {
        		this.lastResultsUrl = new ArrayList<>();
        	}
        	HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext()
                    .getRequest();
        	if (!additionalUrl || !"true".equals(request.getParameter("redirect"))) {
	        	this.lastResultsUrl.add(request.getRequestURL().toString() + "?"
	                    + StringUtils.defaultIfBlank(request.getQueryString(), "") + "&redirect=true");
        	} else if (!this.lastResultsUrl.isEmpty()) {
        		this.lastResultsUrl.remove(this.lastResultsUrl.size() - 1);
        	}
        }
        if (this.pageDisplay != null) {
            this.setLastResultsPage(this.getPageDisplay().getPage());
        }
    }

    public List<FilterKey> getGreenFilter() {
        if (response == null || response.getGreenFilter() == null) {
            return null;
        }
        Set<FilterKey> keySet = response.getGreenFilter().keySet();
        Iterator<FilterKey> keyIterator = keySet.iterator();
        while (keyIterator.hasNext()) {
            FilterKey key = keyIterator.next();
            if ("unknown".equals(key.getFilter())) {
                keyIterator.remove();
            }
        }
        return new ArrayList<FilterKey>(keySet);
    }

    public List<FilterKey> getManuallyAppliedFilters() {
        return manuallyAppliedFilters;
    }

    public void setManuallyAppliedFilters(List<FilterKey> manuallyAppliedFilters) {
        this.manuallyAppliedFilters = manuallyAppliedFilters;
    }

    public String findFacetListClass(String index) {
        if (new Integer(index) < 5) {
            return "firstFiveFacetLists";
        }
        else {
            return "moreFacetLists";
        }
    }

    public boolean displayBrowseByCategory() {
        if (searchEngine.getSearchType().equals(SearchType.SMART_SEARCH)) {
            return false;
        }
        return true;
    }

    public String getModelSearchURL() {
        return "'" + searchEngine.getModelLookahead() + "'";
    }

    public boolean isShowtableview() {
        return showtableview;
    }

    public void setShowtableview(boolean showtableview) {
        this.showtableview = showtableview;
    }

    private Integer getPageUrlParameter() {
        try {
            String pageString = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get(
                    "page");
            if (StringUtils.isNotBlank(pageString)) {
                return Integer.parseInt(pageString);
            }
        }
        catch (Exception e) {
            //do nothing
        }
        return null;
    }
}
