package com.apd.phoenix.shopping.oci.request.processors;

import com.apd.phoenix.shopping.oci.OCIRequestWrapper;

public abstract class OCIRequestProcessor<T extends OCIRequestWrapper> {

    public abstract void processRequest(T request) throws OciProcessorException;

}
