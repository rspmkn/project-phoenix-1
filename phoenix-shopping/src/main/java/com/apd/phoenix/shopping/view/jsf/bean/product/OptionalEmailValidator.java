/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.shopping.view.jsf.bean.product;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import org.apache.commons.lang.StringUtils;
import com.apd.phoenix.shopping.view.jsf.bean.PageCssBean;
import com.apd.phoenix.shopping.view.jsf.bean.login.LoginBean;

/**
 *
 * @author zgutterm
 */
//TODO Refactor organization email validators into one class
@FacesValidator("optionalEmailValidator")
public class OptionalEmailValidator implements Validator {

    public static final String PLEASE_ENTER_A_VALID_EMAIL_ADDRES = "Please enter an email address valid to your organization.";

    @Override
    public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {
        //Cannot inject a bean in a JSF validator
        LoginBean loginBean = context.getApplication().evaluateExpressionGet(context, "#{loginBean}", LoginBean.class);
        PageCssBean pageCssBean = context.getApplication().evaluateExpressionGet(context, "#{pageCssBean}",
                PageCssBean.class);
        //If data has been entered, validate it
        if (StringUtils.isNotEmpty((String) value)) {
            if (!(value instanceof String)) {
                throw new ValidatorException(new FacesMessage(PLEASE_ENTER_A_VALID_EMAIL_ADDRES));
            }

            if (!loginBean.validEmailAddress((String) value, pageCssBean.getSubdomainValue())) {
                throw new ValidatorException(new FacesMessage(PLEASE_ENTER_A_VALID_EMAIL_ADDRES));
            }

        }
    }
}
