/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.shopping.view.jsf.bean.search;

import java.util.Collection;
import com.apd.phoenix.core.utility.EcommercePropertiesLoader;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.business.CatalogXItemBp;
import com.apd.phoenix.service.business.ItemBp;
import com.apd.phoenix.service.model.CatalogXItem;
import com.apd.phoenix.service.product.Product;
import com.apd.phoenix.shopping.view.jsf.bean.utils.FilterKey;

/**
 * Utilities for supporting the Search UI
 */
@Stateless
@LocalBean
public class SearchUtilities implements Searcher {

    public static final String NEW_SEARCH_PARAMETER = "newSearch";
    public static final String SHOW_NON_CORE_PARAMETER = "showNonCore";
    public static final String CATEGORY_MAP_KEY = "Category";
    public static final String ATTRIBUTE_MAP_KEY = "Attribute";
    private static final String CATEGORY_ID = "categoryId";
    private static final Logger logger = LoggerFactory.getLogger(SearchUtilities.class);
    public static final String coreItemIconUrl = EcommercePropertiesLoader.getInstance().getEcommerceProperties()
            .getString("coreItemIconUrl");
    public static final String INK_TONER_MODEL_CODE = "inkTonerModelCode";
    public static final String INK_TONER_MANUFACTURER_CODE = "inkTonerManufacturerCode";
    public static final String INK_TONER_CARTRIDGE_NUMBER = "inkTonerCartridgeNumber";
    @Inject
    private CatalogXItemBp catXItemBp;
    @Inject
    private ItemBp itemBp;

    public static String generateRequestParameterString(QueryData queryData) {
        StringBuilder urlStringBuilder = new StringBuilder(SEARCH_PAGE_BASE_URL + "?");
        StringBuilder requestParameterStringBuilder = new StringBuilder("");
        //the leading "&" will be deleted later
        if (StringUtils.isNotBlank(queryData.getSearchString())) {
            requestParameterStringBuilder.append("&");
            requestParameterStringBuilder.append("searchString=");
            requestParameterStringBuilder.append(queryData.getSearchStringEncoded().toString());
        }
        if (queryData.getRefineSearchStrings() != null && !queryData.getRefineSearchStrings().isEmpty()) {
            requestParameterStringBuilder.append("&");
            requestParameterStringBuilder.append("refineSearchString=");
            requestParameterStringBuilder.append(queryData.getRefineSearchStringsEncoded().toString());
        }
        if (StringUtils.isNotBlank(queryData.getSelectedManufacturer())) {
            requestParameterStringBuilder.append("&");
            requestParameterStringBuilder.append("selectedManufacturer=");
            requestParameterStringBuilder.append(queryData.getSelectedManufacturerEncoded().toString());
        }
        if (StringUtils.isNotBlank(queryData.getActiveFiltersString())) {
            requestParameterStringBuilder.append("&");
            requestParameterStringBuilder.append("filters=");
            requestParameterStringBuilder.append(queryData.getActiveFiltersStringEncoded().toString());
        }
        if (StringUtils.isNotBlank(queryData.getSearchEngineHierarchyKey())) {
            requestParameterStringBuilder.append("&");
            requestParameterStringBuilder.append(HIERARCHY_KEY_PARAMETER + "=");
            requestParameterStringBuilder.append(queryData.getHierarchyKeyEncoded().toString());
        }
        if (queryData.getLowPrice() != -1) {
            requestParameterStringBuilder.append("&");
            requestParameterStringBuilder.append("lowPrice" + "=");
            requestParameterStringBuilder.append(queryData.getLowPrice());
        }
        if (queryData.getHighPrice() != -1) {
            requestParameterStringBuilder.append("&");
            requestParameterStringBuilder.append("highPrice" + "=");
            requestParameterStringBuilder.append(queryData.getHighPrice());
        }
        if (StringUtils.isNotBlank(queryData.getFavoritesId())) {
            requestParameterStringBuilder.append("&");
            requestParameterStringBuilder.append("favoritesId" + "=");
            requestParameterStringBuilder.append(queryData.getFavoritesId());
        }
        if (StringUtils.isNotBlank(queryData.getFavoritesName())) {
            requestParameterStringBuilder.append("&");
            requestParameterStringBuilder.append("favoritesName" + "=");
            requestParameterStringBuilder.append(queryData.getFavoritesName());
        }
        if (StringUtils.isNotBlank(queryData.getFavoritesType())) {
            requestParameterStringBuilder.append("&");
            requestParameterStringBuilder.append("favoritesType" + "=");
            requestParameterStringBuilder.append(queryData.getFavoritesType());
        }
        if (StringUtils.isNotBlank(queryData.getMatchbookId())) {
            requestParameterStringBuilder.append("&");
            requestParameterStringBuilder.append(MB_ID_PARAM + "=");
            requestParameterStringBuilder.append(queryData.getMatchbookId());
        }
        if (StringUtils.isNotBlank(queryData.getInkTonerManufacturerCode())) {
            requestParameterStringBuilder.append("&");
            requestParameterStringBuilder.append(INK_TONER_MANUFACTURER_CODE + "=");
            requestParameterStringBuilder.append(queryData.getInkTonerManufacturerCode());
        }
        if (StringUtils.isNotBlank(queryData.getInkTonerModelCode())) {
            requestParameterStringBuilder.append("&");
            requestParameterStringBuilder.append(INK_TONER_MODEL_CODE + "=");
            requestParameterStringBuilder.append(queryData.getInkTonerModelCode());
        }
        if (StringUtils.isNotBlank(queryData.getInkTonerModel())) {
            requestParameterStringBuilder.append("&");
            requestParameterStringBuilder.append(INK_TONER_MODEL + "=");
            requestParameterStringBuilder.append(queryData.getInkTonerModel());
        }

        if (queryData.getAppliedFilters() != null) {
            if (queryData.getAppliedFilters().get(CATEGORY_MAP_KEY) != null
                    && !queryData.getAppliedFilters().get(CATEGORY_MAP_KEY).isEmpty()) {
                Collection<FilterKey> categoryCollection = queryData.getAppliedFilters().get(CATEGORY_MAP_KEY);
                if (categoryCollection.size() > 1) {
                    logger.error("Error too many categories selected.");
                }
                else {
                    FilterKey category = categoryCollection.iterator().next();
                    //                    queryData.setHierarchyPath(category.getFilter());
                    requestParameterStringBuilder.append("&");
                    requestParameterStringBuilder.append(CATEGORY_ID + "=");
                    requestParameterStringBuilder.append(category.getId());
                }
            }
        }
        if (queryData.getNewSearch() != null) {
            requestParameterStringBuilder.append("&");
            requestParameterStringBuilder.append(NEW_SEARCH_PARAMETER + "=");
            requestParameterStringBuilder.append(queryData.getNewSearch().toString());
        }
        if (!StringUtils.isEmpty(queryData.getGreenFilterId())) {
            requestParameterStringBuilder.append("&");
            requestParameterStringBuilder.append("green" + "=");
            requestParameterStringBuilder.append(queryData.getGreenFilterId());
        }
        if (!StringUtils.isEmpty(queryData.getFilterToRemove())) {
            requestParameterStringBuilder.append("&");
            requestParameterStringBuilder.append("filterToRemove" + "=");
            requestParameterStringBuilder.append(queryData.getFilterToRemove());
        }
        requestParameterStringBuilder.append("&");
        requestParameterStringBuilder.append(SHOW_NON_CORE_PARAMETER + "=");
        requestParameterStringBuilder.append(queryData.isShowNonCore());
        if (requestParameterStringBuilder.length() > 0) {
            //removes leading "&"
            requestParameterStringBuilder.deleteCharAt(0);
        }
        return urlStringBuilder.append(requestParameterStringBuilder).toString();
    }

    public CatalogXItem getFromProduct(Product searchProduct) {
        if (searchProduct == null) {
            return null;
        }
        CatalogXItem toReturn = catXItemBp.findById(searchProduct.getId(), CatalogXItem.class);
        if (toReturn == null
                && EcommercePropertiesLoader.getInstance().getEcommerceProperties().getBoolean("isDevelopmentMode")) {
            //if the id on the Product doesn't map to a CatalogXItem, gets ANY catalogXItem with this product
            //this block of code should never be reached in production
            logger.warn("ID on Product not found! See getFromProduct method in SearchUtilities.");
            CatalogXItem searchItem = new CatalogXItem();
            searchItem.setItem(itemBp.searchItem(searchProduct.getApdSku(), searchProduct.getVendorName()));
            toReturn = catXItemBp.searchByExactExample(searchItem, 0, 0).get(0);
        }
        return toReturn;
    }
}
