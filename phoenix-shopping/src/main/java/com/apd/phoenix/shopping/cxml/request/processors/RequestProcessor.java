/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.apd.phoenix.shopping.cxml.request.processors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author dnorris
 */
public interface RequestProcessor {

    public void processRequest(HttpServletRequest request, HttpServletResponse response, String cXml);

}
