package com.apd.phoenix.shopping.view.jsf.bean.marketplace;

import com.apd.phoenix.service.model.Credential;
import com.apd.phoenix.shopping.view.jsf.bean.login.CredentialSelectionBean;
import com.apd.phoenix.shopping.view.jsf.bean.login.LoginBean;
import java.io.IOException;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.ejb.Stateful;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author dnorris
 */
@Named
@Stateful
@RequestScoped
public class WelcomeBean implements Serializable {

    private static final long serialVersionUID = 8158291904461408623L;
    private static final Logger logger = LoggerFactory.getLogger(WelcomeBean.class);
    private static final String BILLING_ZIP_CODE_PARAM = "billingZipCode";

    @Inject
    private LoginBean loginBean;

    @Inject
    private CredentialSelectionBean credentialSelectionBean;

    private String zipCode;

    private boolean renderZipCodeForm;

    @PostConstruct
    public void init() {
        Credential credential = credentialSelectionBean.getCurrentCredential();
        renderZipCodeForm = credential.isRequireZipCodeInput();
    }

    public boolean renderZipCodeForm() {
        return renderZipCodeForm;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public void punchout() {
        logger.debug("Punching Out");
        ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
        try {
            if (StringUtils.isNotBlank(zipCode)) {
                String billingZipCodeRequestParam = "?" + BILLING_ZIP_CODE_PARAM + "=" + zipCode;
                context.redirect(context.getRequestContextPath() + "/ecommerce/marketplace/punchoutWindow.xhtml"
                        + billingZipCodeRequestParam);
            }
            else {
                context.redirect(context.getRequestContextPath() + "/ecommerce/marketplace/punchoutWindow.xhtml");
            }
        }
        catch (IOException ex) {
            logger.error("Error redirecting to resource.", ex);
            //if there is an error redirecting the user
            loginBean.logout();
        }
    }

}
