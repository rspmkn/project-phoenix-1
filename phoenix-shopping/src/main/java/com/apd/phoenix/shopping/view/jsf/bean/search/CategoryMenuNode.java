package com.apd.phoenix.shopping.view.jsf.bean.search;

import java.io.Serializable;

/**
 *
 * @author anicholson
 */

public class CategoryMenuNode implements Serializable {

    private static final long serialVersionUID = 1L;

    private int level;
    /**
     * The name of the node one level higher in the hierarchy which is the parent of this node. For level=0,
     * parent=null. For level=1, parent is the name of the node at level 1. This is because of the way the
     * levelData object that is put in the value set of categoryData is constructed in CategoryMenu; at level i=0,
     * levelData maps from levels[i] to levels[i], so the node is a parent of itself.
     */
    private SearchHierarchy parent;

    public CategoryMenuNode(int level, SearchHierarchy label) {
        this.level = level;
        this.parent = label;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public SearchHierarchy getParent() {
        return parent;
    }

    public void setParent(SearchHierarchy label) {
        this.parent = label;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CategoryMenuNode)) {
            return false;
        }
        CategoryMenuNode that = (CategoryMenuNode) o;
        return ((this.parent == null ? that.parent == null : this.parent.equals(that.parent)) && this.level == that.level);
    }

    @Override
    public int hashCode() {
        if (parent != null) {
            return parent.hashCode() + level;
        }
        else {
            return "".hashCode() + level;
        }
    }

}