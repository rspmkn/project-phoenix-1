/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.shopping.view.jsf.bean.search;

/**
 *
 * @author dcnorris
 */
public class ProductSort {

    private SortOption selectedSortBy = SortOption.BestMatch;

    public enum SortOption {

        BestMatch("Best Match"), SortByProductNameAsc("Sort By Product Name A-Z"), SortByProductNameDesc(
                "Sort By Product Name Z-A"), SortBySkuAsc("Sort By Sku A-Z"), SortBySkuDesc("Sort By Sku Z-A"), SortByPriceAsc(
                "Sort By Price Low-High"), SortByPriceDesc("Sort By Price High-Low"), SortByBrandAsc(
                "Sort By Brand Ascending"), SortByBrandDesc("Sort By Brand Descending"), MostPopular("Most Popular");

        private final String label;

        private SortOption(String label) {
            this.label = label;
        }

        public String getLabel() {
            return label;
        }
    }

    public SortOption getSelectedSortBy() {
        return selectedSortBy;
    }

    public void setSelectedSortBy(SortOption selectedSortBy) {
        this.selectedSortBy = selectedSortBy;
    }

    public String getSelectedSortByString() {
        return selectedSortBy.name();
    }

    public void setSelectedSortByString(String selectedSortByString) {
        for (SortOption option : SortOption.values()) {
            if (option.name().equals(selectedSortByString)) {
                setSelectedSortBy(option);
                break;
            }
        }
    }
}
