package com.apd.phoenix.shopping.view.jsf.bean.utils;

import java.math.BigDecimal;

public class FilterKey implements Comparable<FilterKey> {

    private String id;
    private String filter;
    private BigDecimal priority;

    public FilterKey() {

    }

    public FilterKey(String id, String filter) {
        this.id = id;
        this.filter = filter;
    }

    @Override
    public int compareTo(FilterKey other) {
        return this.filter.compareTo(other.filter);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFilter() {
        return filter;
    }

    public void setFilter(String filter) {
        this.filter = filter;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((filter == null) ? 0 : filter.hashCode());
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        FilterKey other = (FilterKey) obj;
        if (filter == null) {
            if (other.filter != null)
                return false;
        }
        else if (!filter.equals(other.filter))
            return false;
        if (id == null) {
            if (other.id != null)
                return false;
        }
        else if (!id.equals(other.id))
            return false;
        return true;
    }

    public BigDecimal getPriority() {
        return priority;
    }

    public void setPriority(BigDecimal priority) {
        this.priority = priority;
    }
}
