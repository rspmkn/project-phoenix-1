/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.shopping.view.jsf.bean.search;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Stateful;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;
import com.apd.phoenix.service.business.ItemClassificationTypeBp;
import com.apd.phoenix.service.model.ItemClassificationType;

/**
 *
 * @author RHC
 */
@Named
@Stateful
@LocalBean
@SessionScoped
@Lock(LockType.READ)
public class ItemClassificationTypeBean {

    @Inject
    private ItemClassificationTypeBp itemClassificationTypeBp;

    private List<ItemClassificationType> itemClassificationTypes;

    public List<ItemClassificationType> getItemClassificationTypes() {
    	if (itemClassificationTypes == null) {
    		itemClassificationTypes = itemClassificationTypeBp.findAll(ItemClassificationType.class, 0, 0);
    	}
    	//if the BP returns null, sets field to an empty list, so database isn't checked again
    	if (itemClassificationTypes == null) {
    		itemClassificationTypes = new ArrayList<>();
    	}
    	return itemClassificationTypes;
    }
}
