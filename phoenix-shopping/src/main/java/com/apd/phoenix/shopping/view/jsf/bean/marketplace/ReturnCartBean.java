/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.shopping.view.jsf.bean.marketplace;

import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import javax.ejb.Stateful;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.business.CustomerOrderBp;
import com.apd.phoenix.service.integration.cxml.model.cxml.CXML;
import com.apd.phoenix.service.model.LineItem;
import com.apd.phoenix.service.model.PunchoutSession;
import com.apd.phoenix.shopping.cxml.request.generators.PunchoutOrderMessageFactory;
import com.apd.phoenix.shopping.view.jsf.bean.login.CredentialSelectionBean;

/**
 *
 * @author RHC
 */
@Named
@Stateful
@RequestScoped
public class ReturnCartBean {

    private static final int DESCRIPTION_TRUNCATION_LENGTH = 40;

    private static final Logger logger = LoggerFactory.getLogger(ReturnCartBean.class);

    private String punchoutOrderMessage;

    private JAXBContext cxmlJaxbContext;

    @Inject
    private CredentialSelectionBean credentialSelectionBean;

    @Inject
    private PunchoutOrderMessageFactory punchoutOrderMessageFactory;

    @Inject
    private CustomerOrderBp customerOrderBp;

    private PunchoutSession punchoutSession;

    private String punchoutType;

    public void init() {
        punchoutSession = credentialSelectionBean.getPunchoutSession();
        punchoutSession.setCustomerOrder(customerOrderBp.hydrateForOrderDetails(punchoutSession.getCustomerOrder()));
        punchoutType = punchoutSession.getCredential().getPunchoutType().getLabel();
        if (punchoutType.equalsIgnoreCase("CXML")) {
            initPunchoutOrderMessage();
        }
        credentialSelectionBean.endCurrentPunchoutSession();
    }

    private void initPunchoutOrderMessage() {
        try {
            cxmlJaxbContext = JAXBContext.newInstance(CXML.class);
            CXML cxml = punchoutOrderMessageFactory.getOutgoingPunchoutOrderMessage();
            punchoutOrderMessage = base64Encode(marshallToString(cxmlJaxbContext, cxml));
        }
        catch (JAXBException ex) {
            logger.error("Could not marshall punchout order message.", ex);
        }
        catch (UnsupportedEncodingException ex) {
            logger.error("Could not urlEncode punchout order message.", ex);
        }
    }

    public String getPunchoutOrderMessage() {
        return punchoutOrderMessage;
    }

    private String marshallToString(JAXBContext pContext, Object pObject) throws JAXBException {
        StringWriter sw = new StringWriter();
        String CXML_HEADER = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
                + "<!DOCTYPE cXML SYSTEM \"http://xml.cxml.org/schemas/cXML/1.2.014/cXML.dtd\">";
        Marshaller marshaller = pContext.createMarshaller();
        marshaller.setProperty("com.sun.xml.bind.xmlDeclaration", Boolean.FALSE);
        sw.append(CXML_HEADER);
        marshaller.marshal(pObject, sw);
        logger.debug("OutboundPunchOutOrderMessage: " + sw.toString());
        return sw.toString();
    }

    private String base64Encode(String cXML) throws UnsupportedEncodingException {
        byte[] encoded = Base64.encodeBase64(cXML.getBytes("UTF-8"));
        cXML = new String(encoded, "UTF-8");
        return cXML;
    }

    public PunchoutSession getPunchoutSession() {
        return punchoutSession;
    }

    public String getPunchoutType() {
        return punchoutType;
    }

    public String truncateItemDescription(LineItem item) {
        if (item.getShortName() != null) {
            if (item.getShortName().length() > DESCRIPTION_TRUNCATION_LENGTH) {
                return item.getShortName().substring(0, DESCRIPTION_TRUNCATION_LENGTH);
            }
            else {
                return item.getShortName();
            }
        }
        return "";
    }
}
