package com.apd.phoenix.shopping.rest.client;

import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import com.apd.phoenix.shopping.rest.json.TypeaheadResponse;

@Path("/smartchoice/smart-suggestions.jsonp")
public interface SmartSearchTypeaheadInterface {

    @GET
    TypeaheadResponse getSmartSearchSuggestions(@QueryParam("remoteId") String selectedPath,
            @QueryParam("term") String term, @QueryParam("keywordInterface") String keywordInterface,
            @HeaderParam("Accept") String accept);
}