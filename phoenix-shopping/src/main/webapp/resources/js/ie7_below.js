// Move modals to end of DOM to prevent potential IE7 z-index issues
$('#createIssueLog.modal, #existingIssueLogs.modal, #issueLogComment.modal, #createIssueLogConfirm.modal, #passwordReset.modal').appendTo($('body'));
