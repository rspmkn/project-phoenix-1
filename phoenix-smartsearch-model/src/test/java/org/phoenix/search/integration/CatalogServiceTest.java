package org.phoenix.search.integration;

import java.math.BigInteger;
import org.apache.commons.lang.StringUtils;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import com.ussco.ws.ecatalog.catalog._1.ECatalogInterface;
import com.ussco.ws.ecatalog.catalog._1.ECatalogService;
import com.ussco.ws.ecatalog.catalog._1.FilterListType;
import com.ussco.ws.ecatalog.catalog._1.FilterType;
import com.ussco.ws.ecatalog.catalog._1.FilterValueType;
import com.ussco.ws.ecatalog.catalog._1.GetItemsRequest;
import com.ussco.ws.ecatalog.catalog._1.GetItemsResponse;
import com.ussco.ws.ecatalog.catalog._1.GetSearchRequest;
import com.ussco.ws.ecatalog.catalog._1.GetSearchResponse;
import com.ussco.ws.ecatalog.catalog._1.IdentificationType;
import com.ussco.ws.ecatalog.catalog._1.ItemCrossReferenceType;
import com.ussco.ws.ecatalog.catalog._1.ItemListType;
import com.ussco.ws.ecatalog.catalog._1.ItemResultStyle;
import com.ussco.ws.ecatalog.catalog._1.ItemType;
import com.ussco.ws.ecatalog.catalog._1.ItemsEntryType;
import com.ussco.ws.ecatalog.catalog._1.ItemsRequestType;
import com.ussco.ws.ecatalog.catalog._1.ListNameType;
import com.ussco.ws.ecatalog.catalog._1.MatchType;
import com.ussco.ws.ecatalog.catalog._1.RequestHeaderType;
import com.ussco.ws.ecatalog.catalog._1.SearchRequestType;
import com.ussco.ws.ecatalog.catalog._1.SortType;
import com.ussco.ws.ecatalog.catalog._1.StatusType;

public class CatalogServiceTest {

    private ECatalogService ss;
    private ECatalogInterface port;

    @Before
    public void setup() {
        ss = new ECatalogService();
        port = ss.getECatalogService("https://ppd2-ws.ussco.com/eCatalog/catalog/001/sync", "APD_PPD_01", "Duh6xuKe");
    }

    @Ignore
    @Test
    public void testCatalogService() {

        {
            System.out.println("Invoking getSearch...");
            com.ussco.ws.ecatalog.catalog._1.GetSearchRequest searchRequest = new GetSearchRequest();
            searchRequest.setRequestHeader(new RequestHeaderType());
            searchRequest.getRequestHeader().setIncExcList(new ItemListType());
            searchRequest.getRequestHeader().getIncExcList();
            com.ussco.ws.ecatalog.catalog._1.GetSearchResponse _getSearch__return = port.getSearch(searchRequest);
            System.out.println("getSearch.result=" + _getSearch__return);

        }
        {
            System.out.println("Invoking getProductCompare...");
            com.ussco.ws.ecatalog.catalog._1.GetProductCompareRequest _getProductCompare_productCompareRequest = null;
            com.ussco.ws.ecatalog.catalog._1.GetProductCompareResponse _getProductCompare__return = port
                    .getProductCompare(_getProductCompare_productCompareRequest);
            System.out.println("getProductCompare.result=" + _getProductCompare__return);

        }
        {
            System.out.println("Invoking getRelatedItems...");
            com.ussco.ws.ecatalog.catalog._1.GetRelatedItemRequest _getRelatedItems_relatedItemsRequest = null;
            com.ussco.ws.ecatalog.catalog._1.GetRelatedItemResponse _getRelatedItems__return = port
                    .getRelatedItems(_getRelatedItems_relatedItemsRequest);
            System.out.println("getRelatedItems.result=" + _getRelatedItems__return);

        }

    }

    @Test
    @Ignore
    public void testCatalogProductFromVendorNameAndApdSku() {
        System.out.println("Invoking testCatalogProductFromVendorNameAndApdSku...");
        GetSearchRequest getSearchRequest = new GetSearchRequest();
        getSearchRequest.setSearchRequest(new SearchRequestType());
        getSearchRequest.setRequestHeader(new RequestHeaderType());

        IdentificationType identificationType = new IdentificationType();
        identificationType.setSessionId("111");

        getSearchRequest.getRequestHeader().setIdentificationRequest(identificationType);
        getSearchRequest.getRequestHeader().setIncExcList(new ItemListType());

        ListNameType listNameType = new ListNameType();
        listNameType.setIncExcType("Inclusion");
        listNameType.setListName("Crystal SmartSearch Test 2");

        StatusType statusType = new StatusType();
        statusType.setStatusCode("");
        statusType.setStatusMessage("");
        listNameType.setResultStatus(statusType);

        ItemResultStyle itemResultStyle = new ItemResultStyle();
        itemResultStyle.setItemResultStyle("Detail");

        getSearchRequest.getRequestHeader().getIncExcList().getItemList().add(listNameType);
        getSearchRequest.getRequestHeader().setResultStyle(itemResultStyle);
        getSearchRequest.getRequestHeader().setCatNavDepth(0);

        FilterListType filterListType = new FilterListType();

        FilterType filterType = new FilterType();
        filterType.setFilterStyle("Keyword");
        filterType.setFilterDescription("Keyword");
        filterType.setCrossReference(MatchType.NONE);
        filterType.setSequence(BigInteger.valueOf(1));
        filterType.setKeywordInterface("Standard");

        filterListType.getFilter().add(filterType);
        FilterValueType filterValueType = new FilterValueType();
        filterValueType.setValue("FUR4501");
        filterType.getFilterValue().add(filterValueType);

        SortType sortType = new SortType();
        sortType.setSortCode("BM");

        getSearchRequest.getSearchRequest().setSearchFilters(filterListType);
        getSearchRequest.getSearchRequest().setSearchSort(sortType);
        getSearchRequest.getSearchRequest().setStartAtResult(BigInteger.valueOf(1));
        getSearchRequest.getSearchRequest().setBufferSize(BigInteger.valueOf(10));

        port.getSearch(getSearchRequest);

    }

    @Test
    @Ignore
    public void searchByList() {
        String sessionId = "111";
        String userAgent = "";
        String listName = "_ECATALOG";
        String sku = "pop";

        GetSearchRequest searchRequest = buildEmptyGetSearchObject(sessionId, userAgent);
        searchRequest.getRequestHeader().getIncExcList().getItemList().add(getIncExcList(listName, "Inclusion"));
        searchRequest.getRequestHeader().setResultStyle(getItemResultStyle("Detail"));
        searchRequest.getSearchRequest().getSearchFilters().getFilter().add(
                getFilterType(sku, "Keyword", BigInteger.valueOf(1)));
        searchRequest.getSearchRequest().getSearchFilters().getFilter().add(
                getFilterType("4294967167", "Category", BigInteger.valueOf(2)));
        //searchRequest.getSearchRequest().getSearchFilters().getFilter().add(
        //       getFilterType("4294027694", "Brand", BigInteger.valueOf(3)));
        searchRequest.getSearchRequest().setSearchSort(getSortType("BM"));
        searchRequest.getSearchRequest().setStartAtResult(BigInteger.valueOf(1));
        searchRequest.getSearchRequest().setBufferSize(BigInteger.valueOf(10));
        GetSearchResponse result = port.getSearch(searchRequest);

    }

    @Test
    @Ignore
    public void testGetItem() {
        String sessionId = "111";
        String userAgent = "";
        String listName = "Smart Search Test Catalog";
        String vendorSku = "UNV72230";

        GetItemsRequest itemsRequest = new GetItemsRequest();
        IdentificationType identificationType = new IdentificationType();
        identificationType.setSessionId(sessionId);

        itemsRequest.setRequestHeader(new RequestHeaderType());

        itemsRequest.getRequestHeader().setIdentificationRequest(identificationType);
        itemsRequest.getRequestHeader().setIncExcList(new ItemListType());

        ListNameType listNameType = new ListNameType();
        listNameType.setIncExcType("Inclusion");
        listNameType.setListName(listName);

        StatusType statusType = new StatusType();
        statusType.setStatusCode("");
        statusType.setStatusMessage("");
        listNameType.setResultStatus(statusType);

        itemsRequest.getRequestHeader().getIncExcList().getItemList().add(listNameType);

        ItemResultStyle itemResultStyle = new ItemResultStyle();
        itemResultStyle.setItemResultStyle("Detail");

        itemsRequest.setItemsRequest(new ItemsRequestType());
        ItemsEntryType itemEntry = new ItemsEntryType();
        itemEntry.setItemReference(new ItemCrossReferenceType());
        itemEntry.getItemReference().setReferenceType("");
        itemEntry.getItemReference().setSequence(BigInteger.ONE);
        ItemType itemType = new ItemType();
        itemType.setItemNumber(vendorSku);
        itemEntry.getItem().add(itemType);
        itemsRequest.getItemsRequest().getItemEntry().add(itemEntry);
        GetItemsResponse response = port.getItems(itemsRequest);
        ItemsEntryType itemsEntryType = response.getItemsResponse().getItemEntry().get(0);
        ItemType responseItemType = itemsEntryType.getItem().get(0);
        System.out.println(responseItemType.getDescription());

    }

    public static IdentificationType buildIdentificationType(String sessionId, String userAgent) {
        IdentificationType toReturn = new IdentificationType();
        toReturn.setUserAgent(userAgent);
        toReturn.setSessionId(sessionId);
        return toReturn;
    }

    public static ListNameType getIncExcList(String listName, String incExcType) {
        ListNameType listNameType = new ListNameType();
        listNameType.setIncExcType(incExcType);
        listNameType.setListName(listName);

        StatusType statusType = new StatusType();
        statusType.setStatusCode("");
        statusType.setStatusMessage("");
        listNameType.setResultStatus(statusType);

        return listNameType;
    }

    public static ItemResultStyle getItemResultStyle(String style) {
        ItemResultStyle itemResultStyle = new ItemResultStyle();
        itemResultStyle.setItemResultStyle(style);
        return itemResultStyle;
    }

    public static FilterType getFilterType(String sku, String filterStyle, BigInteger sequence) {

        FilterType filterType = new FilterType();
        filterType.setFilterStyle(filterStyle);
        filterType.setFilterDescription(filterStyle);
        filterType.setCrossReference(MatchType.NONE);
        filterType.setSequence(sequence);
        filterType.setKeywordInterface("Standard");

        FilterValueType filterValueType = new FilterValueType();
        filterValueType.setValue(sku);
        filterType.getFilterValue().add(filterValueType);

        return filterType;
    }

    public static SortType getSortType(String type) {
        SortType sortType = new SortType();
        sortType.setSortCode(type);
        return sortType;
    }

    private static GetSearchRequest buildEmptyGetSearchObject(String sessionId, String userAgent) {
        if (StringUtils.isEmpty(sessionId)) {
            System.out.println("Session id is null, will not be able to reach smartSearch");
        }
        GetSearchRequest searchRequest = new GetSearchRequest();
        searchRequest.setRequestHeader(new RequestHeaderType());
        searchRequest.getRequestHeader().setIncExcList(new ItemListType());
        searchRequest.setSearchRequest(new SearchRequestType());
        searchRequest.getSearchRequest().setSearchFilters(new FilterListType());
        searchRequest.getRequestHeader().setIdentificationRequest(buildIdentificationType(sessionId, userAgent));
        return searchRequest;
    }
}
