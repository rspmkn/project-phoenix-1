package org.phoenix.search.integration;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import com.ussco.ws.ecatalog.catalog._1.MDListType.ListItem;
import com.ussco.ws.ecatalog.catalog._1.*;

public class CatalogMDServiceTest {

    private ECatalogMDService ss;
    private ECatalogMDInterface port;

    @Before
    public void setup() {
        ss = new ECatalogMDService();
        port = ss.getECatalogMDService("https://ppd2-ws.ussco.com/eCatalog/masterData/001/sync", "APD_PPD_01",
                "Duh6xuKe");
    }

    @Test
    @Ignore
    public void testCatalogService() {
        System.out.println("Invoking Update List...");
        UpdateItemListRequest updateListRequest = new UpdateItemListRequest();
        updateListRequest.setItemListRequest(new MDListRequestType());

        MDListType list = new MDListType();

        list.setListName("Crystal SmartSearch Test 2");

        list.setUpdateStyle("Insert");
        ListItem listItem = new ListItem();
        listItem.setItemNumber("FUR4501");
        listItem.setPrice(2.8d);

        ListItem listItem1 = new ListItem();
        listItem1.setItemNumber("RID32910");
        listItem1.setPrice(4.37d);

        list.getListItem().add(listItem);
        list.getListItem().add(listItem1);
        updateListRequest.getItemListRequest().setList(list);
        System.out.println(updateListRequest.toString());

        UpdateItemListResponse updateListResponse = port.updateItemList(updateListRequest);
        System.out.println("getSearch.result=" + updateListResponse);
    }

    @Test
    @Ignore
    public void testGetListOfListNames() {
        GetItemListRequest getItemListRequest = new GetItemListRequest();
        getItemListRequest.setItemListRequest(new MDListRequestType());

        MDListType list = new MDListType();

        getItemListRequest.getItemListRequest().setList(list);
        GetItemListResponse response = port.getItemList(getItemListRequest);
        System.out.println("testGetListOfListNames = " + response);
    }

    @Test
    @Ignore
    public void testGetItemsOfList() {
        GetItemListRequest getItemListRequest = new GetItemListRequest();
        getItemListRequest.setItemListRequest(new MDListRequestType());

        MDListType list = new MDListType();
        list.setListName("Crystal SmartSearch Test 3");
        getItemListRequest.getItemListRequest().setList(list);
        GetItemListResponse response = port.getItemList(getItemListRequest);
    }

    @Test
    @Ignore
    public void testInsertCustomItem() {

        UpdateItemRequest updateItemRequest = new UpdateItemRequest();
        updateItemRequest.setItemRequest(new MDItemRequestType());
        MDCustomItemType mdCustomItemType = new MDCustomItemType();

        mdCustomItemType.setUpdateStyle("Insert");
        mdCustomItemType.setDealerItemNumber("ZZZ5100");
        mdCustomItemType.setDealerDescription("Copy Paper (Legal)");
        mdCustomItemType.setDealerUnitOfMeasure("CT");
        mdCustomItemType.setDealerKeywords("5100");
        mdCustomItemType.setStockedItem("Y");
        mdCustomItemType.setCategoryId("316");
        mdCustomItemType.setListPrice("19.95");
        mdCustomItemType.setSequence(1);
        ImageType imageType = new ImageType();
        imageType.setImageURL("www.customURL.com");
        imageType.setImageType("DealerA");
        imageType.setImageHeight(new BigInteger("100"));
        imageType.setImageWidth(new BigInteger("100"));
        imageType.setImageTitle("Copy Paper Item");
        mdCustomItemType.getItemImage().add(imageType);

        //getItemRequest.getItemRequest().getItem().add(mdCustomItemType);
        updateItemRequest.getItemRequest().getItem().add(mdCustomItemType);
        port.updateItem(updateItemRequest);

        UpdateItemListRequest updateListRequest = new UpdateItemListRequest();
        updateListRequest.setItemListRequest(new MDListRequestType());

        MDListType list = new MDListType();

        list.setListName("Crystals Test Catalog new");

        list.setUpdateStyle("Insert");
        ListItem listItem = new ListItem();
        listItem.setDealerItemNumber("ZZZ5100");
        list.getListItem().add(listItem);
        updateListRequest.getItemListRequest().setList(list);
        System.out.println(updateListRequest.toString());

        UpdateItemListResponse updateListResponse = port.updateItemList(updateListRequest);

    }

    @Test
    @Ignore
    public void testRemoveItemList() {
        RemoveItemListRequest removeItemListRequest = new RemoveItemListRequest();
        removeItemListRequest.setItemListRequest(new MDListRequestType());

        MDListType list = new MDListType();

        list.setListName("Crystals Test Catalog new");

        list.setUpdateStyle("Insert");
        ListItem listItem = new ListItem();
        listItem.setItemNumber("BRTMFC5895CW");
        listItem.setPrice(1210d);

        list.getListItem().add(listItem);

        removeItemListRequest.getItemListRequest().setList(list);
        RemoveItemListResponse removeItemListResponse = port.removeItemList(removeItemListRequest);

    }

    @Test
    @Ignore
    public void testRemoveItem() {
        RemoveItemRequest removeItemRequest = new RemoveItemRequest();
        removeItemRequest.setItemRequest(new MDItemRequestType());

        MDCustomItemType mdCustomItemType = new MDCustomItemType();

        mdCustomItemType.setDealerItemNumber("ZZZ5100");

        removeItemRequest.getItemRequest().getItem().add(mdCustomItemType);
        RemoveItemResponse removeItemResponse = port.removeItem(removeItemRequest);

    }

    @Test
    @Ignore
    public void testTopLevelCategory() {
        GetCategoryRequest getCategoryRequest = new GetCategoryRequest();
        getCategoryRequest.setCategoryRequest(new MDCategoryRequestType());
        port.getCategory(getCategoryRequest);
    }

    @Test
    @Ignore
    public void testGetAllCategory() {
        GetCategoryRequest getCategoryRequest = new GetCategoryRequest();
        getCategoryRequest.setCategoryRequest(new MDCategoryRequestType());
        getCategoryRequest.getCategoryRequest().setCategoryDepth(BigInteger.valueOf(3));
        port.getCategory(getCategoryRequest);
    }

}
