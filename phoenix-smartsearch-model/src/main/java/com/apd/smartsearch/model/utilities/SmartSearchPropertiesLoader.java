package com.apd.smartsearch.model.utilities;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

//Please use TenantConfigRepository for all references to properties files, this way we have a single point of contact with properties
@Deprecated
public class SmartSearchPropertiesLoader {

    private static Logger logger = LoggerFactory.getLogger(SmartSearchPropertiesLoader.class);
    private PropertiesConfiguration smartSearchProperties;
    private static final SmartSearchPropertiesLoader singleton = new SmartSearchPropertiesLoader();

    public static SmartSearchPropertiesLoader getInstance() {
        return singleton;
    }

    private SmartSearchPropertiesLoader() {
        String propertyHome = System.getProperty("phoenix.config.home");
        try (InputStream is = new FileInputStream(propertyHome + "/smart.search.properties")) {
            smartSearchProperties = new PropertiesConfiguration();
            smartSearchProperties.load(is);
        } catch (ConfigurationException | IOException ex) {
            logger.error("Could not load \"smartSearch.properties\": ", ex);
        }
    }

    public PropertiesConfiguration getProperties() {
        return smartSearchProperties;
    }
}
