package com.ussco.ws.ecatalog.catalog._1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Reference Match Types
 * 
 * <p>Java class for ReferenceMatchType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ReferenceMatchType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ReferenceItemNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="MatchType" type="{http://ws.ussco.com/eCatalog/catalog/1}MatchType" />
 *       &lt;attribute name="MatchDescription" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ReferenceMatchType", propOrder = { "referenceItemNumber" })
public class ReferenceMatchType {

    @XmlElement(name = "ReferenceItemNumber")
    protected String referenceItemNumber;
    @XmlAttribute(name = "MatchType")
    protected MatchType matchType;
    @XmlAttribute(name = "MatchDescription")
    protected String matchDescription;

    /**
     * Gets the value of the referenceItemNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReferenceItemNumber() {
        return referenceItemNumber;
    }

    /**
     * Sets the value of the referenceItemNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReferenceItemNumber(String value) {
        this.referenceItemNumber = value;
    }

    /**
     * Gets the value of the matchType property.
     * 
     * @return
     *     possible object is
     *     {@link MatchType }
     *     
     */
    public MatchType getMatchType() {
        return matchType;
    }

    /**
     * Sets the value of the matchType property.
     * 
     * @param value
     *     allowed object is
     *     {@link MatchType }
     *     
     */
    public void setMatchType(MatchType value) {
        this.matchType = value;
    }

    /**
     * Gets the value of the matchDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMatchDescription() {
        return matchDescription;
    }

    /**
     * Sets the value of the matchDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMatchDescription(String value) {
        this.matchDescription = value;
    }

}
