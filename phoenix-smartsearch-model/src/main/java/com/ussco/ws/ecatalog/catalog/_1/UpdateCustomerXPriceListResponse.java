package com.ussco.ws.ecatalog.catalog._1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="customerXPriceListResponse" type="{http://ws.ussco.com/eCatalog/catalog/1}MDCustomerXPriceResponseType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "customerXPriceListResponse" })
@XmlRootElement(name = "updateCustomerXPriceListResponse")
public class UpdateCustomerXPriceListResponse {

    @XmlElement(required = true)
    protected MDCustomerXPriceResponseType customerXPriceListResponse;

    /**
     * Gets the value of the customerXPriceListResponse property.
     * 
     * @return
     *     possible object is
     *     {@link MDCustomerXPriceResponseType }
     *     
     */
    public MDCustomerXPriceResponseType getCustomerXPriceListResponse() {
        return customerXPriceListResponse;
    }

    /**
     * Sets the value of the customerXPriceListResponse property.
     * 
     * @param value
     *     allowed object is
     *     {@link MDCustomerXPriceResponseType }
     *     
     */
    public void setCustomerXPriceListResponse(MDCustomerXPriceResponseType value) {
        this.customerXPriceListResponse = value;
    }

}
