package com.ussco.ws.ecatalog.catalog._1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * A site Map request
 * 
 * <p>Java class for SiteMapRequestType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SiteMapRequestType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="FilterStyle" maxOccurs="unbounded" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;enumeration value="Attribute"/>
 *               &lt;enumeration value="Brand"/>
 *               &lt;enumeration value="Category"/>
 *               &lt;enumeration value="Catalog"/>
 *               &lt;enumeration value="Item"/>
 *               &lt;enumeration value="ItemIndicator"/>
 *               &lt;enumeration value="Keyword"/>
 *               &lt;enumeration value="PriceRange"/>
 *               &lt;enumeration value="SuppliesFinderBrand"/>
 *               &lt;enumeration value="SuppliesFinderDeviceType"/>
 *               &lt;enumeration value="SuppliesFinderModel"/>
 *               &lt;enumeration value="Manufacturer"/>
 *               &lt;enumeration value="CountryOfOrigin"/>
 *               &lt;enumeration value="ProductClass"/>
 *               &lt;enumeration value="Price"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SiteMapRequestType", propOrder = { "filterStyle" })
public class SiteMapRequestType {

    @XmlElement(name = "FilterStyle")
    protected List<String> filterStyle;

    /**
     * Gets the value of the filterStyle property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the filterStyle property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFilterStyle().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getFilterStyle() {
        if (filterStyle == null) {
            filterStyle = new ArrayList<String>();
        }
        return this.filterStyle;
    }

}
