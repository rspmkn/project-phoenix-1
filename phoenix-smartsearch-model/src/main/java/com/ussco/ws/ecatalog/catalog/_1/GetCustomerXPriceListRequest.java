package com.ussco.ws.ecatalog.catalog._1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="customerXPriceListRequest" type="{http://ws.ussco.com/eCatalog/catalog/1}MDCustomerXPriceRequestType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "customerXPriceListRequest" })
@XmlRootElement(name = "getCustomerXPriceListRequest")
public class GetCustomerXPriceListRequest {

    @XmlElement(required = true)
    protected MDCustomerXPriceRequestType customerXPriceListRequest;

    /**
     * Gets the value of the customerXPriceListRequest property.
     * 
     * @return
     *     possible object is
     *     {@link MDCustomerXPriceRequestType }
     *     
     */
    public MDCustomerXPriceRequestType getCustomerXPriceListRequest() {
        return customerXPriceListRequest;
    }

    /**
     * Sets the value of the customerXPriceListRequest property.
     * 
     * @param value
     *     allowed object is
     *     {@link MDCustomerXPriceRequestType }
     *     
     */
    public void setCustomerXPriceListRequest(MDCustomerXPriceRequestType value) {
        this.customerXPriceListRequest = value;
    }

}
