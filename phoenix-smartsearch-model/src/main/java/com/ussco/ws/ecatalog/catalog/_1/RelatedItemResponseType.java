package com.ussco.ws.ecatalog.catalog._1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * A related items response
 * 
 * <p>Java class for RelatedItemResponseType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RelatedItemResponseType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RelatedItem" maxOccurs="unbounded">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;extension base="{http://ws.ussco.com/eCatalog/catalog/1}ItemType">
 *                 &lt;attribute name="RelationshipType">
 *                   &lt;simpleType>
 *                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                       &lt;enumeration value="Accessories"/>
 *                       &lt;enumeration value="CompanionItems"/>
 *                       &lt;enumeration value="CrossSell"/>
 *                       &lt;enumeration value="Family"/>
 *                       &lt;enumeration value="Equivalent"/>
 *                       &lt;enumeration value="Supplies"/>
 *                       &lt;enumeration value="Upsell"/>
 *                       &lt;enumeration value="ValueSell"/>
 *                     &lt;/restriction>
 *                   &lt;/simpleType>
 *                 &lt;/attribute>
 *               &lt;/extension>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *       &lt;attribute name="OriginalItemNumber" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RelatedItemResponseType", propOrder = { "relatedItem" })
public class RelatedItemResponseType {

    @XmlElement(name = "RelatedItem", required = true)
    protected List<RelatedItemResponseType.RelatedItem> relatedItem;
    @XmlAttribute(name = "OriginalItemNumber")
    protected String originalItemNumber;

    /**
     * Gets the value of the relatedItem property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the relatedItem property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRelatedItem().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RelatedItemResponseType.RelatedItem }
     * 
     * 
     */
    public List<RelatedItemResponseType.RelatedItem> getRelatedItem() {
        if (relatedItem == null) {
            relatedItem = new ArrayList<RelatedItemResponseType.RelatedItem>();
        }
        return this.relatedItem;
    }

    /**
     * Gets the value of the originalItemNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOriginalItemNumber() {
        return originalItemNumber;
    }

    /**
     * Sets the value of the originalItemNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOriginalItemNumber(String value) {
        this.originalItemNumber = value;
    }

    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;extension base="{http://ws.ussco.com/eCatalog/catalog/1}ItemType">
     *       &lt;attribute name="RelationshipType">
     *         &lt;simpleType>
     *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *             &lt;enumeration value="Accessories"/>
     *             &lt;enumeration value="CompanionItems"/>
     *             &lt;enumeration value="CrossSell"/>
     *             &lt;enumeration value="Family"/>
     *             &lt;enumeration value="Equivalent"/>
     *             &lt;enumeration value="Supplies"/>
     *             &lt;enumeration value="Upsell"/>
     *             &lt;enumeration value="ValueSell"/>
     *           &lt;/restriction>
     *         &lt;/simpleType>
     *       &lt;/attribute>
     *     &lt;/extension>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class RelatedItem extends ItemType {

        @XmlAttribute(name = "RelationshipType")
        protected String relationshipType;

        /**
         * Gets the value of the relationshipType property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRelationshipType() {
            return relationshipType;
        }

        /**
         * Sets the value of the relationshipType property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRelationshipType(String value) {
            this.relationshipType = value;
        }

    }

}
