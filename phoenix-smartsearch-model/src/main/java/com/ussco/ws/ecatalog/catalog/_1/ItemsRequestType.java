package com.ussco.ws.ecatalog.catalog._1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * An list of items request
 * 
 * <p>Java class for ItemsRequestType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ItemsRequestType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ItemEntry" type="{http://ws.ussco.com/eCatalog/catalog/1}ItemsEntryType" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ItemsRequestType", propOrder = { "itemEntry" })
public class ItemsRequestType {

    @XmlElement(name = "ItemEntry", required = true)
    protected List<ItemsEntryType> itemEntry;

    /**
     * Gets the value of the itemEntry property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the itemEntry property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getItemEntry().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ItemsEntryType }
     * 
     * 
     */
    public List<ItemsEntryType> getItemEntry() {
        if (itemEntry == null) {
            itemEntry = new ArrayList<ItemsEntryType>();
        }
        return this.itemEntry;
    }

}
