package com.ussco.ws.ecatalog.catalog._1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="responseHeader" type="{http://ws.ussco.com/eCatalog/catalog/1}ResponseHeaderType"/>
 *         &lt;element name="productCompareResponse" type="{http://ws.ussco.com/eCatalog/catalog/1}ProductCompareResponseType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "responseHeader", "productCompareResponse" })
@XmlRootElement(name = "getProductCompareResponse")
public class GetProductCompareResponse {

    @XmlElement(required = true)
    protected ResponseHeaderType responseHeader;
    protected ProductCompareResponseType productCompareResponse;

    /**
     * Gets the value of the responseHeader property.
     * 
     * @return
     *     possible object is
     *     {@link ResponseHeaderType }
     *     
     */
    public ResponseHeaderType getResponseHeader() {
        return responseHeader;
    }

    /**
     * Sets the value of the responseHeader property.
     * 
     * @param value
     *     allowed object is
     *     {@link ResponseHeaderType }
     *     
     */
    public void setResponseHeader(ResponseHeaderType value) {
        this.responseHeader = value;
    }

    /**
     * Gets the value of the productCompareResponse property.
     * 
     * @return
     *     possible object is
     *     {@link ProductCompareResponseType }
     *     
     */
    public ProductCompareResponseType getProductCompareResponse() {
        return productCompareResponse;
    }

    /**
     * Sets the value of the productCompareResponse property.
     * 
     * @param value
     *     allowed object is
     *     {@link ProductCompareResponseType }
     *     
     */
    public void setProductCompareResponse(ProductCompareResponseType value) {
        this.productCompareResponse = value;
    }

}
