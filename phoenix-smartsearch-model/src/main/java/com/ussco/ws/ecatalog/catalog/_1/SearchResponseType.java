package com.ussco.ws.ecatalog.catalog._1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * A search response
 * 
 * <p>Java class for SearchResponseType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SearchResponseType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SearchResponseStyle">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;enumeration value="ItemResults"/>
 *               &lt;enumeration value="LandingPage"/>
 *               &lt;enumeration value="SuppliesFinder"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;choice>
 *           &lt;element name="ItemPage" type="{http://ws.ussco.com/eCatalog/catalog/1}ItemResponseType"/>
 *           &lt;element name="LandingPage" type="{http://ws.ussco.com/eCatalog/catalog/1}LandingPageType"/>
 *           &lt;element name="SuppliesFinderPage" type="{http://ws.ussco.com/eCatalog/catalog/1}SuppliesFinderResponseType"/>
 *         &lt;/choice>
 *         &lt;element name="AvailableFilters" type="{http://ws.ussco.com/eCatalog/catalog/1}FilterListType" minOccurs="0"/>
 *         &lt;element name="AppliedFilters" type="{http://ws.ussco.com/eCatalog/catalog/1}FilterListType" minOccurs="0"/>
 *         &lt;element name="AvailableSorts" type="{http://ws.ussco.com/eCatalog/catalog/1}SortListType" minOccurs="0"/>
 *         &lt;element name="AppliedSort" type="{http://ws.ussco.com/eCatalog/catalog/1}SortType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SearchResponseType", propOrder = { "searchResponseStyle", "itemPage", "landingPage",
        "suppliesFinderPage", "availableFilters", "appliedFilters", "availableSorts", "appliedSort" })
public class SearchResponseType {

    @XmlElement(name = "SearchResponseStyle", required = true)
    protected String searchResponseStyle;
    @XmlElement(name = "ItemPage")
    protected ItemResponseType itemPage;
    @XmlElement(name = "LandingPage")
    protected LandingPageType landingPage;
    @XmlElement(name = "SuppliesFinderPage")
    protected SuppliesFinderResponseType suppliesFinderPage;
    @XmlElement(name = "AvailableFilters")
    protected FilterListType availableFilters;
    @XmlElement(name = "AppliedFilters")
    protected FilterListType appliedFilters;
    @XmlElement(name = "AvailableSorts")
    protected SortListType availableSorts;
    @XmlElement(name = "AppliedSort")
    protected SortType appliedSort;

    /**
     * Gets the value of the searchResponseStyle property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSearchResponseStyle() {
        return searchResponseStyle;
    }

    /**
     * Sets the value of the searchResponseStyle property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSearchResponseStyle(String value) {
        this.searchResponseStyle = value;
    }

    /**
     * Gets the value of the itemPage property.
     * 
     * @return
     *     possible object is
     *     {@link ItemResponseType }
     *     
     */
    public ItemResponseType getItemPage() {
        return itemPage;
    }

    /**
     * Sets the value of the itemPage property.
     * 
     * @param value
     *     allowed object is
     *     {@link ItemResponseType }
     *     
     */
    public void setItemPage(ItemResponseType value) {
        this.itemPage = value;
    }

    /**
     * Gets the value of the landingPage property.
     * 
     * @return
     *     possible object is
     *     {@link LandingPageType }
     *     
     */
    public LandingPageType getLandingPage() {
        return landingPage;
    }

    /**
     * Sets the value of the landingPage property.
     * 
     * @param value
     *     allowed object is
     *     {@link LandingPageType }
     *     
     */
    public void setLandingPage(LandingPageType value) {
        this.landingPage = value;
    }

    /**
     * Gets the value of the suppliesFinderPage property.
     * 
     * @return
     *     possible object is
     *     {@link SuppliesFinderResponseType }
     *     
     */
    public SuppliesFinderResponseType getSuppliesFinderPage() {
        return suppliesFinderPage;
    }

    /**
     * Sets the value of the suppliesFinderPage property.
     * 
     * @param value
     *     allowed object is
     *     {@link SuppliesFinderResponseType }
     *     
     */
    public void setSuppliesFinderPage(SuppliesFinderResponseType value) {
        this.suppliesFinderPage = value;
    }

    /**
     * Gets the value of the availableFilters property.
     * 
     * @return
     *     possible object is
     *     {@link FilterListType }
     *     
     */
    public FilterListType getAvailableFilters() {
        return availableFilters;
    }

    /**
     * Sets the value of the availableFilters property.
     * 
     * @param value
     *     allowed object is
     *     {@link FilterListType }
     *     
     */
    public void setAvailableFilters(FilterListType value) {
        this.availableFilters = value;
    }

    /**
     * Gets the value of the appliedFilters property.
     * 
     * @return
     *     possible object is
     *     {@link FilterListType }
     *     
     */
    public FilterListType getAppliedFilters() {
        return appliedFilters;
    }

    /**
     * Sets the value of the appliedFilters property.
     * 
     * @param value
     *     allowed object is
     *     {@link FilterListType }
     *     
     */
    public void setAppliedFilters(FilterListType value) {
        this.appliedFilters = value;
    }

    /**
     * Gets the value of the availableSorts property.
     * 
     * @return
     *     possible object is
     *     {@link SortListType }
     *     
     */
    public SortListType getAvailableSorts() {
        return availableSorts;
    }

    /**
     * Sets the value of the availableSorts property.
     * 
     * @param value
     *     allowed object is
     *     {@link SortListType }
     *     
     */
    public void setAvailableSorts(SortListType value) {
        this.availableSorts = value;
    }

    /**
     * Gets the value of the appliedSort property.
     * 
     * @return
     *     possible object is
     *     {@link SortType }
     *     
     */
    public SortType getAppliedSort() {
        return appliedSort;
    }

    /**
     * Sets the value of the appliedSort property.
     * 
     * @param value
     *     allowed object is
     *     {@link SortType }
     *     
     */
    public void setAppliedSort(SortType value) {
        this.appliedSort = value;
    }

}
