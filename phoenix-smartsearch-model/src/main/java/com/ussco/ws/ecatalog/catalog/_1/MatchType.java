package com.ussco.ws.ecatalog.catalog._1;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>Java class for MatchType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="MatchType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="NONE"/>
 *     &lt;enumeration value="EXTD"/>
 *     &lt;enumeration value="EXTUOMD"/>
 *     &lt;enumeration value="EQVD"/>
 *     &lt;enumeration value="EQVUOMD"/>
 *     &lt;enumeration value="ALTD"/>
 *     &lt;enumeration value="EXT"/>
 *     &lt;enumeration value="EXTUOM"/>
 *     &lt;enumeration value="EQV"/>
 *     &lt;enumeration value="EQVUOM"/>
 *     &lt;enumeration value="ALT"/>
 *     &lt;enumeration value="MIXED"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "MatchType")
@XmlEnum
public enum MatchType {

    NONE, EXTD, EXTUOMD, EQVD, EQVUOMD, ALTD, EXT, EXTUOM, EQV, EQVUOM, ALT, MIXED;

    public String value() {
        return name();
    }

    public static MatchType fromValue(String v) {
        return valueOf(v);
    }

}
