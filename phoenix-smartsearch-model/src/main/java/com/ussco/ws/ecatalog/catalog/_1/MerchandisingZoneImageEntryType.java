package com.ussco.ws.ecatalog.catalog._1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * An image entry in a merchandising zone
 * 
 * <p>Java class for MerchandisingZoneImageEntryType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MerchandisingZoneImageEntryType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://ws.ussco.com/eCatalog/catalog/1}MerchandisingZoneEntryType">
 *       &lt;sequence>
 *         &lt;element name="Image" type="{http://ws.ussco.com/eCatalog/catalog/1}ImageType"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MerchandisingZoneImageEntryType", propOrder = { "image" })
public class MerchandisingZoneImageEntryType extends MerchandisingZoneEntryType {

    @XmlElement(name = "Image", required = true)
    protected ImageType image;

    /**
     * Gets the value of the image property.
     * 
     * @return
     *     possible object is
     *     {@link ImageType }
     *     
     */
    public ImageType getImage() {
        return image;
    }

    /**
     * Sets the value of the image property.
     * 
     * @param value
     *     allowed object is
     *     {@link ImageType }
     *     
     */
    public void setImage(ImageType value) {
        this.image = value;
    }

}
