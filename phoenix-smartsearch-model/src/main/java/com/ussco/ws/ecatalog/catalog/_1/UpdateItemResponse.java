package com.ussco.ws.ecatalog.catalog._1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="itemResponse" type="{http://ws.ussco.com/eCatalog/catalog/1}MDItemResponseType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "itemResponse" })
@XmlRootElement(name = "updateItemResponse")
public class UpdateItemResponse {

    @XmlElement(required = true)
    protected MDItemResponseType itemResponse;

    /**
     * Gets the value of the itemResponse property.
     * 
     * @return
     *     possible object is
     *     {@link MDItemResponseType }
     *     
     */
    public MDItemResponseType getItemResponse() {
        return itemResponse;
    }

    /**
     * Sets the value of the itemResponse property.
     * 
     * @param value
     *     allowed object is
     *     {@link MDItemResponseType }
     *     
     */
    public void setItemResponse(MDItemResponseType value) {
        this.itemResponse = value;
    }

}
