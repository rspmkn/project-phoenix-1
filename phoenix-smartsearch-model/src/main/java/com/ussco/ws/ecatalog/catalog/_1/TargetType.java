package com.ussco.ws.ecatalog.catalog._1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * A target state of any clickable asset
 * 
 * <p>Java class for TargetType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TargetType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice>
 *         &lt;element name="TargetState" type="{http://ws.ussco.com/eCatalog/catalog/1}TargetStateType" minOccurs="0"/>
 *         &lt;element name="TargetURL" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/choice>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TargetType", propOrder = { "targetState", "targetURL" })
public class TargetType {

    @XmlElement(name = "TargetState")
    protected TargetStateType targetState;
    @XmlElement(name = "TargetURL")
    protected String targetURL;

    /**
     * Gets the value of the targetState property.
     * 
     * @return
     *     possible object is
     *     {@link TargetStateType }
     *     
     */
    public TargetStateType getTargetState() {
        return targetState;
    }

    /**
     * Sets the value of the targetState property.
     * 
     * @param value
     *     allowed object is
     *     {@link TargetStateType }
     *     
     */
    public void setTargetState(TargetStateType value) {
        this.targetState = value;
    }

    /**
     * Gets the value of the targetURL property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTargetURL() {
        return targetURL;
    }

    /**
     * Sets the value of the targetURL property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTargetURL(String value) {
        this.targetURL = value;
    }

}
