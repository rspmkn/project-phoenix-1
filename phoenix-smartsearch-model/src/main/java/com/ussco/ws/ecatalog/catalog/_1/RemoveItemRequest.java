package com.ussco.ws.ecatalog.catalog._1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="itemRequest" type="{http://ws.ussco.com/eCatalog/catalog/1}MDItemRequestType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "itemRequest" })
@XmlRootElement(name = "removeItemRequest")
public class RemoveItemRequest {

    @XmlElement(required = true)
    protected MDItemRequestType itemRequest;

    /**
     * Gets the value of the itemRequest property.
     * 
     * @return
     *     possible object is
     *     {@link MDItemRequestType }
     *     
     */
    public MDItemRequestType getItemRequest() {
        return itemRequest;
    }

    /**
     * Sets the value of the itemRequest property.
     * 
     * @param value
     *     allowed object is
     *     {@link MDItemRequestType }
     *     
     */
    public void setItemRequest(MDItemRequestType value) {
        this.itemRequest = value;
    }

}
