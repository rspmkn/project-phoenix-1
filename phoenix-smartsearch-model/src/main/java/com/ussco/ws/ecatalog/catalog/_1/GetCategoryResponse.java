package com.ussco.ws.ecatalog.catalog._1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="categoryResponse" type="{http://ws.ussco.com/eCatalog/catalog/1}MDCategoryResponseType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "categoryResponse" })
@XmlRootElement(name = "getCategoryResponse")
public class GetCategoryResponse {

    @XmlElement(required = true)
    protected MDCategoryResponseType categoryResponse;

    /**
     * Gets the value of the categoryResponse property.
     * 
     * @return
     *     possible object is
     *     {@link MDCategoryResponseType }
     *     
     */
    public MDCategoryResponseType getCategoryResponse() {
        return categoryResponse;
    }

    /**
     * Sets the value of the categoryResponse property.
     * 
     * @param value
     *     allowed object is
     *     {@link MDCategoryResponseType }
     *     
     */
    public void setCategoryResponse(MDCategoryResponseType value) {
        this.categoryResponse = value;
    }

}
