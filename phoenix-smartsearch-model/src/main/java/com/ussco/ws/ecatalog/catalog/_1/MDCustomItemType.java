package com.ussco.ws.ecatalog.catalog._1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>Java class for MDCustomItemType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MDCustomItemType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ResultStatus" type="{http://ws.ussco.com/eCatalog/catalog/1}StatusType" minOccurs="0"/>
 *         &lt;element name="ItemNumber" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="50"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="DealerItemNumber" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="50"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="DealerUnitOfMeasure" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="10"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="DealerDescription" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="100"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="DealerKeywords" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="1000"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="StockedItem" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;enumeration value="Y"/>
 *               &lt;enumeration value="N"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="ItemIndicator" type="{http://ws.ussco.com/eCatalog/catalog/1}ItemIndicatorType" maxOccurs="10" minOccurs="0"/>
 *         &lt;element name="CategoryId" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="20"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="ItemImage" type="{http://ws.ussco.com/eCatalog/catalog/1}ImageType" maxOccurs="3" minOccurs="0"/>
 *         &lt;element name="MPN" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="70"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="ListPrice" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Sequence" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="999999"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *       &lt;/sequence>
 *       &lt;attribute name="UpdateStyle" default="Insert">
 *         &lt;simpleType>
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *             &lt;enumeration value="Insert"/>
 *             &lt;enumeration value="Replace"/>
 *           &lt;/restriction>
 *         &lt;/simpleType>
 *       &lt;/attribute>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MDCustomItemType", propOrder = { "resultStatus", "itemNumber", "dealerItemNumber",
        "dealerUnitOfMeasure", "dealerDescription", "dealerKeywords", "stockedItem", "itemIndicator", "categoryId",
        "itemImage", "mpn", "listPrice", "sequence" })
public class MDCustomItemType {

    @XmlElement(name = "ResultStatus")
    protected StatusType resultStatus;
    @XmlElement(name = "ItemNumber")
    protected String itemNumber;
    @XmlElement(name = "DealerItemNumber")
    protected String dealerItemNumber;
    @XmlElement(name = "DealerUnitOfMeasure")
    protected String dealerUnitOfMeasure;
    @XmlElement(name = "DealerDescription")
    protected String dealerDescription;
    @XmlElement(name = "DealerKeywords")
    protected String dealerKeywords;
    @XmlElement(name = "StockedItem")
    protected String stockedItem;
    @XmlElement(name = "ItemIndicator")
    protected List<ItemIndicatorType> itemIndicator;
    @XmlElement(name = "CategoryId")
    protected String categoryId;
    @XmlElement(name = "ItemImage")
    protected List<ImageType> itemImage;
    @XmlElement(name = "MPN")
    protected String mpn;
    @XmlElement(name = "ListPrice")
    protected String listPrice;
    @XmlElement(name = "Sequence")
    protected Integer sequence;
    @XmlAttribute(name = "UpdateStyle")
    protected String updateStyle;

    /**
     * Gets the value of the resultStatus property.
     * 
     * @return
     *     possible object is
     *     {@link StatusType }
     *     
     */
    public StatusType getResultStatus() {
        return resultStatus;
    }

    /**
     * Sets the value of the resultStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link StatusType }
     *     
     */
    public void setResultStatus(StatusType value) {
        this.resultStatus = value;
    }

    /**
     * Gets the value of the itemNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getItemNumber() {
        return itemNumber;
    }

    /**
     * Sets the value of the itemNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setItemNumber(String value) {
        this.itemNumber = value;
    }

    /**
     * Gets the value of the dealerItemNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDealerItemNumber() {
        return dealerItemNumber;
    }

    /**
     * Sets the value of the dealerItemNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDealerItemNumber(String value) {
        this.dealerItemNumber = value;
    }

    /**
     * Gets the value of the dealerUnitOfMeasure property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDealerUnitOfMeasure() {
        return dealerUnitOfMeasure;
    }

    /**
     * Sets the value of the dealerUnitOfMeasure property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDealerUnitOfMeasure(String value) {
        this.dealerUnitOfMeasure = value;
    }

    /**
     * Gets the value of the dealerDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDealerDescription() {
        return dealerDescription;
    }

    /**
     * Sets the value of the dealerDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDealerDescription(String value) {
        this.dealerDescription = value;
    }

    /**
     * Gets the value of the dealerKeywords property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDealerKeywords() {
        return dealerKeywords;
    }

    /**
     * Sets the value of the dealerKeywords property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDealerKeywords(String value) {
        this.dealerKeywords = value;
    }

    /**
     * Gets the value of the stockedItem property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStockedItem() {
        return stockedItem;
    }

    /**
     * Sets the value of the stockedItem property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStockedItem(String value) {
        this.stockedItem = value;
    }

    /**
     * Gets the value of the itemIndicator property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the itemIndicator property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getItemIndicator().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ItemIndicatorType }
     * 
     * 
     */
    public List<ItemIndicatorType> getItemIndicator() {
        if (itemIndicator == null) {
            itemIndicator = new ArrayList<ItemIndicatorType>();
        }
        return this.itemIndicator;
    }

    /**
     * Gets the value of the categoryId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCategoryId() {
        return categoryId;
    }

    /**
     * Sets the value of the categoryId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCategoryId(String value) {
        this.categoryId = value;
    }

    /**
     * Gets the value of the itemImage property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the itemImage property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getItemImage().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ImageType }
     * 
     * 
     */
    public List<ImageType> getItemImage() {
        if (itemImage == null) {
            itemImage = new ArrayList<ImageType>();
        }
        return this.itemImage;
    }

    /**
     * Gets the value of the mpn property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMPN() {
        return mpn;
    }

    /**
     * Sets the value of the mpn property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMPN(String value) {
        this.mpn = value;
    }

    /**
     * Gets the value of the listPrice property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getListPrice() {
        return listPrice;
    }

    /**
     * Sets the value of the listPrice property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setListPrice(String value) {
        this.listPrice = value;
    }

    /**
     * Gets the value of the sequence property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getSequence() {
        return sequence;
    }

    /**
     * Sets the value of the sequence property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setSequence(Integer value) {
        this.sequence = value;
    }

    /**
     * Gets the value of the updateStyle property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUpdateStyle() {
        if (updateStyle == null) {
            return "Insert";
        }
        else {
            return updateStyle;
        }
    }

    /**
     * Sets the value of the updateStyle property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUpdateStyle(String value) {
        this.updateStyle = value;
    }

}
