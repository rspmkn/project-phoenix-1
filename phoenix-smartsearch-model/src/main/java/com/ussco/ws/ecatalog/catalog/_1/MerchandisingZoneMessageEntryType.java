package com.ussco.ws.ecatalog.catalog._1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * A message entry in a merchandising zone
 * 
 * <p>Java class for MerchandisingZoneMessageEntryType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MerchandisingZoneMessageEntryType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://ws.ussco.com/eCatalog/catalog/1}MerchandisingZoneEntryType">
 *       &lt;sequence>
 *         &lt;element name="Message" type="{http://ws.ussco.com/eCatalog/catalog/1}MessageType"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MerchandisingZoneMessageEntryType", propOrder = { "message" })
public class MerchandisingZoneMessageEntryType extends MerchandisingZoneEntryType {

    @XmlElement(name = "Message", required = true)
    protected MessageType message;

    /**
     * Gets the value of the message property.
     * 
     * @return
     *     possible object is
     *     {@link MessageType }
     *     
     */
    public MessageType getMessage() {
        return message;
    }

    /**
     * Sets the value of the message property.
     * 
     * @param value
     *     allowed object is
     *     {@link MessageType }
     *     
     */
    public void setMessage(MessageType value) {
        this.message = value;
    }

}
