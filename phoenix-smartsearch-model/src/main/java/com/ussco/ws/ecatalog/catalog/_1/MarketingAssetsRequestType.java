package com.ussco.ws.ecatalog.catalog._1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * A Marketing Assets Request
 * 
 * <p>Java class for MarketingAssetsRequestType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MarketingAssetsRequestType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SearchFilters" type="{http://ws.ussco.com/eCatalog/catalog/1}FilterListType" minOccurs="0"/>
 *         &lt;element name="AllowBrandLanding" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="AllowCategoryLanding" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="ItemReferenceFilters" type="{http://ws.ussco.com/eCatalog/catalog/1}ItemReferenceFilterListType" minOccurs="0"/>
 *         &lt;element name="ValidateContent" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MarketingAssetsRequestType", propOrder = { "searchFilters", "allowBrandLanding",
        "allowCategoryLanding", "itemReferenceFilters", "validateContent" })
public class MarketingAssetsRequestType {

    @XmlElement(name = "SearchFilters")
    protected FilterListType searchFilters;
    @XmlElement(name = "AllowBrandLanding", defaultValue = "true")
    protected Boolean allowBrandLanding;
    @XmlElement(name = "AllowCategoryLanding", defaultValue = "true")
    protected Boolean allowCategoryLanding;
    @XmlElement(name = "ItemReferenceFilters")
    protected ItemReferenceFilterListType itemReferenceFilters;
    @XmlElement(name = "ValidateContent", defaultValue = "false")
    protected Boolean validateContent;

    /**
     * Gets the value of the searchFilters property.
     * 
     * @return
     *     possible object is
     *     {@link FilterListType }
     *     
     */
    public FilterListType getSearchFilters() {
        return searchFilters;
    }

    /**
     * Sets the value of the searchFilters property.
     * 
     * @param value
     *     allowed object is
     *     {@link FilterListType }
     *     
     */
    public void setSearchFilters(FilterListType value) {
        this.searchFilters = value;
    }

    /**
     * Gets the value of the allowBrandLanding property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAllowBrandLanding() {
        return allowBrandLanding;
    }

    /**
     * Sets the value of the allowBrandLanding property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAllowBrandLanding(Boolean value) {
        this.allowBrandLanding = value;
    }

    /**
     * Gets the value of the allowCategoryLanding property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAllowCategoryLanding() {
        return allowCategoryLanding;
    }

    /**
     * Sets the value of the allowCategoryLanding property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAllowCategoryLanding(Boolean value) {
        this.allowCategoryLanding = value;
    }

    /**
     * Gets the value of the itemReferenceFilters property.
     * 
     * @return
     *     possible object is
     *     {@link ItemReferenceFilterListType }
     *     
     */
    public ItemReferenceFilterListType getItemReferenceFilters() {
        return itemReferenceFilters;
    }

    /**
     * Sets the value of the itemReferenceFilters property.
     * 
     * @param value
     *     allowed object is
     *     {@link ItemReferenceFilterListType }
     *     
     */
    public void setItemReferenceFilters(ItemReferenceFilterListType value) {
        this.itemReferenceFilters = value;
    }

    /**
     * Gets the value of the validateContent property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isValidateContent() {
        return validateContent;
    }

    /**
     * Sets the value of the validateContent property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setValidateContent(Boolean value) {
        this.validateContent = value;
    }

}
