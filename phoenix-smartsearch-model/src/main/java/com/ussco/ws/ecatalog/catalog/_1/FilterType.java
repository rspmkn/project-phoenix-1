package com.ussco.ws.ecatalog.catalog._1;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * A single filter
 * 
 * <p>Java class for FilterType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="FilterType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="FilterStyle">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;enumeration value="Attribute"/>
 *               &lt;enumeration value="Brand"/>
 *               &lt;enumeration value="Category"/>
 *               &lt;enumeration value="Catalog"/>
 *               &lt;enumeration value="Contract"/>
 *               &lt;enumeration value="Item"/>
 *               &lt;enumeration value="ItemIndicator"/>
 *               &lt;enumeration value="Keyword"/>
 *               &lt;enumeration value="PriceRange"/>
 *               &lt;enumeration value="SuppliesFinderBrand"/>
 *               &lt;enumeration value="SuppliesFinderDeviceType"/>
 *               &lt;enumeration value="SuppliesFinderModel"/>
 *               &lt;enumeration value="Manufacturer"/>
 *               &lt;enumeration value="CountryOfOrigin"/>
 *               &lt;enumeration value="ProductClass"/>
 *               &lt;enumeration value="Price"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="FilterDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FilterValue" type="{http://ws.ussco.com/eCatalog/catalog/1}FilterValueType" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *       &lt;attribute name="displayStyle">
 *         &lt;simpleType>
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *             &lt;enumeration value="Top"/>
 *             &lt;enumeration value="Full"/>
 *           &lt;/restriction>
 *         &lt;/simpleType>
 *       &lt;/attribute>
 *       &lt;attribute name="keywordInterface">
 *         &lt;simpleType>
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *             &lt;enumeration value="Standard"/>
 *             &lt;enumeration value="SuppliesFinder"/>
 *             &lt;enumeration value="Item"/>
 *             &lt;enumeration value="ImageSupplies"/>
 *             &lt;enumeration value="SuppliesFinderModel"/>
 *           &lt;/restriction>
 *         &lt;/simpleType>
 *       &lt;/attribute>
 *       &lt;attribute name="sequence" type="{http://www.w3.org/2001/XMLSchema}integer" />
 *       &lt;attribute name="CrossReference" type="{http://ws.ussco.com/eCatalog/catalog/1}MatchType" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FilterType", propOrder = { "filterStyle", "filterDescription", "filterValue" })
public class FilterType {

    @XmlElement(name = "FilterStyle", required = true)
    protected String filterStyle;
    @XmlElement(name = "FilterDescription")
    protected String filterDescription;
    @XmlElement(name = "FilterValue", required = true)
    protected List<FilterValueType> filterValue;
    @XmlAttribute
    protected String displayStyle;
    @XmlAttribute
    protected String keywordInterface;
    @XmlAttribute
    protected BigInteger sequence;
    @XmlAttribute(name = "CrossReference")
    protected MatchType crossReference;

    /**
     * Gets the value of the filterStyle property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFilterStyle() {
        return filterStyle;
    }

    /**
     * Sets the value of the filterStyle property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFilterStyle(String value) {
        this.filterStyle = value;
    }

    /**
     * Gets the value of the filterDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFilterDescription() {
        return filterDescription;
    }

    /**
     * Sets the value of the filterDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFilterDescription(String value) {
        this.filterDescription = value;
    }

    /**
     * Gets the value of the filterValue property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the filterValue property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFilterValue().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FilterValueType }
     * 
     * 
     */
    public List<FilterValueType> getFilterValue() {
        if (filterValue == null) {
            filterValue = new ArrayList<FilterValueType>();
        }
        return this.filterValue;
    }

    /**
     * Gets the value of the displayStyle property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDisplayStyle() {
        return displayStyle;
    }

    /**
     * Sets the value of the displayStyle property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDisplayStyle(String value) {
        this.displayStyle = value;
    }

    /**
     * Gets the value of the keywordInterface property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getKeywordInterface() {
        return keywordInterface;
    }

    /**
     * Sets the value of the keywordInterface property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setKeywordInterface(String value) {
        this.keywordInterface = value;
    }

    /**
     * Gets the value of the sequence property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getSequence() {
        return sequence;
    }

    /**
     * Sets the value of the sequence property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setSequence(BigInteger value) {
        this.sequence = value;
    }

    /**
     * Gets the value of the crossReference property.
     * 
     * @return
     *     possible object is
     *     {@link MatchType }
     *     
     */
    public MatchType getCrossReference() {
        return crossReference;
    }

    /**
     * Sets the value of the crossReference property.
     * 
     * @param value
     *     allowed object is
     *     {@link MatchType }
     *     
     */
    public void setCrossReference(MatchType value) {
        this.crossReference = value;
    }

}
