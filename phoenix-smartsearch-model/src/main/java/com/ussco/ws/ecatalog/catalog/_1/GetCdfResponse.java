package com.ussco.ws.ecatalog.catalog._1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="cdfResponse" type="{http://ws.ussco.com/eCatalog/catalog/1}MDCdfResponseType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "cdfResponse" })
@XmlRootElement(name = "getCdfResponse")
public class GetCdfResponse {

    @XmlElement(required = true)
    protected MDCdfResponseType cdfResponse;

    /**
     * Gets the value of the cdfResponse property.
     * 
     * @return
     *     possible object is
     *     {@link MDCdfResponseType }
     *     
     */
    public MDCdfResponseType getCdfResponse() {
        return cdfResponse;
    }

    /**
     * Sets the value of the cdfResponse property.
     * 
     * @param value
     *     allowed object is
     *     {@link MDCdfResponseType }
     *     
     */
    public void setCdfResponse(MDCdfResponseType value) {
        this.cdfResponse = value;
    }

}
