package com.ussco.ws.ecatalog.catalog._1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * List of Items + Marketing assets
 * 
 * <p>Java class for MerchItemPageResponseType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MerchItemPageResponseType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Item" type="{http://ws.ussco.com/eCatalog/catalog/1}MerchItemType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="MerchandisingZone" type="{http://ws.ussco.com/eCatalog/catalog/1}MerchandisingZoneType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MerchItemPageResponseType", propOrder = { "item", "merchandisingZone" })
public class MerchItemPageResponseType {

    @XmlElement(name = "Item")
    protected List<MerchItemType> item;
    @XmlElement(name = "MerchandisingZone")
    protected List<MerchandisingZoneType> merchandisingZone;

    /**
     * Gets the value of the item property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the item property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getItem().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link MerchItemType }
     * 
     * 
     */
    public List<MerchItemType> getItem() {
        if (item == null) {
            item = new ArrayList<MerchItemType>();
        }
        return this.item;
    }

    /**
     * Gets the value of the merchandisingZone property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the merchandisingZone property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMerchandisingZone().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link MerchandisingZoneType }
     * 
     * 
     */
    public List<MerchandisingZoneType> getMerchandisingZone() {
        if (merchandisingZone == null) {
            merchandisingZone = new ArrayList<MerchandisingZoneType>();
        }
        return this.merchandisingZone;
    }

}
