package com.ussco.ws.ecatalog.catalog._1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>Java class for MDCustomerXPriceRequestType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MDCustomerXPriceRequestType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CustomerXPrice" type="{http://ws.ussco.com/eCatalog/catalog/1}MDCustomerXPriceType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MDCustomerXPriceRequestType", propOrder = { "customerXPrice" })
public class MDCustomerXPriceRequestType {

    @XmlElement(name = "CustomerXPrice")
    protected List<MDCustomerXPriceType> customerXPrice;

    /**
     * Gets the value of the customerXPrice property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the customerXPrice property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCustomerXPrice().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link MDCustomerXPriceType }
     * 
     * 
     */
    public List<MDCustomerXPriceType> getCustomerXPrice() {
        if (customerXPrice == null) {
            customerXPrice = new ArrayList<MDCustomerXPriceType>();
        }
        return this.customerXPrice;
    }

}
