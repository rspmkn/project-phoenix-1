package com.ussco.ws.ecatalog.catalog._1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="requestHeader" type="{http://ws.ussco.com/eCatalog/catalog/1}RequestHeaderType"/>
 *         &lt;element name="SiteMapRequest" type="{http://ws.ussco.com/eCatalog/catalog/1}SiteMapRequestType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "requestHeader", "siteMapRequest" })
@XmlRootElement(name = "getSiteMapRequest")
public class GetSiteMapRequest {

    @XmlElement(required = true)
    protected RequestHeaderType requestHeader;
    @XmlElement(name = "SiteMapRequest", required = true)
    protected SiteMapRequestType siteMapRequest;

    /**
     * Gets the value of the requestHeader property.
     * 
     * @return
     *     possible object is
     *     {@link RequestHeaderType }
     *     
     */
    public RequestHeaderType getRequestHeader() {
        return requestHeader;
    }

    /**
     * Sets the value of the requestHeader property.
     * 
     * @param value
     *     allowed object is
     *     {@link RequestHeaderType }
     *     
     */
    public void setRequestHeader(RequestHeaderType value) {
        this.requestHeader = value;
    }

    /**
     * Gets the value of the siteMapRequest property.
     * 
     * @return
     *     possible object is
     *     {@link SiteMapRequestType }
     *     
     */
    public SiteMapRequestType getSiteMapRequest() {
        return siteMapRequest;
    }

    /**
     * Sets the value of the siteMapRequest property.
     * 
     * @param value
     *     allowed object is
     *     {@link SiteMapRequestType }
     *     
     */
    public void setSiteMapRequest(SiteMapRequestType value) {
        this.siteMapRequest = value;
    }

}
