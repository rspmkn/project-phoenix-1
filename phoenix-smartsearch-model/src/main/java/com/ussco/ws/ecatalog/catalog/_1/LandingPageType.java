package com.ussco.ws.ecatalog.catalog._1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * A landing page
 * 
 * <p>Java class for LandingPageType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="LandingPageType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="LandingPageName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="AvailableFilters" type="{http://ws.ussco.com/eCatalog/catalog/1}FilterListType" minOccurs="0"/>
 *         &lt;element name="MerchandisingZone" type="{http://ws.ussco.com/eCatalog/catalog/1}MerchandisingZoneType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LandingPageType", propOrder = { "landingPageName", "availableFilters", "merchandisingZone" })
public class LandingPageType {

    @XmlElement(name = "LandingPageName", required = true)
    protected String landingPageName;
    @XmlElement(name = "AvailableFilters")
    protected FilterListType availableFilters;
    @XmlElement(name = "MerchandisingZone")
    protected List<MerchandisingZoneType> merchandisingZone;

    /**
     * Gets the value of the landingPageName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLandingPageName() {
        return landingPageName;
    }

    /**
     * Sets the value of the landingPageName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLandingPageName(String value) {
        this.landingPageName = value;
    }

    /**
     * Gets the value of the availableFilters property.
     * 
     * @return
     *     possible object is
     *     {@link FilterListType }
     *     
     */
    public FilterListType getAvailableFilters() {
        return availableFilters;
    }

    /**
     * Sets the value of the availableFilters property.
     * 
     * @param value
     *     allowed object is
     *     {@link FilterListType }
     *     
     */
    public void setAvailableFilters(FilterListType value) {
        this.availableFilters = value;
    }

    /**
     * Gets the value of the merchandisingZone property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the merchandisingZone property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMerchandisingZone().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link MerchandisingZoneType }
     * 
     * 
     */
    public List<MerchandisingZoneType> getMerchandisingZone() {
        if (merchandisingZone == null) {
            merchandisingZone = new ArrayList<MerchandisingZoneType>();
        }
        return this.merchandisingZone;
    }

}
