package com.ussco.ws.ecatalog.catalog._1;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;

/**
 * An offer response
 * 
 * <p>Java class for OfferResponseType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="OfferResponseType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CampaignId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="OfferGroup" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OfferType">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;enumeration value="FreeGood"/>
 *               &lt;enumeration value="Discount"/>
 *               &lt;enumeration value="InformationOnly"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="OfferFunding" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;enumeration value="Vendor"/>
 *               &lt;enumeration value="Reseller"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="OfferAmount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="FreeGoods" type="{http://ws.ussco.com/eCatalog/catalog/1}OfferFreeGoodsType" minOccurs="0"/>
 *         &lt;element name="ExpirationDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *         &lt;element name="HeadingText" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="HeadingImage" type="{http://ws.ussco.com/eCatalog/catalog/1}ImageType"/>
 *         &lt;element name="SellingStatement" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OfferText" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Resource" type="{http://ws.ussco.com/eCatalog/catalog/1}ResourceType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="QualifyingCriteria" type="{http://ws.ussco.com/eCatalog/catalog/1}OfferQualifyingCriteriaType" minOccurs="0"/>
 *         &lt;element name="Tags" type="{http://ws.ussco.com/eCatalog/catalog/1}TagType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OfferResponseType", propOrder = { "campaignId", "offerGroup", "offerType", "offerFunding",
        "offerAmount", "freeGoods", "expirationDate", "headingText", "headingImage", "sellingStatement", "offerText",
        "resource", "qualifyingCriteria", "tags" })
public class OfferResponseType {

    @XmlElement(name = "CampaignId", required = true)
    protected String campaignId;
    @XmlElement(name = "OfferGroup")
    protected String offerGroup;
    @XmlElement(name = "OfferType", required = true)
    protected String offerType;
    @XmlElement(name = "OfferFunding")
    protected String offerFunding;
    @XmlElement(name = "OfferAmount")
    protected BigDecimal offerAmount;
    @XmlElement(name = "FreeGoods")
    protected OfferFreeGoodsType freeGoods;
    @XmlElement(name = "ExpirationDate")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar expirationDate;
    @XmlElement(name = "HeadingText", required = true)
    protected String headingText;
    @XmlElement(name = "HeadingImage", required = true)
    protected ImageType headingImage;
    @XmlElement(name = "SellingStatement")
    protected String sellingStatement;
    @XmlElement(name = "OfferText")
    protected String offerText;
    @XmlElement(name = "Resource")
    protected List<ResourceType> resource;
    @XmlElement(name = "QualifyingCriteria")
    protected OfferQualifyingCriteriaType qualifyingCriteria;
    @XmlElement(name = "Tags")
    protected List<TagType> tags;

    /**
     * Gets the value of the campaignId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCampaignId() {
        return campaignId;
    }

    /**
     * Sets the value of the campaignId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCampaignId(String value) {
        this.campaignId = value;
    }

    /**
     * Gets the value of the offerGroup property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOfferGroup() {
        return offerGroup;
    }

    /**
     * Sets the value of the offerGroup property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOfferGroup(String value) {
        this.offerGroup = value;
    }

    /**
     * Gets the value of the offerType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOfferType() {
        return offerType;
    }

    /**
     * Sets the value of the offerType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOfferType(String value) {
        this.offerType = value;
    }

    /**
     * Gets the value of the offerFunding property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOfferFunding() {
        return offerFunding;
    }

    /**
     * Sets the value of the offerFunding property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOfferFunding(String value) {
        this.offerFunding = value;
    }

    /**
     * Gets the value of the offerAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getOfferAmount() {
        return offerAmount;
    }

    /**
     * Sets the value of the offerAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setOfferAmount(BigDecimal value) {
        this.offerAmount = value;
    }

    /**
     * Gets the value of the freeGoods property.
     * 
     * @return
     *     possible object is
     *     {@link OfferFreeGoodsType }
     *     
     */
    public OfferFreeGoodsType getFreeGoods() {
        return freeGoods;
    }

    /**
     * Sets the value of the freeGoods property.
     * 
     * @param value
     *     allowed object is
     *     {@link OfferFreeGoodsType }
     *     
     */
    public void setFreeGoods(OfferFreeGoodsType value) {
        this.freeGoods = value;
    }

    /**
     * Gets the value of the expirationDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getExpirationDate() {
        return expirationDate;
    }

    /**
     * Sets the value of the expirationDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setExpirationDate(XMLGregorianCalendar value) {
        this.expirationDate = value;
    }

    /**
     * Gets the value of the headingText property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHeadingText() {
        return headingText;
    }

    /**
     * Sets the value of the headingText property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHeadingText(String value) {
        this.headingText = value;
    }

    /**
     * Gets the value of the headingImage property.
     * 
     * @return
     *     possible object is
     *     {@link ImageType }
     *     
     */
    public ImageType getHeadingImage() {
        return headingImage;
    }

    /**
     * Sets the value of the headingImage property.
     * 
     * @param value
     *     allowed object is
     *     {@link ImageType }
     *     
     */
    public void setHeadingImage(ImageType value) {
        this.headingImage = value;
    }

    /**
     * Gets the value of the sellingStatement property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSellingStatement() {
        return sellingStatement;
    }

    /**
     * Sets the value of the sellingStatement property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSellingStatement(String value) {
        this.sellingStatement = value;
    }

    /**
     * Gets the value of the offerText property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOfferText() {
        return offerText;
    }

    /**
     * Sets the value of the offerText property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOfferText(String value) {
        this.offerText = value;
    }

    /**
     * Gets the value of the resource property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the resource property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getResource().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ResourceType }
     * 
     * 
     */
    public List<ResourceType> getResource() {
        if (resource == null) {
            resource = new ArrayList<ResourceType>();
        }
        return this.resource;
    }

    /**
     * Gets the value of the qualifyingCriteria property.
     * 
     * @return
     *     possible object is
     *     {@link OfferQualifyingCriteriaType }
     *     
     */
    public OfferQualifyingCriteriaType getQualifyingCriteria() {
        return qualifyingCriteria;
    }

    /**
     * Sets the value of the qualifyingCriteria property.
     * 
     * @param value
     *     allowed object is
     *     {@link OfferQualifyingCriteriaType }
     *     
     */
    public void setQualifyingCriteria(OfferQualifyingCriteriaType value) {
        this.qualifyingCriteria = value;
    }

    /**
     * Gets the value of the tags property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the tags property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTags().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TagType }
     * 
     * 
     */
    public List<TagType> getTags() {
        if (tags == null) {
            tags = new ArrayList<TagType>();
        }
        return this.tags;
    }

}
