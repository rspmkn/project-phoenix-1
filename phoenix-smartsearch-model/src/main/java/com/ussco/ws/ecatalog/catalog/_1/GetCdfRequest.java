package com.ussco.ws.ecatalog.catalog._1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="cdfRequest" type="{http://ws.ussco.com/eCatalog/catalog/1}MDCdfRequestType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "cdfRequest" })
public class GetCdfRequest {

    @XmlElement(required = true)
    protected MDCdfRequestType cdfRequest;

    /**
     * Gets the value of the cdfRequest property.
     * 
     * @return
     *     possible object is
     *     {@link MDCdfRequestType }
     *     
     */
    public MDCdfRequestType getCdfRequest() {
        return cdfRequest;
    }

    /**
     * Sets the value of the cdfRequest property.
     * 
     * @param value
     *     allowed object is
     *     {@link MDCdfRequestType }
     *     
     */
    public void setCdfRequest(MDCdfRequestType value) {
        this.cdfRequest = value;
    }

}
