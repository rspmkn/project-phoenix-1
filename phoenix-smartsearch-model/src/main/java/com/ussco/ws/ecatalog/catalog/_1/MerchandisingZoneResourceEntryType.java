package com.ussco.ws.ecatalog.catalog._1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * A resource entry in a merchandising zone
 * 
 * <p>Java class for MerchandisingZoneResourceEntryType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MerchandisingZoneResourceEntryType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://ws.ussco.com/eCatalog/catalog/1}MerchandisingZoneEntryType">
 *       &lt;sequence>
 *         &lt;element name="Resource" type="{http://ws.ussco.com/eCatalog/catalog/1}ResourceType"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MerchandisingZoneResourceEntryType", propOrder = { "resource" })
public class MerchandisingZoneResourceEntryType extends MerchandisingZoneEntryType {

    @XmlElement(name = "Resource", required = true)
    protected ResourceType resource;

    /**
     * Gets the value of the resource property.
     * 
     * @return
     *     possible object is
     *     {@link ResourceType }
     *     
     */
    public ResourceType getResource() {
        return resource;
    }

    /**
     * Sets the value of the resource property.
     * 
     * @param value
     *     allowed object is
     *     {@link ResourceType }
     *     
     */
    public void setResource(ResourceType value) {
        this.resource = value;
    }

}
