package com.ussco.ws.ecatalog.catalog._1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Common response header
 * 
 * <p>Java class for ResponseHeaderType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ResponseHeaderType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ResultStatus" type="{http://ws.ussco.com/eCatalog/catalog/1}StatusType"/>
 *         &lt;element name="CatNavBanner" type="{http://ws.ussco.com/eCatalog/catalog/1}CatNavBannerType" minOccurs="0"/>
 *         &lt;element name="CatNavMerch" type="{http://ws.ussco.com/eCatalog/catalog/1}MerchandisingZoneType" minOccurs="0"/>
 *         &lt;element name="IncExcList" type="{http://ws.ussco.com/eCatalog/catalog/1}ItemListType" minOccurs="0"/>
 *         &lt;element name="PriceList" type="{http://ws.ussco.com/eCatalog/catalog/1}ItemListType" minOccurs="0"/>
 *         &lt;element name="ContractList" type="{http://ws.ussco.com/eCatalog/catalog/1}ItemListType" minOccurs="0"/>
 *         &lt;element name="ResponseTime" type="{http://ws.ussco.com/eCatalog/catalog/1}ResponseTimeType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="GreenFilter" type="{http://ws.ussco.com/eCatalog/catalog/1}FilterType" minOccurs="0"/>
 *         &lt;element name="CountryOfOriginFilter" type="{http://ws.ussco.com/eCatalog/catalog/1}FilterType" minOccurs="0"/>
 *         &lt;element name="IsPremium" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ResponseHeaderType", propOrder = { "resultStatus", "catNavBanner", "catNavMerch", "incExcList",
        "priceList", "contractList", "responseTime", "greenFilter", "countryOfOriginFilter", "isPremium" })
public class ResponseHeaderType {

    @XmlElement(name = "ResultStatus", required = true)
    protected StatusType resultStatus;
    @XmlElement(name = "CatNavBanner")
    protected CatNavBannerType catNavBanner;
    @XmlElement(name = "CatNavMerch")
    protected MerchandisingZoneType catNavMerch;
    @XmlElement(name = "IncExcList")
    protected ItemListType incExcList;
    @XmlElement(name = "PriceList")
    protected ItemListType priceList;
    @XmlElement(name = "ContractList")
    protected ItemListType contractList;
    @XmlElement(name = "ResponseTime")
    protected List<ResponseTimeType> responseTime;
    @XmlElement(name = "GreenFilter")
    protected FilterType greenFilter;
    @XmlElement(name = "CountryOfOriginFilter")
    protected FilterType countryOfOriginFilter;
    @XmlElement(name = "IsPremium")
    protected Boolean isPremium;

    /**
     * Gets the value of the resultStatus property.
     * 
     * @return
     *     possible object is
     *     {@link StatusType }
     *     
     */
    public StatusType getResultStatus() {
        return resultStatus;
    }

    /**
     * Sets the value of the resultStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link StatusType }
     *     
     */
    public void setResultStatus(StatusType value) {
        this.resultStatus = value;
    }

    /**
     * Gets the value of the catNavBanner property.
     * 
     * @return
     *     possible object is
     *     {@link CatNavBannerType }
     *     
     */
    public CatNavBannerType getCatNavBanner() {
        return catNavBanner;
    }

    /**
     * Sets the value of the catNavBanner property.
     * 
     * @param value
     *     allowed object is
     *     {@link CatNavBannerType }
     *     
     */
    public void setCatNavBanner(CatNavBannerType value) {
        this.catNavBanner = value;
    }

    /**
     * Gets the value of the catNavMerch property.
     * 
     * @return
     *     possible object is
     *     {@link MerchandisingZoneType }
     *     
     */
    public MerchandisingZoneType getCatNavMerch() {
        return catNavMerch;
    }

    /**
     * Sets the value of the catNavMerch property.
     * 
     * @param value
     *     allowed object is
     *     {@link MerchandisingZoneType }
     *     
     */
    public void setCatNavMerch(MerchandisingZoneType value) {
        this.catNavMerch = value;
    }

    /**
     * Gets the value of the incExcList property.
     * 
     * @return
     *     possible object is
     *     {@link ItemListType }
     *     
     */
    public ItemListType getIncExcList() {
        return incExcList;
    }

    /**
     * Sets the value of the incExcList property.
     * 
     * @param value
     *     allowed object is
     *     {@link ItemListType }
     *     
     */
    public void setIncExcList(ItemListType value) {
        this.incExcList = value;
    }

    /**
     * Gets the value of the priceList property.
     * 
     * @return
     *     possible object is
     *     {@link ItemListType }
     *     
     */
    public ItemListType getPriceList() {
        return priceList;
    }

    /**
     * Sets the value of the priceList property.
     * 
     * @param value
     *     allowed object is
     *     {@link ItemListType }
     *     
     */
    public void setPriceList(ItemListType value) {
        this.priceList = value;
    }

    /**
     * Gets the value of the contractList property.
     * 
     * @return
     *     possible object is
     *     {@link ItemListType }
     *     
     */
    public ItemListType getContractList() {
        return contractList;
    }

    /**
     * Sets the value of the contractList property.
     * 
     * @param value
     *     allowed object is
     *     {@link ItemListType }
     *     
     */
    public void setContractList(ItemListType value) {
        this.contractList = value;
    }

    /**
     * Gets the value of the responseTime property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the responseTime property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getResponseTime().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ResponseTimeType }
     * 
     * 
     */
    public List<ResponseTimeType> getResponseTime() {
        if (responseTime == null) {
            responseTime = new ArrayList<ResponseTimeType>();
        }
        return this.responseTime;
    }

    /**
     * Gets the value of the greenFilter property.
     * 
     * @return
     *     possible object is
     *     {@link FilterType }
     *     
     */
    public FilterType getGreenFilter() {
        return greenFilter;
    }

    /**
     * Sets the value of the greenFilter property.
     * 
     * @param value
     *     allowed object is
     *     {@link FilterType }
     *     
     */
    public void setGreenFilter(FilterType value) {
        this.greenFilter = value;
    }

    /**
     * Gets the value of the countryOfOriginFilter property.
     * 
     * @return
     *     possible object is
     *     {@link FilterType }
     *     
     */
    public FilterType getCountryOfOriginFilter() {
        return countryOfOriginFilter;
    }

    /**
     * Sets the value of the countryOfOriginFilter property.
     * 
     * @param value
     *     allowed object is
     *     {@link FilterType }
     *     
     */
    public void setCountryOfOriginFilter(FilterType value) {
        this.countryOfOriginFilter = value;
    }

    /**
     * Gets the value of the isPremium property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsPremium() {
        return isPremium;
    }

    /**
     * Sets the value of the isPremium property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsPremium(Boolean value) {
        this.isPremium = value;
    }

}
