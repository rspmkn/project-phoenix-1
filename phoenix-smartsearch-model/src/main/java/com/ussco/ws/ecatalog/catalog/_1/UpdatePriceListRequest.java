package com.ussco.ws.ecatalog.catalog._1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="priceListRequest" type="{http://ws.ussco.com/eCatalog/catalog/1}MDListRequestType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "priceListRequest" })
@XmlRootElement(name = "updatePriceListRequest")
public class UpdatePriceListRequest {

    @XmlElement(required = true)
    protected MDListRequestType priceListRequest;

    /**
     * Gets the value of the priceListRequest property.
     * 
     * @return
     *     possible object is
     *     {@link MDListRequestType }
     *     
     */
    public MDListRequestType getPriceListRequest() {
        return priceListRequest;
    }

    /**
     * Sets the value of the priceListRequest property.
     * 
     * @param value
     *     allowed object is
     *     {@link MDListRequestType }
     *     
     */
    public void setPriceListRequest(MDListRequestType value) {
        this.priceListRequest = value;
    }

}
