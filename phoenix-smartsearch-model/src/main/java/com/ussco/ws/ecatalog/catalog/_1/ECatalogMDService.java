package com.ussco.ws.ecatalog.catalog._1;

import java.net.MalformedURLException;
import java.net.URL;
import javax.xml.namespace.QName;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceFeature;
import javax.xml.ws.Service;
import org.apache.cxf.frontend.ClientProxy;
import org.apache.cxf.interceptor.LoggingInInterceptor;
import org.apache.cxf.interceptor.LoggingOutInterceptor;
import org.apache.cxf.jaxws.JaxWsProxyFactoryBean;
import org.apache.cxf.transport.http.HTTPConduit;
import org.apache.cxf.transports.http.configuration.HTTPClientPolicy;
import com.apd.smartsearch.model.utilities.SmartSearchPropertiesLoader;

/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.1.6 in JDK 6
 * Generated source version: 2.1
 * 
 */
@WebServiceClient(name = "eCatalogMDService", targetNamespace = "http://ws.ussco.com/eCatalog/catalog/1", wsdlLocation = "http://ws.ussco.com/eCatalog/catalog/1/eCatalog.wsdl")
public class ECatalogMDService extends Service {

    private final static URL ECATALOGMDSERVICE_WSDL_LOCATION;
    private static final String SHOULD_LOG_MESSAGES = SmartSearchPropertiesLoader.getInstance().getProperties()
            .getString("logTransactionContent", "false");
    private static final int CONNECTION_TIMEOUT = SmartSearchPropertiesLoader.getInstance().getProperties().getInt(
            "catalogConnectionTimeout", 36000);

    static {
        URL url = null;
        try {
            URL baseUrl;
            baseUrl = com.ussco.ws.ecatalog.catalog._1.ECatalogMDService.class.getResource(".");
            url = new URL(baseUrl, "http://ws.ussco.com/eCatalog/catalog/1/eCatalog.wsdl");
        }
        catch (MalformedURLException e) {
            java.util.logging.Logger.getLogger(ECatalogMDService.class.getName()).log(java.util.logging.Level.INFO,
                    "Can not initialize the default wsdl from {0}",
                    "http://ppd2-wsdl.ussco.com/eCatalog/catalog/1/eCatalog.wsdl");
        }
        ECATALOGMDSERVICE_WSDL_LOCATION = url;
    }

    public ECatalogMDService(URL wsdlLocation, QName serviceName) {
        super(wsdlLocation, serviceName);
    }

    public ECatalogMDService() {
        super(ECATALOGMDSERVICE_WSDL_LOCATION, new QName("http://ws.ussco.com/eCatalog/catalog/1", "eCatalogMDService"));
    }

    /**
     * 
     * @return
     *     returns ECatalogMDInterface
     */
    @WebEndpoint(name = "eCatalogMDServiceSOAP")
    public ECatalogMDInterface getECatalogMDServiceSOAP() {
        return super.getPort(new QName("http://ws.ussco.com/eCatalog/catalog/1", "eCatalogMDServiceSOAP"),
                ECatalogMDInterface.class);
    }

    /**
     * 
     * @param features
     *     A list of {@link javax.xml.ws.WebServiceFeature} to configure on the proxy.  Supported features not in the <code>features</code> parameter will have their default values.
     * @return
     *     returns ECatalogMDInterface
     */
    @WebEndpoint(name = "eCatalogMDServiceSOAP")
    public ECatalogMDInterface getECatalogMDServiceSOAP(WebServiceFeature... features) {
        return super.getPort(new QName("http://ws.ussco.com/eCatalog/catalog/1", "eCatalogMDServiceSOAP"),
                ECatalogMDInterface.class, features);
    }

    public ECatalogMDInterface getECatalogMDService(String serviceurl, String username, String password) {
        JaxWsProxyFactoryBean proxyFactory = new JaxWsProxyFactoryBean();

        if ("true".equals(SHOULD_LOG_MESSAGES)) {
            proxyFactory.getInInterceptors().add(new LoggingInInterceptor());
            proxyFactory.getOutInterceptors().add(new LoggingOutInterceptor());
        }

        proxyFactory.setServiceClass(ECatalogMDInterface.class);
        proxyFactory.setAddress(serviceurl);
        proxyFactory.setUsername(username);
        proxyFactory.setPassword(password);
        ECatalogMDInterface port = (ECatalogMDInterface) proxyFactory.create();
        HTTPConduit httpConduit = (HTTPConduit) ClientProxy.getClient(port).getConduit();
        HTTPClientPolicy httpClientPolicy = new HTTPClientPolicy();
        if (httpConduit.getClient() != null) {
            httpClientPolicy = httpConduit.getClient();
        }
        httpClientPolicy.setConnectionTimeout(CONNECTION_TIMEOUT);
        httpClientPolicy.setReceiveTimeout(CONNECTION_TIMEOUT);
        httpClientPolicy.setAllowChunking(false);
        httpConduit.setClient(httpClientPolicy);
        return port;
    }

}
