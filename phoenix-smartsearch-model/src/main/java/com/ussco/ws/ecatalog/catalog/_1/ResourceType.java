package com.ussco.ws.ecatalog.catalog._1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * A resource
 * 
 * <p>Java class for ResourceType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ResourceType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CampaignId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ResourceURL" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ResourceImage" type="{http://ws.ussco.com/eCatalog/catalog/1}ImageType" minOccurs="0"/>
 *         &lt;element name="ResourceTitle" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ResourceText" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ResourceType" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;enumeration value="UserGuide"/>
 *               &lt;enumeration value="ProductDemo"/>
 *               &lt;enumeration value="Generic"/>
 *               &lt;enumeration value="User Guides"/>
 *               &lt;enumeration value="Product Demos"/>
 *               &lt;enumeration value="Buying Guides"/>
 *               &lt;enumeration value="Videos"/>
 *               &lt;enumeration value="Articles"/>
 *               &lt;enumeration value="Showcases"/>
 *               &lt;enumeration value="Interactive Tools"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="Brand" type="{http://ws.ussco.com/eCatalog/catalog/1}BrandType" minOccurs="0"/>
 *         &lt;element name="Tags" type="{http://ws.ussco.com/eCatalog/catalog/1}TagType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ResourceType", propOrder = { "campaignId", "resourceURL", "resourceImage", "resourceTitle",
        "resourceText", "resourceType", "brand", "tags" })
public class ResourceType {

    @XmlElement(name = "CampaignId")
    protected String campaignId;
    @XmlElement(name = "ResourceURL", required = true)
    protected String resourceURL;
    @XmlElement(name = "ResourceImage")
    protected ImageType resourceImage;
    @XmlElement(name = "ResourceTitle")
    protected String resourceTitle;
    @XmlElement(name = "ResourceText")
    protected String resourceText;
    @XmlElement(name = "ResourceType")
    protected String resourceType;
    @XmlElement(name = "Brand")
    protected BrandType brand;
    @XmlElement(name = "Tags")
    protected List<TagType> tags;

    /**
     * Gets the value of the campaignId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCampaignId() {
        return campaignId;
    }

    /**
     * Sets the value of the campaignId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCampaignId(String value) {
        this.campaignId = value;
    }

    /**
     * Gets the value of the resourceURL property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResourceURL() {
        return resourceURL;
    }

    /**
     * Sets the value of the resourceURL property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResourceURL(String value) {
        this.resourceURL = value;
    }

    /**
     * Gets the value of the resourceImage property.
     * 
     * @return
     *     possible object is
     *     {@link ImageType }
     *     
     */
    public ImageType getResourceImage() {
        return resourceImage;
    }

    /**
     * Sets the value of the resourceImage property.
     * 
     * @param value
     *     allowed object is
     *     {@link ImageType }
     *     
     */
    public void setResourceImage(ImageType value) {
        this.resourceImage = value;
    }

    /**
     * Gets the value of the resourceTitle property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResourceTitle() {
        return resourceTitle;
    }

    /**
     * Sets the value of the resourceTitle property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResourceTitle(String value) {
        this.resourceTitle = value;
    }

    /**
     * Gets the value of the resourceText property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResourceText() {
        return resourceText;
    }

    /**
     * Sets the value of the resourceText property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResourceText(String value) {
        this.resourceText = value;
    }

    /**
     * Gets the value of the resourceType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResourceType() {
        return resourceType;
    }

    /**
     * Sets the value of the resourceType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResourceType(String value) {
        this.resourceType = value;
    }

    /**
     * Gets the value of the brand property.
     * 
     * @return
     *     possible object is
     *     {@link BrandType }
     *     
     */
    public BrandType getBrand() {
        return brand;
    }

    /**
     * Sets the value of the brand property.
     * 
     * @param value
     *     allowed object is
     *     {@link BrandType }
     *     
     */
    public void setBrand(BrandType value) {
        this.brand = value;
    }

    /**
     * Gets the value of the tags property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the tags property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTags().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TagType }
     * 
     * 
     */
    public List<TagType> getTags() {
        if (tags == null) {
            tags = new ArrayList<TagType>();
        }
        return this.tags;
    }

}
