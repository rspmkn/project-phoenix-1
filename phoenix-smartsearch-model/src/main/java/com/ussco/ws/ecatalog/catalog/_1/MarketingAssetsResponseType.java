package com.ussco.ws.ecatalog.catalog._1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Outline of a Marketing Asset
 * 
 * <p>Java class for MarketingAssetsResponseType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MarketingAssetsResponseType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ResponseStyle">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;enumeration value="ItemsPage"/>
 *               &lt;enumeration value="ZoneList"/>
 *               &lt;enumeration value="LandingPage"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;choice>
 *           &lt;element name="ItemPage" type="{http://ws.ussco.com/eCatalog/catalog/1}MerchItemPageResponseType"/>
 *           &lt;element name="LandingPage" type="{http://ws.ussco.com/eCatalog/catalog/1}LandingPageType"/>
 *           &lt;element name="ZoneList" type="{http://ws.ussco.com/eCatalog/catalog/1}MerchandisingZoneType" maxOccurs="unbounded"/>
 *         &lt;/choice>
 *         &lt;element name="AppliedFilters" type="{http://ws.ussco.com/eCatalog/catalog/1}FilterListType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MarketingAssetsResponseType", propOrder = { "responseStyle", "itemPage", "landingPage", "zoneList",
        "appliedFilters" })
public class MarketingAssetsResponseType {

    @XmlElement(name = "ResponseStyle", required = true)
    protected String responseStyle;
    @XmlElement(name = "ItemPage")
    protected MerchItemPageResponseType itemPage;
    @XmlElement(name = "LandingPage")
    protected LandingPageType landingPage;
    @XmlElement(name = "ZoneList")
    protected List<MerchandisingZoneType> zoneList;
    @XmlElement(name = "AppliedFilters")
    protected FilterListType appliedFilters;

    /**
     * Gets the value of the responseStyle property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResponseStyle() {
        return responseStyle;
    }

    /**
     * Sets the value of the responseStyle property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResponseStyle(String value) {
        this.responseStyle = value;
    }

    /**
     * Gets the value of the itemPage property.
     * 
     * @return
     *     possible object is
     *     {@link MerchItemPageResponseType }
     *     
     */
    public MerchItemPageResponseType getItemPage() {
        return itemPage;
    }

    /**
     * Sets the value of the itemPage property.
     * 
     * @param value
     *     allowed object is
     *     {@link MerchItemPageResponseType }
     *     
     */
    public void setItemPage(MerchItemPageResponseType value) {
        this.itemPage = value;
    }

    /**
     * Gets the value of the landingPage property.
     * 
     * @return
     *     possible object is
     *     {@link LandingPageType }
     *     
     */
    public LandingPageType getLandingPage() {
        return landingPage;
    }

    /**
     * Sets the value of the landingPage property.
     * 
     * @param value
     *     allowed object is
     *     {@link LandingPageType }
     *     
     */
    public void setLandingPage(LandingPageType value) {
        this.landingPage = value;
    }

    /**
     * Gets the value of the zoneList property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the zoneList property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getZoneList().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link MerchandisingZoneType }
     * 
     * 
     */
    public List<MerchandisingZoneType> getZoneList() {
        if (zoneList == null) {
            zoneList = new ArrayList<MerchandisingZoneType>();
        }
        return this.zoneList;
    }

    /**
     * Gets the value of the appliedFilters property.
     * 
     * @return
     *     possible object is
     *     {@link FilterListType }
     *     
     */
    public FilterListType getAppliedFilters() {
        return appliedFilters;
    }

    /**
     * Sets the value of the appliedFilters property.
     * 
     * @param value
     *     allowed object is
     *     {@link FilterListType }
     *     
     */
    public void setAppliedFilters(FilterListType value) {
        this.appliedFilters = value;
    }

}
