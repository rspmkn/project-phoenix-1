package com.ussco.ws.ecatalog.catalog._1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * A category entry in a merchandising zone
 * 
 * <p>Java class for MerchandisingZoneCategoryEntryType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MerchandisingZoneCategoryEntryType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://ws.ussco.com/eCatalog/catalog/1}MerchandisingZoneEntryType">
 *       &lt;sequence>
 *         &lt;element name="Category" type="{http://ws.ussco.com/eCatalog/catalog/1}CategoryType"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MerchandisingZoneCategoryEntryType", propOrder = { "category" })
public class MerchandisingZoneCategoryEntryType extends MerchandisingZoneEntryType {

    @XmlElement(name = "Category", required = true)
    protected CategoryType category;

    /**
     * Gets the value of the category property.
     * 
     * @return
     *     possible object is
     *     {@link CategoryType }
     *     
     */
    public CategoryType getCategory() {
        return category;
    }

    /**
     * Sets the value of the category property.
     * 
     * @param value
     *     allowed object is
     *     {@link CategoryType }
     *     
     */
    public void setCategory(CategoryType value) {
        this.category = value;
    }

}
