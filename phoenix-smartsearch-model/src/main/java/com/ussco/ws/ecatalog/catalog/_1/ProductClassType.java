package com.ussco.ws.ecatalog.catalog._1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * A Product Class
 * 
 * <p>Java class for ProductClassType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ProductClassType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ProductClassId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ProductClassDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ProductClassImage" type="{http://ws.ussco.com/eCatalog/catalog/1}ImageType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ProductClassType", propOrder = { "productClassId", "productClassDescription", "productClassImage" })
public class ProductClassType {

    @XmlElement(name = "ProductClassId")
    protected String productClassId;
    @XmlElement(name = "ProductClassDescription")
    protected String productClassDescription;
    @XmlElement(name = "ProductClassImage")
    protected List<ImageType> productClassImage;

    /**
     * Gets the value of the productClassId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProductClassId() {
        return productClassId;
    }

    /**
     * Sets the value of the productClassId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProductClassId(String value) {
        this.productClassId = value;
    }

    /**
     * Gets the value of the productClassDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProductClassDescription() {
        return productClassDescription;
    }

    /**
     * Sets the value of the productClassDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProductClassDescription(String value) {
        this.productClassDescription = value;
    }

    /**
     * Gets the value of the productClassImage property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the productClassImage property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getProductClassImage().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ImageType }
     * 
     * 
     */
    public List<ImageType> getProductClassImage() {
        if (productClassImage == null) {
            productClassImage = new ArrayList<ImageType>();
        }
        return this.productClassImage;
    }

}
