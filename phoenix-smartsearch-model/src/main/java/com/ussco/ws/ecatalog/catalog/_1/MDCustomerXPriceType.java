package com.ussco.ws.ecatalog.catalog._1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>Java class for MDCustomerXPriceType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MDCustomerXPriceType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ReturnStatus" type="{http://ws.ussco.com/eCatalog/catalog/1}StatusType" minOccurs="0"/>
 *         &lt;element name="CustomerNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="BasePriceList" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="OverridePriceList" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MDCustomerXPriceType", propOrder = { "returnStatus", "customerNumber", "basePriceList",
        "overridePriceList" })
public class MDCustomerXPriceType {

    @XmlElement(name = "ReturnStatus")
    protected StatusType returnStatus;
    @XmlElement(name = "CustomerNumber", required = true)
    protected String customerNumber;
    @XmlElement(name = "BasePriceList", required = true)
    protected String basePriceList;
    @XmlElement(name = "OverridePriceList")
    protected String overridePriceList;

    /**
     * Gets the value of the returnStatus property.
     * 
     * @return
     *     possible object is
     *     {@link StatusType }
     *     
     */
    public StatusType getReturnStatus() {
        return returnStatus;
    }

    /**
     * Sets the value of the returnStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link StatusType }
     *     
     */
    public void setReturnStatus(StatusType value) {
        this.returnStatus = value;
    }

    /**
     * Gets the value of the customerNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustomerNumber() {
        return customerNumber;
    }

    /**
     * Sets the value of the customerNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustomerNumber(String value) {
        this.customerNumber = value;
    }

    /**
     * Gets the value of the basePriceList property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBasePriceList() {
        return basePriceList;
    }

    /**
     * Sets the value of the basePriceList property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBasePriceList(String value) {
        this.basePriceList = value;
    }

    /**
     * Gets the value of the overridePriceList property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOverridePriceList() {
        return overridePriceList;
    }

    /**
     * Sets the value of the overridePriceList property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOverridePriceList(String value) {
        this.overridePriceList = value;
    }

}
