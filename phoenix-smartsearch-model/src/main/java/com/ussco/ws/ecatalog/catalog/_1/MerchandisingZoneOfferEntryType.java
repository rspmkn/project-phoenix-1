package com.ussco.ws.ecatalog.catalog._1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * An offer entry in a merchandising zone
 * 
 * <p>Java class for MerchandisingZoneOfferEntryType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MerchandisingZoneOfferEntryType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://ws.ussco.com/eCatalog/catalog/1}MerchandisingZoneEntryType">
 *       &lt;sequence>
 *         &lt;element name="Offer" type="{http://ws.ussco.com/eCatalog/catalog/1}OfferResponseType"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MerchandisingZoneOfferEntryType", propOrder = { "offer" })
public class MerchandisingZoneOfferEntryType extends MerchandisingZoneEntryType {

    @XmlElement(name = "Offer", required = true)
    protected OfferResponseType offer;

    /**
     * Gets the value of the offer property.
     * 
     * @return
     *     possible object is
     *     {@link OfferResponseType }
     *     
     */
    public OfferResponseType getOffer() {
        return offer;
    }

    /**
     * Sets the value of the offer property.
     * 
     * @param value
     *     allowed object is
     *     {@link OfferResponseType }
     *     
     */
    public void setOffer(OfferResponseType value) {
        this.offer = value;
    }

}
