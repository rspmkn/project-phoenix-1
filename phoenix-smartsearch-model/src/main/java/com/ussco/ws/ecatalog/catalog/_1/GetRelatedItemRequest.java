package com.ussco.ws.ecatalog.catalog._1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="requestHeader" type="{http://ws.ussco.com/eCatalog/catalog/1}RequestHeaderType"/>
 *         &lt;element name="relatedItemRequest" type="{http://ws.ussco.com/eCatalog/catalog/1}RelatedItemRequestType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "requestHeader", "relatedItemRequest" })
@XmlRootElement(name = "getRelatedItemRequest")
public class GetRelatedItemRequest {

    @XmlElement(required = true)
    protected RequestHeaderType requestHeader;
    @XmlElement(required = true)
    protected RelatedItemRequestType relatedItemRequest;

    /**
     * Gets the value of the requestHeader property.
     * 
     * @return
     *     possible object is
     *     {@link RequestHeaderType }
     *     
     */
    public RequestHeaderType getRequestHeader() {
        return requestHeader;
    }

    /**
     * Sets the value of the requestHeader property.
     * 
     * @param value
     *     allowed object is
     *     {@link RequestHeaderType }
     *     
     */
    public void setRequestHeader(RequestHeaderType value) {
        this.requestHeader = value;
    }

    /**
     * Gets the value of the relatedItemRequest property.
     * 
     * @return
     *     possible object is
     *     {@link RelatedItemRequestType }
     *     
     */
    public RelatedItemRequestType getRelatedItemRequest() {
        return relatedItemRequest;
    }

    /**
     * Sets the value of the relatedItemRequest property.
     * 
     * @param value
     *     allowed object is
     *     {@link RelatedItemRequestType }
     *     
     */
    public void setRelatedItemRequest(RelatedItemRequestType value) {
        this.relatedItemRequest = value;
    }

}
