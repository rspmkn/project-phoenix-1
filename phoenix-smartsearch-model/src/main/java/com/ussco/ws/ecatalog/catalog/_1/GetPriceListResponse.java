package com.ussco.ws.ecatalog.catalog._1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="priceListResponse" type="{http://ws.ussco.com/eCatalog/catalog/1}MDListResponseType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "priceListResponse" })
@XmlRootElement(name = "getPriceListResponse")
public class GetPriceListResponse {

    @XmlElement(required = true)
    protected MDListResponseType priceListResponse;

    /**
     * Gets the value of the priceListResponse property.
     * 
     * @return
     *     possible object is
     *     {@link MDListResponseType }
     *     
     */
    public MDListResponseType getPriceListResponse() {
        return priceListResponse;
    }

    /**
     * Sets the value of the priceListResponse property.
     * 
     * @param value
     *     allowed object is
     *     {@link MDListResponseType }
     *     
     */
    public void setPriceListResponse(MDListResponseType value) {
        this.priceListResponse = value;
    }

}
