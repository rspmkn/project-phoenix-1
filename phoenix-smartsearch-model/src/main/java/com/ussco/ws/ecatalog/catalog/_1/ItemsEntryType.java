package com.ussco.ws.ecatalog.catalog._1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * An list of items request
 * 
 * <p>Java class for ItemsEntryType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ItemsEntryType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ItemReference" type="{http://ws.ussco.com/eCatalog/catalog/1}ItemCrossReferenceType"/>
 *         &lt;element name="Item" type="{http://ws.ussco.com/eCatalog/catalog/1}ItemType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="ResultStatus" type="{http://ws.ussco.com/eCatalog/catalog/1}StatusType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ItemsEntryType", propOrder = { "itemReference", "item", "resultStatus" })
public class ItemsEntryType {

    @XmlElement(name = "ItemReference", required = true)
    protected ItemCrossReferenceType itemReference;
    @XmlElement(name = "Item")
    protected List<ItemType> item;
    @XmlElement(name = "ResultStatus")
    protected StatusType resultStatus;

    /**
     * Gets the value of the itemReference property.
     * 
     * @return
     *     possible object is
     *     {@link ItemCrossReferenceType }
     *     
     */
    public ItemCrossReferenceType getItemReference() {
        return itemReference;
    }

    /**
     * Sets the value of the itemReference property.
     * 
     * @param value
     *     allowed object is
     *     {@link ItemCrossReferenceType }
     *     
     */
    public void setItemReference(ItemCrossReferenceType value) {
        this.itemReference = value;
    }

    /**
     * Gets the value of the item property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the item property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getItem().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ItemType }
     * 
     * 
     */
    public List<ItemType> getItem() {
        if (item == null) {
            item = new ArrayList<ItemType>();
        }
        return this.item;
    }

    /**
     * Gets the value of the resultStatus property.
     * 
     * @return
     *     possible object is
     *     {@link StatusType }
     *     
     */
    public StatusType getResultStatus() {
        return resultStatus;
    }

    /**
     * Sets the value of the resultStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link StatusType }
     *     
     */
    public void setResultStatus(StatusType value) {
        this.resultStatus = value;
    }

}
