package com.ussco.ws.ecatalog.catalog._1;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * A list of items along with paging information
 * 
 * <p>Java class for ItemResponseType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ItemResponseType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="FirstResult" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *         &lt;element name="ResultsReturned" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *         &lt;element name="TotalResults" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *         &lt;element name="Items" type="{http://ws.ussco.com/eCatalog/catalog/1}ItemResponseListType"/>
 *         &lt;element name="MerchandisingZone" type="{http://ws.ussco.com/eCatalog/catalog/1}MerchandisingZoneType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ItemResponseType", propOrder = { "firstResult", "resultsReturned", "totalResults", "items",
        "merchandisingZone" })
public class ItemResponseType {

    @XmlElement(name = "FirstResult", required = true)
    protected BigInteger firstResult;
    @XmlElement(name = "ResultsReturned", required = true)
    protected BigInteger resultsReturned;
    @XmlElement(name = "TotalResults", required = true)
    protected BigInteger totalResults;
    @XmlElement(name = "Items", required = true)
    protected ItemResponseListType items;
    @XmlElement(name = "MerchandisingZone")
    protected List<MerchandisingZoneType> merchandisingZone;

    /**
     * Gets the value of the firstResult property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getFirstResult() {
        return firstResult;
    }

    /**
     * Sets the value of the firstResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setFirstResult(BigInteger value) {
        this.firstResult = value;
    }

    /**
     * Gets the value of the resultsReturned property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getResultsReturned() {
        return resultsReturned;
    }

    /**
     * Sets the value of the resultsReturned property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setResultsReturned(BigInteger value) {
        this.resultsReturned = value;
    }

    /**
     * Gets the value of the totalResults property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getTotalResults() {
        return totalResults;
    }

    /**
     * Sets the value of the totalResults property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setTotalResults(BigInteger value) {
        this.totalResults = value;
    }

    /**
     * Gets the value of the items property.
     * 
     * @return
     *     possible object is
     *     {@link ItemResponseListType }
     *     
     */
    public ItemResponseListType getItems() {
        return items;
    }

    /**
     * Sets the value of the items property.
     * 
     * @param value
     *     allowed object is
     *     {@link ItemResponseListType }
     *     
     */
    public void setItems(ItemResponseListType value) {
        this.items = value;
    }

    /**
     * Gets the value of the merchandisingZone property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the merchandisingZone property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMerchandisingZone().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link MerchandisingZoneType }
     * 
     * 
     */
    public List<MerchandisingZoneType> getMerchandisingZone() {
        if (merchandisingZone == null) {
            merchandisingZone = new ArrayList<MerchandisingZoneType>();
        }
        return this.merchandisingZone;
    }

}
