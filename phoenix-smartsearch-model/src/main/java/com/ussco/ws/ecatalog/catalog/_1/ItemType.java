package com.ussco.ws.ecatalog.catalog._1;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;

/**
 * An item
 * 
 * <p>Java class for ItemType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ItemType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CampaignId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Sequence" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *         &lt;element name="ItemNumber" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="50"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="OldItemNumber" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="50"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="DealerItemNumber" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="50"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="Description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DealerDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="UnitOfMeasure" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DealerUnitOfMeasure" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Price" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="ListPrice" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="ReferencePrice" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="DealerPrice" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Weight" minOccurs="0">
 *           &lt;complexType>
 *             &lt;simpleContent>
 *               &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>double">
 *                 &lt;attribute name="unitCode" type="{http://www.w3.org/2001/XMLSchema}string" />
 *               &lt;/extension>
 *             &lt;/simpleContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="DealerWeight" minOccurs="0">
 *           &lt;complexType>
 *             &lt;simpleContent>
 *               &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>double">
 *                 &lt;attribute name="unitCode" type="{http://www.w3.org/2001/XMLSchema}string" />
 *               &lt;/extension>
 *             &lt;/simpleContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="BoxWeight" minOccurs="0">
 *           &lt;complexType>
 *             &lt;simpleContent>
 *               &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>double">
 *                 &lt;attribute name="unitCode" type="{http://www.w3.org/2001/XMLSchema}string" />
 *               &lt;/extension>
 *             &lt;/simpleContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="BoxHeight" minOccurs="0">
 *           &lt;complexType>
 *             &lt;simpleContent>
 *               &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>double">
 *                 &lt;attribute name="unitCode" type="{http://www.w3.org/2001/XMLSchema}string" />
 *               &lt;/extension>
 *             &lt;/simpleContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="BoxLength" minOccurs="0">
 *           &lt;complexType>
 *             &lt;simpleContent>
 *               &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>double">
 *                 &lt;attribute name="unitCode" type="{http://www.w3.org/2001/XMLSchema}string" />
 *               &lt;/extension>
 *             &lt;/simpleContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="BoxWidth" minOccurs="0">
 *           &lt;complexType>
 *             &lt;simpleContent>
 *               &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>double">
 *                 &lt;attribute name="unitCode" type="{http://www.w3.org/2001/XMLSchema}string" />
 *               &lt;/extension>
 *             &lt;/simpleContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="CartonHeight" minOccurs="0">
 *           &lt;complexType>
 *             &lt;simpleContent>
 *               &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>double">
 *                 &lt;attribute name="unitCode" type="{http://www.w3.org/2001/XMLSchema}string" />
 *               &lt;/extension>
 *             &lt;/simpleContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="CartonLength" minOccurs="0">
 *           &lt;complexType>
 *             &lt;simpleContent>
 *               &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>double">
 *                 &lt;attribute name="unitCode" type="{http://www.w3.org/2001/XMLSchema}string" />
 *               &lt;/extension>
 *             &lt;/simpleContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="CartonWidth" minOccurs="0">
 *           &lt;complexType>
 *             &lt;simpleContent>
 *               &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>double">
 *                 &lt;attribute name="unitCode" type="{http://www.w3.org/2001/XMLSchema}string" />
 *               &lt;/extension>
 *             &lt;/simpleContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="ItemHeight" minOccurs="0">
 *           &lt;complexType>
 *             &lt;simpleContent>
 *               &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>double">
 *                 &lt;attribute name="unitCode" type="{http://www.w3.org/2001/XMLSchema}string" />
 *               &lt;/extension>
 *             &lt;/simpleContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="ItemLength" minOccurs="0">
 *           &lt;complexType>
 *             &lt;simpleContent>
 *               &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>double">
 *                 &lt;attribute name="unitCode" type="{http://www.w3.org/2001/XMLSchema}string" />
 *               &lt;/extension>
 *             &lt;/simpleContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="ItemWidth" minOccurs="0">
 *           &lt;complexType>
 *             &lt;simpleContent>
 *               &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>double">
 *                 &lt;attribute name="unitCode" type="{http://www.w3.org/2001/XMLSchema}string" />
 *               &lt;/extension>
 *             &lt;/simpleContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="ManufacturerPartNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Brand" type="{http://ws.ussco.com/eCatalog/catalog/1}BrandType" minOccurs="0"/>
 *         &lt;element name="Manufacturer" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ItemImage" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;extension base="{http://ws.ussco.com/eCatalog/catalog/1}ImageType">
 *                 &lt;attribute name="sequence" type="{http://www.w3.org/2001/XMLSchema}integer" />
 *               &lt;/extension>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="ItemIndicator" type="{http://ws.ussco.com/eCatalog/catalog/1}ItemIndicatorType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="MSDSURL" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SellingPoint" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;simpleContent>
 *               &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>string">
 *                 &lt;attribute name="sequence" type="{http://www.w3.org/2001/XMLSchema}integer" />
 *               &lt;/extension>
 *             &lt;/simpleContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="WarrantyText" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ExtendedWarrantyText" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="GreenInformation" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PackageIncludesText" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SellingCopy" maxOccurs="3" minOccurs="0">
 *           &lt;complexType>
 *             &lt;simpleContent>
 *               &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>string">
 *                 &lt;attribute name="style">
 *                   &lt;simpleType>
 *                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                       &lt;enumeration value="Brief"/>
 *                       &lt;enumeration value="Standard"/>
 *                       &lt;enumeration value="Long"/>
 *                     &lt;/restriction>
 *                   &lt;/simpleType>
 *                 &lt;/attribute>
 *               &lt;/extension>
 *             &lt;/simpleContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="SummarySellingStatement" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Category" type="{http://ws.ussco.com/eCatalog/catalog/1}CategoryType" minOccurs="0"/>
 *         &lt;element name="Attribute" type="{http://ws.ussco.com/eCatalog/catalog/1}AttributeType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="ItemRelation" type="{http://ws.ussco.com/eCatalog/catalog/1}RelatedItemResponseType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="MerchandisingZone" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;extension base="{http://ws.ussco.com/eCatalog/catalog/1}MerchandisingZoneType">
 *                 &lt;attribute name="sequence" type="{http://www.w3.org/2001/XMLSchema}integer" />
 *               &lt;/extension>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="CountryOfOrigin" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CartonWeight" minOccurs="0">
 *           &lt;complexType>
 *             &lt;simpleContent>
 *               &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>double">
 *                 &lt;attribute name="unitCode" type="{http://www.w3.org/2001/XMLSchema}string" />
 *               &lt;/extension>
 *             &lt;/simpleContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="CartonPackQuantity" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *         &lt;element name="CartonPackUnit" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BoxPackQuantity" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *         &lt;element name="BoxPackUnit" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PackQuantity" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *         &lt;element name="PackUnit" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ForModelNumbers" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Offer" type="{http://ws.ussco.com/eCatalog/catalog/1}OfferResponseType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="UPC" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="UPCCarton" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DealerUPC" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="UNSPSC" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ShipClass" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DiscontinuedItem" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ActiveIndicator" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;enumeration value="Y"/>
 *               &lt;enumeration value="N"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="SuggestedItem" type="{http://ws.ussco.com/eCatalog/catalog/1}ItemType" minOccurs="0"/>
 *         &lt;element name="MPN" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="70"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="CatalogPageNumber" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="20"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="ResellerCrossReference" type="{http://ws.ussco.com/eCatalog/catalog/1}ResellerCrossReference" minOccurs="0"/>
 *         &lt;element name="ProductClass" type="{http://ws.ussco.com/eCatalog/catalog/1}ProductClassType" minOccurs="0"/>
 *         &lt;element name="Tags" type="{http://ws.ussco.com/eCatalog/catalog/1}TagType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="MerchPreference" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;enumeration value="SL"/>
 *               &lt;enumeration value="BO"/>
 *               &lt;enumeration value="BU"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="StockingDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="StockingIndicator" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="StateRestriction" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ItemType", propOrder = { "campaignId", "sequence", "itemNumber", "oldItemNumber", "dealerItemNumber",
        "description", "dealerDescription", "unitOfMeasure", "dealerUnitOfMeasure", "price", "listPrice",
        "referencePrice", "dealerPrice", "weight", "dealerWeight", "boxWeight", "boxHeight", "boxLength", "boxWidth",
        "cartonHeight", "cartonLength", "cartonWidth", "itemHeight", "itemLength", "itemWidth",
        "manufacturerPartNumber", "brand", "manufacturer", "itemImage", "itemIndicator", "msdsurl", "sellingPoint",
        "warrantyText", "extendedWarrantyText", "greenInformation", "packageIncludesText", "sellingCopy",
        "summarySellingStatement", "category", "attribute", "itemRelation", "merchandisingZone", "countryOfOrigin",
        "cartonWeight", "cartonPackQuantity", "cartonPackUnit", "boxPackQuantity", "boxPackUnit", "packQuantity",
        "packUnit", "forModelNumbers", "offer", "upc", "upcCarton", "dealerUPC", "unspsc", "shipClass",
        "discontinuedItem", "activeIndicator", "suggestedItem", "mpn", "catalogPageNumber", "resellerCrossReference",
        "productClass", "tags", "merchPreference", "stockingDescription", "stockingIndicator", "stateRestriction" })
@XmlSeeAlso( { RelatedItemResponseType.RelatedItem.class })
public class ItemType {

    @XmlElement(name = "CampaignId")
    protected String campaignId;
    @XmlElement(name = "Sequence")
    protected BigInteger sequence;
    @XmlElement(name = "ItemNumber")
    protected String itemNumber;
    @XmlElement(name = "OldItemNumber")
    protected String oldItemNumber;
    @XmlElement(name = "DealerItemNumber")
    protected String dealerItemNumber;
    @XmlElement(name = "Description")
    protected String description;
    @XmlElement(name = "DealerDescription")
    protected String dealerDescription;
    @XmlElement(name = "UnitOfMeasure")
    protected String unitOfMeasure;
    @XmlElement(name = "DealerUnitOfMeasure")
    protected String dealerUnitOfMeasure;
    @XmlElement(name = "Price")
    protected Double price;
    @XmlElement(name = "ListPrice")
    protected Double listPrice;
    @XmlElement(name = "ReferencePrice")
    protected Double referencePrice;
    @XmlElement(name = "DealerPrice")
    protected String dealerPrice;
    @XmlElement(name = "Weight")
    protected ItemType.Weight weight;
    @XmlElement(name = "DealerWeight")
    protected ItemType.DealerWeight dealerWeight;
    @XmlElement(name = "BoxWeight")
    protected ItemType.BoxWeight boxWeight;
    @XmlElement(name = "BoxHeight")
    protected ItemType.BoxHeight boxHeight;
    @XmlElement(name = "BoxLength")
    protected ItemType.BoxLength boxLength;
    @XmlElement(name = "BoxWidth")
    protected ItemType.BoxWidth boxWidth;
    @XmlElement(name = "CartonHeight")
    protected ItemType.CartonHeight cartonHeight;
    @XmlElement(name = "CartonLength")
    protected ItemType.CartonLength cartonLength;
    @XmlElement(name = "CartonWidth")
    protected ItemType.CartonWidth cartonWidth;
    @XmlElement(name = "ItemHeight")
    protected ItemType.ItemHeight itemHeight;
    @XmlElement(name = "ItemLength")
    protected ItemType.ItemLength itemLength;
    @XmlElement(name = "ItemWidth")
    protected ItemType.ItemWidth itemWidth;
    @XmlElement(name = "ManufacturerPartNumber")
    protected String manufacturerPartNumber;
    @XmlElement(name = "Brand")
    protected BrandType brand;
    @XmlElement(name = "Manufacturer")
    protected String manufacturer;
    @XmlElement(name = "ItemImage")
    protected List<ItemType.ItemImage> itemImage;
    @XmlElement(name = "ItemIndicator")
    protected List<ItemIndicatorType> itemIndicator;
    @XmlElement(name = "MSDSURL")
    protected String msdsurl;
    @XmlElement(name = "SellingPoint")
    protected List<ItemType.SellingPoint> sellingPoint;
    @XmlElement(name = "WarrantyText")
    protected String warrantyText;
    @XmlElement(name = "ExtendedWarrantyText")
    protected String extendedWarrantyText;
    @XmlElement(name = "GreenInformation")
    protected String greenInformation;
    @XmlElement(name = "PackageIncludesText")
    protected String packageIncludesText;
    @XmlElement(name = "SellingCopy")
    protected List<ItemType.SellingCopy> sellingCopy;
    @XmlElement(name = "SummarySellingStatement")
    protected String summarySellingStatement;
    @XmlElement(name = "Category")
    protected CategoryType category;
    @XmlElement(name = "Attribute")
    protected List<AttributeType> attribute;
    @XmlElement(name = "ItemRelation")
    protected List<RelatedItemResponseType> itemRelation;
    @XmlElement(name = "MerchandisingZone")
    protected List<ItemType.MerchandisingZone> merchandisingZone;
    @XmlElement(name = "CountryOfOrigin")
    protected String countryOfOrigin;
    @XmlElement(name = "CartonWeight")
    protected ItemType.CartonWeight cartonWeight;
    @XmlElement(name = "CartonPackQuantity")
    protected BigInteger cartonPackQuantity;
    @XmlElement(name = "CartonPackUnit")
    protected String cartonPackUnit;
    @XmlElement(name = "BoxPackQuantity")
    protected BigInteger boxPackQuantity;
    @XmlElement(name = "BoxPackUnit")
    protected String boxPackUnit;
    @XmlElement(name = "PackQuantity")
    protected BigInteger packQuantity;
    @XmlElement(name = "PackUnit")
    protected String packUnit;
    @XmlElement(name = "ForModelNumbers")
    protected String forModelNumbers;
    @XmlElement(name = "Offer")
    protected List<OfferResponseType> offer;
    @XmlElement(name = "UPC")
    protected String upc;
    @XmlElement(name = "UPCCarton")
    protected String upcCarton;
    @XmlElement(name = "DealerUPC")
    protected String dealerUPC;
    @XmlElement(name = "UNSPSC")
    protected String unspsc;
    @XmlElement(name = "ShipClass")
    protected String shipClass;
    @XmlElement(name = "DiscontinuedItem")
    protected String discontinuedItem;
    @XmlElement(name = "ActiveIndicator")
    protected String activeIndicator;
    @XmlElement(name = "SuggestedItem")
    protected ItemType suggestedItem;
    @XmlElement(name = "MPN")
    protected String mpn;
    @XmlElement(name = "CatalogPageNumber")
    protected String catalogPageNumber;
    @XmlElement(name = "ResellerCrossReference")
    protected ResellerCrossReference resellerCrossReference;
    @XmlElement(name = "ProductClass")
    protected ProductClassType productClass;
    @XmlElement(name = "Tags")
    protected List<TagType> tags;
    @XmlElement(name = "MerchPreference")
    protected String merchPreference;
    @XmlElement(name = "StockingDescription")
    protected String stockingDescription;
    @XmlElement(name = "StockingIndicator")
    protected String stockingIndicator;
    @XmlElement(name = "StateRestriction")
    protected String stateRestriction;

    /**
     * Gets the value of the campaignId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCampaignId() {
        return campaignId;
    }

    /**
     * Sets the value of the campaignId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCampaignId(String value) {
        this.campaignId = value;
    }

    /**
     * Gets the value of the sequence property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getSequence() {
        return sequence;
    }

    /**
     * Sets the value of the sequence property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setSequence(BigInteger value) {
        this.sequence = value;
    }

    /**
     * Gets the value of the itemNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getItemNumber() {
        return itemNumber;
    }

    /**
     * Sets the value of the itemNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setItemNumber(String value) {
        this.itemNumber = value;
    }

    /**
     * Gets the value of the oldItemNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOldItemNumber() {
        return oldItemNumber;
    }

    /**
     * Sets the value of the oldItemNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOldItemNumber(String value) {
        this.oldItemNumber = value;
    }

    /**
     * Gets the value of the dealerItemNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDealerItemNumber() {
        return dealerItemNumber;
    }

    /**
     * Sets the value of the dealerItemNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDealerItemNumber(String value) {
        this.dealerItemNumber = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the dealerDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDealerDescription() {
        return dealerDescription;
    }

    /**
     * Sets the value of the dealerDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDealerDescription(String value) {
        this.dealerDescription = value;
    }

    /**
     * Gets the value of the unitOfMeasure property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUnitOfMeasure() {
        return unitOfMeasure;
    }

    /**
     * Sets the value of the unitOfMeasure property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUnitOfMeasure(String value) {
        this.unitOfMeasure = value;
    }

    /**
     * Gets the value of the dealerUnitOfMeasure property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDealerUnitOfMeasure() {
        return dealerUnitOfMeasure;
    }

    /**
     * Sets the value of the dealerUnitOfMeasure property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDealerUnitOfMeasure(String value) {
        this.dealerUnitOfMeasure = value;
    }

    /**
     * Gets the value of the price property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getPrice() {
        return price;
    }

    /**
     * Sets the value of the price property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setPrice(Double value) {
        this.price = value;
    }

    /**
     * Gets the value of the listPrice property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getListPrice() {
        return listPrice;
    }

    /**
     * Sets the value of the listPrice property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setListPrice(Double value) {
        this.listPrice = value;
    }

    /**
     * Gets the value of the referencePrice property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getReferencePrice() {
        return referencePrice;
    }

    /**
     * Sets the value of the referencePrice property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setReferencePrice(Double value) {
        this.referencePrice = value;
    }

    /**
     * Gets the value of the dealerPrice property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDealerPrice() {
        return dealerPrice;
    }

    /**
     * Sets the value of the dealerPrice property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDealerPrice(String value) {
        this.dealerPrice = value;
    }

    /**
     * Gets the value of the weight property.
     * 
     * @return
     *     possible object is
     *     {@link ItemType.Weight }
     *     
     */
    public ItemType.Weight getWeight() {
        return weight;
    }

    /**
     * Sets the value of the weight property.
     * 
     * @param value
     *     allowed object is
     *     {@link ItemType.Weight }
     *     
     */
    public void setWeight(ItemType.Weight value) {
        this.weight = value;
    }

    /**
     * Gets the value of the dealerWeight property.
     * 
     * @return
     *     possible object is
     *     {@link ItemType.DealerWeight }
     *     
     */
    public ItemType.DealerWeight getDealerWeight() {
        return dealerWeight;
    }

    /**
     * Sets the value of the dealerWeight property.
     * 
     * @param value
     *     allowed object is
     *     {@link ItemType.DealerWeight }
     *     
     */
    public void setDealerWeight(ItemType.DealerWeight value) {
        this.dealerWeight = value;
    }

    /**
     * Gets the value of the boxWeight property.
     * 
     * @return
     *     possible object is
     *     {@link ItemType.BoxWeight }
     *     
     */
    public ItemType.BoxWeight getBoxWeight() {
        return boxWeight;
    }

    /**
     * Sets the value of the boxWeight property.
     * 
     * @param value
     *     allowed object is
     *     {@link ItemType.BoxWeight }
     *     
     */
    public void setBoxWeight(ItemType.BoxWeight value) {
        this.boxWeight = value;
    }

    /**
     * Gets the value of the boxHeight property.
     * 
     * @return
     *     possible object is
     *     {@link ItemType.BoxHeight }
     *     
     */
    public ItemType.BoxHeight getBoxHeight() {
        return boxHeight;
    }

    /**
     * Sets the value of the boxHeight property.
     * 
     * @param value
     *     allowed object is
     *     {@link ItemType.BoxHeight }
     *     
     */
    public void setBoxHeight(ItemType.BoxHeight value) {
        this.boxHeight = value;
    }

    /**
     * Gets the value of the boxLength property.
     * 
     * @return
     *     possible object is
     *     {@link ItemType.BoxLength }
     *     
     */
    public ItemType.BoxLength getBoxLength() {
        return boxLength;
    }

    /**
     * Sets the value of the boxLength property.
     * 
     * @param value
     *     allowed object is
     *     {@link ItemType.BoxLength }
     *     
     */
    public void setBoxLength(ItemType.BoxLength value) {
        this.boxLength = value;
    }

    /**
     * Gets the value of the boxWidth property.
     * 
     * @return
     *     possible object is
     *     {@link ItemType.BoxWidth }
     *     
     */
    public ItemType.BoxWidth getBoxWidth() {
        return boxWidth;
    }

    /**
     * Sets the value of the boxWidth property.
     * 
     * @param value
     *     allowed object is
     *     {@link ItemType.BoxWidth }
     *     
     */
    public void setBoxWidth(ItemType.BoxWidth value) {
        this.boxWidth = value;
    }

    /**
     * Gets the value of the cartonHeight property.
     * 
     * @return
     *     possible object is
     *     {@link ItemType.CartonHeight }
     *     
     */
    public ItemType.CartonHeight getCartonHeight() {
        return cartonHeight;
    }

    /**
     * Sets the value of the cartonHeight property.
     * 
     * @param value
     *     allowed object is
     *     {@link ItemType.CartonHeight }
     *     
     */
    public void setCartonHeight(ItemType.CartonHeight value) {
        this.cartonHeight = value;
    }

    /**
     * Gets the value of the cartonLength property.
     * 
     * @return
     *     possible object is
     *     {@link ItemType.CartonLength }
     *     
     */
    public ItemType.CartonLength getCartonLength() {
        return cartonLength;
    }

    /**
     * Sets the value of the cartonLength property.
     * 
     * @param value
     *     allowed object is
     *     {@link ItemType.CartonLength }
     *     
     */
    public void setCartonLength(ItemType.CartonLength value) {
        this.cartonLength = value;
    }

    /**
     * Gets the value of the cartonWidth property.
     * 
     * @return
     *     possible object is
     *     {@link ItemType.CartonWidth }
     *     
     */
    public ItemType.CartonWidth getCartonWidth() {
        return cartonWidth;
    }

    /**
     * Sets the value of the cartonWidth property.
     * 
     * @param value
     *     allowed object is
     *     {@link ItemType.CartonWidth }
     *     
     */
    public void setCartonWidth(ItemType.CartonWidth value) {
        this.cartonWidth = value;
    }

    /**
     * Gets the value of the itemHeight property.
     * 
     * @return
     *     possible object is
     *     {@link ItemType.ItemHeight }
     *     
     */
    public ItemType.ItemHeight getItemHeight() {
        return itemHeight;
    }

    /**
     * Sets the value of the itemHeight property.
     * 
     * @param value
     *     allowed object is
     *     {@link ItemType.ItemHeight }
     *     
     */
    public void setItemHeight(ItemType.ItemHeight value) {
        this.itemHeight = value;
    }

    /**
     * Gets the value of the itemLength property.
     * 
     * @return
     *     possible object is
     *     {@link ItemType.ItemLength }
     *     
     */
    public ItemType.ItemLength getItemLength() {
        return itemLength;
    }

    /**
     * Sets the value of the itemLength property.
     * 
     * @param value
     *     allowed object is
     *     {@link ItemType.ItemLength }
     *     
     */
    public void setItemLength(ItemType.ItemLength value) {
        this.itemLength = value;
    }

    /**
     * Gets the value of the itemWidth property.
     * 
     * @return
     *     possible object is
     *     {@link ItemType.ItemWidth }
     *     
     */
    public ItemType.ItemWidth getItemWidth() {
        return itemWidth;
    }

    /**
     * Sets the value of the itemWidth property.
     * 
     * @param value
     *     allowed object is
     *     {@link ItemType.ItemWidth }
     *     
     */
    public void setItemWidth(ItemType.ItemWidth value) {
        this.itemWidth = value;
    }

    /**
     * Gets the value of the manufacturerPartNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getManufacturerPartNumber() {
        return manufacturerPartNumber;
    }

    /**
     * Sets the value of the manufacturerPartNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setManufacturerPartNumber(String value) {
        this.manufacturerPartNumber = value;
    }

    /**
     * Gets the value of the brand property.
     * 
     * @return
     *     possible object is
     *     {@link BrandType }
     *     
     */
    public BrandType getBrand() {
        return brand;
    }

    /**
     * Sets the value of the brand property.
     * 
     * @param value
     *     allowed object is
     *     {@link BrandType }
     *     
     */
    public void setBrand(BrandType value) {
        this.brand = value;
    }

    /**
     * Gets the value of the manufacturer property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getManufacturer() {
        return manufacturer;
    }

    /**
     * Sets the value of the manufacturer property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setManufacturer(String value) {
        this.manufacturer = value;
    }

    /**
     * Gets the value of the itemImage property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the itemImage property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getItemImage().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ItemType.ItemImage }
     * 
     * 
     */
    public List<ItemType.ItemImage> getItemImage() {
        if (itemImage == null) {
            itemImage = new ArrayList<ItemType.ItemImage>();
        }
        return this.itemImage;
    }

    /**
     * Gets the value of the itemIndicator property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the itemIndicator property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getItemIndicator().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ItemIndicatorType }
     * 
     * 
     */
    public List<ItemIndicatorType> getItemIndicator() {
        if (itemIndicator == null) {
            itemIndicator = new ArrayList<ItemIndicatorType>();
        }
        return this.itemIndicator;
    }

    /**
     * Gets the value of the msdsurl property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMSDSURL() {
        return msdsurl;
    }

    /**
     * Sets the value of the msdsurl property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMSDSURL(String value) {
        this.msdsurl = value;
    }

    /**
     * Gets the value of the sellingPoint property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the sellingPoint property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSellingPoint().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ItemType.SellingPoint }
     * 
     * 
     */
    public List<ItemType.SellingPoint> getSellingPoint() {
        if (sellingPoint == null) {
            sellingPoint = new ArrayList<ItemType.SellingPoint>();
        }
        return this.sellingPoint;
    }

    /**
     * Gets the value of the warrantyText property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWarrantyText() {
        return warrantyText;
    }

    /**
     * Sets the value of the warrantyText property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWarrantyText(String value) {
        this.warrantyText = value;
    }

    /**
     * Gets the value of the extendedWarrantyText property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExtendedWarrantyText() {
        return extendedWarrantyText;
    }

    /**
     * Sets the value of the extendedWarrantyText property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExtendedWarrantyText(String value) {
        this.extendedWarrantyText = value;
    }

    /**
     * Gets the value of the greenInformation property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGreenInformation() {
        return greenInformation;
    }

    /**
     * Sets the value of the greenInformation property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGreenInformation(String value) {
        this.greenInformation = value;
    }

    /**
     * Gets the value of the packageIncludesText property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPackageIncludesText() {
        return packageIncludesText;
    }

    /**
     * Sets the value of the packageIncludesText property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPackageIncludesText(String value) {
        this.packageIncludesText = value;
    }

    /**
     * Gets the value of the sellingCopy property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the sellingCopy property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSellingCopy().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ItemType.SellingCopy }
     * 
     * 
     */
    public List<ItemType.SellingCopy> getSellingCopy() {
        if (sellingCopy == null) {
            sellingCopy = new ArrayList<ItemType.SellingCopy>();
        }
        return this.sellingCopy;
    }

    /**
     * Gets the value of the summarySellingStatement property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSummarySellingStatement() {
        return summarySellingStatement;
    }

    /**
     * Sets the value of the summarySellingStatement property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSummarySellingStatement(String value) {
        this.summarySellingStatement = value;
    }

    /**
     * Gets the value of the category property.
     * 
     * @return
     *     possible object is
     *     {@link CategoryType }
     *     
     */
    public CategoryType getCategory() {
        return category;
    }

    /**
     * Sets the value of the category property.
     * 
     * @param value
     *     allowed object is
     *     {@link CategoryType }
     *     
     */
    public void setCategory(CategoryType value) {
        this.category = value;
    }

    /**
     * Gets the value of the attribute property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the attribute property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAttribute().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AttributeType }
     * 
     * 
     */
    public List<AttributeType> getAttribute() {
        if (attribute == null) {
            attribute = new ArrayList<AttributeType>();
        }
        return this.attribute;
    }

    /**
     * Gets the value of the itemRelation property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the itemRelation property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getItemRelation().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RelatedItemResponseType }
     * 
     * 
     */
    public List<RelatedItemResponseType> getItemRelation() {
        if (itemRelation == null) {
            itemRelation = new ArrayList<RelatedItemResponseType>();
        }
        return this.itemRelation;
    }

    /**
     * Gets the value of the merchandisingZone property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the merchandisingZone property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMerchandisingZone().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ItemType.MerchandisingZone }
     * 
     * 
     */
    public List<ItemType.MerchandisingZone> getMerchandisingZone() {
        if (merchandisingZone == null) {
            merchandisingZone = new ArrayList<ItemType.MerchandisingZone>();
        }
        return this.merchandisingZone;
    }

    /**
     * Gets the value of the countryOfOrigin property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCountryOfOrigin() {
        return countryOfOrigin;
    }

    /**
     * Sets the value of the countryOfOrigin property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCountryOfOrigin(String value) {
        this.countryOfOrigin = value;
    }

    /**
     * Gets the value of the cartonWeight property.
     * 
     * @return
     *     possible object is
     *     {@link ItemType.CartonWeight }
     *     
     */
    public ItemType.CartonWeight getCartonWeight() {
        return cartonWeight;
    }

    /**
     * Sets the value of the cartonWeight property.
     * 
     * @param value
     *     allowed object is
     *     {@link ItemType.CartonWeight }
     *     
     */
    public void setCartonWeight(ItemType.CartonWeight value) {
        this.cartonWeight = value;
    }

    /**
     * Gets the value of the cartonPackQuantity property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getCartonPackQuantity() {
        return cartonPackQuantity;
    }

    /**
     * Sets the value of the cartonPackQuantity property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setCartonPackQuantity(BigInteger value) {
        this.cartonPackQuantity = value;
    }

    /**
     * Gets the value of the cartonPackUnit property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCartonPackUnit() {
        return cartonPackUnit;
    }

    /**
     * Sets the value of the cartonPackUnit property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCartonPackUnit(String value) {
        this.cartonPackUnit = value;
    }

    /**
     * Gets the value of the boxPackQuantity property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getBoxPackQuantity() {
        return boxPackQuantity;
    }

    /**
     * Sets the value of the boxPackQuantity property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setBoxPackQuantity(BigInteger value) {
        this.boxPackQuantity = value;
    }

    /**
     * Gets the value of the boxPackUnit property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBoxPackUnit() {
        return boxPackUnit;
    }

    /**
     * Sets the value of the boxPackUnit property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBoxPackUnit(String value) {
        this.boxPackUnit = value;
    }

    /**
     * Gets the value of the packQuantity property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getPackQuantity() {
        return packQuantity;
    }

    /**
     * Sets the value of the packQuantity property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setPackQuantity(BigInteger value) {
        this.packQuantity = value;
    }

    /**
     * Gets the value of the packUnit property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPackUnit() {
        return packUnit;
    }

    /**
     * Sets the value of the packUnit property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPackUnit(String value) {
        this.packUnit = value;
    }

    /**
     * Gets the value of the forModelNumbers property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getForModelNumbers() {
        return forModelNumbers;
    }

    /**
     * Sets the value of the forModelNumbers property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setForModelNumbers(String value) {
        this.forModelNumbers = value;
    }

    /**
     * Gets the value of the offer property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the offer property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getOffer().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link OfferResponseType }
     * 
     * 
     */
    public List<OfferResponseType> getOffer() {
        if (offer == null) {
            offer = new ArrayList<OfferResponseType>();
        }
        return this.offer;
    }

    /**
     * Gets the value of the upc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUPC() {
        return upc;
    }

    /**
     * Sets the value of the upc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUPC(String value) {
        this.upc = value;
    }

    /**
     * Gets the value of the upcCarton property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUPCCarton() {
        return upcCarton;
    }

    /**
     * Sets the value of the upcCarton property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUPCCarton(String value) {
        this.upcCarton = value;
    }

    /**
     * Gets the value of the dealerUPC property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDealerUPC() {
        return dealerUPC;
    }

    /**
     * Sets the value of the dealerUPC property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDealerUPC(String value) {
        this.dealerUPC = value;
    }

    /**
     * Gets the value of the unspsc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUNSPSC() {
        return unspsc;
    }

    /**
     * Sets the value of the unspsc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUNSPSC(String value) {
        this.unspsc = value;
    }

    /**
     * Gets the value of the shipClass property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShipClass() {
        return shipClass;
    }

    /**
     * Sets the value of the shipClass property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShipClass(String value) {
        this.shipClass = value;
    }

    /**
     * Gets the value of the discontinuedItem property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDiscontinuedItem() {
        return discontinuedItem;
    }

    /**
     * Sets the value of the discontinuedItem property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDiscontinuedItem(String value) {
        this.discontinuedItem = value;
    }

    /**
     * Gets the value of the activeIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getActiveIndicator() {
        return activeIndicator;
    }

    /**
     * Sets the value of the activeIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setActiveIndicator(String value) {
        this.activeIndicator = value;
    }

    /**
     * Gets the value of the suggestedItem property.
     * 
     * @return
     *     possible object is
     *     {@link ItemType }
     *     
     */
    public ItemType getSuggestedItem() {
        return suggestedItem;
    }

    /**
     * Sets the value of the suggestedItem property.
     * 
     * @param value
     *     allowed object is
     *     {@link ItemType }
     *     
     */
    public void setSuggestedItem(ItemType value) {
        this.suggestedItem = value;
    }

    /**
     * Gets the value of the mpn property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMPN() {
        return mpn;
    }

    /**
     * Sets the value of the mpn property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMPN(String value) {
        this.mpn = value;
    }

    /**
     * Gets the value of the catalogPageNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCatalogPageNumber() {
        return catalogPageNumber;
    }

    /**
     * Sets the value of the catalogPageNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCatalogPageNumber(String value) {
        this.catalogPageNumber = value;
    }

    /**
     * Gets the value of the resellerCrossReference property.
     * 
     * @return
     *     possible object is
     *     {@link ResellerCrossReference }
     *     
     */
    public ResellerCrossReference getResellerCrossReference() {
        return resellerCrossReference;
    }

    /**
     * Sets the value of the resellerCrossReference property.
     * 
     * @param value
     *     allowed object is
     *     {@link ResellerCrossReference }
     *     
     */
    public void setResellerCrossReference(ResellerCrossReference value) {
        this.resellerCrossReference = value;
    }

    /**
     * Gets the value of the productClass property.
     * 
     * @return
     *     possible object is
     *     {@link ProductClassType }
     *     
     */
    public ProductClassType getProductClass() {
        return productClass;
    }

    /**
     * Sets the value of the productClass property.
     * 
     * @param value
     *     allowed object is
     *     {@link ProductClassType }
     *     
     */
    public void setProductClass(ProductClassType value) {
        this.productClass = value;
    }

    /**
     * Gets the value of the tags property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the tags property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTags().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TagType }
     * 
     * 
     */
    public List<TagType> getTags() {
        if (tags == null) {
            tags = new ArrayList<TagType>();
        }
        return this.tags;
    }

    /**
     * Gets the value of the merchPreference property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMerchPreference() {
        return merchPreference;
    }

    /**
     * Sets the value of the merchPreference property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMerchPreference(String value) {
        this.merchPreference = value;
    }

    /**
     * Gets the value of the stockingDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStockingDescription() {
        return stockingDescription;
    }

    /**
     * Sets the value of the stockingDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStockingDescription(String value) {
        this.stockingDescription = value;
    }

    /**
     * Gets the value of the stockingIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStockingIndicator() {
        return stockingIndicator;
    }

    /**
     * Sets the value of the stockingIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStockingIndicator(String value) {
        this.stockingIndicator = value;
    }

    /**
     * Gets the value of the stateRestriction property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStateRestriction() {
        return stateRestriction;
    }

    /**
     * Sets the value of the stateRestriction property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStateRestriction(String value) {
        this.stateRestriction = value;
    }

    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;simpleContent>
     *     &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>double">
     *       &lt;attribute name="unitCode" type="{http://www.w3.org/2001/XMLSchema}string" />
     *     &lt;/extension>
     *   &lt;/simpleContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = { "value" })
    public static class BoxHeight {

        @XmlValue
        protected double value;
        @XmlAttribute
        protected String unitCode;

        /**
         * Gets the value of the value property.
         * 
         */
        public double getValue() {
            return value;
        }

        /**
         * Sets the value of the value property.
         * 
         */
        public void setValue(double value) {
            this.value = value;
        }

        /**
         * Gets the value of the unitCode property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getUnitCode() {
            return unitCode;
        }

        /**
         * Sets the value of the unitCode property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setUnitCode(String value) {
            this.unitCode = value;
        }

    }

    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;simpleContent>
     *     &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>double">
     *       &lt;attribute name="unitCode" type="{http://www.w3.org/2001/XMLSchema}string" />
     *     &lt;/extension>
     *   &lt;/simpleContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = { "value" })
    public static class BoxLength {

        @XmlValue
        protected double value;
        @XmlAttribute
        protected String unitCode;

        /**
         * Gets the value of the value property.
         * 
         */
        public double getValue() {
            return value;
        }

        /**
         * Sets the value of the value property.
         * 
         */
        public void setValue(double value) {
            this.value = value;
        }

        /**
         * Gets the value of the unitCode property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getUnitCode() {
            return unitCode;
        }

        /**
         * Sets the value of the unitCode property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setUnitCode(String value) {
            this.unitCode = value;
        }

    }

    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;simpleContent>
     *     &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>double">
     *       &lt;attribute name="unitCode" type="{http://www.w3.org/2001/XMLSchema}string" />
     *     &lt;/extension>
     *   &lt;/simpleContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = { "value" })
    public static class BoxWeight {

        @XmlValue
        protected double value;
        @XmlAttribute
        protected String unitCode;

        /**
         * Gets the value of the value property.
         * 
         */
        public double getValue() {
            return value;
        }

        /**
         * Sets the value of the value property.
         * 
         */
        public void setValue(double value) {
            this.value = value;
        }

        /**
         * Gets the value of the unitCode property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getUnitCode() {
            return unitCode;
        }

        /**
         * Sets the value of the unitCode property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setUnitCode(String value) {
            this.unitCode = value;
        }

    }

    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;simpleContent>
     *     &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>double">
     *       &lt;attribute name="unitCode" type="{http://www.w3.org/2001/XMLSchema}string" />
     *     &lt;/extension>
     *   &lt;/simpleContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = { "value" })
    public static class BoxWidth {

        @XmlValue
        protected double value;
        @XmlAttribute
        protected String unitCode;

        /**
         * Gets the value of the value property.
         * 
         */
        public double getValue() {
            return value;
        }

        /**
         * Sets the value of the value property.
         * 
         */
        public void setValue(double value) {
            this.value = value;
        }

        /**
         * Gets the value of the unitCode property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getUnitCode() {
            return unitCode;
        }

        /**
         * Sets the value of the unitCode property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setUnitCode(String value) {
            this.unitCode = value;
        }

    }

    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;simpleContent>
     *     &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>double">
     *       &lt;attribute name="unitCode" type="{http://www.w3.org/2001/XMLSchema}string" />
     *     &lt;/extension>
     *   &lt;/simpleContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = { "value" })
    public static class CartonHeight {

        @XmlValue
        protected double value;
        @XmlAttribute
        protected String unitCode;

        /**
         * Gets the value of the value property.
         * 
         */
        public double getValue() {
            return value;
        }

        /**
         * Sets the value of the value property.
         * 
         */
        public void setValue(double value) {
            this.value = value;
        }

        /**
         * Gets the value of the unitCode property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getUnitCode() {
            return unitCode;
        }

        /**
         * Sets the value of the unitCode property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setUnitCode(String value) {
            this.unitCode = value;
        }

    }

    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;simpleContent>
     *     &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>double">
     *       &lt;attribute name="unitCode" type="{http://www.w3.org/2001/XMLSchema}string" />
     *     &lt;/extension>
     *   &lt;/simpleContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = { "value" })
    public static class CartonLength {

        @XmlValue
        protected double value;
        @XmlAttribute
        protected String unitCode;

        /**
         * Gets the value of the value property.
         * 
         */
        public double getValue() {
            return value;
        }

        /**
         * Sets the value of the value property.
         * 
         */
        public void setValue(double value) {
            this.value = value;
        }

        /**
         * Gets the value of the unitCode property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getUnitCode() {
            return unitCode;
        }

        /**
         * Sets the value of the unitCode property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setUnitCode(String value) {
            this.unitCode = value;
        }

    }

    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;simpleContent>
     *     &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>double">
     *       &lt;attribute name="unitCode" type="{http://www.w3.org/2001/XMLSchema}string" />
     *     &lt;/extension>
     *   &lt;/simpleContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = { "value" })
    public static class CartonWeight {

        @XmlValue
        protected double value;
        @XmlAttribute
        protected String unitCode;

        /**
         * Gets the value of the value property.
         * 
         */
        public double getValue() {
            return value;
        }

        /**
         * Sets the value of the value property.
         * 
         */
        public void setValue(double value) {
            this.value = value;
        }

        /**
         * Gets the value of the unitCode property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getUnitCode() {
            return unitCode;
        }

        /**
         * Sets the value of the unitCode property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setUnitCode(String value) {
            this.unitCode = value;
        }

    }

    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;simpleContent>
     *     &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>double">
     *       &lt;attribute name="unitCode" type="{http://www.w3.org/2001/XMLSchema}string" />
     *     &lt;/extension>
     *   &lt;/simpleContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = { "value" })
    public static class CartonWidth {

        @XmlValue
        protected double value;
        @XmlAttribute
        protected String unitCode;

        /**
         * Gets the value of the value property.
         * 
         */
        public double getValue() {
            return value;
        }

        /**
         * Sets the value of the value property.
         * 
         */
        public void setValue(double value) {
            this.value = value;
        }

        /**
         * Gets the value of the unitCode property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getUnitCode() {
            return unitCode;
        }

        /**
         * Sets the value of the unitCode property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setUnitCode(String value) {
            this.unitCode = value;
        }

    }

    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;simpleContent>
     *     &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>double">
     *       &lt;attribute name="unitCode" type="{http://www.w3.org/2001/XMLSchema}string" />
     *     &lt;/extension>
     *   &lt;/simpleContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = { "value" })
    public static class DealerWeight {

        @XmlValue
        protected double value;
        @XmlAttribute
        protected String unitCode;

        /**
         * Gets the value of the value property.
         * 
         */
        public double getValue() {
            return value;
        }

        /**
         * Sets the value of the value property.
         * 
         */
        public void setValue(double value) {
            this.value = value;
        }

        /**
         * Gets the value of the unitCode property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getUnitCode() {
            return unitCode;
        }

        /**
         * Sets the value of the unitCode property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setUnitCode(String value) {
            this.unitCode = value;
        }

    }

    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;simpleContent>
     *     &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>double">
     *       &lt;attribute name="unitCode" type="{http://www.w3.org/2001/XMLSchema}string" />
     *     &lt;/extension>
     *   &lt;/simpleContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = { "value" })
    public static class ItemHeight {

        @XmlValue
        protected double value;
        @XmlAttribute
        protected String unitCode;

        /**
         * Gets the value of the value property.
         * 
         */
        public double getValue() {
            return value;
        }

        /**
         * Sets the value of the value property.
         * 
         */
        public void setValue(double value) {
            this.value = value;
        }

        /**
         * Gets the value of the unitCode property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getUnitCode() {
            return unitCode;
        }

        /**
         * Sets the value of the unitCode property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setUnitCode(String value) {
            this.unitCode = value;
        }

    }

    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;extension base="{http://ws.ussco.com/eCatalog/catalog/1}ImageType">
     *       &lt;attribute name="sequence" type="{http://www.w3.org/2001/XMLSchema}integer" />
     *     &lt;/extension>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class ItemImage extends ImageType {

        @XmlAttribute
        protected BigInteger sequence;

        /**
         * Gets the value of the sequence property.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getSequence() {
            return sequence;
        }

        /**
         * Sets the value of the sequence property.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setSequence(BigInteger value) {
            this.sequence = value;
        }

    }

    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;simpleContent>
     *     &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>double">
     *       &lt;attribute name="unitCode" type="{http://www.w3.org/2001/XMLSchema}string" />
     *     &lt;/extension>
     *   &lt;/simpleContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = { "value" })
    public static class ItemLength {

        @XmlValue
        protected double value;
        @XmlAttribute
        protected String unitCode;

        /**
         * Gets the value of the value property.
         * 
         */
        public double getValue() {
            return value;
        }

        /**
         * Sets the value of the value property.
         * 
         */
        public void setValue(double value) {
            this.value = value;
        }

        /**
         * Gets the value of the unitCode property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getUnitCode() {
            return unitCode;
        }

        /**
         * Sets the value of the unitCode property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setUnitCode(String value) {
            this.unitCode = value;
        }

    }

    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;simpleContent>
     *     &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>double">
     *       &lt;attribute name="unitCode" type="{http://www.w3.org/2001/XMLSchema}string" />
     *     &lt;/extension>
     *   &lt;/simpleContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = { "value" })
    public static class ItemWidth {

        @XmlValue
        protected double value;
        @XmlAttribute
        protected String unitCode;

        /**
         * Gets the value of the value property.
         * 
         */
        public double getValue() {
            return value;
        }

        /**
         * Sets the value of the value property.
         * 
         */
        public void setValue(double value) {
            this.value = value;
        }

        /**
         * Gets the value of the unitCode property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getUnitCode() {
            return unitCode;
        }

        /**
         * Sets the value of the unitCode property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setUnitCode(String value) {
            this.unitCode = value;
        }

    }

    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;extension base="{http://ws.ussco.com/eCatalog/catalog/1}MerchandisingZoneType">
     *       &lt;attribute name="sequence" type="{http://www.w3.org/2001/XMLSchema}integer" />
     *     &lt;/extension>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class MerchandisingZone extends MerchandisingZoneType {

        @XmlAttribute
        protected BigInteger sequence;

        /**
         * Gets the value of the sequence property.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getSequence() {
            return sequence;
        }

        /**
         * Sets the value of the sequence property.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setSequence(BigInteger value) {
            this.sequence = value;
        }

    }

    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;simpleContent>
     *     &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>string">
     *       &lt;attribute name="style">
     *         &lt;simpleType>
     *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *             &lt;enumeration value="Brief"/>
     *             &lt;enumeration value="Standard"/>
     *             &lt;enumeration value="Long"/>
     *           &lt;/restriction>
     *         &lt;/simpleType>
     *       &lt;/attribute>
     *     &lt;/extension>
     *   &lt;/simpleContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = { "value" })
    public static class SellingCopy {

        @XmlValue
        protected String value;
        @XmlAttribute
        protected String style;

        /**
         * Gets the value of the value property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getValue() {
            return value;
        }

        /**
         * Sets the value of the value property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setValue(String value) {
            this.value = value;
        }

        /**
         * Gets the value of the style property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getStyle() {
            return style;
        }

        /**
         * Sets the value of the style property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setStyle(String value) {
            this.style = value;
        }

    }

    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;simpleContent>
     *     &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>string">
     *       &lt;attribute name="sequence" type="{http://www.w3.org/2001/XMLSchema}integer" />
     *     &lt;/extension>
     *   &lt;/simpleContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = { "value" })
    public static class SellingPoint {

        @XmlValue
        protected String value;
        @XmlAttribute
        protected BigInteger sequence;

        /**
         * Gets the value of the value property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getValue() {
            return value;
        }

        /**
         * Sets the value of the value property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setValue(String value) {
            this.value = value;
        }

        /**
         * Gets the value of the sequence property.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getSequence() {
            return sequence;
        }

        /**
         * Sets the value of the sequence property.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setSequence(BigInteger value) {
            this.sequence = value;
        }

    }

    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;simpleContent>
     *     &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>double">
     *       &lt;attribute name="unitCode" type="{http://www.w3.org/2001/XMLSchema}string" />
     *     &lt;/extension>
     *   &lt;/simpleContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = { "value" })
    public static class Weight {

        @XmlValue
        protected double value;
        @XmlAttribute
        protected String unitCode;

        /**
         * Gets the value of the value property.
         * 
         */
        public double getValue() {
            return value;
        }

        /**
         * Sets the value of the value property.
         * 
         */
        public void setValue(double value) {
            this.value = value;
        }

        /**
         * Gets the value of the unitCode property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getUnitCode() {
            return unitCode;
        }

        /**
         * Sets the value of the unitCode property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setUnitCode(String value) {
            this.unitCode = value;
        }

    }

}
