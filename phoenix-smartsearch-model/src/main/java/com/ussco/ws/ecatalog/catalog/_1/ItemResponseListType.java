package com.ussco.ws.ecatalog.catalog._1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * A list of items 
 * 
 * <p>Java class for ItemResponseListType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ItemResponseListType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Item" type="{http://ws.ussco.com/eCatalog/catalog/1}ItemType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="CrossReference" type="{http://ws.ussco.com/eCatalog/catalog/1}ReferenceMatchType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ItemResponseListType", propOrder = { "item", "crossReference" })
public class ItemResponseListType {

    @XmlElement(name = "Item")
    protected List<ItemType> item;
    @XmlElement(name = "CrossReference")
    protected ReferenceMatchType crossReference;

    /**
     * Gets the value of the item property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the item property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getItem().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ItemType }
     * 
     * 
     */
    public List<ItemType> getItem() {
        if (item == null) {
            item = new ArrayList<ItemType>();
        }
        return this.item;
    }

    /**
     * Gets the value of the crossReference property.
     * 
     * @return
     *     possible object is
     *     {@link ReferenceMatchType }
     *     
     */
    public ReferenceMatchType getCrossReference() {
        return crossReference;
    }

    /**
     * Sets the value of the crossReference property.
     * 
     * @param value
     *     allowed object is
     *     {@link ReferenceMatchType }
     *     
     */
    public void setCrossReference(ReferenceMatchType value) {
        this.crossReference = value;
    }

}
