package com.ussco.ws.ecatalog.catalog._1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="categoryRequest" type="{http://ws.ussco.com/eCatalog/catalog/1}MDCategoryRequestType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "categoryRequest" })
@XmlRootElement(name = "getCategoryRequest")
public class GetCategoryRequest {

    @XmlElement(required = true)
    protected MDCategoryRequestType categoryRequest;

    /**
     * Gets the value of the categoryRequest property.
     * 
     * @return
     *     possible object is
     *     {@link MDCategoryRequestType }
     *     
     */
    public MDCategoryRequestType getCategoryRequest() {
        return categoryRequest;
    }

    /**
     * Sets the value of the categoryRequest property.
     * 
     * @param value
     *     allowed object is
     *     {@link MDCategoryRequestType }
     *     
     */
    public void setCategoryRequest(MDCategoryRequestType value) {
        this.categoryRequest = value;
    }

}
