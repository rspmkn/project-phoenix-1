package com.ussco.ws.ecatalog.catalog._1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;

/**
 * A single sort
 * 
 * <p>Java class for SortType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SortType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SortCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="SortDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SortType", propOrder = { "sortCode", "sortDescription" })
@XmlSeeAlso( { com.ussco.ws.ecatalog.catalog._1.SortListType.Sort.class })
public class SortType {

    @XmlElement(name = "SortCode", required = true)
    protected String sortCode;
    @XmlElement(name = "SortDescription")
    protected String sortDescription;

    /**
     * Gets the value of the sortCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSortCode() {
        return sortCode;
    }

    /**
     * Sets the value of the sortCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSortCode(String value) {
        this.sortCode = value;
    }

    /**
     * Gets the value of the sortDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSortDescription() {
        return sortDescription;
    }

    /**
     * Sets the value of the sortDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSortDescription(String value) {
        this.sortDescription = value;
    }

}
