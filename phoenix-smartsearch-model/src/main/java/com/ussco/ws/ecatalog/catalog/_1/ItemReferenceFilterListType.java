package com.ussco.ws.ecatalog.catalog._1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * A list of Item References to be used to filter the items
 * 
 * <p>Java class for ItemReferenceFilterListType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ItemReferenceFilterListType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ItemReference" type="{http://ws.ussco.com/eCatalog/catalog/1}ItemCrossReferenceType" maxOccurs="150"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ItemReferenceFilterListType", propOrder = { "itemReference" })
public class ItemReferenceFilterListType {

    @XmlElement(name = "ItemReference", required = true)
    protected List<ItemCrossReferenceType> itemReference;

    /**
     * Gets the value of the itemReference property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the itemReference property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getItemReference().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ItemCrossReferenceType }
     * 
     * 
     */
    public List<ItemCrossReferenceType> getItemReference() {
        if (itemReference == null) {
            itemReference = new ArrayList<ItemCrossReferenceType>();
        }
        return this.itemReference;
    }

}
