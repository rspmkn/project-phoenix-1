package com.ussco.ws.ecatalog.catalog._1;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * A supplies finder request
 * 
 * <p>Java class for SuppliesFinderRequestType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SuppliesFinderRequestType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SearchFilters" type="{http://ws.ussco.com/eCatalog/catalog/1}FilterListType" minOccurs="0"/>
 *         &lt;element name="SearchSort" type="{http://ws.ussco.com/eCatalog/catalog/1}SortType" minOccurs="0"/>
 *         &lt;element name="StartAtResult" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *         &lt;element name="BufferSize" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;maxInclusive value="1000"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="ItemReferenceFilters" type="{http://ws.ussco.com/eCatalog/catalog/1}ItemReferenceFilterListType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SuppliesFinderRequestType", propOrder = { "searchFilters", "searchSort", "startAtResult",
        "bufferSize", "itemReferenceFilters" })
public class SuppliesFinderRequestType {

    @XmlElement(name = "SearchFilters")
    protected FilterListType searchFilters;
    @XmlElement(name = "SearchSort")
    protected SortType searchSort;
    @XmlElement(name = "StartAtResult")
    protected BigInteger startAtResult;
    @XmlElement(name = "BufferSize")
    protected BigInteger bufferSize;
    @XmlElement(name = "ItemReferenceFilters")
    protected ItemReferenceFilterListType itemReferenceFilters;

    /**
     * Gets the value of the searchFilters property.
     * 
     * @return
     *     possible object is
     *     {@link FilterListType }
     *     
     */
    public FilterListType getSearchFilters() {
        return searchFilters;
    }

    /**
     * Sets the value of the searchFilters property.
     * 
     * @param value
     *     allowed object is
     *     {@link FilterListType }
     *     
     */
    public void setSearchFilters(FilterListType value) {
        this.searchFilters = value;
    }

    /**
     * Gets the value of the searchSort property.
     * 
     * @return
     *     possible object is
     *     {@link SortType }
     *     
     */
    public SortType getSearchSort() {
        return searchSort;
    }

    /**
     * Sets the value of the searchSort property.
     * 
     * @param value
     *     allowed object is
     *     {@link SortType }
     *     
     */
    public void setSearchSort(SortType value) {
        this.searchSort = value;
    }

    /**
     * Gets the value of the startAtResult property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getStartAtResult() {
        return startAtResult;
    }

    /**
     * Sets the value of the startAtResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setStartAtResult(BigInteger value) {
        this.startAtResult = value;
    }

    /**
     * Gets the value of the bufferSize property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getBufferSize() {
        return bufferSize;
    }

    /**
     * Sets the value of the bufferSize property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setBufferSize(BigInteger value) {
        this.bufferSize = value;
    }

    /**
     * Gets the value of the itemReferenceFilters property.
     * 
     * @return
     *     possible object is
     *     {@link ItemReferenceFilterListType }
     *     
     */
    public ItemReferenceFilterListType getItemReferenceFilters() {
        return itemReferenceFilters;
    }

    /**
     * Sets the value of the itemReferenceFilters property.
     * 
     * @param value
     *     allowed object is
     *     {@link ItemReferenceFilterListType }
     *     
     */
    public void setItemReferenceFilters(ItemReferenceFilterListType value) {
        this.itemReferenceFilters = value;
    }

}
