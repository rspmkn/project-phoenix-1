package com.ussco.ws.ecatalog.catalog._1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Reseller Cross Reference Information including 
 * 
 * <p>Java class for ResellerCrossReference complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ResellerCrossReference">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CrossReferenceMatchType" type="{http://ws.ussco.com/eCatalog/catalog/1}ReferenceMatchType"/>
 *         &lt;element name="ResellerName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ResellerDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ResellerQuantity" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ResellerUnitOfMeasure" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Notes" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ResellerCrossReference", propOrder = { "crossReferenceMatchType", "resellerName",
        "resellerDescription", "resellerQuantity", "resellerUnitOfMeasure", "notes" })
public class ResellerCrossReference {

    @XmlElement(name = "CrossReferenceMatchType", required = true)
    protected ReferenceMatchType crossReferenceMatchType;
    @XmlElement(name = "ResellerName")
    protected String resellerName;
    @XmlElement(name = "ResellerDescription")
    protected String resellerDescription;
    @XmlElement(name = "ResellerQuantity")
    protected String resellerQuantity;
    @XmlElement(name = "ResellerUnitOfMeasure")
    protected String resellerUnitOfMeasure;
    @XmlElement(name = "Notes")
    protected String notes;

    /**
     * Gets the value of the crossReferenceMatchType property.
     * 
     * @return
     *     possible object is
     *     {@link ReferenceMatchType }
     *     
     */
    public ReferenceMatchType getCrossReferenceMatchType() {
        return crossReferenceMatchType;
    }

    /**
     * Sets the value of the crossReferenceMatchType property.
     * 
     * @param value
     *     allowed object is
     *     {@link ReferenceMatchType }
     *     
     */
    public void setCrossReferenceMatchType(ReferenceMatchType value) {
        this.crossReferenceMatchType = value;
    }

    /**
     * Gets the value of the resellerName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResellerName() {
        return resellerName;
    }

    /**
     * Sets the value of the resellerName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResellerName(String value) {
        this.resellerName = value;
    }

    /**
     * Gets the value of the resellerDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResellerDescription() {
        return resellerDescription;
    }

    /**
     * Sets the value of the resellerDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResellerDescription(String value) {
        this.resellerDescription = value;
    }

    /**
     * Gets the value of the resellerQuantity property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResellerQuantity() {
        return resellerQuantity;
    }

    /**
     * Sets the value of the resellerQuantity property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResellerQuantity(String value) {
        this.resellerQuantity = value;
    }

    /**
     * Gets the value of the resellerUnitOfMeasure property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResellerUnitOfMeasure() {
        return resellerUnitOfMeasure;
    }

    /**
     * Sets the value of the resellerUnitOfMeasure property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResellerUnitOfMeasure(String value) {
        this.resellerUnitOfMeasure = value;
    }

    /**
     * Gets the value of the notes property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNotes() {
        return notes;
    }

    /**
     * Sets the value of the notes property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNotes(String value) {
        this.notes = value;
    }

}
