package com.ussco.ws.ecatalog.catalog._1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * An indicator whose presence indicates a particular quality of the item
 * 
 * <p>Java class for ItemIndicatorType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ItemIndicatorType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IndicatorType">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="30"/>
 *               &lt;enumeration value="ANSI_BIFMA"/>
 *               &lt;enumeration value="AssemblyRequired"/>
 *               &lt;enumeration value="ContractItem"/>
 *               &lt;enumeration value="EPACPGCompliant"/>
 *               &lt;enumeration value="Green"/>
 *               &lt;enumeration value="Hazmat Indicator"/>
 *               &lt;enumeration value="Minority Vendor"/>
 *               &lt;enumeration value="MSDS"/>
 *               &lt;enumeration value="NonReturnable"/>
 *               &lt;enumeration value="Recycled"/>
 *               &lt;enumeration value="RelatedItemsAvailable"/>
 *               &lt;enumeration value="SmallPackage"/>
 *               &lt;enumeration value="StateRestriction"/>
 *               &lt;enumeration value="Stocked"/>
 *               &lt;enumeration value="StockingIndicator"/>
 *               &lt;enumeration value="ValuePack"/>
 *               &lt;enumeration value="Warranty"/>
 *               &lt;enumeration value="DealerIndicator01"/>
 *               &lt;enumeration value="DealerIndicator02"/>
 *               &lt;enumeration value="DealerIndicator03"/>
 *               &lt;enumeration value="DealerIndicator04"/>
 *               &lt;enumeration value="DealerIndicator05"/>
 *               &lt;enumeration value="DealerIndicator06"/>
 *               &lt;enumeration value="DealerIndicator07"/>
 *               &lt;enumeration value="DealerIndicator08"/>
 *               &lt;enumeration value="DealerIndicator09"/>
 *               &lt;enumeration value="DealerIndicator10"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="IndicatorValue">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="50"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ItemIndicatorType", propOrder = { "indicatorType", "indicatorValue" })
public class ItemIndicatorType {

    @XmlElement(name = "IndicatorType", required = true)
    protected String indicatorType;
    @XmlElement(name = "IndicatorValue", required = true)
    protected String indicatorValue;

    /**
     * Gets the value of the indicatorType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIndicatorType() {
        return indicatorType;
    }

    /**
     * Sets the value of the indicatorType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIndicatorType(String value) {
        this.indicatorType = value;
    }

    /**
     * Gets the value of the indicatorValue property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIndicatorValue() {
        return indicatorValue;
    }

    /**
     * Sets the value of the indicatorValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIndicatorValue(String value) {
        this.indicatorValue = value;
    }

}
