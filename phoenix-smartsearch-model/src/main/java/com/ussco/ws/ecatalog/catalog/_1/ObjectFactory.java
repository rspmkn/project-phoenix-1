package com.ussco.ws.ecatalog.catalog._1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;

/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.ussco.ws.ecatalog.catalog._1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _GetCdfRequest_QNAME = new QName("http://ws.ussco.com/eCatalog/catalog/1",
            "getCdfRequest");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.ussco.ws.ecatalog.catalog._1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetItemsRequest }
     * 
     */
    public GetItemsRequest createGetItemsRequest() {
        return new GetItemsRequest();
    }

    /**
     * Create an instance of {@link StatusType }
     * 
     */
    public StatusType createStatusType() {
        return new StatusType();
    }

    /**
     * Create an instance of {@link RemoveItemRequest }
     * 
     */
    public RemoveItemRequest createRemoveItemRequest() {
        return new RemoveItemRequest();
    }

    /**
     * Create an instance of {@link ItemType.BoxLength }
     * 
     */
    public ItemType.BoxLength createItemTypeBoxLength() {
        return new ItemType.BoxLength();
    }

    /**
     * Create an instance of {@link ProductCompareResponseType }
     * 
     */
    public ProductCompareResponseType createProductCompareResponseType() {
        return new ProductCompareResponseType();
    }

    /**
     * Create an instance of {@link GetRelatedItemResponse }
     * 
     */
    public GetRelatedItemResponse createGetRelatedItemResponse() {
        return new GetRelatedItemResponse();
    }

    /**
     * Create an instance of {@link ItemsResponseType }
     * 
     */
    public ItemsResponseType createItemsResponseType() {
        return new ItemsResponseType();
    }

    /**
     * Create an instance of {@link GetRelatedItemRequest }
     * 
     */
    public GetRelatedItemRequest createGetRelatedItemRequest() {
        return new GetRelatedItemRequest();
    }

    /**
     * Create an instance of {@link RemoveCustomerXPriceListRequest }
     * 
     */
    public RemoveCustomerXPriceListRequest createRemoveCustomerXPriceListRequest() {
        return new RemoveCustomerXPriceListRequest();
    }

    /**
     * Create an instance of {@link ItemType.CartonWidth }
     * 
     */
    public ItemType.CartonWidth createItemTypeCartonWidth() {
        return new ItemType.CartonWidth();
    }

    /**
     * Create an instance of {@link SuppliesFinderRequestType }
     * 
     */
    public SuppliesFinderRequestType createSuppliesFinderRequestType() {
        return new SuppliesFinderRequestType();
    }

    /**
     * Create an instance of {@link GetPriceListRequest }
     * 
     */
    public GetPriceListRequest createGetPriceListRequest() {
        return new GetPriceListRequest();
    }

    /**
     * Create an instance of {@link GetCdfResponse }
     * 
     */
    public GetCdfResponse createGetCdfResponse() {
        return new GetCdfResponse();
    }

    /**
     * Create an instance of {@link TargetStateType }
     * 
     */
    public TargetStateType createTargetStateType() {
        return new TargetStateType();
    }

    /**
     * Create an instance of {@link ItemListType }
     * 
     */
    public ItemListType createItemListType() {
        return new ItemListType();
    }

    /**
     * Create an instance of {@link LinkType }
     * 
     */
    public LinkType createLinkType() {
        return new LinkType();
    }

    /**
     * Create an instance of {@link ItemType.ItemLength }
     * 
     */
    public ItemType.ItemLength createItemTypeItemLength() {
        return new ItemType.ItemLength();
    }

    /**
     * Create an instance of {@link MDListItemType }
     * 
     */
    public MDListItemType createMDListItemType() {
        return new MDListItemType();
    }

    /**
     * Create an instance of {@link SortType }
     * 
     */
    public SortType createSortType() {
        return new SortType();
    }

    /**
     * Create an instance of {@link SearchResponseType }
     * 
     */
    public SearchResponseType createSearchResponseType() {
        return new SearchResponseType();
    }

    /**
     * Create an instance of {@link GetProductCompareResponse }
     * 
     */
    public GetProductCompareResponse createGetProductCompareResponse() {
        return new GetProductCompareResponse();
    }

    /**
     * Create an instance of {@link SortListType }
     * 
     */
    public SortListType createSortListType() {
        return new SortListType();
    }

    /**
     * Create an instance of {@link GetSiteMapResponse }
     * 
     */
    public GetSiteMapResponse createGetSiteMapResponse() {
        return new GetSiteMapResponse();
    }

    /**
     * Create an instance of {@link ItemType.CartonLength }
     * 
     */
    public ItemType.CartonLength createItemTypeCartonLength() {
        return new ItemType.CartonLength();
    }

    /**
     * Create an instance of {@link MerchandisingZoneType }
     * 
     */
    public MerchandisingZoneType createMerchandisingZoneType() {
        return new MerchandisingZoneType();
    }

    /**
     * Create an instance of {@link GetItemListRequest }
     * 
     */
    public GetItemListRequest createGetItemListRequest() {
        return new GetItemListRequest();
    }

    /**
     * Create an instance of {@link GetCustomerXPriceListResponse }
     * 
     */
    public GetCustomerXPriceListResponse createGetCustomerXPriceListResponse() {
        return new GetCustomerXPriceListResponse();
    }

    /**
     * Create an instance of {@link ResponseHeaderType }
     * 
     */
    public ResponseHeaderType createResponseHeaderType() {
        return new ResponseHeaderType();
    }

    /**
     * Create an instance of {@link GetOfferResponse }
     * 
     */
    public GetOfferResponse createGetOfferResponse() {
        return new GetOfferResponse();
    }

    /**
     * Create an instance of {@link UpdateItemRequest }
     * 
     */
    public UpdateItemRequest createUpdateItemRequest() {
        return new UpdateItemRequest();
    }

    /**
     * Create an instance of {@link SortListType.Sort }
     * 
     */
    public SortListType.Sort createSortListTypeSort() {
        return new SortListType.Sort();
    }

    /**
     * Create an instance of {@link ItemIndicatorType }
     * 
     */
    public ItemIndicatorType createItemIndicatorType() {
        return new ItemIndicatorType();
    }

    /**
     * Create an instance of {@link MDListRequestType }
     * 
     */
    public MDListRequestType createMDListRequestType() {
        return new MDListRequestType();
    }

    /**
     * Create an instance of {@link GetPriceListResponse }
     * 
     */
    public GetPriceListResponse createGetPriceListResponse() {
        return new GetPriceListResponse();
    }

    /**
     * Create an instance of {@link MerchandisingZoneOfferEntryType }
     * 
     */
    public MerchandisingZoneOfferEntryType createMerchandisingZoneOfferEntryType() {
        return new MerchandisingZoneOfferEntryType();
    }

    /**
     * Create an instance of {@link ItemType.ItemHeight }
     * 
     */
    public ItemType.ItemHeight createItemTypeItemHeight() {
        return new ItemType.ItemHeight();
    }

    /**
     * Create an instance of {@link RemoveCustomerXPriceListResponse }
     * 
     */
    public RemoveCustomerXPriceListResponse createRemoveCustomerXPriceListResponse() {
        return new RemoveCustomerXPriceListResponse();
    }

    /**
     * Create an instance of {@link RequestHeaderType }
     * 
     */
    public RequestHeaderType createRequestHeaderType() {
        return new RequestHeaderType();
    }

    /**
     * Create an instance of {@link CategoryType }
     * 
     */
    public CategoryType createCategoryType() {
        return new CategoryType();
    }

    /**
     * Create an instance of {@link RemoveItemResponse }
     * 
     */
    public RemoveItemResponse createRemoveItemResponse() {
        return new RemoveItemResponse();
    }

    /**
     * Create an instance of {@link ReferenceMatchType }
     * 
     */
    public ReferenceMatchType createReferenceMatchType() {
        return new ReferenceMatchType();
    }

    /**
     * Create an instance of {@link GetItemResponse }
     * 
     */
    public GetItemResponse createGetItemResponse() {
        return new GetItemResponse();
    }

    /**
     * Create an instance of {@link ItemType.Weight }
     * 
     */
    public ItemType.Weight createItemTypeWeight() {
        return new ItemType.Weight();
    }

    /**
     * Create an instance of {@link ItemsRequestType }
     * 
     */
    public ItemsRequestType createItemsRequestType() {
        return new ItemsRequestType();
    }

    /**
     * Create an instance of {@link ItemCrossReferenceType }
     * 
     */
    public ItemCrossReferenceType createItemCrossReferenceType() {
        return new ItemCrossReferenceType();
    }

    /**
     * Create an instance of {@link ItemReferenceFilterListType }
     * 
     */
    public ItemReferenceFilterListType createItemReferenceFilterListType() {
        return new ItemReferenceFilterListType();
    }

    /**
     * Create an instance of {@link FilterListType }
     * 
     */
    public FilterListType createFilterListType() {
        return new FilterListType();
    }

    /**
     * Create an instance of {@link ResellerCrossReference }
     * 
     */
    public ResellerCrossReference createResellerCrossReference() {
        return new ResellerCrossReference();
    }

    /**
     * Create an instance of {@link CdfType }
     * 
     */
    public CdfType createCdfType() {
        return new CdfType();
    }

    /**
     * Create an instance of {@link ItemType.MerchandisingZone }
     * 
     */
    public ItemType.MerchandisingZone createItemTypeMerchandisingZone() {
        return new ItemType.MerchandisingZone();
    }

    /**
     * Create an instance of {@link UpdateCustomerXPriceListRequest }
     * 
     */
    public UpdateCustomerXPriceListRequest createUpdateCustomerXPriceListRequest() {
        return new UpdateCustomerXPriceListRequest();
    }

    /**
     * Create an instance of {@link MerchandisingZoneItemEntryType }
     * 
     */
    public MerchandisingZoneItemEntryType createMerchandisingZoneItemEntryType() {
        return new MerchandisingZoneItemEntryType();
    }

    /**
     * Create an instance of {@link GetMarketingAssetsRequest }
     * 
     */
    public GetMarketingAssetsRequest createGetMarketingAssetsRequest() {
        return new GetMarketingAssetsRequest();
    }

    /**
     * Create an instance of {@link MDItemRequestType }
     * 
     */
    public MDItemRequestType createMDItemRequestType() {
        return new MDItemRequestType();
    }

    /**
     * Create an instance of {@link IdentificationType }
     * 
     */
    public IdentificationType createIdentificationType() {
        return new IdentificationType();
    }

    /**
     * Create an instance of {@link RemoveItemListRequest }
     * 
     */
    public RemoveItemListRequest createRemoveItemListRequest() {
        return new RemoveItemListRequest();
    }

    /**
     * Create an instance of {@link ItemType.DealerWeight }
     * 
     */
    public ItemType.DealerWeight createItemTypeDealerWeight() {
        return new ItemType.DealerWeight();
    }

    /**
     * Create an instance of {@link MerchImageType }
     * 
     */
    public MerchImageType createMerchImageType() {
        return new MerchImageType();
    }

    /**
     * Create an instance of {@link ItemType.BoxHeight }
     * 
     */
    public ItemType.BoxHeight createItemTypeBoxHeight() {
        return new ItemType.BoxHeight();
    }

    /**
     * Create an instance of {@link TargetType }
     * 
     */
    public TargetType createTargetType() {
        return new TargetType();
    }

    /**
     * Create an instance of {@link FilterType }
     * 
     */
    public FilterType createFilterType() {
        return new FilterType();
    }

    /**
     * Create an instance of {@link ItemResponseListType }
     * 
     */
    public ItemResponseListType createItemResponseListType() {
        return new ItemResponseListType();
    }

    /**
     * Create an instance of {@link MerchItemType.MerchandisingZone }
     * 
     */
    public MerchItemType.MerchandisingZone createMerchItemTypeMerchandisingZone() {
        return new MerchItemType.MerchandisingZone();
    }

    /**
     * Create an instance of {@link ItemType.ItemWidth }
     * 
     */
    public ItemType.ItemWidth createItemTypeItemWidth() {
        return new ItemType.ItemWidth();
    }

    /**
     * Create an instance of {@link MDItemResponseType }
     * 
     */
    public MDItemResponseType createMDItemResponseType() {
        return new MDItemResponseType();
    }

    /**
     * Create an instance of {@link MDCustomerXPriceResponseType }
     * 
     */
    public MDCustomerXPriceResponseType createMDCustomerXPriceResponseType() {
        return new MDCustomerXPriceResponseType();
    }

    /**
     * Create an instance of {@link UpdateItemListRequest }
     * 
     */
    public UpdateItemListRequest createUpdateItemListRequest() {
        return new UpdateItemListRequest();
    }

    /**
     * Create an instance of {@link ItemType.SellingPoint }
     * 
     */
    public ItemType.SellingPoint createItemTypeSellingPoint() {
        return new ItemType.SellingPoint();
    }

    /**
     * Create an instance of {@link MDListResponseType }
     * 
     */
    public MDListResponseType createMDListResponseType() {
        return new MDListResponseType();
    }

    /**
     * Create an instance of {@link LandingPageType }
     * 
     */
    public LandingPageType createLandingPageType() {
        return new LandingPageType();
    }

    /**
     * Create an instance of {@link OfferFreeGoodsType }
     * 
     */
    public OfferFreeGoodsType createOfferFreeGoodsType() {
        return new OfferFreeGoodsType();
    }

    /**
     * Create an instance of {@link ListNameType }
     * 
     */
    public ListNameType createListNameType() {
        return new ListNameType();
    }

    /**
     * Create an instance of {@link MDListType.ListItem }
     * 
     */
    public MDListType.ListItem createMDListTypeListItem() {
        return new MDListType.ListItem();
    }

    /**
     * Create an instance of {@link MarketingAssetsRequestType }
     * 
     */
    public MarketingAssetsRequestType createMarketingAssetsRequestType() {
        return new MarketingAssetsRequestType();
    }

    /**
     * Create an instance of {@link MerchandisingZoneResourceEntryType }
     * 
     */
    public MerchandisingZoneResourceEntryType createMerchandisingZoneResourceEntryType() {
        return new MerchandisingZoneResourceEntryType();
    }

    /**
     * Create an instance of {@link GetCdfRequest }
     * 
     */
    public GetCdfRequest createGetCdfRequest() {
        return new GetCdfRequest();
    }

    /**
     * Create an instance of {@link MerchItemType.ItemImage }
     * 
     */
    public MerchItemType.ItemImage createMerchItemTypeItemImage() {
        return new MerchItemType.ItemImage();
    }

    /**
     * Create an instance of {@link UpdatePriceListResponse }
     * 
     */
    public UpdatePriceListResponse createUpdatePriceListResponse() {
        return new UpdatePriceListResponse();
    }

    /**
     * Create an instance of {@link ItemResponseType }
     * 
     */
    public ItemResponseType createItemResponseType() {
        return new ItemResponseType();
    }

    /**
     * Create an instance of {@link TagType }
     * 
     */
    public TagType createTagType() {
        return new TagType();
    }

    /**
     * Create an instance of {@link RelatedItemListResponseType }
     * 
     */
    public RelatedItemListResponseType createRelatedItemListResponseType() {
        return new RelatedItemListResponseType();
    }

    /**
     * Create an instance of {@link MerchandisingZoneBrandEntryType }
     * 
     */
    public MerchandisingZoneBrandEntryType createMerchandisingZoneBrandEntryType() {
        return new MerchandisingZoneBrandEntryType();
    }

    /**
     * Create an instance of {@link MDCdfRequestType }
     * 
     */
    public MDCdfRequestType createMDCdfRequestType() {
        return new MDCdfRequestType();
    }

    /**
     * Create an instance of {@link MessageType }
     * 
     */
    public MessageType createMessageType() {
        return new MessageType();
    }

    /**
     * Create an instance of {@link ResourceType }
     * 
     */
    public ResourceType createResourceType() {
        return new ResourceType();
    }

    /**
     * Create an instance of {@link ItemType.ItemImage }
     * 
     */
    public ItemType.ItemImage createItemTypeItemImage() {
        return new ItemType.ItemImage();
    }

    /**
     * Create an instance of {@link OfferQualifyingCriteriaType }
     * 
     */
    public OfferQualifyingCriteriaType createOfferQualifyingCriteriaType() {
        return new OfferQualifyingCriteriaType();
    }

    /**
     * Create an instance of {@link UpdateItemResponse }
     * 
     */
    public UpdateItemResponse createUpdateItemResponse() {
        return new UpdateItemResponse();
    }

    /**
     * Create an instance of {@link ZoneNameListType }
     * 
     */
    public ZoneNameListType createZoneNameListType() {
        return new ZoneNameListType();
    }

    /**
     * Create an instance of {@link GetSearchResponse }
     * 
     */
    public GetSearchResponse createGetSearchResponse() {
        return new GetSearchResponse();
    }

    /**
     * Create an instance of {@link MarketingAssetsResponseType }
     * 
     */
    public MarketingAssetsResponseType createMarketingAssetsResponseType() {
        return new MarketingAssetsResponseType();
    }

    /**
     * Create an instance of {@link MDCustomItemType }
     * 
     */
    public MDCustomItemType createMDCustomItemType() {
        return new MDCustomItemType();
    }

    /**
     * Create an instance of {@link GetProductCompareRequest }
     * 
     */
    public GetProductCompareRequest createGetProductCompareRequest() {
        return new GetProductCompareRequest();
    }

    /**
     * Create an instance of {@link ItemResultStyle }
     * 
     */
    public ItemResultStyle createItemResultStyle() {
        return new ItemResultStyle();
    }

    /**
     * Create an instance of {@link MDListType }
     * 
     */
    public MDListType createMDListType() {
        return new MDListType();
    }

    /**
     * Create an instance of {@link GetItemRequest }
     * 
     */
    public GetItemRequest createGetItemRequest() {
        return new GetItemRequest();
    }

    /**
     * Create an instance of {@link ItemsEntryType }
     * 
     */
    public ItemsEntryType createItemsEntryType() {
        return new ItemsEntryType();
    }

    /**
     * Create an instance of {@link SearchRequestType }
     * 
     */
    public SearchRequestType createSearchRequestType() {
        return new SearchRequestType();
    }

    /**
     * Create an instance of {@link RelatedItemResponseType.RelatedItem }
     * 
     */
    public RelatedItemResponseType.RelatedItem createRelatedItemResponseTypeRelatedItem() {
        return new RelatedItemResponseType.RelatedItem();
    }

    /**
     * Create an instance of {@link MerchandisingZoneImageEntryType }
     * 
     */
    public MerchandisingZoneImageEntryType createMerchandisingZoneImageEntryType() {
        return new MerchandisingZoneImageEntryType();
    }

    /**
     * Create an instance of {@link ProductCompareRequestType }
     * 
     */
    public ProductCompareRequestType createProductCompareRequestType() {
        return new ProductCompareRequestType();
    }

    /**
     * Create an instance of {@link ItemType.BoxWidth }
     * 
     */
    public ItemType.BoxWidth createItemTypeBoxWidth() {
        return new ItemType.BoxWidth();
    }

    /**
     * Create an instance of {@link RemoveItemListResponse }
     * 
     */
    public RemoveItemListResponse createRemoveItemListResponse() {
        return new RemoveItemListResponse();
    }

    /**
     * Create an instance of {@link OfferRequestType }
     * 
     */
    public OfferRequestType createOfferRequestType() {
        return new OfferRequestType();
    }

    /**
     * Create an instance of {@link GetSearchRequest }
     * 
     */
    public GetSearchRequest createGetSearchRequest() {
        return new GetSearchRequest();
    }

    /**
     * Create an instance of {@link ImageType }
     * 
     */
    public ImageType createImageType() {
        return new ImageType();
    }

    /**
     * Create an instance of {@link CatNavBannerType }
     * 
     */
    public CatNavBannerType createCatNavBannerType() {
        return new CatNavBannerType();
    }

    /**
     * Create an instance of {@link ProductClassType }
     * 
     */
    public ProductClassType createProductClassType() {
        return new ProductClassType();
    }

    /**
     * Create an instance of {@link OfferResponseType }
     * 
     */
    public OfferResponseType createOfferResponseType() {
        return new OfferResponseType();
    }

    /**
     * Create an instance of {@link MerchandisingZoneMessageEntryType }
     * 
     */
    public MerchandisingZoneMessageEntryType createMerchandisingZoneMessageEntryType() {
        return new MerchandisingZoneMessageEntryType();
    }

    /**
     * Create an instance of {@link GetSiteMapRequest }
     * 
     */
    public GetSiteMapRequest createGetSiteMapRequest() {
        return new GetSiteMapRequest();
    }

    /**
     * Create an instance of {@link MDCustomerXPriceRequestType }
     * 
     */
    public MDCustomerXPriceRequestType createMDCustomerXPriceRequestType() {
        return new MDCustomerXPriceRequestType();
    }

    /**
     * Create an instance of {@link RelatedItemRequestType }
     * 
     */
    public RelatedItemRequestType createRelatedItemRequestType() {
        return new RelatedItemRequestType();
    }

    /**
     * Create an instance of {@link AttributeType }
     * 
     */
    public AttributeType createAttributeType() {
        return new AttributeType();
    }

    /**
     * Create an instance of {@link GetCategoryRequest }
     * 
     */
    public GetCategoryRequest createGetCategoryRequest() {
        return new GetCategoryRequest();
    }

    /**
     * Create an instance of {@link GetOfferRequest }
     * 
     */
    public GetOfferRequest createGetOfferRequest() {
        return new GetOfferRequest();
    }

    /**
     * Create an instance of {@link RemovePriceListRequest }
     * 
     */
    public RemovePriceListRequest createRemovePriceListRequest() {
        return new RemovePriceListRequest();
    }

    /**
     * Create an instance of {@link FilterValueType }
     * 
     */
    public FilterValueType createFilterValueType() {
        return new FilterValueType();
    }

    /**
     * Create an instance of {@link GetCategoryResponse }
     * 
     */
    public GetCategoryResponse createGetCategoryResponse() {
        return new GetCategoryResponse();
    }

    /**
     * Create an instance of {@link ItemType.BoxWeight }
     * 
     */
    public ItemType.BoxWeight createItemTypeBoxWeight() {
        return new ItemType.BoxWeight();
    }

    /**
     * Create an instance of {@link ItemType.CartonHeight }
     * 
     */
    public ItemType.CartonHeight createItemTypeCartonHeight() {
        return new ItemType.CartonHeight();
    }

    /**
     * Create an instance of {@link BrandType }
     * 
     */
    public BrandType createBrandType() {
        return new BrandType();
    }

    /**
     * Create an instance of {@link GetSuppliesFinderRequest }
     * 
     */
    public GetSuppliesFinderRequest createGetSuppliesFinderRequest() {
        return new GetSuppliesFinderRequest();
    }

    /**
     * Create an instance of {@link SiteMapResponseType }
     * 
     */
    public SiteMapResponseType createSiteMapResponseType() {
        return new SiteMapResponseType();
    }

    /**
     * Create an instance of {@link SuppliesFinderResponseType }
     * 
     */
    public SuppliesFinderResponseType createSuppliesFinderResponseType() {
        return new SuppliesFinderResponseType();
    }

    /**
     * Create an instance of {@link MDCategoryRequestType }
     * 
     */
    public MDCategoryRequestType createMDCategoryRequestType() {
        return new MDCategoryRequestType();
    }

    /**
     * Create an instance of {@link SiteMapRequestType }
     * 
     */
    public SiteMapRequestType createSiteMapRequestType() {
        return new SiteMapRequestType();
    }

    /**
     * Create an instance of {@link UpdateItemListResponse }
     * 
     */
    public UpdateItemListResponse createUpdateItemListResponse() {
        return new UpdateItemListResponse();
    }

    /**
     * Create an instance of {@link MDCdfResponseType }
     * 
     */
    public MDCdfResponseType createMDCdfResponseType() {
        return new MDCdfResponseType();
    }

    /**
     * Create an instance of {@link GetItemsResponse }
     * 
     */
    public GetItemsResponse createGetItemsResponse() {
        return new GetItemsResponse();
    }

    /**
     * Create an instance of {@link MerchandisingZoneEntryType }
     * 
     */
    public MerchandisingZoneEntryType createMerchandisingZoneEntryType() {
        return new MerchandisingZoneEntryType();
    }

    /**
     * Create an instance of {@link RemovePriceListResponse }
     * 
     */
    public RemovePriceListResponse createRemovePriceListResponse() {
        return new RemovePriceListResponse();
    }

    /**
     * Create an instance of {@link RelatedItemResponseType }
     * 
     */
    public RelatedItemResponseType createRelatedItemResponseType() {
        return new RelatedItemResponseType();
    }

    /**
     * Create an instance of {@link ItemType }
     * 
     */
    public ItemType createItemType() {
        return new ItemType();
    }

    /**
     * Create an instance of {@link UpdateCustomerXPriceListResponse }
     * 
     */
    public UpdateCustomerXPriceListResponse createUpdateCustomerXPriceListResponse() {
        return new UpdateCustomerXPriceListResponse();
    }

    /**
     * Create an instance of {@link MerchItemType }
     * 
     */
    public MerchItemType createMerchItemType() {
        return new MerchItemType();
    }

    /**
     * Create an instance of {@link GetSuppliesFinderResponse }
     * 
     */
    public GetSuppliesFinderResponse createGetSuppliesFinderResponse() {
        return new GetSuppliesFinderResponse();
    }

    /**
     * Create an instance of {@link ResponseTimeType }
     * 
     */
    public ResponseTimeType createResponseTimeType() {
        return new ResponseTimeType();
    }

    /**
     * Create an instance of {@link MerchItemPageResponseType }
     * 
     */
    public MerchItemPageResponseType createMerchItemPageResponseType() {
        return new MerchItemPageResponseType();
    }

    /**
     * Create an instance of {@link MerchandisingZoneCategoryEntryType }
     * 
     */
    public MerchandisingZoneCategoryEntryType createMerchandisingZoneCategoryEntryType() {
        return new MerchandisingZoneCategoryEntryType();
    }

    /**
     * Create an instance of {@link ItemType.SellingCopy }
     * 
     */
    public ItemType.SellingCopy createItemTypeSellingCopy() {
        return new ItemType.SellingCopy();
    }

    /**
     * Create an instance of {@link ItemType.CartonWeight }
     * 
     */
    public ItemType.CartonWeight createItemTypeCartonWeight() {
        return new ItemType.CartonWeight();
    }

    /**
     * Create an instance of {@link MDCustomerXPriceType }
     * 
     */
    public MDCustomerXPriceType createMDCustomerXPriceType() {
        return new MDCustomerXPriceType();
    }

    /**
     * Create an instance of {@link UpdatePriceListRequest }
     * 
     */
    public UpdatePriceListRequest createUpdatePriceListRequest() {
        return new UpdatePriceListRequest();
    }

    /**
     * Create an instance of {@link GetMarketingAssetsResponse }
     * 
     */
    public GetMarketingAssetsResponse createGetMarketingAssetsResponse() {
        return new GetMarketingAssetsResponse();
    }

    /**
     * Create an instance of {@link GetItemListResponse }
     * 
     */
    public GetItemListResponse createGetItemListResponse() {
        return new GetItemListResponse();
    }

    /**
     * Create an instance of {@link MDCategoryResponseType }
     * 
     */
    public MDCategoryResponseType createMDCategoryResponseType() {
        return new MDCategoryResponseType();
    }

    /**
     * Create an instance of {@link GetCustomerXPriceListRequest }
     * 
     */
    public GetCustomerXPriceListRequest createGetCustomerXPriceListRequest() {
        return new GetCustomerXPriceListRequest();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCdfRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.ussco.com/eCatalog/catalog/1", name = "getCdfRequest")
    public JAXBElement<GetCdfRequest> createGetCdfRequest(GetCdfRequest value) {
        return new JAXBElement<GetCdfRequest>(_GetCdfRequest_QNAME, GetCdfRequest.class, null, value);
    }

}
