package com.ussco.ws.ecatalog.catalog._1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * A related item list response
 * 
 * <p>Java class for RelatedItemListResponseType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RelatedItemListResponseType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ItemRelation" type="{http://ws.ussco.com/eCatalog/catalog/1}RelatedItemResponseType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RelatedItemListResponseType", propOrder = { "itemRelation" })
public class RelatedItemListResponseType {

    @XmlElement(name = "ItemRelation")
    protected List<RelatedItemResponseType> itemRelation;

    /**
     * Gets the value of the itemRelation property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the itemRelation property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getItemRelation().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RelatedItemResponseType }
     * 
     * 
     */
    public List<RelatedItemResponseType> getItemRelation() {
        if (itemRelation == null) {
            itemRelation = new ArrayList<RelatedItemResponseType>();
        }
        return this.itemRelation;
    }

}
