package com.ussco.ws.ecatalog.catalog._1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * this woudl describe a possible state that can be passed back to Smart Search, or used along with site map
 * 
 * <p>Java class for TargetStateType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TargetStateType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Filters" type="{http://ws.ussco.com/eCatalog/catalog/1}FilterListType" minOccurs="0"/>
 *         &lt;element name="ItemReferenceFilters" type="{http://ws.ussco.com/eCatalog/catalog/1}ItemReferenceFilterListType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TargetStateType", propOrder = { "filters", "itemReferenceFilters" })
public class TargetStateType {

    @XmlElement(name = "Filters")
    protected FilterListType filters;
    @XmlElement(name = "ItemReferenceFilters")
    protected ItemReferenceFilterListType itemReferenceFilters;

    /**
     * Gets the value of the filters property.
     * 
     * @return
     *     possible object is
     *     {@link FilterListType }
     *     
     */
    public FilterListType getFilters() {
        return filters;
    }

    /**
     * Sets the value of the filters property.
     * 
     * @param value
     *     allowed object is
     *     {@link FilterListType }
     *     
     */
    public void setFilters(FilterListType value) {
        this.filters = value;
    }

    /**
     * Gets the value of the itemReferenceFilters property.
     * 
     * @return
     *     possible object is
     *     {@link ItemReferenceFilterListType }
     *     
     */
    public ItemReferenceFilterListType getItemReferenceFilters() {
        return itemReferenceFilters;
    }

    /**
     * Sets the value of the itemReferenceFilters property.
     * 
     * @param value
     *     allowed object is
     *     {@link ItemReferenceFilterListType }
     *     
     */
    public void setItemReferenceFilters(ItemReferenceFilterListType value) {
        this.itemReferenceFilters = value;
    }

}
