package com.ussco.ws.ecatalog.catalog._1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * A supplies finder response
 * 
 * <p>Java class for SuppliesFinderResponseType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SuppliesFinderResponseType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AvailableFilters" type="{http://ws.ussco.com/eCatalog/catalog/1}FilterListType" minOccurs="0"/>
 *         &lt;element name="AppliedFilters" type="{http://ws.ussco.com/eCatalog/catalog/1}FilterListType" minOccurs="0"/>
 *         &lt;element name="ItemPage" type="{http://ws.ussco.com/eCatalog/catalog/1}ItemResponseType" minOccurs="0"/>
 *         &lt;element name="AvailableSorts" type="{http://ws.ussco.com/eCatalog/catalog/1}SortListType" minOccurs="0"/>
 *         &lt;element name="AppliedSort" type="{http://ws.ussco.com/eCatalog/catalog/1}SortType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SuppliesFinderResponseType", propOrder = { "availableFilters", "appliedFilters", "itemPage",
        "availableSorts", "appliedSort" })
public class SuppliesFinderResponseType {

    @XmlElement(name = "AvailableFilters")
    protected FilterListType availableFilters;
    @XmlElement(name = "AppliedFilters")
    protected FilterListType appliedFilters;
    @XmlElement(name = "ItemPage")
    protected ItemResponseType itemPage;
    @XmlElement(name = "AvailableSorts")
    protected SortListType availableSorts;
    @XmlElement(name = "AppliedSort")
    protected SortType appliedSort;

    /**
     * Gets the value of the availableFilters property.
     * 
     * @return
     *     possible object is
     *     {@link FilterListType }
     *     
     */
    public FilterListType getAvailableFilters() {
        return availableFilters;
    }

    /**
     * Sets the value of the availableFilters property.
     * 
     * @param value
     *     allowed object is
     *     {@link FilterListType }
     *     
     */
    public void setAvailableFilters(FilterListType value) {
        this.availableFilters = value;
    }

    /**
     * Gets the value of the appliedFilters property.
     * 
     * @return
     *     possible object is
     *     {@link FilterListType }
     *     
     */
    public FilterListType getAppliedFilters() {
        return appliedFilters;
    }

    /**
     * Sets the value of the appliedFilters property.
     * 
     * @param value
     *     allowed object is
     *     {@link FilterListType }
     *     
     */
    public void setAppliedFilters(FilterListType value) {
        this.appliedFilters = value;
    }

    /**
     * Gets the value of the itemPage property.
     * 
     * @return
     *     possible object is
     *     {@link ItemResponseType }
     *     
     */
    public ItemResponseType getItemPage() {
        return itemPage;
    }

    /**
     * Sets the value of the itemPage property.
     * 
     * @param value
     *     allowed object is
     *     {@link ItemResponseType }
     *     
     */
    public void setItemPage(ItemResponseType value) {
        this.itemPage = value;
    }

    /**
     * Gets the value of the availableSorts property.
     * 
     * @return
     *     possible object is
     *     {@link SortListType }
     *     
     */
    public SortListType getAvailableSorts() {
        return availableSorts;
    }

    /**
     * Sets the value of the availableSorts property.
     * 
     * @param value
     *     allowed object is
     *     {@link SortListType }
     *     
     */
    public void setAvailableSorts(SortListType value) {
        this.availableSorts = value;
    }

    /**
     * Gets the value of the appliedSort property.
     * 
     * @return
     *     possible object is
     *     {@link SortType }
     *     
     */
    public SortType getAppliedSort() {
        return appliedSort;
    }

    /**
     * Sets the value of the appliedSort property.
     * 
     * @param value
     *     allowed object is
     *     {@link SortType }
     *     
     */
    public void setAppliedSort(SortType value) {
        this.appliedSort = value;
    }

}
