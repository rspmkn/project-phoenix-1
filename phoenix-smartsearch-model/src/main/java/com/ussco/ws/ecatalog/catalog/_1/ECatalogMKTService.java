package com.ussco.ws.ecatalog.catalog._1;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Logger;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceFeature;

/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.1.6 in JDK 6
 * Generated source version: 2.1
 * 
 */
@WebServiceClient(name = "eCatalogMKTService", targetNamespace = "http://ws.ussco.com/eCatalog/catalog/1", wsdlLocation = "http://ws.ussco.com/eCatalog/catalog/1/eCatalog.wsdl")
public class ECatalogMKTService extends Service {

    private final static URL ECATALOGMKTSERVICE_WSDL_LOCATION;
    private final static Logger logger = Logger.getLogger(com.ussco.ws.ecatalog.catalog._1.ECatalogMKTService.class
            .getName());

    static {
        URL url = null;
        try {
            URL baseUrl;
            baseUrl = com.ussco.ws.ecatalog.catalog._1.ECatalogMKTService.class.getResource(".");
            url = new URL(baseUrl, "http://ws.ussco.com/eCatalog/catalog/1/eCatalog.wsdl");
        }
        catch (MalformedURLException e) {
            logger
                    .warning("Failed to create URL for the wsdl Location: 'http://ws.ussco.com/eCatalog/catalog/1/eCatalog.wsdl', retrying as a local file");
            logger.warning(e.getMessage());
        }
        ECATALOGMKTSERVICE_WSDL_LOCATION = url;
    }

    public ECatalogMKTService(URL wsdlLocation, QName serviceName) {
        super(wsdlLocation, serviceName);
    }

    public ECatalogMKTService() {
        super(ECATALOGMKTSERVICE_WSDL_LOCATION, new QName("http://ws.ussco.com/eCatalog/catalog/1",
                "eCatalogMKTService"));
    }

    /**
     * 
     * @return
     *     returns ECatalogMKTInterface
     */
    @WebEndpoint(name = "eCatalogMKTServiceSOAP")
    public ECatalogMKTInterface getECatalogMKTServiceSOAP() {
        return super.getPort(new QName("http://ws.ussco.com/eCatalog/catalog/1", "eCatalogMKTServiceSOAP"),
                ECatalogMKTInterface.class);
    }

    /**
     * 
     * @param features
     *     A list of {@link javax.xml.ws.WebServiceFeature} to configure on the proxy.  Supported features not in the <code>features</code> parameter will have their default values.
     * @return
     *     returns ECatalogMKTInterface
     */
    @WebEndpoint(name = "eCatalogMKTServiceSOAP")
    public ECatalogMKTInterface getECatalogMKTServiceSOAP(WebServiceFeature... features) {
        return super.getPort(new QName("http://ws.ussco.com/eCatalog/catalog/1", "eCatalogMKTServiceSOAP"),
                ECatalogMKTInterface.class, features);
    }

}
