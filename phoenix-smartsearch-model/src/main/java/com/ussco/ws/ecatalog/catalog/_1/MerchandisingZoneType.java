package com.ussco.ws.ecatalog.catalog._1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;

/**
 * A merchandising zone
 * 
 * <p>Java class for MerchandisingZoneType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MerchandisingZoneType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ZoneName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ZoneStyle" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ZoneType">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;enumeration value="BrandList"/>
 *               &lt;enumeration value="CategoryList"/>
 *               &lt;enumeration value="ItemList"/>
 *               &lt;enumeration value="OfferList"/>
 *               &lt;enumeration value="ResourceList"/>
 *               &lt;enumeration value="ImageList"/>
 *               &lt;enumeration value="MessageList"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="HeadingText" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="HeadingImage" type="{http://ws.ussco.com/eCatalog/catalog/1}ImageType" minOccurs="0"/>
 *         &lt;element name="HeadingTarget" type="{http://ws.ussco.com/eCatalog/catalog/1}TargetType" minOccurs="0"/>
 *         &lt;element name="SellingStatement" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Link" type="{http://ws.ussco.com/eCatalog/catalog/1}LinkType" minOccurs="0"/>
 *         &lt;choice>
 *           &lt;element name="MerchandisingBrand" type="{http://ws.ussco.com/eCatalog/catalog/1}MerchandisingZoneBrandEntryType" maxOccurs="unbounded" minOccurs="0"/>
 *           &lt;element name="MerchandisingCategory" type="{http://ws.ussco.com/eCatalog/catalog/1}MerchandisingZoneCategoryEntryType" maxOccurs="unbounded" minOccurs="0"/>
 *           &lt;element name="MerchandisingItem" type="{http://ws.ussco.com/eCatalog/catalog/1}MerchandisingZoneItemEntryType" maxOccurs="unbounded" minOccurs="0"/>
 *           &lt;element name="MerchandisingOffer" type="{http://ws.ussco.com/eCatalog/catalog/1}MerchandisingZoneOfferEntryType" maxOccurs="unbounded" minOccurs="0"/>
 *           &lt;element name="MerchandisingResource" type="{http://ws.ussco.com/eCatalog/catalog/1}MerchandisingZoneResourceEntryType" maxOccurs="unbounded" minOccurs="0"/>
 *           &lt;element name="MerchandisingImage" type="{http://ws.ussco.com/eCatalog/catalog/1}MerchandisingZoneImageEntryType" maxOccurs="unbounded" minOccurs="0"/>
 *           &lt;element name="MerchandisingMessage" type="{http://ws.ussco.com/eCatalog/catalog/1}MerchandisingZoneMessageEntryType" maxOccurs="unbounded" minOccurs="0"/>
 *           &lt;element name="Brand" type="{http://ws.ussco.com/eCatalog/catalog/1}BrandType" maxOccurs="unbounded" minOccurs="0"/>
 *           &lt;element name="Category" type="{http://ws.ussco.com/eCatalog/catalog/1}CategoryType" maxOccurs="unbounded" minOccurs="0"/>
 *           &lt;element name="Item" type="{http://ws.ussco.com/eCatalog/catalog/1}ItemType" maxOccurs="unbounded" minOccurs="0"/>
 *           &lt;element name="Offer" type="{http://ws.ussco.com/eCatalog/catalog/1}OfferResponseType" maxOccurs="unbounded" minOccurs="0"/>
 *           &lt;element name="Resource" type="{http://ws.ussco.com/eCatalog/catalog/1}ResourceType" maxOccurs="unbounded" minOccurs="0"/>
 *           &lt;element name="Image" type="{http://ws.ussco.com/eCatalog/catalog/1}MerchImageType" maxOccurs="unbounded" minOccurs="0"/>
 *           &lt;element name="Message" type="{http://ws.ussco.com/eCatalog/catalog/1}MessageType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;/choice>
 *         &lt;element name="Tags" type="{http://ws.ussco.com/eCatalog/catalog/1}TagType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MerchandisingZoneType", propOrder = { "zoneName", "zoneStyle", "zoneType", "headingText",
        "headingImage", "headingTarget", "sellingStatement", "link", "merchandisingBrand", "merchandisingCategory",
        "merchandisingItem", "merchandisingOffer", "merchandisingResource", "merchandisingImage",
        "merchandisingMessage", "brand", "category", "item", "offer", "resource", "image", "message", "tags" })
@XmlSeeAlso( { com.ussco.ws.ecatalog.catalog._1.MerchItemType.MerchandisingZone.class, ItemType.MerchandisingZone.class })
public class MerchandisingZoneType {

    @XmlElement(name = "ZoneName", required = true)
    protected String zoneName;
    @XmlElement(name = "ZoneStyle", required = true)
    protected String zoneStyle;
    @XmlElement(name = "ZoneType", required = true)
    protected String zoneType;
    @XmlElement(name = "HeadingText")
    protected String headingText;
    @XmlElement(name = "HeadingImage")
    protected ImageType headingImage;
    @XmlElement(name = "HeadingTarget")
    protected TargetType headingTarget;
    @XmlElement(name = "SellingStatement")
    protected String sellingStatement;
    @XmlElement(name = "Link")
    protected LinkType link;
    @XmlElement(name = "MerchandisingBrand")
    protected List<MerchandisingZoneBrandEntryType> merchandisingBrand;
    @XmlElement(name = "MerchandisingCategory")
    protected List<MerchandisingZoneCategoryEntryType> merchandisingCategory;
    @XmlElement(name = "MerchandisingItem")
    protected List<MerchandisingZoneItemEntryType> merchandisingItem;
    @XmlElement(name = "MerchandisingOffer")
    protected List<MerchandisingZoneOfferEntryType> merchandisingOffer;
    @XmlElement(name = "MerchandisingResource")
    protected List<MerchandisingZoneResourceEntryType> merchandisingResource;
    @XmlElement(name = "MerchandisingImage")
    protected List<MerchandisingZoneImageEntryType> merchandisingImage;
    @XmlElement(name = "MerchandisingMessage")
    protected List<MerchandisingZoneMessageEntryType> merchandisingMessage;
    @XmlElement(name = "Brand")
    protected List<BrandType> brand;
    @XmlElement(name = "Category")
    protected List<CategoryType> category;
    @XmlElement(name = "Item")
    protected List<ItemType> item;
    @XmlElement(name = "Offer")
    protected List<OfferResponseType> offer;
    @XmlElement(name = "Resource")
    protected List<ResourceType> resource;
    @XmlElement(name = "Image")
    protected List<MerchImageType> image;
    @XmlElement(name = "Message")
    protected List<MessageType> message;
    @XmlElement(name = "Tags")
    protected List<TagType> tags;

    /**
     * Gets the value of the zoneName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getZoneName() {
        return zoneName;
    }

    /**
     * Sets the value of the zoneName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setZoneName(String value) {
        this.zoneName = value;
    }

    /**
     * Gets the value of the zoneStyle property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getZoneStyle() {
        return zoneStyle;
    }

    /**
     * Sets the value of the zoneStyle property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setZoneStyle(String value) {
        this.zoneStyle = value;
    }

    /**
     * Gets the value of the zoneType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getZoneType() {
        return zoneType;
    }

    /**
     * Sets the value of the zoneType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setZoneType(String value) {
        this.zoneType = value;
    }

    /**
     * Gets the value of the headingText property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHeadingText() {
        return headingText;
    }

    /**
     * Sets the value of the headingText property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHeadingText(String value) {
        this.headingText = value;
    }

    /**
     * Gets the value of the headingImage property.
     * 
     * @return
     *     possible object is
     *     {@link ImageType }
     *     
     */
    public ImageType getHeadingImage() {
        return headingImage;
    }

    /**
     * Sets the value of the headingImage property.
     * 
     * @param value
     *     allowed object is
     *     {@link ImageType }
     *     
     */
    public void setHeadingImage(ImageType value) {
        this.headingImage = value;
    }

    /**
     * Gets the value of the headingTarget property.
     * 
     * @return
     *     possible object is
     *     {@link TargetType }
     *     
     */
    public TargetType getHeadingTarget() {
        return headingTarget;
    }

    /**
     * Sets the value of the headingTarget property.
     * 
     * @param value
     *     allowed object is
     *     {@link TargetType }
     *     
     */
    public void setHeadingTarget(TargetType value) {
        this.headingTarget = value;
    }

    /**
     * Gets the value of the sellingStatement property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSellingStatement() {
        return sellingStatement;
    }

    /**
     * Sets the value of the sellingStatement property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSellingStatement(String value) {
        this.sellingStatement = value;
    }

    /**
     * Gets the value of the link property.
     * 
     * @return
     *     possible object is
     *     {@link LinkType }
     *     
     */
    public LinkType getLink() {
        return link;
    }

    /**
     * Sets the value of the link property.
     * 
     * @param value
     *     allowed object is
     *     {@link LinkType }
     *     
     */
    public void setLink(LinkType value) {
        this.link = value;
    }

    /**
     * Gets the value of the merchandisingBrand property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the merchandisingBrand property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMerchandisingBrand().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link MerchandisingZoneBrandEntryType }
     * 
     * 
     */
    public List<MerchandisingZoneBrandEntryType> getMerchandisingBrand() {
        if (merchandisingBrand == null) {
            merchandisingBrand = new ArrayList<MerchandisingZoneBrandEntryType>();
        }
        return this.merchandisingBrand;
    }

    /**
     * Gets the value of the merchandisingCategory property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the merchandisingCategory property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMerchandisingCategory().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link MerchandisingZoneCategoryEntryType }
     * 
     * 
     */
    public List<MerchandisingZoneCategoryEntryType> getMerchandisingCategory() {
        if (merchandisingCategory == null) {
            merchandisingCategory = new ArrayList<MerchandisingZoneCategoryEntryType>();
        }
        return this.merchandisingCategory;
    }

    /**
     * Gets the value of the merchandisingItem property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the merchandisingItem property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMerchandisingItem().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link MerchandisingZoneItemEntryType }
     * 
     * 
     */
    public List<MerchandisingZoneItemEntryType> getMerchandisingItem() {
        if (merchandisingItem == null) {
            merchandisingItem = new ArrayList<MerchandisingZoneItemEntryType>();
        }
        return this.merchandisingItem;
    }

    /**
     * Gets the value of the merchandisingOffer property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the merchandisingOffer property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMerchandisingOffer().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link MerchandisingZoneOfferEntryType }
     * 
     * 
     */
    public List<MerchandisingZoneOfferEntryType> getMerchandisingOffer() {
        if (merchandisingOffer == null) {
            merchandisingOffer = new ArrayList<MerchandisingZoneOfferEntryType>();
        }
        return this.merchandisingOffer;
    }

    /**
     * Gets the value of the merchandisingResource property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the merchandisingResource property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMerchandisingResource().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link MerchandisingZoneResourceEntryType }
     * 
     * 
     */
    public List<MerchandisingZoneResourceEntryType> getMerchandisingResource() {
        if (merchandisingResource == null) {
            merchandisingResource = new ArrayList<MerchandisingZoneResourceEntryType>();
        }
        return this.merchandisingResource;
    }

    /**
     * Gets the value of the merchandisingImage property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the merchandisingImage property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMerchandisingImage().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link MerchandisingZoneImageEntryType }
     * 
     * 
     */
    public List<MerchandisingZoneImageEntryType> getMerchandisingImage() {
        if (merchandisingImage == null) {
            merchandisingImage = new ArrayList<MerchandisingZoneImageEntryType>();
        }
        return this.merchandisingImage;
    }

    /**
     * Gets the value of the merchandisingMessage property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the merchandisingMessage property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMerchandisingMessage().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link MerchandisingZoneMessageEntryType }
     * 
     * 
     */
    public List<MerchandisingZoneMessageEntryType> getMerchandisingMessage() {
        if (merchandisingMessage == null) {
            merchandisingMessage = new ArrayList<MerchandisingZoneMessageEntryType>();
        }
        return this.merchandisingMessage;
    }

    /**
     * Gets the value of the brand property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the brand property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getBrand().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link BrandType }
     * 
     * 
     */
    public List<BrandType> getBrand() {
        if (brand == null) {
            brand = new ArrayList<BrandType>();
        }
        return this.brand;
    }

    /**
     * Gets the value of the category property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the category property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCategory().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CategoryType }
     * 
     * 
     */
    public List<CategoryType> getCategory() {
        if (category == null) {
            category = new ArrayList<CategoryType>();
        }
        return this.category;
    }

    /**
     * Gets the value of the item property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the item property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getItem().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ItemType }
     * 
     * 
     */
    public List<ItemType> getItem() {
        if (item == null) {
            item = new ArrayList<ItemType>();
        }
        return this.item;
    }

    /**
     * Gets the value of the offer property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the offer property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getOffer().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link OfferResponseType }
     * 
     * 
     */
    public List<OfferResponseType> getOffer() {
        if (offer == null) {
            offer = new ArrayList<OfferResponseType>();
        }
        return this.offer;
    }

    /**
     * Gets the value of the resource property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the resource property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getResource().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ResourceType }
     * 
     * 
     */
    public List<ResourceType> getResource() {
        if (resource == null) {
            resource = new ArrayList<ResourceType>();
        }
        return this.resource;
    }

    /**
     * Gets the value of the image property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the image property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getImage().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link MerchImageType }
     * 
     * 
     */
    public List<MerchImageType> getImage() {
        if (image == null) {
            image = new ArrayList<MerchImageType>();
        }
        return this.image;
    }

    /**
     * Gets the value of the message property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the message property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMessage().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link MessageType }
     * 
     * 
     */
    public List<MessageType> getMessage() {
        if (message == null) {
            message = new ArrayList<MessageType>();
        }
        return this.message;
    }

    /**
     * Gets the value of the tags property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the tags property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTags().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TagType }
     * 
     * 
     */
    public List<TagType> getTags() {
        if (tags == null) {
            tags = new ArrayList<TagType>();
        }
        return this.tags;
    }

}
