package com.ussco.ws.ecatalog.catalog._1;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * A filter value
 * 
 * <p>Java class for FilterValueType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="FilterValueType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Value" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="AvailableResults" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *         &lt;element name="Brand" type="{http://ws.ussco.com/eCatalog/catalog/1}BrandType" minOccurs="0"/>
 *         &lt;element name="DidYouMean" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AutoSpellCorrect" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SubFilter" type="{http://ws.ussco.com/eCatalog/catalog/1}FilterType" minOccurs="0"/>
 *         &lt;element name="InternalId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="displayStyle">
 *         &lt;simpleType>
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *             &lt;enumeration value="Highlighted"/>
 *             &lt;enumeration value="Top"/>
 *             &lt;enumeration value="Full"/>
 *           &lt;/restriction>
 *         &lt;/simpleType>
 *       &lt;/attribute>
 *       &lt;attribute name="sequence" type="{http://www.w3.org/2001/XMLSchema}integer" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FilterValueType", propOrder = { "description", "value", "availableResults", "brand", "didYouMean",
        "autoSpellCorrect", "subFilter", "internalId" })
public class FilterValueType {

    @XmlElement(name = "Description")
    protected String description;
    @XmlElement(name = "Value", required = true)
    protected String value;
    @XmlElement(name = "AvailableResults")
    protected BigInteger availableResults;
    @XmlElement(name = "Brand")
    protected BrandType brand;
    @XmlElement(name = "DidYouMean")
    protected String didYouMean;
    @XmlElement(name = "AutoSpellCorrect")
    protected String autoSpellCorrect;
    @XmlElement(name = "SubFilter")
    protected FilterType subFilter;
    @XmlElement(name = "InternalId")
    protected String internalId;
    @XmlAttribute
    protected String displayStyle;
    @XmlAttribute
    protected BigInteger sequence;

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the value property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValue() {
        return value;
    }

    /**
     * Sets the value of the value property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValue(String value) {
        this.value = value;
    }

    /**
     * Gets the value of the availableResults property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getAvailableResults() {
        return availableResults;
    }

    /**
     * Sets the value of the availableResults property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setAvailableResults(BigInteger value) {
        this.availableResults = value;
    }

    /**
     * Gets the value of the brand property.
     * 
     * @return
     *     possible object is
     *     {@link BrandType }
     *     
     */
    public BrandType getBrand() {
        return brand;
    }

    /**
     * Sets the value of the brand property.
     * 
     * @param value
     *     allowed object is
     *     {@link BrandType }
     *     
     */
    public void setBrand(BrandType value) {
        this.brand = value;
    }

    /**
     * Gets the value of the didYouMean property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDidYouMean() {
        return didYouMean;
    }

    /**
     * Sets the value of the didYouMean property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDidYouMean(String value) {
        this.didYouMean = value;
    }

    /**
     * Gets the value of the autoSpellCorrect property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAutoSpellCorrect() {
        return autoSpellCorrect;
    }

    /**
     * Sets the value of the autoSpellCorrect property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAutoSpellCorrect(String value) {
        this.autoSpellCorrect = value;
    }

    /**
     * Gets the value of the subFilter property.
     * 
     * @return
     *     possible object is
     *     {@link FilterType }
     *     
     */
    public FilterType getSubFilter() {
        return subFilter;
    }

    /**
     * Sets the value of the subFilter property.
     * 
     * @param value
     *     allowed object is
     *     {@link FilterType }
     *     
     */
    public void setSubFilter(FilterType value) {
        this.subFilter = value;
    }

    /**
     * Gets the value of the internalId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInternalId() {
        return internalId;
    }

    /**
     * Sets the value of the internalId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInternalId(String value) {
        this.internalId = value;
    }

    /**
     * Gets the value of the displayStyle property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDisplayStyle() {
        return displayStyle;
    }

    /**
     * Sets the value of the displayStyle property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDisplayStyle(String value) {
        this.displayStyle = value;
    }

    /**
     * Gets the value of the sequence property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getSequence() {
        return sequence;
    }

    /**
     * Sets the value of the sequence property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setSequence(BigInteger value) {
        this.sequence = value;
    }

}
