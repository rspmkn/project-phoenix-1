package com.ussco.ws.ecatalog.catalog._1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * How much item information to present
 * 
 * <p>Java class for ItemResultStyle complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ItemResultStyle">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ItemResultStyle">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;enumeration value="Brief"/>
 *               &lt;enumeration value="List"/>
 *               &lt;enumeration value="Detail"/>
 *               &lt;enumeration value="Supplemental"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ItemResultStyle", propOrder = { "itemResultStyle" })
public class ItemResultStyle {

    @XmlElement(name = "ItemResultStyle", required = true)
    protected String itemResultStyle;

    /**
     * Gets the value of the itemResultStyle property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getItemResultStyle() {
        return itemResultStyle;
    }

    /**
     * Sets the value of the itemResultStyle property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setItemResultStyle(String value) {
        this.itemResultStyle = value;
    }

}
