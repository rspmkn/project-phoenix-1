package com.ussco.ws.ecatalog.catalog._1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="responseHeader" type="{http://ws.ussco.com/eCatalog/catalog/1}ResponseHeaderType"/>
 *         &lt;element name="SiteMapResponse" type="{http://ws.ussco.com/eCatalog/catalog/1}SiteMapResponseType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "responseHeader", "siteMapResponse" })
@XmlRootElement(name = "getSiteMapResponse")
public class GetSiteMapResponse {

    @XmlElement(required = true)
    protected ResponseHeaderType responseHeader;
    @XmlElement(name = "SiteMapResponse", required = true)
    protected SiteMapResponseType siteMapResponse;

    /**
     * Gets the value of the responseHeader property.
     * 
     * @return
     *     possible object is
     *     {@link ResponseHeaderType }
     *     
     */
    public ResponseHeaderType getResponseHeader() {
        return responseHeader;
    }

    /**
     * Sets the value of the responseHeader property.
     * 
     * @param value
     *     allowed object is
     *     {@link ResponseHeaderType }
     *     
     */
    public void setResponseHeader(ResponseHeaderType value) {
        this.responseHeader = value;
    }

    /**
     * Gets the value of the siteMapResponse property.
     * 
     * @return
     *     possible object is
     *     {@link SiteMapResponseType }
     *     
     */
    public SiteMapResponseType getSiteMapResponse() {
        return siteMapResponse;
    }

    /**
     * Sets the value of the siteMapResponse property.
     * 
     * @param value
     *     allowed object is
     *     {@link SiteMapResponseType }
     *     
     */
    public void setSiteMapResponse(SiteMapResponseType value) {
        this.siteMapResponse = value;
    }

}
