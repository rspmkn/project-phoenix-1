package com.ussco.ws.ecatalog.catalog._1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Common request header
 * 
 * <p>Java class for RequestHeaderType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RequestHeaderType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IdentificationRequest" type="{http://ws.ussco.com/eCatalog/catalog/1}IdentificationType"/>
 *         &lt;element name="IncExcList" type="{http://ws.ussco.com/eCatalog/catalog/1}ItemListType" minOccurs="0"/>
 *         &lt;element name="PriceMethod" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;enumeration value="Customer"/>
 *               &lt;enumeration value="PriceList"/>
 *               &lt;enumeration value="ReferenceSort"/>
 *               &lt;enumeration value="None"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="PriceList" type="{http://ws.ussco.com/eCatalog/catalog/1}ItemListType" minOccurs="0"/>
 *         &lt;element name="ContractList" type="{http://ws.ussco.com/eCatalog/catalog/1}ItemListType" minOccurs="0"/>
 *         &lt;element name="ResultStyle" type="{http://ws.ussco.com/eCatalog/catalog/1}ItemResultStyle" minOccurs="0"/>
 *         &lt;element name="CatNavDepth" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="2"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="IncludeDiscontinuedItem" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="IncludeSuggestedItem" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="GetGreenFilter" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="GetCountryOfOriginFilter" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="ZoneList" type="{http://ws.ussco.com/eCatalog/catalog/1}ZoneNameListType" minOccurs="0"/>
 *         &lt;element name="MerchandisingCompatibility" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RequestHeaderType", propOrder = { "identificationRequest", "incExcList", "priceMethod", "priceList",
        "contractList", "resultStyle", "catNavDepth", "includeDiscontinuedItem", "includeSuggestedItem",
        "getGreenFilter", "getCountryOfOriginFilter", "zoneList", "merchandisingCompatibility" })
public class RequestHeaderType {

    @XmlElement(name = "IdentificationRequest", required = true)
    protected IdentificationType identificationRequest;
    @XmlElement(name = "IncExcList")
    protected ItemListType incExcList;
    @XmlElement(name = "PriceMethod")
    protected String priceMethod;
    @XmlElement(name = "PriceList")
    protected ItemListType priceList;
    @XmlElement(name = "ContractList")
    protected ItemListType contractList;
    @XmlElement(name = "ResultStyle")
    protected ItemResultStyle resultStyle;
    @XmlElement(name = "CatNavDepth", defaultValue = "0")
    protected Integer catNavDepth;
    @XmlElement(name = "IncludeDiscontinuedItem", defaultValue = "false")
    protected Boolean includeDiscontinuedItem;
    @XmlElement(name = "IncludeSuggestedItem", defaultValue = "false")
    protected Boolean includeSuggestedItem;
    @XmlElement(name = "GetGreenFilter", defaultValue = "false")
    protected Boolean getGreenFilter;
    @XmlElement(name = "GetCountryOfOriginFilter", defaultValue = "false")
    protected Boolean getCountryOfOriginFilter;
    @XmlElement(name = "ZoneList")
    protected ZoneNameListType zoneList;
    @XmlElement(name = "MerchandisingCompatibility", defaultValue = "false")
    protected Boolean merchandisingCompatibility;

    /**
     * Gets the value of the identificationRequest property.
     * 
     * @return
     *     possible object is
     *     {@link IdentificationType }
     *     
     */
    public IdentificationType getIdentificationRequest() {
        return identificationRequest;
    }

    /**
     * Sets the value of the identificationRequest property.
     * 
     * @param value
     *     allowed object is
     *     {@link IdentificationType }
     *     
     */
    public void setIdentificationRequest(IdentificationType value) {
        this.identificationRequest = value;
    }

    /**
     * Gets the value of the incExcList property.
     * 
     * @return
     *     possible object is
     *     {@link ItemListType }
     *     
     */
    public ItemListType getIncExcList() {
        return incExcList;
    }

    /**
     * Sets the value of the incExcList property.
     * 
     * @param value
     *     allowed object is
     *     {@link ItemListType }
     *     
     */
    public void setIncExcList(ItemListType value) {
        this.incExcList = value;
    }

    /**
     * Gets the value of the priceMethod property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPriceMethod() {
        return priceMethod;
    }

    /**
     * Sets the value of the priceMethod property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPriceMethod(String value) {
        this.priceMethod = value;
    }

    /**
     * Gets the value of the priceList property.
     * 
     * @return
     *     possible object is
     *     {@link ItemListType }
     *     
     */
    public ItemListType getPriceList() {
        return priceList;
    }

    /**
     * Sets the value of the priceList property.
     * 
     * @param value
     *     allowed object is
     *     {@link ItemListType }
     *     
     */
    public void setPriceList(ItemListType value) {
        this.priceList = value;
    }

    /**
     * Gets the value of the contractList property.
     * 
     * @return
     *     possible object is
     *     {@link ItemListType }
     *     
     */
    public ItemListType getContractList() {
        return contractList;
    }

    /**
     * Sets the value of the contractList property.
     * 
     * @param value
     *     allowed object is
     *     {@link ItemListType }
     *     
     */
    public void setContractList(ItemListType value) {
        this.contractList = value;
    }

    /**
     * Gets the value of the resultStyle property.
     * 
     * @return
     *     possible object is
     *     {@link ItemResultStyle }
     *     
     */
    public ItemResultStyle getResultStyle() {
        return resultStyle;
    }

    /**
     * Sets the value of the resultStyle property.
     * 
     * @param value
     *     allowed object is
     *     {@link ItemResultStyle }
     *     
     */
    public void setResultStyle(ItemResultStyle value) {
        this.resultStyle = value;
    }

    /**
     * Gets the value of the catNavDepth property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCatNavDepth() {
        return catNavDepth;
    }

    /**
     * Sets the value of the catNavDepth property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCatNavDepth(Integer value) {
        this.catNavDepth = value;
    }

    /**
     * Gets the value of the includeDiscontinuedItem property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIncludeDiscontinuedItem() {
        return includeDiscontinuedItem;
    }

    /**
     * Sets the value of the includeDiscontinuedItem property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIncludeDiscontinuedItem(Boolean value) {
        this.includeDiscontinuedItem = value;
    }

    /**
     * Gets the value of the includeSuggestedItem property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIncludeSuggestedItem() {
        return includeSuggestedItem;
    }

    /**
     * Sets the value of the includeSuggestedItem property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIncludeSuggestedItem(Boolean value) {
        this.includeSuggestedItem = value;
    }

    /**
     * Gets the value of the getGreenFilter property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isGetGreenFilter() {
        return getGreenFilter;
    }

    /**
     * Sets the value of the getGreenFilter property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setGetGreenFilter(Boolean value) {
        this.getGreenFilter = value;
    }

    /**
     * Gets the value of the getCountryOfOriginFilter property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isGetCountryOfOriginFilter() {
        return getCountryOfOriginFilter;
    }

    /**
     * Sets the value of the getCountryOfOriginFilter property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setGetCountryOfOriginFilter(Boolean value) {
        this.getCountryOfOriginFilter = value;
    }

    /**
     * Gets the value of the zoneList property.
     * 
     * @return
     *     possible object is
     *     {@link ZoneNameListType }
     *     
     */
    public ZoneNameListType getZoneList() {
        return zoneList;
    }

    /**
     * Sets the value of the zoneList property.
     * 
     * @param value
     *     allowed object is
     *     {@link ZoneNameListType }
     *     
     */
    public void setZoneList(ZoneNameListType value) {
        this.zoneList = value;
    }

    /**
     * Gets the value of the merchandisingCompatibility property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isMerchandisingCompatibility() {
        return merchandisingCompatibility;
    }

    /**
     * Sets the value of the merchandisingCompatibility property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setMerchandisingCompatibility(Boolean value) {
        this.merchandisingCompatibility = value;
    }

}
