package com.ussco.ws.ecatalog.catalog._1;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;

/**
 * An image
 * 
 * <p>Java class for ImageType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ImageType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ImageURL">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="300"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="ImageType">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="30"/>
 *               &lt;enumeration value="AltItem1Large"/>
 *               &lt;enumeration value="AltItem1StandardA"/>
 *               &lt;enumeration value="AltItem1StandardB"/>
 *               &lt;enumeration value="AltItem1StandardC"/>
 *               &lt;enumeration value="AltItem1ThumbnailA"/>
 *               &lt;enumeration value="AltItem1ThumbnailB"/>
 *               &lt;enumeration value="AltItem1ThumbnailC"/>
 *               &lt;enumeration value="AltItem2Large"/>
 *               &lt;enumeration value="AltItem2StandardA"/>
 *               &lt;enumeration value="AltItem2StandardB"/>
 *               &lt;enumeration value="AltItem2StandardC"/>
 *               &lt;enumeration value="AltItem2ThumbnailA"/>
 *               &lt;enumeration value="AltItem2ThumbnailB"/>
 *               &lt;enumeration value="AltItem2ThumbnailC"/>
 *               &lt;enumeration value="AltItem3Large"/>
 *               &lt;enumeration value="AltItem3StandardA"/>
 *               &lt;enumeration value="AltItem3StandardB"/>
 *               &lt;enumeration value="AltItem3StandardC"/>
 *               &lt;enumeration value="AltItem3ThumbnailA"/>
 *               &lt;enumeration value="AltItem3ThumbnailB"/>
 *               &lt;enumeration value="AltItem3ThumbnailC"/>
 *               &lt;enumeration value="AltItem4Large"/>
 *               &lt;enumeration value="AltItem4StandardA"/>
 *               &lt;enumeration value="AltItem4StandardB"/>
 *               &lt;enumeration value="AltItem4StandardC"/>
 *               &lt;enumeration value="AltItem4ThumbnailA"/>
 *               &lt;enumeration value="AltItem4ThumbnailB"/>
 *               &lt;enumeration value="AltItem4ThumbnailC"/>
 *               &lt;enumeration value="AltItem5Large"/>
 *               &lt;enumeration value="AltItem5StandardA"/>
 *               &lt;enumeration value="AltItem5StandardB"/>
 *               &lt;enumeration value="AltItem5StandardC"/>
 *               &lt;enumeration value="AltItem5ThumbnailA"/>
 *               &lt;enumeration value="AltItem5ThumbnailB"/>
 *               &lt;enumeration value="AltItem5ThumbnailC"/>
 *               &lt;enumeration value="AltItem6Large"/>
 *               &lt;enumeration value="AltItem6StandardA"/>
 *               &lt;enumeration value="AltItem6StandardB"/>
 *               &lt;enumeration value="AltItem6StandardC"/>
 *               &lt;enumeration value="AltItem6ThumbnailA"/>
 *               &lt;enumeration value="AltItem6ThumbnailB"/>
 *               &lt;enumeration value="AltItem6ThumbnailC"/>
 *               &lt;enumeration value="AltItem7Large"/>
 *               &lt;enumeration value="AltItem7StandardA"/>
 *               &lt;enumeration value="AltItem7StandardB"/>
 *               &lt;enumeration value="AltItem7StandardC"/>
 *               &lt;enumeration value="AltItem7ThumbnailA"/>
 *               &lt;enumeration value="AltItem7ThumbnailB"/>
 *               &lt;enumeration value="AltItem7ThumbnailC"/>
 *               &lt;enumeration value="AltItem8Large"/>
 *               &lt;enumeration value="AltItem8StandardA"/>
 *               &lt;enumeration value="AltItem8StandardB"/>
 *               &lt;enumeration value="AltItem8StandardC"/>
 *               &lt;enumeration value="AltItem8ThumbnailA"/>
 *               &lt;enumeration value="AltItem8ThumbnailB"/>
 *               &lt;enumeration value="AltItem8ThumbnailC"/>
 *               &lt;enumeration value="AltItem9Large"/>
 *               &lt;enumeration value="AltItem9StandardA"/>
 *               &lt;enumeration value="AltItem9StandardB"/>
 *               &lt;enumeration value="AltItem9StandardC"/>
 *               &lt;enumeration value="AltItem9ThumbnailA"/>
 *               &lt;enumeration value="AltItem9ThumbnailB"/>
 *               &lt;enumeration value="AltItem9ThumbnailC"/>
 *               &lt;enumeration value="AltItem10Large"/>
 *               &lt;enumeration value="AltItem10StandardA"/>
 *               &lt;enumeration value="AltItem10StandardB"/>
 *               &lt;enumeration value="AltItem10StandardC"/>
 *               &lt;enumeration value="AltItem10ThumbnailA"/>
 *               &lt;enumeration value="AltItem10ThumbnailB"/>
 *               &lt;enumeration value="AltItem10ThumbnailC"/>
 *               &lt;enumeration value="BrandLarge"/>
 *               &lt;enumeration value="BrandStandard"/>
 *               &lt;enumeration value="BrandThumbnail"/>
 *               &lt;enumeration value="CategoryLarge"/>
 *               &lt;enumeration value="CategoryStandard"/>
 *               &lt;enumeration value="CategoryThumbnail"/>
 *               &lt;enumeration value="DealerA"/>
 *               &lt;enumeration value="DealerB"/>
 *               &lt;enumeration value="DealerC"/>
 *               &lt;enumeration value="Generic"/>
 *               &lt;enumeration value="ItemLarge"/>
 *               &lt;enumeration value="ItemStandardA"/>
 *               &lt;enumeration value="ItemStandardB"/>
 *               &lt;enumeration value="ItemStandardC"/>
 *               &lt;enumeration value="ItemThumbnailA"/>
 *               &lt;enumeration value="ItemThumbnailB"/>
 *               &lt;enumeration value="ItemThumbnailC"/>
 *               &lt;enumeration value="ResourceA"/>
 *               &lt;enumeration value="ResourceB"/>
 *               &lt;enumeration value="ResourceC"/>
 *               &lt;enumeration value="IconSmall"/>
 *               &lt;enumeration value="IconStandard"/>
 *               &lt;enumeration value="IconLarge"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="ImageHeight" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *         &lt;element name="ImageWidth" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *         &lt;element name="ImageTitle" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="150"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ImageType", propOrder = { "imageURL", "imageType", "imageHeight", "imageWidth", "imageTitle" })
@XmlSeeAlso( { com.ussco.ws.ecatalog.catalog._1.MerchItemType.ItemImage.class,
        com.ussco.ws.ecatalog.catalog._1.ItemType.ItemImage.class })
public class ImageType {

    @XmlElement(name = "ImageURL", required = true)
    protected String imageURL;
    @XmlElement(name = "ImageType", required = true)
    protected String imageType;
    @XmlElement(name = "ImageHeight")
    protected BigInteger imageHeight;
    @XmlElement(name = "ImageWidth")
    protected BigInteger imageWidth;
    @XmlElement(name = "ImageTitle")
    protected String imageTitle;

    /**
     * Gets the value of the imageURL property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getImageURL() {
        return imageURL;
    }

    /**
     * Sets the value of the imageURL property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setImageURL(String value) {
        this.imageURL = value;
    }

    /**
     * Gets the value of the imageType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getImageType() {
        return imageType;
    }

    /**
     * Sets the value of the imageType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setImageType(String value) {
        this.imageType = value;
    }

    /**
     * Gets the value of the imageHeight property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getImageHeight() {
        return imageHeight;
    }

    /**
     * Sets the value of the imageHeight property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setImageHeight(BigInteger value) {
        this.imageHeight = value;
    }

    /**
     * Gets the value of the imageWidth property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getImageWidth() {
        return imageWidth;
    }

    /**
     * Sets the value of the imageWidth property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setImageWidth(BigInteger value) {
        this.imageWidth = value;
    }

    /**
     * Gets the value of the imageTitle property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getImageTitle() {
        return imageTitle;
    }

    /**
     * Sets the value of the imageTitle property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setImageTitle(String value) {
        this.imageTitle = value;
    }

}
