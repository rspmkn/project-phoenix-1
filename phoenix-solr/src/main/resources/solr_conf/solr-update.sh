#!/bin/bash

#location of config zip, and target directories
ZIP_LOCATION=~/solr-conf-deploy.zip
UNZIP_TARGET=~/solr-conf-target
COLLECTION_TEMPDIR=~/collectionTemp
#gets the Zookeeper IP address from command line param
ZK_HOST=$1
#pulls the new config name, if it's specified
NEW_COLLECTION=$2

function setupConfig {
	echo "Executing update for collection $1"
	#sets up a temporary collection directory, with the default values
	rm -rf $COLLECTION_TEMPDIR
	mkdir $COLLECTION_TEMPDIR
	cp $UNZIP_TARGET/* $COLLECTION_TEMPDIR
	#if a directory exists for separate collections, copies its configs over (usually just the synonyms file)
	if [ -d "$UNZIP_TARGET/$1" ]; then
		cp $UNZIP_TARGET/$1/* $COLLECTION_TEMPDIR
	else
		echo "No custom configuration found in $1, using default"
	fi
	#runs the "upconfig" zkcli command to update Zookeeper
	/opt/solr/solrHome/cloud-scripts/zkcli.sh -cmd upconfig -zkhost $ZK_HOST:2181 -d $COLLECTION_TEMPDIR/ -n $1
}

if [ -z $ZK_HOST ]; then
	echo "You must specify the Zookeeper IP as the first parameter"
	exit 1
fi

if [ ! -f $ZIP_LOCATION ]; then
	echo "Could not find $ZIP_LOCATION"
	exit 1
fi

#unzips the configuration zip to the target directory
rm -rf $UNZIP_TARGET
mkdir $UNZIP_TARGET
unzip $ZIP_LOCATION -d $UNZIP_TARGET
if [ -z $NEW_COLLECTION ]; then
	#runs the "list" zkcli command and pulls the configName field, which gets the collection names
	#then iterates for each collection
	/opt/solr/solrHome/cloud-scripts/zkcli.sh -cmd list -zkhost $ZK_HOST:2181 | grep configName | grep -o [^\"]*_collection | while read -r collectionName ; do
		setupConfig $collectionName
	done
else
	setupConfig $NEW_COLLECTION
fi

