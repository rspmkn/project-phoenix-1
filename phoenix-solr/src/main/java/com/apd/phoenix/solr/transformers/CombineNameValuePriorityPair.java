package com.apd.phoenix.solr.transformers;

import java.math.BigInteger;
import java.util.Map;

public class CombineNameValuePriorityPair {

    public Object transformRow(Map<String, Object> row) {
        String name = (String) row.get("NAME");
        String value = (String) row.get("VALUE");
        BigInteger priority = (BigInteger) row.get("PRIORITY");
        row.remove("NAME");
        row.remove("VALUE");
        row.remove("PRIOIRITY");

        if (priority != null) {
            String filtersPriority = name + " | " + priority.toString();
            row.put("filtersPriority", filtersPriority);
        }

        String filters = name + " | " + value;
        row.put("filters", filters);

        return row;
    }
}
