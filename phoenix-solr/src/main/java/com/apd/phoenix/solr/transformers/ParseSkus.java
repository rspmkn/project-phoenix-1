package com.apd.phoenix.solr.transformers;

import java.math.BigInteger;
import java.util.Map;

public class ParseSkus {

    public Object transformRow(Map<String, Object> row, Map<Long, String> skuTypeMap) {
        Long typeId = ((BigInteger) row.get("TYPE_ID")).longValue();
        String name = skuTypeMap.get(typeId);
        String value = (String) row.get("VALUE");

        row.remove("TYPE_ID");
        row.remove("VALUE");

        if ("dealer".equals(name)) {
            row.put("APD_SKU", value);
        }
        else if ("manufacturer".equals(name)) {
            row.put("MANUFACTURER_SKU", value);
        }
        else if ("customer".equals(name)) {
            row.put("CUSTOMER_SKU", value);
        }
        else if ("vendor".equals(name)) {
            row.put("VENDOR_SKU", value);
        }

        return row;
    }
}
