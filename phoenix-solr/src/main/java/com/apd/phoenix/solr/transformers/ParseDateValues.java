package com.apd.phoenix.solr.transformers;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import oracle.sql.TIMESTAMP;

public class ParseDateValues {

    private static final Logger LOGGER = LoggerFactory.getLogger(ParseDateValues.class);

    private static final DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");

    public Object transformRow(Map<String, Object> row) {
        TIMESTAMP expireDateVal = (TIMESTAMP) row.get("COREEXPIREDATE");
        TIMESTAMP startDateVal = (TIMESTAMP) row.get("CORESTARTDATE");

        //Put the correct date object into the original column
        row.remove("COREEXPIREDATE");
        row.remove("CORESTARTDATE");

        if (expireDateVal == null || startDateVal == null) {
            return row;
        }

        try {
            Date expireDate = new Date(expireDateVal.timestampValue().getTime());
            Date startDate = new Date(startDateVal.timestampValue().getTime());
            row.put("COREEXPIREDATE", df.format(expireDate));
            row.put("CORESTARTDATE", df.format(startDate));
        }
        catch (Exception e) {
            LOGGER.error("An error occured:", e);
        }

        return row;
    }
}
