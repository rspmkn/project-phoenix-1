package com.apd.phoenix.solr.transformers;

import java.util.Map;
import org.apache.commons.lang.StringUtils;

public class ClassificationIcons {

    public Object transformRow(Map<String, Object> row) {
        String iconName = (String) row.get("NAME");
        String iconUrl = (String) row.get("ICONURL");
        String toolTipLabel = (String) row.get("TOOLTIPLABEL");
        String value = (String) row.get("VALUE");
        row.remove("NAME");
        row.remove("ICONURL");
        row.remove("TOOLTIPLABEL");
        row.remove("VALUE");

        if (StringUtils.isEmpty(value) || StringUtils.equalsIgnoreCase(value, "n")
                || StringUtils.equalsIgnoreCase(value, "no") || StringUtils.equalsIgnoreCase(value, "null")) {
            return row;
        }
        if (StringUtils.isNotBlank(iconUrl)) {
            if (StringUtils.isNotBlank(toolTipLabel)) {
                String mapValue = iconUrl + " | " + toolTipLabel;
                row.put("classificationIconMap", mapValue);
            }
            else {
                String mapValue = iconUrl + " | " + iconName;
                row.put("classificationIconMap", mapValue);
            }
        }

        return row;
    }
}
