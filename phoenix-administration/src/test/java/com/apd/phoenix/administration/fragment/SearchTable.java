package com.apd.phoenix.administration.fragment;

import java.util.List;
import org.jboss.arquillian.graphene.spi.annotations.Root;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import static org.jboss.arquillian.graphene.Graphene.guardAjax;

/* Page Fragment representing the standard two column Search Table */
public class SearchTable {

    private static final Logger LOGGER = LoggerFactory.getLogger(SearchTable.class);
    final static String SEARCH_FIELD_CSS = "input[name^='credentialPropertyTypeEditForm:credentialPropertyTypeSearch']";
    final static String SEARCH_LINK_CSS = "a[id^='credentialPropertyTypeEditForm:credentialPropertyTypeSearch']:nth-of-type(1)";

    final static String SEARCH_ROW_CSS = "tr[id^='credentialPropertyTypeEditForm:credentialPropertyTypeSearch:resultList']";

    //TODO: Was unable to get a CSS Selector to work for just the 1st column.  Either fix this or add more identifying elements to these divs.
    //final static String SEARCH_CELL_VALUE = "tr[id^='credentialPropertyTypeEditForm:credentialPropertyTypeSearch:resultList'] td div div.rf-edt-c-cnt:not(a)";

    final static String SEARCH_CELL_LINK = "tr[id^='credentialPropertyTypeEditForm:credentialPropertyTypeSearch:resultList'] td div div.rf-edt-c-cnt a";

    @Root
    private WebElement root;

    //Represents the Input Field used to filter searches
    @FindBy(css = SEARCH_FIELD_CSS)
    private WebElement searchField;

    //Represents the 'Search' link user clicks to retireve results
    @FindBy(css = SEARCH_LINK_CSS)
    private WebElement searchLink;

    //A List of the Search results (<tr>s returned) 
    @FindBy(css = SEARCH_ROW_CSS)
    private List<WebElement> resultList;

    //Represents the Values column of the Search Results
    //	@FindBy(css = SEARCH_CELL_VALUE)
    //	private List<WebElement> resultListValues;

    //Represents the "Action" column of 'Select' links from the search results
    @FindBy(css = SEARCH_CELL_LINK)
    private List<WebElement> resultListLinks;

    //Perform a search.  Empty String for searchValue returns all results.
    public void search(String searchValue) {
        this.searchField.clear();
        this.searchField.sendKeys(searchValue);

        //Long startTimeInMillis = System.currentTimeMillis();

        //Waits until result is returned before continuing
        guardAjax(this.searchLink).click();

        //Long endTimeInMillis = System.currentTimeMillis();

        //System.out.println("Time Taken for Save: " + (endTimeInMillis - startTimeInMillis));
    }

    //Returns the nth row.
    public WebElement findRowByIndex(int index) {
        return this.resultList.get(index);
    }

    //TODO: Test this.  Returns the first <tr> element that matches the search 'value'.
    public WebElement findRowByValue(String value) {
        WebElement foundRow = null;

        LOGGER.info("Entering Loop with Search String '" + value + "'...");

        for (WebElement we : resultList) {

            if (we.getText().contains(value)) {
                LOGGER.info("Found the Row we were looking for!");
                foundRow = we;
                break;
            }
        }

        return foundRow;
    }

    //Return the index of the first row that matches the search 'value'
    public Integer findRowIndexByValue(String value) {

        Integer foundIndex = null;

        LOGGER.info("Entering Loop with Search String '" + value + "'...");

        for (int i = 0; i < resultList.size(); i = i + 1) {

            if (resultList.get(i).getText().contains(value)) {
                LOGGER.info("Found the Row Index we were looking for!");
                foundIndex = i;
                break;
            }
        }

        return foundIndex;
    }

    //Selects a particular search result given the id.
    public void clickRow(int rowId) {

        guardAjax(this.resultListLinks.get(rowId)).click();

    }

    //Getters and Setters
    public WebElement getRoot() {
        return root;
    }

    public void setRoot(WebElement root) {
        this.root = root;
    }

    public WebElement getSearchField() {
        return searchField;
    }

    public void setSearchField(WebElement searchField) {
        this.searchField = searchField;
    }

    public WebElement getSearchLink() {
        return searchLink;
    }

    public void setSearchLink(WebElement searchLink) {
        this.searchLink = searchLink;
    }

    public List<WebElement> getResultList() {
        return resultList;
    }

    public void setResultList(List<WebElement> resultList) {
        this.resultList = resultList;
    }

    //	public List<WebElement> getResultListValues() {
    //		return resultListValues;
    //	}
    //
    //	public void setResultListValues(List<WebElement> resultListValues) {
    //		this.resultListValues = resultListValues;
    //	}

    public List<WebElement> getResultListLinks() {
        return resultListLinks;
    }

    public void setResultListLinks(List<WebElement> resultListLinks) {
        this.resultListLinks = resultListLinks;
    }

}
