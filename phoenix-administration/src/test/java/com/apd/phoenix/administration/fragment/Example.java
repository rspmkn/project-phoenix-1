package com.apd.phoenix.administration.fragment;

import org.jboss.arquillian.graphene.spi.annotations.Root;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class Example {

    @Root
    private WebElement root;

    @FindBy(css = "relativeLocatorOfThisPageFragmentPart")
    private WebElement otherPageFragmentPart;

    @FindBy(xpath = "relativeLocatorOfThisPageFragmentPart")
    private WebElement alsoPageFragmentPart;

    public void firstServiceEncapsulated() {
        otherPageFragmentPart.click();
    }

    public void secondServciceEncapsulated() {
        alsoPageFragmentPart.clear();
    }

    //other services and help methods
}
