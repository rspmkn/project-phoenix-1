package com.apd.phoenix.administration.fragment;

import java.util.List;
import org.jboss.arquillian.graphene.spi.annotations.Root;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/* Page Fragment representing the left-hand Navigation Bar on every page */
public class NavigationBar {

    private static final Logger LOGGER = LoggerFactory.getLogger(NavigationBar.class);
    //final static String NAV_BAR_MAIN_ELEMENT_CSS_CLASS = "div[class^='rf-pm-top-gr-hdr']";     
    final static String NAV_BAR_SUB_ELEMENT_CSS_CLASS = "div[class^='rf-pm-gr-hdr']";

    final static String NAV_BAR_ELEMENT_CSS = "div[id$='Group:hdr']";
    final static String NAV_BAR_LINK_CSS = "a[id$='Link']";

    @Root
    private WebElement root;

    //Represents All Categories on the Nav Bar, including sub categories; 
    @FindBy(css = NAV_BAR_ELEMENT_CSS)
    private List<WebElement> divs;

    //Represents only the subcategories in the Nav Bar, which are currently not visible.
    @FindBy(css = NAV_BAR_SUB_ELEMENT_CSS_CLASS)
    private List<WebElement> subDivs;

    //Represents all the links in the Nav Bar
    @FindBy(css = NAV_BAR_LINK_CSS)
    private List<WebElement> navLinks;

    //Find and return any category (Headings and SubHeadings)
    public WebElement findHeading(String searchString) {

        WebElement foundHeading = null;

        LOGGER.info("Entering Loop with Search String '" + searchString + "'...");

        for (WebElement we : this.divs) {
            if (we.getAttribute("id").contains(searchString)) {
                LOGGER.info("Found Main Div!");
                foundHeading = we;
                break;
            }
        }

        return foundHeading;
    }

    //Find and return a sub category
    public WebElement findSubHeading(String searchString) {

        WebElement foundSubDiv = null;

        //WebElement mainHeadingId = findMainHeading(heading);

        LOGGER.info("Entering Loop with Search String '" + searchString + "'...");

        for (WebElement we : this.subDivs) {

            if (we.getAttribute("id").contains(searchString)) {
                LOGGER.info("Found Sub Div!");
                foundSubDiv = we;
                break;
            }
        }

        return foundSubDiv;
    }

    // Find and return a Nav link
    public WebElement findNavLink(String searchString) {

        WebElement foundLink = null;

        LOGGER.info("Entering Loop with Search String '" + searchString + "'...");

        for (WebElement we : this.navLinks) {

            if (we.getAttribute("id").contains(searchString)) {
                LOGGER.info("Found Link!");
                foundLink = we;
                break;
            }
        }

        return foundLink;
    }

    public WebElement getRoot() {
        return root;
    }

    public void setRoot(WebElement root) {
        this.root = root;
    }

    public List<WebElement> getDivs() {
        return divs;
    }

    public void setDivs(List<WebElement> divs) {
        this.divs = divs;
    }

    public List<WebElement> getSubDivs() {
        return subDivs;
    }

    public void setSubDivs(List<WebElement> subDivs) {
        this.subDivs = subDivs;
    }

    public List<WebElement> getNavLinks() {

        return navLinks;
    }

    public void setNavLinks(List<WebElement> navLinks) {
        this.navLinks = navLinks;
    }

}
