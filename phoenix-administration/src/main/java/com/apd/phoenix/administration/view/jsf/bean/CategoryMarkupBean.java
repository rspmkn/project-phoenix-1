package com.apd.phoenix.administration.view.jsf.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateful;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.ConversationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import com.apd.phoenix.service.model.CategoryMarkup;
import com.apd.phoenix.service.model.Catalog;

/**
 * Backing bean for CategoryMarkup entities.
 * <p>
 * This class provides CRUD functionality for all CategoryMarkup entities. It focuses
 * purely on Java EE 6 standards (e.g. <tt>&#64;ConversationScoped</tt> for
 * state management, <tt>PersistenceContext</tt> for persistence,
 * <tt>CriteriaBuilder</tt> for searches) rather than introducing a CRUD framework or
 * custom base class.
 */

@Named
@Stateful
@ConversationScoped
@Deprecated
public class CategoryMarkupBean implements Serializable {

    private static final long serialVersionUID = 1L;

    /*
     * Support creating and retrieving CategoryMarkup entities
     */

    private Long id;

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    private CategoryMarkup categoryMarkup;

    public CategoryMarkup getCategoryMarkup() {
        return this.categoryMarkup;
    }

    @Inject
    private Conversation conversation;

    private EntityManager entityManager;

    public String create() {

        this.conversation.begin();
        return "create?faces-redirect=true";
    }

    public void retrieve() {

        if (FacesContext.getCurrentInstance().isPostback()) {
            return;
        }

        if (this.conversation.isTransient()) {
            this.conversation.begin();
        }

        if (this.id == null) {
            this.categoryMarkup = this.example;
        }
        else {
            this.categoryMarkup = findById(getId());
        }
    }

    public CategoryMarkup findById(Long id) {

        return this.entityManager.find(CategoryMarkup.class, id);
    }

    /*
     * Support updating and deleting CategoryMarkup entities
     */

    public String update() {
        this.conversation.end();

        try {
            if (this.id == null) {
                this.entityManager.persist(this.categoryMarkup);
                return "search?faces-redirect=true";
            }
            else {
                this.entityManager.merge(this.categoryMarkup);
                return "view?faces-redirect=true&id=" + this.categoryMarkup.getId();
            }
        }
        catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), e.getMessage()));
            return null;
        }
    }

    public String delete() {
        this.conversation.end();

        try {
            this.entityManager.remove(findById(getId()));
            this.entityManager.flush();
            return "search?faces-redirect=true";
        }
        catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), e.getMessage()));
            return null;
        }
    }

    /*
     * Support searching CategoryMarkup entities with pagination
     */

    private int page;
    private long count;
    private List<CategoryMarkup> pageItems;

    private CategoryMarkup example = new CategoryMarkup();

    public int getPage() {
        return this.page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getPageSize() {
        return 10;
    }

    public CategoryMarkup getExample() {
        return this.example;
    }

    public void setExample(CategoryMarkup example) {
        this.example = example;
    }

    public void search() {
        this.page = 0;
    }

    public void paginate() {

        CriteriaBuilder builder = this.entityManager.getCriteriaBuilder();

        // Populate this.count

        CriteriaQuery<Long> countCriteria = builder.createQuery(Long.class);
        Root<CategoryMarkup> root = countCriteria.from(CategoryMarkup.class);
        countCriteria = countCriteria.select(builder.count(root)).where(getSearchPredicates(root));
        this.count = this.entityManager.createQuery(countCriteria).getSingleResult();

        // Populate this.pageItems

        CriteriaQuery<CategoryMarkup> criteria = builder.createQuery(CategoryMarkup.class);
        root = criteria.from(CategoryMarkup.class);
        TypedQuery<CategoryMarkup> query = this.entityManager.createQuery(criteria.select(root).where(
                getSearchPredicates(root)));
        query.setFirstResult(this.page * getPageSize()).setMaxResults(getPageSize());
        this.pageItems = query.getResultList();
    }

    private Predicate[] getSearchPredicates(Root<CategoryMarkup> root) {

        CriteriaBuilder builder = this.entityManager.getCriteriaBuilder();
        List<Predicate> predicatesList = new ArrayList<Predicate>();

        Catalog catalog = this.example.getCatalog();
        if (catalog != null) {
            predicatesList.add(builder.equal(root.get("catalog"), catalog));
        }

        return predicatesList.toArray(new Predicate[predicatesList.size()]);
    }

    public List<CategoryMarkup> getPageItems() {
        return this.pageItems;
    }

    public long getCount() {
        return this.count;
    }

    /*
     * Support listing and POSTing back CategoryMarkup entities (e.g. from inside an
     * HtmlSelectOneMenu)
     */

    public List<CategoryMarkup> getAll() {

        CriteriaQuery<CategoryMarkup> criteria = this.entityManager.getCriteriaBuilder().createQuery(
                CategoryMarkup.class);
        return this.entityManager.createQuery(criteria.select(criteria.from(CategoryMarkup.class))).getResultList();
    }

    @Resource
    private SessionContext sessionContext;

    public Converter getConverter() {

        final CategoryMarkupBean ejbProxy = this.sessionContext.getBusinessObject(CategoryMarkupBean.class);

        return new Converter() {

            @Override
            public Object getAsObject(FacesContext context, UIComponent component, String value) {

                return ejbProxy.findById(Long.valueOf(value));
            }

            @Override
            public String getAsString(FacesContext context, UIComponent component, Object value) {

                if (value == null) {
                    return "";
                }

                return String.valueOf(((CategoryMarkup) value).getId());
            }
        };
    }

    /*
     * Support adding children to bidirectional, one-to-many tables
     */

    private CategoryMarkup add = new CategoryMarkup();

    public CategoryMarkup getAdd() {
        return this.add;
    }

    public CategoryMarkup getAdded() {
        CategoryMarkup added = this.add;
        this.add = new CategoryMarkup();
        return added;
    }
}