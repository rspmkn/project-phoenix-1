package com.apd.phoenix.administration.view.jsf.bean;

import java.util.List;
import javax.enterprise.context.ConversationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import com.apd.phoenix.service.persistence.jpa.PaymentTypeDao;
import com.apd.phoenix.service.model.PaymentType;
import com.apd.phoenix.web.searchbeans.AbstractSearchBean;
import java.io.Serializable;

@Named
@ConversationScoped
public class PaymentTypeSearchBean extends AbstractSearchBean<PaymentType> implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 3789958454021131699L;
    @Inject
    PaymentTypeDao bp;

    @Override
    public String[] getCriteria() {
        String[] toReturn = { "Name" };
        return toReturn;
    }

    @Override
    protected List<PaymentType> search() {
        PaymentType search = new PaymentType();
        search.setName(this.getValues().get(0).getValue());
        return bp.searchByExample(search, 0, 0);
    }

}
