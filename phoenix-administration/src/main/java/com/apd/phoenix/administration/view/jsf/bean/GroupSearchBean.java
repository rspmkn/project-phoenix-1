package com.apd.phoenix.administration.view.jsf.bean;

import java.util.List;
import javax.enterprise.context.ConversationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import com.apd.phoenix.service.model.Group;
import com.apd.phoenix.service.persistence.jpa.GroupDao;
import com.apd.phoenix.web.searchbeans.AbstractSearchBean;
import java.io.Serializable;

@Named
@ConversationScoped
public class GroupSearchBean extends AbstractSearchBean<Group> implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 8648562873226499609L;
    @Inject
    GroupDao dao;

    @Override
    public String[] getCriteria() {
        String[] toReturn = { "Name" };
        return toReturn;
    }

    @Override
    protected List<Group> search() {
        Group search = new Group();
        search.setName(this.getValues().get(0).getValue());
        return dao.searchByExample(search, 0, 0);
    }

}
