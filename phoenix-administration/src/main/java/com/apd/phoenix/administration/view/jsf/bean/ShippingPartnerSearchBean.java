package com.apd.phoenix.administration.view.jsf.bean;

import java.util.List;
import javax.enterprise.context.ConversationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import com.apd.phoenix.service.persistence.jpa.ShippingPartnerDao;
import com.apd.phoenix.service.model.ShippingPartner;
import com.apd.phoenix.web.searchbeans.AbstractSearchBean;
import java.io.Serializable;

@Named
@ConversationScoped
public class ShippingPartnerSearchBean extends AbstractSearchBean<ShippingPartner> implements Serializable {

    @Inject
    ShippingPartnerDao dao;

    @Override
    public String[] getCriteria() {
        String[] toReturn = { "Name" };
        return toReturn;
    }

    @Override
    protected List<ShippingPartner> search() {
        ShippingPartner search = new ShippingPartner();
        search.setName(this.getValues().get(0).getValue());
        return dao.searchByExample(search, 0, 0);
    }

}
