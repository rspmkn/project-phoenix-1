/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.apd.phoenix.administration.view.jsf.bean;

import com.apd.phoenix.service.model.Desktop;
import com.apd.phoenix.service.persistence.jpa.DesktopDao;
import com.apd.phoenix.web.searchbeans.DesktopSearchBean;
import java.io.Serializable;
import java.util.List;
import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateful;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.ConversationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author anicholson
 */

@Named
@Stateful
@ConversationScoped
public class DesktopBean implements Serializable {

    private static final long serialVersionUID = 1l;

    public void validateSelection(FacesContext context, UIComponent component, Object value) {
        if (this.currentInstance.getId() == null) {
            ((UIInput) component).setValid(false);
            FacesMessage message = new FacesMessage();
            message.setSeverity(FacesMessage.SEVERITY_ERROR);
            message.setSummary("Select Desktop to edit");
            message.setDetail("You need to select a Desktop to edit.");
            context.addMessage(component.getClientId(), message);
            return;
        }
    }

    private Long id;

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    private Desktop currentInstance;

    public Desktop getDesktop() {
        return this.currentInstance;
    }

    @Inject
    private DesktopDao dao;

    @Inject
    private DesktopSearchBean searchBean;

    @Inject
    private Conversation conversation;

    public String create() {
        this.conversation.begin();
        return "desktopAdd?faces-redirect=true";
    }

    public void retrieve() {
        if (this.searchBean.getSelection() != null) {
            this.currentInstance = this.searchBean.getSelection();
            this.searchBean.setSelection(null);
        }

        if (FacesContext.getCurrentInstance().isPostback()) {
            return;
        }

        if (this.conversation.isTransient()) {
            this.conversation.begin();
        }

        if (this.id == null) {
            this.currentInstance = this.example;
        }
        else {
            this.currentInstance = dao.findById(id, Desktop.class);
        }

    }

    /*
     * Support updating and deleting Desktop entities
     */

    public String update() {
        if (!validToUpdate()) {
            return null;
        }

        if (!this.conversation.isTransient()) {
            this.conversation.end();
        }
        try {

            if (currentInstance.getId() == null) {
                dao.create(this.currentInstance);
                return "desktopAdd?faces-redirect=true";
            }
            else {
                dao.update(this.currentInstance);
                return "desktopEdit?faces-redirect=true";
            }
        }
        catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), e.getMessage()));
            return null;
        }
    }

    public String delete() {
        this.conversation.end();
        try {
            dao.delete(this.currentInstance.getId(), Desktop.class);
            return "desktopEdit?faces-redirect=true";
        }
        catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), e.getMessage()));
            return null;
        }
    }

    /*
     * Support searching Desktop entities with pagination
     */

    private int page;
    private long count;
    private List<Desktop> pageItems;

    private Desktop example = new Desktop();

    public int getPage() {
        return this.page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getPageSize() {
        return 10;
    }

    public Desktop getExample() {
        return this.example;
    }

    public void setExample(Desktop example) {
        this.example = example;
    }

    public void search() {
        this.page = 0;
    }

    public void paginate() {
        this.pageItems = dao.searchByExample(this.example, getPage() * getPageSize(), getPageSize());

        this.count = dao.resultQuantity(this.example);
    }

    public List<Desktop> getPageItems() {
        return this.pageItems;
    }

    public long getCount() {
        return this.count;
    }

    /*
     * Support listing and POSTing back Desktop entities (e.g. from inside an
     * HtmlSelectOneMenu)
     */

    public List<Desktop> getAll() {

        return dao.findAll(Desktop.class, 0, 0);
    }

    public List<Desktop> searchByExample(Desktop search) {
        return dao.searchByExample(search, 0, 0);
    }

    public List<Desktop> searchByString(String s) {
        Desktop search = new Desktop();
        search.setName(s);
        return searchByExample(search);
    }

    @Resource
    private SessionContext sessionContext;

    public Converter getConverter() {

        return new Converter() {

            @Override
            public Object getAsObject(FacesContext context, UIComponent component, String value) {
                return dao.findById(Long.valueOf(value), Desktop.class);
            }

            @Override
            public String getAsString(FacesContext context, UIComponent component, Object value) {

                if (value == null) {
                    return "";
                }

                return String.valueOf(((Desktop) value).getId());
            }
        };
    }

    /*
     * Support adding children to bidirectional, one-to-many tables
     */

    private Desktop add = new Desktop();

    public Desktop getAdd() {
        return this.add;
    }

    public Desktop getAdded() {
        Desktop added = this.add;
        this.add = new Desktop();
        return added;
    }

    private boolean validToUpdate() {
        Desktop searchDesktop = new Desktop();
        searchDesktop.setName(this.currentInstance.getName());
        List<Desktop> searchList = this.dao.searchByExactExample(searchDesktop, 0, 0);
        if (searchList.size() != 0 && !searchList.get(0).getId().equals(this.currentInstance.getId())) {
            FacesMessage message = new FacesMessage();
            message.setSeverity(FacesMessage.SEVERITY_ERROR);
            message.setSummary("Name already exists");
            message.setDetail("A desktop already exists with the specified name.");
            FacesContext.getCurrentInstance().addMessage(null, message);
            return false;
        }
        return true;
    }
}
