package com.apd.phoenix.administration.view.jsf.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateful;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.ConversationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import com.apd.phoenix.service.model.ItemImage;
import com.apd.phoenix.service.model.ImageType;
import com.apd.phoenix.service.model.Item;

/**
 * Backing bean for ItemImage entities.
 * <p>
 * This class provides CRUD functionality for all ItemImage entities. It focuses
 * purely on Java EE 6 standards (e.g. <tt>&#64;ConversationScoped</tt> for
 * state management, <tt>PersistenceContext</tt> for persistence,
 * <tt>CriteriaBuilder</tt> for searches) rather than introducing a CRUD framework or
 * custom base class.
 */

@Named
@Stateful
@ConversationScoped
@Deprecated
public class ItemImageBean implements Serializable {

    private static final long serialVersionUID = 1L;

    /*
     * Support creating and retrieving ItemImage entities
     */

    private Long id;

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    private ItemImage itemImage;

    public ItemImage getItemImage() {
        return this.itemImage;
    }

    @Inject
    private Conversation conversation;

    private EntityManager entityManager;

    public String create() {

        this.conversation.begin();
        return "create?faces-redirect=true";
    }

    public void retrieve() {

        if (FacesContext.getCurrentInstance().isPostback()) {
            return;
        }

        if (this.conversation.isTransient()) {
            this.conversation.begin();
        }

        if (this.id == null) {
            this.itemImage = this.example;
        }
        else {
            this.itemImage = findById(getId());
        }
    }

    public ItemImage findById(Long id) {

        return this.entityManager.find(ItemImage.class, id);
    }

    /*
     * Support updating and deleting ItemImage entities
     */

    public String update() {
        this.conversation.end();

        try {
            if (this.id == null) {
                this.entityManager.persist(this.itemImage);
                return "search?faces-redirect=true";
            }
            else {
                this.entityManager.merge(this.itemImage);
                return "view?faces-redirect=true&id=" + this.itemImage.getId();
            }
        }
        catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), e.getMessage()));
            return null;
        }
    }

    public String delete() {
        this.conversation.end();

        try {
            this.entityManager.remove(findById(getId()));
            this.entityManager.flush();
            return "search?faces-redirect=true";
        }
        catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), e.getMessage()));
            return null;
        }
    }

    /*
     * Support searching ItemImage entities with pagination
     */

    private int page;
    private long count;
    private List<ItemImage> pageItems;

    private ItemImage example = new ItemImage();

    public int getPage() {
        return this.page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getPageSize() {
        return 10;
    }

    public ItemImage getExample() {
        return this.example;
    }

    public void setExample(ItemImage example) {
        this.example = example;
    }

    public void search() {
        this.page = 0;
    }

    public void paginate() {

        CriteriaBuilder builder = this.entityManager.getCriteriaBuilder();

        // Populate this.count

        CriteriaQuery<Long> countCriteria = builder.createQuery(Long.class);
        Root<ItemImage> root = countCriteria.from(ItemImage.class);
        countCriteria = countCriteria.select(builder.count(root)).where(getSearchPredicates(root));
        this.count = this.entityManager.createQuery(countCriteria).getSingleResult();

        // Populate this.pageItems

        CriteriaQuery<ItemImage> criteria = builder.createQuery(ItemImage.class);
        root = criteria.from(ItemImage.class);
        TypedQuery<ItemImage> query = this.entityManager.createQuery(criteria.select(root).where(
                getSearchPredicates(root)));
        query.setFirstResult(this.page * getPageSize()).setMaxResults(getPageSize());
        this.pageItems = query.getResultList();
    }

    private Predicate[] getSearchPredicates(Root<ItemImage> root) {

        CriteriaBuilder builder = this.entityManager.getCriteriaBuilder();
        List<Predicate> predicatesList = new ArrayList<Predicate>();

        String imageUrl = this.example.getImageUrl();
        if (imageUrl != null && !"".equals(imageUrl)) {
            predicatesList.add(builder.like(root.<String> get("imageUrl"), '%' + imageUrl + '%'));
        }
        Item item = this.example.getItem();
        if (item != null) {
            predicatesList.add(builder.equal(root.get("item"), item));
        }
        ImageType imageType = this.example.getImageType();
        if (imageType != null) {
            predicatesList.add(builder.equal(root.get("imageType"), imageType));
        }

        return predicatesList.toArray(new Predicate[predicatesList.size()]);
    }

    public List<ItemImage> getPageItems() {
        return this.pageItems;
    }

    public long getCount() {
        return this.count;
    }

    /*
     * Support listing and POSTing back ItemImage entities (e.g. from inside an
     * HtmlSelectOneMenu)
     */

    public List<ItemImage> getAll() {

        CriteriaQuery<ItemImage> criteria = this.entityManager.getCriteriaBuilder().createQuery(ItemImage.class);
        return this.entityManager.createQuery(criteria.select(criteria.from(ItemImage.class))).getResultList();
    }

    @Resource
    private SessionContext sessionContext;

    public Converter getConverter() {

        final ItemImageBean ejbProxy = this.sessionContext.getBusinessObject(ItemImageBean.class);

        return new Converter() {

            @Override
            public Object getAsObject(FacesContext context, UIComponent component, String value) {

                return ejbProxy.findById(Long.valueOf(value));
            }

            @Override
            public String getAsString(FacesContext context, UIComponent component, Object value) {

                if (value == null) {
                    return "";
                }

                return String.valueOf(((ItemImage) value).getId());
            }
        };
    }

    /*
     * Support adding children to bidirectional, one-to-many tables
     */

    private ItemImage add = new ItemImage();

    public ItemImage getAdd() {
        return this.add;
    }

    public ItemImage getAdded() {
        ItemImage added = this.add;
        this.add = new ItemImage();
        return added;
    }
}