package com.apd.phoenix.administration.view.jsf.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateful;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.ConversationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import com.apd.phoenix.service.model.CatalogXItemXItemPropertyType;
import com.apd.phoenix.service.model.CatalogXItem;
import com.apd.phoenix.service.model.ItemPropertyType;

/**
 * Backing bean for CatalogXItemXCatalogXItemPropertyType entities.
 * <p>
 * This class provides CRUD functionality for all CatalogXItemXCatalogXItemPropertyType entities. It focuses
 * purely on Java EE 6 standards (e.g. <tt>&#64;ConversationScoped</tt> for
 * state management, <tt>PersistenceContext</tt> for persistence,
 * <tt>CriteriaBuilder</tt> for searches) rather than introducing a CRUD framework or
 * custom base class.
 */

@Named
@Stateful
@ConversationScoped
@Deprecated
public class CatalogXItemXItemPropertyTypeBean implements Serializable {

    private static final long serialVersionUID = 1L;

    /*
     * Support creating and retrieving CatalogXItemXCatalogXItemPropertyType entities
     */

    private Long id;

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    private CatalogXItemXItemPropertyType catalogXItemXCatalogXItemPropertyType;

    public CatalogXItemXItemPropertyType getCatalogXItemXCatalogXItemPropertyType() {
        return this.catalogXItemXCatalogXItemPropertyType;
    }

    @Inject
    private Conversation conversation;

    private EntityManager entityManager;

    public String create() {

        this.conversation.begin();
        return "create?faces-redirect=true";
    }

    public void retrieve() {

        if (FacesContext.getCurrentInstance().isPostback()) {
            return;
        }

        if (this.conversation.isTransient()) {
            this.conversation.begin();
        }

        if (this.id == null) {
            this.catalogXItemXCatalogXItemPropertyType = this.example;
        }
        else {
            this.catalogXItemXCatalogXItemPropertyType = findById(getId());
        }
    }

    public CatalogXItemXItemPropertyType findById(Long id) {

        return this.entityManager.find(CatalogXItemXItemPropertyType.class, id);
    }

    /*
     * Support updating and deleting CatalogXItemXCatalogXItemPropertyType entities
     */

    public String update() {
        this.conversation.end();

        try {
            if (this.id == null) {
                this.entityManager.persist(this.catalogXItemXCatalogXItemPropertyType);
                return "search?faces-redirect=true";
            }
            else {
                this.entityManager.merge(this.catalogXItemXCatalogXItemPropertyType);
                return "view?faces-redirect=true&id=" + this.catalogXItemXCatalogXItemPropertyType.getId();
            }
        }
        catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), e.getMessage()));
            return null;
        }
    }

    public String delete() {
        this.conversation.end();

        try {
            this.entityManager.remove(findById(getId()));
            this.entityManager.flush();
            return "search?faces-redirect=true";
        }
        catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), e.getMessage()));
            return null;
        }
    }

    /*
     * Support searching CatalogXItemXCatalogXItemPropertyType entities with pagination
     */

    private int page;
    private long count;
    private List<CatalogXItemXItemPropertyType> pageItems;

    private CatalogXItemXItemPropertyType example = new CatalogXItemXItemPropertyType();

    public int getPage() {
        return this.page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getPageSize() {
        return 10;
    }

    public CatalogXItemXItemPropertyType getExample() {
        return this.example;
    }

    public void setExample(CatalogXItemXItemPropertyType example) {
        this.example = example;
    }

    public void search() {
        this.page = 0;
    }

    public void paginate() {

        CriteriaBuilder builder = this.entityManager.getCriteriaBuilder();

        // Populate this.count

        CriteriaQuery<Long> countCriteria = builder.createQuery(Long.class);
        Root<CatalogXItemXItemPropertyType> root = countCriteria.from(CatalogXItemXItemPropertyType.class);
        countCriteria = countCriteria.select(builder.count(root)).where(getSearchPredicates(root));
        this.count = this.entityManager.createQuery(countCriteria).getSingleResult();

        // Populate this.pageItems

        CriteriaQuery<CatalogXItemXItemPropertyType> criteria = builder
                .createQuery(CatalogXItemXItemPropertyType.class);
        root = criteria.from(CatalogXItemXItemPropertyType.class);
        TypedQuery<CatalogXItemXItemPropertyType> query = this.entityManager.createQuery(criteria.select(root).where(
                getSearchPredicates(root)));
        query.setFirstResult(this.page * getPageSize()).setMaxResults(getPageSize());
        this.pageItems = query.getResultList();
    }

    private Predicate[] getSearchPredicates(Root<CatalogXItemXItemPropertyType> root) {

        CriteriaBuilder builder = this.entityManager.getCriteriaBuilder();
        List<Predicate> predicatesList = new ArrayList<Predicate>();

        ItemPropertyType type = this.example.getType();
        if (type != null) {
            predicatesList.add(builder.equal(root.get("type"), type));
        }
        String value = this.example.getValue();
        if (value != null && !"".equals(value)) {
            predicatesList.add(builder.like(root.<String> get("value"), '%' + value + '%'));
        }

        return predicatesList.toArray(new Predicate[predicatesList.size()]);
    }

    public List<CatalogXItemXItemPropertyType> getPageItems() {
        return this.pageItems;
    }

    public long getCount() {
        return this.count;
    }

    /*
     * Support listing and POSTing back CatalogXItemXCatalogXItemPropertyType entities (e.g. from inside an
     * HtmlSelectOneMenu)
     */

    public List<CatalogXItemXItemPropertyType> getAll() {

        CriteriaQuery<CatalogXItemXItemPropertyType> criteria = this.entityManager.getCriteriaBuilder().createQuery(
                CatalogXItemXItemPropertyType.class);
        return this.entityManager.createQuery(criteria.select(criteria.from(CatalogXItemXItemPropertyType.class)))
                .getResultList();
    }

    @Resource
    private SessionContext sessionContext;

    public Converter getConverter() {

        final CatalogXItemXItemPropertyTypeBean ejbProxy = this.sessionContext
                .getBusinessObject(CatalogXItemXItemPropertyTypeBean.class);

        return new Converter() {

            @Override
            public Object getAsObject(FacesContext context, UIComponent component, String value) {

                return ejbProxy.findById(Long.valueOf(value));
            }

            @Override
            public String getAsString(FacesContext context, UIComponent component, Object value) {

                if (value == null) {
                    return "";
                }

                return String.valueOf(((CatalogXItemXItemPropertyType) value).getId());
            }
        };
    }

    /*
     * Support adding children to bidirectional, one-to-many tables
     */

    private CatalogXItemXItemPropertyType add = new CatalogXItemXItemPropertyType();

    public CatalogXItemXItemPropertyType getAdd() {
        return this.add;
    }

    public CatalogXItemXItemPropertyType getAdded() {
        CatalogXItemXItemPropertyType added = this.add;
        this.add = new CatalogXItemXItemPropertyType();
        return added;
    }
}