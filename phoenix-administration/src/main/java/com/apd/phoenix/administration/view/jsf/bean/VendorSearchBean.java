package com.apd.phoenix.administration.view.jsf.bean;

import java.io.Serializable;
import java.util.List;
import javax.enterprise.context.ConversationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import com.apd.phoenix.service.business.VendorBp;
import com.apd.phoenix.service.model.Vendor;
import com.apd.phoenix.web.searchbeans.AbstractSearchBean;

@Named
@ConversationScoped
public class VendorSearchBean extends AbstractSearchBean<Vendor> implements Serializable {

    private static final long serialVersionUID = -2861890677539398750L;

    @Inject
    VendorBp bp;

    @Override
    public String[] getCriteria() {
        String[] toReturn = { "Name" };
        return toReturn;
    }

    @Override
    protected List<Vendor> search() {
        Vendor search = new Vendor();
        search.setName(this.getValues().get(0).getValue());
        return bp.searchByExample(search, 0, 0);
    }
}
