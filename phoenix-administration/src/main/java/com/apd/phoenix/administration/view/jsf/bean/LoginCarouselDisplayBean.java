package com.apd.phoenix.administration.view.jsf.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateful;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.ConversationScoped;
import javax.faces.convert.Converter;
import javax.inject.Inject;
import javax.inject.Named;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.apd.phoenix.service.model.LoginCarouselDisplay;
import com.apd.phoenix.service.persistence.jpa.LoginCarouselDisplayDao;
import com.apd.phoenix.web.AbstractControllerBean;

@Named
@Stateful
@ConversationScoped
public class LoginCarouselDisplayBean extends AbstractControllerBean<LoginCarouselDisplay> implements Serializable {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(LoginCarouselDisplayBean.class);

    private static final long serialVersionUID = 1L;

    @Inject
    private LoginCarouselDisplayDao dao;

    @Inject
    private Conversation conversation;

    private List<LoginCarouselDisplay> loginCarouselDisplayList;

    private List<LoginCarouselDisplay> toDelete = new ArrayList<>();

    private LoginCarouselDisplay currentLoginCarouselDisplay;

    public void retrieve() {
        if (this.loginCarouselDisplayList == null) {
            this.loginCarouselDisplayList = this.dao.findAll(LoginCarouselDisplay.class, 0, 0);
        }
        if (this.conversation.isTransient()) {
            this.conversation.begin();
        }
    }

    public String delete() {
        //do nothing
        return null;
    }

    public String update() {
        try {
            for (LoginCarouselDisplay loginCarouselDisplay : this.loginCarouselDisplayList) {
                this.dao.update(loginCarouselDisplay);
            }
            for (LoginCarouselDisplay loginCarouselDisplay : this.toDelete) {
            	if (loginCarouselDisplay.getId() != null) {
            		this.dao.delete(loginCarouselDisplay.getId(), LoginCarouselDisplay.class);
            	}
            }
            if (!this.conversation.isTransient()) {
                this.conversation.end();
            }
            return "globalLoginCarouselDisplayEdit?faces-redirect=true";
        }
        catch (Exception e) {
            return null;
        }
    }

    public List<LoginCarouselDisplay> getLoginCarouselDisplayList() {
        return loginCarouselDisplayList;
    }

    public void setLoginCarouselDisplayList(List<LoginCarouselDisplay> loginCarouselDisplayList) {
        this.loginCarouselDisplayList = loginCarouselDisplayList;
    }

    public LoginCarouselDisplay getCurrentLoginCarouselDisplay() {
        return this.currentLoginCarouselDisplay;
    }

    public void setCurrentLoginCarouselDisplay(LoginCarouselDisplay newLoginCarouselDisplay) {
        this.currentLoginCarouselDisplay = newLoginCarouselDisplay;
    }

    /**
     * This method is called when the user presses the "New LoginCarouselDisplay" button, and creates 
     * a new loginCarouselDisplay to modify.
     */
    public void createLoginCarouselDisplay() {
        this.currentLoginCarouselDisplay = new LoginCarouselDisplay();
    }

    /**
     * Removes the selected loginCarouselDisplay from the set of loginCarouselDisplays on the current Account instance.
     * 
     * @param toRemove
     */
    public void removeLoginCarouselDisplay(LoginCarouselDisplay toRemove) {
        this.loginCarouselDisplayList.remove(toRemove);
        this.toDelete.add(toRemove);
    }

    /**
     * Saves the selected LoginCarouselDisplay onto the current Account instance.
     */
    public void saveLoginCarouselDisplay() {
    	if(!this.loginCarouselDisplayList.contains(this.currentLoginCarouselDisplay)){
    		this.loginCarouselDisplayList.add(this.currentLoginCarouselDisplay);
    	}
    }

	@Override
	public Converter getConverter() {
		// NOT USED
		LOGGER.error("Unexpected method call. This bean shouldn't need to do conversion.");
		return null;
	}
}