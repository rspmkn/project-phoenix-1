package com.apd.phoenix.administration.view.jsf.bean.event;

import javax.enterprise.inject.Produces;
import com.apd.phoenix.service.model.CustomerOrder;

/**
 * @author Red Hat Middleware Consulting. Red Hat, Inc. 
 * Jun 18, 2013
 *
 */

public class OrderPlacedEvent {

    @Produces
    private CustomerOrder customerOrder;

    /**
     * Getter for the <code>customerOrder/<code>
     * @return the customerOrder
     */
    public CustomerOrder getCustomerOrder() {
        return customerOrder;
    }

    /**
     *Setter for the <code>customerOrder/<code>
     * @param customerOrder the customerOrder to set
     */
    public void setCustomerOrder(CustomerOrder customerOrder) {
        this.customerOrder = customerOrder;
    }
}
