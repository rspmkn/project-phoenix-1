package com.apd.phoenix.administration.view.jsf.bean;

import java.util.List;
import javax.enterprise.context.ConversationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import com.apd.phoenix.service.persistence.jpa.ActionTypeDao;
import com.apd.phoenix.service.model.ActionType;
import com.apd.phoenix.web.searchbeans.AbstractSearchBean;
import java.io.Serializable;

@Named
@ConversationScoped
public class ActionTypeSearchBean extends AbstractSearchBean<ActionType> implements Serializable {

    @Inject
    ActionTypeDao dao;

    @Override
    public String[] getCriteria() {
        String[] toReturn = { "Value" };
        return toReturn;
    }

    @Override
    protected List<ActionType> search() {
        ActionType search = new ActionType();
        search.setName(this.getValues().get(0).getValue());
        return dao.searchByExample(search, 0, 0);
    }

}
