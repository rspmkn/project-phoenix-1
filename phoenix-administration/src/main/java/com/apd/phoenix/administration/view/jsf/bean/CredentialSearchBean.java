package com.apd.phoenix.administration.view.jsf.bean;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.enterprise.context.ConversationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import com.apd.phoenix.service.business.CredentialBp;
import com.apd.phoenix.service.model.Credential;
import com.apd.phoenix.web.PropertiesBean;
import com.apd.phoenix.web.searchbeans.AbstractSearchBean;
import java.io.Serializable;

@Named
@ConversationScoped
public class CredentialSearchBean extends AbstractSearchBean<Credential> implements Serializable {

    private static final long serialVersionUID = 6567519951367774181L;

    @Inject
    private PropertiesBean propertiesBean;

    @Inject
    CredentialBp bp;

    @Override
    public String[] getCriteria() {
        String[] toReturn = { "Name" };
        return toReturn;
    }

    @Override
    public String[] getColumns() {
        if (propertiesBean.get("project.state", "isPreRelease", "false").equals("true")) {
            String[] toReturn = { "Name", "System ID" };
            return toReturn;
        }
        else {
            String[] toReturn = { "Name" };
            return toReturn;
        }
    }

    @Override
    protected List<Credential> search() {
        Credential search = new Credential();
        search.setName(this.getValues().get(0).getValue());
        return bp.searchByExample(search, 0, 0);
    }

    @Override
    public Map<String, String> resultRow(Credential searchResult) {
        Map<String, String> toReturn = new HashMap<String, String>();
        toReturn.put(getColumns()[0], searchResult.getName());
        if (propertiesBean.get("project.state", "isPreRelease", "false").equals("true")) {
            toReturn.put(getColumns()[1], searchResult.getId().toString());
        }
        return toReturn;
    }

}
