package com.apd.phoenix.administration.view.jsf.bean;

import java.io.Serializable;
import java.util.List;
import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateful;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.ConversationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.model.SelectItem;
import javax.inject.Inject;
import javax.inject.Named;
import com.apd.phoenix.service.model.NotificationProperties;
import com.apd.phoenix.service.model.NotificationProperties.Addressing;
import com.apd.phoenix.service.model.OrderLog.EventType;
import com.apd.phoenix.service.persistence.jpa.NotificationPropertiesDao;

/**
 * Backing bean for NotificationProperties entities.
 * <p>
 * This class provides CRUD functionality for all NotificationProperties entities. It focuses
 * purely on Java EE 6 standards (e.g. <tt>&#64;ConversationScoped</tt> for
 * state management, <tt>PersistenceContext</tt> for persistence,
 * <tt>CriteriaBuilder</tt> for searches) rather than introducing a CRUD framework or
 * custom base class.
 */

@Named
@Stateful
@ConversationScoped
public class NotificationPropertiesBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    /*
     * Support creating and retrieving NotificationProperties entities
     */

    public void validateSelection(FacesContext context, UIComponent component, Object value) {
        if (this.currentInstance.getId() == null) {
            ((UIInput) component).setValid(false);
            FacesMessage message = new FacesMessage();
            message.setSeverity(FacesMessage.SEVERITY_ERROR);
            message.setSummary("Select Event Type to edit");
            message.setDetail("You need to select an Event Type to edit.");
            context.addMessage(component.getClientId(), message);
            return;
        }
    }

    private Long id;

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    private NotificationProperties currentInstance;

    public NotificationProperties getNotificationProperties() {
        return this.currentInstance;
    }

    @Inject
    private NotificationPropertiesDao dao;

    @Inject
    private NotificationPropertiesSearchBean searchBean;

    @Inject
    private Conversation conversation;

    public String create() {

        this.conversation.begin();
        return "eventTypeAdd?faces-redirect=true";
    }

    public void retrieve() {
        if (this.searchBean.getSelection() != null) {
            this.currentInstance = this.searchBean.getSelection();
            this.searchBean.setSelection(null);
        }

        if (FacesContext.getCurrentInstance().isPostback()) {
            return;
        }

        if (this.conversation.isTransient()) {
            this.conversation.begin();
        }

        if (this.id == null) {
            this.currentInstance = this.example;
        }
        else {
            this.currentInstance = dao.findById(id, NotificationProperties.class);
        }

    }

    /*
     * Support updating and deleting NotificationProperties entities
     */

    public String update() {
        this.conversation.end();

        try {

            if (currentInstance.getId() == null) {
                dao.create(this.currentInstance);
                return "eventTypeAdd?faces-redirect=true";
            }
            else {
                dao.update(this.currentInstance);
                return "eventTypeEdit?faces-redirect=true";
            }
        }
        catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(e.getMessage()));
            return null;
        }
    }

    public String delete() {
        this.conversation.end();
        try {
            dao.delete(this.currentInstance.getId(), NotificationProperties.class);
            return "eventTypeEdit?faces-redirect=true";
        }
        catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(e.getMessage()));
            return null;
        }
    }

    /*
     * Support searching NotificationProperties entities with pagination
     */

    private int page;
    private long count;
    private List<NotificationProperties> pageItems;

    private NotificationProperties example = new NotificationProperties();

    public int getPage() {
        return this.page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getPageSize() {
        return 10;
    }

    public NotificationProperties getExample() {
        return this.example;
    }

    public void setExample(NotificationProperties example) {
        this.example = example;
    }

    public void search() {
        this.page = 0;
    }

    public void paginate() {
        this.pageItems = dao.searchByExample(this.example, getPage() * getPageSize(), getPageSize());

        this.count = dao.resultQuantity(this.example);
    }

    public List<NotificationProperties> getPageItems() {
        return this.pageItems;
    }

    public long getCount() {
        return this.count;
    }

    /*
     * Support listing and POSTing back NotificationProperties entities (e.g. from inside an
     * HtmlSelectOneMenu)
     */

    public List<NotificationProperties> getAll() {

        return dao.findAll(NotificationProperties.class, 0, 0);
    }

    public List<NotificationProperties> searchByExample(NotificationProperties search) {
        return dao.searchByExample(search, 0, 0);
    }

    @Resource
    private SessionContext sessionContext;

    public Converter getConverter() {
        return new Converter() {

            @Override
            public Object getAsObject(FacesContext context, UIComponent component, String value) {
                return dao.findById(Long.valueOf(value), NotificationProperties.class);
            }

            @Override
            public String getAsString(FacesContext context, UIComponent component, Object value) {

                if (value == null) {
                    return "";
                }

                return String.valueOf(((NotificationProperties) value).getId());
            }
        };
    }

    /*
     * Support adding children to bidirectional, one-to-many tables
     */

    private NotificationProperties add = new NotificationProperties();

    public NotificationProperties getAdd() {
        return this.add;
    }

    public NotificationProperties getAdded() {
        NotificationProperties added = this.add;
        this.add = new NotificationProperties();
        return added;
    }

    public SelectItem[] getAddressing() {
        SelectItem[] items = new SelectItem[Addressing.values().length];
        int i = 0;
        for (Addressing g : Addressing.values()) {
            items[i++] = new SelectItem(g, g.getLabel());
        }
        return items;
    }

    public SelectItem[] getEventType() {
        SelectItem[] items = new SelectItem[EventType.values().length];
        int i = 0;
        for (EventType g : EventType.values()) {
            items[i++] = new SelectItem(g, g.getLabel());
        }
        return items;
    }
}
