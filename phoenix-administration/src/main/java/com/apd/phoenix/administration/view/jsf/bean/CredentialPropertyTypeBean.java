package com.apd.phoenix.administration.view.jsf.bean;

import java.io.Serializable;
import java.util.List;
import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateful;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.ConversationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.inject.Inject;
import javax.inject.Named;
import com.apd.phoenix.service.model.CredentialPropertyType;
import com.apd.phoenix.service.persistence.jpa.CredentialPropertyTypeDao;

/**
 * Backing bean for CredentialPropertyType entities.
 * <p>
 * This class provides CRUD functionality for all CredentialPropertyType entities. It focuses
 * purely on Java EE 6 standards (e.g. <tt>&#64;ConversationScoped</tt> for
 * state management, <tt>PersistenceContext</tt> for persistence,
 * <tt>CriteriaBuilder</tt> for searches) rather than introducing a CRUD framework or
 * custom base class.
 */

@Named
@Stateful
@ConversationScoped
public class CredentialPropertyTypeBean implements Serializable {

    /*
     * Support creating and retrieving CredentialPropertyType entities
     */

    private static final long serialVersionUID = -4959402460934077719L;

    public void validateSelection(FacesContext context, UIComponent component, Object value) {
        if (this.currentInstance.getId() == null) {
            ((UIInput) component).setValid(false);
            FacesMessage message = new FacesMessage();
            message.setSeverity(FacesMessage.SEVERITY_ERROR);
            message.setSummary("Select Credential Property Type to edit");
            message.setDetail("You need to select a Credential Property Type to edit.");
            context.addMessage(component.getClientId(), message);
            return;
        }
    }

    //   private Long id;
    //
    //   public Long getId()
    //   {
    //      return this.id;
    //   }
    //
    //   public void setId(Long id)
    //   {
    //      this.id = id;
    //   }

    private CredentialPropertyType currentInstance = new CredentialPropertyType();

    public CredentialPropertyType getCredentialPropertyType() {
        return this.currentInstance;
    }

    @Inject
    private CredentialPropertyTypeDao dao;

    @Inject
    private CredentialPropertyTypeSearchBean searchBean;

    @Inject
    private Conversation conversation;

    public String create() {

        this.conversation.begin();
        return "credentialPropertyTypeAdd?faces-redirect=true";
    }

    public void retrieve() {
        if (this.searchBean.getSelection() != null) {
            this.currentInstance = this.searchBean.getSelection();
            this.searchBean.setSelection(null);
        }

        if (FacesContext.getCurrentInstance().isPostback()) {
            return;
        }

        if (this.conversation.isTransient()) {
            this.conversation.begin();
        }
    }

    /*
     * Support updating and deleting CredentialPropertyType entities
     */

    public String update() {
        if (!validToUpdate()) {
            return null;
        }

        if (!this.conversation.isTransient()) {
            this.conversation.end();
        }
        try {

            if (currentInstance.getId() == null) {
                dao.create(this.currentInstance);
                return "credentialPropertyTypeAdd?faces-redirect=true";
            }
            else {
                dao.update(this.currentInstance);
                return "credentialPropertyTypeEdit?faces-redirect=true";
            }
        }
        catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), e.getMessage()));
            return null;
        }
    }

    public String delete() {
        this.conversation.end();
        try {
            dao.delete(this.currentInstance.getId(), CredentialPropertyType.class);
            return "credentialPropertyTypeEdit?faces-redirect=true";
        }
        catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), e.getMessage()));
            return null;
        }
    }

    /*
     * Support searching CredentialPropertyType entities with pagination
     */

    private int page;
    private long count;
    private List<CredentialPropertyType> pageItems;

    private CredentialPropertyType example = new CredentialPropertyType();

    public int getPage() {
        return this.page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getPageSize() {
        return 10;
    }

    public CredentialPropertyType getExample() {
        return this.example;
    }

    public void setExample(CredentialPropertyType example) {
        this.example = example;
    }

    public void search() {
        this.page = 0;
    }

    public void paginate() {
        this.pageItems = dao.searchByExample(this.example, getPage() * getPageSize(), getPageSize());
        this.count = dao.resultQuantity(this.example);
    }

    public List<CredentialPropertyType> getPageItems() {
        return this.pageItems;
    }

    public long getCount() {
        return this.count;
    }

    /*
     * Support listing and POSTing back CredentialPropertyType entities (e.g. from inside an
     * HtmlSelectOneMenu)
     */

    public List<CredentialPropertyType> getAll() {

        return dao.findAll(CredentialPropertyType.class, 0, 0);
    }

    public List<CredentialPropertyType> searchByEntity(CredentialPropertyType search) {
        return dao.searchByExample(search, 0, 0);
    }

    public List<CredentialPropertyType> searchByString(String s) {
        CredentialPropertyType search = new CredentialPropertyType();
        search.setName(s);
        return searchByEntity(search);
    }

    @Resource
    private SessionContext sessionContext;

    public Converter getConverter() {

        return new Converter() {

            @Override
            public Object getAsObject(FacesContext context, UIComponent component, String value) {
                return dao.findById(Long.valueOf(value), CredentialPropertyType.class);
            }

            @Override
            public String getAsString(FacesContext context, UIComponent component, Object value) {

                if (value == null) {
                    return "";
                }

                return String.valueOf(((CredentialPropertyType) value).getId());
            }
        };
    }

    /*
     * Support adding children to bidirectional, one-to-many tables
     */

    private CredentialPropertyType add = new CredentialPropertyType();

    public CredentialPropertyType getAdd() {
        return this.add;
    }

    public CredentialPropertyType getAdded() {
        CredentialPropertyType added = this.add;
        this.add = new CredentialPropertyType();
        return added;
    }

    private boolean validToUpdate() {
        CredentialPropertyType searchCredentialPropertyType = new CredentialPropertyType();
        searchCredentialPropertyType.setName(this.currentInstance.getName());
        List<CredentialPropertyType> searchList = this.dao.searchByExactExample(searchCredentialPropertyType, 0, 0);
        if (searchList.size() != 0 && !searchList.get(0).getId().equals(this.currentInstance.getId())) {
            FacesMessage message = new FacesMessage();
            message.setSeverity(FacesMessage.SEVERITY_ERROR);
            message.setSummary("Name already exists");
            message.setDetail("A credential property type already exists with the specified name.");
            FacesContext.getCurrentInstance().addMessage(null, message);
            return false;
        }
        return true;
    }
}