package com.apd.phoenix.administration.view.jsf.bean;

import java.io.Serializable;
import java.util.List;
import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateful;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.ConversationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.inject.Inject;
import javax.inject.Named;
import com.apd.phoenix.service.model.AccountPropertyType;
import com.apd.phoenix.service.persistence.jpa.AccountPropertyTypeDao;

/**
 * Backing bean for AccountPropertyType entities.
 * <p>
 * This class provides CRUD functionality for all AccountPropertyType entities. It focuses
 * purely on Java EE 6 standards (e.g. <tt>&#64;ConversationScoped</tt> for
 * state management, <tt>PersistenceContext</tt> for persistence,
 * <tt>CriteriaBuilder</tt> for searches) rather than introducing a CRUD framework or
 * custom base class.
 */

@Named
@Stateful
@ConversationScoped
public class AccountPropertyTypeBean implements Serializable {

    private static final long serialVersionUID = 1L;

    /*
     * Support creating and retrieving AccountPropertyType entities
     */

    public void validateSelection(FacesContext context, UIComponent component, Object value) {
        if (this.currentInstance.getId() == null) {
            ((UIInput) component).setValid(false);
            FacesMessage message = new FacesMessage();
            message.setSeverity(FacesMessage.SEVERITY_ERROR);
            message.setSummary("Select Account Property Type to edit");
            message.setDetail("You need to select a Account Property Type to edit.");
            context.addMessage(component.getClientId(), message);
            return;
        }
    }

    private Long id;

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    private AccountPropertyType currentInstance = new AccountPropertyType();

    public AccountPropertyType getAccountPropertyType() {
        return this.currentInstance;
    }

    @Inject
    private AccountPropertyTypeDao dao;

    @Inject
    private AccountPropertyTypeSearchBean searchBean;

    @Inject
    private Conversation conversation;

    public String create() {

        this.conversation.begin();
        return "accountPropertyTypeAdd?faces-redirect=true";
    }

    public void retrieve() {
        if (this.searchBean.getSelection() != null) {
            this.currentInstance = this.searchBean.getSelection();
            this.searchBean.setSelection(null);
        }

        if (FacesContext.getCurrentInstance().isPostback()) {
            return;
        }

        if (this.conversation.isTransient()) {
            this.conversation.begin();
        }
    }

    /*
     * Support updating and deleting AccountPropertyType entities
     */

    public String update() {
        if (!validToUpdate()) {
            return null;
        }

        if (!this.conversation.isTransient()) {
            this.conversation.end();
        }
        try {

            if (currentInstance.getId() == null) {
                dao.create(this.currentInstance);
                return "accountPropertyTypeAdd?faces-redirect=true";
            }
            else {
                dao.update(this.currentInstance);
                return "accountPropertyTypeEdit?faces-redirect=true";
            }
        }
        catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), e.getMessage()));
            return null;
        }
    }

    public String delete() {
        this.conversation.end();
        try {
            dao.delete(this.currentInstance.getId(), AccountPropertyType.class);
            return "accountPropertyTypeEdit?faces-redirect=true";
        }
        catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), e.getMessage()));
            return null;
        }
    }

    /*
     * Support searching AccountPropertyType entities with pagination
     */

    private int page;
    private long count;
    private List<AccountPropertyType> pageItems;

    private AccountPropertyType example = new AccountPropertyType();

    public int getPage() {
        return this.page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getPageSize() {
        return 10;
    }

    public AccountPropertyType getExample() {
        return this.example;
    }

    public void setExample(AccountPropertyType example) {
        this.example = example;
    }

    public void search() {
        this.page = 0;
    }

    public void paginate() {
        this.pageItems = dao.searchByExample(this.example, getPage() * getPageSize(), getPageSize());
        this.count = dao.resultQuantity(this.example);
    }

    public List<AccountPropertyType> getPageItems() {
        return this.pageItems;
    }

    public long getCount() {
        return this.count;
    }

    /*
     * Support listing and POSTing back AccountPropertyType entities (e.g. from inside an
     * HtmlSelectOneMenu)
     */

    public List<AccountPropertyType> getAll() {

        return dao.findAll(AccountPropertyType.class, 0, 0);
    }

    public List<AccountPropertyType> searchByEntity(AccountPropertyType search) {
        return dao.searchByExample(search, 0, 0);
    }

    public List<AccountPropertyType> searchByString(String s) {
        AccountPropertyType search = new AccountPropertyType();
        search.setName(s);
        return searchByEntity(search);
    }

    @Resource
    private SessionContext sessionContext;

    public Converter getConverter() {

        return new Converter() {

            @Override
            public Object getAsObject(FacesContext context, UIComponent component, String value) {
                return dao.findById(Long.valueOf(value), AccountPropertyType.class);
            }

            @Override
            public String getAsString(FacesContext context, UIComponent component, Object value) {

                if (value == null) {
                    return "";
                }

                return String.valueOf(((AccountPropertyType) value).getId());
            }
        };
    }

    /*
     * Support adding children to bidirectional, one-to-many tables
     */

    private AccountPropertyType add = new AccountPropertyType();

    public AccountPropertyType getAdd() {
        return this.add;
    }

    public AccountPropertyType getAdded() {
        AccountPropertyType added = this.add;
        this.add = new AccountPropertyType();
        return added;
    }

    private boolean validToUpdate() {
        AccountPropertyType searchAccountPropertyType = new AccountPropertyType();
        searchAccountPropertyType.setName(this.currentInstance.getName());
        List<AccountPropertyType> searchList = this.dao.searchByExactExample(searchAccountPropertyType, 0, 0);
        if (searchList.size() != 0 && !searchList.get(0).getId().equals(this.currentInstance.getId())) {
            FacesMessage message = new FacesMessage();
            message.setSeverity(FacesMessage.SEVERITY_ERROR);
            message.setSummary("Name already exists");
            message.setDetail("An account property type already exists with the specified name.");
            FacesContext.getCurrentInstance().addMessage(null, message);
            return false;
        }
        return true;
    }
}