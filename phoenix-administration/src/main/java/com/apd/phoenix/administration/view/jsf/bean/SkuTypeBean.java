package com.apd.phoenix.administration.view.jsf.bean;

import java.io.Serializable;
import java.util.List;
import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateful;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.ConversationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.inject.Inject;
import javax.inject.Named;
import com.apd.phoenix.service.model.SkuType;
import com.apd.phoenix.service.persistence.jpa.SkuTypeDao;

/**
 * Backing bean for SkuType entities.
 * <p>
 * This class provides CRUD functionality for all SkuType entities. It focuses
 * purely on Java EE 6 standards (e.g. <tt>&#64;ConversationScoped</tt> for
 * state management, <tt>PersistenceContext</tt> for persistence,
 * <tt>CriteriaBuilder</tt> for searches) rather than introducing a CRUD framework or
 * custom base class.
 */

@Named
@Stateful
@ConversationScoped
public class SkuTypeBean implements Serializable {

    private static final long serialVersionUID = 1L;

    /*
     * Support creating and retrieving SkuType entities
     */

    public void validateSelection(FacesContext context, UIComponent component, Object value) {
        if (this.currentInstance.getId() == null) {
            ((UIInput) component).setValid(false);
            FacesMessage message = new FacesMessage();
            message.setSeverity(FacesMessage.SEVERITY_ERROR);
            message.setSummary("Select Sku Type to edit");
            message.setDetail("You need to select an Sku Type to edit.");
            context.addMessage(component.getClientId(), message);
            return;
        }
    }

    private Long id;

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    private SkuType currentInstance;

    public SkuType getSkuType() {
        return this.currentInstance;
    }

    @Inject
    private SkuTypeDao dao;

    @Inject
    private SkuTypeSearchBean searchBean;

    @Inject
    private Conversation conversation;

    public String create() {

        this.conversation.begin();
        return "skuTypeAdd?faces-redirect=true";
    }

    public void retrieve() {
        if (this.searchBean.getSelection() != null) {
            this.currentInstance = this.searchBean.getSelection();
            this.searchBean.setSelection(null);
        }

        if (FacesContext.getCurrentInstance().isPostback()) {
            return;
        }

        if (this.conversation.isTransient()) {
            this.conversation.begin();
        }

        if (this.id == null) {
            this.currentInstance = this.example;
        }
        else {
            this.currentInstance = dao.findById(id, SkuType.class);
        }

    }

    /*
     * Support updating and deleting SkuType entities
     */

    public String update() {
        if (!validToUpdate()) {
            return null;
        }

        if (!this.conversation.isTransient()) {
            this.conversation.end();
        }

        try {

            if (currentInstance.getId() == null) {
                dao.create(this.currentInstance);
                return "skuTypeAdd?faces-redirect=true";
            }
            else {
                dao.update(this.currentInstance);
                return "skuTypeEdit?faces-redirect=true";
            }
        }
        catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), e.getMessage()));
            return null;
        }
    }

    public String delete() {
        this.conversation.end();
        try {
            dao.delete(this.currentInstance.getId(), SkuType.class);
            return "skuTypeEdit?faces-redirect=true";
        }
        catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), e.getMessage()));
            return null;
        }
    }

    /*
     * Support searching SkuType entities with pagination
     */

    private int page;
    private long count;
    private List<SkuType> pageItems;

    private SkuType example = new SkuType();

    public int getPage() {
        return this.page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getPageSize() {
        return 10;
    }

    public SkuType getExample() {
        return this.example;
    }

    public void setExample(SkuType example) {
        this.example = example;
    }

    public void search() {
        this.page = 0;
    }

    public void paginate() {
        this.pageItems = dao.searchByExample(this.example, getPage() * getPageSize(), getPageSize());

        this.count = dao.resultQuantity(this.example);
    }

    public List<SkuType> getPageItems() {
        return this.pageItems;
    }

    public long getCount() {
        return this.count;
    }

    /*
     * Support listing and POSTing back SkuType entities (e.g. from inside an
     * HtmlSelectOneMenu)
     */

    public List<SkuType> getAll() {

        return dao.findAll(SkuType.class, 0, 0);
    }

    public List<SkuType> searchByExample(SkuType search) {
        return dao.searchByExample(search, 0, 0);
    }

    public List<SkuType> searchByString(String s) {
        SkuType search = new SkuType();
        search.setName(s);
        return searchByExample(search);
    }

    @Resource
    private SessionContext sessionContext;

    public Converter getConverter() {

        return new Converter() {

            @Override
            public Object getAsObject(FacesContext context, UIComponent component, String value) {
                return dao.findById(Long.valueOf(value), SkuType.class);
            }

            @Override
            public String getAsString(FacesContext context, UIComponent component, Object value) {

                if (value == null) {
                    return "";
                }

                return String.valueOf(((SkuType) value).getId());
            }
        };
    }

    /*
     * Support adding children to bidirectional, one-to-many tables
     */

    private SkuType add = new SkuType();

    public SkuType getAdd() {
        return this.add;
    }

    public SkuType getAdded() {
        SkuType added = this.add;
        this.add = new SkuType();
        return added;
    }

    private boolean validToUpdate() {
        SkuType searchSkuType = new SkuType();
        searchSkuType.setName(this.currentInstance.getName());
        List<SkuType> searchList = this.dao.searchByExactExample(searchSkuType, 0, 0);
        if (searchList.size() != 0 && !searchList.get(0).getId().equals(this.currentInstance.getId())) {
            FacesMessage message = new FacesMessage();
            message.setSeverity(FacesMessage.SEVERITY_ERROR);
            message.setSummary("Name already exists");
            message.setDetail("A SkuType already exists with the specified name.");
            FacesContext.getCurrentInstance().addMessage(null, message);
            return false;
        }
        return true;
    }
}