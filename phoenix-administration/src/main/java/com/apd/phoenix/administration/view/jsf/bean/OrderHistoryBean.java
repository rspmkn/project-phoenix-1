package com.apd.phoenix.administration.view.jsf.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateful;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.ConversationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import com.apd.phoenix.service.model.CustomerOrder;
import com.apd.phoenix.service.model.OrderLog;
import com.apd.phoenix.service.model.OrderStatus;

/**
 * Backing bean for OrderHistory entities.
 * <p>
 * This class provides CRUD functionality for all OrderHistory entities. It focuses
 * purely on Java EE 6 standards (e.g. <tt>&#64;ConversationScoped</tt> for
 * state management, <tt>PersistenceContext</tt> for persistence,
 * <tt>CriteriaBuilder</tt> for searches) rather than introducing a CRUD framework or
 * custom base class.
 */

@Named
@Stateful
@ConversationScoped
@Deprecated
public class OrderHistoryBean implements Serializable {

    private static final long serialVersionUID = 1L;

    /*
     * Support creating and retrieving OrderHistory entities
     */

    private Long id;

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    private OrderLog orderHistory;

    public OrderLog getOrderHistory() {
        return this.orderHistory;
    }

    @Inject
    private Conversation conversation;

    private EntityManager entityManager;

    public String create() {

        this.conversation.begin();
        return "create?faces-redirect=true";
    }

    public void retrieve() {

        if (FacesContext.getCurrentInstance().isPostback()) {
            return;
        }

        if (this.conversation.isTransient()) {
            this.conversation.begin();
        }

        if (this.id == null) {
            this.orderHistory = this.example;
        }
        else {
            this.orderHistory = findById(getId());
        }
    }

    public OrderLog findById(Long id) {

        return this.entityManager.find(OrderLog.class, id);
    }

    /*
     * Support updating and deleting OrderHistory entities
     */

    public String update() {
        this.conversation.end();

        try {
            if (this.id == null) {
                this.entityManager.persist(this.orderHistory);
                return "search?faces-redirect=true";
            }
            else {
                this.entityManager.merge(this.orderHistory);
                return "view?faces-redirect=true&id=" + this.orderHistory.getId();
            }
        }
        catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), e.getMessage()));
            return null;
        }
    }

    public String delete() {
        this.conversation.end();

        try {
            this.entityManager.remove(findById(getId()));
            this.entityManager.flush();
            return "search?faces-redirect=true";
        }
        catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), e.getMessage()));
            return null;
        }
    }

    /*
     * Support searching OrderHistory entities with pagination
     */

    private int page;
    private long count;
    private List<OrderLog> pageItems;

    private OrderLog example = new OrderLog();

    public int getPage() {
        return this.page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getPageSize() {
        return 10;
    }

    public OrderLog getExample() {
        return this.example;
    }

    public void setExample(OrderLog example) {
        this.example = example;
    }

    public void search() {
        this.page = 0;
    }

    public void paginate() {

        CriteriaBuilder builder = this.entityManager.getCriteriaBuilder();

        // Populate this.count

        CriteriaQuery<Long> countCriteria = builder.createQuery(Long.class);
        Root<OrderLog> root = countCriteria.from(OrderLog.class);
        countCriteria = countCriteria.select(builder.count(root)).where(getSearchPredicates(root));
        this.count = this.entityManager.createQuery(countCriteria).getSingleResult();

        // Populate this.pageItems

        CriteriaQuery<OrderLog> criteria = builder.createQuery(OrderLog.class);
        root = criteria.from(OrderLog.class);
        TypedQuery<OrderLog> query = this.entityManager.createQuery(criteria.select(root).where(
                getSearchPredicates(root)));
        query.setFirstResult(this.page * getPageSize()).setMaxResults(getPageSize());
        this.pageItems = query.getResultList();
    }

    private Predicate[] getSearchPredicates(Root<OrderLog> root) {

        CriteriaBuilder builder = this.entityManager.getCriteriaBuilder();
        List<Predicate> predicatesList = new ArrayList<Predicate>();

        CustomerOrder customerOrder = this.example.getCustomerOrder();
        if (customerOrder != null) {
            predicatesList.add(builder.equal(root.get("customerOrder"), customerOrder));
        }
        OrderStatus orderStatus = this.example.getOrderStatus();
        if (orderStatus != null) {
            predicatesList.add(builder.equal(root.get("orderStatus"), orderStatus));
        }
        //        EventType eventType = this.example.getEventType();
        //        if (eventType != null) {
        //            predicatesList.add(builder.equal(root.get("eventType"), eventType));
        //        }

        return predicatesList.toArray(new Predicate[predicatesList.size()]);
    }

    public List<OrderLog> getPageItems() {
        return this.pageItems;
    }

    public long getCount() {
        return this.count;
    }

    /*
     * Support listing and POSTing back OrderHistory entities (e.g. from inside an
     * HtmlSelectOneMenu)
     */

    public List<OrderLog> getAll() {

        CriteriaQuery<OrderLog> criteria = this.entityManager.getCriteriaBuilder().createQuery(OrderLog.class);
        return this.entityManager.createQuery(criteria.select(criteria.from(OrderLog.class))).getResultList();
    }

    @Resource
    private SessionContext sessionContext;

    public Converter getConverter() {

        final OrderHistoryBean ejbProxy = this.sessionContext.getBusinessObject(OrderHistoryBean.class);

        return new Converter() {

            @Override
            public Object getAsObject(FacesContext context, UIComponent component, String value) {

                return ejbProxy.findById(Long.valueOf(value));
            }

            @Override
            public String getAsString(FacesContext context, UIComponent component, Object value) {

                if (value == null) {
                    return "";
                }

                return String.valueOf(((OrderLog) value).getId());
            }
        };
    }

    /*
     * Support adding children to bidirectional, one-to-many tables
     */

    private OrderLog add = new OrderLog();

    public OrderLog getAdd() {
        return this.add;
    }

    public OrderLog getAdded() {
        OrderLog added = this.add;
        this.add = new OrderLog();
        return added;
    }
}