package com.apd.phoenix.administration.view.jsf.bean;

import java.io.Serializable;
import java.util.List;
import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateful;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.ConversationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.inject.Inject;
import javax.inject.Named;
import com.apd.phoenix.service.persistence.jpa.AccountXCredentialXUserDao;
import com.apd.phoenix.service.model.AccountXCredentialXUser;

/**
 * Backing bean for AccountXCredentialXUser entities.
 * <p>
 * This class provides CRUD functionality for all AccountXCredentialXUser entities. It focuses
 * purely on Java EE 6 standards (e.g. <tt>&#64;ConversationScoped</tt> for
 * state management, <tt>PersistenceContext</tt> for persistence,
 * <tt>CriteriaBuilder</tt> for searches) rather than introducing a CRUD framework or
 * custom base class.
 */

@Named
@Stateful
@ConversationScoped
public class AccountXCredentialXUserBean implements Serializable {

    private static final long serialVersionUID = 1L;

    /*
     * Support creating and retrieving AccountXCredentialXUser entities
     */

    private Long id;

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    private AccountXCredentialXUser accountXCredentialXUser;

    public AccountXCredentialXUser getAccountXCredentialXUser() {
        return this.accountXCredentialXUser;
    }

    @Inject
    private Conversation conversation;

    @Inject
    private AccountXCredentialXUserDao dao;

    public String create() {
        this.conversation.begin();
        return "create?faces-redirect=true";
    }

    public void retrieve() {
        if (FacesContext.getCurrentInstance().isPostback()) {
            return;
        }

        if (this.conversation.isTransient()) {
            this.conversation.begin();
        }

        if (this.id == null) {
            this.accountXCredentialXUser = this.example;
        }
        else {
            this.accountXCredentialXUser = dao.findById(id, AccountXCredentialXUser.class);
        }

    }

    /*
     * Support updating and deleting AccountXCredentialXUser entities
     */

    public String update() {
        this.conversation.end();

        try {
            dao.update(this.accountXCredentialXUser);

            if (accountXCredentialXUser.getId() == null) {
                return "search?faces-redirect=true";
            }
            else {
                return "view?faces-redirect=true&id=" + accountXCredentialXUser.getId();
            }
        }
        catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), e.getMessage()));
            return null;
        }
    }

    public String delete() {
        this.conversation.end();
        try {
            dao.delete(this.id, AccountXCredentialXUser.class);
            return "search?faces-redirect=true";
        }
        catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), e.getMessage()));
            return null;
        }
    }

    /*
     * Support searching AccountXCredentialXUser entities with pagination
     */

    private int page;
    private long count;
    private List<AccountXCredentialXUser> pageItems;

    private AccountXCredentialXUser example = new AccountXCredentialXUser();

    public int getPage() {
        return this.page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getPageSize() {
        return 10;
    }

    public AccountXCredentialXUser getExample() {
        return this.example;
    }

    public void setExample(AccountXCredentialXUser example) {
        this.example = example;
    }

    public void search() {
        this.page = 0;
    }

    public void paginate() {
        this.pageItems = dao.searchByExample(this.example, getPage() * getPageSize(), getPageSize());

        this.count = dao.resultQuantity(this.example);
    }

    public List<AccountXCredentialXUser> getPageItems() {
        return this.pageItems;
    }

    public long getCount() {
        return this.count;
    }

    /*
     * Support listing and POSTing back AccountXCredentialXUser entities (e.g. from inside an
     * HtmlSelectOneMenu)
     */

    public List<AccountXCredentialXUser> getAll() {

        return dao.findAll(AccountXCredentialXUser.class, 0, 0);
    }

    @Resource
    private SessionContext sessionContext;

    public Converter getConverter() {

        final AccountXCredentialXUserBean ejbProxy = this.sessionContext
                .getBusinessObject(AccountXCredentialXUserBean.class);

        return new Converter() {

            @Override
            public Object getAsObject(FacesContext context, UIComponent component, String value) {

                ejbProxy.setId(Long.valueOf(value));
                ejbProxy.retrieve();
                return ejbProxy.getAccountXCredentialXUser();
            }

            @Override
            public String getAsString(FacesContext context, UIComponent component, Object value) {

                if (value == null) {
                    return "";
                }

                return String.valueOf(((AccountXCredentialXUser) value).getId());
            }
        };
    }

    /*
     * Support adding children to bidirectional, one-to-many tables
     */

    private AccountXCredentialXUser add = new AccountXCredentialXUser();

    public AccountXCredentialXUser getAdd() {
        return this.add;
    }

    public AccountXCredentialXUser getAdded() {
        AccountXCredentialXUser added = this.add;
        this.add = new AccountXCredentialXUser();
        return added;
    }
}