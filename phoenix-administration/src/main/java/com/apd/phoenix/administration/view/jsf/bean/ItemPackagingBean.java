package com.apd.phoenix.administration.view.jsf.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateful;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.ConversationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import com.apd.phoenix.service.model.ItemPackaging;
import com.apd.phoenix.service.model.Item;
import com.apd.phoenix.service.model.PackagingDimension;
import com.apd.phoenix.service.model.PackagingType;

/**
 * Backing bean for ItemPackaging entities.
 * <p>
 * This class provides CRUD functionality for all ItemPackaging entities. It focuses
 * purely on Java EE 6 standards (e.g. <tt>&#64;ConversationScoped</tt> for
 * state management, <tt>PersistenceContext</tt> for persistence,
 * <tt>CriteriaBuilder</tt> for searches) rather than introducing a CRUD framework or
 * custom base class.
 */

@Named
@Stateful
@ConversationScoped
@Deprecated
public class ItemPackagingBean implements Serializable {

    private static final long serialVersionUID = 1L;

    /*
     * Support creating and retrieving ItemPackaging entities
     */

    private Long id;

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    private ItemPackaging itemPackaging;

    public ItemPackaging getItemPackaging() {
        return this.itemPackaging;
    }

    @Inject
    private Conversation conversation;

    private EntityManager entityManager;

    public String create() {

        this.conversation.begin();
        return "create?faces-redirect=true";
    }

    public void retrieve() {

        if (FacesContext.getCurrentInstance().isPostback()) {
            return;
        }

        if (this.conversation.isTransient()) {
            this.conversation.begin();
        }

        if (this.id == null) {
            this.itemPackaging = this.example;
        }
        else {
            this.itemPackaging = findById(getId());
        }
    }

    public ItemPackaging findById(Long id) {

        return this.entityManager.find(ItemPackaging.class, id);
    }

    /*
     * Support updating and deleting ItemPackaging entities
     */

    public String update() {
        this.conversation.end();

        try {
            if (this.id == null) {
                this.entityManager.persist(this.itemPackaging);
                return "search?faces-redirect=true";
            }
            else {
                this.entityManager.merge(this.itemPackaging);
                return "view?faces-redirect=true&id=" + this.itemPackaging.getId();
            }
        }
        catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), e.getMessage()));
            return null;
        }
    }

    public String delete() {
        this.conversation.end();

        try {
            this.entityManager.remove(findById(getId()));
            this.entityManager.flush();
            return "search?faces-redirect=true";
        }
        catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), e.getMessage()));
            return null;
        }
    }

    /*
     * Support searching ItemPackaging entities with pagination
     */

    private int page;
    private long count;
    private List<ItemPackaging> pageItems;

    private ItemPackaging example = new ItemPackaging();

    public int getPage() {
        return this.page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getPageSize() {
        return 10;
    }

    public ItemPackaging getExample() {
        return this.example;
    }

    public void setExample(ItemPackaging example) {
        this.example = example;
    }

    public void search() {
        this.page = 0;
    }

    public void paginate() {

        CriteriaBuilder builder = this.entityManager.getCriteriaBuilder();

        // Populate this.count

        CriteriaQuery<Long> countCriteria = builder.createQuery(Long.class);
        Root<ItemPackaging> root = countCriteria.from(ItemPackaging.class);
        countCriteria = countCriteria.select(builder.count(root)).where(getSearchPredicates(root));
        this.count = this.entityManager.createQuery(countCriteria).getSingleResult();

        // Populate this.pageItems

        CriteriaQuery<ItemPackaging> criteria = builder.createQuery(ItemPackaging.class);
        root = criteria.from(ItemPackaging.class);
        TypedQuery<ItemPackaging> query = this.entityManager.createQuery(criteria.select(root).where(
                getSearchPredicates(root)));
        query.setFirstResult(this.page * getPageSize()).setMaxResults(getPageSize());
        this.pageItems = query.getResultList();
    }

    private Predicate[] getSearchPredicates(Root<ItemPackaging> root) {

        CriteriaBuilder builder = this.entityManager.getCriteriaBuilder();
        List<Predicate> predicatesList = new ArrayList<Predicate>();

        PackagingDimension packagingDimension = this.example.getPackagingDimension();
        if (packagingDimension != null) {
            predicatesList.add(builder.equal(root.get("packagingDimension"), packagingDimension));
        }
        PackagingType packagingType = this.example.getPackagingType();
        if (packagingType != null) {
            predicatesList.add(builder.equal(root.get("packagingType"), packagingType));
        }
        Item item = this.example.getItem();
        if (item != null) {
            predicatesList.add(builder.equal(root.get("item"), item));
        }

        return predicatesList.toArray(new Predicate[predicatesList.size()]);
    }

    public List<ItemPackaging> getPageItems() {
        return this.pageItems;
    }

    public long getCount() {
        return this.count;
    }

    /*
     * Support listing and POSTing back ItemPackaging entities (e.g. from inside an
     * HtmlSelectOneMenu)
     */

    public List<ItemPackaging> getAll() {

        CriteriaQuery<ItemPackaging> criteria = this.entityManager.getCriteriaBuilder()
                .createQuery(ItemPackaging.class);
        return this.entityManager.createQuery(criteria.select(criteria.from(ItemPackaging.class))).getResultList();
    }

    @Resource
    private SessionContext sessionContext;

    public Converter getConverter() {

        final ItemPackagingBean ejbProxy = this.sessionContext.getBusinessObject(ItemPackagingBean.class);

        return new Converter() {

            @Override
            public Object getAsObject(FacesContext context, UIComponent component, String value) {

                return ejbProxy.findById(Long.valueOf(value));
            }

            @Override
            public String getAsString(FacesContext context, UIComponent component, Object value) {

                if (value == null) {
                    return "";
                }

                return String.valueOf(((ItemPackaging) value).getId());
            }
        };
    }

    /*
     * Support adding children to bidirectional, one-to-many tables
     */

    private ItemPackaging add = new ItemPackaging();

    public ItemPackaging getAdd() {
        return this.add;
    }

    public ItemPackaging getAdded() {
        ItemPackaging added = this.add;
        this.add = new ItemPackaging();
        return added;
    }
}