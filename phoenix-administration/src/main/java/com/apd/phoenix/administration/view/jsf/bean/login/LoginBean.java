package com.apd.phoenix.administration.view.jsf.bean.login;

import java.io.Serializable;
import javax.ejb.Stateful;
import javax.enterprise.context.ConversationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.business.SystemUserBp;
import com.apd.phoenix.service.model.SystemUser;
import com.apd.phoenix.service.utility.JSFUtils;
import com.apd.phoenix.web.ValidationProperties;
import com.apd.phoenix.core.EmailSender;
import com.apd.phoenix.core.EncryptionUtils;

@Named
@Stateful
@ConversationScoped
public class LoginBean implements Serializable {

    private static final long serialVersionUID = -1127601116139535684L;
    private static final Logger LOGGER = LoggerFactory.getLogger(LoginBean.class);
    private static final String resetPasswordMessage = "login.info.passwordReset";
    private String username;
    private String password;
    private String passResetUsername;
    private SystemUser systemUser;
    private EmailSender emailSender;

    @Inject
    private SystemUserBp systemUserBp;

    // Get System User from database via Login value
    public SystemUser getSystemUser() {

        //Returns the current user instance if it is not null and exists on the database
        if (this.systemUser != null && this.systemUser.getId() != null) {
            return this.systemUser;
        }

        ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();

        try {
            //looks up the user based on the context's principal.
            this.username = context.getUserPrincipal().getName();
            //searches for the user with that login
            this.systemUser = systemUserBp.getSystemUserByLogin(this.username).get(0);
        }
        catch (Exception e) {
            //if any error occurs (such as the principal being null or no user existing for that name)
            //the session is invalidated.
            this.systemUser = new SystemUser();
            context.invalidateSession();
        }

        return this.systemUser;
    }

    public void setSystemUser(SystemUser systemUser) {
        this.systemUser = systemUser;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPassResetUsername() {
        return passResetUsername;
    }

    public void setPassResetUsername(String passResetUsername) {
        this.passResetUsername = passResetUsername;
    }

    public String validateLogin() {
        return "index?faces-redirect=true";
    }

    private boolean isValidUser() {
        this.systemUser = getSystemUser();

        if (systemUser.getLogin() == null || systemUser.getLogin().isEmpty()) {
            return false;
        }

        LOGGER.info("***Found user: " + this.systemUser.getLogin());
        return true;
    }

    public void resetPassword() {
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage("resetPasswordInfo", new FacesMessage(FacesMessage.SEVERITY_INFO,
                "Password Reset Information", ValidationProperties.get(resetPasswordMessage)));
        if (isValidUser()) {

            Boolean emailSuccess = sendEmailReset(systemUser.getPerson().getEmail());
            if (emailSuccess) {
                systemUserBp.update(systemUser);
            }

        }
        else {
            this.systemUser = new SystemUser();
            this.systemUser.setLogin(this.passResetUsername);
            sendEmailReset("invalid@apd.com");
        }
        clearForm();
    }

    private Boolean sendEmailReset(String receiverEmail) {

        emailSender = new EmailSender();

        emailSender.setTo(receiverEmail);
        emailSender.setFrom(ValidationProperties.get("login.email.content.from"));
        emailSender.setSubject(ValidationProperties.get("login.email.content.subject"));

        //Reset to a random new password.
        String randomPassword = EncryptionUtils.randomAlphanumeric();
        emailSender.setBody(ValidationProperties.get("login.email.content.message") + randomPassword);

        FacesContext context = FacesContext.getCurrentInstance();

        try {
            emailSender.send();

            //Reset the password for the user in the database
            systemUserBp.encryptAndSetTemporaryPassword(systemUser, randomPassword);
            String message = ValidationProperties.get("login.email.sent.prefix") + this.systemUser.getLogin()
                    + ValidationProperties.get("login.email.sent.suffix");
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, message, message));

            return true; //Success

        }
        catch (Exception e) {
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, ValidationProperties
                    .get("login.email.error"), ValidationProperties.get("login.email.error")));

            LOGGER.error("**Error Sending Email.  Stack Trace is:", e);

            return false; //Failure
        }

    }

    public void clearForm() {
        JSFUtils.clearForm("passwordReset_form");
        this.passResetUsername = "";
    }
}
