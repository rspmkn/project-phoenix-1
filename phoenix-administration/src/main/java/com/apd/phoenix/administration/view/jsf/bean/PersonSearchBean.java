package com.apd.phoenix.administration.view.jsf.bean;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.enterprise.context.ConversationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import com.apd.phoenix.service.model.Person;
import com.apd.phoenix.service.persistence.jpa.PersonDao;
import com.apd.phoenix.web.searchbeans.AbstractSearchBean;
import java.io.Serializable;

@Named
@ConversationScoped
public class PersonSearchBean extends AbstractSearchBean<Person> implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -7993623818480853354L;
    @Inject
    PersonDao bp;

    @Override
    public String[] getCriteria() {
        String[] toReturn = { "First Name", "Last Name", "Email Address" };
        return toReturn;
    }

    @Override
    protected List<Person> search() {
        Person search = new Person();
        search.setFirstName(this.getValues().get(0).getValue());
        search.setLastName(this.getValues().get(1).getValue());
        search.setEmail(this.getValues().get(2).getValue());
        return bp.searchByExample(search, 0, 0);
    }

    @Override
    public String[] getColumns() {
        String[] toReturn = { "First", "Last", "Email" };
        return toReturn;
    }

    @Override
    public Map<String, String> resultRow(Person searchResult) {
        Map<String, String> toReturn = new HashMap<String, String>();
        toReturn.put(getColumns()[0], searchResult.getFirstName());
        toReturn.put(getColumns()[1], searchResult.getLastName());
        toReturn.put(getColumns()[2], searchResult.getEmail());
        return toReturn;
    }

}
