package com.apd.phoenix.administration.view.jsf.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import javax.ejb.Stateful;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.ConversationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.business.CredentialBp;
import com.apd.phoenix.service.business.PhoneNumberBp;
import com.apd.phoenix.service.model.Account;
import com.apd.phoenix.service.model.AccountXAccountPropertyType;
import com.apd.phoenix.service.model.Address;
import com.apd.phoenix.service.model.Bulletin;
import com.apd.phoenix.service.model.Credential;
import com.apd.phoenix.service.model.CustomerSpecificIconUrl;
import com.apd.phoenix.service.model.CxmlConfiguration;
import com.apd.phoenix.service.model.Group;
import com.apd.phoenix.service.model.IpAddress;
import com.apd.phoenix.service.model.NotificationProperties;
import com.apd.phoenix.service.model.PaymentInformation;
import com.apd.phoenix.service.model.Person;
import com.apd.phoenix.service.model.PhoneNumber;
import com.apd.phoenix.service.model.PoNumber;
import com.apd.phoenix.service.model.listener.AccountListener;
import com.apd.phoenix.web.AccountAddressPagenationControllerBean;
import com.apd.phoenix.web.searchbeans.AccountSearchBean;

/**
 * Backing bean for Account entities.
 * <p>
 * This class provides CRUD functionality for all Account entities. It focuses
 * purely on Java EE 6 standards (e.g. <tt>&#64;ConversationScoped</tt> for
 * state management, <tt>PersistenceContext</tt> for persistence,
 * <tt>CriteriaBuilder</tt> for searches) rather than introducing a CRUD framework or
 * custom base class.
 */

@Named
@Stateful
@ConversationScoped
public class AccountBean extends AccountAddressPagenationControllerBean implements Serializable {

    private static final long serialVersionUID = 1L;

    @Inject
    AccountSearchBean accountSearchBean;

    /*
     * Support creating and retrieving Account entities
     */

    private Long id;
    private PhoneNumber currentNumber = new PhoneNumber();
    // The Notification Property currently being edited.
    private NotificationProperties currentNotification = new NotificationProperties();
    private Account currentAccount = new Account();
    //private ProcessConfiguration currentProcessConfiguration = new ProcessConfiguration();
    //private Process currentProcess = new Process();
    private boolean changeParentDisabled = false;
    private boolean addChildDisabled = false;
    private boolean linkingAccount = false;
    private String childOrParent = "";
    // The Payment Information currently being edited.
    private PaymentInformation currentPaymentInformation = new PaymentInformation();
    // The IP address currently being edited.
    private IpAddress currentIpAddress = new IpAddress();
    private boolean addingRootAccount = false;
    private boolean recalculateLineage = false;

    private static final Logger LOG = LoggerFactory.getLogger(AccountBean.class);

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    private Bulletin currentBulletin = new Bulletin();

    private PoNumber currentPoNumber = new PoNumber();

    private AccountXAccountPropertyType currentProperty = new AccountXAccountPropertyType();

    private List<Credential> credentialsRemoved = new ArrayList<Credential>();

    private CustomerSpecificIconUrl currentIcon = new CustomerSpecificIconUrl();

    public PoNumber getCurrentPoNumber() {
        return this.currentPoNumber;
    }

    public void setCurrentPoNumber(PoNumber po) {
        this.currentPoNumber = po;
    }

    /**
     * This method is called when the user presses the "New PO Number" button, and creates 
     * a new PO number to modify.
     */
    public void createPoNumber() {
        this.currentPoNumber = new PoNumber();
    }

    /**
     * Removes the selected PO Number from the set of pos on the current Account instance.
     * 
     * @param toRemove
     */
    public void removePoNumber(PoNumber toRemove) {
        this.currentInstance.getBlanketPos().remove(toRemove);
    }

    /**
     * Saves the selected PO Number onto the current Account instance.
     */
    public void savePoNumber() {
        this.currentInstance.getBlanketPos().add(this.currentPoNumber);
    }

    public NotificationProperties getCurrentNotification() {
        return this.currentNotification;
    }

    public void setCurrentNotification(NotificationProperties notification) {
        this.currentNotification = notification;
    }

    /**
     * This method is called when the user presses the "New Notification" button, and creates 
     * a new Notification to modify.
     */
    public void createNotification() {
        this.currentNotification = new NotificationProperties();
    }

    /**
     * Removes the selected bulletin from the set of bulletins on the current Credential instance.
     * 
     * @param toRemove
     */
    public void removeNotification(NotificationProperties toRemove) {
        this.currentInstance.getNotificationProperties().remove(toRemove);
    }

    /**
     * Saves the selected Notification onto the current Credential instance.
     */
    public void saveNotification() {
        this.currentInstance.getNotificationProperties().add(this.currentNotification);
    }

    public Bulletin getCurrentBulletin() {
        return this.currentBulletin;
    }

    public void setCurrentBulletin(Bulletin newBulletin) {
        this.currentBulletin = newBulletin;
    }

    /**
     * This method is called when the user presses the "New Bulletin" button, and creates 
     * a new bulletin to modify.
     */
    public void createBulletin() {
        this.currentBulletin = new Bulletin();
    }

    /**
     * Removes the selected bulletin from the set of bulletins on the current Account instance.
     * 
     * @param toRemove
     */
    public void removeBulletin(Bulletin toRemove) {
        this.currentInstance.getBulletins().remove(toRemove);
    }

    /**
     * Saves the selected Bulletin onto the current Account instance.
     */
    public void saveBulletin() {
        this.currentInstance.getBulletins().add(this.currentBulletin);
    }

    public AccountXAccountPropertyType getCurrentProperty() {
        return this.currentProperty;
    }

    public void setCurrentProperty(AccountXAccountPropertyType property) {
        this.currentProperty = property;
    }

    /**
     * This method is called when the user presses the "New Property" button, and creates 
     * a new property to modify.
     */
    public void createProperty() {
        this.currentProperty = new AccountXAccountPropertyType();
    }

    /**
     * Removes the selected property from the set of properties on the current Account instance.
     * 
     * @param toRemove
     */
    public void removeProperty(AccountXAccountPropertyType toRemove) {
        this.currentInstance.getProperties().remove(toRemove);
    }

    /**
     * Saves the selected property onto the current Account instance.
     */
    public void saveProperty() {
        this.currentInstance.getProperties().add(this.currentProperty);
    }

    public void removeCredential(Credential toRemove) {
        credentialsRemoved.add(toRemove);
        toRemove.setRootAccount(null);
        this.currentInstance.getCredentials().remove(toRemove);
    }

    public void removeGroup(Group toRemove) {
        this.currentInstance.getGroups().remove(toRemove);
    }

    public PaymentInformation getCurrentPaymentInformation() {
        return this.currentPaymentInformation;
    }

    public void setCurrentPaymentInformation(PaymentInformation paymentInformation) {
        this.currentPaymentInformation = paymentInformation;
    }

    /**
     * This method is called when the user presses the "Edit Payment Information" button, and creates 
     * a new Payment Information or presents the existing Payment Information.
     */
    public void fetchPaymentInformation() {
        if (this.currentInstance.getPaymentInformation() == null) {
            this.currentPaymentInformation = new PaymentInformation();
        }
        else {
            this.currentPaymentInformation = this.currentInstance.getPaymentInformation();
        }
    }

    public void togglePaymentInfoAddress() {
        if (this.currentPaymentInformation.getBillingAddress() == null) {
            this.currentPaymentInformation.setBillingAddress(new Address());
        }
        else {
            this.currentPaymentInformation.setBillingAddress(null);
        }
    }

    /**
     * Saves the Payment Information onto the current Credential instance.
     */
    public void savePaymentInformation() {
        this.currentInstance.setPaymentInformation(this.currentPaymentInformation);
    }

    public void clearPaymentInformation() {
        this.currentInstance.setPaymentInformation(null);
    }

    public IpAddress getCurrentIpAddress() {
        return this.currentIpAddress;
    }

    public void setCurrentIpAddress(IpAddress address) {
        this.currentIpAddress = address;
    }

    /**
     * This method is called when the user presses the "Add IP Address" button, and creates 
     * a new IP Address to modify.
     */
    public void createIpAddress() {
        this.currentIpAddress = new IpAddress();
    }

    /**
     * Removes the selected address from the set of adressess on the current Credential instance.
     * 
     * @param toRemove
     */
    public void removeIpAddress(IpAddress toRemove) {
        this.currentInstance.getIps().remove(toRemove);
    }

    /**
     * Saves the selected IP Address onto the current Credential instance.
     */
    public void saveIpAddress() {
        this.currentInstance.getIps().add(this.currentIpAddress);
    }

    public CustomerSpecificIconUrl getCurrentIcon() {
        return this.currentIcon;
    }

    public void setCurrentIcon(CustomerSpecificIconUrl newIcon) {
        this.currentIcon = newIcon;
    }

    /**
     * This method is called when the user presses the "New Bulletin" button, and creates 
     * a new bulletin to modify.
     */
    public void createIcon() {
        this.currentIcon = new CustomerSpecificIconUrl();
    }

    /**
     * Removes the selected bulletin from the set of bulletins on the current Account instance.
     * 
     * @param toRemove
     */
    public void removeIcon(CustomerSpecificIconUrl toRemove) {
        this.currentInstance.getIconUrls().remove(toRemove);
    }

    public void clearIconProperty() {
        this.currentIcon.setPropertyType(null);
    }

    public void clearIconClassification() {
        this.currentIcon.setClassificationType(null);
    }

    /**
     * Saves the selected Bulletin onto the current Account instance.
     */
    public void saveIcon() {
        this.currentInstance.getIconUrls().add(this.currentIcon);
    }

    @Inject
    private Conversation conversation;

    @Inject
    private CredentialBp credentialBp;

    public String create() {
        this.conversation.begin();
        return "accountAdd?faces-redirect=true";
    }

    public void retrieve() {
        if (accountSearchBean.getSelection() != null) {
            if (!this.linkingAccount) {
                this.currentInstance = bp.findById(accountSearchBean.getSelection().getId(), Account.class);
                bp.eagerLoad(this.currentInstance);
            }
            else {
                this.currentAccount = bp.findById(accountSearchBean.getSelection().getId(), Account.class);
                bp.eagerLoad(this.currentAccount);
            }
            populatePageNumberDisplay();
            accountSearchBean.setSelection(null);
        }

        if (FacesContext.getCurrentInstance().isPostback()) {
            return;
        }

        if (this.conversation.isTransient()) {
            this.conversation.begin();
        }

        if (this.currentInstance.getId() == null) {
            this.currentInstance = this.example;
        }
        else {
            this.currentInstance = bp.findById(this.currentInstance.getId(), Account.class);
        }
        bp.eagerLoad(this.currentInstance);
        populatePageNumberDisplay();

    }

    public void setCredential(Credential toAdd) {
        if (toAdd.getRootAccount() != null && toAdd.getRootAccount().getId() != null
                && !toAdd.getRootAccount().getId().equals(this.currentInstance.getId())) {
            //TODO: handle case where Credential is already assigned
            //In this case, creates a new, cloned Credential instance
            //TODO: class not found exception when cloning
            //toAdd = credentialBp.cloneEntity(toAdd);
        }
        else {
            toAdd.setRootAccount(this.currentInstance);
            this.currentInstance.getCredentials().add(toAdd);
        }
    }

    public void setGroup(Group toAdd) {
        this.currentInstance.getGroups().add(toAdd);
    }

    /*
     * Support updating and deleting Account entities
     */

    public String update() {
        if (!validToUpdate()) {
            return null;
        }

        if (!this.conversation.isTransient()) {
            this.conversation.end();
            resetParentChildButtons();
        }
        try {
            for (Credential c : this.currentInstance.getCredentials()) {
                credentialBp.update(c);
            }
            for (Credential c : credentialsRemoved) {
                credentialBp.update(c);
            }

            if (this.currentInstance.getId() == null) {
                this.currentInstance.setCreationDate(new Date());
            }

            Account persistedAccount = bp.update(this.currentInstance);
            bp.eagerLoad(this.currentInstance);

            if (this.recalculateLineage) {
                AccountListener.updateListener(persistedAccount);
            }

            if (this.addingRootAccount) {
                return "rootAccountAdd?faces-redirect=true";
            }
            else if (currentInstance.getId() == null) {
                return "accountAdd?faces-redirect=true";
            }
            else {
                return "accountEdit?faces-redirect=true";
            }
        }
        catch (Exception e) {
            LOG.error(e.getMessage());
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, "Unexpected Error", "Unexpected Error"));
            return null;
        }
    }

    public String delete() {
        this.conversation.end();
        try {
            bp.delete(this.currentInstance.getId(), Account.class);
            return "accountEdit?faces-redirect=true";
        }
        catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), e.getMessage()));
            return null;
        }
    }

    /*
     * Support searching Account entities with pagination
     */

    private int page;
    private long count;
    private List<Account> pageItems;

    private Account example = new Account();

    public int getPage() {
        return this.page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    @SuppressWarnings("static-method")
    public int getPageSize() {
        return 10;
    }

    public Account getExample() {
        return this.example;
    }

    public void setExample(Account example) {
        this.example = example;
    }

    public void search() {
        this.page = 0;
    }

    public void paginate() {
        this.pageItems = bp.searchByExample(this.example, getPage() * getPageSize(), getPageSize());

        this.count = bp.resultQuantity(this.example);
    }

    public List<Account> getPageItems() {
        return this.pageItems;
    }

    public long getCount() {
        return this.count;
    }

    /*
     * Support listing and POSTing back Account entities (e.g. from inside an
     * HtmlSelectOneMenu)
     */

    public List<Account> getAll() {

        return bp.findAll(Account.class, 0, 0);
    }

    public List<Account> searchByExample(Account search) {
        return bp.searchByExample(search, 0, 0);
    }

    /**
     * Once the current instance of this bean has a business key set, the rest of the fields can be 
     * populated using this method.
     */
    public void retrieveByBusinessKey() {
        Account search = new Account();
        search.setName(this.currentInstance.getName());
        this.currentInstance = null;
        this.currentInstance = bp.searchByExample(search, 0, 0).get(0);
        bp.eagerLoad(this.currentInstance);
    }

    public List<Account> searchByString(String s) {
        Account search = new Account();
        search.setName(s);
        return searchByExample(search);
    }

    //   @Resource
    //   private SessionContext sessionContext;

    /*
     * Support adding children to bidirectional, one-to-many tables
     */

    private Account add = new Account();

    public Account getAdd() {
        return this.add;
    }

    public Account getAdded() {
        Account added = this.add;
        this.add = new Account();
        return added;
    }

    @Inject
    private PhoneNumberBp phoneNumberBp;

    public String getPhoneNumberReadable() {
        return phoneNumberBp.getReadableNumber(this.currentNumber);
    }

    public void setPhoneNumberReadable(String phoneNumberReadable) {
        this.phoneNumberBp.setReadableNumber(this.currentNumber, phoneNumberReadable);
    }

    public String getPhoneNumberReadableRegex() {
        return this.phoneNumberBp.getReadableNumberRegex();
    }

    public void removePhoneNumber(PhoneNumber toRemove) {
        this.currentInstance.getNumbers().remove(toRemove);
    }

    public void addPhoneNumber() {
        this.currentInstance.getNumbers().add(this.currentNumber);
        this.currentNumber.setAccount(this.currentInstance);
        this.currentNumber = new PhoneNumber();
    }

    public void createPhoneNumber() {
        this.currentNumber = new PhoneNumber();
    }

    public PhoneNumber getCurrentNumber() {
        return currentNumber;
    }

    public void setCurrentNumber(PhoneNumber currentNumber) {
        this.currentNumber = currentNumber;
    }

    public void addChildAccount() {
        if (this.childOrParent.equalsIgnoreCase("child")) {
            this.bp.addChildAccount(this.currentInstance, this.currentAccount);
            disableChangeParent();
        }
        else {
            this.bp.addChildAccount(this.currentAccount, this.currentInstance);
            disableAddChild();
        }
        this.linkingAccount = false;
        this.recalculateLineage = true;
    }

    public void removeChildAccount() {

    }

    public Account getCurrentAccount() {
        return currentAccount;
    }

    public void setCurrentAccount(Account currentAccount) {
        this.currentAccount = currentAccount;
    }

    public boolean isLinkingAccount() {
        return linkingAccount;
    }

    public void setLinkingAccount(boolean linkingAccount) {
        this.linkingAccount = linkingAccount;
    }

    public void setChildOrParent(String childOrParent) {
        this.linkingAccount = true;
        this.childOrParent = childOrParent;
    }

    public void clearAccount() {
        this.currentAccount = new Account();
    }

    public void disableChangeParent() {
        this.addChildDisabled = false;
        this.changeParentDisabled = true;
    }

    public void disableAddChild() {
        this.addChildDisabled = true;
        this.changeParentDisabled = false;
    }

    public void resetParentChildButtons() {
        this.addChildDisabled = false;
        this.changeParentDisabled = false;
    }

    /*
     public Process getCurrentProcess() {
     return currentProcess;
     }

     public void setCurrentProcess(Process currentProcess) {
     this.currentProcess = currentProcess;
     }
     public void createProcess() {
     this.currentProcess = new Process();
     }
     public void addProcessVariable() {
     this.currentProcessConfiguration = new ProcessConfiguration();
     this.currentProcessConfiguration.getProcessVariables().add(new ProcessVariable());
     this.currentProcess.getProcessConfigurations().add(this.currentProcessConfiguration);
     }
     public void addProcess() {
     this.currentInstance.getProcessConfigurations().add(this.currentProcess);
     this.currentProcess.setAccount(this.currentInstance);
     this.currentProcess = new Process();
    
     this.currentInstance.getAddresses().add(this.currentAddress);
     this.currentAddress.setAccount(this.currentInstance);
     this.currentAddress = new Address();
     }
     public void removeProcess(Process toRemove) {
     this.currentInstance.getAddresses().remove(toRemove);
     }
     */

    @Inject
    private ValidationBean validationBean;

    public void validateUniqueProperties(FacesContext context, UIComponent component, Object value) {
        Collection<Object> toReturn = new HashSet<Object>();
        for (Object o : this.currentInstance.getProperties()) {
            toReturn.add(o);
        }
        if (this.validationBean.repeatedEntry(toReturn, "getType")) {
            ((UIInput) component).setValid(false);
            FacesMessage message = new FacesMessage();
            message.setSeverity(FacesMessage.SEVERITY_ERROR);
            message.setSummary("Multiple properties with the same type");
            message.setDetail("You can't have multiple properties with the same type.");
            context.addMessage(component.getClientId(), message);
        }
    }

    public void rootValidation(FacesContext context, UIComponent component, Object value) {
        if (this.currentAccount == null || this.currentAccount.getName() == null
                || this.currentAccount.getName().isEmpty()) {
            ((UIInput) component).setValid(false);
            FacesMessage message = new FacesMessage();
            message.setSeverity(FacesMessage.SEVERITY_ERROR);
            message.setSummary("No Account Selected");
            message.setDetail("Please Choose a Related Account using the Search Box.");
            context.addMessage(component.getClientId(), message);
            return;
        }

        if ((this.childOrParent.equalsIgnoreCase("child") && !this.bp.canAddParentToChild(this.currentInstance,
                this.currentAccount))
                || (this.childOrParent.equalsIgnoreCase("parent") && !this.bp.canAddParentToChild(this.currentAccount,
                        this.currentInstance))) {
            ((UIInput) component).setValid(false);
            FacesMessage message = new FacesMessage();
            message.setSeverity(FacesMessage.SEVERITY_ERROR);
            message.setSummary("Setting the parent to an account in a different tree");
            message.setDetail("An account in a different tree as the child account can't be used as the parent.");
            context.addMessage(component.getClientId(), message);
            return;
        }
        if ((this.childOrParent.equalsIgnoreCase("child") && this.bp.isCyclicTree(this.currentInstance,
                this.currentAccount))
                || (this.childOrParent.equalsIgnoreCase("parent") && this.bp.isCyclicTree(this.currentAccount,
                        this.currentInstance))) {
            ((UIInput) component).setValid(false);
            FacesMessage message = new FacesMessage();
            message.setSeverity(FacesMessage.SEVERITY_ERROR);
            message.setSummary("Tree will be cyclical");
            message.setDetail("Setting this account relationship will result in accounts being parents of themselves.");
            context.addMessage(component.getClientId(), message);
            return;
        }
    }

    private boolean validToUpdate() {
        boolean toReturn = true;
        Account searchAccount = new Account();
        searchAccount.setName(this.currentInstance.getName());
        List<Account> searchList = this.bp.searchByExactExample(searchAccount, 0, 0);
        if (searchList.size() != 0 && !searchList.get(0).getId().equals(this.currentInstance.getId())) {
            FacesMessage message = new FacesMessage();
            message.setSeverity(FacesMessage.SEVERITY_ERROR);
            message.setSummary("Name already exists");
            message.setDetail("An account already exists with the specified name.");
            FacesContext.getCurrentInstance().addMessage(null, message);
            toReturn = false;
        }
        if (this.currentInstance.getId() == null && this.currentInstance.getParentAccount() == null
                && !this.addingRootAccount) {
            FacesMessage message = new FacesMessage();
            message.setSeverity(FacesMessage.SEVERITY_ERROR);
            message.setSummary("You must select a parent account");
            message.setDetail("The parent account for this account cannot be null.");
            FacesContext.getCurrentInstance().addMessage(null, message);
            toReturn = false;
        }
        if (StringUtils.isNotEmpty(this.currentInstance.getLocationCode())) {
            searchAccount = new Account();
            searchAccount.setRootAccount(new Account());
            searchAccount.getRootAccount().setId(this.currentInstance.getId());
            if (this.currentInstance.getRootAccount() != null) {
                searchAccount.getRootAccount().setId(this.currentInstance.getRootAccount().getId());
            }
            if (searchAccount.getRootAccount().getId() != null) {
                searchAccount.setLocationCode(this.currentInstance.getLocationCode());
                //gets first two accounts with the location code
                searchList = bp.searchByExactExample(searchAccount, 0, 2);
                //if there are at least two accounts with the location code, or one account with the code and it's
                //not this account, then the location code already exists
                if (searchList.size() != 0 && !searchList.get(0).equals(this.currentInstance)) {
                    FacesMessage message = new FacesMessage();
                    message.setSeverity(FacesMessage.SEVERITY_ERROR);
                    message.setSummary("Location Code already exists");
                    message.setDetail("An account already exists with the specified location code.");
                    FacesContext.getCurrentInstance().addMessage(null, message);
                    toReturn = false;
                }
            }

        }
        if (this.currentInstance.getCxmlConfiguration() != null
                && this.currentInstance.getCxmlConfiguration().getId() != null) {
            searchAccount = new Account();
            searchAccount.setCxmlConfiguration(new CxmlConfiguration());
            searchAccount.getCxmlConfiguration().setId(this.currentInstance.getCxmlConfiguration().getId());
            searchList = this.bp.searchByExactExample(searchAccount, 0, 0);
            searchList.remove(this.currentInstance);
            if (!searchList.isEmpty()) {
                FacesMessage message = new FacesMessage();
                message.setSeverity(FacesMessage.SEVERITY_ERROR);
                message.setSummary("CXML Configuration Already Assigned");
                message.setDetail("The CXML configuration is already assigned to another account");
                FacesContext.getCurrentInstance().addMessage(null, message);
                toReturn = false;
            }
        }
        return toReturn;
    }

    /**
     * This method validates an IP address. Invalidates if the address is blank, or there is already an address with 
     * that value.
     * 
     * @param context
     * @param component
     * @param value
     */
    public void addressValidator(FacesContext context, UIComponent component, Object value) {
        if (value == null || value.toString().isEmpty()) {
            ((UIInput) component).setValid(false);
            FacesMessage message = new FacesMessage();
            message.setSeverity(FacesMessage.SEVERITY_ERROR);
            message.setSummary("No Address Specified");
            message.setDetail("Please enter an IP address.");
            context.addMessage(component.getClientId(), message);
            return;
        }
        for (IpAddress address : this.getAccount().getIps()) {
            if (!address.equals(this.getCurrentIpAddress()) && address.getValue().equals(value.toString())) {
                ((UIInput) component).setValid(false);
                FacesMessage message = new FacesMessage();
                message.setSeverity(FacesMessage.SEVERITY_ERROR);
                message.setSummary("Duplicate Address");
                message.setDetail("This address has already been added.");
                context.addMessage(component.getClientId(), message);
                return;
            }
        }
    }

    public boolean isChangeParentDisabled() {
        return changeParentDisabled
                || (this.currentInstance.getRootAccount() == null && this.currentInstance.getId() != null);
    }

    public void setChangeParentDisabled(boolean changeParentDisabled) {
        this.changeParentDisabled = changeParentDisabled;
    }

    public boolean isAddChildDisabled() {
        return addChildDisabled || (this.currentInstance.getId() == null);
    }

    public void setAddChildDisabled(boolean addChildDisabled) {
        this.addChildDisabled = addChildDisabled;
    }

    public void setCxmlConfiguration(CxmlConfiguration cxmlConfiguration) {
        this.currentInstance.setCxmlConfiguration(cxmlConfiguration);
    }

    public boolean isAddingRootAccount() {
        return addingRootAccount;
    }

    public void setAddingRootAccount(boolean addingRootAccount) {
        this.addingRootAccount = addingRootAccount;
    }

    public Collection<Account> getChildren() {
        //TODO: fix this as part of PHOEN-4721
        return this.bp.getChildren(this.currentInstance);
    }

    public void setPerson(Person person) {
        this.currentInstance.setPrimaryContact(person);
    }

}
