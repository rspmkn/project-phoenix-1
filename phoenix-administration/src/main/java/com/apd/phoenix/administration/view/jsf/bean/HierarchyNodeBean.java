package com.apd.phoenix.administration.view.jsf.bean;

import java.io.Serializable;
import java.util.List;
import javax.ejb.Stateful;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.ConversationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.inject.Inject;
import javax.inject.Named;
import com.apd.phoenix.service.business.HierarchyNodeBp;
import com.apd.phoenix.service.model.HierarchyNode;
import com.apd.phoenix.service.model.Sku;

/**
 * Backing bean for HierarchyNode entities.
 * <p>
 * This class provides CRUD functionality for all HierarchyNode entities. It focuses
 * purely on Java EE 6 standards (e.g. <tt>&#64;ConversationScoped</tt> for
 * state management, <tt>PersistenceContext</tt> for persistence,
 * <tt>CriteriaBuilder</tt> for searches) rather than introducing a CRUD framework or
 * custom base class.
 */

@Named
@Stateful
@ConversationScoped
public class HierarchyNodeBean implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;
    private int page;
    private long count;
    private List<HierarchyNode> pageHierarchyNodes;
    private HierarchyNode currentInstance = new HierarchyNode();
    private Sku currentSku = new Sku();
    private HierarchyNode example = new HierarchyNode();
    private HierarchyNode add = new HierarchyNode();

    //   @Resource
    //   private SessionContext sessionContext;

    @Inject
    private HierarchyNodeBp hierarchyNodeBp;

    @Inject
    private Conversation conversation;

    @Inject
    private HierarchyNodeSearchBean hierarchyNodeSearchBean;

    public String create() {
        this.conversation.begin();
        return "hierarchyNodeAdd?faces-redirect=true";
    }

    public void retrieve() {
        if (hierarchyNodeSearchBean.getSelection() != null) {
            this.currentInstance = hierarchyNodeBp.findById(hierarchyNodeSearchBean.getSelection().getId(),
                    HierarchyNode.class);
            hierarchyNodeBp.eagerLoad(this.currentInstance);

            hierarchyNodeSearchBean.setSelection(null);
        }

        if (FacesContext.getCurrentInstance().isPostback()) {
            return;
        }

        if (this.conversation.isTransient()) {
            this.conversation.begin();
        }

        if (this.currentInstance.getId() == null) {
            this.currentInstance = this.example;
        }
        else {
            this.currentInstance = hierarchyNodeBp.findById(this.currentInstance.getId(), HierarchyNode.class);
        }

        hierarchyNodeBp.eagerLoad(this.currentInstance);

    }

    /*
     * Support updating and deleting HierarchyNode entities
     */

    public String update() {
        this.conversation.end();

        try {
            if (currentInstance.getId() == null) {
                hierarchyNodeBp.update(this.currentInstance);
                return "hierarchyNodeAdd?faces-redirect=true";
            }
            else {
                hierarchyNodeBp.update(this.currentInstance);
                hierarchyNodeBp.eagerLoad(this.currentInstance);
                return "hierarchyNodeEdit?faces-redirect=true";
            }
        }
        catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(e.getMessage()));
            return null;
        }
    }

    public String delete() {
        this.conversation.end();
        try {
            hierarchyNodeBp.delete(this.currentInstance.getId(), HierarchyNode.class);
            return "hierarchyNodeEdit?faces-redirect=true";
        }
        catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(e.getMessage()));
            return null;
        }
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public HierarchyNode getHierarchyNode() {
        return this.currentInstance;
    }

    public int getPage() {
        return this.page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    @SuppressWarnings("static-method")
    public int getPageSize() {
        return 10;
    }

    public HierarchyNode getExample() {
        return this.example;
    }

    public void setExample(HierarchyNode example) {
        this.example = example;
    }

    public void search() {
        this.page = 0;
    }

    public void paginate() {
        this.pageHierarchyNodes = hierarchyNodeBp.searchByExample(this.example, getPage() * getPageSize(),
                getPageSize());

        this.count = hierarchyNodeBp.resultQuantity(this.example);
    }

    public List<HierarchyNode> getPageHierarchyNodes() {
        return this.pageHierarchyNodes;
    }

    public long getCount() {
        return this.count;
    }

    public List<HierarchyNode> getAll() {

        return hierarchyNodeBp.findAll(HierarchyNode.class, 0, 0);
    }

    public List<HierarchyNode> searchByExample(HierarchyNode search) {
        return hierarchyNodeBp.searchByExample(search, 0, 0);
    }

    /**
     * Once the current instance of this bean has a business key set, the rest of the fields can be 
     * populated using this method.
     */
    public void retrieveByBusinessKey() {
        HierarchyNode search = new HierarchyNode();
        search.setDescription(this.currentInstance.getDescription());
        this.currentInstance = null;
        this.currentInstance = hierarchyNodeBp.searchByExample(search, 0, 0).get(0);
        hierarchyNodeBp.eagerLoad(this.currentInstance);
    }

    public List<HierarchyNode> searchByString(String s) {
        HierarchyNode search = new HierarchyNode();
        search.setDescription(s);
        return searchByExample(search);
    }

    public Converter getConverter() {

        return new Converter() {

            @Override
            public Object getAsObject(FacesContext context, UIComponent component, String value) {
                return hierarchyNodeBp.findById(Long.valueOf(value), HierarchyNode.class);
            }

            @Override
            public String getAsString(FacesContext context, UIComponent component, Object value) {

                if (value == null) {
                    return "";
                }

                return String.valueOf(((HierarchyNode) value).getId());
            }
        };
    }

    public HierarchyNode getAdd() {
        return this.add;
    }

    public HierarchyNode getAdded() {
        HierarchyNode added = this.add;
        this.add = new HierarchyNode();
        return added;
    }

}