package com.apd.phoenix.administration.view.jsf.bean;

import java.util.List;
import javax.enterprise.context.ConversationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import com.apd.phoenix.service.model.InvoiceSendMethod;
import com.apd.phoenix.service.persistence.jpa.InvoiceSendMethodDao;
import com.apd.phoenix.web.searchbeans.AbstractSearchBean;
import java.io.Serializable;

@Named
@ConversationScoped
public class InvoiceSendMethodSearchBean extends AbstractSearchBean<InvoiceSendMethod> implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 4564141656617269064L;
    @Inject
    InvoiceSendMethodDao dao;

    @Override
    public String[] getCriteria() {
        String[] toReturn = { "Name" };
        return toReturn;
    }

    @Override
    protected List<InvoiceSendMethod> search() {
        InvoiceSendMethod search = new InvoiceSendMethod();
        search.setName(this.getValues().get(0).getValue());
        return dao.searchByExample(search, 0, 0);
    }

}
