package com.apd.phoenix.administration.view.jsf.bean;

import java.io.Serializable;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateful;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.ConversationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.inject.Inject;
import javax.inject.Named;
import com.apd.phoenix.service.business.PermissionBp;
import com.apd.phoenix.service.model.Permission;

/**
 * Backing bean for Permission entities.
 * <p>
 * This class provides CRUD functionality for all Permission entities. It
 * focuses purely on Java EE 6 standards (e.g. <tt>&#64;ConversationScoped</tt>
 * for state management, <tt>PersistenceContext</tt> for persistence,
 * <tt>CriteriaBuilder</tt> for searches) rather than introducing a CRUD
 * framework or custom base class.
 */

@Named
@Stateful
@ConversationScoped
public class PermissionBean implements Serializable {

    private static final long serialVersionUID = 1L;
    @Inject
    private PermissionBp permissionBp;
    @Inject
    private Conversation conversation;

    @Resource
    private SessionContext sessionContext;

    private Long id;
    private Permission permission;
    private Permission example = new Permission();
    private Permission add = new Permission();

    private int page;
    private long count;

    private List<Permission> pageItems;

    public void validateSelection(FacesContext context, UIComponent component, Object value) {
        if (this.permission.getId() == null) {
            ((UIInput) component).setValid(false);
            FacesMessage message = new FacesMessage();
            message.setSeverity(FacesMessage.SEVERITY_ERROR);
            message.setSummary("Select Permission to edit");
            message.setDetail("You need to select a Permission to edit.");
            context.addMessage(component.getClientId(), message);
            return;
        }
    }

    /*
     * Support creating and retrieving Permission entities
     */
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Permission getPermission() {
        return this.permission;
    }

    public String create() {
        this.conversation.begin();
        return "permissionAdd?faces-redirect=true";
    }

    public void retrieve() {

        if (FacesContext.getCurrentInstance().isPostback()) {
            return;
        }

        if (this.conversation.isTransient()) {
            this.conversation.begin();
        }

        if (this.id == null) {
            this.permission = this.example;
        }
        else {
            this.permission = permissionBp.findById(id, Permission.class);
        }
    }

    public Permission findById(Long id) {

        return this.permissionBp.findById(id, Permission.class);
    }

    /*
     * Support updating and deleting Permission entities
     */

    public String update() {
        this.conversation.end();

        try {
            if (this.permission.getId() == null) {
                this.permissionBp.create(this.permission);
                return "permissionAdd?faces-redirect=true";
            }
            else {
                this.permissionBp.update(this.permission);
                return "permissionEdit?faces-redirect=true&id=" + this.permission.getId();
            }
        }
        catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(e.getMessage()));
            return null;
        }
    }

    public String delete() {
        this.conversation.end();

        try {
            this.permissionBp.delete(this.permission.getId(), Permission.class);
            return "permissionEdit?faces-redirect=true";
        }
        catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(e.getMessage()));
            return null;
        }
    }

    /*
     * Support searching Permission entities with pagination
     */
    public int getPage() {
        return this.page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getPageSize() {
        return 10;
    }

    public Permission getExample() {
        return this.example;
    }

    public void setExample(Permission example) {
        this.example = example;
    }

    public void search() {
        this.page = 0;
    }

    public List<Permission> searchByExample(Permission search) {
        return permissionBp.searchByExample(search, 0, 0);
    }

    public List<Permission> searchByString(String s) {
        Permission search = new Permission();
        search.setName(s);
        return searchByExample(search);
    }

    public void paginate() {
        this.count = permissionBp.resultQuantity(this.example);
        this.pageItems = permissionBp.searchByExample(this.example, getPage() * getPageSize(), getPageSize());
    }

    public List<Permission> getPageItems() {
        return this.pageItems;
    }

    public long getCount() {
        return this.count;
    }

    /*
     * Support listing and POSTing back Permission entities (e.g. from
     * inside an HtmlSelectOneMenu)
     */

    public List<Permission> getAll() {
        return permissionBp.findAll(Permission.class, 0, 0);
    }

    public Converter getConverter() {

        final PermissionBean ejbProxy = this.sessionContext.getBusinessObject(PermissionBean.class);

        return new Converter() {

            @Override
            public Object getAsObject(FacesContext context, UIComponent component, String value) {

                return ejbProxy.findById(Long.valueOf(value));
            }

            @Override
            public String getAsString(FacesContext context, UIComponent component, Object value) {

                if (value == null) {
                    return "";
                }

                return String.valueOf(((Permission) value).getId());
            }
        };
    }

    /*
     * Support adding children to bidirectional, one-to-many tables
     */
    public Permission getAdd() {
        return this.add;
    }

    public Permission getAdded() {
        Permission added = this.add;
        this.add = new Permission();
        return added;
    }
}