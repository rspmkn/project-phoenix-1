package com.apd.phoenix.administration.view.jsf.bean;

import java.util.List;
import javax.enterprise.context.ConversationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import com.apd.phoenix.service.persistence.jpa.CashoutPageDao;
import com.apd.phoenix.service.model.CashoutPage;
import com.apd.phoenix.web.searchbeans.AbstractSearchBean;
import java.io.Serializable;

@Named
@ConversationScoped
public class CashoutPageSearchBean extends AbstractSearchBean<CashoutPage> implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 3789958454021131699L;
    @Inject
    CashoutPageDao bp;

    @Override
    public String[] getCriteria() {
        String[] toReturn = { "Name" };
        return toReturn;
    }

    @Override
    protected List<CashoutPage> search() {
        CashoutPage search = new CashoutPage();
        search.setName(this.getValues().get(0).getValue());
        return bp.searchByExample(search, 0, 0);
    }

}
