package com.apd.phoenix.administration.view.jsf.bean;

import java.io.Serializable;
import java.util.List;
import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateful;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.ConversationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.inject.Inject;
import javax.inject.Named;
import com.apd.phoenix.service.model.ShippingPartner;
import com.apd.phoenix.service.persistence.jpa.ShippingPartnerDao;

/**
 * Backing bean for ShippingPartner entities.
 * <p>
 * This class provides CRUD functionality for all ShippingPartner entities. It focuses
 * purely on Java EE 6 standards (e.g. <tt>&#64;ConversationScoped</tt> for
 * state management, <tt>PersistenceContext</tt> for persistence,
 * <tt>CriteriaBuilder</tt> for searches) rather than introducing a CRUD framework or
 * custom base class.
 */

@Named
@Stateful
@ConversationScoped
public class ShippingPartnerBean implements Serializable {

    private static final long serialVersionUID = 1L;

    /*
     * Support creating and retrieving ShippingPartner entities
     */

    public void validateSelection(FacesContext context, UIComponent component, Object value) {
        if (this.currentInstance.getId() == null) {
            ((UIInput) component).setValid(false);
            FacesMessage message = new FacesMessage();
            message.setSeverity(FacesMessage.SEVERITY_ERROR);
            message.setSummary("Select ShippingPartner to edit");
            message.setDetail("You need to select an ShippingPartner to edit.");
            context.addMessage(component.getClientId(), message);
            return;
        }
    }

    private Long id;

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    private ShippingPartner currentInstance;

    public ShippingPartner getShippingPartner() {
        return this.currentInstance;
    }

    @Inject
    private ShippingPartnerDao dao;

    @Inject
    private ShippingPartnerSearchBean searchBean;

    @Inject
    private Conversation conversation;

    public String create() {

        this.conversation.begin();
        return "shippingPartnerAdd?faces-redirect=true";
    }

    public void retrieve() {
        if (this.searchBean.getSelection() != null) {
            this.currentInstance = this.searchBean.getSelection();
            this.searchBean.setSelection(null);
        }

        if (FacesContext.getCurrentInstance().isPostback()) {
            return;
        }

        if (this.conversation.isTransient()) {
            this.conversation.begin();
        }

        if (this.id == null) {
            this.currentInstance = this.example;
        }
        else {
            this.currentInstance = dao.findById(id, ShippingPartner.class);
        }

    }

    /*
     * Support updating and deleting ShippingPartner entities
     */

    public String update() {
        if (!validToUpdate()) {
            return null;
        }

        if (!this.conversation.isTransient()) {
            this.conversation.end();
        }

        try {

            if (currentInstance.getId() == null) {
                dao.create(this.currentInstance);
                return "shippingPartnerAdd?faces-redirect=true";
            }
            else {
                dao.update(this.currentInstance);
                return "shippingPartnerEdit?faces-redirect=true";
            }
        }
        catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), e.getMessage()));
            return null;
        }
    }

    public String delete() {
        this.conversation.end();
        try {
            dao.delete(this.currentInstance.getId(), ShippingPartner.class);
            return "shippingPartnerEdit?faces-redirect=true";
        }
        catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), e.getMessage()));
            return null;
        }
    }

    /*
     * Support searching ShippingPartner entities with pagination
     */

    private int page;
    private long count;
    private List<ShippingPartner> pageItems;

    private ShippingPartner example = new ShippingPartner();

    public int getPage() {
        return this.page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getPageSize() {
        return 10;
    }

    public ShippingPartner getExample() {
        return this.example;
    }

    public void setExample(ShippingPartner example) {
        this.example = example;
    }

    public void search() {
        this.page = 0;
    }

    public void paginate() {
        this.pageItems = dao.searchByExample(this.example, getPage() * getPageSize(), getPageSize());

        this.count = dao.resultQuantity(this.example);
    }

    public List<ShippingPartner> getPageItems() {
        return this.pageItems;
    }

    public long getCount() {
        return this.count;
    }

    /*
     * Support listing and POSTing back ShippingPartner entities (e.g. from inside an
     * HtmlSelectOneMenu)
     */

    public List<ShippingPartner> getAll() {

        return dao.findAll(ShippingPartner.class, 0, 0);
    }

    public List<ShippingPartner> searchByExample(ShippingPartner search) {
        return dao.searchByExample(search, 0, 0);
    }

    public List<ShippingPartner> searchByString(String s) {
        ShippingPartner search = new ShippingPartner();
        search.setName(s);
        return searchByExample(search);
    }

    @Resource
    private SessionContext sessionContext;

    public Converter getConverter() {

        return new Converter() {

            @Override
            public Object getAsObject(FacesContext context, UIComponent component, String value) {
                return dao.findById(Long.valueOf(value), ShippingPartner.class);
            }

            @Override
            public String getAsString(FacesContext context, UIComponent component, Object value) {

                if (value == null) {
                    return "";
                }

                return String.valueOf(((ShippingPartner) value).getId());
            }
        };
    }

    /*
     * Support adding children to bidirectional, one-to-many tables
     */

    private ShippingPartner add = new ShippingPartner();

    public ShippingPartner getAdd() {
        return this.add;
    }

    public ShippingPartner getAdded() {
        ShippingPartner added = this.add;
        this.add = new ShippingPartner();
        return added;
    }

    private boolean validToUpdate() {
        ShippingPartner searchShippingPartner = new ShippingPartner();
        searchShippingPartner.setName(this.currentInstance.getName());
        List<ShippingPartner> searchList = this.dao.searchByExactExample(searchShippingPartner, 0, 0);
        if (searchList.size() != 0 && !searchList.get(0).getId().equals(this.currentInstance.getId())) {
            FacesMessage message = new FacesMessage();
            message.setSeverity(FacesMessage.SEVERITY_ERROR);
            message.setSummary("Name already exists");
            message.setDetail("A ShippingPartner already exists with the specified name.");
            FacesContext.getCurrentInstance().addMessage(null, message);
            return false;
        }
        return true;
    }
}