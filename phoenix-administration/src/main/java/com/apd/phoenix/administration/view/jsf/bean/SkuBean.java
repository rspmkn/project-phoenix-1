package com.apd.phoenix.administration.view.jsf.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateful;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.ConversationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import com.apd.phoenix.service.model.Sku;
import com.apd.phoenix.service.model.HierarchyNode;
import com.apd.phoenix.service.model.Item;
import com.apd.phoenix.service.model.SkuType;

/**
 * Backing bean for Sku entities.
 * <p>
 * This class provides CRUD functionality for all Sku entities. It focuses
 * purely on Java EE 6 standards (e.g. <tt>&#64;ConversationScoped</tt> for
 * state management, <tt>PersistenceContext</tt> for persistence,
 * <tt>CriteriaBuilder</tt> for searches) rather than introducing a CRUD framework or
 * custom base class.
 */

@Named
@Stateful
@ConversationScoped
@Deprecated
public class SkuBean implements Serializable {

    private static final long serialVersionUID = 1L;

    /*
     * Support creating and retrieving Sku entities
     */

    private Long id;

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    private Sku sku;

    public Sku getSku() {
        return this.sku;
    }

    @Inject
    private Conversation conversation;

    private EntityManager entityManager;

    public String create() {

        this.conversation.begin();
        return "create?faces-redirect=true";
    }

    public void retrieve() {

        if (FacesContext.getCurrentInstance().isPostback()) {
            return;
        }

        if (this.conversation.isTransient()) {
            this.conversation.begin();
        }

        if (this.id == null) {
            this.sku = this.example;
        }
        else {
            this.sku = findById(getId());
        }
    }

    public Sku findById(Long id) {

        return this.entityManager.find(Sku.class, id);
    }

    /*
     * Support updating and deleting Sku entities
     */

    public String update() {
        this.conversation.end();

        try {
            if (this.id == null) {
                this.entityManager.persist(this.sku);
                return "search?faces-redirect=true";
            }
            else {
                this.entityManager.merge(this.sku);
                return "view?faces-redirect=true&id=" + this.sku.getId();
            }
        }
        catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), e.getMessage()));
            return null;
        }
    }

    public String delete() {
        this.conversation.end();

        try {
            this.entityManager.remove(findById(getId()));
            this.entityManager.flush();
            return "search?faces-redirect=true";
        }
        catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), e.getMessage()));
            return null;
        }
    }

    /*
     * Support searching Sku entities with pagination
     */

    private int page;
    private long count;
    private List<Sku> pageItems;

    private Sku example = new Sku();

    public int getPage() {
        return this.page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getPageSize() {
        return 10;
    }

    public Sku getExample() {
        return this.example;
    }

    public void setExample(Sku example) {
        this.example = example;
    }

    public void search() {
        this.page = 0;
    }

    public List<Sku> getPageItems() {
        return this.pageItems;
    }

    public long getCount() {
        return this.count;
    }

    /*
     * Support listing and POSTing back Sku entities (e.g. from inside an
     * HtmlSelectOneMenu)
     */

    public List<Sku> getAll() {

        CriteriaQuery<Sku> criteria = this.entityManager.getCriteriaBuilder().createQuery(Sku.class);
        return this.entityManager.createQuery(criteria.select(criteria.from(Sku.class))).getResultList();
    }

    @Resource
    private SessionContext sessionContext;

    public Converter getConverter() {

        final SkuBean ejbProxy = this.sessionContext.getBusinessObject(SkuBean.class);

        return new Converter() {

            @Override
            public Object getAsObject(FacesContext context, UIComponent component, String value) {

                return ejbProxy.findById(Long.valueOf(value));
            }

            @Override
            public String getAsString(FacesContext context, UIComponent component, Object value) {

                if (value == null) {
                    return "";
                }

                return String.valueOf(((Sku) value).getId());
            }
        };
    }

    /*
     * Support adding children to bidirectional, one-to-many tables
     */

    private Sku add = new Sku();

    public Sku getAdd() {
        return this.add;
    }

    public Sku getAdded() {
        Sku added = this.add;
        this.add = new Sku();
        return added;
    }
}