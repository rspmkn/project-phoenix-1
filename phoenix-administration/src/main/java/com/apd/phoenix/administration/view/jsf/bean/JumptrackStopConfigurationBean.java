package com.apd.phoenix.administration.view.jsf.bean;

import com.apd.phoenix.service.business.JumptrackStopConfigurationBp;
import com.apd.phoenix.service.model.JumptrackStopConfiguration;
import java.io.Serializable;
import java.util.List;
import javax.ejb.Stateful;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.ConversationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

/**
 * Backing bean for JumptrackStopConfiguration entities.
 */

@Named
@Stateful
@ConversationScoped
public class JumptrackStopConfigurationBean implements Serializable {

    private static final long serialVersionUID = 1L;

    public void validateSelection(FacesContext context, UIComponent component, Object value) {
        if (this.currentInstance.getId() == null) {
            ((UIInput) component).setValid(false);
            FacesMessage message = new FacesMessage();
            message.setSeverity(FacesMessage.SEVERITY_ERROR);
            message.setSummary("Select Jumptrack Stop Configuration to edit");
            message.setDetail("You need to select a Jumptrack Stop Configuration to edit.");
            context.addMessage(component.getClientId(), message);
            return;
        }
    }

    private Long id;

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    private JumptrackStopConfiguration currentInstance;

    public JumptrackStopConfiguration getJumptrackStopConfiguration() {
        return this.currentInstance;
    }

    @Inject
    private JumptrackStopConfigurationBp bp;

    @Inject
    private JumptrackStopConfigurationSearchBean searchBean;

    @Inject
    private Conversation conversation;

    public String create() {
        this.conversation.begin();
        return "jumptrackStopConfigurationAdd?faces-redirect=true";
    }

    public void retrieve() {
        if (this.searchBean.getSelection() != null) {
            this.currentInstance = this.searchBean.getSelection();
            this.searchBean.setSelection(null);
        }

        if (FacesContext.getCurrentInstance().isPostback()) {
            return;
        }

        if (this.conversation.isTransient()) {
            this.conversation.begin();
        }

        if (this.id == null) {
            this.currentInstance = new JumptrackStopConfiguration();
        }
        else {
            this.currentInstance = bp.findById(id, JumptrackStopConfiguration.class);
        }

    }

    /*
     * Support updating and deleting JumptrackStopConfiguration entities
     */

    public String update() {
        if (!validToUpdate()) {
            return null;
        }

        if (!this.conversation.isTransient()) {
            this.conversation.end();
        }
        try {

            if (currentInstance.getId() == null) {
                bp.create(this.currentInstance);
                return "jumptrackStopConfigurationAdd?faces-redirect=true";
            }
            else {
                bp.update(this.currentInstance);
                return "jumptrackStopConfigurationEdit?faces-redirect=true";
            }
        }
        catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), e.getMessage()));
            return null;
        }
    }

    public String delete() {
        this.conversation.end();
        try {
            bp.delete(this.currentInstance.getId(), JumptrackStopConfiguration.class);
            return "jumptrackStopConfigurationEdit?faces-redirect=true";
        }
        catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), e.getMessage()));
            return null;
        }
    }

    private boolean validToUpdate() {
        JumptrackStopConfiguration searchJumptrackStopConfiguration = new JumptrackStopConfiguration();
        searchJumptrackStopConfiguration.setZip(this.currentInstance.getZip());
        List<JumptrackStopConfiguration> searchList = this.bp.searchByExactExample(searchJumptrackStopConfiguration, 0,
                0);
        if (searchList.size() != 0 && !searchList.get(0).getId().equals(this.currentInstance.getId())) {
            FacesMessage message = new FacesMessage();
            message.setSeverity(FacesMessage.SEVERITY_ERROR);
            message.setSummary("Configuration exists for that zip");
            message.setDetail("A configuration already exists for that zip code");
            FacesContext.getCurrentInstance().addMessage(null, message);
            return false;
        }
        return true;
    }
}