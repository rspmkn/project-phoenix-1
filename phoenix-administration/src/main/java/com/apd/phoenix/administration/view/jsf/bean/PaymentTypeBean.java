package com.apd.phoenix.administration.view.jsf.bean;

import java.io.Serializable;
import java.util.List;
import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateful;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.ConversationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.inject.Inject;
import javax.inject.Named;
import com.apd.phoenix.service.model.PaymentType;
import com.apd.phoenix.service.persistence.jpa.PaymentTypeDao;

/**
 * Backing bean for PaymentType entities.
 * <p>
 * This class provides CRUD functionality for all PaymentType entities. It focuses
 * purely on Java EE 6 standards (e.g. <tt>&#64;ConversationScoped</tt> for
 * state management, <tt>PersistenceContext</tt> for persistence,
 * <tt>CriteriaBuilder</tt> for searches) rather than introducing a CRUD framework or
 * custom base class.
 */

@Named
@Stateful
@ConversationScoped
public class PaymentTypeBean implements Serializable {

    private static final long serialVersionUID = 1L;

    /*
     * Support creating and retrieving PaymentType entities
     */

    public void validateSelection(FacesContext context, UIComponent component, Object value) {
        if (this.currentInstance.getId() == null) {
            ((UIInput) component).setValid(false);
            FacesMessage message = new FacesMessage();
            message.setSeverity(FacesMessage.SEVERITY_ERROR);
            message.setSummary("Select Event Type to edit");
            message.setDetail("You need to select an Event Type to edit.");
            context.addMessage(component.getClientId(), message);
            return;
        }
    }

    private Long id;

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    private PaymentType currentInstance;

    public PaymentType getPaymentType() {
        return this.currentInstance;
    }

    @Inject
    private PaymentTypeDao dao;

    @Inject
    private PaymentTypeSearchBean searchBean;

    @Inject
    private Conversation conversation;

    public String create() {

        this.conversation.begin();
        return "paymentTypeAdd?faces-redirect=true";
    }

    public void retrieve() {
        if (this.searchBean.getSelection() != null) {
            this.currentInstance = this.searchBean.getSelection();
            this.searchBean.setSelection(null);
        }

        if (FacesContext.getCurrentInstance().isPostback()) {
            return;
        }

        if (this.conversation.isTransient()) {
            this.conversation.begin();
        }

        if (this.id == null) {
            this.currentInstance = this.example;
        }
        else {
            this.currentInstance = dao.findById(id, PaymentType.class);
        }

    }

    /*
     * Support updating and deleting PaymentType entities
     */

    public String update() {
        this.conversation.end();

        try {

            if (currentInstance.getId() == null) {
                dao.create(this.currentInstance);
                return "paymentTypeAdd?faces-redirect=true";
            }
            else {
                dao.update(this.currentInstance);
                return "paymentTypeEdit?faces-redirect=true";
            }
        }
        catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(e.getMessage()));
            return null;
        }
    }

    public String delete() {
        this.conversation.end();
        try {
            dao.delete(this.currentInstance.getId(), PaymentType.class);
            return "paymentTypeEdit?faces-redirect=true";
        }
        catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(e.getMessage()));
            return null;
        }
    }

    /*
     * Support searching PaymentType entities with pagination
     */

    private int page;
    private long count;
    private List<PaymentType> pageItems;

    private PaymentType example = new PaymentType();

    public int getPage() {
        return this.page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getPageSize() {
        return 10;
    }

    public PaymentType getExample() {
        return this.example;
    }

    public void setExample(PaymentType example) {
        this.example = example;
    }

    public void search() {
        this.page = 0;
    }

    public void paginate() {
        this.pageItems = dao.searchByExample(this.example, getPage() * getPageSize(), getPageSize());

        this.count = dao.resultQuantity(this.example);
    }

    public List<PaymentType> getPageItems() {
        return this.pageItems;
    }

    public long getCount() {
        return this.count;
    }

    /*
     * Support listing and POSTing back PaymentType entities (e.g. from inside an
     * HtmlSelectOneMenu)
     */

    public List<PaymentType> getAll() {

        return dao.findAll(PaymentType.class, 0, 0);
    }

    public List<PaymentType> searchByExample(PaymentType search) {
        return dao.searchByExample(search, 0, 0);
    }

    public List<PaymentType> searchByString(String s) {
        PaymentType search = new PaymentType();
        search.setName(s);
        return searchByExample(search);
    }

    @Resource
    private SessionContext sessionContext;

    public Converter getConverter() {
        return new Converter() {

            @Override
            public Object getAsObject(FacesContext context, UIComponent component, String value) {
                return dao.findById(Long.valueOf(value), PaymentType.class);
            }

            @Override
            public String getAsString(FacesContext context, UIComponent component, Object value) {

                if (value == null) {
                    return "";
                }

                return String.valueOf(((PaymentType) value).getId());
            }
        };
    }

    /*
     * Support adding children to bidirectional, one-to-many tables
     */

    private PaymentType add = new PaymentType();

    public PaymentType getAdd() {
        return this.add;
    }

    public PaymentType getAdded() {
        PaymentType added = this.add;
        this.add = new PaymentType();
        return added;
    }
}