package com.apd.phoenix.administration.view.jsf.bean;

import java.io.Serializable;
import javax.ejb.Stateful;
import javax.enterprise.context.ConversationScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.inject.Inject;
import javax.inject.Named;
import com.apd.phoenix.service.model.CardInformation;
import com.apd.phoenix.service.model.PaymentInformation;
import com.apd.phoenix.service.model.PeriodType;
import com.apd.phoenix.service.payment.ws.client.PaymentGatewayService;
import com.apd.phoenix.service.payment.ws.client.PaymentGatewayService.CardVaultStorageException;
import com.apd.phoenix.service.persistence.jpa.PaymentInformationDao;

/**
 * Backing bean for PaymentInformation entities.
 * <p>
 * This class provides CRUD functionality for all PaymentInformation entities. It focuses
 * purely on Java EE 6 standards (e.g. <tt>&#64;ConversationScoped</tt> for
 * state management, <tt>PersistenceContext</tt> for persistence,
 * <tt>CriteriaBuilder</tt> for searches) rather than introducing a CRUD framework or
 * custom base class.
 */

@Named
@Stateful
@ConversationScoped
public class PaymentInformationBean implements Serializable {

    private static final int NUMBERS_SHOWN = 12;

    private static final String NUMBER_MASK = "*";

    private static final String CARD_STRING = "card";

    @Inject
    private PaymentInformationDao dao;

    @Inject
    private PaymentGatewayService paymentService;

    private PaymentInformation currentInstance;

    private String enteredNumber = "";

    private static final long serialVersionUID = 1L;

    public Converter getConverter() {

        return new Converter() {

            @Override
            public Object getAsObject(FacesContext context, UIComponent component, String value) {

                return dao.findById(Long.valueOf(value), PaymentInformation.class);
            }

            @Override
            public String getAsString(FacesContext context, UIComponent component, Object value) {

                if (value == null) {
                    return "";
                }

                return String.valueOf(((PeriodType) value).getId());
            }
        };
    }

    /**
     * This method takes the PaymentInformation entity being edited, and whether a field is related to credit card 
     * information, and returns whether that field should be rendered.
     * 
     * @param paymentInfo
     * @param isCardInfo
     * @return
     */
    public boolean shouldRender(PaymentInformation paymentInfo, boolean isCardInfo) {
        this.currentInstance = paymentInfo;
        if (paymentInfo.getPaymentType() == null) {
            return false;
        }
        //If the payment type contains "card" and the field is card related, or the payment type doesn't contain
        //"card" and the field is not card related, returns true. Otherwise, returns false.
        return (paymentInfo.getPaymentType().getName().toLowerCase().contains(CARD_STRING) == isCardInfo);
    }

    /**
     * This method takes a PaymentInformation parameter, and removes fields that are not related to its type. If 
     * the payment is paid by invoice, it sets the card to null, and if the payment is paid by card, the invoice send 
     * method is set to null.
     * 
     * @param paymentInfo
     */
    public void clearUnneededFields(PaymentInformation paymentInfo) {
        this.currentInstance = paymentInfo;
        if (paymentInfo.getPaymentType().getName().toLowerCase().contains(CARD_STRING)) {
            paymentInfo.setInvoiceSendMethod(null);
            paymentInfo.setCard(new CardInformation());
            paymentInfo.getCard().setGhost(true);
            paymentInfo.setSummary(false);
        }
        else {
            paymentInfo.setCard(null);
        }
    }

    /**
     * Getter for the card number of the current payment information. If there is an existing card number, returns 
     * only the last NUMBERS_SHOWN characters, preceded by NUMBER_MASK characters.
     * 
     * @return
     */
    public String getHiddenCard() {
        StringBuilder toReturn = new StringBuilder();
        for (int i = 0; i < NUMBERS_SHOWN; i++) {
            toReturn.append(NUMBER_MASK);
        }
        toReturn.append(this.currentInstance.getCard().getIdentifier());
        return toReturn.toString();
    }

    /**
     * Takes a String card number, and checks if it matches the result of the getHiddenCard method. If it doesn't 
     * match, then the value has been change, and the new card number is saved.
     * 
     * @param card
     */
    public void setHiddenCard(String card) {
        if (this.currentInstance != null && this.currentInstance.getCard() != null && !card.equals(getHiddenCard())) {
            this.currentInstance.getCard().setNumber(card);
            enteredNumber = card;
        }
    }

    /**
     * Called when the payment information modal closes. If the entered card number is stored, passes the card though the 
     * payment gateway service.
     * @throws com.apd.phoenix.service.payment.ws.client.PaymentGatewayService.CardVaultStorageException
     */
    public void storeCreditCard() throws CardVaultStorageException {
        if (this.currentInstance != null && this.currentInstance.getCard() != null
                && this.enteredNumber.equals(this.currentInstance.getCard().getNumber())) {
            this.paymentService.storeCreditCard(this.currentInstance);
        }
    }
}