package com.apd.phoenix.administration.view.jsf.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateful;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.ConversationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.model.SelectItem;
import javax.inject.Inject;
import javax.inject.Named;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.business.CashoutPageBp;
import com.apd.phoenix.service.model.CashoutPageXField;
import com.apd.phoenix.service.model.CashoutPage;
import com.apd.phoenix.service.model.Field;
import com.apd.phoenix.service.model.CashoutPage.NoCardAction;
import com.apd.phoenix.service.persistence.jpa.FieldDao;

/**
 * Backing bean for CashoutPage entities.
 * <p>
 * This class provides CRUD functionality for all CashoutPage entities. It focuses
 * purely on Java EE 6 standards (e.g. <tt>&#64;ConversationScoped</tt> for
 * state management, <tt>PersistenceContext</tt> for persistence,
 * <tt>CriteriaBuilder</tt> for searches) rather than introducing a CRUD framework or
 * custom base class.
 */

@Named
@Stateful
@ConversationScoped
public class CashoutPageBean implements Serializable {

    private static final long serialVersionUID = 1L;
    private static final String PAYMENT_TYPE_SELECTION = "payment type selection";
    private static final String[] PAYMENT_TYPE_REQUIRED_FIELDS = { "bill name", "bill company", "bill address 1",
            "bill address 2", "bill city", "bill states", "bill zip" };

    private CashoutPage currentInstance = new CashoutPage();

    private static final Logger LOG = LoggerFactory.getLogger(CashoutPageBean.class);

    public CashoutPage getCashoutPage() {
        return this.currentInstance;
    }

    public void setCashoutPage(CashoutPage cashoutPage) {
        this.currentInstance = cashoutPage;
    }

    // The Field currently being edited.
    private CashoutPageXField currentCashoutPageXField = new CashoutPageXField();

    public CashoutPageXField getCurrentCashoutPageXField() {
        return this.currentCashoutPageXField;
    }

    public void setCurrentCashoutPageXField(CashoutPageXField field) {
        this.field = field.getField();
        this.currentCashoutPageXField = field;
    }

    /**
     * This method is called when the user presses the "New Property" button, and creates 
     * a new Property to modify.
     */
    public void createCashoutPageXField() {
        this.field = null;
        this.currentCashoutPageXField = new CashoutPageXField();
    }

    /**
     * Removes the selected property from the set of properties on the current CashoutPage instance.
     * 
     * @param toRemove
     */
    public void removeCashoutPageXField(CashoutPageXField toRemove) {
        this.currentInstance.getPageXFields().remove(toRemove);
    }

    /**
     * Saves the selected property onto the current CashoutPage instance.
     */
    public void saveCashoutPageXField() {
        this.currentInstance.getPageXFields().add(this.currentCashoutPageXField);
    }

    @Inject
    private CashoutPageBp bp;

    @Inject
    private Conversation conversation;

    @Inject
    private CashoutPageSearchBean searchBean;

    public String create() {
        this.conversation.begin();
        return "cashoutPageAdd?faces-redirect=true";
    }

    public void retrieve() {
        if (searchBean.getSelection() != null && useSearchResult) {
            this.currentInstance = bp.findById(searchBean.getSelection().getId(), CashoutPage.class);
            eagerLoad();
            searchBean.setSelection(null);
        }

        if (FacesContext.getCurrentInstance().isPostback()) {
            return;
        }
        if (this.conversation.isTransient()) {
            this.conversation.begin();
        }

    }

    /*
     * Support updating and deleting CashoutPage entities
     */

    public String update() {
        if (!validToUpdate()) {
            return null;
        }
        if (!this.conversation.isTransient()) {
            this.conversation.end();
        }

        try {

            if (currentInstance.getId() == null) {
                this.currentInstance = bp.update(this.currentInstance);
                return "cashoutPageAdd?faces-redirect=true";
            }
            else {
                this.currentInstance = bp.update(this.currentInstance);
                eagerLoad();
                return "cashoutPageEdit?faces-redirect=true";
            }
        }
        catch (Exception e) {
            LOG.error(e.getMessage());
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, "Unexpected Error", "Unexpected Error"));
            return null;
        }
    }

    public String delete() {
        this.conversation.end();

        try {
            bp.delete(this.currentInstance.getId(), CashoutPage.class);
            return "cashoutPageEdit?faces-redirect=true";
        }
        catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), e.getMessage()));
            return null;
        }
    }

    /*
     * Support searching CashoutPage entities with pagination
     */

    private int page;
    private long count;
    private List<CashoutPage> pageItems;

    private CashoutPage example = new CashoutPage();

    public int getPage() {
        return this.page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getPageSize() {
        return 10;
    }

    public CashoutPage getExample() {
        return this.example;
    }

    public void setExample(CashoutPage example) {
        this.example = example;
    }

    public void search() {
        this.page = 0;
    }

    public void paginate() {
        this.pageItems = bp.searchByExample(this.example, getPage() * getPageSize(), getPageSize());

        this.count = bp.resultQuantity(this.example);
    }

    public List<CashoutPage> getPageItems() {
        return this.pageItems;
    }

    public long getCount() {
        return this.count;
    }

    /*
     * Support listing and POSTing back CashoutPage entities (e.g. from inside an
     * HtmlSelectOneMenu)
     */

    public List<CashoutPage> getAll() {

        return bp.findAll(CashoutPage.class, 0, 0);
    }

    public List<CashoutPage> searchByExample(CashoutPage search) {
        return bp.searchByExample(search, 0, 0);
    }

    @Resource
    private SessionContext sessionContext;

    public Converter getConverter() {

        final CashoutPageBean ejbProxy = this.sessionContext.getBusinessObject(CashoutPageBean.class);

        return new Converter() {

            @Override
            public Object getAsObject(FacesContext context, UIComponent component, String value) {
                if (ejbProxy.currentInstance == null) {
                    ejbProxy.currentInstance = new CashoutPage();
                }
                ejbProxy.retrieve();
                return ejbProxy.getCashoutPage();
            }

            @Override
            public String getAsString(FacesContext context, UIComponent component, Object value) {

                if (value == null) {
                    return "";
                }

                return String.valueOf(((CashoutPage) value).getId());
            }
        };
    }

    /*
     * Support adding children to bidirectional, one-to-many tables
     */

    private CashoutPage add = new CashoutPage();

    public CashoutPage getAdd() {
        return this.add;
    }

    public CashoutPage getAdded() {
        CashoutPage added = this.add;
        this.add = new CashoutPage();
        return added;
    }

    private void eagerLoad() {
        this.currentInstance = bp.eagerLoad(this.currentInstance);
    }

    private boolean validToUpdate() {
        CashoutPage searchPage = new CashoutPage();
        searchPage.setName(this.currentInstance.getName());
        List<CashoutPage> searchList = this.bp.searchByExactExample(searchPage, 0, 0);
        if (searchList.size() != 0 && !searchList.get(0).getId().equals(this.currentInstance.getId())) {
            FacesMessage message = new FacesMessage();
            message.setSeverity(FacesMessage.SEVERITY_ERROR);
            message.setSummary("Name already exists");
            message.setDetail("A cashout page already exists with the specified name.");
            FacesContext.getCurrentInstance().addMessage(null, message);
            return false;
        }
        
        List<String> cashOutfields = new ArrayList<>();
        for (CashoutPageXField pageXField : this.currentInstance.getPageXFields()) {
        	cashOutfields.add(pageXField.getField().getName());
        }
        if(cashOutfields.contains(PAYMENT_TYPE_SELECTION) 
        		&& !cashOutfields.containsAll(Arrays.asList(PAYMENT_TYPE_REQUIRED_FIELDS))) {
        	FacesMessage message = new FacesMessage();
            message.setSeverity(FacesMessage.SEVERITY_ERROR);
            message.setSummary("Payment Type Selection field requires following fields "
                    + "Bill Name, Bill Company, Bill Address 1,"
                    + "Bill Address 2, Bill State, Bill City and Bill Zip");
            FacesContext.getCurrentInstance().addMessage(null, message);
            return false;
        }
        return true;
    }

    private Field field;

    @Inject
    private FieldDao fieldDao;

    public Field getField() {
        return this.field;
    }

    public void setField(Field field) {
        this.field = fieldDao.eagerLoad(field);
        if (this.currentCashoutPageXField.getLabel() == null || this.currentCashoutPageXField.getLabel().isEmpty()) {
            this.currentCashoutPageXField.setLabel(field.getName());
        }
    }

    public void validateFieldSelection(FacesContext context, UIComponent component, Object value) {
        if (this.field == null) {
            ((UIInput) component).setValid(false);
            FacesMessage message = new FacesMessage();
            message.setSeverity(FacesMessage.SEVERITY_ERROR);
            message.setSummary("You must select a field");
            message.setDetail("You have to select a field to add to the page.");
            context.addMessage(component.getClientId(), message);
            return;
        }
        for (CashoutPageXField pageXField : this.currentInstance.getPageXFields()) {
            if (((pageXField.getMapping() != null && pageXField.getMapping().equals(
                    this.currentCashoutPageXField.getMapping())) || (pageXField.getInboundMapping() != null && pageXField
                    .getInboundMapping().equals(this.currentCashoutPageXField.getInboundMapping())))
                    && pageXField != this.currentCashoutPageXField) {
                ((UIInput) component).setValid(false);
                FacesMessage message = new FacesMessage();
                message.setSeverity(FacesMessage.SEVERITY_ERROR);
                message.setSummary("Duplicate mapping");
                message.setDetail("This mapping has already been added to the page");
                context.addMessage(component.getClientId(), message);
                return;
            }
        }
        this.currentCashoutPageXField.setField(this.field);
        this.field = null;
    }

    public void validateLabel(FacesContext context, UIComponent component, Object value) {
        if (value != null && ((String) value).contains("|")) {
            ((UIInput) component).setValid(false);
            FacesMessage message = new FacesMessage();
            message.setSeverity(FacesMessage.SEVERITY_ERROR);
            message.setSummary("Invalid character '|'");
            message.setDetail("'|' is an invalid character for the cashout page label.");
            context.addMessage(component.getClientId(), message);
            if (this.field == null) {
                this.field = this.currentCashoutPageXField.getField();
            }
            return;
        }
    }

    private boolean useSearchResult = true;

    public void ignoreSearchResult() {
        useSearchResult = false;
    }

    public String cloneSelectedCashoutPage(CashoutPage selected) {
        this.currentInstance = this.bp.cloneEntity(selected);
        this.currentInstance.setName(this.currentInstance.getName() + " copy");
        useSearchResult = true;
        return "cashoutPageAdd?faces-redirect=true";
    }

    public SelectItem[] getNoCardActions() {
        SelectItem[] items = new SelectItem[NoCardAction.values().length];
        int i = 0;
        for (NoCardAction g : NoCardAction.values()) {
            items[i++] = new SelectItem(g, g.getDescription());
        }
        return items;
    }

}