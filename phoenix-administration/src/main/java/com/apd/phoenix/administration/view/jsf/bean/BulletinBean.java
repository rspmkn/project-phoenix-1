package com.apd.phoenix.administration.view.jsf.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateful;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.ConversationScoped;
import javax.faces.convert.Converter;
import javax.inject.Inject;
import javax.inject.Named;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.apd.phoenix.service.business.BulletinBp;
import com.apd.phoenix.service.model.Bulletin;
import com.apd.phoenix.service.model.Credential;
import com.apd.phoenix.web.AbstractControllerBean;
import com.apd.phoenix.web.PropertiesLoader;

@Named
@Stateful
@ConversationScoped
public class BulletinBean extends AbstractControllerBean<Bulletin> implements Serializable {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(BulletinBean.class);

    private static final long serialVersionUID = 1L;

    @Inject
    private BulletinBp bulletinBp;

    @Inject
    private Conversation conversation;

    private List<Bulletin> bulletinList;

    private List<Bulletin> toDelete = new ArrayList<>();

    private Bulletin currentBulletin;

    public void retrieve() {
        if (this.bulletinList == null) {
            this.bulletinList = this.bulletinBp.getAllGlobalBulletinsForAdministration();
        }
        if (this.conversation.isTransient()) {
            this.conversation.begin();
        }
    }

    public String delete() {
        //do nothing
        return null;
    }

    public String update() {
        try {
            for (Bulletin bulletin : this.bulletinList) {
                this.bulletinBp.update(bulletin);
            }
            for (Bulletin bulletin : this.toDelete) {
            	if (bulletin.getId() != null) {
            		this.bulletinBp.delete(bulletin.getId(), Bulletin.class);
            	}
            }
            if (!this.conversation.isTransient()) {
                this.conversation.end();
            }
            return "globalBulletinEdit?faces-redirect=true";
        }
        catch (Exception e) {
            return null;
        }
    }

    public List<Bulletin> getBulletinList() {
        return bulletinList;
    }

    public void setBulletinList(List<Bulletin> bulletinList) {
        this.bulletinList = bulletinList;
    }

    public Bulletin getCurrentBulletin() {
        return this.currentBulletin;
    }

    public void setCurrentBulletin(Bulletin newBulletin) {
        this.currentBulletin = newBulletin;
    }

    /**
     * This method is called when the user presses the "New Bulletin" button, and creates 
     * a new bulletin to modify.
     */
    public void createBulletin() {
        this.currentBulletin = new Bulletin();
        this.currentBulletin.setGlobal(true);
    }

    /**
     * Removes the selected bulletin from the set of bulletins on the current Account instance.
     * 
     * @param toRemove
     */
    public void removeBulletin(Bulletin toRemove) {
        this.bulletinList.remove(toRemove);
        this.toDelete.add(toRemove);
    }

    /**
     * Saves the selected Bulletin onto the current Account instance.
     */
    public void saveBulletin() {
    	if(!this.bulletinList.contains(this.currentBulletin)){
    		this.bulletinList.add(this.currentBulletin);
    	}
    }

	@Override
	public Converter getConverter() {
		// NOT USED
		LOGGER.error("Unexpected method call. This bean shouldn't need to do conversion.");
		return null;
	}
}