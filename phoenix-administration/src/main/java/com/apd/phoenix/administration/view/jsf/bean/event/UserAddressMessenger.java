/**
 * 
 */
package com.apd.phoenix.administration.view.jsf.bean.event;

import javax.ejb.Stateless;
import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.inject.Named;

/**
 * @author Red Hat Consulting - Red Hat, Inc.
 *
 */

@Named("UA_Messenger")
@Stateless
public class UserAddressMessenger {

    @Inject
    Event<UserAddressEvent> events;

    public void message() {
        events.fire(new UserAddressEvent("Yay! A new user address needs to be created"));
    }

}
