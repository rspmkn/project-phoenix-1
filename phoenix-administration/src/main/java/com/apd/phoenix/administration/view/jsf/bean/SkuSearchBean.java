package com.apd.phoenix.administration.view.jsf.bean;

import java.io.File;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.enterprise.context.ConversationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import com.apd.phoenix.service.business.SkuBp;
import com.apd.phoenix.service.model.Sku;
import com.apd.phoenix.web.searchbeans.AbstractSearchBean;

@Named
@ConversationScoped
public class SkuSearchBean extends AbstractSearchBean<Sku> implements Serializable {

    private static final long serialVersionUID = -5840504081385283214L;

    @Inject
    private SkuBp bp;

    @Override
    public String[] getCriteria() {
        String[] toReturn = { "Vendor Catalog", "SKU Type", "SKU", "Keyword" };
        return toReturn;
    }

    @Override
    protected List<Sku> search() {
        return bp.skuList(this.getValues().get(0).getValue(), this.getValues().get(1).getValue(), this.getValues().get(
                2).getValue(), this.getValues().get(3).getValue(), (this.getPageDisplay().getPage() - 1)
                * this.getPageDisplay().getSize(), this.getPageDisplay().getSize());
    }

    @Override
    public String[] getColumns() {
        String[] toReturn = { "Vendor Catalog Name", "SKU Type", "SKU", "Name" };
        return toReturn;
    }

    @Override
    public Map<String, String> resultRow(Sku searchResult) {
        Map<String, String> toReturn = new HashMap<String, String>();
        toReturn.put(getColumns()[0], searchResult.getItem().getVendorCatalog().getName());
        toReturn.put(getColumns()[1], searchResult.getType().getName());
        toReturn.put(getColumns()[2], searchResult.getValue());
        toReturn.put(getColumns()[3], searchResult.getItem().getName());
        return toReturn;
    }

    @Override
    public int getResultQuantity() {
        return bp.searchCount(this.getValues().get(0).getValue(), this.getValues().get(1).getValue(), this.getValues()
                .get(2).getValue(), this.getValues().get(3).getValue());
    }

    @Override
    public File resultsCsv() {
        return this.bp.skuCsv(getColumns(), this.getValues().get(0).getValue(), this.getValues().get(1).getValue(),
                this.getValues().get(2).getValue(), this.getValues().get(3).getValue());
    }

}
