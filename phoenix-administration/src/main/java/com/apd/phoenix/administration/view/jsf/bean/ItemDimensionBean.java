package com.apd.phoenix.administration.view.jsf.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateful;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.ConversationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import com.apd.phoenix.service.model.ItemDimension;
import com.apd.phoenix.service.model.DimensionType;
import com.apd.phoenix.service.model.Item;
import com.apd.phoenix.service.model.UnitOfMeasure;

/**
 * Backing bean for ItemDimension entities.
 * <p>
 * This class provides CRUD functionality for all ItemDimension entities. It focuses
 * purely on Java EE 6 standards (e.g. <tt>&#64;ConversationScoped</tt> for
 * state management, <tt>PersistenceContext</tt> for persistence,
 * <tt>CriteriaBuilder</tt> for searches) rather than introducing a CRUD framework or
 * custom base class.
 */

@Named
@Stateful
@ConversationScoped
@Deprecated
public class ItemDimensionBean implements Serializable {

    private static final long serialVersionUID = 1L;

    /*
     * Support creating and retrieving ItemDimension entities
     */

    private Long id;

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    private ItemDimension itemDimension;

    public ItemDimension getItemDimension() {
        return this.itemDimension;
    }

    @Inject
    private Conversation conversation;

    private EntityManager entityManager;

    public String create() {

        this.conversation.begin();
        return "create?faces-redirect=true";
    }

    public void retrieve() {

        if (FacesContext.getCurrentInstance().isPostback()) {
            return;
        }

        if (this.conversation.isTransient()) {
            this.conversation.begin();
        }

        if (this.id == null) {
            this.itemDimension = this.example;
        }
        else {
            this.itemDimension = findById(getId());
        }
    }

    public ItemDimension findById(Long id) {

        return this.entityManager.find(ItemDimension.class, id);
    }

    /*
     * Support updating and deleting ItemDimension entities
     */

    public String update() {
        this.conversation.end();

        try {
            if (this.id == null) {
                this.entityManager.persist(this.itemDimension);
                return "search?faces-redirect=true";
            }
            else {
                this.entityManager.merge(this.itemDimension);
                return "view?faces-redirect=true&id=" + this.itemDimension.getId();
            }
        }
        catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), e.getMessage()));
            return null;
        }
    }

    public String delete() {
        this.conversation.end();

        try {
            this.entityManager.remove(findById(getId()));
            this.entityManager.flush();
            return "search?faces-redirect=true";
        }
        catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), e.getMessage()));
            return null;
        }
    }

    /*
     * Support searching ItemDimension entities with pagination
     */

    private int page;
    private long count;
    private List<ItemDimension> pageItems;

    private ItemDimension example = new ItemDimension();

    public int getPage() {
        return this.page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getPageSize() {
        return 10;
    }

    public ItemDimension getExample() {
        return this.example;
    }

    public void setExample(ItemDimension example) {
        this.example = example;
    }

    public void search() {
        this.page = 0;
    }

    public void paginate() {

        CriteriaBuilder builder = this.entityManager.getCriteriaBuilder();

        // Populate this.count

        CriteriaQuery<Long> countCriteria = builder.createQuery(Long.class);
        Root<ItemDimension> root = countCriteria.from(ItemDimension.class);
        countCriteria = countCriteria.select(builder.count(root)).where(getSearchPredicates(root));
        this.count = this.entityManager.createQuery(countCriteria).getSingleResult();

        // Populate this.pageItems

        CriteriaQuery<ItemDimension> criteria = builder.createQuery(ItemDimension.class);
        root = criteria.from(ItemDimension.class);
        TypedQuery<ItemDimension> query = this.entityManager.createQuery(criteria.select(root).where(
                getSearchPredicates(root)));
        query.setFirstResult(this.page * getPageSize()).setMaxResults(getPageSize());
        this.pageItems = query.getResultList();
    }

    private Predicate[] getSearchPredicates(Root<ItemDimension> root) {

        CriteriaBuilder builder = this.entityManager.getCriteriaBuilder();
        List<Predicate> predicatesList = new ArrayList<Predicate>();

        DimensionType dimensionType = this.example.getDimensionType();
        if (dimensionType != null) {
            predicatesList.add(builder.equal(root.get("dimensionType"), dimensionType));
        }
        UnitOfMeasure unitOfMeasure = this.example.getUnitOfMeasure();
        if (unitOfMeasure != null) {
            predicatesList.add(builder.equal(root.get("unitOfMeasure"), unitOfMeasure));
        }
        Item item = this.example.getItem();
        if (item != null) {
            predicatesList.add(builder.equal(root.get("item"), item));
        }

        return predicatesList.toArray(new Predicate[predicatesList.size()]);
    }

    public List<ItemDimension> getPageItems() {
        return this.pageItems;
    }

    public long getCount() {
        return this.count;
    }

    /*
     * Support listing and POSTing back ItemDimension entities (e.g. from inside an
     * HtmlSelectOneMenu)
     */

    public List<ItemDimension> getAll() {

        CriteriaQuery<ItemDimension> criteria = this.entityManager.getCriteriaBuilder()
                .createQuery(ItemDimension.class);
        return this.entityManager.createQuery(criteria.select(criteria.from(ItemDimension.class))).getResultList();
    }

    @Resource
    private SessionContext sessionContext;

    public Converter getConverter() {

        final ItemDimensionBean ejbProxy = this.sessionContext.getBusinessObject(ItemDimensionBean.class);

        return new Converter() {

            @Override
            public Object getAsObject(FacesContext context, UIComponent component, String value) {

                return ejbProxy.findById(Long.valueOf(value));
            }

            @Override
            public String getAsString(FacesContext context, UIComponent component, Object value) {

                if (value == null) {
                    return "";
                }

                return String.valueOf(((ItemDimension) value).getId());
            }
        };
    }

    /*
     * Support adding children to bidirectional, one-to-many tables
     */

    private ItemDimension add = new ItemDimension();

    public ItemDimension getAdd() {
        return this.add;
    }

    public ItemDimension getAdded() {
        ItemDimension added = this.add;
        this.add = new ItemDimension();
        return added;
    }
}