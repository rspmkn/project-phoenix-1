package com.apd.phoenix.administration.view.jsf.bean;

import java.util.List;
import javax.enterprise.context.ConversationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import com.apd.phoenix.service.business.JumptrackStopConfigurationBp;
import com.apd.phoenix.service.model.JumptrackStopConfiguration;
import com.apd.phoenix.web.searchbeans.AbstractSearchBean;
import java.io.Serializable;

@Named
@ConversationScoped
public class JumptrackStopConfigurationSearchBean extends AbstractSearchBean<JumptrackStopConfiguration> implements
        Serializable {

    private static final long serialVersionUID = 1L;

    @Inject
    private JumptrackStopConfigurationBp bp;

    @Override
    public String[] getCriteria() {
        String[] toReturn = { "Zip" };
        return toReturn;
    }

    @Override
    protected List<JumptrackStopConfiguration> search() {
        JumptrackStopConfiguration search = new JumptrackStopConfiguration();
        search.setZip(this.getValues().get(0).getValue());
        return bp.searchByExample(search, 0, 0);
    }

}
