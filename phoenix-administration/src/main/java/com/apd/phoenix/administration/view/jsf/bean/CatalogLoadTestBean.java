package com.apd.phoenix.administration.view.jsf.bean;

import java.io.Serializable;
import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Singleton;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.inject.Named;
import com.apd.phoenix.core.utility.TenantConfigRepository;
import com.apd.phoenix.service.business.CatalogBpLoadTest;

/**
 * This class is the view-layer bean for managing catalog load tests. There won't be a direct
 * link to the load test page, and it is intended to only be run in the lower environments.
 * 
 * @author RHC
 *
 */
@Named
@Singleton
@Lock(LockType.READ)
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
public class CatalogLoadTestBean implements Serializable {

    private static final long serialVersionUID = 1L;

    @Inject
    private CatalogBpLoadTest loadTestBp;

    private Long testResult = null;

    public String initLoadTest() {
        if (this.canRunLoadTests()) {
            this.setTestResult(loadTestBp.loadTest());
        }
        return null;
    }

    public Long getTestResult() {
        return testResult;
    }

    public void setTestResult(Long testResult) {
        this.testResult = testResult;
    }

    public boolean canRunLoadTests() {
        return "false".equals(TenantConfigRepository.getInstance().getProperty("project.state",
                "isProductionEnvironment"));
    }
}