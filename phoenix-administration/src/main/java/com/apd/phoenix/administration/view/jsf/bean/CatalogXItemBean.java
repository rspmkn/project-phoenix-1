package com.apd.phoenix.administration.view.jsf.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.ejb.Stateful;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.ConversationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.inject.Inject;
import javax.inject.Named;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.business.CatalogBp;
import com.apd.phoenix.service.business.CatalogXItemBp;
import com.apd.phoenix.service.business.FavoritesListBp;
import com.apd.phoenix.service.business.UnitOfMeasureBp;
import com.apd.phoenix.service.model.Catalog;
import com.apd.phoenix.service.model.CatalogXItem;
import com.apd.phoenix.service.model.CatalogXItemXItemPropertyType;
import com.apd.phoenix.service.model.FavoritesList;
import com.apd.phoenix.service.model.Item;
import com.apd.phoenix.service.model.ItemPropertyType;
import com.apd.phoenix.service.model.ItemXItemPropertyType;
import com.apd.phoenix.service.model.PricingType;
import com.apd.phoenix.service.model.UnitOfMeasure;
import com.apd.phoenix.service.model.listener.CatalogXItemListener;
import com.apd.phoenix.service.persistence.jpa.PricingTypeDao;
import com.apd.phoenix.web.AbstractControllerBean;
import com.apd.phoenix.web.ValidationProperties;

/**
 * Backing bean for CatalogXItem entities.
 * <p>
 * This class provides CRUD functionality for all CatalogXItem entities. It focuses
 * purely on Java EE 6 standards (e.g. <tt>&#64;ConversationScoped</tt> for
 * state management, <tt>PersistenceContext</tt> for persistence,
 * <tt>CriteriaBuilder</tt> for searches) rather than introducing a CRUD framework or
 * custom base class.
 */

@Named
@Stateful
@ConversationScoped
public class CatalogXItemBean extends AbstractControllerBean<CatalogXItem> implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    @Inject
    private CatalogXItemBp bp;

    private static final Logger LOG = LoggerFactory.getLogger(CatalogXItemBean.class);

    /**
     * This method takes a CatalogXItem and returns a List of CatalogXItemXItemPropertyTypes associated with that item.
     * 
     * @param item
     * @return
     */
    public List<CatalogXItemXItemPropertyType> getItemProperties(CatalogXItem item) {
        List<CatalogXItemXItemPropertyType> toReturn = new ArrayList<CatalogXItemXItemPropertyType>();
        if (item == null) {
            return toReturn;
        }
        item = bp.eagerLoad(item);
        Set<ItemPropertyType> propertySet = new HashSet<ItemPropertyType>();
        for (CatalogXItemXItemPropertyType property : item.getProperties()) {
            toReturn.add(property);
            propertySet.add(property.getType());
        }
        Catalog parent = item.getCatalog().getParent();
        if (parent != null) {
            CatalogXItem searchCatXItem = new CatalogXItem();
            searchCatXItem.setItem(new Item());
            searchCatXItem.getItem().setId(item.getItem().getId());
            searchCatXItem.setCatalog(new Catalog());
            searchCatXItem.getCatalog().setId(parent.getId());
            List<CatalogXItem> catXItemList = bp.searchByExactExample(searchCatXItem, 0, 0);
            if (catXItemList.size() != 0) {
                CatalogXItem parentItem = catXItemList.get(0);
                for (CatalogXItemXItemPropertyType property : parentItem.getProperties()) {
                    if (!propertySet.contains(property.getType())) {
                        toReturn.add(property);
                        propertySet.add(property.getType());
                    }
                }
            }
        }
        for (ItemXItemPropertyType property : item.getItem().getPropertiesReadOnly()) {
            if (!propertySet.contains(property.getType())) {
                CatalogXItemXItemPropertyType newProperty = new CatalogXItemXItemPropertyType();
                newProperty.setType(property.getType());
                newProperty.setValue(property.getValue());
                toReturn.add(newProperty);
                propertySet.add(property.getType());
            }
        }
        return toReturn;
    }

    /*
     * Support creating and retrieving CatalogXItem entities
     */

    public void validateSelection(FacesContext context, UIComponent component, Object value) {
        if (this.currentInstance.getId() == null) {
            ((UIInput) component).setValid(false);
            FacesMessage message = new FacesMessage();
            message.setSeverity(FacesMessage.SEVERITY_ERROR);
            message.setSummary("Select Customer Catalog Item to edit");
            message.setDetail("You need to select an Customer Catalog Item to edit.");
            context.addMessage(component.getClientId(), message);
            return;
        }
    }

    private CatalogXItem currentInstance = new CatalogXItem();

    public CatalogXItem getCatalogXItem() {
        return this.currentInstance;
    }

    @Inject
    private CatalogXItemSearchBean searchBean;

    @Inject
    private Conversation conversation;

    @Inject
    private UnitOfMeasureBp unitOfMeasureBp;

    public String create() {
        this.conversation.begin();
        return "catalogXItemAdd?faces-redirect=true";
    }

    public void retrieve() {
        if (this.searchBean.getSelection() != null) {
            this.currentInstance = (CatalogXItem) this.searchBean.getSelection()[0];
            this.searchBean.setSelection(null);
            this.currentInstance = this.eagerLoad(this.currentInstance);
        }

        if (FacesContext.getCurrentInstance().isPostback()) {
            return;
        }

        if (this.conversation.isTransient()) {
            this.conversation.begin();
        }
    }

    /*
     * Support updating and deleting CatalogXItem entities
     */

    public String update() {
        if (!validToUpdate()) {
            return null;
        }

        try {
            if (!StringUtils.isEmpty(customerUomToAdd)
                    && (currentInstance.getCustomerUnitOfmeasure() == null || !customerUomToAdd.equals(currentInstance
                            .getCustomerUnitOfmeasure().getName()))) {
                UnitOfMeasure unitOfMeasure = unitOfMeasureBp.getByName(customerUomToAdd);
                if (unitOfMeasure == null) {
                    unitOfMeasure = new UnitOfMeasure();
                    unitOfMeasure.setName(customerUomToAdd);
                    unitOfMeasureBp.create(unitOfMeasure);
                }
                currentInstance.setCustomerUnitOfmeasure(unitOfMeasure);
            }

            if (currentInstance.getId() == null) {
                bp.calculatePrice(this.currentInstance);
                bp.create(this.currentInstance);
                if (!this.conversation.isTransient()) {
                    this.conversation.end();
                }
                return "catalogXItemAdd?faces-redirect=true";
            }
            else {
                this.currentInstance = bp.update(this.currentInstance);
                bp.calculatePrice(this.currentInstance);
                bp.update(this.currentInstance);
                if (changed) {
                    CatalogXItemListener.addUpdateSolrDocument(this.currentInstance.getId());
                    changed = false;
                }
                if (!this.conversation.isTransient()) {
                    this.conversation.end();
                }
                return "catalogXItemEdit?faces-redirect=true";
            }
        }
        catch (Exception e) {
            LOG.error(e.getMessage());
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, "Unexpected Error", "Unexpected Error"));
            return null;
        }
    }

    public String delete() {
        this.conversation.end();
        try {
            bp.delete(this.currentInstance.getId(), CatalogXItem.class);
            return "catalogXItemEdit?faces-redirect=true";
        }
        catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), e.getMessage()));
            return null;
        }
    }

    /*
     * Support listing and POSTing back CatalogXItem entities (e.g. from inside an
     * HtmlSelectOneMenu)
     */

    public List<CatalogXItem> getAll() {

        return bp.findAll(CatalogXItem.class, 0, 0);
    }

    public Converter getConverter() {

        return new Converter() {

            @Override
            public Object getAsObject(FacesContext context, UIComponent component, String value) {

                return bp.findById(Long.valueOf(value), CatalogXItem.class);
            }

            @Override
            public String getAsString(FacesContext context, UIComponent component, Object value) {

                if (value == null) {
                    return "";
                }

                return String.valueOf(((CatalogXItem) value).getId());
            }
        };
    }

    @Inject
    private PricingTypeDao pricingTypeDao;

    public Converter getPricingTypeConverter() {

        return new Converter() {

            @Override
            public Object getAsObject(FacesContext context, UIComponent component, String value) {

                return pricingTypeDao.findById(Long.valueOf(value), PricingType.class);
            }

            @Override
            public String getAsString(FacesContext context, UIComponent component, Object value) {

                if (value == null) {
                    return "";
                }

                return String.valueOf(((PricingType) value).getId());
            }
        };
    }

    public List<PricingType> getAllPricingTypes() {

        return pricingTypeDao.findAll(PricingType.class, 0, 0);
    }

    /*
     * Support adding children to bidirectional, one-to-many tables
     */

    private CatalogXItem add = new CatalogXItem();

    public CatalogXItem getAdd() {
        return this.add;
    }

    public CatalogXItem getAdded() {
        CatalogXItem added = this.add;
        this.add = new CatalogXItem();
        return added;
    }

    private boolean validToUpdate() {
        if (this.currentInstance.getItem() == null) {
            FacesMessage message = new FacesMessage();
            message.setSeverity(FacesMessage.SEVERITY_ERROR);
            message.setSummary(ValidationProperties.get("catxitem.creation.error.noitem"));
            message.setDetail(ValidationProperties.get("catxitem.creation.error.noitem"));
            FacesContext.getCurrentInstance().addMessage(null, message);
            return false;
        }
        if (this.currentInstance.getCatalog() == null) {
            FacesMessage message = new FacesMessage();
            message.setSeverity(FacesMessage.SEVERITY_ERROR);
            message.setSummary(ValidationProperties.get("catxitem.creation.error.nocatalog"));
            message.setDetail(ValidationProperties.get("catxitem.creation.error.nocatalog"));
            FacesContext.getCurrentInstance().addMessage(null, message);
            return false;
        }
        CatalogXItem searchCatalogXItem = new CatalogXItem();
        searchCatalogXItem.setItem(new Item());
        searchCatalogXItem.setCatalog(new Catalog());
        searchCatalogXItem.getItem().setId(this.currentInstance.getItem().getId());
        searchCatalogXItem.getCatalog().setId(this.currentInstance.getCatalog().getId());
        List<CatalogXItem> searchList = this.bp.searchByExactExample(searchCatalogXItem, 0, 0);
        if (searchList.size() != 0 && !searchList.get(0).getId().equals(this.currentInstance.getId())) {
            FacesMessage message = new FacesMessage();
            message.setSeverity(FacesMessage.SEVERITY_ERROR);
            message.setSummary(ValidationProperties.get("catxitem.creation.error.duplicate"));
            message.setDetail(ValidationProperties.get("catxitem.creation.error.duplicate"));
            FacesContext.getCurrentInstance().addMessage(null, message);
            return false;
        }
        if ((this.currentInstance.getPricingParameter() != null && this.currentInstance.getPricingType() != null)
                && !this.currentInstance.getPricingParameter().matches(
                        this.currentInstance.getPricingType().getParameterRegEx())) {
            FacesMessage message = new FacesMessage();
            message.setSeverity(FacesMessage.SEVERITY_ERROR);
            message.setSummary(ValidationProperties.get("catxitem.creation.error.invalidpriceparam"));
            message.setDetail(ValidationProperties.get("catxitem.creation.error.invalidpriceparam"));
            FacesContext.getCurrentInstance().addMessage(null, message);
            return false;
        }
        return true;
    }

    /**
     * This method loads the values of a search result. Should be replaced by the search result query returning the 
     * needed values.
     * 
     * @param toLoad
     * @return
     */
    private CatalogXItem eagerLoad(CatalogXItem toLoad) {
        CatalogXItem toReturn = this.bp.eagerLoad(toLoad);
        toReturn.getItem().toString();
        toReturn.getCatalog().toString();
        toReturn.getCatalog().getCustomer().toString();
        if (toReturn.getSubstituteItem() != null) {
            toReturn.getSubstituteItem().getItem().toString();
        }
        return toReturn;
    }

    private boolean changed = false;

    private String customerUomToAdd;

    public String getSkuValue() {
        if (this.currentInstance == null || StringUtils.isBlank(this.currentInstance.getCustomerSkuString())) {
            return "";
        }
        return this.currentInstance.getCustomerSkuString();
    }

    public void setSkuValue(String value) {
        if (StringUtils.isNotBlank(value)) {
            this.currentInstance.setCustomerSkuString(value);
        }
        else {
            this.currentInstance.setCustomerSkuString(null);
        }
    }

    public String getCustomerUomValue() {
        if (currentInstance.getCustomerUnitOfmeasure() == null
                || currentInstance.getCustomerUnitOfmeasure().getName() == null) {
            return "";
        }
        return currentInstance.getCustomerUnitOfmeasure().getName();
    }

    public void setCustomerUomValue(String customerUom) {
        customerUomToAdd = customerUom;
    }

    // The property currently being edited.
    private CatalogXItemXItemPropertyType currentProperty = new CatalogXItemXItemPropertyType();

    public CatalogXItemXItemPropertyType getCurrentProperty() {
        return this.currentProperty;
    }

    public void setCurrentProperty(CatalogXItemXItemPropertyType property) {
        this.currentProperty = property;
    }

    /**
     * This method is called when the user presses the "New Property" button, and creates 
     * a new Property to modify.
     */
    public void createProperty() {
        this.currentProperty = new CatalogXItemXItemPropertyType();
    }

    /**
     * Removes the selected property from the set of properties on the current CatalogXItem instance.
     * 
     * @param toRemove
     */
    public void removeProperty(CatalogXItemXItemPropertyType toRemove) {
        this.currentInstance.getProperties().remove(toRemove);
    }

    /**
     * Saves the selected property onto the current CatalogXItem instance.
     */
    public void saveProperty() {
        this.currentInstance.getProperties().add(this.currentProperty);
    }

    @Inject
    private ValidationBean validationBean;

    public void validateUniqueProperties(FacesContext context, UIComponent component, Object value) {
        Collection<Object> toReturn = new HashSet<Object>();
        for (Object o : this.currentInstance.getProperties()) {
            toReturn.add(o);
        }
        if (this.validationBean.repeatedEntry(toReturn, "getType")) {
            ((UIInput) component).setValid(false);
            FacesMessage message = new FacesMessage();
            message.setSeverity(FacesMessage.SEVERITY_ERROR);
            message.setSummary("Multiple properties with the same type");
            message.setDetail("You can't have multiple properties with the same type.");
            context.addMessage(null, message);
        }
    }

    public void clearSubstitute() {
        this.bp.clearSubstitute(this.currentInstance);
    }

    @Inject
    private CatalogBp catalogBp;

    public void setCatalog(Catalog toSet) {
        this.currentInstance.setCatalog(catalogBp.eagerLoad(toSet));
    }

    @Inject
    private FavoritesListBp listBp;

    public void removeFromList(FavoritesList list) {
        this.currentInstance.getFavorites().remove(list);
    }

    private String enteredListName;

    public String getEnteredListName() {
        return enteredListName;
    }

    public void setEnteredListName(String enteredListName) {
        this.enteredListName = enteredListName;
    }

    public List<FavoritesList> getCatalogFavorites() {
    	if (this.currentInstance != null && this.currentInstance.getCatalog() != null && this.currentInstance.getCatalog().getId() != null) {
	    	FavoritesList searchList = new FavoritesList();
	    	searchList.setCatalog(new Catalog());
	    	searchList.getCatalog().setId(this.currentInstance.getCatalog().getId());
	        return ViewUtils.asList(listBp.searchByExactExample(searchList, 0, 0));
    	}
    	return new ArrayList<>();
    }

    public List<String> getCatalogFavoritesNames(String nameSoFar) {
        List<String> toReturn = new ArrayList<String>();
        for (FavoritesList list : this.getCatalogFavorites()) {
            if (list.getName().toLowerCase().startsWith(nameSoFar.toLowerCase())) {
                toReturn.add(list.getName());
            }
        }
        return toReturn;
    }

    public void saveToEnteredList() {
        for (FavoritesList list : this.getCatalogFavorites()) {
            if (list.getName().equals(enteredListName)) {
                this.currentInstance.getFavorites().add(list);
                return;
            }
        }
        FavoritesList newList = new FavoritesList();
        newList.setCatalog(this.catalogBp.findById(this.currentInstance.getCatalog().getId(), Catalog.class));
        newList.setName(this.enteredListName);
        this.currentInstance.getFavorites().add(newList);
    }

    public void setSubstituteItem(CatalogXItem substitute) {
        this.bp.setSubstitute(this.currentInstance, this.bp.findById(substitute.getId(), CatalogXItem.class));

    }
}