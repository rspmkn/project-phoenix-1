/**
 * 
 */
package com.apd.phoenix.administration.view.jsf.bean.event;

/**
 * @author Red Hat Consulting - Red Hat, Inc.
 *
 */
public class UserAddressEvent {

    private String message;

    public UserAddressEvent(String string) {
        this.message = string;
    }

    /**
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message the message to set
     */
    public void setMessage(String message) {
        this.message = message;
    }

}
