package com.apd.phoenix.administration.view.jsf.bean;

import com.apd.phoenix.service.model.PaymentType;
import com.apd.phoenix.service.model.PeriodType;
import java.io.Serializable;
import java.util.List;
import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateful;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.ConversationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.inject.Inject;
import javax.inject.Named;
import com.apd.phoenix.service.persistence.jpa.PeriodTypeDao;

/**
 * Backing bean for PeriodType entities.
 * <p>
 * This class provides CRUD functionality for all PeriodType entities. It focuses
 * purely on Java EE 6 standards (e.g. <tt>&#64;ConversationScoped</tt> for
 * state management, <tt>PersistenceContext</tt> for persistence,
 * <tt>CriteriaBuilder</tt> for searches) rather than introducing a CRUD framework or
 * custom base class.
 */

@Named
@Stateful
@ConversationScoped
public class PeriodTypeBean implements Serializable {

    private static final long serialVersionUID = 1L;

    /*
     * Support creating and retrieving PeriodType entities
     */

    public void validateSelection(FacesContext context, UIComponent component, Object value) {
        if (this.currentInstance.getId() == null) {
            ((UIInput) component).setValid(false);
            FacesMessage message = new FacesMessage();
            message.setSeverity(FacesMessage.SEVERITY_ERROR);
            message.setSummary("Select Period Type to edit");
            message.setDetail("You need to select an Period Type to edit.");
            context.addMessage(component.getClientId(), message);
            return;
        }
    }

    private Long id;

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    private PeriodType currentInstance;

    public PeriodType getPeriodType() {
        return this.currentInstance;
    }

    @Inject
    private PeriodTypeDao dao;

    @Inject
    private PeriodTypeSearchBean searchBean;

    @Inject
    private Conversation conversation;

    public String create() {
        this.conversation.begin();
        return "periodTypeAdd?faces-redirect=true";
    }

    public void retrieve() {
        if (this.searchBean.getSelection() != null) {
            this.currentInstance = this.searchBean.getSelection();
            this.searchBean.setSelection(null);
        }

        if (FacesContext.getCurrentInstance().isPostback()) {
            return;
        }

        if (this.conversation.isTransient()) {
            this.conversation.begin();
        }

        if (this.id == null) {
            this.currentInstance = this.example;
        }
        else {
            this.currentInstance = dao.findById(id, PeriodType.class);
        }

    }

    /*
     * Support updating and deleting PeriodType entities
     */

    public String update() {
        if (!validToUpdate()) {
            return null;
        }

        if (!this.conversation.isTransient()) {
            this.conversation.end();
        }
        try {

            if (currentInstance.getId() == null) {
                dao.create(this.currentInstance);
                return "periodTypeAdd?faces-redirect=true";
            }
            else {
                dao.update(this.currentInstance);
                return "periodTypeEdit?faces-redirect=true";
            }
        }
        catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), e.getMessage()));
            return null;
        }
    }

    public String delete() {
        this.conversation.end();
        try {
            dao.delete(this.currentInstance.getId(), PeriodType.class);
            return "periodTypeEdit?faces-redirect=true";
        }
        catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), e.getMessage()));
            return null;
        }
    }

    /*
     * Support searching PeriodType entities with pagination
     */

    private int page;
    private long count;
    private List<PeriodType> pageItems;

    private PeriodType example = new PeriodType();

    public int getPage() {
        return this.page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getPageSize() {
        return 10;
    }

    public PeriodType getExample() {
        return this.example;
    }

    public void setExample(PeriodType example) {
        this.example = example;
    }

    public void search() {
        this.page = 0;
    }

    public void paginate() {
        this.pageItems = dao.searchByExample(this.example, getPage() * getPageSize(), getPageSize());

        this.count = dao.resultQuantity(this.example);
    }

    public List<PeriodType> getPageItems() {
        return this.pageItems;
    }

    public long getCount() {
        return this.count;
    }

    /*
     * Support listing and POSTing back PeriodType entities (e.g. from inside an
     * HtmlSelectOneMenu)
     */

    public List<PeriodType> getAll() {

        return dao.findAll(PeriodType.class, 0, 0);
    }

    public List<PeriodType> searchByExample(PeriodType search) {
        return dao.searchByExample(search, 0, 0);
    }

    public List<PeriodType> searchByString(String s) {
        PeriodType search = new PeriodType();
        search.setName(s);
        return searchByExample(search);
    }

    @Resource
    private SessionContext sessionContext;

    public Converter getConverter() {

        return new Converter() {

            @Override
            public Object getAsObject(FacesContext context, UIComponent component, String value) {
                return dao.findById(Long.valueOf(value), PeriodType.class);
            }

            @Override
            public String getAsString(FacesContext context, UIComponent component, Object value) {

                if (value == null) {
                    return "";
                }

                return String.valueOf(((PeriodType) value).getId());
            }
        };
    }

    /*
     * Support adding children to bidirectional, one-to-many tables
     */

    private PeriodType add = new PeriodType();

    public PeriodType getAdd() {
        return this.add;
    }

    public PeriodType getAdded() {
        PeriodType added = this.add;
        this.add = new PeriodType();
        return added;
    }

    private boolean validToUpdate() {
        PeriodType searchPeriodType = new PeriodType();
        searchPeriodType.setName(this.currentInstance.getName());
        List<PeriodType> searchList = this.dao.searchByExactExample(searchPeriodType, 0, 0);
        if (searchList.size() != 0 && !searchList.get(0).getId().equals(this.currentInstance.getId())) {
            FacesMessage message = new FacesMessage();
            message.setSeverity(FacesMessage.SEVERITY_ERROR);
            message.setSummary("Name already exists");
            message.setDetail("An action type already exists with the specified name.");
            FacesContext.getCurrentInstance().addMessage(null, message);
            return false;
        }
        return true;
    }
}