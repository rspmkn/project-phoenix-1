package com.apd.phoenix.administration.view.jsf.bean;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.enterprise.context.ConversationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import com.apd.phoenix.service.persistence.jpa.HierarchyNodeDao;
import com.apd.phoenix.web.searchbeans.AbstractSearchBean;

@Named
@ConversationScoped
public class TopLevelHierarchyNodeSearchBean extends AbstractSearchBean<String> implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -7410621341249725079L;

    @Inject
    private HierarchyNodeDao dao;

    @Override
    public String[] getCriteria() {
        String[] toReturn = { "HierarchyNode Name" };
        return toReturn;
    }

    @Override
    protected List<String> search() {
        List<String> toReturn = dao.getTopLevelCategory(this.getValues().get(0).getValue());
        return toReturn;
    }

    @Override
    public Map<String, String> resultRow(String searchResult) {
        Map<String, String> toReturn = new HashMap<String, String>();
        toReturn.put(getColumns()[0], searchResult);
        return toReturn;
    }

}
