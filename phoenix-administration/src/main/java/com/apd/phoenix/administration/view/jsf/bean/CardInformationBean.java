package com.apd.phoenix.administration.view.jsf.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateful;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.ConversationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import com.apd.phoenix.service.model.CardInformation;

/**
 * Backing bean for CardInformation entities.
 * <p>
 * This class provides CRUD functionality for all CardInformation entities. It focuses
 * purely on Java EE 6 standards (e.g. <tt>&#64;ConversationScoped</tt> for
 * state management, <tt>PersistenceContext</tt> for persistence,
 * <tt>CriteriaBuilder</tt> for searches) rather than introducing a CRUD framework or
 * custom base class.
 */

@Named
@Stateful
@ConversationScoped
@Deprecated
public class CardInformationBean implements Serializable {

    private static final long serialVersionUID = 1L;

    /*
     * Support creating and retrieving CardInformation entities
     */

    private Long id;

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    private CardInformation cardInformation;

    public CardInformation getCardInformation() {
        return this.cardInformation;
    }

    @Inject
    private Conversation conversation;

    private EntityManager entityManager;

    public String create() {

        this.conversation.begin();
        return "create?faces-redirect=true";
    }

    public void retrieve() {

        if (FacesContext.getCurrentInstance().isPostback()) {
            return;
        }

        if (this.conversation.isTransient()) {
            this.conversation.begin();
        }

        if (this.id == null) {
            this.cardInformation = this.example;
        }
        else {
            this.cardInformation = findById(getId());
        }
    }

    public CardInformation findById(Long id) {

        return this.entityManager.find(CardInformation.class, id);
    }

    /*
     * Support updating and deleting CardInformation entities
     */

    public String update() {
        this.conversation.end();

        try {
            if (this.id == null) {
                this.entityManager.persist(this.cardInformation);
                return "search?faces-redirect=true";
            }
            else {
                this.entityManager.merge(this.cardInformation);
                return "view?faces-redirect=true&id=" + this.cardInformation.getId();
            }
        }
        catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), e.getMessage()));
            return null;
        }
    }

    public String delete() {
        this.conversation.end();

        try {
            this.entityManager.remove(findById(getId()));
            this.entityManager.flush();
            return "search?faces-redirect=true";
        }
        catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), e.getMessage()));
            return null;
        }
    }

    /*
     * Support searching CardInformation entities with pagination
     */

    private int page;
    private long count;
    private List<CardInformation> pageItems;

    private CardInformation example = new CardInformation();

    public int getPage() {
        return this.page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getPageSize() {
        return 10;
    }

    public CardInformation getExample() {
        return this.example;
    }

    public void setExample(CardInformation example) {
        this.example = example;
    }

    public void search() {
        this.page = 0;
    }

    public void paginate() {

        CriteriaBuilder builder = this.entityManager.getCriteriaBuilder();

        // Populate this.count

        CriteriaQuery<Long> countCriteria = builder.createQuery(Long.class);
        Root<CardInformation> root = countCriteria.from(CardInformation.class);
        countCriteria = countCriteria.select(builder.count(root)).where(getSearchPredicates(root));
        this.count = this.entityManager.createQuery(countCriteria).getSingleResult();

        // Populate this.pageItems

        CriteriaQuery<CardInformation> criteria = builder.createQuery(CardInformation.class);
        root = criteria.from(CardInformation.class);
        TypedQuery<CardInformation> query = this.entityManager.createQuery(criteria.select(root).where(
                getSearchPredicates(root)));
        query.setFirstResult(this.page * getPageSize()).setMaxResults(getPageSize());
        this.pageItems = query.getResultList();
    }

    private Predicate[] getSearchPredicates(Root<CardInformation> root) {
        List<Predicate> predicatesList = new ArrayList<Predicate>();

        return predicatesList.toArray(new Predicate[predicatesList.size()]);
    }

    public List<CardInformation> getPageItems() {
        return this.pageItems;
    }

    public long getCount() {
        return this.count;
    }

    /*
     * Support listing and POSTing back CardInformation entities (e.g. from inside an
     * HtmlSelectOneMenu)
     */

    public List<CardInformation> getAll() {

        CriteriaQuery<CardInformation> criteria = this.entityManager.getCriteriaBuilder().createQuery(
                CardInformation.class);
        return this.entityManager.createQuery(criteria.select(criteria.from(CardInformation.class))).getResultList();
    }

    @Resource
    private SessionContext sessionContext;

    public Converter getConverter() {

        final CardInformationBean ejbProxy = this.sessionContext.getBusinessObject(CardInformationBean.class);

        return new Converter() {

            @Override
            public Object getAsObject(FacesContext context, UIComponent component, String value) {

                return ejbProxy.findById(Long.valueOf(value));
            }

            @Override
            public String getAsString(FacesContext context, UIComponent component, Object value) {

                if (value == null) {
                    return "";
                }

                return String.valueOf(((CardInformation) value).getId());
            }
        };
    }

    /*
     * Support adding children to bidirectional, one-to-many tables
     */

    private CardInformation add = new CardInformation();

    public CardInformation getAdd() {
        return this.add;
    }

    public CardInformation getAdded() {
        CardInformation added = this.add;
        this.add = new CardInformation();
        return added;
    }
}