package com.apd.phoenix.administration.view.jsf.bean;

import java.io.Serializable;
import java.util.List;
import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateful;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.ConversationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.inject.Inject;
import javax.inject.Named;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.business.RoleBp;
import com.apd.phoenix.service.model.Permission;
import com.apd.phoenix.service.model.Role;
import com.apd.phoenix.web.searchbeans.PermissionSearchBean;
import com.apd.phoenix.web.searchbeans.RoleSearchBean;

/**
 * Backing bean for Role entities.
 * <p>
 * This class provides CRUD functionality for all Role entities. It
 * focuses purely on Java EE 6 standards (e.g. <tt>&#64;ConversationScoped</tt>
 * for state management, <tt>PersistenceContext</tt> for persistence,
 * <tt>CriteriaBuilder</tt> for searches) rather than introducing a CRUD
 * framework or custom base class.
 */

@Named
@Stateful
@ConversationScoped
public class RoleBean implements Serializable {

    private static final long serialVersionUID = 1L;

    private static final Logger LOG = LoggerFactory.getLogger(AccountBean.class);

    private Long id;
    private Role role = new Role();
    private Role example = new Role();
    private Role copy = new Role();
    private Role add = new Role();
    private String convoId;

    private int page;
    private long count;

    private List<Role> pageItems;

    @Inject
    private RoleBp roleBp;
    @Inject
    private Conversation conversation;
    @Inject
    private RoleSearchBean roleSearchBean;
    @Inject
    private PermissionSearchBean permissionSearchBean;

    @Resource
    private SessionContext sessionContext;

    public void validateSelection(FacesContext context, UIComponent component, Object value) {
        //		if (this.role.getId() == null) {
        //			((UIInput) component).setValid(false);
        //			FacesMessage message = new FacesMessage();
        //			message.setSeverity(FacesMessage.SEVERITY_ERROR);
        //			message.setSummary("Select Role to edit");
        //			message.setDetail("You need to select a Role to edit.");
        //			context.addMessage(component.getClientId(), message);
        //			return;
        //		}
    }

    public String create() {
        this.conversation.begin();
        return "roleAdd?faces-redirect=true";
    }

    public void setPermission(Permission toSet) {
        this.role.getPermissions().add(toSet);
    }

    public void retrieve() {
        this.convoId = this.conversation.getId();

        if (this.roleSearchBean.getSelection() != null) {
            this.role = roleSearchBean.getSelection();
            this.roleSearchBean.setSelection(null);
        }

        if (FacesContext.getCurrentInstance().isPostback()) {
            return;
        }

        if (this.conversation.isTransient()) {
            this.conversation.begin();
        }
        this.convoId = this.conversation.getId();
        if (this.id == null) {
            this.role = this.example;
        }
        else {
            this.role = roleBp.findById(getId(), Role.class);
        }

        if (this.id == null || !this.role.getId().equals(example.getId())) {
            this.role = example;
        }
        if (this.copy.getId() != null) {
            this.role = this.copy;
            this.role.setId(null);
            this.role.setName(null);
            this.copy = new Role();
        }
        this.convoId = this.conversation.getId();
    }

    public String update() {
        if (!validToCreate()) {
            return null;
        }
        this.convoId = this.conversation.getId();
        if (!this.conversation.isTransient()) {
            this.conversation.end();
        }
        this.convoId = this.conversation.getId();
        try {

            //this.role = roleBp.update(this.role);
            this.id = this.role.getId();

            if (this.role.getId() == null) {
                this.roleBp.update(this.role);
                return "roleAdd?faces-redirect=true";
            }
            else {
                this.roleBp.update(this.role);
                return "roleEdit?faces-redirect=true";
            }
        }
        catch (Exception e) {
            LOG.error(e.getMessage());
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, "Unexpected Error", "Unexpected Error"));
            return null;
        }
    }

    public String delete() {
        this.conversation.end();

        try {
            this.roleBp.delete(this.role.getId(), Role.class);
            return "roleEdit?faces-redirect=true";
        }
        catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), e.getMessage()));
            return null;
        }
    }

    public String copy() {
        try {
            this.copy = this.role;
            return "roleAdd?faces-redirect=true";
        }
        catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), e.getMessage()));
            return null;
        }
    }

    public Role findById(Long id) {

        return this.roleBp.findById(id, Role.class);
    }

    public List<Role> getAll() {
        return roleBp.findAll(Role.class, 0, 0);
    }

    public List<Role> searchByExample(Role search) {
        return roleBp.searchByExample(search, 0, 0);
    }

    public List<Role> searchByString(String s) {
        Role search = new Role();
        search.setName(s);
        return searchByExample(search);
    }

    public Converter getConverter() {

        final RoleBean ejbProxy = this.sessionContext.getBusinessObject(RoleBean.class);

        return new Converter() {

            @Override
            public Object getAsObject(FacesContext context, UIComponent component, String value) {
                if (ejbProxy.role == null) {
                    ejbProxy.role = new Role();
                }
                ejbProxy.setId(Long.valueOf(value));
                ejbProxy.retrieve();
                return ejbProxy.getRole();
            }

            @Override
            public String getAsString(FacesContext context, UIComponent component, Object value) {

                if (value == null) {
                    return "";
                }

                return String.valueOf(((Role) value).getId());
            }
        };
    }

    /*	
     public void assignPermissionToRole(Permission permission) {
     permission.getRoles().add(this.role);
     this.role.getPermissions().add(permission);
     }
     */
    public void search() {
        this.page = 0;
    }

    public void paginate() {
        this.count = roleBp.resultQuantity(this.example);
        this.pageItems = roleBp.searchByExample(this.example, getPage() * getPageSize(), getPageSize());
    }

    /* Getters and Setters */
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Role getRole() {
        return this.role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public Role getAdd() {
        return this.add;
    }

    public Role getAdded() {
        Role added = this.add;
        this.add = new Role();
        return added;
    }

    public void removePermission(Permission toRemove) {
        this.role.getPermissions().remove(toRemove);
    }

    /*
     * Support searching Role entities with pagination
     */
    public int getPage() {
        return this.page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getPageSize() {
        return 10;
    }

    public Role getExample() {
        return this.example;
    }

    public void setExample(Role example) {
        this.example = example;
    }

    public List<Role> getPageItems() {
        return this.pageItems;
    }

    public long getCount() {
        return this.count;
    }

    private boolean validToCreate() {
        Role searchRole = new Role();
        searchRole.setName(this.role.getName());
        List<Role> searchList = this.roleBp.searchByExactExample(searchRole, 0, 0);
        if (searchList.size() != 0 && !searchList.get(0).getId().equals(this.role.getId())) {
            FacesMessage message = new FacesMessage();
            message.setSeverity(FacesMessage.SEVERITY_ERROR);
            message.setSummary("Name already exists");
            message.setDetail("A role already exists with the specified name.");
            FacesContext.getCurrentInstance().addMessage(null, message);
            return false;
        }
        return true;
    }
}