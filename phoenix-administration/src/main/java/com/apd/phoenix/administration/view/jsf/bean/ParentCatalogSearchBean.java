package com.apd.phoenix.administration.view.jsf.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.enterprise.context.ConversationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import com.apd.phoenix.service.model.Catalog;
import com.apd.phoenix.service.model.SystemUser;
import com.apd.phoenix.service.model.Vendor;
import com.apd.phoenix.service.persistence.jpa.CatalogDao;
import com.apd.phoenix.web.searchbeans.AbstractSearchBean;

@Named
@ConversationScoped
public class ParentCatalogSearchBean extends AbstractSearchBean<Catalog> implements Serializable {

    /**
     * Generated serial ID
     */
    private static final long serialVersionUID = -6379624268920244721L;

    @Inject
    CatalogDao dao;

    @Override
    public String[] getCriteria() {
        String[] toReturn = { "Catalog Name" };
        return toReturn;
    }

    @Override
    protected List<Catalog> search() {
        return dao.searchCustomerCatalogByName(this.getValues().get(0).getValue());
    }

    @Override
    public String[] getColumns() {
        String[] toReturn = { "Name" };
        return toReturn;
    }

    @Override
    public Map<String, String> resultRow(Catalog searchResult) {
        Map<String, String> toReturn = new HashMap<String, String>();
        toReturn.put(getColumns()[0], searchResult.getName());
        return toReturn;
    }

}
