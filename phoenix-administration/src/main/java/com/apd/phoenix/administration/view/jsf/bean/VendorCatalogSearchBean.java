package com.apd.phoenix.administration.view.jsf.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.enterprise.context.ConversationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import com.apd.phoenix.service.model.Catalog;
import com.apd.phoenix.service.model.Vendor;
import com.apd.phoenix.service.persistence.jpa.CatalogDao;
import com.apd.phoenix.web.PropertiesBean;
import com.apd.phoenix.web.searchbeans.AbstractSearchBean;

@Named
@ConversationScoped
public class VendorCatalogSearchBean extends AbstractSearchBean<Catalog> implements Serializable {

    /**
     * Generated serial ID
     */
    private static final long serialVersionUID = -6379624268920244721L;

    @Inject
    CatalogDao dao;

    @Inject
    private PropertiesBean propertiesBean;

    @Override
    public String[] getCriteria() {
        String[] toReturn = { "Catalog Name", "Vendor Name" };
        return toReturn;
    }

    @Override
    protected List<Catalog> search() {
        Catalog catalog = new Catalog();
        catalog.setName(this.getValues().get(0).getValue());
        Vendor vendor = new Vendor();
        vendor.setName(this.getValues().get(1).getValue());
        catalog.setVendor(vendor);
        List<Catalog> results = dao.searchByExample(catalog, 0, 0);
        List<Catalog> toReturn = new ArrayList<Catalog>();
        for (Catalog c : results) {
            if (c.getCustomer() == null) {
                toReturn.add(c);
            }
        }
        return toReturn;
    }

    @Override
    public String[] getColumns() {
        if (propertiesBean.get("project.state", "isPreRelease", "false").equals("true")) {
            String[] toReturn = { "Name", "Vendor", "System ID" };
            return toReturn;
        }
        else {
            String[] toReturn = { "Name", "Vendor" };
            return toReturn;
        }
    }

    @Override
    public Map<String, String> resultRow(Catalog searchResult) {
        searchResult = dao.hydrateForSearchResults(searchResult);
        Map<String, String> toReturn = new HashMap<String, String>();
        toReturn.put(getColumns()[0], searchResult.getName());
        if (searchResult.getVendor() != null) {
            toReturn.put(getColumns()[1], searchResult.getVendor().getName());
        }
        if (propertiesBean.get("project.state", "isPreRelease", "false").equals("true")) {
            toReturn.put(getColumns()[2], searchResult.getId().toString());
        }
        return toReturn;
    }

}
