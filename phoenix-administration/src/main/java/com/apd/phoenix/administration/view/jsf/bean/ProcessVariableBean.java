package com.apd.phoenix.administration.view.jsf.bean;

import java.io.Serializable;
import java.util.List;
import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateful;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.ConversationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.inject.Inject;
import javax.inject.Named;
import com.apd.phoenix.service.model.ProcessVariable;
import com.apd.phoenix.service.persistence.jpa.ProcessVariableDao;

/**
 * Backing bean for ProcessVariable entities.
 * <p>
 * This class provides CRUD functionality for all ProcessVariable entities. It focuses
 * purely on Java EE 6 standards (e.g. <tt>&#64;ConversationScoped</tt> for
 * state management, <tt>PersistenceContext</tt> for persistence,
 * <tt>CriteriaBuilder</tt> for searches) rather than introducing a CRUD framework or
 * custom base class.
 */

@Named
@Stateful
@ConversationScoped
public class ProcessVariableBean implements Serializable {

    private static final long serialVersionUID = 1L;

    /*
     * Support creating and retrieving ProcessVariable entities
     */

    public void validateSelection(FacesContext context, UIComponent component, Object value) {
        if (this.currentInstance.getId() == null) {
            ((UIInput) component).setValid(false);
            FacesMessage message = new FacesMessage();
            message.setSeverity(FacesMessage.SEVERITY_ERROR);
            message.setSummary("Select Process Variable to edit");
            message.setDetail("You need to select an Process Variable to edit.");
            context.addMessage(component.getClientId(), message);
            return;
        }
    }

    private Long id;

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    private ProcessVariable currentInstance;

    public ProcessVariable getProcessVariable() {
        return this.currentInstance;
    }

    @Inject
    private ProcessVariableDao dao;

    @Inject
    private ProcessVariableSearchBean searchBean;

    @Inject
    private Conversation conversation;

    public String create() {

        this.conversation.begin();
        return "processVariableAdd?faces-redirect=true";
    }

    public void retrieve() {
        if (this.searchBean.getSelection() != null) {
            this.currentInstance = this.searchBean.getSelection();
            this.searchBean.setSelection(null);
        }

        if (FacesContext.getCurrentInstance().isPostback()) {
            return;
        }

        if (this.conversation.isTransient()) {
            this.conversation.begin();
        }

        if (this.id == null) {
            this.currentInstance = this.example;
        }
        else {
            this.currentInstance = dao.findById(id, ProcessVariable.class);
        }

    }

    /*
     * Support updating and deleting ProcessVariable entities
     */

    public String update() {
        this.conversation.end();

        try {

            if (currentInstance.getId() == null) {
                dao.create(this.currentInstance);
                return "processVariableAdd?faces-redirect=true";
            }
            else {
                dao.update(this.currentInstance);
                return "processVariableEdit?faces-redirect=true";
            }
        }
        catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), e.getMessage()));
            return null;
        }
    }

    public String delete() {
        this.conversation.end();
        try {
            dao.delete(this.currentInstance.getId(), ProcessVariable.class);
            return "processVariableEdit?faces-redirect=true";
        }
        catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), e.getMessage()));
            return null;
        }
    }

    /*
     * Support searching ProcessVariable entities with pagination
     */

    private int page;
    private long count;
    private List<ProcessVariable> pageItems;

    private ProcessVariable example = new ProcessVariable();

    public int getPage() {
        return this.page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getPageSize() {
        return 10;
    }

    public ProcessVariable getExample() {
        return this.example;
    }

    public void setExample(ProcessVariable example) {
        this.example = example;
    }

    public void search() {
        this.page = 0;
    }

    public void paginate() {
        this.pageItems = dao.searchByExample(this.example, getPage() * getPageSize(), getPageSize());

        this.count = dao.resultQuantity(this.example);
    }

    public List<ProcessVariable> getPageItems() {
        return this.pageItems;
    }

    public long getCount() {
        return this.count;
    }

    /*
     * Support listing and POSTing back ProcessVariable entities (e.g. from inside an
     * HtmlSelectOneMenu)
     */

    public List<ProcessVariable> getAll() {

        return dao.findAll(ProcessVariable.class, 0, 0);
    }

    public List<ProcessVariable> searchByExample(ProcessVariable search) {
        return dao.searchByExample(search, 0, 0);
    }

    public List<ProcessVariable> searchByString(String s) {
        ProcessVariable search = new ProcessVariable();
        search.setName(s);
        return searchByExample(search);
    }

    @Resource
    private SessionContext sessionContext;

    public Converter getConverter() {

        final ProcessVariableBean ejbProxy = this.sessionContext.getBusinessObject(ProcessVariableBean.class);

        return new Converter() {

            @Override
            public Object getAsObject(FacesContext context, UIComponent component, String value) {

                ejbProxy.setId(Long.valueOf(value));
                ejbProxy.retrieve();
                return ejbProxy.getProcessVariable();
            }

            @Override
            public String getAsString(FacesContext context, UIComponent component, Object value) {

                if (value == null) {
                    return "";
                }

                return String.valueOf(((ProcessVariable) value).getId());
            }
        };
    }

    /*
     * Support adding children to bidirectional, one-to-many tables
     */

    private ProcessVariable add = new ProcessVariable();

    public ProcessVariable getAdd() {
        return this.add;
    }

    public ProcessVariable getAdded() {
        ProcessVariable added = this.add;
        this.add = new ProcessVariable();
        return added;
    }
}