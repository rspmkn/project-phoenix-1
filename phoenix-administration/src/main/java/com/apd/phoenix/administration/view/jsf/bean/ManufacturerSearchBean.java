package com.apd.phoenix.administration.view.jsf.bean;

import java.io.Serializable;
import java.util.List;
import javax.enterprise.context.ConversationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import com.apd.phoenix.service.business.ManufacturerBp;
import com.apd.phoenix.service.model.Manufacturer;
import com.apd.phoenix.web.searchbeans.AbstractSearchBean;

@Named
@ConversationScoped
public class ManufacturerSearchBean extends AbstractSearchBean<Manufacturer> implements Serializable {

    @Inject
    ManufacturerBp bp;

    @Override
    public String[] getCriteria() {
        String[] toReturn = { "Name" };
        return toReturn;
    }

    @Override
    protected List<Manufacturer> search() {
        Manufacturer search = new Manufacturer();
        search.setName(this.getValues().get(0).getValue());
        return bp.searchByExample(search, 0, 0);
    }

}
