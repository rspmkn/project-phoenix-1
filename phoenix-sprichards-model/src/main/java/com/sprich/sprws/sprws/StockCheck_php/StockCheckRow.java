/**
 * StockCheckRow.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.sprich.sprws.sprws.StockCheck_php;

public class StockCheckRow implements java.io.Serializable {

    private java.lang.String dcNum;

    private java.lang.String dcName;

    private java.lang.String available;

    private java.lang.String uom;

    private java.lang.String onOrder;

    private java.lang.String expected;

    private java.lang.String sprinter;

    private java.lang.String cutOff;

    private java.lang.String leadTime;

    private java.lang.String dcType;

    public StockCheckRow() {
    }

    public StockCheckRow(java.lang.String dcNum, java.lang.String dcName, java.lang.String available,
            java.lang.String uom, java.lang.String onOrder, java.lang.String expected, java.lang.String sprinter,
            java.lang.String cutOff, java.lang.String leadTime, java.lang.String dcType) {
        this.dcNum = dcNum;
        this.dcName = dcName;
        this.available = available;
        this.uom = uom;
        this.onOrder = onOrder;
        this.expected = expected;
        this.sprinter = sprinter;
        this.cutOff = cutOff;
        this.leadTime = leadTime;
        this.dcType = dcType;
    }

    /**
     * Gets the dcNum value for this StockCheckRow.
     * 
     * @return dcNum
     */
    public java.lang.String getDcNum() {
        return dcNum;
    }

    /**
     * Sets the dcNum value for this StockCheckRow.
     * 
     * @param dcNum
     */
    public void setDcNum(java.lang.String dcNum) {
        this.dcNum = dcNum;
    }

    /**
     * Gets the dcName value for this StockCheckRow.
     * 
     * @return dcName
     */
    public java.lang.String getDcName() {
        return dcName;
    }

    /**
     * Sets the dcName value for this StockCheckRow.
     * 
     * @param dcName
     */
    public void setDcName(java.lang.String dcName) {
        this.dcName = dcName;
    }

    /**
     * Gets the available value for this StockCheckRow.
     * 
     * @return available
     */
    public java.lang.String getAvailable() {
        return available;
    }

    /**
     * Sets the available value for this StockCheckRow.
     * 
     * @param available
     */
    public void setAvailable(java.lang.String available) {
        this.available = available;
    }

    /**
     * Gets the uom value for this StockCheckRow.
     * 
     * @return uom
     */
    public java.lang.String getUom() {
        return uom;
    }

    /**
     * Sets the uom value for this StockCheckRow.
     * 
     * @param uom
     */
    public void setUom(java.lang.String uom) {
        this.uom = uom;
    }

    /**
     * Gets the onOrder value for this StockCheckRow.
     * 
     * @return onOrder
     */
    public java.lang.String getOnOrder() {
        return onOrder;
    }

    /**
     * Sets the onOrder value for this StockCheckRow.
     * 
     * @param onOrder
     */
    public void setOnOrder(java.lang.String onOrder) {
        this.onOrder = onOrder;
    }

    /**
     * Gets the expected value for this StockCheckRow.
     * 
     * @return expected
     */
    public java.lang.String getExpected() {
        return expected;
    }

    /**
     * Sets the expected value for this StockCheckRow.
     * 
     * @param expected
     */
    public void setExpected(java.lang.String expected) {
        this.expected = expected;
    }

    /**
     * Gets the sprinter value for this StockCheckRow.
     * 
     * @return sprinter
     */
    public java.lang.String getSprinter() {
        return sprinter;
    }

    /**
     * Sets the sprinter value for this StockCheckRow.
     * 
     * @param sprinter
     */
    public void setSprinter(java.lang.String sprinter) {
        this.sprinter = sprinter;
    }

    /**
     * Gets the cutOff value for this StockCheckRow.
     * 
     * @return cutOff
     */
    public java.lang.String getCutOff() {
        return cutOff;
    }

    /**
     * Sets the cutOff value for this StockCheckRow.
     * 
     * @param cutOff
     */
    public void setCutOff(java.lang.String cutOff) {
        this.cutOff = cutOff;
    }

    /**
     * Gets the leadTime value for this StockCheckRow.
     * 
     * @return leadTime
     */
    public java.lang.String getLeadTime() {
        return leadTime;
    }

    /**
     * Sets the leadTime value for this StockCheckRow.
     * 
     * @param leadTime
     */
    public void setLeadTime(java.lang.String leadTime) {
        this.leadTime = leadTime;
    }

    /**
     * Gets the dcType value for this StockCheckRow.
     * 
     * @return dcType
     */
    public java.lang.String getDcType() {
        return dcType;
    }

    /**
     * Sets the dcType value for this StockCheckRow.
     * 
     * @param dcType
     */
    public void setDcType(java.lang.String dcType) {
        this.dcType = dcType;
    }

    private java.lang.Object __equalsCalc = null;

    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof StockCheckRow))
            return false;
        StockCheckRow other = (StockCheckRow) obj;
        if (obj == null)
            return false;
        if (this == obj)
            return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true
                && ((this.dcNum == null && other.getDcNum() == null) || (this.dcNum != null && this.dcNum.equals(other
                        .getDcNum())))
                && ((this.dcName == null && other.getDcName() == null) || (this.dcName != null && this.dcName
                        .equals(other.getDcName())))
                && ((this.available == null && other.getAvailable() == null) || (this.available != null && this.available
                        .equals(other.getAvailable())))
                && ((this.uom == null && other.getUom() == null) || (this.uom != null && this.uom
                        .equals(other.getUom())))
                && ((this.onOrder == null && other.getOnOrder() == null) || (this.onOrder != null && this.onOrder
                        .equals(other.getOnOrder())))
                && ((this.expected == null && other.getExpected() == null) || (this.expected != null && this.expected
                        .equals(other.getExpected())))
                && ((this.sprinter == null && other.getSprinter() == null) || (this.sprinter != null && this.sprinter
                        .equals(other.getSprinter())))
                && ((this.cutOff == null && other.getCutOff() == null) || (this.cutOff != null && this.cutOff
                        .equals(other.getCutOff())))
                && ((this.leadTime == null && other.getLeadTime() == null) || (this.leadTime != null && this.leadTime
                        .equals(other.getLeadTime())))
                && ((this.dcType == null && other.getDcType() == null) || (this.dcType != null && this.dcType
                        .equals(other.getDcType())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;

    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getDcNum() != null) {
            _hashCode += getDcNum().hashCode();
        }
        if (getDcName() != null) {
            _hashCode += getDcName().hashCode();
        }
        if (getAvailable() != null) {
            _hashCode += getAvailable().hashCode();
        }
        if (getUom() != null) {
            _hashCode += getUom().hashCode();
        }
        if (getOnOrder() != null) {
            _hashCode += getOnOrder().hashCode();
        }
        if (getExpected() != null) {
            _hashCode += getExpected().hashCode();
        }
        if (getSprinter() != null) {
            _hashCode += getSprinter().hashCode();
        }
        if (getCutOff() != null) {
            _hashCode += getCutOff().hashCode();
        }
        if (getLeadTime() != null) {
            _hashCode += getLeadTime().hashCode();
        }
        if (getDcType() != null) {
            _hashCode += getDcType().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc = new org.apache.axis.description.TypeDesc(
            StockCheckRow.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://sprws.sprich.com/sprws/StockCheck.php?wsdl",
                "StockCheckRow"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dcNum");
        elemField.setXmlName(new javax.xml.namespace.QName("", "DcNum"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dcName");
        elemField.setXmlName(new javax.xml.namespace.QName("", "DcName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("available");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Available"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("uom");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Uom"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("onOrder");
        elemField.setXmlName(new javax.xml.namespace.QName("", "OnOrder"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("expected");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Expected"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sprinter");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Sprinter"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cutOff");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CutOff"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("leadTime");
        elemField.setXmlName(new javax.xml.namespace.QName("", "LeadTime"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dcType");
        elemField.setXmlName(new javax.xml.namespace.QName("", "DcType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(java.lang.String mechType,
            java.lang.Class _javaType, javax.xml.namespace.QName _xmlType) {
        return new org.apache.axis.encoding.ser.BeanSerializer(_javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(java.lang.String mechType,
            java.lang.Class _javaType, javax.xml.namespace.QName _xmlType) {
        return new org.apache.axis.encoding.ser.BeanDeserializer(_javaType, _xmlType, typeDesc);
    }

}
