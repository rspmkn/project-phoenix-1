/**
 * StockCheckResults.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.sprich.sprws.sprws.StockCheck_php;

public class StockCheckResults implements java.io.Serializable {

    private java.lang.String errorMessage;

    private java.lang.String rtnStatus;

    private java.lang.String rtnMessage;

    private java.lang.String sprItemNum;

    private java.lang.String stripNumber;

    private java.lang.String upcNumber;

    private java.lang.String oldNumber;

    private java.lang.String itemStatus;

    private java.lang.String productClass;

    private java.lang.String description;

    private java.lang.String sellUom;

    private java.lang.String catalogPage;

    private java.lang.String matrixPage;

    private java.lang.String itemWeight;

    private java.lang.String itemCubes;

    private java.lang.String brokenQtyAllowed;

    private java.lang.String infoProcessing;

    private java.lang.String readyAssemble;

    private java.lang.String furniture;

    private java.lang.String datedGoods;

    private java.lang.String countryOfOrigin;

    private java.lang.String serviceSupply;

    private java.lang.String noReturn;

    private java.lang.String specialOrder;

    private java.lang.String upsShippable;

    private java.lang.String hazmatMessage;

    private java.lang.String retailPrice;

    private java.lang.String retailUom;

    private java.lang.String matrixPrice;

    private java.lang.String matrixUom;

    private java.lang.String currencyCode;

    private java.lang.String orderMinimum;

    private java.lang.String upchargeMessage;

    private com.sprich.sprws.sprws.StockCheck_php.StockCheckRow[] resultsRows;

    public StockCheckResults() {
    }

    public StockCheckResults(java.lang.String errorMessage, java.lang.String rtnStatus, java.lang.String rtnMessage,
            java.lang.String sprItemNum, java.lang.String stripNumber, java.lang.String upcNumber,
            java.lang.String oldNumber, java.lang.String itemStatus, java.lang.String productClass,
            java.lang.String description, java.lang.String sellUom, java.lang.String catalogPage,
            java.lang.String matrixPage, java.lang.String itemWeight, java.lang.String itemCubes,
            java.lang.String brokenQtyAllowed, java.lang.String infoProcessing, java.lang.String readyAssemble,
            java.lang.String furniture, java.lang.String datedGoods, java.lang.String countryOfOrigin,
            java.lang.String serviceSupply, java.lang.String noReturn, java.lang.String specialOrder,
            java.lang.String upsShippable, java.lang.String hazmatMessage, java.lang.String retailPrice,
            java.lang.String retailUom, java.lang.String matrixPrice, java.lang.String matrixUom,
            java.lang.String currencyCode, java.lang.String orderMinimum, java.lang.String upchargeMessage,
            com.sprich.sprws.sprws.StockCheck_php.StockCheckRow[] resultsRows) {
        this.errorMessage = errorMessage;
        this.rtnStatus = rtnStatus;
        this.rtnMessage = rtnMessage;
        this.sprItemNum = sprItemNum;
        this.stripNumber = stripNumber;
        this.upcNumber = upcNumber;
        this.oldNumber = oldNumber;
        this.itemStatus = itemStatus;
        this.productClass = productClass;
        this.description = description;
        this.sellUom = sellUom;
        this.catalogPage = catalogPage;
        this.matrixPage = matrixPage;
        this.itemWeight = itemWeight;
        this.itemCubes = itemCubes;
        this.brokenQtyAllowed = brokenQtyAllowed;
        this.infoProcessing = infoProcessing;
        this.readyAssemble = readyAssemble;
        this.furniture = furniture;
        this.datedGoods = datedGoods;
        this.countryOfOrigin = countryOfOrigin;
        this.serviceSupply = serviceSupply;
        this.noReturn = noReturn;
        this.specialOrder = specialOrder;
        this.upsShippable = upsShippable;
        this.hazmatMessage = hazmatMessage;
        this.retailPrice = retailPrice;
        this.retailUom = retailUom;
        this.matrixPrice = matrixPrice;
        this.matrixUom = matrixUom;
        this.currencyCode = currencyCode;
        this.orderMinimum = orderMinimum;
        this.upchargeMessage = upchargeMessage;
        this.resultsRows = resultsRows;
    }

    /**
     * Gets the errorMessage value for this StockCheckResults.
     * 
     * @return errorMessage
     */
    public java.lang.String getErrorMessage() {
        return errorMessage;
    }

    /**
     * Sets the errorMessage value for this StockCheckResults.
     * 
     * @param errorMessage
     */
    public void setErrorMessage(java.lang.String errorMessage) {
        this.errorMessage = errorMessage;
    }

    /**
     * Gets the rtnStatus value for this StockCheckResults.
     * 
     * @return rtnStatus
     */
    public java.lang.String getRtnStatus() {
        return rtnStatus;
    }

    /**
     * Sets the rtnStatus value for this StockCheckResults.
     * 
     * @param rtnStatus
     */
    public void setRtnStatus(java.lang.String rtnStatus) {
        this.rtnStatus = rtnStatus;
    }

    /**
     * Gets the rtnMessage value for this StockCheckResults.
     * 
     * @return rtnMessage
     */
    public java.lang.String getRtnMessage() {
        return rtnMessage;
    }

    /**
     * Sets the rtnMessage value for this StockCheckResults.
     * 
     * @param rtnMessage
     */
    public void setRtnMessage(java.lang.String rtnMessage) {
        this.rtnMessage = rtnMessage;
    }

    /**
     * Gets the sprItemNum value for this StockCheckResults.
     * 
     * @return sprItemNum
     */
    public java.lang.String getSprItemNum() {
        return sprItemNum;
    }

    /**
     * Sets the sprItemNum value for this StockCheckResults.
     * 
     * @param sprItemNum
     */
    public void setSprItemNum(java.lang.String sprItemNum) {
        this.sprItemNum = sprItemNum;
    }

    /**
     * Gets the stripNumber value for this StockCheckResults.
     * 
     * @return stripNumber
     */
    public java.lang.String getStripNumber() {
        return stripNumber;
    }

    /**
     * Sets the stripNumber value for this StockCheckResults.
     * 
     * @param stripNumber
     */
    public void setStripNumber(java.lang.String stripNumber) {
        this.stripNumber = stripNumber;
    }

    /**
     * Gets the upcNumber value for this StockCheckResults.
     * 
     * @return upcNumber
     */
    public java.lang.String getUpcNumber() {
        return upcNumber;
    }

    /**
     * Sets the upcNumber value for this StockCheckResults.
     * 
     * @param upcNumber
     */
    public void setUpcNumber(java.lang.String upcNumber) {
        this.upcNumber = upcNumber;
    }

    /**
     * Gets the oldNumber value for this StockCheckResults.
     * 
     * @return oldNumber
     */
    public java.lang.String getOldNumber() {
        return oldNumber;
    }

    /**
     * Sets the oldNumber value for this StockCheckResults.
     * 
     * @param oldNumber
     */
    public void setOldNumber(java.lang.String oldNumber) {
        this.oldNumber = oldNumber;
    }

    /**
     * Gets the itemStatus value for this StockCheckResults.
     * 
     * @return itemStatus
     */
    public java.lang.String getItemStatus() {
        return itemStatus;
    }

    /**
     * Sets the itemStatus value for this StockCheckResults.
     * 
     * @param itemStatus
     */
    public void setItemStatus(java.lang.String itemStatus) {
        this.itemStatus = itemStatus;
    }

    /**
     * Gets the productClass value for this StockCheckResults.
     * 
     * @return productClass
     */
    public java.lang.String getProductClass() {
        return productClass;
    }

    /**
     * Sets the productClass value for this StockCheckResults.
     * 
     * @param productClass
     */
    public void setProductClass(java.lang.String productClass) {
        this.productClass = productClass;
    }

    /**
     * Gets the description value for this StockCheckResults.
     * 
     * @return description
     */
    public java.lang.String getDescription() {
        return description;
    }

    /**
     * Sets the description value for this StockCheckResults.
     * 
     * @param description
     */
    public void setDescription(java.lang.String description) {
        this.description = description;
    }

    /**
     * Gets the sellUom value for this StockCheckResults.
     * 
     * @return sellUom
     */
    public java.lang.String getSellUom() {
        return sellUom;
    }

    /**
     * Sets the sellUom value for this StockCheckResults.
     * 
     * @param sellUom
     */
    public void setSellUom(java.lang.String sellUom) {
        this.sellUom = sellUom;
    }

    /**
     * Gets the catalogPage value for this StockCheckResults.
     * 
     * @return catalogPage
     */
    public java.lang.String getCatalogPage() {
        return catalogPage;
    }

    /**
     * Sets the catalogPage value for this StockCheckResults.
     * 
     * @param catalogPage
     */
    public void setCatalogPage(java.lang.String catalogPage) {
        this.catalogPage = catalogPage;
    }

    /**
     * Gets the matrixPage value for this StockCheckResults.
     * 
     * @return matrixPage
     */
    public java.lang.String getMatrixPage() {
        return matrixPage;
    }

    /**
     * Sets the matrixPage value for this StockCheckResults.
     * 
     * @param matrixPage
     */
    public void setMatrixPage(java.lang.String matrixPage) {
        this.matrixPage = matrixPage;
    }

    /**
     * Gets the itemWeight value for this StockCheckResults.
     * 
     * @return itemWeight
     */
    public java.lang.String getItemWeight() {
        return itemWeight;
    }

    /**
     * Sets the itemWeight value for this StockCheckResults.
     * 
     * @param itemWeight
     */
    public void setItemWeight(java.lang.String itemWeight) {
        this.itemWeight = itemWeight;
    }

    /**
     * Gets the itemCubes value for this StockCheckResults.
     * 
     * @return itemCubes
     */
    public java.lang.String getItemCubes() {
        return itemCubes;
    }

    /**
     * Sets the itemCubes value for this StockCheckResults.
     * 
     * @param itemCubes
     */
    public void setItemCubes(java.lang.String itemCubes) {
        this.itemCubes = itemCubes;
    }

    /**
     * Gets the brokenQtyAllowed value for this StockCheckResults.
     * 
     * @return brokenQtyAllowed
     */
    public java.lang.String getBrokenQtyAllowed() {
        return brokenQtyAllowed;
    }

    /**
     * Sets the brokenQtyAllowed value for this StockCheckResults.
     * 
     * @param brokenQtyAllowed
     */
    public void setBrokenQtyAllowed(java.lang.String brokenQtyAllowed) {
        this.brokenQtyAllowed = brokenQtyAllowed;
    }

    /**
     * Gets the infoProcessing value for this StockCheckResults.
     * 
     * @return infoProcessing
     */
    public java.lang.String getInfoProcessing() {
        return infoProcessing;
    }

    /**
     * Sets the infoProcessing value for this StockCheckResults.
     * 
     * @param infoProcessing
     */
    public void setInfoProcessing(java.lang.String infoProcessing) {
        this.infoProcessing = infoProcessing;
    }

    /**
     * Gets the readyAssemble value for this StockCheckResults.
     * 
     * @return readyAssemble
     */
    public java.lang.String getReadyAssemble() {
        return readyAssemble;
    }

    /**
     * Sets the readyAssemble value for this StockCheckResults.
     * 
     * @param readyAssemble
     */
    public void setReadyAssemble(java.lang.String readyAssemble) {
        this.readyAssemble = readyAssemble;
    }

    /**
     * Gets the furniture value for this StockCheckResults.
     * 
     * @return furniture
     */
    public java.lang.String getFurniture() {
        return furniture;
    }

    /**
     * Sets the furniture value for this StockCheckResults.
     * 
     * @param furniture
     */
    public void setFurniture(java.lang.String furniture) {
        this.furniture = furniture;
    }

    /**
     * Gets the datedGoods value for this StockCheckResults.
     * 
     * @return datedGoods
     */
    public java.lang.String getDatedGoods() {
        return datedGoods;
    }

    /**
     * Sets the datedGoods value for this StockCheckResults.
     * 
     * @param datedGoods
     */
    public void setDatedGoods(java.lang.String datedGoods) {
        this.datedGoods = datedGoods;
    }

    /**
     * Gets the countryOfOrigin value for this StockCheckResults.
     * 
     * @return countryOfOrigin
     */
    public java.lang.String getCountryOfOrigin() {
        return countryOfOrigin;
    }

    /**
     * Sets the countryOfOrigin value for this StockCheckResults.
     * 
     * @param countryOfOrigin
     */
    public void setCountryOfOrigin(java.lang.String countryOfOrigin) {
        this.countryOfOrigin = countryOfOrigin;
    }

    /**
     * Gets the serviceSupply value for this StockCheckResults.
     * 
     * @return serviceSupply
     */
    public java.lang.String getServiceSupply() {
        return serviceSupply;
    }

    /**
     * Sets the serviceSupply value for this StockCheckResults.
     * 
     * @param serviceSupply
     */
    public void setServiceSupply(java.lang.String serviceSupply) {
        this.serviceSupply = serviceSupply;
    }

    /**
     * Gets the noReturn value for this StockCheckResults.
     * 
     * @return noReturn
     */
    public java.lang.String getNoReturn() {
        return noReturn;
    }

    /**
     * Sets the noReturn value for this StockCheckResults.
     * 
     * @param noReturn
     */
    public void setNoReturn(java.lang.String noReturn) {
        this.noReturn = noReturn;
    }

    /**
     * Gets the specialOrder value for this StockCheckResults.
     * 
     * @return specialOrder
     */
    public java.lang.String getSpecialOrder() {
        return specialOrder;
    }

    /**
     * Sets the specialOrder value for this StockCheckResults.
     * 
     * @param specialOrder
     */
    public void setSpecialOrder(java.lang.String specialOrder) {
        this.specialOrder = specialOrder;
    }

    /**
     * Gets the upsShippable value for this StockCheckResults.
     * 
     * @return upsShippable
     */
    public java.lang.String getUpsShippable() {
        return upsShippable;
    }

    /**
     * Sets the upsShippable value for this StockCheckResults.
     * 
     * @param upsShippable
     */
    public void setUpsShippable(java.lang.String upsShippable) {
        this.upsShippable = upsShippable;
    }

    /**
     * Gets the hazmatMessage value for this StockCheckResults.
     * 
     * @return hazmatMessage
     */
    public java.lang.String getHazmatMessage() {
        return hazmatMessage;
    }

    /**
     * Sets the hazmatMessage value for this StockCheckResults.
     * 
     * @param hazmatMessage
     */
    public void setHazmatMessage(java.lang.String hazmatMessage) {
        this.hazmatMessage = hazmatMessage;
    }

    /**
     * Gets the retailPrice value for this StockCheckResults.
     * 
     * @return retailPrice
     */
    public java.lang.String getRetailPrice() {
        return retailPrice;
    }

    /**
     * Sets the retailPrice value for this StockCheckResults.
     * 
     * @param retailPrice
     */
    public void setRetailPrice(java.lang.String retailPrice) {
        this.retailPrice = retailPrice;
    }

    /**
     * Gets the retailUom value for this StockCheckResults.
     * 
     * @return retailUom
     */
    public java.lang.String getRetailUom() {
        return retailUom;
    }

    /**
     * Sets the retailUom value for this StockCheckResults.
     * 
     * @param retailUom
     */
    public void setRetailUom(java.lang.String retailUom) {
        this.retailUom = retailUom;
    }

    /**
     * Gets the matrixPrice value for this StockCheckResults.
     * 
     * @return matrixPrice
     */
    public java.lang.String getMatrixPrice() {
        return matrixPrice;
    }

    /**
     * Sets the matrixPrice value for this StockCheckResults.
     * 
     * @param matrixPrice
     */
    public void setMatrixPrice(java.lang.String matrixPrice) {
        this.matrixPrice = matrixPrice;
    }

    /**
     * Gets the matrixUom value for this StockCheckResults.
     * 
     * @return matrixUom
     */
    public java.lang.String getMatrixUom() {
        return matrixUom;
    }

    /**
     * Sets the matrixUom value for this StockCheckResults.
     * 
     * @param matrixUom
     */
    public void setMatrixUom(java.lang.String matrixUom) {
        this.matrixUom = matrixUom;
    }

    /**
     * Gets the currencyCode value for this StockCheckResults.
     * 
     * @return currencyCode
     */
    public java.lang.String getCurrencyCode() {
        return currencyCode;
    }

    /**
     * Sets the currencyCode value for this StockCheckResults.
     * 
     * @param currencyCode
     */
    public void setCurrencyCode(java.lang.String currencyCode) {
        this.currencyCode = currencyCode;
    }

    /**
     * Gets the orderMinimum value for this StockCheckResults.
     * 
     * @return orderMinimum
     */
    public java.lang.String getOrderMinimum() {
        return orderMinimum;
    }

    /**
     * Sets the orderMinimum value for this StockCheckResults.
     * 
     * @param orderMinimum
     */
    public void setOrderMinimum(java.lang.String orderMinimum) {
        this.orderMinimum = orderMinimum;
    }

    /**
     * Gets the upchargeMessage value for this StockCheckResults.
     * 
     * @return upchargeMessage
     */
    public java.lang.String getUpchargeMessage() {
        return upchargeMessage;
    }

    /**
     * Sets the upchargeMessage value for this StockCheckResults.
     * 
     * @param upchargeMessage
     */
    public void setUpchargeMessage(java.lang.String upchargeMessage) {
        this.upchargeMessage = upchargeMessage;
    }

    /**
     * Gets the resultsRows value for this StockCheckResults.
     * 
     * @return resultsRows
     */
    public com.sprich.sprws.sprws.StockCheck_php.StockCheckRow[] getResultsRows() {
        return resultsRows;
    }

    /**
     * Sets the resultsRows value for this StockCheckResults.
     * 
     * @param resultsRows
     */
    public void setResultsRows(com.sprich.sprws.sprws.StockCheck_php.StockCheckRow[] resultsRows) {
        this.resultsRows = resultsRows;
    }

    private java.lang.Object __equalsCalc = null;

    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof StockCheckResults))
            return false;
        StockCheckResults other = (StockCheckResults) obj;
        if (obj == null)
            return false;
        if (this == obj)
            return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true
                && ((this.errorMessage == null && other.getErrorMessage() == null) || (this.errorMessage != null && this.errorMessage
                        .equals(other.getErrorMessage())))
                && ((this.rtnStatus == null && other.getRtnStatus() == null) || (this.rtnStatus != null && this.rtnStatus
                        .equals(other.getRtnStatus())))
                && ((this.rtnMessage == null && other.getRtnMessage() == null) || (this.rtnMessage != null && this.rtnMessage
                        .equals(other.getRtnMessage())))
                && ((this.sprItemNum == null && other.getSprItemNum() == null) || (this.sprItemNum != null && this.sprItemNum
                        .equals(other.getSprItemNum())))
                && ((this.stripNumber == null && other.getStripNumber() == null) || (this.stripNumber != null && this.stripNumber
                        .equals(other.getStripNumber())))
                && ((this.upcNumber == null && other.getUpcNumber() == null) || (this.upcNumber != null && this.upcNumber
                        .equals(other.getUpcNumber())))
                && ((this.oldNumber == null && other.getOldNumber() == null) || (this.oldNumber != null && this.oldNumber
                        .equals(other.getOldNumber())))
                && ((this.itemStatus == null && other.getItemStatus() == null) || (this.itemStatus != null && this.itemStatus
                        .equals(other.getItemStatus())))
                && ((this.productClass == null && other.getProductClass() == null) || (this.productClass != null && this.productClass
                        .equals(other.getProductClass())))
                && ((this.description == null && other.getDescription() == null) || (this.description != null && this.description
                        .equals(other.getDescription())))
                && ((this.sellUom == null && other.getSellUom() == null) || (this.sellUom != null && this.sellUom
                        .equals(other.getSellUom())))
                && ((this.catalogPage == null && other.getCatalogPage() == null) || (this.catalogPage != null && this.catalogPage
                        .equals(other.getCatalogPage())))
                && ((this.matrixPage == null && other.getMatrixPage() == null) || (this.matrixPage != null && this.matrixPage
                        .equals(other.getMatrixPage())))
                && ((this.itemWeight == null && other.getItemWeight() == null) || (this.itemWeight != null && this.itemWeight
                        .equals(other.getItemWeight())))
                && ((this.itemCubes == null && other.getItemCubes() == null) || (this.itemCubes != null && this.itemCubes
                        .equals(other.getItemCubes())))
                && ((this.brokenQtyAllowed == null && other.getBrokenQtyAllowed() == null) || (this.brokenQtyAllowed != null && this.brokenQtyAllowed
                        .equals(other.getBrokenQtyAllowed())))
                && ((this.infoProcessing == null && other.getInfoProcessing() == null) || (this.infoProcessing != null && this.infoProcessing
                        .equals(other.getInfoProcessing())))
                && ((this.readyAssemble == null && other.getReadyAssemble() == null) || (this.readyAssemble != null && this.readyAssemble
                        .equals(other.getReadyAssemble())))
                && ((this.furniture == null && other.getFurniture() == null) || (this.furniture != null && this.furniture
                        .equals(other.getFurniture())))
                && ((this.datedGoods == null && other.getDatedGoods() == null) || (this.datedGoods != null && this.datedGoods
                        .equals(other.getDatedGoods())))
                && ((this.countryOfOrigin == null && other.getCountryOfOrigin() == null) || (this.countryOfOrigin != null && this.countryOfOrigin
                        .equals(other.getCountryOfOrigin())))
                && ((this.serviceSupply == null && other.getServiceSupply() == null) || (this.serviceSupply != null && this.serviceSupply
                        .equals(other.getServiceSupply())))
                && ((this.noReturn == null && other.getNoReturn() == null) || (this.noReturn != null && this.noReturn
                        .equals(other.getNoReturn())))
                && ((this.specialOrder == null && other.getSpecialOrder() == null) || (this.specialOrder != null && this.specialOrder
                        .equals(other.getSpecialOrder())))
                && ((this.upsShippable == null && other.getUpsShippable() == null) || (this.upsShippable != null && this.upsShippable
                        .equals(other.getUpsShippable())))
                && ((this.hazmatMessage == null && other.getHazmatMessage() == null) || (this.hazmatMessage != null && this.hazmatMessage
                        .equals(other.getHazmatMessage())))
                && ((this.retailPrice == null && other.getRetailPrice() == null) || (this.retailPrice != null && this.retailPrice
                        .equals(other.getRetailPrice())))
                && ((this.retailUom == null && other.getRetailUom() == null) || (this.retailUom != null && this.retailUom
                        .equals(other.getRetailUom())))
                && ((this.matrixPrice == null && other.getMatrixPrice() == null) || (this.matrixPrice != null && this.matrixPrice
                        .equals(other.getMatrixPrice())))
                && ((this.matrixUom == null && other.getMatrixUom() == null) || (this.matrixUom != null && this.matrixUom
                        .equals(other.getMatrixUom())))
                && ((this.currencyCode == null && other.getCurrencyCode() == null) || (this.currencyCode != null && this.currencyCode
                        .equals(other.getCurrencyCode())))
                && ((this.orderMinimum == null && other.getOrderMinimum() == null) || (this.orderMinimum != null && this.orderMinimum
                        .equals(other.getOrderMinimum())))
                && ((this.upchargeMessage == null && other.getUpchargeMessage() == null) || (this.upchargeMessage != null && this.upchargeMessage
                        .equals(other.getUpchargeMessage())))
                && ((this.resultsRows == null && other.getResultsRows() == null) || (this.resultsRows != null && java.util.Arrays
                        .equals(this.resultsRows, other.getResultsRows())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;

    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getErrorMessage() != null) {
            _hashCode += getErrorMessage().hashCode();
        }
        if (getRtnStatus() != null) {
            _hashCode += getRtnStatus().hashCode();
        }
        if (getRtnMessage() != null) {
            _hashCode += getRtnMessage().hashCode();
        }
        if (getSprItemNum() != null) {
            _hashCode += getSprItemNum().hashCode();
        }
        if (getStripNumber() != null) {
            _hashCode += getStripNumber().hashCode();
        }
        if (getUpcNumber() != null) {
            _hashCode += getUpcNumber().hashCode();
        }
        if (getOldNumber() != null) {
            _hashCode += getOldNumber().hashCode();
        }
        if (getItemStatus() != null) {
            _hashCode += getItemStatus().hashCode();
        }
        if (getProductClass() != null) {
            _hashCode += getProductClass().hashCode();
        }
        if (getDescription() != null) {
            _hashCode += getDescription().hashCode();
        }
        if (getSellUom() != null) {
            _hashCode += getSellUom().hashCode();
        }
        if (getCatalogPage() != null) {
            _hashCode += getCatalogPage().hashCode();
        }
        if (getMatrixPage() != null) {
            _hashCode += getMatrixPage().hashCode();
        }
        if (getItemWeight() != null) {
            _hashCode += getItemWeight().hashCode();
        }
        if (getItemCubes() != null) {
            _hashCode += getItemCubes().hashCode();
        }
        if (getBrokenQtyAllowed() != null) {
            _hashCode += getBrokenQtyAllowed().hashCode();
        }
        if (getInfoProcessing() != null) {
            _hashCode += getInfoProcessing().hashCode();
        }
        if (getReadyAssemble() != null) {
            _hashCode += getReadyAssemble().hashCode();
        }
        if (getFurniture() != null) {
            _hashCode += getFurniture().hashCode();
        }
        if (getDatedGoods() != null) {
            _hashCode += getDatedGoods().hashCode();
        }
        if (getCountryOfOrigin() != null) {
            _hashCode += getCountryOfOrigin().hashCode();
        }
        if (getServiceSupply() != null) {
            _hashCode += getServiceSupply().hashCode();
        }
        if (getNoReturn() != null) {
            _hashCode += getNoReturn().hashCode();
        }
        if (getSpecialOrder() != null) {
            _hashCode += getSpecialOrder().hashCode();
        }
        if (getUpsShippable() != null) {
            _hashCode += getUpsShippable().hashCode();
        }
        if (getHazmatMessage() != null) {
            _hashCode += getHazmatMessage().hashCode();
        }
        if (getRetailPrice() != null) {
            _hashCode += getRetailPrice().hashCode();
        }
        if (getRetailUom() != null) {
            _hashCode += getRetailUom().hashCode();
        }
        if (getMatrixPrice() != null) {
            _hashCode += getMatrixPrice().hashCode();
        }
        if (getMatrixUom() != null) {
            _hashCode += getMatrixUom().hashCode();
        }
        if (getCurrencyCode() != null) {
            _hashCode += getCurrencyCode().hashCode();
        }
        if (getOrderMinimum() != null) {
            _hashCode += getOrderMinimum().hashCode();
        }
        if (getUpchargeMessage() != null) {
            _hashCode += getUpchargeMessage().hashCode();
        }
        if (getResultsRows() != null) {
            for (int i = 0; i < java.lang.reflect.Array.getLength(getResultsRows()); i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getResultsRows(), i);
                if (obj != null && !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc = new org.apache.axis.description.TypeDesc(
            StockCheckResults.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://sprws.sprich.com/sprws/StockCheck.php?wsdl",
                "StockCheckResults"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("errorMessage");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ErrorMessage"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("rtnStatus");
        elemField.setXmlName(new javax.xml.namespace.QName("", "RtnStatus"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("rtnMessage");
        elemField.setXmlName(new javax.xml.namespace.QName("", "RtnMessage"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sprItemNum");
        elemField.setXmlName(new javax.xml.namespace.QName("", "SprItemNum"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("stripNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("", "StripNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("upcNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("", "UpcNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("oldNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("", "OldNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("itemStatus");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ItemStatus"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("productClass");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ProductClass"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("description");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Description"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sellUom");
        elemField.setXmlName(new javax.xml.namespace.QName("", "SellUom"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("catalogPage");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CatalogPage"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("matrixPage");
        elemField.setXmlName(new javax.xml.namespace.QName("", "MatrixPage"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("itemWeight");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ItemWeight"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("itemCubes");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ItemCubes"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("brokenQtyAllowed");
        elemField.setXmlName(new javax.xml.namespace.QName("", "BrokenQtyAllowed"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("infoProcessing");
        elemField.setXmlName(new javax.xml.namespace.QName("", "InfoProcessing"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("readyAssemble");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ReadyAssemble"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("furniture");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Furniture"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datedGoods");
        elemField.setXmlName(new javax.xml.namespace.QName("", "DatedGoods"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("countryOfOrigin");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CountryOfOrigin"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("serviceSupply");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ServiceSupply"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("noReturn");
        elemField.setXmlName(new javax.xml.namespace.QName("", "NoReturn"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("specialOrder");
        elemField.setXmlName(new javax.xml.namespace.QName("", "SpecialOrder"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("upsShippable");
        elemField.setXmlName(new javax.xml.namespace.QName("", "UpsShippable"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("hazmatMessage");
        elemField.setXmlName(new javax.xml.namespace.QName("", "HazmatMessage"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("retailPrice");
        elemField.setXmlName(new javax.xml.namespace.QName("", "RetailPrice"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("retailUom");
        elemField.setXmlName(new javax.xml.namespace.QName("", "RetailUom"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("matrixPrice");
        elemField.setXmlName(new javax.xml.namespace.QName("", "MatrixPrice"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("matrixUom");
        elemField.setXmlName(new javax.xml.namespace.QName("", "MatrixUom"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("currencyCode");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CurrencyCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("orderMinimum");
        elemField.setXmlName(new javax.xml.namespace.QName("", "OrderMinimum"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("upchargeMessage");
        elemField.setXmlName(new javax.xml.namespace.QName("", "UpchargeMessage"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("resultsRows");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ResultsRows"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://sprws.sprich.com/sprws/StockCheck.php?wsdl",
                "StockCheckRow"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(java.lang.String mechType,
            java.lang.Class _javaType, javax.xml.namespace.QName _xmlType) {
        return new org.apache.axis.encoding.ser.BeanSerializer(_javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(java.lang.String mechType,
            java.lang.Class _javaType, javax.xml.namespace.QName _xmlType) {
        return new org.apache.axis.encoding.ser.BeanDeserializer(_javaType, _xmlType, typeDesc);
    }

}
