/**
 * StockCheck.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.sprich.sprws.sprws.StockCheck_php;

public interface StockCheck extends javax.xml.rpc.Service {

    public java.lang.String getStockCheckPortAddress();

    public com.sprich.sprws.sprws.StockCheck_php.StockCheckPortType getStockCheckPort()
            throws javax.xml.rpc.ServiceException;

    public com.sprich.sprws.sprws.StockCheck_php.StockCheckPortType getStockCheckPort(java.net.URL portAddress)
            throws javax.xml.rpc.ServiceException;
}
