/**
 * StockCheckInputs.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.sprich.sprws.sprws.StockCheck_php;

public class StockCheckInputs implements java.io.Serializable {

    private java.lang.String groupCode;

    private java.lang.String userID;

    private java.lang.String password;

    private java.lang.String action;

    private java.lang.String custNumber;

    private java.lang.String dcNumber;

    private java.lang.String itemNumber;

    private java.lang.String sortBy;

    private java.lang.String minInFullPacks;

    private java.lang.String availableOnly;

    public StockCheckInputs() {
    }

    public StockCheckInputs(java.lang.String groupCode, java.lang.String userID, java.lang.String password,
            java.lang.String action, java.lang.String custNumber, java.lang.String dcNumber,
            java.lang.String itemNumber, java.lang.String sortBy, java.lang.String minInFullPacks,
            java.lang.String availableOnly) {
        this.groupCode = groupCode;
        this.userID = userID;
        this.password = password;
        this.action = action;
        this.custNumber = custNumber;
        this.dcNumber = dcNumber;
        this.itemNumber = itemNumber;
        this.sortBy = sortBy;
        this.minInFullPacks = minInFullPacks;
        this.availableOnly = availableOnly;
    }

    /**
     * Gets the groupCode value for this StockCheckInputs.
     * 
     * @return groupCode
     */
    public java.lang.String getGroupCode() {
        return groupCode;
    }

    /**
     * Sets the groupCode value for this StockCheckInputs.
     * 
     * @param groupCode
     */
    public void setGroupCode(java.lang.String groupCode) {
        this.groupCode = groupCode;
    }

    /**
     * Gets the userID value for this StockCheckInputs.
     * 
     * @return userID
     */
    public java.lang.String getUserID() {
        return userID;
    }

    /**
     * Sets the userID value for this StockCheckInputs.
     * 
     * @param userID
     */
    public void setUserID(java.lang.String userID) {
        this.userID = userID;
    }

    /**
     * Gets the password value for this StockCheckInputs.
     * 
     * @return password
     */
    public java.lang.String getPassword() {
        return password;
    }

    /**
     * Sets the password value for this StockCheckInputs.
     * 
     * @param password
     */
    public void setPassword(java.lang.String password) {
        this.password = password;
    }

    /**
     * Gets the action value for this StockCheckInputs.
     * 
     * @return action
     */
    public java.lang.String getAction() {
        return action;
    }

    /**
     * Sets the action value for this StockCheckInputs.
     * 
     * @param action
     */
    public void setAction(java.lang.String action) {
        this.action = action;
    }

    /**
     * Gets the custNumber value for this StockCheckInputs.
     * 
     * @return custNumber
     */
    public java.lang.String getCustNumber() {
        return custNumber;
    }

    /**
     * Sets the custNumber value for this StockCheckInputs.
     * 
     * @param custNumber
     */
    public void setCustNumber(java.lang.String custNumber) {
        this.custNumber = custNumber;
    }

    /**
     * Gets the dcNumber value for this StockCheckInputs.
     * 
     * @return dcNumber
     */
    public java.lang.String getDcNumber() {
        return dcNumber;
    }

    /**
     * Sets the dcNumber value for this StockCheckInputs.
     * 
     * @param dcNumber
     */
    public void setDcNumber(java.lang.String dcNumber) {
        this.dcNumber = dcNumber;
    }

    /**
     * Gets the itemNumber value for this StockCheckInputs.
     * 
     * @return itemNumber
     */
    public java.lang.String getItemNumber() {
        return itemNumber;
    }

    /**
     * Sets the itemNumber value for this StockCheckInputs.
     * 
     * @param itemNumber
     */
    public void setItemNumber(java.lang.String itemNumber) {
        this.itemNumber = itemNumber;
    }

    /**
     * Gets the sortBy value for this StockCheckInputs.
     * 
     * @return sortBy
     */
    public java.lang.String getSortBy() {
        return sortBy;
    }

    /**
     * Sets the sortBy value for this StockCheckInputs.
     * 
     * @param sortBy
     */
    public void setSortBy(java.lang.String sortBy) {
        this.sortBy = sortBy;
    }

    /**
     * Gets the minInFullPacks value for this StockCheckInputs.
     * 
     * @return minInFullPacks
     */
    public java.lang.String getMinInFullPacks() {
        return minInFullPacks;
    }

    /**
     * Sets the minInFullPacks value for this StockCheckInputs.
     * 
     * @param minInFullPacks
     */
    public void setMinInFullPacks(java.lang.String minInFullPacks) {
        this.minInFullPacks = minInFullPacks;
    }

    /**
     * Gets the availableOnly value for this StockCheckInputs.
     * 
     * @return availableOnly
     */
    public java.lang.String getAvailableOnly() {
        return availableOnly;
    }

    /**
     * Sets the availableOnly value for this StockCheckInputs.
     * 
     * @param availableOnly
     */
    public void setAvailableOnly(java.lang.String availableOnly) {
        this.availableOnly = availableOnly;
    }

    private java.lang.Object __equalsCalc = null;

    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof StockCheckInputs))
            return false;
        StockCheckInputs other = (StockCheckInputs) obj;
        if (obj == null)
            return false;
        if (this == obj)
            return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true
                && ((this.groupCode == null && other.getGroupCode() == null) || (this.groupCode != null && this.groupCode
                        .equals(other.getGroupCode())))
                && ((this.userID == null && other.getUserID() == null) || (this.userID != null && this.userID
                        .equals(other.getUserID())))
                && ((this.password == null && other.getPassword() == null) || (this.password != null && this.password
                        .equals(other.getPassword())))
                && ((this.action == null && other.getAction() == null) || (this.action != null && this.action
                        .equals(other.getAction())))
                && ((this.custNumber == null && other.getCustNumber() == null) || (this.custNumber != null && this.custNumber
                        .equals(other.getCustNumber())))
                && ((this.dcNumber == null && other.getDcNumber() == null) || (this.dcNumber != null && this.dcNumber
                        .equals(other.getDcNumber())))
                && ((this.itemNumber == null && other.getItemNumber() == null) || (this.itemNumber != null && this.itemNumber
                        .equals(other.getItemNumber())))
                && ((this.sortBy == null && other.getSortBy() == null) || (this.sortBy != null && this.sortBy
                        .equals(other.getSortBy())))
                && ((this.minInFullPacks == null && other.getMinInFullPacks() == null) || (this.minInFullPacks != null && this.minInFullPacks
                        .equals(other.getMinInFullPacks())))
                && ((this.availableOnly == null && other.getAvailableOnly() == null) || (this.availableOnly != null && this.availableOnly
                        .equals(other.getAvailableOnly())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;

    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getGroupCode() != null) {
            _hashCode += getGroupCode().hashCode();
        }
        if (getUserID() != null) {
            _hashCode += getUserID().hashCode();
        }
        if (getPassword() != null) {
            _hashCode += getPassword().hashCode();
        }
        if (getAction() != null) {
            _hashCode += getAction().hashCode();
        }
        if (getCustNumber() != null) {
            _hashCode += getCustNumber().hashCode();
        }
        if (getDcNumber() != null) {
            _hashCode += getDcNumber().hashCode();
        }
        if (getItemNumber() != null) {
            _hashCode += getItemNumber().hashCode();
        }
        if (getSortBy() != null) {
            _hashCode += getSortBy().hashCode();
        }
        if (getMinInFullPacks() != null) {
            _hashCode += getMinInFullPacks().hashCode();
        }
        if (getAvailableOnly() != null) {
            _hashCode += getAvailableOnly().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc = new org.apache.axis.description.TypeDesc(
            StockCheckInputs.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://sprws.sprich.com/sprws/StockCheck.php?wsdl",
                "StockCheckInputs"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("groupCode");
        elemField.setXmlName(new javax.xml.namespace.QName("", "GroupCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("userID");
        elemField.setXmlName(new javax.xml.namespace.QName("", "UserID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("password");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Password"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("action");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Action"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("custNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CustNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dcNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("", "DcNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("itemNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ItemNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sortBy");
        elemField.setXmlName(new javax.xml.namespace.QName("", "SortBy"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("minInFullPacks");
        elemField.setXmlName(new javax.xml.namespace.QName("", "MinInFullPacks"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("availableOnly");
        elemField.setXmlName(new javax.xml.namespace.QName("", "AvailableOnly"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(java.lang.String mechType,
            java.lang.Class _javaType, javax.xml.namespace.QName _xmlType) {
        return new org.apache.axis.encoding.ser.BeanSerializer(_javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(java.lang.String mechType,
            java.lang.Class _javaType, javax.xml.namespace.QName _xmlType) {
        return new org.apache.axis.encoding.ser.BeanDeserializer(_javaType, _xmlType, typeDesc);
    }

}
