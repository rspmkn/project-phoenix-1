/**
 * StockCheckPortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.sprich.sprws.sprws.StockCheck_php;

public interface StockCheckPortType extends java.rmi.Remote {

    /**
     * Using Stock Check Service allows insuring an item is in stock
     * before the customer
     * 		places the item in their shopping cart.  The system can suggest
     * a a comparable item
     * 		if ithe item is not in stock to avoid losing the sale and to keep
     * your customer
     * 		satisfied.  This service provides item inventory visibility in all
     * 39 of the S.P.Richards
     * 		Distribution Centers.  If the Item is out of stock, the Item can
     * be placed on backorder,
     * 		notifying the customer of the arrival due date
     */
    public com.sprich.sprws.sprws.StockCheck_php.StockCheckResults stockCheck(
            com.sprich.sprws.sprws.StockCheck_php.StockCheckInputs input) throws java.rmi.RemoteException;
}
