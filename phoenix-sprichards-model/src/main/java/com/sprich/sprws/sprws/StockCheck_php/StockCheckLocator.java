/**
 * StockCheckLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.sprich.sprws.sprws.StockCheck_php;

public class StockCheckLocator extends org.apache.axis.client.Service implements
        com.sprich.sprws.sprws.StockCheck_php.StockCheck {

    public StockCheckLocator() {
    }

    public StockCheckLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public StockCheckLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName)
            throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for StockCheckPort
    private java.lang.String StockCheckPort_address = "http://sprws.sprich.com/sprws/StockCheck.php";

    public java.lang.String getStockCheckPortAddress() {
        return StockCheckPort_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String StockCheckPortWSDDServiceName = "StockCheckPort";

    public java.lang.String getStockCheckPortWSDDServiceName() {
        return StockCheckPortWSDDServiceName;
    }

    public void setStockCheckPortWSDDServiceName(java.lang.String name) {
        StockCheckPortWSDDServiceName = name;
    }

    public com.sprich.sprws.sprws.StockCheck_php.StockCheckPortType getStockCheckPort()
            throws javax.xml.rpc.ServiceException {
        java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(StockCheckPort_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getStockCheckPort(endpoint);
    }

    public com.sprich.sprws.sprws.StockCheck_php.StockCheckPortType getStockCheckPort(java.net.URL portAddress)
            throws javax.xml.rpc.ServiceException {
        try {
            com.sprich.sprws.sprws.StockCheck_php.StockCheckBindingStub _stub = new com.sprich.sprws.sprws.StockCheck_php.StockCheckBindingStub(
                    portAddress, this);
            _stub.setPortName(getStockCheckPortWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setStockCheckPortEndpointAddress(java.lang.String address) {
        StockCheckPort_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (com.sprich.sprws.sprws.StockCheck_php.StockCheckPortType.class
                    .isAssignableFrom(serviceEndpointInterface)) {
                com.sprich.sprws.sprws.StockCheck_php.StockCheckBindingStub _stub = new com.sprich.sprws.sprws.StockCheck_php.StockCheckBindingStub(
                        new java.net.URL(StockCheckPort_address), this);
                _stub.setPortName(getStockCheckPortWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  "
                + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface)
            throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("StockCheckPort".equals(inputPortName)) {
            return getStockCheckPort();
        }
        else {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://sprws.sprich.com/sprws/StockCheck.php?wsdl", "StockCheck");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://sprws.sprich.com/sprws/StockCheck.php?wsdl",
                    "StockCheckPort"));
        }
        return ports.iterator();
    }

    /**
     * Set the endpoint address for the specified port name.
     */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address)
            throws javax.xml.rpc.ServiceException {

        if ("StockCheckPort".equals(portName)) {
            setStockCheckPortEndpointAddress(address);
        }
        else { // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
     * Set the endpoint address for the specified port name.
     */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address)
            throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
