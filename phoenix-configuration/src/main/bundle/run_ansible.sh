#!/bin/sh
export ANSIBLE_PLAYBOOK_TAGS=$1
export VERSION=$2
export BUCKET=$3
export ENV=$4

. /root/.bash_profile
INTERNAL_IP=$(ifconfig  | grep 'inet addr:'| grep -v '127.0.0.1' | cut -d: -f2 | awk '{ print $1}')
# Download and extract the latest playbook from S3
eval "ansible all -i "localhost," -m s3 -a 'bucket=apd-deployment-artifacts object=/ansible-playbooks/ansible-deploy-$VERSION.tar.gz dest=/tmp/ansible-deploy.tar.gz mode=get overwrite=true' -c local"
rm -r -f /tmp/deploy
tar xzvf /tmp/ansible-deploy.tar.gz -C /tmp

export ENVFILE="/tmp/deploy/group_vars/"$ENV"_vars.yml"
echo "$ENVFILE"

chmod +x /tmp/deploy/hosts/ec2.py

if [ -f "$ENVFILE" ]; then
      /bin/cp -f /tmp/deploy/group_vars/"$ENV"_vars.yml /var/env_vars.yml
      echo "found ENV VARS locally"
else
      # If the environment variables are stored in S3, use them instead
      eval "ansible all -i "localhost," -m s3 -a 'bucket=$BUCKET-$ENV-configs object=/env_vars.yml dest=/var/env_vars.yml mode=get overwrite=true' -c local"
fi

export TENANTFILE="/tmp/deploy/group_vars/"$ENV"_tenant_vars.yml"
echo "$TENANTFILE"

if [ -f "$TENANTFILE" ]; then
      echo "Tenant vars found locally for ENV $ENV"
      /bin/cp -f /tmp/deploy/group_vars/"$ENV"_tenant_vars.yml /tmp/deploy/group_vars/tenant_vars.yml
else
	  echo "Pulling tenant vars from s3"
	  eval "ansible all -i "localhost," -m s3 -a 'bucket=$BUCKET-$ENV-configs object=/tenant_vars.yml dest=/tmp/deploy/group_vars/tenant_vars.yml mode=get overwrite=true' -c local"
fi

# Finally run the playbook
cd /tmp/deploy
eval "ansible-playbook -i hosts ./site.yml --limit=$INTERNAL_IP -c local -e '@/var/env_vars.yml' --tags='$ANSIBLE_PLAYBOOK_TAGS'"

