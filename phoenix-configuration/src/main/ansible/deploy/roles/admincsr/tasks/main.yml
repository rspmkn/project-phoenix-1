---
# Tasks to deploy and configure the admin application, csr application asynchronous workflow/rules execution
- name: Grab tenants information
  include_vars: group_vars/tenant_vars.yml  
  tags:
    - properties
    - phoenix-deployment
    - admin-deployment
    - csr-deployment

# Properties file deployment
- name: deploy properties files
  template: 
    src: "templates/{{ item }}.j2" 
    dest: "{{ phoenix_conf }}/{{ item }}"
  with_items: 
    - amq.properties
    - aws.integration.properties
    - cache.properties
    - camel.edi-inbound.integration.properties
    - camel.edi-outbound.integration.properties
    - camel.item.integration.properties
    - catalog.upload.integration.properties
    - ecommerce.properties
    - email.override.properties
    - guvnor.properties
    - generic.properties
    - integration.properties
    - jumptrack.integration.properties
    - paymentGateway.properties
    - project.state.properties
    - remote.task.properties
    - report.service.properties
    - smart.search.properties
    - usps.properties
    - walmart-kits.integration.properties
    - capital.integration.properties
  notify: restart jboss
  tags:
    - properties
    - phoenix-deployment
    - admin-deployment
    - csr-deployment

- name: make tenant configuration subdirectories
  file:
    path: "{{ phoenix_conf }}{{ item.name }}"
    state: directory
  with_items: "{{ tenants }}"
  tags:
    - properties
    - phoenix-deployment
    - admin-deployment
    - csr-deployment

- name: create tenant-specific properties files for each tenant
  template:
    src: "templates/{{ item[1] }}.j2"
    dest: "{{ phoenix_conf }}{{ item[0].name }}/{{ item[1] }}"
  with_nested: 
    - "{{ tenants }}"
    - [ 'tenant.properties', 'ussco.integration.properties', 'sprichards.integration.properties' ]
  notify: restart jboss
  tags:
    - properties
    - phoenix-deployment
    - admin-deployment
    - csr-deployment

# Admin app deployment

# Download admin distribution file from s3
- name: download admin app from s3
  s3: 
    bucket: "{{ deployment_artifacts_bucket }}" 
    object: "/eap-deployment-artifacts/phoenix-administration-{{ admin_war_version }}.war" 
    dest: "/tmp/phoenix-administration-{{ admin_war_version }}.war" 
    mode: get 
    overwrite: true
  changed_when: false
  tags:
    - admin-deployment
    - phoenix-deployment

## To maintain idempotency we won't remove the existing deployment unless theirs a newer version to deploy, therefore we need to capture the name of the file that's currently deployed
- name: Grab existing admin deployment
  shell: ls -1 {{ jboss_home }}/standalone/deployments/phoenix-administration-*
  register: existing_admin
  ignore_errors: true
  changed_when: false
  tags:
    - admin-deployment
    - phoenix-deployment

- name: Copy admin war to deployments folder
  copy: 
    src: "/tmp/phoenix-administration-{{ admin_war_version }}.war"
    dest: "{{ jboss_home }}/standalone/deployments/phoenix-administration-{{ admin_war_version }}.war"
  register: admin_copy_result
  tags:
    - admin-deployment
    - phoenix-deployment
  notify: restart jboss

## This only executes if the copy task ran, if it did, we need to clean up the older version of the artifact using the output from the "Grab existing * deployment" task above
- name: Remove any currently deployed phoenix admin applications
  file: 
     path: "{{ item }}"
     state: absent
  when: admin_copy_result.changed
  with_items: existing_admin.stdout_lines
  tags:
    - admin-deployment
    - phoenix-deployment
    
# CSR App Deployment

# Download csr distribution file from s3
- name: down csr app from s3
  s3: 
    bucket: "{{ deployment_artifacts_bucket }}"
    object: /eap-deployment-artifacts/phoenix-customerservice-{{ csr_war_version }}.war 
    dest: /tmp/phoenix-customerservice-{{ csr_war_version }}.war 
    mode: get 
    overwrite: true
  changed_when: false
  tags:
    - csr-deployment
    - phoenix-deployment

## To maintain idempotency we won't remove the existing deployment unless theirs a newer version to deploy, therefore we need to capture the name of the file that's currently deployed
- name: Grab existing customer service deployment
  shell: ls -1 {{ jboss_home }}/standalone/deployments/phoenix-customerservice-*
  register: existing_csr
  ignore_errors: true
  changed_when: false
  tags:
    - csr-deployment
    - phoenix-deployment

## Copy will only run if the file has changed (i.e. there's a newer version to deploy)
- name: Copy CSR war to deployments folder
  copy: 
    src: "/tmp/phoenix-customerservice-{{ csr_war_version }}.war"
    dest: "{{ jboss_home }}/standalone/deployments/phoenix-customerservice-{{ csr_war_version }}.war"
  register: csr_copy_result
  tags:
    - csr-deployment
    - phoenix-deployment
  notify: restart jboss

## This only executes if the copy task ran, if it did, we need to clean up the older version of the artifact using the output from the "Grab existing * deployment" task above
- name: Remove any currently deployed phoenix CSR applications
  file: 
     path: "{{ item }}"
     state: absent
  when: csr_copy_result.changed
  with_items: existing_csr.stdout_lines
  tags:
    - csr-deployment
    - phoenix-deployment

- name: Execute the user data script that will configure JBoss EAP and deploy the Ecommerce and Batch Service jboss-ec2-applications
  template: 
    src: scripts/user_data.j2
    dest: /usr/share/jboss-ec2-eap/scripts/user_data.sh 
    group: root
    owner: root 
    mode: 0755
  notify: run user script
