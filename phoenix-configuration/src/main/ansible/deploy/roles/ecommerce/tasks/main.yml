---
# Tasks to deploy and configure the ecommerce application and asynchronous workflow/rules execution
- name: Grab tenants information
  include_vars: group_vars/tenant_vars.yml  
  tags:
    - properties
    - phoenix-deployment
    - ecommerce-deployment
    - integration-ws-deployment

# Create properties files from templates
- name: deploy properties files
  template: 
    src: "templates/{{ item }}.j2" 
    dest: "{{ phoenix_conf }}/{{ item }}"
  with_items: 
    - amq.properties
    - aws.integration.properties
    - brms.integration.properties
    - cache.properties
    - camel.cxml.integration.properties
    - camel.edi-inbound.integration.properties
    - camel.edi-outbound.integration.properties
    - camel.item.integration.properties
    - camel.xcbl.integration.properties
    - catalog.upload.integration.properties
    - ecommerce.properties
    - email.integration.properties
    - email.override.properties
    - ferguson.integration.properties
    - fritolay.integration.properties
    - guvnor.properties
    - hii.integration.properties
    - integration.properties
    - jumptrack.integration.properties
    - marfield.integration.properties
    - marquette.integration.properties
    - ngc.integration.properties
    - novant.integration.properties
    - novant.properties
    - paymentGateway.properties
    - pepsi.integration.properties
    - project.state.properties
    - remote.task.properties
    - report.service.properties
    - smart.search.properties
    - solomon.integration.properties
    - solr.integration.properties
    - solr.properties
    - staples.integration.properties
    - tax.integration.properties
    - themeMap.properties
    - usps.integration.properties
    - usps.properties
    - walmart-dept99.integration.properties
    - walmart-kits.integration.properties
    - xcbl.partner.unique.identifier.properties
    - generic.properties
    - capital.integration.properties
  notify: restart jboss
  tags:
    - properties
    - phoenix-deployment
    - ecommerce-deployment
    - integration-ws-deployment

- name: make tenant configuration subdirectories
  file:
    path: "{{ phoenix_conf }}{{ item.name }}"
    state: directory
  with_items: "{{ tenants }}"
  tags:
    - properties
    - phoenix-deployment
    - ecommerce-deployment
    - integration-ws-deployment

- name: create tenant-specific properties files for each tenant
  template:
    src: "templates/{{ item[1] }}.j2"
    dest: "{{ phoenix_conf }}{{ item[0].name }}/{{ item[1] }}"
  with_nested: 
    - "{{ tenants }}"
    - [ 'tenant.properties', 'ussco.integration.properties', 'sprichards.integration.properties' ]
  notify: restart jboss
  tags:
    - properties
    - phoenix-deployment
    - ecommerce-deployment
    - integration-ws-deployment

# Ecommerce application deployment
- name: download shopping app from s3
  s3: 
    bucket: "{{ deployment_artifacts_bucket }}" 
    object: "/eap-deployment-artifacts/phoenix-shopping-{{ ecommerce_war_version }}.war"
    dest: /tmp/phoenix-shopping-{{ ecommerce_war_version }}.war 
    mode: get
    overwrite: true
  changed_when: false
  tags:
    - ecommerce-deployment
    - phoenix-deployment

## To maintain idempotency we won't remove the existing deployment unless theirs a newer version to deploy, therefore we need to capture the name of the file that's currently deployed
- name: Grab existing shopping deployment
  shell: ls -1 {{ jboss_home }}/standalone/deployments/phoenix-shopping-*
  register: existing_shopping
  ignore_errors: true
  changed_when: false
  tags:
    - ecommerce-deployment
    - phoenix-deployment

## Copy will only run if the file has changed (i.e. there's a newer version to deploy)
- name: deploy ecommerce
  copy:
    src: /tmp/phoenix-shopping-{{ ecommerce_war_version }}.war
    dest: "{{ jboss_home }}/standalone/deployments/phoenix-shopping-{{ecommerce_war_version}}.war"
  register: ecommerce_deployment_result
  notify: restart jboss
  tags:
    - ecommerce-deployment
    - phoenix-deployment

## This only executes if the copy task ran, if it did, we need to clean up the older version of the artifact using the output from the "Grab existing * deployment" task above
- name: remove any older versions of deployed phoenix shopping applications
  file: 
    path: "{{ item }}"
    state: absent
  when: ecommerce_deployment_result.changed
  with_items: existing_shopping.stdout_lines
  tags:
    - ecommerce-deployment
    - phoenix-deployment

# Integration WS deployment
- name: download integration app from s3
  s3: 
    bucket: "{{ deployment_artifacts_bucket }}"
    object: /eap-deployment-artifacts/phoenix-integration-ws-{{ integration_ws_war_version }}.war 
    dest: /tmp/phoenix-integration-ws-{{ integration_ws_war_version }}.war 
    mode: get 
    overwrite: true
  changed_when: false
  tags:
    - integration-ws-deployment
    - phoenix-deployment

## To maintain idempotency we won't remove the existing deployment unless theirs a newer version to deploy, therefore we need to capture the name of the file that's currently deployed
- name: Grab existing integration deployment
  shell: ls -1 {{ jboss_home }}/standalone/deployments/phoenix-integration-ws-*
  register: existing_integration_ws
  ignore_errors: true
  changed_when: false
  tags:
    - integration-ws-deployment
    - phoenix-deployment

## Copy will only run if the file has changed (i.e. there's a newer version to deploy)
- name: deploy integration
  copy:
    src: /tmp/phoenix-integration-ws-{{ integration_ws_war_version }}.war
    dest: "{{ jboss_home }}/standalone/deployments/phoenix-integration-ws-{{ integration_ws_war_version }}.war"
  notify: restart jboss
  register: integration_ws_deployment_result
  tags:
    - integration-ws-deployment
    - phoenix-deployment 

## This only executes if the copy task ran, if it did, we need to clean up the older version of the artifact using the output from the "Grab existing * deployment" task above
- name: remove any older version phoenix integration apps
  file: 
    path: "{{ item }}"
    state: absent
  when: integration_ws_deployment_result.changed
  with_items: existing_integration_ws.stdout_lines
  tags:
    - integration-ws-deployment
    - phoenix-deployment

# Execute the user data script that will configure JBoss EAP and deploy the Ecommerce and Batch Service jboss-ec2-applications
- name: Execute the user data script that will configure JBoss EAP and deploy the Ecommerce and Batch Service jboss-ec2-applications
  template: 
    src: scripts/user_data.j2
    dest: /usr/share/jboss-ec2-eap/scripts/user_data.sh 
    group: root
    owner: root 
    mode: 0755
  notify: run user script
