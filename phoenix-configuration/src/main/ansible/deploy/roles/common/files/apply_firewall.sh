#!/bin/bash
#
# Run iptables against a set of rules from /etc/firewall/rules.d/*
# 
# 1) Ensure iptables service is not running
# 2) Iterate over each line of each file

SERVICE=/sbin/service
IPTABLES=/sbin/iptables
RULES_PATH=/etc/firewall/rules.d


# Stop running iptables service. If the service is not running, then our
# rules and policies will NOT be reset. 
$SERVICE iptables stop

for file in $RULES_PATH/*.rules; do
    cat $file | while read RULE; do
        eval "$IPTABLES $RULE" || echo "*** Failure on file: $file" >&2
    done
done
