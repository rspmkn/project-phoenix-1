#!/bin/bash

. variables_CHANGEME.sh

sqlplus amqbroker/amqbroker@\"${INSTANCE_NAME}.cyortqtf1ptj.us-east-1.rds.amazonaws.com:1521/APDEBIZ\" <<'ENDSQL'

	@amqbroker-export;

ENDSQL

sqlplus guvnor/guvnor@\"${INSTANCE_NAME}.cyortqtf1ptj.us-east-1.rds.amazonaws.com:1521/APDEBIZ\" <<'ENDSQL'

	@guvnor-export;

ENDSQL

sqlplus jbpm/jbpm@\"${INSTANCE_NAME}.cyortqtf1ptj.us-east-1.rds.amazonaws.com:1521/APDEBIZ\" <<'ENDSQL'

	@jbpm-export;

ENDSQL

sqlplus phoenix/phoenix@\"${INSTANCE_NAME}.cyortqtf1ptj.us-east-1.rds.amazonaws.com:1521/APDEBIZ\" <<'ENDSQL'

	@phoenix-export;

ENDSQL

