#!/bin/bash

. variables_CHANGEME.sh

sqlplus APDEBIZMASTER/apdebizmaster@\"${INSTANCE_NAME}.cyortqtf1ptj.us-east-1.rds.amazonaws.com:1521/APDEBIZ\" <<'ENDSQL'

CREATE USER phoenix_view
 IDENTIFIED BY phoenix_view
 DEFAULT TABLESPACE users
 TEMPORARY TABLESPACE temp;

GRANT CREATE SESSION to phoenix_view;

CREATE ROLE phoenix_read;

BEGIN
  FOR x IN (SELECT * FROM dba_tables WHERE owner='PHOENIX')
  LOOP
    EXECUTE IMMEDIATE 'GRANT SELECT ON PHOENIX.' || x.table_name || 
                                  ' TO phoenix_read';
  END LOOP;
END;
/

GRANT phoenix_read TO phoenix_view;

CREATE OR REPLACE TRIGGER view_login_trigger
AFTER logon ON DATABASE WHEN (USER = 'PHOENIX_VIEW')
BEGIN
    execute immediate 'ALTER SESSION SET CURRENT_SCHEMA = PHOENIX';
END;
/

ENDSQL

