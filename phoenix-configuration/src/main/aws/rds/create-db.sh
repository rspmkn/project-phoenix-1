#!/bin/bash

. variables_CHANGEME.sh

rds-create-db-instance ${INSTANCE_NAME} -sg ${SECURITY_GROUP} -au false -c db.m1.xlarge -e oracle-se -v 11.2.0.2.v5 --iops 2000 --license-model bring-your-own-license --multi-az false --db-name APDEBIZ -u APDEBIZMASTER -p apdebizmaster --port 1521 -s 300 -pub false --db-subnet-group-name ${DB_SUBNET_GROUP_NAME}

AVAILABLE=""

while [ -z "${AVAILABLE}" ] ; do
	echo "DB Instance ${INSTANCE_NAME} not yet available"
	AVAILABLE=`rds-describe-db-instances ${INSTANCE_NAME} | grep "DBINSTANCE" | grep "APDEBIZMASTER  available"`
	sleep 10
done

echo "DB Instance ${INSTANCE_NAME} is available"

. ./db-permissions.sh

. ./add-tables.sh

. ./phoenix-view.sh

. ./set-passwords.sh

