--------------------------------------------------------
--  File created - Tuesday-December-09-2014   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Table ACTIVEMQ_ACKS
--------------------------------------------------------

  CREATE TABLE "AMQBROKER"."ACTIVEMQ_ACKS" 
   (	"CONTAINER" VARCHAR2(250 BYTE), 
	"SUB_DEST" VARCHAR2(250 BYTE), 
	"CLIENT_ID" VARCHAR2(250 BYTE), 
	"SUB_NAME" VARCHAR2(250 BYTE), 
	"SELECTOR" VARCHAR2(250 BYTE), 
	"LAST_ACKED_ID" NUMBER, 
	"PRIORITY" NUMBER DEFAULT 5, 
	"XID" BLOB
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS" 
 LOB ("XID") STORE AS BASICFILE (
  TABLESPACE "USERS" ENABLE STORAGE IN ROW CHUNK 8192 RETENTION 
  NOCACHE LOGGING 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)) ;
--------------------------------------------------------
--  DDL for Table ACTIVEMQ_LOCK
--------------------------------------------------------

  CREATE TABLE "AMQBROKER"."ACTIVEMQ_LOCK" 
   (	"ID" NUMBER, 
	"TIME" NUMBER, 
	"BROKER_NAME" VARCHAR2(250 BYTE)
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  DDL for Table ACTIVEMQ_MSGS
--------------------------------------------------------

  CREATE TABLE "AMQBROKER"."ACTIVEMQ_MSGS" 
   (	"ID" NUMBER, 
	"CONTAINER" VARCHAR2(250 BYTE), 
	"MSGID_PROD" VARCHAR2(250 BYTE), 
	"MSGID_SEQ" NUMBER, 
	"EXPIRATION" NUMBER, 
	"MSG" BLOB, 
	"PRIORITY" NUMBER, 
	"XID" BLOB
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS" 
 LOB ("MSG") STORE AS BASICFILE (
  TABLESPACE "USERS" ENABLE STORAGE IN ROW CHUNK 8192 RETENTION 
  NOCACHE LOGGING 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)) 
 LOB ("XID") STORE AS BASICFILE (
  TABLESPACE "USERS" ENABLE STORAGE IN ROW CHUNK 8192 RETENTION 
  NOCACHE LOGGING 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)) ;
--------------------------------------------------------
--  DDL for Table TEST
--------------------------------------------------------

  CREATE TABLE "AMQBROKER"."TEST" 
   (	"COLUMN1" VARCHAR2(20 BYTE)
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS" ;
REM INSERTING into AMQBROKER.ACTIVEMQ_ACKS
SET DEFINE OFF;
REM INSERTING into AMQBROKER.ACTIVEMQ_LOCK
SET DEFINE OFF;
Insert into AMQBROKER.ACTIVEMQ_LOCK (ID,TIME,BROKER_NAME) values (1,null,null);
REM INSERTING into AMQBROKER.ACTIVEMQ_MSGS
SET DEFINE OFF;
REM INSERTING into AMQBROKER.TEST
SET DEFINE OFF;
--------------------------------------------------------
--  DDL for Index SYS_C0016954
--------------------------------------------------------

  CREATE UNIQUE INDEX "AMQBROKER"."SYS_C0016954" ON "AMQBROKER"."ACTIVEMQ_MSGS" ("ID") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  DDL for Index SYS_C0016960
--------------------------------------------------------

  CREATE UNIQUE INDEX "AMQBROKER"."SYS_C0016960" ON "AMQBROKER"."ACTIVEMQ_LOCK" ("ID") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  DDL for Index ACTIVEMQ_MSGS_EIDX
--------------------------------------------------------

  CREATE INDEX "AMQBROKER"."ACTIVEMQ_MSGS_EIDX" ON "AMQBROKER"."ACTIVEMQ_MSGS" ("EXPIRATION") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  DDL for Index ACTIVEMQ_MSGS_MIDX
--------------------------------------------------------

  CREATE INDEX "AMQBROKER"."ACTIVEMQ_MSGS_MIDX" ON "AMQBROKER"."ACTIVEMQ_MSGS" ("MSGID_PROD", "MSGID_SEQ") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  DDL for Index ACTIVEMQ_MSGS_CIDX
--------------------------------------------------------

  CREATE INDEX "AMQBROKER"."ACTIVEMQ_MSGS_CIDX" ON "AMQBROKER"."ACTIVEMQ_MSGS" ("CONTAINER") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  DDL for Index ACTIVEMQ_MSGS_PIDX
--------------------------------------------------------

  CREATE INDEX "AMQBROKER"."ACTIVEMQ_MSGS_PIDX" ON "AMQBROKER"."ACTIVEMQ_MSGS" ("PRIORITY") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  Constraints for Table ACTIVEMQ_ACKS
--------------------------------------------------------

  ALTER TABLE "AMQBROKER"."ACTIVEMQ_ACKS" ADD PRIMARY KEY ("CONTAINER", "CLIENT_ID", "SUB_NAME", "PRIORITY")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS"  ENABLE;
  ALTER TABLE "AMQBROKER"."ACTIVEMQ_ACKS" MODIFY ("PRIORITY" NOT NULL ENABLE);
  ALTER TABLE "AMQBROKER"."ACTIVEMQ_ACKS" MODIFY ("SUB_NAME" NOT NULL ENABLE);
  ALTER TABLE "AMQBROKER"."ACTIVEMQ_ACKS" MODIFY ("CLIENT_ID" NOT NULL ENABLE);
  ALTER TABLE "AMQBROKER"."ACTIVEMQ_ACKS" MODIFY ("CONTAINER" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table ACTIVEMQ_MSGS
--------------------------------------------------------

  ALTER TABLE "AMQBROKER"."ACTIVEMQ_MSGS" ADD CONSTRAINT "SYS_C0016954" PRIMARY KEY ("ID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS"  ENABLE;
  ALTER TABLE "AMQBROKER"."ACTIVEMQ_MSGS" MODIFY ("ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table ACTIVEMQ_LOCK
--------------------------------------------------------

  ALTER TABLE "AMQBROKER"."ACTIVEMQ_LOCK" ADD CONSTRAINT "SYS_C0016960" PRIMARY KEY ("ID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS"  ENABLE;
  ALTER TABLE "AMQBROKER"."ACTIVEMQ_LOCK" MODIFY ("ID" NOT NULL ENABLE);
