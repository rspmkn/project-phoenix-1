package com.apd.phoenix.service.integration.transaction.api;

import java.io.Serializable;

public class CxmlTransaction implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -4460046476009294018L;

    private Object header;

    private Object request;

    public Object getHeader() {
        return header;
    }

    public void setHeader(Object header) {
        this.header = header;
    }

    public Object getRequest() {
        return request;
    }

    public void setRequest(Object request) {
        this.request = request;
    }

}
