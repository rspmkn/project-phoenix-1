//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.5-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2014.08.13 at 10:13:13 PM EDT 
//


package com.perfect.liteenvelope;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for OSNBusinessDocumentTypeCode.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="OSNBusinessDocumentTypeCode">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}NMTOKEN">
 *     &lt;enumeration value="Invoice"/>
 *     &lt;enumeration value="InvoiceResponse"/>
 *     &lt;enumeration value="MessageAcknowledgement"/>
 *     &lt;enumeration value="Order"/>
 *     &lt;enumeration value="ChangeOrder"/>
 *     &lt;enumeration value="ChangeCancellation"/>
 *     &lt;enumeration value="OrderResponse"/>
 *     &lt;enumeration value="OrderStatusResult"/>
 *     &lt;enumeration value="Other"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "OSNBusinessDocumentTypeCode")
@XmlEnum
public enum OSNBusinessDocumentTypeCode {

    @XmlEnumValue("Invoice")
    INVOICE("Invoice"),
    @XmlEnumValue("InvoiceResponse")
    INVOICE_RESPONSE("InvoiceResponse"),
    @XmlEnumValue("MessageAcknowledgement")
    MESSAGE_ACKNOWLEDGEMENT("MessageAcknowledgement"),
    @XmlEnumValue("Order")
    ORDER("Order"),
    @XmlEnumValue("ChangeOrder")
    CHANGE_ORDER("ChangeOrder"),
    @XmlEnumValue("ChangeCancellation")
    CHANGE_CANCELLATION("ChangeCancellation"),
    @XmlEnumValue("OrderResponse")
    ORDER_RESPONSE("OrderResponse"),
    @XmlEnumValue("OrderStatusResult")
    ORDER_STATUS_RESULT("OrderStatusResult"),
    @XmlEnumValue("Other")
    OTHER("Other");
    private final String value;

    OSNBusinessDocumentTypeCode(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static OSNBusinessDocumentTypeCode fromValue(String v) {
        for (OSNBusinessDocumentTypeCode c: OSNBusinessDocumentTypeCode.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
