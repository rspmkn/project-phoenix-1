//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.5-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2014.08.13 at 10:13:13 PM EDT 
//


package rrn.org_xcbl.schemas.xcbl.v3_5.xcbl35;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AvailabilityToPromiseResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AvailabilityToPromiseResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{rrn:org.xcbl:schemas/xcbl/v3_5/xcbl35.xsd}AvailabilityToPromiseResponseHeader"/>
 *         &lt;element ref="{rrn:org.xcbl:schemas/xcbl/v3_5/xcbl35.xsd}AvailabilityToPromiseResponseDetail" minOccurs="0"/>
 *         &lt;element ref="{rrn:org.xcbl:schemas/xcbl/v3_5/xcbl35.xsd}AvailabilityToPromiseResponseSummary" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AvailabilityToPromiseResponse", propOrder = {
    "availabilityToPromiseResponseHeader",
    "availabilityToPromiseResponseDetail",
    "availabilityToPromiseResponseSummary"
})
public class AvailabilityToPromiseResponse
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "AvailabilityToPromiseResponseHeader", required = true)
    protected AvailabilityToPromiseResponseHeader availabilityToPromiseResponseHeader;
    @XmlElement(name = "AvailabilityToPromiseResponseDetail")
    protected AvailabilityToPromiseResponseDetail availabilityToPromiseResponseDetail;
    @XmlElement(name = "AvailabilityToPromiseResponseSummary")
    protected AvailabilityToPromiseResponseSummary availabilityToPromiseResponseSummary;

    /**
     * Gets the value of the availabilityToPromiseResponseHeader property.
     * 
     * @return
     *     possible object is
     *     {@link AvailabilityToPromiseResponseHeader }
     *     
     */
    public AvailabilityToPromiseResponseHeader getAvailabilityToPromiseResponseHeader() {
        return availabilityToPromiseResponseHeader;
    }

    /**
     * Sets the value of the availabilityToPromiseResponseHeader property.
     * 
     * @param value
     *     allowed object is
     *     {@link AvailabilityToPromiseResponseHeader }
     *     
     */
    public void setAvailabilityToPromiseResponseHeader(AvailabilityToPromiseResponseHeader value) {
        this.availabilityToPromiseResponseHeader = value;
    }

    /**
     * Gets the value of the availabilityToPromiseResponseDetail property.
     * 
     * @return
     *     possible object is
     *     {@link AvailabilityToPromiseResponseDetail }
     *     
     */
    public AvailabilityToPromiseResponseDetail getAvailabilityToPromiseResponseDetail() {
        return availabilityToPromiseResponseDetail;
    }

    /**
     * Sets the value of the availabilityToPromiseResponseDetail property.
     * 
     * @param value
     *     allowed object is
     *     {@link AvailabilityToPromiseResponseDetail }
     *     
     */
    public void setAvailabilityToPromiseResponseDetail(AvailabilityToPromiseResponseDetail value) {
        this.availabilityToPromiseResponseDetail = value;
    }

    /**
     * Gets the value of the availabilityToPromiseResponseSummary property.
     * 
     * @return
     *     possible object is
     *     {@link AvailabilityToPromiseResponseSummary }
     *     
     */
    public AvailabilityToPromiseResponseSummary getAvailabilityToPromiseResponseSummary() {
        return availabilityToPromiseResponseSummary;
    }

    /**
     * Sets the value of the availabilityToPromiseResponseSummary property.
     * 
     * @param value
     *     allowed object is
     *     {@link AvailabilityToPromiseResponseSummary }
     *     
     */
    public void setAvailabilityToPromiseResponseSummary(AvailabilityToPromiseResponseSummary value) {
        this.availabilityToPromiseResponseSummary = value;
    }

}
