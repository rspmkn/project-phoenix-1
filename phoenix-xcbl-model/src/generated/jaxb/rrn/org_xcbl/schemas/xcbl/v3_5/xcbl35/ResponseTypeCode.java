//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.5-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2014.08.13 at 10:13:13 PM EDT 
//


package rrn.org_xcbl.schemas.xcbl.v3_5.xcbl35;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ResponseTypeCode.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ResponseTypeCode">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}NMTOKEN">
 *     &lt;enumeration value="Other"/>
 *     &lt;enumeration value="Accepted"/>
 *     &lt;enumeration value="AcceptedContentsRejected"/>
 *     &lt;enumeration value="AcceptedWithAmendment"/>
 *     &lt;enumeration value="AcceptedWithAmendmentInDetailSection"/>
 *     &lt;enumeration value="AcceptedWithAmendmentInHeadingSection"/>
 *     &lt;enumeration value="AcceptedWithAmendmentNoConfirmationIsRequired"/>
 *     &lt;enumeration value="AcceptedWithoutAmendment"/>
 *     &lt;enumeration value="AcceptedWithoutReserves"/>
 *     &lt;enumeration value="AcceptedWithReserves"/>
 *     &lt;enumeration value="AcknowledgeNoDetailOrChange"/>
 *     &lt;enumeration value="AcknowledgeWithDetailAndChange"/>
 *     &lt;enumeration value="AcknowledgeWithDetailNoChange"/>
 *     &lt;enumeration value="AdviceWithDetails"/>
 *     &lt;enumeration value="AdviceWithoutDetails"/>
 *     &lt;enumeration value="Agreed"/>
 *     &lt;enumeration value="AlreadyDelivered"/>
 *     &lt;enumeration value="ApprovedAsAmended"/>
 *     &lt;enumeration value="ApprovedAsSubmitted"/>
 *     &lt;enumeration value="AuctionHeld"/>
 *     &lt;enumeration value="AuthorityDeclined"/>
 *     &lt;enumeration value="AuthorityToDeduct"/>
 *     &lt;enumeration value="BuyerClaimsAgainstInvoice"/>
 *     &lt;enumeration value="ChargeBackToSeller"/>
 *     &lt;enumeration value="Checked"/>
 *     &lt;enumeration value="ConditionallyAccepted"/>
 *     &lt;enumeration value="Countersued"/>
 *     &lt;enumeration value="CourtActionDismissed"/>
 *     &lt;enumeration value="DirectDocumentaryCreditCollection"/>
 *     &lt;enumeration value="FinalResponse"/>
 *     &lt;enumeration value="GroupedCreditAdvices"/>
 *     &lt;enumeration value="GroupedDebitAdvices"/>
 *     &lt;enumeration value="InitialClaimReceived"/>
 *     &lt;enumeration value="InterimResponse"/>
 *     &lt;enumeration value="LegalActionPursued"/>
 *     &lt;enumeration value="MeetingHeld"/>
 *     &lt;enumeration value="NotAccepted"/>
 *     &lt;enumeration value="NotAcceptedProvisional"/>
 *     &lt;enumeration value="NotChecked"/>
 *     &lt;enumeration value="NotInProcess"/>
 *     &lt;enumeration value="NotProcessed"/>
 *     &lt;enumeration value="OriginalConfirmationOfOriginalAnnouncement"/>
 *     &lt;enumeration value="OriginalConfirmationOfRevisedAnnouncement"/>
 *     &lt;enumeration value="PaymentDenied"/>
 *     &lt;enumeration value="Pending"/>
 *     &lt;enumeration value="PendingAwaitingAdditionalMaterial"/>
 *     &lt;enumeration value="PendingAwaitingReview"/>
 *     &lt;enumeration value="PendingIncomplete"/>
 *     &lt;enumeration value="Rejected"/>
 *     &lt;enumeration value="RejectedDuplicate"/>
 *     &lt;enumeration value="RejectedNoDetail"/>
 *     &lt;enumeration value="RejectedNotAsAgreed"/>
 *     &lt;enumeration value="RejectedResubmitWithCorrections"/>
 *     &lt;enumeration value="RejectedViolatesIndustryPractice"/>
 *     &lt;enumeration value="RejectedWithCounterOffer"/>
 *     &lt;enumeration value="RejectItemChange"/>
 *     &lt;enumeration value="RejectWithDetail"/>
 *     &lt;enumeration value="RejectWithExceptionDetailOnly"/>
 *     &lt;enumeration value="ResultDisputed"/>
 *     &lt;enumeration value="ResultOpposed"/>
 *     &lt;enumeration value="ResultSetAside"/>
 *     &lt;enumeration value="SellerRejectsDispute"/>
 *     &lt;enumeration value="SellerWillIssueCreditNote"/>
 *     &lt;enumeration value="Settlement"/>
 *     &lt;enumeration value="SingleCreditItemOfAGroup"/>
 *     &lt;enumeration value="SingleDebitItemOfAGroup"/>
 *     &lt;enumeration value="UnderInvestigation"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "ResponseTypeCode")
@XmlEnum
public enum ResponseTypeCode {

    @XmlEnumValue("Other")
    OTHER("Other"),
    @XmlEnumValue("Accepted")
    ACCEPTED("Accepted"),
    @XmlEnumValue("AcceptedContentsRejected")
    ACCEPTED_CONTENTS_REJECTED("AcceptedContentsRejected"),
    @XmlEnumValue("AcceptedWithAmendment")
    ACCEPTED_WITH_AMENDMENT("AcceptedWithAmendment"),
    @XmlEnumValue("AcceptedWithAmendmentInDetailSection")
    ACCEPTED_WITH_AMENDMENT_IN_DETAIL_SECTION("AcceptedWithAmendmentInDetailSection"),
    @XmlEnumValue("AcceptedWithAmendmentInHeadingSection")
    ACCEPTED_WITH_AMENDMENT_IN_HEADING_SECTION("AcceptedWithAmendmentInHeadingSection"),
    @XmlEnumValue("AcceptedWithAmendmentNoConfirmationIsRequired")
    ACCEPTED_WITH_AMENDMENT_NO_CONFIRMATION_IS_REQUIRED("AcceptedWithAmendmentNoConfirmationIsRequired"),
    @XmlEnumValue("AcceptedWithoutAmendment")
    ACCEPTED_WITHOUT_AMENDMENT("AcceptedWithoutAmendment"),
    @XmlEnumValue("AcceptedWithoutReserves")
    ACCEPTED_WITHOUT_RESERVES("AcceptedWithoutReserves"),
    @XmlEnumValue("AcceptedWithReserves")
    ACCEPTED_WITH_RESERVES("AcceptedWithReserves"),
    @XmlEnumValue("AcknowledgeNoDetailOrChange")
    ACKNOWLEDGE_NO_DETAIL_OR_CHANGE("AcknowledgeNoDetailOrChange"),
    @XmlEnumValue("AcknowledgeWithDetailAndChange")
    ACKNOWLEDGE_WITH_DETAIL_AND_CHANGE("AcknowledgeWithDetailAndChange"),
    @XmlEnumValue("AcknowledgeWithDetailNoChange")
    ACKNOWLEDGE_WITH_DETAIL_NO_CHANGE("AcknowledgeWithDetailNoChange"),
    @XmlEnumValue("AdviceWithDetails")
    ADVICE_WITH_DETAILS("AdviceWithDetails"),
    @XmlEnumValue("AdviceWithoutDetails")
    ADVICE_WITHOUT_DETAILS("AdviceWithoutDetails"),
    @XmlEnumValue("Agreed")
    AGREED("Agreed"),
    @XmlEnumValue("AlreadyDelivered")
    ALREADY_DELIVERED("AlreadyDelivered"),
    @XmlEnumValue("ApprovedAsAmended")
    APPROVED_AS_AMENDED("ApprovedAsAmended"),
    @XmlEnumValue("ApprovedAsSubmitted")
    APPROVED_AS_SUBMITTED("ApprovedAsSubmitted"),
    @XmlEnumValue("AuctionHeld")
    AUCTION_HELD("AuctionHeld"),
    @XmlEnumValue("AuthorityDeclined")
    AUTHORITY_DECLINED("AuthorityDeclined"),
    @XmlEnumValue("AuthorityToDeduct")
    AUTHORITY_TO_DEDUCT("AuthorityToDeduct"),
    @XmlEnumValue("BuyerClaimsAgainstInvoice")
    BUYER_CLAIMS_AGAINST_INVOICE("BuyerClaimsAgainstInvoice"),
    @XmlEnumValue("ChargeBackToSeller")
    CHARGE_BACK_TO_SELLER("ChargeBackToSeller"),
    @XmlEnumValue("Checked")
    CHECKED("Checked"),
    @XmlEnumValue("ConditionallyAccepted")
    CONDITIONALLY_ACCEPTED("ConditionallyAccepted"),
    @XmlEnumValue("Countersued")
    COUNTERSUED("Countersued"),
    @XmlEnumValue("CourtActionDismissed")
    COURT_ACTION_DISMISSED("CourtActionDismissed"),
    @XmlEnumValue("DirectDocumentaryCreditCollection")
    DIRECT_DOCUMENTARY_CREDIT_COLLECTION("DirectDocumentaryCreditCollection"),
    @XmlEnumValue("FinalResponse")
    FINAL_RESPONSE("FinalResponse"),
    @XmlEnumValue("GroupedCreditAdvices")
    GROUPED_CREDIT_ADVICES("GroupedCreditAdvices"),
    @XmlEnumValue("GroupedDebitAdvices")
    GROUPED_DEBIT_ADVICES("GroupedDebitAdvices"),
    @XmlEnumValue("InitialClaimReceived")
    INITIAL_CLAIM_RECEIVED("InitialClaimReceived"),
    @XmlEnumValue("InterimResponse")
    INTERIM_RESPONSE("InterimResponse"),
    @XmlEnumValue("LegalActionPursued")
    LEGAL_ACTION_PURSUED("LegalActionPursued"),
    @XmlEnumValue("MeetingHeld")
    MEETING_HELD("MeetingHeld"),
    @XmlEnumValue("NotAccepted")
    NOT_ACCEPTED("NotAccepted"),
    @XmlEnumValue("NotAcceptedProvisional")
    NOT_ACCEPTED_PROVISIONAL("NotAcceptedProvisional"),
    @XmlEnumValue("NotChecked")
    NOT_CHECKED("NotChecked"),
    @XmlEnumValue("NotInProcess")
    NOT_IN_PROCESS("NotInProcess"),
    @XmlEnumValue("NotProcessed")
    NOT_PROCESSED("NotProcessed"),
    @XmlEnumValue("OriginalConfirmationOfOriginalAnnouncement")
    ORIGINAL_CONFIRMATION_OF_ORIGINAL_ANNOUNCEMENT("OriginalConfirmationOfOriginalAnnouncement"),
    @XmlEnumValue("OriginalConfirmationOfRevisedAnnouncement")
    ORIGINAL_CONFIRMATION_OF_REVISED_ANNOUNCEMENT("OriginalConfirmationOfRevisedAnnouncement"),
    @XmlEnumValue("PaymentDenied")
    PAYMENT_DENIED("PaymentDenied"),
    @XmlEnumValue("Pending")
    PENDING("Pending"),
    @XmlEnumValue("PendingAwaitingAdditionalMaterial")
    PENDING_AWAITING_ADDITIONAL_MATERIAL("PendingAwaitingAdditionalMaterial"),
    @XmlEnumValue("PendingAwaitingReview")
    PENDING_AWAITING_REVIEW("PendingAwaitingReview"),
    @XmlEnumValue("PendingIncomplete")
    PENDING_INCOMPLETE("PendingIncomplete"),
    @XmlEnumValue("Rejected")
    REJECTED("Rejected"),
    @XmlEnumValue("RejectedDuplicate")
    REJECTED_DUPLICATE("RejectedDuplicate"),
    @XmlEnumValue("RejectedNoDetail")
    REJECTED_NO_DETAIL("RejectedNoDetail"),
    @XmlEnumValue("RejectedNotAsAgreed")
    REJECTED_NOT_AS_AGREED("RejectedNotAsAgreed"),
    @XmlEnumValue("RejectedResubmitWithCorrections")
    REJECTED_RESUBMIT_WITH_CORRECTIONS("RejectedResubmitWithCorrections"),
    @XmlEnumValue("RejectedViolatesIndustryPractice")
    REJECTED_VIOLATES_INDUSTRY_PRACTICE("RejectedViolatesIndustryPractice"),
    @XmlEnumValue("RejectedWithCounterOffer")
    REJECTED_WITH_COUNTER_OFFER("RejectedWithCounterOffer"),
    @XmlEnumValue("RejectItemChange")
    REJECT_ITEM_CHANGE("RejectItemChange"),
    @XmlEnumValue("RejectWithDetail")
    REJECT_WITH_DETAIL("RejectWithDetail"),
    @XmlEnumValue("RejectWithExceptionDetailOnly")
    REJECT_WITH_EXCEPTION_DETAIL_ONLY("RejectWithExceptionDetailOnly"),
    @XmlEnumValue("ResultDisputed")
    RESULT_DISPUTED("ResultDisputed"),
    @XmlEnumValue("ResultOpposed")
    RESULT_OPPOSED("ResultOpposed"),
    @XmlEnumValue("ResultSetAside")
    RESULT_SET_ASIDE("ResultSetAside"),
    @XmlEnumValue("SellerRejectsDispute")
    SELLER_REJECTS_DISPUTE("SellerRejectsDispute"),
    @XmlEnumValue("SellerWillIssueCreditNote")
    SELLER_WILL_ISSUE_CREDIT_NOTE("SellerWillIssueCreditNote"),
    @XmlEnumValue("Settlement")
    SETTLEMENT("Settlement"),
    @XmlEnumValue("SingleCreditItemOfAGroup")
    SINGLE_CREDIT_ITEM_OF_A_GROUP("SingleCreditItemOfAGroup"),
    @XmlEnumValue("SingleDebitItemOfAGroup")
    SINGLE_DEBIT_ITEM_OF_A_GROUP("SingleDebitItemOfAGroup"),
    @XmlEnumValue("UnderInvestigation")
    UNDER_INVESTIGATION("UnderInvestigation");
    private final String value;

    ResponseTypeCode(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ResponseTypeCode fromValue(String v) {
        for (ResponseTypeCode c: ResponseTypeCode.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
