//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.5-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2014.08.13 at 10:13:13 PM EDT 
//


package rrn.org_xcbl.schemas.xcbl.v3_5.xcbl35;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CatalogAudience complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CatalogAudience">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;attribute name="CatalogAudienceCoded" type="{rrn:org.xcbl:schemas/xcbl/v3_5/xcbl35.xsd}CatalogAudienceCode" default="Public" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CatalogAudience")
public class CatalogAudience
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    @XmlAttribute(name = "CatalogAudienceCoded")
    protected CatalogAudienceCode catalogAudienceCoded;

    /**
     * Gets the value of the catalogAudienceCoded property.
     * 
     * @return
     *     possible object is
     *     {@link CatalogAudienceCode }
     *     
     */
    public CatalogAudienceCode getCatalogAudienceCoded() {
        if (catalogAudienceCoded == null) {
            return CatalogAudienceCode.PUBLIC;
        } else {
            return catalogAudienceCoded;
        }
    }

    /**
     * Sets the value of the catalogAudienceCoded property.
     * 
     * @param value
     *     allowed object is
     *     {@link CatalogAudienceCode }
     *     
     */
    public void setCatalogAudienceCoded(CatalogAudienceCode value) {
        this.catalogAudienceCoded = value;
    }

}
