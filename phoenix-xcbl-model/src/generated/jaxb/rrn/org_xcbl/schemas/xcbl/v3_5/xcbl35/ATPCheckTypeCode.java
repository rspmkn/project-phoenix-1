//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.5-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2014.08.13 at 10:13:13 PM EDT 
//


package rrn.org_xcbl.schemas.xcbl.v3_5.xcbl35;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ATPCheckTypeCode.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ATPCheckTypeCode">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}NMTOKEN">
 *     &lt;enumeration value="Other"/>
 *     &lt;enumeration value="NotSpecified"/>
 *     &lt;enumeration value="ConfirmQuantityAndDate"/>
 *     &lt;enumeration value="ConfirmAvailableQuantityForRequestedDate"/>
 *     &lt;enumeration value="ConfirmAvailableDateForRequestedQuantity"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "ATPCheckTypeCode")
@XmlEnum
public enum ATPCheckTypeCode {

    @XmlEnumValue("Other")
    OTHER("Other"),
    @XmlEnumValue("NotSpecified")
    NOT_SPECIFIED("NotSpecified"),
    @XmlEnumValue("ConfirmQuantityAndDate")
    CONFIRM_QUANTITY_AND_DATE("ConfirmQuantityAndDate"),
    @XmlEnumValue("ConfirmAvailableQuantityForRequestedDate")
    CONFIRM_AVAILABLE_QUANTITY_FOR_REQUESTED_DATE("ConfirmAvailableQuantityForRequestedDate"),
    @XmlEnumValue("ConfirmAvailableDateForRequestedQuantity")
    CONFIRM_AVAILABLE_DATE_FOR_REQUESTED_QUANTITY("ConfirmAvailableDateForRequestedQuantity");
    private final String value;

    ATPCheckTypeCode(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ATPCheckTypeCode fromValue(String v) {
        for (ATPCheckTypeCode c: ATPCheckTypeCode.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
