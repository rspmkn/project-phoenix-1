//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.5-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2014.08.13 at 10:13:13 PM EDT 
//


package rrn.org_xcbl.schemas.xcbl.v3_5.xcbl35;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SourcingResultResponseCode.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="SourcingResultResponseCode">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}NMTOKEN">
 *     &lt;enumeration value="Other"/>
 *     &lt;enumeration value="Accepted"/>
 *     &lt;enumeration value="RejectedResend"/>
 *     &lt;enumeration value="RejectedDoNotResend"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "SourcingResultResponseCode")
@XmlEnum
public enum SourcingResultResponseCode {

    @XmlEnumValue("Other")
    OTHER("Other"),
    @XmlEnumValue("Accepted")
    ACCEPTED("Accepted"),
    @XmlEnumValue("RejectedResend")
    REJECTED_RESEND("RejectedResend"),
    @XmlEnumValue("RejectedDoNotResend")
    REJECTED_DO_NOT_RESEND("RejectedDoNotResend");
    private final String value;

    SourcingResultResponseCode(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static SourcingResultResponseCode fromValue(String v) {
        for (SourcingResultResponseCode c: SourcingResultResponseCode.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
