//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.5-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2014.08.13 at 10:13:13 PM EDT 
//


package rrn.org_xcbl.schemas.xcbl.v3_5.xcbl35;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for UserStatus complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="UserStatus">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="UserStatusCoded" type="{rrn:org.xcbl:schemas/xcbl/v3_5/xcbl35.xsd}UserStatusCode"/>
 *         &lt;element name="UserStatusCodedOther" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserStatus", propOrder = {
    "userStatusCoded",
    "userStatusCodedOther"
})
public class UserStatus
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "UserStatusCoded", required = true)
    protected UserStatusCode userStatusCoded;
    @XmlElement(name = "UserStatusCodedOther")
    protected String userStatusCodedOther;

    /**
     * Gets the value of the userStatusCoded property.
     * 
     * @return
     *     possible object is
     *     {@link UserStatusCode }
     *     
     */
    public UserStatusCode getUserStatusCoded() {
        return userStatusCoded;
    }

    /**
     * Sets the value of the userStatusCoded property.
     * 
     * @param value
     *     allowed object is
     *     {@link UserStatusCode }
     *     
     */
    public void setUserStatusCoded(UserStatusCode value) {
        this.userStatusCoded = value;
    }

    /**
     * Gets the value of the userStatusCodedOther property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserStatusCodedOther() {
        return userStatusCodedOther;
    }

    /**
     * Sets the value of the userStatusCodedOther property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserStatusCodedOther(String value) {
        this.userStatusCodedOther = value;
    }

}
