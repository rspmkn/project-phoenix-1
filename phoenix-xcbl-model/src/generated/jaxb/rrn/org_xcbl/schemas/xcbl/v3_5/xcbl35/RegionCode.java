//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.5-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2014.08.13 at 10:13:13 PM EDT 
//


package rrn.org_xcbl.schemas.xcbl.v3_5.xcbl35;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RegionCode.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="RegionCode">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}NMTOKEN">
 *     &lt;enumeration value="Other"/>
 *     &lt;enumeration value="AUACT"/>
 *     &lt;enumeration value="AUNSW"/>
 *     &lt;enumeration value="AUNT"/>
 *     &lt;enumeration value="AUQLD"/>
 *     &lt;enumeration value="AUSA"/>
 *     &lt;enumeration value="AUTAS"/>
 *     &lt;enumeration value="AUVIC"/>
 *     &lt;enumeration value="AUWA"/>
 *     &lt;enumeration value="BRAC"/>
 *     &lt;enumeration value="BRAL"/>
 *     &lt;enumeration value="BRAM"/>
 *     &lt;enumeration value="BRAP"/>
 *     &lt;enumeration value="BRBA"/>
 *     &lt;enumeration value="BRCE"/>
 *     &lt;enumeration value="BRDF"/>
 *     &lt;enumeration value="BRES"/>
 *     &lt;enumeration value="BRGO"/>
 *     &lt;enumeration value="BRMA"/>
 *     &lt;enumeration value="BRMG"/>
 *     &lt;enumeration value="BRMS"/>
 *     &lt;enumeration value="BRMT"/>
 *     &lt;enumeration value="BRPA"/>
 *     &lt;enumeration value="BRPB"/>
 *     &lt;enumeration value="BRPE"/>
 *     &lt;enumeration value="BRPI"/>
 *     &lt;enumeration value="BRPR"/>
 *     &lt;enumeration value="BRRJ"/>
 *     &lt;enumeration value="BRRN"/>
 *     &lt;enumeration value="BRRO"/>
 *     &lt;enumeration value="BRRR"/>
 *     &lt;enumeration value="BRRS"/>
 *     &lt;enumeration value="BRSC"/>
 *     &lt;enumeration value="BRSE"/>
 *     &lt;enumeration value="BRSP"/>
 *     &lt;enumeration value="BRTO"/>
 *     &lt;enumeration value="CAAB"/>
 *     &lt;enumeration value="CABC"/>
 *     &lt;enumeration value="CAMB"/>
 *     &lt;enumeration value="CANB"/>
 *     &lt;enumeration value="CANF"/>
 *     &lt;enumeration value="CANS"/>
 *     &lt;enumeration value="CANT"/>
 *     &lt;enumeration value="CAON"/>
 *     &lt;enumeration value="CAPE"/>
 *     &lt;enumeration value="CAQC"/>
 *     &lt;enumeration value="CASK"/>
 *     &lt;enumeration value="CAYT"/>
 *     &lt;enumeration value="GBAM"/>
 *     &lt;enumeration value="GBAR"/>
 *     &lt;enumeration value="GBAT"/>
 *     &lt;enumeration value="GBBA"/>
 *     &lt;enumeration value="GBBB"/>
 *     &lt;enumeration value="GBBE"/>
 *     &lt;enumeration value="GBBF"/>
 *     &lt;enumeration value="GBBH"/>
 *     &lt;enumeration value="GBBK"/>
 *     &lt;enumeration value="GBBL"/>
 *     &lt;enumeration value="GBBM"/>
 *     &lt;enumeration value="GBBN"/>
 *     &lt;enumeration value="GBBO"/>
 *     &lt;enumeration value="GBBP"/>
 *     &lt;enumeration value="GBBR"/>
 *     &lt;enumeration value="GBBS"/>
 *     &lt;enumeration value="GBBU"/>
 *     &lt;enumeration value="GBBY"/>
 *     &lt;enumeration value="GBCA"/>
 *     &lt;enumeration value="GBCE"/>
 *     &lt;enumeration value="GBCG"/>
 *     &lt;enumeration value="GBCH"/>
 *     &lt;enumeration value="GBCI"/>
 *     &lt;enumeration value="GBCK"/>
 *     &lt;enumeration value="GBCL"/>
 *     &lt;enumeration value="GBCO"/>
 *     &lt;enumeration value="GBCR"/>
 *     &lt;enumeration value="GBCS"/>
 *     &lt;enumeration value="GBCU"/>
 *     &lt;enumeration value="GBCV"/>
 *     &lt;enumeration value="GBDB"/>
 *     &lt;enumeration value="GBDF"/>
 *     &lt;enumeration value="GBDG"/>
 *     &lt;enumeration value="GBDL"/>
 *     &lt;enumeration value="GBDN"/>
 *     &lt;enumeration value="GBDO"/>
 *     &lt;enumeration value="GBDU"/>
 *     &lt;enumeration value="GBDV"/>
 *     &lt;enumeration value="GBDY"/>
 *     &lt;enumeration value="GBER"/>
 *     &lt;enumeration value="GBES"/>
 *     &lt;enumeration value="GBFI"/>
 *     &lt;enumeration value="GBFM"/>
 *     &lt;enumeration value="GBGL"/>
 *     &lt;enumeration value="GBGM"/>
 *     &lt;enumeration value="GBGR"/>
 *     &lt;enumeration value="GBGS"/>
 *     &lt;enumeration value="GBGW"/>
 *     &lt;enumeration value="GBGY"/>
 *     &lt;enumeration value="GBHA"/>
 *     &lt;enumeration value="GBHI"/>
 *     &lt;enumeration value="GBHL"/>
 *     &lt;enumeration value="GBHR"/>
 *     &lt;enumeration value="GBHT"/>
 *     &lt;enumeration value="GBHW"/>
 *     &lt;enumeration value="GBIW"/>
 *     &lt;enumeration value="GBKE"/>
 *     &lt;enumeration value="GBKH"/>
 *     &lt;enumeration value="GBLA"/>
 *     &lt;enumeration value="GBLC"/>
 *     &lt;enumeration value="GBLD"/>
 *     &lt;enumeration value="GBLE"/>
 *     &lt;enumeration value="GBLI"/>
 *     &lt;enumeration value="GBLM"/>
 *     &lt;enumeration value="GBLO"/>
 *     &lt;enumeration value="GBLR"/>
 *     &lt;enumeration value="GBLS"/>
 *     &lt;enumeration value="GBLT"/>
 *     &lt;enumeration value="GBLU"/>
 *     &lt;enumeration value="GBMA"/>
 *     &lt;enumeration value="GBMG"/>
 *     &lt;enumeration value="GBMI"/>
 *     &lt;enumeration value="GBMK"/>
 *     &lt;enumeration value="GBMO"/>
 *     &lt;enumeration value="GBMY"/>
 *     &lt;enumeration value="GBNA"/>
 *     &lt;enumeration value="GBNB"/>
 *     &lt;enumeration value="GBNC"/>
 *     &lt;enumeration value="GBND"/>
 *     &lt;enumeration value="GBNE"/>
 *     &lt;enumeration value="GBNH"/>
 *     &lt;enumeration value="GBNK"/>
 *     &lt;enumeration value="GBNL"/>
 *     &lt;enumeration value="GBNM"/>
 *     &lt;enumeration value="GBNS"/>
 *     &lt;enumeration value="GBNT"/>
 *     &lt;enumeration value="GBNU"/>
 *     &lt;enumeration value="GBOM"/>
 *     &lt;enumeration value="GBOR"/>
 *     &lt;enumeration value="GBOX"/>
 *     &lt;enumeration value="GBPB"/>
 *     &lt;enumeration value="GBPL"/>
 *     &lt;enumeration value="GBPM"/>
 *     &lt;enumeration value="GBPO"/>
 *     &lt;enumeration value="GBPY"/>
 *     &lt;enumeration value="GBRD"/>
 *     &lt;enumeration value="GBRE"/>
 *     &lt;enumeration value="GBRM"/>
 *     &lt;enumeration value="GBRU"/>
 *     &lt;enumeration value="GBSC"/>
 *     &lt;enumeration value="GBSD"/>
 *     &lt;enumeration value="GBSE"/>
 *     &lt;enumeration value="GBSG"/>
 *     &lt;enumeration value="GBSH"/>
 *     &lt;enumeration value="GBSK"/>
 *     &lt;enumeration value="GBSL"/>
 *     &lt;enumeration value="GBSN"/>
 *     &lt;enumeration value="GBSO"/>
 *     &lt;enumeration value="GBSP"/>
 *     &lt;enumeration value="GBSR"/>
 *     &lt;enumeration value="GBST"/>
 *     &lt;enumeration value="GBSU"/>
 *     &lt;enumeration value="GBSW"/>
 *     &lt;enumeration value="GBSY"/>
 *     &lt;enumeration value="GBTA"/>
 *     &lt;enumeration value="GBTD"/>
 *     &lt;enumeration value="GBTH"/>
 *     &lt;enumeration value="GBTO"/>
 *     &lt;enumeration value="GBTW"/>
 *     &lt;enumeration value="GBWA"/>
 *     &lt;enumeration value="GBWD"/>
 *     &lt;enumeration value="GBWG"/>
 *     &lt;enumeration value="GBWI"/>
 *     &lt;enumeration value="GBWL"/>
 *     &lt;enumeration value="GBWM"/>
 *     &lt;enumeration value="GBWO"/>
 *     &lt;enumeration value="GBWR"/>
 *     &lt;enumeration value="GBWT"/>
 *     &lt;enumeration value="GBYK"/>
 *     &lt;enumeration value="GBYN"/>
 *     &lt;enumeration value="GBYS"/>
 *     &lt;enumeration value="GBYW"/>
 *     &lt;enumeration value="ITAG"/>
 *     &lt;enumeration value="ITAL"/>
 *     &lt;enumeration value="ITAN"/>
 *     &lt;enumeration value="ITAO"/>
 *     &lt;enumeration value="ITAP"/>
 *     &lt;enumeration value="ITAQ"/>
 *     &lt;enumeration value="ITAR"/>
 *     &lt;enumeration value="ITAT"/>
 *     &lt;enumeration value="ITAV"/>
 *     &lt;enumeration value="ITBA"/>
 *     &lt;enumeration value="ITBG"/>
 *     &lt;enumeration value="ITBI"/>
 *     &lt;enumeration value="ITBL"/>
 *     &lt;enumeration value="ITBN"/>
 *     &lt;enumeration value="ITBO"/>
 *     &lt;enumeration value="ITBR"/>
 *     &lt;enumeration value="ITBS"/>
 *     &lt;enumeration value="ITBZ"/>
 *     &lt;enumeration value="ITCA"/>
 *     &lt;enumeration value="ITCB"/>
 *     &lt;enumeration value="ITCE"/>
 *     &lt;enumeration value="ITCH"/>
 *     &lt;enumeration value="ITCL"/>
 *     &lt;enumeration value="ITCN"/>
 *     &lt;enumeration value="ITCO"/>
 *     &lt;enumeration value="ITCR"/>
 *     &lt;enumeration value="ITCS"/>
 *     &lt;enumeration value="ITCT"/>
 *     &lt;enumeration value="ITCZ"/>
 *     &lt;enumeration value="ITEN"/>
 *     &lt;enumeration value="ITFE"/>
 *     &lt;enumeration value="ITFG"/>
 *     &lt;enumeration value="ITFI"/>
 *     &lt;enumeration value="ITFO"/>
 *     &lt;enumeration value="ITFR"/>
 *     &lt;enumeration value="ITGE"/>
 *     &lt;enumeration value="ITGO"/>
 *     &lt;enumeration value="ITGR"/>
 *     &lt;enumeration value="ITIM"/>
 *     &lt;enumeration value="ITIS"/>
 *     &lt;enumeration value="ITKR"/>
 *     &lt;enumeration value="ITLC"/>
 *     &lt;enumeration value="ITLE"/>
 *     &lt;enumeration value="ITLI"/>
 *     &lt;enumeration value="ITLO"/>
 *     &lt;enumeration value="ITLT"/>
 *     &lt;enumeration value="ITLU"/>
 *     &lt;enumeration value="ITMC"/>
 *     &lt;enumeration value="ITME"/>
 *     &lt;enumeration value="ITMI"/>
 *     &lt;enumeration value="ITMN"/>
 *     &lt;enumeration value="ITMO"/>
 *     &lt;enumeration value="ITMS"/>
 *     &lt;enumeration value="ITMT"/>
 *     &lt;enumeration value="ITNA"/>
 *     &lt;enumeration value="ITNO"/>
 *     &lt;enumeration value="ITNU"/>
 *     &lt;enumeration value="ITOR"/>
 *     &lt;enumeration value="ITPA"/>
 *     &lt;enumeration value="ITPC"/>
 *     &lt;enumeration value="ITPD"/>
 *     &lt;enumeration value="ITPE"/>
 *     &lt;enumeration value="ITPG"/>
 *     &lt;enumeration value="ITPI"/>
 *     &lt;enumeration value="ITPN"/>
 *     &lt;enumeration value="ITPO"/>
 *     &lt;enumeration value="ITPR"/>
 *     &lt;enumeration value="ITPS"/>
 *     &lt;enumeration value="ITPT"/>
 *     &lt;enumeration value="ITPV"/>
 *     &lt;enumeration value="ITPZ"/>
 *     &lt;enumeration value="ITRA"/>
 *     &lt;enumeration value="ITRC"/>
 *     &lt;enumeration value="ITRE"/>
 *     &lt;enumeration value="ITRG"/>
 *     &lt;enumeration value="ITRI"/>
 *     &lt;enumeration value="ITRM"/>
 *     &lt;enumeration value="ITRN"/>
 *     &lt;enumeration value="ITRO"/>
 *     &lt;enumeration value="ITRV"/>
 *     &lt;enumeration value="ITSA"/>
 *     &lt;enumeration value="ITSI"/>
 *     &lt;enumeration value="ITSO"/>
 *     &lt;enumeration value="ITSP"/>
 *     &lt;enumeration value="ITSR"/>
 *     &lt;enumeration value="ITSS"/>
 *     &lt;enumeration value="ITSV"/>
 *     &lt;enumeration value="ITTA"/>
 *     &lt;enumeration value="ITTE"/>
 *     &lt;enumeration value="ITTN"/>
 *     &lt;enumeration value="ITTO"/>
 *     &lt;enumeration value="ITTP"/>
 *     &lt;enumeration value="ITTR"/>
 *     &lt;enumeration value="ITTS"/>
 *     &lt;enumeration value="ITTV"/>
 *     &lt;enumeration value="ITUD"/>
 *     &lt;enumeration value="ITVA"/>
 *     &lt;enumeration value="ITVC"/>
 *     &lt;enumeration value="ITVE"/>
 *     &lt;enumeration value="ITVI"/>
 *     &lt;enumeration value="ITVR"/>
 *     &lt;enumeration value="ITVT"/>
 *     &lt;enumeration value="ITVV"/>
 *     &lt;enumeration value="USAK"/>
 *     &lt;enumeration value="USAL"/>
 *     &lt;enumeration value="USAR"/>
 *     &lt;enumeration value="USAS"/>
 *     &lt;enumeration value="USAZ"/>
 *     &lt;enumeration value="USCA"/>
 *     &lt;enumeration value="USCO"/>
 *     &lt;enumeration value="USCT"/>
 *     &lt;enumeration value="USDC"/>
 *     &lt;enumeration value="USDE"/>
 *     &lt;enumeration value="USFL"/>
 *     &lt;enumeration value="USGA"/>
 *     &lt;enumeration value="USGU"/>
 *     &lt;enumeration value="USHI"/>
 *     &lt;enumeration value="USIA"/>
 *     &lt;enumeration value="USID"/>
 *     &lt;enumeration value="USIL"/>
 *     &lt;enumeration value="USIN"/>
 *     &lt;enumeration value="USKS"/>
 *     &lt;enumeration value="USKY"/>
 *     &lt;enumeration value="USLA"/>
 *     &lt;enumeration value="USMA"/>
 *     &lt;enumeration value="USMD"/>
 *     &lt;enumeration value="USME"/>
 *     &lt;enumeration value="USMI"/>
 *     &lt;enumeration value="USMN"/>
 *     &lt;enumeration value="USMO"/>
 *     &lt;enumeration value="USMP"/>
 *     &lt;enumeration value="USMS"/>
 *     &lt;enumeration value="USMT"/>
 *     &lt;enumeration value="USNC"/>
 *     &lt;enumeration value="USND"/>
 *     &lt;enumeration value="USNE"/>
 *     &lt;enumeration value="USNH"/>
 *     &lt;enumeration value="USNJ"/>
 *     &lt;enumeration value="USNM"/>
 *     &lt;enumeration value="USNV"/>
 *     &lt;enumeration value="USNY"/>
 *     &lt;enumeration value="USOH"/>
 *     &lt;enumeration value="USOK"/>
 *     &lt;enumeration value="USOR"/>
 *     &lt;enumeration value="USPA"/>
 *     &lt;enumeration value="USPR"/>
 *     &lt;enumeration value="USRI"/>
 *     &lt;enumeration value="USSC"/>
 *     &lt;enumeration value="USSD"/>
 *     &lt;enumeration value="USTN"/>
 *     &lt;enumeration value="USTX"/>
 *     &lt;enumeration value="USUT"/>
 *     &lt;enumeration value="USVA"/>
 *     &lt;enumeration value="USVI"/>
 *     &lt;enumeration value="USVT"/>
 *     &lt;enumeration value="USWA"/>
 *     &lt;enumeration value="USWI"/>
 *     &lt;enumeration value="USWV"/>
 *     &lt;enumeration value="USWY"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "RegionCode")
@XmlEnum
public enum RegionCode {

    @XmlEnumValue("Other")
    OTHER("Other"),
    AUACT("AUACT"),
    AUNSW("AUNSW"),
    AUNT("AUNT"),
    AUQLD("AUQLD"),
    AUSA("AUSA"),
    AUTAS("AUTAS"),
    AUVIC("AUVIC"),
    AUWA("AUWA"),
    BRAC("BRAC"),
    BRAL("BRAL"),
    BRAM("BRAM"),
    BRAP("BRAP"),
    BRBA("BRBA"),
    BRCE("BRCE"),
    BRDF("BRDF"),
    BRES("BRES"),
    BRGO("BRGO"),
    BRMA("BRMA"),
    BRMG("BRMG"),
    BRMS("BRMS"),
    BRMT("BRMT"),
    BRPA("BRPA"),
    BRPB("BRPB"),
    BRPE("BRPE"),
    BRPI("BRPI"),
    BRPR("BRPR"),
    BRRJ("BRRJ"),
    BRRN("BRRN"),
    BRRO("BRRO"),
    BRRR("BRRR"),
    BRRS("BRRS"),
    BRSC("BRSC"),
    BRSE("BRSE"),
    BRSP("BRSP"),
    BRTO("BRTO"),
    CAAB("CAAB"),
    CABC("CABC"),
    CAMB("CAMB"),
    CANB("CANB"),
    CANF("CANF"),
    CANS("CANS"),
    CANT("CANT"),
    CAON("CAON"),
    CAPE("CAPE"),
    CAQC("CAQC"),
    CASK("CASK"),
    CAYT("CAYT"),
    GBAM("GBAM"),
    GBAR("GBAR"),
    GBAT("GBAT"),
    GBBA("GBBA"),
    GBBB("GBBB"),
    GBBE("GBBE"),
    GBBF("GBBF"),
    GBBH("GBBH"),
    GBBK("GBBK"),
    GBBL("GBBL"),
    GBBM("GBBM"),
    GBBN("GBBN"),
    GBBO("GBBO"),
    GBBP("GBBP"),
    GBBR("GBBR"),
    GBBS("GBBS"),
    GBBU("GBBU"),
    GBBY("GBBY"),
    GBCA("GBCA"),
    GBCE("GBCE"),
    GBCG("GBCG"),
    GBCH("GBCH"),
    GBCI("GBCI"),
    GBCK("GBCK"),
    GBCL("GBCL"),
    GBCO("GBCO"),
    GBCR("GBCR"),
    GBCS("GBCS"),
    GBCU("GBCU"),
    GBCV("GBCV"),
    GBDB("GBDB"),
    GBDF("GBDF"),
    GBDG("GBDG"),
    GBDL("GBDL"),
    GBDN("GBDN"),
    GBDO("GBDO"),
    GBDU("GBDU"),
    GBDV("GBDV"),
    GBDY("GBDY"),
    GBER("GBER"),
    GBES("GBES"),
    GBFI("GBFI"),
    GBFM("GBFM"),
    GBGL("GBGL"),
    GBGM("GBGM"),
    GBGR("GBGR"),
    GBGS("GBGS"),
    GBGW("GBGW"),
    GBGY("GBGY"),
    GBHA("GBHA"),
    GBHI("GBHI"),
    GBHL("GBHL"),
    GBHR("GBHR"),
    GBHT("GBHT"),
    GBHW("GBHW"),
    GBIW("GBIW"),
    GBKE("GBKE"),
    GBKH("GBKH"),
    GBLA("GBLA"),
    GBLC("GBLC"),
    GBLD("GBLD"),
    GBLE("GBLE"),
    GBLI("GBLI"),
    GBLM("GBLM"),
    GBLO("GBLO"),
    GBLR("GBLR"),
    GBLS("GBLS"),
    GBLT("GBLT"),
    GBLU("GBLU"),
    GBMA("GBMA"),
    GBMG("GBMG"),
    GBMI("GBMI"),
    GBMK("GBMK"),
    GBMO("GBMO"),
    GBMY("GBMY"),
    GBNA("GBNA"),
    GBNB("GBNB"),
    GBNC("GBNC"),
    GBND("GBND"),
    GBNE("GBNE"),
    GBNH("GBNH"),
    GBNK("GBNK"),
    GBNL("GBNL"),
    GBNM("GBNM"),
    GBNS("GBNS"),
    GBNT("GBNT"),
    GBNU("GBNU"),
    GBOM("GBOM"),
    GBOR("GBOR"),
    GBOX("GBOX"),
    GBPB("GBPB"),
    GBPL("GBPL"),
    GBPM("GBPM"),
    GBPO("GBPO"),
    GBPY("GBPY"),
    GBRD("GBRD"),
    GBRE("GBRE"),
    GBRM("GBRM"),
    GBRU("GBRU"),
    GBSC("GBSC"),
    GBSD("GBSD"),
    GBSE("GBSE"),
    GBSG("GBSG"),
    GBSH("GBSH"),
    GBSK("GBSK"),
    GBSL("GBSL"),
    GBSN("GBSN"),
    GBSO("GBSO"),
    GBSP("GBSP"),
    GBSR("GBSR"),
    GBST("GBST"),
    GBSU("GBSU"),
    GBSW("GBSW"),
    GBSY("GBSY"),
    GBTA("GBTA"),
    GBTD("GBTD"),
    GBTH("GBTH"),
    GBTO("GBTO"),
    GBTW("GBTW"),
    GBWA("GBWA"),
    GBWD("GBWD"),
    GBWG("GBWG"),
    GBWI("GBWI"),
    GBWL("GBWL"),
    GBWM("GBWM"),
    GBWO("GBWO"),
    GBWR("GBWR"),
    GBWT("GBWT"),
    GBYK("GBYK"),
    GBYN("GBYN"),
    GBYS("GBYS"),
    GBYW("GBYW"),
    ITAG("ITAG"),
    ITAL("ITAL"),
    ITAN("ITAN"),
    ITAO("ITAO"),
    ITAP("ITAP"),
    ITAQ("ITAQ"),
    ITAR("ITAR"),
    ITAT("ITAT"),
    ITAV("ITAV"),
    ITBA("ITBA"),
    ITBG("ITBG"),
    ITBI("ITBI"),
    ITBL("ITBL"),
    ITBN("ITBN"),
    ITBO("ITBO"),
    ITBR("ITBR"),
    ITBS("ITBS"),
    ITBZ("ITBZ"),
    ITCA("ITCA"),
    ITCB("ITCB"),
    ITCE("ITCE"),
    ITCH("ITCH"),
    ITCL("ITCL"),
    ITCN("ITCN"),
    ITCO("ITCO"),
    ITCR("ITCR"),
    ITCS("ITCS"),
    ITCT("ITCT"),
    ITCZ("ITCZ"),
    ITEN("ITEN"),
    ITFE("ITFE"),
    ITFG("ITFG"),
    ITFI("ITFI"),
    ITFO("ITFO"),
    ITFR("ITFR"),
    ITGE("ITGE"),
    ITGO("ITGO"),
    ITGR("ITGR"),
    ITIM("ITIM"),
    ITIS("ITIS"),
    ITKR("ITKR"),
    ITLC("ITLC"),
    ITLE("ITLE"),
    ITLI("ITLI"),
    ITLO("ITLO"),
    ITLT("ITLT"),
    ITLU("ITLU"),
    ITMC("ITMC"),
    ITME("ITME"),
    ITMI("ITMI"),
    ITMN("ITMN"),
    ITMO("ITMO"),
    ITMS("ITMS"),
    ITMT("ITMT"),
    ITNA("ITNA"),
    ITNO("ITNO"),
    ITNU("ITNU"),
    ITOR("ITOR"),
    ITPA("ITPA"),
    ITPC("ITPC"),
    ITPD("ITPD"),
    ITPE("ITPE"),
    ITPG("ITPG"),
    ITPI("ITPI"),
    ITPN("ITPN"),
    ITPO("ITPO"),
    ITPR("ITPR"),
    ITPS("ITPS"),
    ITPT("ITPT"),
    ITPV("ITPV"),
    ITPZ("ITPZ"),
    ITRA("ITRA"),
    ITRC("ITRC"),
    ITRE("ITRE"),
    ITRG("ITRG"),
    ITRI("ITRI"),
    ITRM("ITRM"),
    ITRN("ITRN"),
    ITRO("ITRO"),
    ITRV("ITRV"),
    ITSA("ITSA"),
    ITSI("ITSI"),
    ITSO("ITSO"),
    ITSP("ITSP"),
    ITSR("ITSR"),
    ITSS("ITSS"),
    ITSV("ITSV"),
    ITTA("ITTA"),
    ITTE("ITTE"),
    ITTN("ITTN"),
    ITTO("ITTO"),
    ITTP("ITTP"),
    ITTR("ITTR"),
    ITTS("ITTS"),
    ITTV("ITTV"),
    ITUD("ITUD"),
    ITVA("ITVA"),
    ITVC("ITVC"),
    ITVE("ITVE"),
    ITVI("ITVI"),
    ITVR("ITVR"),
    ITVT("ITVT"),
    ITVV("ITVV"),
    USAK("USAK"),
    USAL("USAL"),
    USAR("USAR"),
    USAS("USAS"),
    USAZ("USAZ"),
    USCA("USCA"),
    USCO("USCO"),
    USCT("USCT"),
    USDC("USDC"),
    USDE("USDE"),
    USFL("USFL"),
    USGA("USGA"),
    USGU("USGU"),
    USHI("USHI"),
    USIA("USIA"),
    USID("USID"),
    USIL("USIL"),
    USIN("USIN"),
    USKS("USKS"),
    USKY("USKY"),
    USLA("USLA"),
    USMA("USMA"),
    USMD("USMD"),
    USME("USME"),
    USMI("USMI"),
    USMN("USMN"),
    USMO("USMO"),
    USMP("USMP"),
    USMS("USMS"),
    USMT("USMT"),
    USNC("USNC"),
    USND("USND"),
    USNE("USNE"),
    USNH("USNH"),
    USNJ("USNJ"),
    USNM("USNM"),
    USNV("USNV"),
    USNY("USNY"),
    USOH("USOH"),
    USOK("USOK"),
    USOR("USOR"),
    USPA("USPA"),
    USPR("USPR"),
    USRI("USRI"),
    USSC("USSC"),
    USSD("USSD"),
    USTN("USTN"),
    USTX("USTX"),
    USUT("USUT"),
    USVA("USVA"),
    USVI("USVI"),
    USVT("USVT"),
    USWA("USWA"),
    USWI("USWI"),
    USWV("USWV"),
    USWY("USWY");
    private final String value;

    RegionCode(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static RegionCode fromValue(String v) {
        for (RegionCode c: RegionCode.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
