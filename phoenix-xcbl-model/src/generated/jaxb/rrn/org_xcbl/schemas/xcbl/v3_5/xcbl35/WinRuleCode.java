//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.5-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2014.08.13 at 10:13:13 PM EDT 
//


package rrn.org_xcbl.schemas.xcbl.v3_5.xcbl35;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for WinRuleCode.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="WinRuleCode">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}NMTOKEN">
 *     &lt;enumeration value="HighestBidWins"/>
 *     &lt;enumeration value="WhoExceedsReservePriceWins"/>
 *     &lt;enumeration value="ClearOffInventory"/>
 *     &lt;enumeration value="LargestQuantityWins"/>
 *     &lt;enumeration value="MaximizeRevenue"/>
 *     &lt;enumeration value="LowestBidWins"/>
 *     &lt;enumeration value="YankeeRule"/>
 *     &lt;enumeration value="MVBForwardRule"/>
 *     &lt;enumeration value="MVBReverseRule"/>
 *     &lt;enumeration value="DutchRule"/>
 *     &lt;enumeration value="Other"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "WinRuleCode")
@XmlEnum
public enum WinRuleCode {

    @XmlEnumValue("HighestBidWins")
    HIGHEST_BID_WINS("HighestBidWins"),
    @XmlEnumValue("WhoExceedsReservePriceWins")
    WHO_EXCEEDS_RESERVE_PRICE_WINS("WhoExceedsReservePriceWins"),
    @XmlEnumValue("ClearOffInventory")
    CLEAR_OFF_INVENTORY("ClearOffInventory"),
    @XmlEnumValue("LargestQuantityWins")
    LARGEST_QUANTITY_WINS("LargestQuantityWins"),
    @XmlEnumValue("MaximizeRevenue")
    MAXIMIZE_REVENUE("MaximizeRevenue"),
    @XmlEnumValue("LowestBidWins")
    LOWEST_BID_WINS("LowestBidWins"),
    @XmlEnumValue("YankeeRule")
    YANKEE_RULE("YankeeRule"),
    @XmlEnumValue("MVBForwardRule")
    MVB_FORWARD_RULE("MVBForwardRule"),
    @XmlEnumValue("MVBReverseRule")
    MVB_REVERSE_RULE("MVBReverseRule"),
    @XmlEnumValue("DutchRule")
    DUTCH_RULE("DutchRule"),
    @XmlEnumValue("Other")
    OTHER("Other");
    private final String value;

    WinRuleCode(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static WinRuleCode fromValue(String v) {
        for (WinRuleCode c: WinRuleCode.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
