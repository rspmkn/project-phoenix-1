//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.5-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2014.08.13 at 10:13:13 PM EDT 
//


package rrn.org_xcbl.schemas.xcbl.v3_5.xcbl35;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ActualPaymentStatusCode.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ActualPaymentStatusCode">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}NMTOKEN">
 *     &lt;enumeration value="Other"/>
 *     &lt;enumeration value="PaidInFull"/>
 *     &lt;enumeration value="PartPaid"/>
 *     &lt;enumeration value="NotPaid"/>
 *     &lt;enumeration value="OverPaid"/>
 *     &lt;enumeration value="FreeOfCharge"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "ActualPaymentStatusCode")
@XmlEnum
public enum ActualPaymentStatusCode {

    @XmlEnumValue("Other")
    OTHER("Other"),
    @XmlEnumValue("PaidInFull")
    PAID_IN_FULL("PaidInFull"),
    @XmlEnumValue("PartPaid")
    PART_PAID("PartPaid"),
    @XmlEnumValue("NotPaid")
    NOT_PAID("NotPaid"),
    @XmlEnumValue("OverPaid")
    OVER_PAID("OverPaid"),
    @XmlEnumValue("FreeOfCharge")
    FREE_OF_CHARGE("FreeOfCharge");
    private final String value;

    ActualPaymentStatusCode(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ActualPaymentStatusCode fromValue(String v) {
        for (ActualPaymentStatusCode c: ActualPaymentStatusCode.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
