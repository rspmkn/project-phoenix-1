//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.5-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2014.08.13 at 10:13:13 PM EDT 
//


package rrn.org_xcbl.schemas.xcbl.v3_5.xcbl35;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for InvoiceSummary complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="InvoiceSummary">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="NumberOfLines" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element ref="{rrn:org.xcbl:schemas/xcbl/v3_5/xcbl35.xsd}InvoiceTotals" minOccurs="0"/>
 *         &lt;element ref="{rrn:org.xcbl:schemas/xcbl/v3_5/xcbl35.xsd}ListOfTaxSummary" minOccurs="0"/>
 *         &lt;element ref="{rrn:org.xcbl:schemas/xcbl/v3_5/xcbl35.xsd}AllowOrChargeSummary" minOccurs="0"/>
 *         &lt;element ref="{rrn:org.xcbl:schemas/xcbl/v3_5/xcbl35.xsd}InvoicePaymentStatus" minOccurs="0"/>
 *         &lt;element ref="{rrn:org.xcbl:schemas/xcbl/v3_5/xcbl35.xsd}ListOfActualPayment" minOccurs="0"/>
 *         &lt;element name="SummaryNote" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "InvoiceSummary", propOrder = {
    "numberOfLines",
    "invoiceTotals",
    "listOfTaxSummary",
    "allowOrChargeSummary",
    "invoicePaymentStatus",
    "listOfActualPayment",
    "summaryNote"
})
public class InvoiceSummary
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "NumberOfLines")
    protected Integer numberOfLines;
    @XmlElement(name = "InvoiceTotals")
    protected InvoiceTotals invoiceTotals;
    @XmlElement(name = "ListOfTaxSummary")
    protected ListOfTaxSummary listOfTaxSummary;
    @XmlElement(name = "AllowOrChargeSummary")
    protected AllowOrChargeSummary allowOrChargeSummary;
    @XmlElement(name = "InvoicePaymentStatus")
    protected InvoicePaymentStatus invoicePaymentStatus;
    @XmlElement(name = "ListOfActualPayment")
    protected ListOfActualPayment listOfActualPayment;
    @XmlElement(name = "SummaryNote")
    protected String summaryNote;

    /**
     * Gets the value of the numberOfLines property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getNumberOfLines() {
        return numberOfLines;
    }

    /**
     * Sets the value of the numberOfLines property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setNumberOfLines(Integer value) {
        this.numberOfLines = value;
    }

    /**
     * Gets the value of the invoiceTotals property.
     * 
     * @return
     *     possible object is
     *     {@link InvoiceTotals }
     *     
     */
    public InvoiceTotals getInvoiceTotals() {
        return invoiceTotals;
    }

    /**
     * Sets the value of the invoiceTotals property.
     * 
     * @param value
     *     allowed object is
     *     {@link InvoiceTotals }
     *     
     */
    public void setInvoiceTotals(InvoiceTotals value) {
        this.invoiceTotals = value;
    }

    /**
     * Gets the value of the listOfTaxSummary property.
     * 
     * @return
     *     possible object is
     *     {@link ListOfTaxSummary }
     *     
     */
    public ListOfTaxSummary getListOfTaxSummary() {
        return listOfTaxSummary;
    }

    /**
     * Sets the value of the listOfTaxSummary property.
     * 
     * @param value
     *     allowed object is
     *     {@link ListOfTaxSummary }
     *     
     */
    public void setListOfTaxSummary(ListOfTaxSummary value) {
        this.listOfTaxSummary = value;
    }

    /**
     * Gets the value of the allowOrChargeSummary property.
     * 
     * @return
     *     possible object is
     *     {@link AllowOrChargeSummary }
     *     
     */
    public AllowOrChargeSummary getAllowOrChargeSummary() {
        return allowOrChargeSummary;
    }

    /**
     * Sets the value of the allowOrChargeSummary property.
     * 
     * @param value
     *     allowed object is
     *     {@link AllowOrChargeSummary }
     *     
     */
    public void setAllowOrChargeSummary(AllowOrChargeSummary value) {
        this.allowOrChargeSummary = value;
    }

    /**
     * Gets the value of the invoicePaymentStatus property.
     * 
     * @return
     *     possible object is
     *     {@link InvoicePaymentStatus }
     *     
     */
    public InvoicePaymentStatus getInvoicePaymentStatus() {
        return invoicePaymentStatus;
    }

    /**
     * Sets the value of the invoicePaymentStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link InvoicePaymentStatus }
     *     
     */
    public void setInvoicePaymentStatus(InvoicePaymentStatus value) {
        this.invoicePaymentStatus = value;
    }

    /**
     * Gets the value of the listOfActualPayment property.
     * 
     * @return
     *     possible object is
     *     {@link ListOfActualPayment }
     *     
     */
    public ListOfActualPayment getListOfActualPayment() {
        return listOfActualPayment;
    }

    /**
     * Sets the value of the listOfActualPayment property.
     * 
     * @param value
     *     allowed object is
     *     {@link ListOfActualPayment }
     *     
     */
    public void setListOfActualPayment(ListOfActualPayment value) {
        this.listOfActualPayment = value;
    }

    /**
     * Gets the value of the summaryNote property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSummaryNote() {
        return summaryNote;
    }

    /**
     * Sets the value of the summaryNote property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSummaryNote(String value) {
        this.summaryNote = value;
    }

}
