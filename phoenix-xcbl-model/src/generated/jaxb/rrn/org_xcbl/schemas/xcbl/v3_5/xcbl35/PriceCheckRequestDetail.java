//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.5-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2014.08.13 at 10:13:13 PM EDT 
//


package rrn.org_xcbl.schemas.xcbl.v3_5.xcbl35;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PriceCheckRequestDetail complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PriceCheckRequestDetail">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{rrn:org.xcbl:schemas/xcbl/v3_5/xcbl35.xsd}ListOfPriceCheckRequestItemDetail"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PriceCheckRequestDetail", propOrder = {
    "listOfPriceCheckRequestItemDetail"
})
public class PriceCheckRequestDetail
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "ListOfPriceCheckRequestItemDetail", required = true)
    protected ListOfPriceCheckRequestItemDetail listOfPriceCheckRequestItemDetail;

    /**
     * Gets the value of the listOfPriceCheckRequestItemDetail property.
     * 
     * @return
     *     possible object is
     *     {@link ListOfPriceCheckRequestItemDetail }
     *     
     */
    public ListOfPriceCheckRequestItemDetail getListOfPriceCheckRequestItemDetail() {
        return listOfPriceCheckRequestItemDetail;
    }

    /**
     * Sets the value of the listOfPriceCheckRequestItemDetail property.
     * 
     * @param value
     *     allowed object is
     *     {@link ListOfPriceCheckRequestItemDetail }
     *     
     */
    public void setListOfPriceCheckRequestItemDetail(ListOfPriceCheckRequestItemDetail value) {
        this.listOfPriceCheckRequestItemDetail = value;
    }

}
