//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.5-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2014.08.13 at 10:13:13 PM EDT 
//


package rrn.org_xcbl.schemas.xcbl.v3_5.xcbl35;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for MaterialGroupedShippingResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MaterialGroupedShippingResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="DetailResponseCoded" type="{rrn:org.xcbl:schemas/xcbl/v3_5/xcbl35.xsd}DetailResponseCode"/>
 *         &lt;element name="DetailResponseCodedOther" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OriginalMaterialGroupedShippingDetail" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element ref="{rrn:org.xcbl:schemas/xcbl/v3_5/xcbl35.xsd}MaterialGroupedShippingDetail"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="ChangedMaterialGroupedShippingDetail" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element ref="{rrn:org.xcbl:schemas/xcbl/v3_5/xcbl35.xsd}MaterialGroupedShippingDetail"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="LineItemNote" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element ref="{rrn:org.xcbl:schemas/xcbl/v3_5/xcbl35.xsd}ListOfStructuredNote" minOccurs="0"/>
 *         &lt;element ref="{rrn:org.xcbl:schemas/xcbl/v3_5/xcbl35.xsd}ListOfAttachment" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MaterialGroupedShippingResponse", propOrder = {
    "detailResponseCoded",
    "detailResponseCodedOther",
    "originalMaterialGroupedShippingDetail",
    "changedMaterialGroupedShippingDetail",
    "lineItemNote",
    "listOfStructuredNote",
    "listOfAttachment"
})
public class MaterialGroupedShippingResponse
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "DetailResponseCoded", required = true)
    protected DetailResponseCode detailResponseCoded;
    @XmlElement(name = "DetailResponseCodedOther")
    protected String detailResponseCodedOther;
    @XmlElement(name = "OriginalMaterialGroupedShippingDetail")
    protected MaterialGroupedShippingResponse.OriginalMaterialGroupedShippingDetail originalMaterialGroupedShippingDetail;
    @XmlElement(name = "ChangedMaterialGroupedShippingDetail")
    protected MaterialGroupedShippingResponse.ChangedMaterialGroupedShippingDetail changedMaterialGroupedShippingDetail;
    @XmlElement(name = "LineItemNote")
    protected String lineItemNote;
    @XmlElement(name = "ListOfStructuredNote")
    protected ListOfStructuredNote listOfStructuredNote;
    @XmlElement(name = "ListOfAttachment")
    protected ListOfAttachment listOfAttachment;

    /**
     * Gets the value of the detailResponseCoded property.
     * 
     * @return
     *     possible object is
     *     {@link DetailResponseCode }
     *     
     */
    public DetailResponseCode getDetailResponseCoded() {
        return detailResponseCoded;
    }

    /**
     * Sets the value of the detailResponseCoded property.
     * 
     * @param value
     *     allowed object is
     *     {@link DetailResponseCode }
     *     
     */
    public void setDetailResponseCoded(DetailResponseCode value) {
        this.detailResponseCoded = value;
    }

    /**
     * Gets the value of the detailResponseCodedOther property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDetailResponseCodedOther() {
        return detailResponseCodedOther;
    }

    /**
     * Sets the value of the detailResponseCodedOther property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDetailResponseCodedOther(String value) {
        this.detailResponseCodedOther = value;
    }

    /**
     * Gets the value of the originalMaterialGroupedShippingDetail property.
     * 
     * @return
     *     possible object is
     *     {@link MaterialGroupedShippingResponse.OriginalMaterialGroupedShippingDetail }
     *     
     */
    public MaterialGroupedShippingResponse.OriginalMaterialGroupedShippingDetail getOriginalMaterialGroupedShippingDetail() {
        return originalMaterialGroupedShippingDetail;
    }

    /**
     * Sets the value of the originalMaterialGroupedShippingDetail property.
     * 
     * @param value
     *     allowed object is
     *     {@link MaterialGroupedShippingResponse.OriginalMaterialGroupedShippingDetail }
     *     
     */
    public void setOriginalMaterialGroupedShippingDetail(MaterialGroupedShippingResponse.OriginalMaterialGroupedShippingDetail value) {
        this.originalMaterialGroupedShippingDetail = value;
    }

    /**
     * Gets the value of the changedMaterialGroupedShippingDetail property.
     * 
     * @return
     *     possible object is
     *     {@link MaterialGroupedShippingResponse.ChangedMaterialGroupedShippingDetail }
     *     
     */
    public MaterialGroupedShippingResponse.ChangedMaterialGroupedShippingDetail getChangedMaterialGroupedShippingDetail() {
        return changedMaterialGroupedShippingDetail;
    }

    /**
     * Sets the value of the changedMaterialGroupedShippingDetail property.
     * 
     * @param value
     *     allowed object is
     *     {@link MaterialGroupedShippingResponse.ChangedMaterialGroupedShippingDetail }
     *     
     */
    public void setChangedMaterialGroupedShippingDetail(MaterialGroupedShippingResponse.ChangedMaterialGroupedShippingDetail value) {
        this.changedMaterialGroupedShippingDetail = value;
    }

    /**
     * Gets the value of the lineItemNote property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLineItemNote() {
        return lineItemNote;
    }

    /**
     * Sets the value of the lineItemNote property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLineItemNote(String value) {
        this.lineItemNote = value;
    }

    /**
     * Gets the value of the listOfStructuredNote property.
     * 
     * @return
     *     possible object is
     *     {@link ListOfStructuredNote }
     *     
     */
    public ListOfStructuredNote getListOfStructuredNote() {
        return listOfStructuredNote;
    }

    /**
     * Sets the value of the listOfStructuredNote property.
     * 
     * @param value
     *     allowed object is
     *     {@link ListOfStructuredNote }
     *     
     */
    public void setListOfStructuredNote(ListOfStructuredNote value) {
        this.listOfStructuredNote = value;
    }

    /**
     * Gets the value of the listOfAttachment property.
     * 
     * @return
     *     possible object is
     *     {@link ListOfAttachment }
     *     
     */
    public ListOfAttachment getListOfAttachment() {
        return listOfAttachment;
    }

    /**
     * Sets the value of the listOfAttachment property.
     * 
     * @param value
     *     allowed object is
     *     {@link ListOfAttachment }
     *     
     */
    public void setListOfAttachment(ListOfAttachment value) {
        this.listOfAttachment = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element ref="{rrn:org.xcbl:schemas/xcbl/v3_5/xcbl35.xsd}MaterialGroupedShippingDetail"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "materialGroupedShippingDetail"
    })
    public static class ChangedMaterialGroupedShippingDetail
        implements Serializable
    {

        private final static long serialVersionUID = 1L;
        @XmlElement(name = "MaterialGroupedShippingDetail", required = true)
        protected MaterialGroupedShippingDetail materialGroupedShippingDetail;

        /**
         * Gets the value of the materialGroupedShippingDetail property.
         * 
         * @return
         *     possible object is
         *     {@link MaterialGroupedShippingDetail }
         *     
         */
        public MaterialGroupedShippingDetail getMaterialGroupedShippingDetail() {
            return materialGroupedShippingDetail;
        }

        /**
         * Sets the value of the materialGroupedShippingDetail property.
         * 
         * @param value
         *     allowed object is
         *     {@link MaterialGroupedShippingDetail }
         *     
         */
        public void setMaterialGroupedShippingDetail(MaterialGroupedShippingDetail value) {
            this.materialGroupedShippingDetail = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element ref="{rrn:org.xcbl:schemas/xcbl/v3_5/xcbl35.xsd}MaterialGroupedShippingDetail"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "materialGroupedShippingDetail"
    })
    public static class OriginalMaterialGroupedShippingDetail
        implements Serializable
    {

        private final static long serialVersionUID = 1L;
        @XmlElement(name = "MaterialGroupedShippingDetail", required = true)
        protected MaterialGroupedShippingDetail materialGroupedShippingDetail;

        /**
         * Gets the value of the materialGroupedShippingDetail property.
         * 
         * @return
         *     possible object is
         *     {@link MaterialGroupedShippingDetail }
         *     
         */
        public MaterialGroupedShippingDetail getMaterialGroupedShippingDetail() {
            return materialGroupedShippingDetail;
        }

        /**
         * Sets the value of the materialGroupedShippingDetail property.
         * 
         * @param value
         *     allowed object is
         *     {@link MaterialGroupedShippingDetail }
         *     
         */
        public void setMaterialGroupedShippingDetail(MaterialGroupedShippingDetail value) {
            this.materialGroupedShippingDetail = value;
        }

    }

}
