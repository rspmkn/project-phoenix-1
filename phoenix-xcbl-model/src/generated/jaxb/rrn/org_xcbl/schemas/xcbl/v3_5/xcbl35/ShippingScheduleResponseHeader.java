//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.5-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2014.08.13 at 10:13:13 PM EDT 
//


package rrn.org_xcbl.schemas.xcbl.v3_5.xcbl35;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ShippingScheduleResponseHeader complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ShippingScheduleResponseHeader">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ScheduleResponseID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ScheduleResponseIssueDate" type="{rrn:org.xcbl:schemas/xcbl/v3_5/xcbl35.xsd}xcblDatetime"/>
 *         &lt;element name="ScheduleReference">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element ref="{rrn:org.xcbl:schemas/xcbl/v3_5/xcbl35.xsd}Reference"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element ref="{rrn:org.xcbl:schemas/xcbl/v3_5/xcbl35.xsd}ListOfReferenceCoded" minOccurs="0"/>
 *         &lt;element name="BuyerParty">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element ref="{rrn:org.xcbl:schemas/xcbl/v3_5/xcbl35.xsd}Party"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="SellerParty">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element ref="{rrn:org.xcbl:schemas/xcbl/v3_5/xcbl35.xsd}Party"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element ref="{rrn:org.xcbl:schemas/xcbl/v3_5/xcbl35.xsd}Purpose"/>
 *         &lt;element ref="{rrn:org.xcbl:schemas/xcbl/v3_5/xcbl35.xsd}ResponseType"/>
 *         &lt;element ref="{rrn:org.xcbl:schemas/xcbl/v3_5/xcbl35.xsd}Language"/>
 *         &lt;element name="OriginalShippingScheduleHeader" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element ref="{rrn:org.xcbl:schemas/xcbl/v3_5/xcbl35.xsd}ShippingScheduleHeader"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="ChangedShippingScheduleHeader" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element ref="{rrn:org.xcbl:schemas/xcbl/v3_5/xcbl35.xsd}ShippingScheduleHeader"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="ShippingScheduleResponseHeaderNote" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element ref="{rrn:org.xcbl:schemas/xcbl/v3_5/xcbl35.xsd}ListOfStructuredNote" minOccurs="0"/>
 *         &lt;element ref="{rrn:org.xcbl:schemas/xcbl/v3_5/xcbl35.xsd}ListOfAttachment" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ShippingScheduleResponseHeader", propOrder = {
    "scheduleResponseID",
    "scheduleResponseIssueDate",
    "scheduleReference",
    "listOfReferenceCoded",
    "buyerParty",
    "sellerParty",
    "purpose",
    "responseType",
    "language",
    "originalShippingScheduleHeader",
    "changedShippingScheduleHeader",
    "shippingScheduleResponseHeaderNote",
    "listOfStructuredNote",
    "listOfAttachment"
})
public class ShippingScheduleResponseHeader
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "ScheduleResponseID", required = true)
    protected String scheduleResponseID;
    @XmlElement(name = "ScheduleResponseIssueDate", required = true)
    protected String scheduleResponseIssueDate;
    @XmlElement(name = "ScheduleReference", required = true)
    protected ShippingScheduleResponseHeader.ScheduleReference scheduleReference;
    @XmlElement(name = "ListOfReferenceCoded")
    protected ListOfReferenceCoded listOfReferenceCoded;
    @XmlElement(name = "BuyerParty", required = true)
    protected ShippingScheduleResponseHeader.BuyerParty buyerParty;
    @XmlElement(name = "SellerParty", required = true)
    protected ShippingScheduleResponseHeader.SellerParty sellerParty;
    @XmlElement(name = "Purpose", required = true)
    protected Purpose purpose;
    @XmlElement(name = "ResponseType", required = true)
    protected ResponseType responseType;
    @XmlElement(name = "Language", required = true)
    protected Language language;
    @XmlElement(name = "OriginalShippingScheduleHeader")
    protected ShippingScheduleResponseHeader.OriginalShippingScheduleHeader originalShippingScheduleHeader;
    @XmlElement(name = "ChangedShippingScheduleHeader")
    protected ShippingScheduleResponseHeader.ChangedShippingScheduleHeader changedShippingScheduleHeader;
    @XmlElement(name = "ShippingScheduleResponseHeaderNote")
    protected String shippingScheduleResponseHeaderNote;
    @XmlElement(name = "ListOfStructuredNote")
    protected ListOfStructuredNote listOfStructuredNote;
    @XmlElement(name = "ListOfAttachment")
    protected ListOfAttachment listOfAttachment;

    /**
     * Gets the value of the scheduleResponseID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getScheduleResponseID() {
        return scheduleResponseID;
    }

    /**
     * Sets the value of the scheduleResponseID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setScheduleResponseID(String value) {
        this.scheduleResponseID = value;
    }

    /**
     * Gets the value of the scheduleResponseIssueDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getScheduleResponseIssueDate() {
        return scheduleResponseIssueDate;
    }

    /**
     * Sets the value of the scheduleResponseIssueDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setScheduleResponseIssueDate(String value) {
        this.scheduleResponseIssueDate = value;
    }

    /**
     * Gets the value of the scheduleReference property.
     * 
     * @return
     *     possible object is
     *     {@link ShippingScheduleResponseHeader.ScheduleReference }
     *     
     */
    public ShippingScheduleResponseHeader.ScheduleReference getScheduleReference() {
        return scheduleReference;
    }

    /**
     * Sets the value of the scheduleReference property.
     * 
     * @param value
     *     allowed object is
     *     {@link ShippingScheduleResponseHeader.ScheduleReference }
     *     
     */
    public void setScheduleReference(ShippingScheduleResponseHeader.ScheduleReference value) {
        this.scheduleReference = value;
    }

    /**
     * Gets the value of the listOfReferenceCoded property.
     * 
     * @return
     *     possible object is
     *     {@link ListOfReferenceCoded }
     *     
     */
    public ListOfReferenceCoded getListOfReferenceCoded() {
        return listOfReferenceCoded;
    }

    /**
     * Sets the value of the listOfReferenceCoded property.
     * 
     * @param value
     *     allowed object is
     *     {@link ListOfReferenceCoded }
     *     
     */
    public void setListOfReferenceCoded(ListOfReferenceCoded value) {
        this.listOfReferenceCoded = value;
    }

    /**
     * Gets the value of the buyerParty property.
     * 
     * @return
     *     possible object is
     *     {@link ShippingScheduleResponseHeader.BuyerParty }
     *     
     */
    public ShippingScheduleResponseHeader.BuyerParty getBuyerParty() {
        return buyerParty;
    }

    /**
     * Sets the value of the buyerParty property.
     * 
     * @param value
     *     allowed object is
     *     {@link ShippingScheduleResponseHeader.BuyerParty }
     *     
     */
    public void setBuyerParty(ShippingScheduleResponseHeader.BuyerParty value) {
        this.buyerParty = value;
    }

    /**
     * Gets the value of the sellerParty property.
     * 
     * @return
     *     possible object is
     *     {@link ShippingScheduleResponseHeader.SellerParty }
     *     
     */
    public ShippingScheduleResponseHeader.SellerParty getSellerParty() {
        return sellerParty;
    }

    /**
     * Sets the value of the sellerParty property.
     * 
     * @param value
     *     allowed object is
     *     {@link ShippingScheduleResponseHeader.SellerParty }
     *     
     */
    public void setSellerParty(ShippingScheduleResponseHeader.SellerParty value) {
        this.sellerParty = value;
    }

    /**
     * Gets the value of the purpose property.
     * 
     * @return
     *     possible object is
     *     {@link Purpose }
     *     
     */
    public Purpose getPurpose() {
        return purpose;
    }

    /**
     * Sets the value of the purpose property.
     * 
     * @param value
     *     allowed object is
     *     {@link Purpose }
     *     
     */
    public void setPurpose(Purpose value) {
        this.purpose = value;
    }

    /**
     * Gets the value of the responseType property.
     * 
     * @return
     *     possible object is
     *     {@link ResponseType }
     *     
     */
    public ResponseType getResponseType() {
        return responseType;
    }

    /**
     * Sets the value of the responseType property.
     * 
     * @param value
     *     allowed object is
     *     {@link ResponseType }
     *     
     */
    public void setResponseType(ResponseType value) {
        this.responseType = value;
    }

    /**
     * Gets the value of the language property.
     * 
     * @return
     *     possible object is
     *     {@link Language }
     *     
     */
    public Language getLanguage() {
        return language;
    }

    /**
     * Sets the value of the language property.
     * 
     * @param value
     *     allowed object is
     *     {@link Language }
     *     
     */
    public void setLanguage(Language value) {
        this.language = value;
    }

    /**
     * Gets the value of the originalShippingScheduleHeader property.
     * 
     * @return
     *     possible object is
     *     {@link ShippingScheduleResponseHeader.OriginalShippingScheduleHeader }
     *     
     */
    public ShippingScheduleResponseHeader.OriginalShippingScheduleHeader getOriginalShippingScheduleHeader() {
        return originalShippingScheduleHeader;
    }

    /**
     * Sets the value of the originalShippingScheduleHeader property.
     * 
     * @param value
     *     allowed object is
     *     {@link ShippingScheduleResponseHeader.OriginalShippingScheduleHeader }
     *     
     */
    public void setOriginalShippingScheduleHeader(ShippingScheduleResponseHeader.OriginalShippingScheduleHeader value) {
        this.originalShippingScheduleHeader = value;
    }

    /**
     * Gets the value of the changedShippingScheduleHeader property.
     * 
     * @return
     *     possible object is
     *     {@link ShippingScheduleResponseHeader.ChangedShippingScheduleHeader }
     *     
     */
    public ShippingScheduleResponseHeader.ChangedShippingScheduleHeader getChangedShippingScheduleHeader() {
        return changedShippingScheduleHeader;
    }

    /**
     * Sets the value of the changedShippingScheduleHeader property.
     * 
     * @param value
     *     allowed object is
     *     {@link ShippingScheduleResponseHeader.ChangedShippingScheduleHeader }
     *     
     */
    public void setChangedShippingScheduleHeader(ShippingScheduleResponseHeader.ChangedShippingScheduleHeader value) {
        this.changedShippingScheduleHeader = value;
    }

    /**
     * Gets the value of the shippingScheduleResponseHeaderNote property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShippingScheduleResponseHeaderNote() {
        return shippingScheduleResponseHeaderNote;
    }

    /**
     * Sets the value of the shippingScheduleResponseHeaderNote property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShippingScheduleResponseHeaderNote(String value) {
        this.shippingScheduleResponseHeaderNote = value;
    }

    /**
     * Gets the value of the listOfStructuredNote property.
     * 
     * @return
     *     possible object is
     *     {@link ListOfStructuredNote }
     *     
     */
    public ListOfStructuredNote getListOfStructuredNote() {
        return listOfStructuredNote;
    }

    /**
     * Sets the value of the listOfStructuredNote property.
     * 
     * @param value
     *     allowed object is
     *     {@link ListOfStructuredNote }
     *     
     */
    public void setListOfStructuredNote(ListOfStructuredNote value) {
        this.listOfStructuredNote = value;
    }

    /**
     * Gets the value of the listOfAttachment property.
     * 
     * @return
     *     possible object is
     *     {@link ListOfAttachment }
     *     
     */
    public ListOfAttachment getListOfAttachment() {
        return listOfAttachment;
    }

    /**
     * Sets the value of the listOfAttachment property.
     * 
     * @param value
     *     allowed object is
     *     {@link ListOfAttachment }
     *     
     */
    public void setListOfAttachment(ListOfAttachment value) {
        this.listOfAttachment = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element ref="{rrn:org.xcbl:schemas/xcbl/v3_5/xcbl35.xsd}Party"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "party"
    })
    public static class BuyerParty
        implements Serializable
    {

        private final static long serialVersionUID = 1L;
        @XmlElement(name = "Party", required = true)
        protected Party party;

        /**
         * Gets the value of the party property.
         * 
         * @return
         *     possible object is
         *     {@link Party }
         *     
         */
        public Party getParty() {
            return party;
        }

        /**
         * Sets the value of the party property.
         * 
         * @param value
         *     allowed object is
         *     {@link Party }
         *     
         */
        public void setParty(Party value) {
            this.party = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element ref="{rrn:org.xcbl:schemas/xcbl/v3_5/xcbl35.xsd}ShippingScheduleHeader"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "shippingScheduleHeader"
    })
    public static class ChangedShippingScheduleHeader
        implements Serializable
    {

        private final static long serialVersionUID = 1L;
        @XmlElement(name = "ShippingScheduleHeader", required = true)
        protected ShippingScheduleHeader shippingScheduleHeader;

        /**
         * Gets the value of the shippingScheduleHeader property.
         * 
         * @return
         *     possible object is
         *     {@link ShippingScheduleHeader }
         *     
         */
        public ShippingScheduleHeader getShippingScheduleHeader() {
            return shippingScheduleHeader;
        }

        /**
         * Sets the value of the shippingScheduleHeader property.
         * 
         * @param value
         *     allowed object is
         *     {@link ShippingScheduleHeader }
         *     
         */
        public void setShippingScheduleHeader(ShippingScheduleHeader value) {
            this.shippingScheduleHeader = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element ref="{rrn:org.xcbl:schemas/xcbl/v3_5/xcbl35.xsd}ShippingScheduleHeader"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "shippingScheduleHeader"
    })
    public static class OriginalShippingScheduleHeader
        implements Serializable
    {

        private final static long serialVersionUID = 1L;
        @XmlElement(name = "ShippingScheduleHeader", required = true)
        protected ShippingScheduleHeader shippingScheduleHeader;

        /**
         * Gets the value of the shippingScheduleHeader property.
         * 
         * @return
         *     possible object is
         *     {@link ShippingScheduleHeader }
         *     
         */
        public ShippingScheduleHeader getShippingScheduleHeader() {
            return shippingScheduleHeader;
        }

        /**
         * Sets the value of the shippingScheduleHeader property.
         * 
         * @param value
         *     allowed object is
         *     {@link ShippingScheduleHeader }
         *     
         */
        public void setShippingScheduleHeader(ShippingScheduleHeader value) {
            this.shippingScheduleHeader = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element ref="{rrn:org.xcbl:schemas/xcbl/v3_5/xcbl35.xsd}Reference"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "reference"
    })
    public static class ScheduleReference
        implements Serializable
    {

        private final static long serialVersionUID = 1L;
        @XmlElement(name = "Reference", required = true)
        protected Reference reference;

        /**
         * Gets the value of the reference property.
         * 
         * @return
         *     possible object is
         *     {@link Reference }
         *     
         */
        public Reference getReference() {
            return reference;
        }

        /**
         * Sets the value of the reference property.
         * 
         * @param value
         *     allowed object is
         *     {@link Reference }
         *     
         */
        public void setReference(Reference value) {
            this.reference = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element ref="{rrn:org.xcbl:schemas/xcbl/v3_5/xcbl35.xsd}Party"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "party"
    })
    public static class SellerParty
        implements Serializable
    {

        private final static long serialVersionUID = 1L;
        @XmlElement(name = "Party", required = true)
        protected Party party;

        /**
         * Gets the value of the party property.
         * 
         * @return
         *     possible object is
         *     {@link Party }
         *     
         */
        public Party getParty() {
            return party;
        }

        /**
         * Sets the value of the party property.
         * 
         * @param value
         *     allowed object is
         *     {@link Party }
         *     
         */
        public void setParty(Party value) {
            this.party = value;
        }

    }

}
