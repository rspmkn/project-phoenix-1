//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.5-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2014.08.13 at 10:13:13 PM EDT 
//


package rrn.org_xcbl.schemas.xcbl.v3_5.xcbl35;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TradingPartnerOrganizationDeletion complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TradingPartnerOrganizationDeletion">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="TradingPartnerIdentifications">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element ref="{rrn:org.xcbl:schemas/xcbl/v3_5/xcbl35.xsd}Identifications"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="TradingPartnerDisplayName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TradingPartnerOrganizationDeletion", propOrder = {
    "tradingPartnerIdentifications",
    "tradingPartnerDisplayName"
})
public class TradingPartnerOrganizationDeletion
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "TradingPartnerIdentifications", required = true)
    protected TradingPartnerOrganizationDeletion.TradingPartnerIdentifications tradingPartnerIdentifications;
    @XmlElement(name = "TradingPartnerDisplayName", required = true)
    protected String tradingPartnerDisplayName;

    /**
     * Gets the value of the tradingPartnerIdentifications property.
     * 
     * @return
     *     possible object is
     *     {@link TradingPartnerOrganizationDeletion.TradingPartnerIdentifications }
     *     
     */
    public TradingPartnerOrganizationDeletion.TradingPartnerIdentifications getTradingPartnerIdentifications() {
        return tradingPartnerIdentifications;
    }

    /**
     * Sets the value of the tradingPartnerIdentifications property.
     * 
     * @param value
     *     allowed object is
     *     {@link TradingPartnerOrganizationDeletion.TradingPartnerIdentifications }
     *     
     */
    public void setTradingPartnerIdentifications(TradingPartnerOrganizationDeletion.TradingPartnerIdentifications value) {
        this.tradingPartnerIdentifications = value;
    }

    /**
     * Gets the value of the tradingPartnerDisplayName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTradingPartnerDisplayName() {
        return tradingPartnerDisplayName;
    }

    /**
     * Sets the value of the tradingPartnerDisplayName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTradingPartnerDisplayName(String value) {
        this.tradingPartnerDisplayName = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element ref="{rrn:org.xcbl:schemas/xcbl/v3_5/xcbl35.xsd}Identifications"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "identifications"
    })
    public static class TradingPartnerIdentifications
        implements Serializable
    {

        private final static long serialVersionUID = 1L;
        @XmlElement(name = "Identifications", required = true)
        protected Identifications identifications;

        /**
         * Gets the value of the identifications property.
         * 
         * @return
         *     possible object is
         *     {@link Identifications }
         *     
         */
        public Identifications getIdentifications() {
            return identifications;
        }

        /**
         * Sets the value of the identifications property.
         * 
         * @param value
         *     allowed object is
         *     {@link Identifications }
         *     
         */
        public void setIdentifications(Identifications value) {
            this.identifications = value;
        }

    }

}
