//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.5-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2014.08.13 at 10:13:13 PM EDT 
//


package rrn.org_xcbl.schemas.xcbl.v3_5.xcbl35;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ValidBidCurrency complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ValidBidCurrency">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="BidCurrency">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element ref="{rrn:org.xcbl:schemas/xcbl/v3_5/xcbl35.xsd}Currency"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element ref="{rrn:org.xcbl:schemas/xcbl/v3_5/xcbl35.xsd}RateOfExchangeDetail" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ValidBidCurrency", propOrder = {
    "bidCurrency",
    "rateOfExchangeDetail"
})
public class ValidBidCurrency
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "BidCurrency", required = true)
    protected ValidBidCurrency.BidCurrency bidCurrency;
    @XmlElement(name = "RateOfExchangeDetail")
    protected RateOfExchangeDetail rateOfExchangeDetail;

    /**
     * Gets the value of the bidCurrency property.
     * 
     * @return
     *     possible object is
     *     {@link ValidBidCurrency.BidCurrency }
     *     
     */
    public ValidBidCurrency.BidCurrency getBidCurrency() {
        return bidCurrency;
    }

    /**
     * Sets the value of the bidCurrency property.
     * 
     * @param value
     *     allowed object is
     *     {@link ValidBidCurrency.BidCurrency }
     *     
     */
    public void setBidCurrency(ValidBidCurrency.BidCurrency value) {
        this.bidCurrency = value;
    }

    /**
     * Gets the value of the rateOfExchangeDetail property.
     * 
     * @return
     *     possible object is
     *     {@link RateOfExchangeDetail }
     *     
     */
    public RateOfExchangeDetail getRateOfExchangeDetail() {
        return rateOfExchangeDetail;
    }

    /**
     * Sets the value of the rateOfExchangeDetail property.
     * 
     * @param value
     *     allowed object is
     *     {@link RateOfExchangeDetail }
     *     
     */
    public void setRateOfExchangeDetail(RateOfExchangeDetail value) {
        this.rateOfExchangeDetail = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element ref="{rrn:org.xcbl:schemas/xcbl/v3_5/xcbl35.xsd}Currency"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "currency"
    })
    public static class BidCurrency
        implements Serializable
    {

        private final static long serialVersionUID = 1L;
        @XmlElement(name = "Currency", required = true)
        protected Currency currency;

        /**
         * Gets the value of the currency property.
         * 
         * @return
         *     possible object is
         *     {@link Currency }
         *     
         */
        public Currency getCurrency() {
            return currency;
        }

        /**
         * Sets the value of the currency property.
         * 
         * @param value
         *     allowed object is
         *     {@link Currency }
         *     
         */
        public void setCurrency(Currency value) {
            this.currency = value;
        }

    }

}
