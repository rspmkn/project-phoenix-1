//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.5-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2014.08.13 at 10:13:13 PM EDT 
//


package rrn.org_xcbl.schemas.xcbl.v3_5.xcbl35;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CategoryAttribute complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CategoryAttribute">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AttributeID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element ref="{rrn:org.xcbl:schemas/xcbl/v3_5/xcbl35.xsd}AttributeName" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element ref="{rrn:org.xcbl:schemas/xcbl/v3_5/xcbl35.xsd}AttributeType"/>
 *         &lt;element name="DefaultUOM" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element ref="{rrn:org.xcbl:schemas/xcbl/v3_5/xcbl35.xsd}UOM"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element ref="{rrn:org.xcbl:schemas/xcbl/v3_5/xcbl35.xsd}IsRequired" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CategoryAttribute", propOrder = {
    "attributeID",
    "attributeName",
    "attributeType",
    "defaultUOM",
    "isRequired"
})
public class CategoryAttribute
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "AttributeID", required = true)
    protected String attributeID;
    @XmlElement(name = "AttributeName")
    protected List<AttributeName> attributeName;
    @XmlElement(name = "AttributeType", required = true)
    protected AttributeType attributeType;
    @XmlElement(name = "DefaultUOM")
    protected CategoryAttribute.DefaultUOM defaultUOM;
    @XmlElement(name = "IsRequired")
    protected IsRequired isRequired;

    /**
     * Gets the value of the attributeID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAttributeID() {
        return attributeID;
    }

    /**
     * Sets the value of the attributeID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAttributeID(String value) {
        this.attributeID = value;
    }

    /**
     * Gets the value of the attributeName property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the attributeName property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAttributeName().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AttributeName }
     * 
     * 
     */
    public List<AttributeName> getAttributeName() {
        if (attributeName == null) {
            attributeName = new ArrayList<AttributeName>();
        }
        return this.attributeName;
    }

    /**
     * Gets the value of the attributeType property.
     * 
     * @return
     *     possible object is
     *     {@link AttributeType }
     *     
     */
    public AttributeType getAttributeType() {
        return attributeType;
    }

    /**
     * Sets the value of the attributeType property.
     * 
     * @param value
     *     allowed object is
     *     {@link AttributeType }
     *     
     */
    public void setAttributeType(AttributeType value) {
        this.attributeType = value;
    }

    /**
     * Gets the value of the defaultUOM property.
     * 
     * @return
     *     possible object is
     *     {@link CategoryAttribute.DefaultUOM }
     *     
     */
    public CategoryAttribute.DefaultUOM getDefaultUOM() {
        return defaultUOM;
    }

    /**
     * Sets the value of the defaultUOM property.
     * 
     * @param value
     *     allowed object is
     *     {@link CategoryAttribute.DefaultUOM }
     *     
     */
    public void setDefaultUOM(CategoryAttribute.DefaultUOM value) {
        this.defaultUOM = value;
    }

    /**
     * Gets the value of the isRequired property.
     * 
     * @return
     *     possible object is
     *     {@link IsRequired }
     *     
     */
    public IsRequired getIsRequired() {
        return isRequired;
    }

    /**
     * Sets the value of the isRequired property.
     * 
     * @param value
     *     allowed object is
     *     {@link IsRequired }
     *     
     */
    public void setIsRequired(IsRequired value) {
        this.isRequired = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element ref="{rrn:org.xcbl:schemas/xcbl/v3_5/xcbl35.xsd}UOM"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "uom"
    })
    public static class DefaultUOM
        implements Serializable
    {

        private final static long serialVersionUID = 1L;
        @XmlElement(name = "UOM", required = true)
        protected UOM uom;

        /**
         * Gets the value of the uom property.
         * 
         * @return
         *     possible object is
         *     {@link UOM }
         *     
         */
        public UOM getUOM() {
            return uom;
        }

        /**
         * Sets the value of the uom property.
         * 
         * @param value
         *     allowed object is
         *     {@link UOM }
         *     
         */
        public void setUOM(UOM value) {
            this.uom = value;
        }

    }

}
