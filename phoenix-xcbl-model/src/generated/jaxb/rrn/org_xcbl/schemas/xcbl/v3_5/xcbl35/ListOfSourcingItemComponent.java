//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.5-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2014.08.13 at 10:13:13 PM EDT 
//


package rrn.org_xcbl.schemas.xcbl.v3_5.xcbl35;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ListOfSourcingItemComponent complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ListOfSourcingItemComponent">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SourcingItemComponent" maxOccurs="unbounded">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element ref="{rrn:org.xcbl:schemas/xcbl/v3_5/xcbl35.xsd}SourcingCreateDetail"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ListOfSourcingItemComponent", propOrder = {
    "sourcingItemComponent"
})
public class ListOfSourcingItemComponent
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "SourcingItemComponent", required = true)
    protected List<ListOfSourcingItemComponent.SourcingItemComponent> sourcingItemComponent;

    /**
     * Gets the value of the sourcingItemComponent property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the sourcingItemComponent property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSourcingItemComponent().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ListOfSourcingItemComponent.SourcingItemComponent }
     * 
     * 
     */
    public List<ListOfSourcingItemComponent.SourcingItemComponent> getSourcingItemComponent() {
        if (sourcingItemComponent == null) {
            sourcingItemComponent = new ArrayList<ListOfSourcingItemComponent.SourcingItemComponent>();
        }
        return this.sourcingItemComponent;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element ref="{rrn:org.xcbl:schemas/xcbl/v3_5/xcbl35.xsd}SourcingCreateDetail"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "sourcingCreateDetail"
    })
    public static class SourcingItemComponent
        implements Serializable
    {

        private final static long serialVersionUID = 1L;
        @XmlElement(name = "SourcingCreateDetail", required = true)
        protected SourcingCreateDetail sourcingCreateDetail;

        /**
         * Gets the value of the sourcingCreateDetail property.
         * 
         * @return
         *     possible object is
         *     {@link SourcingCreateDetail }
         *     
         */
        public SourcingCreateDetail getSourcingCreateDetail() {
            return sourcingCreateDetail;
        }

        /**
         * Sets the value of the sourcingCreateDetail property.
         * 
         * @param value
         *     allowed object is
         *     {@link SourcingCreateDetail }
         *     
         */
        public void setSourcingCreateDetail(SourcingCreateDetail value) {
            this.sourcingCreateDetail = value;
        }

    }

}
