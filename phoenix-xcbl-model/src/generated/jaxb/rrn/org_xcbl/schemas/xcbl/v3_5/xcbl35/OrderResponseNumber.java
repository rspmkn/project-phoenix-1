//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.5-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2014.08.13 at 10:13:13 PM EDT 
//


package rrn.org_xcbl.schemas.xcbl.v3_5.xcbl35;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for OrderResponseNumber complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="OrderResponseNumber">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="BuyerOrderResponseNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="SellerOrderResponseNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element ref="{rrn:org.xcbl:schemas/xcbl/v3_5/xcbl35.xsd}ListOfMessageID" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OrderResponseNumber", propOrder = {
    "buyerOrderResponseNumber",
    "sellerOrderResponseNumber",
    "listOfMessageID"
})
public class OrderResponseNumber
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "BuyerOrderResponseNumber", required = true)
    protected String buyerOrderResponseNumber;
    @XmlElement(name = "SellerOrderResponseNumber")
    protected String sellerOrderResponseNumber;
    @XmlElement(name = "ListOfMessageID")
    protected ListOfMessageID listOfMessageID;

    /**
     * Gets the value of the buyerOrderResponseNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBuyerOrderResponseNumber() {
        return buyerOrderResponseNumber;
    }

    /**
     * Sets the value of the buyerOrderResponseNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBuyerOrderResponseNumber(String value) {
        this.buyerOrderResponseNumber = value;
    }

    /**
     * Gets the value of the sellerOrderResponseNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSellerOrderResponseNumber() {
        return sellerOrderResponseNumber;
    }

    /**
     * Sets the value of the sellerOrderResponseNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSellerOrderResponseNumber(String value) {
        this.sellerOrderResponseNumber = value;
    }

    /**
     * Gets the value of the listOfMessageID property.
     * 
     * @return
     *     possible object is
     *     {@link ListOfMessageID }
     *     
     */
    public ListOfMessageID getListOfMessageID() {
        return listOfMessageID;
    }

    /**
     * Sets the value of the listOfMessageID property.
     * 
     * @param value
     *     allowed object is
     *     {@link ListOfMessageID }
     *     
     */
    public void setListOfMessageID(ListOfMessageID value) {
        this.listOfMessageID = value;
    }

}
