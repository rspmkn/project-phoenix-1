//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.5-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2014.08.13 at 10:13:13 PM EDT 
//


package rrn.org_xcbl.schemas.xcbl.v3_5.xcbl35;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ChangeOrderDetail complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ChangeOrderDetail">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{rrn:org.xcbl:schemas/xcbl/v3_5/xcbl35.xsd}ListOfChangeOrderItemDetail" minOccurs="0"/>
 *         &lt;element ref="{rrn:org.xcbl:schemas/xcbl/v3_5/xcbl35.xsd}ListOfChangeOrderPackageDetail" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ChangeOrderDetail", propOrder = {
    "listOfChangeOrderItemDetail",
    "listOfChangeOrderPackageDetail"
})
public class ChangeOrderDetail
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "ListOfChangeOrderItemDetail")
    protected ListOfChangeOrderItemDetail listOfChangeOrderItemDetail;
    @XmlElement(name = "ListOfChangeOrderPackageDetail")
    protected ListOfChangeOrderPackageDetail listOfChangeOrderPackageDetail;

    /**
     * Gets the value of the listOfChangeOrderItemDetail property.
     * 
     * @return
     *     possible object is
     *     {@link ListOfChangeOrderItemDetail }
     *     
     */
    public ListOfChangeOrderItemDetail getListOfChangeOrderItemDetail() {
        return listOfChangeOrderItemDetail;
    }

    /**
     * Sets the value of the listOfChangeOrderItemDetail property.
     * 
     * @param value
     *     allowed object is
     *     {@link ListOfChangeOrderItemDetail }
     *     
     */
    public void setListOfChangeOrderItemDetail(ListOfChangeOrderItemDetail value) {
        this.listOfChangeOrderItemDetail = value;
    }

    /**
     * Gets the value of the listOfChangeOrderPackageDetail property.
     * 
     * @return
     *     possible object is
     *     {@link ListOfChangeOrderPackageDetail }
     *     
     */
    public ListOfChangeOrderPackageDetail getListOfChangeOrderPackageDetail() {
        return listOfChangeOrderPackageDetail;
    }

    /**
     * Sets the value of the listOfChangeOrderPackageDetail property.
     * 
     * @param value
     *     allowed object is
     *     {@link ListOfChangeOrderPackageDetail }
     *     
     */
    public void setListOfChangeOrderPackageDetail(ListOfChangeOrderPackageDetail value) {
        this.listOfChangeOrderPackageDetail = value;
    }

}
