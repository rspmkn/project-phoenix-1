package com.apd.phoenix.service.card.management;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>Java class for RenameCustomerCodeParams complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RenameCustomerCodeParams">
 *   &lt;complexContent>
 *     &lt;extension base="{http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Parameters.CreditCardManagement}CreditCardManagementParams">
 *       &lt;sequence>
 *         &lt;element name="CustomerIdentifier" type="{http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions}CustomerIdentifier" minOccurs="0"/>
 *         &lt;element name="NewCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RenameCustomerCodeParams", namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Parameters.CreditCardManagement", propOrder = {
        "customerIdentifier", "newCode" })
public class RenameCustomerCodeParams extends CreditCardManagementParams {

    @XmlElementRef(name = "CustomerIdentifier", namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Parameters.CreditCardManagement", type = JAXBElement.class, required = false)
    protected JAXBElement<CustomerIdentifier> customerIdentifier;
    @XmlElementRef(name = "NewCode", namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Parameters.CreditCardManagement", type = JAXBElement.class, required = false)
    protected JAXBElement<String> newCode;

    /**
     * Gets the value of the customerIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link CustomerIdentifier }{@code >}
     *     
     */
    public JAXBElement<CustomerIdentifier> getCustomerIdentifier() {
        return customerIdentifier;
    }

    /**
     * Sets the value of the customerIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link CustomerIdentifier }{@code >}
     *     
     */
    public void setCustomerIdentifier(JAXBElement<CustomerIdentifier> value) {
        this.customerIdentifier = ((JAXBElement<CustomerIdentifier>) value);
    }

    /**
     * Gets the value of the newCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getNewCode() {
        return newCode;
    }

    /**
     * Sets the value of the newCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setNewCode(JAXBElement<String> value) {
        this.newCode = ((JAXBElement<String>) value);
    }

}
