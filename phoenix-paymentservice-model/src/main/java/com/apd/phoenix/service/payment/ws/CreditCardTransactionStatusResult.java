package com.apd.phoenix.service.payment.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for CreditCardTransactionStatusResult complex type.
 *
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 *
 * <pre>
 * &lt;complexType name="CreditCardTransactionStatusResult">
 *   &lt;complexContent>
 *     &lt;extension base="{http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardTransactionDefinitions}CreditCardTransactionAuthorizationResult">
 *       &lt;sequence>
 *         &lt;element name="SummaryStatus" type="{http://schemas.datacontract.org/2004/07/ThreeDelta.Common.Enums}CreditCardTransactionSummaryStatus" minOccurs="0"/>
 *         &lt;element name="TransactionType" type="{http://schemas.datacontract.org/2004/07/ThreeDelta.Common.Enums}TransactionType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CreditCardTransactionStatusResult", namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardTransactionDefinitions", propOrder = {
        "summaryStatus", "transactionType" })
public class CreditCardTransactionStatusResult extends CreditCardTransactionAuthorizationResult {

    @XmlElement(name = "SummaryStatus")
    protected CreditCardTransactionSummaryStatus summaryStatus;
    @XmlElement(name = "TransactionType")
    protected TransactionType transactionType;

    /**
     * Gets the value of the summaryStatus property.
     *
     * @return possible object is {@link CreditCardTransactionSummaryStatus }
     *
     */
    public CreditCardTransactionSummaryStatus getSummaryStatus() {
        return summaryStatus;
    }

    /**
     * Sets the value of the summaryStatus property.
     *
     * @param value allowed object is
     *     {@link CreditCardTransactionSummaryStatus }
     *
     */
    public void setSummaryStatus(CreditCardTransactionSummaryStatus value) {
        this.summaryStatus = value;
    }

    /**
     * Gets the value of the transactionType property.
     *
     * @return possible object is {@link TransactionType }
     *
     */
    public TransactionType getTransactionType() {
        return transactionType;
    }

    /**
     * Sets the value of the transactionType property.
     *
     * @param value allowed object is {@link TransactionType }
     *
     */
    public void setTransactionType(TransactionType value) {
        this.transactionType = value;
    }
}
