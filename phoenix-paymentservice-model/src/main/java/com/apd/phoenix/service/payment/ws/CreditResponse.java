package com.apd.phoenix.service.payment.ws;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for anonymous complex type.
 *
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 *
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CreditResult" type="{http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardTransactionDefinitions}CreditCardTransactionResult" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "creditResult" })
@XmlRootElement(name = "CreditResponse")
public class CreditResponse {

    @XmlElementRef(name = "CreditResult", namespace = "http://3DSI.org/WebServices/CreditCardTransaction", type = JAXBElement.class, required = false)
    protected JAXBElement<CreditCardTransactionResult> creditResult;

    /**
     * Gets the value of the creditResult property.
     *
     * @return possible object is
     * {@link JAXBElement }{@code <}{@link CreditCardTransactionResult }{@code >}
     *
     */
    public JAXBElement<CreditCardTransactionResult> getCreditResult() {
        return creditResult;
    }

    /**
     * Sets the value of the creditResult property.
     *
     * @param value allowed object is
     * {@link JAXBElement }{@code <}{@link CreditCardTransactionResult }{@code >}
     *
     */
    public void setCreditResult(JAXBElement<CreditCardTransactionResult> value) {
        this.creditResult = value;
    }
}
