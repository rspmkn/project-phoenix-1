package com.apd.phoenix.service.payment.ws;

import com.apd.phoenix.service.payment.utilities.PaymentGatewayPropertiesLoader;
import java.net.URL;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceFeature;
import org.apache.cxf.frontend.ClientProxy;
import org.apache.cxf.interceptor.LoggingInInterceptor;
import org.apache.cxf.interceptor.LoggingOutInterceptor;
import org.apache.cxf.jaxws.JaxWsProxyFactoryBean;
import org.apache.cxf.transport.http.HTTPConduit;
import org.apache.cxf.transports.http.configuration.HTTPClientPolicy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class was generated by Apache CXF 2.7.6 2013-09-19T16:41:57.802-04:00
 * Generated source version: 2.7.6
 *
 */
@WebServiceClient(name = "CreditCardTransactionService", wsdlLocation = "https://services.pwsdemo.com/CreditCardTransactionService.svc", targetNamespace = "http://3DSI.org/WebServices/CreditCardTransaction")
public class CreditCardTransactionService extends Service {

    private static final Logger logger = LoggerFactory.getLogger(CreditCardTransactionService.class);
    public static URL WSDL_LOCATION;
    public static final String SERVICE_URL = PaymentGatewayPropertiesLoader.getInstance().getProperties().getString(
            "serviceUrl");
    private static final String SHOULD_LOG_MESSAGES = PaymentGatewayPropertiesLoader.getInstance().getProperties()
            .getString("logTransactionContent", "false");
    public final static QName SERVICE = new QName("http://3DSI.org/WebServices/CreditCardTransaction",
            "CreditCardTransactionService");
    public final static QName CreditCardTransactionSoap = new QName(
            "http://3DSI.org/WebServices/CreditCardTransaction", "CreditCardTransactionSoap");

    static {
        URL url = null;
        url = CreditCardTransactionService.class.getClassLoader().getResource(
                "wsdl/PwsDemo_creditcardtransactionservice.wsdl");

        WSDL_LOCATION = url;
    }

    public CreditCardTransactionService(URL wsdlLocation) {
        super(wsdlLocation, SERVICE);
    }

    public CreditCardTransactionService(URL wsdlLocation, QName serviceName) {
        super(wsdlLocation, serviceName);
    }

    public CreditCardTransactionService() {
        super(WSDL_LOCATION, SERVICE);
    }

    //This constructor requires JAX-WS API 2.2. You will need to endorse the 2.2
    //API jar or re-run wsdl2java with "-frontend jaxws21" to generate JAX-WS 2.1
    //compliant code instead.
    public CreditCardTransactionService(WebServiceFeature... features) {
        super(WSDL_LOCATION, SERVICE, features);
    }

    //This constructor requires JAX-WS API 2.2. You will need to endorse the 2.2
    //API jar or re-run wsdl2java with "-frontend jaxws21" to generate JAX-WS 2.1
    //compliant code instead.
    public CreditCardTransactionService(URL wsdlLocation, WebServiceFeature... features) {
        super(wsdlLocation, SERVICE, features);
    }

    //This constructor requires JAX-WS API 2.2. You will need to endorse the 2.2
    //API jar or re-run wsdl2java with "-frontend jaxws21" to generate JAX-WS 2.1
    //compliant code instead.
    public CreditCardTransactionService(URL wsdlLocation, QName serviceName, WebServiceFeature... features) {
        super(wsdlLocation, serviceName, features);
    }

    /**
     *
     * @return returns ICreditCardTransaction
     */
    @WebEndpoint(name = "CreditCardTransactionSoap")
    public ICreditCardTransaction getCreditCardTransactionSoap() {
        return super.getPort(CreditCardTransactionSoap, ICreditCardTransaction.class);
    }

    /**
     *
     * @param features A list of {@link javax.xml.ws.WebServiceFeature} to
     * configure on the proxy. Supported features not in * the
     * <code>features</code> parameter will have their default values.
     * @return returns ICreditCardTransaction
     */
    @WebEndpoint(name = "CreditCardTransactionSoap")
    public ICreditCardTransaction getCreditCardTransactionSoap(WebServiceFeature... features) {
        return super.getPort(CreditCardTransactionSoap, ICreditCardTransaction.class, features);
    }

    public ICreditCardTransaction getCreditCardTransationService() {
        JaxWsProxyFactoryBean proxyFactory = new JaxWsProxyFactoryBean();

        if ("true".equals(SHOULD_LOG_MESSAGES)) {
            proxyFactory.getInInterceptors().add(new LoggingInInterceptor());
            proxyFactory.getOutInterceptors().add(new LoggingOutInterceptor());
        }

        proxyFactory.setServiceClass(ICreditCardTransaction.class);
        proxyFactory.setAddress(SERVICE_URL);
        ICreditCardTransaction port = (ICreditCardTransaction) proxyFactory.create();
        HTTPConduit httpConduit = (HTTPConduit) ClientProxy.getClient(port).getConduit();
        HTTPClientPolicy httpClientPolicy = new HTTPClientPolicy();
        if (httpConduit.getClient() != null) {
            httpClientPolicy = httpConduit.getClient();
        }
        httpClientPolicy.setConnectionTimeout(36000);
        httpClientPolicy.setAllowChunking(false);
        httpConduit.setClient(httpClientPolicy);
        return port;
    }
}
