package com.apd.phoenix.service.card.management;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="clientCredentials" type="{http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions}ClientCredentials" minOccurs="0"/>
 *         &lt;element name="updateStoredCardExpirationDateParams" type="{http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Parameters.CreditCardManagement}UpdateStoredCardExpirationDateParams" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "clientCredentials", "updateStoredCardExpirationDateParams" })
@XmlRootElement(name = "UpdateStoredCreditCardExpirationDate")
public class UpdateStoredCreditCardExpirationDate {

    @XmlElementRef(name = "clientCredentials", namespace = "http://3DSI.org/WebServices/CreditCardManagement", type = JAXBElement.class, required = false)
    protected JAXBElement<ClientCredentials> clientCredentials;
    @XmlElementRef(name = "updateStoredCardExpirationDateParams", namespace = "http://3DSI.org/WebServices/CreditCardManagement", type = JAXBElement.class, required = false)
    protected JAXBElement<UpdateStoredCardExpirationDateParams> updateStoredCardExpirationDateParams;

    /**
     * Gets the value of the clientCredentials property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ClientCredentials }{@code >}
     *     
     */
    public JAXBElement<ClientCredentials> getClientCredentials() {
        return clientCredentials;
    }

    /**
     * Sets the value of the clientCredentials property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ClientCredentials }{@code >}
     *     
     */
    public void setClientCredentials(JAXBElement<ClientCredentials> value) {
        this.clientCredentials = ((JAXBElement<ClientCredentials>) value);
    }

    /**
     * Gets the value of the updateStoredCardExpirationDateParams property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link UpdateStoredCardExpirationDateParams }{@code >}
     *     
     */
    public JAXBElement<UpdateStoredCardExpirationDateParams> getUpdateStoredCardExpirationDateParams() {
        return updateStoredCardExpirationDateParams;
    }

    /**
     * Sets the value of the updateStoredCardExpirationDateParams property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link UpdateStoredCardExpirationDateParams }{@code >}
     *     
     */
    public void setUpdateStoredCardExpirationDateParams(JAXBElement<UpdateStoredCardExpirationDateParams> value) {
        this.updateStoredCardExpirationDateParams = ((JAXBElement<UpdateStoredCardExpirationDateParams>) value);
    }

}
