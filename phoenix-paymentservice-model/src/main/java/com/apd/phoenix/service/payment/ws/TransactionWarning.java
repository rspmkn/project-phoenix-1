package com.apd.phoenix.service.payment.ws;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for TransactionWarning.
 *
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * <p>
 * <
 * pre>
 * &lt;simpleType name="TransactionWarning"> &lt;restriction
 * base="{http://www.w3.org/2001/XMLSchema}string"> &lt;enumeration
 * value="None"/> &lt;enumeration value="CardNotCreatedTokenInUse"/>
 * &lt;enumeration value="CustomerDoesNotExists"/> &lt;enumeration
 * value="CardNotCreatedAccessDenied"/> &lt;enumeration
 * value="CardNotCreatedCardAccountNumberInUse"/> &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 *
 */
@XmlType(name = "TransactionWarning", namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Common.Enums")
@XmlEnum
public enum TransactionWarning {

    @XmlEnumValue("None")
    NONE("None"), @XmlEnumValue("CardNotCreatedTokenInUse")
    CARD_NOT_CREATED_TOKEN_IN_USE("CardNotCreatedTokenInUse"), @XmlEnumValue("CustomerDoesNotExists")
    CUSTOMER_DOES_NOT_EXISTS("CustomerDoesNotExists"), @XmlEnumValue("CardNotCreatedAccessDenied")
    CARD_NOT_CREATED_ACCESS_DENIED("CardNotCreatedAccessDenied"), @XmlEnumValue("CardNotCreatedCardAccountNumberInUse")
    CARD_NOT_CREATED_CARD_ACCOUNT_NUMBER_IN_USE("CardNotCreatedCardAccountNumberInUse");

    private final String value;

    TransactionWarning(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static TransactionWarning fromValue(String v) {
        for (TransactionWarning c : TransactionWarning.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }
}
