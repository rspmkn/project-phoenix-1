package com.apd.phoenix.service.payment.ws;

import com.apd.phoenix.service.payment.ws.Void;
import java.math.BigDecimal;
import java.math.BigInteger;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.datatype.Duration;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.namespace.QName;

/**
 * This object contains factory methods for each Java content interface and Java
 * element interface generated in the com.apd.phoenix.service.payment.ws.client
 * package.
 * <p>
 * An ObjectFactory allows you to programatically construct new instances of the
 * Java representation for XML content. The Java representation of XML content
 * can consist of schema derived interfaces and classes representing the binding
 * of schema type definitions, element declarations and model groups. Factory
 * methods for each of these are provided in this class.
 *
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _AnyURI_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/",
            "anyURI");
    private final static QName _CardSecurityCodeIndicator_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Common.Enums", "CardSecurityCodeIndicator");
    private final static QName _Char_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "char");
    private final static QName _TransactionWarning_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Common.Enums", "TransactionWarning");
    private final static QName _Address_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", "Address");
    private final static QName _RuleInfringement_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", "RuleInfringement");
    private final static QName _Float_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "float");
    private final static QName _Long_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "long");
    private final static QName _LineItemDetailPurchasing_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardTransactionDefinitions",
            "LineItemDetailPurchasing");
    private final static QName _Base64Binary_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/",
            "base64Binary");
    private final static QName _Byte_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "byte");
    private final static QName _ArrayOfLineItemDetail_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardTransactionDefinitions",
            "ArrayOfLineItemDetail");
    private final static QName _LineItemDetailTempServices_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardTransactionDefinitions",
            "LineItemDetailTempServices");
    private final static QName _LineItemDetail_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardTransactionDefinitions",
            "LineItemDetail");
    private final static QName _AuthorizeAndCaptureParams_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Parameters.CreditCardTransactionParameters",
            "AuthorizeAndCaptureParams");
    private final static QName _Contact_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", "Contact");
    private final static QName _CaptureParams_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Parameters.CreditCardTransactionParameters",
            "CaptureParams");
    private final static QName _Boolean_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/",
            "boolean");
    private final static QName _ClientCredentials_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", "ClientCredentials");
    private final static QName _ForceParams_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Parameters.CreditCardTransactionParameters",
            "ForceParams");
    private final static QName _UnsignedByte_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/",
            "unsignedByte");
    private final static QName _AnyType_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/",
            "anyType");
    private final static QName _Int_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "int");
    private final static QName _GetTransactionStatusParams_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Parameters.CreditCardTransactionParameters",
            "GetTransactionStatusParams");
    private final static QName _Double_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/",
            "double");
    private final static QName _RefundParams_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Parameters.CreditCardTransactionParameters",
            "RefundParams");
    private final static QName _ArrayOfTransactionWarning_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Common.Enums", "ArrayOfTransactionWarning");
    private final static QName _TerminalIdentifier_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", "TerminalIdentifier");
    private final static QName _VoidParams_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Parameters.CreditCardTransactionParameters",
            "VoidParams");
    private final static QName _DateTime_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/",
            "dateTime");
    private final static QName _CreditParams_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Parameters.CreditCardTransactionParameters",
            "CreditParams");
    private final static QName _AdditionalAmounts_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardTransactionDefinitions",
            "AdditionalAmounts");
    private final static QName _CreditCardResponseStatus_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Common.Enums", "CreditCardResponseStatus");
    private final static QName _QName_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "QName");
    private final static QName _CountryCode_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Common.Enums", "CountryCode");
    private final static QName _UnsignedShort_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/",
            "unsignedShort");
    private final static QName _Email_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", "Email");
    private final static QName _CreditCardTransactionVerificationResult_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardTransactionDefinitions",
            "CreditCardTransactionVerificationResult");
    private final static QName _OriginalTransactionParams_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Parameters.CreditCardTransactionParameters",
            "OriginalTransactionParams");
    private final static QName _CreditCardTransactionCreditCard_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardTransactionDefinitions",
            "CreditCardTransactionCreditCard");
    private final static QName _Short_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "short");
    private final static QName _AuthorizeParams_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Parameters.CreditCardTransactionParameters",
            "AuthorizeParams");
    private final static QName _ArrayOfEmail_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", "ArrayOfEmail");
    private final static QName _CreditCardTransactionResult_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardTransactionDefinitions",
            "CreditCardTransactionResult");
    private final static QName _TransactionType_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Common.Enums", "TransactionType");
    private final static QName _QuickCaptureParams_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Parameters.CreditCardTransactionParameters",
            "QuickCaptureParams");
    private final static QName _CreditCardTransactionSummaryStatus_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Common.Enums", "CreditCardTransactionSummaryStatus");
    private final static QName _WebServiceResult_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", "WebServiceResult");
    private final static QName _StateProvinceCode_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Common.Enums", "StateProvinceCode");
    private final static QName _CreditCardTransaction_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardTransactionDefinitions",
            "CreditCardTransaction");
    private final static QName _ApplicationFault_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ApplicationError", "ApplicationFault");
    private final static QName _CreditCardTransactionStatusResult_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardTransactionDefinitions",
            "CreditCardTransactionStatusResult");
    private final static QName _StoredCardIdentifier_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions",
            "StoredCardIdentifier");
    private final static QName _UnsignedInt_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/",
            "unsignedInt");
    private final static QName _Decimal_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/",
            "decimal");
    private final static QName _CardVerificationParams_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Parameters.CreditCardTransactionParameters",
            "CardVerificationParams");
    private final static QName _ArrayOfstring_QNAME = new QName(
            "http://schemas.microsoft.com/2003/10/Serialization/Arrays", "ArrayOfstring");
    private final static QName _QuickRefundParams_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Parameters.CreditCardTransactionParameters",
            "QuickRefundParams");
    private final static QName _CreditCardTypes_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Enums", "CreditCardTypes");
    private final static QName _QuickForceParams_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Parameters.CreditCardTransactionParameters",
            "QuickForceParams");
    private final static QName _CurrencyCode_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Common.Enums", "CurrencyCode");
    private final static QName _Guid_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "guid");
    private final static QName _CreditCard_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", "CreditCard");
    private final static QName _Duration_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/",
            "duration");
    private final static QName _ArrayOfRuleInfringement_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions",
            "ArrayOfRuleInfringement");
    private final static QName _CreditCardTransactionAuthorizationResult_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardTransactionDefinitions",
            "CreditCardTransactionAuthorizationResult");
    private final static QName _String_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/",
            "string");
    private final static QName _AvsResponse_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Common.Enums", "AvsResponse");
    private final static QName _UnsignedLong_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/",
            "unsignedLong");
    private final static QName _CardSecurityCodeResponse_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Common.Enums", "CardSecurityCodeResponse");
    private final static QName _FailureReason_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Enums", "FailureReason");
    private final static QName _VoidVoidParams_QNAME = new QName("http://3DSI.org/WebServices/CreditCardTransaction",
            "voidParams");
    private final static QName _VoidClientCredentials_QNAME = new QName(
            "http://3DSI.org/WebServices/CreditCardTransaction", "clientCredentials");
    private final static QName _RuleInfringementDescription_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", "Description");
    private final static QName _ForceForceParams_QNAME = new QName("http://3DSI.org/WebServices/CreditCardTransaction",
            "forceParams");
    private final static QName _GetTransactionStatusGetTransactionStatusParams_QNAME = new QName(
            "http://3DSI.org/WebServices/CreditCardTransaction", "getTransactionStatusParams");
    private final static QName _ContactTitle_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", "Title");
    private final static QName _ContactLastName_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", "LastName");
    private final static QName _ContactFirstName_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", "FirstName");
    private final static QName _ContactSuffix_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", "Suffix");
    private final static QName _ContactFax_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", "Fax");
    private final static QName _ContactPhone_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", "Phone");
    private final static QName _ContactMiddleName_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", "MiddleName");
    private final static QName _ForceParamsCreditCardTransaction_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Parameters.CreditCardTransactionParameters",
            "CreditCardTransaction");
    private final static QName _ForceParamsTerminalIdentifier_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Parameters.CreditCardTransactionParameters",
            "TerminalIdentifier");
    private final static QName _ForceParamsAuthCode_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Parameters.CreditCardTransactionParameters",
            "AuthCode");
    private final static QName _VoidResponseVoidResult_QNAME = new QName(
            "http://3DSI.org/WebServices/CreditCardTransaction", "VoidResult");
    private final static QName _QuickForceQuickForceParams_QNAME = new QName(
            "http://3DSI.org/WebServices/CreditCardTransaction", "quickForceParams");
    private final static QName _CardVerificationResponseCardVerificationResult_QNAME = new QName(
            "http://3DSI.org/WebServices/CreditCardTransaction", "CardVerificationResult");
    private final static QName _ForceResponseForceResult_QNAME = new QName(
            "http://3DSI.org/WebServices/CreditCardTransaction", "ForceResult");
    private final static QName _AddressCity_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", "City");
    private final static QName _AddressAddressLine2_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", "AddressLine2");
    private final static QName _AddressAddressLine1_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", "AddressLine1");
    private final static QName _AddressPostalCode_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", "PostalCode");
    private final static QName _ApplicationFaultMessage_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ApplicationError", "Message");
    private final static QName _AuthorizeAndCaptureResponseAuthorizeAndCaptureResult_QNAME = new QName(
            "http://3DSI.org/WebServices/CreditCardTransaction", "AuthorizeAndCaptureResult");
    private final static QName _TerminalIdentifierTerminalCode_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", "TerminalCode");
    private final static QName _TerminalIdentifierMerchantCode_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", "MerchantCode");
    private final static QName _TerminalIdentifierLocationCode_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", "LocationCode");
    private final static QName _LineItemDetailPurchasingUnitOfMeasure_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardTransactionDefinitions",
            "UnitOfMeasure");
    private final static QName _LineItemDetailPurchasingCommodityCode_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardTransactionDefinitions",
            "CommodityCode");
    private final static QName _LineItemDetailPurchasingProductCode_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardTransactionDefinitions",
            "ProductCode");
    private final static QName _LineItemDetailPurchasingDescription_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardTransactionDefinitions",
            "Description");
    private final static QName _QuickCaptureQuickCaptureParams_QNAME = new QName(
            "http://3DSI.org/WebServices/CreditCardTransaction", "quickCaptureParams");
    private final static QName _StoredCardIdentifierToken_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", "Token");
    private final static QName _StoredCardIdentifierCustomerCode_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", "CustomerCode");
    private final static QName _AuthorizeAuthorizeParams_QNAME = new QName(
            "http://3DSI.org/WebServices/CreditCardTransaction", "authorizeParams");
    private final static QName _QuickRefundResponseQuickRefundResult_QNAME = new QName(
            "http://3DSI.org/WebServices/CreditCardTransaction", "QuickRefundResult");
    private final static QName _QuickForceResponseQuickForceResult_QNAME = new QName(
            "http://3DSI.org/WebServices/CreditCardTransaction", "QuickForceResult");
    private final static QName _CreditResponseCreditResult_QNAME = new QName(
            "http://3DSI.org/WebServices/CreditCardTransaction", "CreditResult");
    private final static QName _QuickCaptureResponseQuickCaptureResult_QNAME = new QName(
            "http://3DSI.org/WebServices/CreditCardTransaction", "QuickCaptureResult");
    private final static QName _CaptureResponseCaptureResult_QNAME = new QName(
            "http://3DSI.org/WebServices/CreditCardTransaction", "CaptureResult");
    private final static QName _CreditCardTransactionTransactionKey_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardTransactionDefinitions",
            "TransactionKey");
    private final static QName _CreditCardTransactionInvoiceNumber_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardTransactionDefinitions",
            "InvoiceNumber");
    private final static QName _CreditCardTransactionNote_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardTransactionDefinitions",
            "Note");
    private final static QName _CreditCardTransactionStoredCardIdentifier_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardTransactionDefinitions",
            "StoredCardIdentifier");
    private final static QName _CreditCardTransactionShipFromPostalCode_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardTransactionDefinitions",
            "ShipFromPostalCode");
    private final static QName _CreditCardTransactionCustomerReferenceValue_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardTransactionDefinitions",
            "CustomerReferenceValue");
    private final static QName _CreditCardTransactionOrderNumber_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardTransactionDefinitions",
            "OrderNumber");
    private final static QName _CreditCardTransactionReceiptEmailAddress_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardTransactionDefinitions",
            "ReceiptEmailAddress");
    private final static QName _CreditCardTransactionUserDefinedFields_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardTransactionDefinitions",
            "UserDefinedFields");
    private final static QName _CreditCardTransactionLineDetail_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardTransactionDefinitions",
            "LineDetail");
    private final static QName _CreditCardTransactionCreditCard1_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardTransactionDefinitions",
            "CreditCard");
    private final static QName _CaptureCaptureParams_QNAME = new QName(
            "http://3DSI.org/WebServices/CreditCardTransaction", "captureParams");
    private final static QName _CardVerificationCardVerificationParams_QNAME = new QName(
            "http://3DSI.org/WebServices/CreditCardTransaction", "cardVerificationParams");
    private final static QName _CreditCardTransactionCreditCardTrackData_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardTransactionDefinitions",
            "TrackData");
    private final static QName _CreditCardTransactionCreditCardCardSecurityCode_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardTransactionDefinitions",
            "CardSecurityCode");
    private final static QName _OriginalTransactionParamsOriginalTransactionKey_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Parameters.CreditCardTransactionParameters",
            "OriginalTransactionKey");
    private final static QName _RefundResponseRefundResult_QNAME = new QName(
            "http://3DSI.org/WebServices/CreditCardTransaction", "RefundResult");
    private final static QName _CardVerificationParamsStoredCardIdentifier_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Parameters.CreditCardTransactionParameters",
            "StoredCardIdentifier");
    private final static QName _CardVerificationParamsTransactionKey_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Parameters.CreditCardTransactionParameters",
            "TransactionKey");
    private final static QName _CardVerificationParamsCreditCard_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Parameters.CreditCardTransactionParameters",
            "CreditCard");
    private final static QName _AuthorizeResponseAuthorizeResult_QNAME = new QName(
            "http://3DSI.org/WebServices/CreditCardTransaction", "AuthorizeResult");
    private final static QName _CreditCardCardAccountNumber_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", "CardAccountNumber");
    private final static QName _CreditCardReceiptEmailAddresses_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions",
            "ReceiptEmailAddresses");
    private final static QName _CreditCardBillingAddress_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", "BillingAddress");
    private final static QName _CreditCardShippingAddress_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", "ShippingAddress");
    private final static QName _CreditCardNameOnCard_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", "NameOnCard");
    private final static QName _CreditCardCardholder_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", "Cardholder");
    private final static QName _AuthorizeAndCaptureAuthorizeAndCaptureParams_QNAME = new QName(
            "http://3DSI.org/WebServices/CreditCardTransaction", "authorizeAndCaptureParams");
    private final static QName _CreditCreditParams_QNAME = new QName(
            "http://3DSI.org/WebServices/CreditCardTransaction", "creditParams");
    private final static QName _WebServiceResultValidationFailures_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", "ValidationFailures");
    private final static QName _EmailDisplayAs_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", "DisplayAs");
    private final static QName _GetTransactionStatusResponseGetTransactionStatusResult_QNAME = new QName(
            "http://3DSI.org/WebServices/CreditCardTransaction", "GetTransactionStatusResult");
    private final static QName _CreditCardTransactionResultProcessorResponse_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardTransactionDefinitions",
            "ProcessorResponse");
    private final static QName _CreditCardTransactionResultRuleInfringements_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardTransactionDefinitions",
            "RuleInfringements");
    private final static QName _CreditCardTransactionResultToken_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardTransactionDefinitions",
            "Token");
    private final static QName _CreditCardTransactionResultTransactionWarning_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardTransactionDefinitions",
            "TransactionWarning");
    private final static QName _CreditCardTransactionResultThirdPartyResponse_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardTransactionDefinitions",
            "ThirdPartyResponse");
    private final static QName _CreditCardTransactionVerificationResultProcessorAvsResponse_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardTransactionDefinitions",
            "ProcessorAvsResponse");
    private final static QName _CreditCardTransactionVerificationResultProcessorCardSecurityCodeResponse_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardTransactionDefinitions",
            "ProcessorCardSecurityCodeResponse");
    private final static QName _ClientCredentialsUserName_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", "UserName");
    private final static QName _ClientCredentialsPassword_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", "Password");
    private final static QName _ClientCredentialsClientCode_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", "ClientCode");
    private final static QName _RefundRefundParams_QNAME = new QName(
            "http://3DSI.org/WebServices/CreditCardTransaction", "refundParams");
    private final static QName _LineItemDetailTempServicesEmployeeName_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardTransactionDefinitions",
            "EmployeeName");
    private final static QName _LineItemDetailTempServicesPositionCode_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardTransactionDefinitions",
            "PositionCode");
    private final static QName _LineItemDetailTempServicesEmployeeId_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardTransactionDefinitions",
            "EmployeeId");
    private final static QName _LineItemDetailTempServicesProjectCode_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardTransactionDefinitions",
            "ProjectCode");
    private final static QName _LineItemDetailTempServicesPhoneNumber_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardTransactionDefinitions",
            "PhoneNumber");
    private final static QName _LineItemDetailTempServicesSupervisor_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardTransactionDefinitions",
            "Supervisor");
    private final static QName _LineItemDetailTempServicesTrackingNumber_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardTransactionDefinitions",
            "TrackingNumber");
    private final static QName _LineItemDetailTempServicesCostCenter_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardTransactionDefinitions",
            "CostCenter");
    private final static QName _LineItemDetailTempServicesUnionCode_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardTransactionDefinitions",
            "UnionCode");
    private final static QName _LineItemDetailTempServicesRequestorId_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardTransactionDefinitions",
            "RequestorId");
    private final static QName _LineItemDetailTempServicesProjectDescription_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardTransactionDefinitions",
            "ProjectDescription");
    private final static QName _QuickRefundQuickRefundParams_QNAME = new QName(
            "http://3DSI.org/WebServices/CreditCardTransaction", "quickRefundParams");
    private final static QName _CreditCardTransactionAuthorizationResultAuthCode_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardTransactionDefinitions",
            "AuthCode");

    /**
     * Create a new ObjectFactory that can be used to create new instances of
     * schema derived classes for package:
     * com.apd.phoenix.service.payment.ws.client
     *
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ApplicationFault }
     *
     */
    public ApplicationFault createApplicationFault() {
        return new ApplicationFault();
    }

    /**
     * Create an instance of {@link ArrayOfTransactionWarning }
     *
     */
    public ArrayOfTransactionWarning createArrayOfTransactionWarning() {
        return new ArrayOfTransactionWarning();
    }

    /**
     * Create an instance of {@link ArrayOfstring }
     *
     */
    public ArrayOfstring createArrayOfstring() {
        return new ArrayOfstring();
    }

    /**
     * Create an instance of {@link GetTransactionStatusParams }
     *
     */
    public GetTransactionStatusParams createGetTransactionStatusParams() {
        return new GetTransactionStatusParams();
    }

    /**
     * Create an instance of {@link AuthorizeAndCaptureParams }
     *
     */
    public AuthorizeAndCaptureParams createAuthorizeAndCaptureParams() {
        return new AuthorizeAndCaptureParams();
    }

    /**
     * Create an instance of {@link CaptureParams }
     *
     */
    public CaptureParams createCaptureParams() {
        return new CaptureParams();
    }

    /**
     * Create an instance of {@link OriginalTransactionParams }
     *
     */
    public OriginalTransactionParams createOriginalTransactionParams() {
        return new OriginalTransactionParams();
    }

    /**
     * Create an instance of {@link QuickRefundParams }
     *
     */
    public QuickRefundParams createQuickRefundParams() {
        return new QuickRefundParams();
    }

    /**
     * Create an instance of {@link CardVerificationParams }
     *
     */
    public CardVerificationParams createCardVerificationParams() {
        return new CardVerificationParams();
    }

    /**
     * Create an instance of {@link RefundParams }
     *
     */
    public RefundParams createRefundParams() {
        return new RefundParams();
    }

    /**
     * Create an instance of {@link ForceParams }
     *
     */
    public ForceParams createForceParams() {
        return new ForceParams();
    }

    /**
     * Create an instance of {@link VoidParams }
     *
     */
    public VoidParams createVoidParams() {
        return new VoidParams();
    }

    /**
     * Create an instance of {@link QuickForceParams }
     *
     */
    public QuickForceParams createQuickForceParams() {
        return new QuickForceParams();
    }

    /**
     * Create an instance of {@link QuickCaptureParams }
     *
     */
    public QuickCaptureParams createQuickCaptureParams() {
        return new QuickCaptureParams();
    }

    /**
     * Create an instance of {@link CreditParams }
     *
     */
    public CreditParams createCreditParams() {
        return new CreditParams();
    }

    /**
     * Create an instance of {@link AuthorizeParams }
     *
     */
    public AuthorizeParams createAuthorizeParams() {
        return new AuthorizeParams();
    }

    /**
     * Create an instance of {@link AuthorizeAndCaptureResponse }
     *
     */
    public AuthorizeAndCaptureResponse createAuthorizeAndCaptureResponse() {
        return new AuthorizeAndCaptureResponse();
    }

    /**
     * Create an instance of {@link CreditCardTransactionAuthorizationResult }
     *
     */
    public CreditCardTransactionAuthorizationResult createCreditCardTransactionAuthorizationResult() {
        return new CreditCardTransactionAuthorizationResult();
    }

    /**
     * Create an instance of {@link QuickRefundResponse }
     *
     */
    public QuickRefundResponse createQuickRefundResponse() {
        return new QuickRefundResponse();
    }

    /**
     * Create an instance of {@link CreditCardTransactionResult }
     *
     */
    public CreditCardTransactionResult createCreditCardTransactionResult() {
        return new CreditCardTransactionResult();
    }

    /**
     * Create an instance of {@link QuickCapture }
     *
     */
    public QuickCapture createQuickCapture() {
        return new QuickCapture();
    }

    /**
     * Create an instance of {@link ClientCredentials }
     *
     */
    public ClientCredentials createClientCredentials() {
        return new ClientCredentials();
    }

    /**
     * Create an instance of {@link AuthorizeAndCapture }
     *
     */
    public AuthorizeAndCapture createAuthorizeAndCapture() {
        return new AuthorizeAndCapture();
    }

    /**
     * Create an instance of {@link Void }
     *
     */
    public Void createVoid() {
        return new Void();
    }

    /**
     * Create an instance of {@link Credit }
     *
     */
    public Credit createCredit() {
        return new Credit();
    }

    /**
     * Create an instance of {@link RefundResponse }
     *
     */
    public RefundResponse createRefundResponse() {
        return new RefundResponse();
    }

    /**
     * Create an instance of {@link CardVerificationResponse }
     *
     */
    public CardVerificationResponse createCardVerificationResponse() {
        return new CardVerificationResponse();
    }

    /**
     * Create an instance of {@link CreditCardTransactionVerificationResult }
     *
     */
    public CreditCardTransactionVerificationResult createCreditCardTransactionVerificationResult() {
        return new CreditCardTransactionVerificationResult();
    }

    /**
     * Create an instance of {@link AuthorizeResponse }
     *
     */
    public AuthorizeResponse createAuthorizeResponse() {
        return new AuthorizeResponse();
    }

    /**
     * Create an instance of {@link QuickRefund }
     *
     */
    public QuickRefund createQuickRefund() {
        return new QuickRefund();
    }

    /**
     * Create an instance of {@link Force }
     *
     */
    public Force createForce() {
        return new Force();
    }

    /**
     * Create an instance of {@link ForceResponse }
     *
     */
    public ForceResponse createForceResponse() {
        return new ForceResponse();
    }

    /**
     * Create an instance of {@link QuickForceResponse }
     *
     */
    public QuickForceResponse createQuickForceResponse() {
        return new QuickForceResponse();
    }

    /**
     * Create an instance of {@link Authorize }
     *
     */
    public Authorize createAuthorize() {
        return new Authorize();
    }

    /**
     * Create an instance of {@link Capture }
     *
     */
    public Capture createCapture() {
        return new Capture();
    }

    /**
     * Create an instance of {@link GetTransactionStatus }
     *
     */
    public GetTransactionStatus createGetTransactionStatus() {
        return new GetTransactionStatus();
    }

    /**
     * Create an instance of {@link QuickCaptureResponse }
     *
     */
    public QuickCaptureResponse createQuickCaptureResponse() {
        return new QuickCaptureResponse();
    }

    /**
     * Create an instance of {@link GetTransactionStatusResponse }
     *
     */
    public GetTransactionStatusResponse createGetTransactionStatusResponse() {
        return new GetTransactionStatusResponse();
    }

    /**
     * Create an instance of {@link CreditCardTransactionStatusResult }
     *
     */
    public CreditCardTransactionStatusResult createCreditCardTransactionStatusResult() {
        return new CreditCardTransactionStatusResult();
    }

    /**
     * Create an instance of {@link CreditResponse }
     *
     */
    public CreditResponse createCreditResponse() {
        return new CreditResponse();
    }

    /**
     * Create an instance of {@link Refund }
     *
     */
    public Refund createRefund() {
        return new Refund();
    }

    /**
     * Create an instance of {@link VoidResponse }
     *
     */
    public VoidResponse createVoidResponse() {
        return new VoidResponse();
    }

    /**
     * Create an instance of {@link CardVerification }
     *
     */
    public CardVerification createCardVerification() {
        return new CardVerification();
    }

    /**
     * Create an instance of {@link CaptureResponse }
     *
     */
    public CaptureResponse createCaptureResponse() {
        return new CaptureResponse();
    }

    /**
     * Create an instance of {@link QuickForce }
     *
     */
    public QuickForce createQuickForce() {
        return new QuickForce();
    }

    /**
     * Create an instance of {@link LineItemDetail }
     *
     */
    public LineItemDetail createLineItemDetail() {
        return new LineItemDetail();
    }

    /**
     * Create an instance of {@link ArrayOfLineItemDetail }
     *
     */
    public ArrayOfLineItemDetail createArrayOfLineItemDetail() {
        return new ArrayOfLineItemDetail();
    }

    /**
     * Create an instance of {@link LineItemDetailTempServices }
     *
     */
    public LineItemDetailTempServices createLineItemDetailTempServices() {
        return new LineItemDetailTempServices();
    }

    /**
     * Create an instance of {@link AdditionalAmounts }
     *
     */
    public AdditionalAmounts createAdditionalAmounts() {
        return new AdditionalAmounts();
    }

    /**
     * Create an instance of {@link LineItemDetailPurchasing }
     *
     */
    public LineItemDetailPurchasing createLineItemDetailPurchasing() {
        return new LineItemDetailPurchasing();
    }

    /**
     * Create an instance of {@link CreditCardTransaction }
     *
     */
    public CreditCardTransaction createCreditCardTransaction() {
        return new CreditCardTransaction();
    }

    /**
     * Create an instance of {@link CreditCardTransactionCreditCard }
     *
     */
    public CreditCardTransactionCreditCard createCreditCardTransactionCreditCard() {
        return new CreditCardTransactionCreditCard();
    }

    /**
     * Create an instance of {@link TerminalIdentifier }
     *
     */
    public TerminalIdentifier createTerminalIdentifier() {
        return new TerminalIdentifier();
    }

    /**
     * Create an instance of {@link RuleInfringement }
     *
     */
    public RuleInfringement createRuleInfringement() {
        return new RuleInfringement();
    }

    /**
     * Create an instance of {@link Email }
     *
     */
    public Email createEmail() {
        return new Email();
    }

    /**
     * Create an instance of {@link WebServiceResult }
     *
     */
    public WebServiceResult createWebServiceResult() {
        return new WebServiceResult();
    }

    /**
     * Create an instance of {@link Contact }
     *
     */
    public Contact createContact() {
        return new Contact();
    }

    /**
     * Create an instance of {@link Address }
     *
     */
    public Address createAddress() {
        return new Address();
    }

    /**
     * Create an instance of {@link ArrayOfRuleInfringement }
     *
     */
    public ArrayOfRuleInfringement createArrayOfRuleInfringement() {
        return new ArrayOfRuleInfringement();
    }

    /**
     * Create an instance of {@link StoredCardIdentifier }
     *
     */
    public StoredCardIdentifier createStoredCardIdentifier() {
        return new StoredCardIdentifier();
    }

    /**
     * Create an instance of {@link ArrayOfEmail }
     *
     */
    public ArrayOfEmail createArrayOfEmail() {
        return new ArrayOfEmail();
    }

    /**
     * Create an instance of {@link CreditCard }
     *
     */
    public CreditCard createCreditCard() {
        return new CreditCard();
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link String }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "anyURI")
    public JAXBElement<String> createAnyURI(String value) {
        return new JAXBElement<>(_AnyURI_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link CardSecurityCodeIndicator }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Common.Enums", name = "CardSecurityCodeIndicator")
    public JAXBElement<CardSecurityCodeIndicator> createCardSecurityCodeIndicator(CardSecurityCodeIndicator value) {
        return new JAXBElement<>(_CardSecurityCodeIndicator_QNAME,
                CardSecurityCodeIndicator.class, null, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "char")
    public JAXBElement<Integer> createChar(Integer value) {
        return new JAXBElement<>(_Char_QNAME, Integer.class, null, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link TransactionWarning }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Common.Enums", name = "TransactionWarning")
    public JAXBElement<TransactionWarning> createTransactionWarning(TransactionWarning value) {
        return new JAXBElement<>(_TransactionWarning_QNAME, TransactionWarning.class, null, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link Address }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", name = "Address")
    public JAXBElement<Address> createAddress(Address value) {
        return new JAXBElement<>(_Address_QNAME, Address.class, null, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link RuleInfringement }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", name = "RuleInfringement")
    public JAXBElement<RuleInfringement> createRuleInfringement(RuleInfringement value) {
        return new JAXBElement<>(_RuleInfringement_QNAME, RuleInfringement.class, null, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link Float }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "float")
    public JAXBElement<Float> createFloat(Float value) {
        return new JAXBElement<>(_Float_QNAME, Float.class, null, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link Long }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "long")
    public JAXBElement<Long> createLong(Long value) {
        return new JAXBElement<>(_Long_QNAME, Long.class, null, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link LineItemDetailPurchasing }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardTransactionDefinitions", name = "LineItemDetailPurchasing")
    public JAXBElement<LineItemDetailPurchasing> createLineItemDetailPurchasing(LineItemDetailPurchasing value) {
        return new JAXBElement<>(_LineItemDetailPurchasing_QNAME,
                LineItemDetailPurchasing.class, null, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link byte[]}{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "base64Binary")
    public JAXBElement<byte[]> createBase64Binary(byte[] value) {
        return new JAXBElement<>(_Base64Binary_QNAME, byte[].class, null, ((byte[]) value));
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link Byte }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "byte")
    public JAXBElement<Byte> createByte(Byte value) {
        return new JAXBElement<>(_Byte_QNAME, Byte.class, null, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link ArrayOfLineItemDetail }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardTransactionDefinitions", name = "ArrayOfLineItemDetail")
    public JAXBElement<ArrayOfLineItemDetail> createArrayOfLineItemDetail(ArrayOfLineItemDetail value) {
        return new JAXBElement<>(_ArrayOfLineItemDetail_QNAME, ArrayOfLineItemDetail.class, null,
                value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link LineItemDetailTempServices }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardTransactionDefinitions", name = "LineItemDetailTempServices")
    public JAXBElement<LineItemDetailTempServices> createLineItemDetailTempServices(LineItemDetailTempServices value) {
        return new JAXBElement<>(_LineItemDetailTempServices_QNAME,
                LineItemDetailTempServices.class, null, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link LineItemDetail }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardTransactionDefinitions", name = "LineItemDetail")
    public JAXBElement<LineItemDetail> createLineItemDetail(LineItemDetail value) {
        return new JAXBElement<>(_LineItemDetail_QNAME, LineItemDetail.class, null, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link AuthorizeAndCaptureParams }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Parameters.CreditCardTransactionParameters", name = "AuthorizeAndCaptureParams")
    public JAXBElement<AuthorizeAndCaptureParams> createAuthorizeAndCaptureParams(AuthorizeAndCaptureParams value) {
        return new JAXBElement<>(_AuthorizeAndCaptureParams_QNAME,
                AuthorizeAndCaptureParams.class, null, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link Contact }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", name = "Contact")
    public JAXBElement<Contact> createContact(Contact value) {
        return new JAXBElement<>(_Contact_QNAME, Contact.class, null, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link CaptureParams }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Parameters.CreditCardTransactionParameters", name = "CaptureParams")
    public JAXBElement<CaptureParams> createCaptureParams(CaptureParams value) {
        return new JAXBElement<>(_CaptureParams_QNAME, CaptureParams.class, null, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "boolean")
    public JAXBElement<Boolean> createBoolean(Boolean value) {
        return new JAXBElement<>(_Boolean_QNAME, Boolean.class, null, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link ClientCredentials }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", name = "ClientCredentials")
    public JAXBElement<ClientCredentials> createClientCredentials(ClientCredentials value) {
        return new JAXBElement<>(_ClientCredentials_QNAME, ClientCredentials.class, null, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link ForceParams }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Parameters.CreditCardTransactionParameters", name = "ForceParams")
    public JAXBElement<ForceParams> createForceParams(ForceParams value) {
        return new JAXBElement<>(_ForceParams_QNAME, ForceParams.class, null, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link Short }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "unsignedByte")
    public JAXBElement<Short> createUnsignedByte(Short value) {
        return new JAXBElement<>(_UnsignedByte_QNAME, Short.class, null, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link Object }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "anyType")
    public JAXBElement<Object> createAnyType(Object value) {
        return new JAXBElement<>(_AnyType_QNAME, Object.class, null, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "int")
    public JAXBElement<Integer> createInt(Integer value) {
        return new JAXBElement<>(_Int_QNAME, Integer.class, null, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link GetTransactionStatusParams }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Parameters.CreditCardTransactionParameters", name = "GetTransactionStatusParams")
    public JAXBElement<GetTransactionStatusParams> createGetTransactionStatusParams(GetTransactionStatusParams value) {
        return new JAXBElement<>(_GetTransactionStatusParams_QNAME,
                GetTransactionStatusParams.class, null, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link Double }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "double")
    public JAXBElement<Double> createDouble(Double value) {
        return new JAXBElement<>(_Double_QNAME, Double.class, null, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link RefundParams }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Parameters.CreditCardTransactionParameters", name = "RefundParams")
    public JAXBElement<RefundParams> createRefundParams(RefundParams value) {
        return new JAXBElement<>(_RefundParams_QNAME, RefundParams.class, null, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link ArrayOfTransactionWarning }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Common.Enums", name = "ArrayOfTransactionWarning")
    public JAXBElement<ArrayOfTransactionWarning> createArrayOfTransactionWarning(ArrayOfTransactionWarning value) {
        return new JAXBElement<>(_ArrayOfTransactionWarning_QNAME,
                ArrayOfTransactionWarning.class, null, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link TerminalIdentifier }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", name = "TerminalIdentifier", scope = GetTransactionStatus.class)
    public JAXBElement<TerminalIdentifier> createGetTransactionStatusTerminalIdentifier(TerminalIdentifier value) {
        return new JAXBElement<>(_TerminalIdentifier_QNAME, TerminalIdentifier.class, GetTransactionStatusParams.class, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link TerminalIdentifier }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", name = "TerminalIdentifier")
    public JAXBElement<TerminalIdentifier> createTerminalIdentifier(TerminalIdentifier value) {
        return new JAXBElement<>(_TerminalIdentifier_QNAME, TerminalIdentifier.class, null, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link VoidParams }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Parameters.CreditCardTransactionParameters", name = "VoidParams")
    public JAXBElement<VoidParams> createVoidParams(VoidParams value) {
        return new JAXBElement<>(_VoidParams_QNAME, VoidParams.class, null, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "dateTime")
    public JAXBElement<XMLGregorianCalendar> createDateTime(XMLGregorianCalendar value) {
        return new JAXBElement<>(_DateTime_QNAME, XMLGregorianCalendar.class, null, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link CreditParams }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Parameters.CreditCardTransactionParameters", name = "CreditParams")
    public JAXBElement<CreditParams> createCreditParams(CreditParams value) {
        return new JAXBElement<>(_CreditParams_QNAME, CreditParams.class, null, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link AdditionalAmounts }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardTransactionDefinitions", name = "AdditionalAmounts")
    public JAXBElement<AdditionalAmounts> createAdditionalAmounts(AdditionalAmounts value) {
        return new JAXBElement<>(_AdditionalAmounts_QNAME, AdditionalAmounts.class, null, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link CreditCardResponseStatus }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Common.Enums", name = "CreditCardResponseStatus")
    public JAXBElement<CreditCardResponseStatus> createCreditCardResponseStatus(CreditCardResponseStatus value) {
        return new JAXBElement<>(_CreditCardResponseStatus_QNAME,
                CreditCardResponseStatus.class, null, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link QName }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "QName")
    public JAXBElement<QName> createQName(QName value) {
        return new JAXBElement<>(_QName_QNAME, QName.class, null, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link CountryCode }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Common.Enums", name = "CountryCode")
    public JAXBElement<CountryCode> createCountryCode(CountryCode value) {
        return new JAXBElement<>(_CountryCode_QNAME, CountryCode.class, null, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "unsignedShort")
    public JAXBElement<Integer> createUnsignedShort(Integer value) {
        return new JAXBElement<>(_UnsignedShort_QNAME, Integer.class, null, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link Email }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", name = "Email")
    public JAXBElement<Email> createEmail(Email value) {
        return new JAXBElement<>(_Email_QNAME, Email.class, null, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link CreditCardTransactionVerificationResult }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardTransactionDefinitions", name = "CreditCardTransactionVerificationResult")
    public JAXBElement<CreditCardTransactionVerificationResult> createCreditCardTransactionVerificationResult(
            CreditCardTransactionVerificationResult value) {
        return new JAXBElement<>(_CreditCardTransactionVerificationResult_QNAME,
                CreditCardTransactionVerificationResult.class, null, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link OriginalTransactionParams }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Parameters.CreditCardTransactionParameters", name = "OriginalTransactionParams")
    public JAXBElement<OriginalTransactionParams> createOriginalTransactionParams(OriginalTransactionParams value) {
        return new JAXBElement<>(_OriginalTransactionParams_QNAME,
                OriginalTransactionParams.class, null, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link CreditCardTransactionCreditCard }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardTransactionDefinitions", name = "CreditCardTransactionCreditCard")
    public JAXBElement<CreditCardTransactionCreditCard> createCreditCardTransactionCreditCard(
            CreditCardTransactionCreditCard value) {
        return new JAXBElement<>(_CreditCardTransactionCreditCard_QNAME,
                CreditCardTransactionCreditCard.class, null, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link Short }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "short")
    public JAXBElement<Short> createShort(Short value) {
        return new JAXBElement<>(_Short_QNAME, Short.class, null, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link AuthorizeParams }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Parameters.CreditCardTransactionParameters", name = "AuthorizeParams")
    public JAXBElement<AuthorizeParams> createAuthorizeParams(AuthorizeParams value) {
        return new JAXBElement<>(_AuthorizeParams_QNAME, AuthorizeParams.class, null, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link ArrayOfEmail }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", name = "ArrayOfEmail")
    public JAXBElement<ArrayOfEmail> createArrayOfEmail(ArrayOfEmail value) {
        return new JAXBElement<>(_ArrayOfEmail_QNAME, ArrayOfEmail.class, null, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link CreditCardTransactionResult }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardTransactionDefinitions", name = "CreditCardTransactionResult")
    public JAXBElement<CreditCardTransactionResult> createCreditCardTransactionResult(CreditCardTransactionResult value) {
        return new JAXBElement<>(_CreditCardTransactionResult_QNAME,
                CreditCardTransactionResult.class, null, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link TransactionType }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Common.Enums", name = "TransactionType")
    public JAXBElement<TransactionType> createTransactionType(TransactionType value) {
        return new JAXBElement<>(_TransactionType_QNAME, TransactionType.class, null, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link QuickCaptureParams }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Parameters.CreditCardTransactionParameters", name = "QuickCaptureParams")
    public JAXBElement<QuickCaptureParams> createQuickCaptureParams(QuickCaptureParams value) {
        return new JAXBElement<>(_QuickCaptureParams_QNAME, QuickCaptureParams.class, null, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link CreditCardTransactionSummaryStatus }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Common.Enums", name = "CreditCardTransactionSummaryStatus")
    public JAXBElement<CreditCardTransactionSummaryStatus> createCreditCardTransactionSummaryStatus(
            CreditCardTransactionSummaryStatus value) {
        return new JAXBElement<>(_CreditCardTransactionSummaryStatus_QNAME,
                CreditCardTransactionSummaryStatus.class, null, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link WebServiceResult }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", name = "WebServiceResult")
    public JAXBElement<WebServiceResult> createWebServiceResult(WebServiceResult value) {
        return new JAXBElement<>(_WebServiceResult_QNAME, WebServiceResult.class, null, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link StateProvinceCode }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Common.Enums", name = "StateProvinceCode")
    public JAXBElement<StateProvinceCode> createStateProvinceCode(StateProvinceCode value) {
        return new JAXBElement<>(_StateProvinceCode_QNAME, StateProvinceCode.class, null, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link CreditCardTransaction }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardTransactionDefinitions", name = "CreditCardTransaction")
    public JAXBElement<CreditCardTransaction> createCreditCardTransaction(CreditCardTransaction value) {
        return new JAXBElement<>(_CreditCardTransaction_QNAME, CreditCardTransaction.class, null,
                value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link ApplicationFault }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ApplicationError", name = "ApplicationFault")
    public JAXBElement<ApplicationFault> createApplicationFault(ApplicationFault value) {
        return new JAXBElement<>(_ApplicationFault_QNAME, ApplicationFault.class, null, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link CreditCardTransactionStatusResult }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardTransactionDefinitions", name = "CreditCardTransactionStatusResult")
    public JAXBElement<CreditCardTransactionStatusResult> createCreditCardTransactionStatusResult(
            CreditCardTransactionStatusResult value) {
        return new JAXBElement<>(_CreditCardTransactionStatusResult_QNAME,
                CreditCardTransactionStatusResult.class, null, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link StoredCardIdentifier }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", name = "StoredCardIdentifier")
    public JAXBElement<StoredCardIdentifier> createStoredCardIdentifier(StoredCardIdentifier value) {
        return new JAXBElement<>(_StoredCardIdentifier_QNAME, StoredCardIdentifier.class, null,
                value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link Long }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "unsignedInt")
    public JAXBElement<Long> createUnsignedInt(Long value) {
        return new JAXBElement<>(_UnsignedInt_QNAME, Long.class, null, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "decimal")
    public JAXBElement<BigDecimal> createDecimal(BigDecimal value) {
        return new JAXBElement<>(_Decimal_QNAME, BigDecimal.class, null, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link CardVerificationParams }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Parameters.CreditCardTransactionParameters", name = "CardVerificationParams")
    public JAXBElement<CardVerificationParams> createCardVerificationParams(CardVerificationParams value) {
        return new JAXBElement<>(_CardVerificationParams_QNAME, CardVerificationParams.class,
                null, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link ArrayOfstring }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/Arrays", name = "ArrayOfstring")
    public JAXBElement<ArrayOfstring> createArrayOfstring(ArrayOfstring value) {
        return new JAXBElement<>(_ArrayOfstring_QNAME, ArrayOfstring.class, null, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link QuickRefundParams }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Parameters.CreditCardTransactionParameters", name = "QuickRefundParams")
    public JAXBElement<QuickRefundParams> createQuickRefundParams(QuickRefundParams value) {
        return new JAXBElement<>(_QuickRefundParams_QNAME, QuickRefundParams.class, null, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link CreditCardTypes }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Enums", name = "CreditCardTypes")
    public JAXBElement<CreditCardTypes> createCreditCardTypes(CreditCardTypes value) {
        return new JAXBElement<>(_CreditCardTypes_QNAME, CreditCardTypes.class, null, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link QuickForceParams }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Parameters.CreditCardTransactionParameters", name = "QuickForceParams")
    public JAXBElement<QuickForceParams> createQuickForceParams(QuickForceParams value) {
        return new JAXBElement<>(_QuickForceParams_QNAME, QuickForceParams.class, null, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link CurrencyCode }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Common.Enums", name = "CurrencyCode")
    public JAXBElement<CurrencyCode> createCurrencyCode(CurrencyCode value) {
        return new JAXBElement<>(_CurrencyCode_QNAME, CurrencyCode.class, null, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link String }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "guid")
    public JAXBElement<String> createGuid(String value) {
        return new JAXBElement<>(_Guid_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link CreditCard }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", name = "CreditCard")
    public JAXBElement<CreditCard> createCreditCard(CreditCard value) {
        return new JAXBElement<>(_CreditCard_QNAME, CreditCard.class, null, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link Duration }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "duration")
    public JAXBElement<Duration> createDuration(Duration value) {
        return new JAXBElement<>(_Duration_QNAME, Duration.class, null, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link ArrayOfRuleInfringement }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", name = "ArrayOfRuleInfringement")
    public JAXBElement<ArrayOfRuleInfringement> createArrayOfRuleInfringement(ArrayOfRuleInfringement value) {
        return new JAXBElement<>(_ArrayOfRuleInfringement_QNAME, ArrayOfRuleInfringement.class,
                null, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link CreditCardTransactionAuthorizationResult }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardTransactionDefinitions", name = "CreditCardTransactionAuthorizationResult")
    public JAXBElement<CreditCardTransactionAuthorizationResult> createCreditCardTransactionAuthorizationResult(
            CreditCardTransactionAuthorizationResult value) {
        return new JAXBElement<>(
                _CreditCardTransactionAuthorizationResult_QNAME, CreditCardTransactionAuthorizationResult.class, null,
                value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link String }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "string")
    public JAXBElement<String> createString(String value) {
        return new JAXBElement<>(_String_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link AvsResponse }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Common.Enums", name = "AvsResponse")
    public JAXBElement<AvsResponse> createAvsResponse(AvsResponse value) {
        return new JAXBElement<>(_AvsResponse_QNAME, AvsResponse.class, null, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link BigInteger }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "unsignedLong")
    public JAXBElement<BigInteger> createUnsignedLong(BigInteger value) {
        return new JAXBElement<>(_UnsignedLong_QNAME, BigInteger.class, null, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link CardSecurityCodeResponse }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Common.Enums", name = "CardSecurityCodeResponse")
    public JAXBElement<CardSecurityCodeResponse> createCardSecurityCodeResponse(CardSecurityCodeResponse value) {
        return new JAXBElement<>(_CardSecurityCodeResponse_QNAME,
                CardSecurityCodeResponse.class, null, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link FailureReason }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Enums", name = "FailureReason")
    public JAXBElement<FailureReason> createFailureReason(FailureReason value) {
        return new JAXBElement<>(_FailureReason_QNAME, FailureReason.class, null, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link VoidParams }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://3DSI.org/WebServices/CreditCardTransaction", name = "voidParams", scope = Void.class)
    public JAXBElement<VoidParams> createVoidVoidParams(VoidParams value) {
        return new JAXBElement<>(_VoidVoidParams_QNAME, VoidParams.class, Void.class, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link ClientCredentials }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://3DSI.org/WebServices/CreditCardTransaction", name = "clientCredentials", scope = Void.class)
    public JAXBElement<ClientCredentials> createVoidClientCredentials(ClientCredentials value) {
        return new JAXBElement<>(_VoidClientCredentials_QNAME, ClientCredentials.class, Void.class,
                value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link String }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", name = "Description", scope = RuleInfringement.class)
    public JAXBElement<String> createRuleInfringementDescription(String value) {
        return new JAXBElement<>(_RuleInfringementDescription_QNAME, String.class, RuleInfringement.class, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link ClientCredentials }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://3DSI.org/WebServices/CreditCardTransaction", name = "clientCredentials", scope = Force.class)
    public JAXBElement<ClientCredentials> createForceClientCredentials(ClientCredentials value) {
        return new JAXBElement<>(_VoidClientCredentials_QNAME, ClientCredentials.class, Force.class,
                value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link ForceParams }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://3DSI.org/WebServices/CreditCardTransaction", name = "forceParams", scope = Force.class)
    public JAXBElement<ForceParams> createForceForceParams(ForceParams value) {
        return new JAXBElement<>(_ForceForceParams_QNAME, ForceParams.class, Force.class, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link ClientCredentials }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://3DSI.org/WebServices/CreditCardTransaction", name = "clientCredentials", scope = GetTransactionStatus.class)
    public JAXBElement<ClientCredentials> createGetTransactionStatusClientCredentials(ClientCredentials value) {
        return new JAXBElement<>(_VoidClientCredentials_QNAME, ClientCredentials.class,
                GetTransactionStatus.class, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link GetTransactionStatusParams }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://3DSI.org/WebServices/CreditCardTransaction", name = "getTransactionStatusParams", scope = GetTransactionStatus.class)
    public JAXBElement<GetTransactionStatusParams> createGetTransactionStatusGetTransactionStatusParams(
            GetTransactionStatusParams value) {
        return new JAXBElement<>(_GetTransactionStatusGetTransactionStatusParams_QNAME,
                GetTransactionStatusParams.class, GetTransactionStatus.class, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link String }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", name = "Title", scope = Contact.class)
    public JAXBElement<String> createContactTitle(String value) {
        return new JAXBElement<>(_ContactTitle_QNAME, String.class, Contact.class, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link String }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", name = "LastName", scope = Contact.class)
    public JAXBElement<String> createContactLastName(String value) {
        return new JAXBElement<>(_ContactLastName_QNAME, String.class, Contact.class, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link String }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", name = "FirstName", scope = Contact.class)
    public JAXBElement<String> createContactFirstName(String value) {
        return new JAXBElement<>(_ContactFirstName_QNAME, String.class, Contact.class, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link String }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", name = "Suffix", scope = Contact.class)
    public JAXBElement<String> createContactSuffix(String value) {
        return new JAXBElement<>(_ContactSuffix_QNAME, String.class, Contact.class, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link String }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", name = "Fax", scope = Contact.class)
    public JAXBElement<String> createContactFax(String value) {
        return new JAXBElement<>(_ContactFax_QNAME, String.class, Contact.class, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link Email }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", name = "Email", scope = Contact.class)
    public JAXBElement<Email> createContactEmail(Email value) {
        return new JAXBElement<>(_Email_QNAME, Email.class, Contact.class, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link String }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", name = "Phone", scope = Contact.class)
    public JAXBElement<String> createContactPhone(String value) {
        return new JAXBElement<>(_ContactPhone_QNAME, String.class, Contact.class, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link String }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", name = "MiddleName", scope = Contact.class)
    public JAXBElement<String> createContactMiddleName(String value) {
        return new JAXBElement<>(_ContactMiddleName_QNAME, String.class, Contact.class, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link CreditCardTransaction }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Parameters.CreditCardTransactionParameters", name = "CreditCardTransaction", scope = ForceParams.class)
    public JAXBElement<CreditCardTransaction> createForceParamsCreditCardTransaction(CreditCardTransaction value) {
        return new JAXBElement<>(_ForceParamsCreditCardTransaction_QNAME,
                CreditCardTransaction.class, ForceParams.class, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link TerminalIdentifier }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Parameters.CreditCardTransactionParameters", name = "TerminalIdentifier", scope = ForceParams.class)
    public JAXBElement<TerminalIdentifier> createForceParamsTerminalIdentifier(TerminalIdentifier value) {
        return new JAXBElement<>(_ForceParamsTerminalIdentifier_QNAME, TerminalIdentifier.class,
                ForceParams.class, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link String }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Parameters.CreditCardTransactionParameters", name = "AuthCode", scope = ForceParams.class)
    public JAXBElement<String> createForceParamsAuthCode(String value) {
        return new JAXBElement<>(_ForceParamsAuthCode_QNAME, String.class, ForceParams.class, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link CreditCardTransactionResult }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://3DSI.org/WebServices/CreditCardTransaction", name = "VoidResult", scope = VoidResponse.class)
    public JAXBElement<CreditCardTransactionResult> createVoidResponseVoidResult(CreditCardTransactionResult value) {
        return new JAXBElement<>(_VoidResponseVoidResult_QNAME,
                CreditCardTransactionResult.class, VoidResponse.class, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link ClientCredentials }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://3DSI.org/WebServices/CreditCardTransaction", name = "clientCredentials", scope = QuickForce.class)
    public JAXBElement<ClientCredentials> createQuickForceClientCredentials(ClientCredentials value) {
        return new JAXBElement<>(_VoidClientCredentials_QNAME, ClientCredentials.class,
                QuickForce.class, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link QuickForceParams }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://3DSI.org/WebServices/CreditCardTransaction", name = "quickForceParams", scope = QuickForce.class)
    public JAXBElement<QuickForceParams> createQuickForceQuickForceParams(QuickForceParams value) {
        return new JAXBElement<>(_QuickForceQuickForceParams_QNAME, QuickForceParams.class,
                QuickForce.class, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link CreditCardTransaction }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Parameters.CreditCardTransactionParameters", name = "CreditCardTransaction", scope = AuthorizeParams.class)
    public JAXBElement<CreditCardTransaction> createAuthorizeParamsCreditCardTransaction(CreditCardTransaction value) {
        return new JAXBElement<>(_ForceParamsCreditCardTransaction_QNAME,
                CreditCardTransaction.class, AuthorizeParams.class, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link TerminalIdentifier }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Parameters.CreditCardTransactionParameters", name = "TerminalIdentifier", scope = AuthorizeParams.class)
    public JAXBElement<TerminalIdentifier> createAuthorizeParamsTerminalIdentifier(TerminalIdentifier value) {
        return new JAXBElement<>(_ForceParamsTerminalIdentifier_QNAME, TerminalIdentifier.class,
                AuthorizeParams.class, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link CreditCardTransactionVerificationResult }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://3DSI.org/WebServices/CreditCardTransaction", name = "CardVerificationResult", scope = CardVerificationResponse.class)
    public JAXBElement<CreditCardTransactionVerificationResult> createCardVerificationResponseCardVerificationResult(
            CreditCardTransactionVerificationResult value) {
        return new JAXBElement<>(
                _CardVerificationResponseCardVerificationResult_QNAME, CreditCardTransactionVerificationResult.class,
                CardVerificationResponse.class, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link CreditCardTransactionResult }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://3DSI.org/WebServices/CreditCardTransaction", name = "ForceResult", scope = ForceResponse.class)
    public JAXBElement<CreditCardTransactionResult> createForceResponseForceResult(CreditCardTransactionResult value) {
        return new JAXBElement<>(_ForceResponseForceResult_QNAME,
                CreditCardTransactionResult.class, ForceResponse.class, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link String }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", name = "City", scope = Address.class)
    public JAXBElement<String> createAddressCity(String value) {
        return new JAXBElement<>(_AddressCity_QNAME, String.class, Address.class, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link String }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", name = "AddressLine2", scope = Address.class)
    public JAXBElement<String> createAddressAddressLine2(String value) {
        return new JAXBElement<>(_AddressAddressLine2_QNAME, String.class, Address.class, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link String }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", name = "AddressLine1", scope = Address.class)
    public JAXBElement<String> createAddressAddressLine1(String value) {
        return new JAXBElement<>(_AddressAddressLine1_QNAME, String.class, Address.class, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link String }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", name = "PostalCode", scope = Address.class)
    public JAXBElement<String> createAddressPostalCode(String value) {
        return new JAXBElement<>(_AddressPostalCode_QNAME, String.class, Address.class, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link String }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ApplicationError", name = "Message", scope = ApplicationFault.class)
    public JAXBElement<String> createApplicationFaultMessage(String value) {
        return new JAXBElement<>(_ApplicationFaultMessage_QNAME, String.class, ApplicationFault.class, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link CreditCardTransactionAuthorizationResult }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://3DSI.org/WebServices/CreditCardTransaction", name = "AuthorizeAndCaptureResult", scope = AuthorizeAndCaptureResponse.class)
    public JAXBElement<CreditCardTransactionAuthorizationResult> createAuthorizeAndCaptureResponseAuthorizeAndCaptureResult(
            CreditCardTransactionAuthorizationResult value) {
        return new JAXBElement<>(
                _AuthorizeAndCaptureResponseAuthorizeAndCaptureResult_QNAME,
                CreditCardTransactionAuthorizationResult.class, AuthorizeAndCaptureResponse.class, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link String }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", name = "TerminalCode", scope = TerminalIdentifier.class)
    public JAXBElement<String> createTerminalIdentifierTerminalCode(String value) {
        return new JAXBElement<>(_TerminalIdentifierTerminalCode_QNAME, String.class, TerminalIdentifier.class,
                value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link String }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", name = "MerchantCode", scope = TerminalIdentifier.class)
    public JAXBElement<String> createTerminalIdentifierMerchantCode(String value) {
        return new JAXBElement<>(_TerminalIdentifierMerchantCode_QNAME, String.class, TerminalIdentifier.class,
                value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link String }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", name = "LocationCode", scope = TerminalIdentifier.class)
    public JAXBElement<String> createTerminalIdentifierLocationCode(String value) {
        return new JAXBElement<>(_TerminalIdentifierLocationCode_QNAME, String.class, TerminalIdentifier.class,
                value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link String }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardTransactionDefinitions", name = "UnitOfMeasure", scope = LineItemDetailPurchasing.class)
    public JAXBElement<String> createLineItemDetailPurchasingUnitOfMeasure(String value) {
        return new JAXBElement<>(_LineItemDetailPurchasingUnitOfMeasure_QNAME, String.class,
                LineItemDetailPurchasing.class, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link String }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardTransactionDefinitions", name = "CommodityCode", scope = LineItemDetailPurchasing.class)
    public JAXBElement<String> createLineItemDetailPurchasingCommodityCode(String value) {
        return new JAXBElement<>(_LineItemDetailPurchasingCommodityCode_QNAME, String.class,
                LineItemDetailPurchasing.class, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link String }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardTransactionDefinitions", name = "ProductCode", scope = LineItemDetailPurchasing.class)
    public JAXBElement<String> createLineItemDetailPurchasingProductCode(String value) {
        return new JAXBElement<>(_LineItemDetailPurchasingProductCode_QNAME, String.class,
                LineItemDetailPurchasing.class, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link String }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardTransactionDefinitions", name = "Description", scope = LineItemDetailPurchasing.class)
    public JAXBElement<String> createLineItemDetailPurchasingDescription(String value) {
        return new JAXBElement<>(_LineItemDetailPurchasingDescription_QNAME, String.class,
                LineItemDetailPurchasing.class, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link ClientCredentials }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://3DSI.org/WebServices/CreditCardTransaction", name = "clientCredentials", scope = QuickCapture.class)
    public JAXBElement<ClientCredentials> createQuickCaptureClientCredentials(ClientCredentials value) {
        return new JAXBElement<>(_VoidClientCredentials_QNAME, ClientCredentials.class,
                QuickCapture.class, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link QuickCaptureParams }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://3DSI.org/WebServices/CreditCardTransaction", name = "quickCaptureParams", scope = QuickCapture.class)
    public JAXBElement<QuickCaptureParams> createQuickCaptureQuickCaptureParams(QuickCaptureParams value) {
        return new JAXBElement<>(_QuickCaptureQuickCaptureParams_QNAME, QuickCaptureParams.class,
                QuickCapture.class, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link String }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", name = "Token", scope = StoredCardIdentifier.class)
    public JAXBElement<String> createStoredCardIdentifierToken(String value) {
        return new JAXBElement<>(_StoredCardIdentifierToken_QNAME, String.class, StoredCardIdentifier.class,
                value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link String }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", name = "CustomerCode", scope = StoredCardIdentifier.class)
    public JAXBElement<String> createStoredCardIdentifierCustomerCode(String value) {
        return new JAXBElement<>(_StoredCardIdentifierCustomerCode_QNAME, String.class,
                StoredCardIdentifier.class, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link ClientCredentials }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://3DSI.org/WebServices/CreditCardTransaction", name = "clientCredentials", scope = Authorize.class)
    public JAXBElement<ClientCredentials> createAuthorizeClientCredentials(ClientCredentials value) {
        return new JAXBElement<>(_VoidClientCredentials_QNAME, ClientCredentials.class,
                Authorize.class, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link AuthorizeParams }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://3DSI.org/WebServices/CreditCardTransaction", name = "authorizeParams", scope = Authorize.class)
    public JAXBElement<AuthorizeParams> createAuthorizeAuthorizeParams(AuthorizeParams value) {
        return new JAXBElement<>(_AuthorizeAuthorizeParams_QNAME, AuthorizeParams.class,
                Authorize.class, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link CreditCardTransactionResult }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://3DSI.org/WebServices/CreditCardTransaction", name = "QuickRefundResult", scope = QuickRefundResponse.class)
    public JAXBElement<CreditCardTransactionResult> createQuickRefundResponseQuickRefundResult(
            CreditCardTransactionResult value) {
        return new JAXBElement<>(_QuickRefundResponseQuickRefundResult_QNAME,
                CreditCardTransactionResult.class, QuickRefundResponse.class, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link CreditCardTransactionResult }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://3DSI.org/WebServices/CreditCardTransaction", name = "QuickForceResult", scope = QuickForceResponse.class)
    public JAXBElement<CreditCardTransactionResult> createQuickForceResponseQuickForceResult(
            CreditCardTransactionResult value) {
        return new JAXBElement<>(_QuickForceResponseQuickForceResult_QNAME,
                CreditCardTransactionResult.class, QuickForceResponse.class, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link CreditCardTransactionResult }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://3DSI.org/WebServices/CreditCardTransaction", name = "CreditResult", scope = CreditResponse.class)
    public JAXBElement<CreditCardTransactionResult> createCreditResponseCreditResult(CreditCardTransactionResult value) {
        return new JAXBElement<>(_CreditResponseCreditResult_QNAME,
                CreditCardTransactionResult.class, CreditResponse.class, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link CreditCardTransactionResult }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://3DSI.org/WebServices/CreditCardTransaction", name = "QuickCaptureResult", scope = QuickCaptureResponse.class)
    public JAXBElement<CreditCardTransactionResult> createQuickCaptureResponseQuickCaptureResult(
            CreditCardTransactionResult value) {
        return new JAXBElement<>(_QuickCaptureResponseQuickCaptureResult_QNAME,
                CreditCardTransactionResult.class, QuickCaptureResponse.class, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link CreditCardTransactionResult }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://3DSI.org/WebServices/CreditCardTransaction", name = "CaptureResult", scope = CaptureResponse.class)
    public JAXBElement<CreditCardTransactionResult> createCaptureResponseCaptureResult(CreditCardTransactionResult value) {
        return new JAXBElement<>(_CaptureResponseCaptureResult_QNAME,
                CreditCardTransactionResult.class, CaptureResponse.class, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link String }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardTransactionDefinitions", name = "TransactionKey", scope = CreditCardTransaction.class)
    public JAXBElement<String> createCreditCardTransactionTransactionKey(String value) {
        return new JAXBElement<>(_CreditCardTransactionTransactionKey_QNAME, String.class,
                CreditCardTransaction.class, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link String }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardTransactionDefinitions", name = "InvoiceNumber", scope = CreditCardTransaction.class)
    public JAXBElement<String> createCreditCardTransactionInvoiceNumber(String value) {
        return new JAXBElement<>(_CreditCardTransactionInvoiceNumber_QNAME, String.class,
                CreditCardTransaction.class, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link String }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardTransactionDefinitions", name = "Note", scope = CreditCardTransaction.class)
    public JAXBElement<String> createCreditCardTransactionNote(String value) {
        return new JAXBElement<>(_CreditCardTransactionNote_QNAME, String.class, CreditCardTransaction.class,
                value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link StoredCardIdentifier }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardTransactionDefinitions", name = "StoredCardIdentifier", scope = CreditCardTransaction.class)
    public JAXBElement<StoredCardIdentifier> createCreditCardTransactionStoredCardIdentifier(StoredCardIdentifier value) {
        return new JAXBElement<>(_CreditCardTransactionStoredCardIdentifier_QNAME,
                StoredCardIdentifier.class, CreditCardTransaction.class, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link AdditionalAmounts }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardTransactionDefinitions", name = "AdditionalAmounts", scope = CreditCardTransaction.class)
    public JAXBElement<AdditionalAmounts> createCreditCardTransactionAdditionalAmounts(AdditionalAmounts value) {
        return new JAXBElement<>(_AdditionalAmounts_QNAME, AdditionalAmounts.class,
                CreditCardTransaction.class, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link String }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardTransactionDefinitions", name = "ShipFromPostalCode", scope = CreditCardTransaction.class)
    public JAXBElement<String> createCreditCardTransactionShipFromPostalCode(String value) {
        return new JAXBElement<>(_CreditCardTransactionShipFromPostalCode_QNAME, String.class,
                CreditCardTransaction.class, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link String }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardTransactionDefinitions", name = "CustomerReferenceValue", scope = CreditCardTransaction.class)
    public JAXBElement<String> createCreditCardTransactionCustomerReferenceValue(String value) {
        return new JAXBElement<>(_CreditCardTransactionCustomerReferenceValue_QNAME, String.class,
                CreditCardTransaction.class, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link String }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardTransactionDefinitions", name = "OrderNumber", scope = CreditCardTransaction.class)
    public JAXBElement<String> createCreditCardTransactionOrderNumber(String value) {
        return new JAXBElement<>(_CreditCardTransactionOrderNumber_QNAME, String.class,
                CreditCardTransaction.class, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link ArrayOfEmail }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardTransactionDefinitions", name = "ReceiptEmailAddress", scope = CreditCardTransaction.class)
    public JAXBElement<ArrayOfEmail> createCreditCardTransactionReceiptEmailAddress(ArrayOfEmail value) {
        return new JAXBElement<>(_CreditCardTransactionReceiptEmailAddress_QNAME, ArrayOfEmail.class,
                CreditCardTransaction.class, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link ArrayOfstring }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardTransactionDefinitions", name = "UserDefinedFields", scope = CreditCardTransaction.class)
    public JAXBElement<ArrayOfstring> createCreditCardTransactionUserDefinedFields(ArrayOfstring value) {
        return new JAXBElement<>(_CreditCardTransactionUserDefinedFields_QNAME, ArrayOfstring.class,
                CreditCardTransaction.class, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link ArrayOfLineItemDetail }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardTransactionDefinitions", name = "LineDetail", scope = CreditCardTransaction.class)
    public JAXBElement<ArrayOfLineItemDetail> createCreditCardTransactionLineDetail(ArrayOfLineItemDetail value) {
        return new JAXBElement<>(_CreditCardTransactionLineDetail_QNAME,
                ArrayOfLineItemDetail.class, CreditCardTransaction.class, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link CreditCardTransactionCreditCard }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardTransactionDefinitions", name = "CreditCard", scope = CreditCardTransaction.class)
    public JAXBElement<CreditCardTransactionCreditCard> createCreditCardTransactionCreditCard1(
            CreditCardTransactionCreditCard value) {
        return new JAXBElement<>(_CreditCardTransactionCreditCard1_QNAME,
                CreditCardTransactionCreditCard.class, CreditCardTransaction.class, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link ClientCredentials }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://3DSI.org/WebServices/CreditCardTransaction", name = "clientCredentials", scope = Capture.class)
    public JAXBElement<ClientCredentials> createCaptureClientCredentials(ClientCredentials value) {
        return new JAXBElement<>(_VoidClientCredentials_QNAME, ClientCredentials.class, Capture.class,
                value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link CaptureParams }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://3DSI.org/WebServices/CreditCardTransaction", name = "captureParams", scope = Capture.class)
    public JAXBElement<CaptureParams> createCaptureCaptureParams(CaptureParams value) {
        return new JAXBElement<>(_CaptureCaptureParams_QNAME, CaptureParams.class, Capture.class, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link ClientCredentials }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://3DSI.org/WebServices/CreditCardTransaction", name = "clientCredentials", scope = CardVerification.class)
    public JAXBElement<ClientCredentials> createCardVerificationClientCredentials(ClientCredentials value) {
        return new JAXBElement<>(_VoidClientCredentials_QNAME, ClientCredentials.class,
                CardVerification.class, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link CardVerificationParams }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://3DSI.org/WebServices/CreditCardTransaction", name = "cardVerificationParams", scope = CardVerification.class)
    public JAXBElement<CardVerificationParams> createCardVerificationCardVerificationParams(CardVerificationParams value) {
        return new JAXBElement<>(_CardVerificationCardVerificationParams_QNAME,
                CardVerificationParams.class, CardVerification.class, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link String }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardTransactionDefinitions", name = "TrackData", scope = CreditCardTransactionCreditCard.class)
    public JAXBElement<String> createCreditCardTransactionCreditCardTrackData(String value) {
        return new JAXBElement<>(_CreditCardTransactionCreditCardTrackData_QNAME, String.class,
                CreditCardTransactionCreditCard.class, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link String }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardTransactionDefinitions", name = "CardSecurityCode", scope = CreditCardTransactionCreditCard.class)
    public JAXBElement<String> createCreditCardTransactionCreditCardCardSecurityCode(String value) {
        return new JAXBElement<>(_CreditCardTransactionCreditCardCardSecurityCode_QNAME, String.class,
                CreditCardTransactionCreditCard.class, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link TerminalIdentifier }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Parameters.CreditCardTransactionParameters", name = "TerminalIdentifier", scope = OriginalTransactionParams.class)
    public JAXBElement<TerminalIdentifier> createOriginalTransactionParamsTerminalIdentifier(TerminalIdentifier value) {
        return new JAXBElement<>(_ForceParamsTerminalIdentifier_QNAME, TerminalIdentifier.class,
                OriginalTransactionParams.class, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link String }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Parameters.CreditCardTransactionParameters", name = "OriginalTransactionKey", scope = OriginalTransactionParams.class)
    public JAXBElement<String> createOriginalTransactionParamsOriginalTransactionKey(String value) {
        return new JAXBElement<>(_OriginalTransactionParamsOriginalTransactionKey_QNAME, String.class,
                OriginalTransactionParams.class, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link CreditCardTransaction }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Parameters.CreditCardTransactionParameters", name = "CreditCardTransaction", scope = RefundParams.class)
    public JAXBElement<CreditCardTransaction> createRefundParamsCreditCardTransaction(CreditCardTransaction value) {
        return new JAXBElement<>(_ForceParamsCreditCardTransaction_QNAME,
                CreditCardTransaction.class, RefundParams.class, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link TerminalIdentifier }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Parameters.CreditCardTransactionParameters", name = "TerminalIdentifier", scope = RefundParams.class)
    public JAXBElement<TerminalIdentifier> createRefundParamsTerminalIdentifier(TerminalIdentifier value) {
        return new JAXBElement<>(_ForceParamsTerminalIdentifier_QNAME, TerminalIdentifier.class,
                RefundParams.class, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link String }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Parameters.CreditCardTransactionParameters", name = "OriginalTransactionKey", scope = RefundParams.class)
    public JAXBElement<String> createRefundParamsOriginalTransactionKey(String value) {
        return new JAXBElement<>(_OriginalTransactionParamsOriginalTransactionKey_QNAME, String.class,
                RefundParams.class, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link CreditCardTransactionResult }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://3DSI.org/WebServices/CreditCardTransaction", name = "RefundResult", scope = RefundResponse.class)
    public JAXBElement<CreditCardTransactionResult> createRefundResponseRefundResult(CreditCardTransactionResult value) {
        return new JAXBElement<>(_RefundResponseRefundResult_QNAME,
                CreditCardTransactionResult.class, RefundResponse.class, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link StoredCardIdentifier }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Parameters.CreditCardTransactionParameters", name = "StoredCardIdentifier", scope = CardVerificationParams.class)
    public JAXBElement<StoredCardIdentifier> createCardVerificationParamsStoredCardIdentifier(StoredCardIdentifier value) {
        return new JAXBElement<>(_CardVerificationParamsStoredCardIdentifier_QNAME,
                StoredCardIdentifier.class, CardVerificationParams.class, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link String }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Parameters.CreditCardTransactionParameters", name = "TransactionKey", scope = CardVerificationParams.class)
    public JAXBElement<String> createCardVerificationParamsTransactionKey(String value) {
        return new JAXBElement<>(_CardVerificationParamsTransactionKey_QNAME, String.class,
                CardVerificationParams.class, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link CreditCardTransactionCreditCard }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Parameters.CreditCardTransactionParameters", name = "CreditCard", scope = CardVerificationParams.class)
    public JAXBElement<CreditCardTransactionCreditCard> createCardVerificationParamsCreditCard(
            CreditCardTransactionCreditCard value) {
        return new JAXBElement<>(_CardVerificationParamsCreditCard_QNAME,
                CreditCardTransactionCreditCard.class, CardVerificationParams.class, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link TerminalIdentifier }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Parameters.CreditCardTransactionParameters", name = "TerminalIdentifier", scope = CardVerificationParams.class)
    public JAXBElement<TerminalIdentifier> createCardVerificationParamsTerminalIdentifier(TerminalIdentifier value) {
        return new JAXBElement<>(_ForceParamsTerminalIdentifier_QNAME, TerminalIdentifier.class,
                CardVerificationParams.class, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link String }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Parameters.CreditCardTransactionParameters", name = "TransactionKey", scope = QuickRefundParams.class)
    public JAXBElement<String> createQuickRefundParamsTransactionKey(String value) {
        return new JAXBElement<>(_CardVerificationParamsTransactionKey_QNAME, String.class,
                QuickRefundParams.class, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link CreditCardTransactionAuthorizationResult }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://3DSI.org/WebServices/CreditCardTransaction", name = "AuthorizeResult", scope = AuthorizeResponse.class)
    public JAXBElement<CreditCardTransactionAuthorizationResult> createAuthorizeResponseAuthorizeResult(
            CreditCardTransactionAuthorizationResult value) {
        return new JAXBElement<>(_AuthorizeResponseAuthorizeResult_QNAME,
                CreditCardTransactionAuthorizationResult.class, AuthorizeResponse.class, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link CreditCardTransaction }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Parameters.CreditCardTransactionParameters", name = "CreditCardTransaction", scope = AuthorizeAndCaptureParams.class)
    public JAXBElement<CreditCardTransaction> createAuthorizeAndCaptureParamsCreditCardTransaction(
            CreditCardTransaction value) {
        return new JAXBElement<>(_ForceParamsCreditCardTransaction_QNAME,
                CreditCardTransaction.class, AuthorizeAndCaptureParams.class, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link TerminalIdentifier }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Parameters.CreditCardTransactionParameters", name = "TerminalIdentifier", scope = AuthorizeAndCaptureParams.class)
    public JAXBElement<TerminalIdentifier> createAuthorizeAndCaptureParamsTerminalIdentifier(TerminalIdentifier value) {
        return new JAXBElement<>(_ForceParamsTerminalIdentifier_QNAME, TerminalIdentifier.class,
                AuthorizeAndCaptureParams.class, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link String }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", name = "CardAccountNumber", scope = CreditCard.class)
    public JAXBElement<String> createCreditCardCardAccountNumber(String value) {
        return new JAXBElement<>(_CreditCardCardAccountNumber_QNAME, String.class, CreditCard.class, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link ArrayOfEmail }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", name = "ReceiptEmailAddresses", scope = CreditCard.class)
    public JAXBElement<ArrayOfEmail> createCreditCardReceiptEmailAddresses(ArrayOfEmail value) {
        return new JAXBElement<>(_CreditCardReceiptEmailAddresses_QNAME, ArrayOfEmail.class,
                CreditCard.class, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link Address }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", name = "BillingAddress", scope = CreditCard.class)
    public JAXBElement<Address> createCreditCardBillingAddress(Address value) {
        return new JAXBElement<>(_CreditCardBillingAddress_QNAME, Address.class, CreditCard.class, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link Address }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", name = "ShippingAddress", scope = CreditCard.class)
    public JAXBElement<Address> createCreditCardShippingAddress(Address value) {
        return new JAXBElement<>(_CreditCardShippingAddress_QNAME, Address.class, CreditCard.class, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link String }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", name = "NameOnCard", scope = CreditCard.class)
    public JAXBElement<String> createCreditCardNameOnCard(String value) {
        return new JAXBElement<>(_CreditCardNameOnCard_QNAME, String.class, CreditCard.class, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link Contact }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", name = "Cardholder", scope = CreditCard.class)
    public JAXBElement<Contact> createCreditCardCardholder(Contact value) {
        return new JAXBElement<>(_CreditCardCardholder_QNAME, Contact.class, CreditCard.class, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link ClientCredentials }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://3DSI.org/WebServices/CreditCardTransaction", name = "clientCredentials", scope = AuthorizeAndCapture.class)
    public JAXBElement<ClientCredentials> createAuthorizeAndCaptureClientCredentials(ClientCredentials value) {
        return new JAXBElement<>(_VoidClientCredentials_QNAME, ClientCredentials.class,
                AuthorizeAndCapture.class, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link AuthorizeAndCaptureParams }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://3DSI.org/WebServices/CreditCardTransaction", name = "authorizeAndCaptureParams", scope = AuthorizeAndCapture.class)
    public JAXBElement<AuthorizeAndCaptureParams> createAuthorizeAndCaptureAuthorizeAndCaptureParams(
            AuthorizeAndCaptureParams value) {
        return new JAXBElement<>(_AuthorizeAndCaptureAuthorizeAndCaptureParams_QNAME,
                AuthorizeAndCaptureParams.class, AuthorizeAndCapture.class, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link ClientCredentials }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://3DSI.org/WebServices/CreditCardTransaction", name = "clientCredentials", scope = Credit.class)
    public JAXBElement<ClientCredentials> createCreditClientCredentials(ClientCredentials value) {
        return new JAXBElement<>(_VoidClientCredentials_QNAME, ClientCredentials.class, Credit.class,
                value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link CreditParams }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://3DSI.org/WebServices/CreditCardTransaction", name = "creditParams", scope = Credit.class)
    public JAXBElement<CreditParams> createCreditCreditParams(CreditParams value) {
        return new JAXBElement<>(_CreditCreditParams_QNAME, CreditParams.class, Credit.class, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link ArrayOfstring }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", name = "ValidationFailures", scope = WebServiceResult.class)
    public JAXBElement<ArrayOfstring> createWebServiceResultValidationFailures(ArrayOfstring value) {
        return new JAXBElement<>(_WebServiceResultValidationFailures_QNAME, ArrayOfstring.class,
                WebServiceResult.class, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link String }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", name = "Address", scope = Email.class)
    public JAXBElement<String> createEmailAddress(String value) {
        return new JAXBElement<>(_Address_QNAME, String.class, Email.class, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link String }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", name = "DisplayAs", scope = Email.class)
    public JAXBElement<String> createEmailDisplayAs(String value) {
        return new JAXBElement<>(_EmailDisplayAs_QNAME, String.class, Email.class, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link CreditCardTransactionStatusResult }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://3DSI.org/WebServices/CreditCardTransaction", name = "GetTransactionStatusResult", scope = GetTransactionStatusResponse.class)
    public JAXBElement<CreditCardTransactionStatusResult> createGetTransactionStatusResponseGetTransactionStatusResult(
            CreditCardTransactionStatusResult value) {
        return new JAXBElement<>(
                _GetTransactionStatusResponseGetTransactionStatusResult_QNAME, CreditCardTransactionStatusResult.class,
                GetTransactionStatusResponse.class, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link String }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardTransactionDefinitions", name = "TransactionKey", scope = CreditCardTransactionResult.class)
    public JAXBElement<String> createCreditCardTransactionResultTransactionKey(String value) {
        return new JAXBElement<>(_CreditCardTransactionTransactionKey_QNAME, String.class,
                CreditCardTransactionResult.class, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link String }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardTransactionDefinitions", name = "ProcessorResponse", scope = CreditCardTransactionResult.class)
    public JAXBElement<String> createCreditCardTransactionResultProcessorResponse(String value) {
        return new JAXBElement<>(_CreditCardTransactionResultProcessorResponse_QNAME, String.class,
                CreditCardTransactionResult.class, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link ArrayOfRuleInfringement }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardTransactionDefinitions", name = "RuleInfringements", scope = CreditCardTransactionResult.class)
    public JAXBElement<ArrayOfRuleInfringement> createCreditCardTransactionResultRuleInfringements(
            ArrayOfRuleInfringement value) {
        return new JAXBElement<>(_CreditCardTransactionResultRuleInfringements_QNAME,
                ArrayOfRuleInfringement.class, CreditCardTransactionResult.class, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link String }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardTransactionDefinitions", name = "Token", scope = CreditCardTransactionResult.class)
    public JAXBElement<String> createCreditCardTransactionResultToken(String value) {
        return new JAXBElement<>(_CreditCardTransactionResultToken_QNAME, String.class,
                CreditCardTransactionResult.class, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link ArrayOfTransactionWarning }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardTransactionDefinitions", name = "TransactionWarning", scope = CreditCardTransactionResult.class)
    public JAXBElement<ArrayOfTransactionWarning> createCreditCardTransactionResultTransactionWarning(
            ArrayOfTransactionWarning value) {
        return new JAXBElement<>(_CreditCardTransactionResultTransactionWarning_QNAME,
                ArrayOfTransactionWarning.class, CreditCardTransactionResult.class, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link String }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardTransactionDefinitions", name = "ThirdPartyResponse", scope = CreditCardTransactionResult.class)
    public JAXBElement<String> createCreditCardTransactionResultThirdPartyResponse(String value) {
        return new JAXBElement<>(_CreditCardTransactionResultThirdPartyResponse_QNAME, String.class,
                CreditCardTransactionResult.class, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link String }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardTransactionDefinitions", name = "ProcessorAvsResponse", scope = CreditCardTransactionVerificationResult.class)
    public JAXBElement<String> createCreditCardTransactionVerificationResultProcessorAvsResponse(String value) {
        return new JAXBElement<>(_CreditCardTransactionVerificationResultProcessorAvsResponse_QNAME,
                String.class, CreditCardTransactionVerificationResult.class, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link String }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardTransactionDefinitions", name = "ProcessorCardSecurityCodeResponse", scope = CreditCardTransactionVerificationResult.class)
    public JAXBElement<String> createCreditCardTransactionVerificationResultProcessorCardSecurityCodeResponse(
            String value) {
        return new JAXBElement<>(_CreditCardTransactionVerificationResultProcessorCardSecurityCodeResponse_QNAME,
                String.class, CreditCardTransactionVerificationResult.class, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link String }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Parameters.CreditCardTransactionParameters", name = "AuthCode", scope = QuickForceParams.class)
    public JAXBElement<String> createQuickForceParamsAuthCode(String value) {
        return new JAXBElement<>(_ForceParamsAuthCode_QNAME, String.class, QuickForceParams.class, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link CreditCardTransaction }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Parameters.CreditCardTransactionParameters", name = "CreditCardTransaction", scope = CaptureParams.class)
    public JAXBElement<CreditCardTransaction> createCaptureParamsCreditCardTransaction(CreditCardTransaction value) {
        return new JAXBElement<>(_ForceParamsCreditCardTransaction_QNAME,
                CreditCardTransaction.class, CaptureParams.class, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link TerminalIdentifier }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Parameters.CreditCardTransactionParameters", name = "TerminalIdentifier", scope = CaptureParams.class)
    public JAXBElement<TerminalIdentifier> createCaptureParamsTerminalIdentifier(TerminalIdentifier value) {
        return new JAXBElement<>(_ForceParamsTerminalIdentifier_QNAME, TerminalIdentifier.class,
                CaptureParams.class, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link String }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", name = "UserName", scope = ClientCredentials.class)
    public JAXBElement<String> createClientCredentialsUserName(String value) {
        return new JAXBElement<>(_ClientCredentialsUserName_QNAME, String.class, ClientCredentials.class, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link String }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", name = "Password", scope = ClientCredentials.class)
    public JAXBElement<String> createClientCredentialsPassword(String value) {
        return new JAXBElement<>(_ClientCredentialsPassword_QNAME, String.class, ClientCredentials.class, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link String }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", name = "ClientCode", scope = ClientCredentials.class)
    public JAXBElement<String> createClientCredentialsClientCode(String value) {
        return new JAXBElement<>(_ClientCredentialsClientCode_QNAME, String.class, ClientCredentials.class, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link ClientCredentials }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://3DSI.org/WebServices/CreditCardTransaction", name = "clientCredentials", scope = Refund.class)
    public JAXBElement<ClientCredentials> createRefundClientCredentials(ClientCredentials value) {
        return new JAXBElement<>(_VoidClientCredentials_QNAME, ClientCredentials.class, Refund.class,
                value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link RefundParams }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://3DSI.org/WebServices/CreditCardTransaction", name = "refundParams", scope = Refund.class)
    public JAXBElement<RefundParams> createRefundRefundParams(RefundParams value) {
        return new JAXBElement<>(_RefundRefundParams_QNAME, RefundParams.class, Refund.class, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link String }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardTransactionDefinitions", name = "EmployeeName", scope = LineItemDetailTempServices.class)
    public JAXBElement<String> createLineItemDetailTempServicesEmployeeName(String value) {
        return new JAXBElement<>(_LineItemDetailTempServicesEmployeeName_QNAME, String.class,
                LineItemDetailTempServices.class, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link String }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardTransactionDefinitions", name = "PositionCode", scope = LineItemDetailTempServices.class)
    public JAXBElement<String> createLineItemDetailTempServicesPositionCode(String value) {
        return new JAXBElement<>(_LineItemDetailTempServicesPositionCode_QNAME, String.class,
                LineItemDetailTempServices.class, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link String }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardTransactionDefinitions", name = "EmployeeId", scope = LineItemDetailTempServices.class)
    public JAXBElement<String> createLineItemDetailTempServicesEmployeeId(String value) {
        return new JAXBElement<>(_LineItemDetailTempServicesEmployeeId_QNAME, String.class,
                LineItemDetailTempServices.class, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link String }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardTransactionDefinitions", name = "ProjectCode", scope = LineItemDetailTempServices.class)
    public JAXBElement<String> createLineItemDetailTempServicesProjectCode(String value) {
        return new JAXBElement<>(_LineItemDetailTempServicesProjectCode_QNAME, String.class,
                LineItemDetailTempServices.class, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link String }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardTransactionDefinitions", name = "PhoneNumber", scope = LineItemDetailTempServices.class)
    public JAXBElement<String> createLineItemDetailTempServicesPhoneNumber(String value) {
        return new JAXBElement<>(_LineItemDetailTempServicesPhoneNumber_QNAME, String.class,
                LineItemDetailTempServices.class, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link String }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardTransactionDefinitions", name = "Supervisor", scope = LineItemDetailTempServices.class)
    public JAXBElement<String> createLineItemDetailTempServicesSupervisor(String value) {
        return new JAXBElement<>(_LineItemDetailTempServicesSupervisor_QNAME, String.class,
                LineItemDetailTempServices.class, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link String }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardTransactionDefinitions", name = "UnitOfMeasure", scope = LineItemDetailTempServices.class)
    public JAXBElement<String> createLineItemDetailTempServicesUnitOfMeasure(String value) {
        return new JAXBElement<>(_LineItemDetailPurchasingUnitOfMeasure_QNAME, String.class,
                LineItemDetailTempServices.class, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link String }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardTransactionDefinitions", name = "TrackingNumber", scope = LineItemDetailTempServices.class)
    public JAXBElement<String> createLineItemDetailTempServicesTrackingNumber(String value) {
        return new JAXBElement<>(_LineItemDetailTempServicesTrackingNumber_QNAME, String.class,
                LineItemDetailTempServices.class, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link String }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardTransactionDefinitions", name = "CostCenter", scope = LineItemDetailTempServices.class)
    public JAXBElement<String> createLineItemDetailTempServicesCostCenter(String value) {
        return new JAXBElement<>(_LineItemDetailTempServicesCostCenter_QNAME, String.class,
                LineItemDetailTempServices.class, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link String }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardTransactionDefinitions", name = "UnionCode", scope = LineItemDetailTempServices.class)
    public JAXBElement<String> createLineItemDetailTempServicesUnionCode(String value) {
        return new JAXBElement<>(_LineItemDetailTempServicesUnionCode_QNAME, String.class,
                LineItemDetailTempServices.class, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link String }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardTransactionDefinitions", name = "RequestorId", scope = LineItemDetailTempServices.class)
    public JAXBElement<String> createLineItemDetailTempServicesRequestorId(String value) {
        return new JAXBElement<>(_LineItemDetailTempServicesRequestorId_QNAME, String.class,
                LineItemDetailTempServices.class, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link String }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardTransactionDefinitions", name = "ProjectDescription", scope = LineItemDetailTempServices.class)
    public JAXBElement<String> createLineItemDetailTempServicesProjectDescription(String value) {
        return new JAXBElement<>(_LineItemDetailTempServicesProjectDescription_QNAME, String.class,
                LineItemDetailTempServices.class, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link ClientCredentials }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://3DSI.org/WebServices/CreditCardTransaction", name = "clientCredentials", scope = QuickRefund.class)
    public JAXBElement<ClientCredentials> createQuickRefundClientCredentials(ClientCredentials value) {
        return new JAXBElement<>(_VoidClientCredentials_QNAME, ClientCredentials.class,
                QuickRefund.class, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link QuickRefundParams }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://3DSI.org/WebServices/CreditCardTransaction", name = "quickRefundParams", scope = QuickRefund.class)
    public JAXBElement<QuickRefundParams> createQuickRefundQuickRefundParams(QuickRefundParams value) {
        return new JAXBElement<>(_QuickRefundQuickRefundParams_QNAME, QuickRefundParams.class,
                QuickRefund.class, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link String }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardTransactionDefinitions", name = "AuthCode", scope = CreditCardTransactionAuthorizationResult.class)
    public JAXBElement<String> createCreditCardTransactionAuthorizationResultAuthCode(String value) {
        return new JAXBElement<>(_CreditCardTransactionAuthorizationResultAuthCode_QNAME, String.class,
                CreditCardTransactionAuthorizationResult.class, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link CreditCardTransaction }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Parameters.CreditCardTransactionParameters", name = "CreditCardTransaction", scope = CreditParams.class)
    public JAXBElement<CreditCardTransaction> createCreditParamsCreditCardTransaction(CreditCardTransaction value) {
        return new JAXBElement<>(_ForceParamsCreditCardTransaction_QNAME,
                CreditCardTransaction.class, CreditParams.class, value);
    }

    /**
     * Create an instance of
     * {@link JAXBElement }{@code <}{@link TerminalIdentifier }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Parameters.CreditCardTransactionParameters", name = "TerminalIdentifier", scope = CreditParams.class)
    public JAXBElement<TerminalIdentifier> createCreditParamsTerminalIdentifier(TerminalIdentifier value) {
        return new JAXBElement<>(_ForceParamsTerminalIdentifier_QNAME, TerminalIdentifier.class,
                CreditParams.class, value);
    }
}
