package com.apd.phoenix.service.payment.ws;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for CreditCardTransactionCreditCard complex type.
 *
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 *
 * <pre>
 * &lt;complexType name="CreditCardTransactionCreditCard">
 *   &lt;complexContent>
 *     &lt;extension base="{http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions}CreditCard">
 *       &lt;sequence>
 *         &lt;element name="CardSecurityCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CardSecurityCodeIndicator" type="{http://schemas.datacontract.org/2004/07/ThreeDelta.Common.Enums}CardSecurityCodeIndicator" minOccurs="0"/>
 *         &lt;element name="TrackData" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CreditCardTransactionCreditCard", namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardTransactionDefinitions", propOrder = {
        "cardSecurityCode", "cardSecurityCodeIndicator", "trackData" })
public class CreditCardTransactionCreditCard extends CreditCard {

    @XmlElementRef(name = "CardSecurityCode", namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardTransactionDefinitions", type = JAXBElement.class, required = false)
    protected JAXBElement<String> cardSecurityCode;
    @XmlElement(name = "CardSecurityCodeIndicator")
    protected CardSecurityCodeIndicator cardSecurityCodeIndicator;
    @XmlElementRef(name = "TrackData", namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardTransactionDefinitions", type = JAXBElement.class, required = false)
    protected JAXBElement<String> trackData;

    /**
     * Gets the value of the cardSecurityCode property.
     *
     * @return possible object is
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     *
     */
    public JAXBElement<String> getCardSecurityCode() {
        return cardSecurityCode;
    }

    /**
     * Sets the value of the cardSecurityCode property.
     *
     * @param value allowed object is
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     *
     */
    public void setCardSecurityCode(JAXBElement<String> value) {
        this.cardSecurityCode = value;
    }

    /**
     * Gets the value of the cardSecurityCodeIndicator property.
     *
     * @return possible object is {@link CardSecurityCodeIndicator }
     *
     */
    public CardSecurityCodeIndicator getCardSecurityCodeIndicator() {
        return cardSecurityCodeIndicator;
    }

    /**
     * Sets the value of the cardSecurityCodeIndicator property.
     *
     * @param value allowed object is {@link CardSecurityCodeIndicator }
     *
     */
    public void setCardSecurityCodeIndicator(CardSecurityCodeIndicator value) {
        this.cardSecurityCodeIndicator = value;
    }

    /**
     * Gets the value of the trackData property.
     *
     * @return possible object is
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     *
     */
    public JAXBElement<String> getTrackData() {
        return trackData;
    }

    /**
     * Sets the value of the trackData property.
     *
     * @param value allowed object is
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     *
     */
    public void setTrackData(JAXBElement<String> value) {
        this.trackData = value;
    }
}
