package com.apd.phoenix.service.card.management;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>Java class for HostedCardVaultSubmissionInfoResult complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="HostedCardVaultSubmissionInfoResult">
 *   &lt;complexContent>
 *     &lt;extension base="{http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardManagementDefinitions}CreditCardManagementResult">
 *       &lt;sequence>
 *         &lt;element name="CardAccountNumberLast4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CardStored" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="CardType" type="{http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Enums}CreditCardTypes" minOccurs="0"/>
 *         &lt;element name="CustomerIdentifier" type="{http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions}CustomerIdentifier" minOccurs="0"/>
 *         &lt;element name="ExpirationMonth" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="ExpirationYear" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="NonStoredCardFailureMessage" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NonStoredCardFailureReason" type="{http://schemas.datacontract.org/2004/07/ThreeDelta.Common.Enums}HostedCardVaultFailureReason" minOccurs="0"/>
 *         &lt;element name="Status" type="{http://schemas.datacontract.org/2004/07/ThreeDelta.Common.Enums}HostedCardVaultSubmissionStatus" minOccurs="0"/>
 *         &lt;element name="SubmissionId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SubmissionType" type="{http://schemas.datacontract.org/2004/07/ThreeDelta.Common.Enums}HostedCardVaultSubmissionType" minOccurs="0"/>
 *         &lt;element name="Token" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "HostedCardVaultSubmissionInfoResult", namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardManagementDefinitions", propOrder = {
        "cardAccountNumberLast4", "cardStored", "cardType", "customerIdentifier", "expirationMonth", "expirationYear",
        "nonStoredCardFailureMessage", "nonStoredCardFailureReason", "status", "submissionId", "submissionType",
        "token" })
public class HostedCardVaultSubmissionInfoResult extends CreditCardManagementResult {

    @XmlElementRef(name = "CardAccountNumberLast4", namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardManagementDefinitions", type = JAXBElement.class, required = false)
    protected JAXBElement<String> cardAccountNumberLast4;
    @XmlElement(name = "CardStored")
    protected Boolean cardStored;
    @XmlElement(name = "CardType")
    protected CreditCardTypes cardType;
    @XmlElementRef(name = "CustomerIdentifier", namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardManagementDefinitions", type = JAXBElement.class, required = false)
    protected JAXBElement<CustomerIdentifier> customerIdentifier;
    @XmlElement(name = "ExpirationMonth")
    protected Integer expirationMonth;
    @XmlElement(name = "ExpirationYear")
    protected Integer expirationYear;
    @XmlElementRef(name = "NonStoredCardFailureMessage", namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardManagementDefinitions", type = JAXBElement.class, required = false)
    protected JAXBElement<String> nonStoredCardFailureMessage;
    @XmlElement(name = "NonStoredCardFailureReason")
    protected HostedCardVaultFailureReason nonStoredCardFailureReason;
    @XmlElement(name = "Status")
    protected HostedCardVaultSubmissionStatus status;
    @XmlElementRef(name = "SubmissionId", namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardManagementDefinitions", type = JAXBElement.class, required = false)
    protected JAXBElement<String> submissionId;
    @XmlElement(name = "SubmissionType")
    protected HostedCardVaultSubmissionType submissionType;
    @XmlElementRef(name = "Token", namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardManagementDefinitions", type = JAXBElement.class, required = false)
    protected JAXBElement<String> token;

    /**
     * Gets the value of the cardAccountNumberLast4 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCardAccountNumberLast4() {
        return cardAccountNumberLast4;
    }

    /**
     * Sets the value of the cardAccountNumberLast4 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCardAccountNumberLast4(JAXBElement<String> value) {
        this.cardAccountNumberLast4 = ((JAXBElement<String>) value);
    }

    /**
     * Gets the value of the cardStored property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getCardStored() {
        return cardStored;
    }

    /**
     * Sets the value of the cardStored property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setCardStored(Boolean value) {
        this.cardStored = value;
    }

    /**
     * Gets the value of the cardType property.
     * 
     * @return
     *     possible object is
     *     {@link CreditCardTypes }
     *     
     */
    public CreditCardTypes getCardType() {
        return cardType;
    }

    /**
     * Sets the value of the cardType property.
     * 
     * @param value
     *     allowed object is
     *     {@link CreditCardTypes }
     *     
     */
    public void setCardType(CreditCardTypes value) {
        this.cardType = value;
    }

    /**
     * Gets the value of the customerIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link CustomerIdentifier }{@code >}
     *     
     */
    public JAXBElement<CustomerIdentifier> getCustomerIdentifier() {
        return customerIdentifier;
    }

    /**
     * Sets the value of the customerIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link CustomerIdentifier }{@code >}
     *     
     */
    public void setCustomerIdentifier(JAXBElement<CustomerIdentifier> value) {
        this.customerIdentifier = ((JAXBElement<CustomerIdentifier>) value);
    }

    /**
     * Gets the value of the expirationMonth property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getExpirationMonth() {
        return expirationMonth;
    }

    /**
     * Sets the value of the expirationMonth property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setExpirationMonth(Integer value) {
        this.expirationMonth = value;
    }

    /**
     * Gets the value of the expirationYear property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getExpirationYear() {
        return expirationYear;
    }

    /**
     * Sets the value of the expirationYear property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setExpirationYear(Integer value) {
        this.expirationYear = value;
    }

    /**
     * Gets the value of the nonStoredCardFailureMessage property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getNonStoredCardFailureMessage() {
        return nonStoredCardFailureMessage;
    }

    /**
     * Sets the value of the nonStoredCardFailureMessage property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setNonStoredCardFailureMessage(JAXBElement<String> value) {
        this.nonStoredCardFailureMessage = ((JAXBElement<String>) value);
    }

    /**
     * Gets the value of the nonStoredCardFailureReason property.
     * 
     * @return
     *     possible object is
     *     {@link HostedCardVaultFailureReason }
     *     
     */
    public HostedCardVaultFailureReason getNonStoredCardFailureReason() {
        return nonStoredCardFailureReason;
    }

    /**
     * Sets the value of the nonStoredCardFailureReason property.
     * 
     * @param value
     *     allowed object is
     *     {@link HostedCardVaultFailureReason }
     *     
     */
    public void setNonStoredCardFailureReason(HostedCardVaultFailureReason value) {
        this.nonStoredCardFailureReason = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link HostedCardVaultSubmissionStatus }
     *     
     */
    public HostedCardVaultSubmissionStatus getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link HostedCardVaultSubmissionStatus }
     *     
     */
    public void setStatus(HostedCardVaultSubmissionStatus value) {
        this.status = value;
    }

    /**
     * Gets the value of the submissionId property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSubmissionId() {
        return submissionId;
    }

    /**
     * Sets the value of the submissionId property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSubmissionId(JAXBElement<String> value) {
        this.submissionId = ((JAXBElement<String>) value);
    }

    /**
     * Gets the value of the submissionType property.
     * 
     * @return
     *     possible object is
     *     {@link HostedCardVaultSubmissionType }
     *     
     */
    public HostedCardVaultSubmissionType getSubmissionType() {
        return submissionType;
    }

    /**
     * Sets the value of the submissionType property.
     * 
     * @param value
     *     allowed object is
     *     {@link HostedCardVaultSubmissionType }
     *     
     */
    public void setSubmissionType(HostedCardVaultSubmissionType value) {
        this.submissionType = value;
    }

    /**
     * Gets the value of the token property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getToken() {
        return token;
    }

    /**
     * Sets the value of the token property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setToken(JAXBElement<String> value) {
        this.token = ((JAXBElement<String>) value);
    }

}
