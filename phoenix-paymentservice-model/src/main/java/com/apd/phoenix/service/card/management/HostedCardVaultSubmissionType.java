package com.apd.phoenix.service.card.management;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>Java class for HostedCardVaultSubmissionType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="HostedCardVaultSubmissionType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Unknown"/>
 *     &lt;enumeration value="AddCard"/>
 *     &lt;enumeration value="AddCardAndRunTransaction"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "HostedCardVaultSubmissionType", namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Common.Enums")
@XmlEnum
public enum HostedCardVaultSubmissionType {

    @XmlEnumValue("Unknown")
    UNKNOWN("Unknown"), @XmlEnumValue("AddCard")
    ADD_CARD("AddCard"), @XmlEnumValue("AddCardAndRunTransaction")
    ADD_CARD_AND_RUN_TRANSACTION("AddCardAndRunTransaction");

    private final String value;

    HostedCardVaultSubmissionType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static HostedCardVaultSubmissionType fromValue(String v) {
        for (HostedCardVaultSubmissionType c : HostedCardVaultSubmissionType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
