package com.apd.phoenix.service.card.management;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>Java class for CreditCard complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CreditCard">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="BillingAddress" type="{http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions}Address" minOccurs="0"/>
 *         &lt;element name="CardAccountNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CardType" type="{http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Enums}CreditCardTypes" minOccurs="0"/>
 *         &lt;element name="Cardholder" type="{http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions}Contact" minOccurs="0"/>
 *         &lt;element name="ExpirationMonth" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="ExpirationYear" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="NameOnCard" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ReceiptEmailAddresses" type="{http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions}ArrayOfEmail" minOccurs="0"/>
 *         &lt;element name="ShippingAddress" type="{http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions}Address" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CreditCard", namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", propOrder = {
        "billingAddress", "cardAccountNumber", "cardType", "cardholder", "expirationMonth", "expirationYear",
        "nameOnCard", "receiptEmailAddresses", "shippingAddress" })
@XmlSeeAlso( { StoredCreditCard.class })
public class CreditCard {

    @XmlElementRef(name = "BillingAddress", namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", type = JAXBElement.class, required = false)
    protected JAXBElement<Address> billingAddress;
    @XmlElementRef(name = "CardAccountNumber", namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", type = JAXBElement.class, required = false)
    protected JAXBElement<String> cardAccountNumber;
    @XmlElement(name = "CardType")
    protected CreditCardTypes cardType;
    @XmlElementRef(name = "Cardholder", namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", type = JAXBElement.class, required = false)
    protected JAXBElement<Contact> cardholder;
    @XmlElement(name = "ExpirationMonth")
    protected Integer expirationMonth;
    @XmlElement(name = "ExpirationYear")
    protected Integer expirationYear;
    @XmlElementRef(name = "NameOnCard", namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", type = JAXBElement.class, required = false)
    protected JAXBElement<String> nameOnCard;
    @XmlElementRef(name = "ReceiptEmailAddresses", namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfEmail> receiptEmailAddresses;
    @XmlElementRef(name = "ShippingAddress", namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", type = JAXBElement.class, required = false)
    protected JAXBElement<Address> shippingAddress;

    /**
     * Gets the value of the billingAddress property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Address }{@code >}
     *     
     */
    public JAXBElement<Address> getBillingAddress() {
        return billingAddress;
    }

    /**
     * Sets the value of the billingAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Address }{@code >}
     *     
     */
    public void setBillingAddress(JAXBElement<Address> value) {
        this.billingAddress = ((JAXBElement<Address>) value);
    }

    /**
     * Gets the value of the cardAccountNumber property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCardAccountNumber() {
        return cardAccountNumber;
    }

    /**
     * Sets the value of the cardAccountNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCardAccountNumber(JAXBElement<String> value) {
        this.cardAccountNumber = ((JAXBElement<String>) value);
    }

    /**
     * Gets the value of the cardType property.
     * 
     * @return
     *     possible object is
     *     {@link CreditCardTypes }
     *     
     */
    public CreditCardTypes getCardType() {
        return cardType;
    }

    /**
     * Sets the value of the cardType property.
     * 
     * @param value
     *     allowed object is
     *     {@link CreditCardTypes }
     *     
     */
    public void setCardType(CreditCardTypes value) {
        this.cardType = value;
    }

    /**
     * Gets the value of the cardholder property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Contact }{@code >}
     *     
     */
    public JAXBElement<Contact> getCardholder() {
        return cardholder;
    }

    /**
     * Sets the value of the cardholder property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Contact }{@code >}
     *     
     */
    public void setCardholder(JAXBElement<Contact> value) {
        this.cardholder = ((JAXBElement<Contact>) value);
    }

    /**
     * Gets the value of the expirationMonth property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getExpirationMonth() {
        return expirationMonth;
    }

    /**
     * Sets the value of the expirationMonth property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setExpirationMonth(Integer value) {
        this.expirationMonth = value;
    }

    /**
     * Gets the value of the expirationYear property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getExpirationYear() {
        return expirationYear;
    }

    /**
     * Sets the value of the expirationYear property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setExpirationYear(Integer value) {
        this.expirationYear = value;
    }

    /**
     * Gets the value of the nameOnCard property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getNameOnCard() {
        return nameOnCard;
    }

    /**
     * Sets the value of the nameOnCard property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setNameOnCard(JAXBElement<String> value) {
        this.nameOnCard = ((JAXBElement<String>) value);
    }

    /**
     * Gets the value of the receiptEmailAddresses property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfEmail }{@code >}
     *     
     */
    public JAXBElement<ArrayOfEmail> getReceiptEmailAddresses() {
        return receiptEmailAddresses;
    }

    /**
     * Sets the value of the receiptEmailAddresses property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfEmail }{@code >}
     *     
     */
    public void setReceiptEmailAddresses(JAXBElement<ArrayOfEmail> value) {
        this.receiptEmailAddresses = ((JAXBElement<ArrayOfEmail>) value);
    }

    /**
     * Gets the value of the shippingAddress property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Address }{@code >}
     *     
     */
    public JAXBElement<Address> getShippingAddress() {
        return shippingAddress;
    }

    /**
     * Sets the value of the shippingAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Address }{@code >}
     *     
     */
    public void setShippingAddress(JAXBElement<Address> value) {
        this.shippingAddress = ((JAXBElement<Address>) value);
    }

}
