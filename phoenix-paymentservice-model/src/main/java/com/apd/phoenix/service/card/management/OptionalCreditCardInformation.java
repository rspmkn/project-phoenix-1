package com.apd.phoenix.service.card.management;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>Java class for OptionalCreditCardInformation complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="OptionalCreditCardInformation">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="BillingAddress" type="{http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions}Address" minOccurs="0"/>
 *         &lt;element name="Cardholder" type="{http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions}Contact" minOccurs="0"/>
 *         &lt;element name="DisplayNotesDuringTransaction" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="FriendlyName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NameOnCard" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Notes" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ShippingAddress" type="{http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions}Address" minOccurs="0"/>
 *         &lt;element name="Token" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OptionalCreditCardInformation", namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", propOrder = {
        "billingAddress", "cardholder", "displayNotesDuringTransaction", "friendlyName", "nameOnCard", "notes",
        "shippingAddress", "token" })
public class OptionalCreditCardInformation {

    @XmlElementRef(name = "BillingAddress", namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", type = JAXBElement.class, required = false)
    protected JAXBElement<Address> billingAddress;
    @XmlElementRef(name = "Cardholder", namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", type = JAXBElement.class, required = false)
    protected JAXBElement<Contact> cardholder;
    @XmlElement(name = "DisplayNotesDuringTransaction")
    protected Boolean displayNotesDuringTransaction;
    @XmlElementRef(name = "FriendlyName", namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", type = JAXBElement.class, required = false)
    protected JAXBElement<String> friendlyName;
    @XmlElementRef(name = "NameOnCard", namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", type = JAXBElement.class, required = false)
    protected JAXBElement<String> nameOnCard;
    @XmlElementRef(name = "Notes", namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", type = JAXBElement.class, required = false)
    protected JAXBElement<String> notes;
    @XmlElementRef(name = "ShippingAddress", namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", type = JAXBElement.class, required = false)
    protected JAXBElement<Address> shippingAddress;
    @XmlElementRef(name = "Token", namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", type = JAXBElement.class, required = false)
    protected JAXBElement<String> token;

    /**
     * Gets the value of the billingAddress property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Address }{@code >}
     *     
     */
    public JAXBElement<Address> getBillingAddress() {
        return billingAddress;
    }

    /**
     * Sets the value of the billingAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Address }{@code >}
     *     
     */
    public void setBillingAddress(JAXBElement<Address> value) {
        this.billingAddress = ((JAXBElement<Address>) value);
    }

    /**
     * Gets the value of the cardholder property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Contact }{@code >}
     *     
     */
    public JAXBElement<Contact> getCardholder() {
        return cardholder;
    }

    /**
     * Sets the value of the cardholder property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Contact }{@code >}
     *     
     */
    public void setCardholder(JAXBElement<Contact> value) {
        this.cardholder = ((JAXBElement<Contact>) value);
    }

    /**
     * Gets the value of the displayNotesDuringTransaction property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getDisplayNotesDuringTransaction() {
        return displayNotesDuringTransaction;
    }

    /**
     * Sets the value of the displayNotesDuringTransaction property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDisplayNotesDuringTransaction(Boolean value) {
        this.displayNotesDuringTransaction = value;
    }

    /**
     * Gets the value of the friendlyName property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getFriendlyName() {
        return friendlyName;
    }

    /**
     * Sets the value of the friendlyName property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setFriendlyName(JAXBElement<String> value) {
        this.friendlyName = ((JAXBElement<String>) value);
    }

    /**
     * Gets the value of the nameOnCard property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getNameOnCard() {
        return nameOnCard;
    }

    /**
     * Sets the value of the nameOnCard property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setNameOnCard(JAXBElement<String> value) {
        this.nameOnCard = ((JAXBElement<String>) value);
    }

    /**
     * Gets the value of the notes property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getNotes() {
        return notes;
    }

    /**
     * Sets the value of the notes property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setNotes(JAXBElement<String> value) {
        this.notes = ((JAXBElement<String>) value);
    }

    /**
     * Gets the value of the shippingAddress property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Address }{@code >}
     *     
     */
    public JAXBElement<Address> getShippingAddress() {
        return shippingAddress;
    }

    /**
     * Sets the value of the shippingAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Address }{@code >}
     *     
     */
    public void setShippingAddress(JAXBElement<Address> value) {
        this.shippingAddress = ((JAXBElement<Address>) value);
    }

    /**
     * Gets the value of the token property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getToken() {
        return token;
    }

    /**
     * Sets the value of the token property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setToken(JAXBElement<String> value) {
        this.token = ((JAXBElement<String>) value);
    }

}
