package com.apd.phoenix.service.payment.ws;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for CreditCardTransactionVerificationResult complex type.
 *
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 *
 * <pre>
 * &lt;complexType name="CreditCardTransactionVerificationResult">
 *   &lt;complexContent>
 *     &lt;extension base="{http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardTransactionDefinitions}CreditCardTransactionResult">
 *       &lt;sequence>
 *         &lt;element name="AddressAvsResponse" type="{http://schemas.datacontract.org/2004/07/ThreeDelta.Common.Enums}AvsResponse" minOccurs="0"/>
 *         &lt;element name="CardSecurityCodeResponse" type="{http://schemas.datacontract.org/2004/07/ThreeDelta.Common.Enums}CardSecurityCodeResponse" minOccurs="0"/>
 *         &lt;element name="PostalCodeAvsResponse" type="{http://schemas.datacontract.org/2004/07/ThreeDelta.Common.Enums}AvsResponse" minOccurs="0"/>
 *         &lt;element name="ProcessorAvsResponse" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ProcessorCardSecurityCodeResponse" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CreditCardTransactionVerificationResult", namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardTransactionDefinitions", propOrder = {
        "addressAvsResponse", "cardSecurityCodeResponse", "postalCodeAvsResponse", "processorAvsResponse",
        "processorCardSecurityCodeResponse" })
@XmlSeeAlso( { CreditCardTransactionAuthorizationResult.class })
public class CreditCardTransactionVerificationResult extends CreditCardTransactionResult {

    @XmlElement(name = "AddressAvsResponse")
    protected AvsResponse addressAvsResponse;
    @XmlElement(name = "CardSecurityCodeResponse")
    protected CardSecurityCodeResponse cardSecurityCodeResponse;
    @XmlElement(name = "PostalCodeAvsResponse")
    protected AvsResponse postalCodeAvsResponse;
    @XmlElementRef(name = "ProcessorAvsResponse", namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardTransactionDefinitions", type = JAXBElement.class, required = false)
    protected JAXBElement<String> processorAvsResponse;
    @XmlElementRef(name = "ProcessorCardSecurityCodeResponse", namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardTransactionDefinitions", type = JAXBElement.class, required = false)
    protected JAXBElement<String> processorCardSecurityCodeResponse;

    /**
     * Gets the value of the addressAvsResponse property.
     *
     * @return possible object is {@link AvsResponse }
     *
     */
    public AvsResponse getAddressAvsResponse() {
        return addressAvsResponse;
    }

    /**
     * Sets the value of the addressAvsResponse property.
     *
     * @param value allowed object is {@link AvsResponse }
     *
     */
    public void setAddressAvsResponse(AvsResponse value) {
        this.addressAvsResponse = value;
    }

    /**
     * Gets the value of the cardSecurityCodeResponse property.
     *
     * @return possible object is {@link CardSecurityCodeResponse }
     *
     */
    public CardSecurityCodeResponse getCardSecurityCodeResponse() {
        return cardSecurityCodeResponse;
    }

    /**
     * Sets the value of the cardSecurityCodeResponse property.
     *
     * @param value allowed object is {@link CardSecurityCodeResponse }
     *
     */
    public void setCardSecurityCodeResponse(CardSecurityCodeResponse value) {
        this.cardSecurityCodeResponse = value;
    }

    /**
     * Gets the value of the postalCodeAvsResponse property.
     *
     * @return possible object is {@link AvsResponse }
     *
     */
    public AvsResponse getPostalCodeAvsResponse() {
        return postalCodeAvsResponse;
    }

    /**
     * Sets the value of the postalCodeAvsResponse property.
     *
     * @param value allowed object is {@link AvsResponse }
     *
     */
    public void setPostalCodeAvsResponse(AvsResponse value) {
        this.postalCodeAvsResponse = value;
    }

    /**
     * Gets the value of the processorAvsResponse property.
     *
     * @return possible object is
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     *
     */
    public JAXBElement<String> getProcessorAvsResponse() {
        return processorAvsResponse;
    }

    /**
     * Sets the value of the processorAvsResponse property.
     *
     * @param value allowed object is
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     *
     */
    public void setProcessorAvsResponse(JAXBElement<String> value) {
        this.processorAvsResponse = value;
    }

    /**
     * Gets the value of the processorCardSecurityCodeResponse property.
     *
     * @return possible object is
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     *
     */
    public JAXBElement<String> getProcessorCardSecurityCodeResponse() {
        return processorCardSecurityCodeResponse;
    }

    /**
     * Sets the value of the processorCardSecurityCodeResponse property.
     *
     * @param value allowed object is
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     *
     */
    public void setProcessorCardSecurityCodeResponse(JAXBElement<String> value) {
        this.processorCardSecurityCodeResponse = value;
    }
}
