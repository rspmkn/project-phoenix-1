package com.apd.phoenix.service.card.management;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>Java class for HostedCardVaultSubmissionIdResult complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="HostedCardVaultSubmissionIdResult">
 *   &lt;complexContent>
 *     &lt;extension base="{http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardManagementDefinitions}CreditCardManagementResult">
 *       &lt;sequence>
 *         &lt;element name="SubmissionId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "HostedCardVaultSubmissionIdResult", namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardManagementDefinitions", propOrder = { "submissionId" })
public class HostedCardVaultSubmissionIdResult extends CreditCardManagementResult {

    @XmlElementRef(name = "SubmissionId", namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardManagementDefinitions", type = JAXBElement.class, required = false)
    protected JAXBElement<String> submissionId;

    /**
     * Gets the value of the submissionId property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSubmissionId() {
        return submissionId;
    }

    /**
     * Sets the value of the submissionId property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSubmissionId(JAXBElement<String> value) {
        this.submissionId = ((JAXBElement<String>) value);
    }

}
