package com.apd.phoenix.service.card.management;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>Java class for CreditCardManagementFailureReason.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="CreditCardManagementFailureReason">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="None"/>
 *     &lt;enumeration value="ValidationFailed"/>
 *     &lt;enumeration value="ProcessingFailed"/>
 *     &lt;enumeration value="PermissionDenied"/>
 *     &lt;enumeration value="CreditCardDoesNotExist"/>
 *     &lt;enumeration value="CustomerIdentifierInvalid"/>
 *     &lt;enumeration value="CardNumberInUse"/>
 *     &lt;enumeration value="TokenInUse"/>
 *     &lt;enumeration value="CardAccountNumberDoesNotMatch"/>
 *     &lt;enumeration value="LocationIdentifierInvalid"/>
 *     &lt;enumeration value="CustomerCodeIsInUse"/>
 *     &lt;enumeration value="CustomerDoesNotExist"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "CreditCardManagementFailureReason", namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Enums")
@XmlEnum
public enum CreditCardManagementFailureReason {

    @XmlEnumValue("None")
    NONE("None"), @XmlEnumValue("ValidationFailed")
    VALIDATION_FAILED("ValidationFailed"), @XmlEnumValue("ProcessingFailed")
    PROCESSING_FAILED("ProcessingFailed"), @XmlEnumValue("PermissionDenied")
    PERMISSION_DENIED("PermissionDenied"), @XmlEnumValue("CreditCardDoesNotExist")
    CREDIT_CARD_DOES_NOT_EXIST("CreditCardDoesNotExist"), @XmlEnumValue("CustomerIdentifierInvalid")
    CUSTOMER_IDENTIFIER_INVALID("CustomerIdentifierInvalid"), @XmlEnumValue("CardNumberInUse")
    CARD_NUMBER_IN_USE("CardNumberInUse"), @XmlEnumValue("TokenInUse")
    TOKEN_IN_USE("TokenInUse"), @XmlEnumValue("CardAccountNumberDoesNotMatch")
    CARD_ACCOUNT_NUMBER_DOES_NOT_MATCH("CardAccountNumberDoesNotMatch"), @XmlEnumValue("LocationIdentifierInvalid")
    LOCATION_IDENTIFIER_INVALID("LocationIdentifierInvalid"), @XmlEnumValue("CustomerCodeIsInUse")
    CUSTOMER_CODE_IS_IN_USE("CustomerCodeIsInUse"), @XmlEnumValue("CustomerDoesNotExist")
    CUSTOMER_DOES_NOT_EXIST("CustomerDoesNotExist");

    private final String value;

    CreditCardManagementFailureReason(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static CreditCardManagementFailureReason fromValue(String v) {
        for (CreditCardManagementFailureReason c : CreditCardManagementFailureReason.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
