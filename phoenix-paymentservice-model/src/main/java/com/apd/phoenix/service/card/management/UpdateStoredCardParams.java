package com.apd.phoenix.service.card.management;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>Java class for UpdateStoredCardParams complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="UpdateStoredCardParams">
 *   &lt;complexContent>
 *     &lt;extension base="{http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Parameters.CreditCardManagement}CreditCardManagementParams">
 *       &lt;sequence>
 *         &lt;element name="CreditCard" type="{http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions}StoredCreditCard" minOccurs="0"/>
 *         &lt;element name="CustomerIdentifier" type="{http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions}CustomerIdentifier" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UpdateStoredCardParams", namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Parameters.CreditCardManagement", propOrder = {
        "creditCard", "customerIdentifier" })
public class UpdateStoredCardParams extends CreditCardManagementParams {

    @XmlElementRef(name = "CreditCard", namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Parameters.CreditCardManagement", type = JAXBElement.class, required = false)
    protected JAXBElement<StoredCreditCard> creditCard;
    @XmlElementRef(name = "CustomerIdentifier", namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Parameters.CreditCardManagement", type = JAXBElement.class, required = false)
    protected JAXBElement<CustomerIdentifier> customerIdentifier;

    /**
     * Gets the value of the creditCard property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link StoredCreditCard }{@code >}
     *     
     */
    public JAXBElement<StoredCreditCard> getCreditCard() {
        return creditCard;
    }

    /**
     * Sets the value of the creditCard property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link StoredCreditCard }{@code >}
     *     
     */
    public void setCreditCard(JAXBElement<StoredCreditCard> value) {
        this.creditCard = ((JAXBElement<StoredCreditCard>) value);
    }

    /**
     * Gets the value of the customerIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link CustomerIdentifier }{@code >}
     *     
     */
    public JAXBElement<CustomerIdentifier> getCustomerIdentifier() {
        return customerIdentifier;
    }

    /**
     * Sets the value of the customerIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link CustomerIdentifier }{@code >}
     *     
     */
    public void setCustomerIdentifier(JAXBElement<CustomerIdentifier> value) {
        this.customerIdentifier = ((JAXBElement<CustomerIdentifier>) value);
    }

}
