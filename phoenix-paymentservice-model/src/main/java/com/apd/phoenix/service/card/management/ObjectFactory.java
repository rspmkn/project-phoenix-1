package com.apd.phoenix.service.card.management;

import java.math.BigDecimal;
import java.math.BigInteger;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.datatype.Duration;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.namespace.QName;

/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.apd.phoenix.service.card.management package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ListCustomerLocationsClientCredentials_QNAME = new QName(
            "http://3DSI.org/WebServices/CreditCardManagement", "clientCredentials");
    private final static QName _ListCustomerLocationsListLocationsParams_QNAME = new QName(
            "http://3DSI.org/WebServices/CreditCardManagement", "listLocationsParams");
    private final static QName _GetHostedCardVaultSubmissionInfoGetHostedCardVaultSubmissionInfoParams_QNAME = new QName(
            "http://3DSI.org/WebServices/CreditCardManagement", "getHostedCardVaultSubmissionInfoParams");
    private final static QName _GetTokenForCardNumberGetTokenForCardNumberParams_QNAME = new QName(
            "http://3DSI.org/WebServices/CreditCardManagement", "getTokenForCardNumberParams");
    private final static QName _CreateHostedCardVaultSubmissionIdResponseCreateHostedCardVaultSubmissionIdResult_QNAME = new QName(
            "http://3DSI.org/WebServices/CreditCardManagement", "CreateHostedCardVaultSubmissionIdResult");
    private final static QName _CreateHostedCardVaultSubmissionIdCreateHostedCardVaultSubmissionIdParams_QNAME = new QName(
            "http://3DSI.org/WebServices/CreditCardManagement", "createHostedCardVaultSubmissionIdParams");
    private final static QName _MoveCreditCardMoveCreditCardParams_QNAME = new QName(
            "http://3DSI.org/WebServices/CreditCardManagement", "moveCreditCardParams");
    private final static QName _ShareCustomerShareCustomerParams_QNAME = new QName(
            "http://3DSI.org/WebServices/CreditCardManagement", "shareCustomerParams");
    private final static QName _GetTokenForCardNumberResponseGetTokenForCardNumberResult_QNAME = new QName(
            "http://3DSI.org/WebServices/CreditCardManagement", "GetTokenForCardNumberResult");
    private final static QName _UpdateStoredCreditCardExpirationDateUpdateStoredCardExpirationDateParams_QNAME = new QName(
            "http://3DSI.org/WebServices/CreditCardManagement", "updateStoredCardExpirationDateParams");
    private final static QName _GetCustomerResponseGetCustomerResult_QNAME = new QName(
            "http://3DSI.org/WebServices/CreditCardManagement", "GetCustomerResult");
    private final static QName _UpdateCustomerUpdateCustomerParams_QNAME = new QName(
            "http://3DSI.org/WebServices/CreditCardManagement", "updateCustomerParams");
    private final static QName _RenameTokenResponseRenameTokenResult_QNAME = new QName(
            "http://3DSI.org/WebServices/CreditCardManagement", "RenameTokenResult");
    private final static QName _UpdateCustomerResponseUpdateCustomerResult_QNAME = new QName(
            "http://3DSI.org/WebServices/CreditCardManagement", "UpdateCustomerResult");
    private final static QName _AddStoredCreditCardResponseAddStoredCreditCardResult_QNAME = new QName(
            "http://3DSI.org/WebServices/CreditCardManagement", "AddStoredCreditCardResult");
    private final static QName _DeleteCustomerResponseDeleteCustomerResult_QNAME = new QName(
            "http://3DSI.org/WebServices/CreditCardManagement", "DeleteCustomerResult");
    private final static QName _AddStoredCreditCardAddStoredCardParams_QNAME = new QName(
            "http://3DSI.org/WebServices/CreditCardManagement", "addStoredCardParams");
    private final static QName _UpdateStoredCreditCardExpirationDateResponseUpdateStoredCreditCardExpirationDateResult_QNAME = new QName(
            "http://3DSI.org/WebServices/CreditCardManagement", "UpdateStoredCreditCardExpirationDateResult");
    private final static QName _MoveCreditCardResponseMoveCreditCardResult_QNAME = new QName(
            "http://3DSI.org/WebServices/CreditCardManagement", "MoveCreditCardResult");
    private final static QName _RenameCustomerCodeRenameCustomerCodeParams_QNAME = new QName(
            "http://3DSI.org/WebServices/CreditCardManagement", "renameCustomerCodeParams");
    private final static QName _ShareCustomerResponseShareCustomerResult_QNAME = new QName(
            "http://3DSI.org/WebServices/CreditCardManagement", "ShareCustomerResult");
    private final static QName _DeleteStoredCreditCardResponseDeleteStoredCreditCardResult_QNAME = new QName(
            "http://3DSI.org/WebServices/CreditCardManagement", "DeleteStoredCreditCardResult");
    private final static QName _RenameCustomerCodeResponseRenameCustomerCodeResult_QNAME = new QName(
            "http://3DSI.org/WebServices/CreditCardManagement", "RenameCustomerCodeResult");
    private final static QName _RenameTokenRenameTokenParams_QNAME = new QName(
            "http://3DSI.org/WebServices/CreditCardManagement", "renameTokenParams");
    private final static QName _ListCustomerLocationsResponseListCustomerLocationsResult_QNAME = new QName(
            "http://3DSI.org/WebServices/CreditCardManagement", "ListCustomerLocationsResult");
    private final static QName _UpdateStoredCreditCardUpdateStoredCardParams_QNAME = new QName(
            "http://3DSI.org/WebServices/CreditCardManagement", "updateStoredCardParams");
    private final static QName _GetStoredCreditCardResponseGetStoredCreditCardResult_QNAME = new QName(
            "http://3DSI.org/WebServices/CreditCardManagement", "GetStoredCreditCardResult");
    private final static QName _CreateCustomerCreateCustomerParams_QNAME = new QName(
            "http://3DSI.org/WebServices/CreditCardManagement", "createCustomerParams");
    private final static QName _DeleteCustomerDeleteCustomerParams_QNAME = new QName(
            "http://3DSI.org/WebServices/CreditCardManagement", "deleteCustomerParams");
    private final static QName _GetCustomerGetCustomerParams_QNAME = new QName(
            "http://3DSI.org/WebServices/CreditCardManagement", "getCustomerParams");
    private final static QName _CreateCustomerResponseCreateCustomerResult_QNAME = new QName(
            "http://3DSI.org/WebServices/CreditCardManagement", "CreateCustomerResult");
    private final static QName _GetStoredCreditCardGetStoredCreditCardParams_QNAME = new QName(
            "http://3DSI.org/WebServices/CreditCardManagement", "getStoredCreditCardParams");
    private final static QName _UpdateStoredCreditCardResponseUpdateStoredCreditCardResult_QNAME = new QName(
            "http://3DSI.org/WebServices/CreditCardManagement", "UpdateStoredCreditCardResult");
    private final static QName _GetHostedCardVaultSubmissionInfoResponseGetHostedCardVaultSubmissionInfoResult_QNAME = new QName(
            "http://3DSI.org/WebServices/CreditCardManagement", "GetHostedCardVaultSubmissionInfoResult");
    private final static QName _DeleteStoredCreditCardDeleteStoredCardParams_QNAME = new QName(
            "http://3DSI.org/WebServices/CreditCardManagement", "deleteStoredCardParams");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.apd.phoenix.service.card.management
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ListCustomerLocations }
     * 
     */
    public ListCustomerLocations createListCustomerLocations() {
        return new ListCustomerLocations();
    }

    /**
     * Create an instance of {@link GetHostedCardVaultSubmissionInfo }
     * 
     */
    public GetHostedCardVaultSubmissionInfo createGetHostedCardVaultSubmissionInfo() {
        return new GetHostedCardVaultSubmissionInfo();
    }

    /**
     * Create an instance of {@link GetTokenForCardNumber }
     * 
     */
    public GetTokenForCardNumber createGetTokenForCardNumber() {
        return new GetTokenForCardNumber();
    }

    /**
     * Create an instance of {@link CreateHostedCardVaultSubmissionIdResponse }
     * 
     */
    public CreateHostedCardVaultSubmissionIdResponse createCreateHostedCardVaultSubmissionIdResponse() {
        return new CreateHostedCardVaultSubmissionIdResponse();
    }

    /**
     * Create an instance of {@link CreateHostedCardVaultSubmissionId }
     * 
     */
    public CreateHostedCardVaultSubmissionId createCreateHostedCardVaultSubmissionId() {
        return new CreateHostedCardVaultSubmissionId();
    }

    /**
     * Create an instance of {@link MoveCreditCard }
     * 
     */
    public MoveCreditCard createMoveCreditCard() {
        return new MoveCreditCard();
    }

    /**
     * Create an instance of {@link ShareCustomer }
     * 
     */
    public ShareCustomer createShareCustomer() {
        return new ShareCustomer();
    }

    /**
     * Create an instance of {@link GetTokenForCardNumberResponse }
     * 
     */
    public GetTokenForCardNumberResponse createGetTokenForCardNumberResponse() {
        return new GetTokenForCardNumberResponse();
    }

    /**
     * Create an instance of {@link UpdateStoredCreditCardExpirationDate }
     * 
     */
    public UpdateStoredCreditCardExpirationDate createUpdateStoredCreditCardExpirationDate() {
        return new UpdateStoredCreditCardExpirationDate();
    }

    /**
     * Create an instance of {@link GetCustomerResponse }
     * 
     */
    public GetCustomerResponse createGetCustomerResponse() {
        return new GetCustomerResponse();
    }

    /**
     * Create an instance of {@link UpdateCustomer }
     * 
     */
    public UpdateCustomer createUpdateCustomer() {
        return new UpdateCustomer();
    }

    /**
     * Create an instance of {@link RenameTokenResponse }
     * 
     */
    public RenameTokenResponse createRenameTokenResponse() {
        return new RenameTokenResponse();
    }

    /**
     * Create an instance of {@link UpdateCustomerResponse }
     * 
     */
    public UpdateCustomerResponse createUpdateCustomerResponse() {
        return new UpdateCustomerResponse();
    }

    /**
     * Create an instance of {@link AddStoredCreditCardResponse }
     * 
     */
    public AddStoredCreditCardResponse createAddStoredCreditCardResponse() {
        return new AddStoredCreditCardResponse();
    }

    /**
     * Create an instance of {@link DeleteCustomerResponse }
     * 
     */
    public DeleteCustomerResponse createDeleteCustomerResponse() {
        return new DeleteCustomerResponse();
    }

    /**
     * Create an instance of {@link AddStoredCreditCard }
     * 
     */
    public AddStoredCreditCard createAddStoredCreditCard() {
        return new AddStoredCreditCard();
    }

    /**
     * Create an instance of {@link UpdateStoredCreditCardExpirationDateResponse }
     * 
     */
    public UpdateStoredCreditCardExpirationDateResponse createUpdateStoredCreditCardExpirationDateResponse() {
        return new UpdateStoredCreditCardExpirationDateResponse();
    }

    /**
     * Create an instance of {@link MoveCreditCardResponse }
     * 
     */
    public MoveCreditCardResponse createMoveCreditCardResponse() {
        return new MoveCreditCardResponse();
    }

    /**
     * Create an instance of {@link RenameCustomerCode }
     * 
     */
    public RenameCustomerCode createRenameCustomerCode() {
        return new RenameCustomerCode();
    }

    /**
     * Create an instance of {@link ShareCustomerResponse }
     * 
     */
    public ShareCustomerResponse createShareCustomerResponse() {
        return new ShareCustomerResponse();
    }

    /**
     * Create an instance of {@link DeleteStoredCreditCardResponse }
     * 
     */
    public DeleteStoredCreditCardResponse createDeleteStoredCreditCardResponse() {
        return new DeleteStoredCreditCardResponse();
    }

    /**
     * Create an instance of {@link RenameCustomerCodeResponse }
     * 
     */
    public RenameCustomerCodeResponse createRenameCustomerCodeResponse() {
        return new RenameCustomerCodeResponse();
    }

    /**
     * Create an instance of {@link RenameToken }
     * 
     */
    public RenameToken createRenameToken() {
        return new RenameToken();
    }

    /**
     * Create an instance of {@link ListCustomerLocationsResponse }
     * 
     */
    public ListCustomerLocationsResponse createListCustomerLocationsResponse() {
        return new ListCustomerLocationsResponse();
    }

    /**
     * Create an instance of {@link UpdateStoredCreditCard }
     * 
     */
    public UpdateStoredCreditCard createUpdateStoredCreditCard() {
        return new UpdateStoredCreditCard();
    }

    /**
     * Create an instance of {@link GetStoredCreditCardResponse }
     * 
     */
    public GetStoredCreditCardResponse createGetStoredCreditCardResponse() {
        return new GetStoredCreditCardResponse();
    }

    /**
     * Create an instance of {@link CreateCustomer }
     * 
     */
    public CreateCustomer createCreateCustomer() {
        return new CreateCustomer();
    }

    /**
     * Create an instance of {@link DeleteCustomer }
     * 
     */
    public DeleteCustomer createDeleteCustomer() {
        return new DeleteCustomer();
    }

    /**
     * Create an instance of {@link GetCustomer }
     * 
     */
    public GetCustomer createGetCustomer() {
        return new GetCustomer();
    }

    /**
     * Create an instance of {@link CreateCustomerResponse }
     * 
     */
    public CreateCustomerResponse createCreateCustomerResponse() {
        return new CreateCustomerResponse();
    }

    /**
     * Create an instance of {@link GetStoredCreditCard }
     * 
     */
    public GetStoredCreditCard createGetStoredCreditCard() {
        return new GetStoredCreditCard();
    }

    /**
     * Create an instance of {@link UpdateStoredCreditCardResponse }
     * 
     */
    public UpdateStoredCreditCardResponse createUpdateStoredCreditCardResponse() {
        return new UpdateStoredCreditCardResponse();
    }

    /**
     * Create an instance of {@link GetHostedCardVaultSubmissionInfoResponse }
     * 
     */
    public GetHostedCardVaultSubmissionInfoResponse createGetHostedCardVaultSubmissionInfoResponse() {
        return new GetHostedCardVaultSubmissionInfoResponse();
    }

    /**
     * Create an instance of {@link DeleteStoredCreditCard }
     * 
     */
    public DeleteStoredCreditCard createDeleteStoredCreditCard() {
        return new DeleteStoredCreditCard();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ClientCredentials }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://3DSI.org/WebServices/CreditCardManagement", name = "clientCredentials", scope = ListCustomerLocations.class)
    public JAXBElement<ClientCredentials> createListCustomerLocationsClientCredentials(ClientCredentials value) {
        return new JAXBElement<ClientCredentials>(_ListCustomerLocationsClientCredentials_QNAME,
                ClientCredentials.class, ListCustomerLocations.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListLocationsParams }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://3DSI.org/WebServices/CreditCardManagement", name = "listLocationsParams", scope = ListCustomerLocations.class)
    public JAXBElement<ListLocationsParams> createListCustomerLocationsListLocationsParams(ListLocationsParams value) {
        return new JAXBElement<ListLocationsParams>(_ListCustomerLocationsListLocationsParams_QNAME,
                ListLocationsParams.class, ListCustomerLocations.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetHostedCardVaultSubmissionInfoParams }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://3DSI.org/WebServices/CreditCardManagement", name = "getHostedCardVaultSubmissionInfoParams", scope = GetHostedCardVaultSubmissionInfo.class)
    public JAXBElement<GetHostedCardVaultSubmissionInfoParams> createGetHostedCardVaultSubmissionInfoGetHostedCardVaultSubmissionInfoParams(
            GetHostedCardVaultSubmissionInfoParams value) {
        return new JAXBElement<GetHostedCardVaultSubmissionInfoParams>(
                _GetHostedCardVaultSubmissionInfoGetHostedCardVaultSubmissionInfoParams_QNAME,
                GetHostedCardVaultSubmissionInfoParams.class, GetHostedCardVaultSubmissionInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ClientCredentials }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://3DSI.org/WebServices/CreditCardManagement", name = "clientCredentials", scope = GetHostedCardVaultSubmissionInfo.class)
    public JAXBElement<ClientCredentials> createGetHostedCardVaultSubmissionInfoClientCredentials(
            ClientCredentials value) {
        return new JAXBElement<ClientCredentials>(_ListCustomerLocationsClientCredentials_QNAME,
                ClientCredentials.class, GetHostedCardVaultSubmissionInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ClientCredentials }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://3DSI.org/WebServices/CreditCardManagement", name = "clientCredentials", scope = GetTokenForCardNumber.class)
    public JAXBElement<ClientCredentials> createGetTokenForCardNumberClientCredentials(ClientCredentials value) {
        return new JAXBElement<ClientCredentials>(_ListCustomerLocationsClientCredentials_QNAME,
                ClientCredentials.class, GetTokenForCardNumber.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetTokenForCardNumberParams }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://3DSI.org/WebServices/CreditCardManagement", name = "getTokenForCardNumberParams", scope = GetTokenForCardNumber.class)
    public JAXBElement<GetTokenForCardNumberParams> createGetTokenForCardNumberGetTokenForCardNumberParams(
            GetTokenForCardNumberParams value) {
        return new JAXBElement<GetTokenForCardNumberParams>(_GetTokenForCardNumberGetTokenForCardNumberParams_QNAME,
                GetTokenForCardNumberParams.class, GetTokenForCardNumber.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link HostedCardVaultSubmissionIdResult }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://3DSI.org/WebServices/CreditCardManagement", name = "CreateHostedCardVaultSubmissionIdResult", scope = CreateHostedCardVaultSubmissionIdResponse.class)
    public JAXBElement<HostedCardVaultSubmissionIdResult> createCreateHostedCardVaultSubmissionIdResponseCreateHostedCardVaultSubmissionIdResult(
            HostedCardVaultSubmissionIdResult value) {
        return new JAXBElement<HostedCardVaultSubmissionIdResult>(
                _CreateHostedCardVaultSubmissionIdResponseCreateHostedCardVaultSubmissionIdResult_QNAME,
                HostedCardVaultSubmissionIdResult.class, CreateHostedCardVaultSubmissionIdResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateHostedCardVaultSubmissionIdParams }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://3DSI.org/WebServices/CreditCardManagement", name = "createHostedCardVaultSubmissionIdParams", scope = CreateHostedCardVaultSubmissionId.class)
    public JAXBElement<CreateHostedCardVaultSubmissionIdParams> createCreateHostedCardVaultSubmissionIdCreateHostedCardVaultSubmissionIdParams(
            CreateHostedCardVaultSubmissionIdParams value) {
        return new JAXBElement<CreateHostedCardVaultSubmissionIdParams>(
                _CreateHostedCardVaultSubmissionIdCreateHostedCardVaultSubmissionIdParams_QNAME,
                CreateHostedCardVaultSubmissionIdParams.class, CreateHostedCardVaultSubmissionId.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ClientCredentials }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://3DSI.org/WebServices/CreditCardManagement", name = "clientCredentials", scope = CreateHostedCardVaultSubmissionId.class)
    public JAXBElement<ClientCredentials> createCreateHostedCardVaultSubmissionIdClientCredentials(
            ClientCredentials value) {
        return new JAXBElement<ClientCredentials>(_ListCustomerLocationsClientCredentials_QNAME,
                ClientCredentials.class, CreateHostedCardVaultSubmissionId.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ClientCredentials }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://3DSI.org/WebServices/CreditCardManagement", name = "clientCredentials", scope = MoveCreditCard.class)
    public JAXBElement<ClientCredentials> createMoveCreditCardClientCredentials(ClientCredentials value) {
        return new JAXBElement<ClientCredentials>(_ListCustomerLocationsClientCredentials_QNAME,
                ClientCredentials.class, MoveCreditCard.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MoveCreditCardParams }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://3DSI.org/WebServices/CreditCardManagement", name = "moveCreditCardParams", scope = MoveCreditCard.class)
    public JAXBElement<MoveCreditCardParams> createMoveCreditCardMoveCreditCardParams(MoveCreditCardParams value) {
        return new JAXBElement<MoveCreditCardParams>(_MoveCreditCardMoveCreditCardParams_QNAME,
                MoveCreditCardParams.class, MoveCreditCard.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ClientCredentials }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://3DSI.org/WebServices/CreditCardManagement", name = "clientCredentials", scope = ShareCustomer.class)
    public JAXBElement<ClientCredentials> createShareCustomerClientCredentials(ClientCredentials value) {
        return new JAXBElement<ClientCredentials>(_ListCustomerLocationsClientCredentials_QNAME,
                ClientCredentials.class, ShareCustomer.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ShareCustomerParams }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://3DSI.org/WebServices/CreditCardManagement", name = "shareCustomerParams", scope = ShareCustomer.class)
    public JAXBElement<ShareCustomerParams> createShareCustomerShareCustomerParams(ShareCustomerParams value) {
        return new JAXBElement<ShareCustomerParams>(_ShareCustomerShareCustomerParams_QNAME, ShareCustomerParams.class,
                ShareCustomer.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TokenCreditCardManagementResult }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://3DSI.org/WebServices/CreditCardManagement", name = "GetTokenForCardNumberResult", scope = GetTokenForCardNumberResponse.class)
    public JAXBElement<TokenCreditCardManagementResult> createGetTokenForCardNumberResponseGetTokenForCardNumberResult(
            TokenCreditCardManagementResult value) {
        return new JAXBElement<TokenCreditCardManagementResult>(
                _GetTokenForCardNumberResponseGetTokenForCardNumberResult_QNAME, TokenCreditCardManagementResult.class,
                GetTokenForCardNumberResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateStoredCardExpirationDateParams }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://3DSI.org/WebServices/CreditCardManagement", name = "updateStoredCardExpirationDateParams", scope = UpdateStoredCreditCardExpirationDate.class)
    public JAXBElement<UpdateStoredCardExpirationDateParams> createUpdateStoredCreditCardExpirationDateUpdateStoredCardExpirationDateParams(
            UpdateStoredCardExpirationDateParams value) {
        return new JAXBElement<UpdateStoredCardExpirationDateParams>(
                _UpdateStoredCreditCardExpirationDateUpdateStoredCardExpirationDateParams_QNAME,
                UpdateStoredCardExpirationDateParams.class, UpdateStoredCreditCardExpirationDate.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ClientCredentials }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://3DSI.org/WebServices/CreditCardManagement", name = "clientCredentials", scope = UpdateStoredCreditCardExpirationDate.class)
    public JAXBElement<ClientCredentials> createUpdateStoredCreditCardExpirationDateClientCredentials(
            ClientCredentials value) {
        return new JAXBElement<ClientCredentials>(_ListCustomerLocationsClientCredentials_QNAME,
                ClientCredentials.class, UpdateStoredCreditCardExpirationDate.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CustomerCreditCardManagementResult }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://3DSI.org/WebServices/CreditCardManagement", name = "GetCustomerResult", scope = GetCustomerResponse.class)
    public JAXBElement<CustomerCreditCardManagementResult> createGetCustomerResponseGetCustomerResult(
            CustomerCreditCardManagementResult value) {
        return new JAXBElement<CustomerCreditCardManagementResult>(_GetCustomerResponseGetCustomerResult_QNAME,
                CustomerCreditCardManagementResult.class, GetCustomerResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ClientCredentials }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://3DSI.org/WebServices/CreditCardManagement", name = "clientCredentials", scope = UpdateCustomer.class)
    public JAXBElement<ClientCredentials> createUpdateCustomerClientCredentials(ClientCredentials value) {
        return new JAXBElement<ClientCredentials>(_ListCustomerLocationsClientCredentials_QNAME,
                ClientCredentials.class, UpdateCustomer.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateCustomerParams }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://3DSI.org/WebServices/CreditCardManagement", name = "updateCustomerParams", scope = UpdateCustomer.class)
    public JAXBElement<UpdateCustomerParams> createUpdateCustomerUpdateCustomerParams(UpdateCustomerParams value) {
        return new JAXBElement<UpdateCustomerParams>(_UpdateCustomerUpdateCustomerParams_QNAME,
                UpdateCustomerParams.class, UpdateCustomer.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TokenCreditCardManagementResult }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://3DSI.org/WebServices/CreditCardManagement", name = "RenameTokenResult", scope = RenameTokenResponse.class)
    public JAXBElement<TokenCreditCardManagementResult> createRenameTokenResponseRenameTokenResult(
            TokenCreditCardManagementResult value) {
        return new JAXBElement<TokenCreditCardManagementResult>(_RenameTokenResponseRenameTokenResult_QNAME,
                TokenCreditCardManagementResult.class, RenameTokenResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreditCardManagementResult }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://3DSI.org/WebServices/CreditCardManagement", name = "UpdateCustomerResult", scope = UpdateCustomerResponse.class)
    public JAXBElement<CreditCardManagementResult> createUpdateCustomerResponseUpdateCustomerResult(
            CreditCardManagementResult value) {
        return new JAXBElement<CreditCardManagementResult>(_UpdateCustomerResponseUpdateCustomerResult_QNAME,
                CreditCardManagementResult.class, UpdateCustomerResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TokenCreditCardManagementResult }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://3DSI.org/WebServices/CreditCardManagement", name = "AddStoredCreditCardResult", scope = AddStoredCreditCardResponse.class)
    public JAXBElement<TokenCreditCardManagementResult> createAddStoredCreditCardResponseAddStoredCreditCardResult(
            TokenCreditCardManagementResult value) {
        return new JAXBElement<TokenCreditCardManagementResult>(
                _AddStoredCreditCardResponseAddStoredCreditCardResult_QNAME, TokenCreditCardManagementResult.class,
                AddStoredCreditCardResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreditCardManagementResult }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://3DSI.org/WebServices/CreditCardManagement", name = "DeleteCustomerResult", scope = DeleteCustomerResponse.class)
    public JAXBElement<CreditCardManagementResult> createDeleteCustomerResponseDeleteCustomerResult(
            CreditCardManagementResult value) {
        return new JAXBElement<CreditCardManagementResult>(_DeleteCustomerResponseDeleteCustomerResult_QNAME,
                CreditCardManagementResult.class, DeleteCustomerResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ClientCredentials }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://3DSI.org/WebServices/CreditCardManagement", name = "clientCredentials", scope = AddStoredCreditCard.class)
    public JAXBElement<ClientCredentials> createAddStoredCreditCardClientCredentials(ClientCredentials value) {
        return new JAXBElement<ClientCredentials>(_ListCustomerLocationsClientCredentials_QNAME,
                ClientCredentials.class, AddStoredCreditCard.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddStoredCardParams }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://3DSI.org/WebServices/CreditCardManagement", name = "addStoredCardParams", scope = AddStoredCreditCard.class)
    public JAXBElement<AddStoredCardParams> createAddStoredCreditCardAddStoredCardParams(AddStoredCardParams value) {
        return new JAXBElement<AddStoredCardParams>(_AddStoredCreditCardAddStoredCardParams_QNAME,
                AddStoredCardParams.class, AddStoredCreditCard.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreditCardManagementResult }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://3DSI.org/WebServices/CreditCardManagement", name = "UpdateStoredCreditCardExpirationDateResult", scope = UpdateStoredCreditCardExpirationDateResponse.class)
    public JAXBElement<CreditCardManagementResult> createUpdateStoredCreditCardExpirationDateResponseUpdateStoredCreditCardExpirationDateResult(
            CreditCardManagementResult value) {
        return new JAXBElement<CreditCardManagementResult>(
                _UpdateStoredCreditCardExpirationDateResponseUpdateStoredCreditCardExpirationDateResult_QNAME,
                CreditCardManagementResult.class, UpdateStoredCreditCardExpirationDateResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreditCardManagementResult }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://3DSI.org/WebServices/CreditCardManagement", name = "MoveCreditCardResult", scope = MoveCreditCardResponse.class)
    public JAXBElement<CreditCardManagementResult> createMoveCreditCardResponseMoveCreditCardResult(
            CreditCardManagementResult value) {
        return new JAXBElement<CreditCardManagementResult>(_MoveCreditCardResponseMoveCreditCardResult_QNAME,
                CreditCardManagementResult.class, MoveCreditCardResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RenameCustomerCodeParams }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://3DSI.org/WebServices/CreditCardManagement", name = "renameCustomerCodeParams", scope = RenameCustomerCode.class)
    public JAXBElement<RenameCustomerCodeParams> createRenameCustomerCodeRenameCustomerCodeParams(
            RenameCustomerCodeParams value) {
        return new JAXBElement<RenameCustomerCodeParams>(_RenameCustomerCodeRenameCustomerCodeParams_QNAME,
                RenameCustomerCodeParams.class, RenameCustomerCode.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ClientCredentials }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://3DSI.org/WebServices/CreditCardManagement", name = "clientCredentials", scope = RenameCustomerCode.class)
    public JAXBElement<ClientCredentials> createRenameCustomerCodeClientCredentials(ClientCredentials value) {
        return new JAXBElement<ClientCredentials>(_ListCustomerLocationsClientCredentials_QNAME,
                ClientCredentials.class, RenameCustomerCode.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreditCardManagementResult }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://3DSI.org/WebServices/CreditCardManagement", name = "ShareCustomerResult", scope = ShareCustomerResponse.class)
    public JAXBElement<CreditCardManagementResult> createShareCustomerResponseShareCustomerResult(
            CreditCardManagementResult value) {
        return new JAXBElement<CreditCardManagementResult>(_ShareCustomerResponseShareCustomerResult_QNAME,
                CreditCardManagementResult.class, ShareCustomerResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreditCardManagementResult }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://3DSI.org/WebServices/CreditCardManagement", name = "DeleteStoredCreditCardResult", scope = DeleteStoredCreditCardResponse.class)
    public JAXBElement<CreditCardManagementResult> createDeleteStoredCreditCardResponseDeleteStoredCreditCardResult(
            CreditCardManagementResult value) {
        return new JAXBElement<CreditCardManagementResult>(
                _DeleteStoredCreditCardResponseDeleteStoredCreditCardResult_QNAME, CreditCardManagementResult.class,
                DeleteStoredCreditCardResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreditCardManagementResult }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://3DSI.org/WebServices/CreditCardManagement", name = "RenameCustomerCodeResult", scope = RenameCustomerCodeResponse.class)
    public JAXBElement<CreditCardManagementResult> createRenameCustomerCodeResponseRenameCustomerCodeResult(
            CreditCardManagementResult value) {
        return new JAXBElement<CreditCardManagementResult>(_RenameCustomerCodeResponseRenameCustomerCodeResult_QNAME,
                CreditCardManagementResult.class, RenameCustomerCodeResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ClientCredentials }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://3DSI.org/WebServices/CreditCardManagement", name = "clientCredentials", scope = RenameToken.class)
    public JAXBElement<ClientCredentials> createRenameTokenClientCredentials(ClientCredentials value) {
        return new JAXBElement<ClientCredentials>(_ListCustomerLocationsClientCredentials_QNAME,
                ClientCredentials.class, RenameToken.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RenameTokenParams }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://3DSI.org/WebServices/CreditCardManagement", name = "renameTokenParams", scope = RenameToken.class)
    public JAXBElement<RenameTokenParams> createRenameTokenRenameTokenParams(RenameTokenParams value) {
        return new JAXBElement<RenameTokenParams>(_RenameTokenRenameTokenParams_QNAME, RenameTokenParams.class,
                RenameToken.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LocationListCreditCardManagementResult }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://3DSI.org/WebServices/CreditCardManagement", name = "ListCustomerLocationsResult", scope = ListCustomerLocationsResponse.class)
    public JAXBElement<LocationListCreditCardManagementResult> createListCustomerLocationsResponseListCustomerLocationsResult(
            LocationListCreditCardManagementResult value) {
        return new JAXBElement<LocationListCreditCardManagementResult>(
                _ListCustomerLocationsResponseListCustomerLocationsResult_QNAME,
                LocationListCreditCardManagementResult.class, ListCustomerLocationsResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ClientCredentials }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://3DSI.org/WebServices/CreditCardManagement", name = "clientCredentials", scope = UpdateStoredCreditCard.class)
    public JAXBElement<ClientCredentials> createUpdateStoredCreditCardClientCredentials(ClientCredentials value) {
        return new JAXBElement<ClientCredentials>(_ListCustomerLocationsClientCredentials_QNAME,
                ClientCredentials.class, UpdateStoredCreditCard.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateStoredCardParams }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://3DSI.org/WebServices/CreditCardManagement", name = "updateStoredCardParams", scope = UpdateStoredCreditCard.class)
    public JAXBElement<UpdateStoredCardParams> createUpdateStoredCreditCardUpdateStoredCardParams(
            UpdateStoredCardParams value) {
        return new JAXBElement<UpdateStoredCardParams>(_UpdateStoredCreditCardUpdateStoredCardParams_QNAME,
                UpdateStoredCardParams.class, UpdateStoredCreditCard.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link StoredCardCreditCardManagementResult }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://3DSI.org/WebServices/CreditCardManagement", name = "GetStoredCreditCardResult", scope = GetStoredCreditCardResponse.class)
    public JAXBElement<StoredCardCreditCardManagementResult> createGetStoredCreditCardResponseGetStoredCreditCardResult(
            StoredCardCreditCardManagementResult value) {
        return new JAXBElement<StoredCardCreditCardManagementResult>(
                _GetStoredCreditCardResponseGetStoredCreditCardResult_QNAME,
                StoredCardCreditCardManagementResult.class, GetStoredCreditCardResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateCustomerParams }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://3DSI.org/WebServices/CreditCardManagement", name = "createCustomerParams", scope = CreateCustomer.class)
    public JAXBElement<CreateCustomerParams> createCreateCustomerCreateCustomerParams(CreateCustomerParams value) {
        return new JAXBElement<CreateCustomerParams>(_CreateCustomerCreateCustomerParams_QNAME,
                CreateCustomerParams.class, CreateCustomer.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ClientCredentials }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://3DSI.org/WebServices/CreditCardManagement", name = "clientCredentials", scope = CreateCustomer.class)
    public JAXBElement<ClientCredentials> createCreateCustomerClientCredentials(ClientCredentials value) {
        return new JAXBElement<ClientCredentials>(_ListCustomerLocationsClientCredentials_QNAME,
                ClientCredentials.class, CreateCustomer.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ClientCredentials }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://3DSI.org/WebServices/CreditCardManagement", name = "clientCredentials", scope = DeleteCustomer.class)
    public JAXBElement<ClientCredentials> createDeleteCustomerClientCredentials(ClientCredentials value) {
        return new JAXBElement<ClientCredentials>(_ListCustomerLocationsClientCredentials_QNAME,
                ClientCredentials.class, DeleteCustomer.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeleteCustomerParams }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://3DSI.org/WebServices/CreditCardManagement", name = "deleteCustomerParams", scope = DeleteCustomer.class)
    public JAXBElement<DeleteCustomerParams> createDeleteCustomerDeleteCustomerParams(DeleteCustomerParams value) {
        return new JAXBElement<DeleteCustomerParams>(_DeleteCustomerDeleteCustomerParams_QNAME,
                DeleteCustomerParams.class, DeleteCustomer.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCustomerParams }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://3DSI.org/WebServices/CreditCardManagement", name = "getCustomerParams", scope = GetCustomer.class)
    public JAXBElement<GetCustomerParams> createGetCustomerGetCustomerParams(GetCustomerParams value) {
        return new JAXBElement<GetCustomerParams>(_GetCustomerGetCustomerParams_QNAME, GetCustomerParams.class,
                GetCustomer.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ClientCredentials }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://3DSI.org/WebServices/CreditCardManagement", name = "clientCredentials", scope = GetCustomer.class)
    public JAXBElement<ClientCredentials> createGetCustomerClientCredentials(ClientCredentials value) {
        return new JAXBElement<ClientCredentials>(_ListCustomerLocationsClientCredentials_QNAME,
                ClientCredentials.class, GetCustomer.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreditCardManagementResult }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://3DSI.org/WebServices/CreditCardManagement", name = "CreateCustomerResult", scope = CreateCustomerResponse.class)
    public JAXBElement<CreditCardManagementResult> createCreateCustomerResponseCreateCustomerResult(
            CreditCardManagementResult value) {
        return new JAXBElement<CreditCardManagementResult>(_CreateCustomerResponseCreateCustomerResult_QNAME,
                CreditCardManagementResult.class, CreateCustomerResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ClientCredentials }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://3DSI.org/WebServices/CreditCardManagement", name = "clientCredentials", scope = GetStoredCreditCard.class)
    public JAXBElement<ClientCredentials> createGetStoredCreditCardClientCredentials(ClientCredentials value) {
        return new JAXBElement<ClientCredentials>(_ListCustomerLocationsClientCredentials_QNAME,
                ClientCredentials.class, GetStoredCreditCard.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetStoredCreditCardParams }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://3DSI.org/WebServices/CreditCardManagement", name = "getStoredCreditCardParams", scope = GetStoredCreditCard.class)
    public JAXBElement<GetStoredCreditCardParams> createGetStoredCreditCardGetStoredCreditCardParams(
            GetStoredCreditCardParams value) {
        return new JAXBElement<GetStoredCreditCardParams>(_GetStoredCreditCardGetStoredCreditCardParams_QNAME,
                GetStoredCreditCardParams.class, GetStoredCreditCard.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreditCardManagementResult }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://3DSI.org/WebServices/CreditCardManagement", name = "UpdateStoredCreditCardResult", scope = UpdateStoredCreditCardResponse.class)
    public JAXBElement<CreditCardManagementResult> createUpdateStoredCreditCardResponseUpdateStoredCreditCardResult(
            CreditCardManagementResult value) {
        return new JAXBElement<CreditCardManagementResult>(
                _UpdateStoredCreditCardResponseUpdateStoredCreditCardResult_QNAME, CreditCardManagementResult.class,
                UpdateStoredCreditCardResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link HostedCardVaultSubmissionInfoResult }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://3DSI.org/WebServices/CreditCardManagement", name = "GetHostedCardVaultSubmissionInfoResult", scope = GetHostedCardVaultSubmissionInfoResponse.class)
    public JAXBElement<HostedCardVaultSubmissionInfoResult> createGetHostedCardVaultSubmissionInfoResponseGetHostedCardVaultSubmissionInfoResult(
            HostedCardVaultSubmissionInfoResult value) {
        return new JAXBElement<HostedCardVaultSubmissionInfoResult>(
                _GetHostedCardVaultSubmissionInfoResponseGetHostedCardVaultSubmissionInfoResult_QNAME,
                HostedCardVaultSubmissionInfoResult.class, GetHostedCardVaultSubmissionInfoResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ClientCredentials }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://3DSI.org/WebServices/CreditCardManagement", name = "clientCredentials", scope = DeleteStoredCreditCard.class)
    public JAXBElement<ClientCredentials> createDeleteStoredCreditCardClientCredentials(ClientCredentials value) {
        return new JAXBElement<ClientCredentials>(_ListCustomerLocationsClientCredentials_QNAME,
                ClientCredentials.class, DeleteStoredCreditCard.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeleteStoredCardParams }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://3DSI.org/WebServices/CreditCardManagement", name = "deleteStoredCardParams", scope = DeleteStoredCreditCard.class)
    public JAXBElement<DeleteStoredCardParams> createDeleteStoredCreditCardDeleteStoredCardParams(
            DeleteStoredCardParams value) {
        return new JAXBElement<DeleteStoredCardParams>(_DeleteStoredCreditCardDeleteStoredCardParams_QNAME,
                DeleteStoredCardParams.class, DeleteStoredCreditCard.class, value);
    }

    private final static QName _Duration_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/",
            "duration");
    private final static QName _Long_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "long");
    private final static QName _DateTime_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/",
            "dateTime");
    private final static QName _String_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/",
            "string");
    private final static QName _UnsignedInt_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/",
            "unsignedInt");
    private final static QName _Char_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "char");
    private final static QName _Short_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "short");
    private final static QName _Boolean_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/",
            "boolean");
    private final static QName _Int_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "int");
    private final static QName _QName_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "QName");
    private final static QName _UnsignedLong_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/",
            "unsignedLong");
    private final static QName _UnsignedByte_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/",
            "unsignedByte");
    private final static QName _UnsignedShort_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/",
            "unsignedShort");
    private final static QName _Float_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "float");
    private final static QName _AnyType_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/",
            "anyType");
    private final static QName _Guid_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "guid");
    private final static QName _Decimal_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/",
            "decimal");
    private final static QName _Base64Binary_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/",
            "base64Binary");
    private final static QName _AnyURI_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/",
            "anyURI");
    private final static QName _Byte_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/", "byte");
    private final static QName _Double_QNAME = new QName("http://schemas.microsoft.com/2003/10/Serialization/",
            "double");

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Duration }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "duration")
    public JAXBElement<Duration> createDuration(Duration value) {
        return new JAXBElement<Duration>(_Duration_QNAME, Duration.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "long")
    public JAXBElement<Long> createLong(Long value) {
        return new JAXBElement<Long>(_Long_QNAME, Long.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "dateTime")
    public JAXBElement<XMLGregorianCalendar> createDateTime(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_DateTime_QNAME, XMLGregorianCalendar.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "string")
    public JAXBElement<String> createString(String value) {
        return new JAXBElement<String>(_String_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "unsignedInt")
    public JAXBElement<Long> createUnsignedInt(Long value) {
        return new JAXBElement<Long>(_UnsignedInt_QNAME, Long.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "char")
    public JAXBElement<Integer> createChar(Integer value) {
        return new JAXBElement<Integer>(_Char_QNAME, Integer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Short }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "short")
    public JAXBElement<Short> createShort(Short value) {
        return new JAXBElement<Short>(_Short_QNAME, Short.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "boolean")
    public JAXBElement<Boolean> createBoolean(Boolean value) {
        return new JAXBElement<Boolean>(_Boolean_QNAME, Boolean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "int")
    public JAXBElement<Integer> createInt(Integer value) {
        return new JAXBElement<Integer>(_Int_QNAME, Integer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link QName }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "QName")
    public JAXBElement<QName> createQName(QName value) {
        return new JAXBElement<QName>(_QName_QNAME, QName.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigInteger }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "unsignedLong")
    public JAXBElement<BigInteger> createUnsignedLong(BigInteger value) {
        return new JAXBElement<BigInteger>(_UnsignedLong_QNAME, BigInteger.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Short }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "unsignedByte")
    public JAXBElement<Short> createUnsignedByte(Short value) {
        return new JAXBElement<Short>(_UnsignedByte_QNAME, Short.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "unsignedShort")
    public JAXBElement<Integer> createUnsignedShort(Integer value) {
        return new JAXBElement<Integer>(_UnsignedShort_QNAME, Integer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Float }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "float")
    public JAXBElement<Float> createFloat(Float value) {
        return new JAXBElement<Float>(_Float_QNAME, Float.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Object }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "anyType")
    public JAXBElement<Object> createAnyType(Object value) {
        return new JAXBElement<Object>(_AnyType_QNAME, Object.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "guid")
    public JAXBElement<String> createGuid(String value) {
        return new JAXBElement<String>(_Guid_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "decimal")
    public JAXBElement<BigDecimal> createDecimal(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_Decimal_QNAME, BigDecimal.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link byte[]}{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "base64Binary")
    public JAXBElement<byte[]> createBase64Binary(byte[] value) {
        return new JAXBElement<byte[]>(_Base64Binary_QNAME, byte[].class, null, ((byte[]) value));
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "anyURI")
    public JAXBElement<String> createAnyURI(String value) {
        return new JAXBElement<String>(_AnyURI_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Byte }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "byte")
    public JAXBElement<Byte> createByte(Byte value) {
        return new JAXBElement<Byte>(_Byte_QNAME, Byte.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Double }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/", name = "double")
    public JAXBElement<Double> createDouble(Double value) {
        return new JAXBElement<Double>(_Double_QNAME, Double.class, null, value);
    }

    private final static QName _ArrayOfstring_QNAME = new QName(
            "http://schemas.microsoft.com/2003/10/Serialization/Arrays", "ArrayOfstring");

    /**
     * Create an instance of {@link ArrayOfstring }
     * 
     */
    public ArrayOfstring createArrayOfstring() {
        return new ArrayOfstring();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfstring }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.microsoft.com/2003/10/Serialization/Arrays", name = "ArrayOfstring")
    public JAXBElement<ArrayOfstring> createArrayOfstring(ArrayOfstring value) {
        return new JAXBElement<ArrayOfstring>(_ArrayOfstring_QNAME, ArrayOfstring.class, null, value);
    }

    private final static QName _CountryCode_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Common.Enums", "CountryCode");
    private final static QName _HostedCardVaultFailureReason_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Common.Enums", "HostedCardVaultFailureReason");
    private final static QName _HostedCardVaultSubmissionStatus_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Common.Enums", "HostedCardVaultSubmissionStatus");
    private final static QName _StateProvinceCode_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Common.Enums", "StateProvinceCode");
    private final static QName _HostedCardVaultSubmissionType_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Common.Enums", "HostedCardVaultSubmissionType");

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CountryCode }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Common.Enums", name = "CountryCode")
    public JAXBElement<CountryCode> createCountryCode(CountryCode value) {
        return new JAXBElement<CountryCode>(_CountryCode_QNAME, CountryCode.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link HostedCardVaultFailureReason }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Common.Enums", name = "HostedCardVaultFailureReason")
    public JAXBElement<HostedCardVaultFailureReason> createHostedCardVaultFailureReason(
            HostedCardVaultFailureReason value) {
        return new JAXBElement<HostedCardVaultFailureReason>(_HostedCardVaultFailureReason_QNAME,
                HostedCardVaultFailureReason.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link HostedCardVaultSubmissionStatus }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Common.Enums", name = "HostedCardVaultSubmissionStatus")
    public JAXBElement<HostedCardVaultSubmissionStatus> createHostedCardVaultSubmissionStatus(
            HostedCardVaultSubmissionStatus value) {
        return new JAXBElement<HostedCardVaultSubmissionStatus>(_HostedCardVaultSubmissionStatus_QNAME,
                HostedCardVaultSubmissionStatus.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link StateProvinceCode }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Common.Enums", name = "StateProvinceCode")
    public JAXBElement<StateProvinceCode> createStateProvinceCode(StateProvinceCode value) {
        return new JAXBElement<StateProvinceCode>(_StateProvinceCode_QNAME, StateProvinceCode.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link HostedCardVaultSubmissionType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Common.Enums", name = "HostedCardVaultSubmissionType")
    public JAXBElement<HostedCardVaultSubmissionType> createHostedCardVaultSubmissionType(
            HostedCardVaultSubmissionType value) {
        return new JAXBElement<HostedCardVaultSubmissionType>(_HostedCardVaultSubmissionType_QNAME,
                HostedCardVaultSubmissionType.class, null, value);
    }

    private final static QName _ApplicationFault_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ApplicationError", "ApplicationFault");
    private final static QName _ApplicationFaultMessage_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ApplicationError", "Message");

    /**
     * Create an instance of {@link ApplicationFault }
     * 
     */
    public ApplicationFault createApplicationFault() {
        return new ApplicationFault();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ApplicationFault }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ApplicationError", name = "ApplicationFault")
    public JAXBElement<ApplicationFault> createApplicationFault(ApplicationFault value) {
        return new JAXBElement<ApplicationFault>(_ApplicationFault_QNAME, ApplicationFault.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ApplicationError", name = "Message", scope = ApplicationFault.class)
    public JAXBElement<String> createApplicationFaultMessage(String value) {
        return new JAXBElement<String>(_ApplicationFaultMessage_QNAME, String.class, ApplicationFault.class, value);
    }

    private final static QName _StoredCreditCard_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", "StoredCreditCard");
    private final static QName _Email_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", "Email");
    private final static QName _Customer_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", "Customer");
    private final static QName _Address_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", "Address");
    private final static QName _ArrayOfEmail_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", "ArrayOfEmail");
    private final static QName _WebServiceResult_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", "WebServiceResult");
    private final static QName _LocationIdentifier_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", "LocationIdentifier");
    private final static QName _CustomerIdentifier_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", "CustomerIdentifier");
    private final static QName _Contact_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", "Contact");
    private final static QName _CreditCardManagementFailureReason_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Enums",
            "CreditCardManagementFailureReason");
    private final static QName _CreditCard_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", "CreditCard");
    private final static QName _CreditCardTypes_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Enums", "CreditCardTypes");
    private final static QName _ClientCredentials_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", "ClientCredentials");
    private final static QName _OptionalCreditCardInformation_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions",
            "OptionalCreditCardInformation");
    private final static QName _CreditCardNameOnCard_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", "NameOnCard");
    private final static QName _CreditCardCardAccountNumber_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", "CardAccountNumber");
    private final static QName _CreditCardCardholder_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", "Cardholder");
    private final static QName _CreditCardReceiptEmailAddresses_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions",
            "ReceiptEmailAddresses");
    private final static QName _CreditCardBillingAddress_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", "BillingAddress");
    private final static QName _CreditCardShippingAddress_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", "ShippingAddress");
    private final static QName _LocationIdentifierLocationCode_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", "LocationCode");
    private final static QName _LocationIdentifierMerchantCode_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", "MerchantCode");
    private final static QName _StoredCreditCardToken_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", "Token");
    private final static QName _StoredCreditCardNotes_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", "Notes");
    private final static QName _StoredCreditCardFriendlyName_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", "FriendlyName");
    private final static QName _CustomerTransactionInfo_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", "TransactionInfo");
    private final static QName _CustomerCode_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", "Code");
    private final static QName _CustomerFax_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", "Fax");
    private final static QName _CustomerName_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", "Name");
    private final static QName _CustomerUrl_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", "Url");
    private final static QName _CustomerPhone_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", "Phone");
    private final static QName _AddressPostalCode_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", "PostalCode");
    private final static QName _AddressCity_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", "City");
    private final static QName _AddressAddressLine2_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", "AddressLine2");
    private final static QName _AddressAddressLine1_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", "AddressLine1");
    private final static QName _AddressForeignStateProvinceRegion_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions",
            "ForeignStateProvinceRegion");
    private final static QName _CustomerIdentifierCustomerCode_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", "CustomerCode");
    private final static QName _ClientCredentialsClientCode_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", "ClientCode");
    private final static QName _ClientCredentialsPassword_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", "Password");
    private final static QName _ClientCredentialsUserName_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", "UserName");
    private final static QName _ContactTitle_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", "Title");
    private final static QName _ContactLastName_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", "LastName");
    private final static QName _ContactMiddleName_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", "MiddleName");
    private final static QName _ContactSuffix_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", "Suffix");
    private final static QName _ContactFirstName_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", "FirstName");
    private final static QName _EmailDisplayAs_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", "DisplayAs");
    private final static QName _WebServiceResultValidationFailures_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", "ValidationFailures");

    /**
     * Create an instance of {@link ArrayOfEmail }
     * 
     */
    public ArrayOfEmail createArrayOfEmail() {
        return new ArrayOfEmail();
    }

    /**
     * Create an instance of {@link CreditCard }
     * 
     */
    public CreditCard createCreditCard() {
        return new CreditCard();
    }

    /**
     * Create an instance of {@link LocationIdentifier }
     * 
     */
    public LocationIdentifier createLocationIdentifier() {
        return new LocationIdentifier();
    }

    /**
     * Create an instance of {@link StoredCreditCard }
     * 
     */
    public StoredCreditCard createStoredCreditCard() {
        return new StoredCreditCard();
    }

    /**
     * Create an instance of {@link OptionalCreditCardInformation }
     * 
     */
    public OptionalCreditCardInformation createOptionalCreditCardInformation() {
        return new OptionalCreditCardInformation();
    }

    /**
     * Create an instance of {@link Customer }
     * 
     */
    public Customer createCustomer() {
        return new Customer();
    }

    /**
     * Create an instance of {@link Address }
     * 
     */
    public Address createAddress() {
        return new Address();
    }

    /**
     * Create an instance of {@link CustomerIdentifier }
     * 
     */
    public CustomerIdentifier createCustomerIdentifier() {
        return new CustomerIdentifier();
    }

    /**
     * Create an instance of {@link ClientCredentials }
     * 
     */
    public ClientCredentials createClientCredentials() {
        return new ClientCredentials();
    }

    /**
     * Create an instance of {@link Contact }
     * 
     */
    public Contact createContact() {
        return new Contact();
    }

    /**
     * Create an instance of {@link Email }
     * 
     */
    public Email createEmail() {
        return new Email();
    }

    /**
     * Create an instance of {@link WebServiceResult }
     * 
     */
    public WebServiceResult createWebServiceResult() {
        return new WebServiceResult();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link StoredCreditCard }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", name = "StoredCreditCard")
    public JAXBElement<StoredCreditCard> createStoredCreditCard(StoredCreditCard value) {
        return new JAXBElement<StoredCreditCard>(_StoredCreditCard_QNAME, StoredCreditCard.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Email }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", name = "Email")
    public JAXBElement<Email> createEmail(Email value) {
        return new JAXBElement<Email>(_Email_QNAME, Email.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Customer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", name = "Customer")
    public JAXBElement<Customer> createCustomer(Customer value) {
        return new JAXBElement<Customer>(_Customer_QNAME, Customer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Address }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", name = "Address")
    public JAXBElement<Address> createAddress(Address value) {
        return new JAXBElement<Address>(_Address_QNAME, Address.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfEmail }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", name = "ArrayOfEmail")
    public JAXBElement<ArrayOfEmail> createArrayOfEmail(ArrayOfEmail value) {
        return new JAXBElement<ArrayOfEmail>(_ArrayOfEmail_QNAME, ArrayOfEmail.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link WebServiceResult }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", name = "WebServiceResult")
    public JAXBElement<WebServiceResult> createWebServiceResult(WebServiceResult value) {
        return new JAXBElement<WebServiceResult>(_WebServiceResult_QNAME, WebServiceResult.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LocationIdentifier }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", name = "LocationIdentifier")
    public JAXBElement<LocationIdentifier> createLocationIdentifier(LocationIdentifier value) {
        return new JAXBElement<LocationIdentifier>(_LocationIdentifier_QNAME, LocationIdentifier.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CustomerIdentifier }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", name = "CustomerIdentifier")
    public JAXBElement<CustomerIdentifier> createCustomerIdentifier(CustomerIdentifier value) {
        return new JAXBElement<CustomerIdentifier>(_CustomerIdentifier_QNAME, CustomerIdentifier.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Contact }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", name = "Contact")
    public JAXBElement<Contact> createContact(Contact value) {
        return new JAXBElement<Contact>(_Contact_QNAME, Contact.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreditCardManagementFailureReason }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Enums", name = "CreditCardManagementFailureReason")
    public JAXBElement<CreditCardManagementFailureReason> createCreditCardManagementFailureReason(
            CreditCardManagementFailureReason value) {
        return new JAXBElement<CreditCardManagementFailureReason>(_CreditCardManagementFailureReason_QNAME,
                CreditCardManagementFailureReason.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreditCard }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", name = "CreditCard")
    public JAXBElement<CreditCard> createCreditCard(CreditCard value) {
        return new JAXBElement<CreditCard>(_CreditCard_QNAME, CreditCard.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreditCardTypes }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Enums", name = "CreditCardTypes")
    public JAXBElement<CreditCardTypes> createCreditCardTypes(CreditCardTypes value) {
        return new JAXBElement<CreditCardTypes>(_CreditCardTypes_QNAME, CreditCardTypes.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ClientCredentials }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", name = "ClientCredentials")
    public JAXBElement<ClientCredentials> createClientCredentials(ClientCredentials value) {
        return new JAXBElement<ClientCredentials>(_ClientCredentials_QNAME, ClientCredentials.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OptionalCreditCardInformation }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", name = "OptionalCreditCardInformation")
    public JAXBElement<OptionalCreditCardInformation> createOptionalCreditCardInformation(
            OptionalCreditCardInformation value) {
        return new JAXBElement<OptionalCreditCardInformation>(_OptionalCreditCardInformation_QNAME,
                OptionalCreditCardInformation.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", name = "NameOnCard", scope = CreditCard.class)
    public JAXBElement<String> createCreditCardNameOnCard(String value) {
        return new JAXBElement<String>(_CreditCardNameOnCard_QNAME, String.class, CreditCard.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", name = "CardAccountNumber", scope = CreditCard.class)
    public JAXBElement<String> createCreditCardCardAccountNumber(String value) {
        return new JAXBElement<String>(_CreditCardCardAccountNumber_QNAME, String.class, CreditCard.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Contact }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", name = "Cardholder", scope = CreditCard.class)
    public JAXBElement<Contact> createCreditCardCardholder(Contact value) {
        return new JAXBElement<Contact>(_CreditCardCardholder_QNAME, Contact.class, CreditCard.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfEmail }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", name = "ReceiptEmailAddresses", scope = CreditCard.class)
    public JAXBElement<ArrayOfEmail> createCreditCardReceiptEmailAddresses(ArrayOfEmail value) {
        return new JAXBElement<ArrayOfEmail>(_CreditCardReceiptEmailAddresses_QNAME, ArrayOfEmail.class,
                CreditCard.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Address }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", name = "BillingAddress", scope = CreditCard.class)
    public JAXBElement<Address> createCreditCardBillingAddress(Address value) {
        return new JAXBElement<Address>(_CreditCardBillingAddress_QNAME, Address.class, CreditCard.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Address }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", name = "ShippingAddress", scope = CreditCard.class)
    public JAXBElement<Address> createCreditCardShippingAddress(Address value) {
        return new JAXBElement<Address>(_CreditCardShippingAddress_QNAME, Address.class, CreditCard.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", name = "LocationCode", scope = LocationIdentifier.class)
    public JAXBElement<String> createLocationIdentifierLocationCode(String value) {
        return new JAXBElement<String>(_LocationIdentifierLocationCode_QNAME, String.class, LocationIdentifier.class,
                value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", name = "MerchantCode", scope = LocationIdentifier.class)
    public JAXBElement<String> createLocationIdentifierMerchantCode(String value) {
        return new JAXBElement<String>(_LocationIdentifierMerchantCode_QNAME, String.class, LocationIdentifier.class,
                value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", name = "Token", scope = StoredCreditCard.class)
    public JAXBElement<String> createStoredCreditCardToken(String value) {
        return new JAXBElement<String>(_StoredCreditCardToken_QNAME, String.class, StoredCreditCard.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", name = "Notes", scope = StoredCreditCard.class)
    public JAXBElement<String> createStoredCreditCardNotes(String value) {
        return new JAXBElement<String>(_StoredCreditCardNotes_QNAME, String.class, StoredCreditCard.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", name = "FriendlyName", scope = StoredCreditCard.class)
    public JAXBElement<String> createStoredCreditCardFriendlyName(String value) {
        return new JAXBElement<String>(_StoredCreditCardFriendlyName_QNAME, String.class, StoredCreditCard.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", name = "NameOnCard", scope = OptionalCreditCardInformation.class)
    public JAXBElement<String> createOptionalCreditCardInformationNameOnCard(String value) {
        return new JAXBElement<String>(_CreditCardNameOnCard_QNAME, String.class, OptionalCreditCardInformation.class,
                value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", name = "Token", scope = OptionalCreditCardInformation.class)
    public JAXBElement<String> createOptionalCreditCardInformationToken(String value) {
        return new JAXBElement<String>(_StoredCreditCardToken_QNAME, String.class, OptionalCreditCardInformation.class,
                value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Contact }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", name = "Cardholder", scope = OptionalCreditCardInformation.class)
    public JAXBElement<Contact> createOptionalCreditCardInformationCardholder(Contact value) {
        return new JAXBElement<Contact>(_CreditCardCardholder_QNAME, Contact.class,
                OptionalCreditCardInformation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", name = "Notes", scope = OptionalCreditCardInformation.class)
    public JAXBElement<String> createOptionalCreditCardInformationNotes(String value) {
        return new JAXBElement<String>(_StoredCreditCardNotes_QNAME, String.class, OptionalCreditCardInformation.class,
                value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Address }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", name = "BillingAddress", scope = OptionalCreditCardInformation.class)
    public JAXBElement<Address> createOptionalCreditCardInformationBillingAddress(Address value) {
        return new JAXBElement<Address>(_CreditCardBillingAddress_QNAME, Address.class,
                OptionalCreditCardInformation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", name = "FriendlyName", scope = OptionalCreditCardInformation.class)
    public JAXBElement<String> createOptionalCreditCardInformationFriendlyName(String value) {
        return new JAXBElement<String>(_StoredCreditCardFriendlyName_QNAME, String.class,
                OptionalCreditCardInformation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Address }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", name = "ShippingAddress", scope = OptionalCreditCardInformation.class)
    public JAXBElement<Address> createOptionalCreditCardInformationShippingAddress(Address value) {
        return new JAXBElement<Address>(_CreditCardShippingAddress_QNAME, Address.class,
                OptionalCreditCardInformation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", name = "TransactionInfo", scope = Customer.class)
    public JAXBElement<String> createCustomerTransactionInfo(String value) {
        return new JAXBElement<String>(_CustomerTransactionInfo_QNAME, String.class, Customer.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", name = "Code", scope = Customer.class)
    public JAXBElement<String> createCustomerCode(String value) {
        return new JAXBElement<String>(_CustomerCode_QNAME, String.class, Customer.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", name = "Fax", scope = Customer.class)
    public JAXBElement<String> createCustomerFax(String value) {
        return new JAXBElement<String>(_CustomerFax_QNAME, String.class, Customer.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", name = "Name", scope = Customer.class)
    public JAXBElement<String> createCustomerName(String value) {
        return new JAXBElement<String>(_CustomerName_QNAME, String.class, Customer.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", name = "Url", scope = Customer.class)
    public JAXBElement<String> createCustomerUrl(String value) {
        return new JAXBElement<String>(_CustomerUrl_QNAME, String.class, Customer.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Address }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", name = "BillingAddress", scope = Customer.class)
    public JAXBElement<Address> createCustomerBillingAddress(Address value) {
        return new JAXBElement<Address>(_CreditCardBillingAddress_QNAME, Address.class, Customer.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Address }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", name = "ShippingAddress", scope = Customer.class)
    public JAXBElement<Address> createCustomerShippingAddress(Address value) {
        return new JAXBElement<Address>(_CreditCardShippingAddress_QNAME, Address.class, Customer.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", name = "Phone", scope = Customer.class)
    public JAXBElement<String> createCustomerPhone(String value) {
        return new JAXBElement<String>(_CustomerPhone_QNAME, String.class, Customer.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", name = "PostalCode", scope = Address.class)
    public JAXBElement<String> createAddressPostalCode(String value) {
        return new JAXBElement<String>(_AddressPostalCode_QNAME, String.class, Address.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", name = "City", scope = Address.class)
    public JAXBElement<String> createAddressCity(String value) {
        return new JAXBElement<String>(_AddressCity_QNAME, String.class, Address.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", name = "AddressLine2", scope = Address.class)
    public JAXBElement<String> createAddressAddressLine2(String value) {
        return new JAXBElement<String>(_AddressAddressLine2_QNAME, String.class, Address.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", name = "AddressLine1", scope = Address.class)
    public JAXBElement<String> createAddressAddressLine1(String value) {
        return new JAXBElement<String>(_AddressAddressLine1_QNAME, String.class, Address.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", name = "ForeignStateProvinceRegion", scope = Address.class)
    public JAXBElement<String> createAddressForeignStateProvinceRegion(String value) {
        return new JAXBElement<String>(_AddressForeignStateProvinceRegion_QNAME, String.class, Address.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", name = "LocationCode", scope = CustomerIdentifier.class)
    public JAXBElement<String> createCustomerIdentifierLocationCode(String value) {
        return new JAXBElement<String>(_LocationIdentifierLocationCode_QNAME, String.class, CustomerIdentifier.class,
                value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", name = "CustomerCode", scope = CustomerIdentifier.class)
    public JAXBElement<String> createCustomerIdentifierCustomerCode(String value) {
        return new JAXBElement<String>(_CustomerIdentifierCustomerCode_QNAME, String.class, CustomerIdentifier.class,
                value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", name = "MerchantCode", scope = CustomerIdentifier.class)
    public JAXBElement<String> createCustomerIdentifierMerchantCode(String value) {
        return new JAXBElement<String>(_LocationIdentifierMerchantCode_QNAME, String.class, CustomerIdentifier.class,
                value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", name = "ClientCode", scope = ClientCredentials.class)
    public JAXBElement<String> createClientCredentialsClientCode(String value) {
        return new JAXBElement<String>(_ClientCredentialsClientCode_QNAME, String.class, ClientCredentials.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", name = "Password", scope = ClientCredentials.class)
    public JAXBElement<String> createClientCredentialsPassword(String value) {
        return new JAXBElement<String>(_ClientCredentialsPassword_QNAME, String.class, ClientCredentials.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", name = "UserName", scope = ClientCredentials.class)
    public JAXBElement<String> createClientCredentialsUserName(String value) {
        return new JAXBElement<String>(_ClientCredentialsUserName_QNAME, String.class, ClientCredentials.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", name = "Title", scope = Contact.class)
    public JAXBElement<String> createContactTitle(String value) {
        return new JAXBElement<String>(_ContactTitle_QNAME, String.class, Contact.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", name = "LastName", scope = Contact.class)
    public JAXBElement<String> createContactLastName(String value) {
        return new JAXBElement<String>(_ContactLastName_QNAME, String.class, Contact.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", name = "Fax", scope = Contact.class)
    public JAXBElement<String> createContactFax(String value) {
        return new JAXBElement<String>(_CustomerFax_QNAME, String.class, Contact.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", name = "MiddleName", scope = Contact.class)
    public JAXBElement<String> createContactMiddleName(String value) {
        return new JAXBElement<String>(_ContactMiddleName_QNAME, String.class, Contact.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Email }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", name = "Email", scope = Contact.class)
    public JAXBElement<Email> createContactEmail(Email value) {
        return new JAXBElement<Email>(_Email_QNAME, Email.class, Contact.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", name = "Suffix", scope = Contact.class)
    public JAXBElement<String> createContactSuffix(String value) {
        return new JAXBElement<String>(_ContactSuffix_QNAME, String.class, Contact.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", name = "FirstName", scope = Contact.class)
    public JAXBElement<String> createContactFirstName(String value) {
        return new JAXBElement<String>(_ContactFirstName_QNAME, String.class, Contact.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", name = "Phone", scope = Contact.class)
    public JAXBElement<String> createContactPhone(String value) {
        return new JAXBElement<String>(_CustomerPhone_QNAME, String.class, Contact.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", name = "DisplayAs", scope = Email.class)
    public JAXBElement<String> createEmailDisplayAs(String value) {
        return new JAXBElement<String>(_EmailDisplayAs_QNAME, String.class, Email.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", name = "Address", scope = Email.class)
    public JAXBElement<String> createEmailAddress(String value) {
        return new JAXBElement<String>(_Address_QNAME, String.class, Email.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfstring }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions", name = "ValidationFailures", scope = WebServiceResult.class)
    public JAXBElement<ArrayOfstring> createWebServiceResultValidationFailures(ArrayOfstring value) {
        return new JAXBElement<ArrayOfstring>(_WebServiceResultValidationFailures_QNAME, ArrayOfstring.class,
                WebServiceResult.class, value);
    }

    private final static QName _StoredCardCreditCardManagementResult_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardManagementDefinitions",
            "StoredCardCreditCardManagementResult");
    private final static QName _CreditCardManagementResult_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardManagementDefinitions",
            "CreditCardManagementResult");
    private final static QName _CustomerCreditCardManagementResult_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardManagementDefinitions",
            "CustomerCreditCardManagementResult");
    private final static QName _LocationListCreditCardManagementResult_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardManagementDefinitions",
            "LocationListCreditCardManagementResult");
    private final static QName _TokenCreditCardManagementResult_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardManagementDefinitions",
            "TokenCreditCardManagementResult");
    private final static QName _HostedCardVaultSubmissionInfoResult_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardManagementDefinitions",
            "HostedCardVaultSubmissionInfoResult");
    private final static QName _HostedCardVaultSubmissionIdResult_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardManagementDefinitions",
            "HostedCardVaultSubmissionIdResult");
    private final static QName _StoredCardCreditCardManagementResultCreditCard_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardManagementDefinitions",
            "CreditCard");
    private final static QName _HostedCardVaultSubmissionIdResultSubmissionId_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardManagementDefinitions",
            "SubmissionId");
    private final static QName _CustomerCreditCardManagementResultCustomer_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardManagementDefinitions",
            "Customer");
    private final static QName _HostedCardVaultSubmissionInfoResultCustomerIdentifier_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardManagementDefinitions",
            "CustomerIdentifier");
    private final static QName _HostedCardVaultSubmissionInfoResultCardAccountNumberLast4_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardManagementDefinitions",
            "CardAccountNumberLast4");
    private final static QName _HostedCardVaultSubmissionInfoResultNonStoredCardFailureMessage_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardManagementDefinitions",
            "NonStoredCardFailureMessage");
    private final static QName _HostedCardVaultSubmissionInfoResultToken_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardManagementDefinitions",
            "Token");
    private final static QName _LocationListCreditCardManagementResultLocationCodes_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardManagementDefinitions",
            "LocationCodes");

    /**
     * Create an instance of {@link StoredCardCreditCardManagementResult }
     * 
     */
    public StoredCardCreditCardManagementResult createStoredCardCreditCardManagementResult() {
        return new StoredCardCreditCardManagementResult();
    }

    /**
     * Create an instance of {@link HostedCardVaultSubmissionIdResult }
     * 
     */
    public HostedCardVaultSubmissionIdResult createHostedCardVaultSubmissionIdResult() {
        return new HostedCardVaultSubmissionIdResult();
    }

    /**
     * Create an instance of {@link CustomerCreditCardManagementResult }
     * 
     */
    public CustomerCreditCardManagementResult createCustomerCreditCardManagementResult() {
        return new CustomerCreditCardManagementResult();
    }

    /**
     * Create an instance of {@link CreditCardManagementResult }
     * 
     */
    public CreditCardManagementResult createCreditCardManagementResult() {
        return new CreditCardManagementResult();
    }

    /**
     * Create an instance of {@link HostedCardVaultSubmissionInfoResult }
     * 
     */
    public HostedCardVaultSubmissionInfoResult createHostedCardVaultSubmissionInfoResult() {
        return new HostedCardVaultSubmissionInfoResult();
    }

    /**
     * Create an instance of {@link TokenCreditCardManagementResult }
     * 
     */
    public TokenCreditCardManagementResult createTokenCreditCardManagementResult() {
        return new TokenCreditCardManagementResult();
    }

    /**
     * Create an instance of {@link LocationListCreditCardManagementResult }
     * 
     */
    public LocationListCreditCardManagementResult createLocationListCreditCardManagementResult() {
        return new LocationListCreditCardManagementResult();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link StoredCardCreditCardManagementResult }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardManagementDefinitions", name = "StoredCardCreditCardManagementResult")
    public JAXBElement<StoredCardCreditCardManagementResult> createStoredCardCreditCardManagementResult(
            StoredCardCreditCardManagementResult value) {
        return new JAXBElement<StoredCardCreditCardManagementResult>(_StoredCardCreditCardManagementResult_QNAME,
                StoredCardCreditCardManagementResult.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreditCardManagementResult }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardManagementDefinitions", name = "CreditCardManagementResult")
    public JAXBElement<CreditCardManagementResult> createCreditCardManagementResult(CreditCardManagementResult value) {
        return new JAXBElement<CreditCardManagementResult>(_CreditCardManagementResult_QNAME,
                CreditCardManagementResult.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CustomerCreditCardManagementResult }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardManagementDefinitions", name = "CustomerCreditCardManagementResult")
    public JAXBElement<CustomerCreditCardManagementResult> createCustomerCreditCardManagementResult(
            CustomerCreditCardManagementResult value) {
        return new JAXBElement<CustomerCreditCardManagementResult>(_CustomerCreditCardManagementResult_QNAME,
                CustomerCreditCardManagementResult.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LocationListCreditCardManagementResult }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardManagementDefinitions", name = "LocationListCreditCardManagementResult")
    public JAXBElement<LocationListCreditCardManagementResult> createLocationListCreditCardManagementResult(
            LocationListCreditCardManagementResult value) {
        return new JAXBElement<LocationListCreditCardManagementResult>(_LocationListCreditCardManagementResult_QNAME,
                LocationListCreditCardManagementResult.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TokenCreditCardManagementResult }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardManagementDefinitions", name = "TokenCreditCardManagementResult")
    public JAXBElement<TokenCreditCardManagementResult> createTokenCreditCardManagementResult(
            TokenCreditCardManagementResult value) {
        return new JAXBElement<TokenCreditCardManagementResult>(_TokenCreditCardManagementResult_QNAME,
                TokenCreditCardManagementResult.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link HostedCardVaultSubmissionInfoResult }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardManagementDefinitions", name = "HostedCardVaultSubmissionInfoResult")
    public JAXBElement<HostedCardVaultSubmissionInfoResult> createHostedCardVaultSubmissionInfoResult(
            HostedCardVaultSubmissionInfoResult value) {
        return new JAXBElement<HostedCardVaultSubmissionInfoResult>(_HostedCardVaultSubmissionInfoResult_QNAME,
                HostedCardVaultSubmissionInfoResult.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link HostedCardVaultSubmissionIdResult }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardManagementDefinitions", name = "HostedCardVaultSubmissionIdResult")
    public JAXBElement<HostedCardVaultSubmissionIdResult> createHostedCardVaultSubmissionIdResult(
            HostedCardVaultSubmissionIdResult value) {
        return new JAXBElement<HostedCardVaultSubmissionIdResult>(_HostedCardVaultSubmissionIdResult_QNAME,
                HostedCardVaultSubmissionIdResult.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link StoredCreditCard }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardManagementDefinitions", name = "CreditCard", scope = StoredCardCreditCardManagementResult.class)
    public JAXBElement<StoredCreditCard> createStoredCardCreditCardManagementResultCreditCard(StoredCreditCard value) {
        return new JAXBElement<StoredCreditCard>(_StoredCardCreditCardManagementResultCreditCard_QNAME,
                StoredCreditCard.class, StoredCardCreditCardManagementResult.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardManagementDefinitions", name = "SubmissionId", scope = HostedCardVaultSubmissionIdResult.class)
    public JAXBElement<String> createHostedCardVaultSubmissionIdResultSubmissionId(String value) {
        return new JAXBElement<String>(_HostedCardVaultSubmissionIdResultSubmissionId_QNAME, String.class,
                HostedCardVaultSubmissionIdResult.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Customer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardManagementDefinitions", name = "Customer", scope = CustomerCreditCardManagementResult.class)
    public JAXBElement<Customer> createCustomerCreditCardManagementResultCustomer(Customer value) {
        return new JAXBElement<Customer>(_CustomerCreditCardManagementResultCustomer_QNAME, Customer.class,
                CustomerCreditCardManagementResult.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CustomerIdentifier }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardManagementDefinitions", name = "CustomerIdentifier", scope = HostedCardVaultSubmissionInfoResult.class)
    public JAXBElement<CustomerIdentifier> createHostedCardVaultSubmissionInfoResultCustomerIdentifier(
            CustomerIdentifier value) {
        return new JAXBElement<CustomerIdentifier>(_HostedCardVaultSubmissionInfoResultCustomerIdentifier_QNAME,
                CustomerIdentifier.class, HostedCardVaultSubmissionInfoResult.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardManagementDefinitions", name = "CardAccountNumberLast4", scope = HostedCardVaultSubmissionInfoResult.class)
    public JAXBElement<String> createHostedCardVaultSubmissionInfoResultCardAccountNumberLast4(String value) {
        return new JAXBElement<String>(_HostedCardVaultSubmissionInfoResultCardAccountNumberLast4_QNAME, String.class,
                HostedCardVaultSubmissionInfoResult.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardManagementDefinitions", name = "NonStoredCardFailureMessage", scope = HostedCardVaultSubmissionInfoResult.class)
    public JAXBElement<String> createHostedCardVaultSubmissionInfoResultNonStoredCardFailureMessage(String value) {
        return new JAXBElement<String>(_HostedCardVaultSubmissionInfoResultNonStoredCardFailureMessage_QNAME,
                String.class, HostedCardVaultSubmissionInfoResult.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardManagementDefinitions", name = "Token", scope = HostedCardVaultSubmissionInfoResult.class)
    public JAXBElement<String> createHostedCardVaultSubmissionInfoResultToken(String value) {
        return new JAXBElement<String>(_HostedCardVaultSubmissionInfoResultToken_QNAME, String.class,
                HostedCardVaultSubmissionInfoResult.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardManagementDefinitions", name = "SubmissionId", scope = HostedCardVaultSubmissionInfoResult.class)
    public JAXBElement<String> createHostedCardVaultSubmissionInfoResultSubmissionId(String value) {
        return new JAXBElement<String>(_HostedCardVaultSubmissionIdResultSubmissionId_QNAME, String.class,
                HostedCardVaultSubmissionInfoResult.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardManagementDefinitions", name = "Token", scope = TokenCreditCardManagementResult.class)
    public JAXBElement<String> createTokenCreditCardManagementResultToken(String value) {
        return new JAXBElement<String>(_HostedCardVaultSubmissionInfoResultToken_QNAME, String.class,
                TokenCreditCardManagementResult.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfstring }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardManagementDefinitions", name = "LocationCodes", scope = LocationListCreditCardManagementResult.class)
    public JAXBElement<ArrayOfstring> createLocationListCreditCardManagementResultLocationCodes(ArrayOfstring value) {
        return new JAXBElement<ArrayOfstring>(_LocationListCreditCardManagementResultLocationCodes_QNAME,
                ArrayOfstring.class, LocationListCreditCardManagementResult.class, value);
    }

    private final static QName _CreateCustomerParams_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Parameters.CreditCardManagement",
            "CreateCustomerParams");
    private final static QName _RenameTokenParams_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Parameters.CreditCardManagement",
            "RenameTokenParams");
    private final static QName _GetTokenForCardNumberParams_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Parameters.CreditCardManagement",
            "GetTokenForCardNumberParams");
    private final static QName _UpdateCustomerParams_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Parameters.CreditCardManagement",
            "UpdateCustomerParams");
    private final static QName _CreditCardManagementParams_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Parameters.CreditCardManagement",
            "CreditCardManagementParams");
    private final static QName _RenameCustomerCodeParams_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Parameters.CreditCardManagement",
            "RenameCustomerCodeParams");
    private final static QName _AddStoredCardParams_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Parameters.CreditCardManagement",
            "AddStoredCardParams");
    private final static QName _MoveCreditCardParams_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Parameters.CreditCardManagement",
            "MoveCreditCardParams");
    private final static QName _DeleteCustomerParams_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Parameters.CreditCardManagement",
            "DeleteCustomerParams");
    private final static QName _GetCustomerParams_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Parameters.CreditCardManagement",
            "GetCustomerParams");
    private final static QName _ShareCustomerParams_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Parameters.CreditCardManagement",
            "ShareCustomerParams");
    private final static QName _UpdateStoredCardParams_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Parameters.CreditCardManagement",
            "UpdateStoredCardParams");
    private final static QName _CreateHostedCardVaultSubmissionIdParams_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Parameters.CreditCardManagement",
            "CreateHostedCardVaultSubmissionIdParams");
    private final static QName _UpdateStoredCardExpirationDateParams_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Parameters.CreditCardManagement",
            "UpdateStoredCardExpirationDateParams");
    private final static QName _GetStoredCreditCardParams_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Parameters.CreditCardManagement",
            "GetStoredCreditCardParams");
    private final static QName _ListLocationsParams_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Parameters.CreditCardManagement",
            "ListLocationsParams");
    private final static QName _DeleteStoredCardParams_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Parameters.CreditCardManagement",
            "DeleteStoredCardParams");
    private final static QName _GetHostedCardVaultSubmissionInfoParams_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Parameters.CreditCardManagement",
            "GetHostedCardVaultSubmissionInfoParams");
    private final static QName _AddStoredCardParamsCustomerIdentifier_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Parameters.CreditCardManagement",
            "CustomerIdentifier");
    private final static QName _AddStoredCardParamsCreditCard_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Parameters.CreditCardManagement",
            "CreditCard");
    private final static QName _MoveCreditCardParamsNewCustomerIdentifier_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Parameters.CreditCardManagement",
            "NewCustomerIdentifier");
    private final static QName _MoveCreditCardParamsCurrentCustomerIdentifier_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Parameters.CreditCardManagement",
            "CurrentCustomerIdentifier");
    private final static QName _MoveCreditCardParamsToken_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Parameters.CreditCardManagement",
            "Token");
    private final static QName _RenameTokenParamsOriginalToken_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Parameters.CreditCardManagement",
            "OriginalToken");
    private final static QName _RenameTokenParamsNewToken_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Parameters.CreditCardManagement",
            "NewToken");
    private final static QName _RenameCustomerCodeParamsNewCode_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Parameters.CreditCardManagement",
            "NewCode");
    private final static QName _ShareCustomerParamsLocationToShare_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Parameters.CreditCardManagement",
            "LocationToShare");
    private final static QName _UpdateCustomerParamsCustomer_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Parameters.CreditCardManagement",
            "Customer");
    private final static QName _UpdateCustomerParamsLocationIdentifier_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Parameters.CreditCardManagement",
            "LocationIdentifier");
    private final static QName _GetTokenForCardNumberParamsAccountNumber_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Parameters.CreditCardManagement",
            "AccountNumber");
    private final static QName _GetHostedCardVaultSubmissionInfoParamsSubmissionId_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Parameters.CreditCardManagement",
            "SubmissionId");
    private final static QName _CreateHostedCardVaultSubmissionIdParamsOptionalCreditCardInformation_QNAME = new QName(
            "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Parameters.CreditCardManagement",
            "OptionalCreditCardInformation");

    /**
     * Create an instance of {@link AddStoredCardParams }
     * 
     */
    public AddStoredCardParams createAddStoredCardParams() {
        return new AddStoredCardParams();
    }

    /**
     * Create an instance of {@link UpdateStoredCardParams }
     * 
     */
    public UpdateStoredCardParams createUpdateStoredCardParams() {
        return new UpdateStoredCardParams();
    }

    /**
     * Create an instance of {@link GetCustomerParams }
     * 
     */
    public GetCustomerParams createGetCustomerParams() {
        return new GetCustomerParams();
    }

    /**
     * Create an instance of {@link MoveCreditCardParams }
     * 
     */
    public MoveCreditCardParams createMoveCreditCardParams() {
        return new MoveCreditCardParams();
    }

    /**
     * Create an instance of {@link RenameTokenParams }
     * 
     */
    public RenameTokenParams createRenameTokenParams() {
        return new RenameTokenParams();
    }

    /**
     * Create an instance of {@link RenameCustomerCodeParams }
     * 
     */
    public RenameCustomerCodeParams createRenameCustomerCodeParams() {
        return new RenameCustomerCodeParams();
    }

    /**
     * Create an instance of {@link ShareCustomerParams }
     * 
     */
    public ShareCustomerParams createShareCustomerParams() {
        return new ShareCustomerParams();
    }

    /**
     * Create an instance of {@link GetStoredCreditCardParams }
     * 
     */
    public GetStoredCreditCardParams createGetStoredCreditCardParams() {
        return new GetStoredCreditCardParams();
    }

    /**
     * Create an instance of {@link UpdateCustomerParams }
     * 
     */
    public UpdateCustomerParams createUpdateCustomerParams() {
        return new UpdateCustomerParams();
    }

    /**
     * Create an instance of {@link UpdateStoredCardExpirationDateParams }
     * 
     */
    public UpdateStoredCardExpirationDateParams createUpdateStoredCardExpirationDateParams() {
        return new UpdateStoredCardExpirationDateParams();
    }

    /**
     * Create an instance of {@link CreditCardManagementParams }
     * 
     */
    public CreditCardManagementParams createCreditCardManagementParams() {
        return new CreditCardManagementParams();
    }

    /**
     * Create an instance of {@link DeleteCustomerParams }
     * 
     */
    public DeleteCustomerParams createDeleteCustomerParams() {
        return new DeleteCustomerParams();
    }

    /**
     * Create an instance of {@link DeleteStoredCardParams }
     * 
     */
    public DeleteStoredCardParams createDeleteStoredCardParams() {
        return new DeleteStoredCardParams();
    }

    /**
     * Create an instance of {@link GetTokenForCardNumberParams }
     * 
     */
    public GetTokenForCardNumberParams createGetTokenForCardNumberParams() {
        return new GetTokenForCardNumberParams();
    }

    /**
     * Create an instance of {@link GetHostedCardVaultSubmissionInfoParams }
     * 
     */
    public GetHostedCardVaultSubmissionInfoParams createGetHostedCardVaultSubmissionInfoParams() {
        return new GetHostedCardVaultSubmissionInfoParams();
    }

    /**
     * Create an instance of {@link ListLocationsParams }
     * 
     */
    public ListLocationsParams createListLocationsParams() {
        return new ListLocationsParams();
    }

    /**
     * Create an instance of {@link CreateHostedCardVaultSubmissionIdParams }
     * 
     */
    public CreateHostedCardVaultSubmissionIdParams createCreateHostedCardVaultSubmissionIdParams() {
        return new CreateHostedCardVaultSubmissionIdParams();
    }

    /**
     * Create an instance of {@link CreateCustomerParams }
     * 
     */
    public CreateCustomerParams createCreateCustomerParams() {
        return new CreateCustomerParams();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateCustomerParams }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Parameters.CreditCardManagement", name = "CreateCustomerParams")
    public JAXBElement<CreateCustomerParams> createCreateCustomerParams(CreateCustomerParams value) {
        return new JAXBElement<CreateCustomerParams>(_CreateCustomerParams_QNAME, CreateCustomerParams.class, null,
                value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RenameTokenParams }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Parameters.CreditCardManagement", name = "RenameTokenParams")
    public JAXBElement<RenameTokenParams> createRenameTokenParams(RenameTokenParams value) {
        return new JAXBElement<RenameTokenParams>(_RenameTokenParams_QNAME, RenameTokenParams.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetTokenForCardNumberParams }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Parameters.CreditCardManagement", name = "GetTokenForCardNumberParams")
    public JAXBElement<GetTokenForCardNumberParams> createGetTokenForCardNumberParams(GetTokenForCardNumberParams value) {
        return new JAXBElement<GetTokenForCardNumberParams>(_GetTokenForCardNumberParams_QNAME,
                GetTokenForCardNumberParams.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateCustomerParams }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Parameters.CreditCardManagement", name = "UpdateCustomerParams")
    public JAXBElement<UpdateCustomerParams> createUpdateCustomerParams(UpdateCustomerParams value) {
        return new JAXBElement<UpdateCustomerParams>(_UpdateCustomerParams_QNAME, UpdateCustomerParams.class, null,
                value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreditCardManagementParams }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Parameters.CreditCardManagement", name = "CreditCardManagementParams")
    public JAXBElement<CreditCardManagementParams> createCreditCardManagementParams(CreditCardManagementParams value) {
        return new JAXBElement<CreditCardManagementParams>(_CreditCardManagementParams_QNAME,
                CreditCardManagementParams.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RenameCustomerCodeParams }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Parameters.CreditCardManagement", name = "RenameCustomerCodeParams")
    public JAXBElement<RenameCustomerCodeParams> createRenameCustomerCodeParams(RenameCustomerCodeParams value) {
        return new JAXBElement<RenameCustomerCodeParams>(_RenameCustomerCodeParams_QNAME,
                RenameCustomerCodeParams.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddStoredCardParams }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Parameters.CreditCardManagement", name = "AddStoredCardParams")
    public JAXBElement<AddStoredCardParams> createAddStoredCardParams(AddStoredCardParams value) {
        return new JAXBElement<AddStoredCardParams>(_AddStoredCardParams_QNAME, AddStoredCardParams.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MoveCreditCardParams }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Parameters.CreditCardManagement", name = "MoveCreditCardParams")
    public JAXBElement<MoveCreditCardParams> createMoveCreditCardParams(MoveCreditCardParams value) {
        return new JAXBElement<MoveCreditCardParams>(_MoveCreditCardParams_QNAME, MoveCreditCardParams.class, null,
                value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeleteCustomerParams }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Parameters.CreditCardManagement", name = "DeleteCustomerParams")
    public JAXBElement<DeleteCustomerParams> createDeleteCustomerParams(DeleteCustomerParams value) {
        return new JAXBElement<DeleteCustomerParams>(_DeleteCustomerParams_QNAME, DeleteCustomerParams.class, null,
                value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCustomerParams }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Parameters.CreditCardManagement", name = "GetCustomerParams")
    public JAXBElement<GetCustomerParams> createGetCustomerParams(GetCustomerParams value) {
        return new JAXBElement<GetCustomerParams>(_GetCustomerParams_QNAME, GetCustomerParams.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ShareCustomerParams }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Parameters.CreditCardManagement", name = "ShareCustomerParams")
    public JAXBElement<ShareCustomerParams> createShareCustomerParams(ShareCustomerParams value) {
        return new JAXBElement<ShareCustomerParams>(_ShareCustomerParams_QNAME, ShareCustomerParams.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateStoredCardParams }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Parameters.CreditCardManagement", name = "UpdateStoredCardParams")
    public JAXBElement<UpdateStoredCardParams> createUpdateStoredCardParams(UpdateStoredCardParams value) {
        return new JAXBElement<UpdateStoredCardParams>(_UpdateStoredCardParams_QNAME, UpdateStoredCardParams.class,
                null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateHostedCardVaultSubmissionIdParams }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Parameters.CreditCardManagement", name = "CreateHostedCardVaultSubmissionIdParams")
    public JAXBElement<CreateHostedCardVaultSubmissionIdParams> createCreateHostedCardVaultSubmissionIdParams(
            CreateHostedCardVaultSubmissionIdParams value) {
        return new JAXBElement<CreateHostedCardVaultSubmissionIdParams>(_CreateHostedCardVaultSubmissionIdParams_QNAME,
                CreateHostedCardVaultSubmissionIdParams.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateStoredCardExpirationDateParams }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Parameters.CreditCardManagement", name = "UpdateStoredCardExpirationDateParams")
    public JAXBElement<UpdateStoredCardExpirationDateParams> createUpdateStoredCardExpirationDateParams(
            UpdateStoredCardExpirationDateParams value) {
        return new JAXBElement<UpdateStoredCardExpirationDateParams>(_UpdateStoredCardExpirationDateParams_QNAME,
                UpdateStoredCardExpirationDateParams.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetStoredCreditCardParams }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Parameters.CreditCardManagement", name = "GetStoredCreditCardParams")
    public JAXBElement<GetStoredCreditCardParams> createGetStoredCreditCardParams(GetStoredCreditCardParams value) {
        return new JAXBElement<GetStoredCreditCardParams>(_GetStoredCreditCardParams_QNAME,
                GetStoredCreditCardParams.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListLocationsParams }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Parameters.CreditCardManagement", name = "ListLocationsParams")
    public JAXBElement<ListLocationsParams> createListLocationsParams(ListLocationsParams value) {
        return new JAXBElement<ListLocationsParams>(_ListLocationsParams_QNAME, ListLocationsParams.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeleteStoredCardParams }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Parameters.CreditCardManagement", name = "DeleteStoredCardParams")
    public JAXBElement<DeleteStoredCardParams> createDeleteStoredCardParams(DeleteStoredCardParams value) {
        return new JAXBElement<DeleteStoredCardParams>(_DeleteStoredCardParams_QNAME, DeleteStoredCardParams.class,
                null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetHostedCardVaultSubmissionInfoParams }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Parameters.CreditCardManagement", name = "GetHostedCardVaultSubmissionInfoParams")
    public JAXBElement<GetHostedCardVaultSubmissionInfoParams> createGetHostedCardVaultSubmissionInfoParams(
            GetHostedCardVaultSubmissionInfoParams value) {
        return new JAXBElement<GetHostedCardVaultSubmissionInfoParams>(_GetHostedCardVaultSubmissionInfoParams_QNAME,
                GetHostedCardVaultSubmissionInfoParams.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CustomerIdentifier }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Parameters.CreditCardManagement", name = "CustomerIdentifier", scope = AddStoredCardParams.class)
    public JAXBElement<CustomerIdentifier> createAddStoredCardParamsCustomerIdentifier(CustomerIdentifier value) {
        return new JAXBElement<CustomerIdentifier>(_AddStoredCardParamsCustomerIdentifier_QNAME,
                CustomerIdentifier.class, AddStoredCardParams.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link StoredCreditCard }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Parameters.CreditCardManagement", name = "CreditCard", scope = AddStoredCardParams.class)
    public JAXBElement<StoredCreditCard> createAddStoredCardParamsCreditCard(StoredCreditCard value) {
        return new JAXBElement<StoredCreditCard>(_AddStoredCardParamsCreditCard_QNAME, StoredCreditCard.class,
                AddStoredCardParams.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CustomerIdentifier }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Parameters.CreditCardManagement", name = "CustomerIdentifier", scope = UpdateStoredCardParams.class)
    public JAXBElement<CustomerIdentifier> createUpdateStoredCardParamsCustomerIdentifier(CustomerIdentifier value) {
        return new JAXBElement<CustomerIdentifier>(_AddStoredCardParamsCustomerIdentifier_QNAME,
                CustomerIdentifier.class, UpdateStoredCardParams.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link StoredCreditCard }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Parameters.CreditCardManagement", name = "CreditCard", scope = UpdateStoredCardParams.class)
    public JAXBElement<StoredCreditCard> createUpdateStoredCardParamsCreditCard(StoredCreditCard value) {
        return new JAXBElement<StoredCreditCard>(_AddStoredCardParamsCreditCard_QNAME, StoredCreditCard.class,
                UpdateStoredCardParams.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CustomerIdentifier }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Parameters.CreditCardManagement", name = "CustomerIdentifier", scope = GetCustomerParams.class)
    public JAXBElement<CustomerIdentifier> createGetCustomerParamsCustomerIdentifier(CustomerIdentifier value) {
        return new JAXBElement<CustomerIdentifier>(_AddStoredCardParamsCustomerIdentifier_QNAME,
                CustomerIdentifier.class, GetCustomerParams.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CustomerIdentifier }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Parameters.CreditCardManagement", name = "NewCustomerIdentifier", scope = MoveCreditCardParams.class)
    public JAXBElement<CustomerIdentifier> createMoveCreditCardParamsNewCustomerIdentifier(CustomerIdentifier value) {
        return new JAXBElement<CustomerIdentifier>(_MoveCreditCardParamsNewCustomerIdentifier_QNAME,
                CustomerIdentifier.class, MoveCreditCardParams.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CustomerIdentifier }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Parameters.CreditCardManagement", name = "CurrentCustomerIdentifier", scope = MoveCreditCardParams.class)
    public JAXBElement<CustomerIdentifier> createMoveCreditCardParamsCurrentCustomerIdentifier(CustomerIdentifier value) {
        return new JAXBElement<CustomerIdentifier>(_MoveCreditCardParamsCurrentCustomerIdentifier_QNAME,
                CustomerIdentifier.class, MoveCreditCardParams.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Parameters.CreditCardManagement", name = "Token", scope = MoveCreditCardParams.class)
    public JAXBElement<String> createMoveCreditCardParamsToken(String value) {
        return new JAXBElement<String>(_MoveCreditCardParamsToken_QNAME, String.class, MoveCreditCardParams.class,
                value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CustomerIdentifier }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Parameters.CreditCardManagement", name = "CustomerIdentifier", scope = RenameTokenParams.class)
    public JAXBElement<CustomerIdentifier> createRenameTokenParamsCustomerIdentifier(CustomerIdentifier value) {
        return new JAXBElement<CustomerIdentifier>(_AddStoredCardParamsCustomerIdentifier_QNAME,
                CustomerIdentifier.class, RenameTokenParams.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Parameters.CreditCardManagement", name = "OriginalToken", scope = RenameTokenParams.class)
    public JAXBElement<String> createRenameTokenParamsOriginalToken(String value) {
        return new JAXBElement<String>(_RenameTokenParamsOriginalToken_QNAME, String.class, RenameTokenParams.class,
                value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Parameters.CreditCardManagement", name = "NewToken", scope = RenameTokenParams.class)
    public JAXBElement<String> createRenameTokenParamsNewToken(String value) {
        return new JAXBElement<String>(_RenameTokenParamsNewToken_QNAME, String.class, RenameTokenParams.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Parameters.CreditCardManagement", name = "NewCode", scope = RenameCustomerCodeParams.class)
    public JAXBElement<String> createRenameCustomerCodeParamsNewCode(String value) {
        return new JAXBElement<String>(_RenameCustomerCodeParamsNewCode_QNAME, String.class,
                RenameCustomerCodeParams.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CustomerIdentifier }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Parameters.CreditCardManagement", name = "CustomerIdentifier", scope = RenameCustomerCodeParams.class)
    public JAXBElement<CustomerIdentifier> createRenameCustomerCodeParamsCustomerIdentifier(CustomerIdentifier value) {
        return new JAXBElement<CustomerIdentifier>(_AddStoredCardParamsCustomerIdentifier_QNAME,
                CustomerIdentifier.class, RenameCustomerCodeParams.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LocationIdentifier }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Parameters.CreditCardManagement", name = "LocationToShare", scope = ShareCustomerParams.class)
    public JAXBElement<LocationIdentifier> createShareCustomerParamsLocationToShare(LocationIdentifier value) {
        return new JAXBElement<LocationIdentifier>(_ShareCustomerParamsLocationToShare_QNAME, LocationIdentifier.class,
                ShareCustomerParams.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CustomerIdentifier }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Parameters.CreditCardManagement", name = "CustomerIdentifier", scope = ShareCustomerParams.class)
    public JAXBElement<CustomerIdentifier> createShareCustomerParamsCustomerIdentifier(CustomerIdentifier value) {
        return new JAXBElement<CustomerIdentifier>(_AddStoredCardParamsCustomerIdentifier_QNAME,
                CustomerIdentifier.class, ShareCustomerParams.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CustomerIdentifier }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Parameters.CreditCardManagement", name = "CustomerIdentifier", scope = GetStoredCreditCardParams.class)
    public JAXBElement<CustomerIdentifier> createGetStoredCreditCardParamsCustomerIdentifier(CustomerIdentifier value) {
        return new JAXBElement<CustomerIdentifier>(_AddStoredCardParamsCustomerIdentifier_QNAME,
                CustomerIdentifier.class, GetStoredCreditCardParams.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Parameters.CreditCardManagement", name = "Token", scope = GetStoredCreditCardParams.class)
    public JAXBElement<String> createGetStoredCreditCardParamsToken(String value) {
        return new JAXBElement<String>(_MoveCreditCardParamsToken_QNAME, String.class, GetStoredCreditCardParams.class,
                value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Customer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Parameters.CreditCardManagement", name = "Customer", scope = UpdateCustomerParams.class)
    public JAXBElement<Customer> createUpdateCustomerParamsCustomer(Customer value) {
        return new JAXBElement<Customer>(_UpdateCustomerParamsCustomer_QNAME, Customer.class,
                UpdateCustomerParams.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LocationIdentifier }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Parameters.CreditCardManagement", name = "LocationIdentifier", scope = UpdateCustomerParams.class)
    public JAXBElement<LocationIdentifier> createUpdateCustomerParamsLocationIdentifier(LocationIdentifier value) {
        return new JAXBElement<LocationIdentifier>(_UpdateCustomerParamsLocationIdentifier_QNAME,
                LocationIdentifier.class, UpdateCustomerParams.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CustomerIdentifier }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Parameters.CreditCardManagement", name = "CustomerIdentifier", scope = UpdateStoredCardExpirationDateParams.class)
    public JAXBElement<CustomerIdentifier> createUpdateStoredCardExpirationDateParamsCustomerIdentifier(
            CustomerIdentifier value) {
        return new JAXBElement<CustomerIdentifier>(_AddStoredCardParamsCustomerIdentifier_QNAME,
                CustomerIdentifier.class, UpdateStoredCardExpirationDateParams.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Parameters.CreditCardManagement", name = "Token", scope = UpdateStoredCardExpirationDateParams.class)
    public JAXBElement<String> createUpdateStoredCardExpirationDateParamsToken(String value) {
        return new JAXBElement<String>(_MoveCreditCardParamsToken_QNAME, String.class,
                UpdateStoredCardExpirationDateParams.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CustomerIdentifier }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Parameters.CreditCardManagement", name = "CustomerIdentifier", scope = DeleteCustomerParams.class)
    public JAXBElement<CustomerIdentifier> createDeleteCustomerParamsCustomerIdentifier(CustomerIdentifier value) {
        return new JAXBElement<CustomerIdentifier>(_AddStoredCardParamsCustomerIdentifier_QNAME,
                CustomerIdentifier.class, DeleteCustomerParams.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CustomerIdentifier }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Parameters.CreditCardManagement", name = "CustomerIdentifier", scope = DeleteStoredCardParams.class)
    public JAXBElement<CustomerIdentifier> createDeleteStoredCardParamsCustomerIdentifier(CustomerIdentifier value) {
        return new JAXBElement<CustomerIdentifier>(_AddStoredCardParamsCustomerIdentifier_QNAME,
                CustomerIdentifier.class, DeleteStoredCardParams.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Parameters.CreditCardManagement", name = "Token", scope = DeleteStoredCardParams.class)
    public JAXBElement<String> createDeleteStoredCardParamsToken(String value) {
        return new JAXBElement<String>(_MoveCreditCardParamsToken_QNAME, String.class, DeleteStoredCardParams.class,
                value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CustomerIdentifier }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Parameters.CreditCardManagement", name = "CustomerIdentifier", scope = GetTokenForCardNumberParams.class)
    public JAXBElement<CustomerIdentifier> createGetTokenForCardNumberParamsCustomerIdentifier(CustomerIdentifier value) {
        return new JAXBElement<CustomerIdentifier>(_AddStoredCardParamsCustomerIdentifier_QNAME,
                CustomerIdentifier.class, GetTokenForCardNumberParams.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Parameters.CreditCardManagement", name = "AccountNumber", scope = GetTokenForCardNumberParams.class)
    public JAXBElement<String> createGetTokenForCardNumberParamsAccountNumber(String value) {
        return new JAXBElement<String>(_GetTokenForCardNumberParamsAccountNumber_QNAME, String.class,
                GetTokenForCardNumberParams.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Parameters.CreditCardManagement", name = "SubmissionId", scope = GetHostedCardVaultSubmissionInfoParams.class)
    public JAXBElement<String> createGetHostedCardVaultSubmissionInfoParamsSubmissionId(String value) {
        return new JAXBElement<String>(_GetHostedCardVaultSubmissionInfoParamsSubmissionId_QNAME, String.class,
                GetHostedCardVaultSubmissionInfoParams.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CustomerIdentifier }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Parameters.CreditCardManagement", name = "CustomerIdentifier", scope = ListLocationsParams.class)
    public JAXBElement<CustomerIdentifier> createListLocationsParamsCustomerIdentifier(CustomerIdentifier value) {
        return new JAXBElement<CustomerIdentifier>(_AddStoredCardParamsCustomerIdentifier_QNAME,
                CustomerIdentifier.class, ListLocationsParams.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OptionalCreditCardInformation }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Parameters.CreditCardManagement", name = "OptionalCreditCardInformation", scope = CreateHostedCardVaultSubmissionIdParams.class)
    public JAXBElement<OptionalCreditCardInformation> createCreateHostedCardVaultSubmissionIdParamsOptionalCreditCardInformation(
            OptionalCreditCardInformation value) {
        return new JAXBElement<OptionalCreditCardInformation>(
                _CreateHostedCardVaultSubmissionIdParamsOptionalCreditCardInformation_QNAME,
                OptionalCreditCardInformation.class, CreateHostedCardVaultSubmissionIdParams.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CustomerIdentifier }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Parameters.CreditCardManagement", name = "CustomerIdentifier", scope = CreateHostedCardVaultSubmissionIdParams.class)
    public JAXBElement<CustomerIdentifier> createCreateHostedCardVaultSubmissionIdParamsCustomerIdentifier(
            CustomerIdentifier value) {
        return new JAXBElement<CustomerIdentifier>(_AddStoredCardParamsCustomerIdentifier_QNAME,
                CustomerIdentifier.class, CreateHostedCardVaultSubmissionIdParams.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Customer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Parameters.CreditCardManagement", name = "Customer", scope = CreateCustomerParams.class)
    public JAXBElement<Customer> createCreateCustomerParamsCustomer(Customer value) {
        return new JAXBElement<Customer>(_UpdateCustomerParamsCustomer_QNAME, Customer.class,
                CreateCustomerParams.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LocationIdentifier }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Parameters.CreditCardManagement", name = "LocationIdentifier", scope = CreateCustomerParams.class)
    public JAXBElement<LocationIdentifier> createCreateCustomerParamsLocationIdentifier(LocationIdentifier value) {
        return new JAXBElement<LocationIdentifier>(_UpdateCustomerParamsLocationIdentifier_QNAME,
                LocationIdentifier.class, CreateCustomerParams.class, value);
    }

}
