/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.service.payment.utilities;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PaymentGatewayPropertiesLoader {

    private static Logger logger = LoggerFactory.getLogger(PaymentGatewayPropertiesLoader.class);
    private PropertiesConfiguration paymentGatewayProperties;
    private static final PaymentGatewayPropertiesLoader singleton = new PaymentGatewayPropertiesLoader();

    public static PaymentGatewayPropertiesLoader getInstance() {
        return singleton;
    }

    private PaymentGatewayPropertiesLoader() {
        String propertyHome = System.getProperty("phoenix.config.home");
        
        try(InputStream is = new FileInputStream(propertyHome + "/paymentGateway.properties")) {
            paymentGatewayProperties = new PropertiesConfiguration();
            paymentGatewayProperties.load(is);
        } catch (ConfigurationException | IOException ex) {
            logger.error("Could not load \"paymentGateway.properties\": ", ex);
        }
    }

    public PropertiesConfiguration getProperties() {
        return paymentGatewayProperties;
    }
}
