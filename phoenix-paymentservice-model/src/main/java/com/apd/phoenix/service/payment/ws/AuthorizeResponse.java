package com.apd.phoenix.service.payment.ws;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for anonymous complex type.
 *
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 *
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AuthorizeResult" type="{http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardTransactionDefinitions}CreditCardTransactionAuthorizationResult" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "authorizeResult" })
@XmlRootElement(name = "AuthorizeResponse")
public class AuthorizeResponse {

    @XmlElementRef(name = "AuthorizeResult", namespace = "http://3DSI.org/WebServices/CreditCardTransaction", type = JAXBElement.class, required = false)
    protected JAXBElement<CreditCardTransactionAuthorizationResult> authorizeResult;

    /**
     * Gets the value of the authorizeResult property.
     *
     * @return possible object is
     * {@link JAXBElement }{@code <}{@link CreditCardTransactionAuthorizationResult }{@code >}
     *
     */
    public JAXBElement<CreditCardTransactionAuthorizationResult> getAuthorizeResult() {
        return authorizeResult;
    }

    /**
     * Sets the value of the authorizeResult property.
     *
     * @param value allowed object is
     * {@link JAXBElement }{@code <}{@link CreditCardTransactionAuthorizationResult }{@code >}
     *
     */
    public void setAuthorizeResult(JAXBElement<CreditCardTransactionAuthorizationResult> value) {
        this.authorizeResult = value;
    }
}
