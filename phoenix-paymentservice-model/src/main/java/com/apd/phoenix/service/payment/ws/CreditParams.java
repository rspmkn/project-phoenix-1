package com.apd.phoenix.service.payment.ws;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for CreditParams complex type.
 *
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 *
 * <pre>
 * &lt;complexType name="CreditParams">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AddOrUpdateCard" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="CreditCardTransaction" type="{http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardTransactionDefinitions}CreditCardTransaction" minOccurs="0"/>
 *         &lt;element name="TerminalIdentifier" type="{http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions}TerminalIdentifier" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CreditParams", namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Parameters.CreditCardTransactionParameters", propOrder = {
        "addOrUpdateCard", "creditCardTransaction", "terminalIdentifier" })
public class CreditParams {

    @XmlElement(name = "AddOrUpdateCard")
    protected Boolean addOrUpdateCard;
    @XmlElementRef(name = "CreditCardTransaction", namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Parameters.CreditCardTransactionParameters", type = JAXBElement.class, required = false)
    protected JAXBElement<CreditCardTransaction> creditCardTransaction;
    @XmlElementRef(name = "TerminalIdentifier", namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Parameters.CreditCardTransactionParameters", type = JAXBElement.class, required = false)
    protected JAXBElement<TerminalIdentifier> terminalIdentifier;

    /**
     * Gets the value of the addOrUpdateCard property.
     *
     * @return possible object is {@link Boolean }
     *
     */
    public Boolean isAddOrUpdateCard() {
        return addOrUpdateCard;
    }

    /**
     * Sets the value of the addOrUpdateCard property.
     *
     * @param value allowed object is {@link Boolean }
     *
     */
    public void setAddOrUpdateCard(Boolean value) {
        this.addOrUpdateCard = value;
    }

    /**
     * Gets the value of the creditCardTransaction property.
     *
     * @return possible object is
     * {@link JAXBElement }{@code <}{@link CreditCardTransaction }{@code >}
     *
     */
    public JAXBElement<CreditCardTransaction> getCreditCardTransaction() {
        return creditCardTransaction;
    }

    /**
     * Sets the value of the creditCardTransaction property.
     *
     * @param value allowed object is
     * {@link JAXBElement }{@code <}{@link CreditCardTransaction }{@code >}
     *
     */
    public void setCreditCardTransaction(JAXBElement<CreditCardTransaction> value) {
        this.creditCardTransaction = value;
    }

    /**
     * Gets the value of the terminalIdentifier property.
     *
     * @return possible object is
     * {@link JAXBElement }{@code <}{@link TerminalIdentifier }{@code >}
     *
     */
    public JAXBElement<TerminalIdentifier> getTerminalIdentifier() {
        return terminalIdentifier;
    }

    /**
     * Sets the value of the terminalIdentifier property.
     *
     * @param value allowed object is
     * {@link JAXBElement }{@code <}{@link TerminalIdentifier }{@code >}
     *
     */
    public void setTerminalIdentifier(JAXBElement<TerminalIdentifier> value) {
        this.terminalIdentifier = value;
    }
}
