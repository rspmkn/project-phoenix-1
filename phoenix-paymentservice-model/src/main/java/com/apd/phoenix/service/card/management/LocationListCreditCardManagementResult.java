package com.apd.phoenix.service.card.management;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>Java class for LocationListCreditCardManagementResult complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="LocationListCreditCardManagementResult">
 *   &lt;complexContent>
 *     &lt;extension base="{http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardManagementDefinitions}CreditCardManagementResult">
 *       &lt;sequence>
 *         &lt;element name="LocationCodes" type="{http://schemas.microsoft.com/2003/10/Serialization/Arrays}ArrayOfstring" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LocationListCreditCardManagementResult", namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardManagementDefinitions", propOrder = { "locationCodes" })
public class LocationListCreditCardManagementResult extends CreditCardManagementResult {

    @XmlElementRef(name = "LocationCodes", namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardManagementDefinitions", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfstring> locationCodes;

    /**
     * Gets the value of the locationCodes property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfstring }{@code >}
     *     
     */
    public JAXBElement<ArrayOfstring> getLocationCodes() {
        return locationCodes;
    }

    /**
     * Sets the value of the locationCodes property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfstring }{@code >}
     *     
     */
    public void setLocationCodes(JAXBElement<ArrayOfstring> value) {
        this.locationCodes = ((JAXBElement<ArrayOfstring>) value);
    }

}
