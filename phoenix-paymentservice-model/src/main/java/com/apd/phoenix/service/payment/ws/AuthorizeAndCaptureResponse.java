package com.apd.phoenix.service.payment.ws;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for anonymous complex type.
 *
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 *
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AuthorizeAndCaptureResult" type="{http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardTransactionDefinitions}CreditCardTransactionAuthorizationResult" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "authorizeAndCaptureResult" })
@XmlRootElement(name = "AuthorizeAndCaptureResponse")
public class AuthorizeAndCaptureResponse {

    @XmlElementRef(name = "AuthorizeAndCaptureResult", namespace = "http://3DSI.org/WebServices/CreditCardTransaction", type = JAXBElement.class, required = false)
    protected JAXBElement<CreditCardTransactionAuthorizationResult> authorizeAndCaptureResult;

    /**
     * Gets the value of the authorizeAndCaptureResult property.
     *
     * @return possible object is
     * {@link JAXBElement }{@code <}{@link CreditCardTransactionAuthorizationResult }{@code >}
     *
     */
    public JAXBElement<CreditCardTransactionAuthorizationResult> getAuthorizeAndCaptureResult() {
        return authorizeAndCaptureResult;
    }

    /**
     * Sets the value of the authorizeAndCaptureResult property.
     *
     * @param value allowed object is
     * {@link JAXBElement }{@code <}{@link CreditCardTransactionAuthorizationResult }{@code >}
     *
     */
    public void setAuthorizeAndCaptureResult(JAXBElement<CreditCardTransactionAuthorizationResult> value) {
        this.authorizeAndCaptureResult = value;
    }
}
