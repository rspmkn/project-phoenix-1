package com.apd.phoenix.service.card.management;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>Java class for HostedCardVaultSubmissionStatus.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="HostedCardVaultSubmissionStatus">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="NotSubmitted"/>
 *     &lt;enumeration value="Submitted"/>
 *     &lt;enumeration value="NotFound"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "HostedCardVaultSubmissionStatus", namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Common.Enums")
@XmlEnum
public enum HostedCardVaultSubmissionStatus {

    @XmlEnumValue("NotSubmitted")
    NOT_SUBMITTED("NotSubmitted"), @XmlEnumValue("Submitted")
    SUBMITTED("Submitted"), @XmlEnumValue("NotFound")
    NOT_FOUND("NotFound");

    private final String value;

    HostedCardVaultSubmissionStatus(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static HostedCardVaultSubmissionStatus fromValue(String v) {
        for (HostedCardVaultSubmissionStatus c : HostedCardVaultSubmissionStatus.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
