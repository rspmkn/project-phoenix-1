package com.apd.phoenix.service.payment.ws;

import java.math.BigDecimal;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for CreditCardTransactionResult complex type.
 *
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 *
 * <pre>
 * &lt;complexType name="CreditCardTransactionResult">
 *   &lt;complexContent>
 *     &lt;extension base="{http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions}WebServiceResult">
 *       &lt;sequence>
 *         &lt;element name="FailureReason" type="{http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Enums}FailureReason" minOccurs="0"/>
 *         &lt;element name="ProcessorResponse" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RuleInfringements" type="{http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions}ArrayOfRuleInfringement" minOccurs="0"/>
 *         &lt;element name="ThirdPartyResponse" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Token" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TotalAmount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="TransactionKey" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TransactionWarning" type="{http://schemas.datacontract.org/2004/07/ThreeDelta.Common.Enums}ArrayOfTransactionWarning" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CreditCardTransactionResult", namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardTransactionDefinitions", propOrder = {
        "failureReason", "processorResponse", "ruleInfringements", "thirdPartyResponse", "token", "totalAmount",
        "transactionKey", "transactionWarning" })
@XmlSeeAlso( { CreditCardTransactionVerificationResult.class })
public class CreditCardTransactionResult extends WebServiceResult {

    @XmlElement(name = "FailureReason")
    protected FailureReason failureReason;
    @XmlElementRef(name = "ProcessorResponse", namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardTransactionDefinitions", type = JAXBElement.class, required = false)
    protected JAXBElement<String> processorResponse;
    @XmlElementRef(name = "RuleInfringements", namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardTransactionDefinitions", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfRuleInfringement> ruleInfringements;
    @XmlElementRef(name = "ThirdPartyResponse", namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardTransactionDefinitions", type = JAXBElement.class, required = false)
    protected JAXBElement<String> thirdPartyResponse;
    @XmlElementRef(name = "Token", namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardTransactionDefinitions", type = JAXBElement.class, required = false)
    protected JAXBElement<String> token;
    @XmlElement(name = "TotalAmount")
    protected BigDecimal totalAmount;
    @XmlElementRef(name = "TransactionKey", namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardTransactionDefinitions", type = JAXBElement.class, required = false)
    protected JAXBElement<String> transactionKey;
    @XmlElementRef(name = "TransactionWarning", namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardTransactionDefinitions", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfTransactionWarning> transactionWarning;

    /**
     * Gets the value of the failureReason property.
     *
     * @return possible object is {@link FailureReason }
     *
     */
    public FailureReason getFailureReason() {
        return failureReason;
    }

    /**
     * Sets the value of the failureReason property.
     *
     * @param value allowed object is {@link FailureReason }
     *
     */
    public void setFailureReason(FailureReason value) {
        this.failureReason = value;
    }

    /**
     * Gets the value of the processorResponse property.
     *
     * @return possible object is
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     *
     */
    public JAXBElement<String> getProcessorResponse() {
        return processorResponse;
    }

    /**
     * Sets the value of the processorResponse property.
     *
     * @param value allowed object is
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     *
     */
    public void setProcessorResponse(JAXBElement<String> value) {
        this.processorResponse = value;
    }

    /**
     * Gets the value of the ruleInfringements property.
     *
     * @return possible object is
     * {@link JAXBElement }{@code <}{@link ArrayOfRuleInfringement }{@code >}
     *
     */
    public JAXBElement<ArrayOfRuleInfringement> getRuleInfringements() {
        return ruleInfringements;
    }

    /**
     * Sets the value of the ruleInfringements property.
     *
     * @param value allowed object is
     * {@link JAXBElement }{@code <}{@link ArrayOfRuleInfringement }{@code >}
     *
     */
    public void setRuleInfringements(JAXBElement<ArrayOfRuleInfringement> value) {
        this.ruleInfringements = value;
    }

    /**
     * Gets the value of the thirdPartyResponse property.
     *
     * @return possible object is
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     *
     */
    public JAXBElement<String> getThirdPartyResponse() {
        return thirdPartyResponse;
    }

    /**
     * Sets the value of the thirdPartyResponse property.
     *
     * @param value allowed object is
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     *
     */
    public void setThirdPartyResponse(JAXBElement<String> value) {
        this.thirdPartyResponse = value;
    }

    /**
     * Gets the value of the token property.
     *
     * @return possible object is
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     *
     */
    public JAXBElement<String> getToken() {
        return token;
    }

    /**
     * Sets the value of the token property.
     *
     * @param value allowed object is
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     *
     */
    public void setToken(JAXBElement<String> value) {
        this.token = value;
    }

    /**
     * Gets the value of the totalAmount property.
     *
     * @return possible object is {@link BigDecimal }
     *
     */
    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    /**
     * Sets the value of the totalAmount property.
     *
     * @param value allowed object is {@link BigDecimal }
     *
     */
    public void setTotalAmount(BigDecimal value) {
        this.totalAmount = value;
    }

    /**
     * Gets the value of the transactionKey property.
     *
     * @return possible object is
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     *
     */
    public JAXBElement<String> getTransactionKey() {
        return transactionKey;
    }

    /**
     * Sets the value of the transactionKey property.
     *
     * @param value allowed object is
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     *
     */
    public void setTransactionKey(JAXBElement<String> value) {
        this.transactionKey = value;
    }

    /**
     * Gets the value of the transactionWarning property.
     *
     * @return possible object is
     * {@link JAXBElement }{@code <}{@link ArrayOfTransactionWarning }{@code >}
     *
     */
    public JAXBElement<ArrayOfTransactionWarning> getTransactionWarning() {
        return transactionWarning;
    }

    /**
     * Sets the value of the transactionWarning property.
     *
     * @param value allowed object is
     * {@link JAXBElement }{@code <}{@link ArrayOfTransactionWarning }{@code >}
     *
     */
    public void setTransactionWarning(JAXBElement<ArrayOfTransactionWarning> value) {
        this.transactionWarning = value;
    }
}
