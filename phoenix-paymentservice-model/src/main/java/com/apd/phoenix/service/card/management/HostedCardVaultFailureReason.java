package com.apd.phoenix.service.card.management;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>Java class for HostedCardVaultFailureReason.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="HostedCardVaultFailureReason">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="None"/>
 *     &lt;enumeration value="CustomerDoesNotExist"/>
 *     &lt;enumeration value="ValidationError"/>
 *     &lt;enumeration value="CardNumberInUse"/>
 *     &lt;enumeration value="TokenInUse"/>
 *     &lt;enumeration value="UnexpectedFailure"/>
 *     &lt;enumeration value="PermissionDenied"/>
 *     &lt;enumeration value="InitialGetRequestTooLate"/>
 *     &lt;enumeration value="HostedPageSubmittedTooLate"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "HostedCardVaultFailureReason", namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Common.Enums")
@XmlEnum
public enum HostedCardVaultFailureReason {

    @XmlEnumValue("None")
    NONE("None"), @XmlEnumValue("CustomerDoesNotExist")
    CUSTOMER_DOES_NOT_EXIST("CustomerDoesNotExist"), @XmlEnumValue("ValidationError")
    VALIDATION_ERROR("ValidationError"), @XmlEnumValue("CardNumberInUse")
    CARD_NUMBER_IN_USE("CardNumberInUse"), @XmlEnumValue("TokenInUse")
    TOKEN_IN_USE("TokenInUse"), @XmlEnumValue("UnexpectedFailure")
    UNEXPECTED_FAILURE("UnexpectedFailure"), @XmlEnumValue("PermissionDenied")
    PERMISSION_DENIED("PermissionDenied"), @XmlEnumValue("InitialGetRequestTooLate")
    INITIAL_GET_REQUEST_TOO_LATE("InitialGetRequestTooLate"), @XmlEnumValue("HostedPageSubmittedTooLate")
    HOSTED_PAGE_SUBMITTED_TOO_LATE("HostedPageSubmittedTooLate");

    private final String value;

    HostedCardVaultFailureReason(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static HostedCardVaultFailureReason fromValue(String v) {
        for (HostedCardVaultFailureReason c : HostedCardVaultFailureReason.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
