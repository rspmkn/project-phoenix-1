package com.apd.phoenix.service.payment.ws;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for AdditionalAmounts complex type.
 *
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 *
 * <pre>
 * &lt;complexType name="AdditionalAmounts">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CanadianTaxGST" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="CanadianTaxHST" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="CanadianTaxPST" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="CanadianTaxQST" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="DiscountAmount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="DutyAmount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="FreightAmount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="FreightTaxAmount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="SalesTaxAmount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AdditionalAmounts", namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardTransactionDefinitions", propOrder = {
        "canadianTaxGST", "canadianTaxHST", "canadianTaxPST", "canadianTaxQST", "discountAmount", "dutyAmount",
        "freightAmount", "freightTaxAmount", "salesTaxAmount" })
public class AdditionalAmounts {

    @XmlElement(name = "CanadianTaxGST")
    protected BigDecimal canadianTaxGST;
    @XmlElement(name = "CanadianTaxHST")
    protected BigDecimal canadianTaxHST;
    @XmlElement(name = "CanadianTaxPST")
    protected BigDecimal canadianTaxPST;
    @XmlElement(name = "CanadianTaxQST")
    protected BigDecimal canadianTaxQST;
    @XmlElement(name = "DiscountAmount")
    protected BigDecimal discountAmount;
    @XmlElement(name = "DutyAmount")
    protected BigDecimal dutyAmount;
    @XmlElement(name = "FreightAmount")
    protected BigDecimal freightAmount;
    @XmlElement(name = "FreightTaxAmount")
    protected BigDecimal freightTaxAmount;
    @XmlElement(name = "SalesTaxAmount")
    protected BigDecimal salesTaxAmount;

    /**
     * Gets the value of the canadianTaxGST property.
     *
     * @return possible object is {@link BigDecimal }
     *
     */
    public BigDecimal getCanadianTaxGST() {
        return canadianTaxGST;
    }

    /**
     * Sets the value of the canadianTaxGST property.
     *
     * @param value allowed object is {@link BigDecimal }
     *
     */
    public void setCanadianTaxGST(BigDecimal value) {
        this.canadianTaxGST = value;
    }

    /**
     * Gets the value of the canadianTaxHST property.
     *
     * @return possible object is {@link BigDecimal }
     *
     */
    public BigDecimal getCanadianTaxHST() {
        return canadianTaxHST;
    }

    /**
     * Sets the value of the canadianTaxHST property.
     *
     * @param value allowed object is {@link BigDecimal }
     *
     */
    public void setCanadianTaxHST(BigDecimal value) {
        this.canadianTaxHST = value;
    }

    /**
     * Gets the value of the canadianTaxPST property.
     *
     * @return possible object is {@link BigDecimal }
     *
     */
    public BigDecimal getCanadianTaxPST() {
        return canadianTaxPST;
    }

    /**
     * Sets the value of the canadianTaxPST property.
     *
     * @param value allowed object is {@link BigDecimal }
     *
     */
    public void setCanadianTaxPST(BigDecimal value) {
        this.canadianTaxPST = value;
    }

    /**
     * Gets the value of the canadianTaxQST property.
     *
     * @return possible object is {@link BigDecimal }
     *
     */
    public BigDecimal getCanadianTaxQST() {
        return canadianTaxQST;
    }

    /**
     * Sets the value of the canadianTaxQST property.
     *
     * @param value allowed object is {@link BigDecimal }
     *
     */
    public void setCanadianTaxQST(BigDecimal value) {
        this.canadianTaxQST = value;
    }

    /**
     * Gets the value of the discountAmount property.
     *
     * @return possible object is {@link BigDecimal }
     *
     */
    public BigDecimal getDiscountAmount() {
        return discountAmount;
    }

    /**
     * Sets the value of the discountAmount property.
     *
     * @param value allowed object is {@link BigDecimal }
     *
     */
    public void setDiscountAmount(BigDecimal value) {
        this.discountAmount = value;
    }

    /**
     * Gets the value of the dutyAmount property.
     *
     * @return possible object is {@link BigDecimal }
     *
     */
    public BigDecimal getDutyAmount() {
        return dutyAmount;
    }

    /**
     * Sets the value of the dutyAmount property.
     *
     * @param value allowed object is {@link BigDecimal }
     *
     */
    public void setDutyAmount(BigDecimal value) {
        this.dutyAmount = value;
    }

    /**
     * Gets the value of the freightAmount property.
     *
     * @return possible object is {@link BigDecimal }
     *
     */
    public BigDecimal getFreightAmount() {
        return freightAmount;
    }

    /**
     * Sets the value of the freightAmount property.
     *
     * @param value allowed object is {@link BigDecimal }
     *
     */
    public void setFreightAmount(BigDecimal value) {
        this.freightAmount = value;
    }

    /**
     * Gets the value of the freightTaxAmount property.
     *
     * @return possible object is {@link BigDecimal }
     *
     */
    public BigDecimal getFreightTaxAmount() {
        return freightTaxAmount;
    }

    /**
     * Sets the value of the freightTaxAmount property.
     *
     * @param value allowed object is {@link BigDecimal }
     *
     */
    public void setFreightTaxAmount(BigDecimal value) {
        this.freightTaxAmount = value;
    }

    /**
     * Gets the value of the salesTaxAmount property.
     *
     * @return possible object is {@link BigDecimal }
     *
     */
    public BigDecimal getSalesTaxAmount() {
        return salesTaxAmount;
    }

    /**
     * Sets the value of the salesTaxAmount property.
     *
     * @param value allowed object is {@link BigDecimal }
     *
     */
    public void setSalesTaxAmount(BigDecimal value) {
        this.salesTaxAmount = value;
    }
}
