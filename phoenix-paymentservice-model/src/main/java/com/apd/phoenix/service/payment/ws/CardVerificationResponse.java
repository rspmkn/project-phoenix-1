package com.apd.phoenix.service.payment.ws;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for anonymous complex type.
 *
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 *
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CardVerificationResult" type="{http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardTransactionDefinitions}CreditCardTransactionVerificationResult" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "cardVerificationResult" })
@XmlRootElement(name = "CardVerificationResponse")
public class CardVerificationResponse {

    @XmlElementRef(name = "CardVerificationResult", namespace = "http://3DSI.org/WebServices/CreditCardTransaction", type = JAXBElement.class, required = false)
    protected JAXBElement<CreditCardTransactionVerificationResult> cardVerificationResult;

    /**
     * Gets the value of the cardVerificationResult property.
     *
     * @return possible object is
     * {@link JAXBElement }{@code <}{@link CreditCardTransactionVerificationResult }{@code >}
     *
     */
    public JAXBElement<CreditCardTransactionVerificationResult> getCardVerificationResult() {
        return cardVerificationResult;
    }

    /**
     * Sets the value of the cardVerificationResult property.
     *
     * @param value allowed object is
     * {@link JAXBElement }{@code <}{@link CreditCardTransactionVerificationResult }{@code >}
     *
     */
    public void setCardVerificationResult(JAXBElement<CreditCardTransactionVerificationResult> value) {
        this.cardVerificationResult = value;
    }
}
