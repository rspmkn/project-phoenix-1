package com.apd.phoenix.service.card.management;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RenameCustomerCodeResult" type="{http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardManagementDefinitions}CreditCardManagementResult" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "renameCustomerCodeResult" })
@XmlRootElement(name = "RenameCustomerCodeResponse")
public class RenameCustomerCodeResponse {

    @XmlElementRef(name = "RenameCustomerCodeResult", namespace = "http://3DSI.org/WebServices/CreditCardManagement", type = JAXBElement.class, required = false)
    protected JAXBElement<CreditCardManagementResult> renameCustomerCodeResult;

    /**
     * Gets the value of the renameCustomerCodeResult property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link CreditCardManagementResult }{@code >}
     *     
     */
    public JAXBElement<CreditCardManagementResult> getRenameCustomerCodeResult() {
        return renameCustomerCodeResult;
    }

    /**
     * Sets the value of the renameCustomerCodeResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link CreditCardManagementResult }{@code >}
     *     
     */
    public void setRenameCustomerCodeResult(JAXBElement<CreditCardManagementResult> value) {
        this.renameCustomerCodeResult = ((JAXBElement<CreditCardManagementResult>) value);
    }

}
