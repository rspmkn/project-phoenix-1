package com.apd.phoenix.service.card.management;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>Java class for CreditCardManagementParams complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CreditCardManagementParams">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CreditCardManagementParams", namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Parameters.CreditCardManagement")
@XmlSeeAlso( { ShareCustomerParams.class, DeleteCustomerParams.class, GetCustomerParams.class, RenameTokenParams.class,
        ListLocationsParams.class, GetStoredCreditCardParams.class, DeleteStoredCardParams.class,
        GetTokenForCardNumberParams.class, UpdateCustomerParams.class, CreateHostedCardVaultSubmissionIdParams.class,
        AddStoredCardParams.class, GetHostedCardVaultSubmissionInfoParams.class, UpdateStoredCardParams.class,
        MoveCreditCardParams.class, RenameCustomerCodeParams.class, UpdateStoredCardExpirationDateParams.class,
        CreateCustomerParams.class })
public class CreditCardManagementParams {

}
