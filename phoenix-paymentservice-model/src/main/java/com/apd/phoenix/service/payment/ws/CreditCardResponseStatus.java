package com.apd.phoenix.service.payment.ws;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for CreditCardResponseStatus.
 *
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * <p>
 * <
 * pre>
 * &lt;simpleType name="CreditCardResponseStatus"> &lt;restriction
 * base="{http://www.w3.org/2001/XMLSchema}string"> &lt;enumeration
 * value="None"/> &lt;enumeration value="Approved"/> &lt;enumeration
 * value="Declined"/> &lt;enumeration value="Call"/> &lt;enumeration
 * value="PickupCard"/> &lt;enumeration value="CardSecurityCodeIncorrect"/>
 * &lt;enumeration value="ExpirationDateIncorrect"/> &lt;enumeration
 * value="ProcessorRetry"/> &lt;enumeration value="ProcessorError"/>
 * &lt;/restriction> &lt;/simpleType>
 * </pre>
 *
 */
@XmlType(name = "CreditCardResponseStatus", namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Common.Enums")
@XmlEnum
public enum CreditCardResponseStatus {

    @XmlEnumValue("None")
    NONE("None"), @XmlEnumValue("Approved")
    APPROVED("Approved"), @XmlEnumValue("Declined")
    DECLINED("Declined"), @XmlEnumValue("Call")
    CALL("Call"), @XmlEnumValue("PickupCard")
    PICKUP_CARD("PickupCard"), @XmlEnumValue("CardSecurityCodeIncorrect")
    CARD_SECURITY_CODE_INCORRECT("CardSecurityCodeIncorrect"), @XmlEnumValue("ExpirationDateIncorrect")
    EXPIRATION_DATE_INCORRECT("ExpirationDateIncorrect"), @XmlEnumValue("ProcessorRetry")
    PROCESSOR_RETRY("ProcessorRetry"), @XmlEnumValue("ProcessorError")
    PROCESSOR_ERROR("ProcessorError");

    private final String value;

    CreditCardResponseStatus(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static CreditCardResponseStatus fromValue(String v) {
        for (CreditCardResponseStatus c : CreditCardResponseStatus.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }
}
