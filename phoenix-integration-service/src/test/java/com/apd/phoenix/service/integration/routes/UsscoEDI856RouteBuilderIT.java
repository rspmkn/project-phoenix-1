package com.apd.phoenix.service.integration.routes;

import java.io.InputStream;
import java.io.StringWriter;
import org.apache.camel.CamelContext;
import org.apache.camel.ExchangePattern;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.component.mock.MockEndpoint;
import org.apache.camel.impl.DefaultCamelContext;
import org.apache.commons.io.IOUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

public class UsscoEDI856RouteBuilderIT {

    private CamelContext camelContext;

    private EDI856RouteBuilder route;

    private static final String usscoShipmentNoticeSource = "direct:usscoShipmentNoticeSource";

    private static final String usscoShipmentNoticeLogDLQ = "mock:usscoShipmentNoticeLogDLQ";
    private MockEndpoint mockUsscoShipmentNoticeLogDLQ;

    private static final String usscoShipmentNoticeWorkflowDLQ = "mock:usscoShipmentNoticeWorkflowDLQ";
    private MockEndpoint mockUsscoShipmentNoticeWorkflowDLQ;

    private ProducerTemplate producer;

    @Before
    public void before() {
        camelContext = new DefaultCamelContext();
        route = new EDI856RouteBuilder();
        route.setSource(usscoShipmentNoticeSource);
        route.setLogOutputDLQ(usscoShipmentNoticeLogDLQ);
        route.setOutputDLQ(usscoShipmentNoticeWorkflowDLQ);

        mockUsscoShipmentNoticeLogDLQ = camelContext.getEndpoint(usscoShipmentNoticeLogDLQ, MockEndpoint.class);
        mockUsscoShipmentNoticeWorkflowDLQ = camelContext.getEndpoint(usscoShipmentNoticeWorkflowDLQ,
                MockEndpoint.class);

        producer = camelContext.createProducerTemplate();
    }

    @After
    public void after() throws Exception {
        camelContext.stop();
    }

    @Test
    public void createOrderLog() throws Exception {
        String usscoOrderAckLogHttpUri = "http://localhost:8180/rest/messages/";
        String usscoShipmentNoticeFormattedSink = "mock:usscoShipmentNoticeFormattedSink";
        MockEndpoint mockUsscoShipmentNoticeFormattedSink;
        route.setOutput(usscoShipmentNoticeFormattedSink);
        route.setLogOutputURI(usscoOrderAckLogHttpUri);
        mockUsscoShipmentNoticeFormattedSink = camelContext.getEndpoint(usscoShipmentNoticeFormattedSink,
                MockEndpoint.class);
        camelContext.addRoutes(route);
        camelContext.start();

        InputStream ips = this.getClass().getClassLoader().getResourceAsStream("edi-856/edi856-sample-2.txt");

        StringWriter writer = new StringWriter();
        IOUtils.copy(ips, writer);
        String ediMessage = writer.toString();

        mockUsscoShipmentNoticeFormattedSink.setExpectedMessageCount(1);
        mockUsscoShipmentNoticeLogDLQ.setExpectedMessageCount(0);

        producer.sendBody(usscoShipmentNoticeSource, ExchangePattern.InOut, ediMessage);

        mockUsscoShipmentNoticeFormattedSink.setResultWaitTime(2000);
        mockUsscoShipmentNoticeLogDLQ.setResultWaitTime(2000);
        mockUsscoShipmentNoticeFormattedSink.assertIsSatisfied();
        mockUsscoShipmentNoticeLogDLQ.assertIsSatisfied();

    }

    @Ignore
    @Test
    public void createOrderLogException() throws Exception {
        String usscoOrderAckLogHttpUri = "http://localhost:8180/rest/workflow/order/blah"; //an invalid url
        String usscoShipmentNoticeFormattedSink = "mock:usscoShipmentNoticeFormattedSink";
        MockEndpoint mockUsscoShipmentNoticeFormattedSink;
        route.setOutput(usscoShipmentNoticeFormattedSink);
        route.setLogOutputURI(usscoOrderAckLogHttpUri);
        mockUsscoShipmentNoticeFormattedSink = camelContext.getEndpoint(usscoShipmentNoticeFormattedSink,
                MockEndpoint.class);
        camelContext.addRoutes(route);
        camelContext.start();

        InputStream ips = this.getClass().getClassLoader().getResourceAsStream("edi-856/edi856-sample-2.txt");

        StringWriter writer = new StringWriter();
        IOUtils.copy(ips, writer);
        String ediMessage = writer.toString();

        mockUsscoShipmentNoticeFormattedSink.setExpectedMessageCount(1);
        mockUsscoShipmentNoticeLogDLQ.setExpectedMessageCount(1);

        producer.sendBody(usscoShipmentNoticeSource, ExchangePattern.InOut, ediMessage);

        mockUsscoShipmentNoticeFormattedSink.setResultWaitTime(2000);
        mockUsscoShipmentNoticeLogDLQ.setResultWaitTime(2000);
        mockUsscoShipmentNoticeFormattedSink.assertIsSatisfied();
        mockUsscoShipmentNoticeLogDLQ.assertIsSatisfied();
    }

    @Ignore
    @Test
    public void sendToWorkflow() throws Exception {
        String usscoShipmentNoticeFormattedSink = "http://localhost:8180/rest/workflow/order/advanced-shipment-notice";
        String usscoShipmentNoticeLogSink = "mock:usscoShipmentNoticeLogSink";
        MockEndpoint mockUsscoShipmentNoticeLogSink = camelContext.getEndpoint(usscoShipmentNoticeLogSink,
                MockEndpoint.class);
        route.setOutput(usscoShipmentNoticeFormattedSink);
        route.setLogOutput(usscoShipmentNoticeLogSink);
        camelContext.addRoutes(route);
        camelContext.start();

        mockUsscoShipmentNoticeLogSink.setExpectedMessageCount(1);

        InputStream ips = this.getClass().getClassLoader().getResourceAsStream("edi-856/edi856-sample-2.txt");

        StringWriter writer = new StringWriter();
        IOUtils.copy(ips, writer);
        String ediMessage = writer.toString();

        producer.sendBody(usscoShipmentNoticeSource, ExchangePattern.InOut, ediMessage);

        mockUsscoShipmentNoticeLogSink.setResultWaitTime(2000);
        mockUsscoShipmentNoticeLogSink.assertIsSatisfied();
    }

}
