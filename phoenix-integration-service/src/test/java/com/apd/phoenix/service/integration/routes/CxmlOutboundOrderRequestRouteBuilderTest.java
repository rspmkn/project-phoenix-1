package com.apd.phoenix.service.integration.routes;

import java.io.Serializable;
import java.util.HashMap;
import java.util.HashSet;
import org.apache.camel.CamelContext;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.impl.DefaultCamelContext;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import com.apd.phoenix.service.integration.routes.cxml.CxmlOutboundOrderRequestRouteBuilder;
import com.apd.phoenix.service.model.dto.CashoutPageDto;
import com.apd.phoenix.service.model.dto.CashoutPageXFieldDto;
import com.apd.phoenix.service.model.dto.CredentialDto;
import com.apd.phoenix.service.model.dto.PurchaseOrderDto;

public class CxmlOutboundOrderRequestRouteBuilderTest {

    CamelContext camelContext;
    ProducerTemplate producerTemplate;

    @Before
    public void before() throws Exception {
        camelContext = new DefaultCamelContext();
        producerTemplate = camelContext.createProducerTemplate();
        CxmlOutboundOrderRequestRouteBuilder route = new CxmlOutboundOrderRequestRouteBuilder();
        route.setOutput("stream:out");
        route.setSource("direct:source");
        route.setOutputDLQ("direct:outDLQ");
        camelContext.addRoutes(route);
        camelContext.start();
    }

    @After
    public void after() throws Exception {
        camelContext.stop();
    }

    @Ignore
    @Test
    public void orderRequest() {
        PurchaseOrderDto purchaseOrderDto = EDI850OutboundRouteBuilderTest.generateSampleOrder();
        final CredentialDto credentialDto = new CredentialDto();
        final HashMap<String, Serializable> propertyMap = new HashMap<>();
        credentialDto.setProperties(propertyMap);
        final CashoutPageDto cashoutPageDto = new CashoutPageDto();
        final HashSet<CashoutPageXFieldDto> cashoutPageXFields = new HashSet<>();
        addCashoutPageField(cashoutPageXFields,CashoutPageXFieldDto.CxmlMapping.ADDRESS_ID.getMapping(),
                CashoutPageXFieldDto.CxmlMapping.ADDRESS_ID.getMapping());
        addCashoutPageField(cashoutPageXFields,CashoutPageXFieldDto.CxmlMapping.CONTACT_NAME.getMapping(),
                CashoutPageXFieldDto.CxmlMapping.CONTACT_NAME.getMapping());
        addCashoutPageField(cashoutPageXFields,CashoutPageXFieldDto.CxmlMapping.HEADER_COMMENTS.getMapping(),
                CashoutPageXFieldDto.CxmlMapping.HEADER_COMMENTS.getMapping());
        addCashoutPageField(cashoutPageXFields,CashoutPageXFieldDto.CxmlMapping.HEADER_COMMENTS.getMapping(),
                CashoutPageXFieldDto.CxmlMapping.STREET_2.getMapping());
        addCashoutPageField(cashoutPageXFields,CashoutPageXFieldDto.CxmlMapping.STREET_2.getMapping(),
                CashoutPageXFieldDto.CxmlMapping.STREET_2.getMapping());
        cashoutPageDto.setCashoutPageXFields(cashoutPageXFields);
        credentialDto.setCashoutPageDto(cashoutPageDto);
        purchaseOrderDto.setCredential(credentialDto);
        producerTemplate.sendBody("direct:source", purchaseOrderDto);
    }

    private void addCashoutPageField(final HashSet<CashoutPageXFieldDto> cashoutPageXFields, String field,
            String mapping) {
        final CashoutPageXFieldDto cashoutPageXFieldDto = new CashoutPageXFieldDto();
        cashoutPageXFieldDto.setField(field);
        cashoutPageXFieldDto.setMessageMapping(mapping);
        cashoutPageXFields.add(cashoutPageXFieldDto);
    }
}
