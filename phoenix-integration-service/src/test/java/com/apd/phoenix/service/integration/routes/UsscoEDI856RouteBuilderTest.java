package com.apd.phoenix.service.integration.routes;

import java.io.InputStream;
import java.io.StringWriter;
import org.apache.camel.CamelContext;
import org.apache.camel.ExchangePattern;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.component.mock.MockEndpoint;
import org.apache.camel.impl.DefaultCamelContext;
import org.apache.commons.io.IOUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

public class UsscoEDI856RouteBuilderTest {

    private CamelContext camelContext;

    private EDI856RouteBuilder route;

    private static final String usscoShipmentNoticeSource = "direct:usscoShipmentNoticeSource";

    private static final String usscoShipmentNoticeFormattedSink = "mock:usscoShipmentNoticeFormattedSink";
    private MockEndpoint mockUsscoShipmentNoticeFormattedSink;

    private static final String usscoShipmentNoticeLogSink = "stream:out";

    private static final String usscoOrderLogDLQ = "direct:usscoOrderLogDLQ";

    private static final String usscoShipmentNoticeWorkflowDLQ = "direct:usscoShipmentNoticeWorkflowDLQ";

    private ProducerTemplate producer;

    @Before
    public void before() {
        camelContext = new DefaultCamelContext();
        route = new EDI856RouteBuilder();
        route.setOutput(usscoShipmentNoticeFormattedSink);
        route.setLogOutput(usscoShipmentNoticeLogSink);
        route.setSource(usscoShipmentNoticeSource);
        route.setLogOutputDLQ(usscoOrderLogDLQ);
        route.setOutputDLQ(usscoShipmentNoticeWorkflowDLQ);

        mockUsscoShipmentNoticeFormattedSink = camelContext.getEndpoint(usscoShipmentNoticeFormattedSink,
                MockEndpoint.class);

        producer = camelContext.createProducerTemplate();
    }

    @After
    public void after() throws Exception {
        camelContext.stop();
    }

    @Test
    public void validShipmentNotice() throws Exception {
        camelContext.addRoutes(route);
        camelContext.start();

        InputStream ips = this.getClass().getClassLoader().getResourceAsStream("edi-856/edi856-ussco-prod-1.txt");

        StringWriter writer = new StringWriter();
        IOUtils.copy(ips, writer);
        String ediMessage = writer.toString();

        mockUsscoShipmentNoticeFormattedSink.setExpectedMessageCount(3);

        producer.sendBody(usscoShipmentNoticeSource, ExchangePattern.InOut, ediMessage);

        mockUsscoShipmentNoticeFormattedSink.setResultWaitTime(2000);
        mockUsscoShipmentNoticeFormattedSink.assertIsSatisfied();
    }

    @Ignore
    @Test
    public void invalidShipNotice() throws Exception {
        camelContext.addRoutes(route);
        camelContext.start();

        String ediMessage = "Bad data";

        mockUsscoShipmentNoticeFormattedSink.setExpectedMessageCount(0);

        producer.sendBody(usscoShipmentNoticeSource, ExchangePattern.InOut, ediMessage);

        mockUsscoShipmentNoticeFormattedSink.setResultWaitTime(2000);
        mockUsscoShipmentNoticeFormattedSink.assertIsSatisfied();
    }

}
