package com.apd.phoenix.service.integration.routes;

import java.io.InputStream;
import java.io.StringWriter;
import java.util.List;
import javax.edi.model.x12.v5010.edi850.PurchaseOrder;
import junit.framework.Assert;
import org.apache.camel.CamelContext;
import org.apache.camel.Exchange;
import org.apache.camel.ExchangePattern;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.component.mock.MockEndpoint;
import org.apache.camel.impl.DefaultCamelContext;
import org.apache.commons.io.IOUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

public class InboundEDI850v5010Test {

    private Class<?> inboundDataformat = PurchaseOrder.class;
    private String inboundPurchaseOrderSource = "direct:inboundPurchaseOrderSource";
    private String inboundLogSource = "direct:inboundLogSource";
    private String inboundOutput = "mock:inboundOutput";
    private MockEndpoint mockInboundOutput;
    private String inboundLogOutput = "mock:inboundLogOutput";
    private MockEndpoint mockInboundLogOutput;

    private EDI850v5010InboundRouteBuilder route;

    private ProducerTemplate producer;

    CamelContext camelContext;

    @Before
    public void before() throws Exception {
        camelContext = new DefaultCamelContext();
        route = new EDI850v5010InboundRouteBuilder();
        route.setInboundPurchaseOrderSource(inboundPurchaseOrderSource);
        route.setInboundLogSource(inboundLogSource);
        route.setFaOutput("stream:out");

        mockInboundOutput = camelContext.getEndpoint(inboundOutput, MockEndpoint.class);
        mockInboundLogOutput = camelContext.getEndpoint(inboundLogOutput, MockEndpoint.class);

        camelContext.addRoutes(route);

        producer = camelContext.createProducerTemplate();
    }

    @After
    public void after() throws Exception {
        camelContext.stop();
    }

    @Ignore
    @Test
    public void walmartKitsValid() throws Exception {
        camelContext.addRoutes(route);
        camelContext.start();

        mockInboundOutput.setExpectedMessageCount(1);
        mockInboundLogOutput.setExpectedMessageCount(1);
        InputStream ips = this.getClass().getClassLoader().getResourceAsStream("edi-850/inbound/walmart-kits.850.txt");

        StringWriter writer = new StringWriter();
        IOUtils.copy(ips, writer);
        String ediMessage = writer.toString();

        producer.sendBody(inboundPurchaseOrderSource, ExchangePattern.InOut, ediMessage);

        List<Exchange> exchanges = mockInboundOutput.getReceivedExchanges();
        Assert.assertEquals(1, exchanges.size());
        Exchange exchange = exchanges.get(0);

        mockInboundOutput.setResultWaitTime(2000);
        mockInboundLogOutput.setResultWaitTime(2000);
        mockInboundOutput.assertIsSatisfied();
        mockInboundLogOutput.assertIsSatisfied();
    }

    @Ignore
    @Test
    public void dept99Valid() throws Exception {
        camelContext.addRoutes(route);
        camelContext.start();

        mockInboundOutput.setExpectedMessageCount(1);
        mockInboundLogOutput.setExpectedMessageCount(1);
        InputStream ips = this.getClass().getClassLoader().getResourceAsStream("edi-850/inbound/dept99.850.txt");

        StringWriter writer = new StringWriter();
        IOUtils.copy(ips, writer);
        String ediMessage = writer.toString();

        producer.sendBody(inboundPurchaseOrderSource, ExchangePattern.InOut, ediMessage);

        List<Exchange> exchanges = mockInboundOutput.getReceivedExchanges();
        Assert.assertEquals(1, exchanges.size());
        Exchange exchange = exchanges.get(0);

        mockInboundOutput.setResultWaitTime(2000);
        mockInboundLogOutput.setResultWaitTime(2000);
        mockInboundOutput.assertIsSatisfied();
        mockInboundLogOutput.assertIsSatisfied();
    }
}
