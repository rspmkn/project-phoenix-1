package com.apd.phoenix.service.integration.routes;

import java.io.InputStream;
import java.io.StringWriter;
import java.util.List;
import junit.framework.Assert;
import org.apache.camel.CamelContext;
import org.apache.camel.Exchange;
import org.apache.camel.ExchangePattern;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.component.mock.MockEndpoint;
import org.apache.camel.impl.DefaultCamelContext;
import org.apache.commons.io.IOUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.model.LineItemStatus.LineItemStatusEnum;
import com.apd.phoenix.service.model.dto.LineItemDto;
import com.apd.phoenix.service.model.dto.POAcknowledgementDto;
import com.fasterxml.jackson.databind.ObjectMapper;

public class UsscoEDI855RouteBuilderTest {

    private static final Logger LOG = LoggerFactory.getLogger(UsscoEDI855RouteBuilderTest.class);

    private CamelContext camelContext;

    private String usscoOrderAckSource = "direct:usscoOrderAckSource";

    private String usscoOrderAckLogSink = "stream:out";

    private String usscoOrderAckLogDLQ = "direct:usscoOrderAckLogDLQ";

    private String usscoOrderAckWorkflowDLQ = "direct:usscoOrderAckWorkflowDLQ";

    private String faOutput = "direct:faOutput";

    private ProducerTemplate producer;

    EDI855RouteBuilder route;

    private String usscoOrderAckSink = "mock:usscoOrderAckSink";
    private MockEndpoint mockUsscoOrderAckSink;

    @Before
    public void init() throws Exception {
        camelContext = new DefaultCamelContext();
        route = new EDI855RouteBuilder();
        route.setSource(usscoOrderAckSource);
        route.setOutput(usscoOrderAckSink);
        route.setLogOutput(usscoOrderAckLogSink);
        route.setLogOutputDLQ(usscoOrderAckLogDLQ);
        route.setOutputDLQ(usscoOrderAckWorkflowDLQ);
        route.setFaOutput(faOutput);

        mockUsscoOrderAckSink = camelContext.getEndpoint(usscoOrderAckSink, MockEndpoint.class);

        producer = camelContext.createProducerTemplate();
    }

    @After
    public void after() throws Exception {
        camelContext.stop();
    }

    @Ignore
    @Test
    public void unmarshal855Test() throws Exception {
        camelContext.addRoutes(route);
        camelContext.start();

        mockUsscoOrderAckSink.setExpectedMessageCount(1);
        InputStream ips = this.getClass().getClassLoader().getResourceAsStream("ussco-sample-855-1.txt");

        StringWriter writer = new StringWriter();
        IOUtils.copy(ips, writer);
        String ediMessage = writer.toString();

        producer.sendBody(usscoOrderAckSource, ExchangePattern.InOut, ediMessage);

        mockUsscoOrderAckSink.setResultWaitTime(2000);
        mockUsscoOrderAckSink.assertIsSatisfied();

    }

    @Test
    public void partialBackorder() throws Exception {

        camelContext.addRoutes(route);
        camelContext.start();

        mockUsscoOrderAckSink.setExpectedMessageCount(1);
        InputStream ips = this.getClass().getClassLoader().getResourceAsStream(
                "edi-855/partial-backorder/example-1.txt");

        StringWriter writer = new StringWriter();
        IOUtils.copy(ips, writer);
        String ediMessage = writer.toString();

        producer.sendBody(usscoOrderAckSource, ExchangePattern.InOut, ediMessage);

        mockUsscoOrderAckSink.setResultWaitTime(2000);
        mockUsscoOrderAckSink.assertIsSatisfied();

        List<Exchange> exchanges = mockUsscoOrderAckSink.getReceivedExchanges();
        Assert.assertEquals(1, exchanges.size());
        Exchange exchange = exchanges.get(0);

        ObjectMapper mapper = new ObjectMapper();
        POAcknowledgementDto dto = mapper.readValue((byte[]) exchange.getIn().getBody(), POAcknowledgementDto.class);
        for (LineItemDto lineItem : dto.getLineItems()) {
            LOG.info("Sku: {}", lineItem.getApdSku());
            if (lineItem.getSupplierPartId().equals("PENLRN7A")) {
                Assert.assertEquals(LineItemStatusEnum.PARTIAL_BACKORDERED.getValue(), lineItem.getStatus().getValue());
                Assert.assertEquals(139, lineItem.getQuantity().intValue());
            }
            if (lineItem.getSupplierPartId().equals("PENLRN5C")) {
                Assert.assertEquals(LineItemStatusEnum.ACCEPTED.getValue(), lineItem.getStatus().getValue());
            }
        }
    }

    @Test
    public void fullBackorder() throws Exception {
        camelContext.addRoutes(route);
        camelContext.start();

        mockUsscoOrderAckSink.setExpectedMessageCount(1);
        InputStream ips = this.getClass().getClassLoader().getResourceAsStream(
                "edi-855/partial-backorder/example-2.txt");

        StringWriter writer = new StringWriter();
        IOUtils.copy(ips, writer);
        String ediMessage = writer.toString();

        producer.sendBody(usscoOrderAckSource, ExchangePattern.InOut, ediMessage);

        mockUsscoOrderAckSink.setResultWaitTime(2000);
        mockUsscoOrderAckSink.assertIsSatisfied();

        List<Exchange> exchanges = mockUsscoOrderAckSink.getReceivedExchanges();
        Assert.assertEquals(1, exchanges.size());
        Exchange exchange = exchanges.get(0);

        ObjectMapper mapper = new ObjectMapper();
        POAcknowledgementDto dto = mapper.readValue((byte[]) exchange.getIn().getBody(), POAcknowledgementDto.class);
        for (LineItemDto lineItem : dto.getLineItems()) {
            LOG.info("Sku: {}", lineItem.getApdSku());
            if (lineItem.getSupplierPartId().equals("AVE08887")) {
                Assert.assertEquals(LineItemStatusEnum.BACKORDERED.getValue(), lineItem.getStatus().getValue());
                Assert.assertEquals(0, lineItem.getQuantity().intValue());
            }

        }
    }

    @Test
    public void itemQuantityChanged() throws Exception {
        camelContext.addRoutes(route);
        camelContext.start();

        mockUsscoOrderAckSink.setExpectedMessageCount(1);
        InputStream ips = this.getClass().getClassLoader().getResourceAsStream(
                "edi-855/partial-backorder/example-3.txt");

        StringWriter writer = new StringWriter();
        IOUtils.copy(ips, writer);
        String ediMessage = writer.toString();

        producer.sendBody(usscoOrderAckSource, ExchangePattern.InOut, ediMessage);

        mockUsscoOrderAckSink.setResultWaitTime(2000);
        mockUsscoOrderAckSink.assertIsSatisfied();

        List<Exchange> exchanges = mockUsscoOrderAckSink.getReceivedExchanges();
        Assert.assertEquals(1, exchanges.size());
        Exchange exchange = exchanges.get(0);

        ObjectMapper mapper = new ObjectMapper();
        POAcknowledgementDto dto = mapper.readValue((byte[]) exchange.getIn().getBody(), POAcknowledgementDto.class);
        for (LineItemDto lineItem : dto.getLineItems()) {
            LOG.info("Sku: {}", lineItem.getApdSku());
            if (lineItem.getSupplierPartId().equals("NSN5783515")) {
                Assert.assertEquals(LineItemStatusEnum.ITEM_QUANTITY_CHANGED.getValue(), lineItem.getStatus()
                        .getValue());
                Assert.assertEquals(17, lineItem.getQuantity().intValue());
            }

        }
    }

}