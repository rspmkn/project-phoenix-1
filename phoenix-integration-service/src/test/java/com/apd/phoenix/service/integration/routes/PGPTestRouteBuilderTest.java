package com.apd.phoenix.service.integration.routes;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.List;
import junit.framework.Assert;
import org.apache.camel.CamelContext;
import org.apache.camel.Exchange;
import org.apache.camel.ExchangePattern;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.component.mock.MockEndpoint;
import org.apache.camel.impl.DefaultCamelContext;
import org.apache.commons.io.IOUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class PGPTestRouteBuilderTest {

    CamelContext camelContext;

    ProducerTemplate producer;

    PGPTestRouteBuilder route;

    private static final String PUBLIC_KEY_FILE_NAME = "gpg/apd-phoenix-public-dev.gpg";
    private static final String PRIVATE_KEY_FILE_NAME = "gpg/apd-phoenix-private-dev.gpg";
    private static final String KEY_USERID = "American Product Distributors (APD) <juanitarichardson@americanproduct.com>";
    private static final String ENC_PASSWORD = "b3j5hbat";
    private static final boolean IS_ARMORED = true;
    private static final String source = "direct:source";
    private static final String sink = "mock:sink";
    private MockEndpoint mockSink;

    @Before
    public void before() throws Exception {
        camelContext = new DefaultCamelContext();
        producer = camelContext.createProducerTemplate();
        route = new PGPTestRouteBuilder();
        route.setPublicKeyFileName(PUBLIC_KEY_FILE_NAME);
        route.setPrivateKeyFileName(PRIVATE_KEY_FILE_NAME);
        route.setKeyUserid(KEY_USERID);
        route.setPassword(ENC_PASSWORD);
        route.setArmored(IS_ARMORED);
        route.setSource(source);
        route.setSink(sink);
        mockSink = camelContext.getEndpoint(sink, MockEndpoint.class);
        camelContext.addRoutes(route);
        camelContext.start();
    }

    @After
    public void after() throws Exception {
        camelContext.stop();
    }

    @Test
    public void example() throws InterruptedException, IOException {

        //		InputStream ips = this.getClass().getClassLoader().getResourceAsStream("sample824.txt");
        //
        //        StringWriter writer = new StringWriter();
        //        IOUtils.copy(ips, writer);
        //        String secretMessage = writer.toString();

        String secretMessage = "Hello World";
        mockSink.setExpectedMessageCount(1);
        producer.sendBody(source, ExchangePattern.InOut, secretMessage);
        mockSink.setResultWaitTime(2000);
        mockSink.assertIsSatisfied();
        List<Exchange> results = mockSink.getExchanges();
        for (Exchange result : results) {
            Assert.assertEquals(secretMessage, result.getIn().getBody(String.class));
        }
    }
}
