/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.service.integration.routes;

import java.io.InputStream;
import java.io.StringWriter;
import javax.edi.model.x12.edi850.PurchaseOrder;
import org.apache.camel.CamelContext;
import org.apache.camel.ExchangePattern;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.component.mock.MockEndpoint;
import org.apache.camel.impl.DefaultCamelContext;
import org.apache.commons.io.IOUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

/**
 *
 * @author nreidelb
 */
public class EDI850InboundUSPSTest {

    private ProducerTemplate producer;

    CamelContext camelContext;

    private Class<?> inboundDataformat = PurchaseOrder.class;

    //    private String ediPartnerEndpointDLQ = "mock:edi850PartnerEndpointDLQ";
    //    private MockEndpoint mockEdiPartnerEndpointDLQ;

    //    private String logSource = "direct:ediLogSource";

    //    private String logOutputDLQ = "mock:ediLogOutputDLQ";
    //    private MockEndpoint mockLogOutputDLQ;

    private String inboundEnpoint = "mock:inboundEnpoint";
    private MockEndpoint mockInboundEnpoint;

    private String purchaseOrderSource = "direct:inboundPurchaseOrderSource";

    EDI850v4010InboundRouteBuilder route;

    private static final String KEY_FILE_NAME = "gpg/apd-phoenix-private-dev.gpg";
    private static final String KEY_USERID = "American Product Distributors (APD) <juanitarichardson@americanproduct.com>";
    private static final String ENC_PASSWORD = "b3j5hbat";

    @Before
    public void before() {

        camelContext = new DefaultCamelContext();

        producer = camelContext.createProducerTemplate();

        route = new EDI850v4010InboundRouteBuilder();
        mockInboundEnpoint = camelContext.getEndpoint(inboundEnpoint, MockEndpoint.class);

        route.setInboundDataformat(inboundDataformat);
        route.setInboundPurchaseOrderSource(purchaseOrderSource);
        route.setLogging(false);

    }

    @After
    public void after() throws Exception {
        camelContext.stop();
    }

    @Ignore
    @Test
    public void testEncryptedMessage() throws Exception {
        route.setLogging(false);

        camelContext.addRoutes(route);
        camelContext.start();
        InputStream ips = this.getClass().getClassLoader().getResourceAsStream(
                "edi-850/inbound/usps850.txt.enc.pgp.asc");

        StringWriter writer = new StringWriter();
        IOUtils.copy(ips, writer);
        String ediMessage = writer.toString();

        mockInboundEnpoint.setExpectedMessageCount(1);

        producer.sendBody(purchaseOrderSource, ExchangePattern.InOut, ediMessage);
        mockInboundEnpoint.setResultWaitTime(2000);

        mockInboundEnpoint.assertIsSatisfied();

    }
}
