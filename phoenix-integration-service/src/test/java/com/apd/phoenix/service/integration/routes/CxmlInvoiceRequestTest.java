package com.apd.phoenix.service.integration.routes;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.camel.component.ActiveMQComponent;
import org.apache.activemq.pool.PooledConnectionFactory;
import org.apache.camel.CamelContext;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.component.jms.JmsConfiguration;
import org.apache.camel.impl.DefaultCamelContext;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import com.apd.phoenix.service.model.dto.AddressDto;
import com.apd.phoenix.service.model.dto.CXMLCredentialDto;
import com.apd.phoenix.service.model.dto.CredentialDto;
import com.apd.phoenix.service.model.dto.CredentialTermsDto;
import com.apd.phoenix.service.model.dto.CustomerInvoiceDto;
import com.apd.phoenix.service.model.dto.LineItemDto;
import com.apd.phoenix.service.model.dto.LineItemXShipmentDto;
import com.apd.phoenix.service.model.dto.PersonDto;
import com.apd.phoenix.service.model.dto.PhoneNumberDto;
import com.apd.phoenix.service.model.dto.PurchaseOrderDto;
import com.apd.phoenix.service.model.dto.ShipmentDto;
import com.apd.phoenix.service.model.dto.ShippingPartnerDto;
import com.apd.phoenix.service.model.dto.UnitOfMeasureDto;

public class CxmlInvoiceRequestTest {

    public final static String SOURCE = "activemq:queue:queue.cxml-outbound-invoice-request";

    CamelContext camelContext;

    private ProducerTemplate producer;

    @Before
    public void before() {
        camelContext = new DefaultCamelContext();
        ActiveMQConnectionFactory activeMQConnectionFactory = new ActiveMQConnectionFactory();
        activeMQConnectionFactory.setBrokerURL("failover://tcp://localhost:61616");
        PooledConnectionFactory pooledConnectionFactory = new PooledConnectionFactory();
        pooledConnectionFactory.setConnectionFactory(activeMQConnectionFactory);
        pooledConnectionFactory.setMaxConnections(8);
        JmsConfiguration jmsConfiguration = new JmsConfiguration();
        jmsConfiguration.setConnectionFactory(pooledConnectionFactory);
        jmsConfiguration.setConcurrentConsumers(10);
        ActiveMQComponent activeMQComponent = new ActiveMQComponent();
        activeMQComponent.setConfiguration(jmsConfiguration);
        camelContext.addComponent("activemq", activeMQComponent);

        producer = camelContext.createProducerTemplate();
    }

    @After
    public void after() throws Exception {
        camelContext.stop();
    }

    @Ignore
    @Test
	public void testSendingShipNotice() {
		Map<String, Object> headers = new HashMap<>();
		headers.put("apdPo", "APD23911991");
		headers.put("destination", "http://localhost");
		headers.put("deploymentMode", "test");
		CustomerInvoiceDto customerInvoiceDto = generateCustomerInvoiceDto();
		producer.sendBodyAndHeaders(SOURCE, customerInvoiceDto, headers);
	}

    private CustomerInvoiceDto generateCustomerInvoiceDto() {
		CustomerInvoiceDto customerInvoiceDto = new CustomerInvoiceDto();
		customerInvoiceDto.setApdPo("APD1234");
		customerInvoiceDto.setReceiverCode("adssd");
		customerInvoiceDto.setDestination("http://localhost");
		customerInvoiceDto.setPartnerId("asads");
		customerInvoiceDto.setCreatedDate(new Date());
		customerInvoiceDto.setInvoiceNumber("adsf");
		customerInvoiceDto.setAmount(BigDecimal.valueOf(3.99));
		List<String> specialInstructions = new ArrayList<>();
		specialInstructions.add("Hello World");
		customerInvoiceDto.setSpecalInstructions(specialInstructions);
		customerInvoiceDto.setBillingTypeCode("TEST");

		List<LineItemDto> actualLineItems = new ArrayList<>();

		// Shipment Dto
		ShipmentDto shipmentDto = new ShipmentDto();
		PurchaseOrderDto po = new PurchaseOrderDto();
		po.setApdPoNumber("APD123");
		CredentialDto credentialDto = new CredentialDto();
		Set<CXMLCredentialDto> cXMLCredentialDtos = new HashSet<>();
		CXMLCredentialDto cxmlcred = new CXMLCredentialDto();

		cXMLCredentialDtos.add(cxmlcred);
		credentialDto.setCustomerOutgoingFromCredentials(cXMLCredentialDtos);
		credentialDto.setCustomerOutgoingSenderCredentials(cXMLCredentialDtos);
		credentialDto.setCustomerOutgoingToCredentials(cXMLCredentialDtos);
		credentialDto.setCustomerOutgoingSharedSecret("");
		credentialDto.setOutgoingSharedSecret("");
		credentialDto.setOutgoingSharedSecret("");
		CredentialTermsDto terms = new CredentialTermsDto();
		terms.setDaysUntilDue(10);
		terms.setDaysWhileDiscounted(10);
		terms.setDiscountPercent(10);
		credentialDto.setTerms(terms);

		po.setCredential(credentialDto);
		shipmentDto.setCustomerOrderDto(po);

		List<String> comments = new ArrayList<>();
		comments.add("This is a comment");
		shipmentDto.setComments(comments);
		shipmentDto.setDeliveredTime(new Date());
		shipmentDto.setShipTime(new Date());
		AddressDto addressDto = new AddressDto();
		addressDto.setName("American Product Distributers");
		addressDto.setCity("Charlotte");
		addressDto.setState("NC");
		addressDto.setZip("55555");
		addressDto.setCountry("US");

		PersonDto personDto = new PersonDto();
		PhoneNumberDto phoneNumberDto = new PhoneNumberDto();
		phoneNumberDto.setAreaCode("444");
		phoneNumberDto.setLineNumber("555-5555");
		Set<PhoneNumberDto> phoneNumberDtos = new HashSet<>();
		phoneNumberDtos.add(phoneNumberDto);
		personDto.setPhoneNumberDtos(phoneNumberDtos);
		addressDto.setPersonDto(personDto);

		shipmentDto.setFromAddress(addressDto);
		shipmentDto.setCustomerServiceAddress(addressDto);
		shipmentDto.setTrackingNumber("Z21424234");
		ShippingPartnerDto shippingPartnerDto = new ShippingPartnerDto();
		shippingPartnerDto.setDuns("abcduns");
		shippingPartnerDto.setName("USPS");
		shippingPartnerDto.setScacCode("123scac");
		shipmentDto.setShippingPartnerDto(shippingPartnerDto);
		List<LineItemXShipmentDto> lineItemXShipments = new ArrayList<>();
		LineItemXShipmentDto item1 = new LineItemXShipmentDto();
		item1.setQuantity(new BigInteger("4"));
		LineItemDto lineItemDto = new LineItemDto();
		UnitOfMeasureDto unitOfMeasureDto = new UnitOfMeasureDto();
		unitOfMeasureDto.setName("EA");
		lineItemDto.setUnitOfMeasure(unitOfMeasureDto);
		lineItemDto.setCustomerLineNumber("1");
		actualLineItems.add(lineItemDto);
		item1.setLineItemDto(lineItemDto);
		shipmentDto.setLineItemXShipments(lineItemXShipments);
		customerInvoiceDto.setShipment(shipmentDto);
		
		customerInvoiceDto.setActualItems(actualLineItems);
		return customerInvoiceDto;
	}
}
