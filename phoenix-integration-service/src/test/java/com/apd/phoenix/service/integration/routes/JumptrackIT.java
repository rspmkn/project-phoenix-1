package com.apd.phoenix.service.integration.routes;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.camel.component.ActiveMQComponent;
import org.apache.activemq.pool.PooledConnectionFactory;
import org.apache.camel.CamelContext;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.component.jms.JmsConfiguration;
import org.apache.camel.impl.DefaultCamelContext;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import com.apd.phoenix.service.model.dto.DeliveryDto;
import com.apd.phoenix.service.model.dto.DeliveryDto.Type;
import com.apd.phoenix.service.model.dto.DeliveryLineDto;
import com.apd.phoenix.service.model.dto.DeliveryLineDto.UOM;
import com.apd.phoenix.service.model.dto.ManifestDto;
import com.apd.phoenix.service.model.dto.ManifestRequestDto;
import com.apd.phoenix.service.model.dto.RouteDto;
import com.apd.phoenix.service.model.dto.SiteDto;
import com.apd.phoenix.service.model.dto.StopDto;

public class JumptrackIT {

    CamelContext camelContext;

    ProducerTemplate producer;

    private static String source = "activemq:queue:queue.jumptrack-manifest";

    private static final String brokerUrl = "failover://tcp://localhost:61616";

    @Before
    public void before() throws Exception {
        camelContext = new DefaultCamelContext();
        setupAmq(camelContext, brokerUrl);
        producer = camelContext.createProducerTemplate();
    }

    @After
    public void after() throws Exception {
        camelContext.stop();
    }

    @Ignore
    @Test
    public void testManifest() throws Exception {
        camelContext.start();

        ManifestRequestDto request = generateJumptrackManifestRequestDto();

        producer.sendBody(source, request);

        Thread.sleep(10000);
    }

    private ManifestRequestDto generateJumptrackManifestRequestDto() {
    	ManifestRequestDto manifestRequestDto = new ManifestRequestDto();
    	List<ManifestDto> manifests = new ArrayList<>();
    	ManifestDto manifestDto = new ManifestDto();
    	//Route
    	RouteDto routeDto = new RouteDto();
    	routeDto.setDistribution("North America");
    	routeDto.setRegion("APD Charlotte Warehouse");
    	routeDto.setRoute("31820");
    	manifestDto.setRouteDto(routeDto);
    	//Stops
    	List<StopDto> stopDtos = new ArrayList<>();
    	for(int i = 0;i < 20;i++) {
	    	StopDto stopDto = new StopDto();
	    	stopDto.setDeliveryDate(new Date());
	    	
	    	DeliveryDto deliveryDto = new DeliveryDto();
	    	deliveryDto.setDeliveryPo("1234");
	    	deliveryDto.setId("5662"); //TODO: unique
	    	deliveryDto.setType(Type.DELIVERY);
	    	//Delivery Line
	    	List<DeliveryLineDto> deliveryLineDtos = new ArrayList<>();
	    	DeliveryLineDto deliveryLineDto = new DeliveryLineDto();
	    	deliveryLineDto.setName("APD");
	    	deliveryLineDto.setDescription("Hello World");
	    	deliveryLineDto.setQtyTarget(BigInteger.ONE);
	    	deliveryLineDto.setUom(UOM.BOX);
	    	deliveryLineDtos.add(deliveryLineDto);
	    	deliveryDto.setDeliveryLineDtos(deliveryLineDtos);
	    	stopDto.setDeliveryDto(deliveryDto);
	    	
	    	SiteDto siteDto = new SiteDto();
	    	siteDto.setAccount("APD");
	    	siteDto.setAddress1("123");
	    	siteDto.setAddress2("123");
	    	siteDto.setAddress3("123");
	    	siteDto.setCity("Midland");
	    	siteDto.setName("APD");
	    	siteDto.setState("GA");
	    	siteDto.setZip("31820");
	    	stopDto.setSiteDto(siteDto);
	    	stopDtos.add(stopDto);
    	}
    
    	manifestDto.setStops(stopDtos);
    	
    	//
    	manifests.add(manifestDto);
    	manifestRequestDto.setManifests(manifests);
    	return manifestRequestDto;
    }

    public void setupAmq(CamelContext context, String jmsBrokerUrl) {
        ActiveMQConnectionFactory activeMQConnectionFactory = new ActiveMQConnectionFactory();
        activeMQConnectionFactory.setBrokerURL(jmsBrokerUrl);
        PooledConnectionFactory pooledConnectionFactory = new PooledConnectionFactory();
        pooledConnectionFactory.setConnectionFactory(activeMQConnectionFactory);
        pooledConnectionFactory.setMaxConnections(2);
        JmsConfiguration jmsConfiguration = new JmsConfiguration();
        jmsConfiguration.setConnectionFactory(pooledConnectionFactory);
        jmsConfiguration.setConcurrentConsumers(2);
        ActiveMQComponent activeMQComponent = new ActiveMQComponent();
        activeMQComponent.setConfiguration(jmsConfiguration);
        context.addComponent("activemq", activeMQComponent);
    }
}
