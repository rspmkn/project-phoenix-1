/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.service.integration.routes;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import org.apache.camel.Endpoint;
import org.apache.camel.EndpointInject;
import org.apache.camel.ExchangePattern;
import org.apache.camel.Produce;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.mock.MockEndpoint;
import org.apache.camel.test.junit4.CamelTestSupport;
import org.apache.commons.io.IOUtils;
import org.junit.Test;

/**
 *
 * @author nreidelb
 */
public class EdiEntryPointRouteBuilderTest extends CamelTestSupport {

    @Produce
    private ProducerTemplate producer;

    @EndpointInject(uri = "direct:source")
    private Endpoint source;

    @EndpointInject(uri = "mock:untestedEndpoint")
    private MockEndpoint untestedEndpoint;

    @EndpointInject(uri = "stream:out")
    private Endpoint out;

    @Override
    protected RouteBuilder createRouteBuilder() {
        EDIEntryPointRouteBuilder entryPointRouteBuilder = new EDIEntryPointRouteBuilder();
        entryPointRouteBuilder.setTransactionSource(source.getEndpointUri());
        entryPointRouteBuilder.setTransactionFASink(untestedEndpoint.getEndpointUri());
        entryPointRouteBuilder.setTransactionORSink(untestedEndpoint.getEndpointUri());
        entryPointRouteBuilder.setTransactionINSink(untestedEndpoint.getEndpointUri());
        entryPointRouteBuilder.setTransactionPRSink(untestedEndpoint.getEndpointUri());
        entryPointRouteBuilder.setTransactionSHSink(untestedEndpoint.getEndpointUri());
        entryPointRouteBuilder.setTransactionUnknown(untestedEndpoint.getEndpointUri());
        entryPointRouteBuilder.setTransactionAGSink(untestedEndpoint.getEndpointUri());
        entryPointRouteBuilder.setTransactionTXSink(untestedEndpoint.getEndpointUri());
        entryPointRouteBuilder.setTransactionPOSink(out.getEndpointUri());
        entryPointRouteBuilder.setPartnerId("kits");
        return entryPointRouteBuilder;
    }

    @Test
    public void testInbound850() throws IOException {
        InputStream ips = this.getClass().getClassLoader().getResourceAsStream("sample850WalmartDept99.txt");

        StringWriter writer = new StringWriter();
        IOUtils.copy(ips, writer);
        byte[] ediMessage = writer.toString().getBytes();

        producer.sendBody(source, ExchangePattern.InOut, ediMessage);
    }
}
