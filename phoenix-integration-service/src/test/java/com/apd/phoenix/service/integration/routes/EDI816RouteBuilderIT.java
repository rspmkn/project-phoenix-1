/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.service.integration.routes;

import java.io.InputStream;
import java.io.StringWriter;
import java.util.List;
import org.apache.camel.CamelContext;
import org.apache.camel.Exchange;
import org.apache.camel.ExchangePattern;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.component.mock.MockEndpoint;
import org.apache.camel.impl.DefaultCamelContext;
import org.apache.commons.io.IOUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author nreidelb
 */
public class EDI816RouteBuilderIT {

    private static final Logger LOG = LoggerFactory.getLogger(EDI816RouteBuilderIT.class);

    private CamelContext camelContext;

    private String orgSource = "direct:orgSource";

    private String orgLogSource = "direct:orgLogSource";

    private String orgLogDLQ = "direct:orgLogDLQ";

    private String outputDLQ = "http://localhost:8080/rest/address/update";
    //private String orgSink = "stream:out";

    private String orgSink = "http://localhost:8080/rest/address/update";

    EDI816InboundRouteBuilder builder;
    private ProducerTemplate producer;

    @Before
    public void init() throws Exception {
        camelContext = new DefaultCamelContext();
        builder = new EDI816InboundRouteBuilder();
        builder.setLogOutputDLQ(orgLogDLQ);
        builder.setOrgLogSource(orgLogSource);
        builder.setOutput(orgSink);
        builder.setSource(orgSource);
        builder.setOrgLogOutput(orgLogSource);
        builder.setOutputDLQ(outputDLQ);
        builder.setLogOutputURI(outputDLQ);
        producer = camelContext.createProducerTemplate();
    }

    @After
    public void after() throws Exception {
        camelContext.stop();
    }

    @Test
    public void basicCamelTest() throws Exception {
        camelContext.addRoutes(builder);
        camelContext.start();

        //        mockUsscoInvoiceAckSink.setExpectedMessageCount(0);
        InputStream ips = this.getClass().getClassLoader().getResourceAsStream("edi-816/Walmart_816.txt");

        StringWriter writer = new StringWriter();
        IOUtils.copy(ips, writer);
        String ediMessage = writer.toString();

        producer.sendBody(orgSource, ExchangePattern.InOut, ediMessage);
    }
}
