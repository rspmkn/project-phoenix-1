/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.service.integration.routes;

import java.io.InputStream;
import java.io.StringWriter;
import javax.edi.model.x12.v5010.edi850.PurchaseOrder;
import org.apache.camel.Endpoint;
import org.apache.camel.EndpointInject;
import org.apache.camel.ExchangePattern;
import org.apache.camel.Produce;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.mock.MockEndpoint;
import org.apache.camel.test.junit4.CamelTestSupport;
import org.apache.commons.io.IOUtils;
import org.junit.Ignore;
import org.junit.Test;

/**
 *
 * @author nreidelb
 */
public class EDI850InboundWalmartIT extends CamelTestSupport {

    @Produce
    private ProducerTemplate producer;

    private Class<?> inboundDataformat = PurchaseOrder.class;

    @EndpointInject(uri = "mock:edi850PartnerEndpointDLQ")
    private MockEndpoint ediPartnerEndpointDLQ;

    @EndpointInject(uri = "direct:ediLogSource")
    private Endpoint logSource;

    @EndpointInject(uri = "mock:messages")
    private MockEndpoint logOutput; //overridden by header HTTP_URI

    @EndpointInject(uri = "mock:ediLogOutputDLQ")
    private MockEndpoint logOutputDLQ;

    @EndpointInject(uri = "direct:inboundPurchaseOrderSource")
    private Endpoint purchaseOrderSource;

    @Override
    protected RouteBuilder createRouteBuilder() {

        EDI850v5010InboundRouteBuilder route = new EDI850v5010InboundRouteBuilder();
        route.setInboundPurchaseOrderSource(purchaseOrderSource.getEndpointUri());

        return route;
    }

    @Ignore
    @Test
    public void messageKits() throws Exception {

        InputStream ips = this.getClass().getClassLoader().getResourceAsStream("sample850WalmartKits.txt");

        StringWriter writer = new StringWriter();
        IOUtils.copy(ips, writer);
        String ediMessage = writer.toString();

        producer.sendBody(purchaseOrderSource, ExchangePattern.InOut, ediMessage);

        ediPartnerEndpointDLQ.setResultWaitTime(5000);
        ediPartnerEndpointDLQ.assertIsSatisfied();
        logOutputDLQ.setResultWaitTime(5000);
        logOutputDLQ.assertIsSatisfied();
    }

    @Ignore
    @Test
    public void messageDept99() throws Exception {

        InputStream ips = this.getClass().getClassLoader().getResourceAsStream("sample850WalmartDept99.txt");

        StringWriter writer = new StringWriter();
        IOUtils.copy(ips, writer);
        String ediMessage = writer.toString();

        producer.sendBody(purchaseOrderSource, ExchangePattern.InOut, ediMessage);

        ediPartnerEndpointDLQ.setResultWaitTime(5000);
        ediPartnerEndpointDLQ.assertIsSatisfied();
        logOutputDLQ.setResultWaitTime(5000);
        logOutputDLQ.assertIsSatisfied();
    }

    @Ignore
    @Test
    public void messageUSPS() throws Exception {
        InputStream ips = this.getClass().getClassLoader().getResourceAsStream("usps850.txt");

        StringWriter writer = new StringWriter();
        IOUtils.copy(ips, writer);
        String ediMessage = writer.toString();

        producer.sendBody(purchaseOrderSource, ExchangePattern.InOut, ediMessage);
    }
}
