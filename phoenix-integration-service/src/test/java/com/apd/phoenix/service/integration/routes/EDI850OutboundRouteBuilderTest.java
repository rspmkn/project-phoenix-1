package com.apd.phoenix.service.integration.routes;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import java.util.HashMap;
import org.apache.camel.Endpoint;
import org.apache.camel.EndpointInject;
import org.apache.camel.ExchangePattern;
import org.apache.camel.Produce;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.mock.MockEndpoint;
import org.apache.camel.test.junit4.CamelTestSupport;
import org.junit.Ignore;
import org.junit.Test;
import com.apd.phoenix.service.integration.camel.processor.PurchaseOrderToEdi850Translator;
import com.apd.phoenix.service.model.dto.AccountDto;
import com.apd.phoenix.service.model.dto.AddressDto;
import com.apd.phoenix.service.model.dto.CredentialDto;
import com.apd.phoenix.service.model.dto.LineItemDto;
import com.apd.phoenix.service.model.dto.MiscShipToDto;
import com.apd.phoenix.service.model.dto.PurchaseOrderDto;
import com.apd.phoenix.service.model.dto.UnitOfMeasureDto;

public class EDI850OutboundRouteBuilderTest extends CamelTestSupport {

    @Produce
    private ProducerTemplate producer;

    @EndpointInject(uri = "direct:purchaseOrderSource")
    private Endpoint source;

    @EndpointInject(uri = "direct:edi850Endpoint")
    private Endpoint ediEndpoint;

    @EndpointInject(uri = "mock:ediUsscoEndpoint")
    private MockEndpoint ediUsscoEndpoint;

    @EndpointInject(uri = "mock:edi850PartnerEndpointDLQ")
    private MockEndpoint ediPartnerEndpointDLQ;

    @EndpointInject(uri = "direct:ediLogSource")
    private Endpoint logSource;

    @EndpointInject(uri = "mock:messages")
    private MockEndpoint logOutput; //overridden by header HTTP_URI

    @EndpointInject(uri = "mock:ediLogOutputDLQ")
    private MockEndpoint logOutputDLQ;

    private static String logOuputUri = "http://localhost:8180/rest/messages/";

    @EndpointInject(uri = "direct:resendSource")
    private Endpoint resendSource;

    public static final String GOOD_SKU1 = "UNV79000";
    public static final String GOOD_SKU0 = "UNV27411";
    public static final String GOOD_SKU2 = "UNV21200";
    private static UnitOfMeasureDto uom;
    public static final String BAD_SKU = "UNV27411X";

    @Override
    protected RouteBuilder createRouteBuilder() {

        EDI850OutboundRouteBuilder edi850RouteBuilder = new EDI850OutboundRouteBuilder();

        edi850RouteBuilder.setPurchaseOrderSource(source.getEndpointUri(), "");
        edi850RouteBuilder.setLogOutput(logOutput.getEndpointUri());
        edi850RouteBuilder.setLogOuputURI(logOuputUri);
        edi850RouteBuilder.setLogOutputDLQ(logOutputDLQ.getEndpointUri());
        edi850RouteBuilder.setEdi850PartnerEndpointDLQ(ediPartnerEndpointDLQ.getEndpointUri());
        edi850RouteBuilder.setPurchaseOrderResendSource(resendSource.getEndpointUri(), "");

        return edi850RouteBuilder;
    }

    @Ignore
    @Test
    public void ftpException() throws Exception {

        logOutputDLQ.expectedMessageCount(0);
        ediPartnerEndpointDLQ.expectedMessageCount(1);
        PurchaseOrderDto purchaseOrderDto = generateSampleOrder();

        producer.sendBody(source, ExchangePattern.InOut, purchaseOrderDto);

        ediPartnerEndpointDLQ.setResultWaitTime(5000);
        ediPartnerEndpointDLQ.assertIsSatisfied();
        logOutputDLQ.setResultWaitTime(5000);
        logOutputDLQ.assertIsSatisfied();
    }

    @Ignore
    @Test
    public void messageHttpException() throws Exception {

        ediPartnerEndpointDLQ.expectedMessageCount(0);
        logOutputDLQ.expectedMessageCount(1);
        PurchaseOrderDto purchaseOrder = generateSampleOrder();

        producer.sendBody(source, ExchangePattern.InOut, purchaseOrder);

        ediPartnerEndpointDLQ.setResultWaitTime(5000);
        ediPartnerEndpointDLQ.assertIsSatisfied();
        logOutputDLQ.setResultWaitTime(5000);
        logOutputDLQ.assertIsSatisfied();
    }

    public static PurchaseOrderDto generateSampleOrder() {
        PurchaseOrderDto purchaseOrder = new PurchaseOrderDto();
        purchaseOrder.setPartnerId("USSCO");
        purchaseOrder.setApdPoNumber("APD002014536");
        purchaseOrder.setOrderDate(new Date());
        purchaseOrder.setOrderTotal(new BigDecimal(575.25));
        
        purchaseOrder.setInterchangeId(String.valueOf(10));
        purchaseOrder.setGroupId(String.valueOf(11));
        purchaseOrder.setTransactionId(String.valueOf(13));
        

        AddressDto address = new AddressDto();
        address.setLine1("45 PLATEAU ST");
        address.setLine2("DeskTop SCH Patient Registration PO 63052");
        address.setCity("BRYSON CITY");
        address.setState("NC");
        address.setZip("28713");
        address.setCompanyName("BlahBlah");
        final MiscShipToDto miscShipToDto = new MiscShipToDto();
        miscShipToDto.setDivision("DivisionOfLabor");
        miscShipToDto.setAddressId("ADDRESS_ID1234");
        miscShipToDto.setHeaderComments("Here are some comments which should appear in the header");
        miscShipToDto.setStreet2("Speak friend and enter on the second door on the left, third floor");
        miscShipToDto.setCarrierName("Pony Express");
        miscShipToDto.setContractNumber("theNumberOfContract");
        miscShipToDto.setFinanceNumber("theNumberOfFinance");
        miscShipToDto.setCarrierRouting("routed by stagecoach through Dodge City");
        address.setMiscShipToDto(miscShipToDto);

        purchaseOrder.setShipToAddressDto(address);
        purchaseOrder.changeBillToAddressDto(address);
        
        purchaseOrder.setCustomerPoNumber("Customer002014536");

        AccountDto account = new AccountDto();
        account.setApdAssignedAccountId("MED069NC");
        account.setTerms("Due Upon Receipt");
        account.setName("Mountain Valley Health Center");

        purchaseOrder.setAccount(account);
        
        CredentialDto credentialDto = new CredentialDto();
        final HashMap<String, Serializable> propertiesMap = new HashMap<>();
        propertiesMap.put(PurchaseOrderToEdi850Translator.USSCO_ACCOUNT_NUMBER_PROPERTY_KEY, "958115");
        credentialDto.setProperties(propertiesMap);
        
        purchaseOrder.setCredential(credentialDto);

        uom = new UnitOfMeasureDto();
        uom.setName("EA");

        LineItemDto lineItem1 = new LineItemDto();
        lineItem1.setApdSku("ECS122ELT650H21A");
        lineItem1.setShortName("CompatibleToner with Lexmark 650H21A 25000 Page-Yield Lexmark T650 T652 T654 Bla");
        lineItem1.setQuantity(new BigInteger("1"));
        lineItem1.setUnitOfMeasure(uom);
        lineItem1.setUnitPrice(new BigDecimal(374.25));
        lineItem1.setSupplierPartId(GOOD_SKU0);
        lineItem1.setLineNumber(new Integer(1));

        purchaseOrder.getItems().add(lineItem1);

        LineItemDto lineItem2 = new LineItemDto();
        lineItem2.setApdSku("ECS122ELT650H21A");
        lineItem2.setShortName("CompatibleToner with Lexmark 650H21A 25000 Page-Yield Lexmark T650 T652 T654 Bla");
        lineItem2.setQuantity(new BigInteger("1"));
        lineItem2.setUnitOfMeasure(uom);
        lineItem2.setUnitPrice(new BigDecimal(374.25));
        lineItem2.setSupplierPartId(GOOD_SKU1);
        lineItem2.setLineNumber(new Integer(2));

        purchaseOrder.getItems().add(lineItem2);
        return purchaseOrder;
    }
    //    private static PurchaseOrderDto generateSampleOrder() {
    //        PurchaseOrderDto purchaseOrder = new PurchaseOrderDto();
    //        purchaseOrder.setPartnerId("USSCO");
    //        purchaseOrder.setApdPoNumber("APD002014536");
    //        purchaseOrder.setOrderDate(new Date());
    //        purchaseOrder.setOrderTotal(new BigDecimal(575.25));
    //
    //        AddressDto address = new AddressDto();
    //        address.setLine1("45 PLATEAU ST");
    //        address.setLine2("DeskTop SCH Patient Registration PO 63052");
    //        address.setCity("BRYSON CITY");
    //        address.setState("NC");
    //        address.setZip("28713");
    //
    //        purchaseOrder.setShipToAddressDto(address);
    //        purchaseOrder.changeBillToAddressDto(address);
    //
    //        AccountDto account = new AccountDto();
    //        account.setCustomerID("MED069NC");
    //        account.setTerms("Due Upon Receipt");
    //
    //        purchaseOrder.setAccount(account);
    //
    //        UnitOfMeasureDto uom = new UnitOfMeasureDto();
    //        uom.setName("EA");
    //
    //        LineItemDto lineItem1 = new LineItemDto();
    //        lineItem1.setApdSku("ECS122ELT650H21A");
    //        lineItem1.setDescription("CompatibleToner with Lexmark 650H21A 25000 Page-Yield Lexmark T650 T652 T654 Bla");
    //        lineItem1.setQuantity(new BigInteger("1"));
    //        lineItem1.setUnitOfMeasure(uom);
    //        lineItem1.setUnitPrice(new BigDecimal(374.25));
    //
    //        purchaseOrder.getItems().add(lineItem1);
    //
    //        LineItemDto lineItem2 = new LineItemDto();
    //        lineItem2.setApdSku("ECS122ELT650H21A");
    //        lineItem2.setDescription("CompatibleToner with Lexmark 650H21A 25000 Page-Yield Lexmark T650 T652 T654 Bla");
    //        lineItem2.setQuantity(new BigInteger("1"));
    //        lineItem2.setUnitOfMeasure(uom);
    //        lineItem2.setUnitPrice(new BigDecimal(374.25));
    //
    //        purchaseOrder.getItems().add(lineItem2);
    //        return purchaseOrder;
    //    }
}
