/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.service.integration.routes;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.camel.CamelContext;
import org.apache.camel.Exchange;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.component.mock.MockEndpoint;
import org.apache.camel.impl.DefaultCamelContext;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import com.apd.phoenix.service.model.dto.AddressDto;
import com.apd.phoenix.service.model.dto.ItemDto;
import com.apd.phoenix.service.model.dto.LineItemDto;
import com.apd.phoenix.service.model.dto.LineItemXShipmentDto;
import com.apd.phoenix.service.model.dto.MiscShipToDto;
import com.apd.phoenix.service.model.dto.PurchaseOrderDto;
import com.apd.phoenix.service.model.dto.ShipmentDto;
import com.apd.phoenix.service.model.dto.ShippingPartnerDto;
import com.apd.phoenix.service.model.dto.SkuDto;
import com.apd.phoenix.service.model.dto.SkuTypeDto;
import com.apd.phoenix.service.model.dto.UnitOfMeasureDto;
import org.junit.Ignore;

/**
 *
 * @author nreidelb
 */
public class EDI856OutboundRouteBuilderTest {

    private ProducerTemplate producer;

    private String source = "direct:shipmentSource";

    private String ediEndpoint = "mock:output";

    private MockEndpoint mockEdiEndpoint;

    CamelContext camelContext;

    EDI856OutboundRouteBuilder builder;

    @Before
    public void before() throws Exception {
        camelContext = new DefaultCamelContext();
        producer = camelContext.createProducerTemplate();
        mockEdiEndpoint = camelContext.getEndpoint(ediEndpoint, MockEndpoint.class);
        builder = new EDI856OutboundRouteBuilder();
        builder.setDataModelSource(source);
        builder.setPartnerEndpoint(ediEndpoint);
        builder.setLogDLQ(ediEndpoint);
        builder.setPartnerId("sampleID");

        //        builder.setKeyFileName("file:/home/jeckstei/Documents/projects/APD/eBuy2TestPublicKey.gpg");
        builder.setKeyFileName("eBuy2TestPublicKey.gpg");
        builder.setKeyUserid("ebuy gpg");
        builder.setArmored(true);

    }

    @After
    public void after() throws Exception {
        camelContext.stop();
    }

    @Test
    @Ignore
    public void testEncrypted() throws Exception {
        builder.setEncrypted(true);
        camelContext.addRoutes(builder);
        camelContext.start();
        ShipmentDto shipmentDto = generateShipmentDto();
        mockEdiEndpoint.setExpectedMessageCount(1);
        producer.sendBody(source, shipmentDto);
        mockEdiEndpoint.setResultWaitTime(6000);
        mockEdiEndpoint.assertIsSatisfied();

        List<Exchange> exchanges = mockEdiEndpoint.getReceivedExchanges();

        for (Exchange exchange : exchanges) {
            String body = exchange.getIn().getBody(String.class);
            Assert.assertTrue(!body.contains("ISA*00*"));
        }
    }

    @Ignore
    @Test
    public void testUnencrypted() throws Exception {
        builder.setEncrypted(false);
        camelContext.addRoutes(builder);
        camelContext.start();
        ShipmentDto shipmentDto = generateShipmentDto();
        mockEdiEndpoint.setExpectedMessageCount(1);
        producer.sendBody(source, shipmentDto);
        mockEdiEndpoint.setResultWaitTime(6000);
        mockEdiEndpoint.assertIsSatisfied();

        List<Exchange> exchanges = mockEdiEndpoint.getReceivedExchanges();

        for (Exchange exchange : exchanges) {
            String body = exchange.getIn().getBody(String.class);
            Assert.assertTrue(body.contains("ISA*00*"));
        }
    }

    private ShipmentDto generateShipmentDto() {
        ShipmentDto shipmentDto = new ShipmentDto();
        final AddressDto fromAddressDto = new AddressDto();
        fromAddressDto.setCity("fromCity");
        fromAddressDto.setState("ID");
        fromAddressDto.setZip("11111");
        shipmentDto.setFromAddress(fromAddressDto);
        shipmentDto.setTrackingNumber("trackingNumber");
        shipmentDto.setShipTime(new Date());
        ShippingPartnerDto shippingPartnerDto = new ShippingPartnerDto();                
        shippingPartnerDto.setScacCode("alpha code for carrier");
        shipmentDto.setShippingPartnerDto(shippingPartnerDto);
        final ArrayList<LineItemXShipmentDto> shipments = new ArrayList<>();
        final LineItemXShipmentDto lineItemXShipmentDto = new LineItemXShipmentDto();
        lineItemXShipmentDto.setQuantity(BigInteger.TEN);
        final LineItemDto lineItemDto = new LineItemDto();
        lineItemDto.setApdSku("apdSku");
        lineItemDto.setCustomerLineNumber("AAA12345");
        final ItemDto itemDto = new ItemDto();
        final ArrayList<SkuDto> skus = new ArrayList<>();
        final SkuDto skuDto = new SkuDto();
        final SkuTypeDto skuTypeDto = new SkuTypeDto();
        skuTypeDto.setName("manufacturer");
        skuDto.setType(skuTypeDto);
        skuDto.setValue("manufacturerSku");
        skus.add(skuDto);
        itemDto.setSkus(skus);
        lineItemDto.setItem(itemDto);
        final UnitOfMeasureDto unitOfMeasureDto = new UnitOfMeasureDto();
        unitOfMeasureDto.setName("EA");
        lineItemDto.setUnitOfMeasure(unitOfMeasureDto);
        lineItemXShipmentDto.setLineItemDto(lineItemDto);
        shipments.add(lineItemXShipmentDto);
        shipmentDto.setLineItemXShipments(shipments);
        final PurchaseOrderDto purchaseOrderDto = new PurchaseOrderDto();
        purchaseOrderDto.setApdPoNumber("apdPO");
        purchaseOrderDto.setCustomerPoNumber("customerPO");
        purchaseOrderDto.setOrderDate(new Date());
        final AddressDto addressDto = new AddressDto();
        addressDto.setCity("ShipToVille");
        addressDto.setZip("123456");
        addressDto.setName("give it to Mr. Hayek");
        final MiscShipToDto miscShipToDto = new MiscShipToDto();
        miscShipToDto.setAddressId("the Address ID");
        addressDto.setMiscShipToDto(miscShipToDto);
        purchaseOrderDto.setShipToAddressDto(addressDto);
        shipmentDto.setCustomerOrderDto(purchaseOrderDto);
        return shipmentDto;
    }
}
