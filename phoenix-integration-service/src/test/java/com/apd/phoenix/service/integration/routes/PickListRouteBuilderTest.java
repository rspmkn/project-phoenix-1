package com.apd.phoenix.service.integration.routes;

import org.apache.camel.CamelContext;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.impl.DefaultCamelContext;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

public class PickListRouteBuilderTest {

    CamelContext camelContext;

    ProducerTemplate producer;

    private static final String source = "ftp://ftp-dev.vpc.phoenixordering.com/inbound/picklist/local-john?password=password&username=ftp-dev&binary=false&passiveMode=true&delete=true";

    @Before
    public void before() throws Exception {
        camelContext = new DefaultCamelContext();
        producer = camelContext.createProducerTemplate();
        PickListRouteBuilder route = new PickListRouteBuilder();
        route.setSource(source);
        camelContext.addRoutes(route);
    }

    @After
    public void after() throws Exception {
        camelContext.stop();
    }

    @Ignore
    @Test
    public void testUnmarshal() throws Exception {
        camelContext.start();

        //        InputStream ips = this.getClass().getClassLoader().getResourceAsStream("picklist/example2.csv");
        //
        //        StringWriter writer = new StringWriter();
        //        IOUtils.copy(ips, writer);
        //        String pickListCsv = writer.toString();
        //
        //        producer.sendBody(source, pickListCsv);

        Thread.sleep(20000000);
    }
}
