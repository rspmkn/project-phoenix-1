/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.service.integration.routes;

import java.math.BigInteger;
import java.util.ArrayList;
import org.apache.camel.Endpoint;
import org.apache.camel.EndpointInject;
import org.apache.camel.Produce;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.mock.MockEndpoint;
import org.apache.camel.test.junit4.CamelTestSupport;
import org.junit.Test;
import com.apd.phoenix.service.model.dto.LineItemDto;
import com.apd.phoenix.service.model.dto.LineItemStatusDto;
import com.apd.phoenix.service.model.dto.POAcknowledgementDto;
import com.apd.phoenix.service.model.dto.UnitOfMeasureDto;
import com.apd.phoenix.service.persistence.multitenancy.impl.CurrentTenantIdentifierResolverImpl;
import org.junit.Ignore;

/**
 *
 * @author nreidelb
 */
public class EDI855OutboundRouteBuilderTest extends CamelTestSupport {

    @Produce
    private ProducerTemplate producer;

    @EndpointInject(uri = "direct:shipmentSource")
    private Endpoint source;

    @EndpointInject(uri = "mock:out")
    private MockEndpoint ediEndpoint;

    @Override
    protected RouteBuilder createRouteBuilder() {
        EDI855OutboundRouteBuilder builder = new EDI855OutboundRouteBuilder();
        builder.setDatamodelSource(source.getEndpointUri());
        builder.setPartnerEndpoint(ediEndpoint.getEndpointUri());
        builder.setLogDLQ(ediEndpoint.getEndpointUri());
        builder.setArmored(true);
        builder.setEncrypted(true);
        //Set a tenant manually so that we can grab tenant specific properties for encryption
        CurrentTenantIdentifierResolverImpl.setCurrentTenant(1L);
        return builder;
    }

    //broken because can't load property file
    @Ignore 
    @Test
    public void testRoute() throws InterruptedException {
        
        POAcknowledgementDto poAcknowledgementDto = new POAcknowledgementDto();
        poAcknowledgementDto.setApdPo("APDtestAPDPONUMBER");
        poAcknowledgementDto.setPartnerId("USPS");
        poAcknowledgementDto.setPurchaseOrderDto(EDI850OutboundRouteBuilderTest.generateSampleOrder());
        poAcknowledgementDto.getPurchaseOrderDto().setStatus("ORDERED");
        ArrayList<LineItemDto> itemList = new ArrayList<>();
        LineItemDto lineItemDto = new LineItemDto();
        lineItemDto.setApdSku("APDSKU1");
        lineItemDto.setBackorderedQuantity(BigInteger.TEN);
        lineItemDto.setBuyerPartNumber("BUYERPARTNUMBER");
        lineItemDto.setCustomerLineNumber("customerLineNumber");
        lineItemDto.setQuantity(BigInteger.ONE);
        LineItemStatusDto lineItemStatusDto = new LineItemStatusDto();
        lineItemStatusDto.setValue("ACCEPTED");
        lineItemDto.setStatus(lineItemStatusDto);
        UnitOfMeasureDto unitOfMeasureDto = new UnitOfMeasureDto();
        unitOfMeasureDto.setName("EA");
        lineItemDto.setUnitOfMeasure(unitOfMeasureDto);
        itemList.add(lineItemDto);
        poAcknowledgementDto.setLineItems(itemList);
        ediEndpoint.setExpectedMessageCount(1);
        producer.sendBody(source, poAcknowledgementDto);
        ediEndpoint.setResultWaitTime(2000);
        ediEndpoint.assertIsSatisfied();
    }
}
