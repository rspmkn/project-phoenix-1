package com.apd.phoenix.service.integration.routes;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.camel.component.ActiveMQComponent;
import org.apache.activemq.pool.PooledConnectionFactory;
import org.apache.camel.CamelContext;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.component.jms.JmsConfiguration;
import org.apache.camel.component.mock.MockEndpoint;
import org.apache.camel.impl.DefaultCamelContext;
import org.apache.commons.lang.StringUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.model.dto.AddressDto;
import com.apd.phoenix.service.model.dto.CredentialDto;
import com.apd.phoenix.service.model.dto.CustomerInvoiceDto;
import com.apd.phoenix.service.model.dto.LineItemDto;
import com.apd.phoenix.service.model.dto.LineItemXShipmentDto;
import com.apd.phoenix.service.model.dto.PurchaseOrderDto;
import com.apd.phoenix.service.model.dto.ShipmentDto;
import com.apd.phoenix.service.model.dto.ShippingPartnerDto;
import com.apd.phoenix.service.model.dto.TermsOfSaleDto;
import com.apd.phoenix.service.model.dto.UnitOfMeasureDto;
import java.io.Serializable;
import javax.edi.model.x12.edi810.segment.InvoiceItemGroup;
import javax.edi.model.x12.segment.BaselineItemData;

public class EDI810v5010OutboundRouteBuilderTest {

    private static final Logger LOG = LoggerFactory.getLogger(EDI810v5010OutboundRouteBuilderTest.class);
    private CamelContext camelContext;
    private EDI810v5010OutboundRouteBuilder route;
    private ProducerTemplate producer;

    private static final String outboundInvoiceSource = "direct:outboundInvoiceSource";

    private static final String outboundInvoiceEndpoint = "mock:outboundInvoiceEndpoint";
    private MockEndpoint mockOutboundInvoiceEndpoint;

    private static final String senderId = "PHOEN";

    @Before
    public void init() {
        camelContext = new DefaultCamelContext();
        route = new EDI810v5010OutboundRouteBuilder();
        producer = camelContext.createProducerTemplate();
        route.setOutboundInvoiceSource(outboundInvoiceSource);
        route.setOutboundInvoiceLogSource("direct:outboundInvoiceLogSource");
        route.setOutboundInvoiceEndpoint(outboundInvoiceEndpoint);
        route.setOutboundInvoiceEndpointDlq("stream:out");
        route.setSenderId(senderId);

        mockOutboundInvoiceEndpoint = camelContext.getEndpoint(outboundInvoiceEndpoint, MockEndpoint.class);

    }

    @After
    public void destroy() throws Exception {
        camelContext.stop();
    }

    @Ignore
    @Test
    public void testOutbound810amq() throws Exception {
    	setupAmq(camelContext, "failover://tcp://localhost:61616");
		camelContext.addRoutes(route);
		camelContext.start();
		
		CustomerInvoiceDto customerInvoiceDto = generateCustomerInvoice();
		
		Map<String, Object> headers = new HashMap<>();
		headers.put("receiverCode", "WALMART");
		headers.put("interchange", "0001");
		headers.put("groupId","0002");
		headers.put("transactionId", "0003");
		headers.put("ediVersion", "5010");
		headers.put("partnerId", "WALMARTKITS");
		producer.sendBodyAndHeaders("activemq:queue:queue.edi-outbound-810", customerInvoiceDto, headers);
    }

    @Ignore
    @Test
	public void testOutbound810() throws Exception {
    	
    	setupAmq(camelContext, "failover://tcp://localhost:61616");
		camelContext.addRoutes(route);
		camelContext.start();
		
		CustomerInvoiceDto customerInvoiceDto = generateCustomerInvoice();
		Map<String, Object> headers = new HashMap<>();
		headers.put("receiverCode", "WALMART");
		headers.put("isProduction", false);
		headers.put("interchange", "0001");
		headers.put("groupId","0002");
		headers.put("transactionId", "0003");
		headers.put("ediVersion", "5010");
		headers.put("partnerId", "walmart-kits");
		
		producer.sendBodyAndHeaders(outboundInvoiceSource, customerInvoiceDto, headers);
		
		mockOutboundInvoiceEndpoint.setExpectedMessageCount(1);
		
		producer.sendBodyAndHeaders(outboundInvoiceSource, customerInvoiceDto, headers);
		mockOutboundInvoiceEndpoint.setResultWaitTime(1000);
		mockOutboundInvoiceEndpoint.assertIsSatisfied();
	}

    private CustomerInvoiceDto generateCustomerInvoice() {
		CustomerInvoiceDto customerInvoiceDto = new CustomerInvoiceDto();
		customerInvoiceDto.setAmount(new BigDecimal("1.99"));
		
		customerInvoiceDto.setCreatedDate(new Date());
		
		customerInvoiceDto.setInvoiceNumber("1234x");
		customerInvoiceDto.setBillingTypeCode("AB");
		ShipmentDto shipmentDto = new ShipmentDto();
		PurchaseOrderDto purchaseOrderDto = new PurchaseOrderDto();
		purchaseOrderDto.setApdPoNumber("APD1234");
		purchaseOrderDto.setCustomerPoNumber("APD1234x");
		purchaseOrderDto.setOrderDate(new Date());
		purchaseOrderDto.setTaxTotal(new BigDecimal("1.00"));
		AddressDto billToAddressDto = new AddressDto();
		billToAddressDto.setName("Example Bill To");
		purchaseOrderDto.changeBillToAddressDto(billToAddressDto);
		AddressDto shipToAddressDto = new AddressDto();
		shipToAddressDto.setLine1("100 Main ST");
		shipToAddressDto.setName("WALMART");
		shipToAddressDto.setCity("Charlotte");
		shipToAddressDto.setState("NC");
		shipToAddressDto.setZip("55555");
		shipToAddressDto.setCountry("US");
		purchaseOrderDto.setShipToAddressDto(shipToAddressDto);
		TermsOfSaleDto termsOfSaleDto = new TermsOfSaleDto();
		termsOfSaleDto.setTermsTypeCode("01");
		termsOfSaleDto.setTermsBasisDateCode("3");
		termsOfSaleDto.setTermsDiscountPercent("10.5");
		termsOfSaleDto.setTermsDiscountDaysDue(1);
		termsOfSaleDto.setTermsNetDays("1");
		termsOfSaleDto.setDescription("sample description");
		purchaseOrderDto.setTermsOfSaleDto(termsOfSaleDto);
                final CredentialDto credentialDto = new CredentialDto();
                final HashMap<String, Serializable> hashMap = new HashMap<>();
                credentialDto.setProperties(hashMap);
                purchaseOrderDto.setCredential(credentialDto);
		shipmentDto.setCustomerOrderDto(purchaseOrderDto);
		shipmentDto.setShipmentPrice(new BigDecimal("1.99"));
                ShippingPartnerDto shippingPartnerDto = new ShippingPartnerDto();                
		shippingPartnerDto.setScacCode("A");
                shipmentDto.setShippingPartnerDto(shippingPartnerDto);
		shipmentDto.setRouting("1234");
		shipmentDto.setTrackingNumberQualifier("ZZ");
		shipmentDto.setTrackingNumber("Z1234");
		shipmentDto.setShippingMethodOfPayment("CC");
		shipmentDto.setPacks(new BigInteger("1"));
		shipmentDto.setInnerPacks(new BigInteger("1"));
		List<LineItemXShipmentDto> lineItemXShipments = new ArrayList<>();
		
		LineItemXShipmentDto lineItemXShipmentDto = new LineItemXShipmentDto();
		lineItemXShipmentDto.setQuantity(new BigInteger("1"));
		LineItemDto lineItemDto = new LineItemDto();
		lineItemDto.setApdSku("APD1234");
		lineItemDto.setShortName("Hello world");
		lineItemDto.setLineNumber(1);
		lineItemDto.setCustomerLineNumber("1");
		lineItemDto.setUnitPrice(new BigDecimal("1.99"));
		UnitOfMeasureDto unitOfMeasureDto = new UnitOfMeasureDto();
		unitOfMeasureDto.setName("EA");
		lineItemDto.setUnitOfMeasure(unitOfMeasureDto);
		lineItemXShipmentDto.setLineItemDto(lineItemDto);
		lineItemXShipments.add(lineItemXShipmentDto);
		
		shipmentDto.setLineItemXShipments(lineItemXShipments);
		customerInvoiceDto.setShipment(shipmentDto);
		
		return customerInvoiceDto;
	}

    public void setupAmq(CamelContext context, String jmsBrokerUrl) {
        ActiveMQConnectionFactory activeMQConnectionFactory = new ActiveMQConnectionFactory();
        activeMQConnectionFactory.setBrokerURL(jmsBrokerUrl);
        PooledConnectionFactory pooledConnectionFactory = new PooledConnectionFactory();
        pooledConnectionFactory.setConnectionFactory(activeMQConnectionFactory);
        pooledConnectionFactory.setMaxConnections(8);
        JmsConfiguration jmsConfiguration = new JmsConfiguration();
        jmsConfiguration.setConnectionFactory(pooledConnectionFactory);
        jmsConfiguration.setConcurrentConsumers(10);
        ActiveMQComponent activeMQComponent = new ActiveMQComponent();
        activeMQComponent.setConfiguration(jmsConfiguration);
        context.addComponent("activemq", activeMQComponent);
    }

    @Test
    public void testSort() {
        List<InvoiceItemGroup> items = new ArrayList<InvoiceItemGroup>();
        InvoiceItemGroup invoiceItemGroup = new InvoiceItemGroup();
        invoiceItemGroup.setBaselineItemData(new BaselineItemData());
        ;
        InvoiceItemGroup invoiceItemGroup2 = new InvoiceItemGroup();
        BaselineItemData baselineItemData2 = new BaselineItemData();
        baselineItemData2.setAssignedIdentificationNumber("0");
        invoiceItemGroup2.setBaselineItemData(baselineItemData2);
        InvoiceItemGroup invoiceItemGroup3 = new InvoiceItemGroup();
        BaselineItemData baselineItemData3 = new BaselineItemData();
        baselineItemData3.setAssignedIdentificationNumber("1");
        invoiceItemGroup3.setBaselineItemData(baselineItemData3);
        InvoiceItemGroup invoiceItemGroup4 = new InvoiceItemGroup();
        BaselineItemData baselineItemData4 = new BaselineItemData();
        baselineItemData4.setAssignedIdentificationNumber("2");
        invoiceItemGroup4.setBaselineItemData(baselineItemData3);
        items.add(invoiceItemGroup);
        items.add(invoiceItemGroup2);
        items.add(invoiceItemGroup3);
        items.add(invoiceItemGroup4);
        Collections.sort(items, new Comparator<InvoiceItemGroup>() {

            @Override
            public int compare(InvoiceItemGroup arg0, InvoiceItemGroup arg1) {
                if (arg0.getBaselineItemData() == null
                        || StringUtils.isEmpty(arg0.getBaselineItemData().getAssignedIdentificationNumber())) {
                    return 0;
                }
                if (arg1.getBaselineItemData() == null
                        || StringUtils.isEmpty(arg1.getBaselineItemData().getAssignedIdentificationNumber())) {
                    return 1;
                }
                return arg0.getBaselineItemData().getAssignedIdentificationNumber().compareTo(
                        arg1.getBaselineItemData().getAssignedIdentificationNumber());
            }

        });
        for (InvoiceItemGroup item : items) {
            System.out.println(item.getBaselineItemData().getAssignedIdentificationNumber());
        }
    }
}
