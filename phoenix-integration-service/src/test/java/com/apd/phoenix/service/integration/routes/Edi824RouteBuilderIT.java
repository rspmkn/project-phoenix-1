/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.service.integration.routes;

import com.apd.phoenix.service.model.dto.error.EdiDocumentErrorsDto;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.List;
import junit.framework.Assert;
import org.apache.camel.CamelContext;
import org.apache.camel.Exchange;
import org.apache.camel.ExchangePattern;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.component.mock.MockEndpoint;
import org.apache.camel.impl.DefaultCamelContext;
import org.apache.commons.io.IOUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author nreidelb
 */
public class Edi824RouteBuilderIT {

    private static final Logger LOG = LoggerFactory.getLogger(Edi824RouteBuilderIT.class);

    private CamelContext camelContext;

    private String orgSource = "direct:orgSource";

    private String orgLogSource = "direct:orgLogSource";

    private String orgLogDLQ = "direct:orgLogDLQ";

    private String outputDLQ = "direct:outputDLQ";

    private String orgSink = "http://localhost:8080/rest/integration/errorEmailService/send";

    EDI824RouteBuilder builder;
    private ProducerTemplate producer;

    @Before
    public void init() throws Exception {
        camelContext = new DefaultCamelContext();
        builder = new EDI824RouteBuilder();
        builder.setLogOutputDLQ(orgLogDLQ);
        builder.setOrgLogSource(orgLogSource);
        builder.setOutput(orgSink);
        builder.setSource(orgSource);
        builder.setOrgLogOutput(orgLogSource);
        builder.setOutputDLQ(outputDLQ);
        builder.setLogOutputURI(outputDLQ);
        producer = camelContext.createProducerTemplate();
    }

    @After
    public void after() throws Exception {
        camelContext.stop();
    }

    @Test
    public void dto810Test() throws Exception {
        camelContext.addRoutes(builder);
        camelContext.start();
        InputStream ips = this.getClass().getClassLoader().getResourceAsStream("sample824.txt");

        StringWriter writer = new StringWriter();
        IOUtils.copy(ips, writer);
        String ediMessage = writer.toString();

        producer.sendBody(orgSource, ExchangePattern.InOut, ediMessage);
    }
}
