package com.apd.phoenix.service.integration.routes;

import java.io.InputStream;
import java.io.StringWriter;
import java.util.List;
import org.apache.camel.CamelContext;
import org.apache.camel.Exchange;
import org.apache.camel.ExchangePattern;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.component.mock.MockEndpoint;
import org.apache.camel.impl.DefaultCamelContext;
import org.apache.commons.io.IOUtils;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class EDI997OutboundIT {

    private static final Logger LOG = LoggerFactory.getLogger(EDI997OutboundIT.class);

    private CamelContext camelContext;

    private ProducerTemplate producer;

    /**
     * Test the generation of a 997 from an inbound 855
     * This test requires rest service
     * @throws Exception
     */
    @Ignore
    @Test
    public void valid855() throws Exception {

        String usscoOrderAckSource = "direct:usscoOrderAckSource";

        String usscoOrderAckLogSink = "stream:out";

        String usscoOrderAckLogDLQ = "direct:usscoOrderAckLogDLQ";

        String faOutput = "direct:faOutput";
        //MockEndpoint mockFaOutput;

        String usscoOrderAckWorkflowDLQ = "direct:usscoOrderAckWorkflowDLQ";
        EDI855RouteBuilder route855;
        EDI997RouteBuilder route997 = new EDI997RouteBuilder();
        EDI997OutboundRouteBuilder outboundRoute997 = new EDI997OutboundRouteBuilder();

        String usscoOrderAckSink = "mock:usscoOrderAckSink";
        MockEndpoint mockUsscoOrderAckSink;

        route855 = new EDI855RouteBuilder();
        route855.setSource(usscoOrderAckSource);
        route855.setOutput(usscoOrderAckSink);
        route855.setLogOutput(usscoOrderAckLogSink);
        route855.setLogOutputDLQ(usscoOrderAckLogDLQ);
        route855.setOutputDLQ(usscoOrderAckWorkflowDLQ);
        route855.setFaOutput(faOutput);

        String fASource = "direct:fASource";
        String fAOutputDLQ = "direct:fAOutputDLQ";
        String fAOutboundSource = faOutput;
        String fAOutboundOutput = "mock:fAOutboundOutput";
        String fAResendSource = "direct:fAResendSource";
        String fAResendOutput = "direct:fAResendOutput";
        MockEndpoint mockFAOutboundOutput;

        route997.setSource(fASource);
        route997.setOutputDLQ(fAOutputDLQ);
        route997.setLogging(false);

        outboundRoute997.setOutputDLQ(fAOutputDLQ);
        outboundRoute997.setOutboundSource(fAOutboundSource, "");
        outboundRoute997.setOutboundOutput(fAOutboundOutput);
        outboundRoute997.setfAResendSource(fAResendSource, "");
        outboundRoute997.setfAResendOuput(fAResendOutput);

        mockFAOutboundOutput = camelContext.getEndpoint(fAOutboundOutput, MockEndpoint.class);

        mockUsscoOrderAckSink = camelContext.getEndpoint(usscoOrderAckSink, MockEndpoint.class);

        //mockFaOutput = camelContext.getEndpoint(faOutput, MockEndpoint.class);

        camelContext.addRoutes(route855);
        camelContext.addRoutes(route997);
        camelContext.start();

        mockUsscoOrderAckSink.setExpectedMessageCount(1);
        mockFAOutboundOutput.setExpectedMessageCount(1);
        //mockFaOutput.setExpectedMessageCount(1);
        InputStream ips = this.getClass().getClassLoader().getResourceAsStream("edi-855/facility-change/sample-2.txt");

        StringWriter writer = new StringWriter();
        IOUtils.copy(ips, writer);
        String ediMessage = writer.toString();

        producer.sendBody(usscoOrderAckSource, ExchangePattern.InOut, ediMessage);

        mockUsscoOrderAckSink.setResultWaitTime(2000);
        //mockFaOutput.setResultWaitTime(2000);
        mockFAOutboundOutput.setResultWaitTime(2000);
        mockUsscoOrderAckSink.assertIsSatisfied();
        //mockFaOutput.assertIsSatisfied();
        mockFAOutboundOutput.assertIsSatisfied();

        List<Exchange> exchanges = mockUsscoOrderAckSink.getReceivedExchanges();
        Assert.assertEquals(1, exchanges.size());
        Exchange exchange = exchanges.get(0);

    }

    @Before
    public void init() throws Exception {
        camelContext = new DefaultCamelContext();
        producer = camelContext.createProducerTemplate();
    }

    @After
    public void after() throws Exception {
        camelContext.stop();
    }
}
