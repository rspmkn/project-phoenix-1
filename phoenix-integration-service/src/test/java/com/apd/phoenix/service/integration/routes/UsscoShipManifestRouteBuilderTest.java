package com.apd.phoenix.service.integration.routes;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import org.apache.camel.CamelContext;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.impl.DefaultCamelContext;
import org.apache.commons.io.IOUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

public class UsscoShipManifestRouteBuilderTest {

    private static final String source = "direct:source";

    private CamelContext camelContext;

    private ProducerTemplate producer;

    private UsscoShipManifestRouteBuilder route;

    @Before
    public void before() throws Exception {
        camelContext = new DefaultCamelContext();
        producer = camelContext.createProducerTemplate();

        route = new UsscoShipManifestRouteBuilder();
        route.setSource(source);

        camelContext.addRoutes(route);

        camelContext.start();
    }

    @After
    public void after() throws Exception {
        camelContext.stop();
    }

    @Ignore
    @Test
    public void testManifest() throws IOException, InterruptedException {
        InputStream ips = this.getClass().getClassLoader().getResourceAsStream("manifest/manifest-example.csv");

        StringWriter writer = new StringWriter();
        IOUtils.copy(ips, writer);
        String message = writer.toString();

        producer.sendBody(source, message);

        Thread.sleep(2000);
    }

}
