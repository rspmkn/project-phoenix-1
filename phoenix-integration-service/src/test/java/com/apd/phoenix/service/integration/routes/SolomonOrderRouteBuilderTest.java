package com.apd.phoenix.service.integration.routes;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import org.apache.camel.CamelContext;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.component.mock.MockEndpoint;
import org.apache.camel.impl.DefaultCamelContext;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.apd.phoenix.service.model.dto.AccountDto;
import com.apd.phoenix.service.model.dto.AddressDto;
import com.apd.phoenix.service.model.dto.LineItemDto;
import com.apd.phoenix.service.model.dto.PurchaseOrderDto;
import com.apd.phoenix.service.model.dto.UnitOfMeasureDto;

public class SolomonOrderRouteBuilderTest {

    private static final String source = "direct:source";

    private static final String endpoint = "mock:endpoint";

    private MockEndpoint mockEndpoint;

    private static final String endpointDlq = "mock:endpointDlq";

    private MockEndpoint mockEndpointDlq;

    private CamelContext camelContext;

    private SolomonOrderRouteBuilder route;

    private ProducerTemplate producer;

    @Before
    public void before() throws Exception {
        camelContext = new DefaultCamelContext();
        producer = camelContext.createProducerTemplate();

        route = new SolomonOrderRouteBuilder();
        route.setTest(true);
        route.setSource(source);
        route.setEndpoint(endpoint);
        route.setEndpointDlq(endpointDlq);
        route.setLogging(false);

        camelContext.addRoutes(route);

        mockEndpoint = camelContext.getEndpoint(endpoint, MockEndpoint.class);
        camelContext.start();
    }

    @After
    public void after() throws Exception {
        camelContext.stop();
    }

    @Test
    public void orderTest() throws InterruptedException {
        PurchaseOrderDto po = generateSampleOrder();
        mockEndpoint.setExpectedMessageCount(1);
        producer.sendBody(source, po);
        mockEndpoint.setResultWaitTime(2000);
        mockEndpoint.assertIsSatisfied();
    }

    //    @Test
    //    public void orderClassCastException() throws InterruptedException {
    //        solomonOrderFtpSink.expectedMessageCount(0);
    //
    //        //send an invalid dto
    //        SkuDto skuDto = new SkuDto();
    //        producer.sendBody(solomonOrderSource, ExchangePattern.InOut, skuDto);
    //
    //        solomonOrderFtpSink.setResultWaitTime(2000);
    //        solomonOrderFtpSink.assertIsSatisfied();
    //    }
    //
    //    @Test
    //    public void dtoException() throws InterruptedException {
    //        solomonOrderFtpSink.expectedMessageCount(0);
    //
    //        //send an empty dto
    //        PurchaseOrderDto cod = new PurchaseOrderDto();
    //        producer.sendBody(solomonOrderSource, ExchangePattern.InOut, cod);
    //
    //        solomonOrderFtpSink.setResultWaitTime(2000);
    //        solomonOrderFtpSink.assertIsSatisfied();
    //    }

    private static PurchaseOrderDto generateSampleOrder() {
        PurchaseOrderDto cod = new PurchaseOrderDto();
        cod.setApdPoNumber("APD001937986");
        cod.setOrderDate(new Date());
        cod.setOrderTotal(new BigDecimal(575.25));

        AddressDto address = new AddressDto();
        address.setLine1("45 PLATEAU ST");
        address.setLine2("DeskTop SCH Patient Registration PO 63052");
        address.setCity("BRYSON CITY");
        address.setState("NC");
        address.setZip("28713");

        cod.setShipToAddressDto(address);

        AccountDto account = new AccountDto();
        account.setSolomonCustomerID("MED069NC");
        account.setTerms("Due Upon Receipt");

        cod.setAccount(account);

        UnitOfMeasureDto uom = new UnitOfMeasureDto();
        uom.setName("EA");

        LineItemDto lineItem1 = new LineItemDto();
        lineItem1.setApdSku("ECS122ELT650H21A");
        lineItem1.setDescription("CompatibleToner with Lexmark 650H21A 25000 Page-Yield Lexmark T650 T652 T654 Bla");
        lineItem1.setQuantity(new BigInteger("1"));
        lineItem1.setUnitOfMeasure(uom);
        lineItem1.setUnitPrice(new BigDecimal(374.25));

        cod.getItems().add(lineItem1);

        LineItemDto lineItem2 = new LineItemDto();
        lineItem2.setApdSku("ECS122ELT650H21A");
        lineItem2.setDescription("CompatibleToner with Lexmark 650H21A 25000 Page-Yield Lexmark T650 T652 T654 Bla");
        lineItem2.setQuantity(new BigInteger("1"));
        lineItem2.setUnitOfMeasure(uom);
        lineItem2.setUnitPrice(new BigDecimal(374.25));

        cod.getItems().add(lineItem2);
        return cod;
    }
}
