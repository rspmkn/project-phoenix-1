package com.apd.phoenix.service.integration.routes;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.apache.camel.CamelContext;
import org.apache.camel.ExchangePattern;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.component.mock.MockEndpoint;
import org.apache.camel.impl.DefaultCamelContext;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import com.apd.phoenix.service.model.dto.AccountDto;
import com.apd.phoenix.service.model.dto.AddressDto;
import com.apd.phoenix.service.model.dto.LineItemDto;
import com.apd.phoenix.service.model.dto.PurchaseOrderDto;
import com.apd.phoenix.service.model.dto.UnitOfMeasureDto;

public class EDI850OutboundRouteBuilderIT {

    private CamelContext camelContext;
    private ProducerTemplate producer;

    private static String ediPartnerEndpoint = "ftp://ftp-dev@ftp-dev.vpc.phoenixordering.com/test?password=password";
    private static String ediPartnerEndpointDLQ = "mock:edi850PartnerEndpointDLQ";
    private MockEndpoint mockEdiPartnerEndpointDLQ;

    private static String logOutput = "http://messages"; //overridden by header HTTP_URI
    //private MockEndpoint mockLogOutput;

    private static String logOutputDLQ = "mock:ediLogOutputDLQ";
    private MockEndpoint mockLogOutputDLQ;
    private static String logOuputUri = "http://localhost:8180/rest/messages/";

    EDI850OutboundRouteBuilder edi850RouteBuilder;

    @Before
    public void setup() throws Exception {
        camelContext = new DefaultCamelContext();
        edi850RouteBuilder = new EDI850OutboundRouteBuilder();

        edi850RouteBuilder.setLogOutput(logOutput);
        edi850RouteBuilder.setLogOuputURI(logOuputUri);
        edi850RouteBuilder.setLogOutputDLQ(logOutputDLQ);
        edi850RouteBuilder.setEdi850PartnerEndpointDLQ(ediPartnerEndpointDLQ);

        mockEdiPartnerEndpointDLQ = camelContext.getEndpoint(ediPartnerEndpointDLQ, MockEndpoint.class);
        //mockLogOutput = camelContext.getEndpoint(logOutput, MockEndpoint.class);
        mockLogOutputDLQ = camelContext.getEndpoint(logOutputDLQ, MockEndpoint.class);

        producer = camelContext.createProducerTemplate();
    }

    @Ignore
    @Test
    public void ftpException() throws Exception {

        String ediPartnerEndpoint = "ftp://ftp-dev@localhost?password=password";
        String logOutputUri = "stream:out";
        String source = "direct:queue:queue.edi-outbound-850";
        edi850RouteBuilder.setPurchaseOrderSource(source, "");
        edi850RouteBuilder.setLogOuputURI(logOutputUri);

        camelContext.addRoutes(edi850RouteBuilder);

        camelContext.start();
        //mockLogOutput.expectedMessageCount(1);
        mockLogOutputDLQ.expectedMessageCount(0);
        mockEdiPartnerEndpointDLQ.expectedMessageCount(1);
        PurchaseOrderDto purchaseOrderDto = generateSampleOrder();

        producer.sendBody(source, ExchangePattern.InOut, purchaseOrderDto);

        mockEdiPartnerEndpointDLQ.setResultWaitTime(5000);
        mockEdiPartnerEndpointDLQ.assertIsSatisfied();
        mockLogOutputDLQ.setResultWaitTime(5000);
        mockLogOutputDLQ.assertIsSatisfied();
        //mockLogOutput.setResultWaitTime(5000);
        //mockLogOutput.assertIsSatisfied();

    }

    @Ignore
    @Test
    public void send850ToPartner() throws Exception {
        String partnerEndpoint = "ftp://ftp-dev@ftp-dev.vpc.phoenixordering.com/test?password=password&binary=true&passiveMode=true";
        String source = "direct:queue:queue.edi-outbound-850";
        edi850RouteBuilder.setPurchaseOrderSource(source, "");

        camelContext.addRoutes(edi850RouteBuilder);

        camelContext.start();
        mockEdiPartnerEndpointDLQ.expectedMessageCount(0);
        PurchaseOrderDto purchaseOrder = generateSampleOrder();

        producer.sendBody(source, ExchangePattern.InOut, purchaseOrder);

        mockEdiPartnerEndpointDLQ.setResultWaitTime(5000);
        mockEdiPartnerEndpointDLQ.assertIsSatisfied();

    }

    @Ignore
    @Test
    public void messageHttpException() throws Exception {
        String partnerEndpoint = "ftp://ftp-dev@ftp-dev.vpc.phoenixordering.com/test?password=password&binary=true&passiveMode=true";
        String logOutputUri = "http://localhost:8180/rest/test";
        String source = "direct:queue:queue.edi-outbound-850";
        edi850RouteBuilder.setPurchaseOrderSource(source, "");
        edi850RouteBuilder.setLogOuputURI(logOutputUri);

        camelContext.addRoutes(edi850RouteBuilder);

        camelContext.start();
        mockEdiPartnerEndpointDLQ.expectedMessageCount(0);
        mockLogOutputDLQ.expectedMessageCount(1);
        PurchaseOrderDto purchaseOrder = generateSampleOrder();

        producer.sendBody(source, ExchangePattern.InOut, purchaseOrder);

        mockEdiPartnerEndpointDLQ.setResultWaitTime(5000);
        mockEdiPartnerEndpointDLQ.assertIsSatisfied();
        mockLogOutputDLQ.setResultWaitTime(5000);
        mockLogOutputDLQ.assertIsSatisfied();

    }

    @Ignore
    @Test
    public void dtoFromAmq() throws Exception {
        //edi850RouteBuilder.setOutboundDtoSource("activemq:queue:queue.edi-outbound-850");
        //edi850RouteBuilder.setOutboundRawSource("direct:rawSource");
        SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd-hhmm");

        String source = "activemq:queue:queue.edi-outbound-850";
        edi850RouteBuilder.setPurchaseOrderSource(source, "");
        camelContext.addRoutes(edi850RouteBuilder);

        camelContext.start();

        Thread.sleep(5000);
    }

    private PurchaseOrderDto generateSampleOrder() {
        PurchaseOrderDto purchaseOrder = new PurchaseOrderDto();
        purchaseOrder.setApdPoNumber("APD002014536");
        purchaseOrder.setOrderDate(new Date());
        purchaseOrder.setOrderTotal(new BigDecimal(575.25));

        AddressDto address = new AddressDto();
        address.setLine1("45 PLATEAU ST");
        address.setLine2("DeskTop SCH Patient Registration PO 63052");
        address.setCity("BRYSON CITY");
        address.setState("NC");
        address.setZip("28713");

        purchaseOrder.setShipToAddressDto(address);
        purchaseOrder.changeBillToAddressDto(address);

        AccountDto account = new AccountDto();
        account.setApdAssignedAccountId("MED069NC");
        account.setTerms("Due Upon Receipt");

        purchaseOrder.setAccount(account);

        UnitOfMeasureDto uom = new UnitOfMeasureDto();
        uom.setName("EA");

        LineItemDto lineItem1 = new LineItemDto();
        lineItem1.setApdSku("ECS122ELT650H21A");
        lineItem1.setDescription("CompatibleToner with Lexmark 650H21A 25000 Page-Yield Lexmark T650 T652 T654 Bla");
        lineItem1.setQuantity(new BigInteger("1"));
        lineItem1.setUnitOfMeasure(uom);
        lineItem1.setUnitPrice(new BigDecimal(374.25));

        purchaseOrder.getItems().add(lineItem1);

        LineItemDto lineItem2 = new LineItemDto();
        lineItem2.setApdSku("ECS122ELT650H21A");
        lineItem2.setDescription("CompatibleToner with Lexmark 650H21A 25000 Page-Yield Lexmark T650 T652 T654 Bla");
        lineItem2.setQuantity(new BigInteger("1"));
        lineItem2.setUnitOfMeasure(uom);
        lineItem2.setUnitPrice(new BigDecimal(374.25));

        purchaseOrder.getItems().add(lineItem2);
        return purchaseOrder;
    }

}
