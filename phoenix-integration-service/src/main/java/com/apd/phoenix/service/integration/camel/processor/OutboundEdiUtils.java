/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.service.integration.camel.processor;

import java.util.Date;
import javax.edi.model.x12.segment.GeographicLocation;
import javax.edi.model.x12.segment.GroupEnvelopeHeader;
import javax.edi.model.x12.segment.InterchangeEnvelopeHeader;
import javax.edi.model.x12.segment.Name;
import org.apache.commons.httpclient.util.DateUtil;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.model.dto.AddressDto;

/**
 *
 * @author nreidelb
 */
public class OutboundEdiUtils {

    private static final Logger LOG = LoggerFactory.getLogger(OutboundEdiUtils.class);

    public static final String CONTROL_ID_MAX_PADDING = "000000000";
    public static final String NO_AUTHORIZATION_INFORMATION_PRESENT = "00";
    public static final String ACCREDITED_STANDARDS_COMMITTEE_X12 = "X";
    public static final String X12_PROCEDURES_REVIEW_BOARD_97 = "00401";
    public static final String X12_PROCEDURES_REVIEW_BOARD_97_USSCO = "004010";
    public static final String NO_SECURITY_INFORMATION_PRESENT = "00";
    public static final String TEN_PADDED_SPACES = "          ";
    public static final String APD_PARTNER_ID = "PHOEN";
    public static final String APD_PARTNER_ID_PADDED = "PHOEN          ";
    public static final String SAMPLE_CONTROL_ID = "U";
    public static final String ACKNOWLEDGEMENT_REQUESTED = "1";
    public static final String NO_ACKNOWLEDGEMENT_REQUESTED = "0";
    public static final String SUBELEMENT_SEPARATOR = ">";
    public static final String SAMPLE_INTERCHANGE_ID_QUALIFIER = "ZZ";
    public static final String SENDER_QUALIFIER = "01";
    public static final String TEST_INDICATOR = "testIndicator";
    public static final String APPLICATION_RECIEVERS_CODE = "applicationRecieversCodeISA";
    public static final int INTERCHANGE_CONTROL_NUMBER_LENGTH = 9;
    public static final int LENGTH_OF_INTERCHANGE_RECEIVER_ID = 15;

    public static InterchangeEnvelopeHeader generateDefaultInterchangeEnvelopeHeader(String interchangeId) {
        final InterchangeEnvelopeHeader interchangeEnvelopeHeader = new InterchangeEnvelopeHeader();
        interchangeEnvelopeHeader
                .setAuthorizationInformationQualifier(OutboundEdiUtils.NO_AUTHORIZATION_INFORMATION_PRESENT);
        interchangeEnvelopeHeader.setAuthorizationInformation(OutboundEdiUtils.TEN_PADDED_SPACES);
        interchangeEnvelopeHeader.setSecurityInformationQualifier(OutboundEdiUtils.NO_SECURITY_INFORMATION_PRESENT);
        interchangeEnvelopeHeader.setSecurityInformation(OutboundEdiUtils.TEN_PADDED_SPACES);
        interchangeEnvelopeHeader.setSenderInterchangeIDQualifier(SAMPLE_INTERCHANGE_ID_QUALIFIER);
        interchangeEnvelopeHeader.setInterchangeSenderID(OutboundEdiUtils.APD_PARTNER_ID_PADDED);
        interchangeEnvelopeHeader.setReceiverInterchangeIDQualifier(SAMPLE_INTERCHANGE_ID_QUALIFIER);
        //Interchange Receiver ID not set
        interchangeEnvelopeHeader.setInterchangeDate(new Date());
        interchangeEnvelopeHeader.setInterchangeTime(DateUtil.formatDate(new Date(), "HHMM"));
        interchangeEnvelopeHeader.setInterchangeControlID(OutboundEdiUtils.SAMPLE_CONTROL_ID);
        interchangeEnvelopeHeader
                .setInterchangeControlNumber(zeroPad(interchangeId, INTERCHANGE_CONTROL_NUMBER_LENGTH));
        interchangeEnvelopeHeader.setInterchangeVersionNumber(OutboundEdiUtils.X12_PROCEDURES_REVIEW_BOARD_97);
        interchangeEnvelopeHeader.setiSAAcknowledgmentRequested(OutboundEdiUtils.NO_ACKNOWLEDGEMENT_REQUESTED);
        //Usage Indicator not set
        interchangeEnvelopeHeader.setSubelementSeparator(OutboundEdiUtils.SUBELEMENT_SEPARATOR);
        return interchangeEnvelopeHeader;
    }

    public static GroupEnvelopeHeader generateDefaultGroupEnvelopeHeader(String groupId) {
        GroupEnvelopeHeader groupEnvelopeHeader = new GroupEnvelopeHeader();
        //functional Id code not set
        groupEnvelopeHeader.setApplicationSendersCode("PHOEN"); //TODO
        //application reciever code not set
        groupEnvelopeHeader.setDate(new Date());
        groupEnvelopeHeader.setTime(DateUtil.formatDate(new Date(), "HHMM"));
        groupEnvelopeHeader.setGroupControlNumber(groupId);
        groupEnvelopeHeader.setResponsibleAgencyCode(OutboundEdiUtils.ACCREDITED_STANDARDS_COMMITTEE_X12);
        groupEnvelopeHeader.setVersionReleaseIndustryIdentifierCode(OutboundEdiUtils.X12_PROCEDURES_REVIEW_BOARD_97);
        return groupEnvelopeHeader;
    }

    public static String zeroPad(String value) {
        String s = CONTROL_ID_MAX_PADDING + value;
        return s.substring(s.length() - CONTROL_ID_MAX_PADDING.length());
    }

    public static String zeroPad(String property, int length) {
        if (property == null) {
            return zeroPad("", length);
        }
        if (property.length() > length) {
            LOG.warn("Unexpectedly long property: " + property + " should be only " + Integer.toString(length));
        }
        while (property.length() < length) {
            property = "0" + property;
        }
        return property;
    }

    public static String padWithSpaces(String property, int length) {
        if (property == null) {
            property = "";
        }
        if (property.length() > length) {
            LOG.warn("Unexpectedly long property: " + property + " should be only " + Integer.toString(length));
            return property;
        }
        while (property.length() < length) {
            property = property + " ";
        }
        if (property.length() > length) {
            LOG
                    .warn("Property: " + property + " longer than expected length: " + length
                            + ". Truncating the property.");
            property = property.substring(0, length);
        }
        return property;
    }

    public static String truncateAndRemoveDisallowedChars(String value, int maxSize) {
        return truncateAndRemoveDisallowedChars(value, 1, maxSize);
    }

    public static String truncateAndRemoveDisallowedChars(String value, int minSize, int maxSize) {
        //Remove disallowed characters
        if (value != null) {
            value = value.replace("|", ":");
        }
        if (value == null || minSize > value.length()) {
            if (value != null) {
                LOG.warn("Had to add spaces to " + value + " because it wasn't " + minSize + " characters long");
            }
            return padWithSpaces(value, minSize);
        }
        else if (value.length() <= maxSize) {
            return value;
        }
        else {
            String truncatedValue = value.substring(0, maxSize);
            LOG.warn("Had to trucate " + value + " to " + truncatedValue + " due to edi field length restriction.");
            return truncatedValue;
        }
    }

    public static GeographicLocation createN4(AddressDto address) {
        final GeographicLocation shipFromGeographicLocation = new GeographicLocation();
        ;
        shipFromGeographicLocation.setCityName(OutboundEdiUtils.truncateAndRemoveDisallowedChars(address.getCity(), 2,
                30));
        shipFromGeographicLocation.setStateOrProvinceCode(OutboundEdiUtils.truncateAndRemoveDisallowedChars(address
                .getState(), 2, 2));
        shipFromGeographicLocation
                .setPostalCode(OutboundEdiUtils.truncateAndRemoveDisallowedChars(address.getZip(), 9));
        return shipFromGeographicLocation;
    }
}
