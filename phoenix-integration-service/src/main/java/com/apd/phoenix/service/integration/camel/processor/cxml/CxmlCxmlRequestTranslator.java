package com.apd.phoenix.service.integration.camel.processor.cxml;

import static com.apd.phoenix.service.model.CredentialPropertyType.CredentialPropertyTypeEnum.*;
import java.math.BigDecimal;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import javax.inject.Inject;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;
import com.apd.phoenix.core.utility.TenantConfigRepository;
import com.apd.phoenix.service.business.UnitOfMeasureBp;
import com.apd.phoenix.service.integration.cxml.model.cxml.Address;
import com.apd.phoenix.service.integration.cxml.model.cxml.BillTo;
import com.apd.phoenix.service.integration.cxml.model.cxml.CXML;
import com.apd.phoenix.service.integration.cxml.model.cxml.CarrierIdentifier;
import com.apd.phoenix.service.integration.cxml.model.cxml.Classification;
import com.apd.phoenix.service.integration.cxml.model.cxml.Comments;
import com.apd.phoenix.service.integration.cxml.model.cxml.Country;
import com.apd.phoenix.service.integration.cxml.model.cxml.CountryCode;
import com.apd.phoenix.service.integration.cxml.model.cxml.Credential;
import com.apd.phoenix.service.integration.cxml.model.cxml.DeliverTo;
import com.apd.phoenix.service.integration.cxml.model.cxml.Description;
import com.apd.phoenix.service.integration.cxml.model.cxml.Email;
import com.apd.phoenix.service.integration.cxml.model.cxml.Extrinsic;
import com.apd.phoenix.service.integration.cxml.model.cxml.From;
import com.apd.phoenix.service.integration.cxml.model.cxml.Header;
import com.apd.phoenix.service.integration.cxml.model.cxml.Identity;
import com.apd.phoenix.service.integration.cxml.model.cxml.ItemDetail;
import com.apd.phoenix.service.integration.cxml.model.cxml.ItemID;
import com.apd.phoenix.service.integration.cxml.model.cxml.ItemOut;
import com.apd.phoenix.service.integration.cxml.model.cxml.Money;
import com.apd.phoenix.service.integration.cxml.model.cxml.Name;
import com.apd.phoenix.service.integration.cxml.model.cxml.OrderRequest;
import com.apd.phoenix.service.integration.cxml.model.cxml.OrderRequestHeader;
import com.apd.phoenix.service.integration.cxml.model.cxml.OriginalDocument;
import com.apd.phoenix.service.integration.cxml.model.cxml.Phone;
import com.apd.phoenix.service.integration.cxml.model.cxml.PostalAddress;
import com.apd.phoenix.service.integration.cxml.model.cxml.Request;
import com.apd.phoenix.service.integration.cxml.model.cxml.Sender;
import com.apd.phoenix.service.integration.cxml.model.cxml.SharedSecret;
import com.apd.phoenix.service.integration.cxml.model.cxml.ShipTo;
import com.apd.phoenix.service.integration.cxml.model.cxml.Shipping;
import com.apd.phoenix.service.integration.cxml.model.cxml.Street;
import com.apd.phoenix.service.integration.cxml.model.cxml.SupplierPartAuxiliaryID;
import com.apd.phoenix.service.integration.cxml.model.cxml.Tax;
import com.apd.phoenix.service.integration.cxml.model.cxml.TelephoneNumber;
import com.apd.phoenix.service.integration.cxml.model.cxml.To;
import com.apd.phoenix.service.integration.cxml.model.cxml.Total;
import com.apd.phoenix.service.integration.cxml.model.cxml.UnitPrice;
import com.apd.phoenix.service.model.PaymentType.PaymentTypeEnum;
import com.apd.phoenix.service.model.dto.AddressDto;
import com.apd.phoenix.service.model.dto.CXMLCredentialDto;
import com.apd.phoenix.service.model.dto.CashoutPageXFieldDto;
import com.apd.phoenix.service.model.dto.CashoutPageXFieldDto.CxmlMapping;
import com.apd.phoenix.service.model.dto.CredentialDto;
import com.apd.phoenix.service.model.dto.LineItemDto;
import com.apd.phoenix.service.model.dto.PoNumberDto;
import com.apd.phoenix.service.model.dto.PurchaseOrderDto;
import com.apd.phoenix.service.model.dto.TransactionDto.CXMLOperation;
import com.apd.phoenix.service.persistence.multitenancy.impl.CurrentTenantIdentifierResolverImpl;
import com.apd.phoenix.service.model.dto.UnitOfMeasureDto;
import com.apd.phoenix.service.model.dto.VendorDto;
import com.apd.phoenix.service.utility.CredentialPropertyUtils;
import com.apd.phoenix.service.utility.CxmlMessageUtils;

public class CxmlCxmlRequestTranslator {

    private static final Logger LOG = LoggerFactory.getLogger(CxmlCxmlRequestTranslator.class);
    private static final String YES = "Y";
    public static final String US_DOLLARS = "USD";
    public static final String DEFAULT_SHIPPING_DESCRIPTION = "business";
    public static final String ITEM_TYPE = "item";
    public static final String ZERO_DOLLARS = "0";
    public static final String DEFAULT_NAME = "default";
    public static final String DELIVERTO_ACCOUNTS_PAYABLE = "Accounts Payable";

    @Inject
    CxmlMessageUtils transactionUtil;

    @Inject
    UnitOfMeasureBp unitOfMeasureBp;

    public static final String PHONENUMBER_COUNTRYCODE_ISOCODE_US = "US";
    public static final String PHONENUMBER_COUNTRYCODE_NUMBER_US = "1";
    public static final String REGULAR_TYPE = "regular";
    public static final String VERSION_ONE = "1";
    public static final String APD_USER_AGENT = "Phoenix:PunchOutOrder 1.3";

    public com.apd.phoenix.service.integration.cxml.model.cxml.CXML fromPurchaseOrderToOrderRequest(
            @org.apache.camel.Header("deploymentMode") String deploymentMode,
            @org.apache.camel.Header("credentialDeploymentMode") String credDeploymentMode,
            @org.apache.camel.Body PurchaseOrderDto purchaseOrderDto) {
        if (StringUtils.isNotBlank(credDeploymentMode)) {
            deploymentMode = credDeploymentMode;
        }
        CXML cxml = createOrderRequest(purchaseOrderDto, deploymentMode);
        return cxml;
    }

    private CXML createOrderRequest(PurchaseOrderDto purchaseOrder, String deploymentMode) {
        Set<CashoutPageXFieldDto> cashoutFields = purchaseOrder.getCredential().getCashoutPageDto()
                .getCashoutPageXFields();
        CXML cxml = new CXML();
        cxml.setTimestamp(CxmlMessageUtils.generateTimeStamp());
        cxml.setVersion(CxmlMessageUtils.CXML_VERSION);
        cxml.setXmlLang(CxmlMessageUtils.DEFAULT_LOCALE);
        cxml.setPayloadID(transactionUtil.generatePayloadId());
        CredentialDto credentialDto = purchaseOrder.getCredential();
        final Header header = new Header();
        final From from = new From();
        for (CXMLCredentialDto credentailCXML : credentialDto.getOutgoingFromCredentials()) {
            final Credential credential = new Credential();
            credential.setDomain(credentailCXML.getDomain());
            final Identity identity = new Identity();
            identity.getContent().add(credentailCXML.getIdentifier());
            credential.setIdentity(identity);
            from.getCredential().add(credential);
        }
        header.setFrom(from);
        final To to = new To();
        for (CXMLCredentialDto credentailCXML : credentialDto.getOutgoingToCredentials()) {
            final Credential credential = new Credential();
            credential.setDomain(credentailCXML.getDomain());
            final Identity identity = new Identity();
            identity.getContent().add(credentailCXML.getIdentifier());
            credential.setIdentity(identity);
            to.getCredential().add(credential);
        }
        header.setTo(to);
        final Sender sender = new Sender();
        for (CXMLCredentialDto credentailCXML : credentialDto.getOutgoingSenderCredentials()) {
            final Credential credential = new Credential();
            credential.setDomain(credentailCXML.getDomain());
            final Identity identity = new Identity();
            identity.getContent().add(credentailCXML.getIdentifier());
            credential.setIdentity(identity);
            final SharedSecret sharedSecret = new SharedSecret();
            sharedSecret.getContent().add(credentialDto.getOutgoingSharedSecret());
            credential.getSharedSecretOrDigitalSignatureOrCredentialMac().add(sharedSecret);
            sender.getCredential().add(credential);
            sender.setUserAgent(APD_USER_AGENT);
        }
        header.setSender(sender);
        final OriginalDocument originalDocument = new OriginalDocument();
        originalDocument.setPayloadID(cxml.getPayloadID());//assigns same payload ID since not forwarded document
        header.setOriginalDocument(originalDocument);
        cxml.getHeaderOrMessageOrRequestOrResponse().add(header);
        Request request = new Request();
        request.setDeploymentMode(deploymentMode);
        List<Object> orderRequests = request
                .getProfileRequestOrOrderRequestOrMasterAgreementRequestOrPurchaseRequisitionRequestOrPunchOutSetupRequestOrProviderSetupRequestOrStatusUpdateRequestOrGetPendingRequestOrSubscriptionListRequestOrSubscriptionContentRequestOrSupplierListRequestOrSupplierDataRequestOrSubscriptionStatusUpdateRequestOrCopyRequestOrCatalogUploadRequestOrAuthRequestOrDataRequestOrOrganizationDataRequest();
        final OrderRequest orderRequest = new OrderRequest();
        final OrderRequestHeader orderRequestHeader = new OrderRequestHeader();
        orderRequestHeader.setOrderDate(CxmlMessageUtils.generateTimeStamp());

        // Use the customer PO number associated with the order (if present) for the orderId, otherwise use the APD PO number 
        PoNumberDto customerPoNumberDto = purchaseOrder.retrieveCustomerPoNumber();
        if (customerPoNumberDto != null) {
            orderRequestHeader.setOrderID(customerPoNumberDto.getValue());
        }
        else {
            orderRequestHeader.setOrderID(purchaseOrder.getApdPoNumber());
        }
        orderRequestHeader.setOrderType(REGULAR_TYPE);
        if (StringUtils.isNotBlank(purchaseOrder.getCustomerRevisionNumber())) {
            orderRequestHeader.setOrderVersion(purchaseOrder.getCustomerRevisionNumber());
        }
        else {
            orderRequestHeader.setOrderVersion(VERSION_ONE);
        }
        if (purchaseOrder.getTransactionOperation() != null
                && purchaseOrder.getTransactionOperation().getCXML() != null) {
            orderRequestHeader.setType(purchaseOrder.getTransactionOperation().getCXML().getValue());
        }
        else {
            orderRequestHeader.setType(CXMLOperation.NEW.getValue());
        }

        final Total total = new Total();
        final Money money = new Money();
        money.setCurrency(US_DOLLARS);
        money.setvalue(purchaseOrder.getOrderTotal().toPlainString());
        total.setMoney(money);
        orderRequestHeader.setTotal(total);
        ShipTo shipTo = generateCXMLShipTo(purchaseOrder, cashoutFields);
        boolean lineItemShipToEnabled = CredentialPropertyUtils.equals(SET_LINEITEM_SHIPTO_ON_OUTBOUND_ORDER, YES,
                credentialDto);
        if (!lineItemShipToEnabled) {
            orderRequestHeader.setShipTo(shipTo);
        }

        final Shipping shipping = new Shipping();
        final Money shipMoney = new Money();
        if (purchaseOrder.getEstimatedShippingAmount() != null) {
            shipMoney.setvalue(purchaseOrder.getEstimatedShippingAmount().toPlainString());
        }
        else {
            shipMoney.setvalue(ZERO_DOLLARS);
        }
        shipMoney.setCurrency(US_DOLLARS);
        shipping.setMoney(shipMoney);
        if (purchaseOrder.getShipToAddressDto().getMiscShipToDto().getShippingMethod() != null) {
            shipping.setTrackingDomain(purchaseOrder.getShipToAddressDto().getMiscShipToDto().getShippingMethod());
        }

        final Description description = new Description();
        description.setXmlLang(CxmlMessageUtils.DEFAULT_LOCALE);
        description.setvalue(DEFAULT_SHIPPING_DESCRIPTION);
        shipping.setDescription(description);
        orderRequestHeader.setShipping(shipping);

        final BillTo billTo = new BillTo();

        AddressDto billToAddressDto = purchaseOrder.lookupBillToAddressDto();
        billTo.setAddress(generateXMLAddress(billToAddressDto, cashoutFields, purchaseOrder));
        if (billTo.getAddress() != null && billTo.getAddress().getPostalAddress() != null) {
            billTo.getAddress().getPostalAddress().getDeliverTo().clear();
            DeliverTo deliverToAP = new DeliverTo();
            deliverToAP.setvalue(DELIVERTO_ACCOUNTS_PAYABLE);
            billTo.getAddress().getPostalAddress().getDeliverTo().add(deliverToAP);
        }
        orderRequestHeader.setBillTo(billTo);
        for (CashoutPageXFieldDto cashoutField : cashoutFields) {
            //EXT:
            String mapping = null;
            if (StringUtils.isNotEmpty(cashoutField.getMessageMapping())
                    && cashoutField.getMessageMapping().startsWith("EXT:", 0)) {
                mapping = cashoutField.getMessageMapping().replaceFirst("EXT:", "");
            }
            if (StringUtils.isNotEmpty(cashoutField.getField()) && StringUtils.isNotEmpty(mapping)
                    && cashoutField.getField().equals(mapping)) {
                String computedField = getValueByField(cashoutField.getField(), purchaseOrder, null);
                if (StringUtils.isNotEmpty(computedField) || StringUtils.isNotEmpty(cashoutField.getDefaultValue())) {
                    String value = computedField;
                    if (StringUtils.isEmpty(value)) {
                        value = cashoutField.getDefaultValue();
                    }
                    orderRequestHeader.getExtrinsic().add(createExtrinsic(mapping, value));
                }
            }
            //Other Extrinsics
            if (StringUtils.isNotEmpty(cashoutField.getMessageMapping())) {
                if (cashoutField.getMessageMapping().equals(
                        CashoutPageXFieldDto.CxmlMapping.REQUESTER_PHONE.getMapping())) {
                    String value = getValueByField(cashoutField.getField(), purchaseOrder, null);
                    orderRequestHeader.getExtrinsic().add(createExtrinsic(cashoutField.getMessageMapping(), value));
                }
                if (cashoutField.getMessageMapping().equals(
                        CashoutPageXFieldDto.CxmlMapping.REQUESTER_NAME.getMapping())) {
                    String value = getValueByField(cashoutField.getField(), purchaseOrder, null);
                    orderRequestHeader.getExtrinsic().add(createExtrinsic(cashoutField.getMessageMapping(), value));
                }
                if (cashoutField.getMessageMapping().equals(CashoutPageXFieldDto.CxmlMapping.REF_PO.getMapping())) {
                    String value = getValueByField(cashoutField.getField(), purchaseOrder, null);
                    orderRequestHeader.getExtrinsic().add(createExtrinsic(cashoutField.getMessageMapping(), value));
                }
                if (cashoutField.getMessageMapping().equals(
                        CashoutPageXFieldDto.CxmlMapping.APD_ACCOUNTING_ID.getMapping())) {
                    String value = getValueByField(cashoutField.getField(), purchaseOrder, null);
                    orderRequestHeader.getExtrinsic().add(createExtrinsic(cashoutField.getMessageMapping(), value));
                }
                if (cashoutField.getMessageMapping().equals(CashoutPageXFieldDto.CxmlMapping.FULFILLMENT.getMapping())) {
                    String value = getValueByField(cashoutField.getField(), purchaseOrder, null);
                    orderRequestHeader.getExtrinsic().add(createExtrinsic(cashoutField.getMessageMapping(), value));
                }
                if (cashoutField.getMessageMapping().equals(CashoutPageXFieldDto.CxmlMapping.PAYMENT_TYPE.getMapping())) {
                    String value = getValueByField(cashoutField.getField(), purchaseOrder, null);
                    String cxmlExtrinsicValue = "invoice";
                    if (StringUtils.equalsIgnoreCase(value, PaymentTypeEnum.CREDITCARD.getValue())) {
                        cxmlExtrinsicValue = "cc";
                    }
                    orderRequestHeader.getExtrinsic().add(
                            createExtrinsic(cashoutField.getMessageMapping(), cxmlExtrinsicValue));
                }
                if (cashoutField.getMessageMapping().equals(
                        CashoutPageXFieldDto.CxmlMapping.APD_INTERCHANGE_ID.getMapping())) {
                    String value = getValueByField(cashoutField.getField(), purchaseOrder, null);
                    orderRequestHeader.getExtrinsic().add(createExtrinsic(cashoutField.getMessageMapping(), value));
                }
                if (cashoutField.getMessageMapping().equals(
                        CashoutPageXFieldDto.CxmlMapping.CARRIER_ROUTING.getMapping())) {
                    String value = getValueByField(cashoutField.getField(), purchaseOrder, null);
                    orderRequestHeader.getExtrinsic().add(createExtrinsic(cashoutField.getMessageMapping(), value));
                }
                if (cashoutField.getMessageMapping().equals(
                        CashoutPageXFieldDto.CxmlMapping.HEADER_COMMENTS.getMapping())) {
                    String value = getValueByField(cashoutField.getField(), purchaseOrder, null);
                    Comments comments = new Comments();
                    comments.setvalue(value);
                    orderRequestHeader.setComments(comments);
                }
            }
        }
        orderRequest.setOrderRequestHeader(orderRequestHeader);

        // Sort the lineitems by line number
        Collections.sort(purchaseOrder.getItems(), new Comparator<LineItemDto>() {

            @Override
            public int compare(LineItemDto lineItemDto1, LineItemDto lineItemDto2) {
                String item1LineNumber = lineItemDto1.getCustomerLineNumber();
                String item2LineNumber = lineItemDto2.getCustomerLineNumber();
                if (item1LineNumber == null && item2LineNumber == null) {
                    return 0;
                }
                else if (item1LineNumber != null && item2LineNumber == null) {
                    return 1;
                }
                else if (item1LineNumber == null && item2LineNumber != null) {
                    return -1;
                }
                else if (item1LineNumber != null && item2LineNumber != null) {
                    return item1LineNumber.compareTo(item2LineNumber);
                }
                return 0;
            }
        });

        if (purchaseOrder.getRequestedDeliveryDate() == null) {
            purchaseOrder.setRequestedDeliveryDate(purchaseOrder.getDeliveryDate());
        }
        for (LineItemDto lineItemDto : purchaseOrder.getItems()) {
            try {
                ItemOut lineItem = createItem(lineItemDto, purchaseOrder, cashoutFields, lineItemShipToEnabled);
                if (lineItemShipToEnabled) {
                    lineItem.setShipTo(shipTo);
                }
                orderRequest.getItemOut().add(lineItem);
            }
            catch (Exception e) {
                LOG.error("Failed to create Item " + e.toString());
                throw new RuntimeException(e);
            }
        }
        orderRequests.add(orderRequest);
        cxml.getHeaderOrMessageOrRequestOrResponse().add(request);
        return cxml;
    }

    private static ShipTo generateCXMLShipTo(PurchaseOrderDto purchaseOrder, Set<CashoutPageXFieldDto> cashoutFields) {

        final ShipTo shipTo = new ShipTo();
        final Address shipToAddress = generateXMLAddress(purchaseOrder.getShipToAddressDto(), cashoutFields,
                purchaseOrder);
        String street2Field = getMappedField(cashoutFields, CashoutPageXFieldDto.CxmlMapping.STREET_2.getMapping());
        if (street2Field != null && getValueByField(street2Field, purchaseOrder, null) != null) {
            final Street street2 = new Street();
            street2.setvalue(getValueByField(street2Field, purchaseOrder, null));
            shipToAddress.getPostalAddress().getStreet().add(street2);
        }
        String street3Field = getMappedField(cashoutFields, CashoutPageXFieldDto.CxmlMapping.STREET_3.getMapping());
        if (street3Field != null && getValueByField(street3Field, purchaseOrder, null) != null) {
            final Street street3 = new Street();
            street3.setvalue(getValueByField(street3Field, purchaseOrder, null));
            if (shipToAddress.getPostalAddress().getStreet().size() > 2) {
                shipToAddress.getPostalAddress().getStreet().set(2, street3);
            }
            else {
                shipToAddress.getPostalAddress().getStreet().add(street3);
            }
        }
        String addressIdField = getMappedField(cashoutFields, CashoutPageXFieldDto.CxmlMapping.ADDRESS_ID.getMapping());
        if (addressIdField != null) {
            shipToAddress.setAddressID(getValueByField(addressIdField, purchaseOrder, null));
        }
        else {
            String addressIdBase36 = getMappedField(cashoutFields, CashoutPageXFieldDto.CxmlMapping.ADDRESS_ID_BASE_36
                    .getMapping());
            if (addressIdBase36 != null) {
                shipToAddress.setAddressID(getValueByField(addressIdBase36, purchaseOrder, null));
            }
        }

        String contactName = null;
        // The first DeliverTo on the ShipTo address of an outbound CXML Order Request MUST be used as the target data binding
        // for a field mapped to ContactName on the CashoutPage configuration.
        String fieldMappedToContactNqme = getMappedField(cashoutFields, CashoutPageXFieldDto.CxmlMapping.CONTACT_NAME
                .getMapping());
        if (fieldMappedToContactNqme != null && getValueByField(fieldMappedToContactNqme, purchaseOrder, null) != null) {
            contactName = getValueByField(fieldMappedToContactNqme, purchaseOrder, null);
        }
        if (shipToAddress.getPostalAddress() != null) {
            String desktop = null;
            String fieldMappedToDesktop = getMappedField(cashoutFields, CashoutPageXFieldDto.CxmlMapping.DESKTOP
                    .getMapping());
            if (fieldMappedToDesktop != null && getValueByField(fieldMappedToDesktop, purchaseOrder, null) != null) {
                desktop = getValueByField(fieldMappedToDesktop, purchaseOrder, null);
            }
            DeliverTo deliverToContact = null;
            DeliverTo deliverToDesktop = null;

            if (StringUtils.isNotBlank(contactName) || StringUtils.isNotBlank(desktop)) {
                deliverToContact = new DeliverTo();
                // If contactName is null, set to empty string and add to DeliverTo collection to ensure that the Desktop 
                // field, if provided, is the second DeliverTo element on the CXML ShipTo address
                deliverToContact.setvalue(contactName == null ? "" : contactName);
                shipToAddress.getPostalAddress().getDeliverTo().add(deliverToContact);

                deliverToDesktop = new DeliverTo();
                deliverToDesktop.setvalue(desktop == null ? "" : desktop);
                // The second DeliverTo on the ShipTo address of an outbound CXML Order Request MUST be used as the target data binding
                // for a field mapped to Desktop on the CashoutPage configuration.
                shipToAddress.getPostalAddress().getDeliverTo().add(1, deliverToDesktop);
            }
        }

        String contactEmail = "";
        if (CredentialPropertyUtils.equals(SEND_BUYER_EMAIL_TO_VENDOR, YES, purchaseOrder.getCredential())) {
            String orderEmailField = getMappedField(cashoutFields, CashoutPageXFieldDto.CxmlMapping.ORDER_EMAIL
                    .getMapping());
            if (StringUtils.isBlank(orderEmailField)) {
                orderEmailField = CxmlMapping.ORDER_EMAIL.getMapping();
            }
            if (orderEmailField != null && getValueByField(orderEmailField, purchaseOrder, null) != null) {
                contactEmail = getValueByField(orderEmailField, purchaseOrder, null);
            }
        }
        if (StringUtils.isBlank(contactEmail)) {
            contactEmail = (String) purchaseOrder.getCredential().getProperties().get(
                    DEFAULT_SHIPTO_EMAIL_CONTACT.getValue());
        }
        if (StringUtils.isBlank(contactEmail)) {
            Properties emailsLoader = TenantConfigRepository.getInstance().getPropertiesByTenantId(
                    CurrentTenantIdentifierResolverImpl.getCurrentTenant(), "tenant");
            contactEmail = emailsLoader.getProperty("sales_email_address");
        }
        final Email email = new Email();
        email.setName(DEFAULT_NAME);
        email.setvalue(contactEmail);
        shipToAddress.setEmail(email);

        String contactPhone = "";
        String fieldMappedToContactPhone = getMappedField(cashoutFields, CashoutPageXFieldDto.CxmlMapping.CONTACT_PHONE
                .getMapping());
        if (StringUtils.isBlank(fieldMappedToContactPhone)) {
            fieldMappedToContactPhone = CxmlMapping.CONTACT_PHONE.getMapping();
        }
        if (fieldMappedToContactPhone != null
                && getValueByField(fieldMappedToContactPhone, purchaseOrder, null) != null) {
            contactPhone = getValueByField(fieldMappedToContactPhone, purchaseOrder, null);
        }
        shipToAddress.setPhone(generateCXMLPhone(contactPhone));
        shipTo.setAddress(shipToAddress);

        String carrierRouting = getMappedField(cashoutFields, "EXT:CarrierRouting");
        if (carrierRouting != null) {
            final CarrierIdentifier carrierIdentifier = new CarrierIdentifier();
            carrierIdentifier.setvalue(getValueByField(carrierRouting, purchaseOrder, null));
            shipTo.setCarrierIdentifier(carrierIdentifier);
        }
        return shipTo;
    }

    private static Address generateXMLAddress(AddressDto addressDto, Set<CashoutPageXFieldDto> cashoutFields,
            PurchaseOrderDto po) {
        final Address address = new Address();
        final PostalAddress postalAddress = new PostalAddress();
        if (addressDto.getCountry() != null) {
            address.setIsoCountryCode(addressDto.getCountry());
            final Country country = new Country();
            country.setIsoCountryCode(addressDto.getCountry());
            country.setvalue(addressDto.lookupCountryName(addressDto.getCountry()));
            postalAddress.setCountry(country);
        }
        postalAddress.setCity(addressDto.getCity());
        postalAddress.setPostalCode(addressDto.getZip());
        postalAddress.setState(addressDto.getState());

        final Street street1 = new Street();
        street1.setvalue(addressDto.getLine1());
        postalAddress.getStreet().add(street1);
        if (addressDto.getLine2() != null) {
            final Street street2 = new Street();
            street2.setvalue(addressDto.getLine2());
            postalAddress.getStreet().add(street2);
            if (addressDto.getMiscShipToDto() != null && addressDto.getMiscShipToDto().getStreet2() != null) {
                final Street street3 = new Street();
                street3.setvalue(addressDto.getMiscShipToDto().getStreet2());
                postalAddress.getStreet().add(street3);
                if (addressDto.getMiscShipToDto() != null && addressDto.getMiscShipToDto().getStreet3() != null) {
                    final Street street4 = new Street();
                    street4.setvalue(addressDto.getMiscShipToDto().getStreet3());
                    postalAddress.getStreet().add(street4);
                }
            }
        }
        address.setPostalAddress(postalAddress);

        // Populate the address ID
        address.setAddressID(addressDto.getShipToId());
        if (StringUtils.isBlank(address.getAddressID()) && addressDto.getMiscShipToDto() != null) {
            address.setAddressID(addressDto.getMiscShipToDto().getAddressId());
            if (StringUtils.isBlank(address.getAddressID())) {
                address.setAddressID(addressDto.getMiscShipToDto().getAddressId36());
            }
        }
        if (addressDto.getCompanyName() != null) {
            final Name name = new Name();
            name.setvalue(addressDto.getCompanyName());
            name.setXmlLang(CxmlMessageUtils.DEFAULT_LOCALE);
            address.setName(name);
        }
        return address;
    }

    private static Phone generateCXMLPhone(String phoneNumberValue) {
        if (phoneNumberValue == null) {
            return null;
        }
        // First remove all non-numeric characters
        phoneNumberValue = phoneNumberValue.replaceAll("\\D", "");

        // Area code and number value are required for TelephoneNumber.  
        if (phoneNumberValue.length() < 10) {
            return null;
        }
        if (phoneNumberValue.startsWith("1")) {
            // Since no valid US area code begins with "1" and US is the only supported country code, 
            // assume an initial "1" is the country code. 
            phoneNumberValue = phoneNumberValue.substring(1);
            if (phoneNumberValue.length() < 10) {
                return null;
            }
        }

        String areaCode = phoneNumberValue.substring(0, 3);
        String number = phoneNumberValue.substring(3, 10);
        String extension = phoneNumberValue.length() > 10 ? phoneNumberValue.substring(10) : "";

        TelephoneNumber phoneNumber = new TelephoneNumber();
        CountryCode cxmlCountryCode = new CountryCode();
        cxmlCountryCode.setIsoCountryCode(PHONENUMBER_COUNTRYCODE_ISOCODE_US);
        cxmlCountryCode.setvalue(PHONENUMBER_COUNTRYCODE_NUMBER_US);
        phoneNumber.setCountryCode(cxmlCountryCode);
        phoneNumber.setAreaOrCityCode(areaCode);
        phoneNumber.setNumber(number);
        if (StringUtils.isNotBlank(extension)) {
            phoneNumber.setExtension(extension);
        }
        Phone phone = new Phone();
        phone.setTelephoneNumber(phoneNumber);
        phone.setName(DEFAULT_NAME);
        return phone;
    }

    private ItemOut createItem(LineItemDto lineItem, PurchaseOrderDto po, Set<CashoutPageXFieldDto> fields,
            boolean lineItemShipToEnabled) throws Exception {
        ItemOut createdItem = new ItemOut();

        if (po.getRequestedDeliveryDate() != null) {
            createdItem.setRequestedDeliveryDate(CxmlMessageUtils.generateTimeStamp(po.getRequestedDeliveryDate()));
        }
        createdItem.setQuantity(lineItem.getQuantity().toString());
        if (StringUtils.isNotEmpty(lineItem.getCustomerLineNumber())) {
            createdItem.setLineNumber(lineItem.getCustomerLineNumber());
        }
        else {
            createdItem.setLineNumber(Integer.toString(lineItem.getLineNumber()));
        }
        final ItemID itemID = new ItemID();
        final SupplierPartAuxiliaryID supplierPartAuxiliaryID = new SupplierPartAuxiliaryID();
        if (StringUtils.isBlank(lineItem.getSupplierPartAuxiliaryId())) {
            supplierPartAuxiliaryID.getContent().add(lineItem.getSupplierPartId());
        }
        else {
            supplierPartAuxiliaryID.getContent().add(lineItem.getSupplierPartAuxiliaryId());
        }
        itemID.setSupplierPartAuxiliaryID(supplierPartAuxiliaryID);
        itemID.setSupplierPartID(lineItem.getSupplierPartId());
        createdItem.setItemID(itemID);
        final ItemDetail itemDetail = new ItemDetail();
        final UnitPrice unitPrice = new UnitPrice();
        final Money money = new Money();
        money.setCurrency(US_DOLLARS);
        money.setvalue(lineItem.getCost().toPlainString());
        unitPrice.setMoney(money);
        itemDetail.setUnitPrice(unitPrice);
        final Description description = new Description();
        description.setvalue(lineItem.getDescription());
        itemDetail.getDescription().add(description);
        VendorDto vendor = po.getVendor();
        if (vendor == null || StringUtils.isBlank(vendor.getName())) {
            vendor = new VendorDto();
            vendor.setName(lineItem.getVendorName());
        }
        UnitOfMeasureDto vendorUom = unitOfMeasureBp.convertToVendorUom(vendor, lineItem.getUnitOfMeasure());
        itemDetail.setUnitOfMeasure(vendorUom.getName());
        if (StringUtils.isEmpty(lineItem.getUnspscClassification())) {
            final Classification classification = new Classification();
            classification.setDomain("UNSPSC");
            classification.setvalue(lineItem.getUnspscClassification());
            itemDetail.getClassification().add(classification);
        }
        itemDetail.setLeadTime("0");//default for not supported feature
        //We only support this requester data on the order level but some users want this on each line item
        String taxableField = getMappedField(fields, CashoutPageXFieldDto.CxmlMapping.TAXABLE.getMapping());
        if (taxableField != null) {
            String value = getValueByField(taxableField, po, lineItem);
            if (StringUtils.isEmpty(value)) {
                value = getDefaultValueForMappedField(fields, CashoutPageXFieldDto.CxmlMapping.TAXABLE.getMapping());
            }
            if (StringUtils.isNotEmpty(value)) {
                itemDetail.getExtrinsic().add(createExtrinsic(taxableField, value));
            }
        }

        String countyField = getMappedField(fields, CashoutPageXFieldDto.CxmlMapping.COUNTY.getMapping());
        if (countyField != null) {
            String value = getValueByField(countyField, po, lineItem);
            if (StringUtils.isEmpty(value)) {
                value = getDefaultValueForMappedField(fields, CashoutPageXFieldDto.CxmlMapping.COUNTY.getMapping());
            }
            if (StringUtils.isNotEmpty(value)) {
                itemDetail.getExtrinsic().add(createExtrinsic(countyField, value));
            }
        }

        String requesterField = getMappedField(fields, CashoutPageXFieldDto.CxmlMapping.REQUESTER.getMapping());
        if (requesterField != null) {
            String value = getValueByField(requesterField, po, lineItem);
            if (StringUtils.isEmpty(value)) {
                value = getDefaultValueForMappedField(fields, CashoutPageXFieldDto.CxmlMapping.REQUESTER.getMapping());
            }
            itemDetail.getExtrinsic().add(createExtrinsic(requesterField, value));
        }
        String requesterPhoneField = getMappedField(fields, CashoutPageXFieldDto.CxmlMapping.REQUESTER_PHONE
                .getMapping());
        if (requesterPhoneField != null) {
            String value = getValueByField(requesterPhoneField, po, lineItem);
            if (StringUtils.isEmpty(value)) {
                value = getDefaultValueForMappedField(fields, CashoutPageXFieldDto.CxmlMapping.REQUESTER_PHONE
                        .getMapping());
            }
            itemDetail.getExtrinsic().add(createExtrinsic(requesterPhoneField, value));
        }
        String prNumberField = getMappedField(fields, CashoutPageXFieldDto.CxmlMapping.PR_NO.getMapping());
        if (prNumberField != null && getItemValueByField(prNumberField, lineItem) != null) {
            itemDetail.getExtrinsic().add(
                    createItemExtrinsic(prNumberField, lineItem, CashoutPageXFieldDto.CxmlMapping.PR_NO.getMapping()));
        }
        String alternativeCustomerSkuField = getMappedField(fields, CashoutPageXFieldDto.CxmlMapping.NOVANT_PART_NUMBER
                .getMapping());
        if (alternativeCustomerSkuField != null
                && getItemValueByField(CashoutPageXFieldDto.CxmlMapping.NOVANT_PART_NUMBER.getMapping(), lineItem) != null) {
            itemDetail.getExtrinsic().add(
                    createItemExtrinsic(prNumberField, lineItem, CashoutPageXFieldDto.CxmlMapping.NOVANT_PART_NUMBER
                            .getMapping()));
        }
        String manufactuterPartNumberField = getMappedField(fields,
                CashoutPageXFieldDto.CxmlMapping.MANUFACTURER_PART_NUMBER.getMapping());
        if (manufactuterPartNumberField != null && getItemValueByField(manufactuterPartNumberField, lineItem) != null) {
            itemDetail.getExtrinsic().add(
                    createItemExtrinsic(manufactuterPartNumberField, lineItem,
                            CashoutPageXFieldDto.CxmlMapping.MANUFACTURER_PART_NUMBER.getMapping()));
        }
        String totalEachesField = getMappedField(fields, CashoutPageXFieldDto.CxmlMapping.TOTAL_EACHES.getMapping());
        if (totalEachesField != null && getItemValueByField(totalEachesField, lineItem) != null) {
            itemDetail.getExtrinsic().add(
                    createItemExtrinsic(totalEachesField, lineItem, CashoutPageXFieldDto.CxmlMapping.TOTAL_EACHES
                            .getMapping()));
        }
        String noChargeItemField = getMappedField(fields, CashoutPageXFieldDto.CxmlMapping.NO_CHARGE_ITEM.getMapping());
        if (noChargeItemField != null && getItemValueByField(noChargeItemField, lineItem) != null) {
            itemDetail.getExtrinsic().add(
                    createItemExtrinsic(noChargeItemField, lineItem, CashoutPageXFieldDto.CxmlMapping.NO_CHARGE_ITEM
                            .getMapping()));
        }

        createdItem.getItemDetailOrBlanketItemDetail().add(itemDetail);
        final Tax tax = new Tax();
        final Money taxMoney = new Money();
        taxMoney.setCurrency(US_DOLLARS);
        if (lineItem.getMaximumTaxToCharge() != null) {
            taxMoney.setvalue(lineItem.getMaximumTaxToCharge().toPlainString());
        }
        else {
            taxMoney.setvalue(ZERO_DOLLARS);
        }
        tax.setMoney(taxMoney);
        createdItem.setTax(tax);
        if (lineItemShipToEnabled) {
            final Shipping shipping = new Shipping();
            final Money shipMoney = new Money();
            if (lineItem.getEstimatedShippingAmount() != null) {
                shipMoney.setvalue(lineItem.getEstimatedShippingAmount().toPlainString());
            }
            else {
                shipMoney.setvalue(ZERO_DOLLARS);
            }
            shipMoney.setCurrency(US_DOLLARS);
            shipping.setMoney(shipMoney);
            final Description shippingDescription = new Description();
            shippingDescription.setXmlLang(CxmlMessageUtils.DEFAULT_LOCALE);
            shippingDescription.setvalue(DEFAULT_SHIPPING_DESCRIPTION);
            shipping.setDescription(shippingDescription);
            createdItem.setShipping(shipping);
        }

        return createdItem;
    }

    /**Cashout page x fields map from a database object to field name in the cxml request.
     * 
     * @param cashoutFields All the fields/MappingRelationships being used by this credential
     * @param mapping The mapping specifying a spot on the cxml order request
     * @return A field, which corresponds to a spot on the order request
     */

    private static String getMappedField(Set<CashoutPageXFieldDto> cashoutFields, String mapping) {
        String retString = null;
        for (CashoutPageXFieldDto field : cashoutFields) {
            if (mapping.equals(field.getMessageMapping())) {
                retString = field.getField();
                break;
            }
        }
        return retString;
    }

    private static String getDefaultValueForMappedField(Set<CashoutPageXFieldDto> cashoutFields, String mapping) {
        String retString = null;
        for (CashoutPageXFieldDto field : cashoutFields) {
            if (mapping.equals(field.getMessageMapping())) {
                retString = field.getDefaultValue();
                break;
            }
        }
        return retString;
    }

    private static String getValueByField(String field, PurchaseOrderDto order, LineItemDto item) {
        if (field == null) {
            LOG
                    .warn("Attempted to retrieve property from PurchaseOrderDto using a null field name during CXML OrderRequest translation.");
            return null;
        }
        switch (field) {
            case "Taxable":
                if (item != null && item.getTaxable() != null) {
                    return item.getTaxable() ? "Yes" : "No";
                }
                else if (order != null && !CollectionUtils.isEmpty(order.getItems())
                        && order.getItems().get(0).getTaxable() != null) {
                    return order.getItems().get(0).getTaxable() ? "Yes" : "No";
                }
                else {
                    return order.getTaxTotal().compareTo(BigDecimal.ZERO) == 1 ? "Yes" : "No";
                }
            case "County":
                return order.getShipToAddressDto().getCounty();
            case "refpo":
                return order.getApdPoNumber();
            case "ship name":
                return order.getShipToAddressDto().getName();
            case "ship company":
                return order.getShipToAddressDto().getCompanyName();
            case "ship address 1":
                return order.getShipToAddressDto().getLine1();
            case "ship address 2":
                return order.getShipToAddressDto().getLine2();
            case "ship city":
                return order.getShipToAddressDto().getCity();
            case "ship zip":
                return order.getShipToAddressDto().getZip();
            case "ship contiguous states":
            case "ship states":
                return order.getShipToAddressDto().getState();
            case "ship us and canada":
            case "ship us only":
            case "ship north america":
                return order.getShipToAddressDto().getCountry();
            case "ship date":
                return CxmlMessageUtils.generateTimeStamp(order.getShipDate());
            case "delivery date":
                return CxmlMessageUtils.generateTimeStamp(order.getDeliveryDate());
            case "cancel date":
                return CxmlMessageUtils.generateTimeStamp(order.getCancelDate());
            case "desktop":
            case "Desktop":
            case "desktop free":
                return order.getShipToAddressDto().getMiscShipToDto().getDesktop();
            case "building or department":
                return order.getShipToAddressDto().getMiscShipToDto().getDepartment();
            case "cost center":
            case "existing shipto address selection":
            case "existing billto address selection":
            case "line item comments":
                LOG.error("Invalid mapping:" + field + ". Expected singular result");
                break;
            case "shipping method":
                return order.getShipToAddressDto().getMiscShipToDto().getShippingMethod();
            case "custom po number":
                return getPoNumberByType(order, "customer");
            case "blanket po number":
                return getPoNumberByType(order, "blanket");
            case "email":
                if (order.getShipToAddressDto().getMiscShipToDto() != null
                        && StringUtils.isNotBlank(order.getShipToAddressDto().getMiscShipToDto().getOrderEmail())) {
                    return order.getShipToAddressDto().getMiscShipToDto().getOrderEmail();
                }
                else if (order.getShipToAddressDto().getPersonDto() != null
                        && StringUtils.isNotBlank(order.getShipToAddressDto().getPersonDto().getEmail())) {
                    return order.getShipToAddressDto().getPersonDto().getEmail();
                }
                else {
                    Properties emailsLoader = TenantConfigRepository.getInstance().getPropertiesByTenantId(
                            CurrentTenantIdentifierResolverImpl.getCurrentTenant(), "tenant");
                    return emailsLoader.getProperty("sales_email_address");
                }
            case "credit card name on card":
                if (order.getPaymentInformationDto() != null) {
                    return order.getPaymentInformationDto().getNameOnCard();
                }
                return null;
            case "credit card number":
                if (order.getPaymentInformationDto() != null) {
                    return order.getPaymentInformationDto().getCardNumber();
                }
                return null;
            case "credit card expiration":
                if (order.getPaymentInformationDto() != null) {
                    return order.getPaymentInformationDto().getExpiration().toString();
                }
                return null;
            case "payment type selection":
                if (order.getPaymentInformationDto() != null) {
                    return order.getPaymentInformationDto().getPaymentType();
                }
                return null;
            case "bill name":
                return order.lookupBillToAddressDto().getName();
            case "bill company":
                return order.lookupBillToAddressDto().getCompanyName();
            case "bill address 1":
                return order.lookupBillToAddressDto().getLine1();
            case "bill address 2":
                return order.lookupBillToAddressDto().getLine2();
            case "bill city":
                return order.lookupBillToAddressDto().getCity();
            case "bill zip":
                return order.lookupBillToAddressDto().getZip();
            case "bill states":
            case "bill contiguos states":
                return order.lookupBillToAddressDto().getState();
            case "bill north america":
            case "bill us only":
                return order.lookupBillToAddressDto().getCountry();
            case "comments":
                String commentJoin = "";
                for (String comment : order.getComments()) { //single comment case
                    commentJoin = commentJoin + comment;
                }
                return commentJoin;
            case "order type":
                return order.getOrderType().getValue();
            case "AddressID":
                String addressId = order.getShipToAddressDto().getMiscShipToDto().getAddressId();
                if (StringUtils.isBlank(addressId)) {
                    addressId = order.getShipToAddressDto().getShipToId();
                }
                return addressId;
            case "AddressID(base 36)":
                return order.getShipToAddressDto().getMiscShipToDto().getAddressId36();
            case "Contact Name":
                return order.getShipToAddressDto().getMiscShipToDto().getContactName();
            case "APD_InterchangeID":
                return order.getShipToAddressDto().getMiscShipToDto().getApdInterchangeId();
            case "CarrierRouting":
                return order.getShipToAddressDto().getMiscShipToDto().getCarrierRouting();
            case "DeliveryDate":
                return order.getShipToAddressDto().getMiscShipToDto().getDeliveryDate();
            case "Dept":
                return order.getShipToAddressDto().getMiscShipToDto().getDept();
            case "Facility":
                return order.getShipToAddressDto().getMiscShipToDto().getFacility();
            case "Fedex No":
                return order.getShipToAddressDto().getMiscShipToDto().getFedexNumber();
            case "LocationID":
                return order.getShipToAddressDto().getMiscShipToDto().getLocationId();
            case "Mailstop":
                return order.getShipToAddressDto().getMiscShipToDto().getMailStop();
            case "NOWL":
                return order.getShipToAddressDto().getMiscShipToDto().getNowl();
            case "POL":
                return order.getShipToAddressDto().getMiscShipToDto().getPol();
            case "SolomonID":
                return order.getShipToAddressDto().getMiscShipToDto().getSolomonId();
            case "USAccount":
                return order.getShipToAddressDto().getMiscShipToDto().getUsAccount();
            case "Header Comments":
                return order.getShipToAddressDto().getMiscShipToDto().getHeaderComments();
            case "OldRelease":
                return order.getShipToAddressDto().getMiscShipToDto().getOldRelease();
            case "Order Email":
                if (order.getShipToAddressDto().getMiscShipToDto() != null
                        && StringUtils.isNotBlank(order.getShipToAddressDto().getMiscShipToDto().getOrderEmail())) {
                    return order.getShipToAddressDto().getMiscShipToDto().getOrderEmail();
                }
                else if (order.getShipToAddressDto().getPersonDto() != null
                        && StringUtils.isNotBlank(order.getShipToAddressDto().getPersonDto().getEmail())) {
                    return order.getShipToAddressDto().getPersonDto().getEmail();
                }
                else {
                    Properties emailsLoader = TenantConfigRepository.getInstance().getPropertiesByTenantId(
                            CurrentTenantIdentifierResolverImpl.getCurrentTenant(), "tenant");
                    return emailsLoader.getProperty("sales_email_address");
                }
            case "Street 2":
                return order.getShipToAddressDto().getMiscShipToDto().getStreet2();
            case "Street 3":
                return order.getShipToAddressDto().getMiscShipToDto().getStreet3();
            case "requester name":
                return order.getShipToAddressDto().getMiscShipToDto().getRequesterName();
            case "requestername":
                return order.getShipToAddressDto().getMiscShipToDto().getRequesterName();
            case "requester phone":
            case "Contact Phone":
                return order.getShipToAddressDto().getMiscShipToDto().getRequesterPhone();
            case "apd po":
                return order.getApdPoNumber();
            case "APD_AccountingID":
                return order.getAccount().getSolomonCustomerID();
            case "fulfillment":
                return (String) order.getCredential().getProperties().get(FULFILLMENT.getValue());
            case "paymenttype":
                return order.getPaymentInformationDto().getPaymentType();

            default:
                LOG.error("Could not recognize mapping:" + field);
                return null;

        }
        return null;
    }

    private static String getPoNumberByType(PurchaseOrderDto order, String type) {
        for (PoNumberDto poNum : order.getPoNumbers()) {
            if (type.equals(poNum.getType())) {
                return poNum.getValue();
            }
        }
        return null;
    }

    private static Extrinsic createExtrinsic(String name, String value) {
        final Extrinsic extrinsic = new Extrinsic();
        extrinsic.setName(name);
        extrinsic.getContent().add(value);
        return extrinsic;
    }

    public static com.apd.phoenix.service.integration.cxml.model.cxml.CXML generateCurrentCXMLObject() {
        com.apd.phoenix.service.integration.cxml.model.cxml.CXML cxml = new com.apd.phoenix.service.integration.cxml.model.cxml.CXML();
        cxml.setTimestamp(CxmlMessageUtils.generateTimeStamp());
        cxml.setVersion(CxmlMessageUtils.CXML_VERSION);
        cxml.setXmlLang(CxmlMessageUtils.DEFAULT_LOCALE);
        return cxml;
    }

    private static Object getItemValueByField(String field, LineItemDto lineItem) {
        switch (field) {
            case "PR No.":
            case "Total Eaches":
            case "No Charge Item":
                return lineItem.getItem().getProperties().get(field);
            case "alternative customer sku":
                return lineItem.getItem().lookupCustomerSku();
            case "Manufacturer Part Number":
                return lineItem.getManufacturerPartId();
        }
        return null;
    }

    private static Extrinsic createItemExtrinsic(String field, LineItemDto lineItem, String name) {
        final Extrinsic extrinsic = new Extrinsic();
        extrinsic.setName(name);
        extrinsic.getContent().add(getItemValueByField(field, lineItem));
        return extrinsic;
    }
}
