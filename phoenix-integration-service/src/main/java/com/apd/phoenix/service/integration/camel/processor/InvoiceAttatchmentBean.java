package com.apd.phoenix.service.integration.camel.processor;

import java.io.InputStream;
import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Singleton;
import javax.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.business.OrderLogBp;
import com.apd.phoenix.service.message.api.MessageUtils;
import com.apd.phoenix.service.model.dto.CustomerInvoiceDto;
import com.apd.phoenix.service.model.dto.InvoiceDto;
import com.apd.phoenix.service.report.ReportService;
import com.apd.phoenix.service.utility.ErrorEmailService;

@Singleton
@Lock(LockType.READ)
public class InvoiceAttatchmentBean {

    private static final Logger LOG = LoggerFactory.getLogger(InvoiceAttatchmentBean.class);

    @Inject
    OrderLogBp orderLogBp;

    @Inject
    MessageUtils messageUtils;

    @Inject
    ReportService reportService;

    @Inject
    ErrorEmailService errorEmailService;

    public InputStream retrieveInvoice(InvoiceDto invoice) {
        InputStream stream = null;
        if (invoice instanceof CustomerInvoiceDto) {
            try {
                stream = reportService.generateInvoiceDebitPdf(invoice.getDatabaseId().toString());
                if (stream == null) {
                    sendErrorEmail(invoice);
                }
            }
            catch (Exception e) {
                sendErrorEmail(invoice);
            }
        }
        else {
            try {
                stream = reportService.generateInvoiceCreditPdf(invoice.getDatabaseId().toString());
                if (stream == null) {
                    sendErrorEmail(invoice);
                }
            }
            catch (Exception e) {
                sendErrorEmail(invoice);
            }
        }
        return stream;

    }

    private void sendErrorEmail(InvoiceDto invoice) {
        LOG.error("Could not find attatchement for invoice number " + invoice.getInvoiceNumber());
        errorEmailService.sendMarquetteErrorEmail(invoice, "There was a Jasper error");
    }
}
