package com.apd.phoenix.service.integration.routes;

import org.apache.camel.LoggingLevel;
import com.apd.phoenix.service.integration.camel.processor.WorkflowServiceCaller;
import com.apd.phoenix.service.integration.multitenancy.AbstractTenantAwareRouteBuilder;

public class DenyOrderRouteBuilder extends AbstractTenantAwareRouteBuilder {

    private String denyOrderQueue;

    @Override
    public void configureRouteImplementation() throws Exception {
        onException(Exception.class).maximumRedeliveries(0).handled(true).log(LoggingLevel.INFO,
                "${exception.stacktrace}");

        from(denyOrderQueue).bean(WorkflowServiceCaller.class, "denyOrder");

    }

    public String getDenyOrderQueue() {
        return denyOrderQueue;
    }

    public void setDenyOrderQueue(String denyOrderQueue) {
        this.denyOrderQueue = denyOrderQueue;
    }

}
