package com.apd.phoenix.service.integration.camel.processor.xcbl.osn;

import java.text.SimpleDateFormat;
import java.util.Date;
import javax.inject.Inject;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import org.apache.camel.Exchange;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import rrn.org_xcbl.schemas.xcbl.v3_5.xcbl35.OSNOrder;
import rrn.org_xcbl.schemas.xcbl.v3_5.xcbl35.OSNOrderResponse;
import com.apd.phoenix.service.integration.camel.processor.MessageTranslator;
import com.apd.phoenix.service.integration.camel.processor.xcbl.XCBLOrderResponseTranslator;
import com.apd.phoenix.service.integration.exception.MessageTranslationException;
import com.apd.phoenix.service.model.dto.POAcknowledgementDto;
import com.perfect.liteenvelope.xcbl.EnvelopeType;
import com.perfect.liteenvelope.xcbl.EnvelopeType.DocAttributes;
import com.perfect.liteenvelope.xcbl.IdentificationAttributesType;
import com.perfect.liteenvelope.xcbl.TradingPartnerAttributesType;
import com.perfect.liteenvelope.xcbl.TransmissionAttributesType;

public class OSNOrderResponseTranslator implements MessageTranslator<POAcknowledgementDto, OSNOrderResponse> {

    private static final Logger LOGGER = LoggerFactory.getLogger(OSNOrderResponseTranslator.class);

    public static final String XML_GREGORIAN_DATETIME_FORMAT = "yyyy-MM-dd'T'HH:mm:ss";

    @Inject
    private XCBLOrderResponseTranslator xcblOrderResponseTranslator;

    @Override
    public OSNOrderResponse fromCanonical(POAcknowledgementDto poAcknowledgement) throws MessageTranslationException {
        return null;
    }

    @Override
    public POAcknowledgementDto toCanonical(OSNOrderResponse externalBinding) throws MessageTranslationException {
        throw new UnsupportedOperationException();
    }

    public void process(Exchange exchange) throws Exception {
        POAcknowledgementDto poAck = (POAcknowledgementDto) exchange.getIn().getHeader("poAck");
        OSNOrder osnOrder = (OSNOrder) exchange.getIn().getBody();

        OSNOrderResponse osnOrderResponse = new OSNOrderResponse();
        osnOrderResponse
                .setOrderResponse(xcblOrderResponseTranslator.generateOrderResponse(osnOrder.getOrder(), poAck));

        EnvelopeType envelope = new EnvelopeType();
        DocAttributes docAttributes = new DocAttributes();
        IdentificationAttributesType identificationAttributes = new IdentificationAttributesType();
        String envelopeId = (String) exchange.getIn().getHeader("interchangeId");
        identificationAttributes.setEnvid(envelopeId);
        docAttributes.setIdentificationAttributes(identificationAttributes);

        TransmissionAttributesType transmissionAttributes = new TransmissionAttributesType();
        SimpleDateFormat dateFormat = new SimpleDateFormat(XML_GREGORIAN_DATETIME_FORMAT);
        XMLGregorianCalendar transmissionDate = DatatypeFactory.newInstance().newXMLGregorianCalendar(
                dateFormat.format(new Date()));
        transmissionAttributes.setTransDatetime(transmissionDate);
        docAttributes.setTransmissionAttributes(transmissionAttributes);

        TradingPartnerAttributesType senderAttributes = new TradingPartnerAttributesType();
        // NOTE:  OSNOrderResponse sender = OSNOrder receiver
        String senderId = osnOrder.getEnvelope().getDocAttributes().getRecipientAttributes().getID();
        senderAttributes.setID(senderId);
        docAttributes.setSenderAttributes(senderAttributes);

        TradingPartnerAttributesType recipientAttributes = new TradingPartnerAttributesType();
        // NOTE: OSNOrderResponse receiver = OSNOrder sender
        String receiverId = osnOrder.getEnvelope().getDocAttributes().getSenderAttributes().getID();
        recipientAttributes.setID(receiverId);
        docAttributes.setRecipientAttributes(recipientAttributes);

        envelope.setDocAttributes(docAttributes);
        osnOrderResponse.setEnvelope(envelope);
        exchange.getOut().setBody(osnOrderResponse);
        exchange.getOut().setHeaders(exchange.getIn().getHeaders());
        exchange.getOut().setHeader("envelopeId", envelopeId);
        exchange.getOut().setHeader("senderId", senderId);
        exchange.getOut().setHeader("receiverId", receiverId);
    }
}
