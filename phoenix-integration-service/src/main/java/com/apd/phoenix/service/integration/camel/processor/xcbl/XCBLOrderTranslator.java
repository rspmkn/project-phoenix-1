package com.apd.phoenix.service.integration.camel.processor.xcbl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import rrn.org_xcbl.schemas.xcbl.v3_5.xcbl35.AgencyCode;
import rrn.org_xcbl.schemas.xcbl.v3_5.xcbl35.Contact;
import rrn.org_xcbl.schemas.xcbl.v3_5.xcbl35.ContactNumber;
import rrn.org_xcbl.schemas.xcbl.v3_5.xcbl35.ContactNumberTypeCode;
import rrn.org_xcbl.schemas.xcbl.v3_5.xcbl35.CountryCode;
import rrn.org_xcbl.schemas.xcbl.v3_5.xcbl35.Discounts;
import rrn.org_xcbl.schemas.xcbl.v3_5.xcbl35.ItemDetail;
import rrn.org_xcbl.schemas.xcbl.v3_5.xcbl35.ItemIdentifiers;
import rrn.org_xcbl.schemas.xcbl.v3_5.xcbl35.ItemIdentifiers.CommodityCode;
import rrn.org_xcbl.schemas.xcbl.v3_5.xcbl35.NameAddress;
import rrn.org_xcbl.schemas.xcbl.v3_5.xcbl35.Order;
import rrn.org_xcbl.schemas.xcbl.v3_5.xcbl35.OrderDetail;
import rrn.org_xcbl.schemas.xcbl.v3_5.xcbl35.OrderHeader;
import rrn.org_xcbl.schemas.xcbl.v3_5.xcbl35.OrderReferences;
import rrn.org_xcbl.schemas.xcbl.v3_5.xcbl35.OrderSummary;
import rrn.org_xcbl.schemas.xcbl.v3_5.xcbl35.Party;
import rrn.org_xcbl.schemas.xcbl.v3_5.xcbl35.PaymentInstructions;
import rrn.org_xcbl.schemas.xcbl.v3_5.xcbl35.PaymentMethod;
import rrn.org_xcbl.schemas.xcbl.v3_5.xcbl35.PaymentTerm;
import rrn.org_xcbl.schemas.xcbl.v3_5.xcbl35.PaymentTerms;
import rrn.org_xcbl.schemas.xcbl.v3_5.xcbl35.Price;
import rrn.org_xcbl.schemas.xcbl.v3_5.xcbl35.Quantity;
import rrn.org_xcbl.schemas.xcbl.v3_5.xcbl35.ReferenceCoded;
import rrn.org_xcbl.schemas.xcbl.v3_5.xcbl35.RegionCode;
import rrn.org_xcbl.schemas.xcbl.v3_5.xcbl35.ScheduleLine;
import rrn.org_xcbl.schemas.xcbl.v3_5.xcbl35.UnitPrice;
import com.apd.phoenix.service.business.CredentialBp;
import com.apd.phoenix.service.business.SkuTypeBp;
import com.apd.phoenix.service.business.VendorBp;
import com.apd.phoenix.service.integration.camel.processor.MessageTranslator;
import com.apd.phoenix.service.integration.exception.MessageTranslationException;
import com.apd.phoenix.service.model.Credential;
import com.apd.phoenix.service.model.LineItemStatus.LineItemStatusEnum;
import com.apd.phoenix.service.model.PaymentType.PaymentTypeEnum;
import com.apd.phoenix.service.model.Vendor;
import com.apd.phoenix.service.model.dto.AddressDto;
import com.apd.phoenix.service.model.dto.CredentialTermsDto;
import com.apd.phoenix.service.model.dto.LineItemDto;
import com.apd.phoenix.service.model.dto.LineItemStatusDto;
import com.apd.phoenix.service.model.dto.MiscShipToDto;
import com.apd.phoenix.service.model.dto.PaymentInformationDto;
import com.apd.phoenix.service.model.dto.PersonDto;
import com.apd.phoenix.service.model.dto.PurchaseOrderDto;
import com.apd.phoenix.service.model.dto.SkuDto;
import com.apd.phoenix.service.model.dto.SkuTypeDto;
import com.apd.phoenix.service.model.dto.TransactionDto.XCBLTransactionPurpose;
import com.apd.phoenix.service.model.dto.UnitOfMeasureDto;
import com.apd.phoenix.service.model.factory.DtoFactory;

@LocalBean
@Stateless
public class XCBLOrderTranslator implements MessageTranslator<PurchaseOrderDto, Order> {

    private static final Logger LOG = LoggerFactory.getLogger(XCBLOrderTranslator.class);

    public static final String DEFAULT_CURRENCY_CODE = "USD";
    public static final String DEFAULT_LANGUAGE_CODE = "en";
    public static final String DEFAULT_UOM_CODE = "EA";
    public static final String DEFAULT_SELLER_NAME = "American Product Distributors";
    public static final String UOM_CODED_OTHER = "Other";
    public static final String PAYMENT_TERM_CODED_OTHER = "Other";
    public static final String PAYMENT_MEAN_CODED_BANKCARD = "BankCard";
    public static final String[] XCBL_ORDER_DATE_FORMATS = new String[] { "yyyyMMdd'T'HH:mm:ssXXX",
            "yyyyMMdd'T'HH:mm:ss" };

    public static final String ITEM_PART_ID_DELIMITER = "|";

    public static final String VENDOR_CUSTOM_ITEM_INDICATOR = "$$";
    public static final String MINIMUM_ORDER_FEE = "Minimum Order Fee";

    @Inject
    private CredentialBp credentialBp;

    @Inject
    private VendorBp vendorBp;

    @Override
    public Order fromCanonical(PurchaseOrderDto purchaseOrderDto) {
        throw new UnsupportedOperationException();
    }

    @Override
    public PurchaseOrderDto toCanonical(Order order) throws MessageTranslationException {
        if (LOG.isDebugEnabled()) {
            LOG.debug("body: {}", order);
        }
        PurchaseOrderDto po = new PurchaseOrderDto();
        try {
            processOrderHeader(order.getOrderHeader(), po);
            if (order.getOrderDetail() != null) {
                processOrderDetail(order.getOrderDetail(), po);
            }
            if (order.getOrderSummary() != null) {
                processOrderSummary(order.getOrderSummary(), po);
            }
        }
        catch (Exception e) {
            LOG.error(e.getMessage(), e);
            throw new MessageTranslationException(e + "");
        }
        return po;
    }

    private void processOrderHeader(OrderHeader xcblOrderHeader, PurchaseOrderDto po)
            throws MessageTranslationException {
        if (xcblOrderHeader.getPurpose().getPurposeCoded() != null) {
            String xcblPurposeCoded = xcblOrderHeader.getPurpose().getPurposeCoded().value();
            po.setTransactionOperation(XCBLTransactionPurpose.valueOf(xcblPurposeCoded).getCanonicalModel());
        }
        po.setCustomerPoNumber(xcblOrderHeader.getOrderNumber().getBuyerOrderNumber());
        if (xcblOrderHeader.getOrderNumber().getListOfMessageID() != null
                && !xcblOrderHeader.getOrderNumber().getListOfMessageID().getMessageID().isEmpty()) {
            po.setInterchangeId(xcblOrderHeader.getOrderNumber().getListOfMessageID().getMessageID().get(0)
                    .getIDNumber());
        }
        if (StringUtils.isNotBlank(po.getInterchangeId())) {
            po.setGroupId(po.getInterchangeId());
            po.setTransactionId(po.getTransactionId());
        }

        try {
            processOrderReferences(xcblOrderHeader.getOrderReferences(), po);
            po.setOrderDate(DateUtils.parseDate(xcblOrderHeader.getOrderIssueDate(), XCBL_ORDER_DATE_FORMATS));
            po.setCurrencyCode(DEFAULT_CURRENCY_CODE);
            if (xcblOrderHeader.getOrderDates() != null
                    && StringUtils.isNotBlank(xcblOrderHeader.getOrderDates().getRequestedDeliverByDate())) {
                po.setRequestedDeliveryDate(DateUtils.parseDate(xcblOrderHeader.getOrderDates()
                        .getRequestedDeliverByDate(), XCBL_ORDER_DATE_FORMATS));
            }

            // Required parties
            processBuyerParty(xcblOrderHeader.getOrderParty().getBuyerParty().getParty(), po);
            processSellerParty(xcblOrderHeader.getOrderParty().getSellerParty().getParty(), po);

            // Optional parties
            if (xcblOrderHeader.getOrderParty().getBillToParty() != null) {
                processBillToParty(xcblOrderHeader.getOrderParty().getBillToParty().getParty(), po);
            }
            if (xcblOrderHeader.getOrderParty().getShipToParty() != null) {
                processShipToParty(xcblOrderHeader.getOrderParty().getShipToParty().getParty(), po);
            }
            if (xcblOrderHeader.getOrderParty().getSoldToParty() != null) {
                processSoldToParty(xcblOrderHeader.getOrderParty().getSoldToParty().getParty(), po);
            }

            if (xcblOrderHeader.getOrderPaymentInstructions() != null) {
                po.setTerms(new CredentialTermsDto());
                processPaymentInstructions(xcblOrderHeader.getOrderPaymentInstructions().getPaymentInstructions(), po);
            }
            if (StringUtils.isNotBlank(xcblOrderHeader.getOrderHeaderNote())) {
                po.setComments(new ArrayList<String>());
                po.getComments().add(xcblOrderHeader.getOrderHeaderNote());
            }
        }
        catch (Exception e) {
            LOG.error(e.getMessage(), e);
            throw new MessageTranslationException(e.toString());
        }
    }

    private static void processOrderSummary(OrderSummary xcblOrderSummary, PurchaseOrderDto po) {
        if (xcblOrderSummary.getTotalAmount() != null) {
            po.setSubTotal(xcblOrderSummary.getTotalAmount().getMonetaryValue().getMonetaryAmount());
            po.setCurrencyCode(DEFAULT_CURRENCY_CODE);
        }
        if (xcblOrderSummary.getTotalTax() != null) {
            po.setTaxTotal(xcblOrderSummary.getTotalTax().getMonetaryValue().getMonetaryAmount());
        }
        if (xcblOrderSummary.getNumberOfLines() != null && po.getItems() != null
                && !xcblOrderSummary.getNumberOfLines().equals(po.getItems().size())) {
            LOG
                    .warn("The number of lines listed in the order summary does not match the number of lines read from the order!");
        }
    }

    private static void processOrderReferences(OrderReferences orderReferences, PurchaseOrderDto po) throws Exception {

        //    	if (orderReferences.getAccountCode() != null && orderReferences.getAccountCode().getReference() != null) {
        //    		Credential credential = credentialBp.findCredentialByDuns(
        //    				orderReferences.getAccountCode().getReference().getRefNum());
        //    	}
        try {
            if (orderReferences.getOtherOrderReferences() != null
                    && orderReferences.getOtherOrderReferences().getListOfReferenceCoded() != null
                    && !orderReferences.getOtherOrderReferences().getListOfReferenceCoded().getReferenceCoded()
                            .isEmpty()) {
                for (ReferenceCoded referenceCoded : orderReferences.getOtherOrderReferences()
                        .getListOfReferenceCoded().getReferenceCoded()) {
                    if (referenceCoded.getReferenceTypeCoded().value().equals("PurchaseOrderRevisionNumber")) {
                        po.setCustomerRevisionNumber(referenceCoded.getPrimaryReference().getReference().getRefNum());
                        po.setCustomerRevisionDate(DateUtils.parseDate(referenceCoded.getPrimaryReference()
                                .getReference().getRefDate(), XCBL_ORDER_DATE_FORMATS));
                    }
                }
            }
        }
        catch (Exception e) {
            LOG.error(e.getMessage(), e);
            throw new Exception(e.toString());
        }
    }

    private void processBuyerParty(Party party, PurchaseOrderDto po) throws Exception {
        if (party.getPartyID().getIdentifier().getAgency().getAgencyCoded().value().equalsIgnoreCase(
                AgencyCode.OTHER.toString())) {
            Credential credential = credentialBp.findCredentialByDuns(party.getPartyID().getIdentifier().getIdent());
            po.setCredential(DtoFactory.createCredentialDto(credential));
        }
        else if (party.getPartyID().getIdentifier().getAgency().getAgencyCoded().value().equalsIgnoreCase(
                AgencyCode.DUN_AND_BRADSTREET.value())) {
            Credential credential = credentialBp.findCredentialByDuns(po.getCustomerPoNumber().substring(0, 2)
                    + party.getPartyID().getIdentifier().getIdent());
            if (credential == null) {
                LOG.error("No credential found for DUNS " + po.getCustomerPoNumber().substring(0, 2)
                        + party.getPartyID().getIdentifier().getIdent());
            }
            po.setCredential(DtoFactory.createCredentialDto(credential));
            po.setAccount(DtoFactory.createAccountDto(credential.getRootAccount()));
        }

        AddressDto billTo = toCanonicalAddress(party.getNameAddress());
        if (party.getNameAddress() != null
                && party.getNameAddress().getIdentifier() != null
                && party.getNameAddress().getIdentifier().getAgency() != null
                && party.getNameAddress().getIdentifier().getAgency().getAgencyCoded().equals(
                        AgencyCode.ASSIGNED_BY_BUYER_OR_BUYERS_AGENT)) {
            if (billTo.getMiscShipToDto() == null) {
                billTo.setMiscShipToDto(new MiscShipToDto());
            }
            billTo.getMiscShipToDto().setAddressId(party.getNameAddress().getIdentifier().getIdent());
        }
        if (party.getOrderContact() != null) {
            populateContactInfo(party.getOrderContact().getContact(), billTo);
        }
        PaymentInformationDto paymentInfo = new PaymentInformationDto();
        paymentInfo.setBillToAddress(billTo);
        po.setPaymentInformationDto(paymentInfo);

        //        final String entityIdentifierCode = shipBillGroup.getName().getEntityIdentifierCode();
        //        if (entityIdentifierCode != null) {
        //            switch (entityIdentifierCode) {
        //                case BILL_TO_CODE:
        //                    AddressDto billToAddresss = new AddressDto();
        //                    billToAddresss.setMiscShipToDto(new MiscShipToDto());
        //                    billToAddresss.getMiscShipToDto().setGlNumber(shipBillGroup.getName().getIdentificationCode());
        //                    poDto.changeBillToAddressDto(billToAddresss);
        //                    break;
        //                case SHIP_TO_DISTRIBUTION_CENTER_CODE:
        //                case SHIP_TO_CODE:
        //                    AddressDto shipToAddress = poDto.getShipToAddressDto();
        //                    final GeographicLocation geographicLocation = shipBillGroup.getGeographicLocation();
        //                    if (geographicLocation != null) {
        //                        shipToAddress.setCity(geographicLocation.getCityName());
        //                        shipToAddress.setState(geographicLocation.getStateOrProvinceCode());
        //                        shipToAddress.setZip(geographicLocation.getPostalCode());
        //                        shipToAddress.setCountry(geographicLocation.getCountryCode());
        //                    }
        //                    if (GEOCODE_QUALIFIER.equals(shipBillGroup.getName().getIdentificationCodeQualifier())) {
        //                        shipToAddress.getMiscShipToDto().setGlNumber(shipBillGroup.getName().getIdentificationCode());
        //                    }
        //                    final AddressInformation addressInformation = shipBillGroup.getAddressInformation();
        //                    if (addressInformation != null) {
        //                        shipToAddress.setLine1(addressInformation.getAddressLine1());
        //                        shipToAddress.setLine2(addressInformation.getAddressLine2());
        //                    }
        //                    break;
        //                default:
        //                    break;
        //
        //            }
        //        }
        //    }

    }

    private void processSellerParty(Party party, PurchaseOrderDto po) throws Exception {
        if (party.getPartyID().getIdentifier().getAgency().getAgencyCoded().value().equals(AgencyCode.OTHER)) {
            //TODO retrieve this from credential and internal supplier account Id?
            Vendor searchVendor = new Vendor();
            searchVendor.setName(DEFAULT_SELLER_NAME);
            po.setVendor(DtoFactory.createVendorDto(vendorBp.searchByExample(searchVendor, 0, 0).get(0)));
        }
    }

    private static void processShipToParty(Party party, PurchaseOrderDto po) throws Exception {
        AddressDto shipTo = toCanonicalAddress(party.getNameAddress());
        if (party.getOrderContact() != null) {
            populateContactInfo(party.getOrderContact().getContact(), shipTo);
        }
        if (party.getShippingContact() != null && party.getShippingContact().getContact() != null) {
            populateContactInfo(party.getShippingContact().getContact(), shipTo);
        }
        else if (party.getReceivingContact() != null) {
            populateContactInfo(party.getReceivingContact().getContact(), shipTo);
        }
        //        if (party.getPartyID().getIdentifier().getAgency().getAgencyCoded().equals(AgencyCode.OTHER)) {
        //            shipTo.setShipToId(party.getPartyID().getIdentifier().getIdent());
        //        }
        if (shipTo.getMiscShipToDto() == null) {
            shipTo.setMiscShipToDto(new MiscShipToDto());
        }
        if (StringUtils.isBlank(shipTo.getMiscShipToDto().getDesktop())) {
            String desktop = generateDesktopFromNameAddress(party.getNameAddress());
            if (StringUtils.isNotBlank(desktop)) {
                shipTo.getMiscShipToDto().setDesktop(desktop);
            }
        }
        po.setShipToAddressDto(shipTo);
    }

    /**
     * Concatenate several location fields into a single string to be used as the Desktop DeliverTo field on the shipto details sent to the vendor.
     *  NOTE: This method assumes the mapping/translation logic does not conflict between customers.
     * @param nameAddress
     * @return
     */
    private static String generateDesktopFromNameAddress(NameAddress nameAddress) {
        String desktop = "";
        if (StringUtils.isNotBlank(nameAddress.getDepartment())) {
            desktop += nameAddress.getDepartment() + " ";
        }
        if (StringUtils.isNotBlank(nameAddress.getStreetSupplement1())) {
            desktop += nameAddress.getStreetSupplement1() + " ";
        }
        if (StringUtils.isBlank(desktop)) {
            if (StringUtils.isNotBlank(nameAddress.getBuilding())) {
                desktop += "Bldg:" + nameAddress.getBuilding() + " ";
            }
            if (StringUtils.isNotBlank(nameAddress.getFloor())) {
                desktop += "Fl:" + nameAddress.getFloor() + " ";
            }
            if (StringUtils.isNotBlank(nameAddress.getRoomNumber())) {
                desktop += "Rm:" + nameAddress.getRoomNumber();
            }
        }
        return desktop.trim();
    }

    private static void processBillToParty(Party party, PurchaseOrderDto po) throws Exception {
    }

    private static void processSoldToParty(Party party, PurchaseOrderDto po) throws Exception {
    }

    private static AddressDto toCanonicalAddress(NameAddress xcblAddress) throws Exception {
        AddressDto address = new AddressDto();
        address.setMiscShipToDto(new MiscShipToDto());
        if (StringUtils.isNotBlank(xcblAddress.getName2())) {
            address.setCompanyName(xcblAddress.getName2());
        }
        else if (StringUtils.isNotBlank(xcblAddress.getName1())) {
            address.setCompanyName(xcblAddress.getName1());
        }

        address.getMiscShipToDto().setDivision(xcblAddress.getName1());
        address.setLine1(xcblAddress.getStreet());
        address.setCity(xcblAddress.getCity());
        address.setCounty(xcblAddress.getCounty());

        String regionCode = "";
        String countryCode = "";
        if (xcblAddress.getCountry() != null) {
            countryCode = xcblAddress.getCountry().getCountryCoded().value();
        }
        if (xcblAddress.getRegion() != null) {
            regionCode = xcblAddress.getRegion().getRegionCoded().value();
            // Extract State code by removing the country code prefix from the region code 
            if (!StringUtils.equals(regionCode, RegionCode.OTHER.value())
                    && !StringUtils.equals(countryCode, CountryCode.OTHER.value())
                    && StringUtils.startsWith(regionCode, countryCode)
                    && (regionCode.length() - countryCode.length()) >= 2) {
                regionCode = regionCode.substring(countryCode.length());
            }
        }
        if (StringUtils.equals(countryCode, CountryCode.OTHER.value())) {
            address.setCountry(xcblAddress.getCountry().getCountryCodedOther());
        }
        else {
            address.setCountry(countryCode);
        }
        if (StringUtils.equals(regionCode, RegionCode.OTHER.value())) {
            address.setState(xcblAddress.getRegion().getRegionCodedOther());
        }
        else {
            address.setState(regionCode);
        }
        if (StringUtils.isNotBlank(xcblAddress.getPostalCode())) {
            address.setZip(xcblAddress.getPostalCode());
        }
        return address;
    }

    private static void populateContactInfo(Contact xcblContact, AddressDto address) {
        PersonDto person = new PersonDto();
        person.setFirstName(xcblContact.getContactName());
        person.setLastName(xcblContact.getContactName());
        if (address.getMiscShipToDto() == null) {
            address.setMiscShipToDto(new MiscShipToDto());
        }

        address.getMiscShipToDto().setRequesterName(xcblContact.getContactName()); //--
        // Contact numbers
        if (xcblContact.getListOfContactNumber() != null) {
            for (ContactNumber xcblContactNumber : xcblContact.getListOfContactNumber().getContactNumber()) {
                if (xcblContactNumber.getContactNumberTypeCoded() != null
                        && xcblContactNumber.getContactNumberTypeCoded().equals(ContactNumberTypeCode.EMAIL_ADDRESS)) {
                    address.getMiscShipToDto().setOrderEmail(xcblContactNumber.getContactNumberValue());
                    person.setEmail(xcblContactNumber.getContactNumberValue());
                }
                else {
                    if (xcblContactNumber.getContactNumberTypeCoded() != null
                            && xcblContactNumber.getContactNumberTypeCoded().equals(
                                    ContactNumberTypeCode.TELEPHONE_NUMBER)) {
                        address.getMiscShipToDto().setRequesterPhone(xcblContactNumber.getContactNumberValue());
                    }
                    //TODO (note: Storing a separate phone number record for the order contact,
                    // in addition to the requestor phone value, is not something they do currently)
                    // PhoneNumberDto phoneNumber = new PhoneNumberDto();
                    // phoneNumber.setValue(xcblContactNumber.getContactNumberValue());
                    // PhoneNumberTypeDto phoneNumberType = new PhoneNumberTypeDto();
                    // phoneNumberType.setName("OrderContact");
                    // phoneNumber.setPhoneNumberTypeDto(phoneNumberType);
                    // person.getPhoneNumberDtos().add(phoneNumber);
                }
            }
        }
        address.setPersonDto(person);
    }

    /**
     * This method assumes there is no more than one element in the set of payment instructions for the order
     * 
     * @param xcblPaymentInstructions
     * @param termsDto
     * @throws Exception
     */
    private static void processPaymentInstructions(PaymentInstructions xcblPaymentInstructions, PurchaseOrderDto po)
            throws Exception {

        if (po.getTerms() == null) {
            po.setTerms(new CredentialTermsDto());
        }
        CredentialTermsDto termsDto = po.getTerms();

        if (xcblPaymentInstructions.getPaymentTerms() != null) {
            if (xcblPaymentInstructions.getPaymentTerms().size() > 1) {
                LOG.warn("More than one order payment instruction set was provided with the inbound xcbl order.  "
                        + "Only the first will be persisted.");
            }
            PaymentTerms paymentTerms = xcblPaymentInstructions.getPaymentTerms().get(0);
            if (paymentTerms.getPaymentTerm() != null) {
                if (paymentTerms.getPaymentTerm().size() > 1) {
                    LOG.warn("More than one payment term was provided with the first order payment instruction "
                            + "set for the inbound xcbl order.  Only the first payment term will be parsed for "
                            + "applicable discount details.");
                }
                PaymentTerm paymentTerm = paymentTerms.getPaymentTerm().get(0);
                if (paymentTerm.getPaymentTermCoded().equals(PAYMENT_TERM_CODED_OTHER)
                        && paymentTerm.getPaymentTermDetails() != null
                        && paymentTerm.getPaymentTermDetails().getDiscounts() != null) {
                    Discounts discounts = paymentTerm.getPaymentTermDetails().getDiscounts();
                    termsDto.setDiscountPercent((discounts.getDiscountPercent() == null ? BigDecimal.ZERO : discounts
                            .getDiscountPercent()).multiply(new BigDecimal("100")).intValue());
                    termsDto.setDaysWhileDiscounted(discounts.getDiscountDaysDue());
                    termsDto.setDaysUntilDue(discounts.getNetDaysDue());
                }
            }
        }
        if (xcblPaymentInstructions.getPaymentMethod() != null) {
            if (xcblPaymentInstructions.getPaymentMethod().size() > 1) {
                LOG.warn("More than one payment method was provided with the inbound xcbl order.  "
                        + "Only the first payment method will be persisted on the APD order.");
            }
            PaymentMethod paymentMethod = xcblPaymentInstructions.getPaymentMethod().get(0);
            processPaymentMethod(paymentMethod, po);
        }
    }

    private static void processPaymentMethod(PaymentMethod paymentMethod, PurchaseOrderDto po) throws Exception {
        PaymentInformationDto paymentInfo = po.getPaymentInformationDto();
        if (paymentInfo == null) {
            paymentInfo = new PaymentInformationDto();
        }
        if (paymentMethod.getPaymentMeanCoded().equals(PAYMENT_MEAN_CODED_BANKCARD)
                && paymentMethod.getCardInfo() != null
                && StringUtils.isNotBlank(paymentMethod.getCardInfo().getCardNum())) {
            paymentInfo.setNameOnCard(paymentMethod.getCardInfo().getCardHolderName());
            paymentInfo.setExpiration(DateUtils.parseDate(paymentMethod.getCardInfo().getCardExpirationDate(),
                    XCBL_ORDER_DATE_FORMATS));
            //the card will be stored in card vault when placed using PlaceOrderService
            String cardNumber = paymentMethod.getCardInfo().getCardNum();
            paymentInfo.setIdentity(cardNumber.substring(Math.max(cardNumber.length() - 4, 0), cardNumber.length()));
            paymentInfo.setCardNumber(cardNumber);
            paymentInfo.setPaymentType(PaymentTypeEnum.CREDITCARD.getValue());
            if (paymentMethod.getCardInfo().getCardType() != null) {
                paymentInfo.setCardType(paymentMethod.getCardInfo().getCardType().value());
            }
        }
        po.setPaymentInformationDto(paymentInfo);
    }

    private static void processOrderDetail(OrderDetail orderDetail, PurchaseOrderDto po) throws Exception {
        if (orderDetail.getListOfItemDetail().getItemDetail() != null) {
            po.setItems(new ArrayList<LineItemDto>());
            for (ItemDetail itemDetail : orderDetail.getListOfItemDetail().getItemDetail()) {
                LineItemDto lineItem = new LineItemDto();
                lineItem = processLineItemDetails(itemDetail, po, lineItem);
                if (!lineItem.getFee()) {
                    lineItem.setOrder(po);
                    po.getItems().add(lineItem);
                }
            }
        }
    }

    private static LineItemDto processLineItemDetails(ItemDetail xcblItemDetail, PurchaseOrderDto po,
            LineItemDto lineItem) throws Exception {
        ItemIdentifiers identifiers = xcblItemDetail.getBaseItemDetail().getItemIdentifiers();
        if (identifiers != null) {
            String description = (xcblItemDetail.getLineItemNote() != null) ? xcblItemDetail.getLineItemNote()
                    : identifiers.getItemDescription();
            if (!StringUtils.isEmpty(description) && description.indexOf(MINIMUM_ORDER_FEE) >= 0) {
                lineItem.setFee(true);
            }
            lineItem.setDescription(description);
            CommodityCode commodityCode = identifiers.getCommodityCode();
            if (commodityCode != null) {
                //TODO verify this mapping
                lineItem.setUnspscClassification(commodityCode.getIdentifier().getIdent());
            }
            else if (identifiers.getCategory() != null && identifiers.getCategory().getStandardCategoryID() != null) {
                lineItem.setUnspscClassification(identifiers.getCategory().getStandardCategoryID()
                        .getStandardCategoryType());
            }
            if (StringUtils.isBlank(lineItem.getUnspscClassification())) {
                //should only be called for Pepsi/Frito-Lay orders
                lineItem.setUnspscClassification("14110000");
            }
            if (identifiers.getPartNumbers() != null) {
                if (identifiers.getPartNumbers().getSellerPartNumber() != null) {
                    String[] supplierPartNumberParts = StringUtils.split(identifiers.getPartNumbers()
                            .getSellerPartNumber().getPartNum().getPartID(), ITEM_PART_ID_DELIMITER);
                    if (supplierPartNumberParts.length > 0) {
                        lineItem.setSupplierPartId(supplierPartNumberParts[0]);
                    }
                    if (supplierPartNumberParts.length > 1) {
                        lineItem.setSupplierPartAuxiliaryId(supplierPartNumberParts[1]);
                    }
                    //lineItem.setApdSku(identifiers.getPartNumbers().getSellerPartNumber().getPartNum().getPartID());
                }
                String buyerPartNum = null;
                if (identifiers.getPartNumbers().getBuyerPartNumber() != null) {
                    buyerPartNum = identifiers.getPartNumbers().getBuyerPartNumber().getPartNum().getPartID();
                }
                if (StringUtils.isNotBlank(buyerPartNum)) {
                    lineItem.setBuyerPartNumber(buyerPartNum);
                    SkuDto customerSku = new SkuDto();
                    SkuTypeDto skuType = new SkuTypeDto();
                    skuType.setName(SkuTypeBp.SKUTYPE_CUSTOMER);
                    customerSku.setValue(buyerPartNum);
                    customerSku.setType(skuType);
                    lineItem.setCustomerSku(customerSku);
                }
                String manufacturerPartNum = null;
                if (identifiers.getPartNumbers().getManufacturerPartNumber() != null) {
                    manufacturerPartNum = identifiers.getPartNumbers().getManufacturerPartNumber().getPartID();
                }
                if (StringUtils.isNotBlank(manufacturerPartNum)) {
                    lineItem.setManufacturerPartId(manufacturerPartNum);
                    lineItem.setManufacturerName(identifiers.getPartNumbers().getManufacturerPartNumber()
                            .getManufacturerName());
                }
            }
        }
        // Handle case where supplier part aux ID is needed for non-hosted item customization (e.g. custom stamps), but supplier part aux ID
        // is not included in seller number from inbound order.  In this case, extract the supplier part aux ID from line item description
        if (StringUtils.isBlank(lineItem.getSupplierPartAuxiliaryId())) {
            lineItem.setSupplierPartAuxiliaryId(extractSupplierPartAuxIdFromLineItem(lineItem));
        }

        updateTaxable(lineItem, identifiers);

        // Line numbers
        lineItem.setLineNumber(xcblItemDetail.getBaseItemDetail().getLineItemNum().getSellerLineItemNum());
        lineItem.setCustomerLineNumber(String.valueOf(xcblItemDetail.getBaseItemDetail().getLineItemNum()
                .getBuyerLineItemNum()));

        // Requested quantity and item UOM
        if (xcblItemDetail.getBaseItemDetail().getTotalQuantity() != null) {
            Quantity quantity = xcblItemDetail.getBaseItemDetail().getTotalQuantity().getQuantity();
            lineItem.setQuantity(quantity.getQuantityValue().getValue().toBigInteger());
            String uom = quantity.getUnitOfMeasurement().getUOMCoded().equalsIgnoreCase(UOM_CODED_OTHER) ? quantity
                    .getUnitOfMeasurement().getUOMCodedOther() : quantity.getUnitOfMeasurement().getUOMCoded();
            if (StringUtils.isNotBlank(uom)) {
                UnitOfMeasureDto uomDto = new UnitOfMeasureDto();
                uomDto.setName(uom);
                lineItem.setUnitOfMeasure(uomDto);
            }
        }
        lineItem.setShortName(lineItem.getDescription());
        LineItemStatusDto lineItemStatus = new LineItemStatusDto();
        lineItemStatus.setValue(LineItemStatusEnum.CREATED.getValue());
        lineItem.setStatus(lineItemStatus);
        lineItem.setEstimatedShippingAmount(BigDecimal.ZERO);
        lineItem.setMaximumTaxToCharge(BigDecimal.ZERO);
        if (xcblItemDetail.getPricingDetail() != null) {
            if (xcblItemDetail.getPricingDetail().getTotalValue() != null) {
                lineItem.setSubTotal(xcblItemDetail.getPricingDetail().getTotalValue().getMonetaryValue()
                        .getMonetaryAmount());
            }
            else {
                lineItem.setSubTotal(BigDecimal.ZERO);
            }
            lineItem.setMaximumTaxToCharge(BigDecimal.ZERO);

            List<Price> priceList = xcblItemDetail.getPricingDetail().getListOfPrice().getPrice();
            if (!priceList.isEmpty()) {
                UnitPrice xcblUnitPrice = priceList.get(0).getUnitPrice();
                if (xcblUnitPrice != null) {
                    lineItem.setUnitPrice(xcblUnitPrice.getUnitPriceValue());
                    if (xcblUnitPrice.getUnitOfMeasurement() != null) {
                        String uom = StringUtils.equalsIgnoreCase(xcblUnitPrice.getUnitOfMeasurement().getUOMCoded(),
                                UOM_CODED_OTHER) ? xcblUnitPrice.getUnitOfMeasurement().getUOMCodedOther()
                                : xcblUnitPrice.getUnitOfMeasurement().getUOMCoded();
                        if (StringUtils.isNotBlank(uom)) {
                            UnitOfMeasureDto uomDto = new UnitOfMeasureDto();
                            uomDto.setName(uom);
                            lineItem.setUnitOfMeasure(uomDto);
                        }
                    }
                }
            }
        }

        // Pull delivery/shipment details from lineitem if not previously extracted from order header
        if (po.getShipToAddressDto() == null) {
            if (xcblItemDetail.getBaseItemDetail().getFinalRecipient() != null) {
                processShipToParty(xcblItemDetail.getBaseItemDetail().getFinalRecipient().getParty(), po);
            }
            else if (xcblItemDetail.getShipToParty() != null) {
                processShipToParty(xcblItemDetail.getShipToParty().getParty(), po);
            }
        }
        if (po.getRequestedDeliveryDate() == null && xcblItemDetail.getDeliveryDetail() != null
                && xcblItemDetail.getDeliveryDetail().getListOfScheduleLine() != null) {
            for (ScheduleLine scheduleLine : xcblItemDetail.getDeliveryDetail().getListOfScheduleLine()
                    .getScheduleLine()) {
                if (StringUtils.isNotBlank(scheduleLine.getRequestedDeliveryDate())) {
                    po.setRequestedDeliveryDate(DateUtils.parseDate(scheduleLine.getRequestedDeliveryDate(),
                            XCBL_ORDER_DATE_FORMATS));
                }
                if (lineItem.getUnitOfMeasure() == null && scheduleLine.getQuantity() != null
                        && scheduleLine.getQuantity().getUnitOfMeasurement() != null) {
                    String uom = StringUtils.equalsIgnoreCase(scheduleLine.getQuantity().getUnitOfMeasurement()
                            .getUOMCoded(), UOM_CODED_OTHER) ? scheduleLine.getQuantity().getUnitOfMeasurement()
                            .getUOMCodedOther() : scheduleLine.getQuantity().getUnitOfMeasurement().getUOMCoded();
                    if (StringUtils.isNotBlank(uom)) {
                        UnitOfMeasureDto uomDto = new UnitOfMeasureDto();
                        uomDto.setName(uom);
                        lineItem.setUnitOfMeasure(uomDto);
                    }
                }
            }
        }
        if (lineItem.getUnitOfMeasure() == null) {
            UnitOfMeasureDto uomDto = new UnitOfMeasureDto();
            uomDto.setName(DEFAULT_UOM_CODE);
            lineItem.setUnitOfMeasure(uomDto);
        }
        return lineItem;
    }

    private static String extractSupplierPartAuxIdFromLineItem(LineItemDto lineItem) {
        String supplierPartAuxId = null;
        if (StringUtils.contains(lineItem.getDescription(), VENDOR_CUSTOM_ITEM_INDICATOR)) {
            supplierPartAuxId = StringUtils.substringBefore(lineItem.getDescription(), VENDOR_CUSTOM_ITEM_INDICATOR);
            if (StringUtils.contains(supplierPartAuxId, "TAXABLE")) {
                supplierPartAuxId = StringUtils.substringAfter(supplierPartAuxId, "TAXABLE");
                if (StringUtils.isNotBlank(supplierPartAuxId)) {
                    supplierPartAuxId = StringUtils.remove(supplierPartAuxId, "]");
                }
            }
            if (StringUtils.isNotBlank(supplierPartAuxId)) {
                supplierPartAuxId = lineItem.getSupplierPartId().trim() + VENDOR_CUSTOM_ITEM_INDICATOR
                        + supplierPartAuxId.trim();
            }
        }
        return supplierPartAuxId;
    }

    private static void updateTaxable(LineItemDto item, ItemIdentifiers identifiers) {

        if (StringUtils.isNotBlank(item.getDescription())) {
            item.setDescription(item.getDescription().replaceAll("\\[NON-TAXABLE\\]", ""));
            item.setDescription(item.getDescription().replaceAll("\\[TAXABLE\\]", ""));
        }
        if (identifiers != null) {
            if (StringUtils.contains(identifiers.getItemDescription(), "[NON-TAXABLE]")) {
                item.setTaxable(false);
            }
            else if (StringUtils.contains(identifiers.getItemDescription(), "[TAXABLE]")) {
                item.setTaxable(true);
            }
        }
    }
}