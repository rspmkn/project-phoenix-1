package com.apd.phoenix.service.integration.camel.processor;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import javax.edi.model.x12.edi997.FunctionalAcknowledgement;
import javax.edi.model.x12.edi997.segment.Detail;
import javax.edi.model.x12.edi997.segment.FunctionalAcknowledgementBody;
import javax.edi.model.x12.edi997.segment.Header;
import javax.edi.model.x12.edi997.segment.NoteInformationGroup;
import javax.edi.model.x12.edi997.segment.Trailer;
import javax.edi.model.x12.edi997.segment.TransactionResponseGroup;
import javax.edi.model.x12.segment.ElementNoteInformation;
import javax.edi.model.x12.segment.FunctionalGroupResponseHeader;
import javax.edi.model.x12.segment.FunctionalGroupResponseTrailer;
import javax.edi.model.x12.segment.GroupEnvelopeHeader;
import javax.edi.model.x12.segment.GroupEnvelopeTrailer;
import javax.edi.model.x12.segment.InterchangeEnvelopeHeader;
import javax.edi.model.x12.segment.InterchangeEnvelopeTrailer;
import javax.edi.model.x12.segment.SegmentNoteInformation;
import javax.edi.model.x12.segment.TransactionSetHeader;
import javax.edi.model.x12.segment.TransactionSetResponseHeader;
import javax.edi.model.x12.segment.TransactionSetResponseTrailer;
import javax.edi.model.x12.segment.TransactionSetTrailer;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import com.apd.phoenix.service.model.dto.FunctionalAcknowledgementDto;
import com.apd.phoenix.service.model.dto.NoteDto;
import com.apd.phoenix.service.model.dto.FunctionalAcknowledgementDto.AcknowledgementCode;
import com.apd.phoenix.service.model.dto.OrderLogDto.EventTypeDto;

public class FunctionalAcknowledgementToEdi997Translator implements Processor {

    @Override
    public void process(Exchange exchange) throws Exception {
        FunctionalAcknowledgementDto faDto = (FunctionalAcknowledgementDto) exchange.getIn().getBody();

        FunctionalAcknowledgement faEdi = new FunctionalAcknowledgement();
        //Interchange Header
        InterchangeEnvelopeHeader header = buildInterchangeEnvelopeHeader(faDto);
        faEdi.setEnvelopeHeader(header);
        //Group Header
        GroupEnvelopeHeader groupHeader = buildGroupEnvelopeHeader(faDto);
        faEdi.setGroupEnvelopeHeader(groupHeader);
        //Body
        faEdi.setBody(new HashSet<FunctionalAcknowledgementBody>());
        FunctionalAcknowledgementBody body = new FunctionalAcknowledgementBody();
        Header bodyHeader = new Header();
        //ST
        TransactionSetHeader transactionSetHeader = new TransactionSetHeader();
        transactionSetHeader.setTransactionSetIdentifierCode("997");
        transactionSetHeader.setTransactionSetControlNumber(faDto.getTransactionId());
        bodyHeader.setTransactionSetHeader(transactionSetHeader);
        //AK1
        FunctionalGroupResponseHeader functionalGroupResponseHeader = new FunctionalGroupResponseHeader();
        functionalGroupResponseHeader.setFunctionalIdentifierCode(getFunctionalIdentifierFromEventType(faDto
                .getEventType()));
        functionalGroupResponseHeader.setGroupControlNumber(faDto.getMessageGroupId());
        bodyHeader.setFunctionalGroupResponseHeader(functionalGroupResponseHeader);

        body.setHeader(bodyHeader);
        Detail bodyDetail = new Detail();
        TransactionResponseGroup transactionResponseGroup = new TransactionResponseGroup();
        //AK2
        TransactionSetResponseHeader transactionSetResponseHeader = new TransactionSetResponseHeader();
        transactionSetResponseHeader.setTransactionSetIdentifierCode(getIdentifierCodeFromEventType(faDto
                .getEventType()));
        transactionSetResponseHeader.setTransactionSetControlNumber(faDto.getMessageTransactionId());
        transactionResponseGroup.setTransactionSetResponseHeader(transactionSetResponseHeader);
        transactionResponseGroup.setNoteInformationGroup(new HashSet<NoteInformationGroup>());
        if (faDto.getNoteDtos() != null) {
            for (NoteDto noteDto : faDto.getNoteDtos()) {
                NoteInformationGroup noteInformationGroup = new NoteInformationGroup();
                //AK3
                SegmentNoteInformation segmentNoteInformation = new SegmentNoteInformation();
                segmentNoteInformation.setSegmentIDCode(noteDto.getSegmentNoteDto().getSegmentId());
                segmentNoteInformation.setSegmentPositioninTransactionSet(noteDto.getSegmentNoteDto().getPosition());
                segmentNoteInformation.setLoopIdentifierCode(noteDto.getSegmentNoteDto().getLoopIndentifier());
                segmentNoteInformation.setSegmentSyntaxErrorCode(noteDto.getSegmentNoteDto().getSyntaxErrorCode());
                noteInformationGroup.setSegmentNoteInformation(segmentNoteInformation);
                //AK4
                ElementNoteInformation elementNoteInformation = new ElementNoteInformation();
                elementNoteInformation.setCopyOfBadDataElement(noteDto.getElementNoteDto().getCopyOfBadDataElement());
                elementNoteInformation.setDataElementReferenceNumber(noteDto.getElementNoteDto().getReferenceNumber());
                elementNoteInformation.setDataElementSyntaxErrorCode(noteDto.getElementNoteDto().getSyntaxErrorCode());
                elementNoteInformation.setPositionInSegment(noteDto.getElementNoteDto().getPosition());
                noteInformationGroup.setElementNoteInformation(elementNoteInformation);
                transactionResponseGroup.getNoteInformationGroup().add(noteInformationGroup);
            }
        }

        //AK5
        TransactionSetResponseTrailer transactionSetResponseTrailer = new TransactionSetResponseTrailer();
        transactionSetResponseTrailer.setTransactionSetAcknowledgementCode(faDto.getAcknowledgementCode().name());
        transactionResponseGroup.setTransactionSetResponseTrailer(transactionSetResponseTrailer);
        bodyDetail.setTransactionResponseGroup(new HashSet<TransactionResponseGroup>());
        bodyDetail.getTransactionResponseGroup().add(transactionResponseGroup);

        body.setDetail(bodyDetail);

        Trailer bodyTrailer = new Trailer();
        //AK9
        FunctionalGroupResponseTrailer functionalGroupResponseTrailer = new FunctionalGroupResponseTrailer();
        functionalGroupResponseTrailer.setFunctionalGroupAcknowledgeCode("A");
        functionalGroupResponseTrailer.setNumberOfTransactionSetsIncluded("1");
        functionalGroupResponseTrailer.setNumberOfReceivedTransactionSets("1");
        if (faDto.getAcknowledgementCode() == AcknowledgementCode.A) {
            functionalGroupResponseTrailer.setNumberOfAcceptedTransactionSets("1");
        }
        else {
            functionalGroupResponseTrailer.setNumberOfAcceptedTransactionSets("0");
        }
        bodyTrailer.setFunctionalGroupResponseTrailer(functionalGroupResponseTrailer);
        //SE
        TransactionSetTrailer transactionSetTrailer = new TransactionSetTrailer();
        transactionSetTrailer.setNumberOfIncludedSegments("1");
        transactionSetTrailer.setTransactionControlNumber(faDto.getTransactionId());
        bodyTrailer.setTransactionSetTrailer(transactionSetTrailer);

        body.setTrailer(bodyTrailer);
        faEdi.getBody().add(body);
        //Group Trailer
        GroupEnvelopeTrailer groupTrailer = buildGroupEnvelopeTrailer(faDto);
        groupTrailer.setGroupControlNumber(faDto.getGroupId());
        groupTrailer.setNumberOfTransactionSets("1");
        faEdi.setGroupEnvelopeTrailer(groupTrailer);
        //Interchange Trailer
        InterchangeEnvelopeTrailer trailer = buildInterchangeEnvelopeTrailer(faDto);
        trailer.setInterchangeControlNumber(faDto.getInterchangeId());
        trailer.setNumberOfIncludedGroups("1");
        faEdi.setEnvelopeTrailer(trailer);

        exchange.getOut().setBody(faEdi);
        exchange.getOut().setHeaders(exchange.getIn().getHeaders());
    }

    private InterchangeEnvelopeHeader buildInterchangeEnvelopeHeader(FunctionalAcknowledgementDto faDto) {
        //ISA*00*          *00*          *ZZ*PHOEN          *01*007981038      *131003*1421*U*00400*000384204*1*T*>~
        InterchangeEnvelopeHeader header = new InterchangeEnvelopeHeader();
        header.setAuthorizationInformationQualifier("00");
        header.setAuthorizationInformation("          ");
        header.setSecurityInformationQualifier("00");
        header.setSecurityInformation("          ");
        header.setSenderInterchangeIDQualifier("ZZ");
        header.setInterchangeSenderID("PHOEN          ");
        header.setReceiverInterchangeIDQualifier("01");
        header.setInterchangeReceiverID("007981038      ");
        Date dNow = new Date();
        header.setInterchangeDate(dNow);
        SimpleDateFormat ft = new SimpleDateFormat("hhmm");
        header.setInterchangeTime(ft.format(dNow));
        header.setInterchangeControlID("U");
        header.setInterchangeVersionNumber("00400");
        header.setInterchangeControlNumber(faDto.getInterchangeId());
        header.setiSAAcknowledgmentRequested("0");
        header.setTestIndicator("T");
        return header;
    }

    private GroupEnvelopeHeader buildGroupEnvelopeHeader(FunctionalAcknowledgementDto faDto) {
        //GS*PO*PHOEN*007981038*20131003*1421*183226*X*004010~
        GroupEnvelopeHeader header = new GroupEnvelopeHeader();

        header.setFunctionalIDCode("FA");
        header.setApplicationSendersCode("PHOEN");
        header.setApplicationReceiversCode("007981038");
        Date dNow = new Date();
        header.setDate(dNow);
        SimpleDateFormat ft = new SimpleDateFormat("hhmm");
        header.setTime(ft.format(dNow));
        header.setGroupControlNumber(faDto.getGroupId());
        header.setResponsibleAgencyCode("X");
        header.setVersionReleaseIndustryIdentifierCode("004010");
        return header;
    }

    private GroupEnvelopeTrailer buildGroupEnvelopeTrailer(FunctionalAcknowledgementDto faDto) {
        //GE*1*183226~
        GroupEnvelopeTrailer trailer = new GroupEnvelopeTrailer();
        trailer.setNumberOfTransactionSets("1");
        trailer.setGroupControlNumber("183226");
        return trailer;
    }

    private InterchangeEnvelopeTrailer buildInterchangeEnvelopeTrailer(FunctionalAcknowledgementDto faDto) {
        //IEA*1*000384204~
        InterchangeEnvelopeTrailer trailer = new InterchangeEnvelopeTrailer();
        trailer.setNumberOfIncludedGroups("1");
        trailer.setInterchangeControlNumber("000384204");
        return trailer;
    }

    private String getIdentifierCodeFromEventType(EventTypeDto eventType) {
        if (EventTypeDto.PURCHASE_ORDER_SENT == eventType) {
            return "850";
        }
        if (EventTypeDto.ORDER_ACKNOWLEDGEMENT == eventType) {
            return "855";
        }
        if (EventTypeDto.ADVANCED_SHIPMENT_NOTICE == eventType) {
            return "856";
        }
        if (EventTypeDto.VOUCHED == eventType) {
            return "810";
        }
        return null;
    }

    private String getFunctionalIdentifierFromEventType(EventTypeDto eventType) {
        if (EventTypeDto.PURCHASE_ORDER_SENT == eventType) {
            return "PO";
        }
        if (EventTypeDto.ORDER_ACKNOWLEDGEMENT == eventType) {
            return "PR";
        }
        if (EventTypeDto.ADVANCED_SHIPMENT_NOTICE == eventType) {
            return "SH";
        }
        if (EventTypeDto.VOUCHED == eventType) {
            return "IN";
        }
        return null;
    }

}
