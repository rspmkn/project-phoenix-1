package com.apd.phoenix.service.integration.routes;

import org.apache.camel.LoggingLevel;
import com.apd.phoenix.service.camel.DelayBean;
import com.apd.phoenix.service.camel.SmartSearchCatalogDelayBean;
import com.apd.phoenix.service.integration.camel.processor.SmartSearchUpdateBean;
import com.apd.phoenix.service.integration.multitenancy.AbstractTenantAwareRouteBuilder;

public class SmartSearchUpdatesRouteBuilder extends AbstractTenantAwareRouteBuilder {

    private String source;

    @Override
    public void configureRouteImplementation() throws Exception {

        onException(Exception.class).maximumRedeliveries(0).handled(true).log(LoggingLevel.INFO,
                "Error in smart search update: ${exception.stacktrace}");

        from(getSource())
        /*
         * delays picking up more messages to deploy until everything is deployed to avoid 
         * transaction timeouts while the system is under load from deployment
         */
        .autoStartup(false).routePolicy(DelayBean.getDelayPolicy()).delay().method(SmartSearchCatalogDelayBean.class,
                "computeDelay").bean(SmartSearchUpdateBean.class, "sendMessage").choice();

    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }
}
