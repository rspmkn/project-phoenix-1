package com.apd.phoenix.service.integration.routes;

import org.apache.camel.Exchange;
import org.apache.camel.LoggingLevel;
import org.apache.camel.dataformat.bindy.csv.BindyCsvDataFormat;
import org.apache.camel.spi.DataFormat;
import com.apd.phoenix.service.integration.camel.aggregation.strategy.StringBodyAggregatingStrategy;
import com.apd.phoenix.service.integration.multitenancy.AbstractTenantAwareRouteBuilder;
import com.apd.phoenix.service.integration.shipConfirm.processor.PickConfirmationTranslator;

public class PickConfirmationRouteBuilder extends AbstractTenantAwareRouteBuilder {

    private String source;

    private String endpointDlq;

    private String endpoint;

    @Override
    public void configureRouteImplementation() throws Exception {

        onException(Exception.class).maximumRedeliveries(0).handled(true).log(LoggingLevel.ERROR,
                "Error in processing: ${exception.stacktrace}");

        DataFormat bindy = new BindyCsvDataFormat("com.apd.phoenix.service.integration.soloman.pickConfirm.model");

        //From queue
        from(source).errorHandler(
                deadLetterChannel(endpointDlq).maximumRedeliveries(2).redeliveryDelay(2000).useExponentialBackOff()
                        .backOffMultiplier(2))
                //split on each item in list
                .log(LoggingLevel.INFO, "Starting shipconfirm processing").split(body())
                //translate to ship confirmation
                .bean(PickConfirmationTranslator.class, "pickDtoToPickConfirmDto")
                //marshal into csv
                .marshal(bindy).aggregate(header("tenantId"), new StringBodyAggregatingStrategy()).completionSize(100)
                .completionTimeout(4000)
                //Set filename
                .setHeader(Exchange.FILE_NAME, simple("solomon_${date:now:yyyyMMdd_hhmmss_SSS}.csv"))
                //Send to endpoint
                .log(LoggingLevel.INFO, "Sending shipconfirm CSV").log(LoggingLevel.DEBUG, "${body}").to(endpoint);

    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getEndpoint() {
        return endpoint;
    }

    public void setEndpoint(String endpoint) {
        this.endpoint = endpoint;
    }

    public String getEndpointDlq() {
        return endpointDlq;
    }

    public void setEndpointDlq(String endpointDlq) {
        this.endpointDlq = endpointDlq;
    }

}
