package com.apd.phoenix.service.integration.beans;

import java.util.Properties;
import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.core.utility.TenantConfigRepository;
import com.apd.phoenix.service.integration.routes.EDI810InboundRouteBuilder;
import com.apd.phoenix.service.integration.routes.EDI816InboundRouteBuilder;
import com.apd.phoenix.service.integration.routes.EDI824RouteBuilder;
import com.apd.phoenix.service.integration.routes.EDI850v4010InboundRouteBuilder;
import com.apd.phoenix.service.integration.routes.EDI850v5010InboundRouteBuilder;
import com.apd.phoenix.service.integration.routes.EDI855RouteBuilder;
import com.apd.phoenix.service.integration.routes.EDI856RouteBuilder;
import com.apd.phoenix.service.integration.routes.EDI864RouteBuilder;
import com.apd.phoenix.service.integration.routes.EDI997RouteBuilder;
import com.apd.phoenix.service.integration.routes.EDIEntryPointRouteBuilder;
import com.apd.phoenix.service.integration.utility.EDIOrderUtils;
import com.apd.phoenix.service.integration.utility.RouterBeanUtils;
import com.apd.phoenix.service.message.impl.EDIMessageSender;
import com.apd.phoenix.service.persistence.multitenancy.impl.CurrentTenantIdentifierResolverImpl;

@Startup
@Singleton
public class InboundEDIRouterBean extends JMSRouterBean {

    private static final String EDI_TX_TRANSACTION_SOURCE = "ediTXTransactionSource";
    private static final Logger LOG = LoggerFactory.getLogger(InboundEDIRouterBean.class);

    @PostConstruct
    public void init() throws Exception {

        CurrentTenantIdentifierResolverImpl.setCurrentTenant(TenantConfigRepository.getInstance().getGenericTenantId());

        Properties commonProperties = TenantConfigRepository.getInstance().getProperties(
                "camel.edi-inbound.integration");

        setupAmq(camelContext, commonProperties.getProperty("jmsBrokerUrl"));

        //Each individual camel route must have a unique start order to specify an order for the routes
        int startOrderOffset = 1;

        String partners = commonProperties.getProperty("allPartners");
        String[] partnerList = StringUtils.split(partners, ',');

        if (partnerList != null) {
            for (String partner : partnerList) {
                if (StringUtils.isBlank(partner)) {
                    continue;
                }
                LOG.info("Adding partner EDI 997 route: {}", partner);

                //997
                EDI997RouteBuilder edi997RouteBuilder = new EDI997RouteBuilder();
                Long tenantId = CurrentTenantIdentifierResolverImpl.getCurrentTenant();
                Properties partnerProperties = TenantConfigRepository.getInstance().getPropertiesByTenantId(tenantId,
                        partner + ".integration");
                edi997RouteBuilder.setStartOrderOffset(startOrderOffset);
                startOrderOffset = startOrderOffset
                        + EDIOrderUtils.getTotalOffset(EDIOrderUtils.RouteOrderOffsetInboundEDI997.class);
                edi997RouteBuilder.setOutputDLQ(commonProperties.getProperty("edi997OutputDLQ"));
                edi997RouteBuilder.setSource(partnerProperties.getProperty("ediFATransactionSource"));
                edi997RouteBuilder.setSenderId(partner);
                edi997RouteBuilder.setFormattedSource(partnerProperties.getProperty("edi997FormattedSource"));
                edi997RouteBuilder.setLogSource(partnerProperties.getProperty("edi997LogSource"));
                camelContext.addRoutes(edi997RouteBuilder);
            }
            camelContext.addRoutes(RouterBeanUtils.createCatchallRouteBuilder(partnerList,
                    EDIMessageSender.EDI_OUTBOUND_997_RESEND));
        }

        String edi855partners = commonProperties.getProperty("edi855partners");
        String[] edi855partnersList = StringUtils.split(edi855partners, ',');

        if (edi855partnersList != null) {
            for (String partner : edi855partnersList) {
                if (StringUtils.isBlank(partner)) {
                    continue;
                }
                LOG.info("Adding partner EDI 855 route: {}", partner);

                Properties partnerProperties = TenantConfigRepository.getInstance().getPropertiesByTenantId(
                        CurrentTenantIdentifierResolverImpl.getCurrentTenant(), partner + ".integration");

                //855
                EDI855RouteBuilder edi855RouteBuilder = new EDI855RouteBuilder();
                edi855RouteBuilder.setStartOrderOffset(startOrderOffset);
                startOrderOffset = startOrderOffset
                        + EDIOrderUtils.getTotalOffset(EDIOrderUtils.RouteOrderOffsetInboundEDI855.class);
                edi855RouteBuilder.setLogOutputURI(commonProperties.getProperty("edi855LogOuputURI"));
                edi855RouteBuilder.setLogOutputDLQ(commonProperties.getProperty("edi855LogOutputDLQ"));
                edi855RouteBuilder.setOutput(partnerProperties.getProperty("edi855Output"));
                edi855RouteBuilder.setOutputDLQ(commonProperties.getProperty("edi855OutputDLQ"));
                edi855RouteBuilder.setSource(partnerProperties.getProperty("ediPRTransactionSource"));
                edi855RouteBuilder.setFaOutput(partnerProperties.getProperty("edi997OutboundSource"));
                edi855RouteBuilder.setSenderId(partner);
                String senderCode = partnerProperties.getProperty("APDsenderCode");
                senderCode = senderCode.substring(1, senderCode.length() - 1);
                edi855RouteBuilder.setSenderCode(senderCode);
                String receiverCode = partnerProperties.getProperty("partnerSenderCode");
                receiverCode = receiverCode.substring(1, receiverCode.length() - 1);
                edi855RouteBuilder.setReceiverCode(receiverCode);
                edi855RouteBuilder.setFaSource(partnerProperties.getProperty("edi855FaSource"));
                edi855RouteBuilder.setTestIndicator(partnerProperties.getProperty("testIndicator"));
                edi855RouteBuilder.setFaPrepare(partnerProperties.getProperty("edi855FaPrepare"));
                edi855RouteBuilder.setOrderAckFormattedSource(partnerProperties.getProperty("edi855FormattedSource"));
                edi855RouteBuilder.setOrderAckLogSource(partnerProperties.getProperty("edi855LogSource"));
                edi855RouteBuilder.setSend997(Boolean.TRUE.toString().equals(partnerProperties.get("send997")));
                camelContext.addRoutes(edi855RouteBuilder);
            }
        }

        String edi856partners = commonProperties.getProperty("edi856partners");
        String[] edi856partnersList = StringUtils.split(edi856partners, ',');

        if (edi856partnersList != null) {
            for (String partner : edi856partnersList) {
                if (StringUtils.isBlank(partner)) {
                    continue;
                }
                LOG.info("Adding partner EDI 856 route: {}", partner);

                Properties partnerProperties = TenantConfigRepository.getInstance().getPropertiesByTenantId(
                        CurrentTenantIdentifierResolverImpl.getCurrentTenant(), partner + ".integration");
                //856
                EDI856RouteBuilder edi856RouteBuilder = new EDI856RouteBuilder();
                edi856RouteBuilder.setStartOrderOffset(startOrderOffset);
                startOrderOffset = startOrderOffset
                        + EDIOrderUtils.getTotalOffset(EDIOrderUtils.RouteOrderOffsetInboundEDI856.class);
                edi856RouteBuilder.setLogOutputURI(commonProperties.getProperty("edi856LogOuputURI"));
                edi856RouteBuilder.setLogOutputDLQ(commonProperties.getProperty("edi856LogOutputDLQ"));
                edi856RouteBuilder.setOutput(partnerProperties.getProperty("edi856Output"));
                edi856RouteBuilder.setOutputDLQ(commonProperties.getProperty("edi856OutputDLQ"));
                edi856RouteBuilder.setSource(partnerProperties.getProperty("ediSHTransactionSource"));
                edi856RouteBuilder.setSenderId(partner);
                camelContext.addRoutes(edi856RouteBuilder);
            }
        }

        String edi850v5010partners = commonProperties.getProperty("edi850v5010partners");
        String[] edi850v5010partnersList = StringUtils.split(edi850v5010partners, ',');

        if (edi850v5010partnersList != null) {
            for (String partner : edi850v5010partnersList) {
                if (StringUtils.isBlank(partner)) {
                    continue;
                }
                LOG.info("Adding partner EDI 850v5010 route: {}", partner);

                Properties partnerProperties = TenantConfigRepository.getInstance().getPropertiesByTenantId(
                        CurrentTenantIdentifierResolverImpl.getCurrentTenant(), partner + ".integration");
                EDI850v5010InboundRouteBuilder edi850RouteBuilder = new EDI850v5010InboundRouteBuilder();
                edi850RouteBuilder.setStartOrderOffset(startOrderOffset);
                startOrderOffset = startOrderOffset
                        + EDIOrderUtils.getTotalOffset(EDIOrderUtils.RouteOrderOffsetInboundEDI850v5010.class);
                edi850RouteBuilder.setInboundPurchaseOrderSource(partnerProperties
                        .getProperty("ediPOTransactionSource"));

                edi850RouteBuilder.setFaOutput(partnerProperties.getProperty("edi997OutboundSource"));
                edi850RouteBuilder.setSenderId(partner);
                String senderCode = partnerProperties.getProperty("APDsenderCode");
                senderCode = senderCode.substring(1, senderCode.length() - 1);
                edi850RouteBuilder.setSenderCode(senderCode);
                String receiverCode = partnerProperties.getProperty("partnerSenderCode");
                receiverCode = receiverCode.substring(1, receiverCode.length() - 1);
                edi850RouteBuilder.setReceiverCode(receiverCode);
                edi850RouteBuilder.setTestIndicator(partnerProperties.getProperty("testIndicator"));
                edi850RouteBuilder.setInboundFormatSource(partnerProperties.getProperty("edi850InboundFormatSource"));
                edi850RouteBuilder.setInboundLogSource(partnerProperties.getProperty("edi850InboundLogSource"));
                edi850RouteBuilder.setFaSource(partnerProperties.getProperty("edi850FaSource"));
                edi850RouteBuilder.setFaPrepare(partnerProperties.getProperty("edi850FaPrepare"));
                camelContext.addRoutes(edi850RouteBuilder);
            }
        }

        String edi850v4010partners = commonProperties.getProperty("edi850v4010partners");
        String[] edi850v4010partnersList = StringUtils.split(edi850v4010partners, ',');

        if (edi850v4010partnersList != null) {
            for (String partner : edi850v4010partnersList) {
                if (StringUtils.isBlank(partner)) {
                    continue;
                }
                LOG.info("Adding partner EDI 850v4010 route: {}", partner);

                Properties partnerProperties = TenantConfigRepository.getInstance().getPropertiesByTenantId(
                        CurrentTenantIdentifierResolverImpl.getCurrentTenant(), partner + ".integration");
                EDI850v4010InboundRouteBuilder edi850RouteBuilder = new EDI850v4010InboundRouteBuilder();
                edi850RouteBuilder.setStartOrderOffset(startOrderOffset);
                startOrderOffset = startOrderOffset
                        + EDIOrderUtils.getTotalOffset(EDIOrderUtils.RouteOrderOffsetInboundEDI850v4010.class);
                edi850RouteBuilder.setInboundDataformat(javax.edi.model.x12.edi850.PurchaseOrder.class);
                edi850RouteBuilder.setInboundPurchaseOrderSource(partnerProperties
                        .getProperty("ediPOTransactionSource"));
                edi850RouteBuilder.setSenderId(partner);
                edi850RouteBuilder.setLogging(true);

                edi850RouteBuilder.setSenderId(partnerProperties.getProperty("senderId"));
                edi850RouteBuilder.setFaSource(partnerProperties.getProperty("edi850FaSource"));
                String senderCode = partnerProperties.getProperty("APDsenderCode");
                senderCode = senderCode.substring(1, senderCode.length() - 1);
                edi850RouteBuilder.setSenderCode(partnerProperties.getProperty(senderCode));
                String receiverCode = partnerProperties.getProperty("partnerSenderCode");
                receiverCode = receiverCode.substring(1, receiverCode.length() - 1);
                edi850RouteBuilder.setReceiverCode(partnerProperties.getProperty(receiverCode));
                edi850RouteBuilder.setTestIndicator(partnerProperties.getProperty("testIndicator"));
                edi850RouteBuilder.setFaPrepare(partnerProperties.getProperty("edi850FaPrepare"));
                edi850RouteBuilder.setFaOutput(partnerProperties.getProperty("edi997OutboundSource"));
                edi850RouteBuilder.setInboundFormatSource(partnerProperties.getProperty("edi850InboundFormatSource"));
                edi850RouteBuilder.setInboundLogSource(partnerProperties.getProperty("edi850InboundLogSource"));
                camelContext.addRoutes(edi850RouteBuilder);
            }
        }

        String edi824partners = commonProperties.getProperty("edi824partners");
        String[] edi824partnersList = StringUtils.split(edi824partners, ',');

        if (edi824partnersList != null) {
            for (String partner : edi824partnersList) {
                if (StringUtils.isBlank(partner)) {
                    continue;
                }
                LOG.info("Adding partner edi 824 route: {}", partner);
                Properties partnerProperties = TenantConfigRepository.getInstance().getPropertiesByTenantId(
                        CurrentTenantIdentifierResolverImpl.getCurrentTenant(), partner + ".integration");
                EDI824RouteBuilder edi824RouteBuilder = new EDI824RouteBuilder();
                edi824RouteBuilder.setStartOrderOffset(startOrderOffset);
                startOrderOffset = startOrderOffset
                        + EDIOrderUtils.getTotalOffset(EDIOrderUtils.RouteOrderOffsetInboundEDI824.class);
                edi824RouteBuilder.setSource(partnerProperties.getProperty("ediAGTransactionSource"));
                edi824RouteBuilder.setLogOutputDLQ(commonProperties.getProperty("edi824logOutputDlq"));
                edi824RouteBuilder.setLogOutputURI(commonProperties.getProperty("edi824logOutputUri"));
                edi824RouteBuilder.setOrgLogOutput(partnerProperties.getProperty("edi824logOutput"));
                edi824RouteBuilder.setLogSource(partnerProperties.getProperty("edi824logSource"));
                edi824RouteBuilder.setOutput(partnerProperties.getProperty("edi824output"));
                edi824RouteBuilder.setOutputDLQ(partnerProperties.getProperty("edi824OutputDlq"));
                edi824RouteBuilder.setUsscoorgEDISource(partnerProperties.getProperty("edi824OrgFormattedSource"));
                edi824RouteBuilder.setSenderId(partner);
                camelContext.addRoutes(edi824RouteBuilder);
            }
        }

        String edi816partners = commonProperties.getProperty("edi816partners");
        String[] edi816partnersList = StringUtils.split(edi816partners, ',');

        if (edi816partnersList != null) {
            for (String partner : edi816partnersList) {
                if (StringUtils.isBlank(partner)) {
                    continue;
                }
                LOG.info("Adding partner EDI 816 route: {}", partner);
                Properties partnerProperties = TenantConfigRepository.getInstance().getPropertiesByTenantId(
                        CurrentTenantIdentifierResolverImpl.getCurrentTenant(), partner + ".integration");

                EDI816InboundRouteBuilder edi816InboundRouteBuilder = new EDI816InboundRouteBuilder();
                edi816InboundRouteBuilder.setStartOrderOffset(startOrderOffset);
                startOrderOffset = startOrderOffset
                        + EDIOrderUtils.getTotalOffset(EDIOrderUtils.RouteOrderOffsetInboundEDI816.class);
                edi816InboundRouteBuilder.setLogOutputDLQ(commonProperties.getProperty("edi816LogOutputDLQ"));
                edi816InboundRouteBuilder.setLogOutputURI(commonProperties.getProperty("edi816LogOutputURI"));
                edi816InboundRouteBuilder.setOrgLogOutput(partnerProperties.getProperty("edi816OrgLogOutput"));
                edi816InboundRouteBuilder.setOrgLogSource(partnerProperties.getProperty("edi816OrgLogSource"));
                edi816InboundRouteBuilder.setOutput(partnerProperties.getProperty("edi816Output"));
                edi816InboundRouteBuilder.setOutputDLQ(commonProperties.getProperty("edi816OutputDLQ"));
                edi816InboundRouteBuilder.setSenderId(partner);
                edi816InboundRouteBuilder.setSource(partnerProperties.getProperty("ediORTransactionSource"));
                edi816InboundRouteBuilder
                        .setUsscoorgEDISource(partnerProperties.getProperty("edi816UsscoorgEDISource"));
                camelContext.addRoutes(edi816InboundRouteBuilder);
            }
        }

        String edi810partners = commonProperties.getProperty("edi810partners");
        String[] edi810partnersList = StringUtils.split(edi810partners, ',');

        if (edi810partnersList != null) {
            for (String partner : edi810partnersList) {
                if (StringUtils.isBlank(partner)) {
                    continue;
                }
                LOG.info("Adding partner EDI 810 route: {}", partner);

                Properties partnerProperties = TenantConfigRepository.getInstance().getPropertiesByTenantId(
                        CurrentTenantIdentifierResolverImpl.getCurrentTenant(), partner + ".integration");
                //810
                EDI810InboundRouteBuilder edi810RouteBuilder = new EDI810InboundRouteBuilder();
                edi810RouteBuilder.setStartOrderOffset(startOrderOffset);
                startOrderOffset = startOrderOffset
                        + EDIOrderUtils.getTotalOffset(EDIOrderUtils.RouteOrderOffsetInboundEDI810.class);
                edi810RouteBuilder.setLogOutputURI(commonProperties.getProperty("edi810LogOuputURI"));
                edi810RouteBuilder.setLogOutputDLQ(commonProperties.getProperty("edi810LogOutputDLQ"));
                edi810RouteBuilder.setOutput(partnerProperties.getProperty("edi810Output"));
                edi810RouteBuilder.setOutputDLQ(commonProperties.getProperty("edi810OutputDLQ"));
                edi810RouteBuilder.setSource(partnerProperties.getProperty("ediINTransactionSource"));
                edi810RouteBuilder.setCreditOutput(partnerProperties.getProperty("edi810CreditOutput"));
                edi810RouteBuilder.setSenderId(partner);
                edi810RouteBuilder.setUseDifferetnCreditPath(partnerProperties.getProperty("differentCreditInvoice")
                        .equals(Boolean.TRUE.toString()));
                camelContext.addRoutes(edi810RouteBuilder);
            }
        }

        String edi864partners = commonProperties.getProperty("edi864partners");
        String[] edi864partnersList = StringUtils.split(edi864partners, ',');

        if (edi864partnersList != null) {
            for (String partner : edi864partnersList) {
                if (StringUtils.isBlank(partner)) {
                    continue;
                }
                LOG.info("Adding partner inbound EDI 864 route: {}", partner);

                Properties partnerProperties = TenantConfigRepository.getInstance().getPropertiesByTenantId(
                        CurrentTenantIdentifierResolverImpl.getCurrentTenant(), partner + ".integration");
                EDI864RouteBuilder edi864RouteBuilder = new EDI864RouteBuilder();
                edi864RouteBuilder.setSource(partnerProperties.getProperty(EDI_TX_TRANSACTION_SOURCE));
                edi864RouteBuilder.setLogSource(partnerProperties.getProperty("edi864LogSource"));
                edi864RouteBuilder.setEmailSource(partnerProperties.getProperty("edi864EmailSource"));
                edi864RouteBuilder.setStartOrderOffset(startOrderOffset);
                startOrderOffset = startOrderOffset
                        + EDIOrderUtils.getTotalOffset(EDIOrderUtils.RouteOrderOffsetInboundEDI864.class);
                camelContext.addRoutes(edi864RouteBuilder);

            }
        }

        if (partnerList != null) {
            for (String partner : partnerList) {
                if (StringUtils.isBlank(partner)) {
                    continue;
                }
                LOG.info("Adding partner inbound EDI transaction router: {}", partner);

                Properties partnerProperties = TenantConfigRepository.getInstance().getPropertiesByTenantId(
                        CurrentTenantIdentifierResolverImpl.getCurrentTenant(), partner + ".integration");

                //Main router
                EDIEntryPointRouteBuilder ediEntryPointRouteBuilder = new EDIEntryPointRouteBuilder();
                ediEntryPointRouteBuilder.setStartOrderOffset(startOrderOffset);
                startOrderOffset = startOrderOffset
                        + EDIOrderUtils.getTotalOffset(EDIOrderUtils.RouteOrderOffsetInboundEDI.class);
                ediEntryPointRouteBuilder.setPartnerId(partner);
                String unknownTransactionOutput = partnerProperties.getProperty("ediUnknownTransactionOutput");
                final String faTransactionSource = partnerProperties.getProperty("ediFATransactionSource");
                ediEntryPointRouteBuilder.setTransactionFASink(defaultToUnknown(faTransactionSource,
                        unknownTransactionOutput));
                final String inTransactionSource = partnerProperties.getProperty("ediINTransactionSource");
                ediEntryPointRouteBuilder.setTransactionINSink(defaultToUnknown(inTransactionSource,
                        unknownTransactionOutput));
                final String prTransactionSource = partnerProperties.getProperty("ediPRTransactionSource");
                ediEntryPointRouteBuilder.setTransactionPRSink(defaultToUnknown(prTransactionSource,
                        unknownTransactionOutput));
                final String shTransactionSource = partnerProperties.getProperty("ediSHTransactionSource");
                ediEntryPointRouteBuilder.setTransactionSHSink(defaultToUnknown(shTransactionSource,
                        unknownTransactionOutput));
                final String poTransactionSource = partnerProperties.getProperty("ediPOTransactionSource");
                ediEntryPointRouteBuilder.setTransactionPOSink(defaultToUnknown(poTransactionSource,
                        unknownTransactionOutput));
                final String orTransactionSource = partnerProperties.getProperty("ediORTransactionSource");
                ediEntryPointRouteBuilder.setTransactionORSink(defaultToUnknown(orTransactionSource,
                        unknownTransactionOutput));
                final String agTransactionSource = partnerProperties.getProperty("ediAGTransactionSource");
                ediEntryPointRouteBuilder.setTransactionAGSink(defaultToUnknown(agTransactionSource,
                        unknownTransactionOutput));
                final String txTransactionSource = partnerProperties.getProperty(EDI_TX_TRANSACTION_SOURCE);
                ediEntryPointRouteBuilder.setTransactionTXSink(defaultToUnknown(txTransactionSource,
                        unknownTransactionOutput));
                final String inboundTransactionSource = partnerProperties.getProperty("ediInboundTransactionSource");
                ediEntryPointRouteBuilder.setTransactionSource(defaultToUnknown(inboundTransactionSource,
                        unknownTransactionOutput));
                ediEntryPointRouteBuilder.setTransactionUnknown(unknownTransactionOutput);
                //Decryption
                String decryptEnabled = partnerProperties.getProperty("decryptionEnabled");
                if (decryptEnabled != null && Boolean.parseBoolean(decryptEnabled)) {
                    ediEntryPointRouteBuilder.setArmored(new Boolean(partnerProperties
                            .getProperty("decryptionIsArmored")));
                    ediEntryPointRouteBuilder.setEncrypted(new Boolean(decryptEnabled));
                    ediEntryPointRouteBuilder.setGpgTempDirectory(commonProperties
                            .getProperty("edi.decryption.gpgTmpDirectory"));
                    ediEntryPointRouteBuilder.setGpgExecutableDirectory(commonProperties
                            .getProperty("edi.decryption.gpgExecutableDirectory"));
                }
                camelContext.addRoutes(ediEntryPointRouteBuilder);
            }
        }

        camelContext.start();
    }

    private String defaultToUnknown(final String faTransactionSource, final String unknownTransactionOutput) {
        return StringUtils.isNotBlank(faTransactionSource) ? faTransactionSource : unknownTransactionOutput;
    }

}
