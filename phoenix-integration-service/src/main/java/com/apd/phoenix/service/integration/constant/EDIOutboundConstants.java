package com.apd.phoenix.service.integration.constant;

public interface EDIOutboundConstants {

    public static final String NO_AUTHORIZATION_INFORMATION_PRESENT = "00";
    public static final String FUNCTIONAL_ID_CODE = "PO";
    public static final String ACCREDITED_STANDARDS_COMMITTEE_X12 = "X";
    public static final String INTERCHANGE_VERSION = "00401";
    public static final String X12_PROCEDURES_REVIEW_BOARD_97 = "004010";
    public static final String EDI_810_DOCUMENT = "850";
    public static final String NO_SECURITY_INFORMATION_PRESENT = "00";
    public static final String TEN_PADDED_SPACES = "          ";
    public static final String APD_PARTNER_ID_PADDED = "PHOEN          ";
    public static final String APD_PARTNER_ID = "PHOEN";
    public static final String SAMPLE_CONTROL_ID = "U";
    public static final String NO_ACKNOWLEDGEMENT_REQUESTED = "0";
    public static final String ACKNOWLEDGEMENT_REQUESTED = "1";
    public static final String SUBELEMENT_SEPARATOR = ">";
    public static final String PO_REF_NUM_CODE_CONSUMER_PO = "PO";
    public static final String PO_REF_NUM_CODE_WRAPANDLABEL = "ZZ";
    public static final String PO_REF_NUM_CODE_REG_ADOT = "SU";
    public static final String PO_REF_NUM_CODE_DETAIL_ADOT = "SS";
    public static final String PO_REF_NUM_CODE_ROUTING_INFO = "RU";
    public static final String PO_REF_NUM_CODE_CONSOLIDATED_PO = "11";
    public static final String TX_SET_PURPOSE_CODE_ORIGINAL = "00";
    public static final String PO_TYPE_CODE_NEW_ORDER = "NE";
    public static final String PO_TYPE_CODE_WILL_CALL = "OS";
    public static final String PO1_PRODUCT_ID_TYPE_BUYER_PART_NUM = "BP";
    public static final String PO1_PRODUCT_ID_TYPE_BUYER_ITEM_NUM = "IN";
    public static final String PO1_PRODUCT_ID_TYPE_VENDOR_ITEM_NUM = "VN";
    public static final String SHIP_TO_ENTITY_IDENTIFIER = "ST";
    public static final String ASSIGNED_BY_BUYER_QUALIFIER = "92";
    public static final String USSCO_ACCOUNT_NUMBER_PROPERTY_KEY = "US Account #";
    public static final String BILL_TO_IDENTIFICATION_CODE = "BT";
    public static final String INTERCHANGE_QUALIFIER_FROM_SAMPLE = "ZZ";
    public static final String ITEM_DESCRIPTOR = "F";
    public static final String SAMPLE_RECIEVER_ID = "01";
    public static final String SAMPLE_RECIEVIER_QUALIFIER = "01";
    public static final String SHIP_FROM_ENTITY_CODE = "SF";
    public static final String USSCO_NAME = "UNITED STATIONERS";
    public static final String ASSIGNED_BY_SELLER_QUALIFIER = "91";
    public static final String CONTROL_ID_MAX_PADDING = "000000000";
    public static final String DEALER_INFORMATION = "ORI";
    public static final String LETTERS_AND_NOTES = "L1";
}
