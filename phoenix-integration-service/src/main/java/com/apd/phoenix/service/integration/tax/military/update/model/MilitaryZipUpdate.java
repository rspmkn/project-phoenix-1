package com.apd.phoenix.service.integration.tax.military.update.model;

import java.io.Serializable;
import org.apache.camel.dataformat.bindy.annotation.DataField;
import org.apache.camel.dataformat.bindy.annotation.FixedLengthRecord;

@FixedLengthRecord(length = 89, paddingChar = ' ')
public class MilitaryZipUpdate implements Serializable {

    private static final long serialVersionUID = -234314843746977495L;

    @DataField(pos = 1, length = 88)
    private String record;

    @DataField(pos = 89, length = 1)
    private String action;

    public String getRecord() {
        return record;
    }

    public void setRecord(String record) {
        this.record = record;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

}
