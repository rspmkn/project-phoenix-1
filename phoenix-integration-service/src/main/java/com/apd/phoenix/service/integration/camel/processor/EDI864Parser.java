package com.apd.phoenix.service.integration.camel.processor;

import java.util.ArrayList;
import org.apache.camel.Exchange;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class EDI864Parser {

    private static final int CHARACTERS_IN_SEGMENT_IDENTIFIER = 4;

    private static final String REGEX_TWO_OR_MORE_SPACES = "\\ \\ (\\ )*";

    private static final Logger logger = LoggerFactory.getLogger(EDI864Parser.class);

    private static final String PO_NUMBER_COLUMN_NAME = "PO NUMBER";

    public void process864(Exchange exchange){
		
		String rawMessage = (String) exchange.getIn().getBody();
		String[] lines = rawMessage.split("~");
		ArrayList<String> poNumbers = new ArrayList<>();
		boolean partOfTable = false;
		int locationOfPoNumber = -1;
		for(String line:lines){
			line = line.trim();
			/*
			 * The structure of the error message consists of a series of MSG segments starting with column headers
			 * and then 1+ rows. Each column is separated from the others by 1+ spaces ex:
			 * MSG*INVOICE NUMBER    PO NUMBER  DATA                     COMMENTS*SS~
			 * MSG*30127801   6353990957     0099694460000             ORGANIZATION NAME IS MISSING         PROD*SS~
			 */
			if(line.startsWith("MSG*")){
				line = line.substring(CHARACTERS_IN_SEGMENT_IDENTIFIER);
				String[] lineParts = line.split(REGEX_TWO_OR_MORE_SPACES);
				if(partOfTable){
					String poNumber = lineParts[locationOfPoNumber].trim();
					logger.debug(poNumber);
					poNumbers.add(poNumber);
				}
				else if(line.contains(PO_NUMBER_COLUMN_NAME)){
					partOfTable = true;
					int i = 0;
					//Determine which column the PO Number is in
					for(String part:lineParts){
						if(part.trim().equals(PO_NUMBER_COLUMN_NAME)){
							locationOfPoNumber = i;
							break;
						}
						i++;
					}
					
				}
			}
			else{
				//When we arrive at a line not part of a table, wait until the next table of errors starts
				partOfTable = false;
			}
		}

        exchange.getOut().setBody(rawMessage);
        exchange.getOut().setHeaders(exchange.getIn().getHeaders());
        exchange.getOut().setHeader("poNumbers", poNumbers);
	}
}
