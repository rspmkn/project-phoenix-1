package com.apd.phoenix.service.integration.camel.processor;

import java.math.BigInteger;
import java.util.ArrayList;
import javax.edi.model.x12.edi997.segment.FunctionalAcknowledgementBody;
import javax.edi.model.x12.edi997.segment.NoteInformationGroup;
import javax.edi.model.x12.edi997.segment.TransactionResponseGroup;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.model.dto.ElementNoteDto;
import com.apd.phoenix.service.model.dto.FunctionalAcknowledgementDto;
import com.apd.phoenix.service.model.dto.FunctionalAcknowledgementDto.AcknowledgementCode;
import com.apd.phoenix.service.model.dto.NoteDto;
import com.apd.phoenix.service.model.dto.OrderLogDto.EventTypeDto;
import com.apd.phoenix.service.model.dto.SegmentNoteDto;

public class EDI997toFunctionalAcknowledgementTranslator implements Processor {

    private static final Logger LOG = LoggerFactory.getLogger(EDI997toFunctionalAcknowledgementTranslator.class);

    @Override
    public void process(Exchange exchange) throws Exception {
        FunctionalAcknowledgementDto faDto = new FunctionalAcknowledgementDto();
        faDto.setNoteDtos(new ArrayList<NoteDto>());
        if (exchange.getIn().getBody() instanceof TransactionResponseGroup) {
            TransactionResponseGroup trg = (TransactionResponseGroup) exchange.getIn().getBody();
            String functionalIdentifier = trg.getTransactionSetResponseHeader().getTransactionSetIdentifierCode();
            String transactionId = trg.getTransactionSetResponseHeader().getTransactionSetControlNumber();
            String ackCode = trg.getTransactionSetResponseTrailer().getTransactionSetAcknowledgementCode();
            String transactionIdentifier = trg.getTransactionSetResponseHeader().getTransactionSetIdentifierCode();
            String senderId = (String) exchange.getIn().getHeader("senderId");
            LOG.debug("FA type: {}, id: {}", functionalIdentifier, transactionId);
            faDto.setTransactionId(transactionId);
            faDto.setSenderId(senderId);
            faDto.setAcknowledgementCode(AcknowledgementCode.valueOf(ackCode));
            faDto.setEventType(getEventTypeFromIdentifier(transactionIdentifier));
            //Segment and element errors
            if (trg.getNoteInformationGroup() != null) {

                for (NoteInformationGroup ng : trg.getNoteInformationGroup()) {
                    NoteDto noteDto = new NoteDto();

                    SegmentNoteDto segmentNoteDto = new SegmentNoteDto();
                    segmentNoteDto.setLoopIndentifier(ng.getSegmentNoteInformation().getLoopIdentifierCode());
                    segmentNoteDto.setPosition(ng.getSegmentNoteInformation().getSegmentPositioninTransactionSet());
                    segmentNoteDto.setSegmentId(ng.getSegmentNoteInformation().getSegmentIDCode());
                    BigInteger segmentErrorCode = ng.getSegmentNoteInformation().getSegmentSyntaxErrorCode();
                    segmentNoteDto.setSyntaxErrorCode(segmentErrorCode);
                    segmentNoteDto.setSyntaxErrorDescription(getSegmentErrorDescription(segmentErrorCode.intValue()));
                    ElementNoteDto elementNoteDto = new ElementNoteDto();
                    elementNoteDto.setPosition(ng.getElementNoteInformation().getPositionInSegment());
                    elementNoteDto.setReferenceNumber(ng.getElementNoteInformation().getDataElementReferenceNumber());
                    BigInteger elementErrorCode = ng.getElementNoteInformation().getDataElementSyntaxErrorCode();
                    elementNoteDto.setSyntaxErrorCode(elementErrorCode);
                    elementNoteDto.setSyntaxErrorDescription(getElementErrorDescription(elementErrorCode.intValue()));
                    elementNoteDto.setCopyOfBadDataElement(ng.getElementNoteInformation().getCopyOfBadDataElement());

                    noteDto.setSegmentNoteDto(segmentNoteDto);
                    noteDto.setElementNoteDto(elementNoteDto);
                    faDto.getNoteDtos().add(noteDto);
                }
            }
        }
        else {
            LOG.debug("Sent FA dto without transactionResponseGroups");
            if (exchange.getIn().getBody() instanceof FunctionalAcknowledgementBody) {
                FunctionalAcknowledgementBody body = (FunctionalAcknowledgementBody) exchange.getIn().getBody();
                String ackCode = body.getTrailer().getFunctionalGroupResponseTrailer()
                        .getFunctionalGroupAcknowledgeCode();
                faDto.setGroupId(body.getHeader().getFunctionalGroupResponseHeader().getGroupControlNumber());
                faDto.setAcknowledgementCode(AcknowledgementCode.valueOf(ackCode));
            }
            else {
                LOG.error("Unknown body type.");
            }

        }
        exchange.getOut().setBody(faDto);
        exchange.getOut().setHeaders(exchange.getIn().getHeaders());
    }

    private EventTypeDto getEventTypeFromIdentifier(String indentifier) {
        switch (indentifier) {
            case "850":
                return EventTypeDto.PURCHASE_ORDER_SENT;
            case "855":
                return EventTypeDto.ORDER_ACKNOWLEDGEMENT;
            case "856":
                return EventTypeDto.ADVANCED_SHIPMENT_NOTICE;
            case "810":
                return EventTypeDto.VOUCHED;
            default:
                return null;
        }
    }

    /**
     * AK3 error codes
     * @param errorCode
     * @return
     */
    private String getSegmentErrorDescription(int errorCode) {
        switch (errorCode) {
            case 1:
                return "unrecognized segment id";
            case 2:
                return "unexpected segment";
            case 3:
                return "mandatory segment missing";
            case 4:
                return "loop occurs over maximum allowed";
            case 5:
                return "segment exceeds maximum use";
            case 6:
                return "segment not defined in transaction set";
            case 7:
                return "segment not in proper sequence";
            case 8:
                return "segment has data element errors";
            default:
                return null;
        }
    }

    /**
     * AK4 error codes
     * @param errorCode
     * @return
     */
    private String getElementErrorDescription(int errorCode) {
        switch (errorCode) {
            case 1:
                return "mandatory data element missing";
            case 2:
                return "conditional required data element missing";
            case 3:
                return "too many data elements";
            case 4:
                return "data element too short";
            case 5:
                return "data element too long";
            case 6:
                return "invalid character in data element";
            case 7:
                return "invalid code value";
            case 8:
                return "invalid date";
            case 9:
                return "invalid time";
            case 10:
                return "exclusion condition violated";
            default:
                return null;
        }
    }
}
