package com.apd.phoenix.service.integration.camel.processor.xcbl;

import java.util.Date;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import org.apache.camel.Body;
import org.apache.camel.Header;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.message.api.MessageService;
import com.apd.phoenix.service.model.dto.MessageDto;
import com.apd.phoenix.service.model.dto.MessageMetadataDto;
import com.apd.phoenix.service.model.dto.MessageMetadataDto.CommunicationType;
import com.apd.phoenix.service.model.dto.MessageMetadataDto.MessageType;
import com.apd.phoenix.service.model.dto.OrderLogDto.EventTypeDto;
import com.apd.phoenix.service.model.dto.PurchaseOrderDto;

@Stateless
public class XCBLTransactionLogger {

    // TODO Consolidate duplicate code across *TransactionLogger classes into a parent base class.  
    // If the other Transaction loggers are refactored slightly to match this one, the only difference 
    // between them would be the following line and the two protected methods (conceivably re-purposed as implementations of abstract methods).
    private static final Logger LOG = LoggerFactory.getLogger(XCBLTransactionLogger.class);

    @Inject
    MessageService messageService;

    public void logNewPoTransaction(@Header("apdPo") String apdPo,
            @Header("communicationType") CommunicationType communicationType,
            @Header("destination") String destination, @Header("messageEventType") EventTypeDto messageEventType,
            @Header("envelopeId") String envelopeId, @Header("conversationId") String conversationId,
            @Header("senderId") String senderId, @Header("receiverId") String receiverId,
            @Header("originalMessage") String originalMessage, PurchaseOrderDto body) throws NotSupportedException,
            SystemException, SecurityException, IllegalStateException, RollbackException, HeuristicMixedException,
            HeuristicRollbackException {
        logTransaction(apdPo, communicationType, destination, messageEventType, envelopeId, envelopeId, senderId,
                receiverId, originalMessage);
    }

    public void logTransaction(@Header("apdPo") String apdPo,
            @Header("communicationType") CommunicationType communicationType,
            @Header("destination") String destination, @Header("messageEventType") EventTypeDto messageEventType,
            @Header("envelopeId") String envelopeId, @Header("conversationId") String conversationId,
            @Header("senderId") String senderId, @Header("receiverId") String receiverId, @Body String body)
            throws NotSupportedException, SystemException, SecurityException, IllegalStateException, RollbackException,
            HeuristicMixedException, HeuristicRollbackException {
        if (LOG.isDebugEnabled()) {
            LOG.debug("apdPo: {}", apdPo);
            LOG.debug("body: {}", body);
        }

        MessageMetadataDto metadata = new MessageMetadataDto();
        metadata.setCommunicationType(communicationType);
        metadata.setContentLength(body.length());
        if (StringUtils.isEmpty(destination)) {
            destination = "none";
        }
        metadata.setDestination(destination);
        metadata.setFilePath(getMessageFilePath(apdPo));
        metadata.setMessageDate(new Date());
        metadata.setMessageType(getMessageType());
        metadata.setInterchangeId(envelopeId);
        metadata.setGroupId(envelopeId);
        metadata.setTransactionId(conversationId);
        metadata.setSenderId(senderId);
        metadata.setReceiverId(receiverId);
        MessageDto message = new MessageDto();
        message.setMessageMetadataDto(metadata);
        message.setContent(body);

        messageService.receiveOrderMessage(message, apdPo, messageEventType.name());
    }

    protected MessageType getMessageType() {
        return MessageType.XCBL;
    }

    protected String getMessageFilePath(String apdPo) {
        return apdPo + "/" + apdPo + "-" + StringUtils.lowerCase(getMessageType().getLabel()) + "-"
                + System.currentTimeMillis() + ".txt";
    }
}
