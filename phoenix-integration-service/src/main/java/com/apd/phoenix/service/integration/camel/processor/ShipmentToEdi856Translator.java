/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.service.integration.camel.processor;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Properties;
import javax.edi.model.x12.edi856.AdvanceShipmentNotice;
import javax.edi.model.x12.edi856.segment.AdvanceShipmentNoticeBeginningSegment;
import javax.edi.model.x12.edi856.segment.AdvanceShipmentNoticeBody;
import javax.edi.model.x12.edi856.segment.Detail;
import javax.edi.model.x12.edi856.segment.Header;
import javax.edi.model.x12.edi856.segment.ItemLevelInformationGroup;
import javax.edi.model.x12.edi856.segment.OrderInformationGroup;
import javax.edi.model.x12.edi856.segment.PackageCartonOrItemLevelGroup;
import javax.edi.model.x12.edi856.segment.ShipmentLevelGroup;
import javax.edi.model.x12.edi856.segment.Trailer;
import javax.edi.model.x12.segment.DateTimeReference;
import javax.edi.model.x12.segment.GeographicLocation;
import javax.edi.model.x12.segment.GroupEnvelopeHeader;
import javax.edi.model.x12.segment.GroupEnvelopeTrailer;
import javax.edi.model.x12.segment.HierarchicalLevel;
import javax.edi.model.x12.segment.InterchangeEnvelopeHeader;
import javax.edi.model.x12.segment.InterchangeEnvelopeTrailer;
import javax.edi.model.x12.segment.ItemIdentification;
import javax.edi.model.x12.segment.Name;
import javax.edi.model.x12.segment.PurchaseOrderReference;
import javax.edi.model.x12.segment.ReferenceNumber;
import javax.edi.model.x12.segment.RoutingCarrierDetails;
import javax.edi.model.x12.segment.ShipmentItemDetail;
import javax.edi.model.x12.segment.TransactionSetHeader;
import javax.edi.model.x12.segment.TransactionSetTrailer;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.core.utility.TenantConfigRepository;
import com.apd.phoenix.service.integration.utility.EDIOrderUtils;
import com.apd.phoenix.service.integration.utility.PropertiesLoader;
import com.apd.phoenix.service.model.dto.LineItemXShipmentDto;
import com.apd.phoenix.service.model.dto.ShipmentDto;
import com.apd.phoenix.service.persistence.multitenancy.impl.CurrentTenantIdentifierResolverImpl;

/**
 *
 * @author nreidelb
 */
public class ShipmentToEdi856Translator implements Processor {

    private static final Logger LOG = LoggerFactory.getLogger(ShipmentToEdi856Translator.class);
    public static final String ORIGINAL_PURPOSE_CODE = "00";
    public static final String TIME_FORMAT = "HHmm";
    public static final String HEIRARCHICAL_ID_DEFAULT = "1";
    public static final String SHIPMENT_HEIRARCHICAL_LEVEL_CODE = "S";
    public static final String SCAC_QUALIFIER = "2";
    public static final String TRACKING_NUMBER_QUALIFIER = "2I";
    public static final String SHIPPED_TIME_QUALIFIER = "011";
    public static final String SHIP_TO_IDENTIFICATION_CODE = "ST";
    public static final String POSTAL_SERVICE_CODE_QUALIFIER = "AA";
    public static final String ORDER_LEVEL_CODE = "O";
    public static final String SUPPLIER_ORDER_NO_QUALIFIER = "1V";
    public static final String ITEM_LEVEL_CODE = "I";
    public static final String VENDOR_PART_NUMBER_QUALIFIER = "VP";
    public static final String SERIAL_NUMBER_QUALIFIER = "SN";
    public static final String EDI_856_FUNCTIONAL_ID_CODE = "SH";
    public static final String EDI_856_TRANSACTION_SET_IDENTIFIER_CODE = "856";
    public static final String ONE_MESSAGE_PER_ENVELOPE = "1";
    private static final String APD_CITY = "csr.address.city";
    private static final String APD_STATE = "csr.address.state";
    private static final String APD_ZIP = "csr.address.zip";

    @Override
    public void process(Exchange exchange) throws Exception {
        String interchangeId = (String) exchange.getIn().getHeader("interchangeId");
        String groupId = (String) exchange.getIn().getHeader("groupId");
        String transactionId = (String) exchange.getIn().getHeader("transactionId");
        
        Properties uspsProperties = PropertiesLoader.getAsProperties("usps");
        ShipmentDto shipment = (ShipmentDto) exchange.getIn().getBody();
        AdvanceShipmentNotice advanceShipmentNotice = new AdvanceShipmentNotice();
        //ISA
        final InterchangeEnvelopeHeader interchangeEnvelopeHeader = OutboundEdiUtils.generateDefaultInterchangeEnvelopeHeader(interchangeId);
        interchangeEnvelopeHeader.setInterchangeReceiverID(OutboundEdiUtils.padWithSpaces(uspsProperties.getProperty("applicationRecieversCodeISA"),
                                                                                            OutboundEdiUtils.LENGTH_OF_INTERCHANGE_RECEIVER_ID));
        interchangeEnvelopeHeader.setTestIndicator(uspsProperties.getProperty("testIndicator"));
        advanceShipmentNotice.setEnvelopeHeader(interchangeEnvelopeHeader);
        //GS
        final GroupEnvelopeHeader groupEnvelopeHeader = OutboundEdiUtils.generateDefaultGroupEnvelopeHeader(groupId);
        groupEnvelopeHeader.setFunctionalIDCode(EDI_856_FUNCTIONAL_ID_CODE);
        groupEnvelopeHeader.setApplicationReceiversCode(uspsProperties.getProperty("applicationRecieversCodeGS"));
        advanceShipmentNotice.setGroupEnvelopeHeader(groupEnvelopeHeader);
        
        final ArrayList<AdvanceShipmentNoticeBody> advanceShipmentNotices = new ArrayList<>();
        final AdvanceShipmentNoticeBody body = new AdvanceShipmentNoticeBody();
        
        final Header header = new Header();
        //ST
        final TransactionSetHeader transactionSetHeader = new TransactionSetHeader();
        transactionSetHeader.setTransactionSetIdentifierCode(EDI_856_TRANSACTION_SET_IDENTIFIER_CODE);
        transactionSetHeader.setTransactionSetControlNumber(transactionId);
        header.setTransactionSetHeader(transactionSetHeader);
        //BSN
        final AdvanceShipmentNoticeBeginningSegment advanceShipmentNoticeBeginningSegment = new AdvanceShipmentNoticeBeginningSegment();
        advanceShipmentNoticeBeginningSegment.setTransactionSetPurposeCode(ORIGINAL_PURPOSE_CODE);
        String trackingNumberString = OutboundEdiUtils.truncateAndRemoveDisallowedChars(shipment.getTrackingNumber(),16);
        advanceShipmentNoticeBeginningSegment.setShipmentIdentification(trackingNumberString);
        advanceShipmentNoticeBeginningSegment.setDate(new Date());
        SimpleDateFormat df = new SimpleDateFormat(TIME_FORMAT);
        advanceShipmentNoticeBeginningSegment.setTime(df.format(new Date()));
        header.setBeginningSegment(advanceShipmentNoticeBeginningSegment);
        body.setHeader(header);
        
        final Detail detail = new Detail();
        final ArrayList<ShipmentLevelGroup> shipmentLevelGroups = new ArrayList<>();
        final ShipmentLevelGroup shipmentLevelGroup = new ShipmentLevelGroup();
        //HL
        final HierarchicalLevel hierarchicalLevel = new HierarchicalLevel();
        hierarchicalLevel.setHierarchicalIDNumber(HEIRARCHICAL_ID_DEFAULT);
        hierarchicalLevel.setHierarchicalLevelCode(SHIPMENT_HEIRARCHICAL_LEVEL_CODE);        
        shipmentLevelGroup.setShipmentInformationLevel(hierarchicalLevel);
        //TD5
        final RoutingCarrierDetails routingCarrierDetails = new RoutingCarrierDetails();
        routingCarrierDetails.setIdentificationCodeQualifier(SCAC_QUALIFIER);
        if(shipment.getShippingPartnerDto()!=null){
            routingCarrierDetails.setIdentificationCode(OutboundEdiUtils.truncateAndRemoveDisallowedChars(shipment.getShippingPartnerDto().getScacCode(),2,15));
        }
        shipmentLevelGroup.setRoutingCarrierDetails(routingCarrierDetails);
        //REF
        final ReferenceNumber trackingNumber = new ReferenceNumber();
        trackingNumber.setReferenceIdentificationQualifier(TRACKING_NUMBER_QUALIFIER);
        trackingNumber.setReferenceIdentification(OutboundEdiUtils.truncateAndRemoveDisallowedChars(shipment.getTrackingNumber(), 30));
        shipmentLevelGroup.setReferenceNumber1(trackingNumber);
        //DTM
        final DateTimeReference dateTimeReference = new DateTimeReference();
        //If no date is provided, set ship time to current time.
        if(shipment.getShipTime() != null){
            dateTimeReference.setDate(shipment.getShipTime());
        } else {
            dateTimeReference.setDate(new Date());
        }
        dateTimeReference.setDateTimeQualifier(SHIPPED_TIME_QUALIFIER);
        shipmentLevelGroup.setShipmentDateTime(dateTimeReference);
        //N1
        final Name name = createSimpleShipName(shipment.getCustomerOrderDto().getShipToAddressDto().getName(), SHIP_TO_IDENTIFICATION_CODE);
        name.setIdentificationCodeQualifier(POSTAL_SERVICE_CODE_QUALIFIER);
        name.setIdentificationCode(OutboundEdiUtils.truncateAndRemoveDisallowedChars(shipment.getCustomerOrderDto().getShipToAddressDto().getMiscShipToDto().getFedstripNumber(), 6));
        shipmentLevelGroup.setShipFromNameInformation(name);
       
        if(shipment.getFromAddress()!=null){
            //N4
            final GeographicLocation shipFromGeographicLocation = OutboundEdiUtils.createN4(shipment.getFromAddress());
            shipmentLevelGroup.setShipmentFromLocation(shipFromGeographicLocation);
        } else {
        	final GeographicLocation shipFromGeographicLocation = new GeographicLocation();
            shipFromGeographicLocation.setCityName(this.getTenantPropertyByName(APD_CITY));
            shipFromGeographicLocation.setStateOrProvinceCode(this.getTenantPropertyByName(APD_STATE));
            shipFromGeographicLocation.setPostalCode(this.getTenantPropertyByName(APD_ZIP));
            shipmentLevelGroup.setShipmentFromLocation(shipFromGeographicLocation);
        }
        
        final ArrayList<OrderInformationGroup> orderInformationGroups = new ArrayList<>();
        final OrderInformationGroup orderInformationGroup = new OrderInformationGroup();
        //HL
        final HierarchicalLevel orderReferenceHeader = new HierarchicalLevel();
        orderReferenceHeader.setHierarchicalIDNumber(HEIRARCHICAL_ID_DEFAULT);
        orderReferenceHeader.setHierarchicalLevelCode(ORDER_LEVEL_CODE);
        orderInformationGroup.setPurchaseOrderReferenceHeader(orderReferenceHeader);
        //PRF
        final PurchaseOrderReference purchaseOrderReference = new PurchaseOrderReference();
        if(shipment.getCustomerOrderDto().retrieveCustomerPoNumber()!=null){
            purchaseOrderReference.setPurchaseOrderNumber(OutboundEdiUtils.truncateAndRemoveDisallowedChars(shipment.getCustomerOrderDto().retrieveCustomerPoNumber().getValue(), 16));
        }
        orderInformationGroup.setPurchaseOrderReference(purchaseOrderReference);
        //REF
        final ReferenceNumber referenceNumber = new ReferenceNumber();
        referenceNumber.setReferenceIdentification(OutboundEdiUtils.truncateAndRemoveDisallowedChars(shipment.getCustomerOrderDto().getApdPoNumber(),16));
        referenceNumber.setReferenceIdentificationQualifier(SUPPLIER_ORDER_NO_QUALIFIER);
        orderInformationGroup.setReferenceNumber1(referenceNumber);
        
        final ArrayList<PackageCartonOrItemLevelGroup> packageCartonGroups = new ArrayList<>();
        final PackageCartonOrItemLevelGroup packageCartonGroup = new PackageCartonOrItemLevelGroup();
        
        final ArrayList<ItemLevelInformationGroup> itemLevelInformationGroups = new ArrayList<>();
        for(LineItemXShipmentDto item:shipment.getLineItemXShipments()){
            final ItemLevelInformationGroup itemLevelInformationGroup = new ItemLevelInformationGroup();
            //HL
            final HierarchicalLevel itemHeirarchicalLevel = new HierarchicalLevel();
            itemHeirarchicalLevel.setHierarchicalIDNumber(HEIRARCHICAL_ID_DEFAULT);
            itemHeirarchicalLevel.setHierarchicalLevelCode(ITEM_LEVEL_CODE);
            itemLevelInformationGroup.setItemLevel(itemHeirarchicalLevel);
            //LIN
            final ItemIdentification itemIdentification = new ItemIdentification();
            itemIdentification.setAssignedIdentificationNumber(OutboundEdiUtils.truncateAndRemoveDisallowedChars(item.getLineItemDto().getCustomerLineNumber(), 4));
            itemIdentification.setProductServiceIDQualifier1(VENDOR_PART_NUMBER_QUALIFIER);
            String productServiceId;
            if(item.getLineItemDto().getCustomerSku()!=null){
            	productServiceId = item.getLineItemDto().getCustomerSku().getValue();
            } else{
                productServiceId = item.getLineItemDto().getApdSku();
            }
			itemIdentification.setProductServiceID1(OutboundEdiUtils.truncateAndRemoveDisallowedChars(productServiceId,30));
            if(item.getLineItemDto().getItem()!=null && 
                    item.getLineItemDto().getItem().lookupManufacturerSku()!=null){
                itemIdentification.setProductServiceIDQualifier2(SERIAL_NUMBER_QUALIFIER);
                itemIdentification.setProductServiceID2(OutboundEdiUtils.truncateAndRemoveDisallowedChars(item.getLineItemDto().getItem().lookupManufacturerSku(), 30));
            }
            itemLevelInformationGroup.setItemIdentification(itemIdentification);
            //SN1
            final ShipmentItemDetail shipmentItemDetail = new ShipmentItemDetail();
            shipmentItemDetail.setNumberOfUnitsShipped(item.getQuantity().toString());

			if(! StringUtils.isEmpty(item.getLineItemDto().getCustomerExpectedUoM())){
				shipmentItemDetail.setUnitOfMeasureCode(OutboundEdiUtils.truncateAndRemoveDisallowedChars(item.getLineItemDto().getCustomerExpectedUoM(), 2, 2));
			} else {
				shipmentItemDetail.setUnitOfMeasureCode(OutboundEdiUtils.truncateAndRemoveDisallowedChars(item.getLineItemDto().getUnitOfMeasure().getName(), 2, 2));
			}
            itemLevelInformationGroup.setShipmentItemDetail(shipmentItemDetail);            

            itemLevelInformationGroups.add(itemLevelInformationGroup);
        }
        Collections.sort(itemLevelInformationGroups, getInvoiceItemGroupComparator());
        packageCartonGroup.setItemLevelInformationGroup(itemLevelInformationGroups);
        
        packageCartonGroups.add(packageCartonGroup);        
        orderInformationGroup.setPackageCartonGroup(packageCartonGroups);
        orderInformationGroups.add(orderInformationGroup);
        shipmentLevelGroup.setOrderInformationGroup(orderInformationGroups);
        shipmentLevelGroups.add(shipmentLevelGroup);
        detail.setShipmentLevelGroups(shipmentLevelGroups);
        body.setDetail(detail);
        
        final Trailer trailer = new Trailer();
        //SE
        final TransactionSetTrailer transactionSetTrailer = new TransactionSetTrailer();
        transactionSetTrailer.setTransactionControlNumber(header.getTransactionSetHeader().getTransactionSetControlNumber());
        transactionSetTrailer.setNumberOfIncludedSegments(generateNumberOfSegmens(itemLevelInformationGroups,shipmentLevelGroup));
        trailer.setTransactionSetTrailer(transactionSetTrailer);
        body.setTrailer(trailer);
        
        advanceShipmentNotices.add(body);        
        advanceShipmentNotice.setBody(advanceShipmentNotices);
        
        //IEA
        final InterchangeEnvelopeTrailer interchangeEnvelopeTrailer = new InterchangeEnvelopeTrailer();
        interchangeEnvelopeTrailer.setInterchangeControlNumber(interchangeEnvelopeHeader.getInterchangeControlNumber());
        interchangeEnvelopeTrailer.setNumberOfIncludedGroups(ONE_MESSAGE_PER_ENVELOPE);
        advanceShipmentNotice.setEnvelopeTrailer(interchangeEnvelopeTrailer);
        
        //GE
        final GroupEnvelopeTrailer groupEnvelopeTrailer = new GroupEnvelopeTrailer();
        groupEnvelopeTrailer.setGroupControlNumber(groupEnvelopeHeader.getGroupControlNumber());
        groupEnvelopeTrailer.setNumberOfTransactionSets(ONE_MESSAGE_PER_ENVELOPE);
        advanceShipmentNotice.setGroupEnvelopeTrailer(groupEnvelopeTrailer);
        
        exchange.getOut().setBody(advanceShipmentNotice);
        exchange.getOut().setHeaders(exchange.getIn().getHeaders());
    }

    private Comparator<ItemLevelInformationGroup> getInvoiceItemGroupComparator() {
        return new Comparator<ItemLevelInformationGroup>() {

            @Override
            public int compare(ItemLevelInformationGroup arg0, ItemLevelInformationGroup arg1) {
                return EDIOrderUtils.ediSegmentCompartor(
                        arg0.getItemIdentification().getAssignedIdentificationNumber(), arg1.getItemIdentification()
                                .getAssignedIdentificationNumber());
            }
        };
    }

    private String generateNumberOfSegmens(ArrayList<ItemLevelInformationGroup> itemLevelInformationGroup,
            ShipmentLevelGroup shipmentLevelGroup) {
        //ST,SE,BSN,HL,TD5,REF*2I,DTM,N1,HL,PRF,REF*1V
        int numberOfSegments = 11;
        if (shipmentLevelGroup.getShipmentFromLocation() != null) {
            numberOfSegments++;
        }
        numberOfSegments += (itemLevelInformationGroup.size() * 3);//three segments per line Item
        return Integer.toString(numberOfSegments);
    }

    public static Name createSimpleShipName(String stringName, String code) {
        final Name name = new Name();
        name.setEntityIdentifierCode(code);
        name.setName(OutboundEdiUtils.truncateAndRemoveDisallowedChars(stringName, 60));
        return name;
    }

    private String getTenantPropertyByName(String propertyName) {
        return TenantConfigRepository.getInstance().getProperty(getTenantName(), "tenant", propertyName);
    }

    private String getTenantName() {
        return TenantConfigRepository.getInstance().getTenantNameById(
                CurrentTenantIdentifierResolverImpl.getCurrentTenant());
    }

}
