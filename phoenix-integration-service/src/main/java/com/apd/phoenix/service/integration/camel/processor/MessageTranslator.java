package com.apd.phoenix.service.integration.camel.processor;

import com.apd.phoenix.service.integration.exception.MessageTranslationException;

public interface MessageTranslator<C, E> {

    public E fromCanonical(C canonical) throws MessageTranslationException;

    public C toCanonical(E externalBinding) throws MessageTranslationException;
}
