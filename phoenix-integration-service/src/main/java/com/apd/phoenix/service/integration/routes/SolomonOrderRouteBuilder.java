package com.apd.phoenix.service.integration.routes;

import org.apache.camel.Exchange;
import org.apache.camel.LoggingLevel;
import org.apache.camel.dataformat.bindy.csv.BindyCsvDataFormat;
import org.apache.camel.spi.DataFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.integration.camel.processor.CSVTransactionLogger;
import com.apd.phoenix.service.integration.camel.processor.CustomerOrderToCSVModelTranslator;
import com.apd.phoenix.service.integration.exception.DtoValidationError;
import com.apd.phoenix.service.integration.multitenancy.AbstractTenantAwareRouteBuilder;
import com.apd.phoenix.service.model.dto.MessageMetadataDto.CommunicationType;
import com.apd.phoenix.service.model.dto.OrderLogDto.EventTypeDto;

public class SolomonOrderRouteBuilder extends AbstractTenantAwareRouteBuilder {

    private static final Logger LOG = LoggerFactory.getLogger(SolomonOrderRouteBuilder.class);

    private String source;

    private String endpoint;

    private String endpointDlq;

    private static final String logSource = "direct:solomnPOLogSource";

    private static final String senderId = "apd";

    private boolean isLogging = true;

    @Override
    public void configureRouteImplementation() throws Exception {

        DataFormat bindy = new BindyCsvDataFormat("com.apd.phoenix.service.integration.solomon.model");

        onException(ClassCastException.class).maximumRedeliveries(0).handled(true).log(LoggingLevel.INFO,
                "Incorrect Type: ${exception.stacktrace}");

        onException(DtoValidationError.class).maximumRedeliveries(0).handled(true).log(LoggingLevel.INFO,
                "Invalid Dto: ${exception.stacktrace}");

        from(source)
        //Set apdPo
                //Translate to csv pojo
                .bean(CustomerOrderToCSVModelTranslator.class, "process")
                //marshal to csv
                .marshal(bindy).log(LoggingLevel.INFO, "${body}")
                //Order Log
                .wireTap(logSource)
                //Set filname
                .setHeader(Exchange.FILE_NAME, simple("${header.apdPo}-${date:now:hhmm}.csv"))
                //Send to endpoint
                .to(endpoint);

        //Logging
        from(logSource)
                //Communication Type
                .setHeader("communicationType", constant(CommunicationType.OUTBOUND))
                //Destination
                .setHeader("destination", constant(endpoint)).setHeader("senderId", constant(senderId)).setHeader(
                        "messageEventType", simple(EventTypeDto.ORDER_ACKNOWLEDGEMENT.name())).choice().when()
                .constant(isLogging)
                //log
                .bean(CSVTransactionLogger.class).end();

    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getEndpoint() {
        return endpoint;
    }

    public void setEndpoint(String endpoint) {
        this.endpoint = endpoint;
    }

    public String getEndpointDlq() {
        return endpointDlq;
    }

    public void setEndpointDlq(String endpointDlq) {
        this.endpointDlq = endpointDlq;
    }

    public boolean isLogging() {
        return isLogging;
    }

    public void setLogging(boolean isLogging) {
        this.isLogging = isLogging;
    }

}
