package com.apd.phoenix.service.integration.camel.processor.cxml;

import javax.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.model.dto.AdvanceShipmentNoticeDto;
import com.apd.phoenix.service.model.dto.PurchaseOrderDto;
import com.apd.phoenix.service.workflow.WorkflowService;

public class CxmlRequestProcessor {

    private static final Logger LOG = LoggerFactory.getLogger(CxmlRequestProcessor.class);

    @Inject
    WorkflowService workflowService;

    public void processInboundOrderRequest(PurchaseOrderDto purchaseOrderDto) {
    }

    public void processInboundShipNoticeRequest(AdvanceShipmentNoticeDto advanceShipmentNoticeDto) {
        LOG.info("Sending po ack to workflow");
        workflowService.processAdvancedShipmentNotice(advanceShipmentNoticeDto);
    }
}
