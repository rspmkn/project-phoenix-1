package com.apd.phoenix.service.integration.camel.processor;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import org.apache.camel.Body;
import org.apache.camel.Exchange;
import org.apache.camel.Header;
import org.apache.commons.lang.StringUtils;
import com.apd.phoenix.service.business.CustomerOrderBp;
import com.apd.phoenix.service.model.Account;
import com.apd.phoenix.service.model.CustomerOrder;
import com.apd.phoenix.service.model.PoNumber;
import com.apd.phoenix.service.model.dto.PurchaseOrderDto;
import com.apd.phoenix.service.model.dto.TransactionDto.TransactionOperation;
import com.apd.phoenix.service.persistence.jpa.AccountDao;
import com.apd.phoenix.service.utility.ErrorEmailService;
import com.apd.phoenix.service.workflow.WorkflowService;

@Stateless
public class OrderRequestProcessor {

    @Inject
    CustomerOrderBp customerOrderBp;

    @Inject
    AccountDao accountDao;

    @Inject
    WorkflowService workflowService;

    @Inject
    ErrorEmailService errorEmailService;

    @SuppressWarnings("unchecked")
    public void setupOrderRequest(@Header("partnerId") String partnerId, Exchange exchange) throws Exception {
        Serializable orderExchangeBody = (Serializable) exchange.getIn().getBody();
        List<PurchaseOrderDto> orders = null;
        if (orderExchangeBody instanceof List) {
            orders = (List<PurchaseOrderDto>) orderExchangeBody;
        }
        else {
            orders = new ArrayList<>();
            orders.add((PurchaseOrderDto) orderExchangeBody);
        }

        for (PurchaseOrderDto poDto : orders) {
            if (poDto.getTransactionOperation() == null) {
                poDto.setTransactionOperation(TransactionOperation.NEW);
            }
            poDto.setUpdateEmptyOrder(Boolean.TRUE);
            CustomerOrder customerOrder = customerOrderBp.createEmptyOrderRequest(partnerId);

            if (customerOrder == null) {
                throw new Exception("Error in order request - could not create empty order with partnerId " + partnerId);
            }
            PoNumber poNumber = customerOrder.getApdPo();
            if (poNumber == null || StringUtils.isEmpty(poNumber.getValue())) {
                throw new Exception("Error in order request - could not get APD po from customer order");
            }
            poDto.setApdPoNumber(poNumber.getValue());
        }
        exchange.getOut().setHeaders(exchange.getIn().getHeaders());
        exchange.getOut().setBody(orders);
    }

    @SuppressWarnings("unchecked")
    public void setupOrderRequestForAccount(Exchange exchange) throws Exception {
        Serializable orderExchangeBody = (Serializable) exchange.getIn().getBody();
        List<PurchaseOrderDto> orders = null;
        if (orderExchangeBody instanceof List) {
            orders = (List<PurchaseOrderDto>) orderExchangeBody;
        }
        else {
            orders = new ArrayList<PurchaseOrderDto>();
            orders.add((PurchaseOrderDto) orderExchangeBody);
        }

        for (PurchaseOrderDto poDto : orders) {
            if (poDto.getTransactionOperation() == null) {
                poDto.setTransactionOperation(TransactionOperation.NEW);
            }
            poDto.setUpdateEmptyOrder(Boolean.TRUE);

            String accountId = "";
            // first check to see if an account has already been found for this PurchaseOrder
            if (poDto.getAccount() != null && StringUtils.isNotBlank(poDto.getAccount().getApdAssignedAccountId())) {
                accountId = poDto.getAccount().getApdAssignedAccountId();
            }
            else if (StringUtils.isNotBlank(poDto.getShipToAddressDto().getCompanyName())) {
                // Otherwise we assume that the ShipTo Company Name uniquely identifies the appropriate customer account/sub-account
                accountId = poDto.getShipToAddressDto().getCompanyName();
                List<Account> accounts = searchByAccountId(accountId);
                if (accounts == null || accounts.isEmpty()) {
                    //AccountId moved to miscShipTo.Division
                    if (poDto.getShipToAddressDto().getMiscShipToDto() != null
                            && !StringUtils.isEmpty(poDto.getShipToAddressDto().getMiscShipToDto().getDivision())) {
                        accountId = poDto.getShipToAddressDto().getMiscShipToDto().getDivision();
                        accounts = searchByAccountId(accountId);
                    }
                    if (accounts == null || accounts.isEmpty()) {
                        errorEmailService.sendSectorValidationFailedEmail(poDto, (String) exchange.getIn().getHeader(
                                "partnerId"));
                        throw new Exception("Error in order request - unrecognized account name.");
                    }
                }
            }
            CustomerOrder customerOrder = customerOrderBp.createEmptyOrderRequestForAccount(accountId);
            if (customerOrder == null) {
                throw new Exception("Error in order request - could not create empty order");
            }
            PoNumber poNumber = customerOrder.getApdPo();
            if (poNumber == null || StringUtils.isEmpty(poNumber.getValue())) {
                throw new Exception("Error in order request - could not get APD po from customer order");
            }
            poDto.setApdPoNumber(poNumber.getValue());
        }
        exchange.getOut().setHeaders(exchange.getIn().getHeaders());
        exchange.getOut().setBody(orders);
    }

    private List<Account> searchByAccountId(String accountId) {
        Account accountSearch = new Account();
        accountSearch.setApdAssignedAccountId(accountId);
        accountSearch.setIsActive(true);
        List<Account> accounts = accountDao.searchByExactExample(accountSearch, 0, 0);
        return accounts;
    }

    public void processOrder(@Body PurchaseOrderDto purchaseOrderDto) throws SystemException, NotSupportedException,
            RollbackException, HeuristicMixedException, HeuristicRollbackException {
        workflowService.processOrder(purchaseOrderDto);
    }
}
