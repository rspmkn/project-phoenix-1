package com.apd.phoenix.service.integration.beans;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.camel.component.ActiveMQComponent;
import org.apache.activemq.pool.PooledConnectionFactory;
import org.apache.camel.CamelContext;
import org.apache.camel.component.cdi.CdiCamelContext;
import org.apache.camel.component.jms.JmsConfiguration;
import org.apache.camel.spi.PackageScanClassResolver;
import org.apache.commons.lang.StringUtils;
import org.apacheextras.camel.jboss.JBossPackageScanClassResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.core.utility.TenantConfigRepository;
import com.apd.phoenix.service.integration.multitenancy.TenantFtpEndpointResolver;
import com.apd.phoenix.service.integration.routes.EDI810OutboundRouteBuilder;
import com.apd.phoenix.service.integration.routes.EDI810v5010OutboundRouteBuilder;
import com.apd.phoenix.service.integration.routes.EDI832OutboundRouteBuilder;
import com.apd.phoenix.service.integration.routes.EDI850OutboundRouteBuilder;
import com.apd.phoenix.service.integration.routes.EDI855OutboundRouteBuilder;
import com.apd.phoenix.service.integration.routes.EDI856OutboundRouteBuilder;
import com.apd.phoenix.service.integration.routes.EDI997OutboundRouteBuilder;
import com.apd.phoenix.service.integration.utility.EDIOrderUtils;
import com.apd.phoenix.service.integration.utility.RouterBeanUtils;
import com.apd.phoenix.service.message.impl.EDIMessageSender;
import com.apd.phoenix.service.persistence.multitenancy.impl.CurrentTenantIdentifierResolverImpl;

@Startup
@Singleton
public class OutboundEDIBean {

    private static final Logger LOG = LoggerFactory.getLogger(OutboundEDIBean.class);

    @Inject
    private CdiCamelContext camelContext;

    @PostConstruct
    public void init() throws Exception {

        CurrentTenantIdentifierResolverImpl.setCurrentTenant(TenantConfigRepository.getInstance().getGenericTenantId());

        Properties commonProperties = TenantConfigRepository.getInstance().getProperties(
                "camel.edi-outbound.integration");

        PackageScanClassResolver jbossResolver = new JBossPackageScanClassResolver();
        camelContext.setPackageScanClassResolver(jbossResolver);

        setupAmq(camelContext, commonProperties.getProperty("jmsBrokerUrl"));

        //Each individual camel route must have a unique start order to specify an order for the routes
        int startOrderOffset = 1;

        String edi997partners = commonProperties.getProperty("edi997partners");
        String[] edi997partnersList = StringUtils.split(edi997partners, ',');

        if (edi997partnersList != null) {
            for (String partner : edi997partnersList) {
                if (StringUtils.isBlank(partner)) {
                    continue;
                }
                LOG.info("Adding partner EDI 997 route: {}", partner);

                //997
                EDI997OutboundRouteBuilder edi997RouteBuilder = new EDI997OutboundRouteBuilder();
                Long tenantId = CurrentTenantIdentifierResolverImpl.getCurrentTenant();
                Properties partnerProperties = TenantConfigRepository.getInstance().getPropertiesByTenantId(tenantId,
                        partner + ".integration");
                edi997RouteBuilder.setStartOrderOffset(startOrderOffset);
                edi997RouteBuilder.setRouteTenantId(tenantId);
                startOrderOffset = startOrderOffset
                        + EDIOrderUtils.getTotalOffset(EDIOrderUtils.RouteOrderOffsetInboundEDI997.class);
                edi997RouteBuilder.setOutputDLQ(commonProperties.getProperty("edi997OutputDLQ"));
                edi997RouteBuilder.setOutboundSource(partnerProperties.getProperty("edi997OutboundSource"), partner);
                edi997RouteBuilder.setOutboundOutput(TenantFtpEndpointResolver.getEndpointName(partner,
                        "edi997PartnerEndpoint"));
                edi997RouteBuilder.setfAResendSource(partnerProperties.getProperty("edi997ResendSource"), partner);
                edi997RouteBuilder.setfAResendOuput(TenantFtpEndpointResolver.getEndpointName(partner,
                        "edi997PartnerEndpoint"));
                camelContext.addRoutes(edi997RouteBuilder);
            }
            camelContext.addRoutes(RouterBeanUtils.createCatchallRouteBuilder(edi997partnersList,
                    EDIMessageSender.EDI_OUTBOUND_997_RESEND));
        }

        //850
        String edi850Partners = commonProperties.getProperty("edi850partners");
        String[] edi850PartnerList = StringUtils.split(edi850Partners, ',');

        if (edi850PartnerList != null) {
            for (String partner : edi850PartnerList) {
                if (StringUtils.isBlank(partner)) {
                    continue;
                }
                LOG.info("Adding partner edi 850 route: {}", partner);
                Properties partnerProperties = TenantConfigRepository.getInstance().getPropertiesByTenantId(
                        CurrentTenantIdentifierResolverImpl.getCurrentTenant(), partner + ".integration");
                String logOutputUri = commonProperties.getProperty("edi850LogOuputUri");
                String logOutputDlq = commonProperties.getProperty("edi850LogOutputDLQ");
                String edi850PartnerEndpoint = TenantFtpEndpointResolver.getEndpointName(partner,
                        "edi850PartnerEndpoint");
                String edi850Endpoint = partnerProperties.getProperty("edi850Endpoint");
                String edi850LogSource = partnerProperties.getProperty("edi850LogSource");
                String edi850PartnerEndpointDlq = commonProperties.getProperty("edi850PartnerEndpointDLQ");
                String poSource = partnerProperties.getProperty("edi850Source");
                String poResendSource = partnerProperties.getProperty("edi850ResendSource");
                String as2downNotify = partnerProperties.getProperty("edi850AS2EndpointDownNotify");

                if (LOG.isDebugEnabled()) {
                    LOG.debug("props: " + "," + logOutputUri + "," + logOutputDlq + "," + edi850PartnerEndpoint + ","
                            + edi850PartnerEndpointDlq + "," + poSource + "," + poResendSource);
                }
                EDI850OutboundRouteBuilder edi850RouteBuilder = new EDI850OutboundRouteBuilder();
                edi850RouteBuilder.setRouteTenantId(CurrentTenantIdentifierResolverImpl.getCurrentTenant());
                edi850RouteBuilder.setStartOrderOffset(startOrderOffset);
                startOrderOffset = startOrderOffset
                        + EDIOrderUtils.getTotalOffset(EDIOrderUtils.RouteOrderOffsetOutboundEDI850.class);
                edi850RouteBuilder.setLogOuputURI(logOutputUri);
                edi850RouteBuilder.setLogOutputDLQ(logOutputDlq);
                edi850RouteBuilder.setEdi850PartnerEndpoint(edi850PartnerEndpoint);
                edi850RouteBuilder.setEdi850PartnerEndpointDLQ(edi850PartnerEndpointDlq);
                edi850RouteBuilder.setPurchaseOrderSource(poSource, partner);
                edi850RouteBuilder.setPurchaseOrderResendSource(poResendSource, partner);
                edi850RouteBuilder.setSenderId(TenantConfigRepository.getInstance().getProperty(
                        CurrentTenantIdentifierResolverImpl.getCurrentTenant(), "tenant", "code").toLowerCase());
                edi850RouteBuilder.setEdiLogSource(edi850LogSource);
                edi850RouteBuilder.setEdi850Endpoint(edi850Endpoint);
                edi850RouteBuilder.setNotifyOnAS2Down(as2downNotify);
                edi850RouteBuilder.setBackupPartnerEndpoint(getSpecifiedOrCreateDefaultBackupEndpoint(partner,
                        partnerProperties, edi850PartnerEndpoint));
                camelContext.addRoutes(edi850RouteBuilder);
            }
            camelContext.addRoutes(RouterBeanUtils.createCatchallRouteBuilder(edi850PartnerList,
                    EDIMessageSender.EDI_OUTBOUND_850));
        }

        //EDI 810v4010
        String edi810Partners = commonProperties.getProperty("edi810partners");
        String[] edi810PartnerList = StringUtils.split(edi810Partners, ',');

        if (edi810PartnerList != null) {
            for (String partner : edi810PartnerList) {
                if (StringUtils.isBlank(partner)) {
                    continue;
                }
                LOG.info("Adding partner edi 810 route: {}", partner);
                Properties partnerProperties = TenantConfigRepository.getInstance().getPropertiesByTenantId(
                        CurrentTenantIdentifierResolverImpl.getCurrentTenant(), partner + ".integration");
                EDI810OutboundRouteBuilder edi810OutboundRouteBuilder = new EDI810OutboundRouteBuilder();
                edi810OutboundRouteBuilder.setStartOrderOffset(startOrderOffset);
                startOrderOffset = startOrderOffset
                        + EDIOrderUtils.getTotalOffset(EDIOrderUtils.RouteOrderOffsetOutboundEDI810.class);
                edi810OutboundRouteBuilder.setPartnerId(partner);
                edi810OutboundRouteBuilder.setSenderId(TenantConfigRepository.getInstance().getProperty(
                        CurrentTenantIdentifierResolverImpl.getCurrentTenant(), "tenant", "code").toLowerCase());
                final String invoiceSource = partnerProperties.getProperty("edi810Source");
                edi810OutboundRouteBuilder.setDataModelSource(invoiceSource);
                final String edi810PartnerEndpoint = partnerProperties.getProperty("edi810PartnerEndpoint");
                edi810OutboundRouteBuilder.setOutboundInvoiceSink(edi810PartnerEndpoint);
                final String edi810logDLQ = commonProperties.getProperty("edi810LogOutputDLQ");
                edi810OutboundRouteBuilder.setUspsLogDLQ(edi810logDLQ);
                //Encryption
                edi810OutboundRouteBuilder
                        .setArmored(new Boolean(partnerProperties.getProperty("encryptionIsArmored")));
                edi810OutboundRouteBuilder.setEncrypted(new Boolean(partnerProperties.getProperty("encryptionEnabled",
                        "false")));
                edi810OutboundRouteBuilder.setTestIndicator(partnerProperties.getProperty("testIndicator"));
                edi810OutboundRouteBuilder
                        .setOutboundInvoiceLogSource(partnerProperties.getProperty("edi810LogSource"));
                edi810OutboundRouteBuilder.setBackupPartnerEndpoint(getSpecifiedOrCreateDefaultBackupEndpoint(partner,
                        partnerProperties, edi810PartnerEndpoint));
                edi810OutboundRouteBuilder.setFtpEndPointDown(partnerProperties
                        .getProperty("ediFTPEndpointDownNotify810"));
                edi810OutboundRouteBuilder.setRenumber(Boolean.TRUE.toString().equalsIgnoreCase(
                        partnerProperties.getProperty(EDI810OutboundRouteBuilder.RENUMBER_HEADER, Boolean.FALSE
                                .toString())));
                camelContext.addRoutes(edi810OutboundRouteBuilder);
            }

        }

        //810v5010

        String edi810v5010partners = commonProperties.getProperty("edi810v5010partners");
        String[] edi810v5010partnersList = StringUtils.split(edi810v5010partners, ',');

        if (edi810v5010partnersList != null) {
            for (String partner : edi810v5010partnersList) {
                if (StringUtils.isBlank(partner)) {
                    continue;
                }
                LOG.info("Adding partner EDI 810v5010 route: {}", partner);
                Properties partnerProperties = TenantConfigRepository.getInstance().getPropertiesByTenantId(
                        CurrentTenantIdentifierResolverImpl.getCurrentTenant(), partner + ".integration");
                EDI810v5010OutboundRouteBuilder route = new EDI810v5010OutboundRouteBuilder();
                route.setStartOrderOffset(startOrderOffset);
                startOrderOffset = startOrderOffset
                        + EDIOrderUtils.getTotalOffset(EDIOrderUtils.RouteOrderOffsetOutboundEDI810v5010.class);
                route.setOutboundInvoiceSource(partnerProperties.getProperty("edi810OutboundSource"));
                route.setOutboundInvoiceLogSource(partnerProperties.getProperty("edi810OutboundLogSource"));
                final String partnerEndpoint = partnerProperties.getProperty("edi810OutboundEndpoint");
                route.setOutboundInvoiceEndpoint(partnerEndpoint);
                route.setOutboundInvoiceEndpointDlq(partnerProperties.getProperty("edi810OutboundEndpointDlq"));
                route.setTestIndicator(partnerProperties.getProperty("testIndicator"));
                route.setBackupPartnerEndpoint(getSpecifiedOrCreateDefaultBackupEndpoint(partner, partnerProperties,
                        partnerEndpoint));
                String senderCode = partnerProperties.getProperty("APDsenderCode");
                senderCode = senderCode.substring(1, senderCode.length() - 1);
                route.setSenderId(senderCode);
                route.setPartnerId(partner);
                camelContext.addRoutes(route);
            }
        }

        //810v4010 and 810v5010 both consume from the same queue, so the catchall should include both sets of partnerIds
        List<String> all810Parters = new ArrayList<String>();
        all810Parters.addAll(Arrays.asList(edi810PartnerList));
        all810Parters.addAll(Arrays.asList(edi810v5010partners));
        // TODO: uncomment this line and remove the lowercase check from novant.integration.properties when all novant orders with 
        // partnerid lowercase novant are invoiced
        //        camelContext.addRoutes(RouterBeanUtils.createCatchallRouteBuilder(all810Parters
        //                .toArray(new String[all810Parters.size()]), EDIMessageSender.EDI_OUTBOUND_810));

        //EDI855
        String edi855partners = commonProperties.getProperty("edi855partners");
        String[] edi855partnersList = StringUtils.split(edi855partners, ',');

        if (edi855partnersList != null) {
            for (String partner : edi855partnersList) {
                if (StringUtils.isBlank(partner)) {
                    continue;
                }
                LOG.info("Adding partner EDI 855 route: {}", partner);
                Properties partnerProperties = TenantConfigRepository.getInstance().getPropertiesByTenantId(
                        CurrentTenantIdentifierResolverImpl.getCurrentTenant(), partner + ".integration");
                EDI855OutboundRouteBuilder eDI855OutboundRouteBuilder = new EDI855OutboundRouteBuilder();
                eDI855OutboundRouteBuilder.setStartOrderOffset(startOrderOffset);
                startOrderOffset = startOrderOffset
                        + EDIOrderUtils.getTotalOffset(EDIOrderUtils.RouteOrderOffsetOutboundEDI855.class);
                eDI855OutboundRouteBuilder.setDatamodelSource(partnerProperties.getProperty("edi855Source"));
                final String partnerEndpoint = partnerProperties.getProperty("edi855PartnerEndpoint");
                eDI855OutboundRouteBuilder.setPartnerEndpoint(partnerEndpoint);
                eDI855OutboundRouteBuilder.setLogDLQ(commonProperties.getProperty("edi855LogOutputDLQ"));
                //Encryption
                eDI855OutboundRouteBuilder.setPartnerId(partner);
                eDI855OutboundRouteBuilder
                        .setArmored(new Boolean(partnerProperties.getProperty("encryptionIsArmored")));
                eDI855OutboundRouteBuilder.setEncrypted(new Boolean(partnerProperties.getProperty("encryptionEnabled",
                        "false")));
                eDI855OutboundRouteBuilder.setOutboundAckLogSource("direct:OutboundAckLogSource" + partner);
                eDI855OutboundRouteBuilder.setBackupPartnerEndpoint(getSpecifiedOrCreateDefaultBackupEndpoint(partner,
                        partnerProperties, partnerEndpoint));
                eDI855OutboundRouteBuilder.setFtpEndPointDown(partnerProperties
                        .getProperty("ediFTPEndpointDownNotify855"));
                camelContext.addRoutes(eDI855OutboundRouteBuilder);
            }
            camelContext.addRoutes(RouterBeanUtils.createCatchallRouteBuilder(edi855partnersList,
                    EDIMessageSender.EDI_OUTBOUND_855));
        }

        //EDI 856
        String edi856Partners = commonProperties.getProperty("edi856partners");
        String[] edi856PartnerList = StringUtils.split(edi856Partners, ',');

        if (edi856PartnerList != null) {
            for (String partner : edi856PartnerList) {
                if (StringUtils.isBlank(partner)) {
                    continue;
                }
                LOG.info("Adding partner EDI 856 route: {}", partner);
                Properties partnerProperties = TenantConfigRepository.getInstance().getPropertiesByTenantId(
                        CurrentTenantIdentifierResolverImpl.getCurrentTenant(), partner + ".integration");
                EDI856OutboundRouteBuilder eDI856OutboundRouteBuilder = new EDI856OutboundRouteBuilder();
                eDI856OutboundRouteBuilder.setStartOrderOffset(startOrderOffset);
                startOrderOffset = startOrderOffset
                        + EDIOrderUtils.getTotalOffset(EDIOrderUtils.RouteOrderOffsetOutboundEDI856.class);
                eDI856OutboundRouteBuilder.setDataModelSource(partnerProperties.getProperty("edi856Source"));
                final String partnerEndpoint = partnerProperties.getProperty("edi856PartnerEndpoint");
                eDI856OutboundRouteBuilder.setPartnerEndpoint(partnerEndpoint);
                eDI856OutboundRouteBuilder.setLogDLQ(commonProperties.getProperty("edi856LogOutputDLQ"));
                eDI856OutboundRouteBuilder.setPartnerId(partner);
                eDI856OutboundRouteBuilder
                        .setArmored(new Boolean(partnerProperties.getProperty("encryptionIsArmored")));
                eDI856OutboundRouteBuilder.setEncrypted(new Boolean(partnerProperties.getProperty("encryptionEnabled",
                        "false")));
                eDI856OutboundRouteBuilder
                        .setOutboundShipNotificationLogSource("direct:OutboundShipNotificationLogSource" + partner);
                eDI856OutboundRouteBuilder.setBackupPartnerEndpoint(getSpecifiedOrCreateDefaultBackupEndpoint(partner,
                        partnerProperties, partnerEndpoint));
                eDI856OutboundRouteBuilder.setFtpEndPointDown(partnerProperties
                        .getProperty("ediFTPEndpointDownNotify856"));
                camelContext.addRoutes(eDI856OutboundRouteBuilder);
            }
            camelContext.addRoutes(RouterBeanUtils.createCatchallRouteBuilder(edi856PartnerList,
                    EDIMessageSender.EDI_OUTBOUND_856));
        }

        //EDI 832
        String edi832Partners = commonProperties.getProperty("edi832.partners");
        String[] edi832PartnerList = StringUtils.split(edi832Partners, ',');

        if (edi832PartnerList != null) {
            for (String partner : edi832PartnerList) {
                if (StringUtils.isBlank(partner)) {
                    continue;
                }
                LOG.info("Adding partner EDI 832 route: {}", partner);
                Properties partnerProperties = TenantConfigRepository.getInstance().getPropertiesByTenantId(
                        CurrentTenantIdentifierResolverImpl.getCurrentTenant(), partner + ".integration");
                EDI832OutboundRouteBuilder eDI832OutboundRouteBuilder = new EDI832OutboundRouteBuilder();
                eDI832OutboundRouteBuilder.setStartOrderOffset(startOrderOffset);
                startOrderOffset = startOrderOffset
                        + EDIOrderUtils.getTotalOffset(EDIOrderUtils.RouteOrderOffsetOutboundEDI832.class);
                eDI832OutboundRouteBuilder
                        .setArmored(new Boolean(partnerProperties.getProperty("encryptionIsArmored")));
                eDI832OutboundRouteBuilder.setEncrypted(new Boolean(partnerProperties.getProperty("encryptionEnabled",
                        "false")));
                eDI832OutboundRouteBuilder.setCatalogSink(partnerProperties.getProperty("edi832CatalogSink"));
                eDI832OutboundRouteBuilder.setCatalogSinkDLQ(partnerProperties.getProperty("edi832CatalogSinkDlq"));
                eDI832OutboundRouteBuilder.setPartnerId(partner);
                final String csvOutput = partnerProperties.getProperty("edi832CsvOutput");
                eDI832OutboundRouteBuilder.setCsvOutput(csvOutput);
                eDI832OutboundRouteBuilder.setCsvSink(partnerProperties.getProperty("edi832CsvSink"));
                eDI832OutboundRouteBuilder.setCsvSinkDLQ(partnerProperties.getProperty("edi832CsvSinkDLQ"));
                eDI832OutboundRouteBuilder.setDataSource(partnerProperties.getProperty("edi832DataSource"));
                eDI832OutboundRouteBuilder.setBackupPartnerEndpoint(getSpecifiedOrCreateDefaultBackupEndpoint(partner,
                        partnerProperties, csvOutput));
                eDI832OutboundRouteBuilder.setDataFormatModelPackage(partnerProperties
                        .getProperty("edi832DataFormatModelPackage"));
                camelContext.addRoutes(eDI832OutboundRouteBuilder);

            }
        }

        camelContext.start();
    }

    @PreDestroy
    public void destroy() throws Exception {
        camelContext.stop();
    }

    public void setupAmq(CamelContext context, String jmsBrokerUrl) {
        Properties amqProperties = TenantConfigRepository.getInstance().getProperties("amq");
        ActiveMQConnectionFactory activeMQConnectionFactory = new ActiveMQConnectionFactory();
        activeMQConnectionFactory.setBrokerURL(jmsBrokerUrl);
        PooledConnectionFactory pooledConnectionFactory = new PooledConnectionFactory();
        pooledConnectionFactory.setConnectionFactory(activeMQConnectionFactory);
        pooledConnectionFactory.setMaxConnections(Integer.parseInt(amqProperties.getProperty(getClass().getSimpleName()
                + ".maxConnections", "2")));
        JmsConfiguration jmsConfiguration = new JmsConfiguration();
        jmsConfiguration.setConnectionFactory(pooledConnectionFactory);
        jmsConfiguration.setConcurrentConsumers(Integer.parseInt(amqProperties.getProperty(getClass().getSimpleName()
                + ".concurrentConsumers", "2")));
        ActiveMQComponent activeMQComponent = new ActiveMQComponent();
        activeMQComponent.setConfiguration(jmsConfiguration);
        context.addComponent("activemq", activeMQComponent);
    }

    private String getSpecifiedOrCreateDefaultBackupEndpoint(String partner, Properties partnerProperties,
            String partnerEndpoint) {
        String specifiedEndpoint = partnerProperties.getProperty("backupPartnerEndpoint");
        if (StringUtils.isEmpty(specifiedEndpoint)) {
            return EDIOrderUtils.createBackupSendEndpoint(partnerEndpoint);
        }
        else {
            return TenantFtpEndpointResolver.getEndpointName(partner, "backupPartnerEndpoint");
        }
    }
}
