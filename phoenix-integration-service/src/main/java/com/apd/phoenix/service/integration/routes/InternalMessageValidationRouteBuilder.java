package com.apd.phoenix.service.integration.routes;

import javax.ejb.Stateful;
import javax.inject.Named;
import org.apache.camel.LoggingLevel;
import com.apd.phoenix.service.camel.DelayBean;
import com.apd.phoenix.service.integration.camel.processor.InternalValidationBean;
import com.apd.phoenix.service.integration.camel.processor.WorkflowServiceCaller;
import com.apd.phoenix.service.integration.multitenancy.AbstractTenantAwareRouteBuilder;

@Named
@Stateful
public class InternalMessageValidationRouteBuilder extends AbstractTenantAwareRouteBuilder {

    private String source;

    @Override
    public void configureRouteImplementation() throws Exception {

        onException(Exception.class).maximumRedeliveries(0).handled(true).log(LoggingLevel.INFO,
                "Error in internal validation: ${exception.stacktrace}");

        from(getSource())
        /*
         * delays picking up more messages to deploy until everything is deployed to avoid 
         * transaction timeouts while the system is under load from deployment
         */
        .autoStartup(false).routePolicy(DelayBean.getDelayPolicy()).bean(InternalValidationBean.class,
                "checkForDuplicateCustomerPo").choice().when(simple("${header.valid}")).bean(
                WorkflowServiceCaller.class, "processOrderPostValidation").end();

    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }
}
