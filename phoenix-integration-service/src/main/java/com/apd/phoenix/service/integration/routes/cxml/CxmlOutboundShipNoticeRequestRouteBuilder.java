package com.apd.phoenix.service.integration.routes.cxml;

import javax.xml.bind.JAXBContext;
import org.apache.camel.Exchange;
import org.apache.camel.LoggingLevel;
import org.apache.camel.converter.jaxb.JaxbDataFormat;
import com.apd.phoenix.service.integration.camel.processor.cxml.CxmlFulfillRequestTranslator;
import com.apd.phoenix.service.integration.camel.processor.cxml.CxmlTransactionLogger;
import com.apd.phoenix.service.integration.multitenancy.AbstractTenantAwareRouteBuilder;
import com.apd.phoenix.service.model.dto.MessageMetadataDto.CommunicationType;
import com.apd.phoenix.service.model.dto.OrderLogDto.EventTypeDto;

public class CxmlOutboundShipNoticeRequestRouteBuilder extends AbstractTenantAwareRouteBuilder {

    private String source;

    private static final String output = "http://component";

    private String outputDLQ;

    private static final String logSource = "direct:cxmlOutSHLogSource";

    private String deploymentMode;

    @Override
    public void configureRouteImplementation() throws Exception {
        JaxbDataFormat jaxbDataFormat = new JaxbDataFormat();

        JAXBContext jaxbContext = JAXBContext
                .newInstance(com.apd.phoenix.service.integration.cxml.model.fulfill.CXML.class);
        jaxbDataFormat.setContext(jaxbContext);

        onException(Exception.class).maximumRedeliveries(0).handled(true).log(LoggingLevel.ERROR,
                CxmlOutboundShipNoticeRequestRouteBuilder.class.getName(),
                "Error in cxml processing: ${exception.stacktrace}");

        //Receive DTO
        from(source)
                //Setup DLQ
                .errorHandler(
                        deadLetterChannel(outputDLQ).maximumRedeliveries(2).redeliveryDelay(2000)
                                .useExponentialBackOff().backOffMultiplier(2))
                //Prepare for endpoint
                .setHeader(Exchange.HTTP_URI, simple("${header.destination}")).setHeader("deploymentMode",
                        simple(deploymentMode)).setHeader("apdPo").jxpath("/in/body/customerOrderDto/apdPoNumber")
                //Convert to cxml pojo
                .bean(CxmlFulfillRequestTranslator.class, "fromAdvShipNoticeToShipNoticeRequest").split().body()
                //Marshal to cxml
                .marshal(jaxbDataFormat)
                //Prepare for output
                .bean(CxmlFulfillRequestTranslator.class, "prepareXmlHeaders")
                //Log
                .log(LoggingLevel.INFO, CxmlOutboundShipNoticeRequestRouteBuilder.class.getName(), "${body}").wireTap(
                        logSource)
                //Send to external endpoint
                .log(LoggingLevel.INFO, CxmlOutboundShipNoticeRequestRouteBuilder.class.getName(),
                        "Sending to http endpoint: ${header.CamelHttpUri}")
                //Send to endpoint
                .to(output)
                //Log reponse
                .log(LoggingLevel.INFO, CxmlOutboundShipNoticeRequestRouteBuilder.class.getName(), "response: ${body}")
                .end();

        from(logSource).setHeader("messageEventType", constant(EventTypeDto.ADVANCED_SHIPMENT_NOTICE)).setHeader(
                "communicationType", constant(CommunicationType.OUTBOUND)).bean(CxmlTransactionLogger.class,
                "logCxmlTransaction");
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getOutputDLQ() {
        return outputDLQ;
    }

    public void setOutputDLQ(String outputDLQ) {
        this.outputDLQ = outputDLQ;
    }

    /**
     * @return the deploymentMode
     */
    public String getDeploymentMode() {
        return deploymentMode;
    }

    /**
     * @param deploymentMode the deploymentMode to set
     */
    public void setDeploymentMode(String deploymentMode) {
        this.deploymentMode = deploymentMode;
    }

}
