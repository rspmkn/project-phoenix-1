package com.apd.phoenix.service.integration.camel.processor;

import java.util.Date;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.model.dto.MessageDto;
import com.apd.phoenix.service.model.dto.MessageMetadataDto;
import com.apd.phoenix.service.model.dto.MessageMetadataDto.CommunicationType;
import com.apd.phoenix.service.model.dto.MessageMetadataDto.MessageType;
import com.apd.phoenix.service.model.dto.OrderLogDto.EventTypeDto;
import com.apd.phoenix.service.model.dto.ShipmentDto;

public class ShipmentDtoToOrderLogTranslator implements Processor {

    private static final Logger LOG = LoggerFactory.getLogger(EDI856toOrderLogTranslator.class);

    @Override
    public void process(Exchange exchange) throws Exception {
        String rawMessage = (String) exchange.getIn().getHeader("rawMessage");
        String destination = (String) exchange.getIn().getHeader("CamelFileHost");
        ShipmentDto shipmentDto = (ShipmentDto) exchange.getIn().getBody();
        LOG.debug("Raw Message: {}", rawMessage);
        String apdPo = shipmentDto.getCustomerOrderDto().getApdPoNumber();
        LOG.debug("Apd po number: {}", apdPo);
        if (destination == null) {
            //TODO: Configure how ftp host is extracted from headers
            destination = "none";
        }

        MessageMetadataDto metadata = new MessageMetadataDto();
        metadata.setCommunicationType(CommunicationType.INBOUND);
        metadata.setContentLength(rawMessage.length());
        metadata.setDestination(destination);
        metadata.setFilePath(apdPo + "/" + apdPo + "-marfieldShipmentDocument-" + System.currentTimeMillis() / 1000
                + ".txt");
        metadata.setMessageDate(new Date());
        metadata.setMessageType(MessageType.TEXT);

        MessageDto message = new MessageDto();
        message.setMessageMetadataDto(metadata);
        message.setContent(rawMessage);
        exchange.getOut().setHeaders(exchange.getIn().getHeaders());
        exchange.getOut().setBody(message);
        exchange.getOut().setHeader("apdPo", apdPo);
        exchange.getOut().setHeader("messageEventType", EventTypeDto.ADVANCED_SHIPMENT_NOTICE);

    }
}
