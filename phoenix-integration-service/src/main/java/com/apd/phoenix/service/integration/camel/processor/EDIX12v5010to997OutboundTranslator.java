package com.apd.phoenix.service.integration.camel.processor;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Set;
import javax.edi.model.x12.edi997.FunctionalAcknowledgement;
import javax.edi.model.x12.edi997.segment.Detail;
import javax.edi.model.x12.edi997.segment.FunctionalAcknowledgementBody;
import javax.edi.model.x12.edi997.segment.Header;
import javax.edi.model.x12.edi997.segment.Trailer;
import javax.edi.model.x12.edi997.segment.TransactionResponseGroup;
import javax.edi.model.x12.segment.FunctionalGroupResponseHeader;
import javax.edi.model.x12.segment.FunctionalGroupResponseTrailer;
import javax.edi.model.x12.segment.TransactionSetHeader;
import javax.edi.model.x12.segment.TransactionSetResponseHeader;
import javax.edi.model.x12.segment.TransactionSetResponseTrailer;
import javax.edi.model.x12.segment.TransactionSetTrailer;
import javax.edi.model.x12.v5010.X12Body;
import javax.edi.model.x12.v5010.X12Message;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.integration.factory.EDIOutboundFactory;
import com.apd.phoenix.service.integration.utility.EDIOrderUtils;

public class EDIX12v5010to997OutboundTranslator implements Processor {

    private static final Logger LOG = LoggerFactory.getLogger(EDIX12v5010to997OutboundTranslator.class);

    public static String FA_TRANSACTION_SET_INDENTIFIER_CODE = "997";
    public static String FA_REJECTED = "R";
    public static String FA_ACCEPTED = "A";
    public static String FA_FUNCTIONAL_ID_CODE = "FA";
    public static String GROUP_TRANSACTION_COUNT = "1";
    public static String TRANSACTION_COUNT = "1";

    private int acceptedTS;
    private int receivedTS;
    private int includedTS;
    private boolean isInterchangeValid = true;

    @Override
	public void process(Exchange exchange) throws Exception {
    	
		X12Message x12Message = (X12Message) exchange
		.getIn().getBody();
		
		
    	acceptedTS = x12Message.getBody().size();
    	receivedTS = x12Message.getBody().size();
    	includedTS = x12Message.getBody().size();
    	
    	long interchangeControlId = (long)exchange.getIn().getHeader("interchangeControlId");
    	long groupControlId = (long)exchange.getIn().getHeader("groupControlId");
    	long transactionControlId = (long)exchange.getIn().getHeader("transactionControlId");
    	String senderCode = (String) exchange.getIn().getHeader("senderCode");
    	String receiverCode = (String) exchange.getIn().getHeader("receiverCode");
    	String testIndicator = (String) exchange.getIn().getHeader("testIndicator");
    
    	
    	exchange.getIn().setHeader("isValid", true);
    	

		if (x12Message.getInterchangeEnvelopeHeader()
				.getiSAAcknowledgmentRequested().equals("1")) {
			exchange.getIn().setHeader("fARequested", true);
		} else {
			exchange.getIn().setHeader("fARequested", false);
		}

		FunctionalAcknowledgement functionalAcknowledgement = new FunctionalAcknowledgement();
		functionalAcknowledgement.setEnvelopeHeader(EDIOutboundFactory.generateInterchangeEnvelopeHeader(senderCode, receiverCode, interchangeControlId, testIndicator));
		functionalAcknowledgement.setGroupEnvelopeHeader(EDIOutboundFactory.generateGroupEnvelopeHeader(senderCode, receiverCode, groupControlId, FA_FUNCTIONAL_ID_CODE));
		functionalAcknowledgement.setGroupEnvelopeTrailer(EDIOutboundFactory.generateGroupEnvelopeTrailer(TRANSACTION_COUNT, groupControlId));
		functionalAcknowledgement.setEnvelopeTrailer(EDIOutboundFactory.generateInterchangeEnvelopeTrailer( interchangeControlId, GROUP_TRANSACTION_COUNT));
		
		Collection<FunctionalAcknowledgementBody> bodies = new ArrayList<>();
		FunctionalAcknowledgementBody body = new FunctionalAcknowledgementBody();
		body.setHeader(new Header());
		body.getHeader().setTransactionSetHeader(new TransactionSetHeader());
		body.getHeader().getTransactionSetHeader().setTransactionSetIdentifierCode(FA_TRANSACTION_SET_INDENTIFIER_CODE);
		body.getHeader().getTransactionSetHeader().setTransactionSetControlNumber(EDIOutboundFactory.zeroPad(transactionControlId));
		body.getHeader().setFunctionalGroupResponseHeader(new FunctionalGroupResponseHeader());
		body.getHeader().getFunctionalGroupResponseHeader().setFunctionalIdentifierCode(x12Message.getGroupEnvelopeHeader().getFunctionalIDCode());
		body.getHeader().getFunctionalGroupResponseHeader().setGroupControlNumber(x12Message.getGroupEnvelopeHeader().getGroupControlNumber());
		body.setDetail(new Detail());
		Collection<TransactionResponseGroup> transactionResponseGroup = new ArrayList<>();
		
		

		//Validate envelope, group header/trailer
		Validator validator = Validation.buildDefaultValidatorFactory()
				.getValidator();
		Set<ConstraintViolation<X12Message>> violations = validator
				.validate(x12Message);


		
		if (violations.size() > 0) {
			exchange.getIn().setHeader("isValid", false);
			isInterchangeValid = false;
			for (ConstraintViolation<X12Message> cv : violations) {
				LOG.info("Violation: {}", cv.getConstraintDescriptor());
			}
		} 
		
		//Validate each transaction
		for(X12Body transaction : x12Message.getBody()) {
			Set<ConstraintViolation<X12Body>> transactionViolations = validator
					.validate(transaction);
			

			if(transactionViolations.size() > 0) {
				exchange.getIn().setHeader("isValid", false);
				for (ConstraintViolation<X12Body> cv : transactionViolations) {
					LOG.debug("Violation: {}", cv.getConstraintDescriptor());	
				}
				acceptedTS -= 1;
				TransactionResponseGroup group = new TransactionResponseGroup();
				group.setTransactionSetResponseHeader(new TransactionSetResponseHeader());
                                if(transaction.getHeader()!=null){
                                    group.getTransactionSetResponseHeader().setTransactionSetIdentifierCode(transaction.getHeader().getTransactionSetHeader().getTransactionSetIdentifierCode());
                                    group.getTransactionSetResponseHeader().setTransactionSetControlNumber(transaction.getHeader().getTransactionSetHeader().getTransactionSetControlNumber());
                                }
                                else{
                                    LOG.error("Recieved 850 with malformed header.");
                                    group.getTransactionSetResponseHeader().setTransactionSetIdentifierCode("850 had malformed header.");
                                    group.getTransactionSetResponseHeader().setTransactionSetControlNumber("850 had malfomred header");
                                }
				group.setTransactionSetResponseTrailer(new TransactionSetResponseTrailer());
				group.getTransactionSetResponseTrailer().setTransactionSetAcknowledgementCode(FA_REJECTED);
				transactionResponseGroup.add(group);
			}
			else {
				TransactionResponseGroup group = new TransactionResponseGroup();
				group.setTransactionSetResponseTrailer(new TransactionSetResponseTrailer());
				group.getTransactionSetResponseTrailer().setTransactionSetAcknowledgementCode(FA_ACCEPTED);
				transactionResponseGroup.add(group);
			}
		}

		body.getDetail().setTransactionResponseGroup(transactionResponseGroup);
		body.setTrailer(new Trailer());
		body.getTrailer().setFunctionalGroupResponseTrailer(new FunctionalGroupResponseTrailer());
		
		if(!isInterchangeValid) {
			acceptedTS = 0;
		}
		
		String groupResponseCode = FA_ACCEPTED;
		if(acceptedTS < receivedTS) {
			groupResponseCode = FA_REJECTED;
		}
		body.getTrailer().getFunctionalGroupResponseTrailer().setFunctionalGroupAcknowledgeCode(groupResponseCode);
		body.getTrailer().getFunctionalGroupResponseTrailer().setNumberOfAcceptedTransactionSets(acceptedTS+"");
		body.getTrailer().getFunctionalGroupResponseTrailer().setNumberOfReceivedTransactionSets(receivedTS+"");
		body.getTrailer().getFunctionalGroupResponseTrailer().setNumberOfTransactionSetsIncluded(includedTS+"");
		
		body.getTrailer().setTransactionSetTrailer(new TransactionSetTrailer());
		body.getTrailer().getTransactionSetTrailer().setNumberOfIncludedSegments(EDIOrderUtils.getSegmentCount(body)+"");
		body.getTrailer().getTransactionSetTrailer().setTransactionControlNumber(EDIOutboundFactory.zeroPad(transactionControlId));
		bodies.add(body);
		functionalAcknowledgement.setBody(bodies);
		
		exchange.getIn().setHeader("FA", functionalAcknowledgement);
		exchange.getIn().setHeader("transactionId", Long.toString(transactionControlId));

	}
}
