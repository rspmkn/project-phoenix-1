package com.apd.phoenix.service.integration.camel.processor;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import org.apache.camel.Body;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.integration.exception.DtoValidationError;
import com.apd.phoenix.service.integration.solomon.model.SolomonLineItem;
import com.apd.phoenix.service.integration.solomon.model.SolomonOrder;
import com.apd.phoenix.service.model.dto.AccountDto;
import com.apd.phoenix.service.model.dto.AddressDto;
import com.apd.phoenix.service.model.dto.LineItemDto;
import com.apd.phoenix.service.model.dto.PurchaseOrderDto;

/**
 * CustomerOrderDto -> SolomonOrder
 * @author RHC
 *
 */
public class CustomerOrderToCSVModelTranslator {

    private static final Logger LOG = LoggerFactory.getLogger(CustomerOrderToCSVModelTranslator.class);

    public SolomonOrder process(@Body PurchaseOrderDto customerOrderDto) throws Exception {
		
		validateCustomerOrderDto(customerOrderDto);
		
		
		AccountDto accountDto = customerOrderDto.getAccount();
		AddressDto addressDto = customerOrderDto.getShipToAddressDto();
		
		
		SolomonOrder so = new SolomonOrder();
		
    	so.setCustid(accountDto.getSolomonCustomerID());
    	so.setShipToId(addressDto.getShipToId());
    	if (StringUtils.isBlank(so.getShipToId()) && addressDto.getMiscShipToDto() != null) {
    		so.setShipToId(addressDto.getMiscShipToDto().getAddressId());
    		if (StringUtils.isBlank(so.getShipToId())) {
    			so.setShipToId(addressDto.getMiscShipToDto().getAddressId36());
    		}
    	}
        so.setAddr1(addressDto.getLine1());
        so.setAddr2(addressDto.getLine2());
        so.setCity(addressDto.getCity());
        so.setCounty(addressDto.getCounty());
        so.setState(addressDto.getState());
        so.setZip(addressDto.getZip());
        so.setGeocode(addressDto.getGeoCode());
        if (customerOrderDto.retrieveCustomerPoNumber() != null && StringUtils.isNotBlank(customerOrderDto.retrieveCustomerPoNumber().getValue())) {
        	so.setPonbr(customerOrderDto.retrieveCustomerPoNumber().getValue());
        }
        else if (customerOrderDto.retrieveBlanketPoNumber() != null && StringUtils.isNotBlank(customerOrderDto.retrieveBlanketPoNumber().getValue())) {
        	so.setPonbr(customerOrderDto.retrieveBlanketPoNumber().getValue());
        }
        else {
        	so.setPonbr(customerOrderDto.retrieveApdPoNumber().getValue());
        }
        so.setApdponbr(customerOrderDto.retrieveApdPoNumber().getValue());
        so.setOrderDate(customerOrderDto.getOrderDate());
        so.setRequestDate(customerOrderDto.getOrderDate());
        so.setTerms(accountDto.getTerms());
        so.setOrderTotal(customerOrderDto.getOrderTotal());
        so.setSiteId("NC001");
        
    	List<SolomonLineItem> lineItems = new ArrayList<>();
        
    	for(LineItemDto lineItem : customerOrderDto.getItems()) {
	    	SolomonLineItem sli1 = new SolomonLineItem();
	    	if (lineItem.getTaxable() != null && lineItem.getTaxable()) {
	    		sli1.setTaxable("Yes");
		        sli1.setTaxableAmount(lineItem.getSubTotal());
		        sli1.setTaxAmount(lineItem.getMaximumTaxToCharge());
	    	}
	    	else {
	    		sli1.setTaxable("No");
	    	}
	    	if (sli1.getTaxableAmount() == null) {
		        sli1.setTaxableAmount(BigDecimal.ZERO);
	    	}
	    	if (sli1.getTaxAmount() == null) {
	    		sli1.setTaxAmount(BigDecimal.ZERO);
	    	}
	    	if(lineItem.getVendorSku() != null) {
	    		sli1.setInventoryId(lineItem.getVendorSku().getValue());
	    		sli1.setProdCode(lineItem.getVendorSku().getValue());
		        sli1.setProdID(lineItem.getVendorSku().getValue());
	    	}	        
	        sli1.setDescription(lineItem.getShortName());
	        sli1.setOrdQty(lineItem.getQuantity().intValue());
	        sli1.setUom(lineItem.getUnitOfMeasure().getName());
	        sli1.setUnitPrice(lineItem.getUnitPrice());
	        
	        lineItems.add(sli1);
    	}
        so.setLineItems(lineItems);
        
        return so;
		
	}

    private void validateCustomerOrderDto(PurchaseOrderDto customerOrderDto) throws DtoValidationError {
        //Account validations
        AccountDto accountDto = customerOrderDto.getAccount();
        if (accountDto == null) {
            throw new DtoValidationError("Account cannot be null");
        }

        if (StringUtils.isEmpty(accountDto.getSolomonCustomerID())) {
            throw new DtoValidationError("Account customer ID cannot be null");
        }

        //Address validations
        AddressDto addressDto = customerOrderDto.getShipToAddressDto();
        if (addressDto == null) {
            throw new DtoValidationError("Address cannot be null");
        }

        if (StringUtils.isEmpty(addressDto.getLine1())) {
            throw new DtoValidationError("Address line1 cannot be null");
        }

        if (StringUtils.isEmpty(addressDto.getCity())) {
            throw new DtoValidationError("Address city cannot be null");
        }

        if (StringUtils.isEmpty(addressDto.getState())) {
            throw new DtoValidationError("Address state cannot be null");
        }

        if (StringUtils.isEmpty(addressDto.getZip())) {
            throw new DtoValidationError("Address zip cannot be null");
        }

        //Customer Order Validations

        if (customerOrderDto.retrieveApdPoNumber() == null
                || StringUtils.isEmpty(customerOrderDto.retrieveApdPoNumber().getValue())) {
            throw new DtoValidationError("Customer Order APD PO cannot be null");
        }

        if (customerOrderDto.getOrderDate() == null) {
            throw new DtoValidationError("Customer Order date cannot be null");
        }

        if (customerOrderDto.getOrderTotal() == null || customerOrderDto.getOrderTotal().doubleValue() < 0) {
            throw new DtoValidationError("Customer Order total cannot be null or less than 0");
        }

        //Item Validations
        for (LineItemDto lineItem : customerOrderDto.getItems()) {

            if (StringUtils.isEmpty(lineItem.getApdSku())) {
                throw new DtoValidationError("Line Item cannot be null");
            }

            if (lineItem.getQuantity() == null || lineItem.getQuantity().compareTo(BigInteger.ZERO) == -1) {
                throw new DtoValidationError("Line Item quantity cannot be less than 0");
            }

            if (StringUtils.isEmpty(lineItem.getApdSku())) {
                throw new DtoValidationError("Line Item cannot be null");
            }

        }
    }
}