package com.apd.phoenix.service.integration.utility;

import java.io.InputStream;
import java.io.OutputStream;
import org.apache.camel.Exchange;
import org.apache.camel.converter.crypto.PGPDataFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.core.utility.TenantConfigRepository;
import com.apd.phoenix.service.persistence.multitenancy.impl.CurrentTenantIdentifierResolverImpl;

public class TenantAwarePGPDataFormat extends PGPDataFormat {

    private String partnerId;
    private Logger LOG = LoggerFactory.getLogger(TenantAwarePGPDataFormat.class);

    @Override
    public void marshal(Exchange exchange, Object graph, OutputStream outputStream) throws Exception {
        this.getTenantDetails();
        super.marshal(exchange, graph, outputStream);
    }

    @Override
    public Object unmarshal(Exchange exchange, InputStream encryptedStream) throws Exception {
        this.getTenantDetails();
        return super.unmarshal(exchange, encryptedStream);
    }

    public String getPartnerId() {
        return partnerId;
    }

    public void setPartnerId(String partnerId) {
        this.partnerId = partnerId;
    }

    private void getTenantDetails() {
        Long tenantId = CurrentTenantIdentifierResolverImpl.getCurrentTenant();
        //Get tenant specific key file name and path
        String keyFileName = TenantConfigRepository.getInstance().getProperty(tenantId, "tenant",
                partnerId + "_encryption_key_file_name");
        String keyFilePath = TenantConfigRepository.getInstance().getTenantSpecificEncryptionKeyPath(tenantId,
                keyFileName);

        if (LOG.isDebugEnabled()) {
            LOG.debug("Tenant's encryption key found here: " + keyFilePath);
        }

        if (keyFilePath == null || keyFilePath.equalsIgnoreCase("")) {
            throw new UnsupportedOperationException("No encryption key file found for partner: " + partnerId
                    + " and tenantId: " + tenantId);
        }
        else {
            this.setKeyFileName(keyFilePath);
        }

        String userId = TenantConfigRepository.getInstance().getProperty(tenantId, "tenant",
                partnerId + "_encryption_key_user_id");

        if (userId == null || userId.equalsIgnoreCase("")) {
            throw new UnsupportedOperationException("No encryption key user id found for partner:" + partnerId
                    + " and tenantId: " + tenantId);
        }

        this.setKeyUserid(userId);
    }

}
