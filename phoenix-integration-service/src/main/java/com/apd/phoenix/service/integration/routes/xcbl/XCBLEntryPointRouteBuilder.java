package com.apd.phoenix.service.integration.routes.xcbl;

import org.apache.camel.LoggingLevel;
import com.apd.phoenix.service.integration.camel.processor.GpgCamelCallBean;
import com.apd.phoenix.service.integration.camel.processor.XMLNamespaceFilter;
import com.apd.phoenix.service.integration.camel.processor.xcbl.XCBLEntryPointProcessor;
import com.apd.phoenix.service.integration.multitenancy.AbstractTenantAwareRouteBuilder;

public class XCBLEntryPointRouteBuilder extends AbstractTenantAwareRouteBuilder {

    private String transactionSource;

    private String xmlnsQualifier;

    // Message Acknowledgment
    private String transactionMessageAckSink;

    // Order
    private String transactionOrderSink;

    private String transactionUnsupportedSink;

    private String partnerId;

    private boolean decryptionRequired = false;

    private boolean armored = true;
    private String gpgTempDirectory;
    private String gpgExecutableDirectory;

    @Override
    public void configureRouteImplementation() throws Exception {

        onException(Exception.class).maximumRedeliveries(0).handled(true).log(LoggingLevel.ERROR,
                "Error in XCBL processing: ${exception.stacktrace}");

        from(transactionSource).setHeader("partnerId", simple(partnerId))
                // decrypt message body if necessary
                .choice().when(constant(decryptionRequired)).setHeader("CamelFileName",
                        simple("${header.CamelFileName}${header.partnerId}")).log(LoggingLevel.DEBUG,
                        "XCBL message before PGP decryption: ${body}").to("file:" + gpgTempDirectory).setHeader(
                        GpgCamelCallBean.TMP_DIRECTORY).constant(gpgTempDirectory).setHeader(
                        GpgCamelCallBean.PARTNER_ID).constant(partnerId)
                //decrypts the file using a gpg command line
                .bean(GpgCamelCallBean.class, "setGpgParameters").to("exec:gpg?workingDir=" + gpgExecutableDirectory)
                .log(LoggingLevel.DEBUG, XCBLEntryPointRouteBuilder.class.getName(),
                        "XCBL message after PGP decryption: ${body}").end().setHeader("xmlnsQualifier",
                        constant(xmlnsQualifier))
                // Filter message to apply custom xmlns (pass through unaltered if none specified)
                .bean(XMLNamespaceFilter.class, "filter")
                // Route xcbl message to the appropriate xcbl transaction processing endpoint
                .process(new XCBLEntryPointProcessor()).choice()
                // Order
                .when(header("functionId").isEqualTo("Order")).to(transactionOrderSink)
                // Unsupported/Unknown Transaction Type
                .otherwise().to(transactionUnsupportedSink).end()
                //delete temporary encryption file
                .choice().when(constant(decryptionRequired)).bean(GpgCamelCallBean.class, "setRmParameters").to(
                        "exec:rm?workingDir=" + gpgTempDirectory).end();
    }

    public String getTransactionSource() {
        return transactionSource;
    }

    public void setTransactionSource(String transactionSource) {
        this.transactionSource = transactionSource;
    }

    public String getTransactionMessageAckSink() {
        return transactionMessageAckSink;
    }

    public void setTransactionMessageAckSink(String transactionMessageAckSink) {
        this.transactionMessageAckSink = transactionMessageAckSink;
    }

    public String getTransactionUnsupportedSink() {
        return transactionUnsupportedSink;
    }

    public void setTransactionUnsupportedSink(String transactionUnsupportedSink) {
        this.transactionUnsupportedSink = transactionUnsupportedSink;
    }

    public String getPartnerId() {
        return partnerId;
    }

    public void setPartnerId(String partnerId) {
        this.partnerId = partnerId;
    }

    /**
     * @return the transactionOrderSink
     */
    public String getTransactionOrderSink() {
        return transactionOrderSink;
    }

    /**
     * @param transactionOrderSink
     *            the transactionOrderSink to set
     */
    public void setTransactionOrderSink(String transactionOrderSink) {
        this.transactionOrderSink = transactionOrderSink;
    }

    public boolean isDecryptionRequired() {
        return decryptionRequired;
    }

    public void setDecryptionRequired(boolean decryptionRequired) {
        this.decryptionRequired = decryptionRequired;
    }

    public boolean isArmored() {
        return armored;
    }

    public void setArmored(boolean isArmored) {
        this.armored = isArmored;
    }

    public String getXmlnsQualifier() {
        return xmlnsQualifier;
    }

    public void setXmlnsQualifier(String xmlnsFilterClass) {
        this.xmlnsQualifier = xmlnsFilterClass;
    }

    /**
     * @return the gpgTempDirectory
     */
    public String getGpgTempDirectory() {
        return gpgTempDirectory;
    }

    /**
     * @param gpgTempDirectory the gpgTempDirectory to set
     */
    public void setGpgTempDirectory(String gpgTempDirectory) {
        this.gpgTempDirectory = gpgTempDirectory;
    }

    /**
     * @return the gpgExecutableDirectory
     */
    public String getGpgExecutableDirectory() {
        return gpgExecutableDirectory;
    }

    /**
     * @param gpgExecutableDirectory the gpgExecutableDirectory to set
     */
    public void setGpgExecutableDirectory(String gpgExecutableDirectory) {
        this.gpgExecutableDirectory = gpgExecutableDirectory;
    }
}
