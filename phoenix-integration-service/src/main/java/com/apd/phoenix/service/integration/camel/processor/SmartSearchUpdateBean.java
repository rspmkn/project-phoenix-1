package com.apd.phoenix.service.integration.camel.processor;

import java.util.Map;
import javax.ejb.Stateless;
import javax.inject.Inject;
import org.apache.camel.Exchange;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.search.integration.client.backend.SmartSearchCatalogMaintenanceMessage;
import com.apd.phoenix.service.search.integration.client.backend.SmartSearchCatalogMaintenanceMessage.SmartSearchCatalogMaintenanceMessageContent;
import com.apd.phoenix.service.search.integration.client.backend.SmartSearchCatalogMaintenanceService;

@Stateless
public class SmartSearchUpdateBean {

    private static final Logger LOGGER = LoggerFactory.getLogger(SmartSearchUpdateBean.class);

    @Inject
    private SmartSearchCatalogMaintenanceService maintenanceService;

    public void sendMessage(Exchange e) {
        SmartSearchCatalogMaintenanceMessage message = (SmartSearchCatalogMaintenanceMessage) e.getIn().getBody();
        try {
            switch (message.getAction()) {
                case UPDATE_ITEM:
                    maintenanceService
                            .updateItems((Map<String, Map<Long, SmartSearchCatalogMaintenanceMessageContent>>) message
                                    .getMessage());
                    return;
                case REMOVE_LIST:
                    maintenanceService
                            .removeItems((Map<String, Map<Long, SmartSearchCatalogMaintenanceMessageContent>>) message
                                    .getMessage());
                    return;
                case INSERT_LIST:
                    maintenanceService
                            .insertItems((Map<String, Map<Long, SmartSearchCatalogMaintenanceMessageContent>>) message
                                    .getMessage());
                    return;
            }
        }
        catch (Exception ex) {
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("Error sending message to smart search", ex);
            }
        }
    }

}
