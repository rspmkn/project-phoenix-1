package com.apd.phoenix.service.integration.camel.processor;

import java.util.Date;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.model.dto.MessageDto;
import com.apd.phoenix.service.model.dto.MessageMetadataDto;
import com.apd.phoenix.service.model.dto.MessageMetadataDto.CommunicationType;
import com.apd.phoenix.service.model.dto.MessageMetadataDto.MessageType;

public class ConvertToMessageDTOProcessor implements Processor {

    private static final Logger LOG = LoggerFactory.getLogger(ConvertToMessageDTOProcessor.class);

    @Override
    public void process(Exchange exchange) throws Exception {
        MessageDto messageDto = new MessageDto();
        MessageMetadataDto messageMetadataDto = new MessageMetadataDto();

        CommunicationType communicationType = exchange.getIn().getHeader("messageCommunicationType",
                CommunicationType.class);
        LOG.info("Com Type: " + communicationType.getLabel());
        messageMetadataDto.setCommunicationType(communicationType);

        MessageType messageType = exchange.getIn().getHeader("messageType", MessageType.class);
        LOG.info("Mes Type: " + messageType.getLabel());
        messageMetadataDto.setMessageType(messageType);

        String destination = exchange.getIn().getHeader("messageDestination", String.class);
        LOG.info("dest: " + destination);
        messageMetadataDto.setDestination(destination);

        String filePath = exchange.getIn().getHeader("messageFilepath", String.class);
        LOG.info("filepath: " + filePath);
        messageMetadataDto.setFilePath(filePath);

        messageMetadataDto.setMessageDate(new Date());

        String content = exchange.getIn().getBody(String.class);

        messageMetadataDto.setContentLength(content.getBytes().length);
        messageDto.setContent(content);
        messageDto.setMessageMetadataDto(messageMetadataDto);

        exchange.getOut().setBody(messageDto, MessageDto.class);
        exchange.getOut().setHeaders(exchange.getIn().getHeaders());
    }

}
