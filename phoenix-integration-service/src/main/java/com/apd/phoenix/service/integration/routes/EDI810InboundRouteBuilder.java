/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.service.integration.routes;

import javax.edi.model.x12.edi810.Invoice;
import org.apache.camel.Exchange;
import org.apache.camel.LoggingLevel;
import org.apache.camel.Predicate;
import org.apache.camel.spi.DataFormat;
import com.apd.phoenix.service.dataformat.edi.EDIDataFormat;
import com.apd.phoenix.service.integration.camel.processor.Convert810toOrderLogTranslator;
import com.apd.phoenix.service.integration.camel.processor.EDI810CreditToOrderLogTranslator;
import com.apd.phoenix.service.integration.camel.processor.EDI810ToCreditInvoiceTranslator;
import com.apd.phoenix.service.integration.camel.processor.Edi810ToInvoiceTranslator;
import com.apd.phoenix.service.integration.camel.processor.MessageServiceBean;
import com.apd.phoenix.service.integration.camel.processor.WorkflowServiceCaller;
import com.apd.phoenix.service.integration.utility.EDIOrderUtils;

/**
 *
 * @author nreidelb
 */
public class EDI810InboundRouteBuilder extends EDIRouteBuilder {

    private String standardInvoice = "direct:standardInvoice";

    private String creditInvoice = "direct:creditInvoice";

    private String source;

    private String output;

    private String invoiceFormattedSource = "direct:invoiceFormattedSource";

    private String invoiceLogSource = "direct:invoiceLogSource";

    private String creditFormattedSource = "direct:creditFormattedSource";

    private String creditLogSource = "direct:creditLogSource";

    private String creditLogOutput = "http://placeholder";

    private String creditOutput;

    private String outputDLQ;

    private String logOutputDLQ;

    private String logOutput = "http://placeholder";

    private String logOutputURI;

    private String senderId;
    private Boolean useDifferetnCreditPath;
    private String differntCredit = "direct:differentCredit";

    @Override
    public void configureRouteImplementation() throws Exception {
        DataFormat format = new EDIDataFormat(Invoice.class);

        Predicate differentCreditPredeicate = new Predicate() {

            @Override
            public boolean matches(Exchange exchange) {
                return getUseDifferetnCreditPath();
            }
        };

        //set direct endpoints to have unique names
        differntCredit = differntCredit + senderId;
        standardInvoice = standardInvoice + senderId;
        creditInvoice = creditInvoice + senderId;
        invoiceFormattedSource = invoiceFormattedSource + senderId;
        invoiceLogSource = invoiceLogSource + senderId;
        creditFormattedSource = creditFormattedSource + senderId;
        creditLogSource = creditLogSource + senderId;

        onException(Exception.class).maximumRedeliveries(0).handled(true).log(LoggingLevel.INFO,
                "Error in edi 810 inbound processing: ${exception.stacktrace}");

        from(source).startupOrder(
                getStartOrderOffset()
                        + EDIOrderUtils.getOffset(EDIOrderUtils.RouteOrderOffsetInboundEDI810.INBOUND_810)).unmarshal(
                format)
                //Parse transaction information
                .setHeader("senderId", constant(senderId)).setHeader("interchangeId").jxpath(
                        "/in/body/envelopeHeader/interchangeControlNumber").setHeader("groupId").jxpath(
                        "/in/body/groupEnvelopeHeader/groupControlNumber")
                //Iterate over each invoice
                .split().jxpath("/in/body/body")

                .setHeader("transactionId").jxpath("/in/body/header/transactionSetHeader/transactionSetControlNumber")
                //route to diferent ways based on whether it is a credit or debit invoice
                .choice().when().jxpath("in/body/header/beginningSegmentforInvoice/transactionTypeCode='DI'").to(
                        standardInvoice).when().jxpath(
                        "in/body/header/beginningSegmentforInvoice/transactionTypeCode='DR'").to(standardInvoice)
                .when().jxpath("in/body/header/beginningSegmentforInvoice/transactionTypeCode='CR'").to(creditInvoice);

        from(standardInvoice).startupOrder(
                getStartOrderOffset()
                        + EDIOrderUtils
                                .getOffset(EDIOrderUtils.RouteOrderOffsetInboundEDI810.INBOUND_810_STANDARD_INVOICE))
                .multicast().parallelProcessing().to(invoiceFormattedSource, invoiceLogSource);

        from(invoiceFormattedSource)
                .startupOrder(
                        getStartOrderOffset()
                                + EDIOrderUtils
                                        .getOffset(EDIOrderUtils.RouteOrderOffsetInboundEDI810.INBOUND_810_INVOICE_FORMATTED_SOURCE))
                .errorHandler(
                        deadLetterChannel(logOutputDLQ).maximumRedeliveries(2).redeliveryDelay(2000)
                                .useExponentialBackOff().backOffMultiplier(2)).process(new Edi810ToInvoiceTranslator())
                .bean(WorkflowServiceCaller.class, "processInvoice");

        from(invoiceLogSource).startupOrder(
                getStartOrderOffset()
                        + EDIOrderUtils.getOffset(EDIOrderUtils.RouteOrderOffsetInboundEDI810.INBOUND_810_LOG))
                .errorHandler(
                        deadLetterChannel(outputDLQ).maximumRedeliveries(2).redeliveryDelay(2000)
                                .useExponentialBackOff().backOffMultiplier(2)).process(
                        new Convert810toOrderLogTranslator()).bean(MessageServiceBean.class, "receiveMessage");

        from(creditInvoice).startupOrder(
                getStartOrderOffset()
                        + EDIOrderUtils
                                .getOffset(EDIOrderUtils.RouteOrderOffsetInboundEDI810.INBOUND_810_CREDIT_INVOICE))
        //Some credit invoices are handled the same as debit invoices but negative, others use the separate credit invoice translator.
                .choice().when(differentCreditPredeicate).to(differntCredit).otherwise().to(standardInvoice);

        from(differntCredit).startupOrder(
                getStartOrderOffset()
                        + EDIOrderUtils
                                .getOffset(EDIOrderUtils.RouteOrderOffsetInboundEDI810.INBOUND_810_DIFFERNET_CREDIT))
                .multicast().parallelProcessing().to(creditFormattedSource, creditLogSource);

        from(creditFormattedSource)
                .startupOrder(
                        getStartOrderOffset()
                                + EDIOrderUtils
                                        .getOffset(EDIOrderUtils.RouteOrderOffsetInboundEDI810.INBOUND_810_CREDIT_FORMATTED_SOURCE))
                .errorHandler(
                        deadLetterChannel(logOutputDLQ).maximumRedeliveries(2).redeliveryDelay(2000)
                                .useExponentialBackOff().backOffMultiplier(2)).process(
                        new EDI810ToCreditInvoiceTranslator())
                .bean(WorkflowServiceCaller.class, "processCreditInvoice");

        from(creditLogSource).startupOrder(
                getStartOrderOffset()
                        + EDIOrderUtils.getOffset(EDIOrderUtils.RouteOrderOffsetInboundEDI810.INBOUND_810_CREDIT_LOG))
                .errorHandler(
                        deadLetterChannel(outputDLQ).maximumRedeliveries(2).redeliveryDelay(2000)
                                .useExponentialBackOff().backOffMultiplier(2)).process(
                        new EDI810CreditToOrderLogTranslator()).bean(MessageServiceBean.class, "receiveMessage");
    }

    /**
     * @return the source
     */
    public String getSource() {
        return source;
    }

    /**
     * @param source the source to set
     */
    public void setSource(String source) {
        this.source = source;
    }

    /**
     * @return the output
     */
    public String getOutput() {
        return output;
    }

    /**
     * @param output the output to set
     */
    public void setOutput(String output) {
        this.output = output;
    }

    /**
     * @return the invoiceFormattedSource
     */
    public String getUsscoInvoiceEDISource() {
        return invoiceFormattedSource;
    }

    /**
     * @param invoiceFormattedSource the invoiceFormattedSource to set
     */
    public void setUsscoInvoiceEDISource(String usscoInvoiceEDISource) {
        this.invoiceFormattedSource = usscoInvoiceEDISource;
    }

    /**
     * @return the logOutputDLQ
     */
    public String getLogOutputDLQ() {
        return logOutputDLQ;
    }

    /**
     * @param logOutputDLQ the logOutputDLQ to set
     */
    public void setLogOutputDLQ(String logOutputDLQ) {
        this.logOutputDLQ = logOutputDLQ;
    }

    /**
     * @return the invoiceLogSource
     */
    public String getInvoiceLogSource() {
        return invoiceLogSource;
    }

    /**
     * @param invoiceLogSource the invoiceLogSource to set
     */
    public void setInvoiceLogSource(String invoiceLogSource) {
        this.invoiceLogSource = invoiceLogSource;
    }

    /**
     * @return the logOutput
     */
    public String getInvoiceLogOutput() {
        return logOutput;
    }

    /**
     * @param logOutput the logOutput to set
     */
    public void setInvoiceLogOutput(String invoiceLogOutput) {
        this.logOutput = invoiceLogOutput;
    }

    public String getLogOutputURI() {
        return logOutputURI;
    }

    public void setLogOutputURI(String logOutputURI) {
        this.logOutputURI = logOutputURI;
    }

    /**
     * @return the outputDLQ
     */
    public String getOutputDLQ() {
        return outputDLQ;
    }

    /**
     * @param outputDLQ the outputDLQ to set
     */
    public void setOutputDLQ(String outputDLQ) {
        this.outputDLQ = outputDLQ;
    }

    public String getSenderId() {
        return senderId;
    }

    public void setSenderId(String senderId) {
        this.senderId = senderId;
    }

    /**
     * @return the creditOutput
     */
    public String getCreditOutput() {
        return creditOutput;
    }

    /**
     * @param creditOutput the creditOutput to set
     */
    public void setCreditOutput(String creditOutput) {
        this.creditOutput = creditOutput;
    }

    /**
     * @return the useDifferetnCreditPath
     */
    public Boolean getUseDifferetnCreditPath() {
        return useDifferetnCreditPath;
    }

    /**
     * @param useDifferetnCreditPath the useDifferetnCreditPath to set
     */
    public void setUseDifferetnCreditPath(Boolean useDifferetnCreditPath) {
        this.useDifferetnCreditPath = useDifferetnCreditPath;
    }
}
