package com.apd.phoenix.service.integration.beans;

import java.util.Properties;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.camel.component.ActiveMQComponent;
import org.apache.activemq.pool.PooledConnectionFactory;
import org.apache.camel.CamelContext;
import org.apache.camel.component.cdi.CdiCamelContext;
import org.apache.camel.component.jms.JmsConfiguration;
import org.apache.camel.spi.PackageScanClassResolver;
import org.apacheextras.camel.jboss.JBossPackageScanClassResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.core.utility.TenantConfigRepository;
import com.apd.phoenix.service.integration.routes.MarquetteCSVReportsRouteBuilder;
import com.apd.phoenix.service.integration.routes.PickConfirmationRouteBuilder;
import com.apd.phoenix.service.integration.routes.SolomonOrderRouteBuilder;
import com.apd.phoenix.service.integration.utility.RouterBeanUtils;
import com.apd.phoenix.service.message.impl.CSVMessageSender;

@Startup
@Singleton
public class OutboundCSVBean {

    private static final String PARTNER_ID_NOT_AMERICAN_PRODUCT_DISTRIBUTORS = "partnerId<>'American Product Distributors'";

    private static final Logger LOG = LoggerFactory.getLogger(OutboundCSVBean.class);

    @Inject
    private CdiCamelContext camelContext;

    @PostConstruct
    public void init() throws Exception {
        setup();
    }

    @PreDestroy
    public void destroy() throws Exception {
        camelContext.stop();
    }

    public void setup() throws Exception {

        PackageScanClassResolver jbossResolver = new JBossPackageScanClassResolver();

        camelContext.setPackageScanClassResolver(jbossResolver);

        Properties solomonProperties = TenantConfigRepository.getInstance().getProperties("solomon.integration");

        setupAmq(camelContext, solomonProperties.getProperty("jmsBrokerUrl"));

        Integer startUpOrderOffset = 0;

        //Orders
        SolomonOrderRouteBuilder solomonOrderRoute = new SolomonOrderRouteBuilder();
        solomonOrderRoute.setSource(solomonProperties.getProperty("solomonOrdersSource"));
        solomonOrderRoute.setEndpoint(solomonProperties.getProperty("solomonOrdersEndpoint"));
        solomonOrderRoute.setEndpointDlq(solomonProperties.getProperty("solomonOrdersEndpointDlq"));
        solomonOrderRoute.setLogging(true);
        camelContext.addRoutes(solomonOrderRoute);

        camelContext.addRoutes(RouterBeanUtils.createCatchallRouteBuilder(PARTNER_ID_NOT_AMERICAN_PRODUCT_DISTRIBUTORS,
                CSVMessageSender.CSV_OUTBOUND_PO));

        //Ship Confirmation
        PickConfirmationRouteBuilder pickConfirmRoute = new PickConfirmationRouteBuilder();
        pickConfirmRoute.setSource(solomonProperties.getProperty("solomonShipConfirmSource"));
        pickConfirmRoute.setEndpoint(solomonProperties.getProperty("solomonShipConfirmationEndpoint"));
        pickConfirmRoute.setEndpointDlq(solomonProperties.getProperty("solomonShipConfirmationEndpointDlq"));
        camelContext.addRoutes(pickConfirmRoute);

        Properties marquetteProperties = TenantConfigRepository.getInstance().getProperties("marquette.integration");
        MarquetteCSVReportsRouteBuilder marquetteCSVReportsRouteBuilder = new MarquetteCSVReportsRouteBuilder();
        marquetteCSVReportsRouteBuilder.setInvoiceSource(marquetteProperties.getProperty("marquetteInvoiceSource"));
        marquetteCSVReportsRouteBuilder.setDetailsFileEndpoint(marquetteProperties.getProperty("detailsFileEndpoint"));
        marquetteCSVReportsRouteBuilder.setTransmittalFileEndpoint(marquetteProperties
                .getProperty("transmittalFileEndpoint"));
        marquetteCSVReportsRouteBuilder.setMarquetteDestination(marquetteProperties.getProperty("internalDestination"));
        marquetteCSVReportsRouteBuilder.setOutput(marquetteProperties.getProperty("marquetteFTPDestination"));
        marquetteCSVReportsRouteBuilder.setStartupOrderOffset(startUpOrderOffset);
        marquetteCSVReportsRouteBuilder.setDetailsFileEndpointDLQ(marquetteProperties
                .getProperty("detailsFileEndpointDLQ"));
        marquetteCSVReportsRouteBuilder.setMarquetteDestinationDLQ(marquetteProperties
                .getProperty("marquetteFTPDestinationDLQ"));
        marquetteCSVReportsRouteBuilder.setTransmittalFileEndpointDLQ(marquetteProperties
                .getProperty("transmittalFileEndpointDLQ"));
        marquetteCSVReportsRouteBuilder.setInvoicePDFEndpoint(marquetteProperties.getProperty("invoicePDFEndpoint"));
        marquetteCSVReportsRouteBuilder.setInvoicePDFEndpointDLQ(marquetteProperties
                .getProperty("invoicePDFEndpointDLQ"));
        marquetteCSVReportsRouteBuilder.setPdfRetrievalEndpoint(marquetteProperties
                .getProperty("marquettePdfRetrievalEndpoint"));
        marquetteCSVReportsRouteBuilder.setBackupPartnerEndpoint(marquetteProperties
                .getProperty("backupPartnerEndpoint"));
        marquetteCSVReportsRouteBuilder.setBackupPartnerSource(marquetteProperties.getProperty("backupPartnerSource"));
        camelContext.addRoutes(marquetteCSVReportsRouteBuilder);

        camelContext.start();

    }

    public void setupAmq(CamelContext context, String jmsBrokerUrl) {
        Properties amqProperties = TenantConfigRepository.getInstance().getProperties("amq");
        ActiveMQConnectionFactory activeMQConnectionFactory = new ActiveMQConnectionFactory();
        activeMQConnectionFactory.setBrokerURL(jmsBrokerUrl);
        PooledConnectionFactory pooledConnectionFactory = new PooledConnectionFactory();
        pooledConnectionFactory.setConnectionFactory(activeMQConnectionFactory);
        pooledConnectionFactory.setMaxConnections(Integer.parseInt(amqProperties.getProperty(getClass().getSimpleName()
                + ".maxConnections", "2")));
        JmsConfiguration jmsConfiguration = new JmsConfiguration();
        jmsConfiguration.setConnectionFactory(pooledConnectionFactory);
        jmsConfiguration.setConcurrentConsumers(Integer.parseInt(amqProperties.getProperty(getClass().getSimpleName()
                + ".concurrentConsumers", "2")));
        ActiveMQComponent activeMQComponent = new ActiveMQComponent();
        activeMQComponent.setConfiguration(jmsConfiguration);
        context.addComponent("activemq", activeMQComponent);
    }

}
