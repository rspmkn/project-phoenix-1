package com.apd.phoenix.service.integration.routes;

import javax.edi.model.x12.edi997.FunctionalAcknowledgement;
import org.apache.camel.LoggingLevel;
import org.apache.camel.spi.DataFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.dataformat.edi.EDIDataFormat;
import com.apd.phoenix.service.integration.camel.processor.EDI997toFunctionalAcknowledgementTranslator;
import com.apd.phoenix.service.integration.camel.processor.EDI997toOrderLogTranslator;
import com.apd.phoenix.service.integration.camel.processor.EDITransactionLogger;
import com.apd.phoenix.service.integration.camel.processor.Edi997NoGroupsProcessor;
import com.apd.phoenix.service.integration.camel.processor.WorkflowServiceCaller;
import com.apd.phoenix.service.integration.utility.EDIOrderUtils;
import com.apd.phoenix.service.model.MessageMetadata.CommunicationType;

public class EDI997RouteBuilder extends EDIRouteBuilder {

    private static final Logger LOG = LoggerFactory.getLogger(EDI997RouteBuilder.class);

    private String source;

    private String formattedSource;

    private String logSource;

    private String outputDLQ;

    private String senderId;

    private boolean isLogging = true;

    @Override
    public void configureRouteImplementation() throws Exception {
        DataFormat format = new EDIDataFormat(FunctionalAcknowledgement.class);

        onException(Exception.class).maximumRedeliveries(0).handled(true).log(LoggingLevel.INFO,
                "Error in edi 997 processing: ${exception.stacktrace}");

        //Inbound 997
        from(source).startupOrder(
                getStartOrderOffset()
                        + EDIOrderUtils.getOffset(EDIOrderUtils.RouteOrderOffsetInboundEDI997.INBOUND_997)).unmarshal(
                format)
        //Parse transaction information
                .setHeader("senderId", constant(senderId)).setHeader("interchangeId").jxpath(
                        "/in/body/envelopeHeader/interchangeControlNumber").setHeader("groupId").jxpath(
                        "/in/body/groupEnvelopeHeader/groupControlNumber")
                //Iterate over each transaction
                .split().jxpath("/in/body/body").setHeader("transactionId").jxpath(
                        "/in/body/header/transactionSetHeader/transactionSetControlNumber").bean(
                        Edi997NoGroupsProcessor.class, "responseGroupsFound")
                //Only split for partners that include transaction set response groups
                .choice().when(simple("header.responseGroupNotFound")).log(LoggingLevel.DEBUG,
                        "997 without response group recieved")
                //Logging
                .wireTap(logSource).to(formattedSource).endChoice().otherwise().split().jxpath(
                        "/in/body/detail/transactionResponseGroup")
                //Logging
                .wireTap(logSource).to(formattedSource).end();

        //Inbound Log Route
        from(logSource).startupOrder(
                getStartOrderOffset()
                        + EDIOrderUtils.getOffset(EDIOrderUtils.RouteOrderOffsetInboundEDI997.INBOUND_997_LOG))
                .process(new EDI997toOrderLogTranslator())
                //Set communication type
                .setHeader("communicationType", constant(CommunicationType.INBOUND))
                //Set body to raw message
                .setBody(header("rawMessage"))
                //Log
                .choice().when(constant(isLogging)).bean(EDITransactionLogger.class, "logFATransaction");

        //Inbound Processing Route
        from(formattedSource).startupOrder(
                getStartOrderOffset()
                        + EDIOrderUtils.getOffset(EDIOrderUtils.RouteOrderOffsetInboundEDI997.INBOUND_997_FORMATTED))
                .errorHandler(
                        deadLetterChannel(outputDLQ).maximumRedeliveries(2).redeliveryDelay(2000)
                                .useExponentialBackOff().backOffMultiplier(2)).process(
                        new EDI997toFunctionalAcknowledgementTranslator()).bean(WorkflowServiceCaller.class,
                        "processFunctionalAcknowledgement");

    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getOutputDLQ() {
        return outputDLQ;
    }

    public void setOutputDLQ(String outputDLQ) {
        this.outputDLQ = outputDLQ;
    }

    public String getSenderId() {
        return senderId;
    }

    public void setSenderId(String senderId) {
        this.senderId = senderId;
    }

    public String getFormattedSource() {
        return formattedSource;
    }

    public void setFormattedSource(String formattedSource) {
        this.formattedSource = formattedSource;
    }

    public String getLogSource() {
        return logSource;
    }

    public void setLogSource(String logSource) {
        this.logSource = logSource;
    }

    public boolean isLogging() {
        return isLogging;
    }

    public void setLogging(boolean isLogging) {
        this.isLogging = isLogging;
    }

}
