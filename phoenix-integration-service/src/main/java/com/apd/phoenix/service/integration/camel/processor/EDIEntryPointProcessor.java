package com.apd.phoenix.service.integration.camel.processor;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class EDIEntryPointProcessor implements Processor {

    private static final Logger LOG = LoggerFactory.getLogger(EDIEntryPointProcessor.class);

    @Override
    public void process(Exchange exchange) throws Exception {
        byte[] contentArray = (byte[]) exchange.getIn().getBody();

        String content = new String(contentArray);
        Pattern pattern = Pattern.compile("~[\\s\\n]*GS\\*(\\w\\w)\\*");
        Matcher matcher = pattern.matcher(content);
        matcher.find();
        String functionalId = content.substring(matcher.start(1), matcher.end(1));
        LOG.info("Functional Id: {}", functionalId);

        exchange.getOut().setBody(content);
        exchange.getOut().setHeaders(exchange.getIn().getHeaders());
        exchange.getOut().setHeader("functionId", functionalId);
    }

}
