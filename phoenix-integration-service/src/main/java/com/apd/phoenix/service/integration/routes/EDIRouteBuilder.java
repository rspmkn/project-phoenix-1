/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.service.integration.routes;

import com.apd.phoenix.service.integration.multitenancy.AbstractTenantAwareRouteBuilder;

/**
 *
 * @author nreidelb
 */
public abstract class EDIRouteBuilder extends AbstractTenantAwareRouteBuilder {

    private int startOrderOffset;

    //Outbound endpoint used to specify location for a copy of outbound edi documents
    private String backupPartnerEndpoint;

    /**
     * @return the startOrderOffset
     */
    public int getStartOrderOffset() {
        return startOrderOffset;
    }

    /**
     * @param startOrderOffset the startOrderOffset to set
     */
    public void setStartOrderOffset(int startOrderOffset) {
        this.startOrderOffset = startOrderOffset;
    }

    /**
     * @return the backupEndpoint
     */
    public String getBackupPartnerEndpoint() {
        return backupPartnerEndpoint;
    }

    /**
     * @param backupEndpoint the backupEndpoint to set
     */
    public void setBackupPartnerEndpoint(String backupPartnerEndpoint) {
        this.backupPartnerEndpoint = backupPartnerEndpoint;
    }
}
