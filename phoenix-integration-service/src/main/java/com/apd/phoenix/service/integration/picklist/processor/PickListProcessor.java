package com.apd.phoenix.service.integration.picklist.processor;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;
import java.util.Set;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.mail.internet.MimeMessage;
import javax.persistence.PersistenceException;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.Status;
import javax.transaction.SystemException;
import javax.transaction.TransactionManager;

import org.apache.camel.Body;
import org.apache.camel.Header;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.apd.phoenix.core.utility.TenantConfigRepository;
import com.apd.phoenix.service.email.api.Attachment;
import com.apd.phoenix.service.email.api.Attachment.MimeType;
import com.apd.phoenix.service.email.api.EmailService;
import com.apd.phoenix.service.integration.picklist.model.PickCsv;
import com.apd.phoenix.service.model.LineItem;
import com.apd.phoenix.service.model.Pick;
import com.apd.phoenix.service.persistence.jpa.LineItemDao;
import com.apd.phoenix.service.persistence.jpa.PickDao;

@Stateless
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
public class PickListProcessor {

    private static final Logger LOG = LoggerFactory.getLogger(PickListProcessor.class);

    @Resource(lookup = "java:/TransactionManager")
    private TransactionManager tm;

    @Inject
    PickDao pickDao;

    @Inject
    LineItemDao lineItemDao;
    
    @Inject
    EmailService emailService;

    public void process(@Header("rawMessage") String rawMessage, @Body List<PickCsv> picklist) {
    	List<Response> failed = new ArrayList<>();
    	List<Response> success = new ArrayList<>();
        for (PickCsv pickCsv : picklist) {
            try {
            	Response response = processPick(pickCsv);
    			if(response.isValid()) {
    				success.add(response);
    			}
    			else {
    				failed.add(response);
    			}
			} catch (SecurityException | IllegalStateException
					| NotSupportedException | SystemException
					| RollbackException | HeuristicMixedException
					| HeuristicRollbackException e) {
				LOG.error("Error saving picks");
			}
        }
        
        sendResultMessage(success, failed, rawMessage);
        
    }

    public void process(@Header("rawMessage") String rawMessage, @Body PickCsv pickCsv) {
    	List<Response> failed = new ArrayList<>();
    	List<Response> success = new ArrayList<>();
        try {
        	Response response = processPick(pickCsv);
			if(response.isValid()) {
				success.add(response);
			}
			else {
				failed.add(response);
			}
		} catch (SecurityException | IllegalStateException
				| NotSupportedException | SystemException | RollbackException
				| HeuristicMixedException | HeuristicRollbackException e) {
			LOG.error("Error saving picks");
		}
        
        sendResultMessage(success, failed, rawMessage);
        
    }

    public Response processPick(PickCsv pickCsv) throws NotSupportedException, SystemException, SecurityException,
            IllegalStateException, RollbackException, HeuristicMixedException, HeuristicRollbackException {

    	if(tm.getStatus() == Status.STATUS_NO_TRANSACTION) {
    		LOG.info("Transaction did not exist so creating one");
    		tm.begin();
    	}
        
        Pick pick = translateToPick(pickCsv);
        Response response = validate(pick);
        response.setPickCsv(pickCsv);
        if (response.isValid()) {
            try {
                pick = pickDao.create(pick);
            }
            catch (PersistenceException e) {
                LOG.error("Could not persist pick.");

                if(tm.getStatus() == Status.STATUS_ACTIVE) {
                	tm.rollback();
            	} else {
            		LOG.error("Transaction not found");
            	}
                
                response.setValid(false);
                response.getMessages().add("Could not persist pick.");
                return response;
            }

            if(tm.getStatus() == Status.STATUS_ACTIVE) {
            	tm.commit();
            } else {
        		LOG.error("Transaction not found");
        	}
            return response;
        }

        if(tm.getStatus() == Status.STATUS_ACTIVE) {
        	tm.rollback();
        } else {
    		LOG.error("Transaction not found");
    	}
        return response;
    }

    private Pick translateToPick(PickCsv pickCsv) {
        LineItem lineItem = lineItemDao.getLineItemByVendorSkuAndApdPo(pickCsv.getInventoryId(), pickCsv
                .getApdOrderNumber());
        if (lineItem == null) {
            return null;
        }
        Pick pick = new Pick();
        pick.setLineItem(lineItem);
        pick.setApdOrderNumber(pickCsv.getApdOrderNumber());
        pick.setCustomerPoNumber(pickCsv.getCustomerPoNumber());
        pick.setInventoryId(pickCsv.getInventoryId());
        pick.setLineReference(pickCsv.getLineReference());
        pick.setOrderNumber(pickCsv.getOrderNumber());
        if (pickCsv.getQtyBo() != null) {
            BigDecimal gtyBo = new BigDecimal(pickCsv.getQtyBo());
            pick.setQtyBo(gtyBo.toBigInteger());
        }
        if (pickCsv.getQtyToPick() != null) {
            BigDecimal qtyPick = new BigDecimal(pickCsv.getQtyToPick());
            pick.setQtyToPick(qtyPick.toBigInteger());
        }
        pick.setShipperId(pickCsv.getShipperId());
        return pick;
    }

    private Response validate(Pick pick) {
    	Response response = new Response();
    	response.setValid(true);
    	response.setPick(pick);
        if (pick == null) {
            LOG.error("No lineitem found for pick");
            response.getMessages().add("No lineitem found for pick");
            response.setValid(false);
            return response;
        }
        if (pick.getLineItem().getStatus() == null || pick.getLineItem().getStatus().getValue() == null) {
            LOG.error("No line item status found");
            response.getMessages().add("No line item status found");
            response.setValid(false);
        }
        else if (pick.getLineItem().getQuantityRemaining() == 0) {
            LOG.error("Lineitem already fully shipped");
            response.getMessages().add("Lineitem already fully shipped");
            response.setValid(false);
        }
        if(BigInteger.valueOf(pick.getLineItem().getQuantity()).compareTo(pick.getQtyToPick()) == -1) {
        	LOG.error("Qty to pick is greater than ordered quanitity");
        	response.getMessages().add("Qty to pick is greater than ordered quanitity");
            response.setValid(false);
        }
        return response;
    }

    private void sendResultMessage(List<Response> success, List<Response> failed, String rawMessage) {
        try(InputStream inputStream = new ByteArrayInputStream(rawMessage.getBytes())){
        LOG.info("Sending email for pick report");
        Properties pickProperties = TenantConfigRepository.getInstance().getProperties("solomon.integration");
        String from = pickProperties.getProperty("solomonPickListEmailFrom","admin@apdmarketplace.com");
        Set<String> recipients = new HashSet<>();
        recipients.add(pickProperties.getProperty("solomonPickListEmailTo"));
        Set<String> ccRecipients = new HashSet<>();
        String cc = pickProperties.getProperty("solomonPickListEmailCC");
        if(StringUtils.isNotEmpty(cc)) {
        	ccRecipients.add(cc);
        }
        
        String subject = "[Solomon]Pick List Report";
        
        List<Attachment> attachments = new ArrayList<>();
        Attachment attachment = new Attachment();
        attachment.setFileName("picklist.txt");
        attachment.setMimeType(MimeType.txt);
        attachment.setContent(inputStream);
        attachments.add(attachment);
        String emailBody = "<h1>Pick List Processing Report</h1>"
        		+ "<p>Attempted processing attached file on "+new Date()+"</p>";
        emailBody += "<h3>Failed:</h3><p>";
        for(Response response: failed) {
        	emailBody += "Warning: Ignoring '"+response.getPickCsv().getShipperId()+"'<br />";
        	emailBody += "Reasons:<br />";
        	for(String message : response.messages) {
        		emailBody += "&nbsp;" + message + "<br />";
        	}
        }
        emailBody += "</p>";
        emailBody += "<h3>Successful:</h3><p>";
        for(Response response: success) {
        	emailBody += "Imported ship number '"+response.getPickCsv().getShipperId()+"' for order '"+response.getPickCsv().getApdOrderNumber()+"' for '"+response.getPickCsv().getQtyToPick()+"' of item '"+response.getPickCsv().getInventoryId()+"'<br />";
        }
        emailBody += "</p>";
        MimeMessage message = emailService.prepareRawMessage(from, null, null, recipients, ccRecipients, subject, emailBody, attachments);
		
		emailService.sendEmail(message);
        } catch (IOException ex) {
            LOG.error("Could not rawMessage to inputStream", ex);
        }
    }
    
    public class Response {
    	private Pick pick;
    	private PickCsv pickCsv;
    	private boolean isValid;
    	private List<String> messages = new ArrayList<>();
		public Pick getPick() {
			return pick;
		}
		public void setPick(Pick pick) {
			this.pick = pick;
		}
		public List<String> getMessages() {
			return messages;
		}
		public void setMessages(List<String> messages) {
			this.messages = messages;
		}
		public boolean isValid() {
			return isValid;
		}
		public void setValid(boolean isValid) {
			this.isValid = isValid;
		}
		public PickCsv getPickCsv() {
			return pickCsv;
		}
		public void setPickCsv(PickCsv pickCsv) {
			this.pickCsv = pickCsv;
		}
    	
    	
    }
}
