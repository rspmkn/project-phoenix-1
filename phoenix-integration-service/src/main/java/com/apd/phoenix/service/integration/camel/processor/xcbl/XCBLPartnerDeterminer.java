/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.service.integration.camel.processor.xcbl;

import com.apd.phoenix.service.integration.utility.PropertiesLoader;
import java.util.Properties;
import org.apache.camel.Exchange;
import org.apache.cxf.common.util.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author nreidelb
 */
public class XCBLPartnerDeterminer {

    private static final String PARTNER_ID = "partnerId";

    private static final Logger LOG = LoggerFactory.getLogger(XCBLPartnerDeterminer.class);

    private static Properties commonProperties = PropertiesLoader
            .getAsProperties("xcbl.partner.unique.identifier.properties");

    public void determinePartner(Exchange exchange) {
        exchange.getOut().setBody(exchange.getIn().getBody());
        exchange.getOut().setHeaders(exchange.getIn().getHeaders());

        String camelFileName = (String) exchange.getIn().getHeader("CamelFileName");
        for (String partnerId : commonProperties.stringPropertyNames()) {
            String uniqueFilenameIdentifier = commonProperties.getProperty(partnerId);
            if (camelFileName.contains(uniqueFilenameIdentifier)) {
                exchange.getOut().getHeaders().put(PARTNER_ID, partnerId);
            }
        }
        if (StringUtils.isEmpty((String) exchange.getOut().getHeaders().get(PARTNER_ID))) {
            LOG.error("Could not determine partner for inbound xcbl file with name: " + camelFileName);
        }
    }
}
