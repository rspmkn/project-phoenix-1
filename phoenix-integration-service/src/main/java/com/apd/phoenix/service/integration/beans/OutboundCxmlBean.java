package com.apd.phoenix.service.integration.beans;

import java.util.Properties;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.camel.component.ActiveMQComponent;
import org.apache.activemq.pool.PooledConnectionFactory;
import org.apache.camel.CamelContext;
import org.apache.camel.component.cdi.CdiCamelContext;
import org.apache.camel.component.jms.JmsConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.integration.routes.cxml.CxmlOutboundConfirmationRequestRouteBuilder;
import com.apd.phoenix.service.integration.routes.cxml.CxmlOutboundInvoiceRequestRouteBuilder;
import com.apd.phoenix.service.integration.routes.cxml.CxmlOutboundOrderRequestRouteBuilder;
import com.apd.phoenix.service.integration.routes.cxml.CxmlOutboundShipNoticeRequestRouteBuilder;
import com.apd.phoenix.service.integration.utility.PropertiesLoader;

@Startup
@Singleton
public class OutboundCxmlBean {

    private static final Logger LOG = LoggerFactory.getLogger(OutboundCxmlBean.class);

    @Inject
    private CdiCamelContext camelContext;

    @PostConstruct
    public void init() throws Exception {
        setup();
    }

    @PreDestroy
    public void destroy() throws Exception {
        camelContext.stop();
    }

    public void setup() throws Exception {

        Properties commonProperties = PropertiesLoader.getAsProperties("camel.cxml.integration");

        setupAmq(camelContext, commonProperties.getProperty("jmsBrokerUrl"));

        //Ship Notice

        CxmlOutboundShipNoticeRequestRouteBuilder cxmlOutboundShipNoticeRequestRouteBuilder = new CxmlOutboundShipNoticeRequestRouteBuilder();
        cxmlOutboundShipNoticeRequestRouteBuilder.setSource(commonProperties
                .getProperty("cXMLOutboundShipNoticeSource"));
        cxmlOutboundShipNoticeRequestRouteBuilder.setOutputDLQ(commonProperties
                .getProperty("cXMLOutboundShipNoticeDLQ"));
        cxmlOutboundShipNoticeRequestRouteBuilder.setDeploymentMode(commonProperties.getProperty("deploymentMode"));
        camelContext.addRoutes(cxmlOutboundShipNoticeRequestRouteBuilder);

        //Confirmation Request
        CxmlOutboundConfirmationRequestRouteBuilder cxmlOutboundConfirmationRequestRouteBuilder = new CxmlOutboundConfirmationRequestRouteBuilder();
        cxmlOutboundConfirmationRequestRouteBuilder.setSource(commonProperties
                .getProperty("cxmlOutboundConfirmationRequestSource"));
        cxmlOutboundConfirmationRequestRouteBuilder.setOutputDLQ(commonProperties
                .getProperty("cxmlOutboundConfirmationRequestDLQ"));
        cxmlOutboundConfirmationRequestRouteBuilder.setOutput(commonProperties
                .getProperty("cxmlOutboundConfirmationRequestRouteBuilder"));
        cxmlOutboundConfirmationRequestRouteBuilder.setDeploymentMode(commonProperties.getProperty("deploymentMode"));
        camelContext.addRoutes(cxmlOutboundConfirmationRequestRouteBuilder);

        //Order Request
        CxmlOutboundOrderRequestRouteBuilder cxmlOutboundOrderRequestRouteBuilder = new CxmlOutboundOrderRequestRouteBuilder();
        cxmlOutboundOrderRequestRouteBuilder.setSource(commonProperties.getProperty("cXMLOutboundOrderRequestSource"));
        cxmlOutboundOrderRequestRouteBuilder.setOutputDLQ(commonProperties.getProperty("cXMLOutboundOrderRequestDLQ"));
        cxmlOutboundOrderRequestRouteBuilder.setDeploymentMode(commonProperties.getProperty("deploymentMode"));
        cxmlOutboundOrderRequestRouteBuilder.setCxmlOrderRequestOutboundDLQ(commonProperties
                .getProperty("cxmlOrderRequestOutboundDLQ"));

        camelContext.addRoutes(cxmlOutboundOrderRequestRouteBuilder);

        //Invoice Request
        CxmlOutboundInvoiceRequestRouteBuilder cxmlOutboundInvoiceRequestRouteBuilder = new CxmlOutboundInvoiceRequestRouteBuilder();
        cxmlOutboundInvoiceRequestRouteBuilder.setSource(commonProperties
                .getProperty("cXMLOutboundInvoiceRequestSource"));
        cxmlOutboundInvoiceRequestRouteBuilder.setOutputDLQ(commonProperties
                .getProperty("cXMLOutboundInvoiceRequestDLQ"));
        cxmlOutboundInvoiceRequestRouteBuilder.setDeploymentMode(commonProperties.getProperty("deploymentMode"));

        camelContext.addRoutes(cxmlOutboundInvoiceRequestRouteBuilder);

        camelContext.start();
    }

    public void setupAmq(CamelContext context, String jmsBrokerUrl) {
        Properties amqProperties = PropertiesLoader.getAsProperties("amq");
        ActiveMQConnectionFactory activeMQConnectionFactory = new ActiveMQConnectionFactory();
        activeMQConnectionFactory.setBrokerURL(jmsBrokerUrl);
        PooledConnectionFactory pooledConnectionFactory = new PooledConnectionFactory();
        pooledConnectionFactory.setConnectionFactory(activeMQConnectionFactory);
        pooledConnectionFactory.setMaxConnections(Integer.parseInt(amqProperties.getProperty(getClass().getSimpleName()
                + ".maxConnections", "2")));
        JmsConfiguration jmsConfiguration = new JmsConfiguration();
        jmsConfiguration.setConnectionFactory(pooledConnectionFactory);
        jmsConfiguration.setConcurrentConsumers(Integer.parseInt(amqProperties.getProperty(getClass().getSimpleName()
                + ".concurrentConsumers", "2")));
        ActiveMQComponent activeMQComponent = new ActiveMQComponent();
        activeMQComponent.setConfiguration(jmsConfiguration);
        context.addComponent("activemq", activeMQComponent);
    }
}
