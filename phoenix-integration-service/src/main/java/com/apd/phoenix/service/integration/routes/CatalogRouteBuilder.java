package com.apd.phoenix.service.integration.routes;

import org.apache.camel.Exchange;
import org.apache.camel.LoggingLevel;
import org.apache.camel.dataformat.bindy.csv.BindyCsvDataFormat;
import org.apache.camel.model.dataformat.BindyType;
import org.apache.camel.model.dataformat.JsonLibrary;
import org.apache.camel.spi.DataFormat;
import com.apd.phoenix.service.integration.camel.aggregation.strategy.StringBodyAggregatingStrategy;
import com.apd.phoenix.service.integration.catalog.model.Item;
import com.apd.phoenix.service.integration.catalog.model.ReportItem;
import com.apd.phoenix.service.integration.multitenancy.AbstractTenantAwareRouteBuilder;

public class CatalogRouteBuilder extends AbstractTenantAwareRouteBuilder {

    private String source;

    private String output;

    private String processingEndpoint;

    DataFormat bindy = new BindyCsvDataFormat("com.apd.phoenix.service.integration.catalog.model");

    @Override
    public void configureRouteImplementation() throws Exception {
        //Read csv catalog w/o headers
        from(source).split(body().tokenize("\n")).parallelProcessing().streaming()
                .log(LoggingLevel.INFO, "Reading and processing an item")
                //process one line and send to http endpoint for processing
                .unmarshal().bindy(BindyType.Csv, Item.class).marshal()
                .json(JsonLibrary.Jackson)
                .to(processingEndpoint)
                //convert the response from endpoint back to csv for report
                //Note:The response from the endpoint should be a ReportItem
                .unmarshal().json(JsonLibrary.Jackson, Item.class).marshal().bindy(BindyType.Csv, ReportItem.class)
                //aggregate results
                .aggregate(header(Exchange.FILE_NAME), new StringBodyAggregatingStrategy()).completionSize(50)
                .completionInterval(10000).log(LoggingLevel.INFO, "Created aggregated set")
                //output csv report appending each aggreagation
                .setHeader(Exchange.FILE_NAME, simple("${header.CamelFileName}.output.report.csv")).to(output);
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getOutput() {
        return output;
    }

    public void setOutput(String output) {
        this.output = output;
    }

    public String getProcessingEndpoint() {
        return processingEndpoint;
    }

    public void setProcessingEndpoint(String processingEndpoint) {
        this.processingEndpoint = processingEndpoint;
    }

}
