package com.apd.phoenix.service.integration.routes;

import org.apache.camel.LoggingLevel;
import com.apd.phoenix.service.integration.camel.processor.EDIEntryPointProcessor;
import com.apd.phoenix.service.integration.camel.processor.GpgCamelCallBean;
import com.apd.phoenix.service.integration.utility.EDIOrderUtils;

public class EDIEntryPointRouteBuilder extends EDIRouteBuilder {

    private String transactionSource;

    //EDI 997
    private String transactionFASink;

    //EDI 855
    private String transactionPRSink;

    //EDI 856
    private String transactionSHSink;

    //EDI850
    private String transactionPOSink;

    //EDI824
    private String transactionAGSink;

    //EDI 810
    private String transactionINSink;

    //EDI 816
    private String transactionORSink;

    //EDI 864
    private String transactionTXSink;

    private String transactionUnknown;

    private String partnerId;

    private boolean isEncrypted = false;
    private boolean isArmored = true;
    private String gpgTempDirectory;
    private String gpgExecutableDirectory;

    private int startOrderOffset;

    @Override
    public void configureRouteImplementation() throws Exception {

        onException(Exception.class).maximumRedeliveries(0).handled(true).log(LoggingLevel.INFO,
                "Error in edi entry point processing: ${exception.stacktrace}");

        from(transactionSource)
                .startupOrder(
                        getStartOrderOffset()
                                + EDIOrderUtils.getOffset(EDIOrderUtils.RouteOrderOffsetInboundEDI.ENTRY_POINT))
                .setHeader("partnerId", simple(partnerId))
                //unencrypt
                .choice()
                //encryption
                .when(constant(isEncrypted)).setHeader("CamelFileName",
                        simple("${header.CamelFileName}${header.partnerId}")).log(LoggingLevel.DEBUG,
                        "encrypted: ${body}").to("file:" + gpgTempDirectory).setHeader(GpgCamelCallBean.PARTNER_ID)
                .constant(partnerId).setHeader(GpgCamelCallBean.TMP_DIRECTORY).constant(gpgTempDirectory)
                //decrypts the file using a gpg command line
                .bean(GpgCamelCallBean.class, "setGpgParameters").to("exec:gpg?workingDir=" + gpgExecutableDirectory)
                .log(LoggingLevel.DEBUG, "unencrypted: ${body}")
                //clears password from header and ensures in a state where parser can handle it
                .bean(GpgCamelCallBean.class, "processGpgResult")
                //End choice
                .end()
                //end encryption
                .process(new EDIEntryPointProcessor()).choice()
                //997
                .when(header("functionId").isEqualTo("FA")).to(transactionFASink)
                //855
                .when(header("functionId").isEqualTo("PR")).to(transactionPRSink)
                //856
                .when(header("functionId").isEqualTo("SH")).to(transactionSHSink)
                //850
                .when(header("functionId").isEqualTo("PO")).to(transactionPOSink)
                //824
                .when(header("functionId").isEqualTo("AG")).to(transactionAGSink)
                //810
                .when(header("functionId").isEqualTo("IN")).to(transactionINSink)
                //816
                .when(header("functionId").isEqualTo("OR")).to(transactionORSink)
                //864
                .when(header("functionId").isEqualTo("TX")).to(transactionTXSink)
                //Unknown Transaction Type
                .otherwise().to(transactionUnknown).end().choice()
                //delete temporary encryption file
                .when(constant(isEncrypted)).bean(GpgCamelCallBean.class, "setRmParameters").to(
                        "exec:rm?workingDir=" + gpgTempDirectory).end();

    }

    public String getTransactionSource() {
        return transactionSource;
    }

    public void setTransactionSource(String transactionSource) {
        this.transactionSource = transactionSource;
    }

    public String getTransactionFASink() {
        return transactionFASink;
    }

    public void setTransactionFASink(String transactionFASink) {
        this.transactionFASink = transactionFASink;
    }

    public String getTransactionPRSink() {
        return transactionPRSink;
    }

    public void setTransactionPRSink(String transactionPRSink) {
        this.transactionPRSink = transactionPRSink;
    }

    public String getTransactionSHSink() {
        return transactionSHSink;
    }

    public void setTransactionSHSink(String transactionSHSink) {
        this.transactionSHSink = transactionSHSink;
    }

    public String getTransactionINSink() {
        return transactionINSink;
    }

    public void setTransactionINSink(String transactionINSink) {
        this.transactionINSink = transactionINSink;
    }

    public String getTransactionUnknown() {
        return transactionUnknown;
    }

    public void setTransactionUnknown(String transactionUnknown) {
        this.transactionUnknown = transactionUnknown;
    }

    public String getPartnerId() {
        return partnerId;
    }

    public void setPartnerId(String partnerId) {
        this.partnerId = partnerId;
    }

    /**
     * @return the transactionPOSink
     */
    public String getTransactionPOSink() {
        return transactionPOSink;
    }

    /**
     * @param transactionPOSink the transactionPOSink to set
     */
    public void setTransactionPOSink(String transactionPOSink) {
        this.transactionPOSink = transactionPOSink;
    }

    /**
     * @return the transactionORSink
     */
    public String getTransactionORSink() {
        return transactionORSink;
    }

    /**
     * @param transactionORSink the transactionORSink to set
     */
    public void setTransactionORSink(String transactionORSink) {
        this.transactionORSink = transactionORSink;
    }

    /**
     * @return the transactionAGSink
     */
    public String getTransactionAGSink() {
        return transactionAGSink;
    }

    /**
     * @param transactionAGSink the transactionAGSink to set
     */
    public void setTransactionAGSink(String transactionAGSink) {
        this.transactionAGSink = transactionAGSink;
    }

    public boolean isEncrypted() {
        return isEncrypted;
    }

    public void setEncrypted(boolean isEncrypted) {
        this.isEncrypted = isEncrypted;
    }

    public boolean isArmored() {
        return isArmored;
    }

    public void setArmored(boolean isArmored) {
        this.isArmored = isArmored;
    }

    /**
     * @return the startOrderOffset
     */
    public int getStartOrderOffset() {
        return startOrderOffset;
    }

    /**
     * @param startOrderOffset the startOrderOffset to set
     */
    public void setStartOrderOffset(int startOrderOffset) {
        this.startOrderOffset = startOrderOffset;
    }

    /**
     * @return the gpgTempDirectory
     */
    public String getGpgTempDirectory() {
        return gpgTempDirectory;
    }

    /**
     * @param gpgTempDirectory the gpgTempDirectory to set
     */
    public void setGpgTempDirectory(String gpgTempDirectory) {
        this.gpgTempDirectory = gpgTempDirectory;
    }

    /**
     * @return the gpgExecutableDirectory
     */
    public String getGpgExecutableDirectory() {
        return gpgExecutableDirectory;
    }

    /**
     * @param gpgExecutableDirectory the gpgExecutableDirectory to set
     */
    public void setGpgExecutableDirectory(String gpgExecutableDirectory) {
        this.gpgExecutableDirectory = gpgExecutableDirectory;
    }

    public String getTransactionTXSink() {
        return transactionTXSink;
    }

    public void setTransactionTXSink(String transactionTXSink) {
        this.transactionTXSink = transactionTXSink;
    }

}
