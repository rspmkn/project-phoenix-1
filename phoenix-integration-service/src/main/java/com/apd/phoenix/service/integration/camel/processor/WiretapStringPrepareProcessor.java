package com.apd.phoenix.service.integration.camel.processor;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;

public class WiretapStringPrepareProcessor implements Processor {

    @Override
    public void process(Exchange exchange) throws Exception {
        exchange.getOut().setHeaders(exchange.getIn().getHeaders());
        byte[] contentArray = (byte[]) exchange.getIn().getBody();
        String content = new String(contentArray);
        exchange.getOut().setBody(content);

    }

}
