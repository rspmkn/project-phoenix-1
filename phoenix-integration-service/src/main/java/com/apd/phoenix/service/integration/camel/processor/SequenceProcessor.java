package com.apd.phoenix.service.integration.camel.processor;

import javax.ejb.Stateless;
import javax.inject.Inject;
import org.apache.camel.Exchange;
import com.apd.phoenix.service.persistence.jpa.SequenceDao;
import com.apd.phoenix.service.persistence.jpa.SequenceDao.Sequence;

@Stateless
public class SequenceProcessor {

    @Inject
    SequenceDao sequenceDao;

    public void generateSequences(Exchange exchange) {

        exchange.getIn().getHeaders().put("interchangeControlId", sequenceDao.nextVal(Sequence.INTERCHANGE_CONTROL_ID));
        exchange.getIn().getHeaders().put("groupControlId", sequenceDao.nextVal(Sequence.GROUP_CONTROL_ID));
        exchange.getIn().getHeaders().put("transactionControlId", sequenceDao.nextVal(Sequence.TRANSACTION_CONTROL_ID));
    }

}
