package com.apd.phoenix.service.integration.camel.processor.xcbl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import javax.inject.Inject;
import org.apache.camel.Exchange;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.message.api.Message;
import com.apd.phoenix.service.message.api.MessageUtils;
import com.apd.phoenix.service.model.MessageMetadata;
import com.apd.phoenix.service.model.MessageMetadata.MessageType;
import com.apd.phoenix.service.model.OrderLog.EventType;
import com.apd.phoenix.service.model.dto.POAcknowledgementDto;
import com.apd.phoenix.service.persistence.jpa.MessageMetadataDao;

public class OrderResponseEnricher {

    private static final Logger LOG = LoggerFactory.getLogger(OrderResponseEnricher.class);

    @Inject
    private MessageMetadataDao messageMetadataDao;

    @Inject
    private MessageUtils messageUtils;

    public void process(Exchange exchange) throws IOException {

        POAcknowledgementDto poAck = (POAcknowledgementDto) exchange.getIn().getBody();
        MessageMetadata metadata = messageMetadataDao.getLatestMessageExchangeByOrderNumberAndEvent(poAck.getApdPo(),
                EventType.PURCHASE_ORDER_SENT, MessageType.XCBL);
        Message orderMessage = messageUtils.retrieveMessage(metadata);
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(orderMessage.getContent()));
        StringBuilder resultsBuilder = new StringBuilder();
        String line = null;
        while ((line = bufferedReader.readLine()) != null) {
            resultsBuilder.append(line).append("\n");
        }
        if (LOG.isDebugEnabled()) {
            LOG.debug(resultsBuilder.toString());
        }
        exchange.getOut().setHeaders(exchange.getIn().getHeaders());
        exchange.getOut().setHeader("poAck", poAck);
        exchange.getOut().setHeader("apdPo", poAck.getApdPo());
        exchange.getOut().setBody(resultsBuilder.toString());
    }
}
