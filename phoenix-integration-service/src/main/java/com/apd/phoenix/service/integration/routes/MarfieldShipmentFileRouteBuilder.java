package com.apd.phoenix.service.integration.routes;

import org.apache.camel.ExchangePattern;
import org.apache.camel.LoggingLevel;
import com.apd.phoenix.service.camel.DelayBean;
import com.apd.phoenix.service.integration.camel.processor.MessageServiceBean;
import com.apd.phoenix.service.integration.camel.processor.ShipmentDtoToOrderLogTranslator;
import com.apd.phoenix.service.integration.camel.processor.WorkflowServiceCaller;
import com.apd.phoenix.service.integration.marfield.processor.MarfieldShipmentFileConverter;
import com.apd.phoenix.service.integration.multitenancy.AbstractTenantAwareRouteBuilder;

public class MarfieldShipmentFileRouteBuilder extends AbstractTenantAwareRouteBuilder {

    private String source;
    private int startupOrderOffset;
    private String shipmentNoticeLogSource;
    private String shipmentNoticeFormattedSource;
    private String logOutputDLQ;
    private String outputDLQ;

    @Override
    public void configureRouteImplementation() throws Exception {
        onException(Exception.class).maximumRedeliveries(0).handled(true).log(LoggingLevel.INFO,
                "Error in Marfield Shipment file processing: ${exception.stacktrace}");

        from(getSource()).autoStartup(false).routePolicy(DelayBean.getDelayPolicy()).startupOrder(
                getStartupOrderOffset()).bean(MarfieldShipmentFileConverter.class, "process").split().jxpath("in/body")
                .multicast().parallelProcessing().to(ExchangePattern.InOnly, getShipmentNoticeLogSource(),
                        getShipmentNoticeFormattedSource());

        from(getShipmentNoticeLogSource()).startupOrder(getStartupOrderOffset() + 1).errorHandler(
                deadLetterChannel(getLogOutputDLQ()).maximumRedeliveries(2).redeliveryDelay(2000)
                        .useExponentialBackOff().backOffMultiplier(2)).process(new ShipmentDtoToOrderLogTranslator())
                .bean(MessageServiceBean.class, "receiveMessage");

        from(getShipmentNoticeFormattedSource()).startupOrder(getStartupOrderOffset() + 2).errorHandler(
                deadLetterChannel(getOutputDLQ()).maximumRedeliveries(2).redeliveryDelay(2000).useExponentialBackOff()
                        .backOffMultiplier(2)).bean(WorkflowServiceCaller.class, "processMarfieldShipment");

    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getShipmentNoticeLogSource() {
        return shipmentNoticeLogSource;
    }

    public void setShipmentNoticeLogSource(String shipmentNoticeLogSource) {
        this.shipmentNoticeLogSource = shipmentNoticeLogSource;
    }

    public String getShipmentNoticeFormattedSource() {
        return shipmentNoticeFormattedSource;
    }

    public void setShipmentNoticeFormattedSource(String shipmentNoticeFormattedSource) {
        this.shipmentNoticeFormattedSource = shipmentNoticeFormattedSource;
    }

    public String getLogOutputDLQ() {
        return logOutputDLQ;
    }

    public void setLogOutputDLQ(String logOutputDLQ) {
        this.logOutputDLQ = logOutputDLQ;
    }

    public String getOutputDLQ() {
        return outputDLQ;
    }

    public void setOutputDLQ(String outputDLQ) {
        this.outputDLQ = outputDLQ;
    }

    public int getStartupOrderOffset() {
        return startupOrderOffset;
    }

    public void setStartupOrderOffset(int startupOrderOffset) {
        this.startupOrderOffset = startupOrderOffset;
    }

}
