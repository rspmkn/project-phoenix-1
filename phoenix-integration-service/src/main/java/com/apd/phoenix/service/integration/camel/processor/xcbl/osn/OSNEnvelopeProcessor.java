package com.apd.phoenix.service.integration.camel.processor.xcbl.osn;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.integration.model.xcbl.v3_5.XCBLFactory;
import com.perfect.liteenvelope.MessageAcknowledgement;

/**
 * Message processing class used to decorate the current XCBL exchange with a MessageAcknowledgement 
 * response which acknowledges receipt of the OSN envelope containing the XCBL business document.  The XCBL exchange 
 * may then undergo further processing, before the response is finally delivered to the sender
 * 
 * Precondition: Body of the current message exchange is an XCBL message wrapped in an OSN Lite Envelope
 * @author RH
 */
public class OSNEnvelopeProcessor implements Processor {

    private static final Logger LOG = LoggerFactory.getLogger(OSNEnvelopeProcessor.class);

    @Override
    public void process(Exchange exchange) throws Exception {
        String envelopeId = (String) exchange.getIn().getHeader("envelopeId");
        MessageAcknowledgement messageAck = XCBLFactory.createOSNMessageAcknowledgement(envelopeId);

        if (LOG.isDebugEnabled()) {
            LOG.debug("Processing message acknowledgement response. OSN envelope ID: {}", envelopeId);
        }

        exchange.getIn().setHeader("isValid", true);
        exchange.getIn().setHeader("messageAck", messageAck);
        exchange.getIn().setHeader("transactionId", messageAck.getAcknowledgementReferenceID());
    }
}
