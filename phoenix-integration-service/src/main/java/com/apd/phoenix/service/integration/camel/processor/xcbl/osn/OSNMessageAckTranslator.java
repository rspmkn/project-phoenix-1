package com.apd.phoenix.service.integration.camel.processor.xcbl.osn;

import com.apd.phoenix.service.integration.camel.processor.MessageTranslator;
import com.apd.phoenix.service.model.dto.FunctionalAcknowledgementDto;
import com.perfect.liteenvelope.MessageAcknowledgement;

public class OSNMessageAckTranslator implements MessageTranslator<FunctionalAcknowledgementDto, MessageAcknowledgement> {

    @Override
    public MessageAcknowledgement fromCanonical(FunctionalAcknowledgementDto functionalAck) {
        MessageAcknowledgement xcblMessageAck = new MessageAcknowledgement();
        return xcblMessageAck;
    }

    @Override
    public FunctionalAcknowledgementDto toCanonical(MessageAcknowledgement osnMessageAck) {
        FunctionalAcknowledgementDto functionalAckDto = new FunctionalAcknowledgementDto();
        return functionalAckDto;
    }

}
