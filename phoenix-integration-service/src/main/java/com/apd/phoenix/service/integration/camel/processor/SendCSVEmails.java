package com.apd.phoenix.service.integration.camel.processor;

import java.io.ByteArrayInputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import javax.ejb.Stateless;
import javax.inject.Inject;
import org.apache.camel.Exchange;
import com.apd.phoenix.core.utility.TenantConfigRepository;
import com.apd.phoenix.service.email.api.Attachment;
import com.apd.phoenix.service.email.api.EmailService;
import com.apd.phoenix.service.email.api.Attachment.MimeType;
import com.apd.phoenix.service.persistence.multitenancy.impl.CurrentTenantIdentifierResolverImpl;

@Stateless
public class SendCSVEmails {

    @Inject
    EmailService emailService;

    public void sendInternalEmails(Exchange exchange){
		exchange.setOut(exchange.getIn());
		Set<String> to = new HashSet<>();
		Properties emailOverride = TenantConfigRepository.getInstance().getProperties("email.override");
		if(Boolean.TRUE.toString().equals(emailOverride.get("override"))){
			to.add(emailOverride.getProperty("email"));
		}else{
			to.add("evadinion@americanproduct.com");
			to.add("juanitarichardson@americanproduct.com");
			to.add("acctsreceivable@americanproduct.com");
		};
		List<Attachment> attachments = new ArrayList<>();
		Attachment attachment = new Attachment();
		attachment.setFileName((String) exchange.getIn().getHeader(Exchange.FILE_NAME));
		String csvContent = (String) exchange.getIn().getBody();
		attachment.setContent(new ByteArrayInputStream(csvContent.getBytes(StandardCharsets.UTF_8)));
		attachment.setMimeType(MimeType.csv);
		attachments.add(attachment);
		Properties emailsLoader = TenantConfigRepository.getInstance().getPropertiesByTenantId(CurrentTenantIdentifierResolverImpl.getCurrentTenant(), "tenant");
		emailService.sendEmail(emailsLoader.getProperty("internal.emails.noreply"), to , "Marquette CSV Document" , "Marquette CSV Document Attatched.", attachments);
		
	}
}
