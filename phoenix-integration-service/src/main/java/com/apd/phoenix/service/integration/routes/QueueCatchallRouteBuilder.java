package com.apd.phoenix.service.integration.routes;

import org.apache.camel.LoggingLevel;
import com.apd.phoenix.service.integration.multitenancy.AbstractTenantAwareRouteBuilder;

public class QueueCatchallRouteBuilder extends AbstractTenantAwareRouteBuilder {

    private String queue;
    private String dlq;

    @Override
    public void configureRouteImplementation() throws Exception {
        from(queue).log(LoggingLevel.ERROR, "Unexpected partnerId: ${header.partnerId}").to(dlq);

    }

    public String getDlq() {
        return dlq;
    }

    public void setDlq(String dlq) {
        this.dlq = dlq;
    }

    public String getQueue() {
        return queue;
    }

    public void setQueue(String queue) {
        this.queue = queue;
    }

}
