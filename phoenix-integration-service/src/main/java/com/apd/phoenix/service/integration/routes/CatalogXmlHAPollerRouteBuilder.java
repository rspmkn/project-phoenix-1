package com.apd.phoenix.service.integration.routes;

import com.apd.phoenix.service.camel.DelayBean;
import com.apd.phoenix.service.integration.multitenancy.AbstractTenantAwareRouteBuilder;
import com.apd.phoenix.service.integration.camel.aggregation.strategy.ArrayListAggregationStrategy;

public class CatalogXmlHAPollerRouteBuilder extends AbstractTenantAwareRouteBuilder {

    private String itemSource;

    private String relationshipSource;

    private String output;

    private String brandSource;

    private String usscoRelationshipSource;

    private int aggregationSize;

    private Long aggregationTimeout;

    @Override
    public void configureRouteImplementation() throws Exception {
        from(itemSource)
        /*
         * delays picking up more messages to deploy until everything is deployed to avoid 
         * transaction timeouts while the system is under load from deployment
         */
        .autoStartup(false).routePolicy(DelayBean.getDelayPolicy()).setHeader("type", constant("item")).to(output);

        from(relationshipSource).autoStartup(false).routePolicy(DelayBean.getDelayPolicy()).setHeader("type",
                constant("relationship")).to(output);

        from(brandSource).autoStartup(false).routePolicy(DelayBean.getDelayPolicy()).setHeader("type",
                constant("brand")).to(output);

        from(usscoRelationshipSource).autoStartup(false).routePolicy(DelayBean.getDelayPolicy()).unmarshal().string()
                .split(body().tokenize("\n")).streaming().aggregate(header("tenantId"),
                        new ArrayListAggregationStrategy()).completionSize(aggregationSize).completionTimeout(
                        aggregationTimeout).setHeader("type", constant("usscoRelationship")).to(output);

    }

    public String getItemSource() {
        return itemSource;
    }

    public void setItemSource(String itemSource) {
        this.itemSource = itemSource;
    }

    public String getRelationshipSource() {
        return relationshipSource;
    }

    public void setRelationshipSource(String relationshipSource) {
        this.relationshipSource = relationshipSource;
    }

    public String getOutput() {
        return output;
    }

    public void setOutput(String output) {
        this.output = output;
    }

    public String getBrandSource() {
        return brandSource;
    }

    public void setBrandSource(String brandSource) {
        this.brandSource = brandSource;
    }

    public String getUsscoRelationshipSource() {
        return usscoRelationshipSource;
    }

    public void setUsscoRelationshipSource(String usscoRelationshipSource) {
        this.usscoRelationshipSource = usscoRelationshipSource;
    }

    public int getAggregationSize() {
        return aggregationSize;
    }

    public void setAggregationSize(int aggregationSize) {
        this.aggregationSize = aggregationSize;
    }

    public Long getAggregationTimeout() {
        return aggregationTimeout;
    }

    public void setAggregationTimeout(Long aggregationTimeout) {
        this.aggregationTimeout = aggregationTimeout;
    }

}
