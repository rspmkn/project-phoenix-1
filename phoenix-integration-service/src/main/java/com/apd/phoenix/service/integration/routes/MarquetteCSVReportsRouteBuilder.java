package com.apd.phoenix.service.integration.routes;

import org.apache.camel.Exchange;
import org.apache.camel.LoggingLevel;
import com.apd.phoenix.service.integration.camel.processor.InvoiceAttatchmentBean;
import com.apd.phoenix.service.integration.camel.processor.MarquetteProcessor;
import com.apd.phoenix.service.integration.camel.processor.SendCSVEmails;
import com.apd.phoenix.service.integration.camel.processor.StreamToByteProcessor;
import com.apd.phoenix.service.integration.camel.processor.YesterdayBean;
import com.apd.phoenix.service.integration.multitenancy.AbstractTenantAwareRouteBuilder;

public class MarquetteCSVReportsRouteBuilder extends AbstractTenantAwareRouteBuilder {

    private String invoiceSource;
    private String detailsFileEndpoint;
    private String transmittalFileEndpoint;
    private String marquetteDestination;
    private String output;
    private Integer startupOrderOffset;
    private String detailsFileEndpointDLQ;
    private String transmittalFileEndpointDLQ;
    private String marquetteDestinationDLQ;
    private String pdfRetrievalEndpoint;
    private String invoicePDFEndpoint;
    private String invoicePDFEndpointDLQ;
    private String backupPartnerEndpoint;
    private String backupPartnerSource;

    @Override
    public void configureRouteImplementation() throws Exception {
        onException(Exception.class).maximumRedeliveries(0).handled(true).log(LoggingLevel.INFO,
                "Error in Marquette Invoice file outbound processing: ${exception.stacktrace}").setHeader("exception")
                .simple("${exception.stacktrace}");

        from(getInvoiceSource()).startupOrder(getStartupOrderOffset()).bean(YesterdayBean.class, "setYesterday")
                .multicast().parallelProcessing()
                .to(transmittalFileEndpoint, detailsFileEndpoint, pdfRetrievalEndpoint);

        from(detailsFileEndpoint).startupOrder(getStartupOrderOffset() + 1).errorHandler(
                deadLetterChannel(getDetailsFileEndpointDLQ()).maximumRedeliveries(2).redeliveryDelay(2000)
                        .useExponentialBackOff().backOffMultiplier(2)).bean(MarquetteProcessor.class,
                "invoicesToDetailsFile").setHeader("CamelFileName").simple(
                "${date:header.yesterday:MM-dd-YYYY}-${in.header.credit}INVOICES.csv").to(marquetteDestination);

        from(transmittalFileEndpoint).errorHandler(
                deadLetterChannel(getTransmittalFileEndpointDLQ()).maximumRedeliveries(2).redeliveryDelay(2000)
                        .useExponentialBackOff().backOffMultiplier(2)).startupOrder(getStartupOrderOffset() + 2).bean(
                MarquetteProcessor.class, "invoicesToTransmittalForm").setHeader(Exchange.FILE_NAME).simple(
                "${date:header.yesterday:MM-dd-YYYY}-INV-${in.header.credit}TRANSMITTAL.csv").to(marquetteDestination);

        from(pdfRetrievalEndpoint).split().jxpath("in/body/dtos").to(invoicePDFEndpoint);

        from(invoicePDFEndpoint)
                .errorHandler(
                        deadLetterChannel(invoicePDFEndpointDLQ).maximumRedeliveries(2).redeliveryDelay(2000)
                                .useExponentialBackOff().backOffMultiplier(2))
                .startupOrder(getStartupOrderOffset() + 5)
                .bean(YesterdayBean.class, "setYesterday")
                .setHeader("invoiceNumber")
                .jxpath("in/body/invoiceNumber")
                .bean(InvoiceAttatchmentBean.class, "retrieveInvoice")
                .setHeader(Exchange.FILE_NAME)
                .simple(
                        "Invoice Assignments ${date:header.yesterday:MM-dd-YYYY}/customer-invoice${in.header.invoiceNumber}.pdf")
                .process(new StreamToByteProcessor()).wireTap(backupPartnerSource).to(output);

        from(marquetteDestination).errorHandler(
                deadLetterChannel(getMarquetteDestinationDLQ()).maximumRedeliveries(2).redeliveryDelay(2000)
                        .useExponentialBackOff().backOffMultiplier(2)).startupOrder(getStartupOrderOffset() + 3).bean(
                SendCSVEmails.class, "sendInternalEmails").wireTap(backupPartnerSource).to(output);

        from(backupPartnerSource).errorHandler(
                deadLetterChannel(getMarquetteDestinationDLQ()).maximumRedeliveries(2).redeliveryDelay(2000)
                        .useExponentialBackOff().backOffMultiplier(2)).startupOrder(getStartupOrderOffset() + 4).to(
                backupPartnerEndpoint);

    }

    public String getDetailsFileEndpoint() {
        return detailsFileEndpoint;
    }

    public void setDetailsFileEndpoint(String detailsFileEndpoint) {
        this.detailsFileEndpoint = detailsFileEndpoint;
    }

    public String getInvoiceSource() {
        return invoiceSource;
    }

    public void setInvoiceSource(String invoiceSource) {
        this.invoiceSource = invoiceSource;
    }

    public String getTransmittalFileEndpoint() {
        return transmittalFileEndpoint;
    }

    public void setTransmittalFileEndpoint(String transmittalFileEndpoint) {
        this.transmittalFileEndpoint = transmittalFileEndpoint;
    }

    public String getMarquetteDestination() {
        return marquetteDestination;
    }

    public void setMarquetteDestination(String marquetteDestination) {
        this.marquetteDestination = marquetteDestination;
    }

    public String getOutput() {
        return output;
    }

    public void setOutput(String output) {
        this.output = output;
    }

    public Integer getStartupOrderOffset() {
        return startupOrderOffset;
    }

    public void setStartupOrderOffset(Integer startupOrderOffset) {
        this.startupOrderOffset = startupOrderOffset;
    }

    public String getDetailsFileEndpointDLQ() {
        return detailsFileEndpointDLQ;
    }

    public void setDetailsFileEndpointDLQ(String detailsFileEndpointDLQ) {
        this.detailsFileEndpointDLQ = detailsFileEndpointDLQ;
    }

    public String getTransmittalFileEndpointDLQ() {
        return transmittalFileEndpointDLQ;
    }

    public void setTransmittalFileEndpointDLQ(String transmittalFileEndpointDLQ) {
        this.transmittalFileEndpointDLQ = transmittalFileEndpointDLQ;
    }

    public String getMarquetteDestinationDLQ() {
        return marquetteDestinationDLQ;
    }

    public void setMarquetteDestinationDLQ(String marquetteDestinationDLQ) {
        this.marquetteDestinationDLQ = marquetteDestinationDLQ;
    }

    public String getPdfRetrievalEndpoint() {
        return pdfRetrievalEndpoint;
    }

    public void setPdfRetrievalEndpoint(String pdfRetrievalEndpoint) {
        this.pdfRetrievalEndpoint = pdfRetrievalEndpoint;
    }

    public String getInvoicePDFEndpoint() {
        return invoicePDFEndpoint;
    }

    public void setInvoicePDFEndpoint(String invoicePDFEndpoint) {
        this.invoicePDFEndpoint = invoicePDFEndpoint;
    }

    public String getInvoicePDFEndpointDLQ() {
        return invoicePDFEndpointDLQ;
    }

    public void setInvoicePDFEndpointDLQ(String invoicePDFEndpointDLQ) {
        this.invoicePDFEndpointDLQ = invoicePDFEndpointDLQ;
    }

    public String getBackupPartnerEndpoint() {
        return backupPartnerEndpoint;
    }

    public void setBackupPartnerEndpoint(String backupPartnerEndpoint) {
        this.backupPartnerEndpoint = backupPartnerEndpoint;
    }

    public String getBackupPartnerSource() {
        return backupPartnerSource;
    }

    public void setBackupPartnerSource(String backupPartnerSource) {
        this.backupPartnerSource = backupPartnerSource;
    }

}
