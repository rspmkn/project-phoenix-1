/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.service.integration.camel.processor;

import com.apd.phoenix.service.business.SkuTypeBp;
import com.apd.phoenix.service.model.dto.LineItemDto;
import com.apd.phoenix.service.model.dto.LineItemXReturnDto;
import com.apd.phoenix.service.model.dto.PurchaseOrderDto;
import com.apd.phoenix.service.model.dto.SkuDto;
import com.apd.phoenix.service.model.dto.SkuTypeDto;
import com.apd.phoenix.service.model.dto.UnitOfMeasureDto;
import com.apd.phoenix.service.model.dto.VendorCreditInvoiceDto;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.edi.model.x12.edi810.segment.InvoiceBody;
import javax.edi.model.x12.edi810.segment.InvoiceItemGroup;
import javax.edi.model.x12.segment.BaselineItemData;
import javax.edi.model.x12.segment.ReferenceNumber;
import javax.edi.model.x12.segment.TermsOfSale;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author nreidelb
 */
public class EDI810ToCreditInvoiceTranslator implements Processor {

    public static final String APD_PO_TYPE = "APD";
    private static final Logger LOG = LoggerFactory.getLogger(EDI810ToCreditInvoiceTranslator.class);
    public static final String PURCHASE_ORDER_QUALIFIER = "PO";
    public static final String INVOICE_NUMBER_QUALIFIER = "IV";
    public static final String LETTERS_AND_NOTES_QUALIFIER = "L1";

    @Override
    public void process(Exchange exchange) throws Exception {
        String rawMessage = (String) exchange.getIn().getHeader("rawMessage");
        LOG.debug("Raw Message: {}", rawMessage);

        InvoiceBody invoice = (InvoiceBody) exchange.getIn().getBody();
        VendorCreditInvoiceDto vendorCreditInvoiceDto = createVendorCreditInvoiceDto(invoice);
        exchange.getOut().setHeaders(exchange.getIn().getHeaders());
        exchange.getOut().setBody(vendorCreditInvoiceDto, VendorCreditInvoiceDto.class);
    }

    private VendorCreditInvoiceDto createVendorCreditInvoiceDto(InvoiceBody invoice) {
        VendorCreditInvoiceDto returnOrder = new VendorCreditInvoiceDto();
        returnOrder.setCustomerOrderDto(new PurchaseOrderDto());
        returnOrder.getCustomerOrderDto().setApdPoNumber(
                invoice.getHeader().getBeginningSegmentforInvoice().getPurchaseOrderNumber());
        returnOrder.setRaNumber(invoice.getHeader().getBeginningSegmentforInvoice().getInvoiceNumber());
        returnOrder.setInvoiceNumber(invoice.getHeader().getBeginningSegmentforInvoice().getInvoiceNumber());
        returnOrder.setCreatedDate(invoice.getHeader().getBeginningSegmentforInvoice().getInvoiceDate());
        if (invoice.getHeader().getTermsOfSale() != null) {
            for (TermsOfSale terms : invoice.getHeader().getTermsOfSale()) {
                if (terms != null && terms.getTermsNetDueDate() != null) {
                    returnOrder.setDueDate(terms.getTermsNetDueDate());
                }
            }
        }
        createLineItemDtos(returnOrder, invoice);

        return returnOrder;
    }

    private void createLineItemDtos(VendorCreditInvoiceDto vendorCreditInvoiceDto,InvoiceBody body){
        //list to track which units of measure have been created
        List<UnitOfMeasureDto> unitsOfMeasure = new ArrayList<>();
        Set<LineItemXReturnDto> dtoItems = new HashSet<>();
        Iterable<InvoiceItemGroup> invoiceItems = body.getDetail().getInvoiceItems();
        BigDecimal restockingTotal = BigDecimal.ZERO;
        for(InvoiceItemGroup item:invoiceItems){
            BaselineItemData baseLineItemData = item.getBaselineItemData();
            LineItemXReturnDto lineItemXReturnDto = new LineItemXReturnDto();
            LineItemDto lineItem = new LineItemDto();
            final StringBuilder stringBuilder = new StringBuilder(baseLineItemData.getQuantityInvoiced());
            final BigInteger quantity = new BigInteger(stringBuilder.toString());            
            lineItemXReturnDto.setQuantity(quantity);
            boolean found = false;
            String unit = baseLineItemData.getUnitOfMeasureCode();
            //checks to see if unit of measure has already been created
            for(UnitOfMeasureDto uom:unitsOfMeasure){
                if(uom.getName().equals(unit)){
                    lineItem.setUnitOfMeasure(uom);
                    found = true;
                    break;
                }
            }
            if(!found){
                UnitOfMeasureDto unitOfMeasure = new UnitOfMeasureDto();
                unitOfMeasure.setName(baseLineItemData.getUnitOfMeasureCode());            
                lineItem.setUnitOfMeasure(unitOfMeasure);
                unitsOfMeasure.add(unitOfMeasure);
            }
            lineItem.setCost(baseLineItemData.getUnitPrice());
            lineItem.setApdSku(baseLineItemData.getProductServiceID1());
            SkuDto customerSku = new SkuDto();
            SkuTypeDto customerType = new SkuTypeDto();
            customerType.setName(SkuTypeBp.SKUTYPE_CUSTOMER);
            customerSku.setType(customerType);
            customerSku.setValue(Edi810ToInvoiceTranslator.removeDashes(baseLineItemData.getProductServiceID2()));
            lineItem.setCustomerSku(customerSku);

            String lineNumberString = baseLineItemData.getAssignedIdentificationNumber();
        	if (StringUtils.isNotBlank(lineNumberString)) {
                try {
                	lineItem.setLineNumber(Integer.parseInt(lineNumberString));
                } catch (NumberFormatException e) {
            		LOG.warn("Exception while parsing Line Number: " + lineNumberString);
                }
        	}
            
            ArrayList<String> comments = new ArrayList<>();
            if(item.getProductItemDescription()!=null){
                comments.add(item.getProductItemDescription().getDescription());
            }
            lineItem.setVendorComments(comments);
            for(ReferenceNumber number:item.getReferenceNumbers()){
                switch(number.getReferenceIdentificationQualifier()){
                    case PURCHASE_ORDER_QUALIFIER:
                        vendorCreditInvoiceDto.getCustomerOrderDto().setApdPoNumber(number.getReferenceIdentification());
                        break;
                    case LETTERS_AND_NOTES_QUALIFIER:
                        comments.add(number.getReferenceIdentification() + " " + 
                                number.getReferenceDescription());
                        break;
                    default:
                    	break;
                }
            }
            if(item.getAllowanceChargeOrService() != null && item.getAllowanceChargeOrService().getAllowanceOrChargeTotalAmount()!=null){
                //divide by 100 because the last two digits are decimal value
                final BigDecimal itemRestockingFee = item.getAllowanceChargeOrService().getAllowanceOrChargeTotalAmount().divide(new BigDecimal("100"));
                restockingTotal = restockingTotal.add(itemRestockingFee);
            }
            
            lineItemXReturnDto.setLineItemDto(lineItem);
            dtoItems.add(lineItemXReturnDto);
        }
        vendorCreditInvoiceDto.setRestockingFee(restockingTotal);
        vendorCreditInvoiceDto.setItems(dtoItems);
    }
}
