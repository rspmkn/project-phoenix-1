package com.apd.phoenix.service.integration.factory;

import java.util.Date;
import javax.edi.model.x12.segment.GroupEnvelopeHeader;
import javax.edi.model.x12.segment.GroupEnvelopeTrailer;
import javax.edi.model.x12.segment.InterchangeEnvelopeHeader;
import javax.edi.model.x12.segment.InterchangeEnvelopeTrailer;
import org.apache.commons.httpclient.util.DateUtil;
import com.apd.phoenix.service.integration.constant.EDIOutboundConstants;

public class EDIOutboundFactory {

    public static final String CONTROL_ID_MAX_PADDING = "000000000";

    public static InterchangeEnvelopeHeader generateInterchangeEnvelopeHeader(String senderCode, String receiverCode,
            long interchangeControlNumber, String testIndicator) {
        InterchangeEnvelopeHeader interchangeEnvelopeHeader = new InterchangeEnvelopeHeader();
        interchangeEnvelopeHeader
                .setAuthorizationInformationQualifier(EDIOutboundConstants.NO_AUTHORIZATION_INFORMATION_PRESENT);
        interchangeEnvelopeHeader.setAuthorizationInformation(EDIOutboundConstants.TEN_PADDED_SPACES);
        interchangeEnvelopeHeader.setSecurityInformationQualifier(EDIOutboundConstants.NO_SECURITY_INFORMATION_PRESENT);
        interchangeEnvelopeHeader.setSecurityInformation(EDIOutboundConstants.TEN_PADDED_SPACES);
        interchangeEnvelopeHeader
                .setSenderInterchangeIDQualifier(EDIOutboundConstants.INTERCHANGE_QUALIFIER_FROM_SAMPLE);
        interchangeEnvelopeHeader.setInterchangeSenderID(senderCode);
        interchangeEnvelopeHeader.setReceiverInterchangeIDQualifier(EDIOutboundConstants.SAMPLE_RECIEVIER_QUALIFIER);
        interchangeEnvelopeHeader.setInterchangeReceiverID(receiverCode);
        interchangeEnvelopeHeader.setInterchangeDate(new Date());
        interchangeEnvelopeHeader.setInterchangeTime(DateUtil.formatDate(new Date(), "HHMM"));
        interchangeEnvelopeHeader.setInterchangeControlNumber(zeroPad(interchangeControlNumber));
        interchangeEnvelopeHeader.setInterchangeControlID(EDIOutboundConstants.SAMPLE_CONTROL_ID);
        interchangeEnvelopeHeader.setInterchangeVersionNumber(EDIOutboundConstants.INTERCHANGE_VERSION);
        interchangeEnvelopeHeader.setiSAAcknowledgmentRequested("0");
        interchangeEnvelopeHeader.setTestIndicator(testIndicator);
        interchangeEnvelopeHeader.setSubelementSeparator(EDIOutboundConstants.SUBELEMENT_SEPARATOR);
        return interchangeEnvelopeHeader;
    }

    public static InterchangeEnvelopeTrailer generateInterchangeEnvelopeTrailer(long interchangeControlNumber,
            String numberOfGroups) {
        InterchangeEnvelopeTrailer itrailer = new InterchangeEnvelopeTrailer();
        itrailer.setNumberOfIncludedGroups(numberOfGroups);
        itrailer.setInterchangeControlNumber(zeroPad(interchangeControlNumber));
        return itrailer;
    }

    public static GroupEnvelopeHeader generateGroupEnvelopeHeader(String senderCode, String receiverCode,
            long groupControlNumber, String functionalIdCode) {
        GroupEnvelopeHeader groupEnvelopeHeader = new GroupEnvelopeHeader();
        groupEnvelopeHeader.setFunctionalIDCode(functionalIdCode);
        groupEnvelopeHeader.setApplicationSendersCode(senderCode.trim());
        groupEnvelopeHeader.setApplicationReceiversCode(receiverCode.trim());
        groupEnvelopeHeader.setDate(new Date());
        groupEnvelopeHeader.setTime(DateUtil.formatDate(new Date(), "HHMM"));
        groupEnvelopeHeader.setGroupControlNumber(Long.toString(groupControlNumber));
        groupEnvelopeHeader.setResponsibleAgencyCode(EDIOutboundConstants.ACCREDITED_STANDARDS_COMMITTEE_X12);
        groupEnvelopeHeader
                .setVersionReleaseIndustryIdentifierCode(EDIOutboundConstants.X12_PROCEDURES_REVIEW_BOARD_97);
        return groupEnvelopeHeader;
    }

    public static GroupEnvelopeTrailer generateGroupEnvelopeTrailer(String numberOfTransactionSets,
            long groupControlNumber) {
        GroupEnvelopeTrailer groupEnvelopeTrailer = new GroupEnvelopeTrailer();
        groupEnvelopeTrailer.setNumberOfTransactionSets(numberOfTransactionSets);
        groupEnvelopeTrailer.setGroupControlNumber(Long.toString(groupControlNumber));
        return groupEnvelopeTrailer;
    }

    public static String zeroPad(long value) {
        String s = CONTROL_ID_MAX_PADDING + value;
        return s.substring(s.length() - CONTROL_ID_MAX_PADDING.length());
    }
}
