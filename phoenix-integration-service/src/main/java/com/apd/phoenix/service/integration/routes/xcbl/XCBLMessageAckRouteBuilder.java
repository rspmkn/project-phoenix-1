package com.apd.phoenix.service.integration.routes.xcbl;

import javax.edi.model.x12.edi997.FunctionalAcknowledgement;
import org.apache.camel.Exchange;
import org.apache.camel.ExchangePattern;
import org.apache.camel.LoggingLevel;
import org.apache.camel.model.dataformat.JsonLibrary;
import org.apache.camel.spi.DataFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.dataformat.edi.EDIDataFormat;
import com.apd.phoenix.service.integration.camel.processor.EDI997OutboundToOrderLogTranslator;
import com.apd.phoenix.service.integration.camel.processor.EDI997toFunctionalAcknowledgementTranslator;
import com.apd.phoenix.service.integration.camel.processor.EDI997toOrderLogTranslator;
import com.apd.phoenix.service.integration.multitenancy.AbstractTenantAwareRouteBuilder;

public class XCBLMessageAckRouteBuilder extends AbstractTenantAwareRouteBuilder {

    private static final Logger LOG = LoggerFactory.getLogger(XCBLMessageAckRouteBuilder.class);

    private String inboundSource;

    private String outboundSource;

    private String messageAckResendSource;

    private String messageAckResendOuput;

    private String inboundOutput;

    private String outboundOutput;

    private String logOutput = "http://placeholder";

    private String unmarshalledSource;

    private String inboundLogSource;

    private String outboundLogSource;

    private String outputDLQ;

    private String logOutputDLQ;

    private String logOutputURI;

    private String senderId;

    @Override
    public void configureRouteImplementation() throws Exception {
        DataFormat format = new EDIDataFormat(FunctionalAcknowledgement.class);

        onException(Exception.class).maximumRedeliveries(0).handled(true).log(LoggingLevel.INFO,
                "Error in xcbl message Ack processing: ${exception.stacktrace}");

        //Inbound Message Ack
        from(inboundSource).unmarshal(format)
        //Parse transaction information
                .setHeader("senderId", constant(senderId)).setHeader("interchangeId").jxpath(
                        "/in/body/envelopeHeader/interchangeControlNumber").setHeader("groupId").jxpath(
                        "/in/body/groupEnvelopeHeader/groupControlNumber")
                //Iterate over each transaction
                .split().jxpath("/in/body/body").setHeader("transactionId").jxpath(
                        "/in/body/header/transactionSetHeader/transactionSetControlNumber").split().jxpath(
                        "/in/body/detail/transactionResponseGroup").multicast().parallelProcessing().to(
                        ExchangePattern.InOnly, inboundLogSource, unmarshalledSource);

        //Inbound Log Route
        from(inboundLogSource).errorHandler(
                deadLetterChannel(logOutputDLQ).maximumRedeliveries(2).redeliveryDelay(2000).useExponentialBackOff()
                        .backOffMultiplier(2)).process(new EDI997toOrderLogTranslator()).setHeader(
                Exchange.CONTENT_TYPE, constant("application/json")).setHeader(Exchange.HTTP_URI,
                simple(logOutputURI + "${header.messageEventType}/${header.transactionId}")).marshal().json(
                JsonLibrary.Jackson).to(logOutput);

        //Inbound Processing Route
        from(unmarshalledSource).errorHandler(
                deadLetterChannel(outputDLQ).maximumRedeliveries(2).redeliveryDelay(2000).useExponentialBackOff()
                        .backOffMultiplier(2)).process(new EDI997toFunctionalAcknowledgementTranslator()).setHeader(
                Exchange.CONTENT_TYPE, constant("application/json")).marshal().json(JsonLibrary.Jackson).to(
                inboundOutput);

        //Outbound Message Ack
        from(outboundSource).setHeader(Exchange.FILE_NAME,
                simple("transaction-xcbl-${date:now:yyyy-MM-dd-hhmm-S}.message-ack.txt")).marshal(format).wireTap(
                outboundLogSource)
        //Is the Message Ack requested
                .choice()
                //Partner Message Ack requested
                .when(simple("${header.messageAckRequested}")).to(outboundOutput)
                //Partner Message Ack not requested
                .otherwise().log(LoggingLevel.INFO, "Partner did not request Message Ack");

        //Outbound Log Route
        from(outboundLogSource).errorHandler(
                deadLetterChannel(logOutputDLQ).maximumRedeliveries(2).redeliveryDelay(2000).useExponentialBackOff()
                        .backOffMultiplier(2)).process(new EDI997OutboundToOrderLogTranslator())
                //check if apdPo is set
                .choice().when(simple("")).setHeader(Exchange.CONTENT_TYPE, constant("application/json")).setHeader(
                        Exchange.HTTP_URI, simple(logOutputURI + "${header.messageEventType}/${header.apdPo}"))
                .marshal().json(JsonLibrary.Jackson).to(logOutput)
                //No apdPo detected possibly due to no order in the system yet.  This can occur on an inbound order request that fails
                .otherwise();

        from(messageAckResendSource).errorHandler(
                deadLetterChannel(outputDLQ).maximumRedeliveries(2).redeliveryDelay(2000).useExponentialBackOff()
                        .backOffMultiplier(2)).setHeader(Exchange.FILE_NAME,
                simple("transaction-xcbl-${date:now:yyyy-MM-dd-hhmm-S}.message-ack.txt")).setHeader("partnerId")
                .jxpath("/in/body/partnerId").setBody().jxpath("in/body/rawMessage").to(messageAckResendOuput);

    }

    public String getInboundSource() {
        return inboundSource;
    }

    public void setInboundSource(String inboundSource) {
        this.inboundSource = inboundSource;
    }

    public String getInboundOutput() {
        return inboundOutput;
    }

    public void setInboundOutput(String inboundOutput) {
        this.inboundOutput = inboundOutput;
    }

    public String getLogOutput() {
        return logOutput;
    }

    public void setLogOutput(String logOutput) {
        this.logOutput = logOutput;
    }

    public String getOutputDLQ() {
        return outputDLQ;
    }

    public void setOutputDLQ(String outputDLQ) {
        this.outputDLQ = outputDLQ;
    }

    public String getLogOutputDLQ() {
        return logOutputDLQ;
    }

    public void setLogOutputDLQ(String logOutputDLQ) {
        this.logOutputDLQ = logOutputDLQ;
    }

    public String getLogOutputURI() {
        return logOutputURI;
    }

    public void setLogOutputURI(String logOutputURI) {
        this.logOutputURI = logOutputURI;
    }

    public String getOutboundSource() {
        return outboundSource;
    }

    public void setOutboundSource(String outboundSource) {
        this.outboundSource = outboundSource;
    }

    public String getSenderId() {
        return senderId;
    }

    public void setSenderId(String senderId) {
        this.senderId = senderId;
    }

    public String getOutboundOutput() {
        return outboundOutput;
    }

    public void setOutboundOutput(String outboundOutput) {
        this.outboundOutput = outboundOutput;
    }

    public String getMessageAckResendSource() {
        return messageAckResendSource;
    }

    public void setMessageAckResendSource(String messageAckResendSource) {
        this.messageAckResendSource = messageAckResendSource;
    }

    public String getMessageAckResendOuput() {
        return messageAckResendOuput;
    }

    public void setMessageAckResendOuput(String messageAckResendOuput) {
        this.messageAckResendOuput = messageAckResendOuput;
    }

    public String getUnmarshalledSource() {
        return unmarshalledSource;
    }

    public void setUnmarshalledSource(String unmarshalledSource) {
        this.unmarshalledSource = unmarshalledSource;
    }

    public String getInboundLogSource() {
        return inboundLogSource;
    }

    public void setInboundLogSource(String inboundLogSource) {
        this.inboundLogSource = inboundLogSource;
    }

    public String getOutboundLogSource() {
        return outboundLogSource;
    }

    public void setOutboundLogSource(String outboundLogSource) {
        this.outboundLogSource = outboundLogSource;
    }

}
