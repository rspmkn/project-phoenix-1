package com.apd.phoenix.service.integration.solomon.model;

import java.io.Serializable;
import java.math.BigDecimal;
import org.apache.camel.dataformat.bindy.annotation.CsvRecord;
import org.apache.camel.dataformat.bindy.annotation.DataField;

@CsvRecord(separator = ",", generateHeaderColumns = true, quote = "\"", quoting = true)
public class SolomonLineItem implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 4263401491417883697L;

    @DataField(pos = 13, required = true, columnName = "TAXABLE")
    private String taxable;

    /**
     * This field maps to the apd sku
     */
    @DataField(pos = 17, required = true, columnName = "INVTID")
    private String inventoryId;

    @DataField(pos = 18, required = true, columnName = "DESCR")
    private String description;

    @DataField(pos = 19, required = true, columnName = "ORDQTY")
    private int ordQty;

    @DataField(pos = 20, required = true, columnName = "UOM")
    private String uom;

    @DataField(pos = 21, required = true, columnName = "UNITPRICE", precision = 2)
    private BigDecimal unitPrice;

    /**
     * This field maps to the apd sku
     */
    @DataField(pos = 22, required = true, columnName = "PROD_CODE")
    private String prodCode;

    /**
     * This field maps to the apd sku
     */
    @DataField(pos = 23, required = true, columnName = "PROD_ID")
    private String prodID;

    @DataField(pos = 24, required = true, columnName = "TAXABLE_AMOUNT", precision = 2)
    private BigDecimal taxableAmount;

    @DataField(pos = 25, required = true, columnName = "TAX_AMOUNT", precision = 2)
    private BigDecimal taxAmount;

    public String getTaxable() {
        return taxable;
    }

    public void setTaxable(String taxable) {
        this.taxable = taxable;
    }

    public String getInventoryId() {
        return inventoryId;
    }

    public void setInventoryId(String inventoryId) {
        this.inventoryId = inventoryId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getOrdQty() {
        return ordQty;
    }

    public void setOrdQty(int ordQty) {
        this.ordQty = ordQty;
    }

    public String getUom() {
        return uom;
    }

    public void setUom(String uom) {
        this.uom = uom;
    }

    public BigDecimal getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(BigDecimal unitPrice) {
        this.unitPrice = unitPrice;
    }

    public String getProdCode() {
        return prodCode;
    }

    public void setProdCode(String prodCode) {
        this.prodCode = prodCode;
    }

    public String getProdID() {
        return prodID;
    }

    public void setProdID(String prodID) {
        this.prodID = prodID;
    }

    public BigDecimal getTaxableAmount() {
        return taxableAmount;
    }

    public void setTaxableAmount(BigDecimal taxableAmount) {
        this.taxableAmount = taxableAmount;
    }

    public BigDecimal getTaxAmount() {
        return taxAmount;
    }

    public void setTaxAmount(BigDecimal taxAmount) {
        this.taxAmount = taxAmount;
    }

}
