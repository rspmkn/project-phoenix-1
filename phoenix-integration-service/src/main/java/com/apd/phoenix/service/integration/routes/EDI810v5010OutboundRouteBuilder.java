package com.apd.phoenix.service.integration.routes;

import javax.edi.model.x12.v5010.edi810.Invoice;
import org.apache.camel.Exchange;
import org.apache.camel.LoggingLevel;
import org.apache.camel.spi.DataFormat;
import com.apd.phoenix.service.dataformat.edi.EDIDataFormat;
import com.apd.phoenix.service.integration.camel.processor.EDITransactionLogger;
import com.apd.phoenix.service.integration.camel.processor.InvoiceToEDI810v5010Translator;
import com.apd.phoenix.service.integration.utility.EDIOrderUtils;
import com.apd.phoenix.service.model.dto.MessageMetadataDto.CommunicationType;
import com.apd.phoenix.service.model.dto.MessageMetadataDto.MessageType;
import com.apd.phoenix.service.model.dto.OrderLogDto;
import com.apd.phoenix.service.model.dto.OrderLogDto.EventTypeDto;

public class EDI810v5010OutboundRouteBuilder extends EDIRouteBuilder {

    public static final String SENDER_ID = "senderId";
    public static final String PARTNER_ID = "partnerId";

    private String outboundInvoiceSource;

    private String outboundInvoiceEndpoint;

    private String outboundInvoiceEndpointDlq;

    private String outboundInvoiceLogSource;;

    private String senderId;

    private String testIndicator;

    private String partnerId;

    @Override
    public void configureRouteImplementation() throws Exception {

        DataFormat format = new EDIDataFormat(Invoice.class);

        onException(Exception.class).maximumRedeliveries(0).handled(true).log(LoggingLevel.INFO,
                "${exception.stacktrace}");

        from(outboundInvoiceSource).startupOrder(
                getStartOrderOffset()
                        + EDIOrderUtils.getOffset(EDIOrderUtils.RouteOrderOffsetOutboundEDI810v5010.OUTBOUND_810v5010))
        //dlq
                .errorHandler(
                        deadLetterChannel(outboundInvoiceEndpointDlq).maximumRedeliveries(2).redeliveryDelay(2000)
                                .useExponentialBackOff().backOffMultiplier(2))
                //Test indicator
                .setHeader("testIndicator", constant(testIndicator))
                //Set output filename
                .setHeader(Exchange.FILE_NAME, simple("inv810_apdphoen.dat.${date:now:yyyyMMddhhmmssSSS}"))
                //Outbound communication
                .setHeader("messageCommunicationType", constant(CommunicationType.OUTBOUND))
                //Set the partnerId
                //.setHeader("partnerId").jxpath("/in/body/shipment/customerOrderDto/partnerId")
                //Set Message Type
                .setHeader("messageType", constant(MessageType.EDI))
                //sender code
                .setHeader(SENDER_ID, constant(senderId))
                //partner Id
                .setHeader(PARTNER_ID, constant(getPartnerId()))
                //Set message destination
                .setHeader("destination", constant(outboundInvoiceEndpoint))
                //Set po number
                .setHeader("apdPo").jxpath("/in/body/shipment/customerOrderDto/apdPoNumber")
                //storage filename
                .setHeader("messageFileName", simple("${in.header.CamelFileName}"))
                //Transaction event
                .setHeader("messageEventType", simple(EventTypeDto.CUSTOMER_INVOICED.name()))
                //Translate dto to jaeb pojo
                .bean(new InvoiceToEDI810v5010Translator(), "fromCustomerInvoiceToX12")

                //Set Transaction Headers                     
                .setHeader("senderId", constant(senderId))
                //Marshal
                .marshal(format)
                //Message event type
                .choice().when(simple("${header.resend}")).setHeader("messageEventType",
                        constant(OrderLogDto.EventTypeDto.CUSTOMER_INVOICED_RESEND)).otherwise().setHeader(
                        "messageEventType", constant(OrderLogDto.EventTypeDto.CUSTOMER_INVOICED)).end()
                //Logging
                .wireTap(outboundInvoiceLogSource).wireTap(getBackupPartnerEndpoint())
                //Send to partner endpoint        
                .to(outboundInvoiceEndpoint);

        from(outboundInvoiceLogSource).startupOrder(
                getStartOrderOffset()
                        + EDIOrderUtils
                                .getOffset(EDIOrderUtils.RouteOrderOffsetOutboundEDI810v5010.OUTBOUND_LOG_810v5010))
        //Communication type
                .setHeader("communicationType", constant(CommunicationType.OUTBOUND)).bean(EDITransactionLogger.class,
                        "logEDITransaction");

    }

    public String getOutboundInvoiceSource() {
        return outboundInvoiceSource;
    }

    public void setOutboundInvoiceSource(String outboundInvoiceSource) {
        this.outboundInvoiceSource = outboundInvoiceSource;
    }

    public String getOutboundInvoiceEndpoint() {
        return outboundInvoiceEndpoint;
    }

    public void setOutboundInvoiceEndpoint(String outboundInvoiceEndpoint) {
        this.outboundInvoiceEndpoint = outboundInvoiceEndpoint;
    }

    public String getSenderId() {
        return senderId;
    }

    public void setSenderId(String senderId) {
        this.senderId = senderId;
    }

    public String getOutboundInvoiceLogSource() {
        return outboundInvoiceLogSource;
    }

    public void setOutboundInvoiceLogSource(String outboundInvoiceLogSource) {
        this.outboundInvoiceLogSource = outboundInvoiceLogSource;
    }

    public String getOutboundInvoiceEndpointDlq() {
        return outboundInvoiceEndpointDlq;
    }

    public void setOutboundInvoiceEndpointDlq(String outboundInvoiceEndpointDlq) {
        this.outboundInvoiceEndpointDlq = outboundInvoiceEndpointDlq;
    }

    public String getTestIndicator() {
        return testIndicator;
    }

    public void setTestIndicator(String testIndicator) {
        this.testIndicator = testIndicator;
    }

    /**
     * @return the partnerId
     */
    public String getPartnerId() {
        return partnerId;
    }

    /**
     * @param partnerId the partnerId to set
     */
    public void setPartnerId(String partnerId) {
        this.partnerId = partnerId;
    }

}
