package com.apd.phoenix.service.integration.solomon.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import org.apache.camel.dataformat.bindy.annotation.CsvRecord;
import org.apache.camel.dataformat.bindy.annotation.DataField;
import org.apache.camel.dataformat.bindy.annotation.OneToMany;

@CsvRecord(separator = ",", generateHeaderColumns = true, quote = "\"", quoting = true)
public class SolomonOrder implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 7955515644085597667L;

    @DataField(pos = 1, required = true, columnName = "CUSTID")
    private String custid;

    @DataField(pos = 2, required = true, columnName = "SHIPTOID")
    private String shipToId;

    @DataField(pos = 3, required = true, columnName = "ADDR1")
    private String addr1;

    @DataField(pos = 4, required = true, columnName = "ADDR2")
    private String addr2;

    @DataField(pos = 5, required = true, columnName = "CITY")
    private String city;

    @DataField(pos = 6, required = true, columnName = "COUNTY")
    private String county;

    @DataField(pos = 7, required = true, columnName = "STATE")
    private String state;

    @DataField(pos = 8, required = true, columnName = "ZIP")
    private String zip;

    @DataField(pos = 9, required = true, columnName = "PONBR")
    private String ponbr;

    @DataField(pos = 10, required = true, columnName = "APDPONBR")
    private String apdponbr;

    @DataField(pos = 11, required = true, columnName = "ORDDATE", pattern = "yyyyMMdd")
    private Date orderDate;

    @DataField(pos = 12, required = true, columnName = "REQUESTEDDATE", pattern = "yyyyMMdd")
    private Date requestDate;

    @DataField(pos = 14, required = true, columnName = "TERMS")
    private String terms;

    @DataField(pos = 15, required = true, columnName = "ORDER_TOTAL", precision = 2)
    private BigDecimal orderTotal;

    @DataField(pos = 16, required = true, columnName = "SITEID")
    private String siteId;

    @OneToMany(mappedTo = "com.apd.phoenix.service.integration.soloman.model.SolomonLineItem")
    private List<SolomonLineItem> lineItems;

    @DataField(pos = 26, required = true, columnName = "GEOCODE")
    private String geocode;

    public String getCustid() {
        return custid;
    }

    public void setCustid(String custid) {
        this.custid = custid;
    }

    public String getShipToId() {
        return shipToId;
    }

    public void setShipToId(String shipToId) {
        this.shipToId = shipToId;
    }

    public String getAddr1() {
        return addr1;
    }

    public void setAddr1(String addr1) {
        this.addr1 = addr1;
    }

    public String getAddr2() {
        return addr2;
    }

    public void setAddr2(String addr2) {
        this.addr2 = addr2;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCounty() {
        return county;
    }

    public void setCounty(String county) {
        this.county = county;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public String getPonbr() {
        return ponbr;
    }

    public void setPonbr(String ponbr) {
        this.ponbr = ponbr;
    }

    public String getApdponbr() {
        return apdponbr;
    }

    public void setApdponbr(String apdponbr) {
        this.apdponbr = apdponbr;
    }

    public Date getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(Date orderDate) {
        this.orderDate = orderDate;
    }

    public Date getRequestDate() {
        return requestDate;
    }

    public void setRequestDate(Date requestDate) {
        this.requestDate = requestDate;
    }

    public String getTerms() {
        return terms;
    }

    public void setTerms(String terms) {
        this.terms = terms;
    }

    public BigDecimal getOrderTotal() {
        return orderTotal;
    }

    public void setOrderTotal(BigDecimal orderTotal) {
        this.orderTotal = orderTotal;
    }

    public String getSiteId() {
        return siteId;
    }

    public void setSiteId(String siteId) {
        this.siteId = siteId;
    }

    public List<SolomonLineItem> getLineItems() {
        return lineItems;
    }

    public void setLineItems(List<SolomonLineItem> lineItems) {
        this.lineItems = lineItems;
    }

    public String getGeocode() {
        return geocode;
    }

    public void setGeocode(String geocode) {
        this.geocode = geocode;
    }

}
