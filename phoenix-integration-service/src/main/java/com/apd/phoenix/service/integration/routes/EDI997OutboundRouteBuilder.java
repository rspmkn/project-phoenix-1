package com.apd.phoenix.service.integration.routes;

import javax.edi.model.x12.edi997.FunctionalAcknowledgement;
import org.apache.camel.Exchange;
import org.apache.camel.LoggingLevel;
import org.apache.camel.spi.DataFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.dataformat.edi.EDIDataFormat;
import com.apd.phoenix.service.integration.utility.EDIOrderUtils;

public class EDI997OutboundRouteBuilder extends EDIRouteBuilder {

    private static final Logger LOG = LoggerFactory.getLogger(EDI997OutboundRouteBuilder.class);

    private String outboundSource;

    private String fAResendSource;

    private String fAResendOuput;

    private String outboundOutput;

    private String outputDLQ;

    @Override
    public void configureRouteImplementation() throws Exception {

        onException(Exception.class).maximumRedeliveries(0).handled(true).log(LoggingLevel.INFO,
                "Error in edi 997 processing: ${exception.stacktrace}");

        //Outbound 997
        from(outboundSource).startupOrder(
                getStartOrderOffset()
                        + EDIOrderUtils.getOffset(EDIOrderUtils.RouteOrderOffsetInboundEDI997.INBOUND_887_OUTBOUND))
        //filename
                .setHeader(Exchange.FILE_NAME, simple("fa997_apdphoen.dat.${date:now:yyyyMMddhhmmssSSS}"))
                //TODO:enable logging of all transaction
                .log(LoggingLevel.INFO, "${body}")
                //.wireTap(outboundLogSource)
                //Is the FA requested
                .choice()
                //partner FA requested
                .when(simple("${header.fARequested}")).to(outboundOutput)
                //Partner FA not request
                .otherwise().log(LoggingLevel.INFO, "Partner did not request FA");

        //Outbound Log Route
        //        from(outboundLogSource).errorHandler(
        //                deadLetterChannel(logOutputDLQ).maximumRedeliveries(2).redeliveryDelay(2000).useExponentialBackOff()
        //                        .backOffMultiplier(2)).bean(
        //                EDITransactionLogger.class, "logEDITransaction");

        from(fAResendSource).startupOrder(
                getStartOrderOffset()
                        + EDIOrderUtils.getOffset(EDIOrderUtils.RouteOrderOffsetInboundEDI997.INBOUND_997_RESEND))
                .errorHandler(
                        deadLetterChannel(outputDLQ).maximumRedeliveries(2).redeliveryDelay(2000)
                                .useExponentialBackOff().backOffMultiplier(2))
                //Set filename
                .setHeader(Exchange.FILE_NAME, simple("fa997_apdphoen.dat.${date:now:yyyyMMddhhmmssSSS}")).setHeader(
                        "partnerId").jxpath("/in/body/partnerId").setBody().jxpath("in/body/rawMessage").to(
                        fAResendOuput);

    }

    public String getOutputDLQ() {
        return outputDLQ;
    }

    public void setOutputDLQ(String outputDLQ) {
        this.outputDLQ = outputDLQ;
    }

    public String getOutboundSource() {
        return outboundSource;
    }

    public void setOutboundSource(String outboundSource, String partnerId) {
        this.outboundSource = outboundSource + "?" + getQueueSelectorParameter(partnerId);
    }

    public String getOutboundOutput() {
        return outboundOutput;
    }

    public void setOutboundOutput(String outboundOutput) {
        this.outboundOutput = outboundOutput;
    }

    public String getfAResendSource() {
        return fAResendSource;
    }

    public void setfAResendSource(String fAResendSource, String partnerId) {
        this.fAResendSource = fAResendSource + "?" + getQueueSelectorParameter(partnerId);
    }

    public String getfAResendOuput() {
        return fAResendOuput;
    }

    public void setfAResendOuput(String fAResendOuput) {
        this.fAResendOuput = fAResendOuput;
    }

}
