package com.apd.phoenix.service.integration.tax.zip.update.model;

import java.io.Serializable;
import org.apache.camel.dataformat.bindy.annotation.DataField;
import org.apache.camel.dataformat.bindy.annotation.FixedLengthRecord;

@FixedLengthRecord(length = 19, paddingChar = ' ')
public class ZipPlusFourUpdate implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -6247457286392061598L;

    @DataField(pos = 1, length = 18)
    private String record;

    @DataField(pos = 19, length = 1)
    private String action;

    public String getRecord() {
        return record;
    }

    public void setRecord(String record) {
        this.record = record;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

}
