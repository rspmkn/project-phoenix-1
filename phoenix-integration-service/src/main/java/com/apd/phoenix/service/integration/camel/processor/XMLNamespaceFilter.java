package com.apd.phoenix.service.integration.camel.processor;

import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.xml.namespace.QName;
import javax.xml.stream.FactoryConfigurationError;
import javax.xml.stream.XMLEventFactory;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLEventWriter;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import javax.xml.stream.util.EventReaderDelegate;
import org.apache.camel.Header;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class XMLNamespaceFilter {

    protected static final Logger LOGGER = LoggerFactory.getLogger(XMLNamespaceFilter.class);

    public String filter(@org.apache.camel.Body String xml, @Header("xmlnsQualifier") String xmlns) {
        if (StringUtils.isNotBlank(xmlns)) {
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("Adding XML namespace:" + xmlns);
            }

            StringReader xmlStringReader = new StringReader(xml);
            
			try {
				XMLEventReader xmlEventReader = XMLInputFactory.newInstance().createXMLEventReader(xmlStringReader);
				xmlEventReader = new NamespaceAddingEventReader(xmlEventReader, xmlns);
				ByteArrayOutputStream xmlOut = new ByteArrayOutputStream();
	            XMLEventWriter writer = XMLOutputFactory.newInstance().createXMLEventWriter(xmlOut);
	            writer.add(xmlEventReader);
	            writer.flush();
	            xml = xmlOut.toString("UTF-8");
			} catch (XMLStreamException | FactoryConfigurationError e) {
				LOGGER.error(e.toString());
			} catch (UnsupportedEncodingException e) {
				LOGGER.error(e.toString());
			}
            
        }
        return xml;
    }

    /**
     * Filter adding default namespace declaration to root element.
     */
    public class NamespaceAddingEventReader extends EventReaderDelegate {

        private final XMLEventFactory factory = XMLEventFactory.newInstance();
        private final String namespaceURI;

        private int startElementCount = 0;

        public NamespaceAddingEventReader(XMLEventReader reader, String namespaceURI) {
            super(reader);
            this.namespaceURI = namespaceURI;
        }

        /**
         * Duplicate event with additional namespace declaration.
         * @param startElement
         * @return event with namespace
         */
        private StartElement withNamespace(StartElement startElement) {
            List<Object> namespaces = new ArrayList<Object>();
            namespaces.add(factory.createNamespace(namespaceURI));
            Iterator<?> originalNamespaces = startElement.getNamespaces();
            while (originalNamespaces.hasNext()) {
                namespaces.add(originalNamespaces.next());
            }
            return factory.createStartElement(new QName(namespaceURI, startElement.getName().getLocalPart()),
                    startElement.getAttributes(), namespaces.iterator());
        }

        @Override
        public XMLEvent nextEvent() throws XMLStreamException {
            XMLEvent event = super.nextEvent();
            if (event.isStartElement()) {
                if (++startElementCount == 1) {
                    return withNamespace(event.asStartElement());
                }
            }
            return event;
        }

        @Override
        public XMLEvent peek() throws XMLStreamException {
            XMLEvent event = super.peek();
            if (startElementCount == 0 && event.isStartElement()) {
                return withNamespace(event.asStartElement());
            }
            else {
                return event;
            }
        }
    }
}
