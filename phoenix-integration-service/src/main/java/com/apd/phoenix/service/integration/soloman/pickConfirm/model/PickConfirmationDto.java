package com.apd.phoenix.service.integration.soloman.pickConfirm.model;

import java.math.BigInteger;
import java.util.Date;
import org.apache.camel.dataformat.bindy.annotation.CsvRecord;
import org.apache.camel.dataformat.bindy.annotation.DataField;

@CsvRecord(separator = ",", generateHeaderColumns = false, quote = "\"", quoting = true)
public class PickConfirmationDto {

    @DataField(pos = 1, required = true, columnName = "CUSTID")
    private String custId;

    @DataField(pos = 2, required = true, columnName = "ORDER TYPE")
    private String orderType;

    @DataField(pos = 3, required = true, columnName = "ORDNBR")
    private String orderNumber;

    @DataField(pos = 4, required = true, columnName = "SHIPPER ID")
    private String shipperId;

    @DataField(pos = 5, required = true, columnName = "ORDER DATE", pattern = "yyyy-MM-dd")
    private Date orderDate;

    @DataField(pos = 6, required = true, columnName = "SHIP DATE", pattern = "yyyy-MM-dd")
    private Date shipDate;

    @DataField(pos = 7, required = true, columnName = "SKU")
    private String sku;

    @DataField(pos = 8, required = true, columnName = "QUANTITY")
    private BigInteger quantity;

    public String getCustId() {
        return custId;
    }

    public void setCustId(String custId) {
        this.custId = custId;
    }

    public String getOrderType() {
        return orderType;
    }

    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public String getShipperId() {
        return shipperId;
    }

    public void setShipperId(String shipperId) {
        this.shipperId = shipperId;
    }

    public Date getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(Date orderDate) {
        this.orderDate = orderDate;
    }

    public Date getShipDate() {
        return shipDate;
    }

    public void setShipDate(Date shipDate) {
        this.shipDate = shipDate;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public BigInteger getQuantity() {
        return quantity;
    }

    public void setQuantity(BigInteger quantity) {
        this.quantity = quantity;
    }

}
