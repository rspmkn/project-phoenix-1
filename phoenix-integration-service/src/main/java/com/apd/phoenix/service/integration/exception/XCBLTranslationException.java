package com.apd.phoenix.service.integration.exception;

public class XCBLTranslationException extends MessageTranslationException {

    public XCBLTranslationException(String errorMessage) {
        super(errorMessage);
    }

    private static final long serialVersionUID = 1L;
}
