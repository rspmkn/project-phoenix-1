package com.apd.phoenix.service.integration.routes;

import javax.edi.model.x12.edi856.AdvanceShipmentNotice;
import org.apache.camel.ExchangePattern;
import org.apache.camel.LoggingLevel;
import org.apache.camel.spi.DataFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.dataformat.edi.EDIDataFormat;
import com.apd.phoenix.service.integration.camel.processor.EDI856toAdvanceShipmentNoticeTranslator;
import com.apd.phoenix.service.integration.camel.processor.EDI856toOrderLogTranslator;
import com.apd.phoenix.service.integration.camel.processor.MessageServiceBean;
import com.apd.phoenix.service.integration.camel.processor.WorkflowServiceCaller;
import com.apd.phoenix.service.integration.utility.EDIOrderUtils;

public class EDI856RouteBuilder extends EDIRouteBuilder {

    private static final Logger LOG = LoggerFactory.getLogger(EDI856RouteBuilder.class);

    private String source;

    private String shipmentNoticeFormattedSource = "direct:shipmentNoticeFormattedSource";

    private String output;

    private String shipmentNoticeLogSource = "direct:shipmentNoticeLogSource";

    private String logOutput = "http://placeholder";

    private String logOutputDLQ;

    private String outputDLQ;

    private String logOutputURI;

    private String senderId;

    @Override
    public void configureRouteImplementation() throws Exception {

        shipmentNoticeFormattedSource = shipmentNoticeFormattedSource + senderId;
        shipmentNoticeLogSource = shipmentNoticeLogSource + senderId;

        DataFormat format = new EDIDataFormat(AdvanceShipmentNotice.class);

        onException(Exception.class).maximumRedeliveries(0).handled(true).log(LoggingLevel.INFO,
                "Error in edi 856 inbound processing: ${exception.stacktrace}");

        onException(ClassCastException.class).maximumRedeliveries(0).handled(true).log(LoggingLevel.INFO,
                "Incorrect Type");

        from(source).startupOrder(
                getStartOrderOffset()
                        + EDIOrderUtils.getOffset(EDIOrderUtils.RouteOrderOffsetInboundEDI856.INBOUND_856))
                .unmarshal(format)
                //Parse transaction information
                .setHeader("senderId", constant(senderId)).setHeader("interchangeId").jxpath(
                        "/in/body/envelopeHeader/interchangeControlNumber").setHeader("groupId")
                .jxpath("/in/body/groupEnvelopeHeader/groupControlNumber")
                //Iterate over each transaction
                .split().jxpath("/in/body/body").setHeader("transactionId").jxpath(
                        "/in/body/header/transactionSetHeader/transactionSetControlNumber").setHeader(
                        "shipmentIdentification").jxpath("in/body/header/beginningSegment/shipmentIdentification")
                .split().jxpath("/in/body/detail/shipmentLevelGroups").setHeader("levelGroup").jxpath("/in/body")
                .split().jxpath("/in/body/orderInformationGroup").multicast().parallelProcessing().to(
                        ExchangePattern.InOnly, shipmentNoticeLogSource, shipmentNoticeFormattedSource);

        from(shipmentNoticeLogSource).startupOrder(
                getStartOrderOffset()
                        + EDIOrderUtils.getOffset(EDIOrderUtils.RouteOrderOffsetInboundEDI856.INBOUND_856_LOG))
                .errorHandler(
                        deadLetterChannel(logOutputDLQ).maximumRedeliveries(2).redeliveryDelay(2000)
                                .useExponentialBackOff().backOffMultiplier(2))
                .process(new EDI856toOrderLogTranslator()).bean(MessageServiceBean.class, "receiveMessage");

        from(shipmentNoticeFormattedSource).startupOrder(
                getStartOrderOffset()
                        + EDIOrderUtils.getOffset(EDIOrderUtils.RouteOrderOffsetInboundEDI856.INBOUND_856_FORMATTED))
                .errorHandler(
                        deadLetterChannel(outputDLQ).maximumRedeliveries(2).redeliveryDelay(2000)
                                .useExponentialBackOff().backOffMultiplier(2)).process(
                        new EDI856toAdvanceShipmentNoticeTranslator()).bean(WorkflowServiceCaller.class,
                        "processAdvanceShipmentNotice");
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getOutput() {
        return output;
    }

    public void setOutput(String output) {
        this.output = output;
    }

    public String getLogOutput() {
        return logOutput;
    }

    public void setLogOutput(String logOutput) {
        this.logOutput = logOutput;
    }

    public String getLogOutputDLQ() {
        return logOutputDLQ;
    }

    public void setLogOutputDLQ(String logOutputDLQ) {
        this.logOutputDLQ = logOutputDLQ;
    }

    public String getOutputDLQ() {
        return outputDLQ;
    }

    public void setOutputDLQ(String outputDLQ) {
        this.outputDLQ = outputDLQ;
    }

    public String getLogOutputURI() {
        return logOutputURI;
    }

    public void setLogOutputURI(String logOutputURI) {
        this.logOutputURI = logOutputURI;
    }

    public String getSenderId() {
        return senderId;
    }

    public void setSenderId(String senderId) {
        this.senderId = senderId;
    }

}
