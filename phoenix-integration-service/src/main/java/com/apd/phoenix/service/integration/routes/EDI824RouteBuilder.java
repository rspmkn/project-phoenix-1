package com.apd.phoenix.service.integration.routes;

import javax.edi.model.x12.edi824.ApplicationAdvice;
import org.apache.camel.LoggingLevel;
import org.apache.camel.spi.DataFormat;
import com.apd.phoenix.service.dataformat.edi.EDIDataFormat;
import com.apd.phoenix.service.integration.camel.processor.Edi824Translator;
import com.apd.phoenix.service.integration.camel.processor.MessageServiceBean;
import com.apd.phoenix.service.integration.camel.processor.WorkflowServiceCaller;
import com.apd.phoenix.service.integration.utility.EDIOrderUtils;

public class EDI824RouteBuilder extends EDIRouteBuilder {

    private String source;

    private String output;

    private String orgFormattedSource = "direct:orgFormattedSource";

    private String orgLogSource = "direct:orgLogSource";

    private String outputDLQ;

    private String logOutputDLQ;

    private String logOutput = "http://placeholder";

    private String logOutputURI;

    private String senderId;

    private String logSource;

    @Override
    public void configureRouteImplementation() throws Exception {

        onException(Exception.class).maximumRedeliveries(0).handled(true).log(LoggingLevel.INFO,
                "${exception.stacktrace}");

        DataFormat format = new EDIDataFormat(ApplicationAdvice.class);

        from(source).startupOrder(
                getStartOrderOffset()
                        + EDIOrderUtils.getOffset(EDIOrderUtils.RouteOrderOffsetInboundEDI824.INBOUND_824)).setHeader(
                "rawEdi").jxpath("in/body").unmarshal(format)
                // Parse transaction information
                .setHeader("senderId", constant(getSenderId())).setHeader("interchangeId").jxpath(
                        "/in/body/envelopeHeader/interchangeControlNumber").setHeader("groupId").jxpath(
                        "/in/body/groupEnvelopeHeader/groupControlNumber")
                // Iterate
                .split().jxpath("/in/body/body").setHeader("transactionId").jxpath(
                        "/in/body/header/transactionSetHeader/transactionSetControlNumber").multicast()
                .parallelProcessing().to(orgFormattedSource);

        from(orgFormattedSource).startupOrder(
                getStartOrderOffset()
                        + EDIOrderUtils.getOffset(EDIOrderUtils.RouteOrderOffsetInboundEDI824.INBOUND_824_FORMATTED))
                .errorHandler(
                        deadLetterChannel(logOutputDLQ).maximumRedeliveries(2).redeliveryDelay(2000)
                                .useExponentialBackOff().backOffMultiplier(2)).process(new Edi824Translator()).wireTap(
                        logSource)
                //removing the rawEdi header since it usually has newlines, which breaks Apache/2.2.26,
                //which incorrectly conflates LF (\n) with CRLF (\r\n) and therefore parses the second line
                //of the rawEdi as a new header. See PHOEN-4715
                .removeHeader("rawEdi").bean(WorkflowServiceCaller.class, "processErrorEmail");

        from(logSource).startupOrder(
                getStartOrderOffset()
                        + EDIOrderUtils.getOffset(EDIOrderUtils.RouteOrderOffsetInboundEDI824.INBOUND_824_LOG))
                .errorHandler(
                        deadLetterChannel(logOutputDLQ).maximumRedeliveries(2).redeliveryDelay(2000)
                                .useExponentialBackOff().backOffMultiplier(2)).bean(MessageServiceBean.class,
                        "convert824ToOrderLog").bean(MessageServiceBean.class, "receiveMessage");

    }

    /**
     * @return the source
     */
    public String getSource() {
        return source;
    }

    /**
     * @param source
     *            the source to set
     */
    public void setSource(String source) {
        this.source = source;
    }

    /**
     * @return the output
     */
    public String getOutput() {
        return output;
    }

    /**
     * @param output
     *            the output to set
     */
    public void setOutput(String output) {
        this.output = output;
    }

    /**
     * @return the orgFormattedSource
     */
    public String getUsscoorgEDISource() {
        return orgFormattedSource;
    }

    /**
     * @param orgFormattedSource
     *            the orgFormattedSource to set
     */
    public void setUsscoorgEDISource(String usscoorgEDISource) {
        this.orgFormattedSource = usscoorgEDISource;
    }

    /**
     * @return the logOutputDLQ
     */
    public String getLogOutputDLQ() {
        return logOutputDLQ;
    }

    /**
     * @param logOutputDLQ
     *            the logOutputDLQ to set
     */
    public void setLogOutputDLQ(String logOutputDLQ) {
        this.logOutputDLQ = logOutputDLQ;
    }

    /**
     * @return the orgLogSource
     */
    public String getOrgLogSource() {
        return orgLogSource;
    }

    /**
     * @param orgLogSource
     *            the orgLogSource to set
     */
    public void setOrgLogSource(String orgLogSource) {
        this.orgLogSource = orgLogSource;
    }

    /**
     * @return the logOutput
     */
    public String getOrgLogOutput() {
        return getLogOutput();
    }

    /**
     * @param logOutput
     *            the logOutput to set
     */
    public void setOrgLogOutput(String orgLogOutput) {
        this.setLogOutput(orgLogOutput);
    }

    public String getLogOutputURI() {
        return logOutputURI;
    }

    public void setLogOutputURI(String logOutputURI) {
        this.logOutputURI = logOutputURI;
    }

    /**
     * @return the outputDLQ
     */
    public String getOutputDLQ() {
        return outputDLQ;
    }

    /**
     * @param outputDLQ the outputDLQ to set
     */
    public void setOutputDLQ(String outputDLQ) {
        this.outputDLQ = outputDLQ;
    }

    /**
     * @return the logOutput
     */
    public String getLogOutput() {
        return logOutput;
    }

    /**
     * @param logOutput the logOutput to set
     */
    public void setLogOutput(String logOutput) {
        this.logOutput = logOutput;
    }

    /**
     * @return the senderId
     */
    public String getSenderId() {
        return senderId;
    }

    /**
     * @param senderId the senderId to set
     */
    public void setSenderId(String senderId) {
        this.senderId = senderId;
    }

    public String getLogSource() {
        return logSource;
    }

    public void setLogSource(String logSource) {
        this.logSource = logSource;
    }
}
