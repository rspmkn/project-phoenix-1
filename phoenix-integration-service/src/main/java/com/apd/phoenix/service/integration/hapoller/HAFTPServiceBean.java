package com.apd.phoenix.service.integration.hapoller;

import java.util.Properties;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import javax.ejb.Singleton;
import javax.inject.Inject;
import javax.inject.Named;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.camel.component.ActiveMQComponent;
import org.apache.activemq.pool.PooledConnectionFactory;
import org.apache.camel.CamelContext;
import org.apache.camel.component.cdi.CdiCamelContext;
import org.apache.camel.component.jms.JmsConfiguration;
import org.apache.camel.spi.PackageScanClassResolver;
import org.apache.commons.lang.StringUtils;
import org.apacheextras.camel.jboss.JBossPackageScanClassResolver;
import org.jboss.as.server.ServerEnvironment;
import org.jboss.msc.inject.Injector;
import org.jboss.msc.service.StartContext;
import org.jboss.msc.service.StartException;
import org.jboss.msc.service.StopContext;
import org.jboss.msc.value.InjectedValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.core.utility.TenantConfigRepository;
import com.apd.phoenix.service.integration.routes.CatalogXmlHAPollerRouteBuilder;
import com.apd.phoenix.service.integration.routes.EDIInboundRouteBuilder;
import com.apd.phoenix.service.integration.routes.FlatFileEntryPointRouteBuilder;
import com.apd.phoenix.service.integration.routes.InternalMessageValidationRouteBuilder;
import com.apd.phoenix.service.integration.routes.MarfieldShipmentFileRouteBuilder;
import com.apd.phoenix.service.integration.routes.PickListRouteBuilder;
import com.apd.phoenix.service.integration.routes.SmartSearchUpdatesRouteBuilder;
import com.apd.phoenix.service.integration.routes.TaxHAPollerRouteBuilder;
import com.apd.phoenix.service.integration.routes.UsscoShipManifestRouteBuilder;
import com.apd.phoenix.service.integration.routes.xcbl.XCBLPartnerDeterminerRouteBuilder;
import com.apd.phoenix.service.persistence.multitenancy.impl.CurrentTenantIdentifierResolverImpl;

@Named
@Singleton
public class HAFTPServiceBean implements HAFTPPollerService {

    private static final Logger LOGGER = LoggerFactory.getLogger(HAFTPServiceBean.class.getCanonicalName());
    /**
     * A flag whether the service is started.
     */
    private final AtomicBoolean started = new AtomicBoolean(false);

    @Inject
    private CdiCamelContext camelContext;

    Properties commonProperties = TenantConfigRepository.getInstance().getProperties("camel.edi-inbound.integration");

    private String nodeName;

    private final InjectedValue<ServerEnvironment> env = new InjectedValue<ServerEnvironment>();

    public Injector<ServerEnvironment> getEnvInjector() {
        return this.env;
    }

    /**
     * @return the name of the server node
     */
    public String getValue() throws IllegalStateException, IllegalArgumentException {
        if (!started.get()) {
            throw new IllegalStateException("The service '" + this.getClass().getName() + "' is not ready!");
        }
        return this.nodeName;
    }

    public void start(StartContext arg0) throws StartException {
        if (!started.compareAndSet(false, true)) {
            throw new StartException("The service is still started!");
        }
        LOGGER.info("Start service '" + this.getClass().getName() + "'");
        this.nodeName = this.env.getValue().getNodeName();
        Properties sharedXCBLProperties = TenantConfigRepository.getInstance().getProperties("camel.xcbl.integration");

        //camelContext = new DefaultCamelContext();

        setupAmq(camelContext, commonProperties.getProperty("jmsBrokerUrl"));

        PackageScanClassResolver jbossResolver = new JBossPackageScanClassResolver();

        camelContext.setPackageScanClassResolver(jbossResolver);

        Set<String> tenants = TenantConfigRepository.getInstance().getTenantNames();
        for (String tenantName : tenants) {
            try {
                CurrentTenantIdentifierResolverImpl.setCurrentTenant(TenantConfigRepository.getInstance()
                        .getTenantIdByName(tenantName));
                Long tenantId = CurrentTenantIdentifierResolverImpl.getCurrentTenant();

                //Tax
                Properties taxProperties = TenantConfigRepository.getInstance().getProperties("tax.integration");
                Properties tenantProperties = TenantConfigRepository.getInstance().getPropertiesByTenantId(tenantId,
                        "tenant");
                TaxHAPollerRouteBuilder taxRoute = new TaxHAPollerRouteBuilder();
                taxRoute.setCreateSource(tenantProperties.getProperty("taxCreateSource"));
                taxRoute.setUpdateSource(tenantProperties.getProperty("taxUpdateSource"));
                taxRoute.setZipCreateSource(tenantProperties.getProperty("zipCreateSource"));
                taxRoute.setZipUpdateSource(tenantProperties.getProperty("zipUpdateSource"));
                taxRoute.setMilitaryZipCreateSource(tenantProperties.getProperty("militaryZipCreateSource"));
                taxRoute.setMilitaryZipUpdateSource(tenantProperties.getProperty("militaryZipUpdateSource"));
                taxRoute.setRouteTenantId(tenantId);
                taxRoute.setOutput(taxProperties.getProperty("taxHAEndpoint"));

                camelContext.addRoutes(taxRoute);

                Properties usscoProperties = TenantConfigRepository.getInstance().getPropertiesByTenantId(tenantId,
                        "ussco.integration");
                Properties itemProperties = TenantConfigRepository.getInstance().getPropertiesByTenantId(tenantId,
                        "camel.item.integration");
                //USSCO ECDB2
                CatalogXmlHAPollerRouteBuilder ecdb2Route = new CatalogXmlHAPollerRouteBuilder();
                ecdb2Route.setItemSource(usscoProperties.getProperty("itemSource"));
                ecdb2Route.setRelationshipSource(usscoProperties.getProperty("relationshipSource"));
                ecdb2Route.setBrandSource(usscoProperties.getProperty("brandSource"));
                ecdb2Route.setUsscoRelationshipSource(usscoProperties.getProperty("usscoRelationshipSource"));
                ecdb2Route.setOutput(itemProperties.getProperty("ecdb2HAEndpoint"));

                ecdb2Route.setAggregationSize(Integer.parseInt(itemProperties.getProperty("aggregationSize")));
                ecdb2Route.setAggregationTimeout(Long.parseLong(itemProperties.getProperty("aggregationTimeout")));
                ecdb2Route.setRouteTenantId(tenantId);
                camelContext.addRoutes(ecdb2Route);
            }
            catch (Exception e1) {
                LOGGER.error("Problem adding routes for ECDB2 changes", e1);
            }
        }

        //Picklist
        Properties integrationProperties = TenantConfigRepository.getInstance().getProperties("solomon.integration");
        PickListRouteBuilder pickListRoute = new PickListRouteBuilder();
        pickListRoute.setSource(integrationProperties.getProperty("solomonPickListSource"));
        try {
            camelContext.addRoutes(pickListRoute);
        }
        catch (Exception e1) {
            LOGGER.error("Could not add camel routes: ", e1);
        }

        //Edi

        for (String tenantName : tenants) {
            try {
                CurrentTenantIdentifierResolverImpl.setCurrentTenant(TenantConfigRepository.getInstance()
                        .getTenantIdByName(tenantName));
                Properties tenantProperties = TenantConfigRepository.getInstance().getPropertiesByTenantName(
                        tenantName, "tenant");
                String partners = tenantProperties.getProperty("inbound.edi.partners");
                String[] partnersList = StringUtils.split(partners, ',');
                if (partnersList != null) {
                    for (String partner : partnersList) {
                        if (StringUtils.isBlank(partner)) {
                            continue;
                        }
                        LOGGER.info("Adding partner EDI inbound to JMS: {}", partner);
                        Properties partnerProperties = TenantConfigRepository.getInstance().getPropertiesByTenantName(
                                tenantName, partner + ".integration");
                        EDIInboundRouteBuilder ediInboundRouteBuilder = new EDIInboundRouteBuilder();
                        ediInboundRouteBuilder.setRouteTenantId(CurrentTenantIdentifierResolverImpl.getCurrentTenant());
                        ediInboundRouteBuilder.setOutput(commonProperties.getProperty("ediInbound"));
                        ediInboundRouteBuilder.setOutputDLQ("");
                        ediInboundRouteBuilder.setPartnerId(partner);
                        ediInboundRouteBuilder.setSource(partnerProperties.getProperty("ediInboundSource"));
                        camelContext.addRoutes(ediInboundRouteBuilder);
                    }
                }
            }
            catch (Exception e) {
                LOGGER.error("An error occured:", e);
            }
        }

        InternalMessageValidationRouteBuilder internalMessageValidationRouteBuilder = new InternalMessageValidationRouteBuilder();
        internalMessageValidationRouteBuilder.setSource("activemq:queue:queue.internal-validation-queue");
        try {
            camelContext.addRoutes(internalMessageValidationRouteBuilder);
        }
        catch (Exception e) {
            LOGGER.error("An error occured:", e);
        }

        //SmartSearch catalog updates
        try {
            SmartSearchUpdatesRouteBuilder smartSearchUpdatesRouteBuilder = new SmartSearchUpdatesRouteBuilder();
            smartSearchUpdatesRouteBuilder.setSource("activemq:queue:queue.smartsearch-catalog-message-aggregated");
            camelContext.addRoutes(smartSearchUpdatesRouteBuilder);
        }
        catch (Exception e) {
            LOGGER.error("An error occured:", e);
        }

        for (String tenantName : tenants) {
            try {
                //XCBL
                Properties tenantProperties = TenantConfigRepository.getInstance().getPropertiesByTenantName(
                        tenantName, "tenant");
                if ("true".equalsIgnoreCase(tenantProperties.getProperty("xcbl.enabled", "false"))) {
                    XCBLPartnerDeterminerRouteBuilder xCBLPartnerDeterminerRouteBuilder = new XCBLPartnerDeterminerRouteBuilder();
                    xCBLPartnerDeterminerRouteBuilder.setInboundXCBLSource(tenantProperties
                            .getProperty("inbound.xcbl.camel.entry.point"));
                    xCBLPartnerDeterminerRouteBuilder.setInboundXCBLSink(sharedXCBLProperties
                            .getProperty("inbound.xcbl.entry.point.queue"));
                    camelContext.addRoutes(xCBLPartnerDeterminerRouteBuilder);
                }

                CurrentTenantIdentifierResolverImpl.setCurrentTenant(TenantConfigRepository.getInstance()
                        .getTenantIdByName(tenantName));
                //Ussco Manifest
                Properties usscoProperties = TenantConfigRepository.getInstance().getPropertiesByTenantName(tenantName,
                        "ussco.integration");
                UsscoShipManifestRouteBuilder usscoShipManifestRouteBuilder = new UsscoShipManifestRouteBuilder();
                usscoShipManifestRouteBuilder.setRouteTenantId(CurrentTenantIdentifierResolverImpl.getCurrentTenant());
                usscoShipManifestRouteBuilder.setSource(usscoProperties.getProperty("manifestSource"));
                camelContext.addRoutes(usscoShipManifestRouteBuilder);
            }
            catch (Exception e1) {
                LOGGER.error("Could not add camel routes: ", e1);
            }
        }

        //Marfield processing
        int startOrderOffset = 1;
        for (String tenantName : tenants) {
            try {
                Properties marfieldProperties = TenantConfigRepository.getInstance().getProperties(
                        "marfield.integration");
                Properties tenantProperties = TenantConfigRepository.getInstance().getPropertiesByTenantName(
                        tenantName, "tenant");
                if ("true".equalsIgnoreCase(tenantProperties.getProperty("marfield_integration_enabled"))) {
                    FlatFileEntryPointRouteBuilder flatFileEntryPointRouteBuilder = new FlatFileEntryPointRouteBuilder();
                    flatFileEntryPointRouteBuilder.setTransactionSource(marfieldProperties
                            .getProperty("inboundEdiSource"));
                    flatFileEntryPointRouteBuilder.setStartOrderOffset(startOrderOffset);
                    startOrderOffset++;
                    flatFileEntryPointRouteBuilder.setPartnerId("marfield");
                    flatFileEntryPointRouteBuilder.setProcessorEndpoint(marfieldProperties
                            .getProperty("processorEndpoint"));
                    camelContext.addRoutes(flatFileEntryPointRouteBuilder);

                    MarfieldShipmentFileRouteBuilder marfieldShipmentFileRouteBuilder = new MarfieldShipmentFileRouteBuilder();
                    marfieldShipmentFileRouteBuilder.setSource(marfieldProperties.getProperty("processorEndpoint"));
                    marfieldShipmentFileRouteBuilder.setShipmentNoticeLogSource(marfieldProperties
                            .getProperty("shipmentNoticeLogSource"));
                    marfieldShipmentFileRouteBuilder.setShipmentNoticeFormattedSource(marfieldProperties
                            .getProperty("shipmentNoticeFormattedSource"));
                    marfieldShipmentFileRouteBuilder.setLogOutputDLQ(marfieldProperties
                            .getProperty("shipmentNoticeLogDLQ"));
                    marfieldShipmentFileRouteBuilder.setOutputDLQ(marfieldProperties.getProperty("shipmentNoticeDLQ"));
                    marfieldShipmentFileRouteBuilder.setStartupOrderOffset(startOrderOffset);
                    camelContext.addRoutes(marfieldShipmentFileRouteBuilder);
                }
            }
            catch (Exception e1) {
                LOGGER.error("Could not add camel routes: ", e1);
            }
        }

        try {
            camelContext.start();
        }
        catch (Exception e) {
            LOGGER.error("An error occured:", e);
        }
    }

    public void stop(StopContext arg0) {
        if (!started.compareAndSet(true, false)) {
            LOGGER.warn("The service '" + this.getClass().getName() + "' is not active!");
        }
        else {
            LOGGER.info("Stop service '" + this.getClass().getName() + "'");
        }
        try {
            camelContext.stop();
        }
        catch (Exception e) {
            LOGGER.error("Could not stop camel: ", e);
        }
    }

    public void setupAmq(CamelContext context, String jmsBrokerUrl) {
        Properties amqProperties = TenantConfigRepository.getInstance().getProperties("amq");
        ActiveMQConnectionFactory activeMQConnectionFactory = new ActiveMQConnectionFactory();
        activeMQConnectionFactory.setBrokerURL(jmsBrokerUrl);
        PooledConnectionFactory pooledConnectionFactory = new PooledConnectionFactory();
        pooledConnectionFactory.setConnectionFactory(activeMQConnectionFactory);
        pooledConnectionFactory.setMaxConnections(Integer.parseInt(amqProperties.getProperty(getClass().getSimpleName()
                + ".maxConnections", "2")));
        JmsConfiguration jmsConfiguration = new JmsConfiguration();
        jmsConfiguration.setConnectionFactory(pooledConnectionFactory);
        jmsConfiguration.setConcurrentConsumers(Integer.parseInt(amqProperties.getProperty(getClass().getSimpleName()
                + ".concurrentConsumers", "2")));
        ActiveMQComponent activeMQComponent = new ActiveMQComponent();
        activeMQComponent.setConfiguration(jmsConfiguration);
        context.addComponent("activemq", activeMQComponent);
    }

}
