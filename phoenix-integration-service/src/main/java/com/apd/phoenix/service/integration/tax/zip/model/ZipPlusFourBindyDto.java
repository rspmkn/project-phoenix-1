package com.apd.phoenix.service.integration.tax.zip.model;

import java.io.Serializable;
import org.apache.camel.dataformat.bindy.annotation.DataField;
import org.apache.camel.dataformat.bindy.annotation.FixedLengthRecord;

@FixedLengthRecord(length = 18, paddingChar = ' ')
public class ZipPlusFourBindyDto implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private Long id = null;

    private int version = 0;

    @DataField(pos = 10, length = 4)
    private String hi;

    @DataField(pos = 6, length = 4)
    private String low;

    @DataField(pos = 1, length = 5)
    private String zip;

    @DataField(pos = 14, length = 5)
    private String countyFips;

    public Long getId() {
        return this.id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public int getVersion() {
        return this.version;
    }

    public void setVersion(final int version) {
        this.version = version;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        if (id != null) {
            return id.equals(((ZipPlusFourBindyDto) that).id);
        }
        return super.equals(that);
    }

    @Override
    public int hashCode() {
        if (id != null) {
            return id.hashCode();
        }
        return super.hashCode();
    }

    public String getHi() {
        return this.hi;
    }

    public void setHi(final String hi) {
        this.hi = hi;
    }

    public String getLow() {
        return this.low;
    }

    public void setLow(final String low) {
        this.low = low;
    }

    public String getZip() {
        return this.zip;
    }

    public void setZip(final String zip) {
        this.zip = zip;
    }

    public String getCountyFips() {
        return this.countyFips;
    }

    public void setCountyFips(final String countyFips) {
        this.countyFips = countyFips;
    }

    @Override
    public String toString() {
        String result = getClass().getSimpleName() + " ";
        if (hi != null && !hi.trim().isEmpty())
            result += "hi: " + hi;
        if (low != null && !low.trim().isEmpty())
            result += ", low: " + low;
        if (zip != null && !zip.trim().isEmpty())
            result += ", zip: " + zip;
        if (countyFips != null && !countyFips.trim().isEmpty())
            result += ", countyFips: " + countyFips;
        return result;
    }
}
