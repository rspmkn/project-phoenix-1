/**
 * 
 */
package com.apd.phoenix.service.integration.usps.model;

import java.io.Serializable;
import java.util.Date;
import org.apache.camel.dataformat.bindy.annotation.CsvRecord;
import org.apache.camel.dataformat.bindy.annotation.DataField;

/**
 * POJO for USPS Catalog.
 * 
 * @author Bryan Sauders <bsaunder@redhat.com>
 * 
 */
@CsvRecord(separator = "|", generateHeaderColumns = false, quoting = false, crlf = "UNIX")
public class UspsCatalogItem implements Serializable {

    private static final long serialVersionUID = 7955515644085597667L;

    @DataField(pos = 1, required = true, clip = true, length = 10, columnName = "SUPPLIER-ID")
    private String supplierId;

    @DataField(pos = 2, required = true, clip = true, length = 3, columnName = "TRANSACTION-SET-ID")
    private String transactionSetId;

    @DataField(pos = 3, required = true, clip = true, length = 2, columnName = "RECORD-ID")
    private String recordId;

    @DataField(pos = 4, required = true, clip = true, length = 30, columnName = "ITEM-NO")
    private String itemNo;

    @DataField(pos = 5, required = true, clip = true, length = 80, columnName = "DESCRIPTION")
    private String description;

    @DataField(pos = 6, required = false, clip = true, length = 30, columnName = "SUPPLIER-ITEM-NO")
    private String supplierItemNo;

    @DataField(pos = 7, required = false, clip = true, length = 30, columnName = "MANUF-ITEM-NO")
    private String manufacturerItemNo;

    @DataField(pos = 8, required = true, clip = true, length = 10, columnName = "UNSPSC-CD")
    private Integer unspscCd;

    @DataField(pos = 9, required = true, clip = true, length = 20, columnName = "SUPPLIER-HIERARCHY")
    private String supplierHierarchy;

    @DataField(pos = 10, required = true, clip = true, length = 13, precision = 4, columnName = "ITEM-PRICE")
    private Double itemPrice;

    @DataField(pos = 11, required = true, clip = true, length = 13, precision = 4, columnName = "LIST-PRICE")
    private Double listPrice;

    @DataField(pos = 12, required = true, clip = true, length = 2, columnName = "UOM")
    private String uom;

    @DataField(pos = 13, required = true, clip = true, length = 1, columnName = "BUSINESS-CLASS")
    private String businessClass;

    @DataField(pos = 14, required = true, clip = true, length = 1, columnName = "WOMEN-OWNED")
    private String womenOwned;

    @DataField(pos = 15, required = true, clip = true, length = 2, columnName = "MINORITY-BUSINESS")
    private String minorityBusiness;

    @DataField(pos = 16, required = true, clip = true, length = 2, columnName = "JWOD-BUSINESS")
    private String jwodBusiness;

    @DataField(pos = 17, required = true, clip = true, length = 2, columnName = "OTHER-BUSINESS")
    private String otherbusiness;

    @DataField(pos = 18, required = true, clip = true, length = 16, columnName = "CONTRACT-NO")
    private String contractNo;

    @DataField(pos = 19, required = false, clip = true, length = 6, columnName = "CONTRACT-LINE")
    private String contractLine;

    @DataField(pos = 20, required = false, clip = true, length = 1, columnName = "CONTRACT-MANDATORY")
    private String contractMandatory;

    @DataField(pos = 21, required = false, clip = true, length = 10, columnName = "CLIN-TYPE")
    private String clinType;

    @DataField(pos = 22, required = false, clip = true, length = 6, columnName = "CATALOG-PAGE")
    private String catalogPage;

    @DataField(pos = 23, required = false, clip = true, length = 200, columnName = "SUPPLIER-URL")
    private String supplierUrl;

    @DataField(pos = 24, required = true, clip = true, length = 40, columnName = "IMAGE-FILE-NAME")
    private String imageFileName;

    @DataField(pos = 25, required = false, clip = true, length = 30, columnName = "MANUF-NAME")
    private String manufacturerName;

    @DataField(pos = 26, required = false, clip = true, length = 200, columnName = "MANUF-URL")
    private String manufacturerUrl;

    @DataField(pos = 27, required = true, clip = true, length = 1, columnName = "ITEM-STATUS")
    private String itemStatus;

    @DataField(pos = 28, required = false, clip = true, length = 1000, columnName = "NOTES")
    private String notes;

    @DataField(pos = 29, required = true, clip = true, length = 1, columnName = "CHANGE-CD")
    private String changeCd;

    @DataField(pos = 30, required = true, clip = true, length = 8, pattern = "yyyyMMdd", columnName = "EFFECTIVE-DATE")
    private Date effectiveDate;

    @DataField(pos = 31, required = false, clip = true, length = 8, pattern = "yyyyMMdd", columnName = "EXPIRE-DATE")
    private Date expirationDate;

    @DataField(pos = 32, required = false, clip = true, length = 9, columnName = "MIN-LINE-QTY")
    private Integer minLineQty;

    @DataField(pos = 33, required = false, clip = true, length = 9, columnName = "MAX-LINE-QTY")
    private Integer maxLineQty;

    @DataField(pos = 34, required = false, clip = true, length = 5, columnName = "ISSUE-INCREMENT")
    private Integer issueIncrement;

    @DataField(pos = 35, required = false, clip = true, length = 1, columnName = "GREEN-PROD")
    private String greenProd;

    @DataField(pos = 36, required = false, clip = true, length = 1, columnName = "ENERGY-STAR")
    private String energyStar;

    @DataField(pos = 37, required = false, clip = true, length = 1, columnName = "BIO-PREFERRED")
    private String bioPreferred;

    @DataField(pos = 38, required = false, clip = true, length = 1, columnName = "MSDS-REQ")
    private String msdsReq;

    @DataField(pos = 39, required = false, clip = true, length = 1, columnName = "RECYCLED-CONTENT")
    private String recycledContent;

    @DataField(pos = 40, required = false, clip = true, length = 2, columnName = "EPEAT-RATING")
    private String epeatRating;

    @DataField(pos = 41, required = false, clip = true, length = 1, columnName = "NEMA-STD")
    private String nemaStd;

    @DataField(pos = 42, required = false, clip = true, length = 1, columnName = "EPA-WATERSENSE")
    private String epaWatersense;

    @DataField(pos = 43, required = false, clip = true, length = 1, columnName = "USEPA-PRI-CHEM-FREE")
    private String UsepaPriChemFree;

    @DataField(pos = 44, required = false, clip = true, length = 1, columnName = "RENEWABLE-ENERGY-RESOURCE")
    private String renewableEnergyResource;

    /**
     * @return the supplierId
     */
    public String getSupplierId() {
        return supplierId;
    }

    /**
     * @param supplierId
     *            the supplierId to set
     */
    public void setSupplierId(String supplierId) {
        this.supplierId = supplierId;
    }

    /**
     * @return the transactionSetId
     */
    public String getTransactionSetId() {
        return transactionSetId;
    }

    /**
     * @param transactionSetId
     *            the transactionSetId to set
     */
    public void setTransactionSetId(String transactionSetId) {
        this.transactionSetId = transactionSetId;
    }

    /**
     * @return the recordId
     */
    public String getRecordId() {
        return recordId;
    }

    /**
     * @param recordId
     *            the recordId to set
     */
    public void setRecordId(String recordId) {
        this.recordId = recordId;
    }

    /**
     * @return the itemNo
     */
    public String getItemNo() {
        return itemNo;
    }

    /**
     * @param itemNo
     *            the itemNo to set
     */
    public void setItemNo(String itemNo) {
        this.itemNo = itemNo;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description
     *            the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the supplierItemNo
     */
    public String getSupplierItemNo() {
        return supplierItemNo;
    }

    /**
     * @param supplierItemNo
     *            the supplierItemNo to set
     */
    public void setSupplierItemNo(String supplierItemNo) {
        this.supplierItemNo = supplierItemNo;
    }

    /**
     * @return the manufacturerItemNo
     */
    public String getManufacturerItemNo() {
        return manufacturerItemNo;
    }

    /**
     * @param manufacturerItemNo
     *            the manufacturerItemNo to set
     */
    public void setManufacturerItemNo(String manufItemNo) {
        this.manufacturerItemNo = manufItemNo;
    }

    /**
     * @return the unspscCd
     */
    public Integer getUnspscCd() {
        return unspscCd;
    }

    /**
     * @param unspscCd
     *            the unspscCd to set
     */
    public void setUnspscCd(Integer unspscCd) {
        this.unspscCd = unspscCd;
    }

    /**
     * @return the supplierHierarchy
     */
    public String getSupplierHierarchy() {
        return supplierHierarchy;
    }

    /**
     * @param supplierHierarchy
     *            the supplierHierarchy to set
     */
    public void setSupplierHierarchy(String supplierHierarchy) {
        this.supplierHierarchy = supplierHierarchy;
    }

    /**
     * @return the itemPrice
     */
    public Double getItemPrice() {
        return itemPrice;
    }

    /**
     * @param itemPrice
     *            the itemPrice to set
     */
    public void setItemPrice(Double itemPrice) {
        this.itemPrice = itemPrice;
    }

    /**
     * @return the listPrice
     */
    public Double getListPrice() {
        return listPrice;
    }

    /**
     * @param listPrice
     *            the listPrice to set
     */
    public void setListPrice(Double listPrice) {
        this.listPrice = listPrice;
    }

    /**
     * @return the uom
     */
    public String getUom() {
        return uom;
    }

    /**
     * @param uom
     *            the uom to set
     */
    public void setUom(String uom) {
        this.uom = uom;
    }

    /**
     * @return the businessClass
     */
    public String getBusinessClass() {
        return businessClass;
    }

    /**
     * @param businessClass
     *            the businessClass to set
     */
    public void setBusinessClass(String businessClass) {
        this.businessClass = businessClass;
    }

    /**
     * @return the womenOwned
     */
    public String getWomenOwned() {
        return womenOwned;
    }

    /**
     * @param womenOwned
     *            the womenOwned to set
     */
    public void setWomenOwned(String womenOwned) {
        this.womenOwned = womenOwned;
    }

    /**
     * @return the minorityBusiness
     */
    public String getMinorityBusiness() {
        return minorityBusiness;
    }

    /**
     * @param minorityBusiness
     *            the minorityBusiness to set
     */
    public void setMinorityBusiness(String minorityBusiness) {
        this.minorityBusiness = minorityBusiness;
    }

    /**
     * @return the jwodBusiness
     */
    public String getJwodBusiness() {
        return jwodBusiness;
    }

    /**
     * @param jwodBusiness
     *            the jwodBusiness to set
     */
    public void setJwodBusiness(String jwodBusiness) {
        this.jwodBusiness = jwodBusiness;
    }

    /**
     * @return the otherbusiness
     */
    public String getOtherbusiness() {
        return otherbusiness;
    }

    /**
     * @param otherbusiness
     *            the otherbusiness to set
     */
    public void setOtherbusiness(String otherbusiness) {
        this.otherbusiness = otherbusiness;
    }

    /**
     * @return the contractNo
     */
    public String getContractNo() {
        return contractNo;
    }

    /**
     * @param contractNo
     *            the contractNo to set
     */
    public void setContractNo(String contractNo) {
        this.contractNo = contractNo;
    }

    /**
     * @return the contractLine
     */
    public String getContractLine() {
        return contractLine;
    }

    /**
     * @param contractLine
     *            the contractLine to set
     */
    public void setContractLine(String contractLine) {
        this.contractLine = contractLine;
    }

    /**
     * @return the contractMandatory
     */
    public String getContractMandatory() {
        return contractMandatory;
    }

    /**
     * @param contractMandatory
     *            the contractMandatory to set
     */
    public void setContractMandatory(String contractMandatory) {
        this.contractMandatory = contractMandatory;
    }

    /**
     * @return the clinType
     */
    public String getClinType() {
        return clinType;
    }

    /**
     * @param clinType
     *            the clinType to set
     */
    public void setClinType(String clinType) {
        this.clinType = clinType;
    }

    /**
     * @return the catalogPage
     */
    public String getCatalogPage() {
        return catalogPage;
    }

    /**
     * @param catalogPage
     *            the catalogPage to set
     */
    public void setCatalogPage(String catalogPage) {
        this.catalogPage = catalogPage;
    }

    /**
     * @return the supplierUrl
     */
    public String getSupplierUrl() {
        return supplierUrl;
    }

    /**
     * @param supplierUrl
     *            the supplierUrl to set
     */
    public void setSupplierUrl(String supplierUrl) {
        this.supplierUrl = supplierUrl;
    }

    /**
     * @return the imageFileName
     */
    public String getImageFileName() {
        return imageFileName;
    }

    /**
     * @param imageFileName
     *            the imageFileName to set
     */
    public void setImageFileName(String imageFileName) {
        this.imageFileName = imageFileName;
    }

    /**
     * @return the manufacturerName
     */
    public String getManufName() {
        return manufacturerName;
    }

    /**
     * @param manufacturerName
     *            the manufacturerName to set
     */
    public void setManufacturerName(String manufName) {
        this.manufacturerName = manufName;
    }

    /**
     * @return the manufacturerUrl
     */
    public String getManufacturerUrl() {
        return manufacturerUrl;
    }

    /**
     * @param manufacturerUrl
     *            the manufacturerUrl to set
     */
    public void setManufacturerUrl(String manufUrl) {
        this.manufacturerUrl = manufUrl;
    }

    /**
     * @return the itemStatus
     */
    public String getItemStatus() {
        return itemStatus;
    }

    /**
     * @param itemStatus
     *            the itemStatus to set
     */
    public void setItemStatus(String itemStatus) {
        this.itemStatus = itemStatus;
    }

    /**
     * @return the notes
     */
    public String getNotes() {
        return notes;
    }

    /**
     * @param notes
     *            the notes to set
     */
    public void setNotes(String notes) {
        this.notes = notes;
    }

    /**
     * @return the changeCd
     */
    public String getChangeCd() {
        return changeCd;
    }

    /**
     * @param changeCd
     *            the changeCd to set
     */
    public void setChangeCd(String changeCd) {
        this.changeCd = changeCd;
    }

    /**
     * @return the effectiveDate
     */
    public Date getEffectiveDate() {
        return effectiveDate;
    }

    /**
     * @param effectiveDate
     *            the effectiveDate to set
     */
    public void setEffectiveDate(Date effectiveDate) {
        this.effectiveDate = effectiveDate;
    }

    /**
     * @return the expirationDate
     */
    public Date getExpirationDate() {
        return expirationDate;
    }

    /**
     * @param expirationDate
     *            the expirationDate to set
     */
    public void setExpirationDate(Date expirationDate) {
        this.expirationDate = expirationDate;
    }

    /**
     * @return the minLineQty
     */
    public Integer getMinLineQty() {
        return minLineQty;
    }

    /**
     * @param minLineQty
     *            the minLineQty to set
     */
    public void setMinLineQty(Integer minLineQty) {
        this.minLineQty = minLineQty;
    }

    /**
     * @return the maxLineQty
     */
    public Integer getMaxLineQty() {
        return maxLineQty;
    }

    /**
     * @param maxLineQty
     *            the maxLineQty to set
     */
    public void setMaxLineQty(Integer maxLineQty) {
        this.maxLineQty = maxLineQty;
    }

    /**
     * @return the issueIncrement
     */
    public Integer getIssueIncrement() {
        return issueIncrement;
    }

    /**
     * @param issueIncrement
     *            the issueIncrement to set
     */
    public void setIssueIncrement(Integer issueIncrement) {
        this.issueIncrement = issueIncrement;
    }

    /**
     * @return the greenProd
     */
    public String getGreenProd() {
        return greenProd;
    }

    /**
     * @param greenProd
     *            the greenProd to set
     */
    public void setGreenProd(String greenProd) {
        this.greenProd = greenProd;
    }

    /**
     * @return the energyStar
     */
    public String getEnergyStar() {
        return energyStar;
    }

    /**
     * @param energyStar
     *            the energyStar to set
     */
    public void setEnergyStar(String energyStar) {
        this.energyStar = energyStar;
    }

    /**
     * @return the bioPreferred
     */
    public String getBioPreferred() {
        return bioPreferred;
    }

    /**
     * @param bioPreferred
     *            the bioPreferred to set
     */
    public void setBioPreferred(String bioPreferred) {
        this.bioPreferred = bioPreferred;
    }

    /**
     * @return the msdsReq
     */
    public String getMsdsReq() {
        return msdsReq;
    }

    /**
     * @param msdsReq
     *            the msdsReq to set
     */
    public void setMsdsReq(String msdsReq) {
        this.msdsReq = msdsReq;
    }

    /**
     * @return the recycledContent
     */
    public String getRecycledContent() {
        return recycledContent;
    }

    /**
     * @param recycledContent
     *            the recycledContent to set
     */
    public void setRecycledContent(String recycledContent) {
        this.recycledContent = recycledContent;
    }

    /**
     * @return the epeatRating
     */
    public String getEpeatRating() {
        return epeatRating;
    }

    /**
     * @param epeatRating
     *            the epeatRating to set
     */
    public void setEpeatRating(String epeatRating) {
        this.epeatRating = epeatRating;
    }

    /**
     * @return the nemaStd
     */
    public String getNemaStd() {
        return nemaStd;
    }

    /**
     * @param nemaStd
     *            the nemaStd to set
     */
    public void setNemaStd(String nemaStd) {
        this.nemaStd = nemaStd;
    }

    /**
     * @return the epaWatersense
     */
    public String getEpaWatersense() {
        return epaWatersense;
    }

    /**
     * @param epaWatersense
     *            the epaWatersense to set
     */
    public void setEpaWatersense(String epaWatersense) {
        this.epaWatersense = epaWatersense;
    }

    /**
     * @return the usepaPriChemFree
     */
    public String getUsepaPriChemFree() {
        return UsepaPriChemFree;
    }

    /**
     * @param usepaPriChemFree
     *            the usepaPriChemFree to set
     */
    public void setUsepaPriChemFree(String usepaPriChemFree) {
        UsepaPriChemFree = usepaPriChemFree;
    }

    /**
     * @return the renewableEnergyResource
     */
    public String getRenewableEnergyResource() {
        return renewableEnergyResource;
    }

    /**
     * @param renewableEnergyResource
     *            the renewableEnergyResource to set
     */
    public void setRenewableEnergyResource(String renewableEnergyResource) {
        this.renewableEnergyResource = renewableEnergyResource;
    }

}
