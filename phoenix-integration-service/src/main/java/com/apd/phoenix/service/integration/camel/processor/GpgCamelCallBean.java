/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.service.integration.camel.processor;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import org.apache.camel.Exchange;
import org.apache.camel.component.exec.ExecBinding;
import org.apache.camel.component.exec.ExecResult;
import org.apache.commons.io.IOUtils;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.core.utility.TenantConfigRepository;
import com.apd.phoenix.service.persistence.multitenancy.impl.CurrentTenantIdentifierResolverImpl;

/**
 *
 * @author nreidelb
 */
public class GpgCamelCallBean {

    private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(GpgCamelCallBean.class);
    public static final String ENC_PASSWORD = "encPassword";
    public static final String KEY_USER_ID = "keyUserId";
    public static final String TMP_DIRECTORY = "tmpDirectory";
    public static final String PARTNER_ID = "partnerId";

    public void processGpgResult(Exchange exchange) {
        ExecResult execResult = (ExecResult) exchange.getIn().getBody();
        exchange.getOut().setHeaders(exchange.getIn().getHeaders());
        exchange.getOut().setHeader(ENC_PASSWORD, "PASSWORD_CLEARED");
        try {
            if (execResult.getStderr().available() > 0 && execResult.getExitValue() != 0) {
                LOG.error(IOUtils.toString(execResult.getStderr(), "UTF-8"));
            }
            exchange.getOut().setBody(exchange.getIn().getBody(byte[].class));
        }
        catch (IOException ex) {
            LOG.error(ex.getStackTrace().toString());
        }
    }

    public void setGpgParameters(Exchange exchange){
        exchange.getOut().setHeaders(exchange.getIn().getHeaders());
        Long tenantId = CurrentTenantIdentifierResolverImpl.getCurrentTenant();
        String partnerId = (String) exchange.getIn().getHeader(PARTNER_ID);
        Properties tenantProps = TenantConfigRepository.getInstance().getPropertiesByTenantId(tenantId, "tenant");
        String pass = (String) tenantProps.getProperty(partnerId+"_decryption_key_password");
        String user = (String) tenantProps.getProperty(partnerId+"_decryption_key_user_id");
        
        if(pass == null || user == null){
        	String errMsg = "Attempting to decrypt data for tenant/partner combination which is not currently supported! TenantID: " + tenantId + ", PartnerId: "+partnerId;
        	LOG.error(errMsg);
        	throw new UnsupportedOperationException(errMsg);
        }
        
        List<String> gpgParams = new ArrayList<>();
        gpgParams.add("--batch");
        gpgParams.add( "--yes");
        gpgParams.add( "--passphrase");
        gpgParams.add( pass);
        gpgParams.add( "-u");
        gpgParams.add( user);
        gpgParams.add( "--decrypt");
        gpgParams.add( exchange.getIn().getHeader(TMP_DIRECTORY) + "/" + exchange.getIn().getHeader("CamelFileName"));
        exchange.getOut().getHeaders().put(ExecBinding.EXEC_COMMAND_ARGS, gpgParams);
    }

    public void setRmParameters(Exchange exchange){
        exchange.getOut().setHeaders(exchange.getIn().getHeaders());
        List<String> gpgParams = new ArrayList<>();
        gpgParams.add( (String) exchange.getIn().getHeader("CamelFileName"));
        exchange.getOut().getHeaders().put(ExecBinding.EXEC_COMMAND_ARGS, gpgParams);
    }
}
