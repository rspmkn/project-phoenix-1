/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.service.integration.camel.processor;

import java.util.Date;
import com.apd.phoenix.service.business.AccountBp;
import com.apd.phoenix.service.business.CustomerOrderBp;
import com.apd.phoenix.service.business.ShipmentBp;
import com.apd.phoenix.service.message.api.MessageService;
import com.apd.phoenix.service.model.CustomerOrder;
import com.apd.phoenix.service.model.dto.MessageDto;
import com.apd.phoenix.service.model.dto.MessageMetadataDto;
import com.apd.phoenix.service.model.dto.MessageMetadataDto.CommunicationType;
import com.apd.phoenix.service.model.dto.MessageMetadataDto.MessageType;
import com.apd.phoenix.service.model.dto.OrderLogDto.EventTypeDto;
import com.apd.phoenix.service.model.dto.error.EdiDocumentErrorsDto;
import com.apd.phoenix.service.model.dto.error.EdiErrorDto;
import com.apd.phoenix.service.model.dto.error.RelevantNumberDto;
import javax.ejb.Stateless;
import javax.inject.Inject;
import org.apache.camel.Body;
import org.apache.camel.Exchange;
import org.apache.camel.Header;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author nreidelb
 */
@Stateless
public class MessageServiceBean {

    private static final Logger LOG = LoggerFactory.getLogger(MessageServiceBean.class);

    @Inject
    MessageService messageService;

    @Inject
    CustomerOrderBp customerOrderBp;

    @Inject
    ShipmentBp shipmentBp;

    @Inject
    AccountBp accountBp;

    public void receiveMessage(@Header("apdPo") String token, @Header("messageEventType") String messageEventType,
            @Body MessageDto messageDto) {
        messageService.receiveOrderMessage(messageDto, token, messageEventType);
    }

    public void convert824ToOrderLog(Exchange exchange) {
        CustomerOrder customerOrder = null;
        EdiDocumentErrorsDto ediDocumentErrorsDto = (EdiDocumentErrorsDto) exchange.getIn().getBody();
        for (EdiErrorDto error : ediDocumentErrorsDto.getEdiErrorDtos()) {
            for (RelevantNumberDto number : error.getRelevantNumbers()) {
                customerOrder = shipmentBp.findCustomerOrderBy824RelevantNumber(customerOrder, number);
            }
        }
        MessageDto message = new MessageDto();
        String rawMessage = (String) exchange.getIn().getHeader("rawMessage");
        message.setContent(rawMessage);
        LOG.debug("Raw Message: {}", rawMessage);

        MessageMetadataDto metadata = new MessageMetadataDto();
        metadata.setCommunicationType(CommunicationType.INBOUND);
        metadata.setContentLength(rawMessage.length());
        String destination = (String) exchange.getIn().getHeader("CamelFileHost");
        if (destination == null) {
            //TODO: Configure how ftp host is extracted from headers
            destination = "none";
        }
        metadata.setDestination(destination);
        message.setMessageMetadataDto(metadata);
        metadata.setMessageDate(new Date());
        metadata.setMessageType(MessageType.EDI);
        String groupId = (String) exchange.getIn().getHeader("groupId");
        metadata.setGroupId(groupId);
        String interchangeId = (String) exchange.getIn().getHeader("interchangeId");
        metadata.setInterchangeId(interchangeId);
        String senderId = (String) exchange.getIn().getHeader("senderId");
        metadata.setSenderId(senderId);
        String transactionId = (String) exchange.getIn().getHeader("transactionId");
        metadata.setTransactionId(transactionId);

        exchange.getOut().setHeaders(exchange.getIn().getHeaders());
        if (customerOrder != null) {
            String apdPo = customerOrder.getApdPo().getValue();
            LOG.debug("po number: {}", apdPo);
            metadata.setFilePath(apdPo + "/" + apdPo + "-edi824-" + System.currentTimeMillis() / 1000 + ".txt");
            exchange.getOut().setHeader("apdPo", apdPo);
        }
        exchange.getOut().setBody(message);
        exchange.getOut().setHeader("messageEventType", EventTypeDto.PARTNER_REPORTED_ERRORS.name());

    }
}
