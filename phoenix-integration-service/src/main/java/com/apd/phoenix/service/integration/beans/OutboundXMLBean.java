package com.apd.phoenix.service.integration.beans;

import java.util.Properties;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.camel.component.ActiveMQComponent;
import org.apache.activemq.pool.PooledConnectionFactory;
import org.apache.camel.CamelContext;
import org.apache.camel.component.cdi.CdiCamelContext;
import org.apache.camel.component.jms.JmsConfiguration;
import com.apd.phoenix.service.integration.routes.JumpTrackManifestRouteBuilder;
import com.apd.phoenix.service.integration.routes.JumpTrackStopNotificationRequestBuilder;
import com.apd.phoenix.service.integration.utility.PropertiesLoader;

@Startup
@Singleton
public class OutboundXMLBean {

    @Inject
    private CdiCamelContext camelContext;

    @PostConstruct
    public void init() throws Exception {
        setup();
    }

    @PreDestroy
    public void destroy() throws Exception {
        camelContext.stop();
    }

    public void setup() throws Exception {

        Properties commonProperties = PropertiesLoader.getAsProperties("integration");
        Properties jumptrackProperties = PropertiesLoader.getAsProperties("jumptrack.integration");

        setupAmq(camelContext, commonProperties.getProperty("jmsBrokerUrl"));

        //Jumptrack manifest
        JumpTrackManifestRouteBuilder jumpTrackManifestRouteBuilder = new JumpTrackManifestRouteBuilder();
        jumpTrackManifestRouteBuilder.setSource(jumptrackProperties.getProperty("jumptrackManifestSource"));
        jumpTrackManifestRouteBuilder.setDlq(jumptrackProperties.getProperty("jumptrackManifestDlq"));
        jumpTrackManifestRouteBuilder
                .setJumptrackEndpoint(jumptrackProperties.getProperty("jumptrackManifestEndpoint"));

        camelContext.addRoutes(jumpTrackManifestRouteBuilder);

        //Jumptrack Stop Notice
        JumpTrackStopNotificationRequestBuilder jumpTrackStopNotificationRequestBuilder = new JumpTrackStopNotificationRequestBuilder();
        jumpTrackStopNotificationRequestBuilder.setSource(jumptrackProperties.getProperty("jumptrackStopNoticeSource"));
        jumpTrackStopNotificationRequestBuilder.setDlq(jumptrackProperties.getProperty("jumptrackStopNoticeDlq"));
        jumpTrackStopNotificationRequestBuilder.setJumptrackEndpoint(jumptrackProperties
                .getProperty("jumptrackStopNoticeEndpoint"));

        camelContext.addRoutes(jumpTrackStopNotificationRequestBuilder);

        camelContext.start();
    }

    public void setupAmq(CamelContext context, String jmsBrokerUrl) {
        Properties amqProperties = PropertiesLoader.getAsProperties("amq");
        ActiveMQConnectionFactory activeMQConnectionFactory = new ActiveMQConnectionFactory();
        activeMQConnectionFactory.setBrokerURL(jmsBrokerUrl);
        PooledConnectionFactory pooledConnectionFactory = new PooledConnectionFactory();
        pooledConnectionFactory.setConnectionFactory(activeMQConnectionFactory);
        pooledConnectionFactory.setMaxConnections(Integer.parseInt(amqProperties.getProperty(getClass().getSimpleName()
                + ".maxConnections", "2")));
        JmsConfiguration jmsConfiguration = new JmsConfiguration();
        jmsConfiguration.setConnectionFactory(pooledConnectionFactory);
        jmsConfiguration.setConcurrentConsumers(Integer.parseInt(amqProperties.getProperty(getClass().getSimpleName()
                + ".concurrentConsumers", "2")));
        ActiveMQComponent activeMQComponent = new ActiveMQComponent();
        activeMQComponent.setConfiguration(jmsConfiguration);
        context.addComponent("activemq", activeMQComponent);
    }

}
