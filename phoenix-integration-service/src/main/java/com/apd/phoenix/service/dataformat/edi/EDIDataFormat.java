package com.apd.phoenix.service.dataformat.edi;

import java.io.InputStream;
import java.io.OutputStream;
import java.io.StringReader;
import java.io.StringWriter;
import javax.edi.bind.EDIMarshaller;
import javax.edi.bind.EDIUnmarshaller;
import org.apache.camel.Exchange;
import org.apache.camel.spi.DataFormat;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class EDIDataFormat implements DataFormat {

    private static final Logger LOG = LoggerFactory.getLogger(EDIDataFormat.class);

    private Class<?> classType;

    public EDIDataFormat(Class<?> classType) {
        this.setClassType(classType);
    }

    @Override
    public void marshal(Exchange exchange, Object graph, OutputStream stream) throws Exception {
        StringWriter sw = new StringWriter();
        EDIMarshaller.marshal(graph, sw);
        stream.write(sw.toString().getBytes());
    }

    @Override
    public Object unmarshal(Exchange exchange, InputStream stream) throws Exception {

        StringWriter writer = new StringWriter();
        IOUtils.copy(stream, writer);
        String rawMessage = writer.toString();
        exchange.getOut().setHeader("rawMessage", rawMessage);

        Object edi = EDIUnmarshaller.unmarshal(classType, new StringReader(rawMessage));

        return edi;
    }

    public Class<?> getClassType() {
        return classType;
    }

    public void setClassType(Class<?> classType) {
        this.classType = classType;
    }

}
