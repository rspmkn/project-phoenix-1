/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.service.integration.camel.processor;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.edi.model.x12.edi816.segment.OrgRelationshipsBody;
import javax.edi.model.x12.edi816.segment.OrgRelationshipsGroupChild;
import javax.edi.model.x12.edi816.segment.OrgRelationshipsGroupParent;
import javax.edi.model.x12.edi816.segment.OrgRelationshipsIdentificationGroup;
import javax.edi.model.x12.segment.AddressInformation;
import javax.edi.model.x12.segment.GeographicLocation;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.model.dto.AddressDto;
import com.apd.phoenix.service.model.dto.MiscShipToDto;
import com.apd.phoenix.service.model.dto.AddressUpdateDto;
import javax.edi.model.x12.segment.Name;

/**
 * 
 * 
 */
public class Edi816Translator implements Processor {

    private static final Logger LOG = LoggerFactory.getLogger(Edi816Translator.class);
    public static final String APD_PO_TYPE = "APD";

    @Override
    public void process(Exchange exchange) throws Exception {
        String rawMessage = (String) exchange.getIn().getHeader("rawMessage");
        LOG.debug("Raw Message: {}", rawMessage);

        OrgRelationshipsBody body = (OrgRelationshipsBody) exchange.getIn().getBody();
        AddressUpdateDto orgDto = createOrgRelationsDto816(body);
        exchange.getOut().setBody(orgDto, AddressUpdateDto.class);
    }

    private AddressUpdateDto createOrgRelationsDto816(OrgRelationshipsBody body) {
        AddressUpdateDto orgRelDto = new AddressUpdateDto();
        List<AddressDto> addressDto = createAddressDto(body);
        orgRelDto.setAddresses(addressDto);
        orgRelDto.setCompanyName(body.getHeader().getName().toArray(new Name[10])[0].getName());
        return orgRelDto;
    }

    private List<AddressDto> createAddressDto(OrgRelationshipsBody body) {
                Collection<OrgRelationshipsGroupParent> parents = body.getDetail().getOrgRel();
		List<AddressDto> addrs = new ArrayList<>();
		/*AddressDto addressDto = null;
		MiscShipToDto shipToDto = null;*/
		Iterable<OrgRelationshipsIdentificationGroup> identificationGroups = null;

		for (OrgRelationshipsGroupParent parentGroup : parents) {
                    addrs = addAddressesForPartOfTree(parentGroup, body, addrs);					
		}
		return addrs;

	}

    private AddressDto createAddressHelper(Iterable<OrgRelationshipsIdentificationGroup> identificationGroups,
            OrgRelationshipsBody body) {

        AddressDto addressDto = null;
        MiscShipToDto shipToDto = null;

        addressDto = new AddressDto();
        shipToDto = new MiscShipToDto();
        // GLN and other data - N1
        for (OrgRelationshipsIdentificationGroup identGroup : identificationGroups) {
            shipToDto.setGlnID(identGroup.getName().getIdentificationCode());
            shipToDto.setDeliverToName(identGroup.getName().getName());
            shipToDto.setLocationId(identGroup.getName().getEntityIdentifierCode());
            GeographicLocation location = identGroup.getGeoLocation();
            if (location != null) {
                addressDto.setCountry(location.getCountryCode());
                addressDto.setCity(location.getCityName());
                addressDto.setState(location.getStateOrProvinceCode());
                addressDto.setZip(location.getPostalCode());
                // N3
                AddressInformation addrInfo = identGroup.getAddrInfo();
                if (addrInfo != null) {
                    addressDto.setLine1(addrInfo.getAddressLine1());
                    addressDto.setLine2(addrInfo.getAddressLine2());
                }
            }
            addressDto.setMiscShipToDto(shipToDto);

        }

        return addressDto;
    }

    private List<AddressDto> addAddressesForPartOfTree(OrgRelationshipsGroupParent parentGroup,
            OrgRelationshipsBody body, List<AddressDto> addrs) {
        Iterable<OrgRelationshipsIdentificationGroup> identificationGroups;
        identificationGroups = parentGroup.getOrgRelationshipsIdentificationGroup();
        AddressDto parentDto = createAddressHelper(identificationGroups, body);
        addrs.add(parentDto);
        String levelParentCode = parentGroup.getLevel().getHierarchicalIDNumber(); // parent ID number
        Iterable<OrgRelationshipsGroupParent> children = parentGroup.getOrgRelChild();
        // children
        if (children != null) {
            for (OrgRelationshipsGroupParent childGroup : children) {
                String levelChildCode = childGroup.getLevel().getHierarchicalParentIDNumber(); // ID number for parent in child record
                if ((levelParentCode != null && levelChildCode != null) && (levelParentCode.equals(levelChildCode))) { // child record
                    identificationGroups = childGroup.getOrgRelationshipsIdentificationGroup();
                    AddressDto childDto = createAddressHelper(identificationGroups, body);
                    addrs.add(childDto);
                }
                else {
                    LOG.error("Broken parent/child relationship for Inbound EDI 816.");
                }
            }
        }
        return addrs;
    }
}
