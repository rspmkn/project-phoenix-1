package com.apd.phoenix.service.integration.camel.processor;

import java.io.IOException;
import java.util.List;
import java.util.Properties;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.mail.MessagingException;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import org.apache.camel.Body;
import org.apache.camel.Header;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.core.utility.PropertiesLoader;
import com.apd.phoenix.service.business.CustomerOrderBp;
import com.apd.phoenix.service.business.EmailFactoryBp;
import com.apd.phoenix.service.business.OrderLogBp;
import com.apd.phoenix.service.email.api.EmailService;
import com.apd.phoenix.service.message.api.Message;
import com.apd.phoenix.service.message.api.MessageUtils;
import com.apd.phoenix.service.model.CustomerOrder;
import com.apd.phoenix.service.model.OrderLog;
import com.apd.phoenix.service.model.OrderLog.EventType;
import com.apd.phoenix.service.model.OrderStatus.OrderStatusEnum;
import com.apd.phoenix.service.model.dto.CardNotificationDto;
import com.apd.phoenix.service.model.dto.CustomerCreditInvoiceDto;
import com.apd.phoenix.service.model.dto.CustomerInvoiceDto;
import com.apd.phoenix.service.model.dto.POAcknowledgementDto;
import com.apd.phoenix.service.model.dto.PurchaseOrderDto;
import com.apd.phoenix.service.model.dto.ShipmentDto;

@Stateless
public class EmailMessageSenderEndpoint {

    private static final Logger LOGGER = LoggerFactory.getLogger(EmailMessageSenderEndpoint.class);

    @Inject
    EmailService emailService;

    @Inject
    EmailFactoryBp emailFactoryBp;

    @Inject
    CustomerOrderBp customerOrderBp;

    @Inject
    private MessageUtils messageUtils;

    @Inject
    private OrderLogBp orderLogBp;

    public void sendPurchaseOrderEmail(@Body PurchaseOrderDto purchaseOrderDto) {
        LOGGER.info("Sending Email Purchase Order");

        CustomerOrder customerOrder = customerOrderBp.searchByApdPo(purchaseOrderDto.getApdPoNumber());
        if (customerOrder == null) {
            LOGGER.error("No customer order found for APD po: {}", purchaseOrderDto.getApdPoNumber());
        }

        Message message = emailFactoryBp.createPurchaseOrderEmail(customerOrder, purchaseOrderDto);

        sendMessage(message, EventType.PURCHASE_ORDER_SENT);
        logMessage(customerOrder, message, EventType.PURCHASE_ORDER_SENT);
        return;
    }

    public void sendOrderEmail(@Body POAcknowledgementDto poAcknowledgementDto) {

        EventType type = EventType.ORDER_ACKNOWLEDGEMENT;
        CustomerOrder customerOrder = customerOrderBp.searchByApdPo(poAcknowledgementDto.getApdPo());

        List<Message> messages = emailFactoryBp.createOrderEmails(customerOrder.getId(), poAcknowledgementDto, type);
        Message toSend = null;
        for (Message message : messages) {
            toSend = message;
            sendMessage(message, type);
        }
        logMessage(customerOrder, toSend, type);
    }

    public void sendShipmentNoticeEmail(@Body ShipmentDto shipmentDto) {

        CustomerOrder customerOrder = customerOrderBp.searchByApdPo(shipmentDto.getCustomerOrderDto().getApdPoNumber());
        List<Message> messages = emailFactoryBp.createOrderEmails(customerOrder.getId(), shipmentDto,
                EventType.SHIPMENT_NOTICE);
        Message toSend = null;
        for (Message message : messages) {
            toSend = message;
            sendMessage(message, EventType.SHIPMENT_NOTICE);
        }
        logMessage(customerOrder, toSend, EventType.SHIPMENT_NOTICE);
    }

    private boolean sendMessage(Message message, EventType eventType) {
        if (message != null && message.getContent() != null && message.getContent().markSupported()) {
            try {
                LOGGER.info("Reseting input stream since mark supported");
                message.getContent().reset();
            }
            catch (IOException e) {
                LOGGER.error("Could not retrieve value 'markSupported' from Message", e);
                return false;
            }
        }
        else if (message != null) {
            LOGGER.info("retrieving message from storage since mark is unsupported");
            message = messageUtils.retrieveMessage(message.getMetadata());
        }

        if (message == null || message.getMetadata() == null || message.getContent() == null) {
            LOGGER.error("Message data was null! " + eventType);
            return false;
        }

        try {
            MimeMessage emailMessage = new MimeMessage(null, message.getContent());
            this.applyOverrides(emailMessage);
            emailService.sendEmail(emailMessage);
        }
        catch (Exception e) {
            LOGGER.error("Could not send email", e);
        }
        return true;
    }

    private void applyOverrides(MimeMessage message) {

        Properties emailOverride = PropertiesLoader.getAsProperties("email.override");

        if (emailOverride.getProperty("override", "true").equalsIgnoreCase("true")) {
            String[] destinationList = emailOverride.getProperty("email", "").split(",");
            InternetAddress[] addresses = new InternetAddress[destinationList.length];
            for (int i = 0; i < destinationList.length; i++) {
                try {
                    InternetAddress address = new InternetAddress(destinationList[i]);
                    addresses[i] = address;
                }
                catch (AddressException e) {
                    LOGGER.error("Could not parse email address", e);
                }
            }
            try {
                message.setRecipients(javax.mail.Message.RecipientType.TO, addresses);
                message.setRecipients(javax.mail.Message.RecipientType.CC, new InternetAddress[0]);
                message.setRecipients(javax.mail.Message.RecipientType.BCC, new InternetAddress[0]);
            }
            catch (MessagingException e) {
                LOGGER.error("Error setting override email", e);
            }
        }

        return;
    }

    public void sendInvoiceEmail(@Header("resend") Boolean resend, @Body CustomerInvoiceDto customerInvoiceDto) {

        CustomerOrder customerOrder = customerOrderBp.searchByApdPo(customerInvoiceDto.getShipment()
                .getCustomerOrderDto().getApdPoNumber());
        EventType eventType;
        if (resend != null && resend) {
            eventType = EventType.CUSTOMER_INVOICED_RESEND;
        }
        else {
            eventType = EventType.CUSTOMER_INVOICED;
        }
        List<Message> messages = emailFactoryBp.createOrderEmails(customerOrder.getId(), customerInvoiceDto, eventType);
        Message toSend = null;
        for (Message message : messages) {
            toSend = message;
            sendMessage(message, eventType);
        }
        logMessage(customerOrder, toSend, eventType);
    }

    public void sendCreditInvoiceEmail(@Header("resend") Boolean resend,
            @Body CustomerCreditInvoiceDto customerCreditInvoiceDto) {

        CustomerOrder customerOrder = customerOrderBp.searchByApdPo(customerCreditInvoiceDto.getReturnOrderDto()
                .getOrder().getApdPoNumber());
        EventType messageType;
        if (resend) {
            messageType = EventType.CUSTOMER_CREDIT_INVOICED_RESEND;
        }
        else {
            messageType = EventType.CUSTOMER_CREDIT_INVOICED;
        }
        List<Message> messages = emailFactoryBp.createOrderEmails(customerOrder.getId(), customerCreditInvoiceDto,
                messageType);
        Message toSend = null;
        for (Message message : messages) {
            toSend = message;
            sendMessage(message, messageType);
        }
        logMessage(customerOrder, toSend, messageType);
    }

    public void sendCardNotificationEmail(@Body CardNotificationDto cardNotificationDto) {

        CustomerOrder customerOrder = customerOrderBp.findById(cardNotificationDto.getOrderId(), CustomerOrder.class);
        List<Message> messages = emailFactoryBp.createOrderEmails(cardNotificationDto.getOrderId(),
                cardNotificationDto, orderLogBp.getFromDto(cardNotificationDto.getEventType().getLabel()));
        Message toSend = null;
        for (Message message : messages) {
            toSend = message;
            sendMessage(message, orderLogBp.getFromDto(cardNotificationDto.getEventType().getLabel()));
        }
        logMessage(customerOrder, toSend, orderLogBp.getFromDto(cardNotificationDto.getEventType().getLabel()));
    }

    //TODO: this is very WET code (opposite of DRY). This is copied from MessageServiceImpl. It should be 
    //refactored into part of the email camel route, like CSVTransactionLogger.
    @Deprecated
    private CustomerOrder logMessage(CustomerOrder customerOrder, Message message, EventType eventType) {
        if (customerOrder == null) {
            return null;
        }
        if (message == null) {
            return customerOrder;
        }
        OrderLog orderLog = new OrderLog();
        orderLog.setVersion(0);
        orderLog.setMessageMetadata(message.getMetadata());
        orderLog.setCustomerOrder(customerOrder);
        orderLog.setEventType(eventType);
        return customerOrderBp.addLog(customerOrder, orderLog);
    }

    public void sendRequestForQuoteEmail(@Header("resend") Boolean resend,
            @Body POAcknowledgementDto poAcknowledgementDto) {

        CustomerOrder customerOrder = customerOrderBp.searchByApdPo(poAcknowledgementDto.getApdPo());
        EventType messageType;
        if (resend) {
            messageType = EventType.RFQ_SUBMITTED_RESEND;
        }
        else {
            messageType = EventType.RFQ_SUBMITTED;
        }

        List<Message> messages = emailFactoryBp.createRequestForQuoteEmails(customerOrder.getId(),
                poAcknowledgementDto, messageType);
        Message toSend = null;
        for (Message message : messages) {
            toSend = message;
            sendMessage(message, messageType);
        }
        logMessage(customerOrder, toSend, messageType);
    }

    public void sendProformaInvoiceEmail(@Header("resend") Boolean resend,
            @Body POAcknowledgementDto poAcknowledgementDto) {

        CustomerOrder customerOrder = customerOrderBp.searchByApdPo(poAcknowledgementDto.getApdPo());
        EventType messageType;
        if (resend) {
            messageType = EventType.PROFORMA_INVOICE_RESEND;
        }
        else {
            messageType = EventType.PROFORMA_INVOICE;
        }
        List<Message> messages = emailFactoryBp.createProformaInvoiceEmails(customerOrder.getId(),
                poAcknowledgementDto, messageType);
        Message toSend = null;
        for (Message message : messages) {
            toSend = message;
            sendMessage(message, messageType);
        }
        logMessage(customerOrder, toSend, messageType);
    }
}
