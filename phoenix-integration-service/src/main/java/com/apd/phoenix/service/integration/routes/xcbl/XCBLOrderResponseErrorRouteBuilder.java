package com.apd.phoenix.service.integration.routes.xcbl;

import org.apache.camel.LoggingLevel;
import com.apd.phoenix.service.integration.multitenancy.AbstractTenantAwareRouteBuilder;

/**
 * Note:
 * - The class resolved from outboundOrderResponseTranslatorClass MUST implement com.apd.phoenix.service.integration.camel.processor.MessageTranslator
 * - The class resolved from outboundOrderResponseDataFormatClass MUST yield a valid JAXBContext when passed into javax.xml.bind.JAXBContext.newInstance
 * - The class resolved from inboundMessageAckDataFormatClass MUST yield a valid JAXBContext when passed into javax.xml.bind.JAXBContext.newInstance
 * - The class resolved from inboundOrderDataFormatClass MUST yield a valid JAXBContext when passed into javax.xml.bind.JAXBContext.newInstance
 * @author RH
 *
 */
public class XCBLOrderResponseErrorRouteBuilder extends AbstractTenantAwareRouteBuilder {

    private String outboundOrderErrorSource;
    private String outputDLQ;

    @Override
    public void configureRouteImplementation() throws Exception {

        //Receive DTO
        from(outboundOrderErrorSource).log(LoggingLevel.ERROR, "Failure sending POR")
        //Setup DLQ
                .errorHandler(
                        deadLetterChannel(outputDLQ).maximumRedeliveries(2).redeliveryDelay(2000)
                                .useExponentialBackOff().backOffMultiplier(2)).setHeader("apdPo").jxpath(
                        "/in/body/apdPo").log(LoggingLevel.ERROR, "POR failure for order ${in.header.apdPo}").to(
                        outputDLQ);
    }

    public String getOutboundOrderErrorSource() {
        return outboundOrderErrorSource;
    }

    public void setOutboundOrderErrorSource(String outboundOrderErrorSource) {
        this.outboundOrderErrorSource = outboundOrderErrorSource;
    }

    public String getOutputDLQ() {
        return outputDLQ;
    }

    public void setOutputDLQ(String outputDLQ) {
        this.outputDLQ = outputDLQ;
    }
}
