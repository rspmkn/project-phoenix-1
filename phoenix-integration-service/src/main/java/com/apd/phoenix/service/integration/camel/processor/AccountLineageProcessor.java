package com.apd.phoenix.service.integration.camel.processor;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;
import org.apache.camel.Body;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.business.AccountBp;
import com.apd.phoenix.service.model.Account;
import com.apd.phoenix.service.model.AccountLineage;

@Stateless
public class AccountLineageProcessor {

    @Inject
    private AccountBp accountBp;

    private static final Logger LOGGER = LoggerFactory.getLogger(AccountLineageProcessor.class);

    public void process(@Body Long accountId) {
    	LOGGER.info("Generating lineage for account " + accountId + " and its descendants");
    	List<Account> ancestors = new ArrayList<>();
    	Account movedAccount = accountBp.findById(accountId, Account.class);
    	Account thisAccount = movedAccount;
    	while (thisAccount.getParentAccount() != null) {
    		ancestors.add(thisAccount.getParentAccount());
    		thisAccount = thisAccount.getParentAccount();
    	}
    	movedAccount.getLineage().removeAll(movedAccount.getLineage());
    	if (!ancestors.isEmpty()) {
    		for (int i = 0; i < ancestors.size(); i++) {
    			Account ancestor = ancestors.get(i);
    			AccountLineage newLineage = new AccountLineage();
    			newLineage.setAccount(movedAccount);
    			newLineage.setAncestorAccount(ancestor);
    			//priority is size minus index, because ancestors were added
    			//to the list in reverse order. Root account should have 
    			//priority 1, but it was added last, so its index is size() - 1,
    			//etc.
    			newLineage.setPriority(ancestors.size() - i);
    			movedAccount.getLineage().add(newLineage);
    		}
    	}
    	movedAccount = this.accountBp.update(movedAccount);
    	this.updateChildrenOfAccount(movedAccount);
    }

    private void updateChildrenOfAccount(Account parent) {
        Collection<Account> childAccounts = this.accountBp.getChildren(parent);
        if (childAccounts == null || childAccounts.isEmpty()) {
            return;
        }
        for (Account childAccount : childAccounts) {
            childAccount.getLineage().removeAll(childAccount.getLineage());
            for (AccountLineage parentLineage : parent.getLineage()) {
                AccountLineage newLineage = new AccountLineage();
                newLineage.setPriority(parentLineage.getPriority());
                newLineage.setAncestorAccount(parentLineage.getAncestorAccount());
                newLineage.setAccount(childAccount);
                childAccount.getLineage().add(newLineage);
            }
            AccountLineage newLineage = new AccountLineage();
            newLineage.setPriority(childAccount.getLineage().size() + 1);
            newLineage.setAccount(childAccount);
            newLineage.setAccount(parent);
            childAccount.getLineage().add(newLineage);
            childAccount = this.accountBp.update(childAccount);
            this.updateChildrenOfAccount(childAccount);
        }
    }

}
