package com.apd.phoenix.service.integration.camel.processor;

import java.util.Date;
import javax.edi.model.x12.edi855.segment.POAcknowledgementBody;
import javax.edi.model.x12.edi856.segment.OrderInformationGroup;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.model.dto.MessageDto;
import com.apd.phoenix.service.model.dto.MessageMetadataDto;
import com.apd.phoenix.service.model.dto.MessageMetadataDto.CommunicationType;
import com.apd.phoenix.service.model.dto.MessageMetadataDto.MessageType;
import com.apd.phoenix.service.model.dto.OrderLogDto.EventTypeDto;

public class EDI856toOrderLogTranslator implements Processor {

    private static final Logger LOG = LoggerFactory.getLogger(EDI856toOrderLogTranslator.class);

    @Override
    public void process(Exchange exchange) throws Exception {
        String rawMessage = (String) exchange.getIn().getHeader("rawMessage");
        LOG.debug("Raw Message: {}", rawMessage);

        String destination = (String) exchange.getIn().getHeader("CamelFileHost");
        if (destination == null) {
            //TODO: Configure how ftp host is extracted from headers
            destination = "none";
        }

        OrderInformationGroup body = (OrderInformationGroup) exchange.getIn().getBody();
        String apdPo = body.getPurchaseOrderReference().getPurchaseOrderNumber();
        LOG.debug("po number: {}", apdPo);

        MessageMetadataDto metadata = new MessageMetadataDto();
        metadata.setCommunicationType(CommunicationType.INBOUND);
        metadata.setContentLength(rawMessage.length());
        metadata.setDestination(destination);
        metadata.setFilePath(apdPo + "/" + apdPo + "-edi856-" + System.currentTimeMillis() / 1000 + ".txt");
        metadata.setMessageDate(new Date());
        metadata.setMessageType(MessageType.EDI);
        String groupId = (String) exchange.getIn().getHeader("groupId");
        metadata.setGroupId(groupId);
        String interchangeId = (String) exchange.getIn().getHeader("interchangeId");
        metadata.setInterchangeId(interchangeId);
        String senderId = (String) exchange.getIn().getHeader("senderId");
        metadata.setSenderId(senderId);
        String transactionId = (String) exchange.getIn().getHeader("transactionId");
        metadata.setTransactionId(transactionId);

        MessageDto message = new MessageDto();
        message.setMessageMetadataDto(metadata);
        message.setContent(rawMessage);
        exchange.getOut().setBody(message);
        exchange.getOut().setHeaders(exchange.getIn().getHeaders());
        exchange.getOut().setHeader("apdPo", apdPo);
        exchange.getOut().setHeader("messageEventType", EventTypeDto.ADVANCED_SHIPMENT_NOTICE);
    }

}
