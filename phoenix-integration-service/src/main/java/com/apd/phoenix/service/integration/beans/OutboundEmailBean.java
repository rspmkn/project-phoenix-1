package com.apd.phoenix.service.integration.beans;

import java.util.Properties;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.camel.component.ActiveMQComponent;
import org.apache.activemq.pool.PooledConnectionFactory;
import org.apache.camel.CamelContext;
import org.apache.camel.component.cdi.CdiCamelContext;
import org.apache.camel.component.jms.JmsConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.core.utility.TenantConfigRepository;
import com.apd.phoenix.service.integration.camel.processor.EmailMessageSenderEndpoint;
import com.apd.phoenix.service.integration.routes.MessageBeanRouteBuilder;
import com.apd.phoenix.service.integration.utility.PropertiesLoader;

@Startup
@Singleton
public class OutboundEmailBean {

    private static final Logger LOG = LoggerFactory.getLogger(OutboundEmailBean.class);

    @Inject
    private CdiCamelContext camelContext;

    @PostConstruct
    public void init() throws Exception {
        setup();
    }

    @PreDestroy
    public void destroy() throws Exception {
        camelContext.stop();
    }

    public void setup() throws Exception {
        Properties commonProperties = TenantConfigRepository.getInstance().getProperties("email.integration");

        setupAmq(camelContext, commonProperties.getProperty("jmsBrokerUrl"));

        //Orders
        MessageBeanRouteBuilder emailOrderRouteBuilder = new MessageBeanRouteBuilder();
        emailOrderRouteBuilder.setSource(commonProperties.getProperty("emailOrderSource"));
        emailOrderRouteBuilder.setClazz(EmailMessageSenderEndpoint.class);
        emailOrderRouteBuilder.setMethod("sendOrderEmail");

        //Invoice
        MessageBeanRouteBuilder emailInvoiceRouteBuilder = new MessageBeanRouteBuilder();
        emailInvoiceRouteBuilder.setSource(commonProperties.getProperty("emailInvoiceSource"));
        emailInvoiceRouteBuilder.setClazz(EmailMessageSenderEndpoint.class);
        emailInvoiceRouteBuilder.setMethod("sendInvoiceEmail");

        //Credit Invoice
        MessageBeanRouteBuilder emailCreditInvoiceRouteBuilder = new MessageBeanRouteBuilder();
        emailCreditInvoiceRouteBuilder.setSource(commonProperties.getProperty("emailCreditInvoiceSource"));
        emailCreditInvoiceRouteBuilder.setClazz(EmailMessageSenderEndpoint.class);
        emailCreditInvoiceRouteBuilder.setMethod("sendCreditInvoiceEmail");

        //PurchaseOrder
        MessageBeanRouteBuilder emailPurchaseOrderRouteBuilder = new MessageBeanRouteBuilder();
        emailPurchaseOrderRouteBuilder.setSource(commonProperties.getProperty("emailPurchaseOrderSource"));
        emailPurchaseOrderRouteBuilder.setClazz(EmailMessageSenderEndpoint.class);
        emailPurchaseOrderRouteBuilder.setMethod("sendPurchaseOrderEmail");

        //Shipment Notice
        MessageBeanRouteBuilder emailShipmentNoticeRouteBuilder = new MessageBeanRouteBuilder();
        emailShipmentNoticeRouteBuilder.setSource(commonProperties.getProperty("emailShipmentNoticeSource"));
        emailShipmentNoticeRouteBuilder.setClazz(EmailMessageSenderEndpoint.class);
        emailShipmentNoticeRouteBuilder.setMethod("sendShipmentNoticeEmail");

        //Card Notifications
        MessageBeanRouteBuilder emailCardNotificationRouteBuilder = new MessageBeanRouteBuilder();
        emailCardNotificationRouteBuilder.setSource(commonProperties.getProperty("emailCardNotificationSource"));
        emailCardNotificationRouteBuilder.setClazz(EmailMessageSenderEndpoint.class);
        emailCardNotificationRouteBuilder.setMethod("sendCardNotificationEmail");

        //Request For Quote Notifications
        MessageBeanRouteBuilder emailRfqNotificationRouteBuilder = new MessageBeanRouteBuilder();
        emailRfqNotificationRouteBuilder.setSource(commonProperties.getProperty("emailRequestForQuoteSource"));
        emailRfqNotificationRouteBuilder.setClazz(EmailMessageSenderEndpoint.class);
        emailRfqNotificationRouteBuilder.setMethod("sendRequestForQuoteEmail");

        //Proforma Invoice Notifications
        MessageBeanRouteBuilder emailProformaInvoiceRouteBuilder = new MessageBeanRouteBuilder();
        emailProformaInvoiceRouteBuilder.setSource(commonProperties.getProperty("emailProformaInvoiceSource"));
        emailProformaInvoiceRouteBuilder.setClazz(EmailMessageSenderEndpoint.class);
        emailProformaInvoiceRouteBuilder.setMethod("sendProformaInvoiceEmail");

        camelContext.addRoutes(emailPurchaseOrderRouteBuilder);
        camelContext.addRoutes(emailCreditInvoiceRouteBuilder);
        camelContext.addRoutes(emailInvoiceRouteBuilder);
        camelContext.addRoutes(emailOrderRouteBuilder);
        camelContext.addRoutes(emailShipmentNoticeRouteBuilder);
        camelContext.addRoutes(emailCardNotificationRouteBuilder);
        camelContext.addRoutes(emailRfqNotificationRouteBuilder);
        camelContext.addRoutes(emailProformaInvoiceRouteBuilder);
        camelContext.start();
    }

    public void setupAmq(CamelContext context, String jmsBrokerUrl) {
        Properties amqProperties = PropertiesLoader.getAsProperties("amq");
        ActiveMQConnectionFactory activeMQConnectionFactory = new ActiveMQConnectionFactory();
        activeMQConnectionFactory.setBrokerURL(jmsBrokerUrl);
        PooledConnectionFactory pooledConnectionFactory = new PooledConnectionFactory();
        pooledConnectionFactory.setConnectionFactory(activeMQConnectionFactory);
        pooledConnectionFactory.setMaxConnections(Integer.parseInt(amqProperties.getProperty(getClass().getSimpleName()
                + ".maxConnections", "2")));
        JmsConfiguration jmsConfiguration = new JmsConfiguration();
        jmsConfiguration.setConnectionFactory(pooledConnectionFactory);
        jmsConfiguration.setConcurrentConsumers(Integer.parseInt(amqProperties.getProperty(getClass().getSimpleName()
                + ".concurrentConsumers", "2")));
        ActiveMQComponent activeMQComponent = new ActiveMQComponent();
        activeMQComponent.setConfiguration(jmsConfiguration);
        context.addComponent("activemq", activeMQComponent);
    }

}
