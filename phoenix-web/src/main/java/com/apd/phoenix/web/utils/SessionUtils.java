package com.apd.phoenix.web.utils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class SessionUtils {

    public static void invalidateSessionIfValid(HttpServletRequest httpRequest, HttpSession httpSession) {
        if (httpRequest.getRequestedSessionId() != null && httpRequest.isRequestedSessionIdValid()) {
            httpSession.invalidate();
        }
    }

}
