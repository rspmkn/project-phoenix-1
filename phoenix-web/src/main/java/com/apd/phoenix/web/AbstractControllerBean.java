package com.apd.phoenix.web;

import java.util.List;
import java.util.Map;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import org.apache.commons.lang.StringUtils;
import com.apd.phoenix.service.persistence.jpa.Dao;
import com.apd.phoenix.service.utility.JSFUtils;

@SuppressWarnings("static-method")
public abstract class AbstractControllerBean<T> {

    public static final String PANEL_FORM_ID_SUFFIX = "Form";

    public static String primaryFormId = "work";

    public static Map<String, String> uiPanelIdsMap;

    public Dao<T> dao;

    public static void clearModalPanel(String entityType) {
        String containerId = primaryFormId;
        if (StringUtils.isBlank(entityType) && uiPanelIdsMap != null && uiPanelIdsMap.containsKey(entityType)) {
            containerId = uiPanelIdsMap.get(entityType) + PANEL_FORM_ID_SUFFIX;
        }
        clearFields(containerId);
    }

    public static void clearFields(String containerId) {
        try {
            JSFUtils.clearForm(containerId);
        }
        catch (Exception e) {
            String message = "Unable to clear input fields for container with id:" + containerId;
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_WARN, message, message));
        }
    }

    public String create() {
        return null;
    }

    public String update() {
        return null;
    }

    public String delete() {
        return null;
    }

    public void search() {
    }

    public void retrieve() {
    }

    @SuppressWarnings("unchecked")
    public List<T> getAll() {
        try {
            Class<T> type = Class.class.newInstance();
            return dao.findAll(type, 0, 0);
        }
        catch (Exception e) {
        }
        return null;
    }

    @SuppressWarnings("unchecked")
    public List<T> searchByExample(T search) {
        return dao.searchByExample((T) search.getClass(), 0, 0);
    }

    public abstract Converter getConverter();
}
