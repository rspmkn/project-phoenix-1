package com.apd.phoenix.web;

import java.io.Serializable;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.apd.phoenix.core.utility.TenantConfigRepository;
import com.apd.phoenix.service.persistence.multitenancy.api.CurrentTenantDomain;
import com.apd.phoenix.service.persistence.multitenancy.impl.CurrentTenantIdentifierResolverImpl;

@Named
@Stateless
public class PropertiesBean implements Serializable {
	
	private static Logger LOG = LoggerFactory.getLogger(PropertiesBean.class);
	
	@Inject
	private CurrentTenantDomain currentTenantDomain;

    private static final long serialVersionUID = -5229651643823847245L;

    @SuppressWarnings("static-method")
    public String get(String file, String key) {
    	LOG.debug("getProperty called with file: "+ file+ " and key: "+ key);
    	LOG.debug("current tenant is: "+ currentTenantDomain.toString());
        String currentTenantName = TenantConfigRepository.getInstance().getTenantNameByDomain(currentTenantDomain.getDomain());
        
        if(currentTenantName == null){
        	currentTenantName = TenantConfigRepository.getInstance().getTenantNameById(CurrentTenantIdentifierResolverImpl.getCurrentTenant());
        }

        return TenantConfigRepository.getInstance().getProperty(currentTenantName, file, key);
    }

    @SuppressWarnings("static-method")
    public String get(String file, String key, String defaultValue) {
    	String value = get(file, key);
    	return (value != null ? value : defaultValue); 
    }
}