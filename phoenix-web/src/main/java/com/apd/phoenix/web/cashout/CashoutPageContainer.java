package com.apd.phoenix.web.cashout;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.enterprise.context.Dependent;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.commons.lang.StringUtils;

import com.apd.phoenix.core.StringEscape;
import com.apd.phoenix.service.business.AccountXCredentialXUserBp;
import com.apd.phoenix.service.business.AddressBp;
import com.apd.phoenix.service.business.PaymentTypeBp;
import com.apd.phoenix.service.model.Account;
import com.apd.phoenix.service.model.AccountXCredentialXUser;
import com.apd.phoenix.service.model.Address;
import com.apd.phoenix.service.model.CostCenter;
import com.apd.phoenix.service.model.Credential;
import com.apd.phoenix.service.model.MiscShipTo;
import com.apd.phoenix.service.model.AssignedCostCenter;
import com.apd.phoenix.service.model.CardInformation;
import com.apd.phoenix.service.model.CashoutPage;
import com.apd.phoenix.service.model.CashoutPageXField;
import com.apd.phoenix.service.model.Comment;
import com.apd.phoenix.service.model.CustomerOrder;
import com.apd.phoenix.service.model.Address.AddressFormatField;
import com.apd.phoenix.service.model.CardInformation.CvvIndicator;
import com.apd.phoenix.service.model.CustomerOrder.OrderOrigin;
import com.apd.phoenix.service.model.Department;
import com.apd.phoenix.service.model.Desktop;
import com.apd.phoenix.service.model.Field;
import com.apd.phoenix.service.model.FieldOptions;
import com.apd.phoenix.service.model.FieldType;
import com.apd.phoenix.service.model.LineItem;
import com.apd.phoenix.service.model.MessageMapping;
import com.apd.phoenix.service.model.OrderType;
import com.apd.phoenix.service.model.PaymentInformation;
import com.apd.phoenix.service.model.PoNumber;
import com.apd.phoenix.service.model.PoNumberType;
import com.apd.phoenix.service.model.SystemUser;
import com.apd.phoenix.service.persistence.jpa.AssignedCostCenterDao;
import com.apd.phoenix.service.persistence.jpa.PoNumberTypeDao;
import com.apd.phoenix.web.cashout.ZipSearchBean.ZipSelection;

/**
 * This class is used to store and read the values on a cashout page.
 * 
 * @author RHC
 *
 */
@Named
@Dependent
public class CashoutPageContainer implements Serializable {
	private static final long serialVersionUID = 1L;
	private static final String DEFAULT_ADDRESS_FORMAT = "#LINE1 #LINE2 #CITY, #STATE #ZIP";
	private static final String[] SPECIAL_CHARACTERS = {":", "*", "|", ",", "~", "^"};
	
	private Map<CashoutPageXField, Boolean> requiredMap = new HashMap<>();
	private Map<CashoutPageXField, Boolean> updatableMap = new HashMap<>();
	private Map<CashoutPageXField, Set<Object>> optionsMap = new HashMap<>();
	private Map<CashoutPageXField, Object> valueMap = new HashMap<>();
	private Map<CashoutPageXField, String> labelMap = new HashMap<>();
	private Map<CashoutPageXField, MessageMapping> mappingMap = new HashMap<>();
	private Map<LineItem, String> commentMap = new HashMap<>();
	private Map<LineItem, String> priceChangeNoticeMap = new HashMap<>();
	private Map<String, CashoutPageXField> fieldMap = new HashMap<>();
	private CashoutPage page;
	private String[] allowedCountryCodes;
	
	@Inject
	private AddressBp addressBp;
	
	@Inject
	private PoNumberTypeDao poTypeDao;
	
	@Inject
	private PaymentTypeBp paymentTypeBp;
	
	@Inject
	private AccountXCredentialXUserBp axcxuBp;

    @Inject
    private AssignedCostCenterDao costCenterDao;
        
	/**
	 * This method takes a CashoutPage, and populates the values with the defaults for that page.
	 * 
	 * @param page
	 */
	public void init(CashoutPage page) {
		for (CashoutPageXField field : page.getPageXFields()) {
			valueMap.put(field, field.getDefaultValue());
			labelMap.put(field, field.getLabel());
			requiredMap.put(field, field.getRequired());
			updatableMap.put(field, field.getUpdatable());
			mappingMap.put(field, field.getMapping());
			fieldMap.put(field.getField().getName(), field);
			optionsMap.put(field, new HashSet<>());
			optionsMap.get(field).addAll(field.getField().getOptions());
		}
		for (String fieldName : fieldMap.keySet()) {
			convertTypeOfDefault(fieldName);
		}
		
		setPermanentFields();
		
		this.setPage(page);
	}
	
	/**
	 * Convenience method for adding Fields to every Cashout page.
	 */
	public void setPermanentFields() {
		//This is a field that needs to exist for every CashoutPage, but we do not want to ever reveal to user
		FieldType usAccountNumFieldType = new FieldType();
		usAccountNumFieldType.setName("text");
		Field usAccountNumField = new Field();
		usAccountNumField.setName("EXT:USAccount");
		usAccountNumField.setFieldType(usAccountNumFieldType);
		CashoutPageXField usAccountNum = new CashoutPageXField();
		usAccountNum.setField(usAccountNumField);
		fieldMap.put(usAccountNum.getField().getName(), usAccountNum);
		valueMap.put(usAccountNum, "");
		MessageMapping usAccountNumMessageMapping = new MessageMapping();
		usAccountNumMessageMapping.setName("EXT:USAccount");
		mappingMap.put(usAccountNum, usAccountNumMessageMapping);
	}
	
	/**
	 * This method is called whenever the user selects an existing billto or shipto address. It stores the 
	 * values onto the order, so that when the page is rerendered the fields are populated from the values 
	 * on the shiptos.
	 * 
	 * @param order
	 */
	public void rerender(CustomerOrder order, String addressType) {
                populateRequiredFields(order);
		//stores the changed address, so that its values override any specified values
		if ("BILLTO".equals(addressType)) {
			setValues("existing billto address selection", order, true);
		}
		else if ("SHIPTO".equals(addressType)) {
			setValues("existing shipto address selection", order, true);
		}
		//moves the values from the order to the page
		for (String fieldName : this.fieldMap.keySet()) {
			if (StringUtils.isNotBlank(fieldName) && (fieldName.startsWith("ship ") || fieldName.startsWith("bill "))) {
				setValues(fieldName, order, false);
			}
		}
	}
	
	/**
	 * Takes a CustomerOrder, and maps the values that have been stored on this container to the Order. Inverse of 
	 * orderToPage.
	 * 
	 * @param order
	 */
	public void pageToOrder(CustomerOrder order) {
		populateRequiredFields(order);
		//sets the billto and shipto addresses first. That way, they can be overridded by any values entered by 
		//the user in the fields of the cashout page.
		if (this.fieldMap.containsKey("existing billto address selection")) {
			setValues("existing billto address selection", order, true);
		}
		if (this.fieldMap.containsKey("existing shipto address selection")) {
			setValues("existing shipto address selection", order, true);
		}
		for (String fieldName : this.fieldMap.keySet()) {
			if (!fieldName.equals("existing billto address selection") && !fieldName.equals("existing shipto address selection")) {
				setValues(fieldName, order, true);
			}
		}              
	}
	
	/**
	 * Takes a CustomerOrder, and maps the values from the Order onto this container. Inverse of pageToOrder.
	 * 
	 * @param order
	 */
	public void orderToPage(CustomerOrder order) {
		if (order.getStatus() == null || order.getStatus().getValue() == null || "QUOTED".equals(order.getStatus().getValue())) {
			return;
		}
		populateRequiredFields(order);
		for (String fieldName : this.fieldMap.keySet()) {
			setValues(fieldName, order, false);
		}
	}
	
	public void manualOrderToPage(CustomerOrder order) {
		if (order.getStatus() == null || order.getStatus().getValue() == null) {
			return;
		}
		populateRequiredFields(order);
		for (String fieldName : this.fieldMap.keySet()) {
			setValues(fieldName, order, false);
		}
	}
	
	private void populateRequiredFields(CustomerOrder order) {
		if (order.getPaymentInformation() == null) {
			order.setPaymentInformation(new PaymentInformation());
			if (order.getId() != null) {
				order.getPaymentInformation().setPaymentType(paymentTypeBp.getTypeByValue("Invoice"));
			}
		}
		if (order.getPaymentInformation().getBillingAddress() == null) {
			order.getPaymentInformation().setBillingAddress(new Address());
		}
                if(order.getAddress()==null){
                    order.setAddress(new Address());
                }
		if (order.getAddress().getMiscShipTo() == null) {
			order.getAddress().setMiscShipTo(new MiscShipTo());
		}
	}
	
	/**
	 * Called by orderToPage and pageToOrder. Takes a field name, the order, and the direction of the mapping, and 
	 * maps that field on the container to the order or the field on the order to the container.
	 * 
	 * @param fieldName
	 * @param order
	 * @param pageToOrder
	 */
	private void setValues(String fieldName, CustomerOrder order, boolean pageToOrder) {
		CashoutPageXField field = fieldMap.get(fieldName);
		if (pageToOrder && (this.valueMap.get(field) == null || StringUtils.isBlank(this.valueMap.get(field).toString()))) {
			return;
		}
		if (this.valueMap.get(field) instanceof String) {
			this.valueMap.put(field, this.stripSpecialCharacters((String)this.valueMap.get(field)));
		}
		switch (fieldName) {
		case "ship name":
			if (pageToOrder) {
				order.getAddress().setName(getOrderValueFromField(field));
			} else {
				this.valueMap.put(field, getFieldFromOrderValue(field, order.getAddress().getName()));
			}
			break;
		case "requester phone":
			if (pageToOrder) {
				order.getAddress().getMiscShipTo().setRequesterPhone(getOrderValueFromField(field));
			} else {
				this.valueMap.put(field, getFieldFromOrderValue(field, order.getAddress().getMiscShipTo().getRequesterPhone()));
			}
			break;
		case "ship company":
			if (pageToOrder) {
				order.getAddress().setCompany(getOrderValueFromField(field));
			} else {
				this.valueMap.put(field, getFieldFromOrderValue(field, order.getAddress().getCompany()));
			}
			break;
		case "ship address 1":
			if (pageToOrder) {
				order.getAddress().setLine1(getOrderValueFromField(field));
			} else {
				this.valueMap.put(field, getFieldFromOrderValue(field, order.getAddress().getLine1()));
			}
			break;
		case "ship address 2":
			if (pageToOrder) {
				order.getAddress().setLine2(getOrderValueFromField(field));
			} else {
				this.valueMap.put(field, getFieldFromOrderValue(field, order.getAddress().getLine2()));
			}
			break;
		case "ship city":
			if (pageToOrder) {
				order.getAddress().setCity((String)this.valueMap.get(field));
			} else {
				this.valueMap.put(field, order.getAddress().getCity());
			}
			break;
		case "ship zip":
			if (pageToOrder) {
				order.getAddress().setZip((String)this.valueMap.get(field));
				List<ZipSelection> selectedZips = zipSearchBean.zipSearch((String)this.valueMap.get(field));
				if (this.updatableMap.get(field) && shipZipIndex < selectedZips.size() && shipZipSet) {
					order.getAddress().setCounty(selectedZips.get(shipZipIndex).getCounty());
					order.getAddress().setGeoCode(selectedZips.get(shipZipIndex).getGeocode());
				}
			} else {
				this.valueMap.put(field, order.getAddress().getZip());
			}
			break;
		case "ship states":
			if (pageToOrder) {
				if (this.valueMap.get(field) != null) {
					order.getAddress().setState(this.valueMap.get(field).toString());
				}
				else {
					order.getAddress().setState(null);
				}
			} else {
				this.valueMap.put(field, order.getAddress().getState());
			}
			break;
		case "ship north america":
			if (pageToOrder) {
				order.getAddress().setCountry(((FieldOptions)this.valueMap.get(field)).getValue());
			} else {
				this.valueMap.put(field, order.getAddress().getCountry());
				this.setFieldOptionFromString(field);
			}
			break;
		case "ship us only":
			if (pageToOrder) {
				order.getAddress().setCountry(((FieldOptions)this.valueMap.get(field)).getValue());
			} else {
				this.valueMap.put(field, order.getAddress().getCountry());
				this.setFieldOptionFromString(field);
			}
			break;
		case "ship us and canada":
			if (pageToOrder) {
				order.getAddress().setCountry(((FieldOptions)this.valueMap.get(field)).getValue());
			} else {
				this.valueMap.put(field, order.getAddress().getCountry());
				this.setFieldOptionFromString(field);
			}
			break;
		case "ship date":
			if (pageToOrder) {
				order.setShipDate((Date)this.valueMap.get(field));
			} else {
				this.valueMap.put(field, order.getShipDate());
			}
			break;
		case "delivery date":
			if (pageToOrder) {
				order.setDeliveryDate((Date)this.valueMap.get(field));
			} else {
				this.valueMap.put(field, order.getDeliveryDate());
			}
			break;
		case "cancel date":
			if (pageToOrder) {
				order.setCancelDate((Date)this.valueMap.get(field));
			} else {
				this.valueMap.put(field, order.getCancelDate());
			}
			break;
		case "existing shipto address selection":
			if (pageToOrder) {
				order.setAddress(addressBp.cloneEntity((Address)this.valueMap.get(field)));
				if (order.getAddress().getMiscShipTo() == null) {
					order.getAddress().setMiscShipTo(new MiscShipTo());
				}
			} else {
				if (order.getAddress() != null) {
					this.valueMap.put(field, order.getAddress());
					if (this.optionsMap.get(field) == null) {
						this.optionsMap.put(field, new HashSet<Object>());
					}
					if (!this.optionsMapContainsAddress(field)) {
						this.optionsMap.get(field).add(this.valueMap.get(field));
					}
				}
			}
			break;
		case "desktop":
			if (pageToOrder) {
				order.getAddress().getMiscShipTo().setDesktop(getOrderValueFromFieldAndInput(field, ((Desktop)this.valueMap.get(field)).getName()));
			} else {
				if (order.getAddress() != null && order.getAddress().getMiscShipTo() != null && 
						order.getAddress().getMiscShipTo().getDesktop() != null) {
					Desktop desktop = new Desktop();
					String name = getFieldFromOrderValue(field, order.getAddress().getMiscShipTo().getDesktop());
					desktop.setName(name);
					this.valueMap.put(field, desktop);
					if (this.optionsMap.get(field) == null) {
						this.optionsMap.put(field, new HashSet<Object>());
					}
					if (!this.optionsMap.get(field).contains(desktop)) {
						this.optionsMap.get(field).add(desktop);
					}                                
				}
			}
			break;
		case "desktop combobox":
			if (pageToOrder) {
				order.getAddress().getMiscShipTo().setDesktop(getOrderValueFromFieldAndInput(field, ((Desktop)this.valueMap.get(field)).getName()));
			} else {
				if (order.getAddress() != null && order.getAddress().getMiscShipTo() != null && 
						order.getAddress().getMiscShipTo().getDesktop() != null) {
					Desktop desktop = new Desktop();
					String name = getFieldFromOrderValue(field, order.getAddress().getMiscShipTo().getDesktop());
					desktop.setName(name);
					this.valueMap.put(field, desktop);
					if (this.optionsMap.get(field) == null) {
						this.optionsMap.put(field, new HashSet<Object>());
					}
					if (!this.optionsMap.get(field).contains(desktop)) {
						this.optionsMap.get(field).add(desktop);
					}
				}
			}
			break; 
		case "desktop free":
			if (pageToOrder) {
				order.getAddress().getMiscShipTo().setDesktop(getOrderValueFromFieldAndInput(field, (String)this.valueMap.get(field)));
			} else {
				if (order.getAddress() != null && order.getAddress().getMiscShipTo() != null && 
						order.getAddress().getMiscShipTo().getDesktop() != null) {
					this.valueMap.put(field, getFieldFromOrderValue(field, order.getAddress().getMiscShipTo().getDesktop()));
				}
			}
			break;
		case "building or department":
			if (pageToOrder) {
				order.getAddress().getMiscShipTo().setDepartment(getOrderValueFromFieldAndInput(field, ((Department)this.valueMap.get(field)).getName()));
			} else {
				if (order.getAddress() != null && order.getAddress().getMiscShipTo() != null && 
						order.getAddress().getMiscShipTo().getDepartment() != null) {
					Department newDepartment = new Department();
					newDepartment.setName(getFieldFromOrderValue(field, order.getAddress().getMiscShipTo().getDepartment()));
					this.valueMap.put(field, newDepartment);
					if (this.optionsMap.get(field) == null) {
						this.optionsMap.put(field, new HashSet<Object>());
					}
					if (!this.optionsMap.get(field).contains(this.valueMap.get(field))) {
						this.optionsMap.get(field).add(this.valueMap.get(field));
					}
				}
			}
			break;
		case "building or department combobox":
			if (pageToOrder) {
				order.getAddress().getMiscShipTo().setDepartment(getOrderValueFromFieldAndInput(field, ((Department)this.valueMap.get(field)).getName()));
			} else {
				if (order.getAddress() != null && order.getAddress().getMiscShipTo() != null && 
						order.getAddress().getMiscShipTo().getDepartment() != null) {
					Department newDepartment = new Department();
					newDepartment.setName(getFieldFromOrderValue(field, order.getAddress().getMiscShipTo().getDepartment()));
					this.valueMap.put(field, newDepartment);
					if (this.optionsMap.get(field) == null) {
						this.optionsMap.put(field, new HashSet<Object>());
					}
					if (!this.optionsMap.get(field).contains(newDepartment)) {
						this.optionsMap.get(field).add(newDepartment);
					}
				}
			}
			break;
		case "building or department free":
			if (pageToOrder) {
				order.getAddress().getMiscShipTo().setDepartment(getOrderValueFromField(field));
			} else {
				if (order.getAddress() != null && order.getAddress().getMiscShipTo() != null && 
						order.getAddress().getMiscShipTo().getDepartment() != null) {
					this.valueMap.put(field, getFieldFromOrderValue(field, order.getAddress().getMiscShipTo().getDepartment()));
				}
			}
			break;
		case "cost center":
			if (pageToOrder) {
				order.setAssignedCostCenter((AssignedCostCenter)this.valueMap.get(field));
				if (field.getIncludeLabelInMapping() != null && field.getIncludeLabelInMapping()) {
					order.setAssignedCostCenterLabel(field.getLabel());
				}
			} else {
				if (order.getAssignedCostCenter() != null) {
					this.valueMap.put(field, order.getAssignedCostCenter());
					if (this.optionsMap.get(field) == null) {
						this.optionsMap.put(field, new HashSet<Object>());
					}
					if (!this.optionsMap.get(field).contains(this.valueMap.get(field))) {
						this.optionsMap.get(field).add(this.valueMap.get(field));
					}
				}
			}
			break;
		case "cost center combobox":
			if (pageToOrder) {
				boolean hasBeenAssigned = false;
				for (Object assigned : this.optionsMap.get(field)) {
					if (assigned instanceof AssignedCostCenter && 
							this.costCenterToString((AssignedCostCenter) assigned).equals(this.valueMap.get(field))) {
						order.setAssignedCostCenter((AssignedCostCenter) assigned);
						if (field.getIncludeLabelInMapping() != null && field.getIncludeLabelInMapping()) {
							order.setAssignedCostCenterLabel(field.getLabel());
						}
						hasBeenAssigned = true;
						break;
					}
				}
				if (!hasBeenAssigned) {
					order.setAssignedCostCenter(this.createDummyCostCenter((String)this.valueMap.get(field), order));
					if (field.getIncludeLabelInMapping() != null && field.getIncludeLabelInMapping()) {
						order.setAssignedCostCenterLabel(field.getLabel());
					}
				}
			} else {
				if (order.getAssignedCostCenter() != null) {
					this.valueMap.put(field, order.getAssignedCostCenter().getCostCenter().getName());
					if (this.optionsMap.get(field) == null) {
						this.optionsMap.put(field, new HashSet<Object>());
					}
					if (!this.optionsMap.get(field).contains(order.getAssignedCostCenter())) {
						this.optionsMap.get(field).add(order.getAssignedCostCenter());
					}
				}
			}
			break;
		case "cost center free":
			if (pageToOrder) {
				order.setAssignedCostCenter(this.createDummyCostCenter((String)this.valueMap.get(field), order));
				if (field.getIncludeLabelInMapping() != null && field.getIncludeLabelInMapping()) {
					order.setAssignedCostCenterLabel(field.getLabel());
				}
			} else {
				if (order.getAssignedCostCenter() != null) {
					this.valueMap.put(field, order.getAssignedCostCenter().getCostCenter().getName());
				}
			}
			break;
		case "shipping method":
			if (pageToOrder) {
				order.getAddress().getMiscShipTo().setShippingMethod(((FieldOptions)this.valueMap.get(field)).getValue());
			} else {
				this.valueMap.put(field, order.getAddress().getMiscShipTo().getShippingMethod());
			}
			break;
		case "custom po number":
			if (pageToOrder) {
				if (!((String)this.valueMap.get(field)).isEmpty()) {
					PoNumber customPo = order.getCustomerPo();
					if (customPo == null) {
						customPo = new PoNumber();
						PoNumberType searchType = new PoNumberType();
						searchType.setName("Customer");
						customPo.setType(poTypeDao.searchByExactExample(searchType, 0, 0).get(0));
						customPo.getOrders().add(order);
						order.getPoNumbers().add(customPo);
					}
					customPo.setValue(getOrderValueFromField(field));
				}
			} else {
				for (PoNumber number : order.getPoNumbers()) {
					if (number.getType().getName().equals("Customer")) {
						this.valueMap.put(field, getFieldFromOrderValue(field, number.getValue()));
						break;
					}
				}
			}
			break;
		case "blanket po number":
			if (pageToOrder) {
				PoNumber blanketPo = order.getBlanketPo();
				if (blanketPo == null) {
					blanketPo = new PoNumber();
					blanketPo.setType(((PoNumber)this.valueMap.get(field)).getType());
					blanketPo.getOrders().add(order);
					order.getPoNumbers().add(blanketPo);
				}
				blanketPo.setValue(((PoNumber)this.valueMap.get(field)).getValue());
			} else {
				for (PoNumber number : order.getPoNumbers()) {
					if (number.getType().getName().equals("blanket")) {
						this.valueMap.put(field, number);
						if (this.optionsMap.get(field) == null) {
							this.optionsMap.put(field, new HashSet<Object>());
						}
						if (!this.optionsMap.get(field).contains(this.valueMap.get(field))) {
							this.optionsMap.get(field).add(this.valueMap.get(field));
						}
						break;
					}
				}
			}
			break;
		case "email":
			if (pageToOrder) {
				order.getAddress().getMiscShipTo().setOrderEmail(getOrderValueFromField(field));
			} else {
				this.valueMap.put(field, getFieldFromOrderValue(field, order.getAddress().getMiscShipTo().getOrderEmail()));
			}
			break;
		case "credit card name on card":
			if (pageToOrder) {
				if (!((String)this.valueMap.get(field)).isEmpty()) {
					if (order.getPaymentInformation().getCard() == null) {
						order.getPaymentInformation().setCard(new CardInformation());
						order.getPaymentInformation().getCard().setGhost(false);
					}
					order.getPaymentInformation().getCard().setNameOnCard((String)this.valueMap.get(field));
				}
			} else {
				if (order.getPaymentInformation() != null && order.getPaymentInformation().getCard() != null) {
					this.valueMap.put(field, order.getPaymentInformation().getCard().getNameOnCard());
				}
			}
			break;
		case "credit card number":
			if (pageToOrder) {
				if (!((String)this.valueMap.get(field)).isEmpty()) {
					if (order.getPaymentInformation().getCard() == null) {
						order.getPaymentInformation().setCard(new CardInformation());
						order.getPaymentInformation().getCard().setGhost(false);
					}
					order.getPaymentInformation().getCard().setNumber((String)this.valueMap.get(field));
				}
			} else {
				if (order.getPaymentInformation() != null && order.getPaymentInformation().getCard() != null) {
					this.valueMap.put(field, order.getPaymentInformation().getCard().getIdentifier());
				}
			}
			break;
		case "credit card expiration":
			if (pageToOrder) {
				if (this.valueMap.get(field) != null && !((String)this.valueMap.get(fieldMap.get("credit card number"))).isEmpty()) {
					if (order.getPaymentInformation().getCard() == null) {
						order.getPaymentInformation().setCard(new CardInformation());
						order.getPaymentInformation().getCard().setGhost(false);
					}
					order.getPaymentInformation().getCard().setExpiration(CardInformation.addExpirationMargin((Date)this.valueMap.get(field)));
				}
			} else {
				if (order.getPaymentInformation() != null && order.getPaymentInformation().getCard() != null) {
					this.valueMap.put(field, order.getPaymentInformation().getCard().getExpiration());
				}
			}
			break;
		case "credit card CVV":
			if (pageToOrder) {
				//do nothing
			} else {
				//clearing out CVV
				this.valueMap.put(field, "");
			}
			break;
		case "credit card CVV indicator":
			if (pageToOrder) {
				if (this.valueMap.get(field) != null && !((String)this.valueMap.get(fieldMap.get("credit card number"))).isEmpty()) {
					if (order.getPaymentInformation().getCard() == null) {
						order.getPaymentInformation().setCard(new CardInformation());
						order.getPaymentInformation().getCard().setGhost(false);
					}
					for (CvvIndicator indicator : CvvIndicator.values()) {
						if (indicator.equals(this.valueMap.get(field)) || indicator.toString().equals(this.valueMap.get(field))) {
							order.getPaymentInformation().getCard().setCvvIndicator(indicator);
							break;
						}
					}
				}
			} else {
				if (order.getPaymentInformation() != null && order.getPaymentInformation().getCard() != null) {
					this.valueMap.put(field, order.getPaymentInformation().getCard().getCvvIndicator());
				}
			}
			break;
		case "payment type selection":
			if (!pageToOrder) {
				String type = "Invoice";
				if (order.getPaymentInformation() != null && order.getPaymentInformation().getCard() != null) {
					type = "Credit Card";
				}
				for (Object optionObject : this.optionsMap.get(field)) {
					FieldOptions option = (FieldOptions) optionObject;
					if (option.getValue().equals(type)) {
						this.valueMap.put(field, option);
						break;
					}
				}
			}
			break;
		case "bill name":
			if (pageToOrder) {
				order.getPaymentInformation().getBillingAddress().setName(getOrderValueFromField(field));
			} else if (order.getPaymentInformation().getBillingAddress() != null) {
				this.valueMap.put(field, getFieldFromOrderValue(field, order.getPaymentInformation().getBillingAddress().getName()));
			}
			break;
		case "bill company":
			if (pageToOrder) {
				order.getPaymentInformation().getBillingAddress().setCompany(getOrderValueFromField(field));
			} else if (order.getPaymentInformation().getBillingAddress() != null) {
				this.valueMap.put(field, getFieldFromOrderValue(field, order.getPaymentInformation().getBillingAddress().getCompany()));
			}
			break;
		case "bill address 1":
			if (pageToOrder) {
				order.getPaymentInformation().getBillingAddress().setLine1(getOrderValueFromField(field));
			} else if (order.getPaymentInformation().getBillingAddress() != null) {
				this.valueMap.put(field, getFieldFromOrderValue(field, order.getPaymentInformation().getBillingAddress().getLine1()));
			}
			break;
		case "bill address 2":
			if (pageToOrder) {
				order.getPaymentInformation().getBillingAddress().setLine2(getOrderValueFromField(field));
			} else if (order.getPaymentInformation().getBillingAddress() != null) {
				this.valueMap.put(field, getFieldFromOrderValue(field, order.getPaymentInformation().getBillingAddress().getLine2()));
			}
			break;
		case "bill city":
			if (pageToOrder) {
				order.getPaymentInformation().getBillingAddress().setCity((String)this.valueMap.get(field));
			} else if (order.getPaymentInformation().getBillingAddress() != null) {
				this.valueMap.put(field, order.getPaymentInformation().getBillingAddress().getCity());
			}
			break;
		case "bill zip":
			if (pageToOrder) {
				order.getPaymentInformation().getBillingAddress().setZip((String)this.valueMap.get(field));
				List<ZipSelection> selectedZips = zipSearchBean.zipSearch((String)this.valueMap.get(field));
				if (this.updatableMap.get(field) && billZipIndex < selectedZips.size() && billZipSet) {
					order.getPaymentInformation().getBillingAddress().setCounty(selectedZips.get(billZipIndex).getCounty());
					order.getPaymentInformation().getBillingAddress().setGeoCode(selectedZips.get(billZipIndex).getGeocode());
				}
			} else if (order.getPaymentInformation().getBillingAddress() != null) {
				this.valueMap.put(field, order.getPaymentInformation().getBillingAddress().getZip());
			}
			break;
		case "bill states":
			if (pageToOrder) {
				if (this.valueMap.get(field) != null) {
					order.getPaymentInformation().getBillingAddress().setState(this.valueMap.get(field).toString());
				}
				else {
					order.getPaymentInformation().getBillingAddress().setState(null);
				}
			} else if (order.getPaymentInformation().getBillingAddress() != null) {
				this.valueMap.put(field, order.getPaymentInformation().getBillingAddress().getState());
			}
			break;
		case "bill north america":
			if (pageToOrder) {
				order.getPaymentInformation().getBillingAddress().setCountry(((FieldOptions)this.valueMap.get(field)).getValue());
			} else if (order.getPaymentInformation().getBillingAddress() != null) {
				this.valueMap.put(field, order.getPaymentInformation().getBillingAddress().getCountry());
				this.setFieldOptionFromString(field);
			}
			break;
		case "bill us only":
			if (pageToOrder) {
				order.getPaymentInformation().getBillingAddress().setCountry(((FieldOptions)this.valueMap.get(field)).getValue());
			} else if (order.getPaymentInformation().getBillingAddress() != null) {
				this.valueMap.put(field, order.getPaymentInformation().getBillingAddress().getCountry());
				this.setFieldOptionFromString(field);
			}
			break;
		case "bill us and canada":
			if (pageToOrder) {
				order.getPaymentInformation().getBillingAddress().setCountry(((FieldOptions)this.valueMap.get(field)).getValue());
			} else if (order.getPaymentInformation().getBillingAddress() != null) {
				this.valueMap.put(field, order.getPaymentInformation().getBillingAddress().getCountry());
				this.setFieldOptionFromString(field);
			}
			break;
		case "existing billto address selection":
			if (pageToOrder) {
				order.getPaymentInformation().setBillingAddress(addressBp.cloneEntity((Address)this.valueMap.get(field)));
			} else if (order.getPaymentInformation().getBillingAddress() != null) {
				if (order.getPaymentInformation().getBillingAddress().getId() != null) {
					this.valueMap.put(field, addressBp.findById(order.getPaymentInformation().getBillingAddress().getId(), Address.class));
				}
				else {
					this.valueMap.put(field, order.getPaymentInformation().getBillingAddress());
				}
				if (this.optionsMap.get(field) == null) {
					this.optionsMap.put(field, new HashSet<Object>());
				}
				if (!this.optionsMapContainsAddress(field)) {
					this.optionsMap.get(field).add(this.valueMap.get(field));
				}
			}
			break;
		case "comments":
			if (pageToOrder) {
				Comment newComment = new Comment();
				newComment.setCommentDate(new Date());
				newComment.setContent((String)this.valueMap.get(field));
				newComment.setUser(order.getUser());
				order.getComments().add(newComment);
			} else {
				Comment earliestComment = new Comment();
				for (Comment comment : order.getComments()) {
					if (earliestComment.getCommentDate() == null 
							|| earliestComment.getCommentDate().after(comment.getCommentDate())) {
						earliestComment = comment;
					}
				}
				this.valueMap.put(field, earliestComment.getContent());
			}
			break;
		case "line item comments":
			if (pageToOrder) {
				for (LineItem item : commentMap.keySet()) {
					item.setPurchaseComment(commentMap.get(item));
				}
			} else {
				for (LineItem item : order.getItems()) {
					commentMap.put(item, item.getPurchaseComment());
				}
			}
			break;
		case "order type":
			if (pageToOrder) {
				order.setType((OrderType)this.valueMap.get(field));
			} else {
				this.valueMap.put(field, order.getType());
			}
			break;
                case "order origin":
                        if (pageToOrder) {
                                String origin = (String)this.valueMap.get(field);
                                for (OrderOrigin o : this.getOrderOrigins()) {
                                    if (o.toString().equals(origin)) {
                                        order.setOrderOrigin(o);
                                    }
                                }
                        } else {
                                if (order.getOrderOrigin() != null) {
                                    this.valueMap.put(field, order.getOrderOrigin().toString());
                                }
                        }
                        break;
                }
		//also uses the messageMapping for the field.
		if (field.getField().getFieldType().getName().equals("text") && mappingMap.containsKey(field) 
				&& mappingMap.get(field) != null) {
			switch (mappingMap.get(field).getName()) {
			case "AddressID":
				if (pageToOrder) {
					order.getAddress().getMiscShipTo().setAddressId(getOrderValueFromField(field));
				} else {
					this.valueMap.put(field, getFieldFromOrderValue(field, order.getAddress().getMiscShipTo().getAddressId()));
				}
				break;
			case "AddressID(base 36)":
				if (pageToOrder) {
					order.getAddress().getMiscShipTo().setAddressId36(getOrderValueFromField(field));
				} else {
					this.valueMap.put(field, getFieldFromOrderValue(field, order.getAddress().getMiscShipTo().getAddressId36()));
				}
				break;
			case "Contact Name":
				if (pageToOrder) {
					order.getAddress().getMiscShipTo().setContactName(getOrderValueFromField(field));
				} else {
					this.valueMap.put(field, getFieldFromOrderValue(field, order.getAddress().getMiscShipTo().getContactName()));
				}
				break;
			case "Desktop":
				if (pageToOrder) {
					order.getAddress().getMiscShipTo().setDesktop(getOrderValueFromField(field));
				} else {
					this.valueMap.put(field, getFieldFromOrderValue(field, order.getAddress().getMiscShipTo().getDesktop()));
				}
				break;
			case "EXT:APD_InterchangeID":
				if (pageToOrder) {
					order.getAddress().getMiscShipTo().setApdInterchangeId(getOrderValueFromField(field));
				} else {
					this.valueMap.put(field, getFieldFromOrderValue(field, order.getAddress().getMiscShipTo().getApdInterchangeId()));
				}
				break;
			case "EXT:CarrierRouting":
				if (pageToOrder) {
					order.getAddress().getMiscShipTo().setCarrierRouting(getOrderValueFromField(field));
				} else {
					this.valueMap.put(field, getFieldFromOrderValue(field, order.getAddress().getMiscShipTo().getCarrierRouting()));
				}
				break;
			case "EXT:DeliveryDate":
				if (pageToOrder) {
					order.getAddress().getMiscShipTo().setDeliveryDate(getOrderValueFromField(field));
				} else {
					this.valueMap.put(field, getFieldFromOrderValue(field, order.getAddress().getMiscShipTo().getDeliveryDate()));
				}
				break;
			case "EXT:Dept":
				if (pageToOrder) {
					order.getAddress().getMiscShipTo().setDept(getOrderValueFromField(field));
				} else {
					this.valueMap.put(field, getFieldFromOrderValue(field, order.getAddress().getMiscShipTo().getDept()));
				}
				break;
			case "EXT:Facility":
				if (pageToOrder) {
					order.getAddress().getMiscShipTo().setFacility(getOrderValueFromField(field));
				} else {
					this.valueMap.put(field, getFieldFromOrderValue(field, order.getAddress().getMiscShipTo().getFacility()));
				}
				break;
			case "EXT:Fedex No":
				if (pageToOrder) {
					order.getAddress().getMiscShipTo().setFedexNumber(getOrderValueFromField(field));
				} else {
					this.valueMap.put(field, getFieldFromOrderValue(field, order.getAddress().getMiscShipTo().getFedexNumber()));
				}
				break;
			case "EXT:LocationID":
				if (pageToOrder) {
					order.getAddress().getMiscShipTo().setLocationId(getOrderValueFromField(field));
				} else {
					this.valueMap.put(field, getFieldFromOrderValue(field, order.getAddress().getMiscShipTo().getLocationId()));
				}
				break;
			case "EXT:Mailstop":
				if (pageToOrder) {
					order.getAddress().getMiscShipTo().setMailStop(getOrderValueFromField(field));
				} else {
					this.valueMap.put(field, getFieldFromOrderValue(field, order.getAddress().getMiscShipTo().getMailStop()));
				}
				break;
			case "EXT:NOWL":
				if (pageToOrder) {
					order.getAddress().getMiscShipTo().setNowl(getOrderValueFromField(field));
				} else {
					this.valueMap.put(field, getFieldFromOrderValue(field, order.getAddress().getMiscShipTo().getNowl()));
				}
				break;
			case "EXT:POL":
				if (pageToOrder) {
					order.getAddress().getMiscShipTo().setPol(getOrderValueFromField(field));
				} else {
					this.valueMap.put(field, getFieldFromOrderValue(field, order.getAddress().getMiscShipTo().getPol()));
				}
				break;
			case "EXT:SolomonID":
				if (pageToOrder) {
					order.getAddress().getMiscShipTo().setSolomonId(getOrderValueFromField(field));
				} else {
					this.valueMap.put(field, getFieldFromOrderValue(field, order.getAddress().getMiscShipTo().getSolomonId()));
				}
				break;
			case "EXT:USAccount":
				if (pageToOrder) {
					//this will be overridden by the bean when creating the order in the cashout page
 					order.getAddress().getMiscShipTo().setUsAccount(getOrderValueFromField(field));
				} else {
					this.valueMap.put(field, getFieldFromOrderValue(field, order.getAddress().getMiscShipTo().getUsAccount()));
				}
				break;
			case "Header Comments":
				if (pageToOrder) {
					order.getAddress().getMiscShipTo().setHeaderComments(getOrderValueFromField(field));
				} else {
					this.valueMap.put(field, getFieldFromOrderValue(field, order.getAddress().getMiscShipTo().getHeaderComments()));
				}
				break;
			case "OldRelease":
				if (pageToOrder) {
					order.getAddress().getMiscShipTo().setOldRelease(getOrderValueFromField(field));
				} else {
					this.valueMap.put(field, getFieldFromOrderValue(field, order.getAddress().getMiscShipTo().getOldRelease()));
				}
				break;
			case "Order Email":
				if (pageToOrder) {
					//TODO: remove when the "Order Email" field is merged with the "email" field
					order.getAddress().getMiscShipTo().setOrderEmail(getOrderValueFromField(field));
				} else {
					this.valueMap.put(field, getFieldFromOrderValue(field, order.getAddress().getMiscShipTo().getOrderEmail()));
				}
				break;
			case "Street 2":
				if (pageToOrder) {
					order.getAddress().getMiscShipTo().setStreet2(getOrderValueFromField(field));
				} else {
					this.valueMap.put(field, getFieldFromOrderValue(field, order.getAddress().getMiscShipTo().getStreet2()));
				}
				break;
			case "Street 3":
				if (pageToOrder) {
					order.getAddress().getMiscShipTo().setStreet3(getOrderValueFromField(field));
				} else {
					this.valueMap.put(field, getFieldFromOrderValue(field, order.getAddress().getMiscShipTo().getStreet3()));
				}
				break;
			case "Requester Name":
				if (pageToOrder) {
					order.getAddress().getMiscShipTo().setRequesterName(getOrderValueFromField(field));
				} else {
					this.valueMap.put(field, getFieldFromOrderValue(field, order.getAddress().getMiscShipTo().getRequesterName()));
				}
				break;
			case "Requester Phone":
				if (pageToOrder) {
					order.getAddress().getMiscShipTo().setRequesterPhone(getOrderValueFromField(field));
				} else {
					this.valueMap.put(field, getFieldFromOrderValue(field, order.getAddress().getMiscShipTo().getRequesterPhone()));
				}
				break;
			}
		}
	}
        
        private boolean optionsMapContainsAddress(CashoutPageXField field) {
            if (this.valueMap.get(field) instanceof Address) {
                Set<Object> optAddresses = this.optionsMap.get(field);
                Address toCheck = (Address)this.valueMap.get(field);
                for (Object o : optAddresses) {
                    if (AddressBp.areClones((Address) o, toCheck)) {
                        return true;
                    }
                }
            }
            return false;
        }
	
	private String getFieldFromOrderValue(CashoutPageXField field, String shiptoValue) {
		return StringEscape.removeMappingLabel(shiptoValue);
	}
	
	private String getOrderValueFromField(CashoutPageXField field) {
		return this.getOrderValueFromFieldAndInput(field, (String)this.valueMap.get(field));
	}
	
	private String getOrderValueFromFieldAndInput(CashoutPageXField field, String input) {
		StringBuilder fieldBuilder = new StringBuilder();
		if (field.getIncludeLabelInMapping() != null && field.getIncludeLabelInMapping()) {
			fieldBuilder.append(field.getLabel());
			fieldBuilder.append("|");
		}
		fieldBuilder.append(input);
		return fieldBuilder.toString();
	}	
	
	private String stripSpecialCharacters(String input) {
		if (StringUtils.isNotBlank(input)) {
			for (String character : SPECIAL_CHARACTERS) {
				input = input.replace(character, " ");
			}
		}
		return input;
	}
	
	/**
	 * This method takes the name of a field, and populates the Value with an Object of the correct type (instead of a 
	 * String, which is what it's saved as).
	 * 
	 * @param fieldName
	 */
	private void convertTypeOfDefault(String fieldName) {
		CashoutPageXField field = fieldMap.get(fieldName);
		
		//if a set of options is defined, iterates through the options, and selects the one that matches
		if (optionsMap.get(field) != null && optionsMap.get(field).size() > 0 && valueMap.get(field) != null 
				&& !valueMap.get(field).toString().isEmpty()) {
			if (setFieldOptionFromString(field)) {
				return;
			}
		}
		//code for additional fields here
		
		//for a date field, sets the date to today
		if (field.getField().getFieldType().getName().equals("calendar")) {
			valueMap.put(field, new Date());
			return;
		}
		//finally, if the field is not a text field, sets the value to null
		if (!field.getField().getFieldType().getName().equals("text")) {
			valueMap.put(field, null);
		}
		
	}
	
	/**
	 * Takes a field, and selects the FieldOption associated with it that matches the value in ValueMap.
	 * 
	 * @param field
	 * @return
	 */
	private boolean setFieldOptionFromString(CashoutPageXField field) {
		for (Object option : optionsMap.get(field)) {
			FieldOptions fieldOption = (FieldOptions) option;
			if (fieldOption.getValue().equals(valueMap.get(field))) {
				valueMap.put(field, fieldOption);
				return true;
			}
		}
		return false;
	}
	
	public AssignedCostCenter createDummyCostCenter(String name, CustomerOrder order) {
		AccountXCredentialXUser searchAxcxu = new AccountXCredentialXUser();
		searchAxcxu.setAccount(new Account());
		searchAxcxu.setCredential(new Credential());
		searchAxcxu.setUser(new SystemUser());
		searchAxcxu.getAccount().setId(order.getAccount().getId());
		searchAxcxu.getCredential().setId(order.getCredential().getId());
		searchAxcxu.getUser().setId(order.getUser().getId());
		List<AccountXCredentialXUser> searchList = this.axcxuBp.searchByExactExample(searchAxcxu, 0, 0);
		if (!searchList.isEmpty()) {
			AssignedCostCenter newCostCenter = new AssignedCostCenter();
			newCostCenter.setAxcxu(searchList.get(0));
			for (AssignedCostCenter assignedCenter : costCenterDao.costCentersForCashout(newCostCenter.getAxcxu())) {
				if (name != null && name.equals(assignedCenter.getCostCenter().getName())) {
					return assignedCenter;
				}
			}
			newCostCenter.setCostCenter(new CostCenter());
			newCostCenter.getCostCenter().setName(name);
			return newCostCenter;
		}
		return null;
	}

	public CashoutPage getPage() {
		return page;
	}

	public void setPage(CashoutPage page) {
		this.page = page;
	}

	public Map<CashoutPageXField, Boolean> getRequired() {
		return requiredMap;
	}

	public void setRequired(Map<CashoutPageXField, Boolean> requiredMap) {
		this.requiredMap = requiredMap;
	}

	public Map<CashoutPageXField, Boolean> getUpdatable() {
		return updatableMap;
	}

	public void setUpdatable(Map<CashoutPageXField, Boolean> updatableMap) {
		this.updatableMap = updatableMap;
	}

	public Map<CashoutPageXField, Object> getValue() {
		return valueMap;
	}

	public void setValue(Map<CashoutPageXField, Object> valueMap) {
		this.valueMap = valueMap;
	}

	public Map<CashoutPageXField, String> getLabel() {
		return labelMap;
	}

	public void setLabel(Map<CashoutPageXField, String> labelMap) {
		this.labelMap = labelMap;
	}

	public Map<CashoutPageXField, MessageMapping> getMapping() {
		return this.mappingMap;
	}

	public void setMapping(Map<CashoutPageXField, MessageMapping> mappingMap) {
		this.mappingMap = mappingMap;
	}

	public Map<String, CashoutPageXField> getField() {
		return fieldMap;
	}

	public void setField(Map<String, CashoutPageXField> fieldMap) {
		this.fieldMap = fieldMap;
	}

    public Map<CashoutPageXField, Set<Object>> getOptions() {
		return optionsMap;
	}

	public void setOptions(Map<CashoutPageXField, Set<Object>> optionsMap) {
		this.optionsMap = optionsMap;
	}

	public Map<LineItem, String> getComment() {
		return commentMap;
	}

	public void setComment(Map<LineItem, String> commentMap) {
		this.commentMap = commentMap;
	}

	public String[] getAllowedCountryCodes() {
		return allowedCountryCodes;
	}

	public void setAllowedCountryCodes(String[] allowedCountryCodes) {
		this.allowedCountryCodes = allowedCountryCodes;
	}

	/**
	 * This method is called when the "payment type selection" dropdown has its value changed. It should clear the 
	 * values of the PO numbers and the credit cards.
	 */
	public void clearPaymentFields() {
		this.valueMap.put(this.fieldMap.get("credit card number"), "");
		this.valueMap.put(this.fieldMap.get("credit card CVV"), "");
		this.valueMap.put(this.fieldMap.get("credit card expiration"), new Date());
		this.valueMap.put(this.fieldMap.get("credit card name on card"), "");
		this.valueMap.put(this.fieldMap.get("credit card CVV indicator"), "");
	}
	
	public String costCenterToString(Object input) {
		if (input == null) {
			return "";
		}
		if (!(input instanceof AssignedCostCenter)) {
			return input.toString();
		}
		AssignedCostCenter toConvert = (AssignedCostCenter) input;
		if (toConvert.getCostCenter() == null || StringUtils.isBlank(toConvert.getCostCenter().getName())) {
			return "";
		}
		return toConvert.getCostCenter().getName();
	}
	
	public String billtoAddressToString(Address toConvert) {
		String format = DEFAULT_ADDRESS_FORMAT;
		if (StringUtils.isNotBlank(this.page.getBilltoAddressDisplayFormat())) {
			format = this.page.getBilltoAddressDisplayFormat();
		}
		return addressToString(toConvert, format);
	}
	
	public String shiptoAddressToString(Address toConvert) {
		String format = DEFAULT_ADDRESS_FORMAT;
		if (StringUtils.isNotBlank(this.page.getShiptoAddressDisplayFormat())) {
			format = this.page.getShiptoAddressDisplayFormat();
		}
		return addressToString(toConvert, format);
	}
	
	public String addressToString(Address toConvert, String format) {
		if (toConvert == null) {
			return "";
		}
		String toReturn = format;
		for (AddressFormatField field : AddressFormatField.values()) {
			toReturn = toReturn.replaceAll(field.getLabelInFormat(), StringUtils.defaultIfBlank(field.getValue(toConvert), ""));
		}
		//if there are any duplicate spaces because of missing values, cleans them up 
		toReturn.replaceAll("  ", " ");
 		return toReturn;
	}
	
	@Inject
	private ZipSearchBean zipSearchBean;
	
	private static final String INVALID_ZIP = "invalid";
	
	//sets the default value of the entered zip string to the invalid zip
	private String shipZipString = INVALID_ZIP;
	
	private int shipZipIndex = 0;
	
	//
	private boolean shipZipSet = false;

	//sets the default value of the entered zip string to the invalid zip
	private String billZipString = INVALID_ZIP;
	
	private int billZipIndex = 0;
	
	private boolean billZipSet = false;
	
	public void setShipZip(String value) {
		//do nothing, set during validation
	}
	
	public String getShipZip() {
		return (String)this.valueMap.get(this.fieldMap.get("ship zip"));
	}
	
	public void setBillZip(String value) {
		//do nothing, set during validation
	}
	
	public String getBillZip() {
		return (String)this.valueMap.get(this.fieldMap.get("bill zip"));
	}
	
	public void shipZipValidator(FacesContext context, UIComponent component, Object objectValue) {
		zipValidator(context, component, (String) objectValue, "ship");
	}
	
	public void billZipValidator(FacesContext context, UIComponent component, Object objectValue) {
		zipValidator(context, component, (String) objectValue, "bill");
	}
	
	public boolean billingInUs() {
		return !(nonUSCountry(this.getValue().get(this.getField().get("bill us and canada"))) || nonUSCountry(this.getValue().get(this.getField().get("bill north america"))));
	}
	
	public boolean shippingInUs() {
		return !(nonUSCountry(this.getValue().get(this.getField().get("ship us and canada"))) || nonUSCountry(this.getValue().get(this.getField().get("ship north america"))));
	}
	
	public boolean nonUSCountry(Object selectedCountry){
		if (selectedCountry == null || StringUtils.isBlank(selectedCountry.toString()) || "US".equals(selectedCountry.toString())) {
			return false;
		}
		return true;
	}
	
	private void zipValidator(FacesContext context, UIComponent component, String value, String addressType) {
		boolean addressInUs = true;
		try {
			//if the entered zip strings haven't been set, but the form is submitted, uses the value in the field
			if (addressType.equals("ship")) {
				if (shipZipString.equals(INVALID_ZIP)) {
					shipZipString = value;
				}
				addressInUs = this.shippingInUs();
			}
			else if (addressType.equals("bill")) {
				if (billZipString.equals(INVALID_ZIP)) {
					billZipString = value;
				}
				addressInUs = this.billingInUs();
			}
			//then, finds if an index is specified. If there isn't, uses the previously specified index, if one has been set
			if (!value.contains(":") && addressType.equals("ship") && shipZipSet) {
				value = shipZipString.concat(":").concat("" + shipZipIndex);
			}
			else if (!value.contains(":") && addressType.equals("bill") && billZipSet) {
				value = billZipString.concat(":").concat("" + billZipIndex);
			}
			//searches based on the zip and the index
			String[] values = value.split(":");
			//the length is greater than 1 if an index is specified. If no index is specified, does not set city and state
			if (values.length > 1) {
				ZipSelection zipSelection = zipSearchBean.zipSearch(values[0]).get(Integer.parseInt(values[1]));
				this.valueMap.put(this.fieldMap.get(addressType + " zip"), zipSelection.getZip());
				this.valueMap.put(this.fieldMap.get(addressType + " city"), zipSelection.getCity());
				this.valueMap.put(this.fieldMap.get(addressType + " states"), zipSelection.getState());
				if (addressType.equals("ship")) {
					shipZipString = values[0];
					shipZipIndex = Integer.parseInt(values[1]);
					shipZipSet = true;
				}
				else if (addressType.equals("bill")) {
					billZipString = values[0];
					billZipIndex = Integer.parseInt(values[1]);
					billZipSet = true;
				}
			}
			return;
		}
		catch (NumberFormatException | IndexOutOfBoundsException e) {
			if (addressInUs) {
		        ((UIInput) component).setValid(false);
		        context.addMessage(component.getClientId(), new FacesMessage(FacesMessage.SEVERITY_ERROR, "Enter a valid zip code", "Enter a valid zip code"));
				this.valueMap.put(this.fieldMap.get(addressType + " zip"), null);
				this.valueMap.put(this.fieldMap.get(addressType + " city"), null);
				this.valueMap.put(this.fieldMap.get(addressType + " states"), null);
			}
		}
	}
	
	public void countryCodeValidator(FacesContext context, UIComponent component, Object value) {
		if (value instanceof FieldOptions && this.isCountryCodeValid(((FieldOptions) value).getValue())) {
			return;
		}
		((UIInput) component).setValid(false);
        context.addMessage(component.getClientId(), new FacesMessage(FacesMessage.SEVERITY_ERROR, 
        		"Country not permitted", "Country not permitted"));
	}
	
	public void existingAddressValidator(FacesContext context, UIComponent component, Object value) {
		if (value instanceof Address && this.isCountryCodeValid(((Address) value).getCountry())) {
			return;
		}
		((UIInput) component).setValid(false);
        context.addMessage(component.getClientId(), new FacesMessage(FacesMessage.SEVERITY_ERROR, 
        		"Country not permitted", "Country not permitted"));
	}
	
	private boolean isCountryCodeValid(String enteredCountryCode) {
		if (StringUtils.isBlank(enteredCountryCode) || "US".equals(enteredCountryCode)) {
			return true;
		}
		for (String allowedCode : this.allowedCountryCodes) {	
			if (!StringUtils.isBlank(allowedCode) && allowedCode.equals(enteredCountryCode)) {
				return true;
			}
		}
		return false;
	}
    
    public OrderOrigin[] getOrderOrigins() {
        return CustomerOrder.OrderOrigin.values();
    }

    public SelectItem[] getCvvIndicators() {
        SelectItem[] items = new SelectItem[CvvIndicator.values().length];
        int i = 0;
        for (CvvIndicator g : CvvIndicator.values()) {
            items[i++] = new SelectItem(g, g.getXmlValue().value());
        }
        return items;
    }
    
    public Map<LineItem, String> getPriceChangeNoticeMap() {
    	return this.priceChangeNoticeMap;
    }
    
    public void setPriceChangeNoticeMap(Map<LineItem, String> priceChangeNoticeMap) {
    	this.priceChangeNoticeMap = priceChangeNoticeMap;
    }
}
