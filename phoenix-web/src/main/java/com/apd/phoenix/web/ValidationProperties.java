package com.apd.phoenix.web;

import java.util.Properties;

@Deprecated
public class ValidationProperties extends PropertiesLoader {

    private static Properties props = PropertiesLoader.getAsProperties("validation.properties");

    public static String get(String key) {
        return props.getProperty(key);
    }
}
