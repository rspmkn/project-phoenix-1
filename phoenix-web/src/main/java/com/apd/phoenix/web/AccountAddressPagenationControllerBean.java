package com.apd.phoenix.web;

import java.util.ArrayList;
import java.util.List;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.inject.Inject;

import com.apd.phoenix.core.PageNumberDisplay;
import com.apd.phoenix.service.business.AccountBp;
import com.apd.phoenix.service.business.AddressBp;
import com.apd.phoenix.service.model.Account;
import com.apd.phoenix.service.model.Address;
import com.apd.phoenix.service.model.AddressXAddressPropertyType;

public abstract class AccountAddressPagenationControllerBean extends AbstractControllerBean<Account> {
	@Inject
    protected AccountBp bp;
	
    @Inject
    protected AddressBp addressBp;
    
	protected Account currentInstance = new Account();
	private List<Address> addressPageItems;
    private PageNumberDisplay addressPageNumberDisplay;

	public Converter getConverter() {

        return new Converter() {

            @Override
            public Object getAsObject(FacesContext context, UIComponent component, String value) {
                return bp.findById(Long.valueOf(value), Account.class);
            }

            @Override
            public String getAsString(FacesContext context, UIComponent component, Object value) {

                if (value == null) {
                    return "";
                }

                return String.valueOf(((Account) value).getId());
            }
        };
    }
	
	public List<Address> paginateAddresses() {
        //Don't fetch addresses unless there is an actual account
        if (this.getAccount().getId() == null) {
            return getAddressPageItems();
        }
        setAddressPageItems(this.addressBp.getAddressesForAccountPagenated(this.getAccount().getId(), (this
                .getAddressPageNumberDisplay().getPage() - 1)
                * this.getAddressPageNumberDisplay().getPageSize().toInt(), this.getAddressPageNumberDisplay()
                .getPageSize().toInt()));
        return addressPageItems;

    }
	
	/*
     * When the account is a new account, there will be a small enough number of addresses 
     * that they can all be loaded safely into memory.
     */
    @SuppressWarnings("deprecation")
    public void addAddress() {
        this.currentAddress.setAccount(this.currentInstance);
        if (this.currentInstance.getId() == null) {
            this.currentInstance.getAddresses().add(this.currentAddress);
            this.setAddressPageItems(new ArrayList<Address>(this.currentInstance.getAddresses()));
        }
        else {
            if (this.currentAddress.getId() == null) {
                addressBp.create(this.currentAddress);
            }
            else {
                addressBp.update(currentAddress);
            }
        }
        this.currentAddress = new Address();
    }

    private Address currentAddress = new Address();
    
    public void addAddressField() {
        AddressXAddressPropertyType fieldToAdd = new AddressXAddressPropertyType();
        this.currentAddress.getFields().add(fieldToAdd);
        fieldToAdd.setAddress(this.currentAddress);
    }
    
    public void createAddress() {
        this.currentAddress = new Address();
    }

    public Address getCurrentAddress() {
        return currentAddress;
    }

    public void setCurrentAddress(Address currentAddress) {
        this.currentAddress = addressBp.findById(currentAddress.getId(), Address.class);
    }
    
	protected void populatePageNumberDisplay() {
        if (this.currentInstance != null && this.currentInstance.getId() != null) {
            this.addressPageNumberDisplay = new PageNumberDisplay(addressBp.numberOfAddresses(this.currentInstance
                    .getId()));
        }
    }
	
	public List<Address> getAddressPageItems() {
        if (this.getAccount().getId() != null) {
            return this.paginateAddresses();
        }
        else {
            return addressPageItems == null ? new ArrayList<Address>() : addressPageItems;
        }
    }

    public void setAddressPageNumberDisplay(PageNumberDisplay addressPageNumberDisplay) {
        this.addressPageNumberDisplay = addressPageNumberDisplay;
    }
    public PageNumberDisplay getAddressPageNumberDisplay() {
        if (this.addressPageNumberDisplay == null) {
            //Must be large enough to trigger a second page for pagenation to work.
            addressPageNumberDisplay = new PageNumberDisplay(6);
        }
        return addressPageNumberDisplay;
    }
    
    public Account getAccount() {
        return this.currentInstance;
    }
    
    public void setAccount(Account account){
    	this.currentInstance = account;
    }
    
    public void removeAddress(Address toRemove) {
        toRemove.setAccount(null);
        addressBp.update(toRemove);
    }

	public void setAddressPageItems(List<Address> addressPageItems) {
		this.addressPageItems = addressPageItems;
	}

}
