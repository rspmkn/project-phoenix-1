package com.apd.phoenix.web.cashout;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.ConversationScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.apd.phoenix.service.business.MilitaryZipBp;
import com.apd.phoenix.service.business.TaxRateBp;
import com.apd.phoenix.service.model.MilitaryZip;
import com.apd.phoenix.service.model.TaxRate;

@Named
@ConversationScoped
public class ZipSearchBean implements Serializable {

    private static final Logger LOGGER = LoggerFactory.getLogger(ZipSearchBean.class);

    @Inject
    private TaxRateBp taxRateBp;

    @Inject
    private MilitaryZipBp militaryZipBp;

    private static final long serialVersionUID = -5758011397561170407L;

    private boolean allowMilitary = false;

    private String searchString;

    public boolean isAllowMilitary() {
        return allowMilitary;
    }

    public void setAllowMilitary(boolean allowMilitary) {
        this.allowMilitary = allowMilitary;
    }

    public List<ZipSelection> zipSearch(String entered) {
		searchString = entered;
		if (StringUtils.isBlank(entered)) {
			return new ArrayList<>();
		}
		int index = 0;
		List<ZipSelection> toReturn = new ArrayList<>();
		for (TaxRate rate : taxRateBp.getTaxRates(entered)) {
			ZipSelection selection = new ZipSelection();
			selection.setCity(rate.getCity());
			selection.setState(rate.getState());
			selection.setZip(rate.getZip());
			selection.setCounty(rate.getCounty());
			selection.setGeocode(rate.getGeocode());
			selection.setIndex(index);
			index++;
			toReturn.add(selection);
		}
		if (allowMilitary) {
			for (MilitaryZip zip : militaryZipBp.getMilitaryZips(entered)) {
				ZipSelection selection = new ZipSelection();
				selection.setCity(zip.getCity());
				selection.setState(zip.getState());
				selection.setZip(zip.getZip());
				selection.setCounty(zip.getCounty());
				selection.setIndex(index);
				index++;
				toReturn.add(selection);
			}
		}
		return toReturn;
	}

    public ZipSelection getSelection() {
        LOGGER.info("Getting selection!");
        return null;
    }

    public void setSelection(ZipSelection selection) {
        LOGGER.info("Setting selection!");
    }

    public class ZipSelection implements Serializable {

        private static final long serialVersionUID = -7508442356259018335L;

        private String zip;
        private String city;
        private String state;
        private String county;
        private String geocode;
        private int index = 0;

        public String getZip() {
            return zip;
        }

        public void setZip(String zip) {
            this.zip = zip;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }

        public String getCounty() {
            return county;
        }

        public void setCounty(String county) {
            this.county = county;
        }

        public String getGeocode() {
            return geocode;
        }

        public void setGeocode(String geocode) {
            this.geocode = geocode;
        }

        public int getIndex() {
            return index;
        }

        public void setIndex(int index) {
            this.index = index;
        }

        public String getSearchString() {
            return searchString;
        }

        @Override
        public String toString() {
            return zip;
        }
    }
}
