package com.apd.phoenix.web.searchbeans;

import java.util.List;

import javax.enterprise.context.ConversationScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.apd.phoenix.service.persistence.jpa.AddressTypeDao;
import com.apd.phoenix.service.model.AddressType;

import java.io.Serializable;

@Named
@ConversationScoped
public class AddressTypeSearchBean extends AbstractSearchBean<AddressType> implements Serializable {

    @Inject
    AddressTypeDao dao;

    @Override
    public String[] getCriteria() {
        String[] toReturn = { "Name" };
        return toReturn;
    }

    @Override
    protected List<AddressType> search() {
        AddressType search = new AddressType();
        search.setName(this.getValues().get(0).getValue());
        return dao.searchByExample(search, 0, 0);
    }

}
