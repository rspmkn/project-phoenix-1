package com.apd.phoenix.web.searchbeans;

import java.io.Serializable;
import java.util.List;
import javax.enterprise.context.ConversationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import com.apd.phoenix.service.business.PermissionBp;
import com.apd.phoenix.service.model.Permission;

@Named
@ConversationScoped
public class PermissionSearchBean extends AbstractSearchBean<Permission> implements Serializable {

    @Inject
    PermissionBp bp;

    @Override
    public String[] getCriteria() {
        String[] toReturn = { "Name" };
        return toReturn;
    }

    @Override
    protected List<Permission> search() {
        Permission search = new Permission();
        search.setName(this.getValues().get(0).getValue());
        return bp.searchByExample(search, 0, 0);
    }

}
