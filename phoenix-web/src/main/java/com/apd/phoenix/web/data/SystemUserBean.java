package com.apd.phoenix.web.data;

import java.io.File;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.Set;

import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateful;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.ConversationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.model.SelectItem;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.commons.lang.StringUtils;
import org.richfaces.event.FileUploadEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.apd.phoenix.core.EncryptionUtils;
import com.apd.phoenix.core.utility.PropertiesLoader;
import com.apd.phoenix.core.utility.TenantConfigRepository;
import com.apd.phoenix.service.business.AccountBp;
import com.apd.phoenix.service.business.EntityComparator;
import com.apd.phoenix.service.business.PasswordResetEntryBp;
import com.apd.phoenix.service.business.PhoneNumberBp;
import com.apd.phoenix.service.business.RoleBp;
import com.apd.phoenix.service.business.DuplicatePasswordException;
import com.apd.phoenix.service.business.SystemUserBp;
import com.apd.phoenix.service.model.Account;
import com.apd.phoenix.service.model.AccountXCredentialXUser;
import com.apd.phoenix.service.model.Address;
import com.apd.phoenix.service.model.AddressXAddressPropertyType;
import com.apd.phoenix.service.model.AssignedCostCenter;
import com.apd.phoenix.service.model.AssignedDepartment;
import com.apd.phoenix.service.model.Bulletin;
import com.apd.phoenix.service.model.CashoutPageXField;
import com.apd.phoenix.service.model.CostCenter;
import com.apd.phoenix.service.model.Credential;
import com.apd.phoenix.service.model.Desktop;
import com.apd.phoenix.service.model.LimitApproval;
import com.apd.phoenix.service.model.PaymentInformation;
import com.apd.phoenix.service.model.PermissionXUser;
import com.apd.phoenix.service.model.Person;
import com.apd.phoenix.service.model.PhoneNumber;
import com.apd.phoenix.service.model.RoleXUser;
import com.apd.phoenix.service.model.SystemUser;
import com.apd.phoenix.service.model.SystemUser.CatalogSelectionType;
import com.apd.phoenix.service.persistence.jpa.AccountXCredentialXUserDao;
import com.apd.phoenix.service.persistence.jpa.AssignedCostCenterDao;
import com.apd.phoenix.service.persistence.jpa.CostCenterDao;
import com.apd.phoenix.service.persistence.jpa.CredentialDao;
import com.apd.phoenix.service.persistence.jpa.DepartmentDao;
import com.apd.phoenix.service.persistence.jpa.PersonDao;
import com.apd.phoenix.service.persistence.multitenancy.impl.CurrentTenantIdentifierResolverImpl;
import com.apd.phoenix.service.utility.CsvExportService;
import com.apd.phoenix.service.utility.FileDownloader;
import com.apd.phoenix.web.AbstractControllerBean;
import com.apd.phoenix.web.ValidationProperties;
import com.apd.phoenix.web.searchbeans.AccountXUserSearchBean;
import com.apd.phoenix.web.searchbeans.CostCenterSearchBean;
import com.apd.phoenix.web.searchbeans.DepartmentSearchBean;
import com.apd.phoenix.web.searchbeans.PermissionSearchBean;
import com.apd.phoenix.web.searchbeans.RoleSearchBean;
import com.apd.phoenix.web.searchbeans.UserSearchBean;

/**
 * Backing bean for SystemUser entities.
 * <p>
 * This class provides CRUD functionality for all SystemUser entities. It focuses
 * purely on Java EE 6 standards (e.g. <tt>&#64;ConversationScoped</tt> for
 * state management, <tt>PersistenceContext</tt> for persistence,
 * <tt>CriteriaBuilder</tt> for searches) rather than introducing a CRUD framework or
 * custom base class.
 */

@Named
@Stateful
@ConversationScoped
public class SystemUserBean extends AbstractControllerBean<SystemUser> implements Serializable {

    private static final long serialVersionUID = 1L;

    private static final Logger LOG = LoggerFactory.getLogger(SystemUserBean.class);

    /*
     * Support creating and retrieving SystemUser entities
     */
    public void validateSelection(FacesContext context, UIComponent component, Object value) {
        if (this.systemUser.getId() == null) {
            ((UIInput) component).setValid(false);
            FacesMessage message = new FacesMessage();
            message.setSeverity(FacesMessage.SEVERITY_ERROR);
            message.setSummary("Select User to edit");
            message.setDetail("You need to select a User to edit.");
            context.addMessage(component.getClientId(), message);
            return;
        }
    }

    private Long id;

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    private SystemUser systemUser = new SystemUser();

    public SystemUser getSystemUser() {
        return this.systemUser;
    }

    private Account account;

    public Account getAccount() {
        return this.account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    private Credential credential = new Credential();

    public Credential getCredential() {
        return this.credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    @Inject
    private SystemUserBp bp;

    @Inject
    private AccountBp accountBp;

    @Inject
    private PersonDao personDao;

    @Inject
    private CredentialDao credentialDao;

    @Inject
    private AccountXCredentialXUserDao accountXcredentialXuserDao;

    @Inject
    private UserSearchBean userSearchBean;

    @Inject
    private PermissionSearchBean permissionSearchBean;

    @Inject
    private RoleSearchBean roleSearchBean;

    @Inject
    private AccountXUserSearchBean accountUserSearchBean;

    @Inject
    private Conversation conversation;
    
    @Inject
    private CostCenterDao costCenterDao;
    
    @Inject
    private DepartmentDao departmentDao;
    
    @Inject
    private AssignedCostCenterDao assignedCostCenterDao;

    @Inject
    protected CsvExportService csvService;
    
    @Inject
    private RoleBp roleBp;
    
    @Inject
    private PasswordResetEntryBp passwordResetEntryBp;
    
    private boolean addSelectedPaymentInfo = false;

    private PhoneNumber currentNumber = new PhoneNumber();

    private PermissionXUser currentPermission = new PermissionXUser();

    private RoleXUser currentRole = new RoleXUser();

    private Address currentAddress = new Address();
	
    private Bulletin currentBulletin = new Bulletin();

    private AccountXCredentialXUser currentAccXCredXUser = new AccountXCredentialXUser();

    private AssignedCostCenter currentCostCenter = new AssignedCostCenter();
    
    private AssignedDepartment currentDepartment = new AssignedDepartment();

    // The Payment Information currently being edited.
    private PaymentInformation currentPaymentInformation = new PaymentInformation();
    
    private LimitApproval currentLimitApproval = new LimitApproval();

    private AccountXCredentialXUser searchInstance;

    private SystemUser currentUser;

    private List<AccountXCredentialXUser> existingAccountXCredentialXUser = new ArrayList<AccountXCredentialXUser>();

    private List<AccountXCredentialXUser> axcxuToRemove = new ArrayList<>();
    
    private String defaultPassword="";
    
    private AddressOverrideOption shipAddressOption = AddressOverrideOption.USE_ACCOUNT;
    
    private AddressOverrideOption billAddressOption = AddressOverrideOption.USE_ACCOUNT;

    public void setExistingAccountXCredentialXUser(List<AccountXCredentialXUser> instance) {
        this.existingAccountXCredentialXUser = instance;
    }

    public List<AccountXCredentialXUser> getExistingAccountXCredentialXUser() {
        return this.existingAccountXCredentialXUser;
    }

    private List<Credential> accountCredentials;

    public void setAccountCredentials(List<Credential> credentials) {
        this.accountCredentials = credentials;
    }

    public List<Credential> getAccountCredentials() {
        return this.accountCredentials;
    }

    private AccountXCredentialXUser accountXcredentialXuser;

    @Override
    public String create() {
        this.conversation.begin();
        return "userAdd?faces-redirect=true";
    }

    @Override
    public void retrieve() {

        if (this.userSearchBean.getSelection() != null) {
            this.systemUser = this.bp.findById(userSearchBean.getSelection().getId(), SystemUser.class);
            eagerLoad();
            this.findExistingAccountXCredentialXUser(false);
            this.userSearchBean.setSelection(null);
        }

        if (this.accountUserSearchBean.getSelection() != null) {
            this.systemUser.getAccounts().add(this.accountUserSearchBean.getSelection());
            this.accountUserSearchBean.setSelection(null);
        }

        if (FacesContext.getCurrentInstance().isPostback()) {
            return;
        }

        if (this.conversation.isTransient()) {
            this.conversation.begin();
            if (this.systemUser.getId() == null) {
            	this.systemUser = roleBp.populateUserDefaults(this.systemUser);
            }
        }
    }

    /**
     * Support creating and updating SystemUser entities. Following steps are taken:
     * 
     * If creating a new user
     * 	- Set the user as a Person due to one-to-one mapping
     * 	- Persist the user into DB
     * 	- Associate all credentials with user
     * 
     * If updating the user
     * 	- Disassociate all credentials from user
     * 	- Update user with disassociation
     * 	- Re-associate newly assigned credentials with user
     * 
     * TBD: Need distinguish the maintenance of system user and person. For now, a system user is always created
     * with the person. In future, an indicator is required to mandate the either system user or person
     * 
     * @return
     */
    @Override
    public String update() {
        if (!validToCreate()) {
            return null;
        }
        
        String toReturn = null;

        try {
            if (this.systemUser.getId() == null) {
                this.bp.create(this.systemUser);
                toReturn = "userAdd?faces-redirect=true";
            }
            else {
                this.bp.update(this.systemUser);
                toReturn = "userEdit?faces-redirect=true";
            }

            this.associateCrednetialToUser();
            if (!this.postPersistValid()) {
                eagerLoad();
                return null;
            }
            else {
	            if (!this.conversation.isTransient()) {
	                this.conversation.end();
	            }
	            return toReturn;
            }
        }
        catch (Exception e) {
        	LOG.error(e.getMessage());
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, "Unexpected Error", "Unexpected Error"));
            return null;
        }
    }

    /**
     * Support creating and updating SystemUser entities. Following steps are taken:
     * 
     * - Disassociate all credentials from user
     * - Update user with disassociation
     * - Persist person entity with deleted user
     * - Delete user from DB
     * - Delete person from DB
     * 
     * There is one-to-one between system user and person. And a person is the owning entity.
     * By deleting a system user, it could be:
     * - Both system user and person are removed
     * 	OR
     * - The system user is being converted to person
     * 
     * For now assume both system user and entity are removed
     * 
     * @return
     */
    @Override
    public String delete() {
        this.conversation.end();
        try {
            this.deleteCredentials();
            this.bp.update(this.systemUser);
            Person person = this.systemUser.getPerson();
            this.personDao.update(person);
            bp.delete(this.systemUser.getId(), SystemUser.class);
            this.personDao.delete(person.getId(), Person.class);
            return "userEdit?faces-redirect=true";
        }
        catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), e.getMessage()));
            return null;
        }
    }

    /**
     * Listen for file uploading event and invoke bulk user creation
     * @param event 
     * @throws Exception 
     */
    public void listener(FileUploadEvent event) throws Exception {

        try (InputStream stream = event.getUploadedFile().getInputStream()) {
        	Set<AccountXCredentialXUser> credSet = new HashSet<AccountXCredentialXUser>();
        	credSet.addAll(this.existingAccountXCredentialXUser);

        	boolean overrideDefaultShipping = this.shipAddressOption.equals(AddressOverrideOption.USE_ACCOUNT_NONE_SPECIFIED) || this.shipAddressOption.equals(AddressOverrideOption.USE_GIVEN_NONE_SPECIFIED);
        	boolean overrideDefaultBilling = this.billAddressOption.equals(AddressOverrideOption.USE_ACCOUNT_NONE_SPECIFIED) || this.billAddressOption.equals(AddressOverrideOption.USE_GIVEN_NONE_SPECIFIED);

        	if (this.shipAddressOption.equals(AddressOverrideOption.USE_ACCOUNT) || this.shipAddressOption.equals(AddressOverrideOption.USE_ACCOUNT_NONE_SPECIFIED)) {
        		Set<Address> toRemove = new HashSet<>();
        		for (Address address : this.systemUser.getPerson().getAddresses()) {
        			if (address.getType().getName().equals("SHIPTO")) {
        				toRemove.add(address);
        			}
        		}
        		this.systemUser.getPerson().getAddresses().removeAll(toRemove);
        		for (Account account : this.systemUser.getAccounts()) {
        			for (Address address : account.getAddresses()) {
        				if (address.getType().getName().equals("SHIPTO")) {
        					this.systemUser.getPerson().getAddresses().add(address);
        				}
        			}
        		}
        	}
        	if (this.billAddressOption.equals(AddressOverrideOption.USE_ACCOUNT) || this.billAddressOption.equals(AddressOverrideOption.USE_ACCOUNT_NONE_SPECIFIED)) {
        		Set<Address> toRemove = new HashSet<>();
        		for (Address address : this.systemUser.getPerson().getAddresses()) {
        			if (address.getType().getName().equals("BILLTO")) {
        				toRemove.add(address);
        			}
        		}
        		this.systemUser.getPerson().getAddresses().removeAll(toRemove);
        		for (Account account : this.systemUser.getAccounts()) {
        			for (Address address : account.getAddresses()) {
        				if (address.getType().getName().equals("BILLTO")) {
        					this.systemUser.getPerson().getAddresses().add(address);
        				}
        			}
        		}
        	}
        	
            List<String> errors = bp.createFromCsvWithDefault(stream, this.systemUser, credSet, this.defaultPassword, overrideDefaultShipping, overrideDefaultBilling);
            for (String error : errors) {
                FacesContext.getCurrentInstance().addMessage(null,
                        new FacesMessage(FacesMessage.SEVERITY_ERROR, error, error));
            }
        }
        catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), e.getMessage()));
        }
    }

    public void addAccount() {
        if (this.account == null)
            this.account = new Account();
        this.systemUser.getAccounts().add(this.account);
    }

    public void removeAccount(Account account) {
        if (this.existingAccountXCredentialXUser != null) {
            Iterator<AccountXCredentialXUser> iterator = existingAccountXCredentialXUser.iterator();
            while (iterator.hasNext()) {
            	AccountXCredentialXUser axcxu = iterator.next();
                if (account.getName().equalsIgnoreCase(axcxu.getAccount().getName())) {
                	this.axcxuToRemove.add(axcxu);
                    iterator.remove();
                }
            }
        }
        this.systemUser.getAccounts().remove(account);
    }

    /**
     * Find all available credentials associated with the root account 
     * 
     */
    public void findAccountCredentials() {
        if (this.account != null) {
            Account childAccount;
            Account rootAccount = new Account();
            if (this.account.getRootAccount() == null) {
                rootAccount.setName(this.account.getName());
            }
            else {
                childAccount = accountBp.findById(this.account.getId(), Account.class);
                rootAccount.setName(childAccount.getRootAccount().getName());
            }
            this.credential.setRootAccount(rootAccount);
            this.accountCredentials = credentialDao.searchByExample(this.credential, 0, 0);
        }
    }

    /**
     * Add the credential to the list so it will be rendered in credential modal 
     * 
     * @param credential
     */
    public void addAccountXCredential(Credential credential) {
        if (canAddCredential(credential)) {
            this.accountXcredentialXuser = new AccountXCredentialXUser();
            this.accountXcredentialXuser.setUser(this.systemUser);
            this.accountXcredentialXuser.setAccount(this.account);
            this.accountXcredentialXuser.setCredential(credential);
            this.existingAccountXCredentialXUser.add(this.accountXcredentialXuser);
        }
    }

    /**
     * Remove the credential from the list
     * @param instance 
     * 
     * @param credential
     */
    public void removeAccountXCredential(AccountXCredentialXUser instance) {
        this.existingAccountXCredentialXUser.remove(instance);
        this.axcxuToRemove.add(instance);
    }

    /**
     * Retrieve all credentials associated with the current user, including non-active credentials.
     * 
     */
    private List<AccountXCredentialXUser> findExistingAccountXCredentialXUser(boolean updateOrDeleteUser) {
        if (this.searchInstance == null)
            this.searchInstance = new AccountXCredentialXUser();
        if (this.currentUser == null)
            this.currentUser = new SystemUser();
        this.currentUser.setId(this.systemUser.getId());
        this.searchInstance.setUser(currentUser);
        List<AccountXCredentialXUser> foundItems = this.accountXcredentialXuserDao
                .searchByExample(searchInstance, 0, 0);
        List<AccountXCredentialXUser> toReturn = new ArrayList<>();
        if (!updateOrDeleteUser) {
        	existingAccountXCredentialXUser = new ArrayList<AccountXCredentialXUser>();
            for (AccountXCredentialXUser c : foundItems) {
                c = this.accountXcredentialXuserDao.eagerLoad(c);
                this.existingAccountXCredentialXUser.add(c);
                toReturn.add(c);
            }
        }
        return toReturn;
    }

    /**
     * Iterate the account/credential/user list, create the instance and persist into DB
     * 
     */
    private void associateCrednetialToUser() {
        if (this.existingAccountXCredentialXUser != null && this.existingAccountXCredentialXUser.size() > 0) {
            for (AccountXCredentialXUser c : this.existingAccountXCredentialXUser) {
            	for (AssignedCostCenter center : c.getAllowedCostCenters()) {
            		center.setCostCenter(costCenterDao.findById(center.getCostCenter().getId(), CostCenter.class));
            	}
                this.accountXcredentialXuserDao.update(c);
            }
        }
        for (AccountXCredentialXUser c : this.axcxuToRemove) {
        	for (AssignedCostCenter center : c.getAllowedCostCenters()) {
        		center.setCostCenter(costCenterDao.findById(center.getCostCenter().getId(), CostCenter.class));
        	}
            if (c.getId() != null) {
                this.accountXcredentialXuserDao.delete(c.getId(), AccountXCredentialXUser.class);
            }
        }
    }

    /**
     * Iterate the account/credential/user list, delete the instance
     * 
     */
    private void deleteCredentials() {
        List<AccountXCredentialXUser> results = this.findExistingAccountXCredentialXUser(true);
        for (AccountXCredentialXUser c : results) {
            this.accountXcredentialXuserDao.delete(c.getId(), AccountXCredentialXUser.class);
        }
    }

    public boolean canAddCredential(Credential credential) {
        if (this.existingAccountXCredentialXUser != null && this.existingAccountXCredentialXUser.size() > 0) {
            for (AccountXCredentialXUser c : this.existingAccountXCredentialXUser) {
                if (credential.getRootAccount().getName().equalsIgnoreCase(c.getAccount().getName())
                        && credential.getName().equalsIgnoreCase(c.getCredential().getName()))
                    return false;
            }
        }
        return true;
    }

    public String cantAddCredentialMessage(Credential credential) {
        return "Already assigned";
    }

    private void findParentAccountFromChildAccount() {
        if (this.systemUser.getAccounts().size() > 0) {
            for (Account c : this.systemUser.getAccounts()) {
                if (c.getParentAccount() != null) {
                    c.getParentAccount().getName();
                }
                if (c.getRootAccount() != null) {
                	c.getRootAccount().getName();
                }
            }
        }
    }

    /*
     * Support searching SystemUser entities with pagination
     */
    
    @Inject
    private PhoneNumberBp phoneNumberBp;

	public String getPhoneNumberReadable() {
		return phoneNumberBp.getReadableNumber(this.currentNumber);
	}

	public void setPhoneNumberReadable(String phoneNumberReadable) {
		this.phoneNumberBp.setReadableNumber(this.currentNumber, phoneNumberReadable);
	}

    public String getPhoneNumberReadableRegex() {
        return this.phoneNumberBp.getReadableNumberRegex();
    }

    public void removePhoneNumber(PhoneNumber toRemove) {
        this.systemUser.getPerson().getPhoneNumbers().remove(toRemove);
    }

    public void addPhoneNumber() {
        this.systemUser.getPerson().getPhoneNumbers().add(this.currentNumber);
        this.currentNumber.setPerson(this.systemUser.getPerson());
        this.currentNumber = new PhoneNumber();
    }

    public void createPhoneNumber() {
        this.currentNumber = new PhoneNumber();
    }

    public PhoneNumber getCurrentNumber() {
        return currentNumber;
    }

    public void setCurrentNumber(PhoneNumber currentNumber) {
        this.currentNumber = currentNumber;
    }

    public void addPermission() {
        this.currentPermission.setPermission(permissionSearchBean.clearSelection());
        this.systemUser.getPermissionXUsers().add(this.currentPermission);
        this.createPermission();
    }

    public void createPermission() {
        permissionSearchBean.clearSelection();
        this.currentPermission = new PermissionXUser();
    }

    public PermissionXUser getCurrentPermission() {
        return currentPermission;
    }

    public void setCurrentPermission(PermissionXUser currentPermission) {
        this.currentPermission = currentPermission;
        this.permissionSearchBean.setSelection(currentPermission.getPermission());
    }

    public void removePermission(PermissionXUser toRemove) {
        this.systemUser.getPermissionXUsers().remove(toRemove);
    }

    public void addRole() {
        this.currentRole.setRole(roleSearchBean.clearSelection());
        this.systemUser.getRoleXUsers().add(this.currentRole);
        this.createRole();
    }

    public void createRole() {
        roleSearchBean.clearSelection();
        this.currentRole = new RoleXUser();
    }

    public RoleXUser getCurrentRole() {
        return currentRole;
    }

    public void setCurrentRole(RoleXUser currentRole) {
        this.currentRole = currentRole;
        this.roleSearchBean.setSelection(currentRole.getRole());
    }

    public void removeRole(RoleXUser toRemove) {
        this.systemUser.getRoleXUsers().remove(toRemove);
    }

    public void removeAddress(Address toRemove) {
        this.systemUser.getPerson().getAddresses().remove(toRemove);
    }

    public void addAddressField() {
        AddressXAddressPropertyType fieldToAdd = new AddressXAddressPropertyType();
        this.currentAddress.getFields().add(fieldToAdd);
        fieldToAdd.setAddress(this.currentAddress);
    }

    public void addAddress() {
        this.systemUser.getPerson().getAddresses().add(this.currentAddress);
        this.currentAddress.setPerson(this.systemUser.getPerson());
        this.currentAddress = new Address();
    }

    public void createAddress() {
        this.currentAddress = new Address();
    }

    public Address getCurrentAddress() {
        return this.currentAddress;
    }

    public void setCurrentAddress(Address address) {
        this.currentAddress = address;
    }

    public PaymentInformation getCurrentPaymentInformation() {
        return this.currentPaymentInformation;
    }

    public void setCurrentPaymentInformation(PaymentInformation paymentInformation) {
        this.currentPaymentInformation = paymentInformation;
    }

    /**
     * This method is called when the user presses the "Edit Payment Information" button, and creates 
     * a new Payment Information or presents the existing Payment Information.
     */
    public void fetchPaymentInformation() {
        if (this.systemUser.getPaymentInformation() == null) {
            this.currentPaymentInformation = new PaymentInformation();
        }
        else {
            this.currentPaymentInformation = this.systemUser.getPaymentInformation();
        }
    }
    
    public void togglePaymentInfoAddress() {
    	if (this.currentPaymentInformation.getBillingAddress() == null) {
    		this.currentPaymentInformation.setBillingAddress(new Address());
    	}
    	else {
    		this.currentPaymentInformation.setBillingAddress(null);
    	}
    }

    /**
     * This method is called when the user presses the "Edit Limit Approval" button, and creates 
     * a new Limit Approval or presents the existing Limit Approval.
     */
    public void createLimitApproval() {
        this.currentLimitApproval = new LimitApproval();
    }
    
    /**
     * Saves the Payment Information onto the current Credential instance.
     */
    public void savePaymentInformation() {
        this.systemUser.setPaymentInformation(this.currentPaymentInformation);
    }
    
    public void clearPaymentInformation() {
    	this.systemUser.setPaymentInformation(null);
    }

    /**
     * Saves the Limit Approval onto the current Credential instance.
     */
    public void saveLimitApproval() {
    	this.currentLimitApproval.setAxcxu(currentAccXCredXUser);
        this.currentAccXCredXUser.getLimitApprovals().add(this.currentLimitApproval);
    }
    
    private UIComponent component;

    public UIComponent getComponent() {
        return component;
    }

    public void setComponent(UIComponent component) {
        this.component = component;
    }

    private int page;
    private long count;
    private List<SystemUser> pageItems;

    private SystemUser example = new SystemUser();

    public int getPage() {
        return this.page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    @SuppressWarnings("static-method")
    public int getPageSize() {
        return 10;
    }

    public SystemUser getExample() {
        return this.example;
    }

    public void setExample(SystemUser example) {
        this.example = example;
    }

    @Override
    public void search() {
        this.page = 0;
    }

    public void paginate() {
        this.pageItems = bp.searchByExample(this.example, getPage() * getPageSize(), getPageSize());

        this.count = bp.resultQuantity(this.example);
    }

    public List<SystemUser> getPageItems() {
        return this.pageItems;
    }

    public long getCount() {
        return this.count;
    }

    /*
     * Support listing and POSTing back SystemUser entities (e.g. from inside an
     * HtmlSelectOneMenu)
     */

    @Override
    public List<SystemUser> getAll() {

        return bp.findAll(SystemUser.class, 0, 0);
    }

    public List<SystemUser> search(String s) {
        SystemUser systemUser = new SystemUser();
        systemUser.getPerson().setFirstName(s);
        return this.bp.searchByExample(systemUser, 0, 0);
    }

    @Resource
    private SessionContext sessionContext;

    @Override
    public Converter getConverter() {

        final SystemUserBean ejbProxy = this.sessionContext.getBusinessObject(SystemUserBean.class);

        return new Converter() {

            @Override
            public Object getAsObject(FacesContext context, UIComponent component, String value) {

                ejbProxy.setId(Long.valueOf(value));
                ejbProxy.retrieve();
                return ejbProxy.getSystemUser();
            }

            @Override
            public String getAsString(FacesContext context, UIComponent component, Object value) {

                if (value == null) {
                    return "";
                }

                return String.valueOf(((SystemUser) value).getId());
            }
        };
    }

    /*
     * Support adding children to bidirectional, one-to-many tables
     */

    private SystemUser add = new SystemUser();

    public SystemUser getAdd() {
        return this.add;
    }

    public SystemUser getAdded() {
        SystemUser added = this.add;
        this.add = new SystemUser();
        return added;
    }

    private void eagerLoad() {
        this.systemUser = this.bp.eagerLoad(this.systemUser);
        this.findParentAccountFromChildAccount();
    }

    public void setAddSelectedPaymentInfo(boolean addSelectedPayment) {
        this.addSelectedPaymentInfo = addSelectedPayment;
    }

    public void addAccountToUser(Account account) {
        account = accountBp.eagerLoad(account);
        this.systemUser.getAccounts().add(account);
    }

    private boolean validToCreate() {
        if (this.systemUser.getId() == null) {
            List<SystemUser> searchList = this.bp.getSystemUserByLogin(this.systemUser.getLogin());
            if (searchList.size() != 0) {
                FacesMessage message = new FacesMessage();
                message.setSeverity(FacesMessage.SEVERITY_ERROR);
                message.setSummary("Login already exists");
                message.setDetail("A user already exists with the specified login.");
                FacesContext.getCurrentInstance().addMessage(null, message);
                return false;
            }
        }
        return true;
    }
    
    private boolean postPersistValid() {
        String invalidCredentials = "";
        for (AccountXCredentialXUser axcxu : this.existingAccountXCredentialXUser) {
        	//validates each of the AXCXUs that the user is assigned to
        	if (axcxu.isActive()) {
            	AccountXCredentialXUser toValidate = axcxu;
            	if (toValidate.getId() != null) {
            		toValidate = this.accountXcredentialXuserDao.findById(toValidate.getId(), AccountXCredentialXUser.class);
            	}
            	//checks that, if the user must specify a cost center or department, 
            	//that the user has cost centers or departments assigned
        		Credential credential = credentialDao.findById(toValidate.getCredential().getId(), Credential.class);
        		if (credential.getCashoutPage() != null && credential.getCashoutPage().getPageXFields() != null) {
        			for (CashoutPageXField field : credential.getCashoutPage().getPageXFields()) {
        				if (field.getRequired() && field.getField() != null 
        						&& field.getField().getName().equals("cost center") 
        						&& assignedCostCenterDao.costCentersForCashout(toValidate).isEmpty()) {
        					invalidCredentials = invalidCredentials + "\"" + credential.getName() +"\"";
        				}
        				if (field.getRequired() && field.getField() != null 
        						&& field.getField().getName().equals("building or department") 
        						&& departmentDao.departmentsForCashout(toValidate).isEmpty()) {
        					invalidCredentials = invalidCredentials + "\"" + credential.getName() +"\"";
        					
        				}
        			}
        		}
        	}
        }
        if (StringUtils.isNotBlank(invalidCredentials)) {
            FacesMessage message = new FacesMessage();
            message.setSeverity(FacesMessage.SEVERITY_ERROR);
            String messageString = "User is missing departments or cost centers for credential(s): " + invalidCredentials;
            message.setSummary(messageString);
            message.setDetail(messageString);
            FacesContext.getCurrentInstance().addMessage(null, message);
            return false;
        }
        return true;
    }

    public AccountXCredentialXUser getCurrentAccXCredXUser() {
        return currentAccXCredXUser;
    }

    public void setCurrentAccXCredXUser(AccountXCredentialXUser currentAccXCredXUser) {
        this.currentAccXCredXUser = currentAccXCredXUser;
    }

    public AssignedCostCenter getCurrentCostCenter() {
        return currentCostCenter;
    }

    public void setCurrentCostCenter(AssignedCostCenter currentCostCenter) {
        this.currentCostCenter = currentCostCenter;
    }

    public void clearCurrentCostCenter() {
        this.setCurrentCostCenter(new AssignedCostCenter());
        this.getCurrentCostCenter().setAxcxu(this.currentAccXCredXUser);
    }

    public AssignedDepartment getCurrentDepartment() {
        return currentDepartment;
    }

    public void setCurrentDepartment(AssignedDepartment currentDepartment) {
        this.currentDepartment = currentDepartment;
    }

    public void clearCurrentDepartment() {
        this.setCurrentDepartment(new AssignedDepartment());
        this.getCurrentDepartment().setAxcxu(this.currentAccXCredXUser);
    }
    
    public void removeDepartment(AssignedDepartment department) {
    	department.setAxcxu(null);
    	this.currentAccXCredXUser.getDepartments().remove(department);
    }

    public void saveAccXCredXUser() {
        this.currentAccXCredXUser.setUser(this.systemUser);
    }

    private String plaintext = "";
    private String confirm = "";

    public void setPlaintextPassword(String password) {
        this.plaintext = password;
    }

    @SuppressWarnings("static-method")
    public String getPlaintextPassword() {
        return "";
    }

    public void setConfirmPassword(String password) {
        this.confirm = password;
    }

    @SuppressWarnings("static-method")
    public String getConfirmPassword() {
        return "";
    }

    public void storeNewPassword(String clientId) {
        if (!plaintext.equals(confirm)) {
            FacesContext.getCurrentInstance().addMessage(
                    clientId,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, ValidationProperties
                            .get("user.password.error.notmatch"), ValidationProperties
                            .get("user.password.error.notmatch")));
            return;
        }
        try {
            this.bp.encryptAndSetTemporaryPassword(this.systemUser, this.plaintext);
        }
        catch (DuplicatePasswordException e) {
            FacesContext.getCurrentInstance().addMessage(
                    clientId,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, ValidationProperties
                            .get("user.password.error.notdifferent"), ValidationProperties
                            .get("user.password.error.notdifferent")));
            return;
        }
        this.plaintext = "";
        this.confirm = "";
    }

	public LimitApproval getCurrentLimitApproval() {
		return currentLimitApproval;
	}

	public void setCurrentLimitApproval(LimitApproval currentLimitApproval) {
		this.currentLimitApproval = currentLimitApproval;
	}
	
	public String getDefaultPassword() {
		return this.defaultPassword;
	}
	
	public void setDefaultPassword(String password) {
		this.defaultPassword = password;
	}
	
    public enum AddressOverrideOption {
    	USE_GIVEN("Use the values below only"), USE_GIVEN_NONE_SPECIFIED("Use the values below if nothing is specified in the spreadsheet"), USE_ACCOUNT("Use the account default only"), USE_ACCOUNT_NONE_SPECIFIED("Use the account default if nothing is specified in the spreadsheet");

        private final String label;

        private AddressOverrideOption(String label) {
            this.label = label;
        }

        public String getLabel() {
            return this.label;
        }
    }

    public SelectItem[] getAddressOverrideOption() {
        SelectItem[] items = new SelectItem[AddressOverrideOption.values().length];
        int i = 0;
        for (AddressOverrideOption g : AddressOverrideOption.values()) {
            items[i++] = new SelectItem(g, g.getLabel());
        }
        return items;
    }

	public AddressOverrideOption getShipAddressOption() {
		return shipAddressOption;
	}

	public void setShipAddressOption(AddressOverrideOption shipAddressOption) {
		this.shipAddressOption = shipAddressOption;
	}

	public AddressOverrideOption getBillAddressOption() {
		return billAddressOption;
	}

	public void setBillAddressOption(AddressOverrideOption billAddressOption) {
		this.billAddressOption = billAddressOption;
	}

    public Bulletin getCurrentBulletin() {
        return this.currentBulletin;
    }

    public void setCurrentBulletin(Bulletin newBulletin) {
        this.currentBulletin = newBulletin;
    }

    /**
     * This method is called when the user presses the "New Bulletin" button, and creates 
     * a new bulletin to modify.
     */
    public void createBulletin() {
        this.currentBulletin = new Bulletin();
    }

    /**
     * Removes the selected bulletin from the set of bulletins on the current Account instance.
     * 
     * @param toRemove
     */
    public void removeBulletin(Bulletin toRemove) {
        this.systemUser.getBulletins().remove(toRemove);
    }

    /**
     * Saves the selected Bulletin onto the current Account instance.
     */
    public void saveBulletin() {
        this.systemUser.getBulletins().add(this.currentBulletin);
    }
    
    @Inject
    private CostCenterSearchBean costCenterSearchBean;
    
    public void validateCostCenter(FacesContext context, UIComponent component, Object value) {
    	for (AssignedCostCenter center : this.currentAccXCredXUser.getAllowedCostCenters()) {
    		if (center.getCostCenter() != null && center.getCostCenter().equals(this.costCenterSearchBean.getSelection())) {
                ((UIInput) component).setValid(false);
                FacesMessage message = new FacesMessage();
                message.setSeverity(FacesMessage.SEVERITY_ERROR);
                message.setSummary("This cost center is already assigned to this user's credential");
                message.setDetail("This cost center is already assigned to this user's credential");
                context.addMessage(component.getClientId(), message);
                return;
    		}
    	}
    }
    
    @Inject
    private DepartmentSearchBean departmentSearchBean;
    
    public void validateDepartment(FacesContext context, UIComponent component, Object value) {
    	for (AssignedDepartment department : this.currentAccXCredXUser.getDepartments()) {
    		if (department.getDepartment() != null && department.getDepartment().equals(this.departmentSearchBean.getSelection())) {
                ((UIInput) component).setValid(false);
                FacesMessage message = new FacesMessage();
                message.setSeverity(FacesMessage.SEVERITY_ERROR);
                message.setSummary("This department is already assigned to this user's credential");
                message.setDetail("This department is already assigned to this user's credential");
                context.addMessage(component.getClientId(), message);
                return;
    		}
    	}
    }
    
    public void clearApprover() {
    	if (this.currentAccXCredXUser != null) {
    		this.currentAccXCredXUser.setApproverUser(null);
    	}
    }
    
    public void setApproverToDefault() {
    	Properties integrationProperties = TenantConfigRepository.getInstance().getPropertiesByTenantId(CurrentTenantIdentifierResolverImpl.getCurrentTenant(), "tenant");
    	String defaultApproverLogin = integrationProperties.getProperty("defaultApproverLogin", "defaultapprover");
    	List<SystemUser> approvers = this.bp.getSystemUserByLogin(defaultApproverLogin);
    	if (approvers != null && approvers.size() == 1) {
    		this.currentAccXCredXUser.setApproverUser(approvers.get(0));
    	}
    }

    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public String setPasswordForAllUsers() {
    	String[] header = {"email", "login", "password"};
    	LOG.info("Fetching users with no password!");
    	List<SystemUser> users = this.bp.usersWithNoPassword();
    	LOG.info("Retrieved " + users.size() + " users");
    	List<Object[]> toAdd = new ArrayList<Object[]>(users.size());
    	for (SystemUser user : users) {
    		if (user.getPerson() != null && StringUtils.isNotBlank(user.getPerson().getEmail())) {
    			try {
	        		String password = EncryptionUtils.randomAlphanumeric();
	        		String login = user.getLogin();
	    			String email = user.getPerson().getEmail();
	    			this.bp.encryptAndSetTemporaryPassword(user, password);
	    			String[] row = new String[3];
	    			row[0] = email;
	    			row[1] = login;
	    			row[2] = password;
	    			toAdd.add(row);
    			} catch (DuplicatePasswordException e) {
    				//this line should never be reached, since the exception is only thrown when the password to set 
    				//matches the current password, and the current password is null for all of these users.
    				if (LOG.isDebugEnabled()) {
    					LOG.debug("The random password of one of the users is a duplicate");
    				}
    			}
    		}
    	}
    	for (SystemUser user : users) {
    		this.bp.update(user);
    	}
    	File file = csvService.getFile(header, toAdd);
    	String toReturn = FileDownloader.downloadFile(file);
    	try {
	    	if (file.exists()) {
	    		file.delete();
	    	}
    	} catch (SecurityException e) {
    		LOG.error("Error deleting password file!", e);
    	}
        return toReturn;
    }
    
    public Converter getAccountConverter() {

        return new Converter() {

            @Override
            public Object getAsObject(FacesContext context, UIComponent component, String value) {
                return accountBp.findById(Long.valueOf(value), Account.class);
            }

            @Override
            public String getAsString(FacesContext context, UIComponent component, Object value) {

                if (value == null) {
                    return "";
                }

                return String.valueOf(((Account) value).getId());
            }
        };
    }

    public String resetPassword() {
        FacesContext context = FacesContext.getCurrentInstance();

        context.addMessage("resetPasswordInfo", new FacesMessage(FacesMessage.SEVERITY_INFO, "Sent Password Reset Email",
                "Sent Password Reset Email"));
        try {
            
            if (this.systemUser != null && this.systemUser.getId() != null) {
                
                //TODO: refactor out external context
                ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();

                StringBuffer resetUrl = new StringBuffer();
                resetUrl.append(externalContext.getRequestScheme());
                resetUrl.append("://");
                resetUrl.append(externalContext.getRequestServerName());
                if (externalContext.getRequestServerPort() != 80) {
                    resetUrl.append(":");
                    resetUrl.append(externalContext.getRequestServerPort());
                }
                resetUrl.append("/shopping/ecommerce/login/newPassword.xhtml");

                String bodyMsg = "Click Link to reset Password: \n" + resetUrl.toString();
                passwordResetEntryBp.scheduleReset(this.systemUser, bodyMsg, CurrentTenantIdentifierResolverImpl.getCurrentTenant());
            }

        }
        catch (Exception e) {
            //If some other error occurs
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Unexpected error",
                    "An unexpected error occurred."));
        }
        
        return null;
    }  
    
    public List<AssignedDepartment> getSortedDepartments() {
    	List<AssignedDepartment> assignedDepartments = new ArrayList<AssignedDepartment>();
    	assignedDepartments.addAll(this.currentAccXCredXUser.getDepartments());
        Collections.sort(assignedDepartments, new EntityComparator());
        return assignedDepartments;
    }
    
    public List<AssignedCostCenter> getSortedCostCenters() {
    	List<AssignedCostCenter> costCenters = new ArrayList<AssignedCostCenter>();
    	costCenters.addAll(this.currentAccXCredXUser.getAllowedCostCenters());
        Collections.sort(costCenters, new EntityComparator());
        return costCenters;
    }
    
    public List<Desktop> getSortedDeskTops() {
    	List<Desktop> deskTops = new ArrayList<Desktop>();
    	deskTops.addAll(this.currentAccXCredXUser.getDesktops());
        Collections.sort(deskTops, new EntityComparator());
        return deskTops;
    }

    public SelectItem[] getSelectionTypes() {
        SelectItem[] items = new SelectItem[CatalogSelectionType.values().length];
        int i = 0;
        for (CatalogSelectionType g : CatalogSelectionType.values()) {
            items[i++] = new SelectItem(g, g.getLabel());
        }
        return items;
    }    
    
}
