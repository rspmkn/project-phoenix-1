package com.apd.phoenix.web.searchbeans;

import java.util.List;
import javax.enterprise.context.ConversationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import com.apd.phoenix.service.business.AccountBp;
import com.apd.phoenix.service.business.CredentialBp;
import com.apd.phoenix.service.model.Account;
import com.apd.phoenix.service.model.Credential;
import java.io.Serializable;

@Named
@ConversationScoped
public class AccountXUserSearchBean extends AbstractSearchBean<Account> implements Serializable {

    @Inject
    AccountBp bp;

    @Override
    public String[] getCriteria() {
        String[] toReturn = { "Name" };
        return toReturn;
    }

    @Override
    protected List<Account> search() {
        Account search = new Account();
        search.setName(this.getValues().get(0).getValue());
        return bp.searchByExample(search, 0, 0);
    }

}
