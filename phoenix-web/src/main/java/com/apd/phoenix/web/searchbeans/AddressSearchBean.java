package com.apd.phoenix.web.searchbeans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.enterprise.context.ConversationScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.commons.lang.StringUtils;

import com.apd.phoenix.service.business.AddressBp;
import com.apd.phoenix.service.model.Account;
import com.apd.phoenix.service.model.Address;
import com.apd.phoenix.web.PropertiesBean;


@Named
@ConversationScoped
public class AddressSearchBean extends AbstractSearchBean<Address> implements Serializable {

    @Inject
    private PropertiesBean propertiesBean;

    @Inject
    AddressBp bp;    
 
    private String accountId;

	@Override
    public String[] getCriteria() {
        String[] toReturn =  { "Line1", "City", "Zip"};
        return toReturn;
    }

    @Override
    public String[] getColumns() {        
            String[] toReturn = { "Name", "Company Name", "Line1", "City", "State", "Zip", "Address Type", "Geo Code" };
            return toReturn;        
    }

    @Override
    protected List<Address> search() {
    	Address address = new Address();
    	Long addressId = 0l;
    	if(StringUtils.isNotEmpty(this.getValues().get(0).getValue())) {
    		address.setLine1(this.getValues().get(0).getValue());
    	}
    	if(StringUtils.isNotEmpty(this.getValues().get(1).getValue())) {
    		address.setCity(this.getValues().get(1).getValue());
    	}
    	if(StringUtils.isNotEmpty(this.getValues().get(2).getValue())) {
    		address.setZip(this.getValues().get(2).getValue());
    	}
    	Account account = new Account();
    	if(StringUtils.isNotEmpty(this.accountId)) {
    		addressId = Long.parseLong(this.accountId);
    	}
    	account.setId(addressId);
    	address.setAccount(account);
        return bp.searchByExample(address, 0, 0);
    }

    @Override
    public Map<String, String> resultRow(Address searchResult) {        
        Map<String, String> toReturn = new HashMap<String, String>();
        toReturn.put(getColumns()[0], searchResult.getName());
        toReturn.put(getColumns()[1], searchResult.getCompany());
        toReturn.put(getColumns()[2], searchResult.getLine1());
        toReturn.put(getColumns()[3], searchResult.getCity());
        toReturn.put(getColumns()[4], searchResult.getState());
        toReturn.put(getColumns()[5], searchResult.getZip());
        toReturn.put(getColumns()[6], searchResult.getType().getName());
        toReturn.put(getColumns()[7], searchResult.getGeoCode());                
       
        return toReturn;
    }
    public void performSearch(String searchMessageId, String id) {
    	this.accountId = id;
    	super.performSearch(searchMessageId);
    }
  
}