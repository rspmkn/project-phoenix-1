package com.apd.phoenix.web.searchbeans;

import javax.enterprise.context.ConversationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import com.apd.phoenix.web.searchbeans.UserSearchBean;

@Named
@ConversationScoped
public class UserSearchBean extends AbstractUserSearchBean {

}
