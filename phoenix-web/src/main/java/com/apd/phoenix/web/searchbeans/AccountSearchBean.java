package com.apd.phoenix.web.searchbeans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.enterprise.context.ConversationScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.apd.phoenix.service.business.AccountBp;
import com.apd.phoenix.service.model.Account;
import com.apd.phoenix.web.PropertiesBean;

@Named
@ConversationScoped
public class AccountSearchBean extends AbstractSearchBean<Account> implements Serializable {

    @Inject
    private PropertiesBean propertiesBean;

    @Inject
    AccountBp bp;

    @Override
    public String[] getCriteria() {
        String[] toReturn = { "Name" };
        return toReturn;
    }

    @Override
    public String[] getColumns() {
        if (propertiesBean.get("project.state", "isPreRelease", "false").equals("true")) {
            String[] toReturn = { "Name", "Relationship", "System ID" };
            return toReturn;
        }
        else {
            String[] toReturn = { "Name", "Relationship" };
            return toReturn;
        }
    }

    @Override
    protected List<Account> search() {
        return bp.filteredAccountList(this.getValues().get(0).getValue());
    }

    @Override
    public Map<String, String> resultRow(Account searchResult) {
        searchResult = bp.hydrateForSearchResults(searchResult);
        Map<String, String> toReturn = new HashMap<String, String>();
        toReturn.put(getColumns()[0], searchResult.getName());
        Collection<Account> children = bp.getChildren(searchResult);
        if (searchResult.getRootAccount() == null && children.size() == 0) {
            toReturn.put(getColumns()[1], "Root");
        }
        if (searchResult.getRootAccount() == null && children.size() > 0) {
            toReturn.put(getColumns()[1], "Root/Parent");
        }
        if (searchResult.getRootAccount() != null && children.size() > 0) {
            toReturn.put(getColumns()[1], "Parent/Child");
        }
        if (searchResult.getRootAccount() != null && children.size() == 0) {
            toReturn.put(getColumns()[1], "Child");
        }
        if (propertiesBean.get("project.state", "isPreRelease", "false").equals("true")) {
            toReturn.put(getColumns()[2], searchResult.getId().toString());
        }
        return toReturn;
    }

}
