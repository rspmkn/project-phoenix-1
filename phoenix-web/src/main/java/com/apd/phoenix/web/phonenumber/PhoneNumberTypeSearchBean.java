package com.apd.phoenix.administration.view.jsf.bean;

import java.util.List;
import javax.enterprise.context.ConversationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import com.apd.phoenix.service.persistence.jpa.PhoneNumberTypeDao;
import com.apd.phoenix.service.model.PhoneNumberType;
import com.apd.phoenix.web.searchbeans.AbstractSearchBean;
import java.io.Serializable;

@Named
@ConversationScoped
public class PhoneNumberTypeSearchBean extends AbstractSearchBean<PhoneNumberType> implements Serializable {

    @Inject
    PhoneNumberTypeDao dao;

    @Override
    public String[] getCriteria() {
        String[] toReturn = { "Name" };
        return toReturn;
    }

    @Override
    protected List<PhoneNumberType> search() {
        PhoneNumberType search = new PhoneNumberType();
        search.setName(this.getValues().get(0).getValue());
        return dao.searchByExample(search, 0, 0);
    }

}
