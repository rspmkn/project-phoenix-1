package com.apd.phoenix.web.address;

import java.io.Serializable;

import javax.ejb.Stateful;
import javax.enterprise.context.ConversationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.commons.lang.StringUtils;

import com.apd.phoenix.service.model.Address;
import com.apd.phoenix.web.cashout.ZipSearchBean;
import com.apd.phoenix.web.cashout.ZipSearchBean.ZipSelection;

@Named
@Stateful
@ConversationScoped
public class AddressBean implements Serializable {
	
	@Inject
	private ZipSearchBean zipSearchBean;

    private static final long serialVersionUID = 1L;

	private static final String INVALID_ZIP = "invalid";
    
    private Address address;
    
    private String zipString = INVALID_ZIP;
	
	private int zipIndex = 0;

	private boolean zipSet = false;
	
	public String setAddress(Address address) {
		this.address = address;
		return null;
	}

	public void zipValidator(FacesContext context, UIComponent component, String value) {
		if (address == null) {
			return;
		}
		try {
			//if the entered zip strings haven't been set, but the form is submitted, uses the value in the field
			if (zipString.equals(INVALID_ZIP)) {
				zipString = value;
			}
			//then, finds if an index is specified. If there isn't, uses the previously specified index, if one has been set
			if (!value.contains(":") && zipSet) {
				value = zipString.concat(":").concat("" + zipIndex);
			}
			//searches based on the zip and the index
			String[] values = value.split(":");
			//the length is greater than 1 if an index is specified. If no index is specified, does not set city and state
			if (values.length > 1) {
				ZipSelection zipSelection = zipSearchBean.zipSearch(values[0]).get(Integer.parseInt(values[1]));
				this.address.setZip(zipSelection.getZip());
				this.address.setCity(zipSelection.getCity());
				this.address.setState(zipSelection.getState());
				this.address.setCounty(zipSelection.getCounty());
				this.address.setGeoCode(zipSelection.getGeocode());
				zipString = values[0];
				zipIndex = Integer.parseInt(values[1]);
				zipSet = true;
			}
			return;
		}
		catch (NumberFormatException | IndexOutOfBoundsException e) {
	        ((UIInput) component).setValid(false);
	        context.addMessage(component.getClientId(), new FacesMessage(FacesMessage.SEVERITY_ERROR, "Enter a valid zip code", "Enter a valid zip code"));

			this.address.setZip(null);
			this.address.setCity(null);
			this.address.setState(null);
		}
	}
	
	public boolean isInUs() {
		return address == null || StringUtils.isBlank(address.getCountry()) || address.getCountry().equals(Country.US.toString());
	}
	
	public String getZip() {
		if (this.address != null) {
			return this.address.getZip();
		}
		return null;
	}
	
	public void setZip(String zip) {
		//do nothing, set during validation
	}
	
	public Country getCountry() {
		if (address != null && StringUtils.isNotBlank(address.getCountry())) {
			return Country.valueOf(address.getCountry());
		}
		return Country.US;
	}
	
	public void setCountry(Country country) {
		if (address != null) {
			address.setCountry(country.toString());
		}
	}

    public SelectItem[] getCountries() {
        SelectItem[] items = new SelectItem[Country.values().length];
        int i = 0;
        for (Country g : Country.values()) {
            items[i++] = new SelectItem(g, g.getLabel());
        }
        return items;
    }

    private static enum Country {
    	
    	US("US - United States"), AC("AC - Ascension Island "), AD("AD - Andorra "), AE("AE - United Arab Emirates "), AF("AF - Afghanistan "), AG("AG - Antigua and Barbuda "), AI("AI - Anguilla "), AL("AL - Albania "), AM("AM - Armenia "), AN("AN - Netherlands Antilles "), AO("AO - Angola "), AQ("AQ - Antarctica "), AR("AR - Argentina "), AS("AS - American Samoa "), AT("AT - Austria "), AU("AU - Australia "), AW("AW - Aruba "), AX("AX - Aland Islands "), AZ("AZ - Azerbaijan "), BA("BA - Bosnia and Herzegovina "), BB("BB - Barbados "), BD("BD - Bangladesh "), BE("BE - Belgium "), BF("BF - Burkina Faso "), BG("BG - Bulgaria "), BH("BH - Bahrain "), BI("BI - Burundi "), BJ("BJ - Benin "), BM("BM - Bermuda "), BN("BN - Brunei Darussalam "), BO("BO - Bolivia "), BR("BR - Brazil "), BS("BS - Bahamas "), BT("BT - Bhutan "), BV("BV - Bouvet Island "), BW("BW - Botswana "), BY("BY - Belarus "), BZ("BZ - Belize "), CA("CA - Canada "), CC("CC - Cocos (Keeling) Islands "), CD("CD - Congo, Democratic Republic "), CF("CF - Central African Republic "), CG("CG - Congo "), CH("CH - Switzerland "), CI("CI - Cote D'Ivoire (Ivory Coast) "), CK("CK - Cook Islands "), CL("CL - Chile "), CM("CM - Cameroon "), CN("CN - China "), CO("CO - Colombia "), CR("CR - Costa Rica "), CS("CS - Czechoslovakia (former) "), CU("CU - Cuba "), CV("CV - Cape Verde "), CX("CX - Christmas Island "), CY("CY - Cyprus "), CZ("CZ - Czech Republic "), DE("DE - Germany "), DJ("DJ - Djibouti "), DK("DK - Denmark "), DM("DM - Dominica "), DO("DO - Dominican Republic "), DZ("DZ - Algeria "), EC("EC - Ecuador "), EE("EE - Estonia "), EG("EG - Egypt "), EH("EH - Western Sahara "), ER("ER - Eritrea "), ES("ES - Spain "), ET("ET - Ethiopia "), EU("EU - European Union "), FI("FI - Finland "), FJ("FJ - Fiji "), FK("FK - Falkland Islands (Malvinas) "), FM("FM - Micronesia "), FO("FO - Faroe Islands "), FR("FR - France "), FX("FX - France, Metropolitan "), GA("GA - Gabon "), GB("GB - Great Britain (UK) "), GD("GD - Grenada "), GE("GE - Georgia "), GF("GF - French Guiana "), GG("GG - Guernsey "), GH("GH - Ghana "), GI("GI - Gibraltar "), GL("GL - Greenland "), GM("GM - Gambia "), GN("GN - Guinea "), GP("GP - Guadeloupe "), GQ("GQ - Equatorial Guinea "), GR("GR - Greece "), GS("GS - S. Georgia and S. Sandwich Isls. "), GT("GT - Guatemala "), GU("GU - Guam "), GW("GW - Guinea-Bissau "), GY("GY - Guyana "), HK("HK - Hong Kong "), HM("HM - Heard and McDonald Islands "), HN("HN - Honduras "), HR("HR - Croatia (Hrvatska) "), HT("HT - Haiti "), HU("HU - Hungary "), ID("ID - Indonesia "), IE("IE - Ireland "), IL("IL - Israel "), IM("IM - Isle of Man "), IN("IN - India "), IO("IO - British Indian Ocean Territory "), IQ("IQ - Iraq "), IR("IR - Iran "), IS("IS - Iceland "), IT("IT - Italy "), JE("JE - Jersey "), JM("JM - Jamaica "), JO("JO - Jordan "), JP("JP - Japan "), KE("KE - Kenya "), KG("KG - Kyrgyzstan "), KH("KH - Cambodia "), KI("KI - Kiribati "), KM("KM - Comoros "), KN("KN - Saint Kitts and Nevis "), KP("KP - Korea (North) "), KR("KR - Korea (South) "), KW("KW - Kuwait "), KY("KY - Cayman Islands "), KZ("KZ - Kazakhstan "), LA("LA - Laos "), LB("LB - Lebanon "), LC("LC - Saint Lucia "), LI("LI - Liechtenstein "), LK("LK - Sri Lanka "), LR("LR - Liberia "), LS("LS - Lesotho "), LT("LT - Lithuania "), LU("LU - Luxembourg "), LV("LV - Latvia "), LY("LY - Libya "), MA("MA - Morocco "), MC("MC - Monaco "), MD("MD - Moldova "), ME("ME - Montenegro "), MG("MG - Madagascar "), MH("MH - Marshall Islands "), MK("MK - F.Y.R.O.M. (Macedonia) "), ML("ML - Mali "), MM("MM - Myanmar "), MN("MN - Mongolia "), MO("MO - Macau "), MP("MP - Northern Mariana Islands "), MQ("MQ - Martinique "), MR("MR - Mauritania "), MS("MS - Montserrat "), MT("MT - Malta "), MU("MU - Mauritius "), MV("MV - Maldives "), MW("MW - Malawi "), MX("MX - Mexico "), MY("MY - Malaysia "), MZ("MZ - Mozambique "), NA("NA - Namibia "), NC("NC - New Caledonia "), NE("NE - Niger "), NF("NF - Norfolk Island "), NG("NG - Nigeria "), NI("NI - Nicaragua "), NL("NL - Netherlands "), NO("NO - Norway "), NP("NP - Nepal "), NR("NR - Nauru "), NT("NT - Neutral Zone "), NU("NU - Niue "), NZ("NZ - New Zealand (Aotearoa) "), OM("OM - Oman "), PA("PA - Panama "), PE("PE - Peru "), PF("PF - French Polynesia "), PG("PG - Papua New Guinea "), PH("PH - Philippines "), PK("PK - Pakistan "), PL("PL - Poland "), PM("PM - St. Pierre and Miquelon "), PN("PN - Pitcairn "), PR("PR - Puerto Rico "), PS("PS - Palestinian Territory, Occupied "), PT("PT - Portugal "), PW("PW - Palau "), PY("PY - Paraguay "), QA("QA - Qatar "), RE("RE - Reunion "), RS("RS - Serbia "), RO("RO - Romania "), RU("RU - Russian Federation "), RW("RW - Rwanda "), SA("SA - Saudi Arabia "), SB("SB - Solomon Islands "), SC("SC - Seychelles "), SD("SD - Sudan "), SE("SE - Sweden "), SG("SG - Singapore "), SH("SH - St. Helena "), SI("SI - Slovenia "), SJ("SJ - Svalbard & Jan Mayen Islands "), SK("SK - Slovak Republic "), SL("SL - Sierra Leone "), SM("SM - San Marino "), SN("SN - Senegal "), SO("SO - Somalia "), SR("SR - Suriname "), ST("ST - Sao Tome and Principe "), SU("SU - USSR (former) "), SV("SV - El Salvador "), SY("SY - Syria "), SZ("SZ - Swaziland "), TC("TC - Turks and Caicos Islands "), TD("TD - Chad "), TF("TF - French Southern Territories "), TG("TG - Togo "), TH("TH - Thailand "), TJ("TJ - Tajikistan "), TK("TK - Tokelau "), TM("TM - Turkmenistan "), TN("TN - Tunisia "), TO("TO - Tonga "), TP("TP - East Timor "), TR("TR - Turkey "), TT("TT - Trinidad and Tobago "), TV("TV - Tuvalu "), TW("TW - Taiwan "), TZ("TZ - Tanzania "), UA("UA - Ukraine "), UG("UG - Uganda "), UK("UK - United Kingdom "), UM("UM - US Minor Outlying Islands  "), UY("UY - Uruguay "), UZ("UZ - Uzbekistan "), VA("VA - Vatican City State (Holy See) "), VC("VC - Saint Vincent & the Grenadines "), VE("VE - Venezuela "), VG("VG - British Virgin Islands "), VI("VI - Virgin Islands (U.S.) "), VN("VN - Viet Nam "), VU("VU - Vanuatu "), WF("WF - Wallis and Futuna Islands "), WS("WS - Samoa "), YE("YE - Yemen "), YT("YT - Mayotte "), YU("YU - Serbia and Montenegro (former Yugoslavia) "), ZA("ZA - South Africa "), ZM("ZM - Zambia "), ZW("ZW - Zimbabwe");
    	
    	private final String label;
    	
    	private Country(String label) {
    		this.label = label;
    	}
    	
    	private String getLabel() {
    		return this.label;
    	}
    }
}