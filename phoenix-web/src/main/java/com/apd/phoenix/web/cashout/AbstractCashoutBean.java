package com.apd.phoenix.web.cashout;

import static com.apd.phoenix.service.model.CredentialPropertyType.CredentialPropertyTypeEnum.*;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.enterprise.context.Conversation;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.lang.StringUtils;

import com.apd.phoenix.core.StringEscape;
import com.apd.phoenix.service.business.AccountBp;
import com.apd.phoenix.service.business.AddressBp;
import com.apd.phoenix.service.business.AssignedCostCenterBp;
import com.apd.phoenix.service.business.AssignedDepartmentBp;
import com.apd.phoenix.service.business.CashoutPageBp;
import com.apd.phoenix.service.business.CredentialBp;
import com.apd.phoenix.service.business.CustomerOrderBp;
import com.apd.phoenix.service.business.LineItemBp;
import com.apd.phoenix.service.business.LineItemStatusBp;
import com.apd.phoenix.service.business.OrderStatusBp;
import com.apd.phoenix.service.business.PoNumberBp;
import com.apd.phoenix.service.business.SystemUserBp;
import com.apd.phoenix.service.model.Account;
import com.apd.phoenix.service.model.AccountXCredentialXUser;
import com.apd.phoenix.service.model.Address;
import com.apd.phoenix.service.model.AssignedCostCenter;
import com.apd.phoenix.service.model.CashoutPage.NoCardAction;
import com.apd.phoenix.service.model.CashoutPageXField;
import com.apd.phoenix.service.model.Credential;
import com.apd.phoenix.service.model.CustomerOrder;
import com.apd.phoenix.service.model.LineItem;
import com.apd.phoenix.service.model.LineItemStatus.LineItemStatusEnum;
import com.apd.phoenix.service.model.MiscShipTo;
import com.apd.phoenix.service.model.NotificationProperties;
import com.apd.phoenix.service.model.OrderLog;
import com.apd.phoenix.service.model.OrderStatus.OrderStatusEnum;
import com.apd.phoenix.service.model.PaymentInformation;
import com.apd.phoenix.service.model.PaymentType;
import com.apd.phoenix.service.model.PoNumber;
import com.apd.phoenix.service.business.OrderTypeBp;
import com.apd.phoenix.service.model.SystemUser;
import com.apd.phoenix.service.payment.ws.CreditCardTransactionResult;
import com.apd.phoenix.service.payment.ws.client.PaymentGatewayService;
import com.apd.phoenix.service.payment.ws.client.PaymentGatewayTransaction;
import com.apd.phoenix.service.payment.ws.client.VerificationTransaction;
import com.apd.phoenix.service.persistence.jpa.AssignedCostCenterDao;
import com.apd.phoenix.service.persistence.jpa.DepartmentDao;
import com.apd.phoenix.service.persistence.jpa.DesktopDao;
import com.apd.phoenix.service.persistence.jpa.PaymentTypeDao;
import com.apd.phoenix.service.workflow.WorkflowService;
import com.apd.phoenix.web.ValidationProperties;

import javax.faces.application.FacesMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class is divided into three parts.
 * 
 * The first part is the @PostConstruct method, and related methods. This initializes the Cashout bean, setting values that will 
 * not change unless the credential is changed (ie the session ends).
 * 
 * The second part starts with the retrieve() method, and related methods. This method is called when the user arrives at the 
 * cashout page. It makes sure the order is persisted, and populates the order with the contents of the cart.
 * 
 * Finally, there's the part starting with checkout(). This updates the order status, calculates the tax, and places the order.
 * 
 * @author RHC
 *
 */
public abstract class AbstractCashoutBean implements Serializable {

    private static final String DEFAULT_BILLTO_NAME = "Accounts Payable";

	private static final String PROPERTY_SPLITTER = ",";

    private static final String[] DEFAULT_COUNTRY_CODES = { "US" };

    private static final String FALSE_PROPERTY_VALUE = "no";

    private static final String TRUE_PROPERTY_VALUE = "yes";

    private static final long serialVersionUID = -8528563212135192753L;

    private static final Logger logger = LoggerFactory.getLogger(AbstractCashoutBean.class);

    private static final String NO_PREAUTH_STRATEGY = "none";

    private static final String ONE_DOLLAR_STRATEGY = "one";

    @Inject
    private LineItemStatusBp lineItemStatusBp;

    @Inject
    private OrderTypeBp orderTypeBp;

    @Inject
    private CustomerOrderBp customerOrderBp;

    @Inject
    private CashoutPageBp cashoutBp;

    @Inject
    private CashoutPageContainer container;

    @Inject
    private OrderStatusBp statusBp;

    @Inject
    private AddressBp addressBp;

    @Inject
    private PoNumberBp poBp;

    @Inject
    private DepartmentDao departmentDao;

    @Inject
    private AssignedCostCenterDao costCenterDao;

    @Inject
    private WorkflowService workflowService;

    @Inject
    private PaymentTypeDao paymentTypeDao;

    @Inject
    private Conversation conversation;

    @Inject
    private PaymentGatewayService paymentGatewayService;

    @Inject
    private ZipSearchBean zipSearchBean;

    @Inject
    private DesktopDao desktopDao;

    @Inject
    private AssignedCostCenterBp assignedCostCenterBp;
    
    @Inject
    private AssignedDepartmentBp assignedDepartmentBp;
    
    @Inject
    private LineItemBp lineItemBp;

    protected CustomerOrder currentOrder = new CustomerOrder();

    private boolean shouldPlace = true;

    private static final long MS_PER_DAY = 24L * 60 * 60 * 1000; //86400000 ms in a day

    /**
     * This method returns the AccountXCredentialXUser that the order is being placed under. It must be managed, or 
     * be fully hydrated.
     * 
     * @return
     */
    protected abstract AccountXCredentialXUser getCurrentAXCXU();

    /**
     * Returns the PropertiesConfiguration containing the properties for the app being used
     * 
     * @return
     */
    protected abstract PropertiesConfiguration getProperties();

    /**
     * Used to call any functionality before the order is placed
     */
    protected abstract void prePlaceOrder() throws OrderPlaceException;

    /**
     * Used to call any functionality after the order is placed
     * 
     * @return
     */
    protected abstract String postPlaceOrder();

    /**
     * Returns a Map from String to String of the properties on the Credential being used
     * 
     * @return
     */
    protected abstract Map<String, String> credentialPropertiesMap();

    /**
     * Returns a String of the value of the order type
     * 
     * @return
     */
    protected abstract String orderTypeValue();

    /**
     * This method takes the name of a field, and, if appropriate, populates the dropdown options of that field, based 
     * on the currently-shopping user.
     * 
     * @param fieldName
     */
    protected void setOptionsForUser(String fieldName) {
    	CashoutPageXField field = this.container.getField().get(fieldName);
    	switch (fieldName) {
    	case "existing billto address selection":
    		this.container.getOptions().put(field, new HashSet<>());
    		this.container.getOptions().get(field).addAll(addressBp.addressesForCashout(this.getCurrentAXCXU(), 
    				"BILLTO"));
    		return;
    	case "existing shipto address selection":
    		this.container.getOptions().put(field, new HashSet<>());
    		this.container.getOptions().get(field).addAll(addressBp.addressesForCashout(this.getCurrentAXCXU(), 
    				"SHIPTO"));
    		return;
    	case "blanket po number":
    		this.container.getOptions().put(field, new HashSet<>());
    		this.container.getOptions().get(field).addAll(poBp.poNumbersForCashout(this.getCurrentAXCXU()));
    		return;
    	case "cost center":
    		//TODO: functionality to add cost centers to a Credential in admin app
    		//TODO: adding AssignedCostCenters to Credentials
    		this.container.getOptions().put(field, new HashSet<>());
    		this.container.getOptions().get(field).addAll(costCenterDao.costCentersForCashout(this.getCurrentAXCXU()));
    		return;
    	case "cost center combobox":
    		//TODO: functionality to add cost centers to a Credential in admin app
    		//TODO: adding AssignedCostCenters to Credentials
    		this.container.getOptions().put(field, new HashSet<>());
    		this.container.getOptions().get(field).addAll(costCenterDao.costCentersForCashout(this.getCurrentAXCXU()));
    		return;
    	case "building or department":
    		//TODO: functionality to add departments to a Credential in admin app
    		this.container.getOptions().get(field).addAll(departmentDao.departmentsForCashout(this.getCurrentAXCXU()));
    		return;
    	case "building or department combobox":
    		//TODO: functionality to add departments to a Credential in admin app
    		this.container.getOptions().get(field).addAll(departmentDao.departmentsForCashout(this.getCurrentAXCXU()));
    		return;
        case "desktop":
            this.container.getOptions().get(field).addAll(desktopDao.desktopsForCashout(this.getCurrentAXCXU()));
            return;
        case "desktop combobox":
            this.container.getOptions().get(field).addAll(desktopDao.desktopsForCashout(this.getCurrentAXCXU()));
            return;
    	case "email":
    		if (StringUtils.isBlank((String)this.container.getValue().get(field))) {
        		SystemUser user = this.userBp.findById(this.getCurrentAXCXU().getUser().getId(), SystemUser.class);
        		this.container.getValue().put(field, user.getPerson().getEmail());
    		}
    		
    	}
    }

    //END "create()" SECTION
    //BEGIN "retrieve()" SECTION

    /**
     * This method is called when the checkout page is loaded; it creates and persists the order, in the "Quoted" 
     * status.
     */
    public void retrieve() {
        if (FacesContext.getCurrentInstance().isPostback()) {
            return;
        }

        if (!StringUtils.isBlank(this.credentialPropertiesMap().get(ALLOW_MILITARY_ZIP_CODES.getValue()))
                && this.credentialPropertiesMap().get(ALLOW_MILITARY_ZIP_CODES.getValue()).equalsIgnoreCase(TRUE_PROPERTY_VALUE)) {
            this.zipSearchBean.setAllowMilitary(true);
        }

        if (!StringUtils.isBlank(this.credentialPropertiesMap().get(ALLOWED_COUNTRY_CODES.getValue()))) {
            this.container.setAllowedCountryCodes(this.credentialPropertiesMap().get(ALLOWED_COUNTRY_CODES.getValue()).split(
                    PROPERTY_SPLITTER));
        }
        else {
            this.container.setAllowedCountryCodes(DEFAULT_COUNTRY_CODES);
        }

        this.createOrder();

        if (this.canPersistBeforePlace()) {
            currentOrder = customerOrderBp.update(currentOrder);

            currentOrder = customerOrderBp.hydrateForOrderDetails(currentOrder);
        }
    }   
       
    protected void createOrder() {
        this.createEmptyOrder();
    }
    
    protected boolean canPersistBeforePlace() {
        return true;
    }

    protected void createEmptyOrder() {
    	
    	currentOrder = customerOrderBp.createEmptyOrder(this.getCurrentAXCXU().getAccount(), this.getCurrentAXCXU().getCredential(), this.getCurrentAXCXU().getUser(), orderTypeBp.getTypeFromValue(this.orderTypeValue()), statusBp.getOrderStatus(OrderStatusEnum.QUOTED), this.canPersistBeforePlace());
        
        currentOrder.setAddress(new Address());
        currentOrder.getAddress().setMiscShipTo(new MiscShipTo());
    }

    @Inject
    private SystemUserBp userBp;

    @Inject
    private CredentialBp credentialBp;

    @Inject
    private AccountBp accountBp;

    public enum AuthorizationStrategy {
        FULL, ONE, NONE;
    }

    public CustomerOrder getCurrentOrder() {
        return this.currentOrder;
    }

    public void setCurrentOrder(CustomerOrder order) {
        this.currentOrder = order;
    }

    public CashoutPageContainer getPageValues() {
        return this.container;
    }

    /**
     * This method takes a LineItem, the quantity of the item, and the amount to charge for shipping, and adds the 
     * item to the currentOrder.
     * 
     * If the specified shipRate is not null, the shiprate is used to set the estimated shipping amount on the item.
     * 
     * @param lineItem
     * @param quantity
     * @param shipRate
     */
    protected void addLineItem(LineItem lineItem, int quantity, BigDecimal shipRate) {

        lineItem.setQuantity(quantity);
        lineItem.setOriginalQuantity(quantity);
        lineItem.setOrder(currentOrder);
        if (shipRate != null && shipRate.compareTo(BigDecimal.ZERO) != 0) {
            lineItem.setEstimatedShippingAmount(lineItem.getTotalPrice().multiply(shipRate));
        }
        if (lineItem.getEstimatedShippingAmount() != null) {
        	currentOrder.setEstimatedShippingAmount(currentOrder.getEstimatedShippingAmount().add(lineItem.getEstimatedShippingAmount()));
        }
        lineItem.setLineNumber(currentOrder.getMaxLineNumber() + 1);
        currentOrder.getItems().add(lineItem);

    }
    
    public void removeLineItem(LineItem lineItem) {
        if (lineItem.getEstimatedShippingAmount() != null) {
        	currentOrder.setEstimatedShippingAmount(currentOrder.getEstimatedShippingAmount().subtract(lineItem.getEstimatedShippingAmount()));
        }
        currentOrder.getItems().remove(lineItem);
    }

    //END "retrieve()" SECTION

    public boolean isSpendingLimitMessageHidden() {
        AssignedCostCenter toCheck = null;
        if (this.container.getField().containsKey("cost center")) {
            toCheck = (AssignedCostCenter) this.container.getValue().get(this.container.getField().get("cost center"));
        }
        if (this.container.getField().containsKey("cost center combobox")) {
            CashoutPageXField field = this.container.getField().get("cost center combobox");

            boolean hasBeenAssigned = false;
            if (this.container.getOptions().get(field) != null) {
                for (Object assigned : this.container.getOptions().get(field)) {
                    if (this.container.costCenterToString((AssignedCostCenter) assigned).equals(
                            this.container.getValue().get(field))) {
                        toCheck = (AssignedCostCenter) assigned;
                        hasBeenAssigned = true;
                        break;
                    }
                }
            }
            if (!hasBeenAssigned) {
                toCheck = this.container.createDummyCostCenter((String) this.container.getValue().get(field),
                        this.currentOrder);
            }
        }
        if (this.container.getField().containsKey("cost center free")) {
            CashoutPageXField field = this.container.getField().get("cost center free");
            toCheck = this.container.createDummyCostCenter((String) this.container.getValue().get(field),
                    this.currentOrder);
        }
        return assignedCostCenterBp.isAmountAllowed(toCheck, this.currentOrder.getSubtotal());
    }

    public String getSpendingLimitMessage() {
        String toReturn = this.getProperties().getString("costcenter.spending.limit");
        if (this.credentialPropertiesMap().containsKey("spending limit message")) {
            toReturn = this.credentialPropertiesMap().get("spending limit message");
        }
        return toReturn;
    }

    //BEGIN "placeOrder()" SECTION

    public String holdOrder() {
        this.shouldPlace = false;
        String toReturn = this.placeOrder();
        this.shouldPlace = true;
        return toReturn;
    }

    /**
     * Called when the user presses the "PLACE ORDER" button on the cashout page. It applies the values set by the 
     * user, calculates the tax, sets the status to "Ordered", kicks off workflow, and redirects to the order 
     * confirmation page.
     * 
     * @return
     */
    public String placeOrder() {
    	//if order exists in the database, refreshes it. 
    	//also ensures that it is detached, so we don't get exceptions when setting values that temporarily
    	//violate DB constraints on a managed entity.
    	if (this.currentOrder.getId() != null) {
    		this.currentOrder = this.customerOrderBp.findById(this.currentOrder.getId(), CustomerOrder.class);
    		this.currentOrder = this.customerOrderBp.hydrateForOrderDetailsDifferentTransaction(this.currentOrder);
    	}
        try {
            this.prePlaceOrder();
        }
        catch (OrderPlaceException e) {
            logger.error("Error while attempting to place order:", e);
            //prevents the order from being placed, if prePlaceOrder fails
            return null;
        }
        setOrderFieldsFromCashoutPage(this.currentOrder);
        this.currentOrder.setWillCallEdiOverride(this.currentOrder.getCredential().isWillCallEdiOverride());
        //the approval recipients are set after the notifications are filtered, since the approval recipients will 
        //have an email sent regardless of the Credential notification limits
        this.setApprovalRecipients();
        this.currentOrder.setWrapAndLabel(this.currentOrder.getCredential().isWrapAndLabel());
        this.currentOrder.setAdot(this.currentOrder.getCredential().isAdot());
        boolean payingWithCard = this.currentOrder.getPaymentInformation() != null
                && this.currentOrder.getPaymentInformation().getCard() != null;
        try {
            CreditCardTransactionResult verificationResult = new CreditCardTransactionResult();
            if (payingWithCard) {
            	//for URL punchout users check to estimatedShippingAmount MaximumTaxToCharge are not null
            	if (this.currentOrder.getMaximumTaxToCharge() == null) {
                    this.currentOrder.setMaximumTaxToCharge(BigDecimal.ZERO);
                }
                if (this.currentOrder.getEstimatedShippingAmount() == null) {
                    this.currentOrder.setEstimatedShippingAmount(BigDecimal.ZERO);
                }             
                
                verificationResult = verifyCardInformation();
            }
            if (!payingWithCard || verificationResult.isSucceeded()) {
                //if verification succeeds try to complete placing order
                boolean cardStoredSuccessfully = storeCard(this.currentOrder.getPaymentInformation());
                if (!payingWithCard || cardStoredSuccessfully) {
                    try {
                        String outcome = placeOrderCore();
                        return outcome;
                    }
                    catch (CardAuthorizationException e) {
                        FacesContext.getCurrentInstance().addMessage(
                                null,
                                new FacesMessage(FacesMessage.SEVERITY_ERROR, ValidationProperties.get("cc.authorizationFailed"), ValidationProperties.get("cc.authorizationFailed")));
                        return null;
                    }
                }
                else {
                    FacesContext.getCurrentInstance().addMessage(
                            null,
                            new FacesMessage(FacesMessage.SEVERITY_ERROR, ValidationProperties.get("cc.storage.error"), ValidationProperties.get("cc.storage.error")));
                }
            }
            else {
                SystemUser user = userBp.findById(this.getCurrentAXCXU().getUser().getId(), SystemUser.class);
                if (user.getLastCreditCardAttemptDate() != null) {
                    if (user.getPreviousCreditCardAttemptDate() != null
                            && (new Date().getTime() - user.getPreviousCreditCardAttemptDate().getTime()) < MS_PER_DAY) {
                        //more than one credit card attempt within last day not including current attempt
                        user.setCreditCardAttempts(user.getCreditCardAttempts() + 1);
                        if (user.getCreditCardAttempts() >= this.getProperties().getInt(
                                "MAX_CREDIT_CARD_ATTEMPTS_PER_DAY")) {
                            logger
                                    .info("User "
                                            + user.getLogin()
                                            + " has exceeded the number of allowed credit card verification attempts and they are being prevented from trying to use a credit card for 24 hrs");
                            user.setPreviousCreditCardAttemptDate(user.getLastCreditCardAttemptDate());
                            user.setLastCreditCardAttemptDate(new Date());
                            throw new TooManyCreditCardVerificationFailuresException();
                        }
                    }
                    else if ((new Date().getTime() - user.getLastCreditCardAttemptDate().getTime()) < MS_PER_DAY) {
                        //one credit card attempt within last day not including current attempt
                        user.setCreditCardAttempts(2);
                    }
                    else {
                        //no credit card attempts within last day
                        user.setCreditCardAttempts(1);
                    }
                }
                user.setPreviousCreditCardAttemptDate(user.getLastCreditCardAttemptDate());
                user.setLastCreditCardAttemptDate(new Date());
                FacesContext.getCurrentInstance().addMessage(
                        null,
                        new FacesMessage(FacesMessage.SEVERITY_ERROR, ValidationProperties.get("cc.verificationFailed"), ValidationProperties.get("cc.verificationFailed")));
            }
        }
        catch (TooManyCreditCardVerificationFailuresException e) {
            String tooManyAttemptsMsg = ValidationProperties.get("cc.attemptsAllowedExceeded");
            FacesContext.getCurrentInstance().addMessage(null, 
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, tooManyAttemptsMsg, tooManyAttemptsMsg));
        }
        catch (OrderPlaceException e) {
            logger.error("Error while attempting to place order:", e);
            //prevents the order from being placed, if prePlaceOrder fails
            return null;
        }
        return null;
    }

    public void confirmAddress() throws OrderPlaceException {
        if (this.currentOrder.getAddress() == null || StringUtils.isBlank(this.currentOrder.getAddress().getLine1())) {
            boolean hasNoAddress = true;
            Set<Address> userAddresses = userBp.findById(this.currentOrder.getUser().getId(), SystemUser.class)
                    .getPerson().getAddresses();
            if (userAddresses != null) {
                for (Address a : userAddresses) {
                    if (a != null && a.getType().getName().equals("SHIPTO")) {
                        this.currentOrder.setAddress(addressBp.cloneEntity(a));
                        hasNoAddress = false;
                        break;
                    }
                }
            }
            if (hasNoAddress) {
                Set<Address> accountAddresses = accountBp.findById(this.currentOrder.getAccount().getId(),
                        Account.class).getAddresses();
                if (accountAddresses != null) {
                    for (Address a : accountAddresses) {
                        if (a.getType().getName().equals("SHIPTO")) {
                            this.currentOrder.setAddress(addressBp.cloneEntity(a));
                            hasNoAddress = false;
                            break;
                        }
                    }
                }
            }
            if (hasNoAddress) {
                String noAddress = ValidationProperties.get("cashout.shipAddress.noDefaultAddress");
                FacesContext.getCurrentInstance().addMessage(null, 
                        new FacesMessage(FacesMessage.SEVERITY_ERROR, noAddress, noAddress));
                throw new OrderPlaceException("No default address found for this order");
            }
        }
        if (this.currentOrder.getAddress() != null && this.currentOrder.getAddress().getMiscShipTo() != null) {
        	this.currentOrder.getAddress().getMiscShipTo().setUsAccount(customerOrderBp.getUsAccountNumber(this.currentOrder));
        }
    }
    
    private void setApprovalRecipients() {
    	if (this.currentOrder.getAddress() != null && this.currentOrder.getAddress().getMiscShipTo() != null && StringUtils.isNotBlank(this.currentOrder.getAddress().getMiscShipTo().getDepartment())) {
    		String approvalRecipients = this.assignedDepartmentBp.getApprovalRecipients(this.currentOrder.getAccount(), 
    				this.currentOrder.getCredential(), this.currentOrder.getUser(), StringEscape.removeMappingLabel(this.currentOrder.getAddress().getMiscShipTo().getDepartment()));
    		if (StringUtils.isNotBlank(approvalRecipients)) {
	    		NotificationProperties approvalRecipientsNotification = new NotificationProperties();
	    		approvalRecipientsNotification.setAddressing(NotificationProperties.Addressing.TO);
	    		approvalRecipientsNotification.setEventType(OrderLog.EventType.ORDER_APPROVAL_NEEDED);
	    		approvalRecipientsNotification.setRecipients(approvalRecipients);
	    		this.customerOrderBp.setNotification(approvalRecipientsNotification, this.currentOrder);
    		}
    	}
    }

    private String placeOrderCore() throws CardAuthorizationException, OrderPlaceException {

        if (this.currentOrder.getEstimatedShippingAmount() == null) {
            this.currentOrder.setEstimatedShippingAmount(BigDecimal.ZERO);
        }

    	customerOrderBp.computeOrderTax(this.currentOrder);

        currentOrder.refreshOrderTotal();

        //sets credential values
        currentOrder.setApdSalesPerson(credentialPropertiesMap().get("APD sales person"));
        if (StringUtils.isNotBlank(credentialPropertiesMap().get("contract number"))) {
        	if (currentOrder.getAddress().getMiscShipTo() == null) {
        		currentOrder.getAddress().setMiscShipTo(new MiscShipTo());
        	}
    		currentOrder.getAddress().getMiscShipTo().setContractNumber(credentialPropertiesMap().get("contract number"));
        }

        //sets the name and company, before the order billing is processed
        setCompanyAndName();

        try {
            this.setOrderPaymentInformation();
        }
        catch (CardAuthorizationException e) {
            throw e;
        }

        //if the address has been changed, sets name and company again
        setCompanyAndName();

        currentOrder = customerOrderBp.hydrateForOrderDetails(customerOrderBp.update(currentOrder));
        for (LineItem item : this.currentOrder.getItems()) {
        	lineItemBp.setStatus(item, lineItemStatusBp.getStatus(LineItemStatusEnum.CREATED));
        }

        this.confirmAddress();

        if (this.shouldPlace) {
            currentOrder.setOrderDate(new Date());
            customerOrderBp.setOrderStatus(currentOrder, statusBp.getOrderStatus(OrderStatusEnum.ORDERED));
            workflowService.processOrder(currentOrder);
        }
        
        this.container.orderToPage(currentOrder);
        
        return this.postPlaceOrder();
    }

    private void setOrderFieldsFromCashoutPage(CustomerOrder order) {
        this.container.pageToOrder(order);
    }

    private boolean storeCard(PaymentInformation paymentInformation) {
        if (paymentInformation != null && paymentInformation.getCard() != null) {
            try {
                this.paymentGatewayService.storeCreditCard(paymentInformation);
            }
            catch (PaymentGatewayService.CardVaultStorageException ex) {
                return false;
            }
            return StringUtils.isNotBlank(paymentInformation.getCard().getCardVaultToken());
        }
        else {
            return false;
        }
    }

    /**
     * Checks to see if the user has attempted too many credit card verifications in the past 24 hours, if not then it creates
     * a VerificationTransaction object and calls PaymentGatewayService#verification(). Returns null if a card is not being used.
     * @return result of call to PaymentGatewayService#verification()
     * @throws com.apd.phoenix.shopping.view.jsf.bean.cashout.CashoutBean.TooManyCreditCardVerificationFailuresException 
     */
    private CreditCardTransactionResult verifyCardInformation() throws TooManyCreditCardVerificationFailuresException {
        if (currentOrder.getPaymentInformation() == null || currentOrder.getPaymentInformation().getCard() == null) {
            //not paying with a card, no need to verify
            return null;
        }
        SystemUser user = userBp.findById(this.getCurrentAXCXU().getUser().getId(), SystemUser.class);
        if (user.getLastCreditCardAttemptDate() == null) {
            user.setLastCreditCardAttemptDate(new Date(0));
        }
        if (user.getPreviousCreditCardAttemptDate() == null) {
            user.setPreviousCreditCardAttemptDate(new Date(0));
        }
        if (user.getCreditCardAttempts() == null
                || (new Date().getTime() - user.getLastCreditCardAttemptDate().getTime()) > MS_PER_DAY) {
            user.setCreditCardAttempts(1);
        }
        else if (new Date().getTime() - user.getPreviousCreditCardAttemptDate().getTime() > MS_PER_DAY) {
            user.setCreditCardAttempts(2);
        }
        else if (user.getCreditCardAttempts() >= this.getProperties().getInt("MAX_CREDIT_CARD_ATTEMPTS_PER_DAY")) {
            throw new TooManyCreditCardVerificationFailuresException();
        }
        String cvv = (String) container.getValue().get(container.getField().get("credit card CVV"));
        CustomerOrder customerOrder = currentOrder;
        customerOrder.setCredential(credentialBp.findById(customerOrder.getCredential().getId(), Credential.class));
        VerificationTransaction vt = paymentGatewayService.createVerificationTransaction(customerOrder, cvv);
        CreditCardTransactionResult result = paymentGatewayService.verification(vt);
        return result;
    }

    private void setCompanyAndName() {
        //sets company and name on Addresses if they haven't been set
        String defaultShiptoName = this.currentOrder.getUser().getPerson().getFirstName() + " "
                + this.currentOrder.getUser().getPerson().getLastName();
        String defaultCompany = this.currentOrder.getAccount().getName();
        if (StringUtils.isEmpty(this.currentOrder.getAddress().getName())) {
            this.currentOrder.getAddress().setName(defaultShiptoName);
        }
        if (StringUtils.isEmpty(this.currentOrder.getAddress().getCompany())) {
            this.currentOrder.getAddress().setCompany(defaultCompany);
        }
        if (StringUtils.isEmpty(this.currentOrder.getPaymentInformation().getBillingAddress().getName())) {
            this.currentOrder.getPaymentInformation().getBillingAddress().setName(DEFAULT_BILLTO_NAME);
        }
        if (StringUtils.isEmpty(this.currentOrder.getPaymentInformation().getBillingAddress().getCompany())) {
            this.currentOrder.getPaymentInformation().getBillingAddress().setCompany(defaultCompany);
        }
    }

    /**
     * Called when the order is placed, to set the payment information for the order.
     */
    private void setOrderPaymentInformation() throws CardAuthorizationException {
        PaymentType searchType = new PaymentType();
        //defaults to invoice
        searchType.setName("Invoice");
        //if credit card information is set at cashout, uses that
        if (this.currentOrder.getPaymentInformation() != null
                && this.currentOrder.getPaymentInformation().getCard() != null) {
            //paid with credit card, submitted at cashout
            searchType.setName("Credit Card");
            PaymentType type = paymentTypeDao.searchByExactExample(searchType, 0, 0).get(0);
            this.currentOrder.getPaymentInformation().setPaymentType(type);
            this.currentOrder = customerOrderBp.update(this.currentOrder);
            AuthorizationStrategy strategy;
            if (!StringUtils.isEmpty(credentialPropertiesMap().get(CC_PREAUTH_STRATEGY.getValue()))
                    && credentialPropertiesMap().get(CC_PREAUTH_STRATEGY.getValue()).toLowerCase().equals(ONE_DOLLAR_STRATEGY)) {
                strategy = AuthorizationStrategy.ONE;
            }
            else if (!StringUtils.isEmpty(credentialPropertiesMap().get(CC_PREAUTH_STRATEGY.getValue()))
                    && credentialPropertiesMap().get(CC_PREAUTH_STRATEGY.getValue()).toLowerCase().equals(NO_PREAUTH_STRATEGY)) {
                strategy = AuthorizationStrategy.NONE;
            }
            else {
                strategy = AuthorizationStrategy.FULL;
            }
            if (strategy != AuthorizationStrategy.NONE) {
                PaymentGatewayTransaction transaction = paymentGatewayService
                        .createPaymentGatewayTransaction(this.currentOrder);
                CreditCardTransactionResult result = paymentGatewayService.authorize(transaction);
                if (!result.isSucceeded()) {
                    throw new CardAuthorizationException();
                }
            }
        }
        else if (!NoCardAction.INVOICE.equals(container.getPage().getNoCardAction())
                && (container.getValue().get(container.getField().get("payment type selection")) == null || !"Invoice".equals(container.getValue().get(container.getField().get("payment type selection")).toString()))) {
            //if credit card isn't set, and it's not forced to use invoice, uses set payment information
            PaymentInformation assignedInfo = getAssignedPaymentInformation();
            if (assignedInfo != null) {
                currentOrder.setPaymentInformation(assignedInfo);
                if (currentOrder.getPaymentInformation().getBillingAddress() == null) {
                    currentOrder.getPaymentInformation().setBillingAddress(new Address());
                }
                return;
            }
        }
        if (this.currentOrder.getPaymentInformation() == null) {
            this.currentOrder.setPaymentInformation(new PaymentInformation());
            this.currentOrder.getPaymentInformation().setBillingAddress(new Address());
        }
        if (this.currentOrder.getPaymentInformation().getPaymentType() == null) {
            PaymentType type = paymentTypeDao.searchByExactExample(searchType, 0, 0).get(0);
            this.currentOrder.getPaymentInformation().setPaymentType(type);
        }
    }

    /**
     * Returns the payment information to be used, based on the Account, Credential, and User. This will be a new, 
     * unmanaged entity.
     * 
     * @return
     */
    private PaymentInformation getAssignedPaymentInformation() {
    	return this.customerOrderBp.getAssignedPaymentInformation(accountBp.findById(this.getCurrentAXCXU().getAccount().getId(), Account.class), 
    			credentialBp.findById(this.getCurrentAXCXU().getCredential().getId(), Credential.class), 
    			userBp.findById(this.getCurrentAXCXU().getUser().getId(), SystemUser.class));
    }

    //END "placeOrder()" SECTION

    protected class CardAuthorizationException extends Exception {

        private static final long serialVersionUID = -7025404058700293763L;

    }

    protected class TooManyCreditCardVerificationFailuresException extends Exception {

        private static final long serialVersionUID = -5797578994849839082L;

    }

    protected static class OrderPlaceException extends Exception {

        private static final long serialVersionUID = 2099297066418666453L;

        private String message = "";

        public OrderPlaceException() {

        };

        public OrderPlaceException(String message) {
            this.message = message;
        }

        public String getMessage() {
            return this.message;
        }
    }

	public CashoutPageContainer getContainer() {
		return container;
	}

	public void setContainer(CashoutPageContainer container) {
		this.container = container;
	}

	public Conversation getConversation() {
		return conversation;
	}

	public void setConversation(Conversation conversation) {
		this.conversation = conversation;
	}
	
	public String createRequestforQuote() {		
		this.shouldPlace = false;
        String toReturn = this.placeOrder();
        this.shouldPlace = true;
        currentOrder.setOrderDate(new Date());
        for (PoNumber ponum : currentOrder.getPoNumbers()) {
        	if(CustomerOrder.APD_PO_VALUE.equals(ponum.getType().getName())) {
        		ponum.setValue(ponum.getValue()+"-Q");
        	}
        }
        customerOrderBp.setOrderStatus(currentOrder, statusBp.getOrderStatus(OrderStatusEnum.REQUEST_FOR_QUOTE));
        return toReturn;
        
	}
}
