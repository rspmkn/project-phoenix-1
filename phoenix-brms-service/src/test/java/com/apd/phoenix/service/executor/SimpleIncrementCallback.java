package com.apd.phoenix.service.executor;

import java.util.concurrent.atomic.AtomicLong;
import javax.inject.Named;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.executor.api.CommandCallback;
import com.apd.phoenix.service.executor.api.CommandContext;
import com.apd.phoenix.service.executor.api.ExecutionResults;

/**
 *
 * @author salaboy
 */
@Named(value = "SimpleIncrementCallback")
public class SimpleIncrementCallback implements CommandCallback {

    private static final Logger LOGGER = LoggerFactory.getLogger(SimpleIncrementCallback.class);

    public void onCommandDone(CommandContext ctx, ExecutionResults results) {
        String businessKey = (String) ctx.getData("businessKey");
        LOGGER
                .info(" >>> Before Incrementing = "
                        + ((AtomicLong) AsyncProcessIT.cachedEntities.get(businessKey)).get());
        ((AtomicLong) AsyncProcessIT.cachedEntities.get(businessKey)).incrementAndGet();
        LOGGER.info(" >>> After Incrementing = " + AsyncProcessIT.cachedEntities.get(businessKey));

    }
}
