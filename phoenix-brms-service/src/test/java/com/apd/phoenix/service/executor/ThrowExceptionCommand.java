/*
 * To change this template, choose Tools | Templates and open the template in
 * the editor.
 */
package com.apd.phoenix.service.executor;

import javax.inject.Named;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.executor.api.Command;
import com.apd.phoenix.service.executor.api.CommandContext;
import com.apd.phoenix.service.executor.api.ExecutionResults;

/**
 *
 * @author salaboy
 */
@Named(value = "ThrowExceptionCmd")
public class ThrowExceptionCommand implements Command {

    private static final Logger LOGGER = LoggerFactory.getLogger(ThrowExceptionCommand.class);

    public ExecutionResults execute(CommandContext ctx) {
        LOGGER.info(">>> Hi This is the Exception command!");
        throw new RuntimeException("Test Exception!");
    }

}
