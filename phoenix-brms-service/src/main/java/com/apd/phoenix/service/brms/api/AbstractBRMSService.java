package com.apd.phoenix.service.brms.api;

import javax.inject.Inject;
import org.kie.api.runtime.KieSession;

public abstract class AbstractBRMSService implements BRMSService {

    @Inject
    KieSessionFactory kieSessionFactory;

    @Override
    public KieSession loadKieSession(int sessionId, String snapshot) {
        return kieSessionFactory.loadKieSession(sessionId, snapshot);
    }

    @Override
    public KieSession newKieSession() {
        return kieSessionFactory.newKieSession();
    }

}
