/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.service.brms.handlers;

import java.util.Map;
import org.kie.api.runtime.process.WorkItemHandler;
import org.kie.api.runtime.process.WorkItem;
import org.kie.api.runtime.process.WorkItemManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class SimpleAbortWorkItemHandler implements WorkItemHandler {

    private static final Logger logger = LoggerFactory.getLogger(SimpleAbortWorkItemHandler.class);

    @Override
    public void executeWorkItem(WorkItem wi, WorkItemManager wim) {
        Map<String, Object> results = this.obtainResults(wi);
        wim.completeWorkItem(wi.getId(), results);
    }

    @Override
    public void abortWorkItem(WorkItem wi, WorkItemManager wim) {
        logger.info("Aborting workItem");
        wim.abortWorkItem(wi.getId());
    }

    public abstract Map<String, Object> obtainResults(WorkItem wi);
}
