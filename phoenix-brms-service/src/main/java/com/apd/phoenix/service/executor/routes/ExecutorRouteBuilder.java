package com.apd.phoenix.service.executor.routes;

import javax.ejb.Stateful;
import javax.inject.Named;
import org.apache.camel.LoggingLevel;
import com.apd.phoenix.service.camel.DelayBean;
import com.apd.phoenix.service.executor.processors.ExecutorProcessor;
import com.apd.phoenix.service.integration.multitenancy.AbstractTenantAwareRouteBuilder;

@Named
@Stateful
public class ExecutorRouteBuilder extends AbstractTenantAwareRouteBuilder {

    private String source;

    private String sourceDlq;

    @Override
    public void configureRouteImplementation() {

        //Generic error handler
        errorHandler(deadLetterChannel(sourceDlq).maximumRedeliveries(0));

        //Exception handling
        onException(Exception.class).maximumRedeliveries(0).handled(true)
        //Log
                .log(LoggingLevel.INFO, "Error in workflow processing: ${exception.stacktrace}")
                //Send notification
                .bean(ExecutorProcessor.class, "sendErrorNotification").to(sourceDlq);

        //Command source
        from(source)
        //delays the messages when deploying
                .autoStartup(false).routePolicy(DelayBean.getDelayPolicy())
                //Process command
                .bean(ExecutorProcessor.class, "processCommand");

    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getSourceDlq() {
        return sourceDlq;
    }

    public void setSourceDlq(String sourceDlq) {
        this.sourceDlq = sourceDlq;
    }

}
