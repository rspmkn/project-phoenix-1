/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.service.brms.handlers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import org.kie.api.runtime.process.WorkItem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.business.CustomerOrderBp;
import com.apd.phoenix.service.business.EmailFactoryBp;
import com.apd.phoenix.service.business.OrderProcessLookupBp;
import com.apd.phoenix.service.email.api.EmailService;
import com.apd.phoenix.service.message.api.Message;
import com.apd.phoenix.service.message.api.MessageService;
import com.apd.phoenix.service.model.CustomerOrder;
import com.apd.phoenix.service.model.OrderLog.EventType;
import com.apd.phoenix.service.model.factory.DtoFactory;
import com.apd.phoenix.service.model.factory.DtoFactory.ParsingException;

@Stateless
@LocalBean
public class EmailHandler extends SimpleAbortWorkItemHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(EmailHandler.class);

    @Inject
    CustomerOrderBp customerOrderBp;

    @Inject
    MessageService messageService;

    @Inject
    EmailService emailService;

    @Inject
    EmailFactoryBp emailFactoryBp;

    @Inject
    private OrderProcessLookupBp processLookupBp;

    @Override
    public Map<String, Object> obtainResults(WorkItem wi) {
        Map<String, Object> toReturn = new HashMap<>();
        //the variable name "orderId" is a misnomer, a legacy from when the process ID was set to the order ID
        long orderId = processLookupBp.getOrderFromProcessId((Long) wi.getParameter("orderId")).getId();
        Object content = wi.getParameter("emailContent");
        String eventType = (String) wi.getParameter("eventType");
        
        CustomerOrder customerOrder = customerOrderBp.findById(orderId, CustomerOrder.class); 
        if (content == null) {
        	try {
				content = DtoFactory.createPurchaseOrderDto(customerOrder);
			} catch (ParsingException e) {
				LOGGER.error("could not parse purchase order: ", e);
			}
        }
        
        EventType type = EventType.valueOf(eventType);
        
        List<Message> messages = emailFactoryBp.createOrderEmails(orderId, content, type);
        for (Message message : messages) {
        	if (message != null) {
        		messageService.sendOrderMessage(customerOrder, message, type);
        	}
        	else {
        		LOGGER.error("Tried send an email with a null message");
        	}
        }
        
        return toReturn;
    }
}
