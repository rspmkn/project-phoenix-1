package com.apd.phoenix.service.brms.api;

import org.kie.api.runtime.KieSession;
import org.kie.api.task.TaskService;

public interface BRMSService {

    public KieSession loadKieSession(int sessionId, String snapshot);

    public KieSession newKieSession();

    public TaskService getTaskServiceClient();
}
