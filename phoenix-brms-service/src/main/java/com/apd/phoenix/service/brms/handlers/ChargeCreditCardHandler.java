/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.service.brms.handlers;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import org.apache.commons.lang.StringUtils;
import org.kie.api.runtime.process.WorkItem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.business.CommentBp;
import com.apd.phoenix.service.business.CustomerOrderBp;
import com.apd.phoenix.service.business.EmailFactoryBp;
import com.apd.phoenix.service.business.LineItemXShipmentBp;
import com.apd.phoenix.service.business.OrderProcessLookupBp;
import com.apd.phoenix.service.business.ReturnOrderBp;
import com.apd.phoenix.service.business.ShipmentBp;
import com.apd.phoenix.service.message.api.MessageService;
import com.apd.phoenix.service.model.CardInformation;
import com.apd.phoenix.service.model.CreditCardTransactionLog;
import com.apd.phoenix.service.model.CustomerOrder;
import com.apd.phoenix.service.model.LineItemStatus;
import com.apd.phoenix.service.model.LineItemXShipment;
import com.apd.phoenix.service.model.MessageMetadata.MessageType;
import com.apd.phoenix.service.model.OrderLog;
import com.apd.phoenix.service.model.ReturnOrder;
import com.apd.phoenix.service.model.Shipment;
import com.apd.phoenix.service.model.dto.CardNotificationDto;
import com.apd.phoenix.service.model.dto.LineItemStatusDto;
import com.apd.phoenix.service.model.dto.LineItemXShipmentDto;
import com.apd.phoenix.service.model.dto.ReturnOrderDto;
import com.apd.phoenix.service.model.dto.ShipmentDto;
import com.apd.phoenix.service.model.dto.OrderLogDto.EventTypeDto;
import com.apd.phoenix.service.model.factory.DtoFactory;
import com.apd.phoenix.service.model.factory.DtoFactory.ParsingException;
import com.apd.phoenix.service.payment.ws.CreditCardTransactionResult;
import com.apd.phoenix.service.payment.ws.CreditCardTransactionStatusResult;
import com.apd.phoenix.service.payment.ws.CreditCardTransactionSummaryStatus;
import com.apd.phoenix.service.payment.ws.client.PaymentGatewayPropertiesLoader;
import com.apd.phoenix.service.payment.ws.client.PaymentGatewayService;
import com.apd.phoenix.service.payment.ws.client.PaymentGatewayTransaction;
import com.apd.phoenix.service.payment.ws.client.TransactionType;

@Stateless
@LocalBean
public class ChargeCreditCardHandler extends SimpleAbortWorkItemHandler {

    private static final String OUTPUT_SHOULD_RETRY_CARD = "outputShouldRetryCard";

    private static final Logger logger = LoggerFactory.getLogger(ChargeCreditCardHandler.class);

    @Inject
    ShipmentBp shipmentBp;

    @Inject
    CustomerOrderBp customerOrderBp;

    @Inject
    ReturnOrderBp returnOrderBp;

    @Inject
    MessageService messageService;

    @Inject
    PaymentGatewayService paymentGatewayService;

    @Inject
    LineItemXShipmentBp lineItemXShipmentBp;

    @Inject
    EmailFactoryBp emailFactoryBp;

    @Inject
    private CommentBp commentBp;

    @Inject
    private PaymentTransactionProcessor transactionProcessor;

    @Inject
    private OrderProcessLookupBp processLookupBp;

    @Override
    public Map<String, Object> obtainResults(WorkItem wi) {
        Map<String, Object> toReturn = new HashMap<>();
        ShipmentDto shipmentDto = (ShipmentDto) wi.getParameter("shipmentDto");
        ReturnOrderDto returnOrderDto = (ReturnOrderDto) wi.getParameter("returnOrderDtoIn");
        //support for legacy BRMS workflows (before 1.3.11)
        ReturnOrderDto returnOrderDtoLegacy = (ReturnOrderDto) wi.getParameter("returnOrderDto");
        
        if (shipmentDto != null){
        	toReturn = chargeCard(wi);
        } else if (returnOrderDto != null || returnOrderDtoLegacy != null) {
        	toReturn = creditCard(wi);
        }
        
        return toReturn;
    }

    private Map<String, Object> chargeCard(WorkItem wi){
    	Map<String, Object> toReturn = new HashMap<>();
    	
        ShipmentDto shipmentDto = (ShipmentDto) wi.getParameter("shipmentDto");
        BigDecimal splitCharge = (BigDecimal) wi.getParameter("splitCharge");
        String newCardNumber = (String) wi.getParameter("newCardNumber");
        String newCardDate = (String) wi.getParameter("newCardDate");
        Integer transactionAttempts = (Integer) wi.getParameter("transactionAttempts");
        if (transactionAttempts == null) {
        	transactionAttempts = 0;
        }
        boolean retryLegacyTransactions = (wi.getParameter("retryLegacyTransactions") == null);
        //the variable name "orderId" is a misnomer, a legacy from when the process ID was set to the order ID
        CustomerOrder customerOrder = processLookupBp.getOrderFromProcessId((Long) wi.getParameter("orderId"));
        
        if (newCardNumber != null && !newCardNumber.isEmpty() && newCardDate != null && !newCardDate.isEmpty()){
        	CardInformation old = customerOrder.getPaymentInformation().getCard();
        	CardInformation card = new CardInformation();
        	card.setDebit(old.getDebit());
        	card.setNameOnCard(old.getNameOnCard());
        	card.setGhost(old.isGhost());
        	card.setCvvIndicator(old.getCvvIndicator());
        	card.setNumber(newCardNumber);
            try {
            	card.setExpiration(CardInformation.addExpirationMargin((new SimpleDateFormat("MM-yyyy")).parse(newCardDate)));
            } catch (ParseException ex) {
                logger.error("Unable to parse new card expiration date, " + newCardDate);
                if (logger.isDebugEnabled()) {
                	logger.debug("Stack trace", ex);
                }
                return failWithoutRetries(toReturn, shipmentDto);
            }
        	customerOrder.getPaymentInformation().setCard(card);
            try {
                card = paymentGatewayService.storeCreditCard(customerOrder.getPaymentInformation());
            } catch (Exception ex) {
                logger.error("Error while attempting to store or retrieve card from card vault", ex);
                return failWithoutRetries(toReturn, shipmentDto);
            }
        	customerOrder = customerOrderBp.update(customerOrder);
        }
        
        if (shipmentDto != null){
            List<Set<LineItemXShipment>> chargeList = new ArrayList<>();
            BigDecimal subTotal = BigDecimal.ZERO;
            
            Shipment shipment = shipmentBp.retrieveShipment(shipmentDto);
            
            if (shipment == null) {
            	logger.warn("No shipment found for shipment DTO, probably due to all items failing validation");
                return failWithoutRetries(toReturn, shipmentDto);
            }
            
            //the following block of code is only called for transactions in 1.3.1732 and earlier. For later transactions, 
            //the ShipmentDto state is maintained, so we don't need to find any unpaid Shipment on the order.
            if (retryLegacyTransactions) {
	            //checks to see if every item on the shipment has been paid
	            boolean allPaid = true;
	            for (LineItemXShipment lixs : shipment.getItemXShipments()) {
	            	if (lixs.getPaid() == null || !lixs.getPaid()) {
	            		allPaid = false;
	            	}
	            }
	            
	            //if so, takes a different, unpaid shipment from the order
	            if (allPaid) {
	            	for (Shipment newShipment : customerOrder.getShipments()) {
	            		if (newShipment.getToCharge() != null && newShipment.getToCharge()) {
	            			shipment = newShipment;
	            			shipment.setToCharge(false);
	            			try {
								shipmentDto = DtoFactory.createShipmentDto(shipment);
							} catch (ParsingException e) {
								logger.error("Error creating shipment dto when charging a new shipment", e);
							}
	            		}
	            	}
	            }
            }
            
            //Handle retries
    		//If this is a retry, attempt to see if the previous try succeeded
            Map<String, Boolean> transactionIdToSuccess = new HashMap<>();
            for (LineItemXShipment lixs : shipment.getItemXShipments()) {
            	String transactionId = lixs.getCcTransactionId();
            	if (StringUtils.isNotBlank(transactionId)) {
            		if (!transactionIdToSuccess.containsKey(transactionId)) {
            			CreditCardTransactionStatusResult statusResult;
            			try {
            				statusResult = transactionProcessor.getTransactionStatus(transactionId);
            			} catch (Exception e) {
            				logger.warn("Exception while getting transaction from transaction processor", e);
                			return retry(toReturn, shipmentDto, transactionAttempts, null);
            			}
            			if (statusResult != null && statusResult.isSucceeded() && statusResult.getTransactionType().equals(TransactionType.CAPTURE)){
            				transactionIdToSuccess.put(transactionId, true);
            			} else if (statusResult != null && statusResult.getSummaryStatus() != null 
            					&& statusResult.getSummaryStatus().equals(CreditCardTransactionSummaryStatus.IN_PROGRESS)) {
        					//Go straight to retry
        					logger.warn("Transaction Still in progress, retrying in 5 minutes");
        					return retry(toReturn, shipmentDto, transactionAttempts, transactionId);
            			} else {
            				transactionIdToSuccess.put(transactionId, false);
            			}
            		}
            		
            	}
            }
            transactionProcessor.updateFromPreviousTransaction(transactionIdToSuccess, shipment.getItemXShipments());

            for (LineItemXShipment lixs : shipment.getItemXShipments()) {
        		this.lineItemXShipmentBp.refresh(lixs);
        	}
            
            //Build chargeList
            Set<LineItemXShipment> currentSet = new HashSet<>();
            for (LineItemXShipment lineItemXShipment : shipment.getItemXShipments()){
            	if (!lineItemXShipment.getPaid()) {
            		//Is this really the best way to multiply a BigDecimal by a BigInteger?
            		BigDecimal itemTotal = lineItemXShipment.getLineItem().getUnitPrice().multiply(BigDecimal.valueOf(lineItemXShipment.getQuantity().longValue()));
                
            		//Check to see if we need to flush the currentSet into the chargeList
            		if (splitCharge != null && splitCharge.compareTo(BigDecimal.ZERO) > 0 
            				&& subTotal.add(itemTotal).compareTo(splitCharge) > 0) {
                        Set<LineItemXShipment> toAdd = new HashSet<>();
                        for (LineItemXShipment lineItemToAdd : currentSet){
                            toAdd.add(lineItemToAdd);
                        }
                        chargeList.add(toAdd);
                        subTotal = BigDecimal.ZERO;
                        currentSet = new HashSet<>();
            		} 

        			subTotal = subTotal.add(itemTotal);
        			currentSet.add(lineItemXShipment);
            	}
            }
            //Final flush
            if (!currentSet.isEmpty()){
            	Set<LineItemXShipment> toAdd = new HashSet<>();
            	for (LineItemXShipment lineItemToAdd : currentSet){
            		toAdd.add(lineItemToAdd);
            	}
            	chargeList.add(toAdd);
            }
            
            if (chargeList.isEmpty()) {
            	logger.warn("Charge with no items, possibly due to all being paid already");
            	commentBp.addSystemComment(customerOrder, "A charge attempt was made with no items, possibly because "
            			+ "all items on the shipment \"" + shipment.getTrackingNumber() + "\" have already been charged in an earlier transaction. This is not "
            			+ "a problem unless the items aren't listed as charged at the payment gateway.");
            	this.resendCardNotification(customerOrder, shipment.getTrackingNumber());
        		shipmentDto = this.updateShipmentDto(shipmentDto);
            } else {
            	logger.info("Charging the CC");
            }

            for(Set<LineItemXShipment> lixsSet : chargeList){
            	CreditCardTransactionResult creditCardTransactionResult = null;
            	PaymentGatewayTransaction paymentGatewayTransaction;
            	try {
	            	paymentGatewayTransaction = paymentGatewayService.createPaymentGatewayTransaction(customerOrder, lixsSet);
	            	paymentGatewayService.authorize(paymentGatewayTransaction);
            	} catch (Exception e) {
            		//the "authorize" function doesn't charge the card, so it's fine to retry it
            		logger.error("Exception while creating and authorizing transaction with gateway", e);
            		paymentGatewayTransaction = null;
            	}
            	if (paymentGatewayTransaction != null && paymentGatewayTransaction.getCreditCardTransaction() != null 
            			&& paymentGatewayTransaction.getCreditCardTransaction().getTransactionKey() != null 
            			&& StringUtils.isNotBlank(paymentGatewayTransaction.getCreditCardTransaction().getTransactionKey().getValue())) {
            		transactionProcessor.setCreditCardTransactionId(paymentGatewayTransaction.getCreditCardTransaction().getTransactionKey().getValue(), lixsSet);
                	creditCardTransactionResult = transactionProcessor.processAuthorizedPaymentGatewayTransaction(paymentGatewayTransaction, customerOrder, lixsSet, shipmentDto);
            	}
            	//refreshing the cached version of the lineitemXShipments, in case the values changed in the separate transaction
            	for (LineItemXShipment lixs : lixsSet) {
            		this.lineItemXShipmentBp.refresh(lixs);
            	}
            	//Parse the credit card transaction result
            	//TODO:Incorporate failure paths
            	if (creditCardTransactionResult != null && creditCardTransactionResult.isSucceeded() != null && creditCardTransactionResult.isSucceeded()) {
            		this.sendCardNotification(customerOrder, creditCardTransactionResult.getTransactionKey().getValue());
            		shipmentDto = this.updateShipmentDto(shipmentDto);
            	} else {
            		if (creditCardTransactionResult != null) {
	            		CardNotificationDto notification = new CardNotificationDto();
	                    //the variable name "orderId" is a misnomer, a legacy from when the process ID was set to the order ID
	            		notification.setOrderId(processLookupBp.getOrderFromProcessId((Long) wi.getParameter("orderId")).getId());
	            		notification.setEventType(EventTypeDto.CREDIT_CARD_DECLINED);
	            		if (creditCardTransactionResult.getTransactionKey() != null) {
	            			notification.setTransactionKey(creditCardTransactionResult.getTransactionKey().getValue());
	            		}
	            		notification.setShipmentDto(shipmentDto);
	            		messageService.sendCardNotification(notification, MessageType.EMAIL);
	            		return failWithoutRetries(toReturn, shipmentDto);
            		}
            		else {
            			return retry(toReturn, shipmentDto, transactionAttempts, null);
            		}
            	}
        	}
    		return success(toReturn, shipmentDto);
        }
        
        //should only happen if the shipmentDto is null
    	return failWithoutRetries(toReturn, shipmentDto);
    }

    private void sendCardNotification(CustomerOrder customerOrder, String transactionKey) {
        //If the previous try succeeded, Handle as we would a success
        CardNotificationDto notification = new CardNotificationDto();
        notification.setOrderId(customerOrder.getId());
        notification.setEventType(EventTypeDto.CREDIT_CARD_RECEIPT);
        notification.setTransactionKey(transactionKey);
        messageService.sendCardNotification(notification, MessageType.EMAIL);
    }

    private Map<String, Object> retry(Map<String, Object> toReturn, ShipmentDto shipmentDto,
            Integer transactionAttempts, String transactionKey) {
        if (transactionAttempts == null
                || transactionAttempts < PaymentGatewayPropertiesLoader.getInstance().getProperties().getInt(
                        "technicalErrorRetryAttempts", 5)) {
            toReturn.put(OUTPUT_SHOULD_RETRY_CARD, true);
            toReturn.put("outputTransactionAttempts", transactionAttempts != null ? transactionAttempts + 1 : 1);

        }
        else {
            toReturn.put(OUTPUT_SHOULD_RETRY_CARD, false);
        }
        if (StringUtils.isNotBlank(transactionKey)) {
            toReturn.put("transactionKeyOutput", transactionKey);
        }
        toReturn.put("chargeFailed", true);
        toReturn.put("outputDto", shipmentDto);
        return toReturn;
    }

    private Map<String, Object> failWithoutRetries(Map<String, Object> toReturn, ShipmentDto shipmentDto) {
        toReturn.put(OUTPUT_SHOULD_RETRY_CARD, false);
        toReturn.put("chargeFailed", true);
        toReturn.put("outputDto", shipmentDto);
        return toReturn;
    }

    private Map<String, Object> success(Map<String, Object> toReturn, ShipmentDto shipmentDto) {
        toReturn.put(OUTPUT_SHOULD_RETRY_CARD, false);
        toReturn.put("chargeFailed", false);
        toReturn.put("outputDto", shipmentDto);
        return toReturn;
    }

    private Map<String, Object> creditCard(WorkItem wi){
    	Map<String, Object> toReturn = new HashMap<>();
    	ReturnOrderDto returnOrderDto = (ReturnOrderDto) wi.getParameter("returnOrderDtoIn");
    	if (returnOrderDto == null) {
    		returnOrderDto = (ReturnOrderDto) wi.getParameter("returnOrderDto");
    	}
    	ReturnOrder returnOrder = returnOrderBp.getUnreconciledReturnOrder(returnOrderDto);
        //the variable name "orderId" is a misnomer, a legacy from when the process ID was set to the order ID
    	CustomerOrder customerOrder = processLookupBp.getOrderFromProcessId((Long) wi.getParameter("orderId"));
    	if (returnOrder != null) {
			CreditCardTransactionResult creditCardTransactionResult;
    		try {
		    	PaymentGatewayTransaction paymentGatewayTransaction = paymentGatewayService.createReturnPaymentGatewayTransaction(customerOrder, returnOrder.getItems());
	        	creditCardTransactionResult = paymentGatewayService.credit(paymentGatewayTransaction, returnOrder);
        	} catch (Exception e) {
        		logger.error("Exception while creating and crediting transaction with gateway", e);
        		creditCardTransactionResult = null;
        	}
        	if (creditCardTransactionResult != null && creditCardTransactionResult.isSucceeded() != null && creditCardTransactionResult.isSucceeded()) {
				CardNotificationDto notification = new CardNotificationDto();
		        //the variable name "orderId" is a misnomer, a legacy from when the process ID was set to the order ID
				notification.setOrderId(processLookupBp.getOrderFromProcessId((Long) wi.getParameter("orderId")).getId());
				notification.setEventType(EventTypeDto.CREDIT_CARD_CREDIT);
				notification.setTransactionKey(creditCardTransactionResult.getTransactionKey().getValue());
				messageService.sendCardNotification(notification, MessageType.EMAIL);
				returnOrderBp.reconcile(returnOrder);
				toReturn.put("creditFailedOut", false);
			} else {
				toReturn.put("creditFailedOut", true);
			}
    	}
    	else {
    		logger.error("Tried to reconcile a return that does not exist!");
    	}
    	return toReturn;
    }

    private void resendCardNotification(CustomerOrder order, String trackingNumber) {
        logger.info("Attempting to resend card notification for shipment {}", trackingNumber);
        String transactionKey = null;
        for (OrderLog log : order.getOrderLogs()) {
            if (log instanceof CreditCardTransactionLog) {
                CreditCardTransactionLog ccLog = (CreditCardTransactionLog) log;
                if (ccLog.isTransactionSuccess()
                        && trackingNumber.equals(ccLog.getTrackingNum())
                        && (TransactionType.AUTHORIZE_CAPTURE.getTransactionValue().equals(ccLog.getTransactionType()) || TransactionType.CAPTURE
                                .getTransactionValue().equals(ccLog.getTransactionType()))) {
                    transactionKey = ccLog.getTransactionKey();
                }
            }
        }
        if (StringUtils.isNotBlank(transactionKey)) {
            sendCardNotification(order, transactionKey);
        }
    }

    private ShipmentDto updateShipmentDto(ShipmentDto shipmentDto) {
        if (shipmentDto != null) {
            logger.info("Charging for the following shipment------------");
            logger.info("Tracking number = " + shipmentDto.getTrackingNumber());
            for (LineItemXShipmentDto lixsd : shipmentDto.getLineItemXShipments()) {
                if (lixsd != null) {
                    lixsd.setPaid(true);
                    if (lixsd.getLineItemDto() != null) {
                        if (lixsd.getLineItemDto().getStatus() != null) {
                            if (lixsd.getLineItemDto().getStatus().getValue().equals(
                                    LineItemStatus.LineItemStatusEnum.PARTIAL_SHIP.getValue())) {
                                lixsd.getLineItemDto().setStatus(new LineItemStatusDto());
                                lixsd.getLineItemDto().getStatus().setValue(
                                        LineItemStatus.LineItemStatusEnum.PARTIAL_CHARGE.getValue());
                            }
                            else if (lixsd.getLineItemDto().getStatus().getValue().equals(
                                    LineItemStatus.LineItemStatusEnum.FULLY_SHIPPED.getValue())) {
                                lixsd.getLineItemDto().setStatus(new LineItemStatusDto());
                                lixsd.getLineItemDto().getStatus().setValue(
                                        LineItemStatus.LineItemStatusEnum.FULLY_CHARGED.getValue());
                            }
                        }
                        logger.info("-" + lixsd.getLineItemDto().getApdSku());
                        logger.info("--Quantity=" + lixsd.getQuantity());
                    }
                }
            }
            logger.info("------------------------------------------------");
        }

        return shipmentDto;
    }
}
