package com.apd.phoenix.service.brms.handlers;

import javax.inject.Inject;
import org.kie.api.runtime.process.WorkItem;
import com.apd.phoenix.core.utility.PropertiesLoader;
import com.apd.phoenix.service.brms.core.workflow.PhoenixTaskSummary;
import com.apd.phoenix.service.brms.core.workflow.UserTaskService;
import com.apd.phoenix.service.business.CustomerOrderBp;
import com.apd.phoenix.service.business.OrderLogBp;
import com.apd.phoenix.service.business.OrderProcessLookupBp;
import com.apd.phoenix.service.business.OrderStatusBp;
import com.apd.phoenix.service.model.CustomerOrder;
import com.apd.phoenix.service.model.OrderStatus;
import com.apd.phoenix.service.model.OrderStatus.OrderStatusEnum;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Stateless
@LocalBean
public class UpdateOrderStatusWorkItemHandler extends SimpleAbortWorkItemHandler {

    private static final Logger logger = LoggerFactory.getLogger(UpdateOrderStatusWorkItemHandler.class);

    @Inject
    CustomerOrderBp customerOrderBp;

    @Inject
    OrderLogBp orderLogBp;

    @Inject
    OrderStatusBp orderStatusBp;

    @Inject
    private OrderProcessLookupBp processLookupBp;

    @Inject
    private UserTaskService userTaskService;

    @Override
    public Map<String, Object> obtainResults(WorkItem wi) {
    	HashMap<String, Object> results = new HashMap<>();
        String value = (String) wi.getParameter("status");
        if (value != null) {
            //the variable name "orderId" is a misnomer, a legacy from when the process ID was set to the order ID
            CustomerOrder customerOrder = processLookupBp.getOrderFromProcessId((Long) wi.getParameter("orderId"));
            //used when just logging an event, instead of changing the order status
            Boolean noStatusChange = null;
            if(wi.getParameter("noStatusChange") instanceof String){
            	noStatusChange = Boolean.valueOf((String) wi.getParameter("noStatusChange"));
            } else {
            	noStatusChange = (Boolean) wi.getParameter("noStatusChange");
            }

            results.put("invalidated", Boolean.FALSE);
            logger.info("Searching for '" + value + "'");
            if (value.equals("REVERT_STATUS")) {
                logger.info("Reverting status");
                customerOrderBp.revertOrderStatus(customerOrder);
                return results;
            }
            OrderStatus orderStatus = orderStatusBp.getOrderStatusByValue(value);
            List<PhoenixTaskSummary> tasksToResolve = new ArrayList<>();
            if (orderStatus != null && (OrderStatusEnum.SHIPPED.getValue().equals(orderStatus.getValue()))
					            		|| OrderStatusEnum.CHARGED.getValue().equals(orderStatus.getValue())
					            		|| OrderStatusEnum.BILLED.getValue().equals(orderStatus.getValue())) {
                //Check for open backordered task
                 tasksToResolve.addAll(userTaskService.getBackOrderHumanTasks(customerOrder));

            }
            if(orderStatus != null && (OrderStatusEnum.CHARGED.getValue().equals(orderStatus.getValue())
            		|| OrderStatusEnum.BILLED.getValue().equals(orderStatus.getValue()))){
            	 tasksToResolve.addAll(userTaskService.getItemRejectionTasks(customerOrder));
            }
            
            if(orderStatus != null && (OrderStatusEnum.REJECTED.getValue().equals(orderStatus.getValue()))) {
            	 customerOrderBp.internalRejectOrder(customerOrder);
            }
            
            if(orderStatus != null && (OrderStatusEnum.DENIED.getValue().equals(orderStatus.getValue()))) {
            	 customerOrderBp.denyOrder(customerOrder);
            }
            
            if (noStatusChange != null && noStatusChange) {
            	customerOrder = customerOrderBp.addLog(customerOrder, orderStatus, null);
            }
            else {
            	customerOrder = customerOrderBp.setOrderStatus(customerOrder, orderStatus);
            }

			for(PhoenixTaskSummary task:tasksToResolve){
                //if open task exists, close it
                userTaskService.exitTask(task.getId());
            }
        }
        return results;
    }
}
