/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.service.brms.handlers;

import java.util.HashMap;
import java.util.Map;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import org.kie.api.runtime.process.WorkItem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.business.CustomerOrderBp;
import com.apd.phoenix.service.business.EmailFactoryBp;
import com.apd.phoenix.service.business.OrderProcessLookupBp;
import com.apd.phoenix.service.email.api.EmailService;
import com.apd.phoenix.service.message.api.MessageService;
import com.apd.phoenix.service.model.Credential;
import com.apd.phoenix.service.model.CustomerOrder;
import com.apd.phoenix.service.model.MessageMetadata.MessageType;
import com.apd.phoenix.service.model.dto.LineItemDto;
import com.apd.phoenix.service.model.dto.LineItemXShipmentDto;
import com.apd.phoenix.service.model.dto.ShipmentDto;

@Stateless
@LocalBean
public class SendShipmentNoticeHandler extends SimpleAbortWorkItemHandler {

    private static final Logger logger = LoggerFactory.getLogger(SendShipmentNoticeHandler.class);

    @Inject
    CustomerOrderBp customerOrderBp;

    @Inject
    MessageService messageService;

    @Inject
    private OrderProcessLookupBp processLookupBp;

    @Override
    public Map<String, Object> obtainResults(WorkItem wi) {
        Map<String, Object> toReturn = new HashMap<>();
        ShipmentDto shipmentDto = (ShipmentDto) wi.getParameter("shipmentDto");
        //the variable name "orderId" is a misnomer, a legacy from when the process ID was set to the order ID
        CustomerOrder customerOrder = processLookupBp.getOrderFromProcessId((Long) wi.getParameter("orderId"));
        shipmentDto.getCustomerOrderDto().setPartnerDestination(customerOrder.getCredential().getShipmentNotificationDestination());
        //Linked line Items from inbound dto to those with data from the order
        //TODO: the line item DTOs can get out of sync, and the sku matching isn't guaranteed to work. Refactor.
        for (LineItemXShipmentDto lineItemXShipment : shipmentDto.getLineItemXShipments()) {
            for (LineItemDto lineItemDto : shipmentDto.getCustomerOrderDto().getItems()) {
                if (lineItemDto != null && lineItemXShipment != null && lineItemDto.matchesItemOnOrder(lineItemXShipment.getLineItemDto())) {
                	lineItemDto.setStatus(lineItemXShipment.getLineItemDto().getStatus());
                	lineItemDto.setValid(lineItemXShipment.getLineItemDto().getValid());
                    lineItemXShipment.setLineItemDto(lineItemDto);
                    break;
                }
            }
        }
        Credential credential = customerOrder.getCredential();
        messageService.sendShipmentNotice(shipmentDto, credential.getShipmentNotificationMessageType());      
        if (credential.getShipmentNotificationMessageType() == null || !credential.getShipmentNotificationMessageType().equals(MessageType.EMAIL)) {
        	messageService.sendShipmentNotice(shipmentDto, MessageType.EMAIL);
        }
        toReturn.put("shipmentOutDto", shipmentDto);
        return toReturn;
    }
}
