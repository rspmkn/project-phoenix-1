/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.service.brms.handlers;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import org.kie.api.runtime.process.WorkItem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.business.OrderProcessLookupBp;
import com.apd.phoenix.service.message.api.MessageService;
import com.apd.phoenix.service.model.CanceledQuantity.CancelReason;
import com.apd.phoenix.service.model.CustomerOrder;
import com.apd.phoenix.service.model.Item;
import com.apd.phoenix.service.model.LineItem;
import com.apd.phoenix.service.model.LineItemStatus.LineItemStatusEnum;
import com.apd.phoenix.service.model.MessageMetadata.MessageType;
import com.apd.phoenix.service.model.dto.LineItemDto;
import com.apd.phoenix.service.model.dto.LineItemXShipmentDto;
import com.apd.phoenix.service.model.dto.POAcknowledgementDto;
import com.apd.phoenix.service.model.dto.ShipmentDto;
import com.apd.phoenix.service.model.factory.DtoFactory;
import com.apd.phoenix.service.model.factory.DtoFactory.ParsingException;
import com.apd.phoenix.service.workflow.WorkflowService;

@Stateless
@LocalBean
public class CancelRejectedItemsHandler extends SimpleAbortWorkItemHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(CancelRejectedItemsHandler.class);

    @Inject
    private WorkflowService workflowService;

    @Inject
    private MessageService messageService;

    @Inject
    private OrderProcessLookupBp processLookupBp;

    @Override
    public Map<String, Object> obtainResults(WorkItem wi) {
        Map<String, Object> toReturn = new HashMap<>();

        //the variable name "orderId" is a misnomer, a legacy from when the process ID was set to the order ID
        CustomerOrder customerOrder = processLookupBp.getOrderFromProcessId((Long) wi.getParameter("orderId"));
        
        POAcknowledgementDto poAckDto = (POAcknowledgementDto) wi.getParameter("poAckDtoIn");
        
        if (customerOrder != null && poAckDto != null) {

            ShipmentDto cancelDto = new ShipmentDto();
            
            try {
				cancelDto.setCustomerOrderDto(DtoFactory.createPurchaseOrderDto(customerOrder));
			} catch (ParsingException e) {
				LOGGER.error("Error creating Purchase Order DTO", e);
			}
            
            //generates the cancel DTO by iterating through the item dtos, and for each rejected item, 
            //adding it to the cancellation.
            cancelDto.setLineItemXShipments(new ArrayList<LineItemXShipmentDto>());
            List<LineItemDto> toRemoveFromAck = new ArrayList<LineItemDto>();
            for (LineItemDto lineItemDto : poAckDto.getLineItems()) {
            	if (LineItemStatusEnum.REJECTED.getValue().equals(lineItemDto.getStatus().getValue())) {
		            for (LineItem lineItem : customerOrder.getItems()) {
		            	if (lineItem.matchesItemOnOrder(lineItemDto)) {
			                Item item = lineItem.getItem();
			                LineItemXShipmentDto cancelledItem = new LineItemXShipmentDto();
			                cancelledItem.setLineItemDto(DtoFactory.createLineItemDto(lineItem,item));
			                cancelledItem.setQuantity(BigInteger.valueOf(lineItem.getQuantity()));
			                cancelledItem.setVendorComments(new ArrayList<String>());
			                cancelledItem.getVendorComments().add(CancelReason.DISC_WO_REPLACEMENT.getLabel());
			                cancelDto.getLineItemXShipments().add(cancelledItem);
		            	}
		            }
            	}
            	else {
            		toRemoveFromAck.add(lineItemDto);
            	}
            }
            
            //any items that aren't rejected are removed from the list of item dtos, so that the 
            //non-rejected items aren't sent in the IR to the customer
            poAckDto.getLineItems().removeAll(toRemoveFromAck);

            if (!cancelDto.getLineItemXShipments().isEmpty()) {
            	workflowService.cancelOrder(customerOrder.getApdPo().getValue(), cancelDto);
            }

            if (MessageType.EDI.equals(customerOrder.getCredential().getPoAcknowledgementMessageType())) {
            	messageService.sendPurchaseOrderAcknowledgment(poAckDto, MessageType.EDI);
            }
        	
        }
        
        return toReturn;
    }
}
