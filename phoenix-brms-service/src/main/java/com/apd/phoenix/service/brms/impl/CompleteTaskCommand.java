package com.apd.phoenix.service.brms.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.brms.api.AbstractBRMSCommand;
import com.apd.phoenix.service.executor.api.CommandContext;
import com.apd.phoenix.service.executor.api.ExecutionResults;
import java.util.Map;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Named;

@Named(value = "CompleteTaskCommand")
@Stateless
@LocalBean
public class CompleteTaskCommand extends AbstractBRMSCommand {

    private static final Logger logger = LoggerFactory.getLogger(CompleteTaskCommand.class);

    @Override
    public ExecutionResults execute_safe(CommandContext ctx) throws Exception {
        Long taskId = (Long) ctx.getData("taskId");
        String userId = (String) ctx.getData("userId");
        Map<String, Object> contentDataMap = (Map<String, Object>) ctx.getData("contentDataMap");
        if (userId == null) {
            throw new Exception("USER ID NOT SPECIFIED EXCEPTION");
        }
        else if (taskId == null) {
            throw new Exception("TASK ID NOT SPECIFIED");
        }
        else {
            logger.info("---------testing id=" + taskId);
            try {
                logger.info("---------completing with " + contentDataMap);
                client.complete(taskId, userId, contentDataMap);
            }
            catch (org.kie.internal.task.exception.TaskException e) {
                logger.error("Could not complete the task", e);
            }
        }

        return null;
    }
}
