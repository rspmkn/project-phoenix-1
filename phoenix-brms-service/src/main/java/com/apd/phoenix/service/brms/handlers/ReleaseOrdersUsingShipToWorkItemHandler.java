package com.apd.phoenix.service.brms.handlers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import org.kie.api.runtime.process.WorkItem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.business.AddressBp;
import com.apd.phoenix.service.business.CustomerOrderBp;
import com.apd.phoenix.service.business.OrderProcessLookupBp;
import com.apd.phoenix.service.executor.ExecutorServiceEntryPoint;
import com.apd.phoenix.service.executor.api.CommandContext;
import com.apd.phoenix.service.executor.command.api.SignalCommandConstants;
import com.apd.phoenix.service.model.Address;
import com.apd.phoenix.service.model.CustomerOrder;

@Stateless
@LocalBean
public class ReleaseOrdersUsingShipToWorkItemHandler extends SimpleAbortWorkItemHandler {

    private static final Logger logger = LoggerFactory.getLogger(ReleaseOrdersUsingShipToWorkItemHandler.class);

    @Inject
    CustomerOrderBp customerOrderBp;

    @Inject
    ExecutorServiceEntryPoint executor;

    @Inject
    AddressBp addressBp;

    @Inject
    private OrderProcessLookupBp processLookupBp;

    @Override
    public Map<String, Object> obtainResults(WorkItem wi) {
        Map<String, Object> toReturn = new HashMap<>();
        //the variable name "orderId" is a misnomer, a legacy from when the process ID was set to the order ID
        CustomerOrder customerOrder = processLookupBp.getOrderFromProcessId((Long) wi.getParameter("orderId"));
        String shipToId = customerOrder.getAddress().getShipToId();
        Address addressSearch = new Address();
        addressSearch.setShipToId(shipToId);
        List<Address> addresses = addressBp.searchByExactExample(addressSearch, 0, 0);
        if (addresses != null) {
        	for (Address address : addresses) {
        		address.setPendingShipToAuthorization(false);
                address = addressBp.update(address);
        	}
        }
        logger.info("Releasing orders because shipto("+shipToId+") for orderd=" + customerOrder.getId() + " has been approved!");
        List<Long> orders = customerOrderBp.getAllOrderIdsWithShipToId(shipToId);
        orders.remove(customerOrder.getId());
        CommandContext ctxCMD;
        executor.init();
        for (Long order : orders) {
        	try {
	        	customerOrder = customerOrderBp.findById(order, CustomerOrder.class);
	        	customerOrder.getAddress().setPendingShipToAuthorization(false);
	        	addressBp.update(customerOrder.getAddress());
	        	logger.info("Releasing orderid=" + order);
	            ctxCMD = new CommandContext();
	            ctxCMD.setData("commandKey", UUID.randomUUID().toString());
	            ctxCMD.setData("idType", "CustomerOrder");
	            //TODO: per PHOEN-4493, use the generated ID for the order process instead 
	        	//of assuming the process ID is the same as the order ID
	            ctxCMD.setData("id", order);
	            ctxCMD.setData("type", SignalCommandConstants.SHIPTO_AUTHORIZED_REF);
	            ctxCMD.setData("event", shipToId);
	            executor.scheduleRequest("SignalEventCommand", ctxCMD);
        	}
        	catch (Exception e) {
        		logger.error("Could not release order " + order, e);
        	}
        }
        executor.destroy();
        return toReturn; 
    }
}
