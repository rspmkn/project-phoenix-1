package com.apd.phoenix.service.brms.impl;

import java.util.Map;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.brms.api.AbstractBRMSCommand;
import com.apd.phoenix.service.business.CommentBp;
import com.apd.phoenix.service.business.CustomerOrderBp;
import com.apd.phoenix.service.executor.api.CommandContext;
import com.apd.phoenix.service.executor.api.ExecutionResults;
import com.apd.phoenix.service.model.CustomerOrder;
import com.apd.phoenix.service.utility.ErrorEmailService;
import com.apd.phoenix.service.workflow.WorkflowService;
import org.kie.api.runtime.process.ProcessInstance;

@Named(value = "StartProcessCommand")
@Stateless
@LocalBean
public class StartProcessCommand extends AbstractBRMSCommand {

    private static final Logger LOG = LoggerFactory.getLogger(StartProcessCommand.class);

    @Inject
    private CustomerOrderBp customerOrderBp;

    @Inject
    private ErrorEmailService errorEmailService;

    @Inject
    private WorkflowService workflowService;

    @Inject
    private CommentBp commentBp;

    @Override
    public ExecutionResults execute_safe(CommandContext ctx) throws Exception {
        String processId = (String) ctx.getData("processName");

        if (processId == null) {
            throw new Exception("PROCESS ID NOT SPECIFIED EXCEPTION");
        }
        //TODO: per PHOEN-4493, use the generated ID for the order process instead 
        //of assuming the process ID is the same as the order ID
        CustomerOrder order = customerOrderBp.findById(this.id, CustomerOrder.class);
        if (order != null && !customerOrderBp.isInWorkflowLimbo(order)) {
            LOG.error("Order {} was already in workflow", order.getApdPo().getValue());
            errorEmailService.sendWorkflowResubmittedErrorEmail(order);
            workflowService.processOrderUpdate(order.getId());
            commentBp.addSystemComment(order, "A message sending this order to workflow "
                    + "was made after the order was already in workflow. That message " + "was discarded.");
            return new ExecutionResults();
        }

        @SuppressWarnings("unchecked")
        Map<String, Object> params = (Map<String, Object>) ctx.getData("processParams");

        ProcessInstance processInstance = session.createProcessInstance(processId, params);
        commandHelper.setRecordProcessId(this.id, processInstance.getId());

        try {
            session.startProcessInstance(processInstance.getId());
        }
        catch (Exception e) {
            LOG.error("Caught exception while trying to reach safe state", e);
        }

        ExecutionResults executionResults = new ExecutionResults();
        return executionResults;
    }
}
