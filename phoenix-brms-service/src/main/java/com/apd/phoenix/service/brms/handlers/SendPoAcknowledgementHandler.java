package com.apd.phoenix.service.brms.handlers;

import java.util.HashMap;
import java.util.Map;
import javax.inject.Inject;
import org.kie.api.runtime.process.WorkItem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.business.CustomerOrderBp;
import com.apd.phoenix.service.business.LineItemStatusBp;
import com.apd.phoenix.service.business.OrderProcessLookupBp;
import com.apd.phoenix.service.message.api.MessageService;
import com.apd.phoenix.service.model.Credential;
import com.apd.phoenix.service.model.CustomerOrder;
import com.apd.phoenix.service.model.MessageMetadata.MessageType;
import com.apd.phoenix.service.model.MiscShipTo;
import com.apd.phoenix.service.model.dto.LineItemDto;
import com.apd.phoenix.service.model.dto.MiscShipToDto;
import com.apd.phoenix.service.model.dto.POAcknowledgementDto;
import com.apd.phoenix.service.model.dto.PurchaseOrderDto;
import com.apd.phoenix.service.model.dto.ShipmentDto;

public class SendPoAcknowledgementHandler extends SimpleAbortWorkItemHandler {

    private static final Logger logger = LoggerFactory.getLogger(SendPoAcknowledgementHandler.class);

    @Inject
    CustomerOrderBp customerOrderBp;

    @Inject
    MessageService messageService;

    @Inject
    LineItemStatusBp lineItemStatusBp;

    @Inject
    private OrderProcessLookupBp processLookupBp;

    @Override
    public Map<String, Object> obtainResults(WorkItem wi) {
        Map<String, Object> toReturn = new HashMap<>();
        //the variable name "orderId" is a misnomer, a legacy from when the process ID was set to the order ID
        CustomerOrder customerOrder = processLookupBp.getOrderFromProcessId((Long) wi.getParameter("orderId"));
        ShipmentDto shipmentDto = (ShipmentDto) wi.getParameter("shipmentDto");
        POAcknowledgementDto poAcknowledgementDto;
        
        if (shipmentDto != null) {
        	//shipment DTO is not null if the acknowledgement is being generated after receiving an advance shipment notice
        	poAcknowledgementDto = customerOrderBp.createAcknowledgment(shipmentDto, customerOrder);
        }
        else {
        	poAcknowledgementDto = customerOrderBp.createFullAcknowledgement(customerOrder);
        }
        
        poAcknowledgementDto = customerOrderBp.populateOrderAcknowledgement(customerOrder,
				poAcknowledgementDto);
        Credential credential = customerOrder.getCredential();
    	messageService.sendPurchaseOrderAcknowledgment(poAcknowledgementDto, credential.getPoAcknowledgementMessageType());
        if (credential.getPoAcknowledgementMessageType() == null || !credential.getPoAcknowledgementMessageType().equals(MessageType.EMAIL)) {
        	messageService.sendPurchaseOrderAcknowledgment(poAcknowledgementDto, MessageType.EMAIL);
        }

        toReturn.put("shipmentOutDto", shipmentDto);
        return toReturn;
    }
}
