package com.apd.phoenix.service.brms.handlers;

import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import org.kie.api.runtime.process.WorkItem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.business.CustomerOrderBp;
import com.apd.phoenix.service.business.LineItemBp;
import com.apd.phoenix.service.business.LineItemStatusBp;
import com.apd.phoenix.service.business.OrderProcessLookupBp;
import com.apd.phoenix.service.model.CustomerOrder;
import com.apd.phoenix.service.model.LineItem;
import com.apd.phoenix.service.model.LineItemStatus;
import com.apd.phoenix.service.model.dto.LineItemDto;

@Stateless
@LocalBean
public class UpdateLineItemStatusWorkItemHandler extends SimpleAbortWorkItemHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(UpdateLineItemStatusWorkItemHandler.class);

    @Inject
    CustomerOrderBp customerOrderBp;

    @Inject
    LineItemStatusBp lineItemStatusBp;

    @Inject
    LineItemBp lineItemBp;

    @Inject
    private OrderProcessLookupBp processLookupBp;

    @Override
    public Map<String, Object> obtainResults(WorkItem wi) {
        Map<String, Object> toReturn = new HashMap<>();

        //the variable name "orderId" is a misnomer, a legacy from when the process ID was set to the order ID
        CustomerOrder order = processLookupBp.getOrderFromProcessId((Long) wi.getParameter("orderId"));
        LineItemDto lineItemDto = (LineItemDto) wi.getParameter("lineItemDto");
        LineItemStatus lineItemStatus = null;
        LineItem lineItem = null;
        String note = null;
        if (lineItemDto == null){
        	return toReturn;
        } else if (lineItemDto != null){
            lineItem = lineItemBp.retrieveLineItem(lineItemDto, order);
            LOGGER.info("Checking for value=" + lineItemDto.getStatus().getValue());
            note = lineItemDto.getStatus().getDescription();
            lineItemStatus = lineItemStatusBp.getStatusByValue(lineItemDto.getStatus().getValue());
        }
        if (lineItem == null || (lineItem.getStatus() != null && lineItem.getStatus().getValue().equals("EXCHANGED"))) {
        	LOGGER.error("Line item is null, or an exchange, could not update line item status");
        	return null;
        }
        
        if (lineItemDto.getBackorderedQuantity() != null && lineItemDto.getBackorderedQuantity().compareTo(BigInteger.ZERO) == 1){
        	lineItem.setBackorderedQuantity(lineItemDto.getBackorderedQuantity());
        }
        
        lineItemBp.setStatus(lineItem, lineItemStatus, note);
        
        return toReturn;
    }
}
