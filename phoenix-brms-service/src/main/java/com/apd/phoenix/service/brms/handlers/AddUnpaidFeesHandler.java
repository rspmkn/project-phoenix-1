package com.apd.phoenix.service.brms.handlers;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.inject.Inject;
import org.kie.api.runtime.process.WorkItem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.business.ShipmentBp;
import com.apd.phoenix.service.model.LineItem;
import com.apd.phoenix.service.model.dto.LineItemDto;
import com.apd.phoenix.service.model.dto.LineItemStatusDto;
import com.apd.phoenix.service.model.dto.LineItemXShipmentDto;
import com.apd.phoenix.service.model.dto.PurchaseOrderDto;
import com.apd.phoenix.service.model.dto.ShipmentDto;
import com.apd.phoenix.service.model.factory.DtoFactory;

public class AddUnpaidFeesHandler extends SimpleAbortWorkItemHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(AddUnpaidFeesHandler.class);

    @Inject
    private ShipmentBp shipmentBp;

    @Override
	public Map<String, Object> obtainResults(WorkItem wi) {
		Map<String, Object> results = new HashMap<>();
		ShipmentDto shipmentDto = (ShipmentDto) wi.getParameter("inputShipmentDto");
		
		//TODO: fix for backwards compatibility with BRMS. The last snapshot that needs this is 1.3.1661-HOTFIXb75
		if (shipmentDto == null) {
			shipmentDto = (ShipmentDto) wi.getParameter("shipmentDto");
		}
		
		//adds any unpaid fees to the shipment
		for (LineItem item : shipmentBp.getOrderFromShipmentDto(shipmentDto).getItems()) {
			boolean onShipment = false;
			for (LineItemXShipmentDto lixsDto : shipmentDto.getLineItemXShipments()) {
				if (item.getApdSku() != null && item.getApdSku().equals(lixsDto.getLineItemDto().getApdSku())) {
					onShipment = true;
				}
			}
			if (item.isFee() && item.getQuantity() > item.getQuantityShipped() + item.getQuantityCancelled() && !onShipment) {
				LineItemXShipmentDto feeDto = new LineItemXShipmentDto();
				feeDto.setLineItemDto(DtoFactory.createLineItemDto(item));
				feeDto.setPaid(false);
				feeDto.setQuantity(new BigInteger("" + (item.getQuantity() - item.getQuantityShipped() - item.getQuantityCancelled())));
				feeDto.setQuantityDelivered(0);
				feeDto.setShipping(item.getEstimatedShippingAmount());
				feeDto.getLineItemDto().setCustomerLineNumber(item.getCustomerLineNumber());
				shipmentDto.getLineItemXShipments().add(feeDto);
				//TODO: fix for backwards compatibility with BRMS. The last snapshot that needs this is 1.3.1661-HOTFIXb75
				if (wi.getParameter("inputShipmentDto") == null) {
					feeDto.setValid(true);
					feeDto.getLineItemDto().setValid(true);
					feeDto.getLineItemDto().setStatus(new LineItemStatusDto());
					feeDto.getLineItemDto().getStatus().setValue("FULLY_SHIPPED");
				}
				//shipmentPrice and totalTaxPrice aren't necessary on a shipment.
				LOGGER.info("Adding an unpaid fee to a shipment: " + item.getApdSku());
			}
		}
		
		//then, removes any items with an indicated ship quantity of zero
		List<LineItemXShipmentDto> toRemove = new ArrayList<>();
		for (LineItemXShipmentDto lixs : shipmentDto.getLineItemXShipments()) {
			if (lixs.getQuantity() == null || lixs.getQuantity().compareTo(BigInteger.ZERO) == 0) {
				LOGGER.info("Removing item with shipped quantity of zero: " + lixs.getLineItemDto().getApdSku());
				toRemove.add(lixs);
			}
		}
		shipmentDto.getLineItemXShipments().removeAll(toRemove);

		results.put("outputShipmentDto", shipmentDto);
		return results;
	}
}
