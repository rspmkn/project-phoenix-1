package com.apd.phoenix.service.brms.api;

import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.process.WorkItemHandler;
import org.kie.api.task.TaskService;

public interface BRMSCommandHelper {

    public KieSession getKieSession(long id) throws Exception;

    public void setRecordProcessId(long recordId, long processId);

    public boolean isStale(long recordId);

    public TaskService getTaskService();

    public void registerHandlers(KieSession session, WorkItemHandler humanTaskHandler);

    public void finishedWith(long id);

    public long getProcessId(long recordId);

    public Object getFact(long id);

    public void setProcessVariables(KieSession kieSession, long processId);
}
