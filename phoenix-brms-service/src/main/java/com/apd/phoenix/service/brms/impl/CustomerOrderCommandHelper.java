package com.apd.phoenix.service.brms.impl;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import org.jbpm.workflow.instance.WorkflowProcessInstance;
import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.process.WorkItemHandler;
import org.kie.api.task.TaskService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.brms.api.BRMSCommandHelper;
import com.apd.phoenix.service.business.CustomerOrderBp;
import com.apd.phoenix.service.business.OrderProcessLookupBp;
import com.apd.phoenix.service.model.CustomerOrder;
import com.apd.phoenix.service.model.dto.PurchaseOrderDto;
import com.apd.phoenix.service.model.factory.DtoFactory;
import com.apd.phoenix.service.model.factory.DtoFactory.ParsingException;

@Stateless
@LocalBean
public class CustomerOrderCommandHelper implements BRMSCommandHelper {

    private static final Logger logger = LoggerFactory.getLogger(CustomerOrderCommandHelper.class);

    @Inject
    BRMSServiceImpl apdBRMSService;

    @Inject
    CustomerOrderBp customerOrderBp;

    @Inject
    private OrderProcessLookupBp orderProcessBp;

    @Override
    public KieSession getKieSession(long orderId) throws Exception {
        return apdBRMSService.getKieSessionByOrderId(orderId);
    }

    @Override
    public TaskService getTaskService() {
        return apdBRMSService.getTaskServiceClient();
    }

    @Override
    public void registerHandlers(KieSession session, WorkItemHandler humanTaskHandler) {
        apdBRMSService.registerHandlers(session, humanTaskHandler);
    }

    @Override
    public void finishedWith(long id) {
        apdBRMSService.setSessionInactiveByOrderId(id);
    }

    @Override
    public void setRecordProcessId(long id, long processId) {
        apdBRMSService.setOrderRecordProcessId(id, processId);
    }

    @Override
    public boolean isStale(long orderId) {
        return apdBRMSService.isOrderStale(orderId);
    }

    @Override
    public long getProcessId(long recordId) {
        return apdBRMSService.getOrderProcessId(recordId);
    }

    @Override
    public Object getFact(long processId) {
        return this.getPurchaseOrderDto(processId);
    }

    @Override
    public void setProcessVariables(KieSession kieSession, long processId) {

        WorkflowProcessInstance wpi = (WorkflowProcessInstance) kieSession.getProcessInstance(processId);

        wpi.setVariable("purchaseOrder", this.getPurchaseOrderDto(processId));
    }

    private PurchaseOrderDto getPurchaseOrderDto(long processId) {

        PurchaseOrderDto toReturn = null;
        try {
            CustomerOrder order = orderProcessBp.getOrderFromProcessId(processId);
            if (order != null) {
                toReturn = DtoFactory.createPurchaseOrderDto(order);
            }
            else {
                throw new ParsingException("Null order when searching with ID " + processId);
            }
        }
        catch (ParsingException e) {
            logger.error("Cannot create purchase order fact from order=" + processId);
        }

        return toReturn;
    }

}
