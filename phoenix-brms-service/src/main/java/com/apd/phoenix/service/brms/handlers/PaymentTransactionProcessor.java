/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.service.brms.handlers;

import java.util.Map;
import java.util.Set;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.business.LineItemXShipmentBp;
import com.apd.phoenix.service.model.CustomerOrder;
import com.apd.phoenix.service.model.LineItemXShipment;
import com.apd.phoenix.service.model.dto.ShipmentDto;
import com.apd.phoenix.service.payment.ws.CreditCardTransactionResult;
import com.apd.phoenix.service.payment.ws.CreditCardTransactionStatusResult;
import com.apd.phoenix.service.payment.ws.client.PaymentGatewayService;
import com.apd.phoenix.service.payment.ws.client.PaymentGatewayTransaction;

/**
 * This class performs the payment gateway transaction and, if the transaction is successful, marks the LineItemXShipments as "paid". 
 * This is done in a separate transaction in a different class in order to prevent double-charging the customer if the parent transaction 
 * is rolled back later in workflow.
 * 
 * @author RHC
 *
 */
@Stateless
@LocalBean
@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
public class PaymentTransactionProcessor {

    private static final Logger logger = LoggerFactory.getLogger(PaymentTransactionProcessor.class);

    @Inject
    private PaymentGatewayService paymentGatewayService;

    @Inject
    private LineItemXShipmentBp lineItemXShipmentBp;

    public CreditCardTransactionResult processAuthorizedPaymentGatewayTransaction(
            PaymentGatewayTransaction paymentGatewayTransaction, CustomerOrder customerOrder,
            Set<LineItemXShipment> lixsSet, ShipmentDto shipmentDto) {
        CreditCardTransactionResult creditCardTransactionResult = null;
        //Attempt to charge the card
        try {
            creditCardTransactionResult = paymentGatewayService.capture(paymentGatewayTransaction, lixsSet);
        }
        catch (Exception e) {
            logger
                    .error(
                            "Error when processing credit card transaction (probably due to missing card vault token on card information)",
                            e);
        }

        if (creditCardTransactionResult != null && creditCardTransactionResult.isSucceeded() != null
                && creditCardTransactionResult.isSucceeded()) {
            for (LineItemXShipment lixs : lixsSet) {
                LineItemXShipment newItem = lineItemXShipmentBp.findById(lixs.getId(), LineItemXShipment.class);
                newItem.setPaid(true);
                lineItemXShipmentBp.update(newItem);
            }
        }
        return creditCardTransactionResult;
    }

    public CreditCardTransactionStatusResult getTransactionStatus(String transactionId) {
        return paymentGatewayService.getTransactionStatus(transactionId);
    }

    /**
     * Set the ID of the attempted transaction, in case a timeout occurs and we need to
     * check if the transaction went through
     * 
     * @param transactionId
     * @param lixsSet
     */
    public void setCreditCardTransactionId(String transactionId, Set<LineItemXShipment> lixsSet) {
        for (LineItemXShipment lixs : lixsSet) {
            LineItemXShipment newItem = lineItemXShipmentBp.findById(lixs.getId(), LineItemXShipment.class);
            newItem.setCcTransactionId(transactionId);
            lineItemXShipmentBp.update(newItem);
        }
    }

    /**
     * If previous transaction was a success, but this is a retry, sets "paid" to true, to ensure there isn't
     * another attempt to charge the item.
     * 
     * @param transactionId
     * @param lixsSet
     */
    public void updateFromPreviousTransaction(Map<String, Boolean> transactionIdToSuccess,
            Set<LineItemXShipment> lixsSet) {
        if (transactionIdToSuccess == null || lixsSet == null) {
            return;
        }
        for (LineItemXShipment lixs : lixsSet) {
            Boolean transactionStatus = transactionIdToSuccess.get(lixs.getCcTransactionId());
            if (StringUtils.isNotBlank(lixs.getCcTransactionId()) && transactionStatus != null && transactionStatus) {
                LineItemXShipment newItem = lineItemXShipmentBp.findById(lixs.getId(), LineItemXShipment.class);
                newItem.setPaid(true);
                lineItemXShipmentBp.update(newItem);
            }
        }
    }
}
