/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.service.brms.handlers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import org.kie.internal.SystemEventListenerFactory;
import org.kie.api.runtime.process.WorkItem;
import org.kie.api.task.model.Status;
import org.kie.api.task.TaskService;
import org.kie.api.task.model.TaskSummary;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.brms.core.api.TaskServiceClientFactory;

@Stateless
@LocalBean
public class CancelAllHumanTasksHandler extends SimpleAbortWorkItemHandler {

    private static final Logger logger = LoggerFactory.getLogger(CancelAllHumanTasksHandler.class);

    @Inject
    TaskServiceClientFactory taskServiceClientFactory;

    @Override
    public Map<String, Object> obtainResults(WorkItem wi) {
        Map<String, Object> toReturn = new HashMap<>();
        long processId = wi.getProcessInstanceId();
        TaskService client = taskServiceClientFactory.getTaskService();

        List<Status> statusList = new ArrayList<>();
        statusList.add(Status.Created);
        statusList.add(Status.InProgress);
        statusList.add(Status.Ready);
        statusList.add(Status.Reserved);
        statusList.add(Status.Suspended);

        List<TaskSummary> tasks = client.getTasksByStatusByProcessInstanceId(processId, statusList, "en-UK");
        
        for (TaskSummary summary : tasks){
            logger.info("Exiting taskid=" + summary.getId() + " as Administrator!");
            client.exit(summary.getId(), "Administrator");
        }

        return toReturn;
    }
}
