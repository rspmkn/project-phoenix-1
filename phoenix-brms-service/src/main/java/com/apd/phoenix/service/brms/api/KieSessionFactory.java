package com.apd.phoenix.service.brms.api;

import javax.ejb.Stateless;
import org.kie.api.runtime.KieSession;

@Stateless
public interface KieSessionFactory {

    public KieSession loadKieSession(int sessionId, String snapshot);

    public KieSession newKieSession();
}
