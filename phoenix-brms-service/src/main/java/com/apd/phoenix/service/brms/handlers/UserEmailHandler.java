/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.apd.phoenix.service.brms.handlers;

import com.apd.phoenix.service.business.EmailFactoryBp;
import com.apd.phoenix.service.message.api.Message;
import com.apd.phoenix.service.message.impl.EmailMessageSender;
import com.apd.phoenix.service.model.dto.UserModificationRequestDto;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.inject.Inject;
import org.apache.commons.lang.StringUtils;
import org.kie.api.runtime.process.WorkItem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author anicholson
 */
public class UserEmailHandler extends SimpleAbortWorkItemHandler {

    private static final Logger LOG = LoggerFactory.getLogger(UserEmailHandler.class);

    @Inject
    private EmailFactoryBp emailFactoryBp;

    @Inject
    private EmailMessageSender messageSender;

    @Override
    public Map<String, Object> obtainResults(WorkItem wi) {
        UserModificationRequestDto request = (UserModificationRequestDto) wi.getParameter("request");
        List<Message> messages = emailFactoryBp.createUserModificationEmail(request);
        for (Message m : messages){
        	messageSender.sendMessage(m, null);
        }
        if (request != null && StringUtils.isNotBlank(request.getInternalEmail())) {
        	messages = emailFactoryBp.createUserModificationEmailForCsr(request);
            for (Message m : messages){
            	messageSender.sendMessage(m, null);
            }
        }
        return new HashMap<>();
    }
}
