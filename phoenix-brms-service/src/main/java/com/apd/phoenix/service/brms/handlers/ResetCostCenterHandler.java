/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.service.brms.handlers;

import com.apd.phoenix.service.business.CustomerOrderBp;
import com.apd.phoenix.service.business.OrderProcessLookupBp;
import com.apd.phoenix.service.model.AssignedCostCenter;
import com.apd.phoenix.service.model.CustomerOrder;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import org.kie.api.runtime.process.WorkItem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Stateless
@LocalBean
public class ResetCostCenterHandler extends SimpleAbortWorkItemHandler {

    private static final Logger logger = LoggerFactory.getLogger(ResetCostCenterHandler.class);

    @Inject
    CustomerOrderBp customerOrderBp;

    @Inject
    private OrderProcessLookupBp processLookupBp;

    @Override
    public Map<String, Object> obtainResults(WorkItem wi) {
        Map<String, Object> toReturn = new HashMap<>();
        //the variable name "orderId" is a misnomer, a legacy from when the process ID was set to the order ID
        CustomerOrder customerOrder = processLookupBp.getOrderFromProcessId((Long) wi.getParameter("orderId"));
        AssignedCostCenter assignedCostCenter = customerOrder.getAssignedCostCenter();
        
        if (assignedCostCenter != null){
            BigDecimal orderTotal = customerOrder.getOrderTotal();
            BigDecimal pendingCharge = assignedCostCenter.getPendingCharge() != null ? assignedCostCenter.getPendingCharge() : BigDecimal.ZERO;
            logger.info("Pending amount before reset:" + pendingCharge);
            BigDecimal potentialAmount = pendingCharge.subtract(orderTotal);
            if (potentialAmount.compareTo(BigDecimal.ZERO) == -1){
                assignedCostCenter.setPendingCharge(BigDecimal.ZERO);
            } else {
                assignedCostCenter.setPendingCharge(potentialAmount);
            }
            logger.info("Pending amount after reset:" + pendingCharge);
            logger.info("Order Total:" + orderTotal);
        }
        return toReturn;
    }
}
