/**
 * 
 */
package com.apd.phoenix.service.brms.impl;

import java.io.Serializable;
import javax.annotation.Resource;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceContext;
import javax.transaction.TransactionManager;
import org.drools.core.impl.EnvironmentFactory;
import org.kie.api.KieBase;
import org.kie.api.runtime.Environment;
import org.kie.api.runtime.EnvironmentName;
import org.kie.api.runtime.KieSession;
import org.kie.internal.persistence.jpa.JPAKnowledgeService;
import org.kie.internal.runtime.StatefulKnowledgeSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.brms.api.KieSessionFactory;
import com.apd.phoenix.service.brms.core.api.KieBaseFactory;

/**
 * @author Red Hat Middleware Consulting. Red Hat, Inc.  Jul 3, 2013
 * 
 *         This class is a wrapper around JPAKnowledgeService. It exposes the
 *         same functions, while hiding the parameters that don't change in our
 *         domain (ksessionconfiguration, environment, and kbase)
 * 
 */
@Stateless
@LocalBean
public class KieSessionFactoryImpl implements Serializable, KieSessionFactory {

    private static final long serialVersionUID = 1L;

    private static final Logger LOG = LoggerFactory.getLogger(KieSessionFactoryImpl.class);

    @Resource(lookup = "java:/TransactionManager")
    private TransactionManager tm;

    @PersistenceContext(unitName = "jbpm")
    private EntityManager em;

    @Inject
    private KieBaseFactory knowledgeBaseFactory;

    @Override
    public KieSession newKieSession() {
        LOG.info("Creating new persistent kie session");

        KieBase kbase = knowledgeBaseFactory.getKieBase();
        Environment env = this.createEnvironment();

        return JPAKnowledgeService.newStatefulKnowledgeSession(kbase, null, env);
    }

    @Override
    public KieSession loadKieSession(int ksessionId, String snapshot) {
        LOG.info("Loading id=" + ksessionId + " persistent knowledge session");

        KieBase kbase = knowledgeBaseFactory.getKieBase(snapshot);
        Environment env = this.createEnvironment();

        return JPAKnowledgeService.loadStatefulKnowledgeSession(ksessionId, kbase, null, env);
    }

    private Environment createEnvironment() {
        LOG.info("EM: " + em.getClass());
        Environment env = EnvironmentFactory.newEnvironment();
        env.set(EnvironmentName.ENTITY_MANAGER_FACTORY, getEmf());
        env.set(EnvironmentName.TRANSACTION_MANAGER, tm);
        return env;
    }

    private EntityManagerFactory getEmf() {
        return em.getEntityManagerFactory();
    }
}
