/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.service.brms.handlers;

import java.util.HashMap;
import java.util.Map;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import org.kie.api.runtime.process.WorkItem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.business.CustomerOrderBp;
import com.apd.phoenix.service.business.LineItemBp;
import com.apd.phoenix.service.model.CanceledQuantity;
import com.apd.phoenix.service.model.CustomerOrder;
import com.apd.phoenix.service.model.LineItem;
import com.apd.phoenix.service.model.dto.LineItemXShipmentDto;
import com.apd.phoenix.service.model.dto.ShipmentDto;

@Stateless
@LocalBean
public class CreateCancelHandler extends SimpleAbortWorkItemHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(CreateCancelHandler.class);

    @Inject
    private CustomerOrderBp customerOrderBp;

    @Inject
    private LineItemBp lineItemBp;

    @Override
    public Map<String, Object> obtainResults(WorkItem wi) {
        Map<String, Object> toReturn = new HashMap<>();
        Long orderId = (Long) wi.getParameter("inputOrderId");
        ShipmentDto shipment = (ShipmentDto) wi.getParameter("inputShipmentDto");
        
        if (orderId != null) {
        	CustomerOrder customerOrder = customerOrderBp.findById(orderId, CustomerOrder.class);
        	if (customerOrder != null && shipment != null) {
        		for (LineItemXShipmentDto item : shipment.getLineItemXShipments()) {
        			LineItem lineItem = null;
        			if (item.getLineItemDto() != null) {
        				lineItem = lineItemBp.retrieveLineItem(item.getLineItemDto(), customerOrder);
        			}
        			if (lineItem == null) {
        				LOGGER.error("Couldn't find a line item for a cancel");
        			}
        			else if (item.getValid() == null || !item.getValid()) {
        				if (LOGGER.isDebugEnabled()) {
        					LOGGER.info("Item cancellation was invalid: " + lineItem.getApdSku());
        				}
        			}
        			else {
	                    CanceledQuantity canceledQuantity = new CanceledQuantity();
	                    canceledQuantity.setLineItem(lineItem);
	                    canceledQuantity.setQuantity(item.getQuantity());
	                    if (item.getVendorComments() != null && !item.getVendorComments().isEmpty()) {
	                    	canceledQuantity.setReason(item.getVendorComments().get(0));
	                    }
	                    lineItem.getCanceledQuantities().add(canceledQuantity);
	                    lineItemBp.update(lineItem);
        			}
        		}
        	}
        }
        
        return toReturn;
    }
}
