package com.apd.phoenix.service.brms.impl;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.process.WorkItemHandler;
import org.kie.api.runtime.process.WorkItemManager;
import org.kie.api.task.TaskService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.core.utility.TenantConfigRepository;
import com.apd.phoenix.service.brms.api.AbstractBRMSService;
import com.apd.phoenix.service.brms.core.api.TaskServiceClientFactory;
import com.apd.phoenix.service.brms.handlers.AddUnpaidFeesHandler;
import com.apd.phoenix.service.brms.handlers.CancelAllHumanTasksHandler;
import com.apd.phoenix.service.brms.handlers.CancelRejectedItemsHandler;
import com.apd.phoenix.service.brms.handlers.ChargeCreditCardHandler;
import com.apd.phoenix.service.brms.handlers.CostCenterApprovalHandler;
import com.apd.phoenix.service.brms.handlers.CreateCancelHandler;
import com.apd.phoenix.service.brms.handlers.CreateShipmentHandler;
import com.apd.phoenix.service.brms.handlers.CreateVendorInvoiceHandler;
import com.apd.phoenix.service.brms.handlers.EmailHandler;
import com.apd.phoenix.service.brms.handlers.InsertOrderHandler;
import com.apd.phoenix.service.brms.handlers.InvoiceCustomerHandler;
import com.apd.phoenix.service.brms.handlers.PreauthorizeCreditCardHandler;
import com.apd.phoenix.service.brms.handlers.RejectOrderHandler;
import com.apd.phoenix.service.brms.handlers.ReleaseOrdersUsingShipToWorkItemHandler;
import com.apd.phoenix.service.brms.handlers.RequestShipToWorkItemHandler;
import com.apd.phoenix.service.brms.handlers.ResetCostCenterHandler;
import com.apd.phoenix.service.brms.handlers.SendPoAcknowledgementHandler;
import com.apd.phoenix.service.brms.handlers.SendShipmentNoticeHandler;
import com.apd.phoenix.service.brms.handlers.SendVendorOrderHandler;
import com.apd.phoenix.service.brms.handlers.ShipUnshippedInvoiceItemsWorkItemHandler;
import com.apd.phoenix.service.brms.handlers.TimerServiceWorkItemHandler;
import com.apd.phoenix.service.brms.handlers.UpdateLineItemStatusWorkItemHandler;
import com.apd.phoenix.service.brms.handlers.UpdateOrderStatusWorkItemHandler;
import com.apd.phoenix.service.brms.handlers.UserEmailHandler;
import com.apd.phoenix.service.brms.handlers.WorkflowLogger;
import com.apd.phoenix.service.business.AccountBRMSRecordBp;
import com.apd.phoenix.service.business.AccountBp;
import com.apd.phoenix.service.business.CustomerOrderBp;
import com.apd.phoenix.service.business.OrderBRMSRecordBp;
import com.apd.phoenix.service.business.UserBRMSRecordBp;
import com.apd.phoenix.service.business.UserRequestBp;
import com.apd.phoenix.service.model.Account;
import com.apd.phoenix.service.model.OrderBRMSRecord;
import com.apd.phoenix.service.model.UserRequest;

@Stateless
@LocalBean
public class BRMSServiceImpl extends AbstractBRMSService {

    @Inject
    OrderBRMSRecordBp orderBRMSRecordBp;

    @Inject
    AccountBRMSRecordBp accountBRMSRecordBp;

    @Inject
    UserBRMSRecordBp userBRMSRecordBp;

    @Inject
    UserRequestBp userRequestBp;

    @Inject
    CustomerOrderBp customerOrderBp;

    @Inject
    AccountBp accountBp;

    @Inject
    TaskServiceClientFactory taskServiceClientFactory;

    @Inject
    UpdateOrderStatusWorkItemHandler updateOrderStatusWorkItemHandler;

    @Inject
    UpdateLineItemStatusWorkItemHandler updateLineItemStatusWorkItemHandler;

    @Inject
    ShipUnshippedInvoiceItemsWorkItemHandler shipUnshippedInvoiceItemsWorkItemHandler;

    @Inject
    RequestShipToWorkItemHandler requestShipToWorkItemHandler;

    @Inject
    ReleaseOrdersUsingShipToWorkItemHandler releaseOrdersUsingShipToWorkItemHandler;

    @Inject
    PreauthorizeCreditCardHandler preauthorizeCreditCardHandler;

    @Inject
    ChargeCreditCardHandler chargeCreditCardHandler;

    @Inject
    CostCenterApprovalHandler costCenterApprovalHandler;

    @Inject
    SendVendorOrderHandler sendVendorOrderHandler;

    @Inject
    CancelAllHumanTasksHandler cancelAllHumanTasksHandler;

    @Inject
    CreateVendorInvoiceHandler createVendorInvoiceHandler;

    @Inject
    InvoiceCustomerHandler invoiceCustomerHandler;

    @Inject
    EmailHandler emailHandler;

    @Inject
    InsertOrderHandler insertOrderHandler;

    @Inject
    ResetCostCenterHandler resetCostCenterHandler;

    @Inject
    CreateShipmentHandler createShipmentHandler;

    @Inject
    private AddUnpaidFeesHandler addUnpaidFeesHandler;

    @Inject
    private CreateCancelHandler createCancelHandler;

    @Inject
    private CancelRejectedItemsHandler cancelRejectedItemsHandler;

    @Inject
    SendPoAcknowledgementHandler sendPoAcknowledgementHandler;

    @Inject
    SendShipmentNoticeHandler sendShipmentNoticeHandler;

    @Inject
    UserEmailHandler userEmailHandler;

    @Inject
    WorkflowLogger workflowLogger;

    @Inject
    TimerServiceWorkItemHandler timerServiceWorkItemHandler;

    private static final Logger LOG = LoggerFactory.getLogger(BRMSServiceImpl.class);

    @Inject
    RejectOrderHandler rejectOrderHandler;

    /**
     * Checks for a session id for an order id. If one does not exist then it
     * will create one and add an entry to..
     * 
     * @param orderId
     * @return
     * @throws Exception
     */
    public KieSession getKieSessionByOrderId(long orderId) throws Exception {

        KieSession session = null;
        Boolean isActive = orderBRMSRecordBp.isActive(orderId);
        Boolean replaceSession = orderBRMSRecordBp.getReplaceSession(orderId);
        LOG.info("after check active for order {}, result: {}", orderId, isActive);
        if (isActive == null) {
            //new orders have a new session created
            session = newKieSession();
            orderBRMSRecordBp.createRecord(orderId, session.getId(), true);
        }
        else if (replaceSession != null && replaceSession) {
            //for orders with the "replaceSession" flag set to true, the session is replaced
            session = newKieSession();
            OrderBRMSRecord record = orderBRMSRecordBp.getRecordByOrderId(orderId);
            if (record != null) {
                orderBRMSRecordBp.delete(record.getId(), OrderBRMSRecord.class);
            }
            orderBRMSRecordBp.createRecord(orderId, session.getId(), true);

        }
        else {

            int sessionId = orderBRMSRecordBp.getSessionId(orderId);
            String snapshot = orderBRMSRecordBp.getSnapshot(orderId);
            session = loadKieSession(sessionId, snapshot);
            orderBRMSRecordBp.setActive(orderId, true);
        }

        return session;
    }

    public KieSession getKieSessionByAccountId(long accountId) {
        if (!accountExists(accountId)) {
            // throw Exception;
        }
        KieSession session = null;
        Boolean isActive = accountBRMSRecordBp.isActive(accountId);

        if (isActive == null) {
            session = newKieSession();
            accountBRMSRecordBp.createRecord(accountId, session.getId(), true);
        }
        else if (isActive) {
            // throw StatefulKnowledgeSessionInUseException
        }
        else {
            int sessionId = accountBRMSRecordBp.getSessionId(accountId);
            String snapshot = accountBRMSRecordBp.getSnapshot(accountId);
            session = loadKieSession(sessionId, snapshot);
            accountBRMSRecordBp.setActive(accountId, true);
        }

        return session;
    }

    public KieSession getKieSessionByUserRequestId(long id) {
        if (!userRequestExists(id)) {
            // throw Exception;
        }
        KieSession session = null;
        Boolean isActive = userBRMSRecordBp.isActive(id);

        if (isActive == null) {
            session = newKieSession();

            userBRMSRecordBp.createRecord(id, session.getId(), true);
        }
        else if (isActive) {
            // throw StatefulKnowledgeSessionInUseException
        }
        else {
            int sessionId = userBRMSRecordBp.getSessionId(id);
            String snapshot = userBRMSRecordBp.getSnapshot(id);
            session = loadKieSession(sessionId, snapshot);
            userBRMSRecordBp.setActive(id, true);
        }

        return session;
    }

    @Override
    public TaskService getTaskServiceClient() {
        return taskServiceClientFactory.getTaskService();
    }

    public void registerHandlers(KieSession session, WorkItemHandler humanTaskHandler) {
        Map<String, WorkItemHandler> handlers = new HashMap<>();
        handlers.put("updateLineItemStatus", updateLineItemStatusWorkItemHandler);
        handlers.put("Human Task", humanTaskHandler);
        handlers.put("ShipUnshippedInvoiceItems", shipUnshippedInvoiceItemsWorkItemHandler);
        handlers.put("Updateorderstatus", updateOrderStatusWorkItemHandler);
        handlers.put("RequestShipTo", requestShipToWorkItemHandler);
        handlers.put("ReleaseOrdersUsingShipTo", releaseOrdersUsingShipToWorkItemHandler);
        handlers.put("preauthorizeCC", preauthorizeCreditCardHandler);
        handlers.put("Chargecreditcard", chargeCreditCardHandler);
        handlers.put("costCenterApproval", costCenterApprovalHandler);
        handlers.put("SendVendorOrder", sendVendorOrderHandler);
        handlers.put("cancelAllHumanTasks", cancelAllHumanTasksHandler);
        handlers.put("CreateVendorInvoice", createVendorInvoiceHandler);
        handlers.put("InvoiceCustomer", invoiceCustomerHandler);
        handlers.put("sendEmail", emailHandler);
        handlers.put("insertOrder", insertOrderHandler);
        handlers.put("ResetCostCenter", resetCostCenterHandler);
        handlers.put("createShipment", createShipmentHandler);
        handlers.put("addUnpaidFees", addUnpaidFeesHandler);
        handlers.put("sendOrderAck", sendPoAcknowledgementHandler);
        handlers.put("sendShipment", sendShipmentNoticeHandler);
        handlers.put("sendUserEmail", userEmailHandler);
        handlers.put("workflowLogger", workflowLogger);
        handlers.put("timerServiceDelay", timerServiceWorkItemHandler);
        handlers.put("RejectOrder", rejectOrderHandler);
        handlers.put("createCancel", createCancelHandler);
        handlers.put("cancelRejectedItems", cancelRejectedItemsHandler);
        
        WorkItemManager manager = session.getWorkItemManager();
        for ( Entry<String, WorkItemHandler> entry : handlers.entrySet()){
            manager.registerWorkItemHandler(entry.getKey(), entry.getValue());
        }
    }

    private boolean accountExists(long accountId) {
        return accountBp.findById(accountId, Account.class) != null;
    }

    private boolean userRequestExists(long id) {
        return userRequestBp.findById(id, UserRequest.class) != null;
    }

    public void setSessionInactiveByOrderId(long id) {
        orderBRMSRecordBp.setActive(id, false);
    }

    public void setSessionInactiveByAccountId(long id) {
        accountBRMSRecordBp.setActive(id, false);
    }

    public void setSessionInactiveByUserRequestId(long id) {
        userBRMSRecordBp.setActive(id, false);
    }

    public void setAccountRecordProcessId(long id, long processId) {
        accountBRMSRecordBp.setProcessId(id, processId);
    }

    public void setOrderRecordProcessId(long id, long processId) {
        orderBRMSRecordBp.setProcessId(id, processId);
    }

    public void setUserRequestRecordProccessId(long id, long processId) {
        userBRMSRecordBp.setProcessId(id, processId);
    }

    public boolean isAccountStale(long accountId) {
        return accountBRMSRecordBp.isStale(accountId);
    }

    public boolean isOrderStale(long orderId) {
        return orderBRMSRecordBp.isStale(orderId);
    }

    public boolean isUserRequestStale(long id) {
        return userBRMSRecordBp.isStale(id);
    }

    public long getAccountProcessId(long accountId) {
        return accountBRMSRecordBp.getProcessId(accountId);
    }

    public long getOrderProcessId(long orderId) {
        Long processId = orderBRMSRecordBp.getProcessId(orderId);
        if (processId == null) {
            LOG.error("No Order BRMS Record found for order with database id: " + orderId + ". "
                    + "This is probably caused by the order never having been placed in workflow.");
        }
        return processId;
    }

    public long getUserRequestProccessId(long id) {
        return userBRMSRecordBp.getProcessId(id);
    }

    public long getUserRequestIdByToken(String token) {
        return userRequestBp.getIdbyToken(token);
    }
}
