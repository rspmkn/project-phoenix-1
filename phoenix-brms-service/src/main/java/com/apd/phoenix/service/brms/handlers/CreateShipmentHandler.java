package com.apd.phoenix.service.brms.handlers;

import java.util.HashMap;
import java.util.Map;
import javax.inject.Inject;
import org.kie.api.runtime.process.WorkItem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.business.ShipmentBp;
import com.apd.phoenix.service.model.Shipment;
import com.apd.phoenix.service.model.dto.ShipmentDto;

public class CreateShipmentHandler extends SimpleAbortWorkItemHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(CreateShipmentHandler.class);

    @Inject
    ShipmentBp shipmentBp;

    @Override
	public Map<String, Object> obtainResults(WorkItem wi) {
		Map<String, Object> results = new HashMap<>();
		ShipmentDto shipmentDto = (ShipmentDto) wi.getParameter("shipmentDto");
		Shipment shipment = shipmentBp.persistShipment(shipmentDto);
                if(shipment!=null){
                    shipmentDto.setShipmentDatabaseKey(shipment.getId());
                }
                results.put("shipmentDtoOut", shipmentDto);
		return results;
	}
}
