package com.apd.phoenix.service.brms.impl;

import javax.inject.Inject;
import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.process.WorkItemHandler;
import org.kie.api.task.TaskService;
import com.apd.phoenix.service.brms.api.BRMSCommandHelper;

public class UserRequestCommandHelper implements BRMSCommandHelper {

    @Inject
    BRMSServiceImpl apdBRMSService;

    @Override
    public KieSession getKieSession(long id) throws Exception {
        return apdBRMSService.getKieSessionByUserRequestId(id);
    }

    @Override
    public void setRecordProcessId(long userRequestId, long processId) {
        apdBRMSService.setUserRequestRecordProccessId(userRequestId, processId);
    }

    @Override
    public TaskService getTaskService() {
        return apdBRMSService.getTaskServiceClient();
    }

    @Override
    public void registerHandlers(KieSession session, WorkItemHandler humanTaskHandler) {
        apdBRMSService.registerHandlers(session, humanTaskHandler);
    }

    @Override
    public void finishedWith(long id) {
        apdBRMSService.setSessionInactiveByUserRequestId(id);
    }

    @Override
    public boolean isStale(long id) {
        return apdBRMSService.isUserRequestStale(id);
    }

    @Override
    public long getProcessId(long id) {
        return apdBRMSService.getUserRequestProccessId(id);
    }

    public long getIdByToken(String token) {
        return apdBRMSService.getUserRequestIdByToken(token);
    }

    @Override
    public Object getFact(long userRequestId) {
        return userRequestId;
    }

    @Override
    public void setProcessVariables(KieSession kieSession, long processId) {
        // do nothing

    }

}
