package com.apd.phoenix.service.brms.impl;

import javax.inject.Inject;
import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.process.WorkItemHandler;
import org.kie.api.task.TaskService;
import com.apd.phoenix.service.brms.api.BRMSCommandHelper;

public class AccountCommandHelper implements BRMSCommandHelper {

    @Inject
    BRMSServiceImpl apdBRMSService;

    @Override
    public KieSession getKieSession(long accountId) {
        return apdBRMSService.getKieSessionByAccountId(accountId);
    }

    @Override
    public TaskService getTaskService() {
        return apdBRMSService.getTaskServiceClient();
    }

    @Override
    public void registerHandlers(KieSession session, WorkItemHandler humanTaskHandler) {
        apdBRMSService.registerHandlers(session, humanTaskHandler);
    }

    @Override
    public void finishedWith(long id) {
        apdBRMSService.setSessionInactiveByAccountId(id);
    }

    @Override
    public void setRecordProcessId(long id, long processId) {
        apdBRMSService.setAccountRecordProcessId(id, processId);
    }

    @Override
    public boolean isStale(long recordId) {
        return false;
    }

    @Override
    public long getProcessId(long recordId) {
        return apdBRMSService.getAccountProcessId(recordId);
    }

    @Override
    public Object getFact(long recordId) {
        return recordId;
    }

    @Override
    public void setProcessVariables(KieSession kieSession, long processId) {
        // do nothing
    }

}
